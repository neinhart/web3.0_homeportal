/**
 * Created by Son Daejoong on 2016-12-07.
 */


module.exports = function (grunt) {
    var BASIC_MODULE_INFO = [
        {
            "moduleId": "module.kids",
            "moduleName": "키즈 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.kids/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.family_home",
            "moduleName": "우리집 맞춤 TV 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.family_home/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.subhome",
            "moduleName": "서브홈 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.subhome/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.app_game",
            "moduleName": "앱/게임 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.app_game/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.search",
            "moduleName": "검색 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.search/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.channel_payment",
            "moduleName": "채널 결제 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.channel_payment/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.vod_payment",
            "moduleName": "VOD 결제 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.vod_payment/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.vod",
            "moduleName": "VOD 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.vod/",
            "serverLoading": "false"
        },
        {
            "moduleId": "module.notice",
            "moduleName": "인트로UI 모듈",
            "version": "1.0.0",
            "state": 1,
            "localVersion": "1.0.0",
            "localPath": "module/module.notice/",
            "serverLoading": "false"
        }
    ];

    var Zip = require('node-7z');

    var destZipFileName = "";

    var isTestBuild = false;

    function addZero (num, digit) {

        var strNum = "" + num;
        var resultNum = strNum;

        if (strNum.length < digit) {
            for (var i = strNum.length; i < digit; i++) {
                resultNum = "0" + resultNum;
            }
        }

        return resultNum;
    }


    function zipFileName (options) {
        var date = new Date();
        var stamp = addZero(date.getFullYear(), 4) + addZero((date.getMonth() + 1), 2) + addZero(date.getDate(), 2);
                    //+ addZero(date.getHours(), 2) + addZero(date.getMinutes(), 2) + addZero(date.getSeconds(), 2);

        if (isTestBuild) {
            destZipFileName = "Test_Web3.0_" + options.stb + "_" + options.version + "_" + stamp + "_" + options.mode;
        }
        else {
            //destZipFileName = "Web3.0_" + options.stb + "_" + options.version + "_" + stamp + "_" + options.mode;
            destZipFileName = options.stb + "_W3_" + options.version + "_" + stamp + "_" + options.mode;
        }

        if (options.log) {
            destZipFileName += "_log"
        }
        destZipFileName += ".wgt";
    }

    var appVersion = grunt.file.readJSON('appVersion.json');
    var moduleVersion = grunt.file.readJSON('moduleVersion.json');

    // Project init
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        clean: {
            build: ["build"],
            temp: ["build/temp"],
            testFile: ["build/temp/images/test"],
            ReleaseConfig: ["build/scripts/ReleaseConfig.js", "build/temp/scripts/ReleaseConfig.js"]
        },

        copy: {
            main: {
                files: [
                    {expand: true, cwd: "KTMain", src: "assets/oc/wm.properties", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "images/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "module/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "styles/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "config.xml", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "index.html", dest: "build/temp/"}
                ]
            },
            containAssets: {
                files: [
                    {expand: true, cwd: "KTMain", src: "assets/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "images/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "module/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "styles/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "config.xml", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "index.html", dest: "build/temp/"}
                ]
            },
            GitLabUpload: {
                files: [
                    {expand: true, cwd: "KTMain", src: "assets/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "images/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "module/ModuleStateTable.json", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "styles/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "config.xml", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "index.html", dest: "build/temp/"}
                ]
            },
            GitLabUpload_For_NonAglify: {
                files: [
                    {expand: true, cwd: "KTMain", src: "scripts/managers/http/**", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "module/ModuleStateTable.json", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/homeImpl/homeImpl.js", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/oipf/adapters/SMIAdapter.js", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/oipf/adapters/VodAdapter.js", dest: "build/temp/"},
                    {expand: true, cwd: "KTMain", src: "scripts/oipf/adapters/WatermarkAdapter.js", dest: "build/temp/"}
                ]
            },
            onlyModules: {
                files: [
                    {expand: true, cwd: "KTMain", src: "module/**", dest: "build/temp/"}
                ]
            }
        },

        uglify: {
            full: {
                options: {
                    mangle: true,
                    compress: {
                        drop_console: false
                    },
                    beautify: false
                },
                expand: true,
                cwd: 'build/temp',
                src: '**/*.js',
                dest: 'build/temp'
            },
            onlyRemoveComments: {
                options: {
                    mangle: false,
                    compress: false,
                    beautify: true,
                    preserveComments: false
                },
                expand: true,
                cwd: 'build/temp',
                src: '**/*.js',
                dest: 'build/temp'
            }
        },

        compress: {
            main: {
                options: {
                    mode: "zip",
                    archive: function () {
                        return "build/" + destZipFileName;
                    }
                },
                expand: true,
                cwd: "build/temp/",
                src: ["index.html", "**/*"]
            }
        }

        //zip: {
        //    'test': {
        //        cwd: 'build/temp/',
        //        src: ['build/temp/index.html', "build/temp/scripts/ReleaseConfig.js"],
        //        dest: 'build/test.wgt'
        //    }
        //}
    });

    // Plugin load
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-compress");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-zip");
    grunt.loadNpmTasks('grunt-zip-directories');
    grunt.loadNpmTasks('grunt-shell');


    grunt.registerTask("default", []);

    grunt.registerTask("makeReleaseConfig", function (arg1, arg2, arg3) {
        var version, frameworkVersion, date, mode, logOutput, logLevel;

        var today = new Date();
        date = addZero(today.getFullYear(), 4) + "." + addZero((today.getMonth() + 1), 2) + "." + addZero(today.getDate(), 2);

        // TODO version 이 정리가 될때까지, build zip 파일끼리 비교 가능하도록 build 시간 추가
        var time = addZero(today.getHours(), 2) + ":" + addZero(today.getMinutes(), 2) + ":" + addZero(today.getMilliseconds(), 2);


        if (arg1 === "OTV") {
            version = appVersion.OTV;
        }
        else if (arg1 === "OTS") {
            version = appVersion.OTS;
        }
        else if (arg1 === "TESTBED") {
            version = appVersion.TESTBED;
        }
        else {
            version = "0.0.0";
        }

        if (isTestBuild) {
            version = "Test_" + version
        }

        frameworkVersion = moduleVersion.FRAMEWORK;

        if (arg2 === "LIVE") {
            mode = "LIVE";
        }
        else if (arg2 === "TESTBED") {
            mode = "TEST";
        }
        else {
            mode = "BMT";
        }

        if (arg3 === "ON") {
            logOutput = "true";
            logLevel = 0;
        }
        else {
            logOutput = "false";
            logLevel = 3;
        }

        var ReleaseConfig = 'var KTW = KTW || {};'
                            + 'KTW.TITLE = "KT TV Web 3.0";'
                            + 'KTW.UI_VERSION = "' + version + '";'
                            /**
                             * TODO 일단 framework 버전은 0.0.0 으로 설정, 추후 framework 버전을 어떻게 작성해야 할지 정해야 함
                             */
                            + 'KTW.FRAMEWORK_VERSION = "' + frameworkVersion + '";'
                            + 'KTW.UI_DATE = "' + date + '";'
                            + 'KTW.UI_UPDATE_TIME = "' + time + '";'
                            + 'KTW.PLATFORM_EXE = window.oipfObjectFactory ? "STB" : "PC";'
                            + 'KTW.RELEASE_MODE = "' + mode + '";'
                            + 'KTW.SUPPORT_DEMO = false;'
                            + 'KTW.LOG_OUTPUT = ' + logOutput + ';'
                            + 'KTW.LOG_LEVEL = ' + logLevel + ';';

        grunt.file.write("build/temp/scripts/ReleaseConfig.js", ReleaseConfig, {encoding: "UTF-8"});
    });

    grunt.registerTask("makeZipFileName", function (version, stb, mode, log, isKTDelivery) {
        if (isKTDelivery) {
            destZipFileName = "src_" + version + "_Web3.0.wgt"
        }
        else {
            zipFileName({
                version: version,
                stb: stb,
                mode: mode,
                log: log === "ON" ? true : false
            });
        }
    });

    grunt.registerTask("zipping", function () {
        var src = [];

        grunt.file.recurse("build/temp/", function (abspath, rootdir, subdir, filename) {
            src.push(abspath);
        });

        grunt.config.set("zip", {
            main: {
                cwd: "build/temp/",
                src: src,
                dest: "build/" + destZipFileName,
                compression: "DEFLATE"
            }
        });

        grunt.task.run(["zip:main"]);
    });

    grunt.registerTask("makeModuleStateTable", function () {
        var moduleStateTable = {
            framework: {
                version: moduleVersion.FRAMEWORK
            },
            module: []
        };

        for (var i = 0; i < BASIC_MODULE_INFO.length; i++) {
            var tmpPath = "build/temp/" + BASIC_MODULE_INFO[i].localPath;

            if (grunt.file.isDir(tmpPath)) {
                var isExist = false;
                var versionIdx = 0;
                for (var j = 0; j < moduleVersion.MODULE.length; j++) {
                    if (BASIC_MODULE_INFO[i].moduleId === moduleVersion.MODULE[j].moduleId) {
                        isExist = true;
                        versionIdx = j;
                        break;
                    }
                }

                if (isExist) {
                    BASIC_MODULE_INFO[i].version = moduleVersion.MODULE[versionIdx].version;
                    BASIC_MODULE_INFO[i].localVersion = moduleVersion.MODULE[versionIdx].version;
                    moduleStateTable.module.push(BASIC_MODULE_INFO[i]);
                }
            }
        }

        grunt.file.write("build/temp/module/ModuleStateTable.json", JSON.stringify(moduleStateTable), {encoding: "UTF-8"});
    });

    /**
     * [dj.son] framework & 모듈 자동 버전 업
     */
    grunt.registerTask("autoModuleVersionUp", function (onlyModule) {

        function getNextVersion (version) {
            var tmpVersion = parseInt(version.replace(/\./g, ""), 10);
            ++tmpVersion;

            var topVersion = Math.floor(tmpVersion / 100);
            tmpVersion -= topVersion * 100;
            var middleVersion = Math.floor(tmpVersion / 10);
            tmpVersion -= middleVersion * 10;
            var bottomVersion = tmpVersion;

            return topVersion + "." + middleVersion + "." + bottomVersion;
        }

        if (!onlyModule) {
            moduleVersion.FRAMEWORK = getNextVersion(moduleVersion.FRAMEWORK);
        }

        for (var i = 0; i < moduleVersion.MODULE.length; i++) {
            moduleVersion.MODULE[i].version = getNextVersion(moduleVersion.MODULE[i].version);
        }
    });

    /**
     * [dj.son] moduleVersion.json write
     */
    grunt.registerTask("writeModuleVersionJSON", function () {
        grunt.file.write("moduleVersion.json", JSON.stringify(moduleVersion), {encoding: "UTF-8"});
    });

    /**
     * [dj.son] Build for KT 전달용
     * - 원본 소스에 주석 제거한 버전
     * - framework 및 자동 모듈 버전 업 없음
     * - 릴리즈시 원본 diff 대응하기 위해 ReleaseConfig.js, ModuleStateTable.json 파일을 현재 버전으로 생성하도록 수정
     */
    grunt.registerTask('build_for_KT_Delivery', function (isIncludeFomalRelease) {
        var version = appVersion.OTV;

        var arrTask = ["clean:temp",
                    "copy:main", "clean:testFile", "uglify:onlyRemoveComments", "makeReleaseConfig:OTV:BMT:ON", "makeModuleStateTable", "makeZipFileName:" + version + ":OTV:BMT:ON:true", "zipping",
                    "clean:temp"];

        if (!isIncludeFomalRelease) {
            arrTask.unshift("clean:build");
        }

        grunt.task.run(arrTask);
    });

    /**
     * [dj.son] Build OTV for 내부 릴리즈
     * - framework 및 자동 모듈 버전 업 없음
     */
    grunt.registerTask("build_OTV_내부_릴리즈", function () {
        var version = appVersion.OTV;
        grunt.task.run([
                        "clean:build",
                        "copy:main", "clean:testFile", "uglify:full", "makeReleaseConfig:OTV:BMT:ON", "makeModuleStateTable", "makeZipFileName:" + version + ":OTV:BMT:ON", "zipping",
                        "makeReleaseConfig:OTV:BMT:OFF", "makeZipFileName:" + version + ":OTV:BMT:OFF", "zipping",
                        "makeReleaseConfig:OTV:LIVE:ON", "makeZipFileName:" + version + ":OTV:LIVE:ON", "zipping",
                        "makeReleaseConfig:OTV:LIVE:OFF", "makeZipFileName:" + version + ":OTV:LIVE:OFF", "zipping", "clean:temp"
                        ]);
    });
    /**
     * [sw.nam] Build OTV for daily 릴리즈
     * - framework 를 제외한 모듈 버전 업 수행
     */
    grunt.registerTask("build_OTV_daily_release", function () {
        grunt.task.run([
            "autoModuleVersionUp:true", "build_OTV_내부_릴리즈", "build_for_KT_Delivery:true", "writeModuleVersionJSON"
        ]);
    });

    /**
     * [dj.son] Build OTV for 정식 릴리즈
     * - framework 및 모듈 자동 버전 업 수행
     */
    grunt.registerTask("build_OTV_정식_릴리즈", function () {
        grunt.task.run([
            "autoModuleVersionUp", "build_OTV_내부_릴리즈", "build_for_KT_Delivery:true", "writeModuleVersionJSON"
        ]);
    });

    /**
     * [dj.son] Build OTS for 내부 릴리즈
     * - framework 및 자동 모듈 버전 업 없음
     */
    grunt.registerTask('build_OTS_내부_릴리즈', function () {
        var version = appVersion.OTS;
        grunt.task.run([
            "clean:build",
            "copy:main", "clean:testFile", "uglify:full", "makeReleaseConfig:OTS:BMT:ON", "makeZipFileName:" + version + ":OTS:BMT:ON", "zipping",
            "makeReleaseConfig:OTS:BMT:OFF", "makeZipFileName:" + version + ":OTS:BMT:OFF", "zipping",
            "makeReleaseConfig:OTS:LIVE:ON", "makeZipFileName:" + version + ":OTS:LIVE:ON", "zipping",
            "makeReleaseConfig:OTS:LIVE:OFF", "makeZipFileName:" + version + ":OTS:LIVE:OFF", "zipping", "clean:temp"
        ]);
    });
    /**
     * [dj.son] Build OTS for 정식 릴리즈
     * - framework 및 모듈 자동 버전 업 수행
     */
    grunt.registerTask("build_OTS_정식_릴리즈", function () {
        grunt.task.run([
            "autoModuleVersionUp", "build_OTS_내부_릴리즈", "build_for_KT_Delivery:true", "writeModuleVersionJSON"
        ]);
    });

    /**
     * [dj.son] Build Testbed for 내부 릴리즈
     * - framework 및 자동 모듈 버전 업 없음
     */
    grunt.registerTask('build_TESTBED_내부_릴리즈', function () {
        var version = appVersion.TESTBED;
        grunt.task.run([
            "clean:build",
            "copy:main", "clean:testFile", "uglify:full", "makeReleaseConfig:OTV:TESTBED:ON", "makeZipFileName:" + version + ":OTV:TESTBED:ON", "zipping",
/*            "makeReleaseConfig:TESTBED:BMT:OFF", "makeZipFileName:" + version + ":TESTBED:BMT:OFF", "zipping",
            "makeReleaseConfig:TESTBED:LIVE:ON", "makeZipFileName:" + version + ":TESTBED:LIVE:ON", "zipping",*/
            "makeReleaseConfig:OTV:TESTBED:OFF", "makeZipFileName:" + version + ":OTV:TESTBED:OFF", "zipping", "clean:temp"
        ]);
    });
    /**
     * [dj.son] Build Testbed for 정식 릴리즈
     * - framework 및 모듈 자동 버전 업 수행
     */
    grunt.registerTask("build_TESTBED_정식_릴리즈", function () {
        grunt.task.run([
            "autoModuleVersionUp", "build_TESTBED_내부_릴리즈", "build_for_KT_Delivery:true", "writeModuleVersionJSON"
        ]);
    });

    /**
     * Build for GitLab Upload
     */
    grunt.registerTask('build_for_GitLabUpload', function () {
        grunt.task.run(["clean:build", "copy:GitLabUpload", "uglify:full", "makeReleaseConfig:OTV:BMT", "copy:GitLabUpload_For_NonAglify"]);
    });


    /**
     * [dj.son] Build only modules for 내부 릴리즈
     * - module & framework 자동 버전 업 없음
     */
    grunt.registerTask('build_only_modules_내부_릴리즈', function () {
        destZipFileName = "module.zip";
        grunt.task.run(["clean:build", "copy:onlyModules", "uglify:full", "makeModuleStateTable", "compress", "zipping", "clean:temp"]);
    });
    /**
     * [dj.son] Build only modules for 정식 릴리즈
     * - module 자동 버전 업 있음 (framework 은 미수행)
     */
    grunt.registerTask('build_only_modules_정식_릴리즈', function () {
        grunt.task.run(["autoModuleVersionUp:true", "build_only_modules_내부_릴리즈", "writeModuleVersionJSON"]);
    });


    /**
     * [dj.son] Build for 테스트 버전 릴리즈
     * - framework 및 자동 모듈 버전 업 없음
     * - 모듈 버전은 Test_x.x.x 형식으로 나감
     */
    grunt.registerTask('build_테스트_버전_릴리즈(OTV)', function () {
        var version = "Test_" + appVersion.OTV;
        isTestBuild = true;
        grunt.task.run([
            "clean:build",
            "copy:main", "clean:testFile", "uglify:full", "makeReleaseConfig:OTV:BMT:ON", "makeZipFileName:" + version + ":OTV:BMT:ON", "zipping",
            "makeReleaseConfig:OTV:BMT:OFF", "makeZipFileName:" + version + ":OTV:BMT:OFF", "zipping",
            "makeReleaseConfig:OTV:LIVE:ON", "makeZipFileName:" + version + ":OTV:LIVE:ON", "zipping",
            "makeReleaseConfig:OTV:LIVE:OFF", "makeZipFileName:" + version + ":OTV:LIVE:OFF", "zipping", "clean:temp"
        ]);
    });
};