// use strict 가 들어가면 eval 을 해도 전역으로 실행이 안됨...
/*"use strict";*/

/**
 * create the root namespace and making sure we're not overwriting it
 * KTW === KT TV Web 3.0
 */
var KTW = KTW || {};


// app.properties 내용을 config 로 옮김
// 사실 app.properties 는 필요없다고 판단되나, 만약을 위해 현재 사용중인 property 만 config 로 옮김
KTW.APP_PROPERTY = {
    FILE_SYSTEM_QUOTA_SIZE: 6,
    APP_DEBUG_MODE: true,
    UNICAST_URL: "http://homedist.ktipmedia.co.kr/"
};