/**
 * Created by ksk91_000 on 2017-03-21.
 */
window.CornerManager = new function () {
    var cornerData;
    var contractIdx;
    var callback;

    var setData = function (_data) {
        cornerData = _data;
    };

    var getData = function () {
        return cornerData;
    };

    var setCornerContract = function(index) {
        contractIdx = index;
    };

    var getContractInfo = function () {
        if(cornerData.contractYn!=="Y") return [];
        else if(cornerData.contractInfo==null) return [];
        else if(!Array.isArray(cornerData.contractInfo)) return [cornerData.contractInfo];
        else return cornerData.contractInfo;
    };

    var getCurrentContractInfo = function () {
        return getContractInfo()[contractIdx];
    };

    var getPrice = function () {
        if(cornerData.contractYn=="Y") return getCurrentContractInfo().vatContractPrice;
        else return cornerData.vatProductPrice;
    };

    var setCallback = function(_callback) {
        callback = _callback
    };

    var regCorner = function (pinno) {
        var now = new Date().getTime();
        now = Math.floor(now / 1000);
        if(getData().contractYn == "Y") {
            payment.HDSManager.onlineChVodMSVCbPIN(pinno, cornerData.pkgCd, regCornerComplete, "Y", getCurrentContractInfo().contractCd, now);
        } else {
            payment.HDSManager.onlineChVodSvcReg(pinno, cornerData.pkgCd, now, regCornerComplete);
        }
    };

    var regCornerComplete = function (res, data) {
        if(res) {
            var response = data.responseXML.getElementsByTagName("string")[0].textContent;
            var result = response.split("|");

            if(result[0].split("^")[1]=="True") {
                callback(-99);
                LayerManager.activateLayer({
                    obj: {
                        id: "PaymentCompleteCornerPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: false,
                        params: { callback : function () { callback(true, null, true); } }
                    },
                    new: true,
                    moduleId:"module.vod_payment",
                    visible: true
                });
            } else callback(false, result[1].split("^")[1].split("\n"));
        } else callback(false, "VODE-00013");
    };

    return {
        setCornerData: setData,
        getCornerData: getData,
        getCornerContractInfo: getContractInfo,
        setConnerContract: setCornerContract,
        getCurrentContractInfo: getCurrentContractInfo,
        getPrice: getPrice,
        setCallback: setCallback,
        regCorner: regCorner,
        callback : function (res, data) {
            callback(res, data);
        }
    }
};