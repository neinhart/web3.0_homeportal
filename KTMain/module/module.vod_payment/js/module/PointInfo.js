/**
 * Created by ksk91_000 on 2017-04-03.
 */
(function() {
    payment.pointInfo = {};
    payment.pointInfo.PointInfo = function (isAvailable, point) {
        this.amount = point-0;
        this.useAmount = 0;
        this.isChecked = false;
        this.isAvailable = isAvailable;
        this.div = getCheckBox();

        this.enterKeyAction = function (price) {
            if(isAvailable) {
                this.isChecked^=true;
                return this.isChecked;
            } else {
                return false;
            }
        };

        this.clear = function () {
            this.div.removeClass("checked");
            this.isChecked = false;
            this.useAmount = 0;
            this.setDisable(false);
        };

        this.getMaxAmount = function (price) {
            return Math.min(this.amount, price);
        };

        this.setUseAmount = function (amount) {
            this.amount = amount;
        };

        this.setDisable = function(disable) {
            this.div.toggleClass("disable", disable && this.isAvailable);
        };

        this.setDisableForced = function () {
            this.div.addClass("disable");
        };

        this.isDisabled = function () {
            return this.div.hasClass("disable");
        };

        this.addRestMoney = function (price) {
            return price;
        };

        this.focused = function() {
            this.div.addClass("focus");
            return !this.div.hasClass("disable");
        };

        this.blurred = function () { this.div.removeClass("focus"); }

        this.setPoint = function(price) {
            // 체크가 된 포인트만 사용한다
            if (this.isChecked) {
                this.useAmount = Math.min(this.amount, price);
            } else {
                this.useAmount = 0;
            }

            if (this.useAmount === 0) {
                this.div.removeClass("checked");
                this.isChecked = false;
            } else {
                this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
            }
        };
    };

    payment.pointInfo.KTMembershipPointInfo = function (isAvailable, point, type) {
        payment.pointInfo.PointInfo.call(this, isAvailable, point);
        // type : settlRate
        this.rate = (type==0?100:(type==1?50:type))-0;

        if(isAvailable) {
            var box = this.div.addClass("kt_membership");
            box.find(".title").html("KT 멤버십<light>(금액의 " + this.rate + "%)</light>");
            box.find(".sub_title").html(Util.addComma(this.amount) + "<light>P" + (this.amount>0?" 보유":"") + "</light>");
        }else {
            var box = this.div.addClass("kt_membership");
            box.addClass("no_point");
            box.find(".no_point_btn").text("가입하기");
            box.find(".title").text("KT 멤버십");
            box.find(".sub_title").text("금액의 " + this.rate + "% 사용 가능");
            box.find(".sub_title").css("font-size", "24px");
        }

        this.enterKeyAction = function (price) {
            if(this.isAvailable) {
                if(this.isChecked) {
                    this.div.removeClass("checked");
                    this.isChecked = false;
                    return true;
                } else {
                    this.div.addClass("checked");
                    this.isChecked = true;
                    return true;
                }
            } else {
                //KTMembership App으로 이동.
                var locator = KTW.RELEASE_MODE == "LIVE"?CONSTANT.STAR_POINT.LOCATOR.LIVE:CONSTANT.STAR_POINT.LOCATOR.BMT;
                AppServiceManager.changeService({
                    nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: locator
                    }
                });
                return false;
            }
        };

        this.getMaxAmount = function (price) {
            return Math.floor(price * this.rate / 100);
        };

        // disable: 결제 금액이 0원이고 포인트 선택 안된 상태이면 true로 전달됨
        this.setDisable = function(disable, price) {
            // 멤버십 포인트는 결제 금액이 남아있을 때,
            // 포인트 잔액이 있고, 잔액이 공급가의 N% 이상일 때 사용 가능 처리
            this.div.toggleClass("disable", this.isAvailable && (disable || point <= 0 || point < this.getMaxAmount(PaymentManager.getPaymentPrice())));
        };

        this.setPoint = function(price) {
            var _kt_point;
            if (this.isChecked) {
                // KT맴버십 할인 총액 = Roundup(공급가액 * 할인율)
                _kt_point = Math.ceil(price * this.rate / 100);

                if (_kt_point > this.amount) {
                    _kt_point = 0;
                }
            } else {
                _kt_point = 0;
            }

            this.useAmount = _kt_point;

            if (this.useAmount === 0) {
                this.div.removeClass("checked");
                this.isChecked = false;
            } else {
                this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
            }
        };
    };

    // TV 쿠폰 (KT 제공)
    payment.pointInfo.KtTvCouponPointInfo = function (point) {
        var _point = Number(point);
        payment.pointInfo.PointInfo.call(this, _point>0, _point);

        if(this.isAvailable) {
            var box = this.div.addClass("kt_tv_coupon");
            box.css("margin-right", "8px");
            box.find(".title").html("TV쿠폰<light>(KT 제공)</light>");
            box.find(".sub_title").html(Util.addComma(_point) + "<light>원 보유</light>");
        } else {
            var box = this.div.addClass("kt_tv_coupon");
            box.css("margin-right", "8px");
            box.addClass("disable");
            box.find(".title").html("TV쿠폰");
            box.find(".sub_title").html("없음");
            box.find(".sub_title").css("font-size", "24px");
            box.find(".check_box_icon").css("display", "none");
            this.useAmount = 0;
        }

        this.enterKeyAction = function () {
            if(this.isAvailable) {
                if(this.isChecked) {
                    this.div.removeClass("checked");
                    this.isChecked = false;
                } else {
                    this.div.addClass("checked");
                    this.isChecked = true;
                }
                return true;
            } else {
                return false;
            }
        };

        this.setDisable = function(disable) {
            if (_point > 0) {
                this.div.toggleClass("disable", disable && this.isAvailable);
            } else {
                this.div.toggleClass("disable", true);
            }
        };
    };

    payment.pointInfo.TvPointPointInfo = function (isAvailable, point) {
        payment.pointInfo.PointInfo.call(this, isAvailable, point);

        if(isAvailable) {
            if(point>0) {
                var box = this.div.addClass("tv_point");
                box.css("margin-left", "8px");
                box.find(".title").html("TV포인트");
                box.find(".sub_title").html(Util.addComma(point) + "<light>P 보유</light>");
            } else {
                var box = this.div.addClass("tv_point");
                box.css("margin-left", "8px");
                box.addClass("no_point");
                box.find(".no_point_btn").text("충전하기");
                box.find(".title").text("TV포인트");
                box.find(".sub_title").html(Util.addComma(point) + "<light>P</light>");
            }
        }else {
            var box = this.div.addClass("tv_point");
            box.css("margin-left", "8px");
            box.addClass("no_point");
            box.find(".no_point_btn").text("충전하기");
            box.find(".title").text("TV포인트");
            box.find(".sub_title").text("신용카드 포인트로 충전가능");
            box.find(".sub_title").css("font-size", "24px");
        }

        this.enterKeyAction = function (price) {
            if(this.isAvailable && point>0) {
                if(this.isChecked) {
                    this.div.removeClass("checked");
                    this.isChecked = false;
                    this.useAmount = 0;
                    return true;
                } else {
                    this.div.addClass("checked");
                    this.isChecked = true;
                    this.useAmount = this.getMaxAmount(price);
                    this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
                    return true;
                }
            } else {
                //TvPoint App으로 이동.
                var locator = KTW.RELEASE_MODE == "LIVE"?CONSTANT.TV_POINT.LOCATOR.LIVE:CONSTANT.TV_POINT.LOCATOR.BMT;
                AppServiceManager.changeService({
                    nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: locator
                    }
                });
                return false;
            }
        };

        this.addRestMoney = function (price) {
            if(this.isChecked) {
                var originUseAmount = this.useAmount;
                this.useAmount = this.getMaxAmount(price + this.useAmount);
                this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
                return price - (this.useAmount - originUseAmount);
            }else{
                this.useAmount = 0;
                this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
                return price;
            }
        };

        this.setDisable = function(disable) {
            this.div.toggleClass("disable", disable && this.isAvailable && point>0);
        };
    };

    payment.pointInfo.TvCouponPointInfo = function (point) {
        payment.pointInfo.PointInfo.call(this, point-0>0, point);

        if(this.isAvailable) {
            var box = this.div.addClass("tv_coupon");
            box.css("margin-right", "0px");
            box.find(".title").html("TV쿠폰");
            box.find(".sub_title").html(Util.addComma(point) + "<light>원 보유</light>");
        } else {
            var box = this.div.addClass("tv_coupon");
            box.css("margin-right", "0px");
            box.addClass("no_point");
            box.find(".no_point_btn").text("구매하기");
            box.find(".title").text("TV쿠폰");
            box.find(".sub_title").text("쿠폰 등록 또는 구매 가능");
            box.find(".sub_title").css("font-size", "24px");
        }

        this.enterKeyAction = function (price) {
            if(this.isAvailable) {
                if(this.isChecked) {
                    this.div.removeClass("checked");
                    this.isChecked = false;
                    this.useAmount = 0;
                    return true;
                } else {
                    this.div.addClass("checked");
                    this.isChecked = true;
                    this.useAmount = this.getMaxAmount(price);
                    this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
                    return true;
                }
            } else {
                //TvCoupon App으로 이동.
                var locator = KTW.RELEASE_MODE == "LIVE"?CONSTANT.TV_COUPON.LOCATOR.LIVE:CONSTANT.TV_COUPON.LOCATOR.BMT;
                AppServiceManager.changeService({
                    nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: locator
                    }
                });
                return false;
            }
        };

        this.addRestMoney = function (price) {
            if(this.isChecked) {
                var originUseAmount = this.useAmount;
                this.useAmount = this.getMaxAmount(price + this.useAmount);
                this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
                return price - (this.useAmount - originUseAmount);
            }else{
                this.useAmount = 0;
                this.div.find(".apply_amount").text(Util.addComma(this.useAmount));
                return price;
            }
        };
    };


    function getCheckBox() {
        var box = _$("<div/>", {class: "absolute_wrapper"});
        box.append(_$("<div/>", {class: "no_point_btn"}));
        box.append(_$("<div/>", {class: "check_box_icon"}));
        box.append(_$("<div/>", {class: "apply_amount"}));
        box.append(_$("<div/>", {class: "title"}));
        box.append(_$("<div/>", {class: "sub_title"}));

        return _$("<div/>", {class: "check_box"}).append(box);
    }
})();
