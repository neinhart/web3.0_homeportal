/**
 * Created by kh.kim2 on 2018-02-06.
 */

/**
 * Biz 결제수단 결제 진행
 * @param _callback
 * @param buyObj = {
 *      isVodPpt : true or false,
 *      paymentOpt : 0 신용카드, 1 TV포인트, 2 휴대폰
 *      selectedItem : content or ppt
 *  }
 * @constructor
 */
window.BizPaymentProcess = function(_callback, buyObj) {

    window.log.printDbg("[BizPaymentProcess] buyObj = " + JSON.stringify(buyObj, null, 4));

    var selectedItem = buyObj.selectedItem;
    var paymentOpt = buyObj.paymentOpt;


    if (buyObj.isVodPpt) {
        purchaseVodPpt();
    }
    else {
        purchaseContent();
    }


    function purchaseContent() {
        // push 용 파라미터로 변환
        var pushPkgYn = "";
        switch (selectedItem.pkgYn) {
            case "N": // 단편
                pushPkgYn = "1";
                break;
            case "Y": // 시리즈
                pushPkgYn = "2";
                break;
            case "C": // VOD PPT
                pushPkgYn = "3";
                break;
            case "H": // 채널 PPT
                pushPkgYn = "4";
                break;
            case "K": // 패키지
                pushPkgYn = "5";
                break;
        }

        var pushHdYn = "0";
        switch (selectedItem.resolCd) {
            case "Y":
                pushHdYn = "1";
                break;
            case "N":
                pushHdYn = "2";
                break;
            case "F":
                pushHdYn = "3";
                break;
            case "U":
                pushHdYn = "4";
                break;
        }

        var pushLtFlag = selectedItem.ltFlag ? "2" : "1";

        // 결제수단 별 파라미터 처리
        if (paymentOpt == payment.BIZ_PAYMENT.TV_PAY || paymentOpt == payment.BIZ_PAYMENT.TV_POINT) { // TV페이 모듈
            var ltFlag = selectedItem.ltFlag ? "1" : "";
            var tvPayParams = {
                amount: selectedItem.amount.toString(),
                goodsName: selectedItem.contsName,
                period: selectedItem.period
            };

            var stbIp = oipfAdapter.hwAdapter.networkInterface.getIpAddress();
            var payType1 = (paymentOpt == 0) ? "101" : "103"; // 구매 타입
            var payType2 = (paymentOpt == 0) ? "PAY" : "POINT"; // 결제 타입 구분

            var saId = DEF.SAID;

            var appendData = selectedItem.buyingDate + "|" + saId + "|" + selectedItem.contsId + "|" + selectedItem.contsName + "|" + stbIp + "|" + selectedItem.amount + "|" + ""
                + "|" + selectedItem.pkgYn + "|" + selectedItem.period + "|" + selectedItem.catId + "|" + "H" + "|" + selectedItem.reqPathCd + "|" + ltFlag
                + "|" + (selectedItem.isCmbSeries ? selectedItem.resolCd : "") + "|" + payType1 + "|" + selectedItem.pushContsId + "|" + selectedItem.pushCatId
                + "|" + pushPkgYn + "|" + pushLtFlag + "|" + pushHdYn + "|" + selectedItem.saleYn + "|" + selectedItem.vatYn + "|" + payType2;

            tvPayParams.appendData = appendData;

            callTvPay(tvPayParams);
        }
        else { // 휴대폰 결제
            var convertedData = {
                orderDate: selectedItem.buyingDate,
                userId: (selectedItem.saId || KTW.SAID),
                itemCode: selectedItem.contsId,
                itemName: selectedItem.contsName,
                userIp: oipfAdapter.hwAdapter.networkInterface.getIpAddress(),
                authAmount: selectedItem.amount,
                pkgYn: selectedItem.pkgYn,
                period: selectedItem.period,
                catId: selectedItem.catId,
                reqPathCd: (selectedItem.reqPathCd || "01"),
                ltFlag: selectedItem.ltFlag ? "1" : "0",
                hdYn: (selectedItem.isCmbSeries) ? selectedItem.resolCd : "",
                trgtContsId: selectedItem.pushContsId,
                trgtCatId: selectedItem.pushCatId,
                pushPkgYn: pushPkgYn,
                pushLtFlag: pushLtFlag,
                pushHdYn: pushHdYn,
                saleYn: selectedItem.saleYn,
                vatYn: selectedItem.vatYn,
                isDvdYn: selectedItem.isDvdYn, // 팝업에서 쓸 데이터
                resolName: selectedItem.resolName // 팝업에서 쓸 데이터
            };

            // 휴대폰 결제 팝업 넘어가서
            // 데이터에 mobileNum 추가해서 결제 API 호출
            callMobilePay(convertedData);
        }
    }


    function purchaseVodPpt() {
        // push 용 파라미터로 변환
        var pushPkgYn = "3";
        var pushHdYn = "0";
        var pushLtFlag = selectedItem.ltFlag ? "2" : "1";

        if (paymentOpt == payment.BIZ_PAYMENT.TV_PAY || paymentOpt == payment.BIZ_PAYMENT.TV_POINT) { // TV페이 모듈
            var tvPayParams = {
                amount: selectedItem.amount.toString(),
                goodsName: selectedItem.contsName,
                period: selectedItem.period
            };

            var payType1 = (paymentOpt == 0) ? "101" : "103"; // 구매 타입
            var payType2 = (paymentOpt == 0) ? "PAY" : "POINT"; // 결제 타입 구분

            var ltFlag = selectedItem.ltFlag ? "2" : "";
            var saId = DEF.SAID;
            var stbIp = oipfAdapter.hwAdapter.networkInterface.getIpAddress();

            var appendData = selectedItem.buyingDate + "|" + saId + "|" + selectedItem.contsId + "|" + selectedItem.contsName + "|" + stbIp + "|" + selectedItem.amount + "|" + ""
                + "|" + selectedItem.pkgYn + "|" + selectedItem.period + "|" + selectedItem.catId + "|" + "H" + "|" + selectedItem.reqPathCd + "|" + ltFlag
                + "|" + selectedItem.resolCd + "|" + payType1 + "|" + selectedItem.pushContsId + "|" + selectedItem.pushCatId
                + "|" + pushPkgYn + "|" + pushLtFlag + "|" + pushHdYn + "|" + selectedItem.saleYn + "|" + selectedItem.vatYn + "|" + payType2;

            tvPayParams.appendData = appendData;

            callTvPay(tvPayParams);
        }
        else {
            // 휴대폰 결제
            var convertedData = {
                orderDate: selectedItem.buyingDate,
                userId: (selectedItem.saId || KTW.SAID),
                itemCode: selectedItem.contsId,
                itemName: selectedItem.contsName,
                userIp: oipfAdapter.hwAdapter.networkInterface.getIpAddress(),
                authAmount: selectedItem.amount,
                pkgYn: selectedItem.pkgYn,
                period: selectedItem.period,
                catId: selectedItem.catId,
                reqPathCd: (selectedItem.reqPathCd || "01"),
                ltFlag: selectedItem.ltFlag ? "1" : "0",
                hdYn: "",
                trgtContsId: selectedItem.pushContsId ? selectedItem.pushContsId : selectedItem.contsId,
                trgtCatId: selectedItem.pushCatId,
                pushPkgYn: pushPkgYn,
                pushLtFlag: pushLtFlag,
                pushHdYn: pushHdYn,
                saleYn: selectedItem.saleYn,
                vatYn: selectedItem.vatYn
            };

            callMobilePay(convertedData);
        }
    }


    function callTvPay(params) {
        LayerManager.startLoading({preventKey: true});
        params.callback = onPaymentCompleted;
        TVPayManager.makeBHPayment(params);
    }

    function onPaymentCompleted (result) {
        window.log.printDbg("[onPaymentCompleted]  " + window.log.stringify(result));

        // 결제모듈 승인결과
        if (result.resultCode == 0) {
            // 승인완료
        }
        else if (result.resultCode == 8900) {
            // 사용자에 의한 취소
        }
        else {
            // 미승인
        }

        if (_callback) { // 이전 프로세스는 모두 종료
            _callback(false);
        }
    }

    function callMobilePay(vodData) {
        window.log.printDbg("[BizPaymentProcess] callMobilePay() vodData = ", vodData);
        LayerManager.startLoading({preventKey: true});
        LayerManager.activateLayer({
            obj: {
                id: "PaymentMobilePopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    vodData: vodData,
                    callback: _callback
                }
            },
            moduleId: "module.vod_payment",
            new: true,
            visible: true
        });
//        if (_callback) {
//            _callback(false);
//        }
    }
}
