/**
 * Created by ksk91_000 on 2016-12-28.
 */
window.PaymentManager = new function () {
    var catId;
    var reqPathCd;
    var options = [];
    var optionIdx;
    var isCheckCoupon;
    var callback;

    var setData = function (_data) {
        options = filteringOption(_data.optionList);
        catId = _data.catId;
        reqPathCd = _data.reqPathCd;
        isCheckCoupon = _data.isCheckCoupon;
    };

    function filteringOption(dataList) {
        var options = [];
        for (var i in dataList) {
            // 무료옵션은 구매옵션에서 제거.
            // 구매한 옵션도 구매옵션에서 제거.
            if (dataList[i].price > 0 && (!dataList[i].checkContentData || (dataList[i].checkContentData.buyYn !== "Y" && dataList[i].checkContentData.connerYn !== "Y"))) {
                options.push(dataList[i]);
            }
        }
        return options;
    }

    var getOptions = function () {
        return options;
    };

    var selectOption = function (index) {
        optionIdx = index;
    };

    var getSelectedOption = function () {
        return options[optionIdx];
    };

    var getPaymentPrice = function(isReal) {
        if (!isReal && isCheckCoupon) {
            var coupon = _getCouponPrice() - 0;
            var price = options[optionIdx].price - 0;
            window.log.printDbg("getPaymentPrice() coupon = " + coupon + ", price = " + price);
            if (coupon >= price) { // 이용권
                return 0;
            }
            else { // 할인권
                return price;
            }
        }
        else if (options[optionIdx].isPackage) {
            return options[optionIdx].price;
        }
        else {
            return options[optionIdx].price;
        }
    };

    var _getVatPaymentPrice = function () {
        if (options[optionIdx].vatPrice) {
            return options[optionIdx].vatPrice;
        }
        else {
            return 0;
        }
    };

    var _isCheckCoupon = function () {
        return isCheckCoupon;
    };

    var _getCouponPrice = function () {
        if (!isCheckCoupon) {
            return 0;
        }
        var i;
        if (options.length == 1) {
            i = 0;
        }
        else {
            i = optionIdx;
        }
        if (options[i].isSeries) {
            return options[i].seriesCouponData ? options[i].seriesCouponData.applicableCouponPriceP : 0;
        }
        else {
            return options[i].couponData ? options[i].couponData.applicableCouponPriceP : 0;
        }
    }

    // 결제 팝업 우측 상단에 표시할 할인권 문구
    var _getCouponPriceString = function () {
        if (!isCheckCoupon) {
            return null;
        }

        var i;
        if (options.length == 1) {
            i = 0;
        }
        else {
            i = optionIdx;
        }

        if (options[i].isSeries && options[i].seriesCouponData && options[i].seriesCouponData.couponCase == "SD") {
            var price = options[i].seriesCouponData.applicableCouponPriceP;
            return "시리즈할인 " + Util.addComma(price) + "원 적용";
        }
        else if (!options[i].isSeries && options[i].couponData && options[i].couponData.couponCase == "CD") {
            var price = options[i].couponData.applicableCouponPriceP;
            return "콘텐츠할인 " + Util.addComma(price) + "원 적용";
        }

        return null;
    }

    // 결제 수단 팝업에 표시할 할인권 문구
    var _getCouponString = function () {
        if (!isCheckCoupon) {
            return null;
        }

        var i;
        if (options.length == 1) {
            i = 0;
        }
        else {
            i = optionIdx;
        }

        if (options[i].isSeries) {
            return "시리즈할인권";
        }
        else {
            return "콘텐츠할인권";
        }

        return null;
    }

    var setCallback = function (_callback) {
        callback = _callback
    };

    var purchase = function (pointInfoList) {
        window.log.printDbg("[PaymentManager] purchase() options[optionIdx] = " + JSON.stringify(options[optionIdx], null, 4));
        if(options[optionIdx].isPackage) {
            buyPackage(pointInfoList);
        }
        else if (options[optionIdx].isSeries) {
            buySeries(pointInfoList);
        }
        else {
            buyContents(pointInfoList);
        }
    };

    var buyContents = function (pointInfoList, paymentFunc) {
        var buyingDate = Util.getCurrentDate();
        var buyObj = {
            pkgYn: "N",
            assetId: options[optionIdx].assetId,
            contsName: options[optionIdx].contsName,
            buyingDate: buyingDate,
            price: _getVatPaymentPrice(),
            catId: catId,
            catName: options[optionIdx].catData.catName || options[optionIdx].catData.itemName,
            ltFlag: options[optionIdx].ltFlag,
            reqPathCd: reqPathCd,
            resolCd: options[optionIdx].resolCd,
            isSale: false,
            pointInfoList: pointInfoList,
            isCoupon: isCheckCoupon && options[optionIdx].couponData,
            cCSeridsId: options[optionIdx].catData.cCSersId,
            isCmbSeries: false
        }

        paymentProcess(function (result, data) {
            if (result) {
                options[optionIdx].buyYn = true;
                callback(true, {option: options[optionIdx]});
            } else callback(false);
        }, buyObj);
    };

    var buyPackage = function (pointInfoList, paymentFunc) {
        var buyingDate = Util.getCurrentDate();

        var buyObj = {
            pkgYn: "K",
            assetId: options[optionIdx].pkgVodPrdCd,
            contsName: options[optionIdx].itemName,
            buyingDate: buyingDate,
            price: _getVatPaymentPrice(),
            catId: catId,
            catName: options[optionIdx].itemName,
            ltFlag: options[optionIdx].ltFlag,
            reqPathCd: reqPathCd,
            resolCd: options[optionIdx].resolCd,
            isSale: false,
            pointInfoList: pointInfoList,
            isCoupon: false,
            cCSeridsId: "",
            isCmbSeries: false
        };

        paymentProcess(function (result, data) {
            callback(result);
        }, buyObj);
    };

    var buySeries = function (pointInfoList) {
        var buyingDate = Util.getCurrentDate();
        var optionData = options[optionIdx];
        var buyObj = {
            pkgYn: "Y",
            assetId: options[optionIdx].contsId,
            contsName: options[optionIdx].contsName,
            buyingDate: buyingDate,
            price: _getVatPaymentPrice(),
            catId: catId,
            catName: options[optionIdx].contsName,
            ltFlag: options[optionIdx].ltFlag,
            reqPathCd: reqPathCd,
            resolCd: options[optionIdx].resolCd,
            isSale: false,
            pointInfoList: pointInfoList,
            isCoupon: isCheckCoupon && options[optionIdx].seriesCouponData,
            cCSeridsId: options[optionIdx].catData.cCSersId,
            isCmbSeries: options[optionIdx].catData.cmbYn === "Y"
        };

        paymentProcess(function (result, data) {
            if (result) {
                optionData.buyYn = true;
                callback(true, {option: options[optionIdx]});
            } else callback(false);
        }, buyObj);
    };


    /*
        Biz 구매 진행
     */
    var bizPurchase = function (paymentOpt) {
        window.log.printDbg("[PaymentManager] bizPurchase() options[optionIdx] = " + JSON.stringify(options[optionIdx]));
        if(options[optionIdx].isPackage) {
            bizBuyPackage(paymentOpt);
        }
        else if (options[optionIdx].isSeries) {
            bizBuySeries(paymentOpt);
        }
        else {
            bizBuyContents(paymentOpt);
        }
    };

    var bizBuyContents = function (paymentOpt) {
        var buyingDate = Util.getCurrentDate();

        var selectedItem = {
            pkgYn: "N",
            contsId: options[optionIdx].assetId,
            contsName: options[optionIdx].contsName,
            buyingDate: buyingDate,
            amount: _getVatPaymentPrice(),
            catId: catId,
            ltFlag: options[optionIdx].ltFlag,
            period: options[optionIdx].period,
            reqPathCd: reqPathCd,
            resolCd: options[optionIdx].resolCd,
            saleYn: "N",
            vatYn: "Y",
            isCmbSeries: false,
            isDvdYn: options[optionIdx].isDvdYn,
            resolName: options[optionIdx].resolName,
            pushContsId: options[optionIdx].assetId,
            pushCatId: catId
        }

        var buyObj = {
            "paymentOpt": paymentOpt,
            "selectedItem": selectedItem
        }

        BizPaymentProcess(function (result, data) {
            callback(false); // 결제 수단 별 결제 진행되면 이전 팝업 닫음..
        }, buyObj);

    };

    var bizBuyPackage = function (paymentOpt) {
        var buyingDate = Util.getCurrentDate();

        var selectedItem = {
            pkgYn: "K",
            contsId: options[optionIdx].pkgVodPrdCd,
            contsName: options[optionIdx].itemName,
            buyingDate: buyingDate,
            amount: _getVatPaymentPrice(),
            catId: catId,
            ltFlag: options[optionIdx].ltFlag,
            period: options[optionIdx].period,
            reqPathCd: reqPathCd,
            resolCd: options[optionIdx].resolCd,
            saleYn: "N",
            vatYn: "Y",
            isCmbSeries: false,
            resolName: options[optionIdx].resolName,
            pushContsId: options[optionIdx].pkgVodPrdCd,
            pushCatId: catId
        };

        var buyObj = {
            "paymentOpt": paymentOpt,
            "selectedItem": selectedItem
        }

        BizPaymentProcess(function (result, data) {
            callback(false); // 결제 수단 별 결제 진행되면 이전 팝업 닫음..
        }, buyObj);
    };

    var bizBuySeries = function (paymentOpt) {
        var buyingDate = Util.getCurrentDate();

        var pushContsId = "";
        if (options[optionIdx].data[0].assetId) {
            var assetId = options[optionIdx].data[0].assetId.split("|");
            pushContsId = assetId[["HD", "SD", "FHD", "UHD"].indexOf(options[optionIdx].resolCd)];
        }
        else {
            pushContsId = options[optionIdx].data[0].contsId;
        }

        var selectedItem = {
            pkgYn: "Y",
            contsId: options[optionIdx].contsId,
            contsName: options[optionIdx].contsName,
            buyingDate: buyingDate,
            amount: _getVatPaymentPrice(),
            catId: catId,
            ltFlag: options[optionIdx].ltFlag,
            period: options[optionIdx].period,
            reqPathCd: reqPathCd,
            resolCd: HTool.convertResolCd(options[optionIdx].resolCd),
            saleYn: "N",
            vatYn: "Y",
            isCmbSeries: options[optionIdx].catData.cmbYn === "Y",
            resolName: options[optionIdx].resolName,
            pushContsId: pushContsId,
            pushCatId: catId
        };

        var buyObj = {
            "paymentOpt": paymentOpt,
            "selectedItem": selectedItem
        }

        BizPaymentProcess(function (result, data) {
            callback(false); // 결제 수단 별 결제 진행되면 이전 팝업 닫음..
        }, buyObj);
    };

    return {
        purchase: purchase,
        bizPurchase: bizPurchase,
        setCallback: setCallback,
        setData: setData,
        getOptions: getOptions,
        selectOption: selectOption,
        getSelectedOption: getSelectedOption,
        getPaymentPrice: getPaymentPrice,
        getVatPaymentPrice: _getVatPaymentPrice,
        isCheckCoupon: _isCheckCoupon,
        getCouponPrice: _getCouponPrice,
        getCouponPriceString: _getCouponPriceString,
        getCouponString: _getCouponString,
        callback : function (res, data) {
            callback(res, data);
        }
    }
};

Object.defineProperty(PaymentManager, "TYPE", {value: {NORMAL: 0, POINT: 1, COMPLEX: 2}, writable: false});
Object.defineProperty(PaymentManager, "POINT", {
    value: {TYPE: {STARPOINT: 0, TVPOINT: 1, TVCOUPON: 2, COUPON: 3}},
    writable: false
});
