/**
 * Created by kh.kim2 on 2018-02-06.
 */


/*
    구매 프로세스에서 VOD PPT 상품 데이터 관리
 */
window.VodPptManager = new function () {
    var catId;
    var contsId;
    var reqPathCd;
    var pkgCode;
    var pptName;
    var options = []; // 시청 기간별 옵션
    var optionIdx = 0;
    var callback;

    /**
     *
     * @param _data = {
     *      pkgCode : PPT 상품코드
     *      productList : PPT 상품 리스트 (기간별..)
     *      catId : 현재 컨텐츠의 카테고리 ID
     *      consId : 현재 컨텐츠의 대표 컨텐츠 ID
     *      reqPathCd : 진입코드
     * }
     */
    var setData = function (_data) {
        pkgCode = _data.pkgCode;
        catId = _data.catId;
        contsId = _data.contsId;
        reqPathCd = _data.reqPathCd;
        pptName = _data.pptName;

        var ltFlag = _data.ltFlag == "2" ? true : false;
        var priceList = ltFlag ? _data.vatPptPrice.split("|") : _data.pptPrice.split("|");
        var vatPriceList = ltFlag ? _data.vatPptLtPrice.split("|") : _data.vatPptPrice.split("|");
        var periodList = ltFlag ? _data.pptLtPeriod.split("|") : _data.pptPeriod.split("|");

        optionIdx = 0;
        options = [];
        for (var i in priceList) {
            options[i] = {
                pptPrice: priceList[i],
                vatPptPrice: vatPriceList[i],
                period: periodList[i],
                ltFlag: _data.ltFlag,
                pptTitle: _data.pptTitle,
                pptName: _data.pptName,
                pptCatId: _data.pptCatId,
                pptCd: _data.pptCd
            }
        }
    };

    var getOptions = function () {
        return options;
    };

    var selectOption = function (index) {
        optionIdx = index;
    };

    var getSelectedOption = function () {
        return options[optionIdx];
    };

    var getPaymentPrice = function(isReal) {
        return options[optionIdx].pptPrice;
    };

    var _getVatPaymentPrice = function () {
        return options[optionIdx].vatPptPrice;
    };

    var _getPptName = function () {
        return pptName;
    }

    var setCallback = function (_callback) {
        callback = _callback
    };

    /**
     *
     * @param paymentOpt
     * case 0 : // 신용카드 결제
     * case 1 : // TV포인트 결제
     * case 2 : // 휴대폰 결제
     */
    var purchase = function (paymentOpt) {
        window.log.printDbg("[VodPptManager] purchase() options[optionIdx] = " + JSON.stringify(options[optionIdx], null, 4));


        var buyingDate = Util.getCurrentDate();

        var selectedItem = {
            pkgYn: "C",
            contsId : options[optionIdx].pptCd,
            pushContsId: contsId,
            contsName: options[optionIdx].pptName,
            buyingDate: buyingDate,
            amount: _getVatPaymentPrice(),
            catId: options[optionIdx].pptCatId, // 기간정액 결제 시에는 catPptList에서 준 기간정액 대표 카테고리ID pptCatId를 전달한다
            pushCatId: catId,
            ltFlag: options[optionIdx].ltFlag == "2" ? true : false,
            period: options[optionIdx].period,
            reqPathCd: reqPathCd,
            resolCd: "",
            saleYn: "N",
            vatYn: "Y",
            isCmbSeries: false
        }

        var buyObj = {
            "isVodPpt": true,
            "paymentOpt": paymentOpt,
            "selectedItem": selectedItem

        }

        BizPaymentProcess(function(res, data) {
            callback(false);
        }, buyObj);
    };

    return {
        purchase: purchase,
        setCallback: setCallback,
        setData: setData,
        getOptions: getOptions,
        selectOption: selectOption,
        getSelectedOption: getSelectedOption,
        getPaymentPrice: getPaymentPrice,
        getVatPaymentPrice: _getVatPaymentPrice,
        getPptName: _getPptName,
        callback : function (res, data) {
            callback(res, data);
        }
    }
};

