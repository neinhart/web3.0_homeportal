/**
 * Created by ksk91_000 on 2017-03-24.
 */
window.paymentProcess = function(_callback, buyObj) {
    LayerManager.startLoading();
    var pointType;
    var isHd;
    var payType;
    var callback = function (result, data) {
        if(result) {
            LayerManager.activateLayer({
                obj: {
                    id: "PaymentCompletePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: { callback : function () { _callback(true, data); } }
                },
                new: true,
                moduleId:"module.vod_payment",
                visible: true
            });
        } else _callback(false, data);
    };

    log.printDbg("buyObj = " + JSON.stringify(buyObj));

    if(buyObj.isCmbSeries) {
        isHd = {"HD":"Y", "SD":"N", "FHD":"F", "UHD":"U"}[buyObj.resolCd];
    } else {
        isHd = "";
    }

    // 총 금액 totalPrice 와 일반결제 금액 paymentPrice 가 다르면 포인트를 사용한 것임
    if (buyObj.pointInfoList.totalPrice === buyObj.pointInfoList.paymentPrice) {
        // 포인트 결제 없이 계산 할때는 기존 BuyContentsNxt를 이용
        payType = PaymentManager.TYPE.NORMAL;
        payment.amocManager.buyContentsNxt(paymentComplete, {
            pkgYn        : buyObj.pkgYn,
            contsId      : buyObj.assetId,
            contsName    : buyObj.contsName,
            buyingDate   : buyObj.buyingDate,
            buyingPrice  : buyObj.price,
            buyingType   : "B",
            catId        : buyObj.catId,
            appCd        : "H",
            reqPathCd    : buyObj.reqPathCd,
            ltFlag       : buyObj.ltFlag? 1 : 0,
            hdYn         : isHd,
            saleYn       : "N",
            vatYn        : "Y"
        });
    } else {
        // 포인트를 하나라도 사용하면 complexPayNew API 사용
        // 이용권 100% 결제도 마찬가지
        switch (buyObj.buyingType) {
            case 'C' :
                payType = PaymentManager.TYPE.POINT;
                pointType = PaymentManager.POINT.STARPOINT;
                break;
            case '3' :
                payType = PaymentManager.TYPE.POINT;
                pointType = PaymentManager.POINT.TVPOINT;
                break;
            case '30' :
            case '31' :
                payType = PaymentManager.TYPE.POINT;
                pointType = PaymentManager.POINT.TVCOUPON;
                break;
            case '32' :
                payType = PaymentManager.TYPE.POINT;
                pointType = PaymentManager.POINT.COUPON;
                break;
            default :
                payType = PaymentManager.TYPE.COMPLEX;
                break;
        }

        payment.amocManager.useComplexPayNew(paymentComplete, {
            itemId     : buyObj.assetId,
            itemName   : buyObj.contsName,
            pkgYn      : buyObj.pkgYn,
            hdYn       : isHd,
            ltFlag     : buyObj.ltFlag? 1 : 0,
            price      : buyObj.pointInfoList.supplyPrice,
            vatPrice   : buyObj.pointInfoList.totalPrice,
            amount     : (
                (buyObj.pointInfoList.ktMembership || 0)+
                (buyObj.pointInfoList.ktTvCoupon || 0)+
                (buyObj.pointInfoList.tvPoint || 0)+
                (buyObj.pointInfoList.tvCoupon || 0) +
                (buyObj.pointInfoList.paymentPrice || 0) +
                (buyObj.pointInfoList.couponPrice || 0)),
            buyingDate : buyObj.buyingDate,
            buyingType : buyObj.pointInfoList.buyingType,
            catId      : buyObj.catId,
            catName    : buyObj.catName,
            appCd      : "H",
            reqPathCd  : buyObj.reqPathCd,
            pVoucherAmount : buyObj.pointInfoList.supplyPrice <= buyObj.pointInfoList.couponPrice? buyObj.pointInfoList.supplyPrice : 0, // 이용권
            dcVoucherAmount: buyObj.pointInfoList.supplyPrice > buyObj.pointInfoList.couponPrice? buyObj.pointInfoList.couponPrice : 0, // 할인권
            starPoint  : buyObj.pointInfoList.ktMembership,
            pTvMoney   : buyObj.pointInfoList.ktTvCoupon,
            tvPoint    : buyObj.pointInfoList.tvPoint,
            sTvMoney   : buyObj.pointInfoList.tvCoupon,
            cash       : buyObj.pointInfoList.paymentPrice
        });
    }

    function paymentComplete(res, data) {
        log.printDbg("ComplexPayment > paymentComplete > " + res + " / " + data );
        LayerManager.stopLoading();
        if (res) {
            if((data.flag == 0||data.resultCode == "0000")) {
                if(StorageManager.ps.load(StorageManager.KEY.OTN_PARING) == "Y") {
                    //OTN Paring중일 때 구매공유 여부 전달
                    if(buyObj.pkgYn=="Y") {
                        payment.SMLSManager.recvBuyItemInfo(function (result) {}, DEF.SAID, buyObj.cCSeridsId||"", buyObj.buyingDate, data.expireDate, 1, buyObj.price, getSMLSBuyType(), buyObj.ltFlag ? "Y" : "N", "N", "Y", "", buyObj.resolCd);
                    } else if(buyObj.pkgYn=="N") {
                        payment.SMLSManager.recvBuyItemInfo(function (result) {}, DEF.SAID, buyObj.assetId, buyObj.buyingDate, data.expireDate, 2, buyObj.price, getSMLSBuyType(), buyObj.ltFlag ? "Y" : "N", "N", "Y", buyObj.cCSeridsId||"", buyObj.resolCd);
                    }
                }

                callback(true, data);
                setCacheFlag();
            } else if(payType == PaymentManager.TYPE.NORMAL) {
                // 일반결제 에러처리
                if(data.flag == 1) {
                    HTool.openErrorPopup({message: ERROR_TEXT.ET0018, reboot: true, callback: failCallback});
                } else if(data.flag == 2) {
                    HTool.openErrorPopup({message: ["이미 구매한 콘텐츠입니다"], callback: failCallback});
                } else {
                    HTool.openErrorPopup({message: ERROR_TEXT.ET0018, reboot: true, callback: failCallback});
                }
            } else {
                // 혼합결제 에러처리
                if(data.resultCode === "R8000" || data.resultCode === "R8001" || data.resultCode === "R8002") {
                    HTool.openErrorPopup({message: ERROR_TEXT.ET0018, reboot: true, callback: failCallback});
                }else {
                    HTool.openErrorPopup({message: data.resultMessage.split("\\n"), callback: failCallback});
                }
            }
        } else {
            // 통신 실패 - normal : 13, complex: 18
            if(payType == PaymentManager.TYPE.NORMAL) {
                HTool.openErrorPopup({message: ERROR_TEXT.ET0013, reboot: true, callback: failCallback});
                extensionAdapter.notifySNMPError("VODE-00013");
            } else {
                HTool.openErrorPopup({message: ERROR_TEXT.ET0018, reboot: true, callback: failCallback});
            }
        }

        function failCallback() { callback(false) }
    }

    function getSMLSBuyType() {
        switch (payType-0) {
            case PaymentManager.TYPE.NORMAL: return "N";
            case PaymentManager.TYPE.COMPLEX: return "H";
            case PaymentManager.TYPE.POINT:
                switch (pointType-0) {
                    case PaymentManager.POINT.TYPE.STARPOINT: return "C";
                    case PaymentManager.POINT.TYPE.TVPOINT: return "3";
                    case PaymentManager.POINT.TYPE.TVCOUPON: return "2";
                    case PaymentManager.POINT.TYPE.COUPON: return "9";
                }
        }
    }

    function setCacheFlag() {
        var cacheData = StorageManager.ps.load(StorageManager.KEY.STB_POINT_CACHE);
        if(!cacheData){
            StorageManager.ps.save(StorageManager.KEY.STB_POINT_CACHE, JSON.stringify({ReqFlag: true}));
        } else {
            var data = JSON.parse(cacheData);
            data.ReqFlag = true;
            StorageManager.ps.save(StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
        }
    }
}