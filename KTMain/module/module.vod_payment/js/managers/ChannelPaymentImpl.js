/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>ChannelPaymentImpl</code>
 *
 * 채널 미가입 및 OTS PPV 관련 외부 연동
 * @author jjh1117
 * @since 2017-02-07
 */

"use strict";

window.ChannelPaymentImpl = (function() {
    var callbackFunc = null;

    var pkgData = null;
    var pkgDataList = [];

    var additionalProductSelectIndex = -1;

    function _callbackFuncVodUpSaleInfo(result, data) {
        if (!result) {
            extensionAdapter.notifySNMPError("VODE-00013");
            HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
        } else if (data.upSelId == null) {
            log.printWarn('_callbackChInfo() upSelId is null!!');
            HTool.openErrorPopup({message: ERROR_TEXT.ET005});
            LayerManager.stopLoading();
        }
        else {
            var pkg_list = data.upSelId.split("|");
            for (var i = 0; i < Math.min(pkg_list.length, 3); i++) {
                var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                var param = {
                    "length": pkg_list.length,
                    "idx": i,
                    "pkgCode": pkg_list[i],
                };
                payment.amocManager.getProductInfo(_callbackFuncPackageInfo, pkg_list[i], custEnv ? custEnv.buyTypeYn : "Y", param);
            }
        }
    }

    function _getVodUpSaleInfo(params) {
        pkgData = null;
        pkgDataList = [];
        callbackFunc = null;

        additionalProductSelectIndex = -1;

//        params = {catId : "10000000000000196759"}; //test
        if (params !== undefined && params !== null) {
            callbackFunc = params.callback;
            LayerManager.startLoading();
            if (CONSTANT.IS_OTS === true) {
                LayerManager.stopLoading({preventKey: true});
                LayerManager.activateLayer({
                    obj: {
                        id: "UpsellingOTSPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: null
                    },
                    moduleId: "module.vod_payment",
                    visible: true
                });
                return;
            }
            payment.amocManager.getVodUpSaleInfo(_callbackFuncVodUpSaleInfo, params.catId);
        }
    }

    function _showErrorPopupCallback(keycode) {
        if (keycode === undefined) {
            if (callbackFunc !== null) {
                callbackFunc();
            }
        } else {
            if (keycode === KEY_CODE.BACK || keycode === KEY_CODE.EXIT) {
                if (callbackFunc !== null) {
                    callbackFunc();
                }
                return true;
            }
        }
    }

    /**
     * OTV 채널 개별 상품 정보(channel package info) 얻어오는 callback
     * @param result : AMOC 연동 성공 여부
     * @param data : 개별 상품 정보 Response
     * @param param
     * @private
     */
    function _callbackFuncPackageInfo(result, data, param) {
        log.printDbg('_callbackPkgInfo() result: ' + result + ', data: ' + data + ', param: ' + JSON.stringify(param));

        LayerManager.stopLoading();
        if (!result) {
            if (data !== "abort") {
                /**
                 * result 값이 false 로 오고 결과가 timeout 등의 이유 일 경우 CEMS 리포트
                 */
                OipfAdadapter.extensionAdapter.notifySNMPError("VODE-00013");
            }
        }

        if (result === true) {
            var pkg_type = Number(data.pkgGubun);
            data.pkgCode = param.pkgCode;
            // 기본 상품
            if (pkg_type === 1) {
                // 순서 보정
                pkgDataList[param.idx] = data;
                if (param.length === pkgDataList.length) {
                    // 나중에 호출한 response가 먼저 오면 이전에 null 항목이 있어도 length는 같아지므로
                    // null check도 해준다
                    var hasEmpty = false;
                    for (var i = 0; i < param.length; i++) {
                        if (pkgDataList[i] == null) {
                            hasEmpty = true;
                        }
                    }
                    if (hasEmpty === false) {
                        /**
                         * 업셀링 상품 가입 팝업 노출
                         */
                        _showPurchasePopup(true, true, true, callbackFunc);
                    }
                }
            }
            // 부가 상품
            else if (pkg_type === 2 && data.ossYn === "Y") {
                if (param.length == null) {
                    /**
                     * 채널 정보에서 부가상품이 한개만 존재 하는 경우에 진입
                     */
                    pkgDataList[0] = data;
                    if (data.contractYn == "Y") {
                        /**
                         * 약정 선택이 존재 하는 경우
                         */
                        log.printDbg("_callbackPkgInfo() call focus Choice Monthly");
                        _showPurchasePopup(false, false, true, callbackFunc);
                    } else {
                        log.printDbg("_callbackPkgInfo() call focus Monthly Guide");
                        _showPurchasePopup(false, false, false, callbackFunc);
                    }
                }
                else {
                    /**
                     * 채널 정보에서 부가상품이 여러개 존재 하는 경우에 진입
                     * 해당 Case에 대해 검증 할 방법 찾아야 함.
                     */
                    pkgDataList[param.idx] = data;
                    if (param.length === pkgDataList.length) {
                        var hasEmpty = false;
                        for (var i = 0; i < param.length; i++) {
                            if (pkgDataList[i] == null) {
                                hasEmpty = true;
                            }
                        }
                        if (hasEmpty === false) {
                            /**
                             *  부가상품 선택 팝업 노출
                             */
                            _showAddionalProductSelectPopup(_callbackAdditionalProductSelectPopup, 0);
                        }
                    }
                }
            }
            // 카테고리 PPT 상품
            else if (pkg_type === 3) {
                // N/A
            }
            // 채널 PPT 상품
            else if (pkg_type === 4) {
                // N/A
            }
        }
        else {
            log.printDbg('_callbackFuncPackageInfo() failed');

            _showErrorPopup(payment.DATA.ERROR.ERROR_INFOMATION_FAIL, null, _showErrorPopupCallback);
        }
    }

    function _callbackAdditionalProductSelectPopup(result, selectIndex) {
        LayerManager.deactivateLayer({
            id: PAYMENT_CHANNEL_PURCHASE_ADDITIONAL_PRODUCT_SELECT_POPUP
        });

        log.printDbg('_callbackAdditionalProductSelectPopup() result : ' + result + " , selectIndex : " + selectIndex);
        if (result === true) {
            additionalProductSelectIndex = selectIndex;

            var data = pkgDataList[additionalProductSelectIndex];
            if (data.contractYn == "Y") {
                /**
                 * 약정 선택이 존재 하는 경우
                 */
                log.printDbg("_callbackPkgInfo() call focus Choice Monthly");
                _showPurchasePopup(false, false, true, callbackFunc, additionalProductSelectIndex);
            } else {
                log.printDbg("_callbackPkgInfo() call focus Monthly Guide");
                _showPurchasePopup(false, false, false, callbackFunc, additionalProductSelectIndex);
            }
        } else {
            _showErrorPopupCallback();
        }
    }

    function _callbackFuncBackKey(callbackFunc) {
        log.printDbg("_callbackFuncBackKey() additionalProductSelectIndex : " + additionalProductSelectIndex);
        if (additionalProductSelectIndex < 0) {
            if (callbackFunc !== undefined && callbackFunc !== null) {
                callbackFunc(false);
            }
        } else {
            _showAddionalProductSelectPopup(_callbackAdditionalProductSelectPopup, additionalProductSelectIndex);
        }
    }

    function _showPurchasePopup(isUpselling, isChoiceProduct, isChoiceContract, callbackFunc, selectAdditionalProductIndex) {
        var params = {
            is_upselling: isUpselling,
            is_choice_product: isChoiceProduct,
            is_choice_contract: isChoiceContract,
            package_list: pkgDataList,
            callback: callbackFunc,
            select_additional_product_index: selectAdditionalProductIndex === undefined ? -1 : selectAdditionalProductIndex,
            callback_back_key: _callbackFuncBackKey
        };

        LayerManager.activateLayer({
            obj: {
                id: PAYMENT_CHANNEL_PURCHASE_POPUP,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    data: params
                }
            },
            moduleId: Module.ID.MODULE_VOD_PAYMENT,
            visible: true
        });
    }

    function _showAddionalProductSelectPopup(callbackFunc, selectIndex) {
        var params = {
            additional_product_list: pkgDataList,
            callback: callbackFunc,
            select_product_index: selectIndex // 채널 결제 팝업에서 이전 키 눌렀을 때 다시 부가상품 선택 팝업이 노출 되어야 함.
        };

        LayerManager.activateLayer({
            obj: {
                id: PAYMENT_CHANNEL_PURCHASE_ADDITIONAL_PRODUCT_SELECT_POPUP,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    data: params
                }
            },
            moduleId: Module.ID.MODULE_VOD_PAYMENT,
            visible: true
        });
    }

    /**
     * errorType [0:가입 실패 , 1 : 가입정보 못 얻어 왔을 때 , 2 : OTS  UPSelling 상품인 경우 , 3 : 출력메세지 리스트
     */
    function _showErrorPopup(errorType, descArray, callbackFunc) {
        var isCalled = false;

        var popupData = {
            arrMessage: [],
            arrButton: [{id: "ok", name: "확인"}],
            cbAction: function(buttonId) {
                var consumed = false;
                if (!isCalled) {
                    isCalled = true;
                    if (buttonId === "ok") {
                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                        });
                        if (callbackFunc !== undefined && callbackFunc !== null) {
                            callbackFunc();
                        }
                    } else {
                        if (callbackFunc !== undefined && callbackFunc !== null) {
                            consumed = callbackFunc(buttonId);
                            if (consumed === true) {
                                LayerManager.deactivateLayer({
                                    id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                                });
                            }
                        }
                    }
                }
                return consumed;
            }
        };

        if (errorType === 0) {
            /**
             * 가입 실패 한 경우
             */
            popupData.arrMessage.push({
                type: "Title",
                message: ["알림"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_45",
                message: ["가입한 상품에서는 구매할 수 없습니다"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_30_DARK",
                message: ["문의:국번 없이 100번"],
                cssObj: {}
            });

        } else if (errorType === 1) {
            /**
             * 가입 관련 정보가 없는 경우
             */
            popupData.arrMessage.push({
                type: "Title",
                message: ["안내"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_45",
                message: ["죄송합니다. 지금 리모컨 주문이 되지 않습니다"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_33",
                message: ["잠시 후에 주문해 주세요"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_30_DARK",
                message: ["문의:국번 없이 100번"],
                cssObj: {}
            });

        } else if (errorType === 2) {
            /**
             * OTS UPSELLING 인경우
             */
            popupData.arrMessage.push({
                type: "Title",
                message: ["가입 신청"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_45",
                message: ["시청하시려면 상품 변경이 필요합니다"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_30_DARK",
                message: ["문의:국번 없이 100번"],
                cssObj: {}
            });
        } else if (errorType === 3) {
            popupData.arrMessage.push({
                type: "Title",
                message: ["안내"],
                cssObj: {}
            });

            if (descArray !== undefined && descArray !== null && descArray.length > 0) {
                popupData.arrMessage.push({
                    type: "MSG_45",
                    message: descArray,
                    cssObj: {}
                });
            } else {
                return;
            }
        }

        LayerManager.activateLayer({
            obj: {
                id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    data: popupData
                }
            },
            moduleId: Module.ID.MODULE_VOD_PAYMENT,
            visible: true
        });

    }

    return {
        getVodUpSaleInfo: _getVodUpSaleInfo
    }
}());