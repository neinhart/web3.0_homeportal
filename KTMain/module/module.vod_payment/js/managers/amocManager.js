"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId);
 * posterDm.ajaxMenu(createPoster, failAjax);
 *
 */
payment.amocManager = (function() {
    var usageId;
    var api;
    var data;

    var AMOC_SERVER_HTTP_URL = payment.HTTP.AMOC.HTTP_URL;
    var AMOC_SERVER_HTTPS_URL = payment.HTTP.AMOC.HTTPS_URL;

    var IS_RELEASE = !(DEF.UI_VERSION == "0.01.000");

    /**
     * ajax를 이용하여 json 데이터 수집<br>
     * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
     * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    var ajax = function(async, timeout, url, postData, /**Function*/callback, reqType, param) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAX request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }

        if (postData instanceof Object) {
            if (!postData) {
                postData = {};
            }
            postData.WMOCKey = DEF.AMOC.AUTH_KEY;
        } else {
            postData = postData ? postData + "&WMOCKey=" + DEF.AMOC.AUTH_KEY : "WMOCKey=" + DEF.AMOC.AUTH_KEY;
        }

        return ajaxFactory.createAjax("AMOC Manager", async, (reqType ? reqType : "post"), "json", url, postData, false, timeout, callback, param);
    };

    var ajaxData = function(async, timeout, url, postData, /**Function*/callback) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAXDATA request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }
        postData = postData ? postData : new Object();
        postData.WMOCKey = DEF.AMOC.AUTH_KEY;
        //	    postData = postData ? postData+"&WMOCKey="+DEFAULT_AMOC_AUTH_KEY : "WMOCKey="+DEFAULT_AMOC_AUTH_KEY;

        return ajaxFactory.createAjax("AMOC Manager [DATA]", async, "post", "json", url, postData, false, timeout, callback, null);
    };

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AMOC AJAX request stop!!");
            try {
                request.abort();
            } catch (e) {
                log.printExec(e.message);
            }

            request = null;
        }
    }

    function _buyContentsNxt(callback, data) {
        var createdPostData = {
            saId         : (data.saId || KTW.SAID),
            pkgYn        : data.pkgYn,
            contsId      : data.contsId,
            contsName    : data.contsName,
            belongingName: "",
            buyingDate   : data.buyingDate,
            buyingPrice  : data.buyingPrice,
            buyingType   : data.buyingType,
            catId        : data.catId,
            appCd        : data.appCd,
            reqPathCd    : (data.reqPathCd || "01"),
            ltFlag       : data.ltFlag,
            hdYn         : data.hdYn,
            saleYn       : "N",
            vatYn        : data.vatYn || "Y"
        };

        api = "/amoc-api/vod/buy/in-cash-nxt";
        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, createdPostData, callback);
    }

    /*
    function _useMyStarPointVodNxt(callback, saId, pkgYn, contsId, contsName, buyingDate, buyingPrice, buyingType, catId, appCd,
                                   reqPathCd, ltFlag, hdYn, stbIp, suppInfo, pointType, saleYn, percent) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-pgcpn-api/vod/buy/with-star-point-nxt";

        data = {
            "saId": saId,
            "pkgYn": pkgYn, //묶음 구매 (Y/N)
            "contsId": contsId, //(건별시는 컨텐츠 ID, 묶음 구매시는 CAT ID)
            "contsName": contsName, //역시 컨텐츠명 / 카테고리명
            "buyingDate": buyingDate,
            "buyingPrice": buyingPrice,
            "buyingType": buyingType, //C 입력
            "catId": catId,
            "appCd": appCd, //appcd
            "reqPathCd": reqPathCd,
            "ltFlag": ltFlag,
            "hdYn": hdYn,
            "stbIp": stbIp,
            "suppInfo": suppInfo,
            "pointType": pointType,
            "saleYn": (saleYn ? saleYn : "N"),
            "percent": percent
        };
        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _useTvMoneyVodNxt(callback, saId, pkgYn, contsId, contsName, buyingDate, buyingPrice, buyingType, catId,
                               catName, appCd, reqPathCd, ltFlag, hdYn, stbIp, tvPoint, tvMoney, detPoint1, detPoint2,
                               suppInfo, pointType, saleYn) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-pgcpn-api/vod/buy/with-tvmoney-nxt";

        data = {
            "saId": saId, //
            "pkgYn": pkgYn, //
            "itemId": contsId, //
            "itemName": contsName, //
            "buyingDate": buyingDate, //
            "amount": buyingPrice, //
            "buyingType": buyingType, //
            "catId": catId, //
            "catName": catName, //
            "appCd": appCd, //
            "reqPathCd": reqPathCd, //
            "ltFlag": ltFlag, //
            "hdYn": hdYn, //
            "stbIp": stbIp, //
            "tvPoint": tvPoint, //
            "tvMoney": tvMoney, //
            "detPoint1": detPoint1, //
            "detPoint2": detPoint2, //
            "suppInfo": suppInfo, //
            "pointType": pointType, //
            "saleYn": (saleYn ? saleYn : "N") //
        };

        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }
*/
    function _useComplexPayNew(callback, data) {
        api = "/amoc-pgcpn-api/vod/buy/with-complex-pay-new";
        var createdPostData = {
            saId       : (data.saId || KTW.SAID),
            itemId     : data.itemId,
            itemName   : data.itemName,
            pkgYn      : data.pkgYn,
            hdYn       : data.hdYn,
            ltFlag     : data.ltFlag,
            price      : data.price,
            vatPrice   : data.vatPrice,
            amount     : data.amount,
            buyingDate : data.buyingDate,
            buyingType : (data.buyingType || "H"),
            catId      : data.catId,
            catName    : data.catName,
            appCd      : data.appCd,
            reqPathCd  : (data.reqPathCd || "01")
        };

        createdPostData["pVoucherAmount"] = data.pVoucherAmount || 0;
        createdPostData["dcVoucherAmount"] = data.dcVoucherAmount || 0;
        createdPostData["starPoint"] = data.starPoint || 0;
        createdPostData["pTvMoney"] = data.pTvMoney || 0;
        createdPostData["tvPoint"] = data.tvPoint || 0;
        createdPostData["sTvMoney"] = data.sTvMoney || 0;
        createdPostData["cash"] = data.cash || 0;

        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, createdPostData, callback);
    }

    function _getProductInfo(callback, pkgCode, buyTypeYn, param) {
        api = "/amoc-api/product/info";
        data = "saId=" + DEF.SAID + "&pkgCode=" + pkgCode + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, null, param);
    }

    function _getChannelInfo(callback, serviceId, buyTypeYn, param) {
        api = "/amoc-api/channel/channel-info";
        data = "saId=" + DEF.SAID + "&serviceId=" + serviceId + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    function _getVodUpSaleInfo(callback, catId, param) {
        api = "/amoc-api/vod/category/vod-up-sale-info";
        data = "saId=" + DEF.SAID + "&catId=" + catId;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    function _useMobilePay(callback, data) {
        api = "/amoc-pgcpn-api/vod/buy/use-moble-pay";
        var createdPostData = {
            version: "0100",
            mid: "OTVBIZ1001",
            orderDate: data.orderDate,
            userId: data.userId,
            itemCode: data.itemCode,
            itemName: data.itemName,
            userIp: data.userIp,
            authAmount: data.authAmount,
            mobileNumber: data.mobileNumber,
            pkgYn: data.pkgYn,
            period: data.period,
            catId: data.catId,
            appCd: "H",
            reqPathCd: (data.reqPathCd || "01"),
            ltFlag: data.ltFlag,
            hdYn: data.hdYn,
            trgtContsId: data.trgtContsId,
            trgtCatId: data.trgtCatId,
            pushPkgYn: data.pushPkgYn,
            pushLtFlag: data.pushLtFlag,
            pushHdYn: data.pushHdYn,
            saleYn: data.saleYn,
            vatYn: data.vatYn
        };

        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, createdPostData, callback);
    }

    return {
        ajaxStop: _ajaxStop,
        buyContentsNxt: _buyContentsNxt,
//        useMyStarPointVodNxt: _useMyStarPointVodNxt,
//        useTvMoneyVodNxt: _useTvMoneyVodNxt,
        useComplexPayNew: _useComplexPayNew,
        getVodUpSaleInfo: _getVodUpSaleInfo,
        getChannelInfo: _getChannelInfo,
        getProductInfo: _getProductInfo,
        useMobilePay: _useMobilePay
    }
}());
