"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
payment.SMLSManager = (function(){
    var api;
    var data;
    
    function getUrl(){
		if (DEF.RELEASE_MODE=="LIVE") {
			return payment.HTTP.SMLS.LIVE_URL;
		}else{
			return payment.HTTP.SMLS.TEST_URL;
		}
    }
    /*
    function getHDSUrl() {
    	var url;
    	if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            url = "http://usbsso.megatvdnp.co.kr/"; //개발용 [라이브: ktpay.kt.com]
        }else{
            url = "https://125.147.35.170/"; //test.
        }
    	//http://usbsso.megatvdnp.co.kr/HDSollehtvnowSDP/HDSollehtvnowSDP.asmx?op=ReqMSaidSDP
    	return url;
    }
    */
	/**
	 * ajax를 이용하여 json 데이터 수집<br>
	 * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
	 * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
	var ajax = function(async, timeout, url, postData, /**Function*/callback, parameters){
		//(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
		return ajaxFactory.createAjax("Seamless", async, "post", "json", url, postData, undefined, timeout, callback, parameters);
	};
	
	function _ajaxStop(request){
	    if(request){
	        log.printDbg("SeamLess AJAX request stop!!");
	        request.abort();
	        request = null;
	    }
	};
	
    function _setOTVPassedTime(callback, saId, contsId, stopTime){
        api = "/vod/setOTVPassedTime";
        data = "saId="+saId+"&contsId="+contsId+"&stopTime="+stopTime;
        return ajax(true,0,getUrl()+api,data,callback);
    };
    
    function _recvBuyItemInfo(callback, saId, itemId, buyDT, buyEndDT, itemType, buyAmount, buyTypeCd, ltermYN, refundYN, viewYN, sersId, resolCd){
        api = "/vod/recvBuyItemInfo";
        data = "saId="+saId+"&itemId="+itemId+"&buyDT="+buyDT+"&buyEndDT="+buyEndDT+"&itemType="+itemType+
        			"&buyAmount="+buyAmount+"&buyTypeCd="+buyTypeCd+"&ltermYN="+ltermYN+"&refundYN="+refundYN+"&viewYN="+viewYN+
                    "&sersId="+sersId+"&resolCd="+resolCd;
        return ajax(true,0,getUrl()+api,data,callback);
    };
    
    function _checkBuyShareItemExist(callback, saId, contId, systemFlag, parameters){ 
        api = "/vod/checkBuyShareItemExist";
        data = "saId="+saId+"&contId="+contId+"&systemFlag="+systemFlag;
        return ajax(true,0,getUrl()+api,data,callback,parameters);
    };
    
    function _getOTNBuyShareHistoryList(callback, otvSaId){
        api = "/vod/getOTNBuyShareHistoryList";
        var uhd = CONSTANT.IS_UHD === true ? 'Y' : 'N';
        data = "otvSaId="+otvSaId + "&uhdYn=" + uhd;
        return ajax(true,0,getUrl()+api,data,callback);
    };
    
    // OTV 어플에 페어링된 가입자 정보 전달
    function _getMadePairingInfoList(callback, otvSaId) {
    	api = '/vod/getMadePairingInfoList';
    	data = "otvSaId=" + otvSaId;
    	return ajax(true, 0, getUrl() + api, data , callback);
    };
    
	// otnSaid가 페어링 되어 있는지 체크하는 API
    function _checkExistPairingInfo(callback, otvSaId) {
    	api = '/vod/checkExistPairingInfo';
    	data = "otvSaId=" + otvSaId;
    	///vod/ checkExistPairingInfo?otnSaId=otv000001
    	return ajax(true, 0, getUrl() + api, data , callback);
    }
    
    // 페어링 삭제 요청'TT150319017', 'TT130426916', 'ilikeyou82', '3893316')
    function _deleteMadeParingInfo(callback, otvSaId, otnSaId) {
    	api = "/vod/deleteMadePairingInfo";
    	data = "otvSaId=" + otvSaId + "&otnSaId=" + otnSaId;
    	return ajax(true, 0, getUrl() + api, data , callback);
    }

    return {
    	ajaxStop:_ajaxStop,
    	setOTVPassedTime:_setOTVPassedTime,
    	recvBuyItemInfo:_recvBuyItemInfo,
    	checkBuyShareItemExist:_checkBuyShareItemExist,
    	getOTNBuyShareHistoryList:_getOTNBuyShareHistoryList,
    	checkExistPairingInfo:_checkExistPairingInfo,
    	getMadePairingInfoList:_getMadePairingInfoList,
    	deleteMadeParingInfo:_deleteMadeParingInfo,
    };
}());
