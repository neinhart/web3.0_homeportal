/**
 * Created by User on 2017-08-03.
 */
window.checkPlayableResolProcess = function (resolName, callback) {
    if (JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV)).netType==="SD"){
        if(resolName==="일반" || resolName==="SD") {
            callback(true);
        } else {
            HTool.openErrorPopup({message:["가입하신 상품으로는 시청할 수 없는", "콘텐츠입니다"]});
            callback(false);
        }
    } else if (resolName === "HDR") {
        var isHdrTv = isSupportHdrForTv();
        if (isHdrTv !== true) {
            openHdrErrorPopup(isHdrTv);
        } else {
            callback(true);
        }
    } else {
        callback(true);
    }

    function openHdrErrorPopup(type) {
        var id = "";
        switch (type) {
            case -1:
                id = "VODHdrNoSupportPopup";
                break;
            case -2:
                id = "VODHdrResolutionSetPopup";
                break;
        }

        LayerManager.stopLoading();
        LayerManager.activateLayer({
            obj: {
                id: id,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    btnTxt: "VOD 구매",
                    callback: function(res) { callback(res); }
                }
            },
            visible: true,
            moduleId: "module.vod"
        })
    }

    function isSupportHdrForTv() {
        var is_hdr_tv = oipfAdapter.basicAdapter.getConfigText(oipfDef.CONFIG.KEY.EDID) === "true";
        if (is_hdr_tv === true) {
            // HDR 지원단말, HDR 지원에 필요한 TV 및 연결 상태 충족
            var resolution = oipfAdapter.hwAdapter.getAVOutputHDVideoFormat();
            if (resolution === "2160p60") {
                // 현재 해상도 설정이 2160p60 일 경우에는 정상적으로 VOD 재생 프로세스로 진행
                return true;
            } else {
                // 아닐 경우 해상도 관련 팝업 생성
                return -2;
            }
        } else {
            // 두 개중 어느 하나라도 만족하지 않으면 에러 팝업
            return -1;
        }
    };

}