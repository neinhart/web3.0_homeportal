// Strict Mode On (엄격모드)
"use strict";

/**
 * @author Lazuli
 * 2014.07.30
 */

window.HTool = new function() {

    // 숫자 컴마 찍기
    this.addComma = function(src) {
        src = src + "";
        var temp_str = src.replace(/,/g, "");

        for (var i = 0, retValue = String(), stop = temp_str.length; i < stop; i++) {
            retValue = ((i % 3) == 0) && i != 0 ? temp_str.charAt((stop - i) - 1) + "," + retValue : temp_str.charAt((stop - i) - 1) + retValue;
        }

        return retValue;
    };

    // 숫자앞에 0 붙이기 
    // n : 숫자, digits : 원하는 자리수
    this.leadingZero = function(n, digits) {
        var zero = "";
        n = n.toString();

        if (n.length < digits) {
            for (var i = 0; i < digits - n.length; i++) {
                zero += "0";
            }
        }
        return zero + n;
    };

    // 배열에서 특정 스트링을 가진 배열 값을 삭제.
    this.removeArrayString = function(arr, removeStr) {
        if (HTool.inArray(removeStr, arr)) {
            var index = arr.indexOf(removeStr);
            arr.splice(index, 1);
        }
    };

    // 스트링 Array 에 특정 스트링이 있는지 검사 (Boolean)
    this.inArray = function(str, arry) {
        return $.inArray(str, arry) > -1;
    };

    // OBJECT 배열에서 특정 ITEM(Key)의 값을 검색해서 일치하는 OBJECT를 모두 반환한다.
    // var objArr = [{"ID": "AAA", "NM":"유태상"},{"ID":"BBB","NM":"지니프릭스"}, {"ID":"BBB","NM":"스튜디오B"}];
    // HTool.searchJsonObjectArray(objArr, "ID", "BBB");
    // 결과값 : [{"ID":"BBB","NM":"지니프릭스"}, {"ID":"BBB","NM":"스튜디오B"}]
    this.searchJsonObjectArray = function(objArray, searchKey, searchData) {
        if (Object.prototype.toString.call(objArray) != '[object Array]')
            return [];
        if (objArray.length == 0)
            return [];
        if (objArray[0][searchKey] == null)
            return [];
        if (Object.prototype.toString.call(searchData) == '[object String]') {
            var t = searchData;
            searchData = [];
            searchData[0] = t;
        }
        var result = [];
        var idx = 0;
        for (var ii = 0, ol = objArray.length; ii < ol; ii++) {
            if (HTool.inArray(objArray[ii][searchKey], searchData))
                result[idx++] = objArray[ii];
        }
        idx = null;
        return result;
    };

    // OBJECT 배열에서 특정 ITEM(Key)의 값을 검색해서 일치하는 OBJECT를 제거한다.
    // var objArr = [{"ID": "AAA", "NM":"유태상"},{"ID":"BBB","NM":"지니프릭스"}, {"ID":"BBB","NM":"스튜디오B"}];
    // HTool.removeJsonObjectArray(objArr, "ID", "BBB");
    // 결과값 : [{"ID": "AAA", "NM":"유태상"}]
    this.removeJsonObjectArray = function(objArray, searchKey, searchData) {
        if (Object.prototype.toString.call(objArray) != '[object Array]')
            return null;
        if (objArray.length == 0)
            return null;
        if (objArray[0][searchKey] == null)
            return null;
        if (Object.prototype.toString.call(searchData) == '[object String]') {
            var t = searchData;
            searchData = [];
            searchData[0] = t;
        }
        var result = [];
        var idx = 0;
        for (var ii = 0, ol = objArray.length; ii < ol; ii++) {
            if (!HTool.inArray(objArray[ii][searchKey], searchData))
                result[idx++] = objArray[ii];
        }
        idx = null;
        return result;
    };

    // 불러올 리소스가 같은 이름 패턴으로 여러개 일때 url 셋팅해주는 함수
    // 파일의 시작 인덱스가 0 이어야 하며, 00~10 와 같이 두 자리수는 해당 안됨 -> 0~10과 같은 형식에만 사용 가능
    this.getURLs = function(root, name, type, length) {
        if (arguments.length != 4) log.printErr("[HMF] getURLs Arguments Error");
        var urlArr = [];
        var url = "";

        for (var i = 0; i < length; i++) {
            url = root + name + i + type;
            urlArr.push(url);
        }

        return urlArr;
    };

    // 글자를 한개씩 분리하여 배열에 저장
    this.toCharArray = function(str) {
        if (Object.prototype.toString.call(str) != "[object String]") {
            log.printErr("Not String !!");
        }
        var result = [];
        for (var i = 0, sl = str.length; i < sl; i++) {
            result.push(str.substring(i, i + 1));
        }
        return result;
    };

    // 글자를 원하는 개수만큼 분리하여 배열에 저장
    this.toCharArrayWithLength = function(str, len) {
        if (Object.prototype.toString.call(str) != "[object String]") {
            log.printErr("Not String !!");
        }
        if (isNaN(len)) len = Number(len);
        var arr = [];
        for (var i = 0; i < str.length; i += len) {
            arr.push(str.substring(i, i + len));
        }
        return arr;
    };

    this.getRandomRangeValue = function(first, last) {
        return Math.floor((Math.random() * (last - first)) + first);
    };

    this.toCharArrayWithWidth = function(text, maxWidth) {
        var words = text.split("");
        var lines = [];
        var currentLine = words[0];
        for (var i = 1; i < words.length; i++) {
            var word = words[i];
            var width = g.measureText(currentLine + word).width;
            if (width < maxWidth) {
                currentLine += word;
            } else {
                lines.push(currentLine);
                currentLine = word;
            }
            word = null;
            width = null;
        }
        lines.push(currentLine);
        words = null;
        currentLine = null;
        return lines;
    };

    /**
     * Text문장을 html문으로 변환 하여 반환한다.
     * ======================================
     * \n -> <br>로 변경,
     * <br> 외 태그는 무효함,
     * text로 입력된 <,>,&,",ⓒ,®,™ 등을 html상으로 표현될 수 있도록 html 예약어로 변경해 준다. )
     * ======================================
     * @param txt : 변환하고자 하는 텍스트
     * @param crtxt : 캐리지 리턴 텍스트(미입력시 기본값 사용)
     */
    this.convertTextToHtml = function (txt, crtxt) {
        txt = txt.replace(/&/g, '&amp');
        txt = txt.replace(/</g, '&lt');
        txt = txt.replace(/>/g, '&gt');
        txt = txt.replace(/"/g, '&quot');
        txt = txt.replace(/ⓒ/g, '&copy');
        txt = txt.replace(/®/g, '&reg');
        txt = txt.replace(/™/g, '&trade');
        txt = txt.replace(/™/g, '&trade');
        if(crtxt) txt = txt.replace(new RegExp(crtxt, "g"), '<br>');
        else{
            txt = txt.replace(/\r\n/g, '<br>');
            txt = txt.replace(/\n/g, '<br>');
        }

        return txt;
    };

    // 2차원 배열 만들기
    this.make2DArray = function(num, num2) {
        var arr = new Array(num);
        for (var i = 0; i < num; i++) {
            if (num2 == null)
                arr[i] = [];
            else if (num2 > 0) {
                arr[i] = new Array(num2);
            }
        }
        return arr;
    };

    this.indexPlus = function(curIdx, maxIndex) {
        return (curIdx + 1) % maxIndex;
    };

    this.indexMinus = function(curIdx, maxIndex) {
        return (curIdx - 1 + maxIndex) % maxIndex;
    };

    /**
     * 연산된 인덱스값 반환. 방향키나 OK키로 depth 만큼 이동되는 메뉴, 기타 인덱스 연산등에서 사용
     * @param curIdx 현재 인덱스
     * @param depth 이동(키) 값
     * @param len 전체 메뉴길이
     * @return 연산된 인덱스 값
     */
    this.getIndex = function(curIdx, depth, len) {
        curIdx += depth;
        if (curIdx < 0)
            curIdx += len;
        else if (curIdx >= len)
            curIdx -= len;
        return curIdx;
    };

    /**
     * 현재 인덱스를 제외한 숫자를 랜덤으로 선택
     * @param CurIndex 현재 인덱스
     * @param length 인덱스 길이
     * @return Number 현재 인덱스를 제외한 랜덤숫자
     */
    this.getAnotherIndex = function(CurIndex, length) {
        return (CurIndex + new Random().nextInt(length - 1) + 1) % length;
    };

    /**
     * 한글 String 마지막 글자의 받침여부 확인
     * @param txt 받침 여부를 확인할 문자열 (마지막글자가 한글이어야함).
     * @return boolean 마지막 글자의 받침여부 (한글이 아닌경우 false)
     */
    this.haveBatchim = function(txt) {
        txt = txt.toString();
        var code = txt.charCodeAt(txt.length - 1) - 44032;

        // 한글이 아닐 때
        if (code < 0 || code > 11171)
            return false;

        // 한글일 때
        return !(code % 28 == 0);
    };

    this.getBoolean = function(str) {
        return str == "true" || str == "Y" || "TtYy1".indexOf(str) > -1;
    };

    this.isTrue = function(str) {
        // if (!str) return false;
        // return "TtYy1".indexOf(str.charAt(0)) > -1;
        return str==="Y" || str==="y";
    };

    this.isFalse = function(str) {
        // if (!str) return false;
        // return "FfNn0".indexOf(str.charAt(0)) > -1;
        return str==="N" || str==="n";
    };

    // yyyyMMddHHmmSS
    this.compareTime = function(curDate, endDate) {
        var cd = dateConvert(curDate);
        var ed = dateConvert(endDate);
        if (!ed) ed = new Date();

        return Number(ed) - Number(cd);
    };

    // yyyyMMddHHmmSS
    this.getRemainTime = function(date) {
        var cd = dateConvert(date);
        var ed = new Date();

        return Number(ed) - Number(cd);
    };

    // 밀리초를 시간으로 표시 (hh:mm:ss)
    this.msecToTime = function(msec) {
        var totalSec = parseInt(msec / 1000);
        var totalMin = parseInt(totalSec / 60);

        var nHour = parseInt(totalMin / 60);
        var nMin = totalMin % 60;
        var nSec = totalSec % 60;

        return {
            "hour": HTool.leadingZero(nHour, 2),
            "min": HTool.leadingZero(nMin, 2),
            "sec": HTool.leadingZero(nSec, 2)
        };
    };

    // yyyyMMddHHmmSS
    var dateConvert = function(date) {
        date = date + "";
        var ret = new Date();
        ret.setFullYear(date.substring(0, 4));
        ret.setMonth(date.substring(4, 6) - 1);
        ret.setDate(date.substring(6, 8));
        ret.setHours(date.substring(8, 10));
        ret.setMinutes(date.substring(10, 12));
        ret.setSeconds(date.substring(12, 14));
        return Date.parse(ret);
    };

    this.getDateStr = function() {
        var date = new Date();
        return HTool.leadingZero(date.getFullYear(), 4) +
            HTool.leadingZero(date.getMonth() + 1, 2) +
            HTool.leadingZero(date.getDate(), 2) +
            HTool.leadingZero(date.getHours(), 2) +
            HTool.leadingZero(date.getMinutes(), 2) +
            HTool.leadingZero(date.getSeconds(), 2);
    };

    this.getDateStrWithMilliSeconds = function() {
        var date = new Date();
        return HTool.leadingZero(date.getFullYear(), 4) +
            HTool.leadingZero(date.getMonth() + 1, 2) +
            HTool.leadingZero(date.getDate(), 2) +
            HTool.leadingZero(date.getHours(), 2) +
            HTool.leadingZero(date.getMinutes(), 2) +
            HTool.leadingZero(date.getSeconds(), 2) +
            HTool.leadingZero(date.getMilliseconds(), 2);
    };

    // ObjectArray 정렬
    this.sortingObjectArray = function(objArr, value, sort) {
        if (!sort || sort == 0) {
            objArr.sort(function(a, b) {
                if (!isNaN(a[value]) && !isNaN(b[value])) return Number(a[value]) < Number(b[value]) ? -1 : Number(a[value]) > Number(b[value]) ? 1 : 0;
                else return a[value] < b[value] ? -1 : a[value] > b[value] ? 1 : 0;
            })
        } else {
            objArr.sort(function(a, b) {
                if (!isNaN(a[value]) && !isNaN(b[value])) return Number(a[value]) < Number(b[value]) ? 1 : Number(a[value]) > Number(b[value]) ? -1 : 0;
                else return a[value] < b[value] ? 1 : a[value] > b[value] ? -1 : 0;
            })
        }
    };

    this.ellipsis = function(text, font, size, max_size, postfix) {
        var element;
        var buffer_size;
        var context;
        var text_style;
        var text_width;

        element = getCanvas();
        context = element.getContext("2d");
        text_style = "" + size + "px '" + font + "'";
        context.font = text_style;
        text_width = context.measureText(text).width;
        postfix = postfix ? postfix : "...";
        buffer_size = _getTextLength(postfix, font, size);

        if (max_size >= text_width) {
            return text;
        }
        else if (max_size < buffer_size) {
            return "";
        }
        else {
            for (var i = 0; i < text.length; i++) {
                var temp_text = text.substring(0, text.length - i);
                text_width = context.measureText(temp_text).width;
                if (max_size - buffer_size >= text_width) {
                    return text.substring(0, text.length - i) + postfix;
                }
            }
            return postfix;
        }
    }

    function _getTextLength(text, fontName, fontSize) {
        var textWidth = 0;
        if (text && text.length > 0) {
            var element = getCanvas();
            var context = element.getContext('2d');
            var ff = "";

            if (fontName !== null && fontName !== "") {
                ff = "" + fontSize + "px '" + fontName + "'";
            } else {
                ff = "" + fontSize + "px 'RixHeadM'";
            }
            context.font = ff;
            textWidth = Math.ceil(context.measureText(text).width);
        }

        return textWidth;
    }

    function getCanvas() {
        var tag = document.getElementsByTagName("canvas");
        var element = null;
        if (tag.length > 0) {
            element = tag[0];
        } else {
            element = document.createElement('canvas');
            element.id = 'tmpCanvas';
            element.style.display = 'none';
            document.getElementsByTagName("body")[0].appendChild(element);
        }

        return element;
    }

    /**
     * ObjectArray를 복사 (서로 참조 안함)
     * @param {array} array
     * @returns {array} 복사된 ObjectArray
     */
    this.cloneObjectArray = function(array) {
        return JSON.parse(JSON.stringify(array));
    };

    /**
     * Array를 복사 (서로 참조 안함)
     * @param {Array} array
     * @returns {Array} 복사된 Array
     */
    this.cloneArray = function(array) {
        return array.slice(0);
    };

    /**
     * 유효한 이미지인지 확인
     * @param {image} img
     * @returns {Boolean}
     */
    this.isImage = function(img) {
        if (Object.prototype.toString.call(img) == "[object HTMLImageElement]") {
            if (img.src == "" || !img.complete || !img.naturalWidth) return false;
        } else {
            return false;
        }
        return true;
    };

    this.addPosterOption = function(w, h, q) {
        return "?w=" + w + "&h=" + h + "&quality=" + q;
    };

    this.parseVODTime = function(time) {
        var a, b, c;
        time = time.split(":");
        a = 3600 * parseInt(time[0]) + 60 * parseInt(time[1]);
        if (time[2].indexOf(".") > 0) {
            b = parseInt(time[2].split(".")[0]);
            c = parseInt(time[2].split(".")[1]);
            return (a + b) * 1000 + c;
        } else {
            b = parseInt(time[2]);
            return (a + b) * 1000;
        }
    };

    this.openErrorPopup = function(params) {
        LayerManager.activateLayer({
            obj: {
                id: "VODError",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: params.title||"오류",
                    message: params.message,
                    button: params.button||"닫기",
                    reboot: params.reboot,
                    callback: params.callback
                }
            },
            moduleId: "module.vod",
            visible: true
        });
    }

    this.convertResolCd = function(resolCd) {
        if (resolCd === "HD" || resolCd === "Y") {
            return "Y";
        }
        if (resolCd === "SD" || resolCd === "N") {
            return "N";
        }
        if (resolCd === "FHD" || resolCd === "F") {
            return "F";
        }
        if (resolCd === "UHD" || resolCd === "U") {
            return "U";
        }

        return "";
    }
};

// Java 의 ArrayList 와 동일한 메소드 구현
var ArrayList = function() {
    this.array = [];
};
ArrayList.prototype.add = function() {
    if (arguments.length == 1) this.array[this.array.length] = arguments[0];
    else if (arguments.length == 2) this.array.splice(arguments[1], 0, arguments[0]);
    else log.printErr("[HMF][ArrayList] Add Error - Wrong Arguments");
};
ArrayList.prototype.size = function() {
    return this.array.length;
};
ArrayList.prototype.get = function(index) {
    return this.array[index];
};
ArrayList.prototype.clear = function() {
    this.array = [];
};
ArrayList.prototype.remove = function(obj) {
    for (var i = this.array.length - 1; i >= 0; i--) {
        if (this.array[i] == obj) {
            this.array.splice(i, 1);
        }
    }
};
ArrayList.prototype.removeIndex = function(index) {
    this.array.splice(index, 1);
};
ArrayList.prototype.removeAll = function() {
    this.array = [];
};
ArrayList.prototype.contains = function(obj) {
    return $.inArray(obj, this.array) > -1;
};
ArrayList.prototype.isEmpty = function() {
    return this.array.size <= 0;
};
ArrayList.prototype.getArray = function() {
    return this.array;
};

// Java 의 Random 함수와 동일한 메소드 구현
var Random = function() {
};
Random.prototype.nextInt = function(_num) {
    return Math.floor(Math.random() * _num);
};

