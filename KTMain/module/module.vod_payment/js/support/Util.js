/**
 * Created by ksk91_000 on 2016-12-26.
 */
window.Util = new function() {
    var addComma = function(num) {
        return (num+"").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var indexPlus = function (curIdx, maxIndex) {
        return (curIdx + 1) % maxIndex;
    };

    var indexMinus = function (curIdx, maxIndex) {
        return (curIdx - 1 + maxIndex) % maxIndex;
    };

    /**
     * 연산된 인덱스값 반환. 방향키나 OK키로 depth 만큼 이동되는 메뉴, 기타 인덱스 연산등에서 사용
     * @param curIdx 현재 인덱스
     * @param depth 이동(키) 값
     * @param len 전체 메뉴길이
     * @return 연산된 인덱스 값
     */
    var getIndex = function (curIdx, depth, len) {
        curIdx += depth;
        if (curIdx < 0)
            curIdx += len;
        else if (curIdx >= len)
            curIdx -= len;
        return curIdx;
    };

    var getCurrentDate = function () {
        var today = new Date();
        return today.getFullYear() + leadingZero(today.getMonth()+1, 2) + leadingZero(today.getDate(), 2) +
                leadingZero(today.getHours(), 2) + leadingZero(today.getMinutes(), 2) + leadingZero(today.getSeconds(), 2);
    };

    /**
     * 숫자 앞에 0을 붙인다. (최대 15자리)
     * @param num 0응 붙일 숫자
     * @param length 길이
     * @returns {string}
     */
    var leadingZero = function(num,length) {
        return(1e15+num+"").slice(-length);
    };

    return {
        addComma: addComma,
        indexPlus: indexPlus,
        indexMinus: indexMinus,
        getIndex: getIndex,
        getCurrentDate: getCurrentDate,
        leadingZero: leadingZero,
    }
};