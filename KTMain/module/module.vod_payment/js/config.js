/**
 * Created by ksk91_000 on 2017-03-27.
 */
payment.HTTP = {
    CURATION: {
        LIVE_URL: "http://profile.ktipmedia.co.kr:7003/wasProfile",
        TEST_URL: "http://125.140.114.151:7002/wasProfile"
    },
    AMOC: {
        HTTP_URL: "http://webui.ktipmedia.co.kr:8080",
        HTTPS_URL: "https://webui.ktipmedia.co.kr"
    },
    HDS: {
        LIVE_URL: "https://svcm.homen.co.kr/",
        TEST_URL: "https://125.147.28.132/"
    },
    KTPG: {
        LIVE_URL: "http://ktpay.kt.com:10088/webapi/json/stb/",
        TEST_URL: "http://221.148.188.212:10088/webapi/json/stb/"
    },
    LUPIN: {
        LIVE_URL: "https://ktpay.kt.com/adaptor/ktpg/dcb/",
        TEST_URL: "http://etbips.olleh.com:10088/adaptor/ktpg/dcb/"
    },
    SMART_PUSH: {
        LIVE_URL: "http://wapi.ktipmedia.co.kr:80",
        TEST_URL: "http://211.113.46.215:80"
    },
    SMLS: {
        LIVE_URL: "http://222.122.121.80:8080",
        TEST_URL: "http://203.255.241.154:8080"
    },
    SKY_RP_SERVER: {
        LIVE_URL: "http://220.73.134.62",
        TEST_URL: "http://220.73.134.30"
    },
    CPMS_URL: "http://220.73.134.50:20431"
};

payment.BIZ_PAYMENT = {
    TV_PAY : 0,
    TV_POINT : 1,
    MOBILE : 2
}