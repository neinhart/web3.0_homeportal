/**
 * Created by kh.kim2 on 2018-02-06.
 */
payment.component = payment.component||{};
payment.component.BizPaymentOption = function (parent, _isVodPpt) {
    var focusIdx = 0;
    var currentFocus = 0;
    var selectedIdx = -1;
    var buttonFocus = 0;

    var paymentPrice;
    var keyBlock = false;
    var isFocused = false;

    var BUTTON_IDX_OK = 0;
    var BUTTON_IDX_CANCEL = 1;

    var div;

    var isVodPpt = _isVodPpt;

    var paymentOptionList;
    var paymentOptionIdx;
    var onlyOnePayment = false;

    this.create = function (seq_number) {
        div = _$("<div/>", {class: "option bizPayment"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(
            _$("<div/>", {class: "seq_number"}).text(seq_number),
            _$("<span/>", {class: "text1"}).html("결제 옵션"));
        div.find(".main_area").append(main_area);
        div.append(_$("<div/>", {class: "option_box_area"}));
        div.append(_$("<div/>", {class: "price_area"}));
        div.append(_$("<div/>", {class: "button_area"}));

        if (KTW.CONSTANT.TV_PAY_SUPPORT) {
            paymentOptionList = [
                {
                    paymentName : "신용카드 결제",
                    subText : "TV페이 간편결제"
                },
                {
                    paymentName : "TV포인트",
                    subText : "신용카드 포인트 결제"
                },
                {
                    paymentName : "휴대폰 결제",
                    subText : "스마트폰 소액결제"
                }
            ];

            paymentOptionIdx = {
                0: payment.BIZ_PAYMENT.TV_PAY,
                1: payment.BIZ_PAYMENT.TV_POINT,
                2: payment.BIZ_PAYMENT.MOBILE
            };

            selectedIdx = -1;
            onlyOnePayment = false;
        }
        else {
            paymentOptionList = [
                {
                    paymentName : "휴대폰 결제",
                    subText : "스마트폰 소액결제"
                }
            ];

            paymentOptionIdx = {
                0: payment.BIZ_PAYMENT.MOBILE
            };

            selectedIdx = 0;
            onlyOnePayment = true;
        }


        keyBlock = false;
        makeOptionBox();
        setOptionBox(paymentOptionList);
        setPriceArea();
        setButtonArea();

        return true;
    };

    function setOptionBox(optionList) {
        function setOptionBox(div, option) {
            div.find(".payment_name").text(option.paymentName);
            div.find(".subText").text(option.subText);
        }
        var area = div.find(".option_box_area .option_box_wrapper");
        var i = 0;
        for (; i < optionList.length; i++) {
            setOptionBox(area.eq(i).removeClass("none"), optionList[i]);
        }
        for (; i < 4; i++) {
            setOptionBox(area.eq(i).addClass("none"), {paymentName: "", subText: ""});
        }
    }

    function makeOptionBox() {
        var area = div.find(".option_box_area");
        for(var i=0; i<4; i++) {
            var box = _$("<div/>", {class:"option_box"});
            box.append(_$("<div/>", {class:"optionBtn"}));
            box.append(_$("<div/>", {class:"payment_area"}).html("<span class='payment_name'/>"));
            box.append(_$("<span/>", {class:"subText"}));
            area.append(_$("<div/>", {class:"option_box_wrapper"}).append(box));
        }
    }

    function setPriceArea() {
        var area = div.find(".price_area");
        area.append(_$("<span/>", {class: "payment_amount_txt"}).text("결제 금액"));
        area.append(_$("<span/>", {class: "payment_amount_price"}));
        area.append(_$("<span/>", {class: "payment_amount_won"}).text("원"));
    }

    function setButtonArea() {
        var top_area = _$("<div/>", {class: "button_top_area"});
        top_area.append(_$("<div/>", {class:"ment"}).html("※ 부가세 포함된 가격입니다<br>결제 완료 전 결제수단 변경 시 중복 과금 될 수 있습니다"));
        top_area.append(_$("<div>", {class: "input_btn ok"}).text("확인"));
        top_area.append(_$("<div>", {class: "input_btn cancel"}).text("취소"));

        var wrapper = _$("<div/>", {class:"button_top_area_wrapper"}).append(top_area);

        div.find(".button_area").append(wrapper);

        if (selectedIdx == -1) {
            div.find(".input_btn.ok").addClass("disable");
        }
    }

    this.onKeyAction = function(keyCode) {
        if(keyBlock) return true;
        switch (keyCode) {
            case KEY_CODE.UP:
                if (focusIdx == 0 || onlyOnePayment) {
                    return false;
                }
                else if (focusIdx == 1) {
                    setFocus(0, selectedIdx);
                }
                return true;
            case KEY_CODE.DOWN:
                if (focusIdx === 0) {
                    if (selectedIdx == -1) {
                        // 선택한 결제 수단이 없으면 취소 버튼으로
                        setFocus(focusIdx + 1, BUTTON_IDX_CANCEL);
                    }
                    else {
                        // 확인 버튼으로
                        setFocus(focusIdx + 1, BUTTON_IDX_OK);
                    }
                }
                return true;
            case KEY_CODE.LEFT:
                if (focusIdx == 0) {
                    setFocus(focusIdx, Math.max(0, currentFocus-1));
                }
                else if (focusIdx == 1 && selectedIdx != -1) {
                    setFocus(focusIdx, BUTTON_IDX_OK);
                }
                return true;
            case KEY_CODE.RIGHT:
                if (focusIdx == 0) {
                    setFocus(focusIdx, Math.min(currentFocus + 1, paymentOptionList.length - 1));
                }
                else if (focusIdx == 1) {
                    setFocus(focusIdx, Math.min(BUTTON_IDX_CANCEL, buttonFocus + 1));
                }
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
        }
        if(keyCode>=KEY_CODE.NUM_0 && keyCode<=KEY_CODE.NUM_9) {
            return true;
        }
    };

    function enterKeyAction() {
       if(focusIdx == 0) { // 결제 수단 선택
           selectedIdx = currentFocus;
           div.find(".option_box_area .option_box_wrapper.selected").removeClass("selected");
           div.find(".option_box_area .option_box_wrapper").eq(currentFocus).addClass("selected");
           div.find(".input_btn").removeClass("disable");
           setFocus(focusIdx + 1, BUTTON_IDX_OK);
//           CornerManager.setConnerContract(selectedIdx);
        } else if(focusIdx == 1) { // 확인 취소
            if(buttonFocus == BUTTON_IDX_OK) {
                //확인
                // TV페이 모듈 실행 또는 휴대폰 결제 진행
                if (isVodPpt) {
                    VodPptManager.purchase(paymentOptionIdx[selectedIdx]);
                }
                else {
                    PaymentManager.bizPurchase(paymentOptionIdx[selectedIdx]);
                }

            } else if (buttonFocus == BUTTON_IDX_CANCEL){
                //취소
                LayerManager.deactivateLayer({
                    id: parent.id,
                    remove: true
                });
                if (isVodPpt) {
                    VodPptManager.callback(false);
                }
                else {
                    PaymentManager.callback(false);
                }
            }
        }
    }

    function setFocus(mf, sf) {
        div.find(".focus").removeClass("focus");
        focusIdx = mf;

        if (mf == 0) { // 결제 수단 목록
            if (sf < 0) sf = 0;
            currentFocus = sf;
            div.find(".option_box_area .option_box_wrapper.focus").removeClass("focus");
            div.find(".option_box_area .option_box_wrapper").eq(sf).addClass("focus");
        } else if (mf == 1) { // 확인 취소 버튼
            buttonFocus = sf;
            div.find(".button_area").addClass("focus");
            switch (sf) {
                case BUTTON_IDX_OK:
                    div.find(".input_btn.ok").addClass("focus");
                    break;
                case BUTTON_IDX_CANCEL:
                    div.find(".input_btn.cancel").addClass("focus");
                    break;
            }
        }
    }

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        div.addClass('focus');
        if (!onlyOnePayment) {
            setFocus(0, 0);
        }
        else {
            setFocus(1, 0);
            div.find(".option_box_area .option_box_wrapper").eq(0).addClass("selected");
        }

        if (selectedIdx == -1) {
            div.find(".input_btn.ok").addClass("disable");
        }
        else {
            div.find(".input_btn").removeClass("disable");
        }

        if (isVodPpt) {
            // ppt 가격 설정
            div.find(".payment_amount_price").text(Util.addComma(VodPptManager.getVatPaymentPrice()));
        }
        else {
            div.find(".payment_amount_price").text(Util.addComma(PaymentManager.getVatPaymentPrice()));
        }
        div.find(".text1").text("결제 옵션을 선택해주세요");
        isFocused = true;

        return true;
    };

    this.blurred = function () {
        div.removeClass('focus');
        div.find(".text1").text("결제 옵션");
    };

    this.show = function () {

    };

    this.hide = function () {
//        toggleSpeechMode(false);
    };

};