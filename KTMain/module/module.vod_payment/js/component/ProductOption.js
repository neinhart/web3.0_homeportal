/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component||{};
payment.component.ProductOption = function () {
    var div;

    this.create = function (seq_number) {
        div = _$("<div/>", {class:"option resolution"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(_$("<div/>", {class:"seq_number"}).text(seq_number));
        main_area.append(_$("<span/>", {class:"text1"}).html("구매 상품 : <box>" + CornerManager.getCornerData().productName + "</box>"));
        main_area.append(_$("<span/>", {class:"text2"}).html(CornerManager.getCornerData().isOnlyCorner?"* 월정액 상품만 구매가능":""));
        div.find(".main_area").append(main_area);
        CornerManager.setConnerContract(0);
    };

    this.onKeyAction = function(keyCode) {};

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        return false;
    };

    this.blurred = function () {
    };
};