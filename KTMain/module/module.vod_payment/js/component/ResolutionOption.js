/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component || {};
payment.component.ResolutionOption = function (parent, changeTitleFunc) {
    var mainFocus = 0;
    var resFocus = 0;
    var page = 0;
    var optionList;
    var selectedIdx = -1;
    var indexList = [];

    var div;

    this.create = function(seq_number) {
        div = _$("<div/>", {class:"option resolution"});

        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(
            _$("<div/>", {class:"seq_number"}).text(seq_number),
            _$("<span/>", {class:"text1"}).html("구매 옵션을 선택하세요"),
            _$("<span/>", {class:"resolution_date_txt"}).text("기간 : ")
        );

        var mobile_area = _$("<div/>", {class:"watching_mobile_area"});
        if (KTW.managers.service.ProductInfoManager.isBiz()) {
            mobile_area.append(_$("<span/>", {class: "watching_mobile_txt"}).html(""));
        }
        else {
            mobile_area.append(_$("<span/>", {class: "watching_mobile_txt"}).html("<icon/>올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다"));
        }
        
        div.append(
            _$("<div/>", {class: "main_area"}).append(main_area),
            mobile_area,
            _$("<div/>", {class: "option_box_area"}),
            _$("<div/>", {class: "append_option_area"}));

        makeOptionBox(div.find(".option_box_area"));
        page = 0;

        this.setData(PaymentManager.getOptions());

        var res = !PaymentManager.isCheckCoupon() && optionList.length > 1;
        if (res) {
            setFocus(0, resFocus);
            
            if (optionList[resFocus] && optionList[resFocus].checkContentData && optionList[resFocus].checkContentData.buyYn == "Y") {
                changeFocus(1);
            }
        } return res;
    };

    this.setData = function (data) {
        if (data) {
            optionList = filterBoughtOption(data);

            if (optionList.length == 1) {
                selectedIdx = 0;
                PaymentManager.selectOption(indexList[selectedIdx]);
                div.find(".text1").html("구매 옵션 : " + getMiniTitle(optionList[selectedIdx]));
                div.find(".option_box_area").css("display", "none");

                var couponPrice = PaymentManager.getCouponPriceString();
                if (couponPrice) {
                    parent.div.find(".coupon_txt").html(couponPrice);
                    parent.div.find(".coupon_txt").css("display", "inline-block");

                    changeTitle(changeTitleFunc, optionList[selectedIdx].contsName);
                }
                else {
                    parent.div.find(".coupon_txt").css("display", "none");
                }
            } else if (PaymentManager.isCheckCoupon()){
                for (var i = 0; i < optionList.length; i++) {
                    window.log.printDbg("[ResolutionOption] setData() optionList " + i + " = " + JSON.stringify(optionList[i], null, 4));
                    if((optionList[i].couponData || optionList[i].seriesCouponData) && optionList[i].isCouponSelected) {
                        selectedIdx = i;
                        PaymentManager.selectOption(indexList[selectedIdx]);
                        div.find(".text1").html("구매 옵션 : " + getMiniTitle(optionList[selectedIdx]));
                        div.find(".option_box_area").css("display", "none");

                        var couponPrice = PaymentManager.getCouponPriceString();
                        if (couponPrice) {
                            parent.div.find(".coupon_txt").html(couponPrice);
                            parent.div.find(".coupon_txt").css("display", "inline-block");

                            changeTitle(changeTitleFunc, optionList[selectedIdx].contsName);
                        }
                        else {
                            parent.div.find(".coupon_txt").css("display", "none");
                        }
                    }
                }
            } else {
                var repVod = false;
                resFocus = 0;
                for (var i = 0; i < optionList.length; i++) {
                    if (optionList[i].isRepVod) {
                        resFocus = i;
                        repVod = true;
                        break;
                    }
                }
                if (!repVod) for (var i = 0; i < optionList.length; i++) {
                    if (optionList[i].resolCd === "HD") {
                        resFocus = i;
                        break;
                    }
                }

                PaymentManager.selectOption(indexList[selectedIdx]);
                setOptionBox(div.find(".option_box_area"), optionList);
                div.find(".option_box_area").css("display", "inherit");
            }


            var tmpFocus = selectedIdx == -1 ? resFocus : selectedIdx;

            if (optionList[tmpFocus].period == 999999) {
                div.find(".resolution_date_txt").text("기간 : 해지 시까지");
            } else {
                div.find(".resolution_date_txt").text("기간 : " + Math.floor(optionList[tmpFocus].period / 24) + "일 간 시청 가능");
            }

            if (!KTW.managers.service.ProductInfoManager.isBiz()) {
                if (optionList[tmpFocus].seamlessCode === "2") { // 1:N
                    div.find(".watching_mobile_area").css("visibility", "inherit");
                    div.find(".watching_mobile_txt").html("<icon/>올레 tv 모바일앱(3대까지 동시시청 가능)에서도 함께 시청 가능합니다");
                } else if (optionList[tmpFocus].seamlessCode === "1") { // 1:1
                    div.find(".watching_mobile_area").css("visibility", "inherit");
                    div.find(".watching_mobile_txt").html("<icon/>올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다");
                } else {
                    div.find(".watching_mobile_area").css("visibility", "hidden");
                }
            } else {
                div.find(".watching_mobile_area").css("visibility", "hidden");
            }
        }
    };

    function filterBoughtOption(optionList) {
        var tmp = [];
        for (var i in optionList) {
            if (!optionList[i].buyYn) {
                tmp.push(optionList[i]);
                indexList.push(i + "");
            }
        }
        return tmp;
    }

    function getTitle(option) {
        var result = option.resolName + " ";
        if (option.data.subtitleDubbed == "0") result += "자막";
        else if (option.data.subtitleDubbed == "1") result += "더빙";
        result += option.ltName;

        return result;
    }

    function getMiniTitle(option) {
        var result = "<box>";
        if (option.isSeries) {
            result += ("시리즈" + option.ltName);
        }
        else if (option.data.isDvdYn == "Y") {
            result += "소장";
        }
        else {
            result += ("단편" + option.ltName);
        }

        result += " " + option.resolName;
        if (!PaymentManager.isCheckCoupon() || (PaymentManager.getPaymentPrice(false) - 0) > 0) {
            result += " | " + Util.addComma(PaymentManager.getVatPaymentPrice()) + "원</box>";
        }
        else {
            result += "</box>";
        }

        if (option.data.subtitleDubbed == "0") result += "<box>자막</box>";
        else if (option.data.subtitleDubbed == "1") result += "<box>더빙</box>";

        return result;
    }

    function setOptionBox(div, optionList) {
        function setOptionBox_empty(div) {
            div.find(".resolution_name").text("");
            div.find(".price").html("");
            div.removeClass("selected");
        }

        function setOptionBox(div, option, isSelected) {
            div.find(".resolution_name").text(getTitle(option));
            div.toggleClass("selected", isSelected);
            if (optionList[resFocus].checkContentData && option.checkContentData.buyYn == "Y") {
                div.addClass("disable");
                div.find(".price").html("구매완료");
            }
            else if (PaymentManager.isCheckCoupon() && (option.couponData || option.seriesCouponData)) {
                var coupon;
                if (optionList[resFocus].isSeries) {
                    coupon = option.seriesCouponData;
                }
                else {
                    coupon = option.couponData;
                }

                if (coupon.couponCase == "CC" || coupon.couponCase == "SC") {
                    div.find(".price").html("이용권 보유중");
                }
                else {
                    div.find(".discount").html("할인권");
                    div.find(".discount").css("display", "block");

                    var couponPrice = option.couponData ? option.couponData.applicableCouponPriceP : option.seriesCouponData.applicableCouponPriceP;
                    var newPrice = Math.floor((option.price - couponPrice) * 1.1);
                    if (newPrice < 0) newPrice = 0;
                    div.find(".orgPrice").html( Util.addComma(option.vatPrice) + "<won>원</won><div class='btn_line'></div>");
                    div.find(".price").html("<span class='orgPriceArr'></span>" + Util.addComma(newPrice) + "<won>원</won>");
                }
            }
            else {
                div.find(".price").html(Util.addComma(option.vatPrice) + "<won>원</won>");
            }
        }

        var i = 0, j = page * 4;
        var item = div.find(".option_box_wrapper");
        for (; j < optionList.length && i < 4; i++, j++) setOptionBox(item.eq(i).removeClass("none disable"), optionList[j], j == selectedIdx);
        for (; i < 4; i++) setOptionBox_empty(item.eq(i).addClass("none"));
        div.toggleClass("hasLeft", page > 0).toggleClass("hasRight", j < optionList.length);
    }

    function makeOptionBox(div) {
        for(var i=0; i<4; i++) {
            var box = _$("<div/>", {class: "option_box"});
            box.append(_$("<div/>", {class: "discount"}));
            box.append(_$("<div/>", {class: "optionBtn"}));
            box.append(_$("<span/>", {class: "resolution_name"}));
            box.find(".resolution_name").css("display", "block");
            box.append(_$("<span/>", {class: "orgPrice"}));
            box.append(_$("<span/>", {class: "price"}));
            div.append(_$("<div/>", {class: "option_box_wrapper"}).append(box));
        }
    }

    function changeFocus(amount) {
        var newFocus = Util.getIndex(resFocus, amount, optionList.length);
        if (!optionList[newFocus] || (optionList[resFocus].checkContentData && optionList[newFocus].checkContentData.buyYn == "Y")) {
            changeFocus(amount + (amount / Math.abs(amount)));
        } else setFocus(mainFocus, newFocus);
    }

    function setFocus(mf, sf) {
        mainFocus = mf;
        switch (mainFocus) {
            case 0:
                resFocus = sf;
                if (Math.floor(resFocus / 4) != page) {
                    page = Math.floor(resFocus / 4);
                    setOptionBox(div.find(".option_box_area"), optionList);
                }

                div.find(".option_box_area .option_box_wrapper.focus").removeClass("focus");
                div.find(".option_box_area .option_box_wrapper").eq(resFocus - (page * 4)).addClass("focus");

                if (optionList[resFocus].period == 999999)
                    div.find(".resolution_date_txt").text("기간 : 해지 시까지");
                else if (!isNaN(optionList[resFocus].period))
                    div.find(".resolution_date_txt").text("기간 : " + Math.floor((optionList[resFocus].period) / 24) + "일 간 시청 가능");
                else
                    div.find(".resolution_date_txt").text("");

                if (!KTW.managers.service.ProductInfoManager.isBiz()) {
                    if (optionList[resFocus].seamlessCode === "2") { // 1:N
                        div.find(".watching_mobile_area").css("visibility", "inherit");
                        div.find(".watching_mobile_txt").html("<icon/>올레 tv 모바일앱(3대까지 동시시청 가능)에서도 함께 시청 가능합니다");
                    } else if (optionList[resFocus].seamlessCode === "1") { // 1:1
                        div.find(".watching_mobile_area").css("visibility", "inherit");
                        div.find(".watching_mobile_txt").html("<icon/>올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다");
                    } else {
                        div.find(".watching_mobile_area").css("visibility", "hidden");
                    }
                } else {
                    div.find(".watching_mobile_area").css("visibility", "hidden");
                }

                div.find(".text1").html("구매 옵션을 선택하세요");
                changeTitle(changeTitleFunc, optionList[resFocus].contsName);
                break;
        }
    }

    function selectOption(focus, forced) {
        if (!forced) return checkPlayableResolProcess(optionList[focus].resolName, function (res) {
            if (res) selectOption(resFocus, true);
        });

        selectedIdx = focus;
        div.find(".option_box_area .option_box_wrapper.selected").removeClass("selected");
        div.find(".option_box_area .option_box_wrapper").eq(focus - (page * 4)).addClass("selected");
        PaymentManager.selectOption(indexList[focus]);

        var couponPrice = PaymentManager.getCouponPriceString();
        if (couponPrice) {
            parent.div.find(".coupon_txt").html(couponPrice);
            parent.div.find(".coupon_txt").css("display", "inline-block");

            changeTitle(changeTitleFunc, optionList[selectedIdx].contsName);
        }
        else {
            parent.div.find(".coupon_txt").css("display", "none");
        }

        if(parent.onCompleteStep) {
            parent.onCompleteStep();
        }
    }

    this.onKeyAction = function (keyCode) {
        if (mainFocus == 0) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    changeFocus(-1);
                    return true;
                case KEY_CODE.RIGHT:
                    changeFocus(1);
                    return true;
                case KEY_CODE.ENTER:
                    selectOption(resFocus);
                    return true;
                case KEY_CODE.DOWN:
                    if (selectedIdx == -1) return true;
                    else return false;
                case KEY_CODE.UP:
                    return true;
            }
        }
    };

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        if (optionList.length > 1) {
            div.addClass('focus');
            setOptionBox(div.find(".option_box_area"), optionList);
            div.find(".option_box_area").css("display", "inherit");
            setFocus(mainFocus, selectedIdx == -1 ? resFocus : selectedIdx);
            return true;
        } else {
            return false;
        }
    };

    this.blurred = function () {
        div.removeClass('focus');
        if (selectedIdx != -1) div.find(".text1").html("구매 옵션 : " + getMiniTitle(optionList[selectedIdx]));
        div.find(".option_box_area").css("display", "none");
    };

    function changeTitle(_changeTitleFunc, title) {
        if(_changeTitleFunc) {
            if (PaymentManager.getCouponPriceString()) {
                title = HTool.ellipsis(title, "RixHead B", 45, 600);
            }
            window.log.printDbg("[changeTitleFunc] title = " + title);
            _changeTitleFunc(title);
        }
    }
};