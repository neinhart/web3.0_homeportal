/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component || {};
payment.component.TermOption = function(parent) {
    var focus;
    var div;
    var termText = "";

    this.create = function(seq_number) {
        div = _$("<div/>", {class: "option term"});
        div.append(_$("<div/>", {class: "main_area"}));
        var main_area = _$("<div/>", {class: "absolute_wrapper"});
        main_area.append(_$("<div/>", {class: "seq_number"}).text(seq_number));
        main_area.append(_$("<span/>", {class: "text1"}).text("가입 안내"));
        div.find(".main_area").append(main_area);

        termText = CornerManager.getCornerData().pkgDescW3;
        // termText = CornerManager.getCornerData().pkgDescW3.replace(/\r\n/g, '<br>');
        div.append(_$("<div/>", {class: "term_area"}).html("<div>" + HTool.convertTextToHtml(termText) + "</div>"));
        div.append(_$("<div/>", {class: "btn_area"}));

        setBtnArea();
    };

    function setBtnArea() {
        var btnArea = div.find(".btn_area");
        btnArea.append(_$("<div/>", {class: "btn_text"}).text("월정액 가입 시에는 약관 동의가 필요합니다"));
        btnArea.append(_$("<div/>", {class: "btn detail"}).text("약관 상세보기"));
        btnArea.append(_$("<div/>", {class: "btn agree"}).html("<checkbox/>약관 동의"));
    }

    function setFocus(_focus) {
        focus = _focus;
        div.find(".btn_area .btn").removeClass("focus").eq(focus).addClass("focus");
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT:
            case KEY_CODE.RIGHT:
                setFocus(focus ^ 1);
                return true;
            case KEY_CODE.ENTER:
                if (focus == 0) {
                    LayerManager.activateLayer({
                        obj: {
                            id: "PaymentTermDetailPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: {
                                data: termText,     // 개행문자 변환 전 텍스트를 전달해야 함. (팝업 내부에서도 변환을 하고있음)
                                callback: function(res) {
                                    if (res) {
                                        // 약관동의에 체크되면서 디폴트 포커스 이동 후 다음단계로 이동
                                        div.find(".btn_area .btn.agree").toggleClass("check", res);
                                        parent.onCompleteStep();
                                    }
                                }
                            }
                        },
                        new: true,
                        moduleId: "module.vod_payment",
                        visible: true
                    });
                } else if (focus == 1) {
                    div.find(".btn_area .btn.agree").toggleClass("check");
                    if(div.find(".btn_area .btn.agree").hasClass("check"))
                        parent.onCompleteStep();
                } return true;
            case KEY_CODE.DOWN:
                return !div.find(".btn_area .btn.agree").hasClass("check");
        }
    };

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.find(".text1").text("가입 안내를 확인해 주세요");
        div.addClass("focus");
        if(CornerManager.getCurrentContractInfo()) termText = CornerManager.getCurrentContractInfo().contractDescW3;
        div.find(".term_area div").html(HTool.convertTextToHtml(termText));
        setFocus(1);
        return true;
    };

    this.blurred = function() {
        if(div.find(".btn_area .btn.agree").hasClass("check"))
            div.find(".text1").html("가입 안내 : <box>약관 동의함</box>");
        else
            div.find(".text1").text("가입 안내");
        div.removeClass("focus");
    };
};