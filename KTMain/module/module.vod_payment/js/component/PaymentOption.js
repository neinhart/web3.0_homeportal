/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component||{};
payment.component.PaymentOption = function (parent) {
    var focusIdx = 0;
    var checkboxFocus = 0;
    var passwordFocus = 0;
    var optionData;
    var pointInfos = [],
        pointDetailInfo = {};

    var paymentPrice;
    var pointSetted = false;
    var keyBlock = false;
    var isFocused = false;
    var infoButton = false; // 상세보기
    var firstUseCoupon = false;

    var div;
    var speechAdapter = oipfAdapter.speechRecognizerAdapter;
    var speechMode = false;

    var MAX_POINT_FOCUS = 4;

    var DEF = {
        KT_POINT: 0, // KT 맴버십
        KT_COUPON: 1, // TV 쿠폰 (KT제공)
        TV_POINT: 2, // TV 포인트
        TV_COUPON: 3 // TV 쿠폰
    };

    var DEF_API = {
        "ST" : payment.pointInfo.KTMembershipPointInfo,
        "CP" : payment.pointInfo.KtTvCouponPointInfo,
        "TP" : payment.pointInfo.TvPointPointInfo,
        "CS" : payment.pointInfo.TvCouponPointInfo
    };

    this.create = function (seq_number) {
        div = _$("<div/>", {class: "option payment"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(
            _$("<div/>", {class: "seq_number"}).text(seq_number),
            _$("<span/>", {class: "text1"}).html("결제 옵션 및 구매인증"),
            _$("<div/>", {class: "detail_info"}).html("결제 상세보기"),
            _$("<div>", {class: "password_input_bg"}).text("* * * *"));
        div.find(".main_area").append(main_area);
        div.append(_$("<div/>", {class: "check_box_area"}));
        div.append(_$("<div/>", {class: "price_area"}));
        div.append(_$("<div/>", {class: "password_area"}));

        keyBlock = false;
        setCheckBoxArea();
        setPriceArea();
        setPasswordArea();
        AuthManager.initPW();
        AuthManager.resetPWCheckCount();

        return true;
    };

    function setCheckBoxArea() {
        payment.PointManager.getPoint('9', function(res, data) {
            var area = div.find(".check_box_area");

            if(res) {
                optionData = data.settlWayList;
                var dataForCache = {};

                if (KTW.TEST_CODE) {
                    var sampleData = KTW.TEST_CODE_POINT ? KTW.TEST_CODE_POINT :
                        ["10000",
                            "2000",
                            "1000",
                            "1000",
                            "4000"];

                    optionData = [
                        {
                            // 맴버십
                            settlWayCd: 'ST',
                            blncAmt: sampleData[0],
                            settlRate: '20',
                            mbrTypeCd: 'R'
                        },
                        {
                            // 티비 포인트
                            settlWayCd: 'TP',
                            blncAmt: sampleData[1],
                            tvPointYn: 'Y'
                        },
                        {
                            // 티비 쿠폰 (프로모션)
                            settlWayCd: 'CP',
                            blncAmt: sampleData[2]
                        },
                        {
                            // 티비 쿠폰
                            settlWayCd: 'CS',
                            blncAmt: sampleData[3]
                        },
                        {
                            // 콘텐츠 이용
                            settlWayCd: 'CU',
                            blncAmt: '4000'
                        }
                    ]
                }

                var _data;
                var pointIdx = 0;
                dataForCache.PointAmount = data.totBlncAmt;
                for(var i = 0; i< optionData.length; i++) {
                    _data = optionData[i];

                    if (_data.settlWayCd === "ST") {
                        //KT 맴버십
                        pointInfos[DEF.KT_POINT] = new DEF_API[_data.settlWayCd](_data.mbrTypeCd === "R", _data.blncAmt, _data.settlRate);
                        dataForCache.StarPoint = _data.blncAmt;
                        dataForCache.UsrDiv = _data.mbrTypeCd;
                        dataForCache.MembershipRate = _data.settlRate;
                        if (isFocused) {
                            pointInfos[DEF.KT_POINT].setDisable(false);
                        }
                        pointIdx = DEF.KT_POINT;
                    } else if (_data.settlWayCd === "CP") {
                        // TV 쿠폰 (KT제공)
                        pointInfos[DEF.KT_COUPON] = new DEF_API[_data.settlWayCd](_data.blncAmt);
                        pointIdx = DEF.KT_COUPON;
                        dataForCache.KtCoupon = _data.blncAmt;
                    } else if (_data.settlWayCd === "TP") {
                        // TV 포인트
                        pointInfos[DEF.TV_POINT] = new DEF_API[_data.settlWayCd](_data.tvPointYn === "Y" || (_data.blncAmt-0) > 0,  _data.blncAmt);
                        pointIdx = DEF.TV_POINT;
                        dataForCache.TvPoint = _data.blncAmt;
                        dataForCache.TvPointYn = _data.tvPointYn;
                    } else if (_data.settlWayCd === "CS") {
                        // TV 쿠폰
                        pointInfos[DEF.TV_COUPON] = new DEF_API[_data.settlWayCd](_data.blncAmt);
                        pointIdx = DEF.TV_COUPON;
                        dataForCache.TvMoney = _data.blncAmt;
                    }

                    if (paymentPrice == 0) {
                        pointInfos[pointIdx].setDisableForced();
                    }
                }

                var _optionTxt = "<div class='option_text'>" +
                    "<div id='op_txt1' class='op_txt_main1'>할인</div>" +
                    "<div id='op_txt2' class='op_txt_main2'>기타 결제</div>" +
                    "</div>";

                area.append(_optionTxt,
                    pointInfos[0].div,
                    pointInfos[1].div,
                    pointInfos[2].div,
                    pointInfos[3].div,
                    "<div id='split_bar'></div>"
                );

                updateCacheData(dataForCache);
                pointSetted = true;

                if (PaymentManager.getCouponPrice() > 0 && PaymentManager.getPaymentPrice(false) > 0) {
                    _$(".detail_info").addClass("on");
                    infoButton = true;
                }
            } else {
                pointInfos = [];
                var area = div.find(".check_box_area");
                area.append("<div class='point_load_fail_area'>" +
                    "<span class='text1'>일시적인 장애로 포인트/쿠폰을 사용할 수 없습니다<br>일반 결제로 진행해 주세요</span>" +
                    "<span class='text2'>문의: 국번없이 100번</span>" +
                    "</div>")
            }
        });
    }

    function updateCacheData(data) {
        var originData = StorageManager.ps.load(StorageManager.KEY.STB_POINT_CACHE);
        if (originData) {
            originData = JSON.parse(originData);
            originData.PointAmount = data.PointAmount;
            originData.TvPoint = data.TvPoint;
            originData.StarPoint = data.StarPoint;
            originData.TvMoney = data.TvMoney;
            originData.KtCoupon = data.KtCoupon;
            originData.UsrDiv = data.UsrDiv;
            originData.Grade = data.Grade;
            originData.TvPointYn = data.TvPointYn;
            originData.TvPointEarn = data.TvPointEarn;
            originData.MembershipRate = data.MembershipRate;
            StorageManager.ps.save(StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(originData));
        }
    }

    function setPriceArea() {
        var area = div.find(".price_area");
        area.append(_$("<span/>", {class: "payment_amount_vat"}));
        area.append(_$("<span/>", {class: "payment_amount_txt"}).text("실제 청구 금액"));
        area.append(_$("<span/>", {class: "payment_amount_price"}));
        area.append(_$("<span/>", {class: "payment_amount_won"}).text("원"));
    }

    function setPasswordArea() {
        var top_area = _$("<div/>", {class: "password_top_area"});
        top_area.append(_$("<div>", {class: "password_input_txt"}).text("구매인증 번호 4자리를 입력해 주세요"));
        top_area.append(_$("<div>", {class: "password_input_bg"}).text("* * * *"));
        top_area.append(_$("<div>", {class: "password_input_area"}));
        top_area.append(_$("<div>", {class: "password_input_btn ok"}).text("확인"));
        top_area.append(_$("<div>", {class: "password_input_btn cancel"}).text("취소"));
        div.find(".password_area").append(_$("<div/>", {class:"password_top_area_wrapper"}).append(top_area));
        div.find(".password_area").append(_$("<div/>", {class:"password_ment"}).html("*구매 후 취소되지 않으며 시작 전 광고가 포함될 수 있습니다<br>단, 콘텐츠가 표시 광고된 내용과 다를 시 구매일로부터 3개월 이내 취소 가능합니다"));
    }

    function inputPassword(addNum) {
        var res = AuthManager.inputPW(addNum, 4);
        var star = "";
        for(var i=AuthManager.getPW().length; i>0; i--) star+="* ";
        div.find(".password_input_area").text(star);

        if(res) setFocus(1, 1);
    }

    var inputSpeechPassword = function(rec, input, mode) {
        if (rec && mode === oipfDef.SR_MODE.PIN) {
            var res = AuthManager.replacePW(input, 4);
            var star = "";
            for(var i=AuthManager.getPW().length; i>0; i--) star+="* ";
            div.find(".password_input_area").text(star);

            if(res) setFocus(1, 1);
        }
    };

    function deletePassword() {
        AuthManager.deletePW();
        var star = "";
        for(var i=AuthManager.getPW().length; i>0; i--) star+="* ";
        div.find(".password_input_area").text(star);
    }

    function clearPassword() {
        AuthManager.initPW();
        div.find(".password_input_area").text("");
    }

    this.onKeyAction = function(keyCode) {
        if(keyBlock) return true;
        switch (keyCode) {
            case KEY_CODE.UP:
                if(focusIdx==0) {
                    if (infoButton) {
                        setFocus(-1, 0);
                    } else {
                        return false;
                    }
                }
                else {
                    if(focusIdx==1) {
                        if ((checkboxFocus = getCheckedCheckBoxFocus()) === -1 || PaymentManager.getPaymentPrice(false) == 0) {
                            // 이용권 사용하는 경우도 포인트 선택 못하니까 구매옵션으로 간다.
                            return false;
                        }
                        else {
                            setFocus(0, checkboxFocus);
                        }
                    }
                    else {
                        return false;
                    }
                } return true;
            case KEY_CODE.DOWN:
                if (focusIdx === -1) {
                    setFocus(focusIdx + 1, checkboxFocus);
                } else if (focusIdx === 0) {
                    setFocus(focusIdx + 1, 0);
                }
                return true;
            case KEY_CODE.LEFT:
            case KEY_CODE.DEL:
                if(focusIdx==0) {
                    setFocus(focusIdx, getCheckboxFocus(-1));
                }
                else if(focusIdx==1) {
                    if(passwordFocus==0) deletePassword();
                    else setFocus(focusIdx, passwordFocus-1);
                }
                return true;
            case KEY_CODE.RIGHT:
                if(focusIdx==0) {
                    setFocus(focusIdx, getCheckboxFocus(1));
                }
                else if(focusIdx==1) {
                    setFocus(focusIdx, Math.min(2, passwordFocus+1));
                }
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
        }if(keyCode>=KEY_CODE.NUM_0 && keyCode<=KEY_CODE.NUM_9) {
            if(focusIdx==1 && passwordFocus==0) inputPassword(keyCode-KEY_CODE.NUM_0);
            return true;
        }
    };

    function getCheckedCheckBoxFocus() {
        if(pointInfos.length===0) return -1;
        for(var i=0; i<pointInfos.length; i++) {
            if(pointInfos[i].isChecked) return i;
        }
        checkboxFocus = 3;
        return getCheckboxFocus(1);
    }

    function getCheckboxFocus(amount) {
        var newFocus = Util.getIndex(checkboxFocus, amount, MAX_POINT_FOCUS);
        while(pointInfos[newFocus].isDisabled()) {
            if(newFocus==checkboxFocus) return -1;
            newFocus = Util.getIndex(newFocus, amount/Math.abs(amount), MAX_POINT_FOCUS);
        } return newFocus;
    }

    function enterKeyAction() {
        if (focusIdx === -1) {
            calculatePrice();
            window.log.printDbg(pointDetailInfo);
            // 결제 상세보기
            log.printDbg("========================");
            log.printDbg("단편 구매 = "+ pointDetailInfo.totalPrice);
            log.printDbg("------------------------");
            log.printDbg("프로그램 금액 = "+ pointDetailInfo.supplyPrice);
            log.printDbg("이용권/할인권 = "+ pointDetailInfo.couponPrice);
            log.printDbg("KT 맴버십 = "+ pointDetailInfo.ktMembership);
            log.printDbg("TV 쿠폰(KT제공) = "+ pointDetailInfo.ktTvCoupon);
            log.printDbg("할인 후 금액 = "+ pointDetailInfo.discountPrice);
            log.printDbg("부가세 = "+ pointDetailInfo.vatPrice);
            log.printDbg("합계 = "+ pointDetailInfo.useTotalPrice);
            log.printDbg("TV 포인트 = "+ pointDetailInfo.tvPoint);
            log.printDbg("TV 쿠폰 = "+ pointDetailInfo.tvCoupon);
            log.printDbg("구매 타입 = "+ pointDetailInfo.buyingType);
            log.printDbg("------------------------");
            log.printDbg("실제 청구 금액 = "+ pointDetailInfo.paymentPrice);
            log.printDbg("========================");

            LayerManager.activateLayer({
                obj: {
                    id: "PaymentPointPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        data: pointDetailInfo,
                        callback: function(res) {
                            if (res) {
                                window.log.printDbg(res);
                            }
                        }
                    }
                },
                moduleId: "module.vod_payment",
                visible: true
            });
        } else if(focusIdx==0) {
            if(pointInfos[checkboxFocus].enterKeyAction()) {
                calculatePrice(true);
            }
        } else if(focusIdx==1) {
            if(passwordFocus==1) {
                calculatePrice(false);
                //확인
                keyBlock = true;
                AuthManager.checkUserPW({
                    type: AuthManager.AUTH_TYPE.AUTH_BUY_PIN,
                    callback: checkPWListener,
                    loading: {
                        type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                        lock: true,
                        callback: null,
                        on_off : false
                    }
                });
            }else if(passwordFocus==2){
                //취소
                LayerManager.deactivateLayer({
                    id: parent.id,
                    remove: true
                });
                PaymentManager.callback(false);
            }
        }
    }

    function calculatePrice(actionCheck) {
        // 구매 총액 (공급가 + 부가세)
        var totalPrice = Number(PaymentManager.getVatPaymentPrice());
        // 프로그램 금액 (공급가)
        var supplyPrice = Number(PaymentManager.getPaymentPrice(true));
        // 부가세
        var vatPrice = totalPrice - supplyPrice;
        // 연산 될 금액
        var calcPrice = Number(supplyPrice);
        // 할인 후 금액
        var discountPrice = calcPrice;
        // 콘텐츠 이용권 or 할인권 금액
        var contCouponPrice = Number(PaymentManager.getCouponPrice());
        if (contCouponPrice > supplyPrice) {
            contCouponPrice = supplyPrice;
        }

        // 쿠폰선택없이 바로 구매할 경우를 위해 Default object생성
        if (!pointDetailInfo.totalPrice) {
            pointDetailInfo = {
                // 단편 구매
                totalPrice: totalPrice,
                // 프로그램 금액
                supplyPrice: supplyPrice,
                // 부가세
                vatPrice: vatPrice,
                // 실제 청구 금액
                paymentPrice: paymentPrice
            };
        }

        // 2017.08.23 부가세 표기정책 변경
        // 체크박스
        checkboxCount = 0;
        var usableOtherPoint = true;
        if (contCouponPrice > 0) {
            checkboxCount = 1;
            discountPrice = Number(supplyPrice - contCouponPrice);
            calcPrice = discountPrice;
            vatPrice = Math.floor(discountPrice * 0.1);

            if (discountPrice == 0) {
                usableOtherPoint = false;
            }
        }

        // 부가세 포함 결제의 경우 체크한 순서대로 결제 진행
        // [WEBIIIHOME-3722] 포인트 정보가 없는 경우 계산하지 않음
        if (usableOtherPoint && pointInfos && pointInfos.length > 0) {
            for (var i = 0; i < pointInfos.length; i++) {
                if (i === DEF.TV_POINT) {
                    // 총액에 부가세 추가
                    // 할인 후 금액
                    discountPrice = calcPrice;
                    // 부가세 (할인 후 금액)
                    if (discountPrice !== supplyPrice) {
                        vatPrice = Math.floor(discountPrice * 0.1);
                    }

                    // 부가세 포함 총액
                    calcPrice += vatPrice;
                }

                // 포인트 정보 표기 및 연산
                pointInfos[i].setPoint(calcPrice);
                // 공급가 = 총액 - 사용 포인트
                calcPrice -= pointInfos[i].useAmount;
            }
        }

        paymentPrice = calcPrice;

        // 합계 금액은 TV쿠폰이나 TV포인트 사용한 게 있을 때만 보여준다
        var useTotalPrice = (paymentPrice == discountPrice + vatPrice) ? null : discountPrice + vatPrice;

        // 최종 사용된 포인트 상세 정보
        pointDetailInfo = {
            // 단편 구매
            totalPrice: totalPrice,
            // 프로그램 금액
            supplyPrice: supplyPrice,
            // 할인권 금액
            couponPrice: contCouponPrice,
            // KT 맴버십
            ktMembership: pointInfos[DEF.KT_POINT] ? pointInfos[DEF.KT_POINT].useAmount : 0,
            // TV 쿠폰(KT제공)
            ktTvCoupon: pointInfos[DEF.KT_COUPON] ? pointInfos[DEF.KT_COUPON].useAmount : 0,
            // 할인 후 금액
            discountPrice: discountPrice,
            // 부가세
            vatPrice: vatPrice,
            // 합계
            useTotalPrice: useTotalPrice,
            // TV 포인트
            tvPoint: pointInfos[DEF.TV_POINT] ? pointInfos[DEF.TV_POINT].useAmount : 0,
            // TV 쿠폰
            tvCoupon: pointInfos[DEF.TV_COUPON] ? pointInfos[DEF.TV_COUPON].useAmount : 0,
            // 실제 청구 금액
            paymentPrice: paymentPrice,
            // 할인율
            ktMembRate: pointInfos[DEF.KT_POINT] ? pointInfos[DEF.KT_POINT].rate : 0
        };

        window.log.printDbg(pointDetailInfo);

//        if ((vatPrice-0) > 0) {
            div.find(".payment_amount_vat").text(Util.addComma(vatPrice));
//        }
//        else {
//            div.find(".payment_amount_vat").text("");
//        }
        div.find(".payment_amount_price").text(Util.addComma(calcPrice));

        var _buyTypeDef = ["C", "30", "3", "31", "32", "H"]; // 맴버십 ,쿠폰(제공), 포인트 , 쿠폰, 이용권, 혼합
        var _buyType = _buyTypeDef[5];

        if (usableOtherPoint) {
            for (var i = 0; i < pointInfos.length; i++) {
                pointInfos[i].setDisable(calcPrice === 0 && !pointInfos[i].isChecked, calcPrice);

                // 단독으로 포인트를 이용하고 결제 Cash가 0 일 경우 해당 타입을 넘기도록 한다
                if (contCouponPrice == 0 && pointInfos[i].isChecked && paymentPrice === 0) {
                    _buyType = _buyTypeDef[i];
                }
                // 체크 카운트
                checkboxCount = pointInfos[i].isChecked ? checkboxCount += 1 : checkboxCount;
            }
        }
        else {
            _buyType = _buyTypeDef[4]; // 이용권 100%
        }

        // 결제 상세보기 버튼
        // [WEBIIIHOME-3714] 콘텐츠 이용권을 통해 진입 한 구매 팝업에서 비밀번호 잘못 입력 후 확인 시 결제 상세보기 버튼이 딤 처리 해제되는 현상
        // [WEBIIIHOME-3757] 포인트 선택 시 상세보기 활성화 안되는 이슈 수정
        if (((contCouponPrice > 0 && contCouponPrice < supplyPrice) || (totalPrice > paymentPrice)) && (checkboxCount > 0 && !infoButton)) {
            _$(".detail_info").addClass("on");
            infoButton = true;
        } else if (checkboxCount === 0 && infoButton) {
            infoButton = false;
            _$(".detail_info").removeClass("on");
        }

        pointDetailInfo.buyingType = checkboxCount > 1 ? "H" : _buyType;

        // 포인트 선택 후 잔액이 0원이면 아래로 이동
        if (actionCheck) {
            if (calcPrice === 0) {
                setFocus(1, 0);
            } else if (pointInfos[checkboxFocus].div.hasClass("disable")) {
                setFocus(0, getCheckboxFocus(1));
            }
        }
    }

    function checkPWListener(result) {
        log.printDbg("PaymentOption > checkPWListener > " + result);
        keyBlock = false;
        switch (result) {
            case AuthManager.RESPONSE.HDS_BUY_AUTH_SUCCESS:
                keyBlock = true;
                PaymentManager.purchase(pointDetailInfo);
                break;
            case AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL:
                showToast("인증번호가 일치하지 않습니다. 다시 입력해 주세요");
            case AuthManager.RESPONSE.HDS_BUY_AUTH_ERROR:
                clearPassword();
                setFocus(1, 0);
                break;
            case AuthManager.RESPONSE.CANCEL:
                clearPassword();
                setFocus(1, 0);
                break;
            case AuthManager.RESPONSE.CAS_AUTH_FAIL:
            case AuthManager.RESPONSE.GOTO_INIT:
                LayerManager.deactivateLayer({
                    id: parent.id,
                    remove: true
                });
                PaymentManager.callback(false);
                break;
            case AuthManager.RESPONSE.DISCONNECTED_NETWORK:
                clearPassword();
                setFocus(0, 0);
                HTool.openErrorPopup({message:ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00016)"]), reboot: true, callback: function () { keyBlock = false; }});
                break;
        }
    }

    function setFocus(mf, sf) {
        div.find(".focus").removeClass("focus");
        focusIdx = mf;

        if (mf === -1) {
            // 결제 상세보기
            div.find(".detail_info").addClass("focus");
        } else if(mf==0) {
            if(sf<0) return setFocus(1,0);
            // clearPassword();
            checkboxFocus = sf;
            div.find(".check_box_area").addClass("focus");
            div.find(".check_box").eq(sf).addClass("focus");
            toggleSpeechMode(false);
        } else if(mf==1) {
            passwordFocus = sf;
            div.find(".password_area").addClass("focus");
            switch(sf) {
                case 0: div.find(".password_input_bg").addClass("focus"); toggleSpeechMode(true); break;
                case 1: div.find(".password_input_btn.ok").addClass("focus"); toggleSpeechMode(false); break;
                case 2: div.find(".password_input_btn.cancel").addClass("focus"); toggleSpeechMode(false); break;
            }
        }
    }

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        div.addClass('focus');
        setFocus(1, 0);

        for (var i in pointInfos) {
            pointInfos[i].clear();
        }

        paymentPrice = PaymentManager.getVatPaymentPrice();
        var supplyPrice = Number(PaymentManager.getPaymentPrice(true));
        var vatPrice = paymentPrice - supplyPrice;

        var contCouponPrice = Number(PaymentManager.getCouponPrice());
        if (contCouponPrice > 0) {
            supplyPrice = Number(supplyPrice - contCouponPrice);
            supplyPrice = supplyPrice < 0 ? 0 : supplyPrice;
            vatPrice = Math.floor(supplyPrice * 0.1);
            paymentPrice = supplyPrice + vatPrice;
        }

        if(paymentPrice == 0) {
            for (var i = 0; i < pointInfos.length; i++) {
                pointInfos[i].setDisableForced(true);
            }
        }

        if (contCouponPrice > 0 && supplyPrice > 0) {
            _$(".detail_info").addClass("on");
            infoButton = true;
        }

//        if (vatPrice > 0) {
            div.find(".payment_amount_vat").text(Util.addComma(vatPrice));
//        }
//        else {
//            div.find(".payment_amount_vat").text("");
//        }

        div.find(".payment_amount_price").text(Util.addComma(paymentPrice));
        div.find(".text1").text("결제 옵션을 선택해주세요");
        isFocused = true;

        return true;
    };

    this.blurred = function () {
        div.removeClass('focus');
        div.find(".text1").text("결제 옵션 및 구매인증");
        clearPassword();
        toggleSpeechMode(false);
    };

    this.show = function () {
        clearPassword();
    };

    this.hide = function () {
        toggleSpeechMode(false);
    };

    function toggleSpeechMode(isOn) {
        if(isOn) {
            if(speechMode) return;
            speechMode = true;
            speechAdapter.start(true);
            speechAdapter.addSpeechRecognizerListener(inputSpeechPassword);
        } else {
            if(!speechMode) return;
            speechMode = false;
            speechAdapter.stop();
            speechAdapter.removeSpeechRecognizerListener(inputSpeechPassword);
        }
    }
};