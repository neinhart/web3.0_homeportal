/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component||{};
payment.component.PeriodOption = function (parent) {
    var focus = 0;
    var optionList;
    var selectedIdx = -1;

    var div;

    this.create = function(seq_number) {
        div = _$("<div/>", {class:"option resolution"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(_$("<div/>", {class:"seq_number"}).text(seq_number));
        main_area.append(_$("<span/>", {class:"text1"}).html("약정 기간을 선택해주세요"));
        main_area.append(_$("<span/>", {class:"text2"}).html("약정 기간 종료 시 무약정으로 자동 전환됩니다"));
        div.find(".main_area").append(main_area);
        div.append(_$("<div/>", {class:"option_box_area"}));
        makeOptionBox(div.find(".option_box_area"));

        this.setData(CornerManager.getCornerContractInfo());
        setFocus(0);
    };

    this.setData = function(data) {
        optionList = [];
        for(var i in data) {
            optionList.push({period:getPeriodText(data[i].contractPeriod), discount: data[i].contractDiscountRate, price:data[i].vatContractPrice});
        }
        setOptionBox(div.find(".option_box_area .option_box_wrapper"), optionList);
    };

    function getPeriodText(amount) {
        switch(amount-0) {
            case 0: return "무약정";
            default: return amount + "개월";
        }
    }

    function setOptionBox(div, optionList) {
        function setOptionBox(div, option) {
            div.find(".resolution_name").text(option.period);
            div.find(".discount_rate").text(option.discount-0>0?("(" + option.discount + "% 할인)"):"");
            div.find(".price").html("<won>월 </won>" + Util.addComma(option.price) + "<won>원</won>");
        }
        var i=0;
        for(;i<optionList.length; i++) setOptionBox(div.eq(i).removeClass("none"), optionList[i]);
        for(;i<4; i++) setOptionBox(div.eq(i).addClass("none"), {period:"", price:""});
    }

    function makeOptionBox(div) {
        for(var i=0; i<4; i++) {
            var box = _$("<div/>", {class:"option_box"});
            box.append(_$("<div/>", {class:"optionBtn"}));
            box.append(_$("<div/>", {class:"resolution_area"}).html("<span class='resolution_name'/><span class='discount_rate'/>"));
            box.append(_$("<span/>", {class:"price"}));
            div.append(_$("<div/>", {class:"option_box_wrapper"}).append(box));
        }
    }

    function setFocus(_focus) {
        focus = _focus;
        div.find(".option_box_area .option_box_wrapper.focus").removeClass("focus");
        div.find(".option_box_area .option_box_wrapper").eq(focus).addClass("focus");
    }

    function selectOption(focus) {
        selectedIdx = focus;
        div.find(".option_box_area .option_box_wrapper.selected").removeClass("selected");
        div.find(".option_box_area .option_box_wrapper").eq(focus).addClass("selected");
        CornerManager.setConnerContract(selectedIdx);
        if(parent.onCompleteStep) parent.onCompleteStep();
    }

    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.LEFT:
                setFocus(Math.max(0, focus-1));
                return true;
            case KEY_CODE.RIGHT:
                setFocus(Math.min(focus+1, optionList.length-1));
                return true;
            case KEY_CODE.ENTER:
                selectOption(focus);
                return true;
            case KEY_CODE.DOWN:
                if(selectedIdx==-1) return true;
                else return false;
        }
    };

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        if(optionList.length>1) {
            div.addClass('focus');
            div.find(".text1").html("약정 기간을 선택해주세요");
            return true;
        } else {
            return false;
        }
    };
    
    this.blurred = function () {
        div.removeClass('focus');
        if(selectedIdx!=-1) div.find(".text1").html("약정 기간 : <box>" + optionList[selectedIdx].period + " | 월 " + Util.addComma(optionList[selectedIdx].price) + "원</box>");
    };
};