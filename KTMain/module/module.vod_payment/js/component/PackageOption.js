/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component || {};
payment.component.PackageOption = function () {
    var option;

    var div;

    this.create = function (seq_number) {
        div = _$("<div/>", {class: "option resolution"});
        div.append(_$("<div/>", {class: "main_area"}));
        var main_area = _$("<div/>", {class: "absolute_wrapper"});
        main_area.append(_$("<div/>", {class: "seq_number"}).text(seq_number));
        main_area.append(_$("<span/>", {class: "text1"}).html("화질 옵션을 선택하세요"));
        main_area.append(_$("<span/>", {class: "resolution_date_txt"}).text("기간 : "));
        div.find(".main_area").append(main_area);
        var mobile_area = _$("<div/>", {class: "watching_mobile_area"});
        mobile_area.append(_$("<span/>", {class: "watching_mobile_txt"}));
        div.append(mobile_area);

        this.setData(PaymentManager.getOptions());
        return false;
    };

    this.setData = function (data) {
        console.debug(data);

        if (data) {
            option = data[0];

            PaymentManager.selectOption(0);
            div.find(".text1").html("화질 옵션 : " + getMiniTitle(option));

            if (option.viewTime == 999999) {
                div.find(".resolution_date_txt").text("기간 : 해지 시까지");
            } else {
                div.find(".resolution_date_txt").text("기간 : " + Math.floor(option.viewTime / 24) + "일 간 시청 가능");
            }

            if (!KTW.managers.service.ProductInfoManager.isBiz()) {
                if (option.seamlessCode === "2") {
                    div.find(".watching_mobile_area").css("visibility", "inherit");
                    div.find(".watching_mobile_txt").html("<icon/>올레 tv 모바일앱(3대까지 동시시청 가능)에서도 함께 시청 가능합니다");
                } else if (option.seamlessCode === "1") {
                    div.find(".watching_mobile_area").css("visibility", "inherit");
                    div.find(".watching_mobile_txt").html("<icon/>올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다");
                } else {
                    div.find(".watching_mobile_area").css("visibility", "hidden");
                }
            } else {
                div.find(".watching_mobile_area").css("visibility", "hidden");
            }
        }
    };

    function getMiniTitle(option) {
        return "<box>" + (option.contsName || option.itemName) + "</box>";
    }

    this.onKeyAction = function (keyCode) {
        return false;
    };

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        return false;
    };

    this.blurred = function () {
    };
};