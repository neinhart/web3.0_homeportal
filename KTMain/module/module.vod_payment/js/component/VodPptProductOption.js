/**
 * Created by kh.kim2 on 2018-02-05.
 */
payment.component = payment.component||{};
payment.component.VodPptProductOption = function () {
    var div;

    this.create = function (seq_number) {
        div = _$("<div/>", {class:"option resolution"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(_$("<div/>", {class:"seq_number"}).text(seq_number));
        if (VodPptManager.getOptions().length > 1) {
            main_area.append(_$("<span/>", {class: "text1"}).html("구매 상품 : <box>" + VodPptManager.getPptName() + "</box>"));
        }
        else { //옵션이 하나인 경우 가격도 함께 표시
            main_area.append(_$("<span/>", {class: "text1"}).html("구매 상품 : <box>" + VodPptManager.getPptName() + " | " + Util.addComma(VodPptManager.getVatPaymentPrice()) + "원</box>"));
        }
        div.find(".main_area").append(main_area);
        CornerManager.setConnerContract(0);
    };

    this.onKeyAction = function(keyCode) {};

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        return false;
    };

    this.blurred = function () {
    };
};