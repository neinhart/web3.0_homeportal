/**
 * Created by kh.kim2 on 2018-02-05.
 */
payment.component = payment.component||{};
payment.component.VodPptPeriodOption = function (parent) {
    var focus = 0;
    var optionList;
    var selectedIdx = -1;

    var div;

    this.create = function(seq_number) {
        div = _$("<div/>", {class:"option resolution"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(_$("<div/>", {class:"seq_number"}).text(seq_number));
        main_area.append(_$("<span/>", {class:"text1"}).html("시청 기간을 선택해주세요"));
        div.find(".main_area").append(main_area);
        div.append(_$("<div/>", {class:"option_box_area"}));
        makeOptionBox(div.find(".option_box_area"));

        this.setData(VodPptManager.getOptions());
        setFocus(0);
        return true;
    };

    this.setData = function(data) {
        optionList = [];
        for(var i in data) {
            optionList.push(
                {
                    period: getPeriodText(data[i].period),
                    price: data[i].vatPptPrice
                });
        }
        setOptionBox(div.find(".option_box_area .option_box_wrapper"), optionList);
    };

    function getPeriodText(period) {
        // ppt 기간은 시간 단위로 내려온다
        // 일 단위로 변환하여 표시
        var period_day = Math.floor(period / 24);
        switch(period_day) {
            case 0:
                return period + "시간";
            default:
                return period_day + "일";
        }
    }

    function setOptionBox(div, optionList) {
        function setOptionBox(div, option) {
            div.find(".resolution_name").text(option.period);
            div.find(".price").html(Util.addComma(option.price) + "<won>원</won>");
        }
        var i=0;
        for(;i<optionList.length; i++) setOptionBox(div.eq(i).removeClass("none"), optionList[i]);
        for(;i<4; i++) setOptionBox(div.eq(i).addClass("none"), {period:"", price:""});
    }

    function makeOptionBox(div) {
        for(var i=0; i<4; i++) {
            var box = _$("<div/>", {class:"option_box"});
            box.append(_$("<div/>", {class:"optionBtn"}));
            box.append(_$("<div/>", {class:"resolution_area"}).html("<span class='resolution_name'/>"));
            box.append(_$("<span/>", {class:"price"}));
            div.append(_$("<div/>", {class:"option_box_wrapper"}).append(box));
        }
    }

    function setFocus(_focus) {
        focus = _focus;
        div.find(".option_box_area .option_box_wrapper.focus").removeClass("focus");
        div.find(".option_box_area .option_box_wrapper").eq(focus).addClass("focus");
    }

    function selectOption(focus) {
        selectedIdx = focus;
        div.find(".option_box_area .option_box_wrapper.selected").removeClass("selected");
        div.find(".option_box_area .option_box_wrapper").eq(focus).addClass("selected");
        VodPptManager.selectOption(selectedIdx);
        if(parent.onCompleteStep) parent.onCompleteStep();
    }

    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.LEFT:
                setFocus(Math.max(0, focus-1));
                return true;
            case KEY_CODE.RIGHT:
                setFocus(Math.min(focus+1, optionList.length-1));
                return true;
            case KEY_CODE.ENTER:
                selectOption(focus);
                return true;
            case KEY_CODE.DOWN:
                if(selectedIdx==-1) return true;
                else return false;
        }
    };

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        if(optionList.length>1) {
            div.addClass('focus');
            div.find(".text1").html("시청 기간을 선택해주세요");
            return true;
        } else {
            return false;
        }
    };

    this.blurred = function () {
        div.removeClass('focus');
        if(selectedIdx!=-1) div.find(".text1").html("시청 기간 : <box>" + optionList[selectedIdx].period + " | " + Util.addComma(optionList[selectedIdx].price) + "원</box>");
    };
};