/**
 * Created by ksk91_000 on 2016-12-20.
 */
payment.component = payment.component||{};
payment.component.CornerPaymentOption = function (parent) {
    var focusIdx = 0;
    var div;
    var enterKeyBlockInNetworking = false;
    var speechAdapter = oipfAdapter.speechRecognizerAdapter;
    var speechMode = false;

    this.create = function (seq_number) {
        div = _$("<div/>", {class: "option payment"});
        div.append(_$("<div/>", {class:"main_area"}));
        var main_area = _$("<div/>", {class:"absolute_wrapper"});
        main_area.append(_$("<div/>", {class: "seq_number"}).text(seq_number));
        main_area.append(_$("<span/>", {class: "text1"}).html("구매인증"));
        main_area.append(_$("<div>", {class: "password_input_bg"}).text("* * * *"));
        div.find(".main_area").append(main_area);
        div.append(_$("<div/>", {class: "password_area"}));

        setPasswordArea();
        AuthManager.initPW();
        AuthManager.resetPWCheckCount();

        return true;
    };

    function setPasswordArea() {
        var price_area = _$("<div/>", {class: "price_area"});
        price_area.append(_$("<span/>", {class: "payment_amount_txt"}).text("결제 금액"));
        price_area.append(_$("<span/>", {class: "payment_amount_price"}).text("3,000"));

        var right_area = _$("<div/>", {class: "password_right_area"});
        right_area.append(_$("<div>", {class: "password_input_bg"}).text("* * * *"));
        right_area.append(_$("<div>", {class: "password_input_area"}));
        right_area.append(_$("<div>", {class: "password_input_btn ok"}).text("확인"));
        right_area.append(_$("<div>", {class: "password_input_btn cancel"}).text("취소"));

        div.find(".password_area").append(price_area);
        div.find(".password_area").append(_$("<div/>", {class:"password_top_area_wrapper"}).append(right_area));
        div.find(".password_area").append(_$("<div/>", {class:"password_ment"}).html("*부가세 포함된 가격입니다"));
    }

    function inputPassword(addNum) {
        var res = AuthManager.inputPW(addNum, 4);
        var star = "";
        for(var i=AuthManager.getPW().length; i>0; i--) star+="* ";
        div.find(".password_input_area").text(star);

        if(res) setFocus(1, 1);
    }

    var inputSpeechPassword = function(rec, input, mode) {
        if (rec && mode === oipfDef.SR_MODE.PIN) {
            var res = AuthManager.replacePW(input, 4);
            var star = "";
            for(var i=AuthManager.getPW().length; i>0; i--) star+="* ";
            div.find(".password_input_area").text(star);

            if(res) setFocus(1, 1);
        }
    };

    function deletePassword() {
        AuthManager.deletePW();
        var star = "";
        for(var i=AuthManager.getPW().length; i>0; i--) star+="* ";
        div.find(".password_input_area").text(star);
    }

    function clearPassword() {
        AuthManager.initPW();
        div.find(".password_input_area").text("");
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP: return false;
            case KEY_CODE.DOWN: return true;
            case KEY_CODE.LEFT:
            case KEY_CODE.DEL:
                if(focusIdx==0) deletePassword();
                else setFocus(focusIdx-1);
                return true;
            case KEY_CODE.RIGHT:
                setFocus(Math.min(2, focusIdx+1));
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
        }if(keyCode>=KEY_CODE.NUM_0 && keyCode<=KEY_CODE.NUM_9) {
            if(focusIdx==0) inputPassword(keyCode-KEY_CODE.NUM_0);
            return true;
        }
    };

    function enterKeyAction() {
        if(focusIdx===1) {
            if(enterKeyBlockInNetworking) return;
            enterKeyBlockInNetworking = true;
            //확인
            AuthManager.checkUserPW({
                type: AuthManager.AUTH_TYPE.AUTH_BUY_PIN,
                callback: checkPWListener,
                loading: {
                    type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock: true,
                    callback: null,
                    on_off : false
                }
            });
        }else if(focusIdx===2){
            //취소
            LayerManager.deactivateLayer({
                id: "PaymentCornerPopup",
                remove: true
            });
        }
    }

    function checkPWListener(result) {
        enterKeyBlockInNetworking = false;
        switch (result) {
            case AuthManager.RESPONSE.HDS_BUY_AUTH_SUCCESS:
                CornerManager.regCorner(AuthManager.getPW());
                break;
            case AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL:
                showToast("인증번호가 일치하지 않습니다. 다시 입력해 주세요");
            case AuthManager.RESPONSE.HDS_BUY_AUTH_ERROR:
                clearPassword();
                setFocus(0);
                break;
            case AuthManager.RESPONSE.CANCEL:
                clearPassword();
                setFocus(0);
                break;
            case AuthManager.RESPONSE.GOTO_INIT:
                CornerManager.callback(false);
                LayerManager.deactivateLayer({id:parent, targetOnly: true});
                break;
            case AuthManager.RESPONSE.DISCONNECTED_NETWORK:
                clearPassword();
                setFocus(0);
                HTool.openErrorPopup({message:ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                break;
        }
    }

    function setFocus(focus) {
        div.find(".focus").removeClass("focus");
        focusIdx = focus;
        div.find(".password_area").addClass("focus");
        switch(focus) {
            case 0:
                div.find(".password_input_bg").addClass("focus");
                toggleSpeechMode(true);
                break;
            case 1:
                div.find(".password_input_btn.ok").addClass("focus");
                toggleSpeechMode(false);
                break;
            case 2:
                div.find(".password_input_btn.cancel").addClass("focus");
                toggleSpeechMode(false);
                break;
        }
    }

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        div.addClass('focus');
        setFocus(0);

        div.find(".text1").text("구매인증 번호 4자리를 입력해 주세요");
        div.find(".payment_amount_price").text(Util.addComma(CornerManager.getPrice()));
        return true;
    };

    this.blurred = function () {
        div.removeClass('focus');
        div.find(".text1").text("구매인증");
        toggleSpeechMode(false);
        clearPassword();
    };

    this.show = function () {
        clearPassword();
    };

    this.hide = function () {
        toggleSpeechMode(false);
    };

    function toggleSpeechMode(isOn) {
        if(isOn) {
            if(speechMode) return;
            speechMode = true;
            speechAdapter.start(true);
            speechAdapter.addSpeechRecognizerListener(inputSpeechPassword);
        } else {
            if(!speechMode) return;
            speechMode = false;
            speechAdapter.stop();
            speechAdapter.removeSpeechRecognizerListener(inputSpeechPassword);
        }
    }
};