/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var payment_vod_popup = function PaymentVODPopup(options) {
        Layer.call(this, options);
        var div;
        var optionList = [];
        var focus = 0;
        var callback;
        var callbackConsumed = false;
        var instance = this;

        this.init = function(cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup vodPaymentPopup");

            var param = this.getParams();
            callback = param.callback;
            PaymentManager.setData(param);
            PaymentManager.setCallback(onPaymentEnd);
            optionList.push(new payment.component.ResolutionOption(this, changeTitle));

            if (KTW.managers.service.ProductInfoManager.isMultiroomPackage()) {
                // 개별 결제수단 처리
                optionList.push(new payment.component.BizPaymentOption(this, false));
            }
            else {
                // 일반 결제
                optionList.push(new payment.component.PaymentOption(this));
            }
            var options = PaymentManager.getOptions();
            var title = (options && options.length > 0)? options[0].contsName : "";
            constructDiv(title);

            if(cbCreate) cbCreate(true);

            var that = this;
            if(options.length===1) {
                var optionData = options[0];
                checkPlayableResolProcess(optionData.resolName, function (res) {
                    if(!res) {
                        callCallback(false);
                        LayerManager.deactivateLayer({id: that.id, onlyTarget:true});
                    }
                });
            }
        };

        function changeTitle(title) {
            div.find(".popup_title2").text("'" + title + "'");
        }

        function onPaymentEnd(result, data) {
            callCallback(result, data);
        }

        function constructDiv(title) {
            div.append(_$("<div/>", {class: "popup_title_area"}));
            div.find(".popup_title_area").append(_$("<div/>", {class: "popup_title2"}).text("'" + title + "'"));
            div.find(".popup_title_area").append(_$("<div/>", {class: "coupon_txt"}).html(""));

            var contListArea = _$("<table/>", {class: "popup_contents buyProcessContents"});
            div.append(contListArea);

            contListArea.append(_$("<div/>", {class:'sep_line'}));
            var setFocused = false;
            for(var option in optionList) {
                optionList[option].create(option-0+1)?((setFocused?0:initFocus(option)),setFocused=true):0;
                contListArea.append(optionList[option].getView());
                contListArea.append(_$("<div/>", {class:'sep_line'}));
            } div.find(".option.focus").prev().addClass("white");
        }

        function initFocus(_focus) {
            if(!optionList[_focus]) return;
            optionList[_focus].focused();
            focus = _focus-0;
        }

        function setFocus(_focus) {
            if(!optionList[_focus]) return;
            div.find(".white").removeClass("white");
            if(optionList[_focus].focused()) {
                optionList[focus].blurred();
                focus = _focus-0;
            } div.find(".option.focus").prev().addClass("white");
        }

        function callCallback(res, data) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                callback(res, data);
                LayerManager.deactivateLayer({id: instance.id, onlyTarget: true});
            }
        }

        this.controlKey = function(keyCode) {
            if(optionList[focus].onKeyAction(keyCode)) return true;
            switch(keyCode) {
                case KEY_CODE.DOWN:
                    setFocus(focus+1);
                    return true;
                case KEY_CODE.UP:
                    setFocus(focus-1);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({
                        id: this.id,
                        remove: true
                    });
                    callCallback(false);
                    return true;
            }
        };

        this.show = function (options) {
            LayerManager.stopLoading();
            Layer.prototype.show.apply(this, arguments);

            for(var i in optionList) {
                if(optionList[i].hasOwnProperty("show")) optionList[i].show(options);
            }
        };

        this.hide = function (options) {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(false);

            for(var i in optionList) {
                if(optionList[i].hasOwnProperty("hide")) optionList[i].hide(options);
            }
        };

        this.onCompleteStep = function() {
            setFocus(focus+1);
        };
    };

    payment_vod_popup.prototype = new Layer();
    payment_vod_popup.prototype.constructor = payment_vod_popup;

    //Override create function
    payment_vod_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_vod_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentVODPopup"] = payment_vod_popup;
})();