/**
 * Created by kh.kim2 on 2018-01-09.
 */
(function() {
    var payment_point_popup = function PaymentPointPopup(options) {
        Layer.call(this, options);

        var div;
        var callback;
        var callbackConsumed = false;

        this.init = function(cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup paymentPointPopup");

            var param = this.getParams();
            callback = param.callback;

            constructDiv(param.data);

            if(cbCreate) cbCreate(true);

        };

        var addComma = function(price) {
            if (price === 0) {
                return price;
            }

            return Util.addComma(price);
        }

        function constructDiv(data) {
            var contCoupon,
                ktMembership,
                ktTvCoupon,
                tvPoint,
                tvCoupon,
                exceptVatDiv  = _$("<div/>"),
                includeVatDiv = _$("<div/>"),
                dcDiv = _$("<div/>"),
                sumDiv = _$("<div/>");

            var check = false;
            var margin = 0;
            // 시리즈 할인권인지 콘텐츠 할인권인지 구분해야함
            if (data.couponPrice) {
                contCoupon =  _$("<div/>", {id: "popup_coup"}).append(
                    _$("<div/>", {id: "popup_coup_txt", class: "popup_point_txt2"}).text(PaymentManager.getCouponString()),
                    _$("<span/>", {id: "popup_coup_price", class: "popup_point_txt2"}).text(addComma(data.couponPrice)),
                    _$("<span/>", {id: "popup_coup_price_won", class: "popup_point_txt_won"}).text("원")
                );
                check = true;
            }

            if (data.ktMembership) {
                margin = (check) ? 14 : 0;
                ktMembership =  _$("<div/>", {id: "popup_ktm", style: "margin-top:" + margin + "px"}).append(
                    _$("<div/>", {id: "popup_ktm_txt", class: "popup_point_txt2"}).text("KT 멤버십(금액의 "+data.ktMembRate+"%)"),
                    _$("<span/>", {id: "popup_ktm_price", class: "popup_point_txt2"}).text(addComma(data.ktMembership)),
                    _$("<span/>", {id: "popup_ktm_price_won", class: "popup_point_txt_won"}).text("P")
                );
                check = true;
            }

            if (data.ktTvCoupon) {
                margin = (check) ? 14 : 0;
                ktTvCoupon =  _$("<div/>", {id: "popup_ktc", style: "margin-top:" + margin + "px"}).append(
                    _$("<div/>", {id: "popup_ktc_txt", class: "popup_point_txt2"}).text("TV쿠폰(KT 제공)"),
                    _$("<span/>", {id: "popup_ktc_price", class: "popup_point_txt2"}).text(addComma(data.ktTvCoupon)),
                    _$("<span/>", {id: "popup_ktc_price_won", class: "popup_point_txt_won"}).text("원")
                );
            }

            check = false;
            if (data.tvPoint) {
                tvPoint =  _$("<div/>", {id: "popup_tvp"}).append(
                    _$("<div/>", {id: "popup_tvp_txt", class: "popup_point_txt2"}).text("TV포인트"),
                    _$("<span/>", {id: "popup_tvp_price", class: "popup_point_txt2"}).text(addComma(data.tvPoint)),
                    _$("<span/>", {id: "popup_tvp_price_won", class: "popup_point_txt_won"}).text("P")
                );
                check = true;
            }

            if (data.tvCoupon) {
                margin = (check) ? 14 : 0;
                tvCoupon = _$("<div/>", {id: "popup_tvc", style: "margin-top:" + margin + "px"}).append(
                    _$("<div/>", {id: "popup_tvc_txt", class: "popup_point_txt2"}).text("TV쿠폰"),
                    _$("<span/>", {id: "popup_tvc_price", class: "popup_point_txt2"}).text(addComma(data.tvCoupon)),
                    _$("<span/>", {id: "popup_tvc_price_won", class: "popup_point_txt_won"}).text("원")
                );
            }

            if (contCoupon || ktMembership || ktTvCoupon) {
                exceptVatDiv =  _$("<div/>", {id: "popup_except_vat", class:"popup_box_content"});
                exceptVatDiv.append(contCoupon);
                exceptVatDiv.append(ktMembership);
                exceptVatDiv.append(ktTvCoupon);

                // 할인 후 금액은 위 3가지 중 하나라도 사용한 경우에만 표시
                dcDiv = _$("<div/>", {id: "popup_dc", class:"popup_box_content"}).append(
                    _$("<div/>", {id: "popup_dc_txt", class: "popup_point_txt1"}).text("할인 후 금액"),
                    _$("<span/>", {id: "popup_dc_price", class: "popup_point_txt1"}).text(addComma(data.discountPrice))
                );
            }

            if (tvPoint || tvCoupon) {
                includeVatDiv =  _$("<div/>", {id: "popup_include_vat", class:"popup_box_content"});
                includeVatDiv.append(tvPoint);
                includeVatDiv.append(tvCoupon);

                // 합계는 위 2가지 중 하나라도 사용한 경우에만 표시
                if (data.useTotalPrice) {
                    sumDiv = _$("<div/>", {id: "popup_sum", class: "popup_box_content"}).append(
                        _$("<div/>", {id: "popup_sum_txt", class: "popup_point_txt1"}).text("합계"),
                        _$("<span/>", {id: "popup_sum_price", class: "popup_point_txt1"}).text(addComma(data.useTotalPrice))
                    );
                }
            }

            var totalDiv = _$("<div/>", {id: "popup_total", class:"popup_box_content"}).append(
                _$("<div/>", {id: "popup_total_txt", class: "popup_point_txt1"}).text("프로그램 금액"),
                _$("<span/>", {id: "popup_total_price", class: "popup_point_txt1"}).text(addComma(data.supplyPrice))
            );



            var vatDiv = _$("<div/>", {id: "popup_vat", class:"popup_box_content"}).append(
                _$("<div/>", {id: "popup_vat_txt", class: "popup_point_txt1"}).text("부가세"),
                _$("<span/>", {id: "popup_vat_price", class: "popup_point_txt1"}).text("+ " + addComma(data.vatPrice))
            );



            var payDiv = _$("<div/>", {id: "popup_pay", class:"popup_box_content"}).append(
                _$("<div/>", {id: "popup_pay_txt", class: "popup_point_txt3"}).text("실제 청구 금액"),
                _$("<span/>", {id: "popup_pay_price", class: "popup_point_txt3",style: "vertical-align:middle; font-size:45px;"}).text(addComma(data.paymentPrice))
            );

            div.append(
                _$("<div/>", {class: "popup_title"}).text("결제 상세보기"),
                _$("<div/>", {class: "popup_box"}).append(
                    totalDiv,
                    exceptVatDiv,
                    dcDiv,
                    vatDiv,
                    sumDiv,
                    includeVatDiv,
                    payDiv,
                    _$("<div/>", {id: "popup_last_bar"})
                ),
                _$("<div/>", {class: "popup_button"}).text("확인")
            );
        }

        function callCallback(res, data) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                callback(res, data);
                LayerManager.deactivateLayer({id: "PaymentPointPopup", onlyTarget: true});
            }
        }

        this.controlKey = function(keyCode) {
            switch(keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({
                        id: this.id,
                        remove: true
                    });
                    callCallback(false);
                    return true;
            }
        };

        this.show = function (options) {
            LayerManager.stopLoading();
            Layer.prototype.show.apply(this, arguments);
            var btnOffset = div.find(".popup_button").offset().top - 90;
            window.log.printDbg("btnOffset = " + btnOffset);
            var moveOffset = Math.floor((1080 - btnOffset) / 2);
            window.log.printDbg("moveOffset = " + moveOffset);
            div.find(".popup_title").css("margin-top", moveOffset + "px");
        };

        this.hide = function (options) {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(false);
        };

        this.onCompleteStep = function() {

        };
    };

    payment_point_popup.prototype = new Layer();
    payment_point_popup.prototype.constructor = payment_point_popup;

    //Override create function
    payment_point_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_point_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentPointPopup"] = payment_point_popup;
})();