/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var payment_package_popup = function PaymentPackagePopup(options) {
        Layer.call(this, options);
        var div;
        var optionList = [];
        var focus = 0;
        var callback;

        this.init = function(cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup vodPaymentPopup");

            var param = this.getParams();
            callback = param.callback;
            PaymentManager.setData(param);
            PaymentManager.setCallback(onPaymentEnd);
            PaymentManager.selectOption(0);

            optionList.push(new payment.component.PackageOption(this));
            optionList.push(new payment.component.PaymentOption(this));

            var options = PaymentManager.getOptions();
            var title = (options && options.length > 0)? (options[0].contsName || options[0].itemName)  : "";
            constructDiv(title);

            if(cbCreate) cbCreate(true);
        };

        function onPaymentEnd(result, data) {
            callback(result, data);
        }

        function constructDiv(title) {
            div.append(_$("<div/>", {class: "popup_title"}).text("'" + title + "'"));
            var contListArea = _$("<table/>", {class: "popup_contents buyProcessContents"});
            div.append(contListArea);
            contListArea.append(_$("<div/>", {class:'sep_line'}));

            var setFocused = false;
            for(var option in optionList) {
                optionList[option].create(option-0+1)?((setFocused?0:initFocus(option)),setFocused=true):0;
                contListArea.append(optionList[option].getView());
                contListArea.append(_$("<div/>", {class:'sep_line'}));
            } div.find(".option.focus").prev().addClass("white");
        }

        function initFocus(_focus) {
            if(!optionList[_focus]) return;
            optionList[_focus].focused();
            focus = _focus-0;
        }

        function setFocus(_focus) {
            if(!optionList[_focus]) return;
            div.find(".white").removeClass("white");
            if(optionList[_focus].focused()) {
                optionList[focus].blurred();
                focus = _focus-0;
            } div.find(".option.focus").prev().addClass("white");
        }

        this.controlKey = function(keyCode) {
            if(optionList[focus].onKeyAction(keyCode)) return true;
            switch(keyCode) {
                case KEY_CODE.DOWN:
                    setFocus(Util.indexPlus(focus, optionList.length));
                    return true;
                case KEY_CODE.UP:
                    setFocus(Util.indexMinus(focus, optionList.length));
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({
                        id: this.id,
                        remove: true
                    });
                    if(callback) callback(false);
                    return true;
            }
        }
    };

    payment_package_popup.prototype = new Layer();
    payment_package_popup.prototype.constructor = payment_package_popup;

    //Override create function
    payment_package_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_package_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentPackagePopup"] = payment_package_popup;
})();