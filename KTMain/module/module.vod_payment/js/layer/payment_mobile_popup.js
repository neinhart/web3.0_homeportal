/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

"use strict";

(function() {
    var paymentMobilePopup = function PaymentMobilePopup(options) {
        Layer.call(this, options);

        var util = UTIL;

        var instance = this;
        var div = null;
        var params = null;

        var vodData = null;

        var FOCUS_MOBILE_FIRST_NUMBER = 0;
        var FOCUS_MOBILE_MIDDLE_NUMBER = 1;
        var FOCUS_MOBILE_LAST_NUMBER = 2;

        var FOCUS_OK = 3;
        var FOCUS_CANCEL = 4;

        var initShow = false;

        var callbackFunc = null;

        var rootDiv = null;

        var focusIndex = -1;

        var mobileNumber = []; // div

        var mobileNumberInputString = ["", "", ""];

        var btnOk = null;
        var btnCancel = null;


        this.init = function(cbCreate) {

            params = this.getParams();

            if (params !== null) {
                callbackFunc = params.callback;
                vodData = params.vodData;
            }

            div = this.div;
            div.addClass("payment gniPopup mobilePaymentPopup");
            _createView(div);
            focusIndex = 0;

            if (cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            var consumed = false;
            switch (key_code) {
                case KEY_CODE.BACK:
                    callCallback();
                    consumed = true;
                    break;
                case KEY_CODE.EXIT:
                    callCallback();
                    consumed = true;
                    break;
                case KEY_CODE.OK:
                case KEY_CODE.ENTER:
                    if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex++;
                        _focusUpdate();
                    } else if (focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if (focusIndex === FOCUS_OK) {
                            //checkEmptyElement
                            var isEmptyElement = false;
                            if (mobileNumberInputString[0] === "") {
                                focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                                isEmptyElement = true;
                            } else if (mobileNumberInputString[1] === "") {
                                focusIndex = FOCUS_MOBILE_MIDDLE_NUMBER;
                                isEmptyElement = true;
                            } else if (mobileNumberInputString[2] === "") {
                                focusIndex = FOCUS_MOBILE_LAST_NUMBER;
                                isEmptyElement = true;
                            }

                            if (isEmptyElement) {
                                _focusUpdate();
                                return true;
                            }

//                            LayerManager.deactivateLayer({
//                                id: instance.id,
//                                remove: true
//                            });

                            LayerManager.startLoading();
                            // 모바일 문자 전송 후 전송완료 팝업 처리
                            vodData["mobileNumber"] = (mobileNumberInputString[0] + mobileNumberInputString[1] + mobileNumberInputString[2]);
                            payment.amocManager.useMobilePay(_mobilePayResponse, vodData);

                        } else if (focusIndex === FOCUS_CANCEL) {
                            if (callbackFunc !== null) {
                                callbackFunc(false);
                            }
                            LayerManager.deactivateLayer({
                                id: instance.id,
                                remove: true
                            });
                        }

                    }
                    consumed = true;
                    break;
                case KEY_CODE.LEFT:
                    if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {

                        if (mobileNumberInputString[focusIndex].length <= 0) {
                            focusIndex--;
                            if (focusIndex < FOCUS_MOBILE_FIRST_NUMBER) {
                                focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                            }
                        } else {
                            mobileNumberInputString[focusIndex] = mobileNumberInputString[focusIndex].substring(0, mobileNumberInputString[focusIndex].length - 1);
                            mobileNumber[focusIndex].text(mobileNumberInputString[focusIndex]);
                        }
                    } else if (focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if (focusIndex === FOCUS_CANCEL) {
                            focusIndex = FOCUS_OK;
                        }
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KEY_CODE.RIGHT:
                    if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex++;
                        if (focusIndex > FOCUS_MOBILE_LAST_NUMBER) {
                            focusIndex = FOCUS_MOBILE_LAST_NUMBER;
                        }
                    } else if (focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if (focusIndex === FOCUS_OK) {
                            focusIndex = FOCUS_CANCEL;
                        }
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KEY_CODE.UP:
                    if (focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KEY_CODE.DOWN:
                    if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex = FOCUS_OK;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
            }

            if (consumed === false) {
                var number = util.keyCodeToNumber(key_code);
                if (number >= 0) {
                    if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        var maxlength = 0;
                        if ((focusIndex - FOCUS_MOBILE_FIRST_NUMBER) === 0) {
                            maxlength = 3;
                        } else {
                            maxlength = 4;
                        }
                        if (mobileNumberInputString[focusIndex].length >= maxlength) {
                            mobileNumberInputString[focusIndex] = "" + number;
                        } else {
                            mobileNumberInputString[focusIndex] += number;
                        }

                        mobileNumber[focusIndex].text(mobileNumberInputString[focusIndex]);

                        if (mobileNumberInputString[focusIndex].length >= maxlength) {
                            focusIndex++;
                            if (focusIndex > FOCUS_MOBILE_LAST_NUMBER) {
                                focusIndex = FOCUS_OK;
                            }
                            _focusUpdate();
                        }
                    }
                    consumed = true;
                }
            }

            return consumed;
        };

        function _resetFocusMobileNumber() {
            div.find(".input_box.focus").removeClass("focus");
            for (var i = 0; i < mobileNumber.length; i++) {
                var mobilenumberobj = mobileNumber[i];
                if (focusIndex != i) {
                    if (mobileNumberInputString[i] === "") {
                        if (i === 0) {
                            mobilenumberobj.text("000");
                        } else {
                            mobilenumberobj.text("0000");
                        }
                    }
                }
            }
        }

        function _resetFocusBtn() {
            btnOk.removeClass("focus");
            btnCancel.removeClass("focus");
        }

        function _focusUpdate() {
            _resetFocusMobileNumber();
            _resetFocusBtn();

            if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                // 번호 입력 란에 포커싱 된 경우
                // 현재 입력중인 번호의 default text 를 없애고 빈 영역으로 만든다
                mobileNumber[focusIndex].addClass("focus");
                if (mobileNumberInputString[focusIndex] === "") {
                    mobileNumber[focusIndex].text("");
                }

            } else if (focusIndex === FOCUS_OK) {
                btnOk.addClass("focus");
            } else if (focusIndex === FOCUS_CANCEL) {
                btnCancel.addClass("focus");
            }
        }

        function _showView() {
            log.printDbg("_showView()");

            if (initShow === false) {
                initShow = true;
                _focusUpdate();
            }
        }

        function _keyOk() {
            log.printDbg("_keyOk()");

        }

        function _hideView() {
            log.printDbg("_hideView()");
        }


        function _mobilePayResponse(res, data) {
            LayerManager.stopLoading();

            if (res) {
                if (data.responseCode == "0000") {
                    showSuccessPopup();
                } else {
                    HTool.openErrorPopup({message: data.responseMessage.split("\\n"), reboot: false, callback: callCallback});
                }
            }
            else {
                HTool.openErrorPopup({message: ERROR_TEXT.ET0013, reboot: true, callback: callCallback});
                extensionAdapter.notifySNMPError("VODE-00013");
            }
        }

        function callCallback() {
            if (callbackFunc) {
                callbackFunc(false)
            }
            LayerManager.deactivateLayer({
                id: instance.id,
                remove: true
            });
        }

        function showSuccessPopup() {
            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: "Title",
                    message: ["휴대폰 결제"],
                    cssObj: {}
                }],
                arrButton: [{id: "ok", name: "확인"}],
                cbAction: function (buttonId) {
                    log.printDbg("cbAction() buttonId : " + buttonId);
                    if (!isCalled) {
                        isCalled = true;
                        callCallback();
                        LayerManager.deactivateLayer({
                            id: MOBILE_PAYMENT_SUCCESS_POPUP,
                            remove: true
                        });
                    }
                    return true;
                }
            };

            var arrMessage = ["문자가 전송되었습니다", "휴대폰에서 결제를 진행해 주세요"];
            popupData.arrMessage.push({
                type: "MSG_38",
                message: arrMessage,
                cssObj: {}
            });

            // TODO 완료 팝업이 떠 있는 중에 결제 완료에 의해 PUSH 팝업이 발생하는 경우 이 팝업은 닫아줘야 한다..
            LayerManager.activateLayer({
                obj: {
                    id: MOBILE_PAYMENT_SUCCESS_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        data : popupData
                    }
                },
                moduleId: Module.ID.MODULE_VOD_PAYMENT,
                visible: true
            });
        }



        function _createView(parent_div) {
            log.printDbg("_createView()");


            parent_div.append(_$("<span/>", {class: "popup_title_text"}).html("휴대폰 결제"));

            // 콘텐츠 타이틀, 화질, 기간
            var contsName = "'" + vodData.itemName + "'";
            parent_div.append(_$("<span/>", {class: "conts_name"}).html(contsName));


            var optArea = _$("<div/>", {class: "opt_area"});
            var optName = _$("<span/>", {class: "opt_name"}).html(getMiniTitle(vodData));
            optArea.append(optName);

            parent_div.append(optArea);


            // 휴대폰 번호 입력 영역
            rootDiv = _$("<div/>", {class: "phone_area"});

            rootDiv.append(_$("<div/>", {class: "phone_img_l"}));
            rootDiv.append(_$("<div/>", {class: "phone_img_m"}));
            rootDiv.append(_$("<div/>", {class: "phone_img_r"}));

            parent_div.append(rootDiv);
            parent_div.append(_$("<span/>", {class: "msg"}).text("휴대폰 요금청구서에는 소액결제로 표기"));

            _createMobileNumberElement();
            _createBtnElement();
        }

        function _createMobileNumberElement() {

            rootDiv.append(_$("<span/>", {class: "top_text"}).html("스마트폰에서만 결제 가능합니다<br>결제할 휴대폰 번호를 입력해 주세요"));
            rootDiv.append(_$("<span/>", {class: "bottom_text"}).html("휴대폰 번호"));

            rootDiv.append(_$("<div/>", {class: "bar"}));
            rootDiv.append(_$("<div/>", {class: "bar"}).css("left", "625px"));


            /**
             * LEFT 483 , TOP 237
             */
            var leftStart = 270;

            for (var i = 0; i < 3; i++) {
                var defaultTitle = "";
                if (i === 0) {
                    defaultTitle = "000";
                } else {
                    defaultTitle = "0000";
                }

                var btnDiv = _$("<div/>", {class: "input_box"}).css("left", leftStart + "px");
                btnDiv.html(defaultTitle);
                rootDiv.append(btnDiv);

                mobileNumber[mobileNumber.length] = btnDiv;

                leftStart += 158;
                leftStart += 30;
            }
        }

        function _createBtnElement() {
            var leftStart = 673;

            for (var i = 0; i < 2; i++) {
                var btnDiv = _$("<div/>", {class: "button_area"}).css("left", leftStart + "px");
                div.append(btnDiv);

                if (i === 0) {
                    btnOk = btnDiv;
                    btnOk.text("문자 전송");
                } else {
                    btnCancel = btnDiv;
                    btnCancel.text("취소");
                }

                leftStart+=280;
                leftStart+=12;
            }
        }
    };

    function getMiniTitle(option) {
        var result;

        if (option.pkgYn == "C") {
            // VOD PPT는 화질이 없으므로 가격만 표시
            result = "가격 : <white>";
            result += Util.addComma(VodPptManager.getVatPaymentPrice()) + "원</white>";
        }
        else {
            result = "화질 : <white>"
            if (option.pkgYn == "Y") {
                result += "시리즈";
            }
            else if (option.pkgYn == "K") {
                result += "패키지";
            }
            else if (option.isDvdYn == "Y") {
                result += "소장";
            }
            else {
                result += "단편";
            }

            result += " " + option.resolName + "<smallbar></smallbar>";
            result += Util.addComma(PaymentManager.getVatPaymentPrice()) + "원</white>";
        }

        result += "<bar></bar>";
        if (option.period == 999999) {
            result += "기간 : <white>해지 시까지<white>";
        } else {
            result += "기간 : <white>" + Math.floor((option.period) / 24) + "일 간 시청 가능<white>";
        }

        return result;
    }

    paymentMobilePopup.prototype = new Layer();
    paymentMobilePopup.prototype.constructor = paymentMobilePopup;

    paymentMobilePopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    paymentMobilePopup.prototype.show = function(options) {
        LayerManager.stopLoading();
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    paymentMobilePopup.prototype.hide = function(options) {
        Layer.prototype.hide.call(this);
        this.hideView(options);
    };

    paymentMobilePopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentMobilePopup"] = paymentMobilePopup;

})();