/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var upselling_ots_popup = function UpsellingOTSPopup(options) {
        Layer.call(this, options);
        var div;
        var id = this.id;

        this.init = function(cbCreate) {
            div = this.div;
            this.div.addClass("payment gniPopup upsellingOTSPopup");
            constructDiv();
            if (cbCreate) cbCreate(true);
        };

        function constructDiv() {
            div.append(_$("<div/>", {class: "popupTitle"}).text("알림"));
            div.append(_$("<div/>", {class: "text1"}).html("이 전용 메뉴의 콘텐츠는 15요금제 이상 고객님께 무료로 제공됩니다<br>요금제를 업그레이드하셔서 더 많은 채널과 다양한 유료 VOD를 즐기세요"));
            div.append(_$("<div/>", {class: "text2"}).text("상품 업그레이드 문의 : 국번 없이 100번"));
            div.append(_$("<div/>", {class: "text3"}).text("(평일 09-18시, 토요일 09-13시)"));
            div.append(_$("<div/>", {class: "button"}));
            div.find(".button").append(_$("<div/>", {class: "buttonText"}).text("확인"));
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.EXIT:
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: id});
                    return true;
            }
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        }
    };

    upselling_ots_popup.prototype = new Layer();
    upselling_ots_popup.prototype.constructor = upselling_ots_popup;

    //Override create function
    upselling_ots_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    upselling_ots_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["UpsellingOTSPopup"] = upselling_ots_popup;
})();