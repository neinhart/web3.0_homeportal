/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>UpsellingProductDetailPopup</code>
 * params : {
 *    product_name :
 *    img :
 *    callback :
 * }
 * 업셀링 상품 상세 정보 팝업
 * @author jjh1117
 * @since 2016. 12. 30.
 */

"use strict";

(function() {
    var upsellingProductDetailPopup = function UpsellingProductDetailPopup(options) {
        Layer.call(this, options);

        var util = UTIL;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var title = null;

        var showImageName = null;
        var callbackFunc = null;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();

            if(params !== null) {
                title = "'"+ params.data.product_name +"'" + " 주요채널";
                showImageName = params.data.img;
                callbackFunc = params.data.callback;
            }

            div = this.div;
            _createView(div);

            if(cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };


        this.controlKey = function(key_code) {
            log.printDbg("controlKey() key_code : " + key_code);
            var consumed = false;
            switch (key_code) {
                case KEY_CODE.BACK :
                case KEY_CODE.OK:
                case KEY_CODE.ENTER:
                    if(callbackFunc !== null) {
                        callbackFunc();
                    }
                    consumed = true;
                    break;
                case KEY_CODE.EXIT:
                    if(callbackFunc !== null) {
                        callbackFunc();
                    }
                    break;
            }

            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: CONSTANT.RESOLUTION.WIDTH, height: CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 112 , width: CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:"","letter-spacing":-1.6
                    }
                },
                text: title,
                parent: popup_container_div
            });


            var tempspan = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "product_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 816 , width: CONSTANT.RESOLUTION.WIDTH, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center",
                        display:"","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: popup_container_div
            });

            tempspan.html("※ 상품별 전체 채널 정보는 <span style='border-bottom: solid 2px;'>tv.kt.com</span> 에서 확인 가능합니다");

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn" ,
                    css: {
                        position: "absolute",
                        left: 820, top: 899, width:280, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: ""
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 820 , top: 899+18 , width: 280, height: 32,
                        color: "rgba(255,255,255,1)", "font-size": 30 , "text-align": "center"
                    }
                },
                text: "확인",
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "next_program_reservation_img",
                    src: showImageName,
                    css: {
                        position: "absolute",
                        left: 296,
                        top: 163,
                        width: 1328,
                        height: 648
                    }
                },
                parent: popup_container_div
            });
        }



        function _showView() {
            log.printDbg("_showView()");
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

    };

    upsellingProductDetailPopup.prototype = new Layer();
    upsellingProductDetailPopup.prototype.constructor = upsellingProductDetailPopup;

    upsellingProductDetailPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    upsellingProductDetailPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };
    upsellingProductDetailPopup.prototype.hide = function() {
        this.hideView();
    };
    upsellingProductDetailPopup.prototype.destroy = function() {
        this.destroyView();
    };
    upsellingProductDetailPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_UPSELLING_PRODUCT_DETAIL_POPUP] = upsellingProductDetailPopup;

})();