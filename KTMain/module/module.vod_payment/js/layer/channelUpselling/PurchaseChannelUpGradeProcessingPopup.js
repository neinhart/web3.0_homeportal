/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>PurchaseChannelUpGradeProcessingPopup</code>
 * params : {
 *    callback :
 * }
 * 미가입 채널에 대한 가입 완료 팝업
 * @author jjh1117
 * @since 2016. 12. 30.
 */

"use strict";

(function() {
    var purchaseChannelUpGradeProcessingPopup = function PurchaseChannelUpGradeProcessingPopup(options) {
        Layer.call(this, options);

        var util = UTIL;

        var popup_container_div = null;
        var div = null;
        var params = null;
        var initshow = false;


        var productName = null;
        var callbackFunc = null;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();

            if(params !== null) {
                productName = params.data.product_name;
                callbackFunc = params.data.callback;
            }

            div = this.div;
            _createView(div);

            if(cbCreate) cbCreate(true);
        };

        this.showView = function(options) {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };


        this.controlKey = function(key_code) {
            var consumed = false;
            switch (key_code) {
                case KEY_CODE.OK:
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    consumed = true;
                    break;
            }

            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: CONSTANT.RESOLUTION.WIDTH, height: CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 329 , width: CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center","letter-spacing":-1.65,
                        display:""
                    }
                },
                text: "상품 업그레이드",
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "product_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 401 , width: CONSTANT.RESOLUTION.WIDTH, height: 47,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 45 , "text-align": "center","letter-spacing":-2.25,
                        display:""
                    }
                },
                text: "‘" + productName +"’",
                parent: popup_container_div
            });


            var spinner_tag = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "spinner",
                    css: {
                        position: "absolute",
                        left:((CONSTANT.RESOLUTION.WIDTH - 164) / 2),
                        top: 510,
                        width: 164,
                        height: 28,
                        overflow: "hidden"
                    }
                },
                parent: div
            });

            for (var i = 0; i < 6; i++) {
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "circle_" + i,
                        class: "spin_circle",
                        css: {
                            "margin-top": 4
                        }
                    },
                    parent: spinner_tag
                });
            }


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 609 , width: CONSTANT.RESOLUTION.WIDTH, height: 47,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 45 , "text-align": "center","letter-spacing":-2.25,
                        display:""
                    }
                },
                text: "상품 업그레이드 중 입니다",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 691 , width: CONSTANT.RESOLUTION.WIDTH, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35,
                        display:""
                    }
                },
                text: "※ 변경된 채널 정보를 반영하는데 일정 시간이 소요됩니다",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 735 , width: CONSTANT.RESOLUTION.WIDTH, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35,
                        display:""
                    }
                },
                text: "리모컨의 채널 +/-키로 다른 채널 이동이 가능합니다",
                parent: popup_container_div
            });
        }



        function _showView() {
            log.printDbg("_showView()");

            if(initshow === false) {
                initshow = true;
            }
        }


        function _keyOk() {
            log.printDbg("_keyOk()");

        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

    };

    purchaseChannelUpGradeProcessingPopup.prototype = new Layer();
    purchaseChannelUpGradeProcessingPopup.prototype.constructor = purchaseChannelUpGradeProcessingPopup;

    purchaseChannelUpGradeProcessingPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    purchaseChannelUpGradeProcessingPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };
    purchaseChannelUpGradeProcessingPopup.prototype.hide = function() {
        this.hideView();
    };

    purchaseChannelUpGradeProcessingPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_UPGRADE_PROCESSING_POPUP] = purchaseChannelUpGradeProcessingPopup;
})();