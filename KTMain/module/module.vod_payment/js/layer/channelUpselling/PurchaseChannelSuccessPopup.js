/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>PurchaseChannelSuccessPopup</code>
 * params : {
 *    callback :
 * }
 * 미가입 채널에 대한 가입 완료 팝업
 * @author jjh1117
 * @since 2016. 12. 30.
 */

"use strict";

(function() {
    var purchaseChannelSuccessPopup = function PurchaseChannelSuccessPopup(options) {
        Layer.call(this, options);

        var util = UTIL;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var initshow = false;

        var callbackFunc = null;

        var totalHeight = 0;

        var rootDiv = null;

        var btnList = [];

        var btnFocusIndex = -1;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();

            if (params !== null) {
                callbackFunc = params.data.callback;
            }

            div = this.div;
            _createView(div);

            var topStart = (CONSTANT.RESOLUTION.HEIGHT - totalHeight) / 2;
            rootDiv.css({top: topStart, height: totalHeight});

            if (cbCreate) cbCreate(true);
        };

        this.showView = function(options) {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            var consumed = false;
            switch (key_code) {
                case KEY_CODE.OK:
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    if (callbackFunc !== null) {
                        callbackFunc(btnFocusIndex);
                    }
                    consumed = true;
                    break;
            }

            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: CONSTANT.RESOLUTION.WIDTH, height: CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });

            rootDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "root_div",
                    css: {
                        position: "absolute",
                        left: 0, width: CONSTANT.RESOLUTION.WIDTH
                    }
                },
                parent: popup_container_div
            });

            totalHeight = 0;

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: totalHeight, width: CONSTANT.RESOLUTION.WIDTH, height: 34,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center","letter-spacing":-1.65,
                        display:""
                    }
                },
                text: "가입",
                parent: rootDiv
            });

            totalHeight += 33;

            totalHeight += 41;

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0, top: totalHeight, width: CONSTANT.RESOLUTION.WIDTH, height: 47,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 45 , "text-align": "center","letter-spacing":-2.25,
                        display:""
                    }
                },
                text: "가입이 완료되었습니다",
                parent: rootDiv
            });

            totalHeight += 45;

            totalHeight += 35;

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "sub_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0, top: totalHeight, width: CONSTANT.RESOLUTION.WIDTH, height: 32,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5,
                        display: ""
                    }
                },
                text: "별도의 해지 신청이 없을 경우 자동 연장됩니다",
                parent: rootDiv
            });
            totalHeight += 30;

            totalHeight += 58;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn",
                    css: {
                        position: "absolute",
                        left: 820, top: totalHeight, width:280, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: ""
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 820, top: totalHeight + 18, width: 280, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: rootDiv
            });

            totalHeight += 62;
        }

        function _showView() {
            log.printDbg("_showView()");

            if (initshow === false) {
                initshow = true;
            }
        }

        function _keyOk() {
            log.printDbg("_keyOk()");

        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

    };

    purchaseChannelSuccessPopup.prototype = new Layer();
    purchaseChannelSuccessPopup.prototype.constructor = purchaseChannelSuccessPopup;

    purchaseChannelSuccessPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    purchaseChannelSuccessPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };
    purchaseChannelSuccessPopup.prototype.hide = function() {
        this.hideView();
    };

    purchaseChannelSuccessPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP] = purchaseChannelSuccessPopup;
})();