/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>LicenseListPopup</code>
 * params : {
 *    callback :
 * }
 * 알뜰폰 사업자 리스트 정보 팝업
 * @author jjh1117
 * @since 2017. 02. 20.
 */

"use strict";

(function() {
    var licenseListPopup = function LicenseListPopup(options) {
        Layer.call(this, options);

        var util = UTIL;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var title = null;
        var showImageName = null;
        var callbackFunc = null;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();
            if (params !== null) {
                callbackFunc = params.data.callback;
            }
            div = this.div;
            title = "알뜰폰 사업자";

            showImageName = homeImpl.get(homeImpl.DEF_FRAMEWORK.ENV_CONSTANT).MVNO_PNG.URL;
            _createView(div);
            if (cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("showView()");
            _showView();

        };

        this.hideView = function() {
            log.printDbg("hideView()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroyView()");
        };

        this.controlKey = function(key_code) {
            var consumed = false;

            switch (key_code) {

                case KEY_CODE.OK:
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    if (callbackFunc !== null) {
                        callbackFunc();
                    }
                    consumed = true;
                    break;
                case KEY_CODE.EXIT:
                    if (callbackFunc !== null) {
                        callbackFunc();
                    }
                    break;
            }
            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: CONSTANT.RESOLUTION.WIDTH, height: CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 119, width: CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33, "text-align": "center",
                        display: ""
                    }
                },
                text: title,
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn",
                    css: {
                        position: "absolute",
                        left: 820, top: 898, width:280, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: ""
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 820 , top: 898+18 , width: 280, height: 32,
                        color: "rgba(255,255,255,1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: showImageName,
                    css: {
                        position: "absolute",
                        left: 497,
                        top: 199,
                        width: 926,
                        height: 636
                    }
                },
                parent: popup_container_div
            });
        }

        function _showView() {

        }

        function _hideView() {

        }

    };

    licenseListPopup.prototype = new Layer();
    licenseListPopup.prototype.constructor = licenseListPopup;

    licenseListPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    licenseListPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    licenseListPopup.prototype.hide = function() {
        this.hideView();
    };

    licenseListPopup.prototype.destroy = function() {
        this.destroyView();
    };

    licenseListPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_MOBILE_LICENSE_LIST_POPUP] = licenseListPopup;
})();