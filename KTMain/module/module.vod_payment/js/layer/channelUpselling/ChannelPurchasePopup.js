/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>PurchaseChannelPopup</code>
 * params : {
 *    is_upselling : true/false ,
 *    is_choice_product = true/false,
 *    is_choice_contract = true/false,
 *    package_list : []
 * }
 * 미가입 채널에 대한 부가상품 가입 또는 Upselling 가입 팝업
 * @author jjh1117
 * @since 2017. 02. 03.
 */

"use strict";

(function() {
    var channelPurchasePopup = function ChannelPurchasePopup(options) {
        Layer.call(this, options);

        var util = UTIL;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var selectUpsellingProductView = null;
        var selectMonthlyView = null;
        var termsAgreeView = null;
        var mobilePhoneAuthView = null;
        var pinInputView = null;

        /**
         * 현재 포커스 위치
         * @type {number} 1 : 상품정보 , 2 : 약정기간 or 가입안내 , 3 : 가입안내 or 휴대폰 인증, 4 : 구매인증
         */
        var focusStep = 2;

        var unfocusStep1 = {};
        var unfocusStep2 = {};
        var unfocusStep3 = {};
        var unfocusStep4 = {};

        var focusStep1Div = null;
        var focusStep2Div = null;
        var focusStep3Div = null;
        var focusStep4Div = null;

        var unfocusTopPosition = [];

        var isUpselling = true;
        var isChoiceProduct = false;
        var isChoiceContract = false;

        var selectAdditionalProductIndex = -1;

        var callbackFunc = null;

        var callbackBackKey = null;

        var purchaseTitle = "";

        var step1Title = ["구매 상품", "상품 선택"];

        var selectProduct = null;

        var pkgList = null;

        var descStr = null;

        var initshow = false;

        var channelPurchasePopupResourcePath = chResourcePath + "channelpurchasepopup/";

        this.init = function(cbCreate) {
            params = this.getParams();

            if (params !== null) {
                isUpselling = params.data.is_upselling;
                isChoiceProduct = params.data.is_choice_product;
                isChoiceContract = params.data.is_choice_contract;
                pkgList = params.data.package_list;
                callbackFunc = params.data.callback;
                selectAdditionalProductIndex = params.data.select_additional_product_index;

                callbackBackKey = params.data.callback_back_key;
                log.printDbg("create() isUpselling : " + isUpselling + " , isChoiceProduct : " + isChoiceProduct + " , isChoiceContract : " + isChoiceContract + " , selectAdditionalProductIndex : " + selectAdditionalProductIndex);

                var custEnv = StorageManager.ps.load(StorageManager.KEY.CUST_ENV);
                log.printDbg("create() custEnv : " + custEnv);
                var oldProdCd = JSON.parse(custEnv).pkgCode;
                log.printDbg("create() oldProdCd : " + oldProdCd);

            }

            focusStep = 1;
            purchaseTitle = "가입하기";

            div = this.div;

            _createView(div, this);

            if (cbCreate) cbCreate(true);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            _showView();
        };

        this.hideView = function(options) {
            log.printDbg("hideView()");
            _hideView();
        };


        this.isProductUpselling = function() {
            return isUpselling;
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey() keyCode : " + key_code);
            var consumed = false;
            if (key_code === KEY_CODE.EXIT || key_code === KEY_CODE.BACK) {
                if (key_code === KEY_CODE.BACK) {
                    /**
                     * activate 와 deactivate를 할 경우 순서가 매우 중요함.
                     * deactivate를 꼮 먼저 해야 함.
                     */
                    LayerManager.deactivateLayer({
                        id: PAYMENT_CHANNEL_PURCHASE_POPUP
                    });

                    if (callbackBackKey !== null) {
                        callbackBackKey(callbackFunc);
                    }
                } else {
                    if (callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    LayerManager.deactivateLayer({
                        id: PAYMENT_CHANNEL_PURCHASE_POPUP
                    });
                }
                consumed = true;
            } else {
                if (focusStep === 1) {
                    consumed = selectUpsellingProductView.controlKey(key_code);
                } else if (focusStep === 2) {
                    consumed = termsAgreeView.controlKey(key_code);
                } else if (focusStep === 3) {
                    consumed = mobilePhoneAuthView.controlKey(key_code);
                } else if (focusStep === 4) {
                    consumed = pinInputView.controlKey(key_code);
                }
            }

            return consumed;
        };

        this.goNextStep = function() {
            log.printDbg("goNextStep()");
            if (focusStep === 1) {
                var selectUpsellingProduct = selectUpsellingProductView.getSelectProductString();
                var length = util.getTextLength(selectUpsellingProduct, "RixHead M", 27, -1.35);
                unfocusStep1.step_title.text("상품 선택 :");
                unfocusStep1.step_title.css({width: 122});
                unfocusStep1.step_comment_div.css({width: (length + 19 * 2)});
                unfocusStep1.step_comment_div.css({left: 182});
                unfocusStep1.step_comment_text.css({width: (length + 19 * 2)});
                unfocusStep1.step_comment_text.text(selectUpsellingProduct);
                unfocusStep1.step_comment_div.css("display", "");
                unfocusStep1.step_comment1_text.css("display", "");

                selectProduct = pkgList[selectUpsellingProductView.getSelectProductIndex()];
            } else if (focusStep === 2) {
                if (termsAgreeView.getIsTermsAgree() === true) {
                    var length = util.getTextLength("약관 동의함", "RixHead M", 27, -1.35);
                    unfocusStep2.step_title.text("가입 안내 :");
                    unfocusStep2.step_comment_div.css({width: (length + 19 * 2)});
                    unfocusStep2.step_comment_text.css({width: (length + 19 * 2)});
                    unfocusStep2.step_comment_text.text("약관 동의함");
                    unfocusStep2.step_comment_div.css("display", "");
                    unfocusStep2.step_comment_div.css({left: 182});
                } else {
                    unfocusStep2.step_title.text("가입 안내");
                    unfocusStep2.step_comment_div.css("display", "none");
                }
            } else if (focusStep === 3) {
                if (mobilePhoneAuthView.getIsMobilePhoneAuth() === true) {
                    var length = util.getTextLength("휴대폰 인증 완료", "RixHead M", 27, -1.35);
                    unfocusStep3.step_title.text("휴대폰 인증 :");
                    unfocusStep3.step_title.css({width: 148});

                    unfocusStep3.step_comment_div.css({width: (length + 19 * 2)});
                    unfocusStep3.step_comment_text.css({width: (length + 19 * 2)});
                    unfocusStep3.step_comment_text.text("휴대폰 인증 완료");
                    unfocusStep3.step_comment_div.css("display", "");
                    unfocusStep3.step_comment_div.css({left: 206});
                } else {
                    unfocusStep3.step_title.text(",휴대폰 인증");
                    unfocusStep3.step_comment_div.css("display", "none");
                }
            }

            focusStep++;
            if (focusStep > 4) {
                focusStep = 4;
            }
            _showFocusStep();
        };

        this.goPrevStep = function() {
            log.printDbg("goPrevStep()");

            focusStep--;
            if (focusStep < 1) {
                focusStep = 1;
            }
            _showFocusStep();
        };

        this.showTermsAgreeDetail = function() {
            var params = {
                terms_desc: descStr,
                callback: _callbackTermsAgreeDetail
            };

            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_TERMS_AGREE_DETAIL_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data: params
                    }
                },
                moduleId: Module.ID.MODULE_VOD_PAYMENT,
                visible: true
            });

        };

        this.purchaseChannel = function(pin) {
            log.printDbg("purchaseChannel() pin : " + pin);
            log.printDbg("purchaseChannel() isUpselling : " + isUpselling);

            if (CONSTANT.IS_OTS === true) {
                var selectIndex = selectMonthlyView.getSelectOptionIndex();

                log.printDbg("purchaseChannel() scid : " + selectProduct.details[selectProduct.detailIndex].scid);
                log.printDbg("purchaseChannel() code : " + selectProduct.details[selectProduct.detailIndex].code);
                log.printDbg("purchaseChannel() ctcode : " + selectProduct.details[selectProduct.detailIndex].contract[selectIndex].ctcode);
                LayerManager.startLoading({
                    preventKey: true
                });

                payment.OppvManager.requestOptionalVodUpselling(_optionalUpsellingOTS,
                    selectProduct.details[selectProduct.detailIndex].scid,
                    selectProduct.details[selectProduct.detailIndex].code, null,
                    selectProduct.details[selectProduct.detailIndex].contract[selectIndex].ctcode);

            } else {
                var now = new Date().getTime();
                now = Math.floor(now / 1000);
                log.printDbg("purchaseChannel() now : " + now);
                log.printDbg("purchaseChannel() OTV UPSELLING getPackageCode : " + _getPackageCode());
                payment.HDSManager.onlineChangeProdbPIN(pin,
                    _getPackageCode(), now,
                    _responseHDS);
                // var params = {
                //     product_name: selectUpsellingProductView.getSelectUpSellingPkgName()
                // };
                //
                // LayerManager.activateLayer({
                //     obj: {
                //         id: PAYMENT_CHANNEL_UPGRADE_PROCESSING_POPUP,
                //         type: Layer.TYPE.POPUP,
                //         priority: Layer.PRIORITY.POPUP,
                //         linkage: true,
                //         params: {
                //             data: params
                //         }
                //     },
                //     moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                //     visible: true
                // });
            }
        };

        this.closePurchasePopup = function() {
            log.printDbg("closePurchasePopup()");
            if (callbackFunc !== null) {
                callbackFunc(false);
            }

            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_POPUP
            });

        }

        function _getPackageCode() {
            var pkgCode = null;
            log.printDbg("_getPackageCode()");
            if (selectProduct !== undefined && selectProduct !== null &&
                selectProduct.pkgCode !== undefined && selectProduct.pkgCode !== null) {
                pkgCode = selectProduct.pkgCode;

            }

            log.printDbg("_getPackageCode() return pkgCode : " + pkgCode);
            return pkgCode;
        }

        function _responseHDS(success, xhr, error_message) {
            LayerManager.stopLoading();

            log.printDbg("_responseHDS() success : " + success);
            //
            // LayerManager.deactivateLayer({
            //     id: PAYMENT_CHANNEL_UPGRADE_PROCESSING_POPUP
            // });

            try {
                if (success) {
                    var x = xhr.responseXML.getElementsByTagName("string");
                    var startIndex = x[0].textContent.indexOf("returnVal^True");

                    if (startIndex >= 0) {
                        if (callbackFunc !== null) {
                            callbackFunc(true, isUpselling);
                        }
                    } else {
                        var messageIndex = x[0].textContent.indexOf("returnDESC^") + 11;
                        var message = x[0].textContent.substring(messageIndex);
                        //message = "이 전용 메뉴의 콘텐츠는 15요금제 이상\n고객님께 무료로 제공됩니다. \n요금제를 업그레이드하셔서\n더 많은 채널과 다양한 유료 VOD를 즐기세요.\n문의 : 국번 없이 100번";
                        //message = "고객님은 다회선 가입 고객으로\n요금제 변경 후 업그레이드가 가능합니다.\n다른 단말에서도 처리가 안되실 경우\n고객센터로 문의 부탁드립니다.\n문의 : 국번 없이 100번";
                        // //  message = "현재 시스템 점검 중으로 상품변경이 되지 않습니다.\n잠시 후 다시 이용해 주세요.";

                        message = message.replace(/\\n/g, '\n');
                        if (message !== null) {
                            var hdsMessageArray = message.split("\n");
                            _showErrorPopup(hdsMessageArray, false);
                        } else {
                            _showErrorPopup(undefined, false);
                        }
                    }
                } else {
                    if (error_message !== undefined && error_message !== null) {
                        if (error_message.error === true) {
                            _showErrorPopup(["서비스가 일시적으로 원활하지 않습니다", "네트워크 상태를 확인해주세요" , "동일 현상 반복 시 고객센터로 문의해" , "주시기 바랍니다" , "" , "문의) 국번없이 100번 (VODE-00016)"] , true);
                        }
                    }
                }
            } catch (error) {
                log.printErr("catching error:" + error);

                // R6 CEMS
                // 서번 단절 및 예외사항인 경우이기 때문에 CEMS 리포트
                OipfAdadapter.extensionAdapter.notifySNMPError("VODE-00013");

                _showErrorPopup(undefined , false);
            }
        }

        function _optionalUpsellingOTS(success, data) {
            log.printDbg("_optionalUpsellingOTS success:" + success + " data:" + JSON.stringify(data));
            LayerManager.stopLoading();

            if (success === true && data !== undefined && data !== null && data.rsltCd !== undefined && data.rsltCd !== null && data.rsltCd == '00') {
                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_PURCHASE_POPUP
                });

                var params = {
                    callback: _callbackPurchaseSuccessPopup
                };

                LayerManager.activateLayer({
                    obj: {
                        id: PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP,
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            data: params
                        }
                    },
                    moduleId: Module.ID.MODULE_VOD_PAYMENT,
                    visible: true
                });

            } else {
                _showErrorPopup(undefined , false);
            }
        }

        this.showUpsellingProductDetail = function(productName, ImgUrl) {
            var params = {
                product_name: productName,
                img: ImgUrl,
                callback: _callbackUpsellingProductDetail
            };

            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_UPSELLING_PRODUCT_DETAIL_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data: params
                    }
                },
                moduleId: Module.ID.MODULE_VOD_PAYMENT,
                visible: true
            });
        };

        function _createView(parent_div, _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame vod"});

            var topPos = 0;
            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: CONSTANT.RESOLUTION.WIDTH, height: CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });

            topPos = 220;
            var unfocusStep1Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step1_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 6 + 93 + 2
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep1Div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 6 + 93, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep1Div
            });

            unfocusStep1.root_div = unfocusStep1Div;

            focusStep1Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_step1_div",
                    css: {
                        position: "absolute",
                        // left: 359, top: topPos, width: 1200, height: 378,
                        left: 359, top: topPos, width: 1200, height: 433,
                        display: "none"
                    }
                },
                parent: popup_container_div
            });

            selectUpsellingProductView = new payment.ChannelPurchase.view.SelectUpsellingProductView(_this);
            selectUpsellingProductView.setParentDiv(focusStep1Div);
            selectUpsellingProductView.create();

            topPos += (6 + 93 + 2);
            // topPos -= 2;

            var unfocusStep2Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step2_div",
                    // 상품 업그레이드 관련 문구 추가로 인한 클래스 추가
                    class: "VODUpSale",
                    css: {
                        position: "absolute",
                        // left: 359, top: topPos, width: 1200, height: 2 + 93 + 2
                        left: 359, top: topPos, width: 1200, height: 2 + 93
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep2Div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 2 + 93, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep2Div
            });

            unfocusStep2.root_div = unfocusStep2Div;

            focusStep2Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_step2_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos - 6, width: 1200, height: 416,
                        display: "none"
                    }
                },
                parent: popup_container_div
            });

            termsAgreeView = new payment.ChannelPurchase.view.TermsAgreeView(_this);
            termsAgreeView.setParentDiv(focusStep2Div);
            termsAgreeView.setStepNumber("2");
            termsAgreeView.create();

            topPos += (2 + 93 + 2);
            topPos -= 2;

            var unfocusStep3Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step3_div",
                    // 상품 업그레이드 관련 문구 추가로 인한 클래스 추가
                    class: "VODUpSale",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 2 + 93 + 2
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            var top_line = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep3Div
            });

            var bottom_line = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 2 + 93, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep3Div
            });

            unfocusStep3.root_div = unfocusStep3Div;
            unfocusStep3.top_line = top_line;
            unfocusStep3.bottom_line = bottom_line;

            focusStep3Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_step3_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos - 6, width: 1200, height: 289,
                        display: "none"
                    }
                },
                parent: popup_container_div
            });

            mobilePhoneAuthView = new payment.ChannelPurchase.view.MobilePhoneAuthView(_this);
            mobilePhoneAuthView.setParentDiv(focusStep3Div);
            mobilePhoneAuthView.create();

            topPos += (2 + 93 + 2);
            topPos -= 2;

            var unfocusStep4Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step4_div",
                    // 상품 업그레이드 관련 문구 추가로 인한 클래스 추가
                    class: "VODUpSale",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 2 + 93 + 6
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep4Div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 2 + 93, width: 1200, height: 6,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep4Div
            });

            unfocusStep4.root_div = unfocusStep4Div;

            focusStep4Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_step4_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos - 6, width: 1200, height: 280,
                        display: "none"
                    }
                },
                parent: popup_container_div
            });

            pinInputView = new payment.ChannelPurchase.view.PinInputView(_this);
            pinInputView.setParentDiv(focusStep4Div);
            pinInputView.create();

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 164, width: CONSTANT.RESOLUTION.WIDTH, height: 34,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33, "text-align": "center",
                        display: "", "letter-spacing": -1.65
                    }
                },
                text: purchaseTitle,
                parent: popup_container_div
            });

            _createUnFocusStep1DivElement();
            _createUnFocusStep2DivElement();
            _createUnFocusStep3DivElement();
            _createUnFocusStep4DivElement();
        }

        function _createUnFocusStep1DivElement() {

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 5, top: 29, width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45, "text-align": "left"
                    }
                },
                text: "1",
                parent: unfocusStep1.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 61, top: 38, width: 120, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "left", "letter-spacing": -1.35
                    }
                },
                text: "",
                parent: unfocusStep1.root_div
            });

            var commentDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 178, top: 32, height: 40,
                        "background-color": "rgba(70, 70, 70, 0.8)",
                        display: "none"
                    }
                },
                parent: unfocusStep1.root_div
            });

            var textComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_comment",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0, top: 7, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "center", "letter-spacing": -1.35
                    }
                },
                text: "",
                parent: commentDiv
            });

            var textUpsellingComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_upselling_comment",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 600, top: 37, width: 600, height: 28,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 26, "text-align": "right", "letter-spacing": -1.3
                    }
                },
                text: "3년 약정 기준 (부가세 포함)",
                parent: unfocusStep1.root_div
            });

            unfocusStep1.step_title = textStepTitle;
            unfocusStep1.step_comment_div = commentDiv;
            unfocusStep1.step_comment_text = textComment;
            unfocusStep1.step_comment1_text = textUpsellingComment;
        }

        function _createUnFocusStep2DivElement() {
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 5, top: 22, width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45, "text-align": "left"
                    }
                },
                text: "2",
                parent: unfocusStep2.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 61, top: 33, width: 120, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "left", "letter-spacing": -1.35
                    }
                },
                text: "",
                parent: unfocusStep2.root_div
            });

            var commentDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 178, top: 26, height: 40,
                        "background-color": "rgba(70, 70, 70, 0.8)",
                        display: "none"
                    }
                },
                parent: unfocusStep2.root_div
            });

            var textComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_comment",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0, top: 7, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "center", "letter-spacing": -1.35
                    }
                },
                text: "",
                parent: commentDiv
            });

            unfocusStep2.step_title = textStepTitle;
            unfocusStep2.step_comment_div = commentDiv;
            unfocusStep2.step_comment_text = textComment;
        }

        function _createUnFocusStep3DivElement() {
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 5, top: 22, width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45, "text-align": "left"
                    }
                },
                text: "3",
                parent: unfocusStep3.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 61, top: 33, width: 145, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "left", "letter-spacing": -1.35
                    }
                },
                text: "",
                parent: unfocusStep3.root_div
            });

            var commentDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 178, top: 26, height: 40,
                        "background-color": "rgba(70, 70, 70, 0.8)",
                        display: "none"
                    }
                },
                parent: unfocusStep3.root_div
            });

            var textComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_comment",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0, top: 7, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "center", "letter-spacing": -1.35
                    }
                },
                text: "",
                parent: commentDiv
            });

            unfocusStep3.step_title = textStepTitle;
            unfocusStep3.step_comment_div = commentDiv;
            unfocusStep3.step_comment_text = textComment;
        }

        function _createUnFocusStep4DivElement() {
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 5, top: 22, width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45, "text-align": "left"
                    }
                },
                text: "4",
                parent: unfocusStep4.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 61, top: 33, width: 120, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27, "text-align": "left", "letter-spacing": -1.35
                    }
                },
                text: "구매인증",
                parent: unfocusStep4.root_div
            });

            unfocusStep4.step_title = textStepTitle;

            var tempdiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 901, top: 18, width: 300, height: 62, opacity: 0.3
                    }
                },
                parent: unfocusStep4.root_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "password_box_bg_img",
                    src: channelPurchasePopupResourcePath + "pop_pw_box_w300.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 300,
                        height: 62
                    }
                },
                parent: tempdiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0, top: 19, width: 300, height: 42,
                        color: "rgba(255, 255, 255, 1)", "font-size": 42, "text-align": "center", "letter-spacing": -2.1
                    }
                },
                text: "* * * *",
                parent: tempdiv
            });
        }

        /**
         * 2017.11.17 sw.nam
         * 가입 팝업 에서 나가기 키 입력 시 hide가 호출 되지 않아 구매 인증팝업 PIN 입력 창에 포커스 된 상태에서
         * 음성 핀 UI 를 띄운 후에 나가기 키로 화면을 벗어난 경우 speechRecognizer 의  stop이 호출되지 않는 이슈가 발생 (WEBIIIHOME-3516)
         * 구매 인증 팝업에서 나가기 키를 누르는 경우 pinInputView 의 hide 가 호출되도록 수정
         * @param options
         */
        function _hideView() {
            log.printDbg("_hideView()");
            pinInputView.hide();
        }

        function _showView() {
            log.printDbg("_showView()");

            if (initshow === false) {
                initshow = true;
                unfocusStep1.step_title.text("상품 선택");
                unfocusStep1.step_title.css({width: 122});
                unfocusStep1.step_comment_div.css("display", "none");

                unfocusStep2.step_title.text("가입 안내");
                unfocusStep2.step_comment_div.css("display", "none");

                unfocusStep3.step_title.text("휴대폰 인증");
                unfocusStep3.step_comment_div.css("display", "none");

                unfocusStep4.step_title.text("구매인증");
                _showFocusStep();
            }
        }

        function _showFocusStep() {

            focusStep1Div.css("display", "none");
            focusStep2Div.css("display", "none");
            focusStep3Div.css("display", "none");
            focusStep4Div.css("display", "none");

            /**
             * 음성 핀 초기화
             */
            pinInputView.hide();

            if (focusStep === 1) {
                unfocusStep1.root_div.css({top: unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display", "none");

                focusStep1Div.css("display", "");

                var selectIndex = selectUpsellingProductView.getSelectProductIndex();
                //
                // if(selectIndex <0) {
                //     selectIndex = 0;
                // }

                selectUpsellingProductView.setPackageList(pkgList, selectIndex);
                selectUpsellingProductView.show();

                var tempY = unfocusTopPosition[0];
                tempY += 378;
                tempY -= 2;
                unfocusStep2.root_div.css({top: tempY});
                unfocusStep2.root_div.css("display", "");

                tempY += (2 + 93 + 2);
                tempY -= 2;
                unfocusStep3.root_div.css({top: tempY});
                unfocusStep3.root_div.css("display", "");

                tempY += (2 + 93 + 2);
                tempY -= 2;
                unfocusStep4.root_div.css({top: tempY});
                unfocusStep4.root_div.css("display", "");

            } else if (focusStep === 2) {
                unfocusStep1.root_div.css({top: unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display", "");

                unfocusStep2.root_div.css({top: unfocusTopPosition[1]});
                unfocusStep2.root_div.css("display", "none");

                focusStep2Div.css("display", "");

                descStr = null;

                if (selectProduct !== undefined && selectProduct !== null && selectProduct.upPkgInfoW3 !== undefined && selectProduct.upPkgInfoW3 !== null) {
                    descStr = selectProduct.upPkgInfoW3.trim();
                }
                termsAgreeView.setTermsDesc(descStr);

                termsAgreeView.show();

                for (var i = 0; i < unfocusTopPosition.length; i++) {
                    log.printDbg("unfocusTopPosition[" + i + "] : " + unfocusTopPosition[i]);
                }

                var tempY = unfocusTopPosition[focusStep - 1];
                tempY += 410;
                unfocusStep3.root_div.css({top: tempY});
                log.printDbg("unfocusStep3 TOP : " + tempY);
                unfocusStep3.root_div.css("display", "");

                tempY += (2 + 93);
                unfocusStep4.root_div.css({top: tempY});
                log.printDbg("unfocusStep4 TOP : " + tempY);
                unfocusStep4.root_div.css("display", "");
            } else if (focusStep === 3) {
                unfocusStep1.root_div.css({top: unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display", "");

                unfocusStep2.root_div.css({top: unfocusTopPosition[1]});
                unfocusStep2.root_div.css("display", "");

                unfocusStep3.root_div.css({top: unfocusTopPosition[2]});
                unfocusStep3.root_div.css("display", "none");

                focusStep3Div.css("display", "");

                mobilePhoneAuthView.show();

                var tempY = unfocusTopPosition[focusStep - 1];
                if (mobilePhoneAuthView.getIsMobilePhoneAuth()) {
                    tempY += 147;
                } else {
                    tempY += 289;
                }
                tempY -= 6;
                unfocusStep4.root_div.css({top: tempY});
                unfocusStep4.root_div.css("display", "");
            } else if (focusStep === 4) {
                unfocusStep1.root_div.css({top: unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display", "");

                unfocusStep2.root_div.css({top: unfocusTopPosition[1]});
                unfocusStep2.root_div.css("display", "");

                unfocusStep3.root_div.css({top: unfocusTopPosition[2]});
                unfocusStep3.root_div.css("display", "");

                unfocusStep4.root_div.css({top: unfocusTopPosition[3]});
                unfocusStep4.root_div.css("display", "none");

                focusStep4Div.css("display", "");

                var price = selectProduct.vatUpPrice;

                pinInputView.setBuyPrice(price);
                pinInputView.show();
            }
        }

        function _callbackTermsAgreeDetail(ret) {
            if (ret === true) {
                termsAgreeView.setIsTermsAgree(true);
            }
            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_TERMS_AGREE_DETAIL_POPUP
            });
        }

        function _showErrorPopup(message, isShowSaid) {
            var isCalled = false;

            var popupData = {
                arrMessage: [],
                arrButton: [{id: "ok", name: "확인"}],
                isShowSaid : isShowSaid,
                cbAction: function(buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                        });

                        if (callbackFunc !== null) {
                            callbackFunc(false);
                        }

                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_POPUP
                        });

                    }
                }
            };

            popupData.arrMessage.push({
                type: "Title",
                message: ["알림"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_42",
                message: message === undefined ? ["가입한 상품에서는 구매할 수 없습니다"] : message,
                cssObj: {}
            });

            if (message === undefined) {
                popupData.arrMessage.push({
                    type: "MSG_30_DARK",
                    message: ["문의:국번 없이 100번"],
                    cssObj: {}
                });
            }

            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data: popupData
                    }
                },
                moduleId: Module.ID.MODULE_VOD_PAYMENT,
                visible: true
            });

        }

        /**
         * OTV 부가상품 가입 완료 후 pkgCode update 처리 함.
         */
        function _callbackUpdatePkgInfo() {
            var params = {
                callback : _callbackPurchaseSuccessPopup
            };
            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : params
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });
        }


        function _callbackPurchaseSuccessPopup(ret) {
            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP
            });
            if (callbackFunc !== null) {
                callbackFunc(true, isUpselling);
            }
        }

        function _callbackUpsellingProductDetail() {

            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_UPSELLING_PRODUCT_DETAIL_POPUP
            });
        }

    };

    channelPurchasePopup.prototype = new Layer();
    channelPurchasePopup.prototype.constructor = channelPurchasePopup;
    channelPurchasePopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    channelPurchasePopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    channelPurchasePopup.prototype.hide = function(options) {
        Layer.prototype.hide.call(this);
        this.hideView(options);
    };

    channelPurchasePopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_POPUP] = channelPurchasePopup;
})();