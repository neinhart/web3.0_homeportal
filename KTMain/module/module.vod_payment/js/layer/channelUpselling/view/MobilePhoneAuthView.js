/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MobilePhoneAuthView</code>
 *
 * 휴대폰 인증 STEP#3 View
 * @author jjh1117
 * @since 2016-12-21
 */

"use strict";

(function() {
    payment.ChannelPurchase.view.MobilePhoneAuthView = function(parent) {
        var util = UTIL;

        var parent = parent;
        var div = null;

        /**
         * 휴대폰 인증 여부
         * @type {boolean}
         */
        var isMobilePhoneAuth = false;

        /**
         * 휴대폰 인증 전 STEP#3 화면
         */
        var beforeMobilePhoneAuthDiv = null;
        /**
         * 휴대폰 인증 후 STEP#3 화면
         */
        var afterMobilePhoneAutheDiv = null;


        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        /**
         * 휴대폰 인증 여부 전달
         * @returns {boolean}
         */
        this.getIsMobilePhoneAuth = function() {
            log.printDbg("getIsMobilePhoneAuth() return isMobilePhoneAuth : " + isMobilePhoneAuth);
            return isMobilePhoneAuth;
        };

        this.getDiv = function () {
            return div;
        };

        this.create = function () {
            log.printDbg("create()");
            _createElement();
            beforeMobilePhoneAuthDiv.css("display" , "none");
            afterMobilePhoneAutheDiv.css("display" , "none");
        };

        this.show = function (options) {
            log.printDbg("show()");
            if(isMobilePhoneAuth === true) {
                beforeMobilePhoneAuthDiv.css("display" , "none");
                afterMobilePhoneAutheDiv.css("display" , "");
            }else {
                afterMobilePhoneAutheDiv.css("display" , "none");
                beforeMobilePhoneAuthDiv.css("display" , "");
            }
        };

        this.hide = function (options) {
            log.printDbg("hide()");

            div.css("display", "none");
            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        /**
         * 리모컨 키 처리
         * @param keyCode
         * @returns {boolean}
         */
        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KEY_CODE.UP:
                    parent.goPrevStep();
                    consumed = true;
                    break;
                case KEY_CODE.DOWN:
                    if(isMobilePhoneAuth=== true) {
                        parent.goNextStep();
                    }
                    consumed = true;
                    break;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KEY_CODE.OK:
                    case KEY_CODE.ENTER:
                if(isMobilePhoneAuth=== true) {
                        parent.goNextStep();
                    }else {
                        goMobilePhoneAuth();
                    }
                    consumed = true;
                    break;
            }
            return consumed;
        };


        function _createElement() {
            log.printDbg("_createElement()");
            _createBeforeMobilePhoneAuthElement();
            _createAfterMobilePhoneAuthElement();
        }

        /**
         * 휴대폰 인증 전 STEP#3 화면
         * @private
         */
        function _createBeforeMobilePhoneAuthElement() {
            log.printDbg("_createBeforeMobilePhoneAuthElement()");

            beforeMobilePhoneAuthDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 289
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: beforeMobilePhoneAuthDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 289-6, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: beforeMobilePhoneAuthDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 1 , top: 25 , width: 40, height: 59,
                        color: "rgba(255, 96, 108, 0.5)", "font-size": 57 , "text-align": "left"
                    }
                },
                text: "3",
                parent: beforeMobilePhoneAuthDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mobile_auth_comment1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 60 , top: 39 , width: 550, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "휴대폰 인증",
                parent: beforeMobilePhoneAuthDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mobile_auth_comment2",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 101 , width: 1200, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "상품 업그레이드를 위해 가입자 명의의 휴대폰 인증이 필요합니다",
                parent: beforeMobilePhoneAuthDiv
            });


            var tempdiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 477, top: 155, width: 246, height: 62,
                        "background-color": "rgba(210, 51, 47, 1)"
                    }
                },
                parent: beforeMobilePhoneAuthDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mobile_auth_comment2",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 17 , width: 246, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "휴대폰 인증하기",
                parent: tempdiv
            });
        }

        /**
         * 휴대폰 인증 후 STEP#3 화면
         * @private
         */
        function _createAfterMobilePhoneAuthElement() {
            afterMobilePhoneAutheDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 147
                    }
                },
                parent: div
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 1, width: 1200, height: 6,
                        "background-color": "rgba(128, 128 , 128, 1)"
                    }
                },
                parent: afterMobilePhoneAutheDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 147-6, width: 1200, height: 6,
                        "background-color": "rgba(128, 128 , 128, 1)"
                    }
                },
                parent: afterMobilePhoneAutheDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 1 , top: 25 , width: 40, height: 59,
                        color: "rgba(255, 96, 108, 0.5)", "font-size": 57 , "text-align": "left"
                    }
                },
                text: "3",
                parent: afterMobilePhoneAutheDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mobile_auth_comment1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 60 , top: 39 , width: 550, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "휴대폰 인증",
                parent: afterMobilePhoneAutheDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mobile_auth_comment2",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 61 , top: 90 , width: 600, height: 24,
                        color: "rgba(255, 255, 255, 0.6)", "font-size": 22 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "휴대폰 인증이 완료 되었습니다",
                parent: afterMobilePhoneAutheDiv
            });
        }

        function goMobilePhoneAuth() {
            log.printDbg("goMobilePhoneAuth()");
            LayerManager.startLoading({
                preventKey: true
            });

            var now = new Date().getTime();
            now = Math.floor(now/1000);
            log.printDbg("goMobilePhoneAuth() now : " + now);

            AuthManager.reqCertUserInfo(now , _callbackHdsCertUserInfo);
        }

        function _callbackMobilePhoneAuth(ret) {
            if(ret === true) {
                isMobilePhoneAuth = true;
                parent.goNextStep();
            }
            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_MOBILE_PHONE_AUTH_POPUP
            });

        }

        function _callbackHdsCertUserInfo(result, xhr, error_message) {
            LayerManager.stopLoading();

            if (result) {
                // [jh.lee] reqCertUserInfo 요청이 정상이고 받아온 데이터가 존재하는 경우
                var code = xhr.responseXML.getElementsByTagName("RCode");

                log.printDbg("CertUserInfo code : " + code);

                if (code[0].textContent == "SHD0000100") {
                    // [jh.lee] 성공
                    var gender = xhr.responseXML.getElementsByTagName("Gender")[0].textContent;
                    var nation = xhr.responseXML.getElementsByTagName("Nation")[0].textContent;
                    var uname = xhr.responseXML.getElementsByTagName("Uname")[0].textContent;
                    // [jh.lee] 비밀번호 초기화 팝업을 띄우게 되면 전화번호와 생년월일의 미리보기 문구를 아래형식으로 보여줘야함.
                    var data_param = {"telNo" : "", "birth" : "", "gender":gender, "nation":nation, "uname":uname , callback:_callbackMobilePhoneAuth};
                    log.printDbg("Success param : " + log.stringify(data_param));


                    LayerManager.activateLayer({
                        obj: {
                            id: PAYMENT_CHANNEL_PURCHASE_MOBILE_PHONE_AUTH_POPUP,
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                data : data_param
                            }
                        },
                        moduleId: Module.ID.MODULE_VOD_PAYMENT,
                        visible: true
                    });

                } else {
                    /**
                     * HDS 연동은 성공 하였으나 Response 정보 분석 결과 오류로 전달 된 경우
                     */
                    var message = xhr.responseXML.getElementsByTagName("RMsg")[0].textContent;
                    showErrorPopup(message , false);
                }
            } else {
                if(error_message !== undefined && error_message !== null) {
                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        showErrorPopup(error_message , true);
                    }else {
                        if(error_message.error === true) {
                            showErrorPopup(["가입자 인증 오류입니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다"] , true);
                        }
                    }
                }
            }
        }


        function showErrorPopup(message , isShowSaid) {
            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: "Title",
                    message: ["인증오류"],
                    cssObj: {}
                }],
                arrButton: [{id: "ok", name: "확인"}],
                isShowSaid : isShowSaid,
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;
                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                        });
                    }
                }
            };

            popupData.arrMessage.push({
                type: "MSG_45",
                message: message instanceof Array ? message : [message],
                cssObj: {}
            });

            // popupData.arrMessage.push({
            //     type: "MSG_38",
            //     message: [code],
            //     cssObj: {}
            // });


            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : popupData
                    }
                },
                moduleId: Module.ID.MODULE_VOD_PAYMENT,
                visible: true
            });
        }
    }
})();