/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>PinInputView</code>
 *
 * 구매 인증 STEP#4 View
 * @author jjh1117
 * @since 2016-12-16
 */

"use strict";

(function() {
    payment.ChannelPurchase.view.PinInputView = function(parent) {
        var util = UTIL;

        var FOCUS_INPUTBOX = 0;
        var FOCUS_OK = 1;
        var FOCUS_CANCEL = 2;

        var MAX_INPUT_LENGTH = 4;

        var parent = parent;
        var div = null;

        var focusIndex = FOCUS_INPUTBOX;

        /**
         * Input box 관련 Element
         */
        var passwordInputBoxDiv = null;
        var passwordInputBox = null;
        var textInputBox = null;

        /**
         * 확인 , 취소 버튼 Element
         */
        var btnOk = null;
        var btnCancel = null;

        /**
         * 결제 금액 문구 관련 Element
         * textPriceMonthUnit : "월"
         * textPrice : 금액
         * textPriceUnit : "원"
         */
        var textPriceMonthUnit = null;
        var textPrice = null;
        var textPriceUnit = null;

        var buyTitle = null;
        /**
         * 구매 금액 정보
         */
        var buyPrice = "";

        var channelPurchasePopupResourcePath = chResourcePath + "channelpurchasepopup/";
        var inputBoxUnFocusResourcePath = chResourcePath + "inputbox_unfocus/";

        var isStartSSpeechRecognizer = false;

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        /**
         * 선택한 상품에 금액 정보
         */
        this.setBuyPrice = function(price) {
            log.printDbg("setBuyPrice() price : " + price);
            buyPrice = price;
        };

        this.getDiv = function () {
            return div;
        };

        this.create = function () {
            log.printDbg("create()");
            _createElement();
        };

        this.show = function (options) {
            log.printDbg("show()");
            focusIndex = 0;

            var tempstr = util.setComma(Number(buyPrice));
            textPrice.text(tempstr);

            _focusUpdate();

                /**
                 * 비밀번호 입력 정보 초기화
                 */
                AuthManager.initPW();
                AuthManager.resetPWCheckCount();
                _updateInputBox("");

                buyTitle.text("구매인증 번호 4자리를 입력해 주세요");

                _startSpeechRecognizer();
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");


            _stopSpeechRecognizer();
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        /**
         * 리모컨 키 처리
         * @param keyCode
         * @returns {boolean}
         */
        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KEY_CODE.UP:
                    /**
                     * 이전 STEP으로 돌아감
                     */
                    parent.goPrevStep();
                    buyPrice = "";
                    consumed = true;
                    break;
                case KEY_CODE.DOWN:
                    /**
                     * 마지막 STEP이므로 처리 하지 않음
                     */
                    consumed = true;
                    break;
                case KEY_CODE.LEFT:
                    /**
                     * 포커스가 Input 인 경우 입력 글자 삭제
                     * 아니면 버튼 포커스 이동
                     */
                    if(focusIndex === FOCUS_INPUTBOX) {
                        _deleteNum();
                    }else {
                        focusIndex--;
                        _focusUpdate();
                    }
                    consumed = true;
                    break;
                case KEY_CODE.RIGHT:
                    /**
                     * 버튼 포커스 이동
                     */
                    focusIndex++;
                    if(focusIndex>FOCUS_CANCEL) {
                        focusIndex = FOCUS_CANCEL;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KEY_CODE.OK:
                case KEY_CODE.ENTER:
                    /**
                     * 포커스가 취소 이면 이전 STEP 이동
                     * 포커스가 확인 or Input 이면 구매 시작
                     */
                    if(focusIndex === FOCUS_CANCEL) {
                        parent.closePurchasePopup();
                    }else {
                        /**
                         * 구매핀 인증
                         */
                        _checkPW();

                    }
                    consumed = true;
                    break;
            }

            if(consumed === false) {
                var number = util.keyCodeToNumber(keyCode);
                if (number >= 0) {
                    if(focusIndex === FOCUS_INPUTBOX) {
                        _inputNum(number);
                    }
                    consumed = true;
                }
            }
            log.printDbg("controlKey() return consumed : " + consumed + " , focusIndex : " + focusIndex);
            return consumed;
        };

        function _createElement() {
            log.printDbg("_createElement()");

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 280-6, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 1 , top: 25 , width: 40, height: 59,
                        color: "rgba(255, 96, 108, 0.5)", "font-size": 57 , "text-align": "left"
                    }
                },
                text: "4",
                parent: div
            });

            buyTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "pin_input_comment1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 60 , top: 39 , width: 650, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "구매인증 번호 4자리를 입력해 주세요",
                parent: div
            });

            _createPurchasePriceElement();
            _createButtonElement();
            _createPasswordInputBoxElement();

            var commentText = "";
            if(parent.isProductUpselling() === true) {
                commentText = "※상품 변경 시 기존 약정은 그대로 승계되며\n실제 청구 금액은 고객의 가입 유형에 따라 다를 수 있습니다 (부가세 별도)";
            }else {
                commentText = "※부가세 포함된 가격입니다";
            }
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 197 , width: 1200, height: 55,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 22 , "text-align": "right",
                        "white-space":"pre-line", "line-height": 1.2,"letter-spacing":-1.1
                    }
                },
                text: commentText,
                parent: div
            });

        }

        /**
         *  결제 금액 표시 Element
         */
        function _createPurchasePriceElement() {
            log.printDbg("_createPurchasePriceElement()");


                var purchasePriceRootDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute", left:(418-360) , top: (633-500) , width:430, height: 60
                        }
                    },
                    parent: div
                });



                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "purchase_price_title",
                    class: "font_l",
                        css: {
                            float: "left", "margin-top": 12, "margin-left": 0 , color: "rgba(221, 175, 120, 0.7)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                        }
                    },
                    text: "결제 금액",
                    parent: purchasePriceRootDiv
                });

                textPriceMonthUnit = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_price_month_unit",
                    class: "font_l",
                        css: {
                            float: "left", "margin-top": 14, "margin-left": 8,
                            color: "rgba(255, 255, 255, 0.5)", "font-size": 26 , "text-align": "left","letter-spacing":-1.3
                        }
                    },
                    text: "월",
                    parent: purchasePriceRootDiv
                });

                textPrice = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_price",
                    class: "font_m",
                        css: {
                            float: "left", "margin-top": 0, "margin-left": 8,
                            color: "rgba(255, 255, 255, 1)", "font-size": 45 , "text-align": "right","letter-spacing":-2.25
                        }
                    },
                    text: "",
                    parent: purchasePriceRootDiv
                });


                textPriceUnit = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_price_unit",
                    class: "font_l",
                        css: {
                            float: "left", "margin-top": 14, "margin-left": 4,
                            color: "rgba(255, 255, 255, 0.5)", "font-size": 26 , "text-align": "left","letter-spacing":-1.3
                        }
                    },
                    text: "원",
                    parent: purchasePriceRootDiv
                });
        }

        /**
         *  확인 / 취소 버튼 Element
         */
        function _createButtonElement() {
            log.printDbg("_createButtonElement()");
            var btnDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_ok",
                    css: {
                        position: "absolute",
                        left: 772, top: 118, width:210, height: 62
                    }
                },
                parent: div
            });


            var defaultImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "btn_ok_default_img" ,
                    src: channelPurchasePopupResourcePath +  "pop_btn_w210.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 210,
                        height: 62
                    }
                },
                parent: btnDiv
            });

            var btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_ok_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:210, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv
            });

            var btnDefaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_ok_default_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 210, height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: btnDiv
            });

            var btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_ok_focus_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 210, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: btnDiv
            });

            var btn = {
                btn_root_div : btnDiv,
                btn_default_img : defaultImg,
                btn_focus_div : btnFocusDiv,
                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            } ;

            btnOk = btn;


            btnDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_cancel",
                    css: {
                        position: "absolute",
                        left: 990 , top: 118, width:210, height: 62
                    }
                },
                parent: div
            });


            defaultImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "btn_cancel_default_img" ,
                    src: channelPurchasePopupResourcePath + "pop_btn_w210.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 210,
                        height: 62
                    }
                },
                parent: btnDiv
            });

            btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_cancel_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:210, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv
            });

            btnDefaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_cancel_default_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 210, height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "취소",
                parent: btnDiv
            });

            btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_cancel_focus_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 210, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "취소",
                parent: btnDiv
            });

            btn = {
                btn_root_div : btnDiv,
                btn_default_img : defaultImg,
                btn_focus_div : btnFocusDiv,
                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            } ;

            btnCancel = btn;
        }

        /**
         *  Input Box 관련 Element
         */
        function _createPasswordInputBoxElement() {
            log.printDbg("_createPasswordInputBoxElement()");

            passwordInputBoxDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 824-360, top: 618-500, width: 300, height: 62
                    }
                },
                parent: div
            });

            passwordInputBox = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: channelPurchasePopupResourcePath + "pop_pw_box_w300.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 300,
                        height: 62
                    }
                },
                parent: passwordInputBoxDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 97, top: 20, width: 150, height: 44,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 42 ,"letter-spacing":-2.1
                    }
                },
                text: "* * * *",
                parent: passwordInputBoxDiv
            });

            textInputBox = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_input",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 97, top: 20, width: 150, height: 44,
                        color: "rgba(255, 255, 255, 1)", "font-size": 42 ,"letter-spacing":-2.1
                    }
                },
                text: "",
                parent: passwordInputBoxDiv
            });

        }

        /**
         * 포커스 초기화
         * @private
         */
        function _resetFocusAll() {
            log.printDbg("_resetFocusAll()");

            passwordInputBox.css({opacity:0.3});
            passwordInputBox.attr("src" , inputBoxUnFocusResourcePath+"pop_pw_box_w300.png");

            btnOk.btn_default_img.css("display" , "");
            btnOk.btn_focus_div.css("display" , "none");
            btnOk.btn_default_text.css("display" , "");
            btnOk.btn_focus_text.css("display" , "none");

            btnCancel.btn_default_img.css("display" , "");
            btnCancel.btn_focus_div.css("display" , "none");
            btnCancel.btn_default_text.css("display" , "");
            btnCancel.btn_focus_text.css("display" , "none");
        }

        /**
         * 현재 포커스 업데이트
         * @private
         */
        function _focusUpdate() {
            log.printDbg("_focusUpdate()");
            _resetFocusAll();

            if(focusIndex === FOCUS_INPUTBOX) {
                passwordInputBox.css({opacity:1});
                passwordInputBox.attr("src" , channelPurchasePopupResourcePath + "pop_pw_box_w300.png");
                _startSpeechRecognizer();
            }else if(focusIndex === FOCUS_OK) {
                _stopSpeechRecognizer();
                 passwordInputBox.css({opacity:0.3});
                 passwordInputBox.attr("src" , inputBoxUnFocusResourcePath+"pop_pw_box_w300.png");

                 textInputBox.css({opacity:1});

                btnOk.btn_default_img.css("display" , "none");
                btnOk.btn_focus_div.css("display" , "");
                btnOk.btn_default_text.css("display" , "none");
                btnOk.btn_focus_text.css("display" , "");
            }else if(focusIndex === FOCUS_CANCEL) {
                passwordInputBox.css({opacity:0.3});
                textInputBox.css({opacity:1});
                btnCancel.btn_default_img.css("display" , "none");
                btnCancel.btn_focus_div.css("display" , "");
                btnCancel.btn_default_text.css("display" , "none");
                btnCancel.btn_focus_text.css("display" , "");
            }
        }


        /**
         * Password Input Box GUI 업데이트 (초기화 및 입력된 Key 갯수만큼 화면에 * 표시함)
         */
        function _updateInputBox(password) {
            log.printDbg("updateInputBox()");
            if(password !== undefined && password !== null) {
                if(password.length >0) {
                    var inputText = "";
                    for(var i=0;i<password.length;i++) {
                        inputText+="*";
                        if(i !== (password.length-1)) {
                            inputText += " ";
                        }

                        textInputBox.text(inputText);
                    }
                }else {
                    textInputBox.text("");
                }
            }
        }

        /**
         * */
        function _inputNum(num , maxLength) {
            log.printDbg('_inputNum() num : ' + num);
            var authMaxLength = MAX_INPUT_LENGTH;
            if(maxLength !== undefined && maxLength !== null) {
                authMaxLength = maxLength;
            }
            
            if (AuthManager.inputPW(num, authMaxLength) === true) {
                _updateInputBox(AuthManager.getPW());

                focusIndex++;
                _focusUpdate();
            }
            else {
                _updateInputBox(AuthManager.getPW());
            }
        }


        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum() {
            log.printDbg('_deleteNum() ');
            AuthManager.deletePW();
            _updateInputBox(AuthManager.getPW());
        }

        function _checkPW() {
            var obj = {
                type : AuthManager.AUTH_TYPE.AUTH_BUY_PIN,
                callback : _checkPWListener,
                loading : {
                    type : 0,
                    lock : true,
                    callback : null,
                    on_off : false
                }
            };
            AuthManager.checkUserPW(obj);
        }


        function _checkPWListener(result) {
            log.printDbg('_checkPWListener() result: ' + result);

            if (result === AuthManager.RESPONSE.HDS_BUY_AUTH_SUCCESS) {
                AuthManager.resetPWCheckCount();
                _updateInputBox("");

                var pin = AuthManager.getPW();
                parent.purchaseChannel(pin);
            }
            else {
                // 비밀번호 불일치
                if (AuthManager.getPWCheckCount() < 3) {
                    buyTitle.text("인증번호가 일치하지 않습니다. 다시 입력해 주세요");
                }
                focusIndex = FOCUS_INPUTBOX;
                AuthManager.initPW();
                _focusUpdate();
                _updateInputBox(AuthManager.getPW());
            }
        }

        function _startSpeechRecognizer() {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() isStartSSpeechRecognizer : " + isStartSSpeechRecognizer);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                _stopSpeechRecognizer();
            }
            OipfAdadapter.speechRecognizerAdapter.start(true);
            OipfAdadapter.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            isStartSSpeechRecognizer = true;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() isStartSSpeechRecognizer :  " + isStartSSpeechRecognizer);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                OipfAdadapter.speechRecognizerAdapter.stop();
                OipfAdadapter.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);

                isStartSSpeechRecognizer = false;
            }
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
            // mode = KTW.oipf.Def.SR_MODE.PIN;
            // type = "recognized";
            // input = "000";
            if(mode === 2 && type === "recognized") {
                AuthManager.initPW();
                _updateInputBox("");

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>4) {
                        length = 4;
                    }

                    for(var i=0;i<length;i++) {
                        _inputNum(input[i] , length);
                    }
                }
            }
        }

    }
})();