/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function () {
    var payment_corner_popup = function PaymentCornerPopup(options) {
        Layer.call(this, options);
        var div;
        var optionList = [];
        var focus = 0;
        var callback;
        var callbackConsumed = false;
        var instance = this;

        this.init = function (cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup cornerPaymentPopup");

            var param = this.getParams();
            callback = param.callback;
            CornerManager.setCornerData(param.data);
            CornerManager.setCallback(callCallback);

            optionList.push(new payment.component.ProductOption(this));

            if (param.data.contractYn == "Y") {
                optionList.push(new payment.component.PeriodOption(this));
            }

            optionList.push(new payment.component.TermOption(this));
            optionList.push(new payment.component.CornerPaymentOption(this));

            constructDiv();
            setFocus(1);

            if (cbCreate) {
                cbCreate(true);
            }
        };

        function constructDiv() {
            div.append(_$("<div/>", {class: "popup_title"}).text("가입하기"));
            var contListArea = _$("<table/>", {class: "popup_contents buyProcessContents"});
            div.append(contListArea);

            contListArea.append(_$("<div/>", {class: 'sep_line'}));
            var setFocused = false;
            for (var option in optionList) {
                optionList[option].create(option - 0 + 1) ? ((setFocused ? 0 : initFocus(option)), setFocused = true) : 0;
                contListArea.append(optionList[option].getView());
                contListArea.append(_$("<div/>", {class: 'sep_line'}));
            }
            div.find(".option.focus").prev().addClass("white");
        }

        function initFocus(_focus) {
            if (!optionList[_focus]) return;
            optionList[_focus].focused();
            focus = _focus - 0;
        }

        function setFocus(_focus) {
            if (!optionList[_focus]) {
                return;
            }

            div.find(".white").removeClass("white");

            if (optionList[_focus].focused()) {
                optionList[focus].blurred();
                focus = _focus - 0;
            }
            div.find(".option.focus").prev().addClass("white");
        }

        function callCallback(res, data, forced) {
            if (res == -99) {
                callbackConsumed = true;
            } else if (!callbackConsumed || forced) {
                callbackConsumed = true;
                if (callback) {
                    callback(res, data, instance.id);
                }
            }
        }

        this.controlKey = function (keyCode) {
            if (optionList[focus].onKeyAction(keyCode)) {
                return true;
            }

            switch (keyCode) {
                case KEY_CODE.DOWN:
                    setFocus(focus + 1);
                    return true;
                case KEY_CODE.UP:
                    setFocus(focus - 1);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({
                        id: this.id,
                        remove: true
                    });
                    return true;
            }
        };

        this.show = function (options) {
            LayerManager.stopLoading();
            Layer.prototype.show.apply(this, arguments);

            for (var i in optionList) {
                if (optionList[i].hasOwnProperty("show")) {
                    optionList[i].show(options);
                }
            }
        };

        this.hide = function (options) {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(false);

            for (var i in optionList) {
                if (optionList[i].hasOwnProperty("hide")) {
                    optionList[i].hide(options);
                }
            }
        };

        this.onCompleteStep = function () {
            setFocus(focus + 1);
        }
    };

    payment_corner_popup.prototype = new Layer();
    payment_corner_popup.prototype.constructor = payment_corner_popup;

    //Override create function
    payment_corner_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_corner_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentCornerPopup"] = payment_corner_popup;
})();