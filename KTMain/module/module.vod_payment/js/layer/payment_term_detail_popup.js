/**
 * Created by skkwon on 2017-01-18.
 */

(function () {
    var term_detail_popup = function TermDetailPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus = 0;
        var maxPage, page = 0;
        var callback;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame payment gniPopup term_detail_popup"});

            div.append(_$("<div/>", {class:"title"}));
            div.append(_$("<div/>", {class:"contents_box"}).append("<div class='background'><div class='bg_left'/><div class='bg_mid'/><div class='bg_right'/></div><div class='textArea'><span/></div><div class='scroll_bar'><div class='bar'/></div><div class='measureArea'/>"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("동의")).append(_$("<btn/>").text("닫기")));

            setData(this.getParams().data);
            callback = this.getParams().callback;
            this.div.append(div);
            setFocus(focus);
            if(cbCreate) cbCreate(true);
        };

        function setData(_data) {
            div.find(".title").text("약관 상세");

            var desc = HTool.convertTextToHtml(_data);
            // var desc = _data.replace(/\r\n/g, '<br>');

            div.find(".contents_box .textArea span").html(desc);
            div.find(".contents_box .measureArea").html(desc);
        }

        function setPage(_page) {
            page = _page;
            div.find(".textArea span").css("-webkit-transform", "translateY(" + -414*page + "px)");
            div.find(".scroll_bar .bar").css("-webkit-transform", "translateY(" + 414/maxPage*page + "px)");
        }

        this.show = function () {
            Layer.prototype.show.apply(this, arguments);

            maxPage = Math.ceil(div.find(".contents_box .measureArea")[0].offsetHeight/414);
            div.find(".scroll_bar .bar").css("height", 414/maxPage + "px");
            div.find(".scroll_bar").toggle(maxPage>1);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    setPage(Math.max(0, page-1));
                    return true;
                case KEY_CODE.DOWN:
                    setPage(Math.min(maxPage-1, page+1));
                    return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    setFocus(focus^1);
                    return true;
                case KEY_CODE.ENTER:
                    if(focus==0) {
                        callback(true);
                    } LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };


        var setFocus = function(_focus) {
            div.find(".btn_area .focus").removeClass("focus");
            focus = _focus;
            div.find(".btn_area btn").eq(focus).addClass("focus");
        };
    };

    term_detail_popup.prototype = new Layer();
    term_detail_popup.constructor = term_detail_popup;

    term_detail_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    term_detail_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentTermDetailPopup"] = term_detail_popup;
})();