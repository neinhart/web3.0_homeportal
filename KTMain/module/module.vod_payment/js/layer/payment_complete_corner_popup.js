/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var payment_complete_corner_popup = function PaymentCompleteCornerPopup(options) {
        Layer.call(this, options);
        var div;
        var callback;

        this.init = function(cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup cornerPaymentCompletePopup");

            var param = this.getParams();
            callback = param.callback;
            constructDiv();
            if(cbCreate) cbCreate(true);
        };

        function constructDiv() {
            div.append(_$("<div/>", {class: "popup_title"}).text("가입"));
            div.append(_$("<div/>", {class: "text1"}).html("감사합니다<br>가입이 완료되었습니다"));
            div.append(_$("<div/>", {class: "text2"}).html("별도의 해지 신청이 없을 경우, 자동 연장됩니다"));
            div.append(_$("<div/>", {class: "btn_area"}).append(_$("<div/>", {class:"btn ok"}).text("확인")));
        }

        this.controlKey = function(keyCode) {
            switch(keyCode) {
                case KEY_CODE.EXIT:
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({
                        id: this.id,
                        remove: true
                    });
                    return true;
            }
        }

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
            callback(true);
        }
    };

    payment_complete_corner_popup.prototype = new Layer();
    payment_complete_corner_popup.prototype.constructor = payment_complete_corner_popup;

    //Override create function
    payment_complete_corner_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_complete_corner_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentCompleteCornerPopup"] = payment_complete_corner_popup;
})();