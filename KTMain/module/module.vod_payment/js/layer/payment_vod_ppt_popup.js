/**
 * Created by kh.kim2 on 2018-02-05.
 */
(function () {
    var payment_vod_ppt_popup = function PaymentVodPptPopup(options) {
        Layer.call(this, options);
        var div;
        var optionList = [];
        var focus = 0;
        var callback;
        var callbackConsumed = false;
        var instance = this;

        this.init = function (cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup vodPptPaymentPopup");

            var param = this.getParams();
            callback = param.callback;
            // param.data 는 재생할 콘텐츠 정보 + ppt 상품정보가 함께 있어야 함
            VodPptManager.setData(param.data);
            VodPptManager.setCallback(callCallback);

            optionList.push(new payment.component.VodPptProductOption(this));

            // ppt 기간이 여러개인 경우로 처리
            if (VodPptManager.getOptions().length > 1) {
                optionList.push(new payment.component.VodPptPeriodOption(this));
            }

            // biz payment
            optionList.push(new payment.component.BizPaymentOption(this, true));

            constructDiv();

            if (cbCreate) {
                cbCreate(true);
            }
        };

        function constructDiv() {
            div.append(_$("<div/>", {class: "popup_title"}).text("결제하기"));
            var contListArea = _$("<table/>", {class: "popup_contents buyProcessContents"});
            div.append(contListArea);

            contListArea.append(_$("<div/>", {class: 'sep_line'}));
            var setFocused = false;
            for (var option in optionList) {
                optionList[option].create(option - 0 + 1) ? ((setFocused ? 0 : initFocus(option)), setFocused = true) : 0;
                contListArea.append(optionList[option].getView());
                contListArea.append(_$("<div/>", {class: 'sep_line'}));
            }
            div.find(".option.focus").prev().addClass("white");
        }

        function initFocus(_focus) {
            window.log.printDbg("[PaymentVodPptPopup] initFocus = " + _focus);
            if (!optionList[_focus]) return;
            optionList[_focus].focused();
            focus = _focus - 0;
        }

        function setFocus(_focus) {
            window.log.printDbg("[PaymentVodPptPopup] setFocus = " + _focus);
            if (!optionList[_focus]) {
                return;
            }

            div.find(".white").removeClass("white");

            if (optionList[_focus].focused()) {
                optionList[focus].blurred();
                focus = _focus - 0;
            }
            div.find(".option.focus").prev().addClass("white");
        }

        function callCallback(res, data, forced) {
            if (!callbackConsumed || forced) {
                callbackConsumed = true;
                if (callback) {
                    callback(res, data, instance.id);
                    LayerManager.deactivateLayer({id: instance.id, onlyTarget: true});
                }
            }
        }

        this.controlKey = function (keyCode) {
            if (optionList[focus].onKeyAction(keyCode)) {
                return true;
            }

            switch (keyCode) {
                case KEY_CODE.DOWN:
                    setFocus(focus + 1);
                    return true;
                case KEY_CODE.UP:
                    setFocus(focus - 1);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({
                        id: this.id,
                        remove: true
                    });
                    return true;
            }
        };

        this.show = function (options) {
            LayerManager.stopLoading();
            Layer.prototype.show.apply(this, arguments);

            for (var i in optionList) {
                if (optionList[i].hasOwnProperty("show")) {
                    optionList[i].show(options);
                }
            }
        };

        this.hide = function (options) {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(false);

            for (var i in optionList) {
                if (optionList[i].hasOwnProperty("hide")) {
                    optionList[i].hide(options);
                }
            }
        };

        this.onCompleteStep = function () {
            setFocus(focus + 1);
        }
    };

    payment_vod_ppt_popup.prototype = new Layer();
    payment_vod_ppt_popup.prototype.constructor = payment_vod_ppt_popup;

    //Override create function
    payment_vod_ppt_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_vod_ppt_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentVodPptPopup"] = payment_vod_ppt_popup;
})();