/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var payment_complete_popup = function PaymentCompletePopup(options) {
        Layer.call(this, options);
        var div;
        var callback;
        var id = this.id;

        this.init = function(cbCreate) {
            div = this.div;
            this.div.addClass("gniPopup payment paymentPopup PaymentCompletePopup");

            var param = this.getParams();
            callback = param.callback;
            constructDiv();
            if(cbCreate) cbCreate(true);
        };

        function constructDiv() {
            div.append(_$("<div/>", {class: "text1"}).html("결제가 완료되었습니다"));
        }

        this.controlKey = function(keyCode) {
            switch(keyCode) {
                case KEY_CODE.EXIT:
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    return true;
            }
        };

        this.show = function () {
            Layer.prototype.show.apply(this, arguments);
            setTimeout(function () {
                LayerManager.deactivateLayer({ id: id });
                callback();
            }, 1000);
        }
    };

    payment_complete_popup.prototype = new Layer();
    payment_complete_popup.prototype.constructor = payment_complete_popup;

    //Override create function
    payment_complete_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    payment_complete_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentCompletePopup"] = payment_complete_popup;
})();