/**
 * Created by Taesang on 2017-01-20.
 */
(function() {
    var payment_toast_popup = function SubHomeToastPopup(options) {
        Layer.call(this, options);
        var _this = this, data, closeTimeout;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame payment payment_toast");
            this.div.append(_$("<span/>", {class: "toastText"}));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            data = _this.getParams();
            _this.div.find(".toastText").text(data.text);
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            } closeTimeout = setTimeout(function() {
                LayerManager.deactivateLayer({
                    id: _this.id,
                    remove: true
                });
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }, 3000);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            Layer.prototype.hide.call(this);
        };
    };

    payment_toast_popup.prototype = new Layer();
    payment_toast_popup.prototype.constructor = payment_toast_popup;

    payment_toast_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    payment_toast_popup.prototype.handleKeyEvent = function() {
        return false;
    };

    arrLayer["PaymentToastPopup"] = payment_toast_popup;
})();