/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var payment_askBuy_popup = function PaymentAskBuyPopup(options) {
        Layer.call(this, options);
        var div;
        var cornerData;
        var focusIdx;
        var callback;
        var callbackConsumed = false;
        var btnText = ["확인", "취소"];

        this.init = function(cbCreate) {
            this.div.addClass("gniPopup payment paymentPopup askBuyCorner");
            div = _$("<div/>", {class: "popup_contents"});
            div.append(_$("<span/>", {class: "title"}).text("가입"));
            div.append(_$("<span/>", {class: "contents_name"}));


            div.append(_$("<span/>", {class: "info_area"}));
            div.find(".info_area").append(_$("<span/>", {class: "price_area"}));
            div.find(".info_area").append(_$("<div/>", {class: "separator"}));
            div.find(".info_area").append(_$("<span/>", {class: "time_area"}));


            // div.append(_$("<span/>", {class: "price_area"}));


            div.append(_$("<span/>", {class: "description_area"}).html("월정액 가입이 필요한 상품입니다<br>가입하시겠습니까?"));
            div.append(_$("<div/>", {class: "button_area"}));
            for (var i = 0; i < 2; i++) {
                div.find(".button_area").append(_$("<div/>", {class: "btn"}));
                div.find(".button_area .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            div.find(".button_area .btn").eq(0).addClass("focus");
            focusIdx = 0;
            callback = this.getParams().callback;
            this.div.append(div);
            cbCreate(true);
        };

        this.show = function() {
            cornerData = this.getParams().data;

            div.find(".contents_name").text("'" + cornerData.productName + "'");
            div.find(".price_area").text("월 " + HTool.addComma(cornerData.vatProductPrice) + "원");
            div.find(".time_area").text("가입기간 내");
            Layer.prototype.show.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    focusIdx = (focusIdx != 0)? 0 : 1;
                    this.setBtnFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if(focusIdx == 0) callCallback(true, cornerData);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

        this.setBtnFocus = function() {
            div.find(".button_area .btn.focus").removeClass("focus");
            div.find(".button_area .btn").eq(focusIdx).addClass("focus");
        }


        this.hide = function() {
            Layer.prototype.hide.call(this);
            callCallback(false)
        };

        function callCallback(res, data) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                if(callback) callback(res, data)
            }
        }
    };



    payment_askBuy_popup.prototype = new Layer();
    payment_askBuy_popup.prototype.constructor = payment_askBuy_popup;

    payment_askBuy_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    payment_askBuy_popup.prototype.handleKeyEvent = function(key_code) {
        this.controlKey(key_code);
        return true;
    };

    arrLayer["PaymentAskBuyPopup"] = payment_askBuy_popup;
})();