/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.SquarePosterList = function(parent) {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var itemCnt;
    var isFocused = false;
    var sortGb;
    var buyType;
    var lineVisibleTimeout;
    var isAdultCate = false;
    var isChangedPage = false;
    var reqPathCd = "01";
    var catData;
    var loadedCallback = null;
    var isLoading = false;
    const LINE_AMOUNT = 4;

    this.create = function (cateId, _catData, _reqPathCd) {
        this.cateId = cateId;
        catData = HTool.transCateInfo(_catData);
        this.div = _$("<ul/>", {class: "contentsList square_poster_list"});
        for (var i = 0; i < LINE_AMOUNT * 5; i++)
            this.div.append(getPosterDiv(Math.floor(i / LINE_AMOUNT)));

        sortGb = _catData.sortGb;
        buyType = _catData.buyType;
        reqPathCd = _reqPathCd || reqPathCd;

        (function () {
            var tmp = _catData;
            do {
                if (tmp.catType == "Adult") {
                    isAdultCate = true;
                    break;
                }
                tmp = tmp.parent;
            } while (!!tmp && !!tmp.parent)
        })();

        catData = HTool.transCateInfo(_catData);
    };

    function getPosterDiv(lineNum) {
        var poster = _$("<li class='content poster_square line_" + lineNum + "'>" +
            "<div class='content'>" +
            "<img class='posterImg poster'>" +   // 포스터 이미지
            "<div class='contentsData'>" +
            "<div class='posterTitle'><span/></div>" + // 제목
            "<span class='posterDetail'>" +
            "<span class='isfree'></span>" + //유.무료
            "<img class='isfreeIcon'>" + //유.무료
            "<img class='age'>" +    //연령
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>" +
            "<div class='chart_area'/>" +
            "<div class='over_dim'/>" +
            "</div>" +
            "</li>");

        poster.find(".posterDetail").prepend(stars.getView(stars.TYPE.RED_20));
        return poster;
    }

    function addChartOrder(div, data, idx) {
        div.empty();
        if (data.chartOrder) {
            div.append(_$("<div/>", {class: "chart_icon"}).text((idx+1)));
            div.append(_$("<div/>", {class: "chart_change_area"}));
            var chart_area = div.find(".chart_change_area");
            switch (data.change) {
                case "N" :
                    chart_area.append(_$("<div/>", {class: "chart_change_icon new"}));
                    break;
                case "0" :
                    chart_area.append(_$("<div/>", {class: "chart_change_icon still"}));
                    break;
                default :
                    var sign = data.change.substr(0, 1);
                    var tmpClass = "";
                    if (sign == "-") tmpClass = "minus"; else if (sign == "+") tmpClass = "plus"; else tmpClass = "still";
                    chart_area.append(_$("<div/>", {class: "chart_change_icon " + tmpClass}).text(data.change.substr(1, data.change.length - 1)));
                    break;
            }
        }
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        if (!data.chartOrder) switch (data.newHot) {
            case "N":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"}));
                break;
            case "U":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"}));
                break;
            case "X":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"}));
                break;
            case "B":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"}));
                break;
            case "R":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"}));
                break;
        }
        if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        }
        if (data.resolCd && data.resolCd.indexOf("UHD") >= 0) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        }
        if (data.isDvdYn == "Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        }
        if (data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        }
        if (!isAdultCate && UTIL.isLimitAge(data.prInfo)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked_music.png"}));
        }
    }

    this.setData = function (_dataList, page, _sortGb, _itemCnt, forced) {
        var contList = this.div.find("li");
        if (_dataList) {
            dataList = _dataList;
            itemCnt = _itemCnt;
            totalPage = Math.ceil(itemCnt / (LINE_AMOUNT * 2));
            this.indicator.setSize(totalPage, page);
        } else if (!forced && page == dpPage) return;

        dpPage = page;
        if (totalPage > 1) {
            //top Page
            tmp = page;
            for (var i = 0; i < LINE_AMOUNT * 2; i++) setItem(contList.eq(i), tmp * LINE_AMOUNT * 2 + i);
            //2rd Page
            var tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0; i < LINE_AMOUNT * 2; i++) setItem(contList.eq(i + LINE_AMOUNT * 2), tmp * LINE_AMOUNT * 2 + i);
            //bottom Page
            var tmp = HTool.getIndex(page, 2, totalPage);
            for (var i = 0; i < LINE_AMOUNT; i++) setItem(contList.eq(i + LINE_AMOUNT * 4), tmp * LINE_AMOUNT * 2 + i);
            this.div.removeClass("singlePage");
        } else {
            for (var i = 0; i < LINE_AMOUNT * 5; i++) setItem(contList.eq(i), i);
            this.div.addClass("singlePage");
        }
        if (isFocused && !this.indicator.isFocused()) this.setFocus(focus);
    };

    this.updateData = function (_dataList) {
        dpPage = -1;
        dataList = _dataList;
        // this.setFocus(focus, true);
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if (data) {
            div.removeClass("hidden_cont");
            div.attr("id", getDataListId(idx));
            div.find(".posterImg.poster").attr("src", "");
            div.find(".posterImg.poster")[0].offsetWidth;
            div.find(".posterImg.poster").attr({
                src: data.squareImgUrl ? (data.squareImgUrl + "?w=250&h=250&quality=90"):"",
                onerror: "this.src='" + modulePath + "resource/image/default_music2.png'"
            });
            div.find(".posterTitle span").text(data.itemName || data.contsName);
            if (HTool.isTrue(data.wonYn)) {
                div.find("img.isfreeIcon").show();
                div.find("span.isfree").hide();
            } else if (HTool.isFalse(data.wonYn)) {
                div.find("span.isfree").show();
                div.find("span.isfree").text("무료");
                div.find("img.isfreeIcon").hide();
            } else {
                div.find("span.isfree").hide();
                div.find("img.isfreeIcon").hide();
            }
            div.find("img.age").attr("src", modulePath + "resource/image/icon_age_list_" + UTIL.transPrInfo(data.prInfo) + ".png").toggle(!!data.prInfo);
            stars.setRate(div.find(".rating_star_area"), data.mark);
            addChartOrder(div.find(".chart_area"), data, idx);
            setIcon(div.find(".icon_area"), data);
        } else {
            div.addClass("hidden_cont");
            div.attr("id", null);
            div.find(".posterImg.poster").attr("src", null);
            div.find(".posterTitle span").text("");
            div.find("img.isfreeIcon").hide();
            div.find("span.isfree").hide();
            div.find(".chart_area").empty();
        }
    }

    function setIndicatorFocus(that, focused) {
        if (focused) {
            that.indicator.focused();
            that.div.find("li.focus").removeClass('focus');
        } else {
            that.indicator.blurred();
            that.setFocus(isChangedPage ? page * LINE_AMOUNT * 2 : focus);
            isChangedPage = false;
        }
    }

    this.changeFocus = function (amount) {
        var that = this;
        var oldFocus = focus;
        var newFocus = focus;
        // Set Focus
        if (amount == 1 && focus == itemCnt - 1) newFocus = 0;
        else do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(itemCnt / LINE_AMOUNT) * LINE_AMOUNT);
        while (Math.floor(newFocus / LINE_AMOUNT) > Math.floor((itemCnt - 1) / LINE_AMOUNT));
        if (newFocus >= itemCnt) newFocus = itemCnt - 1;
        focus = newFocus;
        // Set Page
        this.div.find("li.focus").removeClass('focus');
        if (page !== Math.floor(newFocus / (LINE_AMOUNT * 2))) this.changePage((amount > 0 ? 1 : -1), function () { that.setFocus(oldFocus) });

        if (dataList[focus]) {
            this.div.find("li#" + getDataListId(focus)).addClass('focus');
            // Focus Title Animation
            setTextAnimation(this.div, focus);
        }

        if (focus % (LINE_AMOUNT * 2) + 1 > LINE_AMOUNT && !isLoading) {
            isLoading = true;
            this.parent.loadAddedContents(((page + 1) * LINE_AMOUNT * 2 + LINE_AMOUNT), LINE_AMOUNT * 2, function (res) {
                if (loadedCallback) loadedCallback(res);
                loadedCallback = null;
                isLoading = false;
            });
        }
    };

    function setTextAnimation(div, focus) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        if (UTIL.getTextLength(dataList[focus].itemName, "RixHead L", 30) > 215) {
            var posterDiv = div.find("li#" + getDataListId(focus) + " .posterTitle").first();
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.changePage = function (amount, errorCallback) {
        if (totalPage == 1) return;
        var that = this;
        page = HTool.getIndex(page, amount, totalPage);

        if (amount < 0) this.parent.loadAddedContents((page * LINE_AMOUNT * 2), LINE_AMOUNT * 2, function (res) {
            if(res) changePageImpl(that); else if(errorCallback) errorCallback();
        }, true);
        else if (isLoading) {
            LayerManager.startLoading({preventKey: true});
            loadedCallback = function (res) {
                if(res) changePageImpl(that);
                else if(errorCallback) errorCallback();
                LayerManager.stopLoading();
            };
        } else this.parent.loadAddedContents((page * LINE_AMOUNT * 2) + LINE_AMOUNT, LINE_AMOUNT * 2, function (res) {
            if(res) changePageImpl(that); else if(errorCallback) errorCallback();
        }, true);

        function changePageImpl(that) {
            that.div.removeClass("slide_up slide_down");
            that.setData(null, HTool.getIndex(page, amount > 0 ? -1 : 0, totalPage));
            void that.div[0].offsetWidth;
            that.div.addClass("slide_" + (amount > 0 ? 'up' : 'down'));
            that.indicator.setPos(page);
            that.div.find(".hidden_line").removeClass("hidden_line");

            if (page == totalPage - 1) setLineVisible(that.div.find(".line_" + (amount > 0 ? "4" : "2")), 0);
            if (amount > 0) setLineVisible(that.div.find(".line_0,.line_1"), 500);
            else clearLineVisible();
        }
    };

    this.setFocus = function (newFocus, forced) {
        if (!dataList[newFocus]) return;
        focus = newFocus;
        if (forced || page != Math.floor(newFocus / (LINE_AMOUNT * 2))) this.setPage(Math.floor(newFocus / (LINE_AMOUNT * 2)), forced);
        this.div.find("li.focus").removeClass('focus');
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        setTextAnimation(this.div, focus);
    };

    this.setPage = function (newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page, null, null, true);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
        this.div.find(".hidden_line").removeClass("hidden_line");
        if (page == totalPage - 1) setLineVisible(this.div.find(".line_2"), 0);
    };

    function clearLineVisible() {
        if (lineVisibleTimeout) {
            clearTimeout(lineVisibleTimeout);
            lineVisibleTimeout = null;
        }
    }

    function setLineVisible(div, time) {
        clearLineVisible();
        if (time == 0) div.addClass("hidden_line");
        else lineVisibleTimeout = setTimeout(function () {
            div.addClass("hidden_line");
        }, time);
    }

    this.onKeyAction = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.LEFT:
                if (focus % LINE_AMOUNT != 0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.RED:
                if(focus%(LINE_AMOUNT*2)===0) {
                    if(totalPage<=1) return true;
                    if(focus==0 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    this.changeFocus(-(LINE_AMOUNT*2));
                } else this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                if(focus === itemCnt-1) {
                    if(totalPage<=1) return true;
                    this.changeFocus(1);
                    this.changeFocus(LINE_AMOUNT * 2 - 1);
                } else if(focus%(LINE_AMOUNT*2)===LINE_AMOUNT*2-1) {
                    if(totalPage<=1) return true;
                    if(page==totalPage-2 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    else this.changeFocus(LINE_AMOUNT*2);
                } else this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
            case KEY_CODE.ENTER:
                dataList[focus].connerId = this.parent.connerId;
                if (dataList[focus].itemType == 2) {
                    dataList[focus].buyType = dataList[focus].buyType || buyType;
                    dataList[focus].catId = this.cateId;
                }
                try {
                    NavLogMgr.collectSubHome(keyCode, this.parent.viewId, this.parent.title,
                        getDataId(focus), dataList[focus].itemName);
                } catch (e) {
                }
                if(parent != undefined) {
                    var emptyData = parent.getEmptyData();
                    var playListData = {
                        list: (sortGb?dataList:[]),
                        startIdx: emptyData.startIdx,
                        amount: emptyData.lastIdx - emptyData.startIdx
                    };
                } else {
                    var playListData = {
                        list: (sortGb?dataList:[]),
                    };
                }
                var that = this;
                openDetailLayer(this.cateId, getDataId(focus), reqPathCd, {
                    contentsData: dataList[focus],
                    catData: catData,
                    vodPlayListData: playListData,
                    sortGb: sortGb,
                    isAdultCate: isAdultCate,
                    changeContentListener: function (contsId) { that.onChangedContent(contsId); }
                });
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN:
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.LEFT:
                return false;
            case KEY_CODE.ENTER:
            case KEY_CODE.RIGHT:
                setIndicatorFocus(this, false);
                return true;
            case KEY_CODE.RED:
                setIndicatorFocus(this, false);
                this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                setIndicatorFocus(this, false);
                this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
        }
    };

    this.getFocusedContents = function () {
        return dataList[focus];
    };

    this.onChangedContent = function (contsId) {
        for(var i in dataList) {
            if(dataList[i].itemId === contsId || dataList[i].contsId === contsId) {
                return this.setFocus(i-0);
            }
        }
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        if(dataList[dataIdx]) return ( dataList[dataIdx].itemId || dataList[dataIdx].contsId );
        else return "";
    }

    this.focused = function() {
        if(!dataList || dataList.length<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        clearLineVisible();
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
    };

    this.pause = function () {
        clearLineVisible();
        if(this.indicator.isFocused()) focus = isChangedPage?page*LINE_AMOUNT*2:focus;
        this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
    };

    this.resume = function () {
        this.setFocus(focus, true);
        if(!this.indicator.isFocused()) setTextAnimation(this.div, focus);
        else setIndicatorFocus(this, true);
    };
}

SquarePosterList.prototype = new ContentsList();
SquarePosterList.prototype.constructor = SquarePosterList;