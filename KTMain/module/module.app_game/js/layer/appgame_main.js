/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    subHome.layer = subHome.layer || {};
    subHome.layer.appgame_main = function HomeMain(options) {
        Layer.call(this, options);
        var viewMgr;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame appgame appgame_main"});
            this.div.html('<div id="background">' +
                // '<img id="defaultBg" src="' + modulePath + 'resource/image/bg_default.jpg" alt="background" class="clipping">' +
                '<span id="backgroundTitle"></span>' +
                '</div>');

            viewMgr = new subHome.ViewManager();
            this.div.append(viewMgr.getView());
            viewMgr.setParent(this);
            cbCreate(true);
        };

        this.controlKey = function (keyCode) {
            return viewMgr.onKeyAction(keyCode) || (keyCode == KEY_CODE.BACK && (LayerManager.historyBack(), true));
        };

        this.show = function (options) {
            Layer.prototype.show.apply(this, arguments);
            // homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);

            LayerManager.showVBOBackground("appgm_main_background");
            if(options && options.resume) {
                viewMgr.resume();
            } else if(this.getParams()) {
                var params = this.getParams();
                viewMgr.destroy();
                viewMgr.setData(params.data, params.enterMenu);
                if (params.focusIdx) viewMgr.changeCategoryFocus(params.focusIdx);
                else if (params.focusMenu) viewMgr.changeCategoryFocus(findIndex(params.data.children, params.focusMenu));
                else if (params.enterMenu) viewMgr.changeCategoryFocus(findIndex(params.data.children, params.enterMenu), true);
            }
        };

        function findIndex(root, target) {
            var targetIdx = 0;

            /**
             * [WEBIIIHOME-3671] children index 검사시, catType 이 AppGm 인 메뉴는 대상에서 제외하도록 수정 (AppGm 은 앱/게임 모듈 구분을 위한 가상의 카테고리)
             */
            for (var i = 0; i < root.length; i++) {
                if (root[i].catType === window.MENU_CONSTANT.CAT.APPGM) {
                    continue;
                }
                else if (root[i].id == target.id) {
                    return targetIdx;
                }

                ++targetIdx;
            }

            return -1;
        }

        this.getActivateCategoryId = function () {
            return viewMgr.getCurrentCategoryId();
        };

        this.hide = function (options) {
            if (options && options.pause) {
                viewMgr.pause();
            }
            else {
                /**
                 * [dj.son] [WEBIIIHOME-3882] timer clear
                 * 
                 * - 앱/게임은 이런 error 를 발생시킬 여지는 없는데, 혹시 모르니 추가
                 */
                viewMgr.clearContentsChangeTimeout();
            }

            Layer.prototype.hide.apply(this, arguments);
            LayerManager.hideVBOBackground();
        };

        this.remove = function () {
            viewMgr.destroy();
            Layer.prototype.remove.apply(this, arguments);
        }
    };

    subHome.layer.appgame_main.prototype = new Layer();
    subHome.layer.appgame_main.prototype.constructor = subHome.layer.appgame_main;

    //Override create function
    subHome.layer.appgame_main.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    subHome.layer.appgame_main.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["AppGameMain"] = subHome.layer.appgame_main;
})();