/**
 * Created by ksk91_000 on 2016-12-05.
 */
var ViewFactory = new function () {
    function getView(VCategoryObj, callback) {
        var ajax;
        if(VCategoryObj.catType == "SubHome") {
            //서브홈
            var view = new subHome.view.SubHomeView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        }else if(VCategoryObj.children) {
            //카테고리 리스트
            var view = new subHome.view.categoryView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        } else if(VCategoryObj.itemType==3||VCategoryObj.itemType==7||VCategoryObj.itemType==8) {
            //양방향 서비스
            var view = new subHome.view.AppContentsView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        } else {
            //아이템 목록.
            ajax = subHome.amocManager.getItemDetlListW3(function (result, response) {
                //히든메뉴용
                if(result) {
                    response.itemDetailList = response.itemDetailList ? (Array.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList]) : [];
                    if (response.itemDetailList && response.itemDetailList[0] && response.itemDetailList[0].itemType == 0 && response.itemDetailList[0].catType != "PkgVod") {
                        VCategoryObj.children = UTIL.getVCategoryList("ITEM_DETAIL", response.itemDetailList, VCategoryObj);
                        var view = new subHome.view.categoryView(VCategoryObj.id);
                        view.title = HTool.getMenuName(VCategoryObj);
                        view.create(VCategoryObj);
                        callback(view);
                    } else {
                        var view = new subHome.view.ContentsView(VCategoryObj.id);
                        view.title = VCategoryObj.name;
                        view.create({categoryData: VCategoryObj, dataList: response.itemDetailList});
                        callback(view);
                    }
                } else if(response!="abort") {
                    var view = new subHome.view.ContentsView(VCategoryObj.id);
                    view.title = HTool.getMenuName(VCategoryObj);
                    view.create({categoryData: VCategoryObj, dataList: []});
                    callback(view);
                }
            }, VCategoryObj.id, 1, 15);
        } return ajax;
    }
    return {
        getView:getView
    }
};