/**
 * Created by ksk91_000 on 2017-04-05.
 */
window.goToTarget = function (type, target) {
    switch (type-0) {
        case 0:
            var menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) { if(menu.id==data.target) return true; }
            });
            var module = ModuleManager.getModule("module.subhome");
            if(!module) {
                ModuleManager.loadModule({
                    moduleId: "module.vod",
                    cbLoadModule: function (success) {
                        if (success) goToTarget(type, target);
                    }
                });
                return;
            } module.activateLayerByMenu({menu: menuData});
            break;
        case 1:    //시리즈
        case 2:    //컨텐츠
            openDetailLayer(null, data.target);
            break;
        case 3:    //멀티캐스트 양방향 서비스
            var loc = target.split(",");
            var nextState;
            if(loc[0] == CONSTANT.APP_ID.MASHUP)
                nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            else
                nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

            AppServiceManager.changeService({
                nextServiceState: nextState,
                obj: {
                    type: CONSTANT.APP_TYPE.MULTICAST,
                    param: loc[0],
                    ex_param: loc[3]
                }
            });
            break;
        case 7:    //유니캐스트 양방향 서비스
            var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            AppServiceManager.changeService({
                nextServiceState: nextState,
                obj :{
                    type: CONSTANT.APP_TYPE.UNICAST,
                    param: target
                }
            });
            break;
        case 8:    //웹뷰
            AppServiceManager.startChildApp(target);
            break;
    }
}