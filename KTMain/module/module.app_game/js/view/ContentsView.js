/**
 * Created by ksk91_000 on 2016-11-30.
 */
subHome.view.ContentsView = function(viewId) {
    View.call(this, viewId);
    var div = this.div;

    var catData;
    var dataList = [];
    var emptyData = {startIdx:0, lastIdx:0};  //배열에 비어있는 구간
    var posterListComp;
    var indicator;
    var isFocused = false;
    var originData;
    var itemCnt;

    var sortOption = 0;
    var filterOption = [];
    var filterOptionData;
    var viewOption;

    var instance = this;
    var contextMenu;
    var catId;
    var sortGb;
    var filterCdList = "", filterOptList = "";
    this.connerId;

    this.create = function(data) {
        div.attr("class", "contentsView");
        div.append(_$("<div/>", {class:"indicator_area"}));
        div.append(_$("<div/>", {class:"contents_area"}));
        div.append(_$("<div/>", {class:"bottom_dim"}));
        div.append(_$("<div/>", {class:"over_sdw_area"}));

        catId = data.categoryData.id;
        sortGb = data.categoryData.sortGb;
        catData = data.categoryData;
        originData = data;
        itemCnt = originData.dataList.length>0?originData.dataList[0].webCatItemCnt-0:0;
        emptyData.startIdx = data.length;
        emptyData.lastIdx = itemCnt;

        createContentsView.call(this, data);
        createContextMenu();
    };

    function createContextMenu() {
        filterOptionData = getFilter(catId);
        contextMenu = new ContextMenu_VOD();
        contextMenu.addTitle("정렬옵션");
        contextMenu.addDropBox(["최신순", "별점순", "가나다순"]);

        if(filterOptionData.length>0) {
            contextMenu.addSeparateLine();
            for (var i = 0; i < filterOptionData.length; i++) {
                contextMenu.addTitle(filterOptionData[i].filterText);
                contextMenu.addDropBox(filterOptionData[i].filterOptionTextList);
                filterOption[i] = 0;
            }contextMenu.addSeparateLine();
        }

        var posterType = getPosterListType(catData.listType, catData.posterListType);
        viewOption = posterType === -1 || posterType === -2 ? 1 : 0;
        contextMenu.addTitle("VOD 보기 옵션");
        contextMenu.addDropBox(["포스터 보기", "타이틀 보기"], viewOption);

        div.append(contextMenu.getView());
        contextMenu.setEventListener(contextMenuListener)
    }

    function createContentsView(data) {
        if(!Array.isArray(data.dataList)) data.dataList = [data.dataList];

        setOverSdwArea(data);
        dataList = data.dataList;
        itemCnt = data.dataList.length>0?data.dataList[0].webCatItemCnt-0:0;
        var pt = getPosterListType(catData.listType, catData.posterListType);
        if(dataList.length===0) {
            instance.removal = true;
            posterListComp = new NoContentsList();
        }else {
            posterListComp = getPosterListComp(pt);
        } posterListComp.create(catId, catData);
        div.find(".contents_area").empty().append(posterListComp.getView());
        indicator = new Indicator(1);

        posterListComp.setIndicator(indicator);
        posterListComp.parent = this;
        this.connerId = data.categoryData.connerId;
        setData(dataList, 0, sortGb, itemCnt);
        div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());
    }

    /**
     * 포스터 리스트 타입을 반환한다.
     *
     * @param listType
     * @param posterListType
     * @returns {number}   -1 : 미리보기 포스터가 직사각형인 타이틀
     *                      -2 : 미리보기 포스터가 정사각형인 타이틀
     *                      1 : 직사각형 포스터
     *                      2 : 정사각형 포스터
     */
    function getPosterListType(listType, posterListType) {
        var listMode = StorageManager.ps.load(StorageManager.KEY.VOD_LIST_MODE);
        log.printDbg("getPosterListType() - listMode : " + listMode + ", listType : " + listType + ", posterListType : " + posterListType);
        var type = undefined;
        if (listMode === "poster") { // 포스터 보기
            type = posterListType;
        } else if (listMode === "text" || listMode === "list" || listType == 1) { // 타이틀 보기
            if (parseInt(posterListType) === 2) { // 미리보기 포스터가 정사각형인 경우
                type = -2;
            } else { // 미리보기 포스터가 직사각형인 경우
                type = -1;
            }
        } else {
            type = posterListType;
        }
        log.printDbg("getPosterListType() - result, type : " + type);
        return type;
    }

    function getPosterListComp(data) {
        switch (parseInt(data)) {
            case -1: viewOption = 1; return new TitleList(instance, TitleList.TYPE.VERTICAL);// 미리보기 포스터가 직사각형인 경우
            case -2: viewOption = 1; return new TitleList(instance, TitleList.TYPE.SQUARE);// 미리보기 포스터가 정사각형인 경우 
            // case 1 : viewOption = 0; return new HorizontalPosterList();
            case 2 : viewOption = 0; return new SquarePosterList(instance);// 정사각(뮤직등)
            default: viewOption = 0; return new VerticalPosterList(instance);// 직사각(영화등)
        }
    }

    function setOverSdwArea(data) {
        if(data.categoryData.posterListType!=0) return;
        // var d = div.find(".over_sdw_area");
        // d.append("<div class='over_sdw_1'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        // d.append("<div class='over_sdw_2'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        // d.append("<div class='over_sdw_3'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
    }

    function setData(_dataList, page, _sortGb, itemCnt) {
        posterListComp.setData(_dataList, page, _sortGb, itemCnt);
    }

    function changeFocus(amount) {
        posterListComp.changeFocus(amount);
    }

    function changePage(amount) {
        posterListComp.changePage(amount);
    }

    function setFocus(newFocus) {
        posterListComp.setFocus(newFocus);
    }

    function setPage(newPage){
        posterListComp.setPage(newPage);
    }

    function getFilter(id) {
        var MenuDataManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.MENU_DATA_MANAGER);

        var a = MenuDataManager.searchMenu({
            menuData: MenuDataManager.getMenuData(),
            allSearch: false,
            cbCondition: function (menu) { return menu.id == id; }
        })[0];
        if(!a) return[];

        do{
            var filter = SubHomeDataManager.getFilter(a.id);
            if(filter) return filter;
        }while(a=a.parent);
        return [];
    }

    this.loadAddedContents = function (startIdx, amount, callback, showLoading) {
        if(startIdx>itemCnt-1 || (dataList[startIdx] && dataList[Math.min(startIdx+amount-1, itemCnt-1)])) callback(true);
        else {
            if(showLoading) LayerManager.startLoading({preventKey: true});
            if(startIdx <= emptyData.startIdx) emptyData.startIdx = startIdx + amount + 1;
            else emptyData.lastIdx = startIdx;
            subHome.amocManager.getItemDetlListW3(function (result, response) {
                if(result) {
                    response.itemDetailList = response.itemDetailList ? (Array.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList]) : [];
                    for (var i = 0; i < amount && startIdx+i<itemCnt; i++) dataList[startIdx + i] = response.itemDetailList[i];
                    posterListComp.updateData(dataList);
                } else {
                    HTool.openErrorPopup(["편성정보를 불러오지 못했습니다", "잠시 후 다시 시도해 주세요"]);
                }

                if (callback) callback(result);
                if (showLoading) LayerManager.stopLoading();
            }, catId, startIdx + 1, amount, sortOption, filterCdList.substr(1, filterCdList.length-1), filterOptList.substr(1, filterOptList.length-1));
        }
    };

    function contextMenuListener(index, data) {
        log.printDbg("called contextMenuListener() - index : " + index);
        contextMenu.close();
        if (index == 0) {
            if (sortOption == data) return;
            var oldOption = sortOption;
            sortOption = data;
            reloadData(function (res) {
                if(!res) {
                    sortOption = oldOption;
                    contextMenu.setDropBoxFocus(0, oldOption);
                    updateFilterList();
                }
            });
        } else if (index<=filterOptionData.length) {
            if(filterOption[index-1]==data) return;
            var oldOption = filterOption[index-1]||0;
            filterOption[index-1] = data;
            reloadData(function (res) {
                if(!res) {
                    filterOption[index-1] = oldOption;
                    contextMenu.setDropBoxFocus(index, oldOption);
                    updateFilterList();
                }
            });
        } else if(index == filterOptionData.length+1) {
            if(viewOption == data) return;
            viewOption = data;
            StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, viewOption==0?"poster":"list");
            createContentsView.call(instance, {categoryData: catData, dataList: dataList});
            posterListComp.focused();
            instance.viewMgr.clearRemovableQueue();
        } contextMenu.close();

        function updateFilterList() {
            filterCdList = "";
            filterOptList = "";
            for(var i in filterOptionData) {
                if(filterOptionData[i].filterOptionCdList[filterOption[i]]) {
                    filterCdList += "|" + filterOptionData[i].filterCd;
                    filterOptList += "|" + filterOptionData[i].filterOptionCdList[filterOption[i]];
                }
            }
        }

        function reloadData(callback) {
            updateFilterList();
            subHome.amocManager.getItemDetlListW3(function (result, response) {
                if(result && response && response.itemDetailList) {
                    createContentsView.call(instance, { categoryData: catData, dataList: response.itemDetailList });
                    posterListComp.focused();
                    callback(true);
                }else {
                    showToast("선택한 옵션으로 표시할 수 있는 콘텐츠가 없습니다");
                    callback(false);
                }
            }, catId, 1, 15, sortOption, filterCdList.substr(1, filterCdList.length-1), filterOptList.substr(1, filterOptList.length-1));
        }

    }

    function clearFilter() {
        if(sortOption!=0) {
            clearImpl();
            return true;
        } for(var i in filterOption) {
            if(filterOption[i]!=0) {
                clearImpl();
                return true;
            }
        }

        function clearImpl() {
            createContentsView.call(instance, originData);
            sortOption = 0;
            filterCdList = "";
            filterOptList = "";
            filterOption = [];
            contextMenu.clearDropBoxFocus();
            contextMenu.setDropBoxFocus(filterOptionData.length+1, viewOption);
        }
    }

    this.onKeyAction = function(keyCode) {
        if(contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        if(posterListComp.onKeyAction(keyCode)) return true;
        else if(keyCode == KEY_CODE.CONTEXT) {
            var data = posterListComp.getFocusedContents();
            if(data) contextMenu.setVodInfo({
                itemName: data.contsName||data.itemName,
                imgUrl: data.imgUrl,
                contsId: data.itemId||data.contsId,
                catId: catId,
                itemType: data.itemType,
                cmbYn: data.cmbYn,
                seriesType: data.seriesType,
                resolCd: data.resolCd,
                assetId: data.assetId,
                prInfo: data.prInfo
            });
            contextMenu.open();
            return true;
        } else return false;
    };

    this.getEmptyData = function() {
        return {
            startIdx: getFirstIdx(),
            lastIdx: getLastIdx()+1
        };

        function getFirstIdx() {
            for (var i = 0; i < itemCnt; i++) if(!dataList[i]) return i;
        }

        function getLastIdx() {
            for (var i = itemCnt-1; i >= 0; i--) if(!dataList[i]) return i;
        }
    };

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.removeClass("blurred");
        //this.viewMgr.setHomeShot(catData.hsTargetType, catData.hsTargetId, catData.w3HsImgUrl, catData.hsLocator, catData.hsLocator2, catData.hsKTCasLocator);
        if(!posterListComp.focused()) return subHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        isFocused = true;
        this.viewMgr.setContextIndicator("찜 | 정렬");
        return subHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.blurred =  function() {
        isFocused = false;
        if(!clearFilter()) posterListComp.blurred();
        div.addClass("blurred");
        div.removeClass("focused");
        this.viewMgr.setContextIndicator("");
    };

    this.pause = function () {
        if(posterListComp.hasOwnProperty("pause")) posterListComp.pause();
    };

    this.resume = function () {
        if(posterListComp.hasOwnProperty("resume")) posterListComp.resume();
    };
};
subHome.view.ContentsView.prototype = new View();
subHome.view.ContentsView.prototype.constructor = subHome.view.ContentsView;
subHome.view.ContentsView.prototype.type = View.TYPE.CONTENTS;