/**
 * Created by ksk91_000 on 2016-08-19.
 */
subHome.ViewManager = function () {
    var parent;
    var div = _$("<div/>", {class:'viewArea'});

    var instance;

    var currentCateView;
    var currentContView;
    var contentsChangeTimeout = null;
    var contentsLoadAjax = null;

    var ViewMap = {};
    var historyStack = [];
    var removableQueue = [];

    var MODE = {CATEGORY:0, CONTENTS:1};
    var curLoadViewId;

    var homeShot = new subHome.HomeShot();
    var mode = MODE.CATEGORY;
    var curBgIdx = 0;
    var bg_timer;
    var emptyView = new View("emptyView");

    var oldInfo;
    var curInfo;

    div.append(homeShot.getView());

    // div.append(_$("<div/>", {class:"context_menu_indicator"}));
    var context = _$("<div/>", {class:"context_menu_indicator_area"});
    context.append(_$("<div/>", {class:"context_menu_indicator"}));
    context.css("visibility", "hidden");
    div.append(context);

    var cateKeyAction = function(keyCode) {
        var oldView = currentCateView.getCurrentContent();
        var oldCateView = currentCateView;
        if(currentCateView.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.UP:
            case KEY_CODE.DOWN:
            case KEY_CODE.RED:
            case KEY_CODE.BLUE:
                clearContentsAjax();
                if(currentContView.removal) {
                    removeView(currentContView);
                }
                var view = currentCateView.getCurrentContent();
                setContentsChangeTimeout(function() {
                    setCurrentContentData(view);
                }, 300);
                try {
                    oldInfo = oldCateView.getCateInfo();
                    var prevInfo = oldView ;
                    NavLogMgr.collectSubHome(keyCode, oldInfo.id, oldInfo.name, prevInfo.id, prevInfo.name);
                } catch(e) {}
                return true;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                contensChangeImmediately();
                try {
                    oldInfo = oldCateView.getCateInfo();
                    curInfo = oldCateView.getCurrentContent();
                    NavLogMgr.collectSubHome(keyCode, oldInfo.id, oldInfo.name, curInfo.id, curInfo.name);
                } catch(e) {}
                return true;
        }
    };

    var changeFocus = function(view) {
        if(getCurrentView() === view)
            return;

        switch (view.type) {
            case View.TYPE.CATEGORY:
                currentCateView.focused();
                historyStack.push(getCurrentView());
                var focusMode = view.focused();
                if(focusMode === subHome.ViewManager.FOCUS_MODE.NO_FOCUS) {
                    historyBack();
                    return;
                } else if(focusMode === subHome.ViewManager.FOCUS_MODE.BLUR_FOCUS) {
                    var oldCateView = currentCateView;
                    setCurrentCategory(view, true);
                    oldCateView.unfocused ? oldCateView.unfocused() : 0;
                    oldCateView.show();
                } else {
                    view.setPage(0);
                    view.setFocus(0);
                    // 포커스가 변경되었으므로 contents 에 설정할 뷰를 새로 갱신해야 함
                    setCurrentCategory(view);
                    changeTitle(view.title);
                } break;
            case View.TYPE.CONTENTS:
                currentCateView.unfocused();
                historyStack.push(getCurrentView());
                mode = MODE.CONTENTS;
                if(currentContView!==view) setCurrentContent(view);
                var focusMode = view.focused();
                if(focusMode == subHome.ViewManager.FOCUS_MODE.NO_FOCUS) {
                    historyBack();
                    return;
                }else if(focusMode == subHome.ViewManager.FOCUS_MODE.BLUR_FOCUS) currentCateView.unfocused?currentCateView.unfocused():0;
                else {
                    // currentCateView.hide();
                    changeTitle(view.title);
                } break;
        }
        var rmQidx = removableQueue.indexOf(view);
        if(rmQidx>=0) removableQueue.splice(rmQidx, 1);
    };

    var changeCategoryFocus = function(idx, isEnter) {
        if(idx<0) return;
        if(mode == MODE.CATEGORY) {
            currentCateView.setFocus(idx);
        } setCurrentContentData(currentCateView.getCurrentContent(), function () {
            if(isEnter) changeFocus(currentContView);
        });
    };

    var changeTitle = function(title) {
        if (UTIL.getTextLength(title, "RixHead B", 40, -1.2) > 360) {
            parent.div.find("#backgroundTitle").css("font-size", "38px");
            var newTitle = HTool.ellipsis(title, "RixHead B", 38, 360, "...", -1.2);
            parent.div.find("#backgroundTitle").text(newTitle);
        }
        else {
            parent.div.find("#backgroundTitle").css("font-size", "40px");
            parent.div.find("#backgroundTitle").text(title);
        }
    };

    /**
     * [dj.son] HomeShotDataManager 적용
     *
     * - 사용 가능한 모든 홈샷 데이터를 모아 노출 (실제 노출 로직은 HomeShot 컴포넌트안에 있고, 여기는 그냥 데이터 세팅)
     * - 각 View 에서 HomeShot 호출하던것 제거하고 ViewManager 에서 일괄 호출하도록 수정
     * -- 단 CategoryView 에서는 성인인증이 걸릴 수가 있으므로, CategoryView 에서는 유지
     */
    var setHomeShot = function (options) {
        var bShow = false;
        var arrHomeShotData = null;

        if (options && options.menu) {

            /**
             * [dj.son] ViewFactory 에 있는 조건문 참조
             */
            if (options.menu.catType === MENU_CONSTANT.CAT.SUBHOME || options.menu.catType === MENU_CONSTANT.CAT.RANK || options.menu.catType === MENU_CONSTANT.CAT.RANKW3
                || options.menu.catType === MENU_CONSTANT.CAT.RECOM_CAT || options.menu.catType === MENU_CONSTANT.CAT.RECOM_PRODUCT) {
                bShow = false;
            }
            else  if ((options.menu.children && options.menu.children.length > 0)
                || options.menu.itemType === MENU_CONSTANT.ITEM.INT_M || options.menu.itemType === MENU_CONSTANT.ITEM.INT_U || options.menu.itemType === MENU_CONSTANT.ITEM.INT_W) {

                bShow = true;
            }

            if (bShow) {
                arrHomeShotData = HomeShotDataManager.getCategoryHomeShot({
                    menu: options.menu
                });
            }
        }

        homeShot.setHomeShot({
            bShow: bShow,
            arrHomeShotData: arrHomeShotData
        });
    };

    var historyBack = function (callback) {
        if (historyStack.length > 0) {
            // [hw.boo] 로드 중인 데이터 & 페이지 clear(WEBIIIHOME-3642)
            clearContentsAjax();
            clearContentsChangeTimeout();
            var oldView = getCurrentView();
            var currentView = historyStack.pop();
            addRemovableQueue(oldView);
            switch (currentView.type) {
                case View.TYPE.CATEGORY:
                    mode = MODE.CATEGORY;
                    setCurrentCategory(currentView);
                    break;
                case View.TYPE.CONTENTS:
                    mode = MODE.CONTENTS;
                    setCurrentContent(currentView);
                    currentView.hide();
                    currentView.focused();
                    if(callback) callback();
                    break;
            } changeTitle(currentView.title);
            return true;
        } else {
            LayerManager.historyBack();
            return true;
        }
    };

    var reChangeFocus = function () {
        setCurrentCategory(historyStack.pop(), true);
        setCurrentContentData(currentCateView.getCurrentContent(), changeFocus);
    };

    var addView = function(view) {
        ViewMap[view.viewId] = ViewMap[view.viewId]||(div.append(view.div), view);
        view.viewMgr = instance;
    };

    var removeView = function (view) {
        var qIdx = removableQueue.indexOf(view);
        if(qIdx>0) removableQueue.splice(qIdx, 1);
        view.getView().remove();
        delete ViewMap[view.viewId];
    };

    var setCurrentCategory = function (view, noChangePreview) {
        mode = MODE.CATEGORY;
        currentCateView?currentCateView.hide():0;
        currentCateView = view;
        addView(view);
        currentCateView.focused();
        currentCateView.show();
        changeTitle(view.title);
        if(!noChangePreview) {
            if (currentContView === view)
                currentContView = null;
            setCurrentContentData(currentCateView.getCurrentContent());
        }
    };

    var setCurrentContent = function (view, focus) {
        currentContView?currentContView.hide():0;
        currentContView = view;
        addRemovableQueue(currentContView);
        addView(currentContView);
        currentContView.blurred();
        currentContView.show();

        if(focus) {
            mode = MODE.CONTENTS;
            changeTitle(view.title);
            if(currentContView.focused() === subHome.ViewManager.FOCUS_MODE.NO_FOCUS)
                historyBack();
        }

        /**
         * [sjoh] 타입이 0인경우 contents 가 하위 카테고리를 의미하므로 배경의 dim 처리를 제거 및 home shot 을 표시한다.
         * 타입이 1이 아닌경우 배경의 dim 을 표시하며, home shot 을 표시하지 않는다.
         */
        if (currentContView.type === View.TYPE.CATEGORY){
            _$('#bg_black').hide();

            var categorys = currentCateView.getCategoryData();
            var targetMenu = null;

            for (var i = 0; i < categorys.length; i++) {
                if (currentContView.viewId === categorys[i].id) {
                    targetMenu = categorys[i];
                    break;
                }
            }

            setHomeShot({menu: targetMenu});
        }
        else {
            _$('#bg_black').show();

            if (currentContView instanceof subHome.view.AppContentsView) {
                var categorys = currentCateView.getCategoryData();
                var targetMenu = null;

                for (var i = 0; i < categorys.length; i++) {
                    if (currentContView.viewId === categorys[i].id) {
                        targetMenu = categorys[i];
                        break;
                    }
                }

                setHomeShot({menu: targetMenu});
            }
            else {
                setHomeShot();
            }
        }
    };

    var setCurrentContentData =function (viewData, callback) {
        if(ViewMap[viewData.id]) {
            LayerManager.stopLoading();
            setCurrentContent(ViewMap[viewData.id]);
            if(callback) callback(ViewMap[viewData.id]);
        } else {
            setCurrentContent(emptyView);
            LayerManager.startLoading();
            loadViewByData(viewData, function (view) {
                setCurrentContent(view);
                if (callback) callback(view);
                if (view.isLoaded()) LayerManager.stopLoading();
            });
        }
    };

    function addRemovableQueue(view) {
        if(view==emptyView || view==getCurrentView() || historyStack.indexOf(view)>=0) return;
        if(removableQueue.indexOf(view)>=0) removableQueue.splice(removableQueue.indexOf(view), 1);
        if(removableQueue.length>=3) {
            var rmv = removableQueue.pop();
            if(rmv != undefined && rmv!=view) {
                rmv.destroy();
                delete ViewMap[rmv.viewId];
            }
        } removableQueue.unshift(view);
    }

    var clearRemovableQueue = function() {
        if(removableQueue.length < 2) return;

        for(var i in removableQueue) {
            var rmv = removableQueue[i];
            rmv.destroy();
            delete ViewMap[rmv.viewId];
        } curLoadViewId = null;
    };

    var getCurrentView = function() {
        if(mode==MODE.CATEGORY) return currentCateView;
        else if(mode==MODE.CONTENTS) return currentContView;
    };

    var setData = function(data, enterMenu) {
        LayerManager.startLoading();
        if(ViewMap[data.id]) {
            if(ViewMap[data.id].type== View.TYPE.CATEGORY) setCurrentCategory(ViewMap[data.id], !!enterMenu);
            else if(ViewMap[data.id].type== View.TYPE.CONTENTS) setCurrentContent(ViewMap[data.id], true);
            if (ViewMap[data.id].isLoaded()) LayerManager.stopLoading();
        } else {
            loadViewByData(data, function (view) {
                if(view.type== View.TYPE.CATEGORY) setCurrentCategory(view, !!enterMenu);
                else if(view.type== View.TYPE.CONTENTS) setCurrentContent(view, true);
                if (ViewMap[data.id].isLoaded()) LayerManager.stopLoading();
            });
        }
    };

    function loadViewByData(data, callback) {
        console.log("curLoadViewId = " + curLoadViewId);
        if(curLoadViewId == data.id) return;
        curLoadViewId = data.id;
        console.log("nextLoadViewId = " + curLoadViewId);
        contentsLoadAjax = ViewFactory.getView(data, function(view) {
            callback(view);
        })
    }

    function setContentsChangeTimeout(func, time) {
        // setCurrentContent(emptyView);
        // LayerManager.startLoading();
        clearContentsChangeTimeout();
        contentsChangeTimeout = setTimeout(func, time);
    }

    function clearContentsChangeTimeout() {
        if(contentsChangeTimeout) {
            clearTimeout(contentsChangeTimeout);
            contentsChangeTimeout = null;
        }
    }

    function clearContentsAjax() {
        if(contentsLoadAjax) {
            subHome.amocManager.ajaxStop(contentsLoadAjax);
            contentsLoadAjax = null;
            curLoadViewId = null;
        }
    }

    function contensChangeImmediately() {
        clearContentsChangeTimeout();
        setCurrentContentData(currentCateView.getCurrentContent(), changeFocus);
    }

    function getCurrentCategoryId() {
        return getCurrentView().viewId;
    }

    function getCurrentCateID() {
        return currentCateView.getCateInfo().id;
    }

    var setContextIndicator = function (text) {
        div.find(".context_menu_indicator").html(text);
        if (text && text.length > 0) {
            div.find(".context_menu_indicator_area").css("visibility", "inherit");
        }
        else {
            div.find(".context_menu_indicator_area").css("visibility", "hidden");
        }
    };

    var pause = function () {
        getCurrentView().pause();
    };

    var resume = function () {
        getCurrentView().resume();
    };

    instance = {
        setParent: function (_parent) { parent = _parent; },
        setData: setData,
        onKeyAction: function (keyCode) {
            if(mode==MODE.CONTENTS && currentContView && currentContView.onKeyAction(keyCode)) return true;
            else if(mode==MODE.CATEGORY && cateKeyAction(keyCode)) return true;

            if(keyCode==KEY_CODE.PLAY && homeShot.execute()) return true;
            return (keyCode == KEY_CODE.BACK || keyCode == KEY_CODE.LEFT) && historyBack();
        },
        getView: function() { return div; },
        destroy: function() {
            historyStack = [];
            removableQueue = [];
            if(currentCateView) currentCateView.hide();
            if(currentContView) currentContView.hide();
            currentCateView = null;
            currentContView = null;
            for(var id in ViewMap) {
                ViewMap[id].div.remove();
                delete ViewMap[id];
            } ViewMap = {};
        },
        setContextIndicator: setContextIndicator,
        changeCategoryFocus: changeCategoryFocus,
        setHomeShot:setHomeShot,
        historyBack: historyBack,
        reChangeFocus: reChangeFocus,
        setCurrentContentData: setCurrentContentData,
        clearRemovableQueue: clearRemovableQueue,
        getCurrentCategoryId: getCurrentCategoryId,
        getCurrentCateID: getCurrentCateID,
        pause: pause,
        resume: resume,

        clearContentsChangeTimeout: clearContentsChangeTimeout
    };

    return instance;
};
Object.defineProperty(subHome.ViewManager, "FOCUS_MODE", { value: { NO_FOCUS: 0, FOCUS:1, BLUR_FOCUS: 2 },writable:false });