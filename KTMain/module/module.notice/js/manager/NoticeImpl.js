/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>NoticeImpl</code>
 *
 * 채널 미가입 및 OTS PPV 관련 외부 연동
 * @author jjh1117
 * @since 2017-07-10
 */

"use strict";

var NoticeImpl = (function() {
    var log = Log;
    var introImgList = [];
    var introImgTitleList = [];
    var introUidList = [];
    var introCount = 0;
    var blockPeroid = 0;
    var isshowNoticePopup = false;
    
    
    var storageData;

    function _getNoticeInfo() {
        IntroUIManager.getIntroInfo(_callbackFuncNoticeInfo);
    }

    function _checkedCallbackFunc(isChecked) {
        log.printDbg('_checkedCallbackFunc() isChecked: ' + isChecked);
        if(isChecked === false) {
            log.printDbg('_checkedCallbackFunc() storageData: ' + storageData);
            if(storageData === undefined || storageData === null || storageData === "") {
                StorageManager.ps.remove(StorageManager.KEY.INTROUI_POPUP_DATA);
            }else {
                StorageManager.ps.save(StorageManager.KEY.INTROUI_POPUP_DATA , storageData);
            }
        }else {
            var saveIntroDataList = [];

            var myDate = new Date();

            if(blockPeroid === 1) {
                myDate.setDate(myDate.getDate() + 1);
            }else if(blockPeroid === 7) {
                myDate.setDate(myDate.getDate() + 7);
            }

            var endtime = myDate.getTime();
            if(blockPeroid === 0) {
                endtime = 0;
            }else {
                endtime = Math.floor(endtime/1000);
            }

            var nowDate = new Date();
            var now =  Math.floor(nowDate.getTime()/1000);

            var introDataList = null;

            if(storageData !== undefined && storageData !== null && storageData !== "") {
                introDataList = JSON.parse(storageData);
            }

            for (var i = 0; i < introCount; i++) {
                var introData = {};
                introData.introUid = introUidList[i];
                introData.blockPeroid = blockPeroid;

                if(introDataList !== null) {
                    var isSearch = false;
                    for(var j=0 ; j<introDataList.length ; j++) {
                        isSearch = false;
                        var tempIntroData = introDataList[j];
                        if(introData.introUid === tempIntroData.introUid) {
                            isSearch = true;
                            if(now>tempIntroData.next_show_time) {
                                introData.next_show_time = endtime;
                            }else {
                                introData.next_show_time = tempIntroData.next_show_time;
                            }
                            break;
                        }
                    }
                    if(isSearch === false) {
                        introData.next_show_time = endtime;
                    }
                }else {
                    introData.next_show_time = endtime;

                }

                saveIntroDataList[saveIntroDataList.length] = introData;
            }
            StorageManager.ps.save(StorageManager.KEY.INTROUI_POPUP_DATA , JSON.stringify(saveIntroDataList));
        }
    }


    function _callbackFuncNoticeInfo(result, responseData) {
        log.printDbg('_callbackFuncNoticeInfo() result: ' + result + " , responseData : " + (result === true ? JSON.stringify(responseData) : ""));

        isshowNoticePopup = false;
        introImgList = [];
        introImgTitleList = [];
        introUidList = [];
        introCount = 0;
        blockPeroid = 0;

        storageData = null;

        var serviceState = StateManager.serviceState;
        log.printDbg('_callbackFuncNoticeInfo() serviceState: ' + serviceState);
        if(serviceState === KTW_ENV.SERVICE_STATE.OTHER_APP ||
            serviceState === KTW_ENV.SERVICE_STATE.STANDBY ||
            serviceState === KTW_ENV.SERVICE_STATE.TIME_RESTRICTED) {
            result = false;
        }

        if(result === true) {
            /**
             * Response Success 인 경우
             */
            if (responseData !== undefined && responseData !== null && responseData.resultCd !== undefined && responseData.resultCd !== null) {
                if (responseData.resultCd === "0") {
                    if (responseData.list) {
                        var listLength = responseData.list.length;
                        if (listLength === 0) {
                            // delete local storage
                            StorageManager.ps.remove(StorageManager.KEY.INTROUI_POPUP_DATA);
                        }
                        else {
                            // 처리
                            blockPeroid = responseData.noticeBlockPeriod;
                            introCount = responseData.list.length;

                            if(introCount>0) {
                                for (var i = 0; i < introCount; i++) {
                                    introImgList[introImgList.length] = responseData.list[i].imgUrl;
                                    introImgTitleList[introImgTitleList.length] = responseData.list[i].title;
                                    introUidList[introUidList.length] = responseData.list[i].noticeUid;
                                }
                            }
                            storageData = StorageManager.ps.load(StorageManager.KEY.INTROUI_POPUP_DATA);

                            var popupDataImgList = [];
                            var popupDataImgTitleList = [];

                            var isShow = false;
                            for(var i = 0 ; i<introCount ; i++) {
                                isShow = false;
                                if(storageData === undefined || storageData === null || storageData === "") {
                                    isShow = true;
                                }else {
                                    log.printDbg('_callbackFuncNoticeInfo() storageData: ' + storageData);
                                    var introDataList = JSON.parse(storageData);

                                    var isSearch = false;
                                    for(var j=0 ; j<introDataList.length ; j++) {
                                        isSearch = false;
                                        var tempIntroData = introDataList[j];
                                        log.printDbg('_callbackFuncNoticeInfo() introUidList[i]: ' + introUidList[i] + " , tempIntroData.introUid : " + tempIntroData.introUid);
                                        if(introUidList[i] === tempIntroData.introUid) {
                                            isSearch = true;
                                            log.printDbg('_callbackFuncNoticeInfo() tempIntroData.blockPeroid : ' + tempIntroData.blockPeroid);
                                            if(tempIntroData.blockPeroid !== 0) {
                                                /**
                                                 * 초기 저장된 next_show_time 이 0인 경우는 BlockPeroid가 0인 경우임.
                                                 * blockperoid는 변경 될 수 있으며 next_show_time를 다시 한번 체크함.
                                                 */
                                                if(tempIntroData.next_show_time === 0) {
                                                    isShow = false;
                                                }else {
                                                    var now = new Date().getTime();
                                                    now = Math.floor(now/1000);
                                                    log.printDbg('_callbackFuncNoticeInfo() now : ' + now + " , tempIntroData.next_show_time : " + tempIntroData.next_show_time);
                                                    if(now>tempIntroData.next_show_time) {
                                                        isShow = true;
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    log.printDbg('_callbackFuncNoticeInfo() isSearch : ' + isSearch);
                                    if(isSearch === false) {
                                        isShow = true;
                                    }
                                }

                                if(isShow === true) {
                                    popupDataImgList[popupDataImgList.length] =  introImgList[i];
                                    popupDataImgTitleList[popupDataImgTitleList.length] =  introImgTitleList[i];
                                }
                            }

                            if(popupDataImgList.length>0) {
                                isshowNoticePopup = true;
                            }

                            if(isshowNoticePopup === true) {
                                var params = {
                                    intro_img_list : popupDataImgList,
                                    intro_img_title_list : popupDataImgTitleList,
                                    intro_count : popupDataImgList.length,
                                    blockPeroid : blockPeroid,
                                    callback : _checkedCallbackFunc
                                };

                                LayerManager.activateLayer({
                                    obj: {
                                        id: NOTICE_POPUP,
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            data : params
                                        }
                                    },
                                    moduleId: Module.ID.MODULE_NOTICE,
                                    visible: true
                                });
                            }
                        }
                    }
                    else {
                        // delete local storage
                        StorageManager.ps.remove(StorageManager.KEY.INTROUI_POPUP_DATA);
                    }
                }
                else if (responseData.resultCd === "1301") {
                    // delete local storage
                    StorageManager.ps.remove(StorageManager.KEY.INTROUI_POPUP_DATA);
                }
            }
            else {
                // something error do nothing
            }
        }
    }

    return {
        getNoticeInfo: _getNoticeInfo
    }
}());