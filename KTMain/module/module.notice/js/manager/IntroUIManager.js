/**
 * Created by user on 2016-06-10.
 */

"use strict";

var IntroUIManager = (function() {

    var log = Log;

    function _getIntroInfo (callback) {
        log.printDbg("_getIntroInfo() ");

        var url = "";
        var said = KTW_DEF.SAID;
        var frameworkVersion = KTW_DEF.FRAMEWORK_VERSION;
        var model = KTW_DEF.STB_MODEL;


        if (KTW_DEF.RELEASE_MODE === KTW_ENV.ENV.LIVE) {
            url = MODULE_SERVER[1] + "/deploy-api/intro/info";
        }else if(KTW_DEF.RELEASE_MODE === KTW_ENV.ENV.BMT){
            url = MODULE_SERVER[2] + "/deploy-api/intro/info";
        }
        
        var postData = {
            saId : said,
            model: model,
            frameworkVersion: frameworkVersion ,
            version : moduleVersion
        };
        log.printDbg("_getIntroInfo() url :  " + url);
        log.printDbg("_getIntroInfo() postData :  " + JSON.stringify(postData));

        _$.ajax({
            url: url,
            type: "post",
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData)
        }).done(function (data) {
            log.printDbg("_getIntroInfo() done : " + log.stringify(data));
            if(callback !== undefined && callback !== null) {
                callback(true , data);
            }
        }).fail(function () {

            log.printDbg("_getIntroInfo() fail");
            if(callback !== undefined && callback !== null) {
                callback(false , null);
            }
        });


    }



    return {
        getIntroInfo: _getIntroInfo
    };
})();

