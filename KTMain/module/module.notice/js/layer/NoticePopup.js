/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>NoticePopup</code>
 * params : {
 *             intro_uid_list : introUidList,
 *             img_list : imgList,
 *             img_title_list : imgTitleList,
 *             count : itemCount,
 *             blockPeroid : "" + blockPeroid,
 *             allback : null
 * }
 * 업셀링 상품 상세 정보 팝업
 * @author jjh1117
 * @since 2016. 12. 30.
 */

"use strict";

(function() {
    var noticePopup = function NoticePopup(options) {
        Layer.call(this, options);

        var util = Util;
        var log = Log;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var showImageList = [];
        var showImageTitleList = [];
        var introCount = 0;
        var blockPeroid = 0;
        var callbackFunc = null;

        var btnCheckbox = null;
        var closeBtn = null;
        
        var imgFocusIndex = 0;
        var isShowCloseBtn = false;
        var isShowCheckBtn = false;

        var focusBtnType = 0;

        var BTN_INDICATOR= 1;
        var BTN_CLOSE = 2;
        var BTN_CHECK = 3;

        var isChecked = false;

        var isShowIndicator = false;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();

            if(params !== null) {
                showImageList = params.data.intro_img_list;
                showImageTitleList = params.data.intro_img_title_list;
                introCount = params.data.intro_count;
                blockPeroid = params.data.blockPeroid;
                callbackFunc = params.data.callback;
            }

            div = this.div;
            _createView(div);

            if(cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };


        this.controlKey = function(key_code) {
            log.printDbg("controlKey() key_code : " + key_code);
            var consumed = false;
            switch (key_code) {
                case KTW_KEY_CODE.DOWN :
                    if(focusBtnType === BTN_INDICATOR) {
                        focusBtnType = BTN_CLOSE;
                        _focusUpdate();
                    } else if (focusBtnType === BTN_CLOSE || focusBtnType === BTN_CHECK) {
                        if (isShowIndicator) {
                            focusBtnType = BTN_INDICATOR;
                            _focusUpdate();
                        }
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.UP :
                    if(focusBtnType === BTN_CLOSE || focusBtnType === BTN_CHECK) {
                        if (isShowIndicator) {
                            focusBtnType = BTN_INDICATOR;
                            _focusUpdate();
                        }
                    } else if (isShowIndicator === true && focusBtnType === BTN_INDICATOR) {
                        focusBtnType = BTN_CLOSE;
                        _focusUpdate();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT :
                    if(focusBtnType === BTN_INDICATOR) {
                        popup_container_div.find("#img_indicator_" + imgFocusIndex).attr("src" , resourcePath + "img_paging.png");

                        imgFocusIndex--;
                        if(imgFocusIndex<0) {
                            imgFocusIndex = introCount-1;
                        }
                        popup_container_div.find("#notice_img").attr("src" , showImageList[imgFocusIndex]);
                        popup_container_div.find("#popup_title").text(showImageTitleList[imgFocusIndex]);
                        popup_container_div.find("#img_indicator_" + imgFocusIndex).attr("src" , resourcePath + "img_paging_f.png");
                        //consumed = true;
                    }else {
                        if(focusBtnType === BTN_CLOSE) {
                            if(isShowCheckBtn === true) {
                                focusBtnType = BTN_CHECK;
                                _focusUpdate();
                            }
                        }
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.RIGHT :
                    if(focusBtnType === BTN_INDICATOR) {
                        popup_container_div.find("#img_indicator_" + imgFocusIndex).attr("src" , resourcePath + "img_paging.png");

                        imgFocusIndex++;
                        if(imgFocusIndex>=introCount) {
                            imgFocusIndex = 0;
                        }

                        popup_container_div.find("#notice_img").attr("src" , showImageList[imgFocusIndex]);
                        popup_container_div.find("#popup_title").text(showImageTitleList[imgFocusIndex]);
                        popup_container_div.find("#img_indicator_" + imgFocusIndex).attr("src" , resourcePath + "img_paging_f.png");
                        //consumed = true;
                    }else {
                        if(focusBtnType === BTN_CHECK) {
                            if(isShowCloseBtn === true) {
                                focusBtnType = BTN_CLOSE;
                                _focusUpdate();
                            }
                        }
                    }
                    consumed = true;

                    break;
                case KTW_KEY_CODE.BACK :
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    if(focusBtnType === BTN_CLOSE) {
                        if(callbackFunc !== null && isChecked === true) {
                            callbackFunc(isChecked);
                        }
                        
                        LayerManager.deactivateLayer({
                            id: NOTICE_POPUP
                        });
                    }else if(focusBtnType === BTN_CHECK) {
                        if(isChecked === true) {
                            isChecked = false;
                        }else {
                            isChecked = true;
                        }
                    }
                    focusBtnType = BTN_CLOSE;
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.EXIT:
                    // if(callbackFunc !== null) {
                    //     callbackFunc();
                    // }
                    break;
            }

            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title",
                    class: "font_b" ,
                    css: {
                        position: "absolute", left: 0, top: 133, width: KTW_ENV.RESOLUTION.WIDTH , height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33,"text-align" : "center","letter-spacing":-1.65
                    }
                },
                text: "",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "notice_img",
                    src: "",
                    css: {
                        position: "absolute",
                        left: 296,
                        top: (163+19),
                        width: 1328,
                        height: 648
                    }
                },
                parent: popup_container_div
            });


            if(introCount>1) {
                var startLeft = (KTW_ENV.RESOLUTION.WIDTH - ((11*introCount) + (20*(introCount-1)))) / 2;

                for(var i=0;i<introCount;i++) {
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "img_indicator_"+i,
                            src: resourcePath + "img_paging.png",
                            css: {
                                position: "absolute",
                                left: startLeft,
                                top: (163 + 648 + 10 + 19),
                                width: 11,
                                height: 11
                            }
                        },
                        parent: popup_container_div
                    });
                    startLeft += 31;
                }
            }



            var btnDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_2",
                    css: {
                        position: "absolute",
                        left: 296, top:(830+19), width:280 , height: 62
                    }
                },
                parent: div
            });

            var defaultImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "default_btn",
                    src: resourcePath + "pop_btn_w280.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 280,
                        height: 62
                    }
                },
                parent: btnDiv2
            });


            var btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_2_focus" ,
                    class : "focus_box_red_line",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:280 ,  height: 62,
                        "background-color": "rgba(255,255,255,1)",
                        display: "none"
                    }
                },
                parent: btnDiv2
            });


            var btnCheckedImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "btn_2_checked_img" ,
                    src: resourcePath + "pop_cb_dim_uncheck.png",
                    css: {
                        position: "absolute",
                        left: 18 ,
                        top: 12,
                        width: 48,
                        height: 40,
                    }
                },
                parent: btnDiv2
            });

            var btnDefaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_2_default_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 69 , top: 18 , width: 280 - 69, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: btnDiv2
            });

            var btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_2_focus_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 69 , top: 18 , width: 280 - 69, height: 29,
                        color: "rgba(73, 68, 60, 1)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: btnDiv2
            });

            var btn = {
                btn_root_div : btnDiv2,
                btn_default_img : defaultImg,
                btn_focus_div : btnFocusDiv,
                btn_checked_img : btnCheckedImg,
                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            };
            btnCheckbox = btn;


            var btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_div",
                    css: {
                        position: "absolute", left: 296+1048, top: (830+19), width: 280, height: 62 , display : "none"
                    }
                },
                parent: popup_container_div
            });

            var defaultBtn = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "default_btn",
                    src: resourcePath + "pop_btn_w280.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 280,
                        height: 62
                    }
                },
                parent: btnDiv
            });

            var focusBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 280, height: 62,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnDiv
            });

            var defaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center","letter-spacing":-1.5
                    }
                },
                text: "닫기",
                parent: btnDiv
            });

            var focusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center","letter-spacing":-1.5
                    }
                },
                text: "닫기",
                parent: btnDiv
            });

            var btnobj = {
                root_div : btnDiv,
                default_btn : defaultBtn,
                focus_btn : focusBtn,
                default_text : defaultText,
                focus_text : focusText
            };


            closeBtn = btnobj;

        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");


            if(isShowCloseBtn === true) {
                closeBtn.default_btn.css("display" , "");
                closeBtn.focus_btn.css("display" , "none");
                closeBtn.default_text.css("display" , "");
                closeBtn.focus_text.css("display" , "none");
            }


            if(isShowCheckBtn === true) {
                btnCheckbox.btn_focus_text.css("display" , "none");
                btnCheckbox.btn_focus_div.css("display" , "none");
                btnCheckbox.btn_default_text.css("display" , "");
                btnCheckbox.btn_default_img.css("display" , "");

                if(isChecked === true) {
                    btnCheckbox.btn_checked_img.attr("src", resourcePath + "pop_cb_dim_check_red.png");
                }else {
                    btnCheckbox.btn_checked_img.attr("src", resourcePath + "pop_cb_dim_uncheck.png");
                }
            }

            if(focusBtnType === BTN_CLOSE) {
                closeBtn.default_btn.css("display" , "none");
                closeBtn.focus_btn.css("display" , "");
                closeBtn.default_text.css("display" , "none");
                closeBtn.focus_text.css("display" , "");
            }else if(focusBtnType === BTN_CHECK) {
                btnCheckbox.btn_focus_text.css("display" , "");
                btnCheckbox.btn_focus_div.css("display" , "");
                btnCheckbox.btn_default_text.css("display" , "none");
                btnCheckbox.btn_default_img.css("display" , "none");

                if(isChecked === true) {
                    btnCheckbox.btn_checked_img.attr("src", resourcePath + "pop_cb_foc_check_red.png");
                }else {
                    btnCheckbox.btn_checked_img.attr("src", resourcePath + "pop_cb_foc_uncheck.png");
                }
            }
        }


        function _showView() {
            log.printDbg("_showView()");
            imgFocusIndex = 0;
            popup_container_div.find("#notice_img").attr("src" , showImageList[imgFocusIndex]);
            popup_container_div.find("#popup_title").text(showImageTitleList[imgFocusIndex]);
            popup_container_div.find("#img_indicator_0").attr("src" , resourcePath + "img_paging_f.png");

            focusBtnType = BTN_CLOSE;
            closeBtn.root_div.css("display" , "");
            isShowCloseBtn = true;

            if(introCount > 1) {
                isShowIndicator = true;
                focusBtnType = BTN_INDICATOR;
            }

            if(blockPeroid === 0) {
                btnCheckbox.btn_default_text.text("더 이상 보지 않기");
                btnCheckbox.btn_focus_text.text("더 이상 보지 않기");
                isShowCheckBtn = true;
            }else if(blockPeroid === 1) {
                btnCheckbox.btn_default_text.text("오늘 그만 보기");
                btnCheckbox.btn_focus_text.text("오늘 그만 보기");
                isShowCheckBtn = true;
            }else if(blockPeroid === 7) {
                btnCheckbox.btn_default_text.text("7일간 보지 않기");
                btnCheckbox.btn_focus_text.text("7일간 보지 않기");
                isShowCheckBtn = true;
            }

            if(isShowCheckBtn === false) {
                btnCheckbox.btn_root_div.css("display" , "none");
            }
            _focusUpdate();
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

    };

    noticePopup.prototype = new Layer();
    noticePopup.prototype.constructor = noticePopup;

    noticePopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    noticePopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };
    noticePopup.prototype.hide = function() {
        this.hideView();
    };
    noticePopup.prototype.destroy = function() {
        this.destroyView();
    };
    noticePopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[NOTICE_POPUP] = noticePopup;

})();