/**
 * Created by ksk91_000 on 2016-06-27.
 */

window.ServerCodeAdapter = new function () {
    this.getPrInfo = function(prInfo) {
        switch(prInfo) {
            case 0: return "all";
            case 7: return 7;
            case 12: return 12;
            case 15: return 15;
            case 19: return 19;
        }
    }

    this.getResTypeToName = function (resType) {
        var name = '';
        if (resType === '1') {// 제목검색
            name = '\'제목\'';
        } else if (resType === '2') {// 시맨틱검색
            name = '\'관련정보\'';
        } else if (resType === '3') {// 테마검색
            name = '\'테마\'';
        }
        return name;
    }
};