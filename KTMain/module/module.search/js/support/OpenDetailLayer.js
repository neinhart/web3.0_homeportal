/**
 * Created by ksk91_000 on 2016-10-31.
 */
window.openDetailLayer = function(cateId, contsId, reqPathCd, contentsData) {
    if(contentsData && contentsData.itemType==1) cateId = contsId;
    var a = ModuleManager.getModule("module.vod");
    if(!a) {
        ModuleManager.loadModule( {
            moduleId: "module.vod",
            cbLoadModule: function (success) {
                if (success) openDetailLayer(cateId, contsId, reqPathCd, contentsData);
            }
        }); return;
    }else if(contentsData) a.execute({
        method: "showDetailWithData",
        params: {
            cat_id : cateId,
            const_id : contsId,
            req_cd :reqPathCd,
            cateInfo: contentsData,
            contsInfo: contentsData,
        }
    });else a.execute({
        method: "showDetail",
        params: {
            cat_id : cateId,
            const_id : contsId,
            req_cd :reqPathCd
        }
    });
};