/**
 * Created by Yun on 2016-12-08.
 */


(function () {
    searchMenu.layer = searchMenu.layer || {};
    searchMenu.layer.search_result = function SearchResult(options) {
        Layer.call(this, options);
        var INSTANCE = this;
        var compList = [];
        var menuFocusIdx = 0;

        var cateData = [];
        var viewMgr = new searchMenu.ViewManager();

        var menuList = [{title: "VOD", count: 0}, {title:"TV앱", count: 0}, {title:"Daum검색", count: 0}, {title:"연관검색어", count: 0}, {title:"TV프로그램", count: 0}, {title:"가입한 월정액", count: 0}];
        var result_Data;
        var search_Words;
        var req_cd;

        var RESULT_STATE = {SUCCESS: 0, FAIL: 1};
        var mode = RESULT_STATE.SUCCESS;

        var nothingFocusIdx = 0;
        var listOrder = ["VOD", "Package", "TVPro", "TVApp", "RelWord", "Daum"];

        var totalResult_Cnt = 0;
        var spellResult_Cnt = 0;
        var vodTotal_cnt = 0;
        var vodPkgTotal_cnt = 0;
        var epgTotal_Cnt = 0;
        var vodData_List = [];
        var vodPkgData_List = [];
        var tvAppData_List = [];
        var relData_List = [];
        var tvProData_List = [];
        var filteringData_List = [];
        var pkgFilteringData_List = [];
        var spellData_List = [];
        var tmpKey = ["VOD", "VODPkg", "TVApp", "TVPro", "RelWord"];
        var searchResultCnt = {VOD:["", 0], VODPkg:["", 0], TVApp:["", 0], TVPro:["", 0], RelWord:["", 0], DAUM:["", 0]};

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame searchResult search"});

            result_Data = this.getParams().search_Data;
            search_Words = this.getParams().search_Words;
            req_cd = this.getParams().req_cd;

            settingResultData();

            this.div.html("<div id='background'>" +
                "<div id='defaultBG' alt='background'>" +
                "<img id='bg_menu_dim_up'>" +
                "<img id='bg_menu_dim_down'>" +
                "</div>" +
                "<div id='searchResult_page_noti_area'>" +
                "<img class='searchResult_pageUp_img' src='" + modulePath + "resource/icon_pageup.png'/>" +
                "<img class='searchResult_pageDown_img' src='" + modulePath + "resource/icon_pagedown.png'/>" +
                "<span class='searchResult_pageText'>페이지</span>" +
                "</div>" +
                "<div id='searchResult_filter_noti_area'>" +
                "<img class='searchResult_filter_icon' src='module/module.search/resource/icon_option_related.png'>" +
                "<span class='searchResult_filter_text'>검색 필터 설정</span>" +
                "</div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>");

            createComponent();
            // menuFocusing(0);
            if (cbCreate) {
                cbCreate(true);
            }
        };

        function settingResultData() {
            SearchManager.setResetFilterData();
            vodData_List = [];
            vodPkgData_List = [];
            tvProData_List = [];
            relData_List = [];
            for (var idx = 0 ; idx < tmpKey.length; idx ++) {
                SearchManager.setSearchResultCount(tmpKey[idx], "(0)", 0);
            }

            totalResult_Cnt = parseInt(_$(result_Data).find("output").find("totalNum").eq(0).text());
            spellResult_Cnt = _$(result_Data).find("output").find("spellNum").eq(0).text();
            vodTotal_cnt = parseInt(_$(result_Data).find("search[code='VOD']").find("totalNum").text());
            epgTotal_Cnt = parseInt(_$(result_Data).find("search[code='EPG']").find("totalNum").text());
            vodPkgTotal_cnt = parseInt(_$(result_Data).find("search[code='PRODUCT_VOD']").find("totalNum").text());
            var tmpData;

            // 월정액 가입상품 리스트 정렬
            // 8월 검색서버 월정액에 대한 패치가 적용되면 OTV/OTS 모두 결과 보여주도록 수정 및 테스트 필요
            // 패치 전에는 OTS만 가입한 월정액 출력하도록 적용
            // if (window.CONSTANT.IS_OTS) {
                tmpData = _$(result_Data).find("search[code='PRODUCT_VOD']").find("PROD_VOD_ITEM");
                tmpData.each(function () {
                    vodPkgData_List.push({
                        title: _$(this).find("TITLE").text(),
                        imgURL: _$(this).find("URL").text(),
                        cate_Name: _$(this).find("CATEGORY_PULLNAME").text(),
                        cat_Id: _$(this).find("CATEGORY_ID").text(),
                        const_Id: _$(this).find("CONST_ID").text(),
                        seriesYN: _$(this).find("SERIES_YN").text(),
                        wonYn: _$(this).find("WON_YN").text(),
                        prInfo: _$(this).find("RATING").text(),
                        director: _$(this).find("DIRECTOR").text(),
                        actor: _$(this).find("ACTOR").text(),
                        ollehp: _$(this).find("OLLEHP").text(),
                        runTime: _$(this).find("RUNTIME").text(),
                        sdHd: _$(this).find("HD_SD").text(),
                        hdrYn: _$(this).find("HDR_YN").text(),
                        smartDvdYn: _$(this).find("SMART_DVD_YN").text(),
                        // [hw.boo] RES_TYPE 검색결과 유형 추가 (1 : 제목검색, 2 : 시맨틱검색, 3 : 테마검색)
                        resType: _$(this).find("RES_TYPE").text()
                    });
                });

                tmpData = _$(result_Data).find("search[code='PRODUCT_VOD']").find("KEYWORD_LIST").find("KEYWORD_ITEM");

                tmpData.each(function () {
                    pkgFilteringData_List.push({
                        keyType: _$(this).find("KEYWORD_TYPE").text(),
                        keyWord: _$(this).find("WORD").text(),
                        srchOpt: _$(this).find("SRCH_OPT").text(),
                        isCheck: _$(this).find("IS_CHECKED").text()
                    });
                });
            // } else {
            //     if (isNaN(vodPkgTotal_cnt)) vodPkgTotal_cnt = 0;
            //     totalResult_Cnt -= vodPkgTotal_cnt;
            // }
            // 일반 통합검색 리스트 정렬

            tmpData = _$(result_Data).find("search").find("KEYWORD_LIST").find("KEYWORD_ITEM");

            tmpData.each(function () {
                filteringData_List.push({keyType:_$(this).find("KEYWORD_TYPE").text(),
                keyWord:_$(this).find("WORD").text(),
                srchOpt:_$(this).find("SRCH_OPT").text(),
                isCheck:_$(this).find("IS_CHECKED").text()});
            });

            tmpData = _$(result_Data).find("search").find("VOD_ITEM");
            tmpData.each(function () {
                vodData_List.push({title:_$(this).find("TITLE").text(),
                    imgURL:_$(this).find("URL").text(),
                    cate_Name:_$(this).find("CATEGORY_PULLNAME").text(),
                    cat_Id:_$(this).find("CATEGORY_ID").text(),
                    const_Id:_$(this).find("CONST_ID").text(),
                    seriesYN:_$(this).find("SERIES_YN").text(),
                    wonYn:_$(this).find("WON_YN").text(),
                    prInfo:_$(this).find("RATING").text(),
                    director:_$(this).find("DIRECTOR").text(),
                    actor:_$(this).find("ACTOR").text(),
                    ollehp:_$(this).find("OLLEHP").text(),
                    runTime:_$(this).find("RUNTIME").text(),
                    sdHd:_$(this).find("HD_SD").text(),
                    hdrYn:_$(this).find("HDR_YN").text(),
                    smartDvdYn:_$(this).find("SMART_DVD_YN").text(),
                    // [hw.boo] RES_TYPE 검색결과 유형 추가 (1 : 제목검색, 2 : 시맨틱검색, 3 : 테마검색)
                    resType: _$(this).find("RES_TYPE").text()
                });
            });

            tmpData = _$(result_Data).find("search").find("APP_ITEM");

            tmpData.each(function () {
                tvAppData_List.push({appName:_$(this).find("APP_NAME").text(),
                    imgURL:_$(this).find("URL").text(),
                    appType:_$(this).find("APP_TYPE_CD").text(),
                    launchYN:_$(this).find("LAUNCH_YN").text(),
                    cat_Id:_$(this).find("CATEGORY_ID").text(),
                    cid:_$(this).find("CID").text(),
                    assetId:_$(this).find("ASSET_ID").text(),
                    pkgInitPath:_$(this).find("PKG_INIT_PATH").text()});
            });

            tmpData = _$(result_Data).find("search").find("EPG_ITEM");

            tmpData.each(function () {
                tvProData_List.push({chID:_$(this).find("SERVICE_ID").text(),
                    chName:_$(this).find("CHNL_NAME").text(),
                    proName:_$(this).find("PRO_NAME").text(),
                    broadDate:_$(this).find("BROAD_DATE").text(),
                    broTime:_$(this).find("BRO_TIME").text(),
                    endTime:_$(this).find("PRO_ENDTIME").text(),
                    durationTime:_$(this).find("DURATION").text(),
                    prInfo:_$(this).find("AGE_GRD").text(),
                    proKind:_$(this).find("PRODUCT_KIND").text(),
                    chNum:_$(this).find("STB_SERVICE_ID").text()});
            });
            tmpData = _$(result_Data).find("search").find("RELATED_WORDS");

            tmpData.each(function () {
                relData_List.push({sWords:_$(this).find("SEARCH_WORD").text(),
                    searchQry:_$(this).find("SEARCH_QUERY").text()});
            });

            if (relData_List.length > 0) totalResult_Cnt += relData_List.length;

            menuList[0].count = vodData_List.length;
            menuList[1].count = tvAppData_List.length;
            menuList[3].count = relData_List.length;
            menuList[4].count = tvProData_List.length;
            menuList[5].count = vodPkgData_List.length;

            tmpData = _$(result_Data).find("SPELL_EXPAND");

            if (parseInt(spellResult_Cnt) >= 0 && totalResult_Cnt == 0) {
                tmpData.each(function () {
                    spellData_List.push({searchWord:_$(this).find("SEARCH_WORD").text(),
                        wordType:_$(this).find("WORD_TYPE").text()});
                });
                mode = RESULT_STATE.FAIL;
            } else {
                mode = RESULT_STATE.SUCCESS;
            }
        }

        this.show = function (options) {
            Layer.prototype.show.call(this);
            if (options && options.resume) {
                // for (var idx = 0 ; idx < tmpKey.length; idx ++) {
                //     SearchManager.setSearchResultCount(tmpKey[idx], searchResultCnt[tmpKey[idx]][0], searchResultCnt[tmpKey[idx]][1]);
                // }
                viewMgr.resume();
            }
            resizeScreen(true);
            if (mode == RESULT_STATE.FAIL) {
                for (var idx = 0; idx < (spellData_List.length > 5 ? 5 : spellData_List.length); idx++) {
                    INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell").eq(idx).find(".searchResult_spellList_focusIcon").css("left", (236 - (INSTANCE.div.find("#contents .searchResult_spellList_area .searchResult_spellList_cell:eq(" + idx + ") .searchResult_spellList_text").width() / 2)) + "px");
                }
                INSTANCE.div.find("#contents .searchResult_spellList_div").css("margin-top", 347 - (INSTANCE.div.find("#contents .searchResult_spellList_div").height()/2) + "px");
                if (spellData_List.length == 0) {
                    INSTANCE.div.find("#contents .searchResult_spellList_div").css({"margin-top": "241px","margin-left": "-79px"});
                }
                nothingFocusIdx = 0;
                nothingResultListFocus(nothingFocusIdx);
            }
            LayerManager.stopLoading();
        };

        this.hide = function (options) {
            Layer.prototype.hide.call(this);
            if (options && options.pause) viewMgr.pause();
        };


        function searchResultSuccess(key_code) {
            if (compList[menuFocusIdx].getIsFocus()) {
                var keyAction = compList[menuFocusIdx].onKeyAction(key_code);
                if (keyAction.prevMenu) {
                    INSTANCE.div.find("#searchResult_filter_noti_area").removeClass("show");
                    INSTANCE.div.find("#searchResult_Menu .searchList_focus").removeClass("hideBorder");
                    INSTANCE.div.find("#searchResult_Menu #resultMenuFocusArrow_img").removeClass("hide");
                    compList[menuFocusIdx].setUnFocus();
                    viewMgr.setFocus();
                }
                return keyAction.isUsed;
            } else {
                switch (key_code) {
                    case KEY_CODE.UP:
                        menuFocusing(HTool.getIndex(menuFocusIdx, -1, compList.length));
                        return viewMgr.onKeyAction(key_code);
                    case KEY_CODE.DOWN:
                        menuFocusing(HTool.getIndex(menuFocusIdx, 1, compList.length));
                        return viewMgr.onKeyAction(key_code);
                    case KEY_CODE.LEFT:
                        LayerManager.historyBack();
                        return true;
                    case KEY_CODE.RIGHT:
                    case KEY_CODE.ENTER:
                        if (compList[menuFocusIdx].getIsRelationButton()) INSTANCE.div.find("#searchResult_filter_noti_area").addClass("show");
                        compList[menuFocusIdx].setFocus();
                        INSTANCE.div.find("#searchResult_Menu .searchList_focus").addClass("hideBorder");
                        INSTANCE.div.find("#searchResult_Menu #resultMenuFocusArrow_img").addClass("hide");
                        return viewMgr.onKeyAction(key_code);
                    default:
                        return false;
                }
            }
        }

        function setResetCategoryListener () {
            console.debug(compList);
            for(var idx = 0 ; idx < compList.length ; idx ++) {
                if (compList[idx].getListType() == "Package") {
                    compList.splice(idx, 1);
                    break;
                }
            }
            console.debug(compList);
            viewMgr.setResetCategory();
        }

        function nothingResultListFocus(index) {
            INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell").removeClass("focus");
            INSTANCE.div.find("#contents .searchResult_spellList_div .spellList_btn").removeClass("focus");
            if (index == spellData_List.length) {
                INSTANCE.div.find("#contents .searchResult_spellList_div .spellList_btn").addClass("focus");
            } else {
                INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell:eq(" + index + ")").addClass("focus");
            }
        }

        function searchResultFail(key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    nothingResultListFocus(nothingFocusIdx = HTool.getIndex(nothingFocusIdx, -1, spellData_List.length+1));
                    return true;
                case KEY_CODE.DOWN:
                    nothingResultListFocus(nothingFocusIdx = HTool.getIndex(nothingFocusIdx, 1, spellData_List.length+1));
                    return true;
                case KEY_CODE.ENTER:
                    if (nothingFocusIdx == spellData_List.length) {
                        AppServiceManager.changeService({
                            nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                            obj: {
                                type: CONSTANT.APP_TYPE.WEB_BROWSER,
                                param: "http://search.daum.net/search?w=tot&rtupcoll=NNS&nil_ch=32&q=" + search_Words + "&DA=P003"
                            }
                        });
                    } else {
                        var searchWord = spellData_List[nothingFocusIdx].searchWord;
                        SearchManager.searchTOTAL(function(data) {
                            var layers = LayerManager.getLayers("SearchResult");
                            LayerManager.activateLayer({
                                obj: {
                                    id: "SearchResult",
                                    type: Layer.TYPE.NORMAL,
                                    priority: Layer.PRIORITY.NORMAL,
                                    params: {
                                        search_Data: data,
                                        search_Words: searchWord,
                                        req_cd: (req_cd ? req_cd : "04")
                                    }
                                },
                                moduleId: "module.search",
                                visible: true,
                                new: true
                            });
                            for (var i = 0; i < layers.length; i++) {
                                LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
                            }
                        }, function() {
                            LayerManager.activateLayer({
                                obj: {
                                    id: "SearchError",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        title: "오류",
                                        message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                                        button: "닫기",
                                        reboot: false
                                        // callback: _error
                                    }
                                },
                                moduleId: "module.search",
                                visible: true
                            });
                        }, true, "0xFF", 3, false, 0, 30, "0x09", searchWord, "", "POINT", 2, "Y", "", "0.5", "");

                    }
                    return true;
                default:
                    return false;
            }
        }

        this.controlKey = function (key_code) {
            if (mode == RESULT_STATE.SUCCESS) return searchResultSuccess(key_code);
            else if (mode == RESULT_STATE.FAIL) return searchResultFail(key_code);
        };

        function menuFocusing(index) {
            menuFocusIdx = index;
            for (var idx = 0 ; idx < compList.length; idx ++) {
                if (idx == menuFocusIdx) {
                    compList[idx].show();
                } else {
                    compList[idx].hide();
                }
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='resultTitle_area'>" +
                "<div id='resultTitle_text'>'" + search_Words + "' 검색결과" + (parseInt(totalResult_Cnt) == 0 ? "가 없습니다" : "") + "</div>" +
                ((parseInt(spellResult_Cnt) >= 0 && totalResult_Cnt == 0) ? ("<div id='resultSubTitle_text'>아래 검색어로 검색하려고 하셨나요?</div>"):("<div id='resultSubTitle_text'>총 " + totalResult_Cnt + "개의 콘텐츠가 검색 되었습니다") + "</div>") +
                "</div>" +
                "<div id='resultInfo_area'>" +
                "</div>");
            if (parseInt(spellResult_Cnt) >= 0 && totalResult_Cnt == 0) {
                INSTANCE.div.find("#contents").append(_$("<div/>", {class:"searchResult_spellList_area"}));
                INSTANCE.div.find("#contents .searchResult_spellList_area").append(_$("<div/>", {class:"searchResult_spellList_div"}));
                var length = spellData_List.length > 5 ? 5 : spellData_List.length;
                for (var idx = 0 ; idx < length ; idx ++) {
                    INSTANCE.div.find("#contents .searchResult_spellList_div").append(_$("<div/>", {class:"searchResult_spellList_cell"}));
                    INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell").eq(idx);
                    INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell").eq(idx).append(_$("<div/>", {class:"searchResult_spellList_focusIcon"}));
                    INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell").eq(idx).append(_$("<div/>", {class:"searchResult_spellList_text"}).text(spellData_List[idx].searchWord));
                    if (idx < length - 1) {
                        INSTANCE.div.find("#contents .searchResult_spellList_div .searchResult_spellList_cell").eq(idx).append(_$("<div/>", {class:"searchResult_spellList_divide"}));
                    }
                }

                INSTANCE.div.find("#contents .searchResult_spellList_div").append(_$("<div/>", {class:"spellList_btn"}));
                INSTANCE.div.find("#contents .searchResult_spellList_div .spellList_btn").append(_$("<div/>", {class:"spellList_btn_BG"}));
                INSTANCE.div.find("#contents .searchResult_spellList_div .spellList_btn").append(_$("<span/>", {class:"spellList_btn_text"}).text("Daum검색"));
            } else {
                viewMgr.setParent(INSTANCE.div.find("#contents"));
                if (vodData_List.length > 0) {
                    cateData.push(new ViewData("VODSearchResult", searchMenu.view.SingleCompView, {}, "VOD", "("+vodTotal_cnt+")", "VOD"));
                    addComponent(new SearchList_VOD(INSTANCE, setResetCategoryListener), vodData_List, filteringData_List, search_Words, vodTotal_cnt);
                    SearchManager.setSearchResultCount("VOD", "("+vodTotal_cnt+")", vodTotal_cnt);
                    searchResultCnt["VOD"][0] = "("+vodTotal_cnt+")";
                    searchResultCnt["VOD"][1] = vodTotal_cnt;
                }
                if (vodPkgData_List.length > 0) {
                    cateData.push(new ViewData("VODSearchResultPackage", searchMenu.view.SingleCompView, {}, "가입한 월정액", "("+vodPkgTotal_cnt+")", "VODPkg"));
                    addComponent(new SearchList_PackageVOD(INSTANCE), vodPkgData_List, filteringData_List, search_Words, vodPkgTotal_cnt);
                    SearchManager.setSearchResultCount("VODPkg", "("+vodPkgTotal_cnt+")", vodPkgTotal_cnt);
                    searchResultCnt["VODPkg"][0] = "("+vodPkgTotal_cnt+")";
                    searchResultCnt["VODPkg"][1] = vodPkgTotal_cnt;
                }
                if (tvAppData_List.length > 0) {
                    cateData.push(new ViewData("TVAppSearchResult", searchMenu.view.SingleCompView, {}, "TV앱", "("+tvAppData_List.length+")", "TVApp"));
                    addComponent(new SearchList_TVApp(INSTANCE), tvAppData_List, "", search_Words);
                    SearchManager.setSearchResultCount("TVApp", "("+tvAppData_List.length+")", tvAppData_List.length);
                    searchResultCnt["TVApp"][0] = "("+tvAppData_List.length+")";
                    searchResultCnt["TVApp"][1] = tvAppData_List.length;
                }
                if (relData_List.length > 0) {
                    cateData.push(new ViewData("RelationSearchResult", searchMenu.view.SingleCompView, {}, "연관검색어", "("+relData_List.length+")", "RelWord"));
                    addComponent(new SearchList_Relation(INSTANCE), relData_List, "", search_Words);
                    SearchManager.setSearchResultCount("RelWord", "("+relData_List.length+")", relData_List.length);
                    searchResultCnt["RelWord"][0] = "("+relData_List.length+")";
                    searchResultCnt["RelWord"][1] = relData_List.length;
                }
                if (tvProData_List.length > 0) {
                    cateData.push(new ViewData("TVProgramSearchResult", searchMenu.view.SingleCompView, {}, "TV프로그램", "("+epgTotal_Cnt+")", "TVPro"));
                    addComponent(new SearchList_TVProgram(INSTANCE), tvProData_List, "", search_Words, epgTotal_Cnt);
                    SearchManager.setSearchResultCount("TVPro", "("+epgTotal_Cnt+")", epgTotal_Cnt);
                    searchResultCnt["TVPro"][0] = "("+epgTotal_Cnt+")";
                    searchResultCnt["TVPro"][1] = epgTotal_Cnt;
                }
                cateData.push(new ViewData("DAUMSearchResult", searchMenu.view.SingleCompView, {}, "Daum검색", "", "DAUM"));
                addComponent(new SearchList_Daum(INSTANCE), search_Words, "", search_Words);
                viewMgr.setData(new CategoryViewData("searchResultList", cateData, "검색결과"));
                INSTANCE.div.append(viewMgr.getView());

                for (var idx = 0 ; idx < compList.length; idx ++) {
                    compList[idx].setReqPath(req_cd ? req_cd : "04");
                }
                var tmpFlag = false;
                for (var idx = 0 ; idx < listOrder.length ; idx ++) {
                    for (var z = 0 ; z < compList.length ; z ++) {
                        if (listOrder[idx] == compList[z].getListType()) {
                            menuFocusIdx = z;
                            tmpFlag = true;
                            break;
                        }
                    }
                    if (tmpFlag) break;
                }
                viewMgr.changeCategoryFocus(menuFocusIdx);
                menuFocusing(menuFocusIdx);
            }
        }

        function addComponent(component, _data, filterData, _searchWord, totalCount) {
            INSTANCE.div.find("#contents").append(component.getView());
            component.setData(_data, filterData, _searchWord, totalCount);
            compList.push(component);
        }
    };

    searchMenu.layer.search_result.prototype = new Layer();
    searchMenu.layer.search_result.prototype.constructor = searchMenu.layer.search_result;

    searchMenu.layer.search_result.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    searchMenu.layer.search_result.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["SearchResult"] = searchMenu.layer.search_result;
})();