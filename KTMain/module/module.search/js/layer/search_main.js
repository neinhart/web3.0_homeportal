/**
 * Created by Yun on 2016-11-30.
 */
/**
 *  이미지 누락
 *  'ㄸ' 이미지 없음
 */

(function () {
    searchMenu.layer = searchMenu.layer || {};
    searchMenu.layer.search_main = function SearchMain(options) {
        Layer.call(this, options);
        var INSTANCE = this;

        var ime = new IMEModule();

        var focusX = 0;
        var focusY = 3;
        var doubleFocus = 0;
        var BTN_LEN = 0;
        var inputMaxLen = 15;

        var currentTextArray = [];
        var inputLength;
        var focusPosX;
        var isResultCheck = false;
        var recomWord = "";

        var curComp = 0;
        var compList = [];
        var focusOSK = 0;
        var mainClock;
        var trendsWords = null;

        var arrKorCon = [["ㄱ", "ㄲ"], ["ㄴ"], ["ㄷ", "ㄸ"], ["ㄹ"], ["ㅁ"], ["ㅂ", "ㅃ"], ["ㅅ", "ㅆ"], ["ㅇ"], ["ㅈ", "ㅉ"], ["ㅊ"], ["ㅋ"], ["ㅌ"], ["ㅍ"], ["ㅎ"]];
        var arrKorVow = ["ㅏ", "ㅑ", "ㅓ", "ㅕ", "ㅗ", "ㅛ", "ㅜ", "ㅠ", "ㅡ", "ㅣ", "ㅐ", "ㅔ", "ㅒ", "ㅖ"];
        var arrEng_Cap = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
        var arrEng = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
        var arrNum = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
        var arrSpcText = ["?", "!", "%", "^", ":", ";", "~", "♥", "(", ")"];
        var chkCho = ["ㄳ","ㄵ","ㄶ","ㄺ","ㄻ","ㄼ","ㄽ","ㄾ","ㄿ","ㅀ","ㅄ"];
        var speechAdapter = null;
        var isSuccess = true;

        this.init = function (cbCreate) {
            isSuccess = true;
            this.div.attr({class: "arrange_frame searchMain search"});

            this.div.html("<div id='background'>" +
                "<img id='defaultBg' src='" + COMMON_IMAGE.BG_DEFAULT + "' alt='background' class='clipping'>" +
                "<div id='bottomMenu_Bg'></div>" +
                "<div id='bgCanvasContainer' class='clipping'></div>" +
                "<span id='backgroundTitle'>" + HTool.getMenuName(this.getParams()) + "</span>" +
                "<div id='pig_dim'></div>" +
                "<div class='clock_area'></div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>" +
                "<div id='commonArea'>" +
                "<div id='homeShot'>" +
                "</div>" +
                "</div>"
            );

            compList.push(new AutoSearchResult(INSTANCE));
            compList.push(new SearchBottomMenuList(INSTANCE));
            mainClock = new ClockComp({parentDiv:this.div.find(".clock_area")});

            trendsWords = SearchManager.getTrendsWordsData();
            if (!trendsWords || trendsWords == null) {
                LayerManager.startLoading({preventKey: true});
                SearchManager.getTrendWords(function(data) {
                    trendsWords = data;
                    SearchManager.setTrendsWordsData(trendsWords);
                    SearchManager.setTrendsWordsTime((new Date()).getTime());
                    LayerManager.stopLoading();
                    createInit(cbCreate);
                }, function() {
                    LayerManager.stopLoading();
                    LayerManager.activateLayer({
                        obj: {
                            id: "SearchError",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                title: "오류",
                                message: ["서비스가 일시적으로 원활하지 않습니다", "잠시 후 이용해 주세요"],
                                button: "닫기",
                                reboot: false
                                // callback: _error
                            }
                        },
                        moduleId: "module.search",
                        visible: true
                    });
                    isSuccess = false;
                    createInit(cbCreate);
                });
            } else {
                var cacheTime = (new Date()).getTime() - SearchManager.getTrendsWordsTime();
                if (cacheTime >= 3600000) {
                    LayerManager.startLoading({preventKey: true});
                    SearchManager.getTrendWords(function(data) {
                        trendsWords = data;
                        SearchManager.setTrendsWordsData(trendsWords);
                        SearchManager.setTrendsWordsTime((new Date()).getTime());
                        LayerManager.stopLoading();
                        createInit(cbCreate);
                    }, function() {
                        LayerManager.stopLoading();
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchError",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    title: "오류",
                                    message: ["서비스가 일시적으로 원활하지 않습니다", "잠시 후 이용해 주세요"],
                                    button: "닫기",
                                    reboot: false
                                    // callback: _error
                                }
                            },
                            moduleId: "module.search",
                            visible: true
                        });
                        isSuccess = false;
                        createInit(cbCreate);
                    });
                } else {
                    createInit(cbCreate);
                }
            }
        };

        function createInit(cbCreate) {
            if (isSuccess) {
                setComponent();

                isResultCheck = false;
                inputLength = 0;
                focusOSK = 0;
                focusX = 0;
                focusY = 3;
                buttonFocusRefresh(focusX, focusY);

                compList[0].setDataInit();
                compList[0].setUnFocus();
                compList[0].hide();
                compList[1].setUnFocus();
                compList[1].show();
            }
            if (cbCreate) {
                cbCreate(isSuccess);
            }
        }

        this.show = function (options) {
            resizeScreen(false);
            Layer.prototype.show.call(this);
            speechAdapter = window.oipfAdapter.speechRecognizerAdapter;
            speechAdapter.start();
            if (!options || !options.resume) {
                // focusResetting();
                // INSTANCE.div.find("#IMETextArea #searchSrc #searchFocus").offset({left: 138});
                compList[1].setRecentSearchWord(SearchManager.getAllRecentSearchWord());
            }
            if (options && options.resume) {
                if (compList[0].getIsFocus()) compList[0].resume();
                if (compList[1].getIsFocus()) compList[1].resume();
            }
            INSTANCE.div.find("#spanStrWidth").css("display", "block");
            INSTANCE.div.find("#spanStrWidth2").css("display", "block");
            speechAdapter.addSpeechRecognizerListener(speechInputText);
            mainClock.show(this.div.find(".clock_area"));
            input_Text = INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth");
        };

        this.hide = function (options) {
            if (options && options.pause) {
                if (compList[0].getIsFocus()) compList[0].pause();
                if (compList[1].getIsFocus()) compList[1].pause();
            }
            INSTANCE.div.find("#spanStrWidth").css("display", "none");
            INSTANCE.div.find("#spanStrWidth2").css("display", "none");
            mainClock.hide();
            compList[1].setRecentSearchWord(SearchManager.getAllRecentSearchWord());
            if (speechAdapter != null) {
                speechAdapter.stop();
                speechAdapter.removeSpeechRecognizerListener(speechInputText);
                speechAdapter = null;
            }
            Layer.prototype.hide.call(this);
        };

        function focusResetting() {
            isResultCheck = false;
            inputLength = 0;
            focusOSK = 0;
            focusX = 0;
            focusY = 3;
            sumTextStr = "";
            sumTmpArr = [];
            INSTANCE.div.find("#spanStrWidth2").text("검색어를 입력해주세요");
            INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text("");
            changeOSKBoardImage(focusOSK);
            buttonFocusRefresh(focusX, focusY);
            currentTextArray = [];
            INSTANCE.div.find("#IMETextArea #searchSrc #searchFocus").offset({left: 138});
            INSTANCE.div.find("#searchTop_area").removeClass("unFocus");
            sumTextString("");
            for (var idx = 0 ; idx < compList.length ; idx ++) {
                compList[idx].setUnFocus();
            }
        }

        function speechInputText(rec, input) {
            if (rec) {
                sumTextString(input);
                focusX = 0;
                focusY = 3;
                buttonFocusRefresh(focusX, focusY);
                for (var idx = 0 ; idx < compList.length ; idx ++) {
                    compList[idx].setUnFocus();
                }
                if (INSTANCE.div.find("#searchTop_area").hasClass("unFocus")) {
                    INSTANCE.div.find("#searchTop_area").removeClass("unFocus");
                }
            }
        }

        this.controlKey = function (key_code) {
            // KEY_CODE.DEL 지우기
            // KEY_CODE.CHANGE_CHAR 한영

            if (compList[curComp].getIsFocus()) {
                if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9) {
                    return false;
                }

                var keyAction = compList[curComp].onKeyAction(key_code);
                if (keyAction.prevMenu) {
                    buttonFocusRefresh(focusX, 3);
                    INSTANCE.div.find("#searchTop_area").removeClass("unFocus");
                    compList[curComp].setUnFocus();
                    if (keyAction.isPrevKey) {
                        focusResetting();
                    }
                }
                return keyAction.isUsed;
            } else {
                switch (key_code) {
                    case KEY_CODE.UP:
                        if (focusY == 3) {
                            if (focusOSK == 0) {
                                focusX = (focusX == 0 ? 0 : (focusX == 1 ? 5 : (focusX == 2 ? 8 : 11)));
                            } else if (focusOSK == 3) {
                                focusX = (focusX == 0 ? 0 : (focusX == 1 ? 3 : (focusX == 2 ? 6 : 9)));
                            } else {
                                focusX = (focusX == 0 ? 0 : (focusX == 1 ? 4 : (focusX == 2 ? 7 : 10)));
                            }
                        }
                        if (focusOSK == 0 && focusY == 1 && arrKorCon[focusX].length == 2) {
                            if (doubleFocus < 1) {
                                buttonDoubleFocusRefresh(focusX, doubleFocus);
                                doubleFocus++;
                            } else {
                                buttonDoubleFocusRefresh(focusX, doubleFocus);
                                if (focusY > 0) focusY --;
                                buttonFocusRefresh(focusX, focusY);
                                doubleFocus = 0;
                            }
                        } else {
                            if (focusY == 3 && INSTANCE.div.find("#spanStrWidth").text().length == 0) INSTANCE.div.find("#spanStrWidth2").text("초성검색 가능 (예: 홍길동 → ㅎㄱㄷ)");
                            if (focusY > 0) focusY --;
                            buttonFocusRefresh(focusX, focusY);
                        }
                        return true;
                    case KEY_CODE.DOWN:
                        if (focusY == 2) {
                            if (INSTANCE.div.find("#spanStrWidth").text().length == 0)INSTANCE.div.find("#spanStrWidth2").text("검색어를 입력해주세요");
                            if (focusOSK == 0) {
                                focusX = ((focusX >= 0 && focusX < 5) ? 0 : (focusX < 8 ? 1 : (focusX < 11 ? 2 : 3)));
                            } else if (focusOSK == 3) {
                                focusX = ((focusX >= 0 && focusX < 3)?0:(focusX < 6?1:(focusX < 9?2:3)));
                            } else {
                                focusX = ((focusX >= 0 && focusX < 4)?0:(focusX < 7?1:(focusX < 10?2:3)));
                            }
                        }
                        if (focusY == 3 && inputLength > 0 && isResultCheck) {
                            curComp = 0;
                            compList[0].setFocus();
                            INSTANCE.div.find("#thirdArea img").removeClass("focus");
                            INSTANCE.div.find("#searchTop_area").addClass("unFocus");
                        } else if (focusY == 3 && !isResultCheck){
                            curComp = 1;
                            compList[1].setFocus();
                            INSTANCE.div.find("#thirdArea img").removeClass("focus");
                            INSTANCE.div.find("#searchTop_area").addClass("unFocus");
                        } else if (focusY == 1 && arrKorCon[focusX].length == 2) {
                            if (doubleFocus > 0) {
                                buttonDoubleFocusRefresh(focusX, doubleFocus);
                                doubleFocus--;
                            } else {
                                buttonFocusRefresh(focusX, HTool.indexPlus(focusY, 4));
                            }
                        } else {
                            buttonFocusRefresh(focusX, HTool.indexPlus(focusY, 4));
                        }
                        return true;
                    case KEY_CODE.LEFT:
                        if (focusY == 0) {
                            deleteInputText();
                            ime.setText(INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text());
                        } else {
                            buttonFocusRefresh(HTool.getIndex(focusX, -1, BTN_LEN), focusY);
                        }
                        return true;
                    case KEY_CODE.RIGHT:
                        if (focusY == 0) {
                            sumTextString(" ");
                            ime.setText(INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text());
                        } else {
                            buttonFocusRefresh(HTool.getIndex(focusX, 1, BTN_LEN), focusY);
                        }
                        return true;
                    case KEY_CODE.ENTER:
                        enterKorKeyboard();
                        return true;
                    case KEY_CODE.PLAY:
                        gotoSearchResult();
                        return true;
                    case KEY_CODE.RED:
                    case KEY_CODE.DEL:
                        deleteInputText();
                        ime.setText(INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text());
                        return true;
                    case KEY_CODE.BLUE:
                        sumTextString(' ');
                        ime.setText(INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text());
                        return true;
                    case KEY_CODE.STAR:
                        if (focusY == 0) setTextIME(ime.onKeyDown(key_code));
                        else changeOSKBoardImage(HTool.getIndex(focusOSK, 1, 4));
                        return true;
                    case KEY_CODE.CHANGE_CHAR:
                        changeOSKBoardImage(HTool.getIndex(focusOSK, 1, 4));
                        return true;
                    case KEY_CODE.SHOP:
                        if (focusY == 0) setTextIME(ime.onKeyDown(key_code));
                        return true;
                    default:
                        if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9) {
                            if (focusY == 0) setTextIME(ime.onKeyDown(key_code));
                            return true;
                        }
                        return false;
                }
            }
        };

        function enterKorKeyboard() {
            switch (focusY) {
                case 0:
                    gotoSearchResult();
                    break;
                case 1:
                    // focusOSK  0 : 한글, 1: 영어 소문자, 2: 영어 대문자, 3: 특수문자
                    switch (focusOSK) {
                        case 0:
                            if (arrKorCon[focusX].length == 2) {
                                sumTextString(arrKorCon[focusX][doubleFocus]);
                            } else {
                                sumTextString(arrKorCon[focusX][0]);
                            }
                            break;
                        case 1:
                            sumTextString(arrEng[focusX]);
                            break;
                        case 2:
                            sumTextString(arrEng_Cap[focusX]);
                            break;
                        case 3:
                            sumTextString(arrNum[focusX]);
                            break;
                    }
                    break;
                case 2:
                    // focusOSK  0 : 한글, 1: 영어 소문자, 2: 영어 대문자, 3: 특수문자
                    switch (focusOSK) {
                        case 0:
                            sumTextString(arrKorVow[focusX]);
                            break;
                        case 1:
                            sumTextString(arrEng[focusX + 13]);
                            break;
                        case 2:
                            sumTextString(arrEng_Cap[focusX + 13]);
                            break;
                        case 3:
                            sumTextString(arrSpcText[focusX]);
                            break;
                    }
                    break;
                case 3:
                    switch (focusX) {
                        case 0:
                            gotoSearchResult();
                            break;
                        case 1:
                            deleteInputText();
                            break;
                        case 2:
                            sumTextString(' ');
                            break;
                        case 3:
                            changeOSKBoardImage(HTool.getIndex(focusOSK, 1, 4));
                            break;
                    }
                    break;
            }
        }

        function changeOSKBoardImage(index) {
            focusOSK = index;
            ime.setMode(focusOSK);
            switch (index) {
                case 0:
                    // 한글 OSK
                    BTN_LEN = arrKorCon.length;
                    INSTANCE.div.find("#oskBoard_img").attr("src", "module/module.search/resource/osk_home_kor_h.png");
                    break;
                case 1:
                    // 영문 소문자 OSK
                    BTN_LEN = 13;
                    INSTANCE.div.find("#oskBoard_img").attr("src", "module/module.search/resource/osk_home_eng_s_h.png");
                    break;
                case 2:
                    // 영문 대문자 OSK
                    BTN_LEN = 13;
                    INSTANCE.div.find("#oskBoard_img").attr("src", "module/module.search/resource/osk_home_eng_l_h.png");
                    break;
                case 3:
                    // 특수문자 OSK
                    BTN_LEN = arrNum.length;
                    INSTANCE.div.find("#oskBoard_img").attr("src", "module/module.search/resource/osk_home_num_h.png");
                    break;
            }
            if (focusX > BTN_LEN-1) focusX = BTN_LEN-1;
            buttonFocusRefresh(focusX, focusY);
        }

        function buttonDoubleFocusRefresh(index_X, index_Y) {
            if (index_Y == 0) {
                INSTANCE.div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").attr("src", "module/module.search/resource/key_kor_" + (index_X + 1) + "_2_h.png");
            } else {
                INSTANCE.div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").attr("src", "module/module.search/resource/key_kor_" + (index_X + 1) + "_1_h.png");
            }
        }

        function buttonFocusRefresh(index_X, index_Y) {
            focusY = index_Y;
            INSTANCE.div.find("#oskKorFocus_firstArea img").removeClass("focus");
            INSTANCE.div.find("#oskKorFocus_secondArea img").removeClass("focus");
            INSTANCE.div.find("#oskCapEngFocus_firstArea img").removeClass("focus");
            INSTANCE.div.find("#oskCapEngFocus_secondArea img").removeClass("focus");
            INSTANCE.div.find("#oskEngFocus_firstArea img").removeClass("focus");
            INSTANCE.div.find("#oskEngFocus_secondArea img").removeClass("focus");
            INSTANCE.div.find("#oskNumFocus_firstArea img").removeClass("focus");
            INSTANCE.div.find("#oskNumFocus_secondArea img").removeClass("focus");
            INSTANCE.div.find("#thirdArea img").removeClass("focus");
            INSTANCE.div.find("#searchFocus").removeClass("show");
            switch (index_Y) {
                case 0:
                    INSTANCE.div.find("#searchFocus").addClass("show");
                    ime.init(INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text(), focusOSK);
                    break;
                case 1:
                    // focusOSK  0 : 한글, 1: 영어 소문자, 2: 영어 대문자, 3: 특수문자
                    switch (focusOSK) {
                        case 0:
                            focusX = index_X;
                            BTN_LEN = arrKorCon.length;
                            if (arrKorCon[focusX].length == 2) {
                                doubleFocus = 0;
                                INSTANCE.div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").attr("src", "module/module.search/resource/key_kor_" + (index_X + 1) + "_1_h.png");
                            }
                            INSTANCE.div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                        case 1:
                            focusX = index_X;
                            BTN_LEN = 13;
                            INSTANCE.div.find("#oskEngFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                        case 2:
                            focusX = index_X;
                            BTN_LEN = 13;
                            INSTANCE.div.find("#oskCapEngFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                        case 3:
                            focusX = index_X;
                            BTN_LEN = arrSpcText.length;
                            INSTANCE.div.find("#oskNumFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                    }
                    break;
                case 2:
                    // focusOSK  0 : 한글, 1: 영어 소문자, 2: 영어 대문자, 3: 특수문자
                    switch (focusOSK) {
                        case 0:
                            focusX = index_X;
                            BTN_LEN = arrKorVow.length;
                            INSTANCE.div.find("#oskKorFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                        case 1:
                            focusX = index_X;
                            BTN_LEN = 13;
                            INSTANCE.div.find("#oskEngFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                        case 2:
                            focusX = index_X;
                            BTN_LEN = 13;
                            INSTANCE.div.find("#oskCapEngFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                        case 3:
                            focusX = index_X;
                            BTN_LEN = arrSpcText.length;
                            INSTANCE.div.find("#oskNumFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                            break;
                    }
                    break;
                case 3:
                    focusX = index_X;
                    BTN_LEN = 4;
                    INSTANCE.div.find("#thirdArea img:eq(" + index_X + ")").addClass("focus");
                    break;
            }
        }

        function setComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='searchTop_area'><div id='IMETextArea'>" +
                "<div id='searchSrc'>" +
                "<div id='searchIcon'></div>" +
                "<div id='searchFocus'>|</div>" +
                "<div id='searchSpanFocus'></div>" +
                "<span id='spanStrWidth2'>검색어를 입력해주세요</span>" +
                "<span id='spanStrWidth'></span>" +
                "</div>" +
                "<div id='searchBar'></div>" +
                "<div id='searchBox'></div>" +
                "</div>" +
                "<span id='voiceArea'>" +
                "<span id='voiceIcon'></span>" +
                "<span id='voiceText'>음성검색을 이용하면 편리합니다</span>" +
                "</span>" +
                "<div id='IMEBoardArea'>" +
                "<span class='backBG'>" +
                "<span class='backBG_focusOSK_dw'></span>" +
                "<span class='backBG_focusOSK_up'></span>" +
                "</span>" +
                "<img id='oskBoard_img' src='module/module.search/resource/osk_home_kor_h.png'>" +
                "<img id='searchBtn_img' src='module/module.search/resource/osk_btn_search_h.png'>" +
                "<img id='defaultBtn_img' src='module/module.search/resource/osk_btn_default_2.png'>" +
                "<div id='firstArea'>" +
                "<div id='oskKorFocus_firstArea'></div>" +
                "<div id='oskEngFocus_firstArea'></div>" +
                "<div id='oskCapEngFocus_firstArea'></div>" +
                "<div id='oskNumFocus_firstArea'></div>" +
                "</div>" +
                "<div id='secondArea'>" +
                "<div id='oskKorFocus_secondArea'></div>" +
                "<div id='oskEngFocus_secondArea'></div>" +
                "<div id='oskCapEngFocus_secondArea'></div>" +
                "<div id='oskNumFocus_secondArea'></div>" +
                "</div>" +
                "<div id='thirdArea'>" +
                "<img id='searchBtn_f_img' class='focusBtn' src='module/module.search/resource/osk_btn_search_f_h.png'>" +
                "<img id='deleteBtn_f_img' class='focusBtn' src='module/module.search/resource/key_delete_h.png'>" +
                "<img id='spaceBtn_f_img' class='focusBtn' src='module/module.search/resource/key_space_h.png'>" +
                "<img id='oskChangeBtn_f_img' class='focusBtn' src='module/module.search/resource/key_change_h.png'>" +
                "</div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append(compList[1].getView());

            compList[1].setListData(trendsWords, SearchManager.getAllRecentSearchWord());
            // compList[1].setDataInit();
            INSTANCE.div.find("#contents").append(compList[0].getView());
            setOSKFocusComponent();
        }

        function gotoSearchResult() {
            var searchWord = INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text();
            if (searchWord.trim().length == 0 || inputLength == 0) {
                LayerManager.activateLayer({
                    obj : {
                        id: "SearchAlertPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                        }
                    },
                    moduleId: "module.search",
                    new: true,
                    visible: true
                });
                return;
            }
            // focusResetting();
            LayerManager.startLoading({preventKey:true});
            SearchManager.searchTOTAL(function(data) {
                var searchResult_Data = data;
                SearchManager.setSearchType(3);
                SearchManager.setSearchQuery("");
                SearchManager.addRecentSearchWord({constType:"0xFF", searchWord:searchWord, wordType:3, searchQry:""});
                LayerManager.activateLayer({
                    obj: {
                        id: "SearchResult",
                        type: Layer.TYPE.NORMAL,
                        priority: Layer.PRIORITY.NORMAL,
                        params: {
                            search_Data: searchResult_Data,
                            search_Words: searchWord,
                            req_cd: "04"
                        }
                    },
                    moduleId: "module.search",
                    visible: true,
                    new: true
                });
            }, function () {
                LayerManager.stopLoading();
                LayerManager.activateLayer({
                    obj: {
                        id: "SearchError",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            title: "오류",
                            message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                            button: "닫기",
                            reboot: false
                            // callback: _error
                        }
                    },
                    moduleId: "module.search",
                    visible: true
                });
            }, true, "0xFF", 3, false, 0, 30, "0x09", searchWord, "", "POINT", 2, "Y", "", "0.5", "");
        }

        function setOSKFocusComponent() {
            var html = "";
            for (var idx = 1; idx < 15; idx++) {
                if (arrKorCon[idx - 1].length == 2) {
                    html += "<img class='korFocus_img' src='module/module.search/resource/key_kor_" + idx + "_1_h.png'>";
                } else {
                    html += "<img class='korFocus_img' src='module/module.search/resource/key_kor_" + idx + "_h.png'>";
                }
            }
            INSTANCE.div.find("#oskKorFocus_firstArea").append(html);
            html = "";
            for (var idx = 15; idx < 29; idx++) {
                html += "<img class='korFocus_img' src='module/module.search/resource/key_kor_" + idx + "_h.png'>";
            }
            INSTANCE.div.find("#oskKorFocus_secondArea").append(html);

            html = "";
            for (var idx = 0; idx < 13; idx++) {
                html += "<img class='engFocus_img' src='module/module.search/resource/key_eng_s_" + (idx + 1) + "_h.png'>";
            }
            INSTANCE.div.find("#oskEngFocus_firstArea").append(html);
            html = "";
            for (var idx = 13; idx < arrEng.length; idx++) {
                html += "<img class='engFocus_img' src='module/module.search/resource/key_eng_s_" + (idx + 1) + "_h.png'>";
            }
            INSTANCE.div.find("#oskEngFocus_secondArea").append(html);

            html = "";
            for (var idx = 0; idx < 13; idx++) {
                html += "<img class='capEngFocus_img' src='module/module.search/resource/key_eng_l_" + (idx + 1) + "_h.png'>";
            }
            INSTANCE.div.find("#oskCapEngFocus_firstArea").append(html);
            html = "";
            for (var idx = 13; idx < arrEng_Cap.length; idx++) {
                html += "<img class='capEngFocus_img' src='module/module.search/resource/key_eng_l_" + (idx + 1) + "_h.png'>";
            }
            INSTANCE.div.find("#oskCapEngFocus_secondArea").append(html);

            html = "";
            for (var idx = 1; idx < arrNum.length; idx++) {
                html += "<img class='numFocus_img' src='module/module.search/resource/key_num_" + idx + "_h.png'>";
            }
            html += "<img class='numFocus_img' src='module/module.search/resource/key_num_0_h.png'>";
            INSTANCE.div.find("#oskNumFocus_firstArea").append(html);
            html = "";
            for (var idx = 0; idx < arrSpcText.length; idx++) {
                html += "<img class='numFocus_img' src='module/module.search/resource/key_num_" + (idx + 11) + "_h.png'>";
            }
            INSTANCE.div.find("#oskNumFocus_secondArea").append(html);
        }

        function choCheck(text) {
            for (var idx = 0 ; idx < chkCho.length ; idx ++) {
                if (text === chkCho[idx]) return true;
            }
            return false;
        }

        var input_Text;

        // 한글 조합
        function sumTextString(arrayPush) {
            INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth2").text("");

            if (inputLength > inputMaxLen) {
                return;
            }

            var sumTextStr = "";
            var components2 = OSKCore.deleteAtCaret(input_Text) + arrayPush;
            var compound = OSKCore.composeHangul(components2);
            OSKCore.insertAtCaret(input_Text, compound);
            sumTextStr = input_Text.val();

            inputLength = sumTextStr.length;

            INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text(sumTextStr);
            inputHighlightFocusOn();
        }

        function setTextIME(text) {
            INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth2").text("");
            if (text.length > inputMaxLen) {
                return;
            } else {
                input_Text.val("");
                var sumTextStr = "";
                var components2 = OSKCore.deleteAtCaret(input_Text) + text;
                var compound = OSKCore.composeHangul(components2);
                OSKCore.insertAtCaret(input_Text, compound);
                sumTextStr = input_Text.val();
                inputLength = sumTextStr.length;
                INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text(sumTextStr);
                inputHighlightFocusOn();
            }
        }

        function deleteInputText() {
            var currentTxt = INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text();
            inputLength = currentTxt.length;
            if (currentTxt.length == 0) {
                return;
            }
            deleteWordTextString();
        }

        function deleteWordTextString() {
            var currentTxt = INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text();
            if (currentTxt.length == 0) {
                return;
            }
            var compound = OSKCore.deleteAtCaret(input_Text);
            var components = OSKCore.decomposeHangul(compound);
            if (components.length > 1) OSKCore.insertAtCaret(input_Text, OSKCore.composeHangul(components.slice(0, -1)));
            currentTxt = input_Text.val();
            INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text(currentTxt);

            sumTextStr = currentTxt;

            inputHighlightFocusOn();
        }

        function inputHighlightFocusOn() {
            var inputTextMessage = INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text();
            var inputLen = inputTextMessage.length;
            var searchIconX = 85;

            var inputLeftAbsoluteX = INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").position().left;
            var inputFieldWidth = INSTANCE.div.find("#spanStrWidth").outerWidth();

            // 자동완성 검색
            SearchManager.getAutoWords(function (data) {
                var resultData = data;
                compList[0].setDataInit();
                var tmpIdx = 0;
                var tmpData = _$(resultData).find("ACWORD_ITEM");
                if (tmpData.length == 0) {
                    compList[0].hide();
                    compList[1].show();
                    isResultCheck = false;
                } else {
                    tmpData.each(function () {
                        if (_$(this).find("PARENT_ID").text() == "0") {
                            compList[0].setListData(true, _$(this), tmpIdx);
                            tmpIdx++;
                        } else {
                            compList[0].setListData(false, _$(this));
                        }
                    });
                    isResultCheck = true;
                }
                if (compList[0].listDataRefresh()) {
                    isResultCheck = true;
                    compList[0].show();
                    compList[1].hide();
                } else {
                    isResultCheck = false;
                }
                if (INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text().length == 0) {
                    compList[0].hide();
                    compList[1].show();
                    isResultCheck = false;
                }
            }, function () {
                compList[0].setDataInit();
                compList[0].hide();
                compList[1].show();
                isResultCheck = false;
            }, inputTextMessage);

            INSTANCE.div.find("#IMETextArea #searchSrc #spanStrWidth").text(inputTextMessage);
            if (inputLen < 1) {
                focusPosX = 138;
                inputLength = 0;
                sumTextStr = "";
                sumTmpArr = [];
                INSTANCE.div.find("#spanStrWidth2").text(focusY == 3 ? "검색어를 입력해주세요" : "초성검색 가능 (예: 홍길동 → ㅎㄱㄷ)");
                compList[0].hide();
                compList[1].show();
                isResultCheck = false;
            } else {
                focusPosX = searchIconX + inputLeftAbsoluteX + inputFieldWidth + 5;
            }
            INSTANCE.div.find("#IMETextArea #searchSrc #searchFocus").offset({left: focusPosX});
        }
    };

    searchMenu.layer.search_main.prototype = new Layer();
    searchMenu.layer.search_main.prototype.constructor = searchMenu.layer.search_main;

    searchMenu.layer.search_main.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    searchMenu.layer.search_main.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["SearchMain"] = searchMenu.layer.search_main;
})();