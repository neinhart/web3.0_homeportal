/**
 * Created by ksk91_000 on 2016-06-30.
 */
searchMenu.view.categoryView = function() {
    View.call(this, "categoryView");

    var categories;

    var focus = 0;
    var page = 0;
    var div = this.div;

    this.create = function(data) {
        categories = data;

        (function init() {
            focus = 0;
            page = 0;
            var html = "", i;
            html+="<div id='menuArea' class='menuArea'>";
            for(i=0; i<9; i++) html+="<span>" + ((page*9+i)<categories.length?categories[HTool.getIndex(focus, (page*9+i), categories.length)].title:"") + "<subTitle>" + ((page*9+i)<categories.length?SearchManager.getSearchResultCount(categories[HTool.getIndex(focus, (page*9+i), categories.length)].key)[0]:"") + "</subTitle></span>";
            html+="</div>"+
                "<div id='focusArea'>"+
                "<div class='textArea'>"+
                "<span id='cate_focus'>" + categories[focus].title + "<subTitle>" + SearchManager.getSearchResultCount(categories[focus].key)[0] + "</subTitle></span>"+
                "<div class='menuArrow'></div>" +
                "</div>"+
                "</div>";
            html+="<span id='measureArea'></span>";

            div.attr({id:"categoryArea"});
            div.html(html);

            setFocusWidth(categories[focus].title + SearchManager.getSearchResultCount(categories[focus].key));
            div.toggleClass("noAnimation", !!CONSTANT.STB_TYPE.ANDROID);
        })();
    };

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        setTimeout(function() { setFocusWidth(categories[focus].title + SearchManager.getSearchResultCount(categories[focus].key)[0]); }, 200);
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
    };

    this.resume = function () {
        this.setFocus(focus);
    };

    this.pause = function () {
        this.setFocus(focus);
    };

    this.setFocus = function(newFocus) {
        var newPage = Math.floor(newFocus/9);
        if(page!==newPage) this.setPage(newPage);
        focus = newFocus;

        var menuChild = this.div.find("#menuArea span");
        menuChild.each(function (index) {_$(this).toggleClass("focus", page*9+index===focus).toggleClass("bottom", page*9+index>focus);});
        this.div.find("#focusArea").css("-webkit-transform", "translateY(" + (100*(focus%9)) + "px)").find("span").html(categories[focus].title + "<subTitle>" + SearchManager.getSearchResultCount(categories[focus].key)[0] + "</subTitle>");

        setFocusWidth(categories[focus].title + SearchManager.getSearchResultCount(categories[focus].key)[0]);
    };

    this.changeFocus = function(amount) {
        if (amount == 0) return;
        if (categories.length<=1) return;

        focus = HTool.getIndex(focus, amount, categories.length);
        this.setFocus(focus);
    };

    function setFocusWidth(text) {
        div.find("#measureArea").text(text);
        // [2018-02-28 TaeSang] 포커스 라인 고정으로 변경
        div.find('#focusArea').css("width", 360 + "px");
        // [2018-02-28 TaeSang] sdw_mid 클래스를 사용하는 곳이 없어서 주석처리 함
        // div.find('#focusArea .sdw_mid').css("width", Math.max(155, div.find("#measureArea")[0].offsetWidth -109) + "px");
    }

    function getCurrentContent() {
        return categories[focus];
    }

    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return true;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                this.blurred();
                return true;
            default:
                return false;
        }
    };

    this.focused = function() {
        this.setFocus(focus);
        this.div.removeClass("noFocus");
        return true;
    };

    this.blurred =  function() {
        // this.div.css('opacity', 0);
        this.div.addClass("noFocus");
    };

    this.resetCategory = function() {
        for (var idx = 0 ; idx < categories.length ; idx ++) {
            if (categories[idx].title.indexOf("가입한 월정액") > -1) {
                categories.splice(idx, 1);
                break;
            }
        }
        div.empty();
        this.create(categories);
        this.resume();
        this.blurred();
    };

    this.getCurrentContent = getCurrentContent;
};

searchMenu.view.categoryView.prototype = new View();
searchMenu.view.categoryView.prototype.constructor = searchMenu.view.categoryView;
searchMenu.view.categoryView.prototype.type = View.TYPE.CATEGORY;