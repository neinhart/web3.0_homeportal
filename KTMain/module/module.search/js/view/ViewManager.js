/**
 * Created by ksk91_000 on 2016-08-19.
 */
searchMenu.ViewManager = function () {
    var parent;
    var div = _$("<div/>", {class:'viewArea'});

    var currentCateViewData;
    var currentContViewData;
    var ViewDataMap = {};
    var historyStack = [];
    var contentsChangeTimeout = null;

    var MODE = {CATEGORY:0, CONTENTS:1};
    var mode = MODE.CATEGORY;
    var instance;

    var cateKeyAction = function(keyCode) {
        if(currentCateViewData.view.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.UP:
            case KEY_CODE.DOWN:
                setContentsChangeTimeout(function() {setCurrentContent(currentCateViewData.view.getCurrentContent())});
                return true;
        }
    };

    var changeFocus = function(viewData) {
        currentCateViewData.view.focused();
    };

    var changeCategoryFocus = function(idx) {
        if(idx<0) return;
        if(mode == MODE.CATEGORY) {
            currentCateViewData.view.changeFocus(idx);
        } setCurrentContent(currentCateViewData.view.getCurrentContent());
    };

    var changeTitle = function(title) {
        parent.find("#backgroundTitle").text(title);
    };

    var historyBack = function () {
        if (historyStack.length > 0) {
            switch(getCurrentView().getView().type){
                case View.TYPE.CONTENTS:
                    getCurrentView().view.blurred();
                    break;
                case View.TYPE.CATEGORY:
                    getCurrentView().view.hide();
                    break;
            }

            var currentView = historyStack.pop();
            switch (currentView.getView().type) {
                case View.TYPE.CATEGORY:
                    mode = MODE.CATEGORY;
                    setCurrentCategory(currentView);
                    setCurrentContent(currentView.view.getCurrentContent());
                    break;
                case View.TYPE.CONTENTS:
                    mode = MODE.CONTENTS;
                    setCurrentContent(currentView);
                    currentCateViewData.view.hide();
                    currentView.focused();
                    break;
            } changeTitle(currentView.title);
            return true;
        } else return false;
    };

    var addView = function(viewData) {
        ViewDataMap[viewData.viewId] = ViewDataMap[viewData.viewId]||(div.append(viewData.getView().getView()), viewData);
        viewData.getView().viewMgr = instance;
    };

    var setCurrentCategory = function (categoryViewData) {
        mode = MODE.CATEGORY;
        currentCateViewData?currentCateViewData.view.hide():0;
        currentCateViewData = categoryViewData;
        addView(categoryViewData);
        currentCateViewData.view.focused();
        currentCateViewData.view.show();
        setCurrentContent(categoryViewData.view.getCurrentContent());
    };

    var setCurrentContent = function (contentsViewData) {
        currentContViewData?currentContViewData.view.hide():0;
        currentContViewData = contentsViewData;
        addView(currentContViewData);
        currentContViewData.view.blurred();
        currentContViewData.view.show();
    };

    var getCurrentView = function() {
        if(mode==MODE.CATEGORY) return currentCateViewData;
        else if(mode==MODE.CONTENTS) return currentContViewData;
    };

    var setData = function(data) {
        setCurrentCategory(data);
    };

    function setContentsChangeTimeout(func) {
        if(contentsChangeTimeout) {
            clearTimeout(contentsChangeTimeout);
            contentsChangeTimeout = null;
        }contentsChangeTimeout = setTimeout(func, 500);
    }
    
    function contensChangeImmediately() {
        if(contentsChangeTimeout) {
            clearTimeout(contentsChangeTimeout);
            contentsChangeTimeout = null;
            setCurrentContent(currentCateViewData.view.getCurrentContent());
        }
    }

    var pause = function() {
        if (getCurrentView()) getCurrentView().view.pause();
    }; this.pause = pause;

    var resume = function() {
        if (getCurrentView()) getCurrentView().view.resume();
    }; this.resume = resume;

    var setResetCategory = function () {
        if (getCurrentView()) getCurrentView().view.resetCategory();
    }; this.setResetCategory = setResetCategory;
    
    return {
        setParent: function (_parent) { parent = _parent; },
        setData: setData,
        setResetCategory: setResetCategory,
        setFocus: changeFocus,
        changeCategoryFocus: changeCategoryFocus,
        onKeyAction: function (keyCode) {
            if(mode==MODE.CATEGORY) {
                if(cateKeyAction(keyCode)) return true;
            }else if(mode==MODE.CONTENTS){
                if (currentContViewData && currentContViewData.view.onKeyAction(keyCode)) return true;
            }

            return keyCode == KEY_CODE.BACK && historyBack();
        },
        pause: pause,
        resume: resume,
        getView: function() { return div; },
        destroy: function() {
            for(var id in ViewDataMap) ViewDataMap[id].detach();
            ViewDataMap = {};
        },
        historyBack: historyBack,
        changeTitle: changeTitle,
        getCurrentMenuData: function () {
            return currentContViewData;
        },
        setInstance: function (_instance) { instance = _instance; }
    };
};
Object.defineProperty(searchMenu.ViewManager, "FOCUS_MODE", { value: { NO_FOCUS: 0, FOCUS:1, BLUR_FOCUS: 2 },writable:false });