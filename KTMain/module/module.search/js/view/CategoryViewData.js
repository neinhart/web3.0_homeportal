/**
 * Created by ksk91_000 on 2016-11-23.
 */
window.CategoryViewData = function (viewId, dataList, title, key) {
    this.viewId = viewId;
    this.viewClass = searchMenu.view.categoryView;
    this.data = dataList;
    this.title = title;
    this.key = key;
};

CategoryViewData.prototype = new ViewData();
CategoryViewData.prototype.constructor = CategoryViewData;