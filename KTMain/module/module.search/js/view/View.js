/**
 * Created by ksk91_000 on 2016-07-01.
 */
searchMenu.view = searchMenu.view||{};
window.View = function(viewId) {
    this.div = _$("<div/>");
    this.viewId = viewId;
    this.div.attr({id: this.viewId});
    this.title = "";
    this.viewMgr;
};

Object.defineProperty(View, "TYPE", {
    value:{EMPTY:-1, CATEGORY:0, CONTENTS:1},
    writable:!1
});

View.prototype = {
    type: View.TYPE.EMPTY,
    create: function () {
    },
    destroy: function () {
        this.div = null;
    },
    show: function () {
        this.div.show();
    },
    hide: function () {
        this.div.hide();
    },
    pause: function() {
    },
    resume: function () {
    },
    focused: function() {
        this.div.removeClass("blurred");
    },
    blurred: function() {
        this.div.addClass("blurred");
    },
    onKeyAction: function(key_code) {
        if(key_code == KEY_CODE.ENTER) return true;
        return false;
    },
    getView: function() {
        return this.div;
    },
    getTitle: function() {
        return this.title;
    }
};