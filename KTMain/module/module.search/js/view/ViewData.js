/**
 * Created by ksk91_000 on 2016-11-21.
 */
window.ViewData = function(viewId, viewClass, data, title, subTitle, key) {
    this.viewId = viewId;
    this.viewClass = viewClass||View;
    this.data = data;
    this.title = title;
    this.subTitle = subTitle;
    this.key = key;
    this.view;
};

ViewData.prototype.getView = function() {
    if(!this.view) {
        this.view = new this.viewClass(this.viewId);
        this.view.create(this.data);
    }

    return this.view;
};