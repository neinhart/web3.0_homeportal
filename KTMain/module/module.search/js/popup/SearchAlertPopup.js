/**
 * Created by Yun on 2017-04-24.
 */

(function () {
    searchMenu.popup = searchMenu.popup || {};
    searchMenu.popup.searchAlert = function searchAlertPopup(options) {
        Layer.call(this, options);

        var div;

        this.init = function (cbCreate) {
            div = this.div;
            div.addClass("arrange_frame searchResult gniPopup searchAlertPopup");
            div.append(_$("<title/>").css({"position": "absolute", "top": "418px"}).text("알림"));
            div.append(_$("<span/>", {class:"text_white_center_45M"}).css({"position": "absolute", "top": "500px"}).text("검색어를 입력해주세요"));

            div.append(_$("<div/>", {class: "oneBtnArea_280"}).css({"position": "absolute", "top": "622px"}));
            div.find(".oneBtnArea_280").append(_$("<div/>", {class: "btn focus"}));
            div.find(".oneBtnArea_280 .btn").append(_$("<div/>", {class: "whiteBox"}));
            div.find(".oneBtnArea_280 .btn").append(_$("<span/>", {class: "btnText"}).text("확인"));

            cbCreate(true);
        };

        this.show = function () {
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function (keyCode) {
            if (keyCode == KEY_CODE.ENTER || keyCode == KEY_CODE.EXIT) {
                LayerManager.historyBack();
                return true;
            }
            return false;
        }
    };

    searchMenu.popup.searchAlert.prototype = new Layer();
    searchMenu.popup.searchAlert.prototype.constructor = searchMenu.popup.searchAlert;

    searchMenu.popup.searchAlert.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    searchMenu.popup.searchAlert.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["SearchAlertPopup"] = searchMenu.popup.searchAlert;
})();