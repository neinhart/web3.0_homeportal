/**
 * Created by Yun on 2017-03-17.
 */

(function () {
    searchMenu.popup = searchMenu.popup || {};
    searchMenu.popup.listFiltering = function listFilteringPopup(options) {
        Layer.call(this, options);
        var div;
        var filterMenuTitle = ["범위", "장르", "가격", "정렬"];
        var filterMenuTitle_Pkg = ["범위", "장르", "정렬"];
        var btnTitle = ["완료", "초기화", "취소"];
        var isChange = false;
        var isSelWon = false;

        var tmpData = [[{keyWord:"모두 선택"}], [{keyWord:"모두 선택"}], [{keyWord:"모두 선택"}, {keyWord:"유료"}, {keyWord:"무료"}], [{keyWord:"인기도순"}, {keyWord:"별점순"}, {keyWord:"가나다순"}, {keyWord:"날짜순"}]];
        var packageData = [[{keyWord:"모두 선택"}], [{keyWord:"모두 선택"}], [{keyWord:"인기도순"}, {keyWord:"별점순"}, {keyWord:"가나다순"}, {keyWord:"날짜순"}]];
        var tvProFilterData = ["방송시간순", "가나다순"];
        var tvAppFilterData = ["인기도순", "가나다순", "추천순", "최신순"];
        var filterData = [];
        var selectScale = [];
        var selScaleIdxArr = [];
        var selectGenre = [];
        var selGenreIdxArr = [];

        var focusRowIdx = 0;
        var focusVerIdx = 0;
        var selectIdx = [0, 0, 0, 0];
        var curPage = [0, 0, 0, 0];
        var totalPage = [0, 0, 0, 0];

        var POPUP_STATE = "";

        var params;

        this.init = function (cbCreate) {
            div = this.div;
            POPUP_STATE = this.getParams().popType;
            params = this.getParams();
            selectScale = [];
            selectGenre = [];
            div.addClass("arrange_frame searchResult gniPopup listFilteringPopup");
            if (POPUP_STATE == "VOD") {
                filterData = this.getParams().data;
                isChange = false;
                isSelWon = false;
                if (SearchManager.getSelectVODFilterIndex().length > 0) {
                    for (var idx = 0; idx < SearchManager.getSelectVODFilterIndex().length; idx++) {
                        selectIdx[idx] = SearchManager.getSelectVODFilterIndex()[idx];
                    }
                } else {
                    for (var idx = 0; idx < 4; idx++) {
                        selectIdx[idx] = 0;
                    }
                }
                for (var idx = 0; idx < filterData.length; idx++) {
                    if (filterData[idx].keyType == "10") {
                        tmpData[0].push(filterData[idx]);
                    } else {
                        tmpData[1].push(filterData[idx]);
                    }
                }
                for (var idx = 1 ; idx < tmpData[0].length ; idx ++) {
                    if (tmpData[0][idx].isCheck == "Y") {
                        isChange = true;
                        selScaleIdxArr.push((idx));
                        selectScale.push(tmpData[0][idx].keyWord);
                    }
                }
                for (var idx = 1 ; idx < tmpData[1].length ; idx ++) {
                    if (tmpData[1][idx].isCheck == "Y") {
                        isChange = true;
                        selGenreIdxArr.push((idx));
                        selectGenre.push(tmpData[1][idx].keyWord);
                    }
                }
                if (selScaleIdxArr.length == 0) {
                    selectIdx[0] = 0;
                }
                if (selGenreIdxArr.length == 0) {
                    selectIdx[1] = 0;
                }
                totalPage[0] = Math.ceil(tmpData[0].length / 6);
                totalPage[1] = Math.ceil(tmpData[1].length / 6);
                if (totalPage[0] == 1) totalPage[0] = 0;
                if (totalPage[1] == 1) totalPage[1] = 0;
                div.append(_$("<title/>").css({"position": "absolute", "top": "180px"}).text("검색 필터 설정"));
                div.append(_$("<div class='listFilteringPopupBG'></div>"));
                div.append(_$("<div/>", {class: "listFilteringContent_area"}));
                for (var idx = 0; idx < filterMenuTitle.length; idx++) {
                    div.find(".listFilteringContent_area").append(_$("<div/>", {class: "listFilteringContent_div"}));
                    div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).append(_$("<span/>", {class: "listFilteringContent_title"}).text(filterMenuTitle[idx]));
                    for (var z = 0; z < tmpData[idx].length; z++) {
                        div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).append(_$("<div/>", {class: "listFilteringContent_btn_div"}));
                        div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).append(getFocusDiv());
                        div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).append(_$("<div/>", {class: (filterMenuTitle.length - 1 == idx ? "rdo_btn_img" : "chk_btn_img")}));
                        div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).append(_$("<span/>", {class: "btn_title"}).text(tmpData[idx][z].keyWord));
                        if (z >= 6) div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).addClass("hide");
                    }
                    if (tmpData[idx].length > 6) div.find(".listFilteringContent_area .listFilteringContent_div").eq(idx).append(_$("<div/>", {class: "listFilteringContent_downArrow_img"}));
                }
                div.append(_$("<div/>", {class: "listFilteringButton_area"}));
                for (var idx = 0; idx < btnTitle.length; idx++) {
                    div.find(".listFilteringButton_area").append(_$("<div/>", {class: "listFilteringButton"}));
                    div.find(".listFilteringButton_area .listFilteringButton").eq(idx).append(_$("<div/>", {class: "listFilteringButton_title"}).text(btnTitle[idx]));
                }
            } else if (POPUP_STATE == "VOD_PACKAGE") {
                filterData = this.getParams().data;
                isChange = false;
                if (SearchManager.getSelectVODFilterIndex().length > 0) {
                    for (var idx = 0 ; idx < SearchManager.getSelectVODFilterIndex().length ; idx ++) {
                        selectIdx[idx] = SearchManager.getSelectVODFilterIndex()[idx];
                    }
                } else {
                    for (var idx = 0; idx < 4; idx++) {
                        selectIdx[idx] = 0;
                    }
                }
                for (var idx = 0; idx < filterData.length; idx++) {
                    if (filterData[idx].keyType == "10") {
                        packageData[0].push(filterData[idx]);
                    } else {
                        packageData[1].push(filterData[idx]);
                    }
                }
                for (var idx = 1 ; idx < packageData[0].length ; idx ++) {
                    if (packageData[0][idx].isCheck == "Y") {
                        isChange = true;
                        selScaleIdxArr.push((idx));
                        selectScale.push(packageData[0][idx].keyWord);
                    }
                }
                for (var idx = 1 ; idx < packageData[1].length ; idx ++) {
                    if (packageData[1][idx].isCheck == "Y") {
                        isChange = true;
                        selGenreIdxArr.push((idx));
                        selectGenre.push(packageData[1][idx].keyWord);
                    }
                }
                if (selScaleIdxArr.length == 0) {
                    selectIdx[0] = 0;
                }
                if (selGenreIdxArr.length == 0) {
                    selectIdx[1] = 0;
                }

                totalPage[0] = Math.ceil(packageData[0].length / 6);
                totalPage[1] = Math.ceil(packageData[1].length / 6);
                if (totalPage[0] == 1) totalPage[0] = 0;
                if (totalPage[1] == 1) totalPage[1] = 0;
                div.append(_$("<title/>").css({"position": "absolute", "top": "180px"}).text("검색 필터 설정"));
                div.append(_$("<img/>", {
                    class: "listFilteringPopupBG_1_top_img",
                    src: "module/module.search/resource/popup/searchfilter_bg_1_top.png"
                }));
                div.append(_$("<img/>", {
                    class: "listFilteringPopupBG_1_m_img",
                    src: "module/module.search/resource/popup/searchfilter_bg_1_m.png"
                }));
                div.append(_$("<img/>", {
                    class: "listFilteringPopupBG_1_btm_img",
                    src: "module/module.search/resource/popup/searchfilter_bg_1_btm.png"
                }));
                div.append(_$("<div/>", {class: "listFilteringContent_1_area"}));
                for (var idx = 0; idx < filterMenuTitle_Pkg.length; idx++) {
                    div.find(".listFilteringContent_1_area").append(_$("<div/>", {class: "listFilteringContent_div"}));
                    div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).append(_$("<span/>", {class: "listFilteringContent_title"}).text(filterMenuTitle_Pkg[idx]));
                    for (var z = 0; z < packageData[idx].length; z++) {
                        div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).append(_$("<div/>", {class: "listFilteringContent_btn_div"}));
                        div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).append(getFocusDiv());
                        div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).append(_$("<div/>", {class: (filterMenuTitle_Pkg.length - 1 == idx ? "rdo_btn_img" : "chk_btn_img")}));
                        div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).append(_$("<span/>", {class: "btn_title"}).text(packageData[idx][z].keyWord));
                        if (z >= 6) div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).find(".listFilteringContent_btn_div").eq(z).addClass("hide");
                    }
                    if (packageData[idx].length > 6) div.find(".listFilteringContent_1_area .listFilteringContent_div").eq(idx).append(_$("<div/>", {class: "listFilteringContent_downArrow_img"}));
                }
                div.append(_$("<div/>", {class: "listFilteringButton_1_area"}));
                for (var idx = 0; idx < btnTitle.length; idx++) {
                    div.find(".listFilteringButton_1_area").append(_$("<div/>", {class: "listFilteringButton"}));
                    div.find(".listFilteringButton_1_area .listFilteringButton").eq(idx).append(_$("<div/>", {class: "listFilteringButton_title"}).text(btnTitle[idx]));
                }

            } else if (POPUP_STATE == "TV_PROGRAM") {
                div.append(_$("<title/>").css({"position": "absolute", "top": "180px"}).text("검색 필터 설정"));
                div.append(_$("<div class='listFilteringPopupBG_2'></div>"));

                div.append(_$("<div/>", {class: "listFilteringContent_2_area"}));
                div.find(".listFilteringContent_2_area").append(_$("<div/>", {class: "listFilteringContent_div"}));
                div.find(".listFilteringContent_2_area .listFilteringContent_div").append(_$("<span/>", {class: "listFilteringContent_title"}).text(filterMenuTitle[3]));
                for (var z = 0; z < tvProFilterData.length; z++) {
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").append(_$("<div/>", {class: "listFilteringContent_btn_div"}));
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").find(".listFilteringContent_btn_div").eq(z).append(getFocusDiv());
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").find(".listFilteringContent_btn_div").eq(z).append(_$("<div/>", {class: "rdo_btn_img"}));
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").find(".listFilteringContent_btn_div").eq(z).append(_$("<span/>", {class: "btn_title"}).text(tvProFilterData[z]));
                }
                div.append(_$("<div/>", {class: "listFilteringButton_2_area"}));
                for (var idx = 0; idx < btnTitle.length; idx++) {
                    div.find(".listFilteringButton_2_area").append(_$("<div/>", {class: "listFilteringButton"}));
                    div.find(".listFilteringButton_2_area .listFilteringButton").eq(idx).append(_$("<div/>", {class: "listFilteringButton_title"}).text(btnTitle[idx]));
                }
            } else if (POPUP_STATE == "TV_APP") {
                div.append(_$("<title/>").css({"position": "absolute", "top": "180px"}).text("검색 필터 설정"));
                div.append(_$("<div class='listFilteringPopupBG_2'></div>"));

                div.append(_$("<div/>", {class: "listFilteringContent_2_area"}));
                div.find(".listFilteringContent_2_area").append(_$("<div/>", {class: "listFilteringContent_div"}));
                div.find(".listFilteringContent_2_area .listFilteringContent_div").append(_$("<span/>", {class: "listFilteringContent_title"}).text(filterMenuTitle[3]));
                for (var z = 0; z < tvAppFilterData.length; z++) {
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").append(_$("<div/>", {class: "listFilteringContent_btn_div"}));
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").find(".listFilteringContent_btn_div").eq(z).append(getFocusDiv());
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").find(".listFilteringContent_btn_div").eq(z).append(_$("<div/>", {class: "rdo_btn_img"}));
                    div.find(".listFilteringContent_2_area .listFilteringContent_div").find(".listFilteringContent_btn_div").eq(z).append(_$("<span/>", {class: "btn_title"}).text(tvAppFilterData[z]));
                }
                div.append(_$("<div/>", {class: "listFilteringButton_2_area"}));
                for (var idx = 0; idx < btnTitle.length; idx++) {
                    div.find(".listFilteringButton_2_area").append(_$("<div/>", {class: "listFilteringButton"}));
                    div.find(".listFilteringButton_2_area .listFilteringButton").eq(idx).append(_$("<div/>", {class: "listFilteringButton_title"}).text(btnTitle[idx]));
                }
            }
            cbCreate(true);
        };

        function getFocusDiv() {
            var focusDiv =
                "<div class='btn_focus'>" +
                "   <div class='btn_focus_padding'>" +
                "   </div>" +
                "</div>";
            return _$(focusDiv.trim());
        }

        this.show = function () {
            if (POPUP_STATE == "VOD") {
                for (var idx = 0; idx < 4; idx++) {
                    setFilteringDataCache(idx);
                }
                focusVerIdx = 0;
                focusRowIdx = 0;
                buttonFocusRefresh_VOD(focusVerIdx, focusRowIdx);
            } else if (POPUP_STATE == "VOD_PACKAGE") {
                for (var idx = 0; idx < 3; idx++) {
                    setPkgFilteringDataCache(idx);
                }
                focusVerIdx = 0;
                focusRowIdx = 0;
                buttonFocusRefresh_VODPkg(focusVerIdx, focusRowIdx);
            } else if (POPUP_STATE == "TV_PROGRAM") {
                focusVerIdx = 0;
                focusRowIdx = 0;
                // enterKeyEvent_TvProgram(SearchManager.getSelectTVProgramIndex(), focusRowIdx);
                selectIdx[0] = SearchManager.getSelectTVProgramIndex();
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + selectIdx[0] + ") .rdo_btn_img").addClass("select");
                buttonFocusRefresh_TvProgram(focusVerIdx, focusRowIdx);
            } else if (POPUP_STATE == "TV_APP") {
                focusVerIdx = 0;
                focusRowIdx = 0;
                // enterKeyEvent_TvApp(SearchManager.getSelectTVAppIndex(), focusRowIdx);
                selectIdx[0] = SearchManager.getSelectTVAppIndex();
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + selectIdx[0] + ") .rdo_btn_img").addClass("select");
                buttonFocusRefresh_TvApp(focusVerIdx, focusRowIdx);
            }
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        function setFilteringDataCache(type) {
            switch (type) {
                case 0:
                    if (selScaleIdxArr.length == 0) {
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                    } else {
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        for (var idx = 0; idx < selScaleIdxArr.length; idx++) {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + selScaleIdxArr[idx] + ") .chk_btn_img").addClass("select");
                        }
                        if (selScaleIdxArr.length == tmpData[0].length-1) {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        }
                    }
                    break;
                case 1:
                    if (selGenreIdxArr.length == 0) {
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                    } else {
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        for (var idx = 0; idx < selGenreIdxArr.length; idx++) {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + selGenreIdxArr[idx] + ") .chk_btn_img").addClass("select");
                        }
                        if (selGenreIdxArr.length == tmpData[1].length-1) {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        }
                    }
                    break;
                case 2:
                    div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                    switch (selectIdx[2]) {
                        case 0:
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            break;
                        case 1:
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(1) .chk_btn_img").addClass("select");
                            break;
                        case 2:
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(2) .chk_btn_img").addClass("select");
                            break;
                    }
                    break;
                case 3:
                    div.find(".listFilteringContent_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                    div.find(".listFilteringContent_area .listFilteringContent_div:eq(3) .listFilteringContent_btn_div:eq(" + selectIdx[3] + ") .rdo_btn_img").addClass("select");
                    break;
            }
        }
        
// ======================================== VOD 검색결과 필터처리
        function enterKeyEvent_VOD(_selectIdx, rowIndex) {
            switch (rowIndex) {
                case 0:
                    if (focusVerIdx == 0) {
                        if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").hasClass("select")) {
                            selectScale = [];
                            selScaleIdxArr = [];
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        } else {
                            selectScale = [];
                            selScaleIdxArr = [];
                            if (isChange) {
                                for (var idx = 1; idx < tmpData[0].length; idx++) {
                                    selectScale.push(tmpData[0][idx].keyWord);
                                    selScaleIdxArr.push(idx);
                                }
                            }
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                        }
                    } else {
                        for (var idx = 0; idx < selScaleIdxArr.length; idx++) {
                            if (selScaleIdxArr[idx] == focusVerIdx) {
                                selectScale.splice(idx, 1);
                                selScaleIdxArr.splice(idx, 1);
                                break;
                            }
                        }
                        if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").hasClass("select")) {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").removeClass("select");
                        } else {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").addClass("select");
                        }
                        selectScale = [];
                        selScaleIdxArr = [];
                        for (var idx = 1 ; idx < tmpData[0].length ; idx ++) {
                            if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + idx + ") .chk_btn_img").hasClass("select")) {
                                selectScale.push(tmpData[0][idx].keyWord);
                                selScaleIdxArr.push(idx);
                            }

                        }

                        if (selScaleIdxArr.length == tmpData[0].length-1) {
                            if (!isChange) {
                                selectScale = [];
                                selScaleIdxArr = [];
                            }
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        } else {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").removeClass("select");
                        }
                    }
                    break;
                case 1:
                    if (focusVerIdx == 0) {
                        if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").hasClass("select")) {
                            selectGenre = [];
                            selGenreIdxArr = [];
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        } else {
                            selectGenre = [];
                            selGenreIdxArr = [];
                            if (isChange) {
                                for (var idx = 1; idx < tmpData[1].length; idx++) {
                                    selectGenre.push(tmpData[1][idx].keyWord);
                                    selGenreIdxArr.push(idx);
                                }
                            }
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                        }
                    } else {
                        for (var idx = 0 ; idx < selGenreIdxArr.length ; idx ++ ){
                            if (selGenreIdxArr[idx] == focusVerIdx) {
                                selectGenre.splice(idx, 1);
                                selGenreIdxArr.splice(idx, 1);
                                break;
                            }
                        }

                        if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").hasClass("select")) {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").removeClass("select");
                        } else {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").addClass("select");
                        }

                        selectGenre = [];
                        selGenreIdxArr = [];
                        for (var idx = 1 ; idx < tmpData[1].length ; idx ++) {
                            if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + idx + ") .chk_btn_img").hasClass("select")) {
                                selectGenre.push(tmpData[1][idx].keyWord);
                                selGenreIdxArr.push(idx);
                            }

                        }

                        if (selGenreIdxArr.length == tmpData[1].length-1) {
                            if (!isChange) {
                                selectGenre = [];
                                selGenreIdxArr = [];
                            }
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        } else {
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").removeClass("select");
                        }
                    }
                    break;
                case 2:
                    switch (_selectIdx) {
                        case 0:
                            if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(0) .chk_btn_img").hasClass("select")) {
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                            } else {
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            }
                            break;
                        case 1:
                            if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(1) .chk_btn_img").hasClass("select")) {
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(0) .chk_btn_img").removeClass("select");
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(1) .chk_btn_img").removeClass("select");
                            } else {
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(1) .chk_btn_img").addClass("select");
                            }
                            break;
                        case 2:
                            if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(2) .chk_btn_img").hasClass("select")) {
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(0) .chk_btn_img").removeClass("select");
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(2) .chk_btn_img").removeClass("select");
                            } else {
                                div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(2) .chk_btn_img").addClass("select");
                            }
                            break;
                    }
                    selectIdx[rowIndex] = 0;
                    for (var idx = 0 ; idx < 3 ; idx ++) {
                        if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(" + idx + ") .chk_btn_img").hasClass("select")) {
                            selectIdx[rowIndex] = idx;
                            isSelWon = true;
                            break;
                        } else {
                            isSelWon = false;
                        }
                    }
                    if (div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(1) .chk_btn_img").hasClass("select") &&
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(2) .chk_btn_img").hasClass("select")) {
                        div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        selectIdx[rowIndex] = 0;
                    }
                    break;
                case 3:
                    div.find(".listFilteringContent_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                    selectIdx[rowIndex] = _selectIdx;
                    div.find(".listFilteringContent_area .listFilteringContent_div:eq(3) .listFilteringContent_btn_div:eq(" + _selectIdx + ") .rdo_btn_img").addClass("select");
                    focusVerIdx = 0;
                    buttonFocusRefresh_VOD(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 5));
                    break;
                case 4:
                    switch (_selectIdx) {
                        case 0:
                            // 최초 필터설정 후 범위 또는 장르, 가격 선택이 없을 경우 해당 필터 값 모두 담아서 결과 요청
                            var tmpFilterData = "";
                            if (isChange && selectScale.length == 0) {
                                for (var idx = 1 ; idx < tmpData[0].length ; idx ++) {
                                    if (idx == 1) {
                                        tmpFilterData += ("10:" + tmpData[0][idx].keyWord);
                                    } else {
                                        tmpFilterData += (";" + tmpData[0][idx].keyWord);
                                    }
                                }
                            } else {
                                for (var idx = 0; idx < selectScale.length; idx++) {
                                    if (idx == 0) {
                                        tmpFilterData += ("10:" + selectScale[idx]);
                                    } else {
                                        tmpFilterData += (";" + selectScale[idx]);
                                    }
                                }
                            }
                            if (isChange && selectGenre.length == 0) {
                                for (var idx = 1 ; idx < tmpData[1].length ; idx ++) {
                                    if (idx == 1) {
                                        if (tmpFilterData.length > 0) tmpFilterData += ",";
                                        tmpFilterData += ("11:" + tmpData[1][idx].keyWord);
                                    } else {
                                        tmpFilterData += (";" + tmpData[1][idx].keyWord);
                                    }
                                }
                            } else {
                                for (var idx = 0; idx < selectGenre.length; idx++) {
                                    if (idx == 0) {
                                        if (tmpFilterData.length > 0) tmpFilterData += ",";
                                        tmpFilterData += ("11:" + selectGenre[idx]);
                                    } else {
                                        tmpFilterData += (";" + selectGenre[idx]);
                                    }
                                }
                            }
                            if (!isSelWon) {
                                isSelWon = true;
                                selectIdx[2] = 0;
                            }
                            window.log.printDbg("[Set VOD Filter] : " + tmpFilterData);
                            if(params.complete(tmpFilterData, selectIdx[2], selectIdx[3])) {
                                SearchManager.setSelectVODFilterIndex(selectIdx);
                                LayerManager.historyBack();
                            } else {
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "SearchToast",
                                        type: Layer.TYPE.BACKGROUND,
                                        priority: Layer.PRIORITY.REMIND_POPUP,
                                        linkage: true,
                                        params: {
                                            text: "선택한 옵션으로 표시할 수 있는 콘텐츠가 없습니다"
                                        }
                                    },
                                    moduleId: "module.search",
                                    visible: true
                                });
                            }
                            break;
                        case 1:
                            selectIdx[0] = 1;
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                            selectScale = [];
                            selScaleIdxArr = [];
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            selectIdx[1] = 1;
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                            selectGenre = [];
                            selGenreIdxArr = [];
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            selectIdx[2] = 0;
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            div.find(".listFilteringContent_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                            selectIdx[3] = 0;
                            div.find(".listFilteringContent_area .listFilteringContent_div:eq(3) .listFilteringContent_btn_div:eq(" + selectIdx[3] + ") .rdo_btn_img").addClass("select");
                            focusVerIdx = 0;
                            focusRowIdx = 0;
                            buttonFocusRefresh_VOD(focusVerIdx, focusRowIdx);
                            break;
                        case 2:
                            LayerManager.historyBack();
                            break;
                    }
                    break;
            }
        }

        function buttonFocusRefresh_VOD(verIndex, rowIndex) {
            div.find(".listFilteringContent_area .listFilteringContent_div .listFilteringContent_btn_div").removeClass("focus");
            div.find(".listFilteringButton_area .listFilteringButton").removeClass("focus");
            switch (rowIndex) {
                case 0:
                case 1:
                case 2:
                case 3:
                    if (Math.floor(verIndex / 6) != curPage[focusRowIdx]) changePage(Math.floor(verIndex / 6));
                    div.find(".listFilteringContent_area .listFilteringContent_div:eq(" + rowIndex + ") .listFilteringContent_btn_div:eq(" + verIndex + ")").addClass("focus");
                    break;
                case 4:
                    div.find(".listFilteringButton_area .listFilteringButton:eq(" + verIndex + ")").addClass("focus");
                    break;
            }
        }

        function changePage(page) {
            curPage[focusRowIdx] = page;
            div.find(".listFilteringContent_area .listFilteringContent_div:eq(" + focusRowIdx + ") .listFilteringContent_btn_div").addClass("hide");
            for (var idx = (curPage[focusRowIdx] * 6); idx < 6 + (curPage[focusRowIdx] * 6); idx++) {
                div.find(".listFilteringContent_area .listFilteringContent_div:eq(" + focusRowIdx + ") .listFilteringContent_btn_div:eq(" + idx + ")").removeClass("hide")
            }
        }

        function vodFilteringControlKey(key_code) {
            switch (key_code) {
                case KEY_CODE.LEFT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_VOD(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, -1, 5));
                    return true;
                case KEY_CODE.RIGHT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_VOD(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 5));
                    return true;
                case KEY_CODE.UP:
                    buttonFocusRefresh_VOD(focusVerIdx = HTool.getIndex(focusVerIdx, -1, (focusRowIdx != 4 ? tmpData[focusRowIdx].length : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh_VOD(focusVerIdx = HTool.getIndex(focusVerIdx, 1, (focusRowIdx != 4 ? tmpData[focusRowIdx].length : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEvent_VOD(focusVerIdx, focusRowIdx);
                    return true;
                case KEY_CODE.CONTEXT:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        }
        
// ======================================== 월정액 검색결과 필터처리
        function setPkgFilteringDataCache(type) {
            switch (type) {
                case 0:
                    if (selScaleIdxArr.length == 0) {
                        div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                    } else {
                        div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        for (var idx = 0; idx < selScaleIdxArr.length; idx++) {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + selScaleIdxArr[idx] + ") .chk_btn_img").addClass("select");
                        }
                        if (selScaleIdxArr.length == packageData[0].length-1) {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        }
                    }
                    break;
                case 1:
                    if (selGenreIdxArr.length == 0) {
                        div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                    } else {
                        div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        for (var idx = 0; idx < selGenreIdxArr.length; idx++) {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + selGenreIdxArr[idx] + ") .chk_btn_img").addClass("select");
                        }
                        if (selGenreIdxArr.length == packageData[1].length-1) {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        }
                    }
                    break;
                case 2:
                    div.find(".listFilteringContent_1_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                    div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(" + selectIdx[3] + ") .rdo_btn_img").addClass("select");
                    break;
            }
        }

        function enterKeyEvent_VODPkg(_selectIdx, rowIndex) {
            switch (rowIndex) {
                case 0:
                    if (focusVerIdx == 0) {
                        if (div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").hasClass("select")) {
                            selectScale = [];
                            selScaleIdxArr = [];
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        } else {
                            selectScale = [];
                            selScaleIdxArr = [];
                            if (isChange) {
                                for (var idx = 1; idx < packageData[0].length; idx++) {
                                    selectScale.push(packageData[0][idx].keyWord);
                                    selScaleIdxArr.push(idx);
                                }
                            }
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                        }
                    } else {
                        for (var idx = 0; idx < selScaleIdxArr.length; idx++) {
                            if (selScaleIdxArr[idx] == focusVerIdx) {
                                selectScale.splice(idx, 1);
                                selScaleIdxArr.splice(idx, 1);
                                break;
                            }
                        }

                        if (div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").hasClass("select")) {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").removeClass("select");
                        } else {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").addClass("select");
                        }
                        selectScale = [];
                        selScaleIdxArr = [];

                        for (var idx = 1; idx < packageData[0].length ; idx ++) {
                            if (div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(" + idx + ") .chk_btn_img").hasClass("select")) {
                                selectScale.push(packageData[0][idx].keyWord);
                                selScaleIdxArr.push(idx);
                            }
                        }

                        if (selScaleIdxArr.length == packageData[0].length-1) {
                            if (!isChange) {
                                selectScale = [];
                                selScaleIdxArr = [];
                            }
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        } else {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div:eq(0) .chk_btn_img").removeClass("select");
                        }
                    }
                    break;
                case 1:
                    if (focusVerIdx == 0) {
                        if (div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").hasClass("select")) {
                            selectGenre = [];
                            selGenreIdxArr = [];
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                        } else {
                            selectGenre = [];
                            selGenreIdxArr = [];
                            if (isChange) {
                                for (var idx = 1; idx < packageData[1].length; idx++) {
                                    selectGenre.push(packageData[1][idx].keyWord);
                                    selGenreIdxArr.push(idx);
                                }
                            }
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                        }
                    } else {
                        for (var idx = 0 ; idx < selGenreIdxArr.length ; idx ++ ){
                            if (selGenreIdxArr[idx] == focusVerIdx) {
                                selectGenre.splice(idx, 1);
                                selGenreIdxArr.splice(idx, 1);
                                break;
                            }
                        }

                        if (div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").hasClass("select")) {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").removeClass("select");
                        } else {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + focusVerIdx + ") .chk_btn_img").addClass("select");
                        }

                        selectGenre = [];
                        selGenreIdxArr = [];

                        for (var idx = 1; idx < packageData[1].length ; idx ++) {
                            if (div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(" + idx + ") .chk_btn_img").hasClass("select")) {
                                selectGenre.push(packageData[1][idx].keyWord);
                                selGenreIdxArr.push(idx);
                            }
                        }

                        if (selGenreIdxArr.length == packageData[1].length-1) {
                            if (!isChange) {
                                selectGenre = [];
                                selGenreIdxArr = [];
                            }
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").addClass("select");
                        } else {
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div:eq(0) .chk_btn_img").removeClass("select");
                        }
                    }
                    break;
                case 2:
                    div.find(".listFilteringContent_1_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                    selectIdx[3] = _selectIdx;
                    div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(" + _selectIdx + ") .rdo_btn_img").addClass("select");
                    focusVerIdx = 0;
                    buttonFocusRefresh_VODPkg(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 4));
                    break;
                case 3:
                    switch (_selectIdx) {
                        case 0:
                            var tmpFilterData = "";
                            if (isChange && selectScale.length == 0) {
                                for (var idx = 1; idx < packageData[0].length ; idx ++) {
                                    if (idx == 1) {
                                        tmpFilterData += ("10:" + packageData[0][idx].keyWord);
                                    } else {
                                        tmpFilterData += (";" + packageData[0][idx].keyWord);
                                    }
                                }
                            } else {
                                for (var idx = 0; idx < selectScale.length; idx++) {
                                    if (idx == 0) {
                                        tmpFilterData += ("10:" + selectScale[idx]);
                                    } else {
                                        tmpFilterData += (";" + selectScale[idx]);
                                    }
                                }
                            }
                            if (isChange && selectGenre.length == 0) {
                                for (var idx = 1 ; idx < packageData[1].length ; idx ++) {
                                    if (idx == 1) {
                                        if (tmpFilterData.length > 0) tmpFilterData += ",";
                                        tmpFilterData += ("11:" + packageData[1][idx].keyWord);
                                    } else {
                                        tmpFilterData += (";" + packageData[1][idx].keyWord);
                                    }
                                }
                            } else {
                                for (var idx = 0; idx < selectGenre.length; idx++) {
                                    if (idx == 0) {
                                        if (tmpFilterData.length > 0) tmpFilterData += ",";
                                        tmpFilterData += ("11:" + selectGenre[idx]);
                                    } else {
                                        tmpFilterData += (";" + selectGenre[idx]);
                                    }
                                }
                            }

                            window.log.printDbg("[Set Package VOD Filter] : " + tmpFilterData);
                            if (params.complete(tmpFilterData, selectIdx[2], selectIdx[3])) {
                                SearchManager.setSelectVODFilterIndex(selectIdx);
                                LayerManager.historyBack();
                            } else {
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "SearchToast",
                                        type: Layer.TYPE.BACKGROUND,
                                        priority: Layer.PRIORITY.REMIND_POPUP,
                                        linkage: true,
                                        params: {
                                            text: "선택한 옵션으로 표시할 수 있는 콘텐츠가 없습니다"
                                        }
                                    },
                                    moduleId: "module.search",
                                    visible: true
                                })
                            }

                            break;
                        case 1:
                            selectIdx[0] = 1;
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                            selectScale = [];
                            selScaleIdxArr = [];
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(0) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            selectIdx[1] = 1;
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").removeClass("select");
                            selectGenre = [];
                            selGenreIdxArr = [];
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(1) .listFilteringContent_btn_div .chk_btn_img").addClass("select");
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                            selectIdx[3] = 0;
                            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(2) .listFilteringContent_btn_div:eq(" + selectIdx[3] + ") .rdo_btn_img").addClass("select");
                            focusVerIdx = 0;
                            focusRowIdx = 0;
                            buttonFocusRefresh_VODPkg(focusVerIdx, focusRowIdx);
                            break;
                        case 2:
                            LayerManager.historyBack();
                            break;
                    }
                    break;
            }
        }

        function buttonFocusRefresh_VODPkg(verIndex, rowIndex) {
            div.find(".listFilteringContent_1_area .listFilteringContent_div .listFilteringContent_btn_div").removeClass("focus");
            div.find(".listFilteringButton_1_area .listFilteringButton").removeClass("focus");
            switch (rowIndex) {
                case 0:
                case 1:
                case 2:
                    if (Math.floor(verIndex / 6) != curPage[focusRowIdx]) changePkgPage(Math.floor(verIndex / 6));
                    div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(" + rowIndex + ") .listFilteringContent_btn_div:eq(" + verIndex + ")").addClass("focus");
                    break;
                case 3:
                    div.find(".listFilteringButton_1_area .listFilteringButton:eq(" + verIndex + ")").addClass("focus");
                    break;
            }
        }

        function changePkgPage(page) {
            curPage[focusRowIdx] = page;
            div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(" + focusRowIdx + ") .listFilteringContent_btn_div").addClass("hide");
            for (var idx = (curPage[focusRowIdx] * 6); idx < 6 + (curPage[focusRowIdx] * 6); idx++) {
                div.find(".listFilteringContent_1_area .listFilteringContent_div:eq(" + focusRowIdx + ") .listFilteringContent_btn_div:eq(" + idx + ")").removeClass("hide")
            }
        }

        function vodPkgFilteringControlKey(key_code) {
            switch (key_code) {
                case KEY_CODE.LEFT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_VODPkg(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, -1, 4));
                    return true;
                case KEY_CODE.RIGHT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_VODPkg(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 4));
                    return true;
                case KEY_CODE.UP:
                    buttonFocusRefresh_VODPkg(focusVerIdx = HTool.getIndex(focusVerIdx, -1, (focusRowIdx != 3 ? packageData[focusRowIdx].length : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh_VODPkg(focusVerIdx = HTool.getIndex(focusVerIdx, 1, (focusRowIdx != 3 ? packageData[focusRowIdx].length : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEvent_VODPkg(focusVerIdx, focusRowIdx);
                    return true;
                case KEY_CODE.CONTEXT:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        }

// ======================================== TV프로그램 검색결과 필터처리
        function enterKeyEvent_TvProgram(_selectIdx, rowIndex) {
            if (rowIndex == 0) {
                selectIdx[0] = _selectIdx;
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + selectIdx[0] + ") .rdo_btn_img").addClass("select");
                focusVerIdx = 0;
                buttonFocusRefresh_TvProgram(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 2));
            } else {
                switch (_selectIdx) {
                    case 0:
                        SearchManager.setSelectTVProgramIndex(selectIdx[0]);
                        params.complete(selectIdx[0]);
                        LayerManager.historyBack();
                        break;
                    case 1:
                        selectIdx[0] = 0;
                        div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                        div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + selectIdx[0] + ") .rdo_btn_img").addClass("select");
                        focusVerIdx = 0;
                        focusRowIdx = 0;
                        buttonFocusRefresh_TvProgram(focusVerIdx, focusRowIdx);
                        break;
                    case 2:
                        LayerManager.historyBack();
                        break;
                }
            }
        }

        function buttonFocusRefresh_TvProgram(verIndex, rowIndex) {
            div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div").removeClass("focus");
            div.find(".listFilteringButton_2_area .listFilteringButton").removeClass("focus");
            switch (rowIndex) {
                case 0:
                    div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + verIndex + ")").addClass("focus");
                    break;
                case 1:
                    div.find(".listFilteringButton_2_area .listFilteringButton:eq(" + verIndex + ")").addClass("focus");
                    break;
            }
        }

        function tvProgramFilteringControlKey(key_code) {
            switch (key_code) {
                case KEY_CODE.LEFT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_TvProgram(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_TvProgram(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 2));
                    return true;
                case KEY_CODE.UP:
                    buttonFocusRefresh_TvProgram(focusVerIdx = HTool.getIndex(focusVerIdx, -1, (focusRowIdx == 0 ? 2 : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh_TvProgram(focusVerIdx = HTool.getIndex(focusVerIdx, 1, (focusRowIdx == 0 ? 2 : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEvent_TvProgram(focusVerIdx, focusRowIdx);
                    return true;
                case KEY_CODE.CONTEXT:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        }
        
// ======================================== 앱 검색결과
        function enterKeyEvent_TvApp(_selectIdx, rowIndex) {
            if (rowIndex == 0) {
                selectIdx[0] = _selectIdx;
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + selectIdx[0] + ") .rdo_btn_img").addClass("select");
                focusVerIdx = 0;
                buttonFocusRefresh_TvApp(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 2));
            } else {
                switch (_selectIdx) {
                    case 0:
                        SearchManager.setSelectTVAppIndex(selectIdx[0]);
                        params.complete(selectIdx[0]);
                        LayerManager.historyBack();
                        break;
                    case 1:
                        selectIdx[0] = 0;
                        div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div .rdo_btn_img").removeClass("select");
                        div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + selectIdx[0] + ") .rdo_btn_img").addClass("select");
                        focusVerIdx = 0;
                        focusRowIdx = 0;
                        buttonFocusRefresh_TvApp(focusVerIdx, focusRowIdx);
                        break;
                    case 2:
                        LayerManager.historyBack();
                        break;
                }
            }
        }

        function buttonFocusRefresh_TvApp(verIndex, rowIndex) {
            div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div").removeClass("focus");
            div.find(".listFilteringButton_2_area .listFilteringButton").removeClass("focus");
            switch (rowIndex) {
                case 0:
                    div.find(".listFilteringContent_2_area .listFilteringContent_div .listFilteringContent_btn_div:eq(" + verIndex + ")").addClass("focus");
                    break;
                case 1:
                    div.find(".listFilteringButton_2_area .listFilteringButton:eq(" + verIndex + ")").addClass("focus");
                    break;
            }
        }

        function tvAppFilteringControlKey(key_code) {
            switch (key_code) {
                case KEY_CODE.LEFT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_TvApp(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    focusVerIdx = 0;
                    buttonFocusRefresh_TvApp(focusVerIdx, focusRowIdx = HTool.getIndex(focusRowIdx, 1, 2));
                    return true;
                case KEY_CODE.UP:
                    buttonFocusRefresh_TvApp(focusVerIdx = HTool.getIndex(focusVerIdx, -1, (focusRowIdx == 0 ? 4 : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh_TvApp(focusVerIdx = HTool.getIndex(focusVerIdx, 1, (focusRowIdx == 0 ? 4 : 3)), focusRowIdx);
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEvent_TvApp(focusVerIdx, focusRowIdx);
                    return true;
                case KEY_CODE.CONTEXT:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        }

        this.controlKey = function (key_code) {
            if (POPUP_STATE == "VOD") {
                return vodFilteringControlKey(key_code);
            } else if (POPUP_STATE == "VOD_PACKAGE") {
                return vodPkgFilteringControlKey(key_code);
            } else if (POPUP_STATE == "TV_PROGRAM") {
                return tvProgramFilteringControlKey(key_code);
            } else if (POPUP_STATE == "TV_APP") {
                return tvAppFilteringControlKey(key_code);
            }
            return false;
        };
    };

    searchMenu.popup.listFiltering.prototype = new Layer();
    searchMenu.popup.listFiltering.prototype.constructor = searchMenu.popup.listFiltering;

    searchMenu.popup.listFiltering.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    searchMenu.popup.listFiltering.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["ListFilteringPopup"] = searchMenu.popup.listFiltering;
})();