/**
 * Created by Yun on 2017-06-10.
 */

(function() {
    var search_toast_popup = function SearchToastPopup(options) {
        Layer.call(this, options);
        var _this = this, data, closeTimeout;

        this.init = function (cbCreate) {
            this.div.addClass("arrange_frame vod search_toast");
            this.div.append(_$("<span/>", {class: "toastText"}));
            this.div.append(_$("<div/>", {class: "contextArea"}));
            this.div.find(".contextArea").append(_$("<div/>", {class: "contextIcon"}));
            this.div.find(".contextArea").append(_$("<div/>", {class: "contextText"}).text("자막 설정"));
            cbCreate(true);
        };

        this.show = function () {
            Layer.prototype.show.call(this);
            data = _this.getParams();
            if (data.isSMI) this.div.find(".contextArea").show();
            else this.div.find(".contextArea").hide();
            _this.div.find(".toastText").text(data.text);
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            closeTimeout = setTimeout(function () {
                LayerManager.deactivateLayer({
                    id: _this.id,
                    remove: true
                });
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }, 3000);
        };

        // 팝업이 닫힐 때
        this.hide = function () {
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            Layer.prototype.hide.call(this);
        };
    };

    search_toast_popup.prototype = new Layer();
    search_toast_popup.prototype.constructor = search_toast_popup;

    search_toast_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    search_toast_popup.prototype.handleKeyEvent = function () {
        return false;
    };

    arrLayer["SearchToast"] = search_toast_popup;
})();
