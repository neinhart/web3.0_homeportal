/**
 * Created by Yun on 2016-12-22.
 */

window.SearchList_Relation = function (instance) {
    var INSTANCE = instance;

    var resultData;
    var isFocus = false;
    var focusIdx = 0;
    var dataIdx = 0;
    var totalPage = 0;
    var curPage = 0;
    var indicator;

    var req_cd;

    this.setReqPath = function (path) {
        req_cd = path;
    };

    this.getListType = function () {
        return "RelWord";
    };

    this.getView = function () {
        var html = "<div id='relationListBox_area' class='searchListRelation'>" +
                "<table id='searchRelation_table'>";
        for (var idx = 0 ; idx < 9 ; idx ++) {
            html += "<tr><td>" +
                    "<div class='searchRelationList_cell'>" +
                    "<img class='searchRelationList_icon' src='module/module.search/resource/icon_option_search.png'>" +
                    "<div class='searchRelationList_title'></div>" +
                "</div>" +
                "</td></tr>";
        }
        html += "</table><div id='searchRelationList_focus_div'></div>";
        html += "<div class='indicatorArea'></div></div>";
        return html;
    };

    this.setData = function (data, filterData, _searchWord) {
        log.printDbg('called setData()');
        resultData = data;
        indicator = new Indicator(6);
        totalPage = Math.ceil(resultData.length/9);
        if (totalPage == 1) totalPage = 0;
        if (totalPage != 0) INSTANCE.div.find("#relationListBox_area.searchListRelation .indicatorArea").append(indicator.getView());
        indicator.setSize(totalPage, curPage);
        for (var idx = 0 ; idx < (resultData.length > 9 ? 9 : resultData.length) ; idx ++) {
            INSTANCE.div.find("#searchRelation_table .searchRelationList_title:eq(" + idx + ")").text(resultData[idx].sWords);
        }
    };

    this.show = function () {
        INSTANCE.div.find("#relationListBox_area.searchListRelation").removeClass("hide");
    };

    this.hide = function () {
        INSTANCE.div.find("#relationListBox_area.searchListRelation").addClass("hide");
    };

    this.setFocus = function () {
        log.printDbg('called setFocus()');
        isFocus = true;
        focusIdx = 0;
        dataIdx = 0;
        buttonFocusRefresh(focusIdx, dataIdx);
        if (totalPage > 0) {
            INSTANCE.div.find(".indicatorArea").addClass('show');
            INSTANCE.div.find("#searchResult_page_noti_area").addClass("show");
            INSTANCE.div.find('#searchRelation_table').addClass('reduce');
            INSTANCE.div.find("#searchRelationList_focus_div").addClass('reduce');
        }
    };

    this.setUnFocus = function () {
        log.printDbg('called setUnFocus()');
        isFocus = false;
        focusIdx = 0;
        dataIdx = 0;
        buttonFocusRefresh(focusIdx, dataIdx);
        if (totalPage > 0) {
            INSTANCE.div.find(".indicatorArea").removeClass('show');
            INSTANCE.div.find("#searchResult_page_noti_area").removeClass("show");
            INSTANCE.div.find('#searchRelation_table').removeClass('reduce');
            INSTANCE.div.find("#searchRelationList_focus_div").removeClass('reduce');
        }
    };

    this.getIsRelationButton = function () {
        return false;
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    function buttonFocusRefresh(index, dataIndex) {
        focusIdx = index;
        if (Math.floor(dataIndex / 9) != curPage) changePage(Math.floor(dataIndex / 9));
        INSTANCE.div.find("#searchRelation_table tr td .searchRelationList_cell").removeClass("focus");
        INSTANCE.div.find("#searchRelationList_focus_div").removeClass("focus");
        if (isFocus) {
            INSTANCE.div.find("#searchRelationList_focus_div").addClass("focus");
            INSTANCE.div.find("#searchRelationList_focus_div").css("-webkit-transform", "translateY(" + (focusIdx * 83) + "px)");
            INSTANCE.div.find("#searchRelation_table tr:eq("+ focusIdx + ") td .searchRelationList_cell").addClass("focus");
        }
    }

    function changePage(page) {
        curPage = page;
        // if (curPage == 0) focusIdx = dataIdx;
        // else if (focusIdx == 8) focusIdx = (totalPage-1 != curPage ? 8 : (resultData.length-1 - ((totalPage-1)*9)));
        var tagIdx = 0;
        for (var idx = (curPage * 9) ; idx < 9 + (curPage * 9) ; idx ++) {
            INSTANCE.div.find("#searchRelation_table .searchRelationList_title:eq(" + tagIdx + ")").text(resultData[idx] != null ? resultData[idx].sWords : "");
            tagIdx ++;
        }
        indicator.setPos(curPage);
    }

    this.onKeyAction = function (keyCode) {
        var keyAction = {isUsed: false};
        switch (keyCode) {
            case KEY_CODE.UP:
                if (dataIdx > 0) dataIdx--;
                focusIdx = dataIdx % 9;
                buttonFocusRefresh(focusIdx, dataIdx);
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.DOWN:
                dataIdx = (dataIdx < resultData.length - 1) ? dataIdx + 1 : 0;
                focusIdx = dataIdx % 9;
                buttonFocusRefresh(focusIdx, dataIdx);
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.RED:
                if (dataIdx%9 === 0) {
                    if (resultData.length > 9) {
                        if (curPage > 0) {
                            curPage --;
                        }
                        changePage(curPage);
                        dataIdx = curPage * 9;
                        focusIdx = dataIdx % 9;
                        buttonFocusRefresh(focusIdx, dataIdx);
                    }
                } else {
                    dataIdx = curPage * 9;
                    focusIdx = dataIdx % 9;
                    buttonFocusRefresh(focusIdx, dataIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BLUE:
                if (dataIdx === resultData.length-1 || dataIdx%9 === 8) {
                    if (resultData.length > 9) {
                        curPage = (totalPage-1 > curPage) ? curPage + 1: 0;
                        changePage(curPage);
                        dataIdx = Math.min(resultData.length-1, curPage * 9 + 8);
                        focusIdx = dataIdx % 9;
                        buttonFocusRefresh(focusIdx, dataIdx);
                    }
                } else {
                    dataIdx = Math.min(resultData.length-1, curPage * 9 + 8);
                    focusIdx = dataIdx % 9;
                    buttonFocusRefresh(focusIdx, dataIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.LEFT:
                // if (isListFocus && totalPage > 0) {
                //     isListFocus = false;
                //     indicator.focused();
                //     focusIdx = 0;
                //     INSTANCE.div.find("#searchRelationList_focus_div").removeClass("focus");
                //     INSTANCE.div.find("#searchRelation_table tr td .searchRelationList_cell").removeClass("focus");
                //     keyAction.prevMenu = false;
                // } else {
                //     indicator.blurred();
                //     focusIdx = 0;
                //     dataIdx = 0;
                //     buttonFocusRefresh(focusIdx, dataIdx);
                //     keyAction.prevMenu = true;
                // }
                keyAction.prevMenu = true;
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BACK:
                keyAction.prevMenu = true;
                keyAction.isUsed = true;
                // if (isListFocus && totalPage > 0) {
                //     isListFocus = false;
                //     indicator.focused();
                //     focusIdx = 0;
                //     INSTANCE.div.find("#searchRelationList_focus_div").removeClass("focus");
                //     INSTANCE.div.find("#searchRelation_table tr td .searchRelationList_cell").removeClass("focus");
                //     INSTANCE.div.find("#searchRelation_table tr td .searchRelationList_title").removeClass("focus");
                //     keyAction.prevMenu = false;
                // } else {
                //     indicator.blurred();
                //     focusIdx = 0;
                //     dataIdx = 0;
                //     buttonFocusRefresh(focusIdx, dataIdx);
                //     keyAction.prevMenu = true;
                // }
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.ENTER:
                var searchWord = resultData[dataIdx].sWords;
                SearchManager.searchTOTAL(function (data) {
                    SearchManager.setSearchType(3);
                    SearchManager.setSearchQuery("");
                    var layers = LayerManager.getLayers("SearchResult");
                    LayerManager.activateLayer({
                        obj: {
                            id: "SearchResult",
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            params: {
                                search_Data: data,
                                search_Words: searchWord,
                                req_cd: "04"
                            }
                        },
                        moduleId: "module.search",
                        visible: true,
                        new: true
                    });
                    for (var i = 0; i < layers.length; i++) {
                        LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
                    }
                }, function () {
                    LayerManager.activateLayer({
                        obj: {
                            id: "SearchError",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                title: "오류",
                                message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                                button: "닫기",
                                reboot: false,
                                callback: function () {
                                }
                            }
                        },
                        moduleId: "module.search",
                        visible: true
                    });
                }, true, "0xFF", 3, false, 0, 30, "0x09", searchWord, "", "POINT", 2, "Y", "", "0.5", "");

                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };
};