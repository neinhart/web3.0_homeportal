/**
 * Created by Yun on 2016-12-28.
 */


window.SearchBottomMenuList = function (instance) {
    var INSTANCE = instance;
    var isFocus = false;
    var focusIdx = 0;
    var menuCellIdx = 0;
    var listLen = 0;
    var totalPage = 0;
    var curPage = 0;
    var totalData = 0;

    var listTitle = [];
    var listCont = [];

    this.getView = function () {
        var html = "<ul id='bottomList_area'></ul>" +
            "<div id='bottomList_page_area'></div>";
        return html;
    };

    this.show = function () {
        INSTANCE.div.find("#bottomMenu_Bg").removeClass("hide");
        INSTANCE.div.find("#bottomList_area").removeClass("hide");
        INSTANCE.div.find("#bottomList_page_area").removeClass("hide");
        INSTANCE.div.find("#bottomList_page_area").css("left", 960 - (INSTANCE.div.find("#bottomList_page_area").width() / 2) + "px");
        curPage = 0;
        changePage(curPage);
    };

    this.hide = function () {
        INSTANCE.div.find("#bottomMenu_Bg").addClass("hide");
        INSTANCE.div.find("#bottomList_area").addClass("hide");
        INSTANCE.div.find("#bottomList_page_area").addClass("hide");
    };

    this.pause = function () {
        clearTextAnimation(INSTANCE.div);
    };

    this.resume = function () {
        if(isFocus) {
            menuFocusRefresh(focusIdx, menuCellIdx);
            INSTANCE.div.find("#bottomList_area .bottomItem_div").removeClass("notCurPage");
            INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + (listCont[0].length == 0 ? (curPage * 4 + 5): (curPage * 4 + 4)) + ")").addClass("notCurPage");
        }
    };

    this.setFocus = function () {
        isFocus = true;
        focusIdx = 0;
        menuCellIdx = (listCont[0].length == 0 ? 1 : 0);
        INSTANCE.div.find("#bottomList_area").addClass("focus");
        menuFocusRefresh(focusIdx, menuCellIdx);
    };

    this.setUnFocus = function () {
        isFocus = false;
        INSTANCE.div.find("#bottomList_area").removeClass("focus");
        menuFocusRefresh(focusIdx, menuCellIdx);
        resetTextAnimation(INSTANCE.div);
        changePage(0);
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    this.listAgeReset = function () {
        totalData = 0;
        for (var idx = 0; idx < listTitle.length; idx++) {
            if (idx == 0) {
                totalData++;
            } else {
                // if (listCont[idx].length > 0) {
                for (var z = 0; z < listCont[idx].length; z++) {
                    if (parseInt(listTitle[idx].RANK_CODE) >= 10) {
                        if (parseInt(listTitle[idx].RANK_CODE) == 30) {
                            if (z > 2) break;
                        } else {
                            if (z > 1) break;
                            if (window.UTIL.isLimitAge(listCont[idx][z].RATING)) {
                                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).find(".vodPoster_lock_img").css("display", "block");
                            } else {
                                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).find(".vodPoster_lock_img").css("display", "none");
                            }
                        }
                    } else {
                    }
                }
                totalData++;
                // }
            }
        }
    };

    this.setDataInit = function () {
        // var html = "";
        for (var idx = 0; idx < listTitle.length; idx++) {
            if (idx == 0) {
                INSTANCE.div.find("#bottomList_area").append(_$("<li/>", {class: "bottomItem_div"}));
                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(idx).append(_$("<div/>", {class: "bottomItem_title"}).text(listTitle[idx].RANK_NAME));
                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(idx).append(_$("<div/>", {class: "bottomItem_deleteIcon_area"}));
                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(idx).find(".bottomItem_deleteIcon_area").append(_$("<img/>", {
                    class: "deleteIcon_img",
                    src: "module/module.search/resource/key_r.png"
                }));
                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(idx).find(".bottomItem_deleteIcon_area").append(_$("<span/>", {class: "deleteIcon_text"}).text("삭제"));
                for (var z = 0; z < 4; z++) {
                    INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(idx).append(_$("<div/>", {class: "textList_title"}));
                }
                totalData++;
            } else {
                // if (listCont[idx].length > 0) {
                INSTANCE.div.find("#bottomList_area").append(_$("<li/>", {class: "bottomItem_div"}));
                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).append(_$("<div/>", {class: "bottomItem_title"}).text(listTitle[idx].RANK_NAME));
                for (var z = 0; z < listCont[idx].length; z++) {
                    if (parseInt(listTitle[idx].RANK_CODE) >= 10) {
                        if (parseInt(listTitle[idx].RANK_CODE) == 30) {
                            if (z > 2) break;
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).append(_$("<div/>", {class: "appCell_div"}));
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".appCell_div").eq(z).append(_$("<img/>", {
                                class: "appList_img",
                                src: listCont[idx][z].IMG_URL
                            }));
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".appCell_div").eq(z).append(_$("<div/>", {class: "appTitle_text"}).html("<span>" + listCont[idx][z].SEARCH_WORD + "</span>"));
                        } else {
                            if (z > 1) break;
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).append(_$("<div/>", {class: "vodCell_div"}));
                            if (listCont[idx][z].IMG_URL) {
                                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).append(_$("<img/>", {
                                    class: "vodPoster_img",
                                    src: listCont[idx][z].IMG_URL + "?w=98&h=140&quality=90",
                                    onerror: "this.src='" + window.modulePath + "resource/default_poster.png'"
                                }));
                            } else {
                                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).append(_$("<img/>", {
                                    class: "vodPoster_img",
                                    src: window.modulePath + "resource/default_poster.png"
                                }));
                            }
                            if (window.UTIL.isLimitAge(listCont[idx][z].RATING)) {
                                INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).append(_$("<img/>", {
                                    class: "vodPoster_lock_img",
                                    src: window.modulePath + "resource/img_vod_locked.png"
                                }));
                            }
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).append(_$("<div/>", {class: "vodPoster_img_dim"}));
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).append(_$("<div/>", {class: "vodTitle_text"}).html("<span>" + listCont[idx][z].SEARCH_WORD + "</span>"));
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).append(_$("<div/>", {class: "vodPriceType_text"}).html((HTool.isTrue(listCont[idx][z].WON_YN) ? ("<img class='isFreeIcon_img' src='" + window.modulePath + "resource/icon_buy.png'>") : (HTool.isFalse(listCont[idx][z].WON_YN) ? "무료" : ""))));
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).find(".vodPriceType_text").append(_$("<img/>", {
                                class: "vodAge_img",
                                src: "module/module.search/resource/icon_age_list_" + ServerCodeAdapter.getPrInfo(listCont[idx][z].RATING - 0) + ".png"
                            }));
                            INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z).find(".vodPriceType_text").prepend(stars.getView(stars.TYPE.RED_20));
                            stars.setRate(INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).find(".vodCell_div").eq(z), listCont[idx][z].OLLEHP);
                        }
                    } else {
                        INSTANCE.div.find("#bottomList_area .bottomItem_div").eq(totalData).append(_$("<div/>", {class: "textList_title"}).html("<div class='rankNum'>" + (z + 1) + "</div><div class='rankTitle_div'><span>" + listCont[idx][z].SEARCH_WORD + "</span></div>"));
                    }
                }
                totalData++;
                // }
            }
        }
        totalPage = Math.ceil(totalData / 4);
        if (totalPage == 1) {
            totalPage = 0;
            INSTANCE.div.find("#bottomList_page_area #downArrow_img").css("visibility", "hidden");
        } else {
            INSTANCE.div.find("#bottomList_page_area #downArrow_img").css("visibility", "inherit");
        }
        INSTANCE.div.find("#bottomList_area").css("width", (440 * listTitle.length) + "px");
        for (var idx = 0; idx < (totalData % 4 == 1 ? totalPage - 1 : totalPage); idx++) {
            INSTANCE.div.find("#bottomList_page_area").append(_$("<div/>", {class: "bottomList_page_default"}));
        }
        INSTANCE.div.find("#bottomList_page_area .bottomList_page_default:eq(0)").addClass("focus");
    };

    this.setListData = function (_data) {
        totalData = 0;
        var tmpTitleData = [{RANKWORD_CNT: 4, RANK_CODE: "-1", RANK_NAME: "최근 검색어", RANK_TYPE: "-1"}];
        var tmpData = _data.getTrendWorsResult.rank_LIST;
        var tmpListData = _data.getTrendWorsResult.rankword_LIST;
        tmpData.sort(function (a, b) {
            return a["RANK_CODE"] - b["RANK_CODE"];
        });
        listTitle = tmpTitleData.concat(tmpData);
        listCont = [];
        var tmpNum
        for (var idx = 0; idx < listTitle.length; idx++) {
            tmpNum = parseInt(listTitle[idx].RANK_CODE);
            if (tmpNum > 0 && tmpNum < 10) {
                listTitle.splice(idx, 1);
            } else if (listTitle[idx].RANK_CODE == "99") {
                listTitle.splice(idx, 1);
            }
        }
        for (var i = 0; i < listTitle.length; i++) {
            listCont[i] = [];
            if (i > 0) {
                for (var j = 0; j < tmpListData.length; j++) {
                    if (listTitle[i].RANK_TYPE == tmpListData[j].RANK_TYPE) {
                        listCont[i].push(tmpListData[j]);
                    }
                }
            }
        }
        for (var idx = 0; idx < listTitle.length; idx++) {
            if (idx > 0 && listCont[idx].length == 0) {
                listTitle.splice(idx, 1);
                listCont.splice(idx, 1);
            }
        }
        curPage = 0;
        this.setDataInit();
    };

    this.setRecentSearchWord = function (_data) {
        listCont[0] = _data;
        INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(0)").addClass("hide");
        INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(0) .textList_title").text("");
        if (listCont[0].length > 0) {
            INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(0)").removeClass("hide");
            var tmpIdx = 0;
            var listContLen = listCont[0].length;
            for (var idx = (listContLen - 1); idx >= 0; idx--) {
                INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(0) .textList_title:eq(" + tmpIdx + ")").html("<div class='rankTitle_div'><span>" + listCont[0][idx].searchWord + "</span></div>");
                tmpIdx++;
                if (tmpIdx == 4) break;
            }
            INSTANCE.div.find("#bottomList_area").css("width", (500 * listTitle.length) + "px");
            INSTANCE.div.find("#bottomList_page_area").empty();
            for (var idx = 0; idx < totalPage; idx++) {
                INSTANCE.div.find("#bottomList_page_area").append(_$("<div/>", {class: "bottomList_page_default"}));
            }
        }
        INSTANCE.div.find("#bottomList_page_area .bottomList_page_default:eq(" + curPage + ")").addClass("focus");
        INSTANCE.div.find("#bottomList_page_area").css("left", 960 - (INSTANCE.div.find("#bottomList_page_area").width() / 2) + "px");
        // changePage(0);
    };

    function menuFocusRefresh(index, cellIdx) {
        focusIdx = index;
        INSTANCE.div.find("#bottomList_area .bottomItem_div .textList_title").removeClass("focus");
        INSTANCE.div.find("#bottomList_area .bottomItem_div .vodCell_div").removeClass("focus");
        INSTANCE.div.find("#bottomList_area .bottomItem_div .appCell_div").removeClass("focus");
        INSTANCE.div.find("#bottomList_area .bottomItem_div .vodCell_div .vodPriceType_text").removeClass("focus");
        INSTANCE.div.find("#bottomList_area .bottomItem_div .bottomItem_deleteIcon_area").removeClass("show");

        if (isFocus) {
            var isApp = false;
            if (parseInt(listTitle[cellIdx].RANK_CODE) >= 10) {
                if (parseInt(listTitle[cellIdx].RANK_CODE) == 30) {
                    listLen = 3;
                    INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + cellIdx + ") .appCell_div:eq(" + index + ")").addClass("focus");
                    isApp = true;
                } else {
                    listLen = 2;
                    INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + cellIdx + ") .vodCell_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + cellIdx + ") .vodCell_div:eq(" + index + ") .vodPriceType_text").addClass("focus");
                    isApp = false;
                }
                setTextAnimation(INSTANCE.div, index, cellIdx, isApp);
            } else {
                listLen = listCont[menuCellIdx].length;
                INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + cellIdx + ") .textList_title:eq(" + index + ")").addClass("focus");
                if (cellIdx == 0) {
                    INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + cellIdx + ") .bottomItem_deleteIcon_area").addClass("show");
                }
                setKeywordTextAnimation(INSTANCE.div, index, cellIdx);
            }
            if (listCont[0].length == 0) cellIdx--;
            if (Math.floor(cellIdx / 4) != curPage) changePage(Math.floor(cellIdx / 4));
        }
    }

    function resetTextAnimation(div) {
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .textList_title .textAnimating span"));
        div.find("#bottomList_area .bottomItem_div .textList_title .textAnimating").removeClass("textAnimating");
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .appCell_div .textAnimating span"));
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .vodCell_div .textAnimating span"));
        div.find("#bottomList_area .bottomItem_div .appCell_div .textAnimating").removeClass("textAnimating");
        div.find("#bottomList_area .bottomItem_div .vodCell_div .textAnimating").removeClass("textAnimating");
    }

    function setKeywordTextAnimation(div, focus, cellIndex) {
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .textList_title .textAnimating span"));
        div.find("#bottomList_area .bottomItem_div .textList_title .textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        var tmpText = cellIndex == 0 ? listCont[0][(listCont[0].length - 1)-focus].searchWord : listCont[cellIndex][focus].SEARCH_WORD;
        if(UTIL.getTextLength(tmpText, "RixHead M", 39, -2)> (cellIndex == 0 ? 370 : 310)) {
            var posterDiv = div.find("#bottomList_area .bottomItem_div:eq(" + cellIndex + ") .textList_title:eq(" + focus + ") .rankTitle_div");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    function setTextAnimation(div, focus, cellIndex, isApp) {
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .appCell_div .textAnimating span"));
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .vodCell_div .textAnimating span"));
        div.find("#bottomList_area .bottomItem_div .appCell_div .textAnimating").removeClass("textAnimating");
        div.find("#bottomList_area .bottomItem_div .vodCell_div .textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        var tmpText = listCont[cellIndex][focus].SEARCH_WORD;
        if(UTIL.getTextLength(tmpText, "RixHead M", 30)>215) {
            var posterDiv = div.find("#bottomList_area .bottomItem_div:eq(" + cellIndex + ") " + (isApp? ".appCell_div":".vodCell_div") + ":eq(" + focus + ") " + (isApp ? ".appTitle_text":".vodTitle_text"));
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    function clearTextAnimation(div) {
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .textList_title .textAnimating span"));
        div.find("#bottomList_area .bottomItem_div .textList_title .textAnimating").removeClass("textAnimating");
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .appCell_div .textAnimating span"));
        UTIL.clearAnimation(div.find("#bottomList_area .bottomItem_div .vodCell_div .textAnimating span"));
        div.find("#bottomList_area .bottomItem_div .appCell_div .textAnimating").removeClass("textAnimating");
        div.find("#bottomList_area .bottomItem_div .vodCell_div .textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
    }

    function changePage(page) {
        curPage = page;
        if (curPage == 0) INSTANCE.div.find("#bottomList_area .bottomItem_div").removeClass("hideDiv");

        INSTANCE.div.find("#bottomList_area .bottomItem_div").removeClass("notCurPage");
        if (curPage != 0) INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + (listCont[0].length == 0 ? (page * 4) : (page * 4 -1)) + ")").addClass("hideDiv");
        INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + (listCont[0].length == 0 ? (page * 4 + 5): (page * 4 + 4)) + ")").addClass("notCurPage");
        INSTANCE.div.find("#bottomList_page_area .bottomList_page_default").removeClass("focus");
        INSTANCE.div.find("#bottomList_page_area .bottomList_page_default:eq(" + page + ")").addClass("focus");
        INSTANCE.div.find("#bottomList_area").css("-webkit-transform", "translateX(" + (-page * 1771) + "px) translateZ(0px)");
    }

    this.onKeyAction = function (key_code) {
        var keyAction = {inUsed: false, isPrevKey: false};
        switch (key_code) {
            case KEY_CODE.UP:
                keyAction.isUsed = true;
                if (focusIdx == 0) {
                    keyAction.prevMenu = true;
                } else {
                    keyAction.prevMenu = false;
                }
                menuFocusRefresh(HTool.getIndex(focusIdx, -1, listLen), menuCellIdx);
                return keyAction;
            case KEY_CODE.DOWN:
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                menuFocusRefresh(HTool.getIndex(focusIdx, 1, listLen), menuCellIdx);
                return keyAction;
            case KEY_CODE.LEFT:
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                focusIdx = 0;
                menuCellIdx = HTool.getIndex(menuCellIdx, -1, totalData);
                if (menuCellIdx == 0 && listCont[menuCellIdx].length == 0) menuCellIdx = HTool.getIndex(menuCellIdx, -1, totalData);
                menuFocusRefresh(focusIdx, menuCellIdx);
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                focusIdx = 0;
                menuCellIdx = HTool.getIndex(menuCellIdx, 1, totalData);
                if (menuCellIdx == 0 && listCont[menuCellIdx].length == 0) menuCellIdx = HTool.getIndex(menuCellIdx, 1, totalData);
                menuFocusRefresh(focusIdx, menuCellIdx);
                return keyAction;
            case KEY_CODE.ENTER:
                if (parseInt(listTitle[menuCellIdx].RANK_CODE) >= 10) {
                    if (parseInt(listTitle[menuCellIdx].RANK_CODE) == 30) {
                        window.AppServiceManager.changeService({
                            nextServiceState: window.CONSTANT.SERVICE_STATE.OTHER_APP,
                            obj: {
                                type: window.CONSTANT.APP_TYPE.UNICAST,
                                param: listCont[menuCellIdx][focusIdx].CONST_ID
                            }
                        });
                    } else {
                        try {
                            NavLogMgr.collectJumpVOD(NLC.JUMP_START_SEARCH, listCont[menuCellIdx][focusIdx].CATEGORY_ID, listCont[menuCellIdx][focusIdx].CONST_ID, "48");
                        } catch(e) {}
                        openDetailLayer(listCont[menuCellIdx][focusIdx].CATEGORY_ID, listCont[menuCellIdx][focusIdx].CONST_ID, "48");
                    }
                } else {
                    LayerManager.startLoading({preventKey:true});
                    var searchWord = "";
                    var searchData = {constType:"", searchWord:"", wordType:"", searchQry:""};
                    switch (menuCellIdx) {
                        case 0:
                            searchWord = listCont[menuCellIdx][(listCont[0].length - 1) - focusIdx].searchWord;
                            searchData = listCont[menuCellIdx][(listCont[0].length - 1) - focusIdx];
                            SearchManager.addRecentSearchWord({constType:searchData.constType, searchWord:searchWord, wordType:searchData.wordType, searchQry:searchData.searchQry});
                            focusIdx = 0;
                            break;
                        default:
                            searchWord = listCont[menuCellIdx][focusIdx].SEARCH_WORD;
                            searchData.constType = "0xFF";
                            searchData.wordType = listCont[menuCellIdx][focusIdx].WORD_TYPE;
                            searchData.searchQry = "";
                            SearchManager.addRecentSearchWord({constType:"0XFF", searchWord:searchWord, wordType:searchData.wordType, searchQry: ""});
                            break;
                    }
                    SearchManager.searchTOTAL(function(data) {
                        SearchManager.setSearchType(searchData.wordType);
                        SearchManager.setSearchQuery(searchData.searchQry);
                        var searchResult_Data = data;
                        var layers = LayerManager.getLayers("SearchResult");
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchResult",
                                type: Layer.TYPE.NORMAL,
                                priority: Layer.PRIORITY.NORMAL,
                                params: {
                                    search_Data: searchResult_Data,
                                    search_Words: searchWord
                                }
                            },
                            moduleId: "module.search",
                            visible: true,
                            new: true
                        });
                        for (var i = 0; i < layers.length; i++) {
                            LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
                        }
                        menuFocusRefresh(focusIdx, menuCellIdx);
                    }, function() {
                        LayerManager.stopLoading();
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchError",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    title: "오류",
                                    message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                                    button: "닫기",
                                    reboot: false
                                    // callback: _error
                                }
                            },
                            moduleId: "module.search",
                            visible: true
                        });
                    }, true, searchData.constType, searchData.wordType, false, 0, 30, "0x09", searchWord, searchData.searchQry, "POINT", 2, "Y", "", "0.5", "");
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.RED:
                if (menuCellIdx == 0) {
                    resetTextAnimation(INSTANCE.div);
                    SearchManager.setRecentSearchWordDelete((listCont[0].length - 1) - focusIdx);
                    this.setRecentSearchWord(SearchManager.getAllRecentSearchWord());
                    listLen = listCont[0].length;
                    if (listCont[0].length == 0) {
                        keyAction.prevMenu = false;
                        focusIdx = 0;
                        menuCellIdx = HTool.getIndex(menuCellIdx, 1, totalData);
                        menuFocusRefresh(focusIdx, menuCellIdx);
                        INSTANCE.div.find("#bottomList_area .bottomItem_div").removeClass("notCurPage");
                        INSTANCE.div.find("#bottomList_area .bottomItem_div:eq(" + (listCont[0].length == 0 ? (curPage * 4 + 5): (curPage * 4 + 4)) + ")").addClass("notCurPage");
                    } else {
                        keyAction.prevMenu = false;
                        if (focusIdx == listCont[0].length) {
                            menuFocusRefresh(HTool.getIndex(focusIdx, -1, listLen), menuCellIdx);
                        } else {
                            menuFocusRefresh(focusIdx, menuCellIdx);
                        }
                    }
                }
                keyAction.isUsed = true;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9) {
                    keyAction.prevMenu = false;
                    keyAction.isUsed = true;
                }
                return keyAction;
        }
    }
};