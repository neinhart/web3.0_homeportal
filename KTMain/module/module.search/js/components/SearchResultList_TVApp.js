/**
 * Created by Yun on 2016-12-22.
 */

window.SearchList_TVApp = function (instance) {
    var INSTANCE = instance;

    var resultData;
    var isFocus = false;
    var focusIdx = 0;

    var req_cd;
    var indicator;
    var totalPage = 0;
    var curPage = 0;
    var isListFocus = false;
    var searchWord = "";

    this.setReqPath = function (path) {
        req_cd = path;
    };

    this.getListType = function () {
        return "TVApp";
    };

    this.getView = function () {
        var html = "<div id='listBox_area' class='searchListTVApp'>" +
                "<div class='filteringSetting_view_area'></div>" +
                "<div class='indicatorArea'></div>" +
            "<div id='searchListTVApp_area'>";
        // for (var idx = 0 ; idx < 6 ; idx ++) {
        //     html += "<div class='searchListTVApp_item'>" +
        //             "<img class='searchListTVApp_img' src=''>" +
        //             "<div class='searchListTVApp_title'></div>" +
        //         "</div>";
        // }
        html += "</div></div>";

        return html;
    };

    this.setData = function (data, filterData, _searchWord) {
        resultData = data;
        searchWord = _searchWord;
        indicator = new Indicator(6);
        totalPage = Math.ceil(resultData.length/6);
        if (totalPage == 1) totalPage = 0;
        if (totalPage != 0) INSTANCE.div.find("#listBox_area.searchListTVApp .indicatorArea").append(indicator.getView());
        indicator.setSize(totalPage, curPage);
        for (var idx = 0 ; idx < resultData.length ; idx ++) {
            INSTANCE.div.find("#searchListTVApp_area").append(_$("<div/>", {class: "searchListTVApp_item"}));
            INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + idx + ")").append(_$("<img>", {class: "searchListTVApp_img", src: resultData[idx].imgURL}));
            INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + idx + ")").append(_$("<div/>", {class: "searchListTVApp_title"}).text(resultData[idx].appName));

            if (idx > 5) INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + idx + ")").css("display", "none");
        }

        INSTANCE.div.find(".searchListTVApp .filteringSetting_view_area").html(
            "<span class='sortSet_txt'>정렬 : 인기도순</span>"
        );
    };

    this.show = function () {
        var filterIdx = SearchManager.getSelectTVAppIndex();
        var filterText = filterIdx == 0 ? "인기도순":(filterIdx == 1?"가나다순":(filterIdx == 2 ? "추천순":"최신순"));
        INSTANCE.div.find("#listBox_area.searchListTVApp").removeClass("hide");
    };

    this.hide = function () {
        INSTANCE.div.find("#listBox_area.searchListTVApp").addClass("hide");
    };

    this.setFocus = function () {
        isFocus = true;
        focusIdx = 0;
        buttonFocusRefresh(focusIdx);
        if (totalPage > 0) {
            INSTANCE.div.find("#searchResult_page_noti_area").addClass("show");
            INSTANCE.div.find(".indicatorArea").addClass('show');
        }
    };

    this.setUnFocus = function () {
        isFocus = false;
        focusIdx = 0;
        buttonFocusRefresh(focusIdx);
        INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item").removeClass("focus");
        if (totalPage > 0) {
            INSTANCE.div.find("#searchResult_page_noti_area").removeClass("show");
            INSTANCE.div.find(".indicatorArea").removeClass('show');
        }
    };

    this.getIsRelationButton = function () {
        return true;
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    function buttonFocusRefresh(index) {
        focusIdx = index;
        if (Math.floor(index / 6) != curPage) changePage(Math.floor(index / 6));
        INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item").removeClass("focus");
        INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + focusIdx + ")").addClass("focus");
        INSTANCE.div.find(".searchListTVApp .filteringSetting_view_area").removeClass("show");
        if (isFocus) INSTANCE.div.find(".searchListTVApp .filteringSetting_view_area").addClass("show");
    }

    function changePage(page) {
        curPage = page;
        // var tagIdx = 0;
        // for (var idx = (curPage * 6) ; idx < 6 + (curPage * 6) ; idx ++) {
        //     if (resultData[idx] != null) {
        //         INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + tagIdx + ") .searchListTVApp_img").attr("src", resultData[idx].imgURL);
        //         INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + tagIdx + ") .searchListTVApp_img").css("height", "222px");
        //         INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + tagIdx + ") .searchListTVApp_title").text(resultData[idx].appName);
        //     } else {
        //         INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + tagIdx + ") .searchListTVApp_img").attr("src", "");
        //         INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + tagIdx + ") .searchListTVApp_img").css("height", "0px");
        //         INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + tagIdx + ") .searchListTVApp_title").text("");
        //     }
        //     tagIdx ++;
        // }
        INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item").css("display", "none");
        for (var idx = (curPage * 6) ; idx < 6 + (curPage * 6) ; idx ++) {
            INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + idx + ")").css("display", "block");
        }
        indicator.setPos(curPage);
    }

    function filteringDataResetting(_value) {
        var tmpData = _$(_value).find("search").find("APP_ITEM");
        resultData = [];
        tmpData.each(function () {
            resultData.push({appName:_$(this).find("APP_NAME").text(),
                imgURL:_$(this).find("URL").text(),
                appType:_$(this).find("APP_TYPE_CD").text(),
                launchYN:_$(this).find("LAUNCH_YN").text(),
                cat_Id:_$(this).find("CATEGORY_ID").text(),
                cid:_$(this).find("CID").text(),
                assetId:_$(this).find("ASSET_ID").text(),
                pkgInitPath:_$(this).find("PKG_INIT_PATH").text()});
        });
        for (var idx = 0 ; idx < resultData.length ; idx ++) {
            INSTANCE.div.find("#searchListTVApp_area").append(_$("<div/>", {class: "searchListTVApp_item"}));
            INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + idx + ") .searchListTVApp_img").attr("src", resultData[idx].imgURL);
            INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item:eq(" + idx + ") .searchListTVApp_title").text(resultData[idx].appName);
        }
    }

    function gotoLocation() {
        if (resultData[focusIdx].launchYN == "Y") {
            var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            window.AppServiceManager.changeService({
                nextServiceState: nextState,
                obj :{
                    type: window.CONSTANT.APP_TYPE.UNICAST,
                    param: resultData[focusIdx].assetId
                }
            });
        } else {
            window.AppServiceManager.changeService({
                nextServiceState: window.CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: window.CONSTANT.APP_TYPE.MULTICAST,
                    param: resultData[focusIdx].pkgInitPath
                }
            });
        }
    }

    this.onKeyAction = function (keyCode) {
        var keyAction = {isUsed: false};
        switch (keyCode) {
            case KEY_CODE.UP:
                if (focusIdx >= 3) buttonFocusRefresh(focusIdx - 3);
                else buttonFocusRefresh(Math.max(resultData.length - 1, HTool.getIndex(focusIdx, -3, resultData.length)));
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.DOWN:
                if (Math.floor(focusIdx / 3) == Math.floor((resultData.length - 1) / 3)) buttonFocusRefresh((focusIdx % 3 == 1 ? 0 : (focusIdx % 3 == 2 ? 1 : 0)));
                else buttonFocusRefresh(Math.min(resultData.length - 1, focusIdx + 3));
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.RED:
                if (focusIdx%6===0) {
                    if (resultData.length > 6) {
                        if (curPage > 0) {
                            curPage --;
                        }
                        changePage(curPage);
                        focusIdx = curPage * 6;
                        buttonFocusRefresh(focusIdx);
                    }
                } else {
                    focusIdx = curPage * 6;
                    buttonFocusRefresh(focusIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BLUE:
                if (focusIdx === resultData.length - 1 || focusIdx % 6 === 5) {
                    if (resultData.length > 6) {
                        changePage(HTool.getIndex(curPage, 1, totalPage));
                        focusIdx = Math.min(resultData.length -1, curPage * 6 + 5);
                        buttonFocusRefresh(focusIdx);
                    }
                } else {
                    focusIdx = Math.min(resultData.length -1, curPage * 6 + 5);
                    buttonFocusRefresh(focusIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.LEFT:
                if (focusIdx%3 != 0) {
                    buttonFocusRefresh(HTool.getIndex(focusIdx, -1, resultData.length));
                    keyAction.prevMenu = false;
                } else {
                    keyAction.prevMenu = true;
                }
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.RIGHT:
                buttonFocusRefresh(HTool.getIndex(focusIdx, 1, resultData.length));
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.ENTER:
                gotoLocation();
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BACK:
                // if (isListFocus && totalPage > 0) {
                //     isListFocus = false;
                //     indicator.focused();
                //     focusIdx = 0;
                //     INSTANCE.div.find("#searchListTVApp_area .searchListTVApp_item").removeClass("focus");
                //     INSTANCE.div.find(".searchListTVApp .filteringSetting_view_area").removeClass("show");
                //     keyAction.prevMenu = false;
                // } else {
                //     indicator.blurred();
                //     focusIdx = 0;
                //     isListFocus = false;
                //     buttonFocusRefresh(focusIdx);
                //     keyAction.prevMenu = true;
                // }
                keyAction.prevMenu = true;
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.CONTEXT:
                LayerManager.activateLayer({
                    obj : {
                        id: "ListFilteringPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            popType : "TV_APP",
                            complete : function (lineup) {
                                var searchResult_Data;
                                var isConnectError = false;
                                switch (lineup) {
                                    case 0:
                                        SearchManager.searchTOTAL(function(data) {
                                            searchResult_Data = data;
                                        }, function() {
                                            isConnectError = true;
                                            searchError();
                                        }, false, "0x04", 3, false, 0, 30, "0x09", searchWord, "", "POINT", 2, "Y", "", "0.5", "");
                                        break;
                                    case 1:
                                        SearchManager.searchTOTAL(function(data) {
                                            searchResult_Data = data;
                                        }, function() {
                                            isConnectError = true;
                                            searchError();
                                        }, false, "0x04", 3, true, 0, 30, "0x09", searchWord, "", "TITLE", 2, "Y", "", "0.5", "");
                                        break;
                                    case 2:
                                        SearchManager.searchTOTAL(function(data) {
                                            searchResult_Data = data;
                                        }, function() {
                                            isConnectError = true;
                                            searchError();
                                        }, false, "0x04", 3, false, 0, 30, "0x09", searchWord, "", "RECOMM", 2, "Y", "", "0.5", "");
                                        break;
                                    case 3:
                                        SearchManager.searchTOTAL(function(data) {
                                            searchResult_Data = data;
                                        }, function() {
                                            isConnectError = true;
                                            searchError();
                                        }, false, "0x04", 3, false, 0, 30, "0x09", searchWord, "", "INSERT_DATE", 2, "Y", "", "0.5", "");
                                        break;
                                }
                                if (!isConnectError) {
                                    filteringDataResetting(searchResult_Data);
                                    focusIdx = 0;
                                    curPage = 0;
                                    var filterIdx = SearchManager.getSelectTVAppIndex();
                                    var filterText = filterIdx == 0 ? "인기도순" : (filterIdx == 1 ? "가나다순" : (filterIdx == 2 ? "추천순" : "최신순"));
                                    INSTANCE.div.find(".searchListTVApp .filteringSetting_view_area").html(
                                        "<span class='sortSet_txt'>정렬 : " + filterText + "</span>"
                                    );
                                    buttonFocusRefresh(focusIdx);
                                    changePage(curPage);
                                }
                            }
                        }
                    },
                    moduleId: "module.search",
                    new: true,
                    visible: true
                });
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };

    function searchError() {
        LayerManager.activateLayer({
            obj: {
                id: "SearchError",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: "오류",
                    message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                    button: "닫기",
                    reboot: false,
                    callback: function() {
                    }
                }
            },
            moduleId: "module.search",
            visible: true
        });
    }
};