/**
 * Created by Yun on 2016-12-22.
 */
window.SearchList_TVProgram = function (instance) {
    var INSTANCE = instance;

    var resultData;
    var date;
    var isFocus = false;
    var focusIdx = 0;
    var dataIdx = 0;
    var indicator;
    var totalPage = 0;
    var curPage = 0;
    var resultTotalCnt = 0;
    var nextDataCnt = 1;
    var reservationList = [];

    var selFilter = 0;
    var req_cd;
    var programObj;
    var channelObj;
    var isBroadCH = [];
    var searchWord;

    this.setReqPath = function (path) {
        req_cd = path;
    };

    this.getListType = function () {
        return "TVPro";
    };

    this.getView = function () {
        var html = "<div id='tvProgramListBox_area' class='searchListTVProgram'>" +
            "<div class='filteringSetting_view_area'></div>" +
            "<table id='searchTVProgram_table'>";
        for (var idx = 0; idx < 6; idx++) {
            html += "<tr>" +
                "<td>" +
                "<div class='searchTVProgramList_chCell'><div class='channel_num'></div><div class='channel_name'></div></div>" +
                "<div class='searchTVProgramList_cell'>" +
                "<div class='searchTVProgramList_title_area'>" +
                "<div class='reservation_icon'></div>" +
                "<div class='searchTVProgramList_title'><img class='searchTVProgramList_prInfo'></div>" +
                "</div>" +
                "<div class='searchTVProgramList_subTitle'></div>" +
                "<div class='searchTVProgramList_broadBar_area'>" +
                "<span class='searchTVProgramList_broadBar_startTime'>24:00</span>" +
                "<div class='searchTVProgramList_broadBar_grayBar'></div>" +
                "<div class='searchTVProgramList_broadBar_focusBar'></div>" +
                "<span class='searchTVProgramList_broadBar_endTime'>24:00</span>" +
                "</div>" +
                "</div></td>" +
                "</tr>";
        }
        html += "</table>";
        html += "<div id='searchTVPro_focus_div'></div>";
        html += "<div class='indicatorArea'></div></div>";
        return html;
    };

    this.setData = function (data, filterData, _searchWord, totalCount) {
        nextDataCnt = 1;
        searchWord = _searchWord;
        reservationList = [];
        resultData = data;
        var startTime;
        var endTime;
        indicator = new Indicator(6);
        date = getDate();
        totalPage = Math.ceil(totalCount/6);
        resultTotalCnt = totalCount;
        curPage = 0;
        if (totalPage == 1) totalPage = 0;
        if (totalPage != 0) INSTANCE.div.find("#tvProgramListBox_area .indicatorArea").append(indicator.getView());
        indicator.setSize(totalPage, curPage);
        var cDate = new Date();
        var broadDate;
        var broadTime;
        var currentTime;
        var broadStartTime;
        var broadEndTime;
        var pos;
        broadDate = cDate.getFullYear() + (cDate.getMonth()+1 < 10 ? "0" + (cDate.getMonth()+1) : "" + (cDate.getMonth()+1)) + (cDate.getDate() < 10 ? "0" + cDate.getDate() : "" +cDate.getDate());
        broadTime = cDate.getHours() + "" + (cDate.getMinutes() < 10 ? "0" + cDate.getMinutes() : cDate.getMinutes());
        currentTime = getMillisecond(cDate.getHours(), cDate.getMinutes(), cDate.getSeconds());
        var tmpDateText;
        for (var idx = 0 ; idx < 6 ; idx ++) {
            INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ")").addClass("hidden");
        }
        for (var idx = 0; idx < (resultData.length > 6 ? 6 : resultData.length); idx++) {
            INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_chCell:eq(" + idx + ") .channel_num").text(formatChannelStr(resultData[idx].chNum));
            INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_chCell:eq(" + idx + ") .channel_name").text(resultData[idx].chName);
            INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_title:eq(" + idx + ")").html(resultData[idx].proName + "<img class='searchTVProgramList_prInfo'>");
            INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_prInfo:eq(" + idx + ")").attr("src", window.modulePath + "resource/icon_age_txtlist_" + UTIL.transPrInfo(resultData[idx].prInfo) + ".png");
            if (resultData[idx].broadDate == broadDate && (parseInt(resultData[idx].broTime) <= parseInt(broadTime) && parseInt(resultData[idx].endTime.toString().substring(0, 4)) >= parseInt(broadTime))) {
                broadStartTime = getMillisecond(parseInt(resultData[idx].broTime.toString().substring(0, 2)), parseInt(resultData[idx].broTime.toString().substring(2, 4)), 0);
                broadEndTime = getMillisecond(parseInt(resultData[idx].endTime.toString().substring(0, 2)), parseInt(resultData[idx].endTime.toString().substring(2, 4)), 0);
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_subTitle:eq(" + idx + ")").addClass("hidden");
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ")").removeClass("hidden");
                pos = Math.min(((currentTime-broadStartTime) / (broadEndTime-broadStartTime)) * 360, 360);
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ") .searchTVProgramList_broadBar_startTime").text(replaceTimeText(resultData[idx].broTime));
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ") .searchTVProgramList_broadBar_focusBar").css("width", pos + "px");
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ") .searchTVProgramList_broadBar_endTime").text(replaceTimeText(resultData[idx].endTime.toString().substring(0, 4)));
                isBroadCH.push(true);
            } else {
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ")").addClass("hidden");
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_subTitle:eq(" + idx + ")").removeClass("hidden");
                tmpDateText = (date == resultData[idx].broadDate ? "오늘 " : "내일 ") + replaceTimeText(resultData[idx].broTime) + " - " + replaceTimeText(resultData[idx].endTime.toString().substring(0, 4));
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_subTitle:eq(" + idx + ")").text(tmpDateText);
                isBroadCH.push(false);
            }
            log.printDbg(idx + " >> " + resultData[idx].broadDate + " ? " + broadDate + " || " + parseInt(resultData[idx].broTime) + " ? " + parseInt(broadTime) + " || " + parseInt(resultData[idx].endTime.toString().substring(0, 4)) + " ? " + parseInt(broadTime));
        }

        INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").html(
            "<span class='sortSet_txt'>정렬 : 방송시간순</span>"
        );
    };

    function getMillisecond(hour, min, sec) {
        var tmpTimeValue = 0;
        tmpTimeValue = hour * 60 * 60 * 1000;
        tmpTimeValue += min * 60 * 1000;
        tmpTimeValue += sec * 1000;
        return tmpTimeValue;
    }

    function getDate() {
        var date = new Date();
        var year = date.getFullYear();
        var month = new String(date.getMonth() + 1);
        var day = new String(date.getDate());

        if (month.length == 1) {
            month = "0" + month;
        }
        if (day.length == 1) {
            day = "0" + day;
        }

        return year + "" + month + "" + day;
    }

    function replaceTimeText(timeText) {
        return timeText.toString().replace(/\B(?=(\d{2})+(?!\d))/g, ":");
    }

    this.show = function () {
        var filterText = SearchManager.getSelectTVProgramIndex() == 0 ? "방송시간순":"가나다순";
        INSTANCE.div.find("#tvProgramListBox_area.searchListTVProgram").removeClass("hide");
        // INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").show();
        reservationListRefresh(0, true);
    };

    this.hide = function () {
        INSTANCE.div.find("#tvProgramListBox_area.searchListTVProgram").addClass("hide");
        // INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").hide();
    };

    this.setFocus = function () {
        log.printDbg('called setFocus()');
        isFocus = true;
        focusIdx = 0;
        dataIdx = 0;
        buttonFocusRefresh(focusIdx, dataIdx);
        if (totalPage > 0) {
            INSTANCE.div.find(".indicatorArea").addClass('show');
            INSTANCE.div.find("#searchResult_page_noti_area").addClass("show");
            INSTANCE.div.find("#searchTVPro_focus_div").addClass("with_scroll");
            INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_cell").addClass("with_scroll");
        }
    };

    this.setUnFocus = function () {
        log.printDbg('called setUnFocus()');
        isFocus = false;
        focusIdx = 0;
        dataIdx = 0;
        buttonFocusRefresh(focusIdx, dataIdx);
        if (totalPage > 0) {
            INSTANCE.div.find(".indicatorArea").removeClass('show');
            INSTANCE.div.find("#searchResult_page_noti_area").removeClass("show");
            INSTANCE.div.find("#searchTVPro_focus_div").removeClass("with_scroll");
            INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_cell").removeClass("with_scroll");
        }
    };

    this.getIsRelationButton = function () {
        return true;
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    function buttonFocusRefresh(index, dataIndex) {
        focusIdx = index;
        if (Math.floor(dataIndex / 6) != curPage) changePage(Math.floor(dataIndex / 6));
        INSTANCE.div.find("#searchTVPro_focus_div").removeClass("focus");
        INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_cell").removeClass("focus");
        INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_chCell").removeClass("focus");
        INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").removeClass("show");
        if (isFocus) {
            INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").addClass("show");
            INSTANCE.div.find("#searchTVPro_focus_div").addClass("focus");
            INSTANCE.div.find("#searchTVPro_focus_div").css("-webkit-transform", "translateY(" + (focusIdx * 127) + "px)");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + focusIdx + ") .searchTVProgramList_cell").addClass("focus");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + focusIdx + ") .searchTVProgramList_chCell").addClass("focus");
        }
    }

    function changePage(page) {
        curPage = page;
        if (resultData.length < resultTotalCnt && (curPage+1)%5 == 0) {
            nextSearchResultData(nextDataCnt*30);
        }
        INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_cell .reservation_icon").removeClass("show");
        if (curPage == 0) focusIdx = dataIdx;
        else if (focusIdx == 5) focusIdx = (totalPage-1 != curPage ? 5 : (resultData.length-1 - ((totalPage-1) * 6)));
        for (var idx = 0 ; idx < 6 ; idx ++) {
            INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + idx + ")").addClass("hidden");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + idx + ") .searchTVProgramList_chCell .channel_num").text("");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + idx + ") .searchTVProgramList_chCell .channel_name").text("");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + idx + ") .searchTVProgramList_title").text("");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + idx + ") .searchTVProgramList_prInfo").attr("src", "");
            INSTANCE.div.find("#searchTVProgram_table tr td:eq(" + idx + ") .searchTVProgramList_subTitle").text("");
        }
        var cDate = new Date();
        var broadDate;
        var broadTime;
        var currentTime;
        var broadStartTime;
        var broadEndTime;
        var pos;
        broadDate = cDate.getFullYear() + (cDate.getMonth()+1 < 10 ? "0" + (cDate.getMonth()+1) : "" + (cDate.getMonth()+1)) + (cDate.getDate() < 10 ? "0" + cDate.getDate() : "" +cDate.getDate());
        broadTime = cDate.getHours() + "" + (cDate.getMinutes() < 10 ? "0" + cDate.getMinutes() : cDate.getMinutes());
        currentTime = getMillisecond(cDate.getHours(), cDate.getMinutes(), cDate.getSeconds());
        var tmpDateText;
        var tagIdx = 0;
        isBroadCH = [];
        for (var idx = (curPage * 6) ; idx < 6 + (curPage * 6); idx ++) {
            if (resultData[idx] != null) {
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_chCell:eq(" + tagIdx + ") .channel_num").text(formatChannelStr(resultData[idx].chNum));
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_chCell:eq(" + tagIdx + ") .channel_name").text(resultData[idx].chName);
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_title:eq(" + tagIdx + ")").html(resultData[idx].proName + "<img class='searchTVProgramList_prInfo'>");
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_prInfo:eq(" + tagIdx + ")").attr("src", window.modulePath + "resource/icon_age_txtlist_" + UTIL.transPrInfo(resultData[idx].prInfo) + ".png");
                if (resultData[idx].broadDate == broadDate && (parseInt(resultData[idx].broTime) <= parseInt(broadTime) && parseInt(resultData[idx].endTime.toString().substring(0, 4)) >= parseInt(broadTime))) {
                    broadStartTime = getMillisecond(parseInt(resultData[idx].broTime.toString().substring(0, 2)), parseInt(resultData[idx].broTime.toString().substring(2, 4)), 0);
                    broadEndTime = getMillisecond(parseInt(resultData[idx].endTime.toString().substring(0, 2)), parseInt(resultData[idx].endTime.toString().substring(2, 4)), 0);
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_subTitle:eq(" + tagIdx + ")").addClass("hidden");
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + tagIdx + ")").removeClass("hidden");
                    pos = Math.min(((currentTime-broadStartTime) / (broadEndTime-broadStartTime)) * 360, 360);
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + tagIdx + ") .searchTVProgramList_broadBar_startTime").text(replaceTimeText(resultData[idx].broTime));
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + tagIdx + ") .searchTVProgramList_broadBar_focusBar").css("width", pos + "px");
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + tagIdx + ") .searchTVProgramList_broadBar_endTime").text(replaceTimeText(resultData[idx].endTime.toString().substring(0, 4)));
                    isBroadCH.push(true);
                } else {
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_broadBar_area:eq(" + tagIdx + ")").addClass("hidden");
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_subTitle:eq(" + tagIdx + ")").removeClass("hidden");
                    tmpDateText = (date == resultData[idx].broadDate ? "오늘 " : "내일 ") + replaceTimeText(resultData[idx].broTime) + " - " + replaceTimeText(resultData[idx].endTime.toString().substring(0, 4));
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_subTitle:eq(" + tagIdx + ")").text(tmpDateText);
                    isBroadCH.push(false);
                }
                tagIdx++;
            }
        }
        indicator.setPos(curPage);
        reservationListRefresh(0, true);
    }

    function nextSearchResultData(positionIndex) {
        if (selFilter == 0) {
            SearchManager.searchTOTAL(function(data) {
                searchResult_Data = data;
                dataListSetting(data);
            }, function() {
                searchError();
            }, false, "0x02", 3, true, positionIndex, 30, "0x09", searchWord, "", "INSERT_DATE", 2, "Y", "", "0.5", "");
        } else {
            SearchManager.searchTOTAL(function(data) {
                searchResult_Data = data;
                dataListSetting(data);
            }, function() {
                searchError();
            }, false, "0x02", 3, true, positionIndex, 30, "0x09", searchWord, "", "TITLE", 2, "Y", "", "0.5", "");
        }

        function dataListSetting(data) {
            tmpData = _$(data).find("search").find("EPG_ITEM");
            tmpData.each(function () {
                resultData.push({
                    chID: _$(this).find("SERVICE_ID").text(),
                    chName: _$(this).find("CHNL_NAME").text(),
                    proName: _$(this).find("PRO_NAME").text(),
                    broadDate: _$(this).find("BROAD_DATE").text(),
                    broTime: _$(this).find("BRO_TIME").text(),
                    endTime: _$(this).find("PRO_ENDTIME").text(),
                    durationTime: _$(this).find("DURATION").text(),
                    prInfo: _$(this).find("AGE_GRD").text(),
                    proKind: _$(this).find("PRODUCT_KIND").text(),
                    chNum: _$(this).find("STB_SERVICE_ID").text()
                });
            });
            nextDataCnt++;
            totalData = resultData;
        }
    }

    function reservationListRefresh(result, isRefresh) {
        var tmpIdx = 0;
        var tmpIsRes = 0;
        for (var idx = curPage * 6 ; idx < (resultData.length < 6 ? resultData.length : 6 + (curPage * 6)) ; idx ++) {
            if (resultData[idx] != null) {
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_cell:eq(" + tmpIdx + ") .reservation_icon").removeClass("series");
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_cell:eq(" + tmpIdx + ") .reservation_icon").removeClass("single");
                INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_cell:eq(" + tmpIdx + ") .reservation_icon").removeClass("show");
                startTime = HTool.getDateToTime(resultData[idx].broadDate + resultData[idx].broTime + "00") + 1;
                endTime = HTool.getDateToTime(resultData[idx].broadDate + resultData[idx].endTime) - 1;
                channelObj = window.oipfAdapter.navAdapter.getChannelBySID(parseInt(resultData[idx].chID));
                programObj = window.oipfAdapter.programSearchAdapter.getPrograms(startTime, endTime, null, channelObj);
                tmpIsRes = window.ReservationManager.isReserved(programObj[0]);
                if (tmpIsRes != 0) {
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_cell:eq(" + tmpIdx + ") .reservation_icon").addClass((tmpIsRes == 2 ? "series" : "single"));
                    INSTANCE.div.find("#searchTVProgram_table .searchTVProgramList_cell:eq(" + tmpIdx + ") .reservation_icon").addClass("show");
                }
                tmpIdx ++;
            }
        }
    }
    function formatChannelStr(str) {
        str = str.toString();
        var tmpStr = "";
        for (var idx = 0 ; idx < (3-str.length) ; idx ++) {
            tmpStr += "0";
        }
        return tmpStr + str;
    }


    this.onKeyAction = function (keyCode) {
        var keyAction = {isUsed: false};
        switch (keyCode) {
            case KEY_CODE.UP:
                if (dataIdx > 0) dataIdx--;
                focusIdx = dataIdx % 6;
                buttonFocusRefresh(focusIdx, dataIdx);
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.DOWN:
                dataIdx = (dataIdx < resultTotalCnt - 1) ? dataIdx + 1 : 0;
                focusIdx = dataIdx % 6;
                buttonFocusRefresh(focusIdx, dataIdx);
                channelObj = window.oipfAdapter.navAdapter.getChannelBySID(parseInt(resultData[dataIdx].chID));
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.RED:
                if (dataIdx%6 === 0) {
                    if (resultTotalCnt > 6) {
                        if (curPage > 0) {
                            curPage --;
                        }
                        changePage(curPage);
                        dataIdx = curPage * 6;
                        focusIdx = dataIdx % 6;
                        buttonFocusRefresh(focusIdx, dataIdx);
                    }
                } else {
                    dataIdx = curPage * 6;
                    focusIdx = dataIdx % 6;
                    buttonFocusRefresh(focusIdx, dataIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BLUE:
                if (dataIdx === resultTotalCnt-1 || dataIdx%6 === 5) {
                    if (resultTotalCnt > 6) {
                        curPage = (totalPage-1 > curPage) ? curPage + 1: 0;
                        changePage(curPage);
                        dataIdx = Math.min(resultTotalCnt-1, curPage * 6 + 5);
                        focusIdx = dataIdx % 6;
                        buttonFocusRefresh(focusIdx, dataIdx);
                    }
                } else {
                    dataIdx = Math.min(resultTotalCnt-1, curPage * 6 + 5);
                    focusIdx = dataIdx % 6;
                    buttonFocusRefresh(focusIdx, dataIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.LEFT:
            case KEY_CODE.BACK:
                keyAction.isUsed = true;
                keyAction.prevMenu = true;
                // if (isListFocus && totalPage > 0) {
                //     isListFocus = false;
                //     indicator.focused();
                //     focusIdx = 0;
                //     INSTANCE.div.find("#searchTVPro_focus_div").removeClass("focus");
                //     INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_cell").removeClass("focus");
                //     INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_chCell").removeClass("focus");
                //     INSTANCE.div.find("#searchResult_filter_noti_area").removeClass("show");
                //     INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").removeClass("show");
                //     keyAction.isUsed = true;
                //     keyAction.prevMenu = false;
                // } else {
                //     indicator.blurred();
                //     focusIdx = 0;
                //     dataIdx = 0;
                //     isListFocus = false;
                //     buttonFocusRefresh(focusIdx, dataIdx);
                //     keyAction.isUsed = true;
                //     keyAction.prevMenu = true;
                // }
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.ENTER:
                if (isBroadCH[focusIdx]) {
                    var cDate = new Date();
                    var currentTime = getMillisecond(cDate.getHours(), cDate.getMinutes(), cDate.getSeconds());
                    var broadEndTime = getMillisecond(parseInt(resultData[dataIdx].endTime.toString().substring(0, 2)), parseInt(resultData[dataIdx].endTime.toString().substring(2, 4)), 0);
                    channelObj = window.oipfAdapter.navAdapter.getChannelBySID(parseInt(resultData[dataIdx].chID));
                    var tuneCh = window.oipfAdapter.navAdapter.getChannelControl("main");
                    if (channelObj === null || currentTime >= broadEndTime) {
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchToast",
                                type: Layer.TYPE.BACKGROUND,
                                priority: Layer.PRIORITY.REMIND_POPUP,
                                linkage: true,
                                params: {
                                    text: "편성 정보가 변경되어 채널을 전환할 수 없습니다"
                                }
                            },
                            moduleId: "module.search",
                            visible: true
                        });
                    } else {
                        tuneCh.changeChannel(channelObj, false);
                    }
                } else {
                    startTime = HTool.getDateToTime(resultData[dataIdx].broadDate + resultData[dataIdx].broTime) + 1;
                    endTime = HTool.getDateToTime(resultData[dataIdx].broadDate + resultData[dataIdx].endTime) - 1;
                    channelObj = window.oipfAdapter.navAdapter.getChannelBySID(parseInt(resultData[dataIdx].chID));
                    programObj = window.oipfAdapter.programSearchAdapter.getPrograms(startTime, endTime, null, channelObj);
                    if (programObj.length == 0) {
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchToast",
                                type: Layer.TYPE.BACKGROUND,
                                priority: Layer.PRIORITY.REMIND_POPUP,
                                linkage: true,
                                params: {
                                    text: "편성 정보가 변경되어 시청 예약할 수 없습니다"
                                }
                            },
                            moduleId: "module.search",
                            visible: true
                        });
                    } else {
                        window.ReservationManager.add(programObj[0], true, function (result) {
                            var RESULT = window.ReservationManager.ADD_RESULT;
                            if (result === RESULT.SUCCESS_RESERVE || result === RESULT.SUCCESS_SERIES_RESERVE ||
                                result === RESULT.DUPLICATE_RESERVE || result === RESULT.SUCCESS_SERIES_CANCEL ||
                                result === RESULT.SUCCESS_CANCEL) {
                                reservationListRefresh(result, false);
                            }
                        });
                    }
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            // case KEY_CODE.BACK:
            //     if (isListFocus && totalPage > 0) {
            //         isListFocus = false;
            //         indicator.focused();
            //         focusIdx = 0;
            //         INSTANCE.div.find("#searchTVPro_focus_div").removeClass("focus");
            //         INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_cell").removeClass("focus");
            //         INSTANCE.div.find("#searchTVProgram_table tr td .searchTVProgramList_chCell").removeClass("focus");
            //         INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").removeClass("show");
            //         keyAction.prevMenu = false;
            //     } else {
            //         indicator.blurred();
            //         focusIdx = 0;
            //         dataIdx = 0;
            //         isListFocus = false;
            //         buttonFocusRefresh(focusIdx, dataIdx);
            //         keyAction.prevMenu = true;
            //     }
            //     keyAction.isUsed = true;
            //     return keyAction;
            case KEY_CODE.CONTEXT:
                LayerManager.activateLayer({
                    obj : {
                        id: "ListFilteringPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            popType : "TV_PROGRAM",
                            complete : function (lineup) {
                                var tmpData = null;
                                if (lineup == 0) {
                                    SearchManager.searchTOTAL(function(data) {
                                        tmpData =_$(data).find("search").find("EPG_ITEM");
                                    }, function() {
                                        searchError();
                                    }, false,"0x02", 3, true, 0, 30, "0x09", searchWord, "", "INSERT_DATE", 2, "Y", "", "0.5", "");

                                } else {
                                    SearchManager.searchTOTAL(function(data) {
                                        tmpData =_$(data).find("search").find("EPG_ITEM");
                                    }, function() {
                                        searchError();
                                    }, false,"0x02", 3, true, 0, 30, "0x09", searchWord, "", "TITLE", 2, "Y", "", "0.5", "");
                                }
                                resultData = [];
                                tmpData.each(function () {
                                    resultData.push({chID:_$(this).find("SERVICE_ID").text(),
                                        chName:_$(this).find("CHNL_NAME").text(),
                                        proName:_$(this).find("PRO_NAME").text(),
                                        broadDate:_$(this).find("BROAD_DATE").text(),
                                        broTime:_$(this).find("BRO_TIME").text(),
                                        endTime:_$(this).find("PRO_ENDTIME").text(),
                                        durationTime:_$(this).find("DURATION").text(),
                                        prInfo:_$(this).find("AGE_GRD").text(),
                                        proKind:_$(this).find("PRODUCT_KIND").text(),
                                        chNum:_$(this).find("STB_SERVICE_ID").text()});
                                });
                                focusIdx = 0;
                                dataIdx = 0;
                                curPage = 0;
                                var filterText = SearchManager.getSelectTVProgramIndex() == 0 ? "방송시간순":"가나다순";
                                INSTANCE.div.find(".searchListTVProgram .filteringSetting_view_area").html(
                                    "<span class='sortSet_txt'>정렬 : " + filterText + "</span>"
                                );
                                buttonFocusRefresh(focusIdx, dataIdx);
                                changePage(curPage);
                                nextDataCnt = 1;
                            }
                        }
                    },
                    moduleId: "module.search",
                    new: true,
                    visible: true
                });
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };
    function searchError() {
        LayerManager.activateLayer({
            obj: {
                id: "SearchError",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: "오류",
                    message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                    button: "닫기",
                    reboot: false,
                    callback: function () {
                        totalPage = Math.ceil(resultData.length/6);
                        indicator.setSize(totalPage, curPage);
                    }
                }
            },
            moduleId: "module.search",
            visible: true
        });
    }
};