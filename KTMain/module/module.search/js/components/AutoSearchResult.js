/**
 * Created by Yun on 2016-12-14.
 */

window.AutoSearchResult = function (instance) {
    var INSTANCE = instance;
    var mainList = [];
    var subList = [];
    var resultData;
    var tmpIdx;
    var isFocus = false;
    var isLeftFocus = true;
    var focusIdx = 0;
    var dataIdx = 0;
    var subFocusIdx = 0;
    var subDataIdx = 0;
    var vodListData = [];
    var totalPage = 0;
    var curPage = 0;
    var totalSubPage = 0;
    var curSubPage = 0;

    this.getView = function () {
        var div = _$("<div/>", {id:"autoSearchResult_area"});
        div.append(_$("<div/>", {id:"autoSearchResult_BG"}));
        div.append(_$("<div/>", {id:"searchResult_div"}));
        for (var idx = 0 ; idx < 5 ; idx ++) {
            div.find("#searchResult_div").append(_$("<div/>", {class:"resultList_div"}));
            div.find("#searchResult_div .resultList_div").eq(idx).append(_$("<div/>", {class:"resultList_text"}).html("<span></span>"));
            div.find("#searchResult_div .resultList_div").eq(idx).append(_$("<img/>", {class:"subList_Icon", src: window.modulePath + "resource/arw_menu_focus.png"}));
        }
        div.append(_$("<div/>", {id:"listBar_div"}));
        div.append(_$("<img/>", {id:"upArrow_img", src: window.modulePath + "resource/arw_menu_up.png"}));
        div.append(_$("<img/>", {id:"downArrow_img", src: window.modulePath + "resource/arw_menu_dw.png"}));
        div.append(_$("<div/>", {id:"searchSubResult_div"}));
        for (var idx = 0 ; idx < 5; idx ++) {
            div.find("#searchSubResult_div").append(_$("<div/>", {class:"subResultList_div"}));
            div.find("#searchSubResult_div .subResultList_div").eq(idx).append(_$("<div/>", {class:"subResultList_text"}).html("<span></span>"));
        }
        div.append(_$("<div/>", {id:"searchSubResult_VOD_div"}));
        for (var idx = 0 ; idx < 3; idx ++) {
            div.find("#searchSubResult_VOD_div").append(_$("<div/>", {class:"subResultList_VOD_div"}));
            div.find(".subResultList_VOD_div").eq(idx).css({"top": idx*150 });
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).append(_$("<img/>", {class:"subResultList_VOD_img"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).append(_$("<img/>", {class:"subResultList_VOD_lock"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).append(_$("<div/>", {class:"subResultList_VOD_dim"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).append(_$("<div/>", {class:"subResultList_VOD_textArea"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).find(".subResultList_VOD_textArea").append(_$("<div/>", {class:"subResultList_VOD_title"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).find(".subResultList_VOD_textArea").append(_$("<div/>", {class:"subResultList_VOD_info"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).find(".subResultList_VOD_textArea .subResultList_VOD_info").append(stars.getView(0));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).find(".subResultList_VOD_textArea .subResultList_VOD_info").append(_$("<div/>", {class:"subResultList_VOD_subTitle"}));
            div.find("#searchSubResult_VOD_div .subResultList_VOD_div").eq(idx).find(".subResultList_VOD_textArea .subResultList_VOD_info").append(_$("<img/>", {class:"vodAge_img"}));
        }

        return div;
    };

    this.show = function () {
        INSTANCE.div.find("#autoSearchResult_BG").removeClass("hide");
        INSTANCE.div.find("#searchResult_div").removeClass("hide");
        INSTANCE.div.find("#listBar_div").addClass("show");
        totalPage = Math.ceil(mainList.length / 5);
        totalPage = (totalPage != 0)? totalPage : 1;
        // if (totalPage == 1) totalPage = 0;
        curPage = 0;
    };

    this.hide = function () {
        INSTANCE.div.find("#searchSubResult_VOD_div").removeClass("show");
        INSTANCE.div.find("#listBar_div").removeClass("show");
        INSTANCE.div.find("#downArrow_img").removeClass("show");
        INSTANCE.div.find("#upArrow_img").removeClass("show");
        INSTANCE.div.find("#searchResult_div .subList_Icon").removeClass("show");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("unselect");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("select");

        INSTANCE.div.find("#autoSearchResult_BG").addClass("hide");
        INSTANCE.div.find("#searchResult_div").addClass("hide");
        for (var idx = 0; idx < 5; idx++) {
            INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + idx + ") span").text("");
            INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + idx + ") span").text("");
        }
        clearTextAnimation(INSTANCE.div);
    };

    this.resume = function () {
        if (isLeftFocus) setTextAnimation(INSTANCE.div.find("#searchResult_div .resultList_div:eq(" + focusIdx + ") .resultList_text"));
    };

    this.pause = function () {
        clearTextAnimation(INSTANCE.div);
    };

    this.setFocus = function () {
        isFocus = true;
        isLeftFocus = true;
        focusIdx = 0;
        dataIdx = 0;
        INSTANCE.div.find("#searchResult_div").addClass("focus");
        buttonRefresh(0, dataIdx);
    };

    this.setUnFocus = function () {
        isFocus = false;
        isLeftFocus = true;
        INSTANCE.div.find("#searchSubResult_VOD_div").removeClass("show");
        INSTANCE.div.find("#searchResult_div").removeClass("focus");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("focus");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("unselect");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("select");
        INSTANCE.div.find("#searchSubResult_div .subResultList_div").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div").css("opacity", "0.5");
        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_dim").css("display", "block");
        for (var idx = 0; idx < 5; idx++) {
            INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + idx + ") span").text("");
        }
        clearTextAnimation(INSTANCE.div);
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    this.setDataInit = function () {
        mainList = [];
        subList = [];
        for (var idx = 0; idx < 5; idx++) {
            INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + idx + ") span").text("");
            INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + idx + ") span").text("");
        }
    };

    this.setListData = function (type, value, index) {
        if (type) {
            mainList.push({contsType:value.find("CONTENTS_TYPE").text(),
            title:value.find("SEARCH_WORD").text(),
            wordType:value.find("WORD_TYPE").text(),
            spellExpand:value.find("SPELL_EXPAND").text(),
            searchQuery:value.find("SEARCH_QUERY").text()});
            subList.push([]);
            tmpIdx = index;
        } else {
            subList[tmpIdx].push({contsType:value.find("CONTENTS_TYPE").text(),
                title:value.find("SEARCH_WORD").text(),
                wordType:value.find("WORD_TYPE").text(),
                spellExpand:value.find("SPELL_EXPAND").text(),
                searchQuery:value.find("SEARCH_QUERY").text()});
        }

        if (subList) {
            INSTANCE.div.find("#downArrow_img").removeClass("show");
        }

        if (mainList.length > 5) {
            INSTANCE.div.find("#downArrow_img").addClass("show");
        }
    };

    this.listDataRefresh = function () {
        if (mainList.length == 0) return false;
        INSTANCE.div.find("#searchResult_div .subList_Icon").removeClass("show");
        INSTANCE.div.find("#searchResult_div .resultList_text").removeClass("spellState");
        for (var idx = 0; idx < (mainList.length > 5 ? 5 : mainList.length); idx++) {
            if (mainList[idx].spellExpand == "1") INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + idx + ")").addClass("spellState");
            INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + idx + ") span").text(mainList[idx].title);
            INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + idx + ") span").text("");
            if (subList[idx].length > 0) {
                INSTANCE.div.find("#searchResult_div .subList_Icon:eq(" + idx + ")").addClass("show");
                INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + idx + ")").css("width", "360px");
            } else {
                INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + idx + ")").css("width", "380px");
            }
        }
        return true;
    };

    this.onKeyAction = function (key_code) {
        var keyAction = {isUsed: false, isPrevKey: false};
        switch (key_code) {
            case KEY_CODE.UP:
                keyAction.isUsed = true;
                if (isLeftFocus) {
                    if (dataIdx == 0) {
                        keyAction.prevMenu = true;
                    } else {
                        keyAction.prevMenu = false;
                        if (dataIdx > 0)
                            dataIdx --;
                        // dataIdx = HTool.getIndex(dataIdx, -1, mainList.length);
                        buttonRefresh(HTool.getIndex(focusIdx, -1, (totalPage > 0 ? 5 : mainList.length)), dataIdx);
                    }
                } else {
                    if (subDataIdx == 0) {
                        keyAction.prevMenu = true;
                    } else {
                        keyAction.prevMenu = false;
                        if (subDataIdx > 0) subDataIdx = HTool.getIndex(subDataIdx, -1, (subList[dataIdx].length == 0 ? (vodListData.length > 3 ? 3 : vodListData.length) : subList[dataIdx].length));
                        subFocusIdx = subDataIdx % (subList[dataIdx].length == 0 ? 3 : 5);
                        subListButtonRefresh(subFocusIdx, subDataIdx, dataIdx);
                    }
                }
                return keyAction;
            case KEY_CODE.DOWN:
                keyAction.prevMenu = false;
                if (isLeftFocus) {
                    // TODO : [Leebaeng] 2017/06/08 포커스 수정
                    dataIdx = (mainList.length-1 > dataIdx)? dataIdx+1 : 0;
                    // if (mainList.length-1 > dataIdx) dataIdx = dataIdx+1;
                    focusIdx = Math.ceil((dataIdx) % 5);

                    buttonRefresh(focusIdx, dataIdx);
                } else {
                    if (subList[dataIdx].length == 0) {
                        subDataIdx = (vodListData.length-1 > subFocusIdx)? subFocusIdx+1 : 0;
                        subFocusIdx = Math.ceil((subDataIdx)%3);
                    } else {
                        subDataIdx = (subList[dataIdx].length-1 > subDataIdx) ? subDataIdx+1 : 0;
                        subFocusIdx = Math.ceil((subDataIdx)%5);
                    }
                    // subListButtonRefresh(HTool.getIndex(subFocusIdx, 1, (subList[dataIdx].length == 0 ? (vodListData.length > 3 ? 3 : vodListData.length) : (subList[dataIdx].length > 5 ? 5 : subList[dataIdx].length))), dataIdx);
                    subListButtonRefresh(subFocusIdx, subDataIdx, dataIdx);
                }
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.LEFT:
                if (!isLeftFocus) {
                    buttonRefresh(focusIdx, dataIdx);
                    isLeftFocus = true;
                    INSTANCE.div.find("#searchSubResult_div").removeClass("focus");
                    INSTANCE.div.find("#searchSubResult_VOD_div").removeClass("focus");
                    INSTANCE.div.find("#searchSubResult_VOD_div").css("opacity", "0.5");
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_dim").css("display", "block");
                }
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if (isLeftFocus) {
                    if (subList[dataIdx].length == 0 && vodListData.length == 0) return keyAction;
                    isLeftFocus = false;
                    INSTANCE.div.find("#searchSubResult_VOD_div").css("opacity", "1");
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_dim").css("display", "none");
                    subFocusIdx = 0;
                    subDataIdx = 0;
                    subListButtonRefresh(subFocusIdx, subDataIdx, dataIdx);
                    clearTextAnimation(INSTANCE.div);
                }
                return keyAction;
            case KEY_CODE.ENTER:
                if (subList[dataIdx].length > 0) {
                    LayerManager.startLoading({preventKey:true});
                    var tmpData = isLeftFocus ? mainList[dataIdx] : subList[dataIdx][subDataIdx];

                    SearchManager.addRecentSearchWord({constType:tmpData.contsType, searchWord:tmpData.title, wordType:tmpData.wordType, searchQry: tmpData.searchQuery});
                    SearchManager.searchTOTAL(function(data) {
                        SearchManager.setSearchType(tmpData.wordType);
                        SearchManager.setSearchQuery(tmpData.searchQuery);
                        var searchResult_Data = data;
                        var layers = LayerManager.getLayers("SearchResult");
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchResult",
                                type: Layer.TYPE.NORMAL,
                                priority: Layer.PRIORITY.NORMAL,
                                params: {
                                    search_Data: searchResult_Data,
                                    search_Words: tmpData.title
                                }
                            },
                            moduleId: "module.search",
                            visible: true,
                            new: true
                        });
                        for (var i = 0; i < layers.length; i++) {
                            LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
                        }
                    }, function() {
                        LayerManager.stopLoading();
                        LayerManager.activateLayer({
                            obj: {
                                id: "SearchError",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    title: "오류",
                                    message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                                    button: "닫기",
                                    reboot: false
                                    // callback: _error
                                }
                            },
                            moduleId: "module.search",
                            visible: true
                        });
                    }, true, tmpData.contsType, tmpData.wordType, false, 0, 30, "0x09", tmpData.title, tmpData.searchQuery, "POINT", 2, "Y", "", "0.5", "");
                } else {
                    if (isLeftFocus) {
                        LayerManager.startLoading({preventKey:true});
                        var tmpData = isLeftFocus ? mainList[dataIdx] : subList[dataIdx][subDataIdx];

                        SearchManager.addRecentSearchWord({constType:tmpData.contsType, searchWord:tmpData.title, wordType:tmpData.wordType, searchQry: tmpData.searchQuery});
                        SearchManager.searchTOTAL(function(data) {
                            var searchResult_Data = data;
                            SearchManager.setSearchType(tmpData.wordType);
                            SearchManager.setSearchQuery(tmpData.searchQuery);
                            var layers = LayerManager.getLayers("SearchResult");
                            LayerManager.activateLayer({
                                obj: {
                                    id: "SearchResult",
                                    type: Layer.TYPE.NORMAL,
                                    priority: Layer.PRIORITY.NORMAL,
                                    params: {
                                        search_Data: searchResult_Data,
                                        search_Words: tmpData.title
                                    }
                                },
                                moduleId: "module.search",
                                visible: true,
                                new: true
                            });
                            for (var i = 0; i < layers.length; i++) {
                                LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
                            }
                        }, function() {
                            LayerManager.stopLoading();
                            LayerManager.activateLayer({
                                obj: {
                                    id: "SearchError",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        title: "오류",
                                        message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                                        button: "닫기",
                                        reboot: false
                                        // callback: _error
                                    }
                                },
                                moduleId: "module.search",
                                visible: true
                            });
                        }, true,tmpData.contsType, tmpData.wordType, false, 0, 30, "0x09", tmpData.title, tmpData.searchQuery, "POINT", 2, "Y", "", "0.5", "");
                    } else {
                        try {
                            NavLogMgr.collectJumpVOD(NLC.JUMP_START_SEARCH, vodListData[subDataIdx].cat_Id, vodListData[subDataIdx].const_Id, "04");
                        } catch(e) {}
                        openDetailLayer(vodListData[subDataIdx].cat_Id, vodListData[subDataIdx].const_Id, "04");
                    }
                }
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.BACK:
                keyAction.prevMenu = true;
                keyAction.isPrevKey = true;
                keyAction.isUsed = true;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9) {
                    keyAction.prevMenu = false;
                    keyAction.isUsed = true;
                }
                return keyAction;
        }
    };


    function setTextAnimation(_div) {
        clearTextAnimation(INSTANCE.div);
        if (UTIL.getTextLength(mainList[dataIdx].title, "RixHead L", 39) > 360) {
            _div.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: _div
            });
        }
    }

    function clearTextAnimation(_div) {
        UTIL.clearAnimation(_div.find(".textAnimating span"));
        _div.find(".textAnimating").removeClass("textAnimating");
        void _div[0].offsetWidth;
    }



    function searchVODSubList() {
        vodListData = [];
        var tmpData = mainList[dataIdx];
        SearchManager.searchTOTAL(function(data, dataIndex) {
            if (dataIdx == dataIndex) {
                var searchResult_Data = data;
                _$(searchResult_Data).find("search").find("VOD_ITEM").each(function () {
                    vodListData.push({
                        title: _$(this).find("TITLE").text(),
                        imgURL: _$(this).find("URL").text(),
                        cate_Name: _$(this).find("CATEGORY_PULLNAME").text(),
                        cat_Id: _$(this).find("CATEGORY_ID").text(),
                        const_Id: _$(this).find("CONST_ID").text(),
                        seriesYN: _$(this).find("SERIES_YN").text(),
                        wonYN: _$(this).find("WON_YN").text(),
                        director: _$(this).find("DIRECTOR").text(),
                        actor: _$(this).find("ACTOR").text(),
                        ollehp: _$(this).find("OLLEHP").text(),
                        runTime: _$(this).find("RUNTIME").text(),
                        rating: _$(this).find("RATING").text()
                    });
                });
                INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_textArea .subResultList_VOD_info .vodAge_img").attr("src", "");
                INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_dim").css("display", "none");
                INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div").css("display", "none");
                for (var idx = 0; idx < (vodListData.length > 3 ? 3 : vodListData.length); idx++) {
                    if (vodListData[idx].imgURL) INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_img").attr({
                        src: vodListData[idx].imgURL + "?w=98&h=140&quality=90",
                        onerror: "this.src='" + window.modulePath + "resource/default_poster.png'"
                    });
                    else INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_img").attr("src", window.modulePath + "resource/default_poster.png");
                    if (window.UTIL.isLimitAge(vodListData[idx].rating))
                        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_lock").attr("src", window.modulePath + "resource/img_vod_locked.png");
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_dim").css("display", "block");
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_textArea .subResultList_VOD_title").text(vodListData[idx].title);
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_textArea .subResultList_VOD_info .subResultList_VOD_subTitle").html(vodListData[idx].wonYN == "Y" ? ("<img class='isFreeIcon_img' src='" + window.modulePath + "resource/icon_buy.png'>") : "무료");
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_textArea .subResultList_VOD_info .vodAge_img").attr("src", "module/module.search/resource/icon_age_list_" + ServerCodeAdapter.getPrInfo(vodListData[idx].rating - 0) + ".png");
                    stars.setRate(INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ") .subResultList_VOD_textArea .subResultList_VOD_info"), vodListData[idx].ollehp);
                    INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + idx + ")").css("display", "block");
                }
                INSTANCE.div.find("#searchSubResult_VOD_div").addClass("show");
            }
        }, function() {
        }, true, tmpData.contsType, tmpData.wordType, false, 0, 3, "0x09", tmpData.title, tmpData.searchQuery, "POINT", tmpData.spellExpand, "Y", "", "0.5", "", "", dataIdx, true);

    }

    function changePage(page) {
        curPage = page;

        INSTANCE.div.find("#searchResult_div .subList_Icon").removeClass("show");
        INSTANCE.div.find("#searchResult_div .resultList_text").removeClass("spellState");
        var tagIdx = 0;
        for (var idx = (curPage * 5) ; idx < 5 + (curPage * 5) ; idx ++) {
            if (mainList[idx] != null) {
                if (mainList[idx].spellExpand == "1") INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + tagIdx + ")").addClass("spellState");
                INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + tagIdx + ") span").text(mainList[idx].title);
                INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + tagIdx + ") span").text("");
                if (subList[idx].length > 0) {
                    INSTANCE.div.find("#searchResult_div .subList_Icon:eq(" + tagIdx + ")").addClass("show");
                    INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + tagIdx + ")").css("width", "360px");
                } else {
                    INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + tagIdx + ")").css("width", "380px");
                }
            } else {
                INSTANCE.div.find("#searchResult_div .resultList_text:eq(" + tagIdx + ") span").text("");
                INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + tagIdx + ") span").text("");
            }
            tagIdx ++;
        }

        if(totalPage > 0){
            if(page == 0){
                INSTANCE.div.find("#downArrow_img").addClass("show");
                INSTANCE.div.find("#upArrow_img").removeClass("show");
            }else if(totalPage > 0 && page == totalPage-1){
                INSTANCE.div.find("#upArrow_img").addClass("show");
                INSTANCE.div.find("#downArrow_img").removeClass("show");
            }else{
                INSTANCE.div.find("#upArrow_img").addClass("show");
                INSTANCE.div.find("#downArrow_img").addClass("show");
            }
        }
    }

    function subListChangePage(page) {
        curSubPage = page;
        var tagIdx = 0;
        for (var idx = (curSubPage * 5) ; idx < 5 + (curSubPage * 5) ; idx ++) {
            if (subList[dataIdx][idx] != null) {
                INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + tagIdx + ") span").text(subList[dataIdx][idx].title);
            } else {
                INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + tagIdx + ") span").text("");
            }
            tagIdx ++;
        }
    }

    function buttonRefresh(index, dataIndex) {
        focusIdx = index;
        if (Math.floor(dataIndex / 5) != curPage) changePage(Math.floor(dataIndex / 5));
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("unselect");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("select");
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("focus");
        for (var idx = 0; idx < 5; idx++) {
            INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + idx + ") span").text("");
        }
        INSTANCE.div.find("#searchSubResult_div .subResultList_div").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div").removeClass("focus");

        if (isLeftFocus) {
            INSTANCE.div.find("#searchSubResult_VOD_div").removeClass("show");
            INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_img").attr("src", "");
            INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_lock").attr("src", "");
            INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_textArea .subResultList_VOD_title").text("");
            INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_textArea .subResultList_VOD_info .subResultList_VOD_subTitle").html("");
        }


        INSTANCE.div.find("#searchResult_div .resultList_div:eq(" + focusIdx + ")").addClass("focus");
        setTextAnimation(INSTANCE.div.find("#searchResult_div .resultList_div:eq(" + focusIdx + ") .resultList_text"));
        if (subList[dataIndex].length > 0) {
            for (var idx = 0; idx < subList[dataIndex].length; idx++) {
                INSTANCE.div.find("#searchSubResult_div .subResultList_text:eq(" + idx + ") span").text(subList[dataIndex][idx].title);
            }
        } else {
            if (isLeftFocus) searchVODSubList();
        }
    }

    function subListButtonRefresh(index, subDataIndex, dataIndex) {
        subFocusIdx = index;
        if (Math.floor(subDataIndex / 5) != curSubPage) subListChangePage(Math.floor(subDataIndex / 5));
        INSTANCE.div.find("#searchResult_div .resultList_div").removeClass("focus");
        INSTANCE.div.find("#searchResult_div .resultList_div").addClass("unselect");
        INSTANCE.div.find("#searchResult_div .resultList_div:eq(" + focusIdx +")").removeClass("unselect");
        INSTANCE.div.find("#searchResult_div .resultList_div:eq(" + focusIdx +")").addClass("select");
        INSTANCE.div.find("#searchSubResult_div .subResultList_div").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div img").removeClass("focus");
        INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div .subResultList_VOD_textArea .subResultList_VOD_title").removeClass("focus");

        if (subList[dataIndex].length > 0) {
            INSTANCE.div.find("#searchSubResult_div").addClass("focus");
            INSTANCE.div.find("#searchSubResult_div .subResultList_div:eq(" + index + ")").addClass("focus").css("width", INSTANCE.div.find("#searchSubResult_div .subResultList_div:eq(" + index + ") .subResultList_text").width() + 5 + "px");
        } else {
            INSTANCE.div.find("#searchSubResult_VOD_div").addClass("focus");
            INSTANCE.div.find("#searchSubResult_VOD_div .subResultList_VOD_div:eq(" + subFocusIdx + ")").addClass("focus");
        }
    }
};