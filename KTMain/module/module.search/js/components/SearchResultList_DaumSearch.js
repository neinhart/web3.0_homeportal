/**
 * Created by Yun on 2016-12-23.
 */

window.SearchList_Daum = function (instance) {
    var INSTANCE = instance;

    var isFocus = false;

    var req_cd;
    var searchWord;

    this.setReqPath = function (path) {
        req_cd = path;
    };

    this.getListType = function () {
        return "Daum";
    };

    this.getView = function () {
        var html = "<div id='listBox_area' class='searchListDaum'>" +
            "<div id='searchDaum_area'>" +
            "<img id='searchDaum_img' src='module/module.search/resource/img_globe.png'>" +
            "<div id='searchDaum_resultText'>''</div>" +
            "<div id='searchDaum_info'>Daum검색으로 다양한 결과를 찾아보세요</div>" +
            "<div id='searchDaum_btn'>" +
            "<div id='searchDaum_btnBg'></div>" +
            "<div id='searchDaum_btnText'>Daum검색</div>" +
            "</div>" +
            "</div>" +
            "</div>";
        return html;
    };

    this.setData = function (data, filterData, _searchWord) {
        searchWord = _searchWord;
        INSTANCE.div.find("#searchDaum_resultText").text("'" + _searchWord + "'");
    };

    this.show = function () {
        INSTANCE.div.find("#listBox_area.searchListDaum").removeClass("hide");
    };

    this.hide = function () {
        INSTANCE.div.find("#listBox_area.searchListDaum").addClass("hide");
    };

    this.setFocus = function () {
        isFocus = true;
        INSTANCE.div.find("#searchDaum_btnBg").addClass("focus");
        INSTANCE.div.find("#searchDaum_btnText").addClass("focus");
    };

    this.setUnFocus = function () {
        isFocus = false;
        INSTANCE.div.find("#searchDaum_btnBg").removeClass("focus");
        INSTANCE.div.find("#searchDaum_btnText").removeClass("focus");
    };

    this.getIsRelationButton = function () {
        return false;
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    this.onKeyAction = function (keyCode) {
        var keyAction = {isUsed: false};
        switch (keyCode) {
            case KEY_CODE.LEFT:
                keyAction.isUsed = true;
                keyAction.prevMenu = true;
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.ENTER:
                AppServiceManager.changeService({
                    nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: {
                        type: CONSTANT.APP_TYPE.WEB_BROWSER,
                        param: "http://search.daum.net/search?w=tot&rtupcoll=NNS&nil_ch=32&q=" + searchWord + "&DA=P003"
                    }
                });
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BACK:
                keyAction.prevMenu = true;
                keyAction.isUsed = true;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };
};