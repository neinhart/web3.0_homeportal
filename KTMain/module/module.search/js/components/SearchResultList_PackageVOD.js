/**
 * Created by Yun on 2017-05-11.
 */
/**
 * Created by Yun on 2016-12-22.
 */

window.SearchList_PackageVOD = function (instance) {
    var INSTANCE = instance;

    var resultData = [];
    var filterData = [];
    var isFocus = false;
    var focusIdx = 0;
    var dataIdx = 0;
    var totalPage = 0;
    var curPage = 0;
    var req_cd;
    var indicator;
    var searchWord = "";
    var searchResult_Data;
    var selSearchOpt;
    var selSearchFilter;
    var resultTotalCnt = 0;
    var nextDataCnt = 1;

    this.setReqPath = function (path) {
        req_cd = path;
    };

    this.getListType = function () {
        return "Package";
    };

    this.getView = function () {
        var div = _$("<div/>", {id:"listBox_area", class:"searchListPackageVOD"});
        div.append(_$("<div/>", {class:"filteringSetting_view_area"}));
        div.append(_$("<table/>", {id:"searchPackageVOD_table"}));
        for (var idx = 0 ; idx < 6; idx ++) {
            div.find("#searchPackageVOD_table").append(_$("<tr/>"));
            div.find("#searchPackageVOD_table tr").eq(idx).append(_$("<td/>").html("<div class='searchVODList_cell'>" +
                "<div class='searchVODList_title'></div>" +
                "<div class='searchVODList_subTitle'>" +
                    "<span class='searchVODList_resType'></span>" +
                    "<span class='searchVODList_resType_txt'>검색 결과</span>" +
                    "<span class='searchVODList_divide'></span>" +
                    "<span class='searchVODList_category'></span>" +
                "</div>" +
                "<img class='searchVODList_prInfo'>" +
                "</div>"));
        }
        div.append(_$("<div/>", {id:"searchPackageVODList_focus_div"}));
        div.append(_$("<div/>", {id:"vodInfo_area"}));
        div.find("#vodInfo_area").append(_$("<img/>", {id:"vodPoster_sdw", src: window.modulePath + "resource/poster_sdw_w280.png"}));
        div.find("#vodInfo_area").append(_$("<img/>", {id:"vodPoster_line", src: window.modulePath + "resource/poster_line_w280.png"}));
        div.find("#vodInfo_area").append(_$("<img/>", {id:"vodPoster_img"}));
        div.find("#vodInfo_area").append(_$("<div/>", {id:"vodPoster_icon_area"}));
        div.find("#vodInfo_area").append(_$("<img/>", {id:"vodPoster_lock_img", src: window.modulePath + "resource/img_vod_locked.png"}));

        div.find("#vodInfo_area").append(_$("<div/>", {id:"vodInfo_area_1"}));
        div.find("#vodInfo_area_1").append(stars.getView(0));
        div.find("#vodInfo_area_1").append(_$("<div/>", {id:"runtime_text"}));

        div.find("#vodInfo_area").append(_$("<div/>", {id:"vodInfo_area_2"}));
        div.find("#vodInfo_area_2").append(_$("<div/>", {id:"director_area"}));
        div.find("#vodInfo_area_2").append(_$("<div/>", {id:"actor_area"}));
        div.find("#vodInfo_area_2 #director_area").append(_$("<div/>", {id:"director_text_1"}).text("감독"));
        div.find("#vodInfo_area_2 #director_area").append(_$("<div/>", {id:"director_text_2"}));
        div.find("#vodInfo_area_2 #actor_area").append(_$("<div/>", {id:"actor_text_1"}).text("출연"));
        div.find("#vodInfo_area_2 #actor_area").append(_$("<div/>", {id:"actor_text_2"}));
        div.append(_$("<div/>", {class:"indicatorArea"}));
        return div;
    };

    this.setData = function (data, _filterData, _searchWord, totalCount) {
        nextDataCnt = 1;
        resultData = data;
        filterData = _filterData;
        searchWord = _searchWord;
        indicator = new Indicator(6);
        totalPage = Math.ceil(totalCount/6);
        resultTotalCnt = totalCount;
        if (totalPage == 1) totalPage = 0;
        INSTANCE.div.find("#listBox_area.searchListPackageVOD .indicatorArea").append(indicator.getView());
        indicator.setSize(totalPage, curPage);
        updateList(resultData);

        INSTANCE.div.find(".searchListPackageVOD .filteringSetting_view_area").html(
            "<span class='scaleSet_txt'>범위 : 전체</span>" +
            "<div class='filteringSet_bar'></div>" +
            "<span class='genreSet_txt'>장르 : 전체</span>" +
            "<div class='filteringSet_bar'></div>" +
            "<span class='sortSet_txt'>정렬 : 인기도순</span>"
        );
    };

    /**
     * VOD 결과 리스트를 갱신한다.
     *
     * @param listData
     */
    function updateList(listData) {
        var listElement = INSTANCE.div.find('#searchPackageVOD_table tr');
        for (var i = 0; i < 6; i++) {
            var itmeElement = _$(listElement[i]);
            if (!itmeElement) {continue;}
            if (listData[i]) {
                itmeElement.find('.searchVODList_title').text(listData[i].title);
                var subTitle = itmeElement.find(".searchVODList_subTitle");
                if (subTitle.css('visibility') === 'hidden') {
                    subTitle.css("visibility", "");
                }
                itmeElement.find('.searchVODList_resType').text(ServerCodeAdapter.getResTypeToName(listData[i].resType));
                itmeElement.find('.searchVODList_category').text(listData[i].cate_Name);
                itmeElement.find('.searchVODList_prInfo').attr("src", window.modulePath + "resource/icon_age_txtlist_" + UTIL.transPrInfo(listData[i].prInfo) + ".png");
            } else {
                itmeElement.find(".searchVODList_title").text("");
                itmeElement.find(".searchVODList_subTitle").css("visibility", "hidden");
                itmeElement.find(".searchVODList_prInfo").attr("src", "");
            }
        }
    }

    this.show = function () {
        var tmpFilteringData = SearchManager.getSearchPackageVODData();
        if (tmpFilteringData !== null) {
            selSearchFilter = SearchManager.getSelectVODFilterIndex().slice(0)[3];
            selSearchOpt = SearchManager.getSearchOptionData();
            filteringDataResetting(tmpFilteringData);
            focusIdx = 0;
            dataIdx = 0;
            curPage = 0;
            totalPage = Math.ceil(resultTotalCnt/6);
            if (totalPage == 1) totalPage = 0;
            indicator.setSize(totalPage, curPage);
            buttonFocusRefresh(focusIdx, dataIdx);
            changePage(curPage);
            SearchManager.setSearchPackageVODData(null);
        }
        INSTANCE.div.find("#listBox_area.searchListPackageVOD").removeClass("hide");
    };

    this.hide = function () {
        INSTANCE.div.find("#listBox_area.searchListPackageVOD").addClass("hide");
    };

    this.setFocus = function () {
        log.printDbg('called setFocus()');
        isFocus = true;
        focusIdx = 0;
        dataIdx = 0;
        buttonFocusRefresh(focusIdx, dataIdx);
        INSTANCE.div.find("#searchPackageVOD_table").addClass("focus");
        INSTANCE.div.find("#vodInfo_area").addClass("show");
        if (totalPage > 0) {
            INSTANCE.div.find("#searchPackageVOD_table").addClass("with_scroll");
            INSTANCE.div.find("#searchPackageVODList_focus_div").addClass("with_scroll");
            INSTANCE.div.find(".indicatorArea").addClass('show');
            INSTANCE.div.find("#searchResult_page_noti_area").addClass("show");
        }
    };

    this.setUnFocus = function () {
        log.printDbg('called setUnFocus()');
        isFocus = false;
        focusIdx = 0;
        dataIdx = 0;
        buttonFocusRefresh(focusIdx, dataIdx);
        INSTANCE.div.find("#searchPackageVOD_table").removeClass("focus");
        INSTANCE.div.find("#vodInfo_area").removeClass("show");
        if (totalPage > 0) {
            INSTANCE.div.find("#searchPackageVOD_table").removeClass("with_scroll");
            INSTANCE.div.find("#searchPackageVODList_focus_div").removeClass("with_scroll");
            INSTANCE.div.find(".indicatorArea").removeClass('show');
            INSTANCE.div.find("#searchResult_page_noti_area").removeClass("show");
        }
    };

    this.getIsRelationButton = function () {
        return true;
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    function getPosterIcon(index) {
        var res = "";
        var resCnt = 0;
        if (resultData[index].hdrYn == "Y") {
            res += "<img class='vodPoster_icon' src='" + window.modulePath + "resource/tag_poster_hdr.png'>";
            resCnt ++;
        }
        if (resultData[index].sdHd == "U") {
            res += "<img class='vodPoster_icon' src='" + window.modulePath + "resource/tag_poster_uhd.png'>";
            resCnt ++;
        }
        if (resultData[index].smartDvdYn == "Y" && resCnt < 2) {
            res += "<img class='vodPoster_icon' src='" + window.modulePath + "resource/tag_poster_mine.png'>";
        }
        return res;
    }

    function buttonFocusRefresh(index, dataIndex) {
        focusIdx = index;
        if (Math.floor(dataIndex / 6) != curPage) changePage(Math.floor(dataIndex / 6));
        INSTANCE.div.find("#searchPackageVOD_table tr td .searchVODList_cell").removeClass("focus");
        INSTANCE.div.find("#searchPackageVODList_focus_div").removeClass("focus");
        INSTANCE.div.find("#vodPoster_lock_img").removeClass("show");
        INSTANCE.div.find(".searchListPackageVOD .filteringSetting_view_area").removeClass("show");
        if (isFocus) {
            INSTANCE.div.find(".searchListPackageVOD .filteringSetting_view_area").addClass("show");
            INSTANCE.div.find("#searchPackageVODList_focus_div").addClass("focus");
            INSTANCE.div.find("#searchPackageVODList_focus_div").css("-webkit-transform",  "translateY(" + ((focusIdx * 118)-1) + "px)");
            INSTANCE.div.find("#searchPackageVOD_table tr td:eq(" + focusIdx + ") .searchVODList_cell").addClass("focus");

            INSTANCE.div.find("#vodPoster_img").attr("src", resultData[dataIndex].imgURL);
            INSTANCE.div.find("#vodInfo_area #vodPoster_icon_area").html(getPosterIcon(dataIndex));
            stars.setRate(INSTANCE.div.find("#listBox_area.searchListPackageVOD #vodInfo_area_1"), resultData[dataIndex].ollehp);
            if (resultData[dataIndex].runTime) {
                INSTANCE.div.find("#vodInfo_area_1 #runtime_text").text("| " + resultData[dataIndex].runTime + "분");
            } else {
                INSTANCE.div.find("#vodInfo_area_1 #runtime_text").text("");
            }
            if (resultData[dataIndex].director) {
                INSTANCE.div.find("#vodInfo_area_2 #director_text_2").text(resultData[dataIndex].director);
                INSTANCE.div.find("#vodInfo_area_2 #director_area").css("display", "block");
            } else {
                INSTANCE.div.find("#vodInfo_area_2 #director_area").css("display", "none");
                INSTANCE.div.find("#vodInfo_area_2 #director_text_2").text("");
            }

            if (resultData[dataIndex].actor) {
                INSTANCE.div.find("#vodInfo_area_2 #actor_text_2").text(resultData[dataIndex].actor);
                INSTANCE.div.find("#vodInfo_area_2 #actor_area").css("display", "block");
            } else {
                INSTANCE.div.find("#vodInfo_area_2 #actor_area").css("display", "none");
                INSTANCE.div.find("#vodInfo_area_2 #actor_text_2").text("");
            }
            if (window.UTIL.isLimitAge(resultData[dataIndex].prInfo)) INSTANCE.div.find("#vodPoster_lock_img").addClass("show");
        }
    }

    function changePage (page) {
        curPage = page;
        if (nextDataCnt*30 == (curPage+1)*6 && resultData.length < resultTotalCnt && (curPage+1)%5 == 0) {
            nextSearchResultData(nextDataCnt*30);
        }
        updateList(resultData.slice(curPage * 6));
        indicator.setPos(curPage);
    }

    function nextSearchResultData(positionIndex) {
        var tmpSearchType = SearchManager.getSearchType();
        var tmpSearchQry = SearchManager.getSearchQuery();
        switch (selSearchFilter) {
            case 0:
                SearchManager.searchTOTAL(function(data) {
                    searchResult_Data = (data);
                    dataListSetting(data);
                }, function() {
                    searchError();
                }, false, "0x01", tmpSearchType, false, positionIndex, 30, "0x09", searchWord, tmpSearchQry, "POINT", 2, "Y", selSearchOpt, "0.5", "");
                break;
            case 1:
                SearchManager.searchTOTAL(function(data) {
                    searchResult_Data = (data);
                    dataListSetting(data);
                }, function() {
                    searchError();
                }, false, "0x01", tmpSearchType, false, positionIndex, 30, "0x09", searchWord, tmpSearchQry, "OLLEHP", 2, "Y", selSearchOpt, "0.5", "");
                break;
            case 2:
                SearchManager.searchTOTAL(function(data) {
                    searchResult_Data = (data);
                    dataListSetting(data);
                }, function() {
                    searchError();
                }, false, "0x01", tmpSearchType, true, positionIndex, 30, "0x09", searchWord, tmpSearchQry, "TITLE", 2, "Y", selSearchOpt, "0.5", "");
                break;
            case 3:
                SearchManager.searchTOTAL(function(data) {
                    searchResult_Data = (data);
                    dataListSetting(data);
                }, function() {
                    searchError();
                }, false, "0x01", tmpSearchType, false, positionIndex, 30, "0x09", searchWord, tmpSearchQry, "INSERT_DATE", 2, "Y", selSearchOpt, "0.5", "");
                break;
            default:
                SearchManager.searchTOTAL(function(data) {
                    searchResult_Data = (data);
                    dataListSetting(data);
                }, function() {
                    searchError();
                }, false, "0x01", tmpSearchType, false, positionIndex, 30, "0x09", searchWord, tmpSearchQry, "POINT", 2, "Y", selSearchOpt, "0.5", "");
                break;
        }

        function dataListSetting(data) {
            tmpData = _$(data).find("search[code='PRODUCT_VOD']").find("PROD_VOD_ITEM");
            tmpData.each(function () {
                resultData.push({
                    title: _$(this).find("TITLE").text(),
                    imgURL: _$(this).find("URL").text(),
                    cate_Name: _$(this).find("CATEGORY_PULLNAME").text(),
                    cat_Id: _$(this).find("CATEGORY_ID").text(),
                    const_Id: _$(this).find("CONST_ID").text(),
                    seriesYN: _$(this).find("SERIES_YN").text(),
                    wonYn: _$(this).find("WON_YN").text(),
                    prInfo: _$(this).find("RATING").text(),
                    director: _$(this).find("DIRECTOR").text(),
                    actor: _$(this).find("ACTOR").text(),
                    ollehp: _$(this).find("OLLEHP").text(),
                    runTime: _$(this).find("RUNTIME").text(),
                    sdHd:_$(this).find("HD_SD").text(),
                    hdrYn:_$(this).find("HDR_YN").text(),
                    smartDvdYn:_$(this).find("SMART_DVD_YN").text(),
                    // [hw.boo] RES_TYPE 검색결과 유형 추가 (1 : 제목검색, 2 : 시맨틱검색, 3 : 테마검색)
                    resType: _$(this).find("RES_TYPE").text()
                });
            });
            nextDataCnt++;
        }
    }

    function filteringDataResetting(_value) {
        totalResult_Cnt = _$(_value).find("output").find("itemNum").eq(0).text();
        spellResult_Cnt = _$(_value).find("output").find("spellNum").eq(0).text();
        var tmpData;
        var vodTotalCnt;

        scaleFilterData = [];
        genreFilterData = [];
        filterData = [];
        resultTotalCnt = parseInt(_$(_value).find("search[code='PRODUCT_VOD']").find("totalNum").text());
        vodTotalCnt = parseInt(_$(_value).find("search[code='VOD']").find("totalNum").text());
        SearchManager.setSearchResultCount("VODPkg", "("+resultTotalCnt+")", resultTotalCnt);
        SearchManager.setSearchResultCount("VOD", "("+vodTotalCnt+")", vodTotalCnt);
        tmpData = _$(_value).find("search[code='VOD']").find("KEYWORD_LIST").find("KEYWORD_ITEM");
        tmpData.each(function () {
            filterData.push({keyType:_$(this).find("KEYWORD_TYPE").text(),
                keyWord:_$(this).find("WORD").text(),
                srchOpt:_$(this).find("SRCH_OPT").text(),
                isCheck:_$(this).find("IS_CHECKED").text()});
            if (_$(this).find("KEYWORD_TYPE").text() == "10" && _$(this).find("IS_CHECKED").text() == "Y") {
                scaleFilterData.push({keyType:_$(this).find("KEYWORD_TYPE").text(),
                    keyWord:_$(this).find("WORD").text(),
                    srchOpt:_$(this).find("SRCH_OPT").text(),
                    isCheck:_$(this).find("IS_CHECKED").text()});
            } else if (_$(this).find("IS_CHECKED").text() == "Y") {
                genreFilterData.push({keyType:_$(this).find("KEYWORD_TYPE").text(),
                    keyWord:_$(this).find("WORD").text(),
                    srchOpt:_$(this).find("SRCH_OPT").text(),
                    isCheck:_$(this).find("IS_CHECKED").text()});
            }
        });

        var filterText = selSearchFilter == 0 ? "인기도순":(selSearchFilter == 1?"별점순":(selSearchFilter == 2 ? "가나다순":"날짜순"));
        INSTANCE.div.find(".searchListPackageVOD .filteringSetting_view_area").html(
            "<span class='scaleSet_txt'>범위 : " + (scaleFilterData.length == 0 ? "전체" : (scaleFilterData[0].keyWord + (scaleFilterData.length == 1 ? "" : ("...(" + (scaleFilterData.length) + ")")))) + "</span>" +
            "<div class='filteringSet_bar'></div>" +
            "<span class='genreSet_txt'>장르 : " + (genreFilterData.length == 0 ? "전체" : (genreFilterData[0].keyWord + (genreFilterData.length == 1 ? "" : ("...(" + (genreFilterData.length) + ")")))) + "</span>" +
            "<div class='filteringSet_bar'></div>" +
            "<span class='sortSet_txt'>정렬 : " + filterText + "</span>"
        );

        _$(".searchResult #categoryArea #cate_focus subtitle").text("("+resultTotalCnt+")");
        _$(".searchResult #categoryArea #menuArea span.focus subtitle").text("("+resultTotalCnt+")");
        var tmpDiv = _$(".searchResult #categoryArea span");
        for (var idx = 0 ;idx < tmpDiv.length ; idx ++) {
            if (tmpDiv.eq(idx).text().indexOf("VOD") > -1) {
                tmpDiv.eq(idx).find("subtitle").text("(" + vodTotalCnt + ")");
            }
        }
        var tmpTotal = resultTotalCnt;
        var tmpKey = ["VOD", "TVApp", "TVPro", "RelWord"];
        for (var idx = 0 ; idx < 4; idx ++) {
            tmpTotal += SearchManager.getSearchResultCount(tmpKey[idx])[1];
        }
        _$(".searchResult #contents #resultSubTitle_text").text("총 " + tmpTotal + "개의 콘텐츠가 검색 되었습니다");
        resultData = [];
        tmpData = _$(_value).find("search[code='PRODUCT_VOD']").find("PROD_VOD_ITEM");
        tmpData.each(function () {
            resultData.push({title: _$(this).find("TITLE").text(),
                imgURL: _$(this).find("URL").text(),
                cate_Name: _$(this).find("CATEGORY_PULLNAME").text(),
                cat_Id: _$(this).find("CATEGORY_ID").text(),
                const_Id: _$(this).find("CONST_ID").text(),
                seriesYN: _$(this).find("SERIES_YN").text(),
                wonYn: _$(this).find("WON_YN").text(),
                prInfo: _$(this).find("RATING").text(),
                director: _$(this).find("DIRECTOR").text(),
                actor: _$(this).find("ACTOR").text(),
                ollehp: _$(this).find("OLLEHP").text(),
                runTime: _$(this).find("RUNTIME").text(),
                sdHd:_$(this).find("HD_SD").text(),
                hdrYn:_$(this).find("HDR_YN").text(),
                smartDvdYn:_$(this).find("SMART_DVD_YN").text()});
        });
        SearchManager.setSearchVODData(_value);
        nextDataCnt = 1;
    }

    this.onKeyAction = function (keyCode) {
        var keyAction = {isUsed: false};
        switch (keyCode) {
            case KEY_CODE.UP:
                if (dataIdx > 0) dataIdx--;
                focusIdx = dataIdx % 6;
                buttonFocusRefresh(focusIdx, dataIdx);
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.DOWN:
                dataIdx = (dataIdx < resultTotalCnt - 1 ) ? dataIdx + 1 : 0;
                focusIdx = dataIdx % 6;
                buttonFocusRefresh(focusIdx, dataIdx);
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.RED:
                if (dataIdx%6 === 0) {
                    if (resultTotalCnt > 6) {
                        if (curPage > 0) {
                            curPage --;
                        }
                        changePage(curPage);
                        dataIdx = curPage * 6;
                        focusIdx = dataIdx % 6;
                        buttonFocusRefresh(focusIdx, dataIdx);
                    }
                } else {
                    dataIdx = curPage * 6;
                    focusIdx = dataIdx % 6;
                    buttonFocusRefresh(focusIdx, dataIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.BLUE:
                if (dataIdx === resultTotalCnt-1 || dataIdx%6 === 5) {
                    if (resultTotalCnt > 6) {
                        curPage = (totalPage-1 > curPage) ? curPage + 1: 0;
                        changePage(curPage);
                        dataIdx = Math.min(resultTotalCnt-1, curPage * 6 + 5);
                        focusIdx = dataIdx % 6;
                        buttonFocusRefresh(focusIdx, dataIdx);
                    }
                } else {
                    dataIdx = Math.min(resultTotalCnt-1, curPage * 6 + 5);
                    focusIdx = dataIdx % 6;
                    buttonFocusRefresh(focusIdx, dataIdx);
                }
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.LEFT:
            case KEY_CODE.BACK:
                keyAction.isUsed = true;
                keyAction.prevMenu = true;
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.ENTER:
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SEARCH, resultData[focusIdx + (curPage * 6)].cat_Id, resultData[focusIdx + (curPage * 6)].const_Id, req_cd);
                } catch (e) {
                }
                log.printDbg("[Search Package VOD reqPathCd] : " + req_cd);
                openDetailLayer(resultData[focusIdx + (curPage * 6)].cat_Id, resultData[focusIdx + (curPage * 6)].const_Id, req_cd);
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            case KEY_CODE.CONTEXT:
                LayerManager.activateLayer({
                    obj: {
                        id: "ListFilteringPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            data : filterData,
                            listType: "Pkg",
                            popType : "VOD_PACKAGE",
                            complete : function (filterData, price, lineup) {
                                var tmpResult;
                                var tmpSearchType = SearchManager.getSearchType();
                                var tmpSearchQry = SearchManager.getSearchQuery();
                                switch (lineup) {
                                    case 0:
                                        SearchManager.searchTOTAL(function(data) {
                                            tmpResult = data;
                                        }, function() {
                                            searchError();
                                        }, false, "0x01", tmpSearchType, false, 0, 30, "0x09", searchWord, tmpSearchQry, "POINT", 2, "Y", filterData, "0.5", "");
                                        break;
                                    case 1:
                                        SearchManager.searchTOTAL(function(data) {
                                            tmpResult = data;
                                        }, function() {
                                            searchError();
                                        }, false, "0x01", tmpSearchType, false, 0, 30, "0x09", searchWord, tmpSearchQry, "OLLEHP", 2, "Y", filterData, "0.5", "");
                                        break;
                                    case 2:
                                        SearchManager.searchTOTAL(function(data) {
                                            tmpResult = data;
                                        }, function() {
                                            searchError();
                                        }, false, "0x01", tmpSearchType, true, 0, 30, "0x09", searchWord, tmpSearchQry, "TITLE", 2, "Y", filterData, "0.5", "");
                                        break;
                                    case 3:
                                        SearchManager.searchTOTAL(function(data) {
                                            tmpResult = data;
                                        }, function() {
                                            searchError();
                                        }, false, "0x01", tmpSearchType, false, 0, 30, "0x09", searchWord, tmpSearchQry, "INSERT_DATE", 2, "Y", filterData, "0.5", "");
                                        break;
                                }
                                if (parseInt(_$(tmpResult).find("search[code='PRODUCT_VOD']").find("totalNum").text()) == 0 || parseInt(_$(tmpResult).find("output").find("totalNum").eq(0).text()) == 0) return false;
                                selSearchOpt = filterData;
                                SearchManager.setSearchOptionData(filterData);
                                selSearchFilter = lineup;
                                searchResult_Data = tmpResult;
                                filteringDataResetting(searchResult_Data);
                                focusIdx = 0;
                                dataIdx = 0;
                                curPage = 0;
                                totalPage = Math.ceil(resultTotalCnt/6);
                                if (totalPage == 1) totalPage = 0;
                                indicator.setSize(totalPage, curPage);
                                buttonFocusRefresh(focusIdx, dataIdx);
                                changePage(curPage);
                                return true;
                            }
                        }
                    },
                    moduleId: "module.search",
                    new: true,
                    visible: true
                });
                keyAction.isUsed = true;
                keyAction.prevMenu = false;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };
    function searchError() {
        LayerManager.activateLayer({
            obj: {
                id: "SearchError",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: "오류",
                    message: ["검색 요청 중 오류가 발생했습니다", "다른 검색어로 검색하거나", "잠시 후 다시 이용해 주시기 바랍니다"],
                    button: "닫기",
                    reboot: false,
                    callback: function() {
                        totalPage = Math.ceil(resultData.length/6);
                        indicator.setSize(totalPage, curPage);
                    }
                }
            },
            moduleId: "module.search",
            visible: true
        });
    }
};