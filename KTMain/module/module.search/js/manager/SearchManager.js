/**
 * Created by Lazuli on 2016-11-30.
 */

window.SearchManager = (function() {

    var APP_ID;

    var selSearchQry;

    function _setSearchQuery(value) {
        selSearchQry = value;
    }

    var selSearchType;

    function _setSearchType(value) {
        selSearchType = value;
    }

    var searchOptionData = "";

    function _setSearchOptionData(value) {
        searchOptionData = value;
    }

    var searchVODData = null;
    var searchPkgVodData = null;

    function _setSearchVODData(value) {
        searchVODData = value;
    }

    function _setSearchPackageVODData(value) {
        searchPkgVodData = value;
    }

    var trendsWordsData = null;
    var trendsSetTime = null;

    function _setTrendsWordsData(value) {
        trendsWordsData = value;
    }

    function _setTrendsWordsTime(value) {
        trendsSetTime = value;
    }

    var searchResultCnt = {VOD:["", 0], VODPkg:["", 0], TVApp:["", 0], TVPro:["", 0], RelWord:["", 0], DAUM:["", 0]};

    function _setSearchResultCount(key, value, _count) {
        searchResultCnt[key][0] = value;
        searchResultCnt[key][1] = _count;
    }

    function _getSearchResultCount(key) {
        if(key) return searchResultCnt[key];
        return searchResultCnt;
    }

    var selectVODIdx = [];
    var selectPkgVODIdx = [];
    var selectTVProIdx = 0;
    var selectTVAppIdx = 0;

    function _setSelectVODFilterIndex(value) {
        selectVODIdx = value;
    }

    function _setSelectPackageVODFilterIndex(value) {
        selectPkgVODIdx = value;
    }

    var recentSearchWord = [];

    function _addRecentSearchWord(_searchWord) {
        for (var idx = 0; idx < recentSearchWord.length; idx ++) {
            if (recentSearchWord[idx].searchWord == _searchWord.searchWord) {
                _setRecentSearchWordDelete(idx);
                break;
            }
        }
        recentSearchWord.push(_searchWord);
        if (recentSearchWord.length > 4) {
            recentSearchWord.splice(0, 1);
        }
    }

    function _getAllRecentSearchWord() {
        return recentSearchWord;
    }

    function _getRecentSearchWord(index) {
        return recentSearchWord[index];
    }

    function _getRecentSearchWordLength() {
        return recentSearchWord.length;
    }

    function _setRecentSearchWordDelete(index) {
        recentSearchWord.splice(index, 1);
    }

    function _getAppId() {
        log.printDbg("_getAppId()");
        var tmpId;
        // (CONSTANT.IS_HDR === true ? (window.CONSTANT.STB_TYPE.UHD3 ? "UHDIII": (window.CONSTANT.STB_TYPE.ANDROID ? "GIGAGENIE" : "UHDII") ) : "UHD")
        if(CONSTANT.IS_HDR === true) {
            if(window.CONSTANT.STB_TYPE.UHD3) {
                tmpId = "UHDIII";
            }else {
                if(window.CONSTANT.STB_TYPE.ANDROID) {
                    tmpId = "GIGAGENIE";
                }else {
                    tmpId = "UHDII";
                }
            }
        }else {
            tmpId = "UHD";
        }

        APP_ID = tmpId;
        log.printDbg("APP_ID = "+APP_ID);
        return APP_ID;
    }

    function ajaxRequest(url, type, isAsync, cbSuccess, cbError) {
        log.printDbg("[Search Request] : " + url);
        _$.ajax({
            type: "GET",
            url: url,
            dataType: type,
            crossDomain: !0,
            async: isAsync,
            timeout: 5e3,
            error: function (a, b, c) {
                LayerManager.stopLoading();
                log.printErr(a);
                log.printErr(b);
                log.printErr(c);
                cbError();
            },
            success: function (a, b, c) {
                LayerManager.stopLoading();
                cbSuccess(a);
                log.printDbg("a = " + a);
                log.printDbg("b = " + b);
                log.printDbg("c = " + c);
            }
        });
    }

    function encodeStr(str, i) {
        if (!i || "utf-8" == i || !KTW.utils.charsetencodes[i])return encodeURIComponent(str);
        var f = KTW.utils.charsetencodes[i];
        if (!str) str="";
        return str.replace(/./g, function(d) {
            return !f[d] ? encodeURIComponent(d) : f[d];
        });
    }

    // var searchURL = DEF.RELEASE_MODE==="LIVE" ? searchConfig.HTTP.SEARCH.LIVE_URL : searchConfig.HTTP.SEARCH.TEST_URL;
    var searchURL = searchConfig.HTTP.SEARCH.LIVE_URL;

    function getTrendWords(callback, errCallback) {
        var tUrl = searchURL + "/getTrendWords";
        var cust_env, package_code, package_list;
        if (CONSTANT.IS_OTS === true) package_code = "2S55";
        else package_code = "2P01";
        try {
            cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            package_code = cust_env.pkgCode;
        } catch (e) {
            // TODO package 값이 없을 경우 예외처리 필요
            log.printErr(e);
            AppServiceManager.requestCustInfo(function() {
                cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                package_code = cust_env.pkgCode;
            });
        }

        _getAppId();

        tUrl = tUrl + "?maxRankTypeCnt=12"
        + "&maxRankItemCnt=4"
        + "&maxThemaCnt=2"
        + "&maxContsCnt=10"
        + "&product_id=" + package_code
        + "&sa_id=" + DEF.SAID
        + "&appID=" + APP_ID
        + "&reqRankVersion=3.0"
        + "&STB_VER=0.6";

        ajaxRequest(tUrl, "json", true, function(data) {
            callback(data);
        }, function() {
            errCallback();
        });
    }

    function getAutoWords(callback, errCallback, searchWord) {
        var sUrl = searchURL + "/getTotalAutoWords", package_Code = "2P01";
        var package_List = null;
        var cust_env;
        try {
            cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            package_Code = cust_env && cust_env.pkgCode;
            package_List = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
            package_List && (package_Code = package_Code + "," + package_List);
        } catch (e) {
            log.printErr(e);
            AppServiceManager.requestCustInfo(function() {
                cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                package_Code = cust_env && cust_env.pkgCode;
                package_List = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                package_List && (package_Code = package_Code + "," + package_List);
            });
        }

        _getAppId();
        sUrl = sUrl + "?contents_type=0x00&dtype=xml&uquery=" + encodeStr(searchWord, "euckr") + "&returnCount=20&sorting=&cate=&product_id=" +
            package_Code + "&sa_id=" + DEF.SAID + "&spellType=1&adult_yn=true&appID=" +APP_ID +"&STB_VER=0.6";

        ajaxRequest(sUrl, "xml", true, function(data) {
            callback(data);
        }, function() {
            errCallback();
        });
    }

    function searchMyPackageTOTAL(contentsType, searchType, bsort, searchPosition, searchCount, searchField,
                                  searchWord, searchQry, sortFields, spellType, detail,
                                  searchOpt, STB_VER, areaCode, wonYn) {
        var qry = searchURL + "/searchTOTAL";
        var cust_env, area_code, package_list;
        area_code = "306237";
        try {
            cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            area_code = cust_env.dongCd;
            package_list = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
        } catch (e) {
            log.printErr(e);
            AppServiceManager.requestCustInfo(function() {
                cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                area_code = cust_env.dongCd;
                package_list = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
            }, true);
        }
        _getAppId();
        qry = qry + "?contents_type=" + (contentsType || "0xFF")
            + "&searchType=" + (searchType || 3)
            + "&searchWord=" + (searchWord ? encodeStr(searchWord, "euckr") : "")
            + "&sortFields=" + (sortFields || "POINT")
            + "&bsort=" + bsort
            + "&searchField=" + (searchField || "")
            + "&searchQry=" + (encodeStr(searchQry, "euckr") || "")
            + "&searchOpt=" + (searchOpt ? (encodeStr(searchOpt, "euckr") || "") : "")
            + "&searchPosition=" + (searchPosition || 0)
            + "&searchCount=30"
            + "&cate="
            + "&product_id=" + package_list
            + "&sa_id=" + DEF.SAID
            + "&adult_yn=true"
            + "&detail=Y"
            + "&dtype=xml"
            + "&appID=" + APP_ID
            + "&exposure_time=24"
            + "&spellType=" + spellType
            + "&STB_VER=0.6"
            + "&areaCode=" + area_code
            + "&won_yn=" + (wonYn||"")
            + "&STB_mode=" + (window.KidsModeManager.isKidsMode() ? "Kids":"Normal")
            + "&maxThemaCnt=0"
            + "&maxContsCnt=0";

        ajaxRequest(qry, "xml", false, function(data) {
            callback(data);
        }, function() {
            errCallback();
        });
    }

    function searchTOTAL(callback, errCallback, isAsync, contentsType, searchType, bsort, searchPosition, searchCount, searchField,
                         searchWord, searchQry, sortFields, spellType, detail,
                         searchOpt, STB_VER, areaCode, wonYn, dataIndex, isSearchAuto) {
        if (isAsync && !isSearchAuto) LayerManager.startLoading({preventKey:true});
        var qry = searchURL + "/searchTOTAL";
        var cust_env, package_code, package_list, area_code;

        try {
            cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            package_code = cust_env.pkgCode;
            area_code = cust_env.dongCd;
            package_list = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
            if (package_code == null || package_code.length == 0) {
                AppServiceManager.requestCustInfo(function() {
                    package_code = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
                    package_list = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                    package_list && (package_code = package_code + "," + package_list);
                }, true);
            } else {
                if (package_list == null) {
                    AppServiceManager.updatePkgInfo(function() {
                        package_code = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
                        package_list = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                        package_list && (package_code = package_code + "," + package_list);
                    });
                } else {
                    package_list && (package_code = package_code + "," + package_list);
                }
            }
        } catch (e) {
            log.printErr(e);
            AppServiceManager.requestCustInfo(function() {
                cust_env = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                package_code = cust_env.pkgCode;
                area_code = cust_env.dongCd;
                package_list = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                package_list && (package_code = package_code + "," + package_list);
            }, true);
        }
        _getAppId();
        if (searchType == 4 || searchType == 5 || searchType == 6 || searchType == 7) searchField = "";
        qry = qry + "?contents_type=" + (contentsType || "0xFF")
            + "&searchType=" + (searchType || 3)
            + "&searchWord=" + (searchWord ? encodeStr(searchWord, "euckr") : "")
            + "&sortFields=" + (sortFields || "POINT")
            + "&bsort=" + bsort
            + "&searchField=" + (searchField || "")
            + "&searchQry=" + (encodeStr(searchQry, "euckr") || "")
            + "&searchOpt=" + (searchOpt ? (encodeStr(searchOpt, "euckr") || "") : "")
            + "&searchPosition=" + (searchPosition || 0)
            + "&searchCount=30"
            + "&cate="
            + "&product_id=" + package_code
            + "&sa_id=" + DEF.SAID
            + "&adult_yn=true"
            + "&detail=Y"
            + "&dtype=xml"
            + "&appID=" + APP_ID
            + "&exposure_time=24"
            + "&spellType=" + spellType
            + "&STB_VER=0.6"
            + "&areaCode=" + area_code
            + "&won_yn=" + (wonYn||"")
            + "&STB_mode=" + (window.KidsModeManager.isKidsMode() ? "Kids":"Normal")
            + "&maxThemaCnt=0"
            + "&maxContsCnt=0";

        ajaxRequest(qry, "xml", isAsync, function(data) {
            callback(data, dataIndex);
        }, function() {
            errCallback();
        });
    }

    return {
        setSearchQuery: _setSearchQuery,
        getSearchQuery: function() { return selSearchQry; },
        setSearchType: _setSearchType,
        getSearchType: function() { return selSearchType; },
        setSearchOptionData: _setSearchOptionData,
        getSearchOptionData: function() { return searchOptionData; },
        setSearchVODData: _setSearchVODData,
        getSearchVODData: function() { return searchVODData; },
        setSearchPackageVODData: _setSearchPackageVODData,
        getSearchPackageVODData: function() { return searchPkgVodData; },
        setTrendsWordsData : _setTrendsWordsData,
        getTrendsWordsData : function() { return trendsWordsData; },
        setTrendsWordsTime : _setTrendsWordsTime,
        getTrendsWordsTime : function() { return trendsSetTime; },
        setSearchResultCount : _setSearchResultCount,
        getSearchResultCount : _getSearchResultCount,
        setSelectVODFilterIndex: _setSelectVODFilterIndex,
        setSelectPackageVODFilterIndex: _setSelectPackageVODFilterIndex,
        getSelectVODFilterIndex: function() { return selectVODIdx; },
        getSelectPackageVODFilterIndex: function() { return selectPkgVODIdx; },
        setResetFilterData: function() {selectVODIdx=[];selectPkgVODIdx=[];selectTVAppIdx = 0; selectTVProIdx = 0; searchOptionData = ""; searchVODData = null; searchPkgVodData = null;},
        setSelectTVProgramIndex: function (value) { selectTVProIdx = value; },
        getSelectTVProgramIndex: function () {return selectTVProIdx;},
        setSelectTVAppIndex: function (value) { selectTVAppIdx = value; },
        getSelectTVAppIndex: function () { return selectTVAppIdx; },
        getTrendWords: getTrendWords,
        getAutoWords: getAutoWords,
        searchMyPackageTOTAL: searchMyPackageTOTAL,
        searchTOTAL: searchTOTAL,
        addRecentSearchWord : _addRecentSearchWord,
        getAllRecentSearchWord : _getAllRecentSearchWord,
        getRecentSearchWord : _getRecentSearchWord,
        getRecentSearchWordLength : _getRecentSearchWordLength,
        setRecentSearchWordDelete : _setRecentSearchWordDelete,
        getAppId : _getAppId
    }
}());