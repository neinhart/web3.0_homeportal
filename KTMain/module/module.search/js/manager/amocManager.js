"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId);
 * posterDm.ajaxMenu(createPoster, failAjax);
 *
 */
searchMenu.amocManager = (function () {
    var api;
    var data;

    var IS_RELEASE = DEF.RELEASE_MODE == "LIVE";
    var AjaxFactory = homeImpl.get(homeImpl.DEF_FRAMEWORK.AJAX_FACTORY);

    /**
     * ajax를 이용하여 json 데이터 수집<br>
     * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
     * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    var ajax = function (async, timeout, url, postData, /**Function*/callback, reqType, param) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAX request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }

        if (postData instanceof Object) {
            if (!postData) {
                postData = {};
            }
            postData.WMOCKey = DEF.AMOC.AUTH_KEY;
        } else {
            postData = postData ? postData + "&WMOCKey=" + DEF.AMOC.AUTH_KEY : "WMOCKey=" + DEF.AMOC.AUTH_KEY;
        }

        return AjaxFactory.createAjax("AMOC Manager", async, (reqType ? reqType : "post"), "json", url, postData, false, timeout, callback, param);
    };

    var ajaxData = function (async, timeout, url, postData, /**Function*/callback) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAXDATA request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }
        postData = postData ? postData : new Object();
        postData.WMOCKey = DEF.AMOC.AUTH_KEY;
//	    postData = postData ? postData+"&WMOCKey="+DEFAULT_AMOC_AUTH_KEY : "WMOCKey="+DEFAULT_AMOC_AUTH_KEY;

        return AjaxFactory.createAjax("AMOC Manager [DATA]", async, "post", "json", url, postData, false, timeout, callback, null);
    };

    var noop = function () {
    }

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AMOC AJAX request stop!!");
            try {
                request.abort();
            } catch (e) {
                log.printExec(e.message);
            }

            request = null;
        }
    }

    function _useMyStarPointVodNxt(callback, saId, pkgYn, contsId, contsName, buyingDate, buyingPrice, buyingType, catId, appCd,
                                   reqPathCd, ltFlag, hdYn, stbIp, suppInfo, pointType, saleYn, percent) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-pgcpn-api/vod/buy/with-star-point-nxt";

        data = {
            "saId": saId,
            "pkgYn": pkgYn, //묶음 구매 (Y/N)
            "contsId": contsId, //(건별시는 컨텐츠 ID, 묶음 구매시는 CAT ID)
            "contsName": contsName, //역시 컨텐츠명 / 카테고리명
            "buyingDate": buyingDate,
            "buyingPrice": buyingPrice,
            "buyingType": buyingType, //C 입력
            "catId": catId,
            "appCd": appCd, //appcd
            "reqPathCd": reqPathCd,
            "ltFlag": ltFlag,
            "hdYn": hdYn,
            "stbIp": stbIp,
            "suppInfo": suppInfo,
            "pointType": pointType,
            "saleYn": (saleYn ? saleYn : "N"),
            "percent": percent,
        };
        return ajaxData(true, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _useTvMoneyVodNxt(callback, saId, pkgYn, contsId, contsName, buyingDate, buyingPrice, buyingType, catId,
                               catName, appCd, reqPathCd, ltFlag, hdYn, stbIp, tvPoint, tvMoney, detPoint1, detPoint2,
                               suppInfo, pointType, saleYn) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-pgcpn-api/vod/buy/with-tvmoney-nxt";

        data = {
            "saId": saId, //
            "pkgYn": pkgYn, //
            "itemId": contsId, //
            "itemName": contsName, //
            "buyingDate": buyingDate, //
            "amount": buyingPrice, //
            "buyingType": buyingType, //
            "catId": catId, //
            "catName": catName, //
            "appCd": appCd, //
            "reqPathCd": reqPathCd, //
            "ltFlag": ltFlag, //
            "hdYn": hdYn, //
            "stbIp": stbIp, //
            "tvPoint": tvPoint, //
            "tvMoney": tvMoney, //
            "detPoint1": detPoint1, //
            "detPoint2": detPoint2, //
            "suppInfo": suppInfo, //
            "pointType": pointType, //
            "saleYn": (saleYn ? saleYn : "N"), //
        };

        return ajaxData(true, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _useComplexPayNxt(callback, saId, stbIp, itemId, itemName, amount, pkgYn, ltFlag, buyingType, hybridMode, buyingDate, catId, appCd,
                               reqPathCd, hdYn, saleYn, starPoint, tvPoint, tvMoney, cash, catName) {
        if (!reqPathCd) {
            reqPathCd = "01";
        }
        if (!buyingType) {
            buyingType = "H";
        }

        api = "/amoc-pgcpn-api/vod/buy/with-complex-pay-nxt";
        data = {
            "saId": saId,
            "stbIp": stbIp,
            "itemId": itemId,
            "itemName": itemName,
            "amount": amount,
            "pkgYn": pkgYn,
            "ltFlag": ltFlag,
            "buyingType": buyingType,
            "hybridMode": hybridMode,
            "buyingDate": buyingDate,
            "catId": catId,
            "appCd": appCd,
            "detPoint1": 0,
            "detPoint2": 0,
            "reqPathCd": reqPathCd,
            "hdYn": hdYn,
            "saleYn": (saleYn ? saleYn : "N"),
            "starPoint": starPoint,
            "tvPoint": tvPoint,
            "tvMoney": tvMoney,
            "cash": cash,
            "catName": catName,
        };

        return ajaxData(true, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _requestOPC(callback, saId, pkgYn, saleYn) { //미사용
        api = "/amoc-api/vod/buy/with-pgmobile";
        data = "saId=" + saId + "&pkgYn=" + pkgYn + "&saleYn=" + (saleYn ? saleYn : "N") + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _requestOAC(callback, saId, buyingPrice, mobile, telcoCode, subscriberSSN, orderDate, contentType, contsName, stbIp, merchantId) { //미사용
        api = "/amoc-pgcpn-api/vod/buy/pgmobile-auth";
        data = "saId=" + saId + "&buyingPrice=" + buyingPrice + "&mobile=" + mobile + "&telcoCode=" + telcoCode + "&subscriberSSN=" + subscriberSSN
            + "&orderDate=" + orderDate + "&contentType=" + contentType + "&contsName=" + contsName + "&stbIp=" + stbIp + "&merchantId=" + merchantId + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _registPrepaidCpn(callback, domainId, saId, cpnNo) { //미사용
        api = "/amoc-pgcpn-api/vod/coupon-register";
        data = "domainId=" + domainId + "&saId=" + saId + "&cpnNo=" + cpnNo + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _getMyExpireList(callback, domainId, saId, currPageNo, cntPerPage) { //미사용
        api = "/amoc-pgcpn-api/vod/coupon-expire-due-list";
        data = "domainId=" + domainId + "&saId=" + saId + "&currPageNo=" + currPageNo + "&cntPerPage=" + cntPerPage + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    /**
     *
     * @param pptPkgCd: 기간 정액제 상품코드 (getLinkTimeNxt 에서 받은 값 사용. 웹은 없음)
     * @private
     */
    function _authorizePVOD(callback, saId, contsId, buyingDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/pvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&buyingDate=" + buyingDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint + "&pptPkgCd=";
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _authorizeOTNPVOD(callback, saId, contsId, buyingDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/otn-pvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&buyingDate=" + buyingDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _authorizeSVOD(callback, saId, contsId, systemDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/svod";
        data = "saId=" + saId + "&contsId=" + contsId + "&systemDate=" + systemDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _authorizeFVOD(callback, saId, contsId, systemDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/fvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&systemDate=" + systemDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTPS_URL + api, data, callback);
    }

    function _setVodEnd(callback, saId, contsId, second) {
        api = "/amoc-api/vod/end";
        data = "saId=" + saId + "&contsId=" + contsId + "&second=" + second;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    /**
     * request-
     * said: SAID
     * contsId: assetId
     * uhdYn: uhd stb 여부 ('Y'/'N')
     * buyTypeYn: Biz Type 인지 아닌지 여부 'N' 이면 아님 web 은 BizType 없음
     */
    function _getLinkTimeNxt(callback, saId, contsId, isAsync, param) {
        api = "/amoc-api/vod/prepare-nxt";
        data = "saId=" + saId + "&contsId=" + contsId + "&uhdYn=" + (Contant.IS_UHD === true ? "Y" : "N") + "&buyTypeYn=Y";
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    function _getViewHistEP4(postData, callback) {
        api = "/amoc-api/vod/watch-history";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, postData, callback);
    }

    function _setDelWatch(callback, saId, contsId, contYn) {
        api = "/amoc-api/vod/watch-history/delete";
        data = "saId=" + saId + "&contsId=" + contsId + "&contYn=" + contYn + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    /**
     *
     * @param callback
     * @param saId
     * @param catId
     * @param cmbYn : 통합편성여부 (Y/N)
     * @param param
     * @private
     */
    function _getContStatusNxt(callback, saId, catId, cmbYn, param) {
        api = "/amoc-api/vod/category/auth-info-nxt";
        data = "saId=" + saId + "&catId=" + catId + "&cmbYn=" + (cmbYn ? cmbYn : "N") + "&buyTypeYn=Y&uhdYn=" + (Contant.IS_UHD === true ? "Y" : "N");
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     *
     * @param callback
     * @param saId
     * @param catId
     * @param cmbYn : 통합편성여부 (Y/N)
     * @param param
     * @private
     */
    function _getItemPerInfo(callback, catId, param) {
        api = "/amoc-api/vod/category/item-per-info";
        data = "saId=" + DEF.SAID + "&catId=" + catId;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     * 회차/단편
     * @param callback
     * @param catId
     * @param saId
     * @param isAsync
     * @param platType W 가 web
     * @param smartYn Y 면 smart app tv N 이면 stb.
     * @param param
     * @private
     */
    function _getCateContExt(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/series-conts-ext";
        data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     * 회차/통합편성
     * @param callback
     * @param catId
     * @param saId
     * @param isAsync
     * @param platType W 가 web
     * @param smartYn Y 면 smart app tv N 이면 stb.
     * @param param
     * @private
     */
    function _getCateContNxt(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/series-conts-nxt";
        data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    function _getCateContExtW3(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/series-conts-ext-w3";
        // data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=Y&smartYn=N&hdrYn=Y";
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    function _getCateContNxtW3(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/series-conts-nxt-w3";
        // data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=Y&smartYn=N&hdrYn=Y";
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     * - category info 정보 (시리즈 정보)
     * @param callback
     * @param catId
     * @param saId
     * @param isAsync
     * @param param
     * @private
     */
    function _getCateInfoNxt(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/info-nxt";
        data = "catId=" + catId + "&saId=" + saId + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, null, param);
    }

    function _getCateInfoW3(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/info-w3";
        data = "catId=" + catId + "&saId=" + saId + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, null, param);
    }

    /**
     *
     * @param callback
     * @param catId
     * @param contsId
     * @param saId
     * @param isAsync
     * @param platType W 가 web
     * @param smartYn Y 면 smart app tv N 이면 stb.
     * @param param
     * @private
     */
    function _getContentNxt(callback, catId, contsId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/content/info-nxt";
        data = "catId=" + catId + "&contsId=" + contsId + "&saId=" + saId + "&platType=W&smartYn=N&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, null, param);
    }

    function _getContentW3(callback, catId, contsId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/content/info-w3";
        data = "catId=" + catId + "&contsId=" + contsId + "&saId=" + saId + "&platType=W&smartYn=N&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, null, param);
    }

    function _getItemListNxt(callback, catId, startIndex, itemCnt, saId, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/item-list-nxt";
        data = "catId=" + catId + "&startIndex=" + startIndex + "&itemCnt=" + itemCnt + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     * 회차/메뉴/양방향/시리즈/단편 모두 얻을 수 있음.
     * @param callback
     * @param catId
     * @param startIndex
     * @param itemCnt
     * @param saId
     * @param platType W 가 web
     * @param smartYn Y 면 smart app tv N 이면 stb.
     * @param param
     *
     */
    function _getItemDetlListW3(callback, catId, startIndex, itemCnt, param) {
        // [jh.lee] 화면에 그려야할 내용이 맞는지 비교하기 위한 param(next) 값을 추가
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/item-detail-list-w3";
        data = "catId=" + catId + "&startIndex=" + startIndex + "&itemCnt=" + itemCnt + "&saId=" + DEF.SAID + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, null, param);
    }

    function _getCustEnv(callback, saId) {
        api = "/amoc-api/vod/subscriber/info";
        data = "saId=" + saId + "&platformGubun=W&smartYn=N";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _getCustPkgInfo(callback, saId) {
        api = "/amoc-api/vod/subscriber/pkg-cat-list";
        data = "saId=" + saId + "&platformGubun=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _getCustCugInfo(callback, saId) { // [jh.lee] 미사용 노노. 사용함(CUG).
        api = "/amoc-api/vod/subscriber/cug-list";
        data = "saId=" + saId + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _getProductInfo(callback, pkgCode, param) {
        api = "/amoc-api/product/info";
        data = "saId=" + DEF.SAID + "&pkgCode=" + pkgCode + "&buyTypeYn=Y";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, null, param);
    }

    /**
     *
     * @param adultYn : 성인물 포함 여부 (A/Y/N)
     * @param callback
     * @private
     */
    function _getPlayListNxt(adultYn, callback) {
        api = "/amoc-api/vod/subscriber/play-list-nxt";
        data = "saId=" + DEF.SAID + "&adultYn=" + adultYn + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        data += "&hdrYn=" + (CONSTANT.IS_HDR === true ? "Y" : "N");
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _addPlayListCmb(itemType, itemId, catId, hdYn, callback, param) {
        api = "/amoc-api/vod/subscriber/set-play-list";

        data = "saId=" + DEF.SAID + "&itemCount=1&itemType=" + itemType + "&itemId=" + itemId + "&categoryId=" + catId + "&flag=1" + (hdYn ? ("&hdYn=" + hdYn) : "") + "&platformGubun=W";

        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    function _removePlayListCmb(itemIds, delCount, callback, param) {
        api = "/amoc-api/vod/subscriber/set-play-list";

        data = "saId=" + DEF.SAID + "&itemCount=" + delCount + "&itemId=" + itemIds + "&flag=2" + "&platformGubun=W";

        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     *
     * @param itemCount 추가 삭제 개수 (추가는 1개만 가능)
     * @param itemType 1 시리즈 2 일반 (삭제는 생략)
     * @param itemId "|" 로 구분, 최대 50개 (삭제시)
     * @param catId 삭제할때는 Null
     * @param flag 1: 추가, 2: 삭제
     * @param hdYn 통합편성된 시리즈 카테고리/컨텐츠 찝할때 (Y/N/F/U) 값 을 넣음.
     * @private
     */
    function _setPlayListNxt(itemCount, itemType, itemId, catId, flag, hdYn, callback, param) {
        api = "/amoc-api/vod/subscriber/set-play-list-nxt";
        data = {
            'saId': DEF.SAID,
            'itemCount': itemCount,
            'itemType': itemType,
            'itemId': itemId,
            'categoryId': catId,
            'flag': flag,
            'hdYn': hdYn,
        };
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined, param);
    }

    function _getViewHistoryNxt(postData, callback) {
        api = "/amoc-api/vod/subscriber/view-history-nxt";
        postData += "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        postData += "&hdrYn=" + (CONSTANT.IS_HDR === true ? "Y" : "N");
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, postData, callback);
    }

    /**
     * 가입상품 목록 조회
     */
    function _getCustPkgList(postData, callback, sync) {
        api = "/amoc-api/vod/subscriber/pkg-list";

        postData += "&platformGubun=W&buyTypeYn=Y";

        var reqAsync = true;
        if (sync)
            reqAsync = false;

        return ajax(reqAsync, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, postData, callback);
    }

    function _getBuyHistoryNxt(postData, callback) {
        api = "/amoc-api/vod/subscriber/buy-history-nxt";

        postData += "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        postData += "&hdrYn=" + (CONSTANT.IS_HDR === true ? "Y" : "N");

        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, postData, callback);
    }

    function _getBuyDetInfo(buyId, buyingDate, expireDate, callback) {
        api = "/amoc-api/vod/subscriber/buy-det-info";

        data = {
            'saId': DEF.SAID,
            'buyId': buyId,
            'buyingDate': buyingDate,
            'expireDate': expireDate
        };

        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _setDelBuy(postData, callback) {
        api = "/amoc-api/vod/subscriber/buy-history/delete";

        postData += "&platformGubun=W";

        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, postData, callback);
    }

    /**
     * amocManager.getCornerList(function(arg1,arg2){console.log(arg1);console.log(arg2);},"4100","TT120418048");
     */
    function _getCornerList(callback, catId, saId) {
        api = "/amoc-api/vod/category/corner-list";
        data = "catId=" + catId + "&saId=" + saId + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    /**
     * getCategoryId 는 호출하는 쪽에서 platformGubun넘김
     */
    function _getCategoryId(postData, callback) {
        api = "/amoc-api/vod/category/category-id";

        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, postData, callback);
    }

    /**
     * exchangeItemId는 모바일 공유목록에서 전사시리즈 id를 시리즈 아이디로 바꾸는데 사용된다.
     */

    function _exchangeItemId(callback, exchgId, exchgType, seamYn, isHd, saId) {
        api = "/amoc-api/vod/category/exchange-item-id";
        data = "exchgId=" + exchgId + "&exchgType=" + exchgType + "&seamYn=" + seamYn + "&isHd=" + isHd + "&saId=" + saId + "&platformGubun=W";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    /**
     * 컨텐츠/시리즈 부가 상세 정보
     *  1/null: 긴 크기의 시놉시스
     *  2: 별점정보
     *
     *  1.  long synopsisL.
     *  2.  별점구분|참여자수|별점
     *  3,4 별점구분|평균별점|상세별점명|상세 참여자수|상세별점
     *  5,6 별점구분|평균별점|전문가명|상세별점|코멘트
     *  //평균별점은 어느데이타에 근거한 데이터지 ?.
     */
    function _getDetailContentInfo(content_id, info_type, callback, param) {
        api = "/amoc-api/vod/content/detail-content-info";
        data = "saId=" + DEF.SAID + "&contsId=" + content_id + "&infoType=" + info_type;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    /**
     *
     * @param content_id
     * @param cmb_yn
     * @param category_id
     * @param app_cd
     * @param req_path_cd
     * @param check_type : 'A' 찜여부 'B' 구매+찜여부(월정액, 시리즈 포함)
     * @param cmbContsId : 통합편성된 assetID 조합 (| 결합)
     * @param callback
     * @param param
     * @private
     */
    function _checkMyContentNxt(content_id, cmb_yn, category_id, app_cd, req_path_cd, check_type, cmbContsId, callback, param) {
        api = "/amoc-api/vod/subscriber/check-my-content-nxt";
        data = "saId=" + DEF.SAID + "&contsId=" + content_id + "&cmbYn=" + cmb_yn + "&catId=" + category_id + "&appCd=" + app_cd + "&reqPathCd=" + req_path_cd + "&checkType=" + check_type + "&cmbContsId=" + cmbContsId + "&buyTypeYn=Y";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    function _getChannelInfo(callback, serviceId, param) {
        api = "/amoc-api/channel/channel-info";
        data = "saId=" + DEF.SAID + "&serviceId=" + serviceId + "&buyTypeYn=Y";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    /**
     * 가입상품 목록 조회
     */
    function _getMyPkgList(callback) {
        api = "/amoc-api/vod/subscriber/my-pkg-list";
        data = "saId=" + DEF.SAID + "&platformGubun=W&buyTypeYn=Y";
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _getMyPlayItemList(callback, kidsYn) {
        api = "/amoc-api/vod/subscriber/my-play-item-list";
        data = "saId=" + DEF.SAID + "&kidsYn=" + kidsYn;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, function (res, dat) {
            if (dat.playItemList == null) {
                _setMyPlayItemList(function (res, dat) {
                    if (res && dat.resCode == 0) _getMyPlayItemList(callback, kidsYn);
                    else callback(false, dat);
                }, 4, "", "");
            } else if (kidsYn && kidsYn == "Y" && !containKidsList(dat.playItemList)) {
                _setMyPlayItemList(function (res, dat) {
                    if (res && dat.resCode == 0) _getMyPlayItemList(callback, kidsYn);
                    else callback(false, dat);
                }, 5, "", "");
            } else callback(res, dat);
        });

        function containKidsList(list) {
            if (list == null) list = [];
            if (!_$.isArray(list)) list = [list];
            for (var i in list) {
                if (list[i].isKidsYn == "Y") return true;
            }
            return false;
        }
    }

    function _setMyPlayItemList(callback, cmdFlag, playListId, playListNm) {
        api = "/amoc-api/vod/subscriber/set-my-play-item-list";
        data = "saId=" + DEF.SAID;
        data += "&cmdFlag=" + cmdFlag + "&playListId=" + playListId + "&playListNm=" + encodeURI(playListNm).replace(/%C2%A0/gi, "%20");
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _getMyPlayItemDetList(callback, playListId) {
        api = "/amoc-api/vod/subscriber/my-play-item-det-list";
        data = "saId=" + DEF.SAID + "&playListId=" + playListId;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _setMyPlayItemDetList(callback, cmdFlag, playListId, itemCount, contsId, contsKey, itemType, hdYn) {
        api = "/amoc-api/vod/subscriber/set-my-play-item-det-list";
        data = "saId=" + DEF.SAID + "&cmdFlag=" + cmdFlag + "&playListId=" + playListId + "&itemCount=" + itemCount + "&contsId=" + contsId + "&contsKey=" + contsKey + "&itemType=" + itemType + "&hdYn=" + hdYn;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _sortMyPlayItemDetList(callback, playListId, contsKeyList, pContsKeyList) {
        api = "/amoc-api/vod/subscriber/sort-my-play-item-det-list";
        data = "saId=" + DEF.SAID + "&playListId=" + playListId + "&contsKeyList=" + contsKeyList + "&pContsKeyList=" + pContsKeyList;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    function _getMyCornerListW3(callback) {
        api = "/amoc-api/vod/category/my-corner-list-w3";
        data = "saId=" + DEF.SAID;
        return ajax(true, 0, searchConfig.HTTP.AMOC.HTTP_URL + api, data, callback);
    }

    return {
        ajaxStop: _ajaxStop,
        useMyStarPointVodNxt: _useMyStarPointVodNxt,
        useTvMoneyVodNxt: _useTvMoneyVodNxt,
        useComplexPayNxt: _useComplexPayNxt,
        requestOPC: _requestOPC,
        requestOAC: _requestOAC,
        registPrepaidCpn: _registPrepaidCpn,
        getMyExpireList: _getMyExpireList,
        authorizePVOD: _authorizePVOD,
        authorizeOTNPVOD: _authorizeOTNPVOD,
        authorizeSVOD: _authorizeSVOD,
        authorizeFVOD: _authorizeFVOD,
        setVodEnd: _setVodEnd,
        getLinkTimeNxt: _getLinkTimeNxt,
        getViewHistEP4: _getViewHistEP4,
        setDelWatch: _setDelWatch,
        getContStatusNxt: _getContStatusNxt,
        getCateContNxt: _getCateContNxt,
        getCateContExt: _getCateContExt,
        getCateContNxtW3: _getCateContNxtW3,
        getCateContExtW3: _getCateContExtW3,
        getCateInfoNxt: _getCateInfoNxt,
        getCateInfoW3: _getCateInfoW3,
        getContentNxt: _getContentNxt,
        getContentW3: _getContentW3,
        getItemListNxt: _getItemListNxt, //변경 필요.
        getItemDetlListW3: _getItemDetlListW3,   //적용
        getCustEnv: _getCustEnv,
        getCustPkgInfo: _getCustPkgInfo,
        getCustCugInfo: _getCustCugInfo,
        getProductInfo: _getProductInfo,
        getPlayListNxt: _getPlayListNxt,
        setPlayListNxt: _setPlayListNxt,
        getViewHistoryNxt: _getViewHistoryNxt,
        getCustPkgList: _getCustPkgList,
        getBuyHistoryNxt: _getBuyHistoryNxt,
        getBuyDetInfo: _getBuyDetInfo,
        setDelBuy: _setDelBuy,
        getCornerList: _getCornerList,
        getCategoryId: _getCategoryId,
        exchangeItemId: _exchangeItemId,
        getDetailContentInfo: _getDetailContentInfo,
        checkMyContentNxt: _checkMyContentNxt,
        getItemPerInfo: _getItemPerInfo,
        getChannelInfo: _getChannelInfo,
        getMyPkgList: _getMyPkgList,
        getMyPlayItemList: _getMyPlayItemList,
        setMyPlayItemList: _setMyPlayItemList,
        getMyPlayItemDetList: _getMyPlayItemDetList,
        setMyPlayItemDetList: _setMyPlayItemDetList,
        sortMyPlayItemDetList: _sortMyPlayItemDetList,
        getMyCornerListW3: _getMyCornerListW3
    }
}());
