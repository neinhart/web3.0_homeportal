/**
 * Created by Yun on 2016-12-29.
 */

/**
 * 적용 해야될 시나리오 참고 플립북 783p
 * @type {{searchVOD, searchResult}}
 */

window.SearchImpl = (function() {
    var result = "";
    var tmpWordData = "";

    // req_cd, srchword, srchopt, append
    function _searchVOD(params) {

        if(params.append) {
            tmpWordData += params.srchword + " ";
        } else {
            tmpWordData = params.srchword;
        }
        var resultData;
        SearchManager.searchTOTAL(function(data) {
            SearchManager.setSearchType(3);
            SearchManager.setSearchQuery("");
            resultData = data;
            result = "1";
            var layers = LayerManager.getLayers("SearchResult");
            LayerManager.activateLayer({
                obj: {
                    id: "SearchResult",
                    type: Layer.TYPE.NORMAL,
                    priority: Layer.PRIORITY.NORMAL,
                    params: {
                        search_Data: resultData,
                        search_Words: params.srchword,
                        req_cd: params.req_cd
                    }
                },
                moduleId: "module.search",
                visible: true,
                new: true
            });
            for (var i = 0; i < layers.length; i++) {
                LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
            }
        }, function() {
            result = "0";
        }, true, "0xFF", 3, false, 0, 30, "0x09", tmpWordData, "", "POINT", 2, "Y", params.srchopt+"", "");

        return {result:result};
    }

    // req_cd, srchword, srchopt, srchQry, wordType, contType
    function _searchResult(params) {
        log.printDbg(params);
        var resultData;
        if (params.result != null && params.length > 0) {
            resultData = params.result;
            result = "1";
            var layers = LayerManager.getLayers("SearchResult");
            LayerManager.activateLayer({
                obj: {
                    id: "SearchResult",
                    type: Layer.TYPE.NORMAL,
                    priority: Layer.PRIORITY.NORMAL,
                    params: {
                        search_Data: resultData,
                        search_Words: params.srchword,
                        req_cd: params.req_cd
                    }
                },
                moduleId: "module.search",
                visible: true,
                new: true
            });
            for (var i = 0; i < layers.length; i++) {
                LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
            }
        } else {
            SearchManager.searchTOTAL(function(data) {
                SearchManager.setSearchType(params.wordType);
                SearchManager.setSearchQuery(params.srchQry);
                resultData = data;
                result = "1";
                var layers = LayerManager.getLayers("SearchResult");
                LayerManager.activateLayer({
                    obj: {
                        id: "SearchResult",
                        type: Layer.TYPE.NORMAL,
                        priority: Layer.PRIORITY.NORMAL,
                        params: {
                            search_Data: resultData,
                            search_Words: params.srchword,
                            req_cd: params.req_cd
                        }
                    },
                    moduleId: "module.search",
                    visible: true,
                    new: true
                });
                for (var i = 0; i < layers.length; i++) {
                    LayerManager.deactivateLayer({id: layers[i].id, onlyTarget: true});
                }
            }, function() {
                result = "0";
            }, true, params.contType, params.wordType, (params.contType == "0x02"), 0, 30, "0x09", params.srchword, params.srchQry, (params.contType == "0x02" ? "INSERT_DATE":"POINT"), 2, "Y", params.srchopt+"", "")
        }

        return {result:result};
    }

    return {
        searchVOD: _searchVOD,
        searchResult: _searchResult
    }
}());