/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>FullEpgView</code>
 *
 * - 각 메뉴별로 FullEpgView 의 인스턴스를 생성하나, dom element 는 공유
 * - visibility hidden 으로 컨트롤시, div 가 조금 늦게 hidden 되는 현상 때문에 display 속성으로 제어
 *
 * @author dj.son
 * @since 2016-08-17
 */


(function() {

    var log = window.log;
    var util = window.UTIL;

    var navAdapter = window.oipfAdapter.navAdapter;
    var epgUtil = window.epgUtil;

    var CHANNELS_PER_PAGE = 6;
    var CHANNELS_PER_PREVIEW = 7;
    var IS_TIME_UNIT_SEC = epgUtil.DATA.IS_TIME_UNIT_SEC;

    var EPG_ITEM_HEIGHT = 94;
    var PROGRAMME_AREA_WIDTH = 1325;

    var isFocusEpgArea = true;
    var nowTime = null;
    var updateTimer = null;

    var referenceTime = null;
    var programmeIdx = 0;

    var div = null;
    var epgArea = null;
    var epgTimeBox = null;
    var epgContainer = null;
    var nowDimArea = null;
    var focusArea = null;
    var scrollArea = null;
    var subArea = null;
    var isCreate = false;

    var focusProgrammeInfoArea = null;
    var pipChannelInfoArea = null;
    var relationArea = null;

    var pipTimer = null;

    var menuId;
    var indicator;

    var kidsArea;

    var instance;
    var contextMenu;
    var contextTopMenu = [];
    var contextMidMenu;
    var contextBottomMenu = [];
    var isLoaded = false;

    var previewBottomBg;

    /**
     * [dj.son] 기본 element 생성
     */
    function createElement (_this) {
        log.printDbg("createElement()");
        indicator = new Indicator(6);

        div = util.makeElement({
            tag: "<div />",
            attrs: {
                id : menuId,
                class: "full_epg_view_kids",
                css: {
                    display: "none"
                }
            },
            parent: _this.parent.div
        });



        epgArea = util.makeElement({
            tag: "<div />",
            attrs: {},
            parent: div
        });

        epgTimeBox = util.makeElement({
            // focused 된 편성표 위에 보이는 숫자들.

            tag: "<div />",
            attrs: {
                class: "font_m full_epg_time_area",
                css: {
                    "font-size": 26, color: "white", "letter-spacing": -0.78
                }
            },
            parent: epgArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "full_epg_time_day",
                css: {
                    position: "absolute", left: 391, top: 371, width: 200, height: 30, "text-align": "right",
                    color: "rgba(255, 255, 255, 0.8)"
                }
            },
            parent: epgTimeBox
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 610, top: 372
                }
            },
            parent: epgTimeBox
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1046, top: 372
                }
            },
            parent: epgTimeBox
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1482, top: 372
                }
            },
            parent: epgTimeBox
        });
        epgContainer = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "full_epg_container",
                css: {
                    position: "absolute"
                }
            },
            parent: epgArea
        });
        for (var i = 0; i < CHANNELS_PER_PREVIEW +1; i++) {
            // 아이템 영역 생성
            var item = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "full_epg_item",
                    css: {
                        width: 2300
                    }
                },
                parent: epgContainer
            });

/*
            // 1번 인덱스에 처음에 포커스 가있도록 하는거 같다.  필요없음.
            if (i == 1) {
                item.addClass("focus");
            }
*/

            if (i >= CHANNELS_PER_PAGE) {
                item.addClass("full_epg_item_preview");
            }

            var chInfoArea = util.makeElement({
                // 아이템에 들어가는 채널 영역
                tag: "<div />",
                attrs: {
                    class: "full_epg_channel_area",
                    css: {
                        position: "absolute", "margin-left": 0, "margin-top": 0/*, "-webkit-transition": "width 0.5s"*/
                    }
                },
                parent: item
            });
/*            if (i == 1) {
                //chInfoArea.css({"-webkit-transition": "width 0.5s, height 0.5s"});
            }*/
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "full_epg_channel_icon",
                    css: {
                        position: "absolute", opacity: 0.7
                    }
                },
                parent: chInfoArea
            });
            if (KTW.CONSTANT.IS_OTS) {
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "full_epg_channel_sky_icon",
                        css: {
                            position: "absolute", "margin-left": 5, "margin-top": 35
                            //"-webkit-transition": "opacity 0.5s"
                        }
                    },
                    parent: chInfoArea
                });
            }
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "font_b full_epg_channel_num",
                    css: {
                        position: "absolute"/*, color: "rgba(255, 255, 255, 0.7)"*/
                    }
                },
                parent: chInfoArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "full_epg_channel_name",
                    css: {
                        position: "absolute", color: "rgba(255, 255, 255)", "line-height": 1.1, "text-overflow": "ellipsis",
                        "letter-spacing": -1.5
                    }
                },
                parent: chInfoArea
            });

            var previewArea = util.makeElement({
                // 미리보기 영역 ( 포커스 가기 전에 )
                tag: "<div />",
                attrs: {
                    class: "full_epg_preview_area",
                    css: {
                        position: "absolute", "margin-left": 283, "margin-top": 0, width: 557, height: 127, overflow: "hidden",
                        "background-color": "rgba(38, 79, 117, 0.95)"
                    }
                },
                parent: item
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: window.modulePath + "resource/image/sdw_guide_thumb.png",
                    css: {
                        position: "absolute", width: 13, height: 127, opacity: 0.3
                    }
                },
                parent: previewArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "full_epg_preview_channel_name",
                    css: {
                        position: "absolute", "margin-left": 33, "margin-top": 31, width: 476,
                        "font-size": 30, "letter-spacing": -1.5, "font-family": "RixHead L",
                        color: "rgba(255, 255, 255, 0.8)", "white-space": "nowrap", "text-overflow": "ellipsis", overflow: "hidden"
                    }
                },
                parent: previewArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "full_epg_preview_channel_start_time",
                    css: {
                        position: "absolute", "margin-left": 33, "margin-top": 78,
                        "font-size": 21, "letter-spacing": -1.05,  "font-family": "RixHead L",
                        color: "rgba(255, 255, 255, 0.5)"
                    }
                },
                parent: previewArea
            });
            var previewBar = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", "margin-left": 96, "margin-top": 85, width: 150, height: 4,
                        "background-color": "rgba(255, 255, 255, 0.2)"
                    }
                },
                parent: previewArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        width: 150, height: 4, "background-color": "rgba(255, 255, 255, 0.7)"
                    }
                },
                parent: previewBar
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "full_epg_preview_channel_end_time",
                    css: {
                        position: "absolute", "margin-left": 256, "margin-top": 78,
                        "font-size": 21, "letter-spacing": -1.05,  "font-family": "RixHead L",
                        color: "rgba(255, 255, 255, 0.5)"
                    }
                },
                parent: previewArea
            });

            var progArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "full_epg_programme_area",
                    css: {
                        position: "absolute", "margin-top": 0, overflow: "visible"
                        //"-webkit-transition": "margin-left 0.5s, opacity 0.5s"
                    }
                },
                parent: item
            });
            // 포커스 된 영역을 그리는 부분인데, 이제 바뀌니까 필요 없어
           /* if (i === 1) {
                var focusProgramme = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "full_epg_focus_programme",
                        css: {
                            width: 1310
                        }
                    },
                    parent: progArea
                });

                var programmeTitleBox = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "full_epg_focus_programme_title_box",
                        css: {
                            overflow: "visible"
                        }
                    },
                    parent: focusProgramme
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        name: "full_epg_focus_programme_icon_book",
                        src: window.modulePath + "resource/icon_book.png",
                        css: {
                            position: "absolute", "margin-left": 29, "margin-top": 32, visibility: "hidden"
                        }
                    },
                    parent: programmeTitleBox
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        name: "full_epg_focus_programme_name",
                        class: "font_l",
                        css: {
                            position: "absolute", "margin-left": 29, "margin-top": 32, height: 45, "font-size": 35, color: "rgba(255, 255, 255, 1)",
                            "letter-spacing": -1.75, "white-space": "nowrap"
                        }
                    },
                    parent: programmeTitleBox
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        name: "full_epg_focus_programme_age_icon",
                        css: {
                            position: "absolute", "margin-top": 32
                        }
                    },
                    parent: programmeTitleBox
                });
                var progressArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "full_epg_focus_programme_progress_area"
                    },
                    parent: focusProgramme
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        name: "full_epg_focus_programme_start_time",
                        css: {
                            position: "absolute", "margin-left": 30, "margin-top": 88,
                            "font-size": 23, color: "rgba(255, 255, 255, 0.5)"
                        }
                    },
                    parent: progressArea
                });
                var progressBar = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "full_epg_focus_programme_progress_bar",
                        css: {
                            position: "absolute", "margin-left": 103, "margin-top": 95, width: 300, height: 5,
                            "background-color": "rgba(255, 255, 255, 0.2)"
                        }
                    },
                    parent: progressArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            width: 150, height: 5, "background-color": "rgba(255, 255, 255, 1)"
                        }
                    },
                    parent: progressBar
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        name: "full_epg_focus_programme_end_time",
                        css: {
                            position: "absolute", "margin-left": 417, "margin-top": 88,
                            "font-size": 23, color: "rgba(255, 255, 255, 0.5)"
                        }
                    },
                    parent: progressArea
                });
                var tagArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "full_epg_focus_programme_tag_area",
                        css: {
                            position: "absolute", "margin-left": 500, "margin-top": 82, overflow: "visible",
                            visibility: "hidden"
                        }
                    },
                    parent: focusProgramme
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        name: "full_epg_focus_programme_tag_subtitles",
                        src: window.modulePath + "resource/image/icon_tag_subtitles.png",
                        css: {
                            position: "absolute", visibility: "hidden"
                        }
                    },
                    parent: tagArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        name: "full_epg_focus_programme_tag_comment",
                        src: window.modulePath + "resource/image/icon_tag_comment.png",
                        css: {
                            position: "absolute", visibility: "hidden"
                        }
                    },
                    parent: tagArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        name: "full_epg_focus_programme_tag_sign",
                        src: window.modulePath + "resource/image/icon_tag_sign.png",
                        css: {
                            position: "absolute", visibility: "hidden"
                        }
                    },
                    parent: tagArea
                });
            }*/

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "full_epg_programme_list"
                },
                parent: progArea
            });
        }
        nowDimArea = util.makeElement({
            // 현재 시간 흘러가는 UI
            tag: "<div />",
            attrs: {
                class: "nowDimArea",
                css: {
                    position: "absolute", left: 611, top: 402, overflow: "visible", visibility: "hidden"
                }
            },
            parent: epgArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_now_dim_1",
                css: {
                    position: "absolute", left: 0, top: 0, height: 92,
                    "background-color": "#1E4469"
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_now_line_1",
                src: window.modulePath + "resource/image/timeline_guide_kids.png",
                css: {
                    position: "absolute", top: 0, width: 58, height: 92, "z-index" : 9
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_now_dim_focus",
                css: {
                    position: "absolute", left: 0, top: 93, height: 92,
                    "background-color": "#1E4469"
                }
            },
            parent: nowDimArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_now_line_focus",
                src: window.modulePath + "resource/image/timeline_guide_kids.png",
                css: {
                    position: "absolute", top: 93, width: 58, height: 92 ,"z-index" : 9
                }
            },
            parent: nowDimArea
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_now_dim_2",
                css: {
                    position: "absolute", left: 0, top: 188, height: 92,
                    "background-color": "#1E4469"
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_now_dim_3",
                css: {
                    position: "absolute", left: 0, top: 282, height: 92,
                    "background-color": "#1E4469"
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_now_dim_4",
                css: {
                    position: "absolute", left: 0, top: 376, height: 92,
                    "background-color": "#1E4469"
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_now_dim_5",
                css: {
                    position: "absolute", left: 0, top: 470, height: 92,
                    "background-color": "#1E4469"
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_now_line_2",
                src: window.modulePath + "resource/image/timeline_guide_kids.png",
                css: {
                    position: "absolute", top: 188, width: 58, height: 374 ,"z-index" : 9
                }
            },
            parent: nowDimArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_now_icon",
                src: window.modulePath + "resource/image/guide_now_kids.png",
                css: {
                    position: "absolute", left: -38, top: -46
                }
            },
            parent: nowDimArea
        });
/*        focusArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "full_epg_focus_area",
                css: {
                    position: "absolute", left: 174, top: 349, overflow: "visible"
                }
            },
            parent: epgArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 0, width: 1746, height: 5
                }
            },
            parent: focusArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 243, width: 1746, height: 5
                }
            },
            parent: focusArea
        });*/

        focusProgrammeInfoArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "full_epg_focus_programme_info_area",
                css: {
                    //display: "none"
                }
            },
            parent: epgArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_channel_num_0",
                src: "images/epg_number/epg_number_0.png",
                css: {
                    position: "absolute", left: 176, top: 183, overflow: "visible"
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_channel_num_1",
                src: "images/epg_number/epg_number_1.png",
                css: {
                    position: "absolute", left: 225, top: 183, overflow: "visible"
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_channel_num_2",
                src: "images/epg_number/epg_number_1.png",
                css: {
                    position: "absolute", left: 274, top: 183, overflow: "visible"
                }
            },
            parent: focusProgrammeInfoArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_programme_book",
                css: {
                    position: "absolute", left: 341, top: 180
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "full_epg_focus_programme_name_0",
                class: "font_l",
                css: {
                    position: "absolute", left: 341, top: 180, height: 45, "font-size": 35, color: "rgba(255, 255, 255, 1)",
                    "letter-spacing": -1.75, "white-space": "nowrap"
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "full_epg_focus_programme_name_1",
                class: "font_l",
                css: {
                    position: "absolute", left: 341, top: 222, height: 45, "font-size": 35, color: "rgba(255, 255, 255, 1)",
                    "letter-spacing": -1.75, "white-space": "nowrap"
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_programme_age_icon",
                css: {
                    position: "absolute", left: 341, top: 180
                }
            },
            parent: focusProgrammeInfoArea
        });

        var progressArea = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_focus_programme_progress_area",
                css: {
                    position: "absolute", left: 177, top: 291, width: 700, height: 50
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "full_epg_focus_programme_start_time",
                css: {
                    position: "absolute", "letter-spacing": -1.15,
                    "font-size": 23, color: "rgba(255, 255, 255, 0.5)"
                }
            },
            parent: progressArea
        });
        var progressBar = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_focus_programme_progress_bar",
                css: {
                    position: "absolute", "margin-left": 73, "margin-top": 7, width: 500, height: 5,
                    "background-color": "rgba(255, 255, 255, 0.2)"
                }
            },
            parent: progressArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    width: 150, height: 5, "background-color": "rgba(255, 255, 255, 1)"
                }
            },
            parent: progressBar
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "full_epg_focus_programme_end_time",
                css: {
                    position: "absolute", "margin-left": 587, /*"margin-top": 88, */"letter-spacing": -1.15,
                    "font-size": 23, color: "rgba(255, 255, 255, 0.5)"
                }
            },
            parent: progressArea
        });
        var tagArea = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_focus_programme_tag_area",
                css: {
                    position: "absolute", "margin-left": 836, "margin-top": 284, overflow: "visible",
                    visibility: "hidden"
                }
            },
            parent: focusProgrammeInfoArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_programme_tag_subtitles",
                src: window.modulePath + "resource/image/icon_tag_subtitles.png",
                css: {
                    position: "absolute", visibility: "hidden"
                }
            },
            parent: tagArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_programme_tag_comment",
                src: window.modulePath + "resource/image/icon_tag_comment.png",
                css: {
                    position: "absolute", visibility: "hidden"
                }
            },
            parent: tagArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "full_epg_focus_programme_tag_sign",
                src: window.modulePath + "resource/image/icon_tag_sign.png",
                css: {
                    position: "absolute", visibility: "hidden"
                }
            },
            parent: tagArea
        });

        pipChannelInfoArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "full_epg_pip_channel_info_area",
                css: {
                    position: "absolute", left: 1360, top: 303, width: 560, height: 52, "background-color": "rgba(27, 55, 82, 0.8)"
                }
            },
            parent: epgArea
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "full_epg_pip_channel_num",
                css: {
                    position: "absolute", left: 29, top: 14, "letter-spacing": -0.84, "font-family": "RixHead M",
                    "font-size": 28, color: "rgba(255, 255, 255, 0.8)"
                }
            },
            parent: pipChannelInfoArea
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_pip_channel_name",
                css: {
                    position: "absolute", left: 95, top: 14, "letter-spacing": -0.84, "font-family": "RixHead M",
                    "font-size": 28, color: "rgba(255, 255, 255, 1)"
                }
            },
            parent: pipChannelInfoArea
        });


        scrollArea = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 99, top: 304, overflow: "visible", visibility: "hidden"
                }
            },
            parent: div
        });

        subArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "full_epg_sub_area"
            },
            parent: div
        });

        div.append(indicator.getView());

        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_m",
                name: "full_epg_title",
                css: {
                    position: "absolute", left: 172, top: 84, width:412, "font-size": 54, color: "rgba(255, 255, 255, 0.6)",
                    "letter-spacing": -2.7, "font-family": "RixHead M"
                }
            },
            parent: subArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "full_epg_today",
                css: {
                    position: "absolute", left: 905, top: 108, width: 159, "font-size": 26, color: "rgba(255, 255, 255, 0.6)",
                    "letter-spacing": -1.3
                }
            },
            parent: subArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: window.modulePath + "resource/image/icon_current_time.png",
                css: {
                    position: "absolute", left: 1088, top: 108
                }
            },
            parent: subArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1124, top: 108, width: 94, "font-size": 26, color: "rgba(255, 255, 255, 0.4)",
                    "letter-spacing": -1.3
                }
            },
            text: "현재 시간",
            parent: subArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_m full_epg_now_time",
                css: {
                    position: "absolute", left: 1226, top: 109, "font-size": 29, color: "rgba(255, 255, 255, 0.6)",
                    "letter-spacing": -1.45
                }
            },
            parent: subArea
        });

        relationArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "sub_area",
                css:{
                    visibility: "hidden"
                }
            },
            parent: div
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: window.modulePath + "resource/image/icon_option_related.png",
                css: {
                    position: "absolute", left: 1789, top: 1001
                }
            },
            parent: relationArea
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1823, top: 1000, width: 150, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                    "letter-spacing": -1.2
                }
            },
            text: "옵션",
            parent: relationArea
        });


        var changePageGuideArea = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "full_epg_change_page_guide_area"
            },
            parent: subArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pageup.png",
                css: {
                    position: "absolute", left: 180, top: 998
                }
            },
            parent: changePageGuideArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pagedown.png",
                css: {
                    position: "absolute", left: 208, top: 998
                }
            },
            parent: changePageGuideArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 242, top: 1001, width: 150,"font-size": 24, color: "rgba(255, 255, 255, 0.3)",
                    "letter-spacing": -1.2
                }
            },
            text: "페이지",
            parent: changePageGuideArea
        });



        // blur 이미지 추가
        previewBottomBg  = util.makeElement({
            tag: "<img />",
            attrs: {
                class: "dim_vodList_btm_kids",
                src: window.modulePath + "resource/image/dim_vodlist_btm_kids.png",
                css: {
                    position: "absolute", left: 577, top: 912, width: 1343, height: 168
                }
            },
            parent: div
        });


        settingContextMenuData();
        // div.append(contextMenu.getView());
    }

    function settingContextMenuData() {
        var guideMenu = window.MenuDataManager.searchMenu({
            menuData: window.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id == window.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                    return true;
                }
            }
        })[0];
        contextMenu.addTitle("채널 편성표", "epgTop_title_0");
        var tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id, "epgTop_title_0");
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.SKYLIFE_UHD_CHANNEL);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.MOVIE_CHOICE);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.AUDIO_CHANNEL);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }

        contextMenu.addSeparateLine("epgTop");

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.PROGRAM_SEARCH);
        if (tmpObj) {
            contextMidMenu = tmpObj;
            contextMenu.addTitle("장르별 프로그램", "epgTop_title_1");
            for (var idx = 0 ; idx < contextMidMenu.children.length ; idx ++) {
                contextMenu.addButton(contextMidMenu.children[idx].name, contextMidMenu.children[idx].id, "epgTop_title_1");
            }
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL);
        if (tmpObj) {
            contextMenu.addSeparateLine("epgTop");
            contextMenu.addTitle("TV APP 채널", "epg_title_2");
            contextBottomMenu[contextBottomMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id, "epg_title_2");
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.TV_APP_CHANNEL);
        if (tmpObj) {
            if (contextBottomMenu.length == 0) {
                contextMenu.addSeparateLine("epgTop");
                contextMenu.addTitle("TV APP 채널", "epg_title_2");
            }
            contextBottomMenu[contextBottomMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }
    }

    function getSearchMenuData(menuData, menuId) {
        var menuObj = null;
        if (menuData) {
            for (var idx = 0 ; idx < menuData.children.length ; idx ++) {
                if (menuData.children[idx].id === menuId) {
                    menuObj = menuData.children[idx];
                    break;
                }
            }
        }
        return menuObj;
    }

    /**
     * [dj.son] draw - 타이틀 영역 날짜 및 시간
     */
    function drawSubArea (_this) {
        log.printDbg("drawSubArea()");

        var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === _this.menuId) {
                    return true;
                }
            }
        })[0];

        // title setting
        var title = epgMenu.name;
        if (_this.parent.language === "eng") {
            title = epgMenu.englishItemName;
        }
        subArea.find("[name=full_epg_title]").text(title);

        var today = new Date();

        var strToday = util.numToStr(today.getMonth() + 1, 2) + "월 " + util.numToStr(today.getDate(), 2) + "일 (" + util.transDay(today.getDay(), "kor") + ")";
        subArea.find(".full_epg_today").text(strToday);

        var strTime = util.numToStr(today.getHours(), 2) + ":" + util.numToStr(today.getMinutes(), 2);
        subArea.find(".full_epg_now_time").text(strTime);
    }

    /**
     * [dj.son] epg area 의 기준 시간 세팅
     */
    function drawReferenceTime () {
        log.printDbg("drawReferenceTime()");

        var arrTimeElement = epgTimeBox.children();

        var nowDate = (new Date()).getDate();
        var referenceDate = referenceTime.getDate();
        if (nowDate === referenceDate) {
            _$(arrTimeElement[0]).text("오늘");
        }
        else {
            if (nowDate < referenceDate) {}
            else {
                referenceDate = nowDate + referenceDate;
            }

            if (referenceDate - nowDate === 1) {
                _$(arrTimeElement[0]).text("내일");
            }
            else {
                _$(arrTimeElement[0]).text(util.getFormatDate(referenceTime, "E"));
            }
        }


        _$(arrTimeElement[1]).text(util.numToStr(referenceTime.getHours(), 2) + ":" + util.numToStr(referenceTime.getMinutes(), 2));

        var nextTime = epgUtil.reviseDate(new Date(referenceTime.getTime() + (30 * 60 * 1000)));
        _$(arrTimeElement[2]).text(util.numToStr(nextTime.getHours(), 2) + ":" + util.numToStr(nextTime.getMinutes(), 2));

        var secondNextTime = epgUtil.reviseDate(new Date(nextTime.getTime() + (30 * 60 * 1000)));
        _$(arrTimeElement[3]).text(util.numToStr(secondNextTime.getHours(), 2) + ":" + util.numToStr(secondNextTime.getMinutes(), 2));
    }

    /**
     * [dj.son] draw - Channel
     */
    function drawChannel (_this) {
        log.printDbg("drawChannel()");

        var curChannelList = getCurChannelList(_this);
        var channel = null;

        var arrEpgElement = epgContainer.children();
        var epgElement = null;

        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            epgElement = _$(arrEpgElement[i]);
            channel = curChannelList[i];

            if (channel) {
                var icon = "";
                if (KTW.CONSTANT.IS_OTS && _this.menuId === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                    if (channel.favIDs.indexOf(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE) > -1) {
                        icon = window.modulePath + "resource/image/icon_sky_epg.png";
                    }
                    else if (channel.favIDs.indexOf(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE) > -1) {
                        icon = window.modulePath + "resource/image/icon_olleh_epg.png";
                    }
                    epgElement.find(".full_epg_channel_icon").attr({src: ""});
                    epgElement.find(".full_epg_channel_sky_icon").attr({src: icon});
                }
                else {
                    if (navAdapter.isBlockedChannel(channel)) {
                        icon = window.modulePath + "resource/image/icon_block.png";
                    }
                    else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channel)) {
                        icon = window.modulePath + "resource/icon_fav.png";
                    }
                    epgElement.find(".full_epg_channel_icon").attr({src: icon});
                    epgElement.find(".full_epg_channel_sky_icon").attr({src: ""});
                }

                epgElement.find(".full_epg_channel_num").text(util.numToStr(channel.data.majorChannel, 3));
            }
            else {
                epgElement.find(".full_epg_channel_icon").attr({src: ""});
                epgElement.find(".full_epg_channel_sky_icon").attr({src: ""});
                epgElement.find(".full_epg_channel_num").text("");
            }
        }

        drawChannelName(_this);
    }

    function drawChannelName (_this) {
        log.printDbg("drawChannelName()");

        var curChannelList = getCurChannelList(_this);
        var channel = null;

        var arrEpgElement = epgContainer.children();
        var epgElement = null;

        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            epgElement = _$(arrEpgElement[i]);
            channel = curChannelList[i];

            if (channel) {
                var name = channel.data.name;
/*
                var isFocus = div.hasClass("focus");
                var width = isFocus ? 270 : 200;
                var fontSize = isFocus ? 30 : 27;
                var letterSpacing = isFocus ? -1.5 : -1.35;

                var arrChName = util.splitText(name, "RixHead L", fontSize, width, letterSpacing);
                if (arrChName.length > 1) {
                    name = "";
                    for (var j = 0; j < arrChName.length; j++) {
                        name += arrChName[j] + "\n";
                    }
                    name = name.slice(0, name.length - 1);

                    epgElement.find(".full_epg_channel_name").addClass("line_2");
                }
                else {
                    epgElement.find(".full_epg_channel_name").removeClass("line_2");
                }

                */
                epgElement.find(".full_epg_channel_name").text(name);
            }
            else {
                epgElement.find(".full_epg_channel_name").text("");
            }
        }
    }

    /**
     * [dj.son] draw - Preview
     */
    function drawPreview (_this) {
        log.printDbg("drawPreview()");

        var curDate = new Date();

        // 첫번째 프로그램 가져오기
        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getPreviewProgrammesList()
        });
        var programme = null;

        var arrEpgElement = epgContainer.children();
        var previewElement = null;

        var progStartTime = null;
        var progDuration = null;

        // draw
        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            previewElement = _$(arrEpgElement[i]).find(".full_epg_preview_area");

            programme = programmeList[i];

            if (programme) {
                progStartTime = programme.startTime * 1000;
                progDuration = programme.duration * 1000;

                previewElement.find(".full_epg_preview_channel_name").text(programme.name);

                if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
                    || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
                    || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {

                    previewElement.find(".full_epg_preview_channel_name").css({"margin-top": 50});

                    previewElement.find(".full_epg_preview_channel_start_time").text("");
                    previewElement.find(".full_epg_preview_channel_end_time").text("");
                    previewElement.find("div").css({width: 0});
                }
                else {
                    previewElement.find(".full_epg_preview_channel_name").css({"margin-top": 31});

                    previewElement.find(".full_epg_preview_channel_start_time").text(util.getTimeString(progStartTime));
                    previewElement.find(".full_epg_preview_channel_end_time").text(util.getTimeString(progStartTime + progDuration));

                    previewElement.find("div").css({width: 150});
                    var width = Math.floor(((curDate.getTime() - progStartTime) / progDuration) * 150);
                    previewElement.find("div div").css({width: width});
                }
            }
            else {
                previewElement.find(".full_epg_preview_channel_name").text("");
                previewElement.find(".full_epg_preview_channel_start_time").text("");
                previewElement.find(".full_epg_preview_channel_end_time").text("");
                previewElement.find("div").css({width: 0});
            }
        }
    }

    /**
     * [dj.son] draw - Programmes (programmeList 데이터 기반 draw)
     */
    function drawProgrammes (_this) {
       log.printDbg("drawProgrammes()");

        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });

        var widthPerMin = PROGRAMME_AREA_WIDTH / 90;

        var arrEpgElement = epgContainer.children();
        var programmeAreaElement = null;

        var progList = null;
        var progLength = null;
        var progElement = null;
        var progWidth = null;
        var progTotalWidth = null;

        var startTime = referenceTime.getTime();
        var endTime = epgUtil.reviseDate(new Date(startTime + (90 * 60 * 1000))).getTime();
        if (IS_TIME_UNIT_SEC) {
            startTime = Math.floor(startTime / 1000);
            endTime = Math.floor(endTime / 1000);
        }
        var programme = null;
        var progStartTime = null;
        var progDuration = null;
        var progEndTime = null;

        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            programmeAreaElement = _$(arrEpgElement[i]).find(".full_epg_programme_list");
            programmeAreaElement.children().remove();

            progList = programmeList[i];

            if (progList) {
                progLength = progList.length;
                progTotalWidth = 0;

                for (var j = 0; j < progLength; j++) {
                    programme = progList[j];
                    progStartTime = programme.startTime;
                    progEndTime = progStartTime + programme.duration;

                    // 1. 첫번째 programme 은 epgTime 부터 programme 끝나는 시간까지 width 잡음
                    // 2. 중간 programme 부터는 duration 으로 width 잡음
                    // 3. 마지막 programme 은 start time 부터 epgTime + 90 까지 width 잡음

                    if (j == 0) {
                        progStartTime = startTime;
                    }

                    if (j == progLength - 1) {
                        progEndTime = endTime;
                    }

                    progDuration = progEndTime - progStartTime;

                    progWidth = Math.ceil(widthPerMin * Math.floor(progDuration / 60));

                    progTotalWidth += progWidth;

                    if (j == progLength - 1 && progTotalWidth != PROGRAMME_AREA_WIDTH) {
                        progWidth = progWidth + (PROGRAMME_AREA_WIDTH - progTotalWidth);
                    }

                    var cssObj = {
                        height: EPG_ITEM_HEIGHT - 2, float: "left"/*, "background-color": "rgb(27, 55, 82)"*/
                    };

                    if (j === progLength - 1 ) {
                        cssObj.width = progWidth;
                    }
                    else {
                        cssObj.width = progWidth - 2;
                        cssObj["margin-right"] = 2;
                    }

                    progElement = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "full_epg_programme",
                            css: cssObj
                        },
                        parent: programmeAreaElement
                    });

                    util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "full_epg_programme_bg",
                            css: {
                                position: "absolute", width: cssObj.width, height: cssObj.height, "box-sizing": "border-box"
                            }
                        },
                        parent: progElement
                    });


                    if (j === progLength - 1 ) {
                        var tempdiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                class: "full_epg_focus_programme_bg focus_red_border_without_right_box",
                                css: {
                                    position: "absolute", width: cssObj.width, height: cssObj.height, "z-index": 9, "border" : "6px solid rgb(255,179,46)"
                                }
                            },
                            parent: progElement
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                class: "focus_black_border_without_right_box",
                                css: {
                                    position: "absolute", left : 0 , top : 0 , width: (cssObj.width-12), height: (cssObj.height-12)
                                }
                            },
                            parent: tempdiv
                        });
                    }else {
                        var tempdiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                class: "full_epg_focus_programme_bg focus_red_border_box",
                                css: {
                                    position: "absolute", width: cssObj.width, height: cssObj.height, "z-index": 9, "border" : "6px solid rgb(255,179,46)"
                                }
                            },
                            parent: progElement
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                class: "focus_black_border_box",
                                css: {
                                    position: "absolute", left : 0 , top : 0 , width: (cssObj.width-12), height: (cssObj.height-12)
                                }
                            },
                            parent: tempdiv
                        });

                    }

                    if (progWidth > 50) {
                        var nameCssObj = {
                            position: "absolute", "margin-left": 28, "margin-top": 32, width: (progWidth - 56), height: 40,
                            "white-space": "nowrap", "text-overflow": "ellipsis", "letter-spacing": -1.5, overflow: "hidden", "z-index" : 9
                        };
                        if (progWidth < 84) {
                            // text ... 처리
                            nameCssObj["margin-left"] = 10;
                            nameCssObj["width"] = 50;
                            util.makeElement({
                                tag: "<span />",
                                attrs: {
                                    css: nameCssObj
                                },
                                text: "...",
                                parent: progElement
                            });
                        }
                        else {
                            var reserveResult = KTW.managers.service.ReservationManager.isReserved(progList[j]);
                            var iconBook = "";
                            if (reserveResult !== KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                                if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SHORT) {
                                    iconBook = "images/icon/icon_book.png";
                                }
                                else if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
                                    iconBook = "images/icon/icon_book_s.png";
                                }
                                if (progWidth > 110) {
                                    util.makeElement({
                                        tag: "<img />",
                                        attrs: {
                                            src: iconBook,
                                            css: {
                                                position: "absolute", "margin-left": 30, "margin-top": 30
                                            }
                                        },
                                        parent: progElement
                                    });
                                    nameCssObj["margin-left"] = 70;
                                    nameCssObj["width"] = progWidth - 56 - 40;
                                }

                                util.makeElement({
                                    tag: "<span />",
                                    attrs: {
                                        css: nameCssObj
                                    },
                                    text: progList[j].name,
                                    parent: progElement
                                });
                            }
                            else {
                                util.makeElement({
                                    tag: "<span />",
                                    attrs: {
                                        css: nameCssObj
                                    },
                                    text: progList[j].name,
                                    parent: progElement
                                });
                            }
                        }
                    }

                    var focusBarWidth = progWidth - 2;
                    if (j === progLength - 1) {
                        focusBarWidth = progWidth;
                    }
                }
            }
            else {
                progElement = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "full_epg_programme",
                        css: {
                            height: EPG_ITEM_HEIGHT - 2, width: PROGRAMME_AREA_WIDTH, float: "left"/*, "background-color": "rgba(79, 79, 76, 0.5)"*/
                        }
                    },
                    parent: programmeAreaElement
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "full_epg_programme_bg",
                        css: {
                            position: "absolute", width: PROGRAMME_AREA_WIDTH, height: EPG_ITEM_HEIGHT - 2, "box-sizing": "border-box"
                        }
                    },
                    parent: progElement
                });
            }
        }
    };
  /*  function drawProgrammes (_this) {
        log.printDbg("drawProgrammes()");

        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });

        var widthPerMin = PROGRAMME_AREA_WIDTH / 90;

        var arrEpgElement = epgContainer.children();
        var programmeAreaElement = null;

        var progList = null;
        var progLength = null;
        var progElement = null;
        var progWidth = null;
        var progTotalWidth = null;

        var startTime = referenceTime.getTime();
        var endTime = epgUtil.reviseDate(new Date(startTime + (90 * 60 * 1000))).getTime();
        if (IS_TIME_UNIT_SEC) {
            startTime = Math.floor(startTime / 1000);
            endTime = Math.floor(endTime / 1000);
        }
        var programme = null;
        var progStartTime = null;
        var progDuration = null;
        var progEndTime = null;

        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            programmeAreaElement = _$(arrEpgElement[i]).find(".full_epg_programme_list");
            programmeAreaElement.children().remove();

            progList = programmeList[i];

            if (progList) {
                progLength = progList.length;
                progTotalWidth = 0;

                for (var j = 0; j < progLength; j++) {
                    programme = progList[j];
                    progStartTime = programme.startTime;
                    progEndTime = progStartTime + programme.duration;

                    // 1. 첫번째 programme 은 epgTime 부터 programme 끝나는 시간까지 width 잡음
                    // 2. 중간 programme 부터는 duration 으로 width 잡음
                    // 3. 마지막 programme 은 start time 부터 epgTime + 90 까지 width 잡음

                    if (j == 0) {
                        progStartTime = startTime;
                    }

                    if (j == progLength - 1) {
                        progEndTime = endTime;
                    }

                    progDuration = progEndTime - progStartTime;

                    progWidth = Math.ceil(widthPerMin * Math.floor(progDuration / 60));

                    progTotalWidth += progWidth;

                    if (j == progLength - 1 && progTotalWidth != PROGRAMME_AREA_WIDTH) {
                        progWidth = progWidth + (PROGRAMME_AREA_WIDTH - progTotalWidth);
                    }

                    var cssObj = {
                        height: 92, float: "left"
                    };

                    if (j === progLength - 1 ) {
                        cssObj.width = progWidth;
                    }
                    else {
                        cssObj.width = progWidth - 2;
                        cssObj["margin-right"] = 2;
                    }

                    if (i === 0) {
                        cssObj["background-color"] = "rgba(38, 79, 117, 0.85)";
                    }
                    else if (i > 1) {
                        cssObj["background-color"] = "rgba(38, 79, 117, 0.85)";
                    }

                    progElement = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "full_epg_programme",
                            css: cssObj
                        },
                        parent: programmeAreaElement
                    });

                    if (progWidth > 50) {
                        var nameCssObj = {
                            position: "absolute", "margin-left": 28, "margin-top": 31, width: (progWidth - 56), height: 40,
                            "white-space": "nowrap", "text-overflow": "ellipsis", "letter-spacing": -1.5, overflow: "hidden", "z-index": 9
                        };
                        if (progWidth < 84) {
                            // text ... 처리
                            nameCssObj["margin-left"] = 10;
                            nameCssObj["width"] = 50;
                            util.makeElement({
                                tag: "<span />",
                                attrs: {
                                    css: nameCssObj
                                },
                                text: "...",
                                parent: progElement
                            });
                        }
                        else {
                            var reserveResult = KTW.managers.service.ReservationManager.isReserved(progList[j]);
                            var iconBook = "";
                            if (reserveResult !== KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                                if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SHORT) {
                                    iconBook = window.modulePath + "resource/icon_book.png";
                                }
                                else if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
                                    iconBook = window.modulePath + "resource/icon_book_s.png";
                                }
                                if (progWidth > 110) {
                                    util.makeElement({
                                        tag: "<img />",
                                        attrs: {
                                            src: iconBook,
                                            css: {
                                                position: "absolute", "margin-left": 30, "margin-top": 31
                                            }
                                        },
                                        parent: progElement
                                    });
                                    nameCssObj["margin-left"] = 70;
                                    nameCssObj["width"] = progWidth - 56 - 40;
                                }

                                util.makeElement({
                                    tag: "<span />",
                                    attrs: {
                                        css: nameCssObj
                                    },
                                    text: progList[j].name,
                                    parent: progElement
                                });
                            }
                            else {
                                util.makeElement({
                                    tag: "<span />",
                                    attrs: {
                                        css: nameCssObj
                                    },
                                    text: progList[j].name,
                                    parent: progElement
                                });
                            }
                        }
                    }
                }
            }
            else {
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "full_epg_programme",
                        css: {
                            height: 92, width: PROGRAMME_AREA_WIDTH, float: "left", "background-color": "rgba(79, 79, 76, 0.5)"
                        }
                    },
                    parent: programmeAreaElement
                });
            }
        }
    }
*/

    /**
     * [dj.son] draw - now dim
     */
    function drawNowDim () {
        log.printDbg("drawNowDim()");

        if (div.hasClass("focus")) {
            var endTime = new Date(referenceTime.getTime() + 90 * 60 * 1000);

            if (referenceTime <= nowTime && endTime > nowTime) {
                var widthPerMin = PROGRAMME_AREA_WIDTH / 90;
                var min = Math.floor((nowTime - referenceTime) / 60 / 1000);

                var dim_1 = nowDimArea.find("div[name=full_epg_now_dim_1]");
                var dim_2 = nowDimArea.find("div[name=full_epg_now_dim_2]");
                var dim_3 = nowDimArea.find("div[name=full_epg_now_dim_3]");
                var dim_4 = nowDimArea.find("div[name=full_epg_now_dim_4]");
                var dim_5 = nowDimArea.find("div[name=full_epg_now_dim_5]");
                var dim_focus = nowDimArea.find("div[name=full_epg_now_dim_focus]");


                dim_1.css({
                    left: -1 * (min * widthPerMin), width: min * widthPerMin
                });

                dim_2.css({
                    left: -1 * (min * widthPerMin), width: min * widthPerMin
                });

                dim_3.css({
                    left: -1 * (min * widthPerMin), width: min * widthPerMin
                });

                dim_4.css({
                    left: -1 * (min * widthPerMin), width: min * widthPerMin
                });

                dim_5.css({
                    left: -1 * (min * widthPerMin), width: min * widthPerMin
                });

                dim_focus.css({
                    left: -1 * (min * widthPerMin), width: min * widthPerMin
                });

                nowDimArea.css({
                    left: 592 + (min * widthPerMin),
                    visibility: "inherit"
                });
            }
            else {
                nowDimArea.css({visibility: "hidden"});
            }
        }
        else {
            nowDimArea.css({visibility: "hidden"});
        }

        // 이건 뭘까 ?
/*        if (!isFocusEpgArea) {
            nowDimArea.find("div[name=full_epg_now_dim_focus]").css({visibility: "inherit"});
            nowDimArea.find("img[name=full_epg_now_line_focus]").css({visibility: "inherit"});
        } else {
            nowDimArea.find("div[name=full_epg_now_dim_focus]").css({visibility: "hidden"});
            nowDimArea.find("img[name=full_epg_now_line_focus]").css({visibility: "hidden"});
        }*/
    }

    /**
     * [dj.son] draw - focus programme
     */
    function drawFocusProgramme (_this) {
        log.printDbg("drawFocusProgramme()");

        var curChannelList = getCurChannelList(_this);
        var focusChannelIdx = getFocusChannelIdx(_this);
        var curChannel = curChannelList[focusChannelIdx];
        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[focusChannelIdx];
        var programme = progList[programmeIdx];
        var progStartTime = programme.startTime * 1000;
        var progDuration = programme.duration * 1000;
        var nowTime = (new Date()).getTime();

        // 채널 번호
        var channelNum_0 = Math.floor(curChannel.data.majorChannel / 100);
        var channelNum_1 = Math.floor((curChannel.data.majorChannel - (channelNum_0 * 100)) / 10);
        var channelNum_2 = (curChannel.data.majorChannel - (channelNum_0 * 100) - (channelNum_1 * 10));
        focusProgrammeInfoArea.find("img[name=full_epg_focus_channel_num_0]").attr("src", "images/epg_number/epg_number_" + channelNum_0 + ".png");
        focusProgrammeInfoArea.find("img[name=full_epg_focus_channel_num_1]").attr("src", "images/epg_number/epg_number_" + channelNum_1 + ".png");
        focusProgrammeInfoArea.find("img[name=full_epg_focus_channel_num_2]").attr("src", "images/epg_number/epg_number_" + channelNum_2 + ".png");

        // 예약 아이콘
        var reserveResult = KTW.managers.service.ReservationManager.isReserved(programme);
        var iconBook = null;
        var iconLeft = 341;
        var nameLeft = 341;
        if (reserveResult !== KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
            if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SHORT) {
                iconBook = "images/icon/icon_book.png";
            }
            else if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
                iconBook = "images/icon/icon_book_s.png";
                iconLeft = 336;
            }
        }
        if (iconBook) {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_book]").attr("src", iconBook).css({visibility: "inherit"});
            nameLeft = 341 + 35;
        }
        else {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_book]").css({visibility: "hidden"});
        }

        // 프로그램 이름
        var nameWidth = util.getTextLength(programme.name, "RixHead L", 35, -1.75);
        var maxnameWidth = 960;
        if (iconBook) {
            maxnameWidth-=35;
        }

        maxnameWidth-=47;

        if (nameWidth < maxnameWidth) {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_book]").css({left: iconLeft, top: 202});

            focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_name_0]").css({left: nameLeft, top: 202}).text(programme.name);
            focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_name_1]").text("");

            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_age_icon]").css({left: nameLeft + nameWidth + 10, top: 202});
        }
        else {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_book]").css({left: iconLeft, top: 180});
            maxnameWidth = 960;
            if (iconBook) {
                maxnameWidth-=35;
            }

            var arrTempName = util.stringToMultiLine(programme.name, "RixHead L" , 35 , maxnameWidth , -1.75);
            focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_name_0]").css({left: nameLeft, top: 180}).text(arrTempName[0]);
            focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_name_1]").text(arrTempName[1]);

            var secondLineWidth = util.getTextLength(arrTempName[1], "RixHead L", 35, -1.75);
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_age_icon]").css({left: 341 + secondLineWidth + 10, top: 222});
        }


        // 연령 아이콘
        var age = KTW.utils.epgUtil.getAge(programme);
        var ageImg;
        if (age && age > 0) {
            ageImg = "images/icon/icon_age_txtlist_" + age + ".png"
        }
        else if (age === 0) {
            ageImg = "images/icon/icon_age_txtlist_all.png"
        }
        else {
            ageImg = "";
        }

        if (ageImg === "") {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_age_icon]").css({visibility: "hidden"});
        }
        else {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_age_icon]").attr({src: ageImg}).css({visibility: "inherit"});
        }


        // 프로그레스
        // 2017.05.01 dhlee
        // reviseType은 0 ~ 3 사이의 값이므로 program.reviseType 이 PROGRAM_REVISE_TYPE.NULL(0) 인 경우 false가 된다
        //if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
        if (programme.reviseType != undefined && programme.reviseType != null &&
            (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
            || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
            || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {
            focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_progress_area]").css({visibility: "hidden"});

            focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_tag_area]").css({visibility: "hidden"});
        }
        else {
            if (progStartTime < nowTime && progStartTime + progDuration >= nowTime) {
                focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_start_time]").text(util.getTimeString(progStartTime));
                focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_end_time]").text(util.getTimeString(progStartTime + progDuration));
                var width = Math.floor(((nowTime - progStartTime) / progDuration) * 500);
                focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_progress_bar] div").css({width: width});
                focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_progress_bar]").css({visibility: "inherit"});

                drawTagIcon({
                    _this: _this,
                    programme: programme
                });
                focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_tag_area]").css({visibility: "inherit"});
            }
            else {
                var allTimeInfo = util.getTimeString(progStartTime) + " - " + util.getTimeString(progStartTime + progDuration);
                focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_start_time]").text(allTimeInfo);
                focusProgrammeInfoArea.find("span[name=full_epg_focus_programme_end_time]").text("");
                focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_progress_bar]").css({visibility: "hidden"});

                focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_tag_area]").css({visibility: "hidden"});
            }
            focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_progress_area]").css({visibility: "inherit"});
        }


       // ----------------------------




    /*    var arrEpgElement = epgContainer.children();
        var epgElement = _$(arrEpgElement[1]);
        var programmeFocusArea = epgElement.find(".full_epg_focus_programme");

        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[1];
        var programme = progList[programmeIdx];
        var progStartTime = programme.startTime * 1000;
        var progDuration = programme.duration * 1000;
        var nowTime = (new Date()).getTime();

        var iconAgeLeft = 0;

        util.clearAnimation(programmeFocusArea.find("div[name=full_epg_focus_programme_title_box]"));


        // 채널 번호 (포커스 프로그램 영역)
        var channelNum_0 = Math.floor(curChannel.data.majorChannel / 100);
        var channelNum_1 = Math.floor((curChannel.data.majorChannel - (channelNum_0 * 100)) / 10);
        var channelNum_2 = (curChannel.data.majorChannel - (channelNum_0 * 100) - (channelNum_1 * 10));
        focusProgrammeInfoArea.find("img[name=full_epg_focus_channel_num_0]").attr("src", "images/epg_number/epg_number_" + channelNum_0 + ".png");
        focusProgrammeInfoArea.find("img[name=full_epg_focus_channel_num_1]").attr("src", "images/epg_number/epg_number_" + channelNum_1 + ".png");
        focusProgrammeInfoArea.find("img[name=full_epg_focus_channel_num_2]").attr("src", "images/epg_number/epg_number_" + channelNum_2 + ".png");

        // 예약 아이콘
        var reserveResult = KTW.managers.service.ReservationManager.isReserved(programme);
        var iconBook = programmeFocusArea.find("img[name=full_epg_focus_programme_icon_book]");
        var nameLeft = 341;
        var iconLeft = 341;

        if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SHORT) {
            iconBook.attr("src", window.modulePath + "resource/icon_book.png").css({visibility: "inherit"});
            programmeFocusArea.find("[name=full_epg_focus_programme_name]").css({"margin-left": 69});
            iconAgeLeft = 79;
        }
        else if (reserveResult === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
            iconBook.attr("src", window.modulePath + "resource/icon_book_s.png").css({visibility: "inherit"});
            programmeFocusArea.find("[name=full_epg_focus_programme_name]").css({"margin-left": 69});
            iconAgeLeft = 79;
        }
        else {
            iconBook.css({visibility: "hidden"});
            programmeFocusArea.find("[name=full_epg_focus_programme_name]").css({"margin-left": 29});
            iconAgeLeft = 39;
        }

        if (iconBook) {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_book]").attr("src", iconBook).css({visibility: "inherit"});
            nameLeft = 341 + 35;
        }
        else {
            focusProgrammeInfoArea.find("img[name=full_epg_focus_programme_book]").css({visibility: "hidden"});
        }



        // 프로그램 이름
        var nameWidth = util.getTextLength(programme.name, "RixHead L", 35, -1.75);
        iconAgeLeft += nameWidth;
        programmeFocusArea.find("span[name=full_epg_focus_programme_name]").text(programme.name);

        // 연령 아이콘
        var age = KTW.utils.epgUtil.getAge(programme);
        var ageImg;
        if (age && age > 0) {
            ageImg = window.modulePath + "resource/icon_age_txtlist_" + age + ".png"
        }
        else if (age === 0) {
            ageImg = window.modulePath + "resource/icon_age_txtlist_all.png"
        }
        else {
            ageImg = "";
        }
        programmeFocusArea.find("img[name=full_epg_focus_programme_age_icon]").attr({src: ageImg}).css({"margin-left": iconAgeLeft});

        // 2017.05.04 dhlee 1100 에서 1200 으로 변경 (최대한 넓게 잡아 달라는 요청으로)
        // 기준 값은 1310px 임 (full_epg_focus_programme)
        if (nameWidth > 1200) {
            util.startBoxSlidingAnimation({
                targetBox: programmeFocusArea.find("div[name=full_epg_focus_programme_title_box]"),
                targetWidth: iconAgeLeft + 37,
                boxWidth: 1325,
                speed: 200
            });
        }

        // 프로그레스
        // 2017.05.01 dhlee
        // reviseType은 0 ~ 3 사이의 값이므로 program.reviseType 이 PROGRAM_REVISE_TYPE.NULL(0) 인 경우 false가 된다
        //if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
        if (programme.reviseType != undefined && programme.reviseType != null &&
                (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
                || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
                || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {
            programmeFocusArea.find("div[name=full_epg_focus_programme_progress_area]").css({visibility: "hidden"});

            programmeFocusArea.find("div[name=full_epg_focus_programme_tag_area]").css({visibility: "hidden"});
        }
        else {
            if (progStartTime < nowTime && progStartTime + progDuration >= nowTime) {
                programmeFocusArea.find("span[name=full_epg_focus_programme_start_time]").text(util.getTimeString(progStartTime));
                programmeFocusArea.find("span[name=full_epg_focus_programme_end_time]").text(util.getTimeString(progStartTime + progDuration));
                var width = Math.floor(((nowTime - progStartTime) / progDuration) * 300);
                programmeFocusArea.find("div[name=full_epg_focus_programme_progress_bar] div").css({width: width});
                programmeFocusArea.find("div[name=full_epg_focus_programme_progress_bar]").css({visibility: "inherit"});

                drawTagIcon({
                    _this: _this,
                    programme: programme
                });
                programmeFocusArea.find("div[name=full_epg_focus_programme_tag_area]").css({visibility: "inherit"});
            }
            else {
                var allTimeInfo = util.getTimeString(progStartTime) + " - " + util.getTimeString(progStartTime + progDuration);
                programmeFocusArea.find("span[name=full_epg_focus_programme_start_time]").text(allTimeInfo);
                programmeFocusArea.find("span[name=full_epg_focus_programme_end_time]").text("");
                programmeFocusArea.find("div[name=full_epg_focus_programme_progress_bar]").css({visibility: "hidden"});

                programmeFocusArea.find("div[name=full_epg_focus_programme_tag_area]").css({visibility: "hidden"});
            }
            programmeFocusArea.find("div[name=full_epg_focus_programme_progress_area]").css({visibility: "inherit"});
        }*/
    }

    function drawPipChannelInfo (channel) {
        log.printDbg("drawPipChannelInfo()");

        if (!channel) {
            channel = navAdapter.getCurrentChannel();
        }

        if (channel) {
            pipChannelInfoArea.find("[name=full_epg_pip_channel_num]").text(util.numToStr(channel.data.majorChannel, 3));
            pipChannelInfoArea.find("[name=full_epg_pip_channel_name]").text(channel.data.name);
        }
        else {
            pipChannelInfoArea.find("[name=full_epg_pip_channel_num]").text("");
            pipChannelInfoArea.find("[name=full_epg_pip_channel_name]").text("");
        }
    }


    function drawTagIcon (options) {
        log.printDbg("drawTagIcon()");

        var curChannel = options._this.getCurrentChannel();
        var tagArea = focusProgrammeInfoArea.find("div[name=full_epg_focus_programme_tag_area]");
        var left = 0;

        if (curChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S) {
            var programme = options.programme;

            if (programme && programme.description && programme.description.length > 0) {
                /**
                 *  자막 아이콘 활성화
                 */
                if (programme.description.indexOf('(자)') > -1) {
                    tagArea.find("[name=full_epg_focus_programme_tag_subtitles]").css({left: left, visibility: "inherit"});
                    left += 57;
                }
                else {
                    tagArea.find("[name=full_epg_focus_programme_tag_subtitles]").css({visibility: "hidden"});
                }
                /**
                 * 해설 아이콘 활성화
                 */
                if (programme.description.indexOf('(해)') > -1) {
                    tagArea.find("[name=full_epg_focus_programme_tag_comment]").css({left: left, visibility: "inherit"});
                    left += 57;
                }
                else {
                    tagArea.find("[name=full_epg_focus_programme_tag_comment]").css({visibility: "hidden"});
                }

                /**
                 * 수화 아이콘 활성화
                 */
                if (programme.description.indexOf('(수)') > -1) {
                    tagArea.find("[name=full_epg_focus_programme_tag_sign]").css({left: left, visibility: "inherit"});
                    left += 57;
                }
                else {
                    tagArea.find("[name=full_epg_focus_programme_tag_sign]").css({visibility: "hidden"});
                }
            }
            else {
                tagArea.find("[name=full_epg_focus_programme_tag_subtitles]").css({visibility: "hidden"});
                tagArea.find("[name=full_epg_focus_programme_tag_comment]").css({visibility: "hidden"});
                tagArea.find("[name=full_epg_focus_programme_tag_sign]").css({visibility: "hidden"});
            }
        }
        else {
            // [dj.son] OTV 에서는 main vbo 로 tune 을 해봐야 자막, 해설, 수화 여부 알 수 있음
            tagArea.css({visibility: "hidden"});
        }
    }

    function getCurChannelList (_this) {
        log.printDbg("getCurChannelList()");

        var channelList = _this.getChannelList();
        var channelIdx = _this.getChannelIdx();

        var curPage = Math.floor(channelIdx / CHANNELS_PER_PAGE);
        var startIdx = curPage * CHANNELS_PER_PAGE;

        return channelList.slice(startIdx, startIdx + CHANNELS_PER_PREVIEW);
    }

    function getFocusChannelIdx (_this) {
        return _this.getChannelIdx() % CHANNELS_PER_PAGE;
    }


    function reviseProgrammeList (options) {

        log.printDbg("reviseProgrammeList()");

        options = options || {};

        var programmeList = options.programmeList;

        if (programmeList) {
            var channelList = getCurChannelList(options._this);
            var chLength = channelList.length;
            var progLength = programmeList.length;

            for (var i = progLength - 1; i >= 0; i--) {
                if (programmeList.length > chLength) {
                    programmeList.pop();
                }
                else {
                    break;
                }
            }

        }

        return programmeList;
/*
        log.printDbg("reviseProgrammeList()");

        options = options || {};

        var programmeList = options.programmeList;

        if (programmeList) {
            var length = programmeList.length;

            if (length < 3) {
                //var lastProgList = programmeList[1];
                //
                //if (lastProgList) {
                //    var lastProgrammeChannel = lastProgList.channel ? lastProgList.channel : lastProgList[0].channel;
                //
                //    if (lastProgrammeChannel) {
                //        var curChannel = options._this.getCurrentChannel();
                //
                //        if (curChannel.ccid === lastProgrammeChannel.ccid) {
                //            var temp = programmeList[0];
                //            programmeList[0] = programmeList[1];
                //            programmeList[1] = temp;
                //        }
                //    }
                //}

                programmeList.unshift(null);
            }
            else if (length < CHANNELS_PER_PAGE) {
                //programmeList.unshift(programmeList.pop());
            }
        }
*/

        return programmeList;
    }

    /**
     * [dj.son] set focus class - scrollArea or epgArea
     */
    function setFocusArea (_this) {
        log.printDbg("setFocusArea()");

        if (isFocusEpgArea) {
            epgArea.addClass("focus");
        }
        else {
            epgArea.removeClass("focus");
        }

        drawNowDim(_this);
    }

    /**
     * [dj.son] set epg focus
     */
    function setFocusEpgArea (_this) {
        log.printDbg("setFocusEpgArea()");
/*
        var arrEpgElement = epgContainer.children();
        var epgElement = _$(arrEpgElement[1]);
        var arrProgrammeElement = epgElement.find(".full_epg_programme_list").children();
        var programmeElement = _$(arrProgrammeElement[programmeIdx]);

        // programme item 에 focus
        arrProgrammeElement.removeClass("focus");
        programmeElement.addClass("focus");

        drawFocusProgramme(_this);
        drawNowDim(_this);*/


        var focusChannelIdx = getFocusChannelIdx(_this);
        var arrEpgElement = epgContainer.children();
        var epgElement = _$(arrEpgElement[focusChannelIdx]);
        var arrProgrammeElement = epgElement.find(".full_epg_programme_list").children();
        var programmeElement = _$(arrProgrammeElement[programmeIdx]);


        arrEpgElement.removeClass("focus");
        epgContainer.find(".full_epg_programme_list").children().removeClass("focus");

        epgElement.addClass("focus");
        programmeElement.addClass("focus");

        //focusArea.css({top: 313 + (focusChannelIdx * 108)});

        drawFocusProgramme(_this);
        drawNowDim(_this);
    }

    /**
     * [dj.son] set focus
     */
    function setFocus (_this) {
        log.printDbg("setFocusEpg()");

        setFocusArea(_this);

        setFocusEpgArea(_this);
    }

    function startTimeUpdateTimer (options) {
        log.printDbg("startTimeUpdateTimer()");

        nowTime = new Date();

        if (options.forceUpdate) {
            drawUpdateUI(options._this);
        }

        if (!updateTimer) {
            updateTimer = timeUpdateTimer(options._this);
        }
    }

    function timeUpdateTimer (_this) {
        var restTime = (60 * 1000) - ((nowTime.getSeconds() * 1000) + nowTime.getMilliseconds()) + 100;
        return setTimeout(function () {
            nowTime = new Date();
            drawUpdateUI(_this);

            updateTimer = timeUpdateTimer(_this);
        }, restTime);
    }

    function endTimeUpdateTimer () {
        log.printDbg("endTimeUpdateTimer()");

        if (updateTimer) {
            clearTimeout(updateTimer);
        }

        nowTime = updateTimer = null;
    }

    function drawUpdateUI (_this) {
        log.printDbg("drawUpdateUI()");

        if (div.hasClass("focus")) {
            drawSubArea(_this);

            // [dj.son] programme index update
            var programmeList = reviseProgrammeList({
                _this: _this,
                programmeList: _this.getProgrammesSectionByTime(referenceTime)
            });
            var progList = programmeList[1];
            var curProgramme = progList[programmeIdx];

            if (nowTime.getMinutes() == 0 || nowTime.getMinutes() == 30) {
                if (nowDimArea.css("visibility") !== "hidden") {
                    referenceTime = new Date(referenceTime.getTime() + 30 * 60 * 1000);
                    drawReferenceTime();

                    programmeList = reviseProgrammeList({
                        _this: _this,
                        programmeList: _this.getProgrammesSectionByTime(referenceTime)
                    });
                    progList = programmeList[1];
                }

                _this.updateProgrammeList();

                /**
                 * [dj.son] 프로그램 리스트가 업데이트 될때, programme index 조정
                 *
                 * - 현재 프로그램 리스트에서 포커스 프로그램을 찾아서 해당 값으로 index 설정
                 * - 없으면 0으로 설정
                 */
                var tempProgrammeIdx = 0;
                for (var i = 0; i < progList.length; i++) {
                    var tempProgramme = progList[i];

                    if (tempProgramme.startTime === curProgramme.startTime && tempProgramme.duration === curProgramme.duration) {
                        tempProgrammeIdx = i;
                        break;
                    }
                }
                programmeIdx = tempProgrammeIdx;

                drawProgrammes(_this);
                setFocusEpgArea(_this);
            }
            else {
                /**
                 * [dj.son] 현재 시간이 현재 포커스 프로그램의 종료 시간보다 클 경우, programme index 조정
                 */
                if (curProgramme) {
                    var startTime = curProgramme.startTime;
                    var endTime = startTime + curProgramme.duration;

                    var tempNotTime = nowTime.getTime() / 1000;

                    if (endTime <= tempNotTime) {
                        programmeIdx++;

                        setFocusEpgArea(_this);
                    }
                    else {
                        drawFocusProgramme(_this);
                        drawNowDim(_this);
                    }
                }
                else {
                    drawFocusProgramme(_this);
                    drawNowDim(_this);
                }
            }
        }
        else {
            drawPreview(_this);
        }
    }

    /**
     * [dj.son] Pip start
     */
    function startPip (_this) {
        log.printDbg("startPip()");

        var pipType = KTW.managers.service.PipManager.PIP_TYPE.FULL_EPG;
        if (_this.menuId === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
            pipType = KTW.managers.service.PipManager.PIP_TYPE.FAV_EPG;
        }

        if (pipTimer) {
            clearTimeout(pipTimer);
        }

        pipTimer = setTimeout(function () {
            KTW.managers.service.PipManager.activatePipLayer({pipType: pipType});
            setPipChannel(_this);

            pipTimer = null;
        }, 700);
    }

    /**
     * [dj.son] set pip channel
     */
    function setPipChannel (_this) {
        log.printDbg("setPipChannel()");

        if (!KTW.managers.service.PipManager.isPipLayerShow()) {
            return;
        }

        KTW.managers.service.PipManager.changeChannel(_this.getCurrentChannel());

        //var pip_channel_control = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
        //
        //pip_channel_control.changeChannel(_this.getCurrentChannel());
    }

    /**
     * [dj.son] end pip
     */
    function endPip () {
        log.printDbg("endPip()");

        if (pipTimer) {
            clearTimeout(pipTimer);
            pipTimer = null;
        }

        KTW.managers.service.PipManager.deactivatePipLayer();
    }

    /**
     * [dj.son] check last programme in All Programme List
     */
    function isLastProgramme (options) {
        log.printDbg("isLastProgramme()");

        var programmeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesList()
        });
        var progList = programmeList[1];
        var lastProgramme = progList[progList.length - 1];

        return options.programme == lastProgramme ? true : false;
    }

    /**
     * [dj.son] calculate programmeIdx if focus channel change
     */
    function getNextProgrammeIdx (options) {
        log.printDbg("getNextProgrammeIdx()");

        if (!options || !options.programme || !options.nextProgrammeList) {
            return programmeIdx;
        }

        var nextProgrammeIdx = programmeIdx;

        var startTime = options.programme.startTime;
        var endTime = startTime + options.programme.duration;
        var nextStartTime;
        var nextEndTime;
        var gab = null;
        var tempGab = 0;
        var length = options.nextProgrammeList.length;


        var now = new Date();
        var nowTime = now.getTime() / 1000;
        var tempNowReferenceTime = epgUtil.reviseDate(now);

        // 현재 포커스 프로그램이 과거 & 현재 방영중인 프로그램이면, next 채널의 현재 방영중인 프로그램으로 리턴
        if (tempNowReferenceTime.getTime() === referenceTime.getTime()) {
            if (startTime < nowTime) {
                for (var i = 0; i < length; i++) {
                    nextStartTime = options.nextProgrammeList[i].startTime;
                    nextEndTime = nextStartTime + options.nextProgrammeList[i].duration;
                    if (nextStartTime <= nowTime && nextEndTime > nowTime) {
                        return i;
                    }
                }
            }
        }

        // 현재 포커스 프로그램과 가장 많이 겹치는 프로그램으로 리턴
        for (var i = 0; i < length; i++) {
            nextStartTime = options.nextProgrammeList[i].startTime;
            nextEndTime = nextStartTime + options.nextProgrammeList[i].duration;
            tempGab = null;

            if (nextEndTime < startTime) {
                continue;
            }

            if (nextStartTime > endTime) {
                break;
            }

            if (nextStartTime >= startTime && nextEndTime <= endTime ||
                nextStartTime <= startTime && nextEndTime >= endTime) {
                nextProgrammeIdx = i;
                break;
            }
            else if (nextStartTime < startTime) {
                tempGab = nextEndTime - startTime;
            }
            else if (nextStartTime >= startTime) {
                tempGab = endTime - nextStartTime;
            }

            if (!gab || tempGab > gab) {
                gab = tempGab;
                nextProgrammeIdx = i;
            }
        }

        if (nextProgrammeIdx >= length) {
            nextProgrammeIdx = length - 1;
        }

        return nextProgrammeIdx;
    }

    /**
     * [dj.son] move focus programme
     */
    function moveFocusProgramme (options) {
        log.printDbg("moveFocusProgramme(" + options.direction + ")");

        var focusChannelIdx = getFocusChannelIdx(options._this);
        var programmeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[focusChannelIdx];

        if (options.direction == "left") {
            if (programmeIdx > 0) {
                programmeIdx--;
            }
            else {
                referenceTime = new Date(referenceTime.getTime() - 90 * 60 * 1000);
                var tempNowTime = epgUtil.reviseDate(new Date());
                referenceTime = referenceTime < tempNowTime ? tempNowTime : referenceTime;
                programmeList = reviseProgrammeList({
                    _this: options._this,
                    programmeList: options._this.getProgrammesSectionByTime(referenceTime)
                });
                progList = programmeList[focusChannelIdx];
                programmeIdx = progList.length - 1;

                drawReferenceTime();
                drawProgrammes(options._this);
                drawNowDim(options._this);
            }

            setFocus(options._this);
        }
        else if (options.direction == "right") {
            if (!isLastProgramme({_this: options._this, programme: progList[programmeIdx]})) {
                if (programmeIdx < progList.length - 1) {
                    programmeIdx++;
                }
                else {
                    referenceTime = new Date(referenceTime.getTime() + 90 * 60 * 1000);
                    programmeIdx = 0;

                    drawReferenceTime();
                    drawProgrammes(options._this);
                    drawNowDim(options._this);
                }

                setFocus(options._this);
            }
        }

/*        var programmeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[1];

        if (options.direction == "left") {
            if (programmeIdx > 0) {
                programmeIdx--;
            }
            else {
                referenceTime = new Date(referenceTime.getTime() - 90 * 60 * 1000);
                var tempNowTime = epgUtil.reviseDate(new Date());
                referenceTime = referenceTime < tempNowTime ? tempNowTime : referenceTime;

                programmeList = reviseProgrammeList({
                    _this: options._this,
                    programmeList: options._this.getProgrammesSectionByTime(referenceTime)
                });

                progList = programmeList[1];
                programmeIdx = progList.length - 1;

                drawReferenceTime();
                drawProgrammes(options._this);
                drawNowDim();
            }

            setFocus(options._this);
        }
        else if (options.direction == "right") {
            if (!isLastProgramme({_this: options._this, programme: progList[programmeIdx]})) {
                if (programmeIdx < progList.length - 1) {
                    programmeIdx++;
                }
                else {
                    referenceTime = new Date(referenceTime.getTime() + 90 * 60 * 1000);
                    programmeIdx = 0;

                    drawReferenceTime();
                    drawProgrammes(options._this);
                    drawNowDim();
                }

                setFocus(options._this);
            }
        }*/
    }

    /**
     * [dj.son] move focus channel
     */
    function moveFocusChannel (options) {
/*        log.printDbg("moveFocusChannel(" + options.nextChannelIdx + ")");

        if (options.nextChannelIdx == null || options.nextChannelIdx == undefined) {
            log.printDbg("nextChannelIdx is null... so return");
            return;
        }

        var channelIdx = options._this.getChannelIdx();
        var channelList = options._this.getChannelList();
        var length = channelList.length;

        if (options.nextChannelIdx == channelIdx) {
            log.printDbg("nextChannelIdx == current channel idx... so return");
            return;
        }
        if (options.nextChannelIdx < 0 || options.nextChannelIdx > length - 1) {
            log.printDbg("nextChannelIdx is out of range... so return");
            return;
        }

        var programmeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[1];
        //var nextProgrammeList = programmeList;
        var isUp = true;
        //var isChangeChannelPage = false;

        if ((options.nextChannelIdx > channelIdx && options.nextChannelIdx != length - 1)
            || (options.nextChannelIdx < channelIdx && options.nextChannelIdx == 0)) {
            isUp = false;
        }

        log.printDbg("isUp : " + isUp);

        options._this.changeChannelIdx({
            nextChannelIdx: options.nextChannelIdx,
            isProgrammesListUpdate: true,
            isUp: isUp
        });

        var nextProgrammeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });

        programmeIdx = getNextProgrammeIdx({
            programme: progList[programmeIdx],
            nextProgrammeList: nextProgrammeList[1]
        });

        drawChannel(options._this);
        drawProgrammes(options._this);

        setFocus(options._this);
       // setPipChannel(options._this);*/




/*        log.printDbg("moveFocusChannel(" + options.nextChannelIdx + ")");

        if (options.nextChannelIdx == null || options.nextChannelIdx == undefined) {
            log.printDbg("nextChannelIdx is null... so return");
            return;
        }

        var channelIdx = options._this.getChannelIdx();
        var channelList = options._this.getChannelList();
        var length = channelList.length;

        if (options.nextChannelIdx == channelIdx) {
            log.printDbg("nextChannelIdx == current channel idx... so return");
            return;
        }
        if (options.nextChannelIdx < 0 || options.nextChannelIdx > length - 1) {
            log.printDbg("nextChannelIdx is out of range... so return");
            return;
        }

        // 현재 포커스된 채널의 programme list 가져오기
        var programmeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });
        var focusChannelIdx = getFocusChannelIdx(options._this);
        var curProgList = programmeList[focusChannelIdx];

        var curPage = options._this.getChannelPage();
        var nextPage = Math.floor(options.nextChannelIdx / CHANNELS_PER_PAGE);
        var isProgrammesListUpdate = false;
        if (curPage !== nextPage) {
            isProgrammesListUpdate = true;
        }

        var isUp = true;
        if ((options.nextChannelIdx > channelIdx && options.nextChannelIdx != length - 1)
            || (options.nextChannelIdx < channelIdx && options.nextChannelIdx == 0)) {
            isUp = false;
        }

        log.printDbg("isProgrammesListUpdate : " + isProgrammesListUpdate + ", isUp : " + isUp);



        options._this.changeChannelIdx({
            nextChannelIdx: options.nextChannelIdx,
            isProgrammesListUpdate: isProgrammesListUpdate,
            isUp: isUp
        });

        var nextProgrammeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });

        // next 포커스 채널의 programme list 가져와서 next programmeIdx 얻기
        focusChannelIdx = getFocusChannelIdx(options._this);
        programmeIdx = getNextProgrammeIdx({
            programme: curProgList[programmeIdx],
            nextProgrammeList: nextProgrammeList[focusChannelIdx]
        });

        if (isProgrammesListUpdate) {
            drawChannel(options._this);
            drawProgrammes(options._this);
          //  indicator.setPos(options._this.getChannelPage());
          //  scroll.setCurrentPage(options._this.getChannelPage());
        }

        setFocus(options._this);*/

        log.printDbg("moveFocusChannel(" + options.nextChannelIdx + ")");

        if (options.nextChannelIdx == null || options.nextChannelIdx == undefined) {
            log.printDbg("nextChannelIdx is null... so return");
            return;
        }

        var channelIdx = options._this.getChannelIdx();
        var channelList = options._this.getChannelList();
        var length = channelList.length;

        if (options.nextChannelIdx == channelIdx) {
            log.printDbg("nextChannelIdx == current channel idx... so return");
            return;
        }
        if (options.nextChannelIdx < 0 || options.nextChannelIdx > length - 1) {
            log.printDbg("nextChannelIdx is out of range... so return");
            return;
        }

        // 현재 포커스된 채널의 programme list 가져오기
        var programmeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });
        var focusChannelIdx = getFocusChannelIdx(options._this);
        var curProgList = programmeList[focusChannelIdx];

        var curPage = options._this.getChannelPage();
        var nextPage = Math.floor(options.nextChannelIdx / CHANNELS_PER_PAGE);
        var isProgrammesListUpdate = false;
        if (curPage !== nextPage) {
            isProgrammesListUpdate = true;
        }

        var isUp = true;
        if ((options.nextChannelIdx > channelIdx && options.nextChannelIdx != length - 1)
            || (options.nextChannelIdx < channelIdx && options.nextChannelIdx == 0)) {
            isUp = false;
        }

        log.printDbg("isProgrammesListUpdate : " + isProgrammesListUpdate + ", isUp : " + isUp);

        options._this.changeChannelIdx({
            nextChannelIdx: options.nextChannelIdx,
            isProgrammesListUpdate: isProgrammesListUpdate,
            isUp: isUp
        });

        var nextProgrammeList = reviseProgrammeList({
            _this: options._this,
            programmeList: options._this.getProgrammesSectionByTime(referenceTime)
        });

        // next 포커스 채널의 programme list 가져와서 next programmeIdx 얻기
        focusChannelIdx = getFocusChannelIdx(options._this);
        programmeIdx = getNextProgrammeIdx({
            programme: curProgList[programmeIdx],
            nextProgrammeList: nextProgrammeList[focusChannelIdx]
        });

        if (isProgrammesListUpdate) {
            drawChannel(options._this);
            drawProgrammes(options._this);
        }

        setFocus(options._this);
    }

    /**
     * [dj.son] move one page
     */
    function movePage (options) {
        log.printDbg("movePage(" + options.direction + ")");

        var channelIdx = options._this.getChannelIdx();
        var channelList = options._this.getChannelList();
        var nextChannelIdx = channelIdx;
        var curPage = options._this.getChannelPage();


        if (options.direction === "up") {
            curPage--;

            if (curPage < 0) {
                curPage = options._this.getChannelMaxPage() - 1;
            }
        }
        else if (options.direction === "down") {
            curPage++;

            if (curPage > options._this.getChannelMaxPage() - 1) {
                curPage = 0;
            }
        }

        nextChannelIdx = (curPage * CHANNELS_PER_PAGE) + (channelIdx % CHANNELS_PER_PAGE);

        if (options.bLastIdx) {
            nextChannelIdx = (curPage * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;
        }

        if (nextChannelIdx > channelList.length - 1) {
            nextChannelIdx = channelList.length - 1;
        }

        moveFocusChannel({
            _this: options._this,
            nextChannelIdx: nextChannelIdx
        });
    }

    function setFocusChannelTune (_this) {
        log.printDbg("setFocusChannelTune()");

        var channelList = _this.getChannelList();
        var channelIdx = _this.getChannelIdx();
        var focusChannel = channelList[channelIdx];

        /**
         * [dj.son] [WEBIIIHOME-3240]
         * - DCS 편성표의 경우 채널 튠시, select 만 수행하도록 수정
         *
         * - 같은 채널을 선택했을 경우 튠을 수행하여 miniEpg 가 자연스럽게 노출되도록 수정하였었는데,
         *   이때 ChannelControl 에서 miniEpg 를 activate 하면서 채널 data 를 안넘겨주는 로직을 타면서 miniEpg 가 이전 채널 정보를 보여주는 이슈가 생겼음.
         * - 고도화때 miniEpg 로직 개선 예정이므로 지금 ChannelControl 을 수정하기에는 부담이 됨.
         *   따라서 해당 이슈는 DCS 편성표에서 직접 miniEpg 를 activate 하는 것으로 수정.
         *
         * TODO 추후에 miniEpg 로직이 개선되면 이 부분도 같이 개선
         */
        var curChannel = navAdapter.getCurrentChannel();

        if (curChannel.ccid !== focusChannel.ccid || KTW.managers.service.StateManager.isVODPlayingState()) {
            drawPipChannelInfo(focusChannel);
            mainChannelTune({channel: focusChannel, onlySelect: true});
        }
        else {
            var layer = LayerManager.getLayer(KTW.ui.Layer.ID.MINI_EPG);
            var dataParams = null;
            if (layer) {
                dataParams = layer.getParams();

                if (dataParams !== undefined && dataParams !== null) {
                    var tempParams = {};
                    tempParams.fromKey = true;

                    if (dataParams.data !== undefined && dataParams.data !== null) {
                        tempParams.data = dataParams.data;
                    }
                    dataParams = tempParams;
                }
                else {
                    dataParams = {fromKey:true};
                }
            }
            else {
                dataParams = {fromKey:true};
            }

            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.MINI_EPG,
                    type: KTW.ui.Layer.TYPE.TRIGGER,
                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                    linkage: false,
                    params: dataParams
                },
                clear_normal: true,
                visible: true
            });
        }

    }
    // 2017.08.29 Yun 채널 Tune에 대한 TypeError로 추가
    function mainChannelTune(a) {
        if (a && a.channel)if (window.stateManager.isVODPlayingState())window.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (b) {
            b && b.execute({method: "stopVodWithChannelSelection", params: {channelObj: a.channel}})
        }); else {
            var b = window.oipfAdapter.navAdapter.getChannelControl("main");
            b.changeChannel(a.channel, a.onlySelect)
        }
    }

    function reservationProgramme (options) {
        log.printDbg("reservationProgramme()");

        window.reservationManager.add(options.programme, true, function (result) {
            var RESULT = window.reservationManager.ADD_RESULT;
            if (result === RESULT.SUCCESS_RESERVE || result === RESULT.SUCCESS_CANCEL
                || result === RESULT.SUCCESS_SERIES_RESERVE || result === RESULT.SUCCESS_SERIES_CANCEL
                || result === RESULT.DUPLICATE_RESERVE) {
                drawProgrammes(options._this);
                setFocus(options._this);
            }
        });
    }


    function keyLeft (_this) {
        log.printDbg("keyLeft()");

        var consumed = false;
/*        if (isChannelListExist(_this)) {
            var channelMaxPage = _this.getChannelMaxPage();

            if (isFocusEpgArea) {
                var initTime = epgUtil.reviseDate(new Date());

                if (initTime.getTime() == referenceTime.getTime() && isFirstProgramme(_this)) {
                    if (channelMaxPage > 1) {
                        indicator.focused();
                        isFocusEpgArea = false;
                        setFocusArea();
                        drawNowDim();
                        consumed = true;
                    }
                }
                else {
                    moveFocusProgramme({
                        _this: _this,
                        direction: "left"
                    });
                    consumed = true;
                }
            }
        }*/

        if (isChannelListExist(_this)) {
            var channelMaxPage = _this.getChannelMaxPage();

            if (isFocusEpgArea) {
                var initTime = epgUtil.reviseDate(new Date());

                if (initTime.getTime() == referenceTime.getTime() && isFirstProgramme(_this)) {

                }
                else {
                    moveFocusProgramme({
                        _this: _this,
                        direction: "left"
                    });
                    consumed = true;
                }
            }
        }

        return consumed;
    }

    function keyRight (_this) {
        log.printDbg("keyRight()");
/*
        if (isChannelListExist(_this)) {
            if (isFocusEpgArea) {
                moveFocusProgramme({
                    _this: _this,
                    direction: "right"
                });
            }
            else {
                indicator.blurred();
                isFocusEpgArea = true;
                drawNowDim();
                setFocusArea();
            }
        }*/


        if (isChannelListExist(_this)) {
            if (isFocusEpgArea) {
                moveFocusProgramme({
                    _this: _this,
                    direction: "right"
                });
            }
            else {
                isFocusEpgArea = true;
                setFocusArea(_this);
            }
        }
    }

    function keyUp (_this) {
        log.printDbg("keyUp()");

        if (isChannelListExist(_this)) {
            var channelIdx = _this.getChannelIdx();
            var channelList = _this.getChannelList();
            var nextChannelIdx = channelIdx;

            if (isFocusEpgArea) {
                --nextChannelIdx;
            }
            else {
                var curPage = _this.getChannelPage();

                curPage--;

                if (curPage < 0) {
                    curPage = _this.getChannelMaxPage() - 1;
                }

                nextChannelIdx = curPage * CHANNELS_PER_PAGE;
            }

            if (nextChannelIdx < 0) {
                nextChannelIdx = channelList.length - 1;
            }

            moveFocusChannel({
                _this: _this,
                nextChannelIdx: nextChannelIdx
            });
        }

     /*   if (isChannelListExist(_this)) {
            if (isFocusEpgArea) {
                var channelIdx = _this.getChannelIdx();
                var channelList = _this.getChannelList();
                var nextChannelIdx = channelIdx;

                --nextChannelIdx;

                if (nextChannelIdx < 0) {
                    nextChannelIdx = channelList.length - 1;
                }

                moveFocusChannel({
                    _this: _this,
                    nextChannelIdx: nextChannelIdx
                });
            }
            else {
                movePage({
                    _this: _this,
                    direction: "up"
                });
            }
        }*/
    }

    function keyDown (_this) {
        log.printDbg("keyDown()");

        if (isChannelListExist(_this)) {
            var channelIdx = _this.getChannelIdx();
            var channelList = _this.getChannelList();
            var nextChannelIdx = channelIdx;

            if (isFocusEpgArea) {
                ++nextChannelIdx;
            }
            else {
                nextChannelIdx = channelIdx + (CHANNELS_PER_PAGE - (channelIdx % CHANNELS_PER_PAGE));
            }

            if (nextChannelIdx > channelList.length - 1) {
                nextChannelIdx = 0;
            }

            moveFocusChannel({
                _this: _this,
                nextChannelIdx: nextChannelIdx
            });
        }

/*        if (isChannelListExist(_this)) {
            if (isFocusEpgArea) {
                var channelIdx = _this.getChannelIdx();
                var channelList = _this.getChannelList();
                var nextChannelIdx = channelIdx;

                ++nextChannelIdx;

                if (nextChannelIdx > channelList.length - 1) {
                    nextChannelIdx = 0;
                }

                moveFocusChannel({
                    _this: _this,
                    nextChannelIdx: nextChannelIdx
                });
            }
            else {
                movePage({
                    _this: _this,
                    direction: "down"
                });
            }
        }*/
    }

    function keyOk (_this) {
        log.printDbg("keyOk()");

        if (isChannelListExist(_this)) {
            if (isFocusEpgArea) {
                var focusChannelIdx = getFocusChannelIdx(_this);
                var programmeList = reviseProgrammeList({
                    _this: _this,
                    programmeList: _this.getProgrammesSectionByTime(referenceTime)
                });
                var progList = programmeList[focusChannelIdx];
                var selectedProgramme = progList[programmeIdx];
                var startTime = selectedProgramme.startTime * 1000;
                var endTime = (selectedProgramme.startTime + selectedProgramme.duration) * 1000;

                var nowTime = (new Date()).getTime();
                log.printDbg("keyOk(), startTime = " + startTime + ", endTime = " + endTime + ", nowTime = " + nowTime);

                if (startTime <= nowTime && endTime > nowTime) {
                    log.printDbg("keyOk(), now")
                    setFocusChannelTune(_this);
                }
                else if (startTime > nowTime) {
                    reservationProgramme({
                        programme: selectedProgramme,
                        _this: _this
                    });
                }
            }
            else {
                isFocusEpgArea = true;
                setFocusArea(_this);
            }
        }
    }


    function keyRed (_this) {
        log.printDbg("keyRed()");

        var curPage = _this.getChannelPage();
        var channelIdx = _this.getChannelIdx();
        var nextChannelIdx = channelIdx;

        if (isFocusEpgArea) {
            if (isChannelListExist(_this)) {
                if (_this.getChannelMaxPage() > 1 && channelIdx % CHANNELS_PER_PAGE === 0) {
                    movePage({
                        _this: _this,
                        direction: "up"
                    });
                }
                else {
                    nextChannelIdx = curPage * CHANNELS_PER_PAGE;

                    moveFocusChannel({
                        _this: _this,
                        nextChannelIdx: nextChannelIdx
                    });
                }
            }
        }
        else {
            isFocusEpgArea = true;
            setFocusArea(_this);

            nextChannelIdx = curPage * CHANNELS_PER_PAGE;

            moveFocusChannel({
                _this: _this,
                nextChannelIdx: nextChannelIdx
            });
        }
    }

    function keyBlue (_this) {
        log.printDbg("keyBlue()");

        var channelIdx = _this.getChannelIdx();
        var channelList = _this.getChannelList();
        var nextChannelIdx = channelIdx;
        var curPage = _this.getChannelPage();

        if (isFocusEpgArea) {
            if (isChannelListExist(_this)) {
                if (_this.getChannelMaxPage() > 1 && (channelIdx === channelList.length - 1 || channelIdx % CHANNELS_PER_PAGE === CHANNELS_PER_PAGE - 1)) {
                    movePage({
                        _this: _this,
                        direction: "down",
                        bLastIdx: true
                    });
                }
                else {
                    nextChannelIdx = (curPage + 1) * CHANNELS_PER_PAGE - 1;

                    if (nextChannelIdx > channelList.length - 1) {
                        nextChannelIdx = channelList.length - 1;
                    }

                    moveFocusChannel({
                        _this: _this,
                        nextChannelIdx: nextChannelIdx
                    });
                }
            }
        }
        else {
            isFocusEpgArea = true;
            setFocusArea(_this);

            nextChannelIdx = (curPage * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;

            if (nextChannelIdx > channelList.length - 1) {
                nextChannelIdx = channelList.length - 1;
            }

            moveFocusChannel({
                _this: _this,
                nextChannelIdx: nextChannelIdx
            });
        }
    }

    function showRelatedMenu (_this) {
        log.printDbg("showRelatedMenu()");

        var isShowFavoritedChannel = true;
        var isShowProgramDetail = true;
        var isShowTwoChannel = false;
        var isShowFourChannel = true;
        var isShowHitChannel = true;
        var isShowGenreOption = false;

        var focusOptionIndex = 0;
        var focusMenuId = _this.menuId;

        var epgrelatedmenuManager = KTW.managers.service.EpgRelatedMenuManager;

        var curChannel = _this.getCurrentChannel();

        if(navAdapter.isAudioChannel(curChannel) === true) {
            isShowFavoritedChannel = false;
            isShowProgramDetail = false;
        }

        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[1];
        var selectedProgramme = progList[programmeIdx];

        if(selectedProgramme !== undefined && selectedProgramme !== null ) {
            if(selectedProgramme.name === epgUtil.DATA.PROGRAM_TITLE.NULL || selectedProgramme.name === epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                isShowProgramDetail = false;
            }
        }

        //var isBlocked = KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(curChannel);

        //if(isBlocked === true || curChannel.desc === 2) {
        if(curChannel.desc === 2) {
            isShowProgramDetail = false;
        }

        epgrelatedmenuManager.showFullEpgRelatedMenu(curChannel, function (menuIndex) {
            cbRelateMenuLeft({
                _this: _this,
                menuIndex: menuIndex
            });
        }, isShowFavoritedChannel, isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel, isShowGenreOption, focusOptionIndex, function (menuId) {
            cbRelateMenuRight({
                _this: _this,
                menuId: menuId
            });
        }, focusMenuId);
    }

    function cbRelateMenuLeft (options) {
        log.printDbg("cbRelateMenuLeft()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
            // 선호채널 등록 / 해제
            var curChannel = options._this.getCurrentChannel();
            KTW.managers.service.EpgRelatedMenuManager.favoriteChannelOnOff(curChannel);
        }
        else if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
            // OTS 자세히 보기
            var programmeList = reviseProgrammeList({
                _this: options._this,
                programmeList: options._this.getProgrammesSectionByTime(referenceTime)
            });
            var progList = programmeList[1];
            var selectedProgramme = progList[programmeIdx];

            /**
             * 남성욱 연구원 여기서 이 부분만 확인 해 줘요.
             * 현재프로그램인 경우 isShowReservation  : false , 미래프로그램 인 경우 isShowReservation : true
             */

            var now = new Date().getTime();
            if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                now = Math.floor(now/1000);
            }


            var isShowReservation = true;

            if(selectedProgramme !== undefined && selectedProgramme !== null
                && selectedProgramme.name !== epgUtil.DATA.PROGRAM_TITLE.NULL
                && selectedProgramme.name !== epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                if(selectedProgramme.startTime + selectedProgramme.duration <= now) {
                    /**
                     * 과거 프로그램
                     */
                    isShowReservation = false;
                }else {
                    if(now<=selectedProgramme.startTime) {
                        isShowReservation = true;
                    }else {
                        isShowReservation = false;
                    }
                }

                KTW.managers.service.EpgRelatedMenuManager.activateFullEpgProgrammeDetailPopup({
                    channel : options._this.getCurrentChannel(),
                    program : selectedProgramme,
                    is_show_reservation : isShowReservation,
                    full_epg_callback_func : function (btnType) {
                        cbFullEpgProgramDetail({btnType: btnType, _this: options._this});
                    }
                });

            }


        }
        else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
            // 4채널
            KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();
        }
        else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
            // 실시간 인기 채널
            KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();
        }
    }

    function cbFullEpgProgramDetail (options) {
        log.printDbg("cbFullEpgProgramDetail() btnType : " + options.btnType);
        KTW.managers.service.EpgRelatedMenuManager.deactivateFullEpgProgrammeDetailPopup();
        // 시청 btn : 0 , 예약 버튼 : 1 , 닫기 : 2
        if (options.btnType === 0) {
            setFocusChannelTune(options._this);
        }
        else if(options.btnType === 1) {
            var programmeList = reviseProgrammeList({
                _this: options._this,
                programmeList: options._this.getProgrammesSectionByTime(referenceTime)
            });
            var progList = programmeList[1];
            var selectedProgramme = progList[programmeIdx];

            reservationProgramme({
                programme: selectedProgramme,
                _this: options._this
            });
        }
    }

    function cbRelateMenuRight (options) {
        log.printDbg("cbRelateMenuRight()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if (options._this.menuId !== options.menuId) {
            var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id === options.menuId) {
                        return true;
                    }
                }
            })[0];

            if (epgMenu) {
                options._this.parent.jumpTargetEpgView({
                    targetMenu: epgMenu
                });
            }
        }
    }

    function isChannelListExist (_this) {
        log.printDbg("isChannelListExist()");

        var channelList = _this.getChannelList();
        return channelList && channelList.length > 0 ? true : false;
    }

    function cbChannelListUpdate (options) {
        log.printDbg("cbChannelListUpdate(" + options.listId + ")");

        if (options.listId === KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO
            || options.listId === KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FULL_EPG) {
            // 전체 채널
        }
        else if (options.listId === KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE
            || options.listId === KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE) {
            // 선호 채널

            if (options._this.menuId === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                if (isChannelListExist(options._this)) {
                    drawChannel(options._this);
                    drawPreview(options._this);
                    drawProgrammes(options._this);
                    setFocus(options._this);
               //     setPipChannel(options._this);
                }
                else {
                    options._this.parent.setFocusGuideMenu();
                    var favouriteEpgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                                return true;
                            }
                        }
                    })[0];
                    options._this.parent.setEpgView({menu: favouriteEpgMenu});
                    epgArea.css({visibility: "hidden"});
                    subArea.css({visibility: "hidden"});
                }
            }
            else {
                drawChannel(options._this);
                KTW.managers.service.PipManager.reDrawSubArea();
            }
        }
    }

    function checkProgrammeList (_this) {
        log.printDbg("checkProgrammeList()");

        var isExist = false;
        var programmeList = _this.getProgrammesList();

        for (var i = 0; i < programmeList.length; i++) {
            var progList = programmeList[i];
            if (progList.length > 1) {
                isExist = true;
                break;
            }
        }

        return isExist;
    }

    function isExitProgramme (programme) {
        var result = false;
        var now = (new Date()).getTime();

        if (programme) {
            var endTime = (programme.startTime + programme.duration) * 1000;

            if (now > endTime) {
                result = true;
            }
        }

        return result;
    }

    function initFocusProgrammeIdx (_this) {
        log.printDbg("initFocusProgrammeIdx()");

        programmeIdx = 0;

        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[1];

        //2017.05.17 sw.nam progList 가 없는 경우 에러가 나는것을 방지하기 위해 null 처리 추가
        if(progList) {
            for (var i = 0; i < progList.length; i++) {
                if (!isExitProgramme(progList[i])) {
                    programmeIdx = i;
                    break;
                }
            }
        }
    }

    function isFirstProgramme (_this) {
        log.printDbg("isFirstProgramme()");

        if (programmeIdx === 0) {
            return true;
        }

        var result = false;

        var programmeList = reviseProgrammeList({
            _this: _this,
            programmeList: _this.getProgrammesSectionByTime(referenceTime)
        });
        var progList = programmeList[1];
        var preProgramme = progList[programmeIdx - 1];

        if (isExitProgramme(preProgramme)) {
            result = true;
        }

        return result;
    }

    function epgDetailPopupListener(options) {
        // btnType : 0 (채널 시청), 1 (예약)
        if (options.btnType === 0) {
            var tuneCh = window.oipfAdapter.navAdapter.getChannelControl("main");
            tuneCh.changeChannel(options.channel);
        } else if (options.btnType === 1) {
            reservationProgramme({
                programme: options.program,
                _this: instance
            });
        } else {
            LayerManager.deactivateLayer({id: "KidsEpgDetailPopup"});
        }
    }

    function contextMenuListener(index, isFocusing) {
        contextMenu.close();
        if (isFocusing) {
            switch (index) {
                case 0:
                    var curChannel = instance.getCurrentChannel();
                    window.EPG_REL_MANAGER.favoriteChannelOnOff(curChannel);
                    drawChannelArea(instance);
                    break;
                case 1:
                    // OTS 자세히 버튼
                    var curChannel = instance.getCurrentChannel();
                    var programmeList = reviseProgrammeList({
                        _this: instance,
                        programmeList: instance.getProgrammesSectionByTime(referenceTime)
                    });
                    var progList = programmeList[1];
                    var programme = progList[programmeIdx];
                    KidsEpgRelatedMenuManager.activateFullEpgProgrammeDetailPopup({channel: curChannel, program: programme, is_show_reservation: false, btnType: 2}, epgDetailPopupListener);
                    break;
                case 2:
                    // 4채널
                    var targetChannel;
                    var channelController =  oipfAdapter.navAdapter.getChannelControl("main", true);

                    var result = MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === MenuDataManager.MENU_ID.FOUR_CHANNEL_VIEW) {
                                return true;
                            }
                        }
                    })[0];

                    var toBeJumpedChannelLocator = "";
                    if (result) {
                        toBeJumpedChannelLocator = result.locator;
                    } else {
                        toBeJumpedChannelLocator = CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE;
                    }

                    if (toBeJumpedChannelLocator !== null && toBeJumpedChannelLocator !== undefined) {
                        targetChannel = oipfAdapter.navAdapter.getChannelByTriplet(toBeJumpedChannelLocator);
                        channelController.changeChannel(targetChannel);
                    }
                    break;
                case 3:
                    // 실시간 인기 채널
                    LayerManager.activateLayer({
                        obj: {
                            id: Layer.ID.STITCHING,
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            linkage: true,
                            params: {
                                fromKey:true
                            }
                        },
                        visible: true,
                        cbActivate: function() {
                        }
                    });
                    break;
            }
        } else {
            var tmpObj;
            if (index < contextTopMenu.length) {
                tmpObj = contextTopMenu[index];
            } else if (index < contextTopMenu.length + contextMidMenu.children.length) {
                tmpObj = contextMidMenu.children[index - contextTopMenu.length];
            } else if (index < contextTopMenu.length + contextMidMenu.children.length + contextBottomMenu.length) {
                tmpObj = contextBottomMenu[index - (contextTopMenu.length + contextMidMenu.children.length)];
            }
            if (instance.menuId !== tmpObj.id) {
                // TODO 가이드 메뉴 가져오는 로직으로 수정
                var epgMenu = window.MenuDataManager.searchMenu({
                    menuData: window.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.id === tmpObj.id) {
                            return true;
                        }
                    }
                })[0];

                if (epgMenu) {
                    LayerManager.activateLayer({
                        obj: {
                            id: Layer.ID.CHANNEL_GUIDE,
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            params: {
                                menuId: tmpObj.id
                            }
                        },
                        visible: true,
                        clear_normal: true,
                        cbActivate: function() {}
                    });
                }
            }
        }
    }

    window.KidsEpgViewOTS = function(options) {
        window.epgView.call(this, options);
        menuId = options.menuId;
        kidsArea = options.kidsArea;
        this.create = function () {
            log.printDbg("create()");

            if (!isCreate) {
                contextMenu = new ContextMenu_EPG();
                createElement(this);
                isCreate = true;
            }
        };

        this.getView = function () {
            return div;
        };

        this.initData = function () {
            log.printDbg("initData()");

            // 채널 리스트, 프로그램 리스트, channelIdx 설정
            this.initEpgData();
            referenceTime = epgUtil.reviseDate(new Date());

            initFocusProgrammeIdx(this);
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");
            if(window.isEPGOpen) _$(".kidsHome.kidsHome_main #backgroundTitle").text("");
            if (!options || !options.resume) {
                // show
                // this.parent.setBGIcon(true);
                this.initEpgData();
                if (isChannelListExist(this)) {
                    // draw channel
                    drawChannel(this);

                    // draw preview
                    drawPreview(this);

                    startTimeUpdateTimer({
                        _this: this
                    });

                    epgArea.css({visibility: "inherit"});
                    subArea.css({visibility: ""});
                    indicator.setSize(Math.ceil(this.getChannelList().length/CHANNELS_PER_PAGE));
                }
                else {
                    epgArea.css({visibility: "hidden"});
                    subArea.css({visibility: "hidden"});
                    drawNowDim();
                }

                div.removeClass("focus");
                div.css({display: ""});
            }
            else {
                // TODO resume, pause 시 동작 확인해야함
                if (isChannelListExist(this)) {
                    startTimeUpdateTimer({
                        _this: this,
                        forceUpdate: true
                    });

                    if (div.hasClass("focus")) {


                        drawPipChannelInfo();
                    }
                }
            }
            indicator.setPos(this.getChannelPage());
            var _this = this;
            this.setCallbackChannelListUpdate(function (listId) {
                cbChannelListUpdate({
                    _this: _this,
                    listId: listId
                });
            });
            isLoaded = true;
        };

        this.isLoaded = function () {
            return isLoaded;
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            this.setCallbackChannelListUpdate(null);

            endTimeUpdateTimer();

           // endPip();

            if (!options || !options.pause) {
                // hide
                div.css({display: "none"});

                if (div.hasClass("focus")) {
                    this.blurred();
                }
            }
            else {
                // TODO resume, pause 시 동작 확인해야함
            }
        };

        this.focused = function () {
            log.printDbg("focus()");
            div.removeClass("blurred");
            indicator.blurred();
            window.oipfAdapter.basicAdapter.resizeScreen(window.CONSTANT.FULL_EPG_PIG_STYLE.LEFT, window.CONSTANT.FULL_EPG_PIG_STYLE.TOP, window.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH, window.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT);
            window.IframeManager.changeIframe(window.CONSTANT.FULL_EPG_PIG_STYLE.LEFT, window.CONSTANT.FULL_EPG_PIG_STYLE.TOP, window.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH, window.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT);

            //_$("#kidsSubHome_main_background #defaultBg").addClass("ots");
           /* _$("#kidsSubHome_main_background #defaultBg").addclass("clipping");*/

            // focused 시 좌 상단 백그라운드 타이틀 hidden
            _$("#KidsHomeMain #kidsBackground").css({ visibility: "hidden" });
            // 우상단 pip 영역
            _$(".kidsSubhome_background").addClass("clippingForKidsFullEPG");



            kidsArea.div.find(".clock_area").css("visibility", "hidden");

            // 2018.01.26 sw,nam
            // 키즈 채널 편성표의 백그라운드 위치 변경 필요  (뷰매니져 햇갈려서 따로 뺌..)2
            _$("#bg_kids").show();
            _$("#bg_2d_kids").hide();
            _$("#bg_kids_cloud_1").addClass("moved");
            _$("#bg_kids_cloud_2").addClass("moved");

            setTimeout(function() {kidsArea.div.find(".kidsCharacterShot_area").css("display", "none");}, 500);
            var bExist = isChannelListExist(this);

            if (bExist) {
           //     startPip(this);

                referenceTime = epgUtil.reviseDate(new Date());

                drawChannel(this);

                initFocusProgrammeIdx(this);

                // draw reference time
                drawReferenceTime();

                // draw programmes
                drawProgrammes(this);

                // subArea setting
                drawSubArea(this);

                isFocusEpgArea = true;
            }

            div.addClass("focus");

            relationArea.css({visibility: (window.KIDS_MODE_MANAGER.isKidsMode() ? "hidden":"inherit")});

            // [dj.son] drawScrollArea 와 drawNowDim 은 div 의 focus class 유무에 따라 visible 이 결정되므로, div 에 focus class 를 추가한 후 수행
            if (bExist) {
                drawNowDim();
                drawChannelName(this);
                setFocus(this);
            }

            previewBottomBg.css({visibility: "hidden"});
        };

        this.blurred = function (isCreated) {
            log.printDbg("blur()");
            div.addClass("blurred");

            kidsArea.div.find(".clock_area").css("visibility", "inherit");
            var bExist = isChannelListExist(this);


            // 2018.01.26 sw,nam
            // 키즈 채널 편성표의 백그라운드 위치 변경 필요  (뷰매니져 햇갈려서 따로 뺌..)
            _$("#bg_kids").hide();
            _$("#bg_2d_kids").show();
            _$("#bg_kids_cloud_1").removeClass("moved");
            _$("#bg_kids_cloud_2").removeClass("moved");


            _$("#KidsHomeMain #kidsBackground").css({ visibility: "inherit" });
            _$(".kidsSubhome_background").removeClass("clippingForKidsFullEPG");

      /*      kidsArea.div.find("#kidsDefaultBg").addClass("clipping");
            kidsArea.div.find("#pig_area").css("display", "block");*/
            kidsArea.div.find(".main_clock").css("display", "block");

           // endPip();

            //this.parent.setMainPipLocation({
            //    mode: KTW.ui.layer.ChannelGuideLayer.BG_MODE.PIP_DEFAULT
            //});
            // this.parent.setBGIcon(true);
            indicator.blurred();
            contextMenu.close();
            relationArea.css({visibility: "hidden"});
            if (!isCreated) {
                if (bExist) {
                    drawPreview(this);

                    isFocusEpgArea = true;
                }

                div.removeClass("focus");

                drawNowDim();
                drawChannelName(this);
            }

            previewBottomBg.css({visibility: "inherit"});
        };

        this.remove = function () {
            log.printDbg("remove()");
            this.setCallbackChannelListUpdate(null);

            endTimeUpdateTimer();
            this.blurred();
            _$("#kidsSubHome_main_background #defaultBg").removeClass("ots");
            kidsArea.div.find("#pig_dim").removeClass("ots");
            kidsArea.div.find(".clock_area").css("visibility", "inherit");
           // endPip();
            div.remove();
            window.isEPGOpen = false;
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.onKeyAction = function (keyCode) {
            log.printDbg("controlKey()");
            // if (contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);

            var consumed = false;

            switch (keyCode) {
                case KEY_CODE.UP:
                    keyUp(this);
                    indicator.setPos(this.getChannelPage());
                    consumed = true;
                    break;
                case KEY_CODE.DOWN:
                    keyDown(this);
                    indicator.setPos(this.getChannelPage());
                    consumed = true;
                    break;
                case KEY_CODE.LEFT:
                    consumed = keyLeft(this);
                    if (!consumed) {
                        setTimeout(function() {
                            window.oipfAdapter.basicAdapter.resizeScreen(0,0, window.CONSTANT.RESOLUTION.WIDTH, window.CONSTANT.RESOLUTION.HEIGHT);
                        }, 200);
                    }
                    break;
                case KEY_CODE.RIGHT:
                    keyRight(this);
                    consumed = true;
                    break;
                case KEY_CODE.OK:
                    keyOk(this);
                    consumed = true;
                    break;
                case KEY_CODE.CONTEXT:
                    if (KIDS_MODE_MANAGER.isKidsMode()) return true;
                    // showRelatedMenu(this);
                    instance = this;
                    var curChannel = this.getCurrentChannel();
                    curChannel.chNum = div.find(".full_epg_container .full_epg_item.focus .full_epg_channel_num").text();
                    curChannel.chName = div.find(".full_epg_container .full_epg_item.focus .full_epg_channel_name").text();

                    contextMenu.setCurrentChannel(curChannel);
                    contextMenu.setEventListener(contextMenuListener);
                    LayerManager.activateLayer({
                        obj: {
                            id: "KidsEpgRelatedMenuPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                contextM: contextMenu
                            }
                        },
                        moduleId: "module.kids",
                        new: true,
                        visible: true
                    });
                    // contextMenu.open();
                    consumed = true;
                    break;
                case KEY_CODE.BACK:

                    setTimeout(function() {
                        window.oipfAdapter.basicAdapter.resizeScreen(0,0, window.CONSTANT.RESOLUTION.WIDTH, window.CONSTANT.RESOLUTION.HEIGHT);
                    }, 200);
                    this.viewMgr.historyBack();
                    return true;
                case KEY_CODE.FULLEPG :
                case KEY_CODE.FULLEPG_OTS :
                    if (this.menuId === window.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                        LayerManager.clearNormalLayer();
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed(this);
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue(this);

            /**
             * sw.nam 2018.01.30
             * 편성표 핫키 입력 시 clearNormalLayer 호출[WEBIIIHOME-3641]
             */
                case KEY_CODE.FULLEPG:
                case KEY_CODE.FULLEPG_OTS :
                    LayerManager.clearNormalLayer();
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.getTitle = function () {
            return "";
        };

        this.isFullScreen = function () {
            return isChannelListExist(this);
        };

        this.isFocusAvailable = function () {
            var isAvailable = true;

            if (this.menuId === window.MenuDataManager.MENU_ID.UHD_CHANNEL
                || this.menuId === window.MenuDataManager.MENU_ID.SKYLIFE_UHD_CHANNEL) {
                isAvailable = this.isFullScreen();
            }

            return isAvailable;
        };

        this.getDataStatus = function () {
            if (isChannelListExist(this)) {
                return window.epgView.DATA_STATUS.AVAILABLE;
            }
            else {
                if (this.menuId === window.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                    return window.epgView.DATA_STATUS.NO_FAVOURITE;
                }

                if (KTW.CONSTANT.IS_OTS && this.menuId !== window.MenuDataManager.MENU_ID.OLLEH_TV_CHANNEL) {
                    return window.pgView.DATA_STATUS.WEAK_SIGNAL;
                }

                return window.epgView.DATA_STATUS.UPDATE;
            }
        };
        this.pause = function () {
            log.printDbg("pause()");
            this.blurred();
        };
        this.resume = function () {
            log.printDbg("resume()");
            this.focused();
        };
    };

    window.KidsEpgViewOTS.prototype = new window.epgView();
    window.KidsEpgViewOTS.prototype.constructor = window.KidsEpgViewOTS;
    window.KidsEpgViewOTS.prototype.type = View.TYPE.CONTENTS;
})();