/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>GenreChannelViewExtension</code>
 *
 * @author sw.nam
 * @since 2017-01-05
 * 장르별 채널 view implementation
 */

"use strict";

(function() {

    /* Constance */
    var NUMBER_OF_PREV_ITEM = 8;
    var NUMBER_OF_FOCUSED_ITEM = 5;
    var DEFAULT_FOCUSED_ITEM_IDX = 0;

    /* UtilityAPIs */
    var log = window.log;
    var util = window.UTIL;
    var epgUtil = window.epgUtil;
    var navAdapter = window.oipfAdapter.navAdapter;

    /* Elements*/
    var div = null;
    var itemGroup = null;
    var item =[];

    /* scroll*/
    var scrollArea = null;
    var scroll = null;

    /* subArea*/
    var subArea = null;
    var pageIconArea = null;

    /* focus */
    var focusBar = null;
    var focusProgramIdx = DEFAULT_FOCUSED_ITEM_IDX;

    /* Flags */
    var isCreated = false;
    var isFocusedAppArea = true;

    var pipTimer = null;

    var updateTimer = null;

    var menuId;
    var epgTitle;
    var indicator;
    var kidsArea;
    var instance;
    var contextMenu;
    var contextTopMenu = [];
    var contextMidMenu;
    var contextBottomMenu = [];
    var isLoaded = false;
    var totalPage = 0;
    var viewMgr;

    var previewBottomBg;

    function createElement (_this) {
        indicator = new Indicator(1);
        div = util.makeElement({
            tag: "<div />",
            attrs: {
                id: menuId,
                class: "genre_channel_view_extension_kids",
                css: {
                    visibility: "hidden"
                }
            },
            parent: _this.parent.div
        });

        itemGroup = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "itemGroup",
                css: {
                    position: "absolute", /*left: 540, top: 148, */overflow: "visible"
                    //"-webkit-transition": "transform 0.5s"
                }
            },
            parent: div

        });

        scrollArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "scroll",
                css: {
                    position: "absolute", left: 90, top: 122, overflow: "visible", visibility: "hidden"
                }
            },
            parent: div
        });

        scroll = new KTW.ui.component.Scroll({
            parentDiv: scrollArea,
            height: 182,
            forHeight: true
        });

        div.append(indicator.getView());
        subArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "sub_area",
                css:{
                    visibility: "hidden"
                }
            },
            parent: div
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: window.modulePath + "resource/image/icon_option_related.png",
                css: {
                    position: "absolute", left: 1789, top: 1001, width: 27, height: 22
                }
            },
            parent: subArea
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1823, top: 1000, "font-size": 26, color: "rgba(255, 255, 255, 0.3)",
                    "letter-spacing": -1.3
                }
            },
            text: "옵션",
            parent: subArea
        });

        pageIconArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "pageIcon_area",
                css: {
                    position: "absolute", left: 82, top: 903, width: 180
                }
            },
            parent: div
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: window.modulePath + "resource/image/icon_pageup.png",
                css: {
                    float: "left"
                }
            },
            parent: pageIconArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: window.modulePath + "resource/image/icon_pagedown.png",
                css: {
                    float: "left"
                }
            },
            parent: pageIconArea
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    float: "left", color: "white", "font-size": 24, opacity: 0.3, "margin-top": 33, "margin-left": -60, "font-family": "RixHead L"
                }
            },
            text: "페이지",
            parent: pageIconArea
        });

        for (var i =0; i < NUMBER_OF_PREV_ITEM; i++){
            var itemClass = "item";
            if (i > NUMBER_OF_FOCUSED_ITEM - 1) {
                itemClass += " full_screen_hide";
            }
            item[i] = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: itemClass,
                    css: {
                        overflow: "hidden"
                        //"-webkit-transition": "height 0.5s, opacity: 0.5s"
                    }
                },
                parent: itemGroup
            });

            var chArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "chArea",
                    css: {
                        float: "left"
                        //"-webkit-transition": "width 0.5s, height 0.5s, opacity: 0.5s"
                    }
                },
                parent: item[i]
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "icon",
                    css: {
                        position: "absolute", width: 22, height: 22
                    }
                },
                parent: chArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "chNum",
                    css: {
                        position: "absolute", "font-family": "RixHead B"
                    }
                },
                parent: chArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "chName",
                    css: {
                        position: "absolute", "margin-left": 113, "margin-top": 52, width: 130,
                        "font-size": 28, "letter-spacing": -1.4, "font-family": "RixHead L"
                    }
                },
                parent: chArea
            });

            var pipArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "pipArea",
                    css: {
                        float: "left"
                        //"-webkit-transition": "width 0.5s, height 0.5s, opacity: 0.5s"
                    }
                },
                parent: item[i]
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "thumbnail_default",
                    src: window.modulePath + "resource/image/default_thumb_kids.jpg",
                    css: {
                        position: "absolute"
                    }
                },
                parent: pipArea
            });

            var thumbnail = util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "thumbnail",
                    css: {
                        position: "absolute"
                    }
                },
                parent: pipArea
            });
            thumbnail.error(function () {
                _$(this).attr("src", "").css({display: "none"});
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "thumbnail_sdw",
                    src: "images/sdw_guide_thumb.png",
                    css: {
                        position: "absolute", width: 13, height: 127
                    }
                },
                parent: pipArea
            });


            for (var j = 0; j < 3; j++) {
                var programClass = "programArea";
                if (j > 0) {
                    programClass += " full_screen_show";
                }
                var programArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: programClass,
                        css: {
                            float: "left"
                            //"-webkit-transition": "width 0.5s, height 0.5s, opacity: 0.5s"
                        }
                    },
                    parent: item[i]
                });

                //2018.01.25 sw.nam
                //고도화 gui 수정 반영 - 프로그램 포커스 영역 추가
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "programAreaFocusDiv",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "programAreaFocusGreyDiv",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: programArea
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: window.modulePath + "resource/icon_book.png",
                        class: "reservationIcon",
                        css: {
                            position: "absolute", "margin-left": 39, "margin-top": 42
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programName",
                        css: {
                            position: "absolute", "white-space": "nowrap", height: 45
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programName_2",
                        css: {
                            position: "absolute", "margin-left": 27, "margin-top": 70, width: 375, height: 45,
                            "font-size": 36, "letter-spacing": -1.8, "font-family": "RixHead M",
                            "white-space": "nowrap", "text-overflow": "ellipsis", overflow: "hidden"
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "ageIcon",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: programArea
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programStartTime",
                        css: {
                            position: "absolute", "font-family": "RixHead L"
                        }
                    },
                    parent: programArea
                });

                var progressBar = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "entireProgressBar",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "dynamicProgressBar",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: progressBar
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programEndTime",
                        css: {
                            position: "absolute", "font-family": "RixHead L"
                        }
                    },
                    parent: programArea
                });
            }
        }


/*        focusBar = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "focusBar",
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible"
                }
            },
            parent: itemGroup
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: -7, width: 1722, height: 5
                }
            },
            parent: focusBar
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 160, width: 1722, height: 5
                }
            },
            parent: focusBar
        });*/
        // blur 이미지 추가
        previewBottomBg  = util.makeElement({
            tag: "<img />",
            attrs: {
                class: "dim_vodList_btm_kids",
                src: window.modulePath + "resource/image/dim_vodlist_btm_kids.png",
                css: {
                    position: "absolute", left: 577, top: 912, width: 1343, height: 168
                }
            },
            parent: div
        });
    }

    function settingContextMenuData() {
        var guideMenu = window.MenuDataManager.searchMenu({
            menuData: window.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id == window.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                    return true;
                }
            }
        })[0];
        var tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST);
        if (tmpObj) {
            contextMenu.addTitle("채널 편성표", "epgTop_title_0");
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id, "epgTop_title_0");
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.UHD_CHANNEL);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.AUDIO_CHANNEL);
        if (tmpObj) {
            contextTopMenu[contextTopMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id);
        }


        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.GENRE_CHANNEL);
        if (tmpObj) {
            contextMenu.addSeparateLine("epgTop");
            contextMidMenu = tmpObj;
            contextMenu.addTitle(contextMidMenu.name, "epgTop_title_1");
            for (var idx = 0 ; idx < contextMidMenu.children.length ; idx ++) {
                contextMenu.addButton(contextMidMenu.children[idx].name, contextMidMenu.children[idx].id, "epgTop_title_1");
            }
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL);
        if (tmpObj) {
            contextMenu.addSeparateLine("epgTop");
            contextMenu.addTitle("TV APP 채널", "epg_title_2");
            contextBottomMenu[contextBottomMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id, "epg_title_2");
        }

        tmpObj = getSearchMenuData(guideMenu, window.MenuDataManager.MENU_ID.TV_APP_CHANNEL);
        if (tmpObj) {
            if (contextBottomMenu.length == 0) {
                contextMenu.addSeparateLine("epgTop");
                contextMenu.addTitle("TV APP 채널", "epg_title_2");
            }
            contextBottomMenu[contextBottomMenu.length] = tmpObj;
            contextMenu.addButton(tmpObj.name, tmpObj.id, "epg_title_2");
        }
    }

    function getSearchMenuData(menuData, menuId) {
        var menuObj = null;
        if (menuData) {
            for (var idx = 0 ; idx < menuData.children.length ; idx ++) {
                if (menuData.children[idx].id === menuId) {
                    menuObj = menuData.children[idx];
                    break;
                }
            }
        }
        return menuObj;
    }

    function drawChannelArea (_this) {
        log.printDbg("drawPreviewChannelArea()");

        //현재 보여지는 channel list 받아오기.
        var curChannelList = getCurChannelList(_this); // 현재 채널이  0~4 사이에 들어오도록 , 채널리스트를 보여준다.

        var channel = null;
        var chArea = null;
        var icon = null;

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            channel = curChannelList[i];
            chArea = item[i].find(".chArea");

            if (channel) {
                var chNum = channel.data.majorChannel;
                chArea.find(".chNum").text(util.numToStr(chNum,3,null));

                chArea.find(".icon").css({visibility: "inherit"});
                if (navAdapter.isBlockedChannel(channel)) {
                    icon = window.modulePath + "resource/image/icon_block.png";
                }
                else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channel)) {
                    icon = window.modulePath + "resource/image/icon_fav.png"
                }
                else {
                    chArea.find(".icon").css({visibility: "hidden"});
                }
                chArea.find(".icon").attr({src: icon});

                item[i].css({visibility: ""});
            }
            else {
                item[i].css({visibility: "hidden"});
            }
        }

        drawChannelName(_this);
    }

    function drawChannelName (_this) {
        log.printDbg("drawChannelName()");

        //현재 보여지는 channel list 받아오기.
        var curChannelList = getCurChannelList(_this); // 현재 채널이  0~4 사이에 들어오도록 , 채널리스트를 보여준다.
        var channelIdx = getFocusChannelElementIdx(_this);

        var channel = null;
        var chArea = null;

        var isFocus = div.hasClass("focus");
        //var width = isFocus ? 230 : 200;
        //var fontSize = isFocus ? 30: 28;
        var cssObj;

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            if (isFocus) {
                cssObj = {
                    "margin-left": 68, "margin-top": 83, width: 230, height: 35,
                    "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.5)",
                    overflow: "hidden", "text-overflow": "ellipsis", "white-space": "nowrap"
                };
                if (i === channelIdx) {
                    cssObj.color = "rgba(255, 255, 255, 1)";
                }
            }
            else {
                cssObj = {
                    "margin-left": 51, "margin-top": 69, width: 200, height: 35,
                    "font-size": 27, "letter-spacing": -1.35, color: "rgba(255, 255, 255, 0.5)",
                    overflow: "hidden", "text-overflow": "ellipsis", "white-space": "nowrap"
                };
            }

            channel = curChannelList[i];
            chArea = item[i].find(".chArea");

            if (channel) {
                var chName = channel.data.name;

                //var arrChName = util.splitText(chName, "RixHead L", fontSize, width);
                //
                //if (arrChName.length > 1) {
                //    chName = "";
                //    for (var j = 0; j < arrChName.length; j++) {
                //        chName += arrChName[j] + "\n";
                //    }
                //    chName = chName.slice(0, chName.length - 1);
                //
                //    if (isFocus) {
                //        cssObj["margin-top"] = 52;
                //    }
                //    else {
                //        cssObj["margin-top"] = 40;
                //    }
                //    chArea.find(".chName").css({"margin-top": 40});
                //}

                chArea.find(".chName").css(cssObj).text(chName);
            }
        }
    }

    function drawThumbnail (_this) {
        log.printDbg("drawThumbnail()");

        //현재 보여지는 channel list 받아오기.
        //var curChannelList = getCurChannelList(_this); // 현재 채널이  0~4 사이에 들어오도록 , 채널리스트를 보여준다.
        var curProgramList  = _this.getPreviewProgrammesList();
        //var channel = null;
        var pipArea = null;

        if (curProgramList == null || curProgramList === undefined || curProgramList.length <= 0) {
            log.printDbg("drawThumbnail(), curProgram is not exist. so, return");
            return;
        }

        var length = curProgramList.length < NUMBER_OF_PREV_ITEM ? curProgramList.length : NUMBER_OF_PREV_ITEM;
        for (var i = 0; i < length; i++) {
            pipArea = item[i].find(".pipArea");
            //var channel = curChannelList[i];
            var thumbImagePath = "";

            //thumbImagePath = "http://image.ktipmedia.co.kr/channel/CH_" + channel.data.sid + ".png";

            // 2017.06.14 dhlee
            // AuthManager에서 제공하는 PR 값을 이용하도록 한다.
            var pr = window.AuthManager.getPR();
            //var pr = window.oipfAdapter.casAdapter.getParentalRating();
            // console.debug(KTW.utils.epgUtil.getAge(curProgramList[i]));
            // console.debug(pr);
            if (curProgramList[i].channel && curProgramList[i].channel.data) {
                if (navAdapter.isBlockedChannel(curProgramList[i].channel)) {
                    // blocked channel (시청 제한 채널)
                    thumbImagePath = window.modulePath + "resource/image/block_ch.png";
                }
                else if (curProgramList[i].channel.desc === 2) {
                    // 성인 채널
                    thumbImagePath = "images/iframe/block_adult_ch.png";
                }
                // 2017.06.14 dhlee
                // KidsMode인 경우 Framework 에서 PR을 set 하므로 구분해서 처리하지 않는다.
                // 혹시 문제가 발생된다면 Framework 파트에 문의
                //else if (pr !== 0 && KTW.utils.epgUtil.getAge(curProgramList[i]) >= (window.KIDS_MODE_MANAGER.isKidsMode()? 15 : pr)) {
                else if (pr !== 0 && window.epgUtil.getAge(curProgramList[i]) >= pr) {
                    // 연령 제한 프로그램
                    thumbImagePath = window.modulePath + "resource/image/block_age.png";
                }
                else {
                    // 2017.06.14 dhlee
                    // width와 height 를 지정하여 조회하도록 한다.
                    //thumbImagePath = "http://image.ktipmedia.co.kr/channel/CH_" + curProgramList[i].channel.data.sid + ".png";
                    thumbImagePath = "http://image.ktipmedia.co.kr/channel/CH_" + curProgramList[i].channel.data.sid + ".png" + "?w=" + 282 + "&h=" + 158 + "quality=90";
                }
            }

            pipArea.find(".thumbnail").css({display: ""}).attr({src: thumbImagePath, onerror: "this.src='" + modulePath + "resource/image/default_thumb_kids.jpg'"});
        }
    }

    function drawProgramme (options) {
        log.printDbg("drawProgramme()");

        var _this = options._this;
        var programme = options.programme;
        var progElement = options.progElement;
        var isFocus = options.isFocus;

        var currentTime = (new Date()).getTime();

        var nameCssObj = {
            "margin-left": 24, "margin-top": 32, width: 280, "font-size": "", "font-family": ""
        };
        var isReserve = false;

        if (programme) {
            var programName = programme.name;

            var startTime = programme.startTime * 1000;
            var programDuration = programme.duration * 1000;
            var endTime = startTime + programDuration;

            if (div.hasClass("focus") && !options.isPreview) {
                /* 프로그램 예약여부 확인 */
                var reserved = KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED;
                if (!options.isFirstProgramme) {
                    reserved = KTW.managers.service.ReservationManager.isReserved(programme);
                }

                if (reserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                    progElement.find(".reservationIcon").css({visibility: "hidden"});
                    isReserve = false;

                    nameCssObj["margin-left"] = 39;
                    nameCssObj["margin-top"] = 42;
                    nameCssObj["width"] = 350;
                }
                else {
                    if (reserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
                        progElement.find(".reservationIcon").attr("src", window.modulePath + "resource/icon_book_s.png").css({"margin-left": 39, "margin-top": 42, visibility: "inherit"});
                    }
                    else {
                        progElement.find(".reservationIcon").attr("src", window.modulePath + "resource/icon_book.png").css({"margin-left": 39, "margin-top": 42, visibility: "inherit"});
                    }

                    isReserve = true;

                    nameCssObj["margin-left"] = 79;
                    nameCssObj["margin-top"] = 42;
                    nameCssObj["width"] = 310;
                }

                if (isFocus) {
                    progElement.find(".reservationIcon").attr("src", window.modulePath + "resource/icon_book.png").css({"margin-left": 27, "margin-top": 23});

                    nameCssObj["margin-left"] = isReserve ? 67 : 27;
                    nameCssObj["margin-top"] = 23;
                    nameCssObj["width"] = isReserve ? 335 : 375;

                    nameCssObj["font-size"] = 36;
                    nameCssObj["font-family"] = "RixHead M";

                    var arrProgrammeName = util.splitText(programName, "RixHead M", 36, nameCssObj["width"], -1.8);
                    var ageIconCssObj = { "margin-top": 23, visibility: "inherit"} ;

                    if (arrProgrammeName.length > 1) {
                        progElement.find(".programName").text(arrProgrammeName[0]).css(nameCssObj);
                        if (arrProgrammeName.length > 2) {
                            arrProgrammeName[1] += arrProgrammeName[2];
                        }
                        progElement.find(".programName_2").text(arrProgrammeName[1]);

                        var name2Length = util.getTextLength(arrProgrammeName[1], "RixHead M", 36, -1.8);

                        ageIconCssObj["margin-left"] = name2Length > 375 ? 375 + 7 + 27 : name2Length + 7 + 27;
                        ageIconCssObj["margin-top"] = 70;
                    }
                    else {
                        progElement.find(".programName").text(programName).css(nameCssObj);
                        progElement.find(".programName_2").text("");

                        var nameLength = util.getTextLength(programName, "RixHead M", 36, -1.8);

                        ageIconCssObj["margin-left"] = nameCssObj["margin-left"] + nameLength + 7;
                    }

                    // 연령 아이콘
                    var age = KTW.utils.epgUtil.getAge(programme);
                    var ageImg;
                    if (age && age > 0) {
                        ageImg = window.modulePath + "resource/icon_age_list_" + age + ".png";//(isFocusedAppArea ? "_br.png" : ".png");

                    }
                    else if (age === 0) {
                        ageImg = window.modulePath + "resource/icon_age_list_all" + ".png";//(isFocusedAppArea ? "_br.png" : ".png");
                    }
                    else {
                        ageImg = "";
                    }
                    progElement.find(".ageIcon").attr({src: ageImg}).css(ageIconCssObj);
                }
                else {
                    progElement.find(".programName").text(programName).css(nameCssObj);
                    progElement.find(".programName_2").text("");
                    progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
                }
            }
            else {
                progElement.find(".reservationIcon").css({visibility: "hidden"});
                progElement.find(".programName").text(programName).css(nameCssObj);
                progElement.find(".programName_2").text("");
                progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
            }

            // 2017.05.01 dhlee
            // reviseType은 0 ~ 3 사이의 값이므로 program.reviseType 이 PROGRAM_REVISE_TYPE.NULL(0) 인 경우 false가 된다
            //if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
            if (programme.reviseType != undefined && programme.reviseType != null &&
                (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
                || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
                || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {
            //if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
            //    || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
            //    || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {

                progElement.find(".entireProgressBar").css({visibility: "hidden"});
                progElement.find(".programStartTime").text("");
                progElement.find(".programEndTime").text("");
            }
            else {
                if(startTime < currentTime && startTime + programDuration >= currentTime) {
                    progElement.find(".programStartTime").text(util.getTimeString(startTime));
                    progElement.find(".programEndTime").text(util.getTimeString(endTime));
                    var width = Math.floor(((currentTime - startTime) / programDuration) * progElement.find(".entireProgressBar").width());
                    progElement.find(".dynamicProgressBar").css({width: width});
                    progElement.find(".entireProgressBar").css({visibility: "inherit"});
                }
                else {
                    progElement.find(".entireProgressBar").css({visibility: "hidden"});
                    progElement.find(".programStartTime").text(util.getTimeString(startTime) + " - " + util.getTimeString(endTime));
                    progElement.find(".programEndTime").text("");
                }
            }
        }
        else {
            progElement.find(".reservationIcon").css({visibility: "hidden"});
            progElement.find(".programName").text("");
            progElement.find(".programName_2").text("");
            progElement.find(".programStartTime").text("");
            progElement.find(".programEndTime").text("");
            progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
            progElement.find(".entireProgressBar").css({visibility: "hidden"});
        }
    }

    function drawProgrammeList (options) {
        log.printDbg("drawProgrammeList()");

        var focusChannelIdx  = getFocusChannelElementIdx(options._this);
        var channelIdx  = options.channelIdx;
        var programList = options.programList;
        var arrProgElement = options.programArea;
        var programmeIdx = 0;

        if (options && options.programList) {
            if (channelIdx === focusChannelIdx) {
                if (focusProgramIdx % 2 === 1) {
                    programmeIdx = focusProgramIdx - 1;
                }
                else {
                    programmeIdx = focusProgramIdx;
                }
            }

            for (var j = 0; j < arrProgElement.length; j++) {
                var element = _$(arrProgElement[j]);
                var programme = programList[programmeIdx + j];

                drawProgramme({
                    _this: options._this,
                    programme: programme,
                    progElement: element,
                    isFocus: (indicator.isFocused() ? false : (channelIdx === focusChannelIdx && focusProgramIdx % 2 === j)),
                    isPreview: options.isPreview,
                    isFirstProgramme: focusProgramIdx === 0 && j === 0
                });
            }
        }
    }

    function drawProgramArea (_this) {
        log.printDbg("drawProgramArea()");

        var programList = _this.getProgrammesList();

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            drawProgrammeList({
                _this: _this,
                channelIdx: i,
                programList: programList[i],
                programArea: item[i].find(".programArea"),
                isPreview: i < NUMBER_OF_FOCUSED_ITEM ? false : true
            });
        }

/*
        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();
        var thisChannelProgramList = null;

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            thisChannelProgramList = programList[i];

            var arrProgElement = item[i].find(".programArea");
            var programmeIdx = 0;

            if (channelIdx === i) {
                if (focusProgramIdx % 2 === 1) {
                    programmeIdx = focusProgramIdx - 1;
                }
                else {
                    programmeIdx = focusProgramIdx;
                }
            }

            for (var j = 0; j < arrProgElement.length; j++) {
                var element = $(arrProgElement[j]);
                var programme = thisChannelProgramList[programmeIdx + j];

                if (i < NUMBER_OF_FOCUSED_ITEM || j === 0) {
                    drawProgramme({
                        _this: _this,
                        programme: programme,
                        progElement: element,
                        isFocus: channelIdx === i && focusProgramIdx % 2 === j,
                        isPreview: i < NUMBER_OF_FOCUSED_ITEM ? false : true
                    });
                }
            }
        }
        */
    };

    function drawFocusPrgramList (_this) {
        log.printDbg("drawFocusPrgramList()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            var isDraw = false;

            if (channelIdx === i) {
                isDraw = true;
            }
            else {
                if (item[i].hasClass("focus")) {
                    isDraw = true;
                }
            }

            if (isDraw) {
                drawProgrammeList({
                    _this: _this,
                    channelIdx: i,
                    programList: programList[i],
                    programArea: item[i].find(".programArea"),
                    isPreview: i < NUMBER_OF_FOCUSED_ITEM ? false : true
                });
            }
        }
    }

    //현재 channel LIst 를 받아온다.
    function getCurChannelList (_this) {
        log.printDbg("getCurChannelList()");

        var channelList = _this.getChannelList();
        var curPage = _this.getChannelPage();
        var startIdx = curPage * NUMBER_OF_FOCUSED_ITEM;

        return channelList.slice(startIdx, startIdx + NUMBER_OF_PREV_ITEM);
    }

    function getFocusChannelElementIdx (_this) {
        return _this.getChannelIdx() % NUMBER_OF_FOCUSED_ITEM;
    }

    function getFocusProgramElementIdx () {
        return focusProgramIdx % 2;
    }

    function setFocusArea () {
        log.printDbg("setFocusArea()");

        if (isFocusedAppArea) {
            // scroll.setFocus(false);
            itemGroup.addClass("focus");
        }
        else {
            // scroll.setFocus(true);
            itemGroup.removeClass("focus");
        }
    }

    function setFocusEpgArea (_this) {
        log.printDbg("setFocusEpgArea()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var arrProgramArea = item[channelIdx].find(".programArea");
        var focusProgramArea = _$(arrProgramArea[getFocusProgramElementIdx()]);

        itemGroup.children().removeClass("focus");
        item[channelIdx].addClass("focus");

       // focusBar.css({top: (channelIdx * 161)});

        itemGroup.find(".programArea").removeClass("focus");
        focusProgramArea.addClass("focus");
    }

    function isChannelListExist (_this) {
        log.printDbg("isChannelListExist()");

        var channelList = _this.getChannelList();
        return channelList && channelList.length > 0 ? true : false;
    };

    function isLastChannel (_this) {
        log.printDbg("isLastChannel()");

        var channelIdx = _this.getChannelIdx();
        var channelList = _this.getChannelList();
        var flag = false;

        if (channelIdx == channelList.length - 1)
            flag = true;

        return flag;
    };

    function getLastChannelIdx (_this) {
        log.printDbg("getLastChannelIdx");

        var channelList = _this.getChannelList();
        return channelList.length - 1;
    };

    function getCurrentProgrammeLength (_this) {
        log.printDbg("getCurrentProgrammeLength()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();
        var curProgramList = programList[channelIdx];

        return curProgramList.length;
    }

    function isLastPage (_this) {
        log.printDbg("isLastPage()");

        var curPage = _this.getChannelPage();
        var maxPage = _this.getChannelMaxPage();

        return curPage == maxPage-1 ? true : false;
    };

    function isLastProgram (_this) {
        log.printDbg("isLastProgram()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();
        var curProgramList = programList[channelIdx];

        return focusProgramIdx === curProgramList.length - 1 ? true : false;
    };

    function moveFocusChannel (options) {
        log.printDbg("moveFocusChannel(" + options.nextChannelIdx + ")");

        if (options.nextChannelIdx == null || options.nextChannelIdx == undefined) {
            log.printDbg("nextChannelIdx is null... so return");
            return;
        }

        changeChannelIdx(options);

        if (focusProgramIdx > 1) {
            focusProgramIdx = 0;
        }
        else {
            if (focusProgramIdx >= getCurrentProgrammeLength(options._this)) {
                focusProgramIdx = 0;
            }
        }

        if (options.pageUpdate) {
            drawChannelArea(options._this);
            drawThumbnail(options._this);
            drawProgramArea(options._this);
        }
        else {
            //if (options.programUpdate) {
            //    drawProgramArea(options._this);
            //}

            drawFocusPrgramList(options._this);
        }

        //drawProgramArea(options._this);

        // scroll.setCurrentPage(options._this.getChannelPage());
        setFocusEpgArea(options._this);
        setPipChannel(options._this);
    };

    function changeChannelIdx (options) {

        var channelIdx = options._this.getChannelIdx();
        var channelList = options._this.getChannelList();
        var length = channelList.length;

        if (options.nextChannelIdx == channelIdx) {
            log.printDbg("nextChannelIdx == current channel idx... so return");
            return;
        }
        if (options.nextChannelIdx < 0 || options.nextChannelIdx > length - 1) {
            log.printDbg("nextChannelIdx is out of range... so return");
            return;
        }

        var isUp = true;

        if ((options.nextChannelIdx > channelIdx && options.nextChannelIdx != length - 1)
            || (options.nextChannelIdx < channelIdx && options.nextChannelIdx == 0)) {
            isUp = false;
        }

        log.printDbg("isUp : " + isUp);

        options._this.changeChannelIdx({
            nextChannelIdx: options.nextChannelIdx,
            isProgrammesListUpdate: options.pageUpdate,
            isUp: isUp
        });

    }


    function showRelatedMenu (_this) {
        log.printDbg("showRelatedMenu()");

        var isShowFavoritedChannel = true;
        var isShowProgramDetail = false;
        var isShowTwoChannel = false;
        var isShowFourChannel = true;
        var isShowHitChannel = true;
        var isShowGenreOption = false;

        var focusOptionIndex = 0;

        var focusMenuId = _this.menuId;//KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST;

        var epgrelatedmenuManager = KTW.managers.service.EpgRelatedMenuManager;

        var curChannel = _this.getCurrentChannel();

        if(navAdapter.isAudioChannel(curChannel) === true) {
            isShowFavoritedChannel = false;
            isShowProgramDetail = false;
        }


        epgrelatedmenuManager.showFullEpgRelatedMenu(curChannel, function (menuIndex) {
            cbRelateMenuLeft({
                _this: _this,
                menuIndex: menuIndex
            });
        }, isShowFavoritedChannel, isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel, isShowGenreOption, focusOptionIndex, function (menuId) {
            cbRelateMenuRight({
                _this: _this,
                menuId: menuId
            });
        }, focusMenuId);
    }

    function cbRelateMenuLeft (options) {
        log.printDbg("cbRelateMenuLeft()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
            // 선호채널 등록 / 해제

            var curChannel = options._this.getCurrentChannel();
            KTW.managers.service.EpgRelatedMenuManager.favoriteChannelOnOff(curChannel,function () {
                cbFavoriteChannelOnOff(options._this);
            });

        }
        else if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
            /**
             * OTS 자세히 버튼이므로 나오지 않을 상황임.
             */
        }
        else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
            // 4채널
            KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();
        }
        else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
            // 실시간 인기 채널
            KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();
        }
    }

    function cbRelateMenuRight (options) {
        log.printDbg("cbRelateMenuRight()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if (options._this.menuId !== options.menuId) {
            // TODO 가이드 메뉴 가져오는 로직으로 수정
            var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id === options.menuId) {
                        return true;
                    }
                }
            })[0];

            if (epgMenu) {
                options._this.parent.jumpTargetEpgView({
                    targetMenu: epgMenu
                });
            }
        }
    }
    /* PIP implementation*/

    function startPip (_this) {
        log.printDbg("startPip()");

        var pipType = KTW.managers.service.PipManager.PIP_TYPE.FULL_EPG;

        if (pipTimer) {
            clearTimeout(pipTimer);
        }

        pipTimer = setTimeout(function () {
            KTW.managers.service.PipManager.activatePipLayer({pipType: pipType});
            setPipChannel(_this);

            pipTimer = null;
        }, 700);
    }

    function setPipChannel (_this) {
        log.printDbg("setPipChannel()");
        // 2017.06.14 dhlee
        // 기존 로직 그대로 사용하면 됨
        // pr check 는 pip layer 에서 알아서 하므로 그대로 사용하면 됨
        if (!KTW.managers.service.PipManager.isPipLayerShow()) {
             return;
        }

        var channelIdx  = _this.getChannelIdx() % NUMBER_OF_FOCUSED_ITEM;
        //KTW.managers.service.PipManager.setPipSize({ left: 536, top: 138 + (channelIdx * 161), width: 282, height: 158.625 });
        KTW.managers.service.PipManager.setPipSize({ left: 511, top: 123 + (channelIdx * 161), width: 282, height: 158.625 });

        KTW.managers.service.PipManager.changeChannel(_this.getCurrentChannel());

        /*
        var pr = window.oipfAdapter.casAdapter.getParentalRating();
        var channelProIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();
        var curProgramList = programList[channelProIdx];
        var selectedProgram = curProgramList[0];

        if (window.epgUtil.getAge(selectedProgram) >= pr) {
            if (!KTW.managers.service.PipManager.isPipLayerShow()) {
                return;
            }
            var channelIdx  = _this.getChannelIdx() % NUMBER_OF_FOCUSED_ITEM;
            KTW.managers.service.PipManager.setPipSize({ left: 536, top: 139 + (channelIdx * 161), width: 0, height: 0 });

            KTW.managers.service.PipManager.changeChannel(_this.getCurrentChannel());
        } else {
            if (!KTW.managers.service.PipManager.isPipLayerShow()) {
                return;
            }
            var channelIdx  = _this.getChannelIdx() % NUMBER_OF_FOCUSED_ITEM;
            KTW.managers.service.PipManager.setPipSize({ left: 536, top: 139 + (channelIdx * 161), width: 282, height: 158.625 });

            KTW.managers.service.PipManager.changeChannel(_this.getCurrentChannel());
        }
        */
    }

    function endPip () {
        log.printDbg("endPip()");

        if (pipTimer) {
            clearTimeout(pipTimer);
            pipTimer = null;
        }

        KTW.managers.service.PipManager.deactivatePipLayer();
    }

    function cbFavoriteChannelOnOff (_this){
        log.printDbg("cbFavoriteChannelOnOff()");
        drawChannelArea(_this);
    };

    function keyLeft (_this) {
        log.printDbg("keyLeft()");

        var consume = false;
        var focusElementIdx = getFocusProgramElementIdx();

        // 2018.01.29 sw.nam
        // 스크롤 영역은 가지 않는다
        isFocusedAppArea = true;
        if (isFocusedAppArea) {
            if (focusProgramIdx > 0) {
                --focusProgramIdx;
                // if (focusElementIdx === 0) {
                //    drawProgramArea(_this);
                // }
                // else {
                //    drawFocusPrgramList(_this);
                // }

                drawFocusPrgramList(_this);

                setFocusEpgArea(_this);
                consume = true;
            }
            else {
                if (!viewMgr.getParent().isJumpMenu()) {
                    /*
                if (Math.ceil(_this.getChannelList().length/5) == 1) {
                    // 1페이지 이하인 경우
                    consume = false;
                } else {

                    return false;
                  *//*  indicator.focused();
                    consume = true;*//*
                }*/
                    consume = false ;
                    isFocusedAppArea = false;
                    setFocusArea();
                    drawFocusPrgramList(_this);
                    drawProgramArea(_this);
                    setFocusEpgArea(_this);
                }
            }
        }

        return consume;
    }

    function keyRight (_this) {
        log.printDbg("keyRight()");

        var focusElementIdx = getFocusProgramElementIdx();

        isFocusedAppArea = true;
        if (isFocusedAppArea) {
            if (!isLastProgram(_this)) {
                ++focusProgramIdx;
                //if (focusElementIdx === 1) {
                //    drawProgramArea(_this);
                //}
                //else {
                //    drawFocusPrgramList(_this);
                //}

                drawFocusPrgramList(_this);
                setFocusEpgArea(_this);
            }
        }
        else {
            indicator.blurred();
            isFocusedAppArea = true;
            setFocusArea();

            drawFocusPrgramList(_this);
            setFocusEpgArea(_this);
        }
        return true;
    }

    function keyUp (_this) {
        log.printDbg("keyUp");

        var channelIdx = _this.getChannelIdx();
        var curPage = _this.getChannelPage();
        var nextChannelIdx = channelIdx - 1;
        var pageUpdate = false;
        var programUpdate = false;

        if (isFocusedAppArea) {
            if (channelIdx % NUMBER_OF_FOCUSED_ITEM > 0) {
                if (focusProgramIdx > 1) {
                    programUpdate = true;
                }
            }
            else {
                if (channelIdx === 0) {
                    nextChannelIdx = getLastChannelIdx(_this);
                }
                pageUpdate = true;
            }
        }
        else {
            if (curPage > 0) {
                nextChannelIdx = curPage * NUMBER_OF_FOCUSED_ITEM - 1;
            }
            else {
                nextChannelIdx = getLastChannelIdx(_this);
            }
            pageUpdate = true;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyRed (_this, isPage) {
        log.printDbg("keyRed");

        var channelIdx = _this.getChannelIdx();
        var curPage = _this.getChannelPage();
        var nextChannelIdx = channelIdx%5 === 0 ? (curPage === 0 ? (totalPage-1)*NUMBER_OF_FOCUSED_ITEM : (curPage-1)*NUMBER_OF_FOCUSED_ITEM) : (curPage * NUMBER_OF_FOCUSED_ITEM);
        var pageUpdate = false;
        var programUpdate = false;
        if (isPage) {
            if (isFocusedAppArea) {
                if (channelIdx % NUMBER_OF_FOCUSED_ITEM > 0) {
                    if (focusProgramIdx > 1) {
                        programUpdate = true;
                    }
                }
                else {
                    if (channelIdx === 0) {
                        nextChannelIdx = (curPage === 0 ? (totalPage - 1) * NUMBER_OF_FOCUSED_ITEM : (curPage - 1) * NUMBER_OF_FOCUSED_ITEM);
                    }
                    pageUpdate = true;
                }
            }
            else {
                nextChannelIdx = curPage * NUMBER_OF_FOCUSED_ITEM;
                indicator.blurred();
                isFocusedAppArea = true;
                setFocusArea();
            }
        } else {
            nextChannelIdx = 0;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyDown(_this) {
        log.printDbg("keyDown");

        var channelIdx = _this.getChannelIdx();
        var lastChannelIdx = getLastChannelIdx(_this);
        var curPage = _this.getChannelPage();
        var nextChannelIdx = channelIdx + 1;
        var pageUpdate = false;
        var programUpdate = false;

        if (isFocusedAppArea) {
            if (channelIdx < lastChannelIdx && nextChannelIdx % NUMBER_OF_FOCUSED_ITEM > 0) {
                if (focusProgramIdx > 1) {
                    programUpdate = true;
                }
            }
            else {
                if (channelIdx === lastChannelIdx) {
                    nextChannelIdx = 0;
                }
                pageUpdate = true;
            }
        }
        else {
            if (isLastPage(_this)) {
                nextChannelIdx = 0;
            }
            else {
                nextChannelIdx = (curPage + 1) * NUMBER_OF_FOCUSED_ITEM;
            }
            pageUpdate = true;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyBlue(_this, isPage) {
        log.printDbg("keyBlue");

        var channelIdx = _this.getChannelIdx();
        var lastChannelIdx = getLastChannelIdx(_this);
        var curPage = _this.getChannelPage();
        var nextChannelIdx = channelIdx%5 === 4 ? (curPage+1) === (totalPage-1) ? lastChannelIdx : (curPage + 1) * NUMBER_OF_FOCUSED_ITEM+4 : curPage === (totalPage-1) ? lastChannelIdx : (curPage*NUMBER_OF_FOCUSED_ITEM+4);
        var pageUpdate = false;
        var programUpdate = false;

        if (isPage) {
            if (isFocusedAppArea) {
                if (channelIdx < lastChannelIdx && channelIdx % 5 !== 4) {
                    if (focusProgramIdx > 1) {
                        programUpdate = true;
                    }
                }
                else {
                    if (channelIdx === lastChannelIdx) {
                        nextChannelIdx = 4;
                    }
                    pageUpdate = true;
                }
            }
            else {
                if (isLastPage(_this)) {
                    nextChannelIdx = lastChannelIdx;
                }
                else {
                    nextChannelIdx = curPage * NUMBER_OF_FOCUSED_ITEM + 4;
                }
                indicator.blurred();
                isFocusedAppArea = true;
                setFocusArea();
            }
        } else {
            nextChannelIdx = lastChannelIdx;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyOk (_this) {
        log.printDbg("keyOk()");

        if (isFocusedAppArea) {
            if (focusProgramIdx === 0) {
                setFocusChannelTune(_this);
            }
            else {
                var channelIdx  = getFocusChannelElementIdx(_this);
                var programList = _this.getProgrammesList();
                var curProgramList = programList[channelIdx];
                var selectedProgram = curProgramList[focusProgramIdx];

                reservationProgramme({
                    _this: _this,
                    program: selectedProgram
                });
            }
        }
        else {
            indicator.blurred();
            isFocusedAppArea = true;
            setFocusArea();

            drawFocusPrgramList(_this);
            setFocusEpgArea(_this);
        }
    }

    function setFocusChannelTune (_this) {
        log.printDbg("setFocusChannelTune()");

        var channelList = _this.getChannelList();
        var channelIdx = _this.getChannelIdx();
        var focusChannel = channelList[channelIdx];
        //var pr = window.oipfAdapter.casAdapter.getParentalRating();
        //var channelProIdx = getFocusChannelElementIdx(_this);
        //var programList = _this.getProgrammesList();
        //var curProgramList = programList[channelProIdx];
        //var selectedProgram = curProgramList[focusProgramIdx];

        if (focusChannel) {
            if (window.stateManager.isVODPlayingState()) {
                window.ModuleManager.getModuleForced("module.vod", function (vodModule) {
                    if (vodModule) {
                        vodModule.execute({
                            method: "stopVodWithChannelSelection",
                            params: {
                                channelObj: focusChannel
                            }
                        });
                    }
                });
            } else {
                var mainControl = window.oipfAdapter.navAdapter.getChannelControl("main");
                mainControl.changeChannel(focusChannel, false);
            }
        }
    }

    function reservationProgramme (options) {
        log.printDbg("reservationProgramme()");

        if (!options || !options.program) {
            log.printDbg("programme is null.... so return");
            return;
        }

        KTW.managers.service.ReservationManager.add(options.program, true, function (result) {
            var RESULT = KTW.managers.service.ReservationManager.ADD_RESULT;
            if (result === RESULT.SUCCESS_RESERVE || result === RESULT.SUCCESS_CANCEL
                || result === RESULT.SUCCESS_SERIES_RESERVE || result === RESULT.SUCCESS_SERIES_CANCEL
                || result === RESULT.DUPLICATE_RESERVE) {
                drawProgramArea(options._this);
            }
        });
    }


    function startTimeUpdateTimer (_this) {
        log.printDbg("startTimeUpdateTimer()");

        if (!updateTimer) {
            updateTimer = timeUpdateTimer(_this);
        }
    }

    function timeUpdateTimer (_this) {
        return setTimeout(function () {

            checkUpdateProgramList(_this);
            drawProgramArea(_this);

            updateTimer = timeUpdateTimer(_this);
        }, 60 * 1000);
    }

    function endTimeUpdateTimer () {
        log.printDbg("endTimeUpdateTimer()");

        if (updateTimer) {
            clearTimeout(updateTimer);
            updateTimer = null;
        }
    }

    function checkUpdateProgramList (_this) {
        var programList = _this.getProgrammesList();
        var length = programList.length;
        var now = (new Date()).getTime();

        for (var i = 0; i < length; i++) {
            var programmeList = programList[i];
            var curProgramList = programmeList[0];

            if (curProgramList) {
                var endTime = (curProgramList.startTime + curProgramList.duration) * 1000;

                if (endTime < now) {
                    _this.getProgrammesList(true);
                    break;
                }
            }
        }
    }

    function checkProgrammeList (_this) {
        log.printDbg("checkProgrammeList()");

        var isExist = false;
        var programList = _this.getProgrammesList();

        if (programList && programList.length > 0) {
            for (var i = 0; i < programList.length; i++) {
                var progList = programList[i];
                if (progList && progList.length > 1) {
                    isExist = true;
                    break;
                }
            }
        }

        return isExist;
    }

    function contextMenuListener(index, isFocusing) {
        contextMenu.close();
        LayerManager.deactivateLayer({
            id: "KidsEpgRelatedMenuPopup"
        });
        if (isFocusing) {
            switch (index) {
                case 0:
                    var curChannel = instance.getCurrentChannel();
                    window.EPG_REL_MANAGER.favoriteChannelOnOff(curChannel);
                    drawChannelArea(instance);
                    break;
                case 1:
                    // 실시간 인기 채널
                    window.EPG_REL_MANAGER.activateRealTimeChannel();
                    break;
                case 2:
                    // 4채널
                    window.EPG_REL_MANAGER.activateFourChannel();
                    break;
            }
        } else {
            var tmpObj;
            if (index < contextTopMenu.length) {
                tmpObj = contextTopMenu[index];
            } else if (index < contextTopMenu.length + contextMidMenu.children.length) {
                tmpObj = contextMidMenu.children[index - contextTopMenu.length];
            } else if (index < contextTopMenu.length + contextMidMenu.children.length + contextBottomMenu.length) {
                tmpObj = contextBottomMenu[index - (contextTopMenu.length + contextMidMenu.children.length)];
            }
            if (instance.menuId !== tmpObj.id) {
                // TODO 가이드 메뉴 가져오는 로직으로 수정
                var epgMenu = window.MenuDataManager.searchMenu({
                    menuData: window.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.id === tmpObj.id) {
                            return true;
                        }
                    }
                })[0];

                if (epgMenu) {
                    LayerManager.activateLayer({
                        obj: {
                            id: Layer.ID.CHANNEL_GUIDE,
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            params: {
                                menuId: tmpObj.id
                            }
                        },
                        visible: true,
                        clear_normal: true,
                        cbActivate: function() {}
                    });
                }
            }
        }
    }

    window.KidsEpgViewOTV = function(options) {
        window.epgView.call(this, options);
        epgTitle = HTool.getMenuName(options.categoryData);
        menuId = options.menuId;
        kidsArea = options.kidsArea;
        viewMgr = options.viewManager;

        this.curMenu = window.MenuDataManager.searchMenu({
            menuData: window.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === options.menuId) {
                    return true;
                }
            }
        })[0];

        this.create = function() {
            log.printDbg("create()");
            isLoaded = false;
            if (!isCreated) {
                contextMenu = new ContextMenu_EPG();
                createElement(this);
                isCreated = true;
            }
        };

        this.getView = function () {
            return div;
        };

        // this.initData = function () {
        //     this.initEpgData();
        // };

        this.show = function (options) {
            log.printDbg("show()");

            div.css({visibility: "inherit"});



            if (!options || !options.resume) {
                this.initEpgData();
                // this.parent.setBGIcon(true);
                drawChannelArea(this);
                drawThumbnail(this);
                drawProgramArea(this);

                setFocusEpgArea(this);
                totalPage = Math.ceil(this.getChannelList().length/NUMBER_OF_FOCUSED_ITEM);
                indicator.setSize(totalPage);
                if (contextMenu.getMenuListLength() == 0 && !KIDS_MODE_MANAGER.isKidsMode()) {
                    settingContextMenuData();
                }
            }
            else {
                // Test
                // this.focused();
            }
            isLoaded = true;
            startTimeUpdateTimer(this);
            if (window.closeTimeout) {
                clearTimeout(window.closeTimeout);
                window.closeTimeout = null;
            }
            if(window.isEPGOpen) _$(".kidsHome.kidsHome_main #backgroundTitle").text(epgTitle);
            indicator.setPos(this.getChannelPage());
        };

        this.isLoaded = function() {
            return isLoaded;
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            div.css({visibility: "hidden"});

            if (!options || !options.pause) {
                this.blurred();
            }
            else {
            }
            contextMenu.close();
            endPip();

            endTimeUpdateTimer();
        };

        this.focus = function () {
            log.printDbg("origin focus()");
        };

        this.focused = function () {
            log.printDbg("focus()");
            if (totalPage > 1) pageIconArea.css("visibility", "inherit");
            _$("#kidsSubHome_main_background #defaultBg").removeClass("clipping");

            // 2018.01.26 sw,nam
            // 키즈 채널 편성표의 백그라운드 위치 변경 필요  (뷰매니져 햇갈려서 따로 뺌..)2
            _$("#bg_kids").show();
            _$("#bg_2d_kids").hide();
            _$("#bg_kids_cloud_1").addClass("moved");
            _$("#bg_kids_cloud_2").addClass("moved");


        div.removeClass("blurred");
            div.addClass("focus");

            kidsArea.div.find("#kidsDefaultBg").removeClass("clipping");
            kidsArea.div.find("#pig_area").css("display", "none");
            kidsArea.div.find(".main_clock").css("display", "none");
            setTimeout(function() {kidsArea.div.find(".kidsCharacterShot_area").css("display", "none");}, 500);

            div.addClass("focus");
            focusProgramIdx = 0;
            drawChannelName(this);
            drawProgramArea(this);

            subArea.css({visibility: (window.KIDS_MODE_MANAGER.isKidsMode() ? "hidden":"inherit")});

            isFocusedAppArea = true;
            setFocusArea();
            indicator.blurred();

            previewBottomBg.css({visibility: "hidden"});

            startPip(this);
        };

        this.blurred = function (isCreate) {
            log.printDbg("blur()");
            pageIconArea.css("visibility", "hidden");
            _$("#kidsSubHome_main_background #defaultBg").addClass("clipping");

            // 2018.01.26 sw,nam
            // 키즈 채널 편성표의 백그라운드 위치 변경 필요  (뷰매니져 햇갈려서 따로 뺌..)
            _$("#bg_kids").hide();
            _$("#bg_2d_kids").show();
            _$("#bg_kids_cloud_1").removeClass("moved");
            _$("#bg_kids_cloud_2").removeClass("moved");

            div.addClass("blurred");
            div.removeClass("focus");

            kidsArea.div.find("#kidsDefaultBg").addClass("clipping");
            kidsArea.div.find("#pig_area").css("display", "block");
            kidsArea.div.find(".main_clock").css("display", "block");
            // kidsArea.div.find(".kidsCharacterShot_area").css("display", "block");

            div.removeClass("focus");

            focusProgramIdx = 0;
            if (!isCreate) {
                setFocusEpgArea(this);

                drawChannelName(this);
            }
            // drawProgramArea(this);
            contextMenu.close();
            subArea.css({visibility: "hidden"});
            indicator.blurred();

            isFocusedAppArea = true;

            previewBottomBg.css({visibility: "inherit"});

            endPip();
        };

        this.remove = function () {
            log.printDbg("remove()");
            contextMenu.close();
            pageIconArea.css("visibility", "hidden");
            endTimeUpdateTimer();
            _$("#kidsSubHome_main_background #defaultBg").addClass("clipping");
            kidsArea.div.find("#kidsDefaultBg").addClass("clipping");
            kidsArea.div.find("#pig_area").css("display", "block");
            kidsArea.div.find(".main_clock").css("display", "block");
            // kidsArea.div.find(".kidsCharacterShot_area").css("display", "block");
            endPip();
            div.remove();
            window.isEPGOpen = false;
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.onKeyAction = function (keyCode) {
            log.printDbg("controlKey()");
            var consumed = false;

            switch (keyCode) {
                case KEY_CODE.UP:
                    keyUp(this);
                    indicator.setPos(this.getChannelPage());
                    consumed = true;
                    break;
                case KEY_CODE.DOWN:
                    keyDown(this);
                    indicator.setPos(this.getChannelPage());
                    consumed = true;
                    break;
                case KEY_CODE.RED:
                    if (totalPage > 1) {
                        keyRed(this, true);
                        indicator.setPos(this.getChannelPage());
                    } else {
                        keyRed(this, false);
                    }
                    consumed = true;
                    break;
                case KEY_CODE.BLUE:
                    if (totalPage > 1) {
                        keyBlue(this, true);
                        indicator.setPos(this.getChannelPage());
                    } else {
                        keyBlue(this, false);
                    }
                    consumed = true;
                    break;
                case KEY_CODE.LEFT:
                    consumed = keyLeft(this);
                    if (window.isEPGOpen) consumed = true;
                    break;
                case KEY_CODE.RIGHT:
                    keyRight(this);
                    consumed = true;
                    break;
                case KEY_CODE.OK:
                    keyOk(this);
                    consumed = true;
                    break;
                case KEY_CODE.BACK:
                    if (!viewMgr.getParent().isJumpMenu()) {
                        this.viewMgr.historyBack();
                    }
                    else {
                        LayerManager.historyBack();
                    }
                    return true;
                    break;
                case KEY_CODE.CONTEXT:
                    if (!window.KIDS_MODE_MANAGER.isKidsMode()) {
                        var curChannel = this.getCurrentChannel();
                        instance = this;
                        var tmpText = div.find(".itemGroup .item.focus .chArea .chNum").text();
                        curChannel.chNum = div.find(".itemGroup .item.focus .chArea .chNum").text();
                        curChannel.chName = div.find(".itemGroup .item.focus .chArea .chName").text();
                        contextMenu.setCurrentChannel(curChannel);
                        contextMenu.setEventListener(contextMenuListener);
                        LayerManager.activateLayer({
                            obj: {
                                id: "KidsEpgRelatedMenuPopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    contextM: contextMenu
                                }
                            },
                            moduleId: "module.kids",
                            new: true,
                            visible: true
                        });

                        // contextMenu.open();
                    }
                    consumed = true;
                    break;

            /**
             * sw.nam 2018.01.30
             * 편성표 핫키 입력 시 clearNormalLayer 호출 [WEBIIIHOME-3641]
             */
                case KEY_CODE.FULLEPG:
                case KEY_CODE.FULLEPG_OTS :
                    LayerManager.clearNormalLayer();
                    consumed = true;
                    break;


            }

            return consumed;
        };

        this.getTitle = function () {
            return epgTitle;
        };

        this.isFullScreen = function () {
            return isChannelListExist(this);
        };
        this.isFocusAvailable = function () {
            return isChannelListExist(this);
        };

        this.getDataStatus = function () {
            if (!isChannelListExist(this)) {
                return window.epgView.DATA_STATUS.UPDATE;
            }
            else {
                return window.epgView.DATA_STATUS.AVAILABLE;
                //if (!checkProgrammeList(this)) {
                //    return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
                //}
                //else {
                //    return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
                //}
            }
        };
        this.pause = function () {
            log.printDbg("pause()");
            endPip();
        };
        this.resume = function () {
            log.printDbg("resume()");
            this.focused();
        };
    };

    window.KidsEpgViewOTV.prototype = new window.epgView();
    window.KidsEpgViewOTV.prototype.constructor = window.KidsEpgViewOTV;
    window.KidsEpgViewOTV.prototype.type = View.TYPE.CONTENTS;

})();