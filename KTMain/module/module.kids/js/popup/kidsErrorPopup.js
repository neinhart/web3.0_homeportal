/**
 * Created by Yun on 2017-07-03.
 */

(function() {
    var kids_error_popup = function kidsErrorPopup (options) {
        Layer.call(this, options);
        var _this = this, div, errorData, buttonLength = 0, curIdx = 0;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame kidsPopup kids_error");
            this.div.append(_$("<div/>", {class: "errorPopup"}));
            div = this.div.find(".errorPopup");
            div.append(_$("<span/>", {class: "errorTitle"}));
            div.append(_$("<span/>", {class: "errorText"}));
            div.append(_$("<div/>", {class: "button_area"}));
            cbCreate(true);
        };

        function makeButton() {
            buttonLength = 0;
            div.find(".button_area").empty();
            if (errorData.reboot) {
                buttonLength++;
                div.find(".button_area").append(_$("<div class='button'><div class='buttonText'>재부팅</div></div>"));
            }
            if (errorData.button) {
                buttonLength++;
                div.find(".button_area").append(_$("<div class='button'><div class='buttonText'>" + errorData.button + "</div></div>"));
            }
        }

        function makePopup() {
            var errMsg = "";
            var errorLength = errorData.message.length;
            for (var i = 0; i < errorLength; i++) {
                if (i == 0) errMsg += errorData.message[i];
                else errMsg += "<br>" + errorData.message[i];
            }
            div.find(".errorTitle").text(errorData.title);
            div.find(".errorText").html(errMsg);
        }

        function setFocus(_idx) {
            if (buttonLength > 0) {
                div.find(".button").removeClass("focus");
                div.find(".button").eq(_idx != null ? _idx : curIdx).addClass("focus");
            }
        }

        this.show = function() {
            Layer.prototype.show.call(this);
            errorData = _this.getParams();
            makePopup();
            makeButton();
            setFocus(0);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    setFocus(curIdx = HTool.indexMinus(curIdx, buttonLength));
                    return true;
                case KEY_CODE.RIGHT:
                    setFocus(curIdx = HTool.indexPlus(curIdx, buttonLength));
                    return true;
                case KEY_CODE.ENTER:
                    if (errorData.reboot && curIdx == 0) {
                        //TODO 재부팅
                    } else {
                        if (errorData.callback) errorData.callback();
                        LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    if (errorData.callback) errorData.callback();
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    kids_error_popup.prototype = new Layer();
    kids_error_popup.prototype.constructor = kids_error_popup;

    kids_error_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kids_error_popup.prototype.handleKeyEvent = function(key_code) {
        this.controlKey(key_code);
        return true;
    };

    arrLayer["kidsErrorPopup"] = kids_error_popup;
})();