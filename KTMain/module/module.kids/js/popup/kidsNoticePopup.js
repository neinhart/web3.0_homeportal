/**
 * Created by Yun on 2017-04-06.
 */

(function () {
    var kids_notice_popup = function kidsNoticePopup(options) {
        Layer.call(this, options);
        var _this = this, data, closeTimeout;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame kids kids_notice_popup");
            this.div.append(_$("<div/>", {class: "kids_notice_img_area"}));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            data = _this.getParams();
            _this.div.find(".kids_notice_img_area").addClass(data.isTimeCheck ? "time_check" : "count_check");
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            } else closeTimeout = setTimeout(function() {
                LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }, 10000);
        };

        this.hide = function() {
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            Layer.prototype.hide.call(this);
        };
    };

    kids_notice_popup.prototype = new Layer();
    kids_notice_popup.prototype.constructor = kids_notice_popup;

    kids_notice_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kids_notice_popup.prototype.handleKeyEvent = function() {
        return false;
    };

    arrLayer["KidsNotice"] = kids_notice_popup;
})();