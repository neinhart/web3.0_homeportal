/**
 * Created by Yun on 2017-02-17.
 */

(function () {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.kidsCharacterSetting = function kidsCharacterSettingPopup(options) {
        Layer.call(this, options);
        var div, btnText = ["저장", "취소"];
        var rdoText = ["ON", "OFF"];

        var focusIdx = 0;
        var focusRow = 0;
        var selectIdx = [0, 1];
        var directCnt = 0;
        var params = null;
        var charData = null;
        var selData = null;
        var isSelect = false;


        this.init = function (cbCreate) {
            div = this.div;
            params = this.getParams();
            charData = params.data;
            selData = KidsCharacterManager.getSelectCharacterData();
            selectIdx[1] = 1;
            isSelect = false;
            for (var idx = 0 ; idx < charData.length ; idx ++) {
                if (charData[idx].characterId == selData.characterId) {
                    selectIdx[1] = idx;
                    isSelect = true;
                    break;
                }
            }
            div.addClass("arrange_frame kidsPopup gniPopup kidsCharacterSetting");
            div.append(_$("<img/>", {class: "kidsPopup_decoration_img", src: modulePath + "resource/image/img_kidsmode_set_pop.png"}));
            div.append(_$("<div/>", {class: "kidsPopup_title"}).text("캐릭터 설정"));
            div.append(_$("<div/>", {class: "kidsPopup_subTitle text_white_center_42M"}).html("선택한 캐릭터가 시청제한 설정에 맞춰<br>TV를 자연스럽게 끌 수 있도록 도와 드립니다"));
            div.append(_$("<div/>", {class: "kidsPopup_alertTitle text_gray_center_27L"}).html("Tip! 리모컨의 별(*) 키를 누르시면<br>캐릭터가 나와서 부모님 대신 TV를 끌 수 있게 도와줍니다"));
            div.append(_$("<div/>", {class: "kidsPopup_bar_area"}));
            div.append(_$("<div/>", {class: "twoRdoBtn_area"}));
            div.find(".twoRdoBtn_area").append(_$("<span/>", {class: "rdoBtn_title"}).text("캐릭터 사용"));
            for (var idx = 0; idx < 2; idx++) {
                div.find(".twoRdoBtn_area").append(_$("<div/>", {class: "rdoBtn"}));
                div.find(".twoRdoBtn_area .rdoBtn").eq(idx).append(_$("<img/>", {
                    class: "rdoBtn_bg",
                    src: modulePath + "resource/image/pop_btn_kids_w284.png"
                }));
                div.find(".twoRdoBtn_area .rdoBtn").eq(idx).append(_$("<img/>", {
                    class: "rdoBtn_img",
                    src: modulePath + "resource/image/rdo_btn_d.png"
                }));
                div.find(".twoRdoBtn_area .rdoBtn").eq(idx).append(_$("<span/>", {class: "rdoBtn_text"}).text(rdoText[idx]));
            }
            div.append(_$("<div/>", {class: "kidsCharacterList_area"}));
            div.find(".kidsCharacterList_area").append(_$("<img/>", {class: "kidsCharacterList_arwLeft_img", src:modulePath + "resource/image/arw_l.png"}));
            // for (var idx = 0 ; idx < 3; idx ++) {
            div.find(".kidsCharacterList_area").append(_$("<div/>", {class:"kidsCharacter_div"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(0).append(_$("<img/>", {class:"kidsCharacter_sdw_img", src:modulePath + "resource/image/set_cha_sdw.png"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(0).append(_$("<img/>", {class:"kidsCharacter_img", src:params.data[(selectIdx[1]-1 < 0 ? params.data.length-1:selectIdx[1]-1)].imgUrl}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(0).append(_$("<img/>", {class:"kidsCharacter_select_img", src:modulePath + "resource/image/set_kids_character_check.png"}));
            div.find(".kidsCharacterList_area").append(_$("<div/>", {class:"kidsCharacter_div"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(1).append(_$("<img/>", {class:"kidsCharacter_sdw_img", src:modulePath + "resource/image/set_cha_sdw.png"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(1).append(_$("<img/>", {class:"kidsCharacter_img", src:params.data[selectIdx[1]].imgUrl}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(1).append(_$("<img/>", {class:"kidsCharacter_select_img", src:modulePath + "resource/image/set_kids_character_check_f.png"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(1).append(_$("<div/>", {class:"kidsCharacterName_text"}).text(params.data[selectIdx[1]].characterName));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(1).append(_$("<div/>", {class:"kidsCharacterList_index"}).text(selectIdx[1] + "/" + params.data.length));
            div.find(".kidsCharacterList_area").append(_$("<div/>", {class:"kidsCharacter_div"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(2).append(_$("<img/>", {class:"kidsCharacter_sdw_img", src:modulePath + "resource/image/set_cha_sdw.png"}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(2).append(_$("<img/>", {class:"kidsCharacter_img", src:params.data[(selectIdx[1]+1 > params.data.length-1 ? 0:selectIdx[1]+1)].imgUrl}));
            div.find(".kidsCharacterList_area .kidsCharacter_div").eq(2).append(_$("<img/>", {class:"kidsCharacter_select_img", src:modulePath + "resource/image/set_kids_character_check.png"}));
            // }
            div.find(".kidsCharacterList_area").append(_$("<img/>", {class:"kidsCharacterList_selector_img", src:modulePath + "resource/image/set_kids_character_sel.png"}));
            div.find(".kidsCharacterList_area").append(_$("<img/>", {class: "kidsCharacterList_arwRight_img", src:modulePath + "resource/image/arw_r.png"}));

            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            if (isSelect) {
                div.find(".kidsCharacterList_area .kidsCharacter_div:eq(1) .kidsCharacter_select_img").addClass("select");
            }
            cbCreate(true);
        };

        this.show = function () {
            params = this.getParams();
            if (KidsCharacterManager.getIsSelectCharacter()) {
                focusIdx = 0;
                selectIdx[0] = 0;
            } else {
                focusIdx = 1;
                selectIdx[0] = 1;
            }
            focusRow = 0;
            buttonFocusRefresh(focusIdx, focusRow);
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    directCnt = -1;
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, -1, (focusRow == 1 ? params.data.length : 2)), focusRow);
                    return true;
                case KEY_CODE.RIGHT:
                    directCnt = 1;
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, 1, (focusRow == 1 ? params.data.length : 2)), focusRow);
                    return true;
                case KEY_CODE.UP:
                    switch (focusRow){
                        case 0:
                            focusRow = 2;
                            focusIdx = 0; // 무조건 저장으로 포커스 가도록
                            break;
                        case 1 :
                            div.find(".kidsCharacterList_area .kidsCharacter_div .kidsCharacter_select_img").removeClass("select");
                            selectIdx[1] = focusIdx;
                            focusIdx = selectIdx[0];
                            focusRow = 0;
                            break;
                        case 2 :
                            if (selectIdx[0] == 1){
                                focusIdx = selectIdx[0];
                                focusRow = 0;
                            }else{
                                focusIdx = selectIdx[1];
                                focusRow = 1;
                            }
                            break;
                    }
                    buttonFocusRefresh(focusIdx, focusRow);
                    return true;
                case KEY_CODE.DOWN:
                    switch (focusRow){
                        case 0:
                            if(selectIdx[0] == 0){
                                focusRow = 1;
                                focusIdx = selectIdx[1];
                            }else{
                                focusRow = 2;
                                focusIdx = 0; // 무조건 저장으로 포커스 설정
                            }
                            break;
                        case 1 :
                            div.find(".kidsCharacterList_area .kidsCharacter_div .kidsCharacter_select_img").removeClass("select");
                            selectIdx[1] = focusIdx;
                            focusIdx = 0; // 무조건 저장으로 포커스 설정
                            focusRow = 2;
                            break;
                        case 2 :
                            focusRow = 0;
                            focusIdx = selectIdx[0];
                            break;
                    }
                    buttonFocusRefresh(focusIdx, focusRow);
                    return true;
                case KEY_CODE.ENTER:
                    switch (focusRow){
                        case 0:
                            selectIdx[0] = focusIdx; // 사용여부 값 저장
                            focusRow = (focusIdx == 0)? 1 : 2;
                            focusIdx = (focusRow == 1)? selectIdx[1] : 0; // 이전 선택 캐릭터 인덱스로 설정
                            buttonFocusRefresh(focusIdx, focusRow);
                            break;
                        case 1:
                            div.find(".kidsCharacterList_area .kidsCharacter_div .kidsCharacter_select_img").removeClass("select");
                            selectIdx[1] = focusIdx;
                            focusIdx = 0;
                            focusRow = 2;
                            buttonFocusRefresh(focusIdx, focusRow);
                            break;
                        case 2:
                            if (focusIdx == 0) {
                                if (selectIdx[0] == 0) {
                                    LayerManager.startLoading({preventKey: true});
                                    KidsCharacterManager.setKidsSelectCharacter(params.data[selectIdx[1]].characterId, function(data, result){
                                        LayerManager.stopLoading();
                                        if (result) {
                                            if (data.resultCd == 0) {
                                                KidsCharacterManager.setSelectCharacterData(params.data[selectIdx[1]]);
                                                KidsCharacterManager.setIsSelectCharacter(true);
                                            } else {
                                                KidsCharacterManager.setSelectCharacterData("");
                                                KidsCharacterManager.setIsSelectCharacter(false);
                                            }
                                            params.complete();
                                            LayerManager.historyBack();
                                        } else {
                                            LayerManager.historyBack();
                                            LayerManager.activateLayer({
                                                obj: {
                                                    id: "kidsErrorPopup",
                                                    type: Layer.TYPE.POPUP,
                                                    priority: Layer.PRIORITY.POPUP,
                                                    linkage: true,
                                                    params: {
                                                        title: "오류",
                                                        message: ["네트워크 연결이 올바르지 않습니다", "인터넷 연결을 확인해 주세요"],
                                                        button: "닫기",
                                                        reboot: false
                                                        // callback: _error
                                                    }
                                                },
                                                moduleId: "module.kids",
                                                visible: true
                                            });
                                        }
                                    });
                                } else {
                                    LayerManager.startLoading({preventKey: true});
                                    KidsCharacterManager.setKidsSelectCharacter("", function(data, result){
                                        LayerManager.stopLoading();
                                        if (result) {
                                            KidsCharacterManager.setIsCallCharacterInfo(true);
                                            KidsCharacterManager.setIsSelectCharacter(false);
                                            params.complete();
                                            LayerManager.historyBack();
                                        } else {
                                            LayerManager.historyBack();
                                            LayerManager.activateLayer({
                                                obj: {
                                                    id: "kidsErrorPopup",
                                                    type: Layer.TYPE.POPUP,
                                                    priority: Layer.PRIORITY.POPUP,
                                                    linkage: true,
                                                    params: {
                                                        title: "오류",
                                                        message: ["네트워크 연결이 올바르지 않습니다", "인터넷 연결을 확인해 주세요"],
                                                        button: "닫기",
                                                        reboot: false
                                                        // callback: _error
                                                    }
                                                },
                                                moduleId: "module.kids",
                                                visible: true
                                            });
                                        }
                                    });
                                }
                            } else {
                                LayerManager.historyBack();
                            }
                            break;
                    }
                    return true;
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index, rowIndex) {
            div.find(".twoRdoBtn_area .rdoBtn").removeClass("focus");
            div.find(".kidsCharacterList_area").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoRdoBtn_area .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_d.png");
            div.find(".twoRdoBtn_area .rdoBtn:eq(" + selectIdx[0] + ") .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_select_d.png");
            switch (rowIndex) {
                case 0:
                    div.find(".twoRdoBtn_area .rdoBtn:eq(" + index + ")").addClass("focus");
                    drawCheckIconOnCharacter(selectIdx[1]);
                    break;
                case 1:
                    div.find(".kidsCharacterList_area .kidsCharacter_div .kidsCharacter_select_img").removeClass("select");
                    div.find(".kidsCharacterList_area").addClass("focus");

                    // Display Left
                    div.find(".kidsCharacterList_area .kidsCharacter_div:eq(0) .kidsCharacter_img").attr("src", params.data[(index-1 < 0 ? params.data.length-1:index-1)].imgUrl);

                    // Display Center(Focued)
                    div.find(".kidsCharacterList_area .kidsCharacter_div:eq(1) .kidsCharacter_img").attr("src", params.data[index].imgUrl);

                    // Display Right
                    div.find(".kidsCharacterList_area .kidsCharacter_div:eq(2) .kidsCharacter_img").attr("src", params.data[(index+1 > params.data.length-1 ? 0:index+1)].imgUrl);

                    // Display Name & Idx
                    div.find(".kidsCharacterList_area .kidsCharacter_div:eq(1) .kidsCharacterName_text").text(params.data[index].characterName);
                    div.find(".kidsCharacterList_area .kidsCharacter_div:eq(1) .kidsCharacterList_index").text((index+1) + "/" + params.data.length);
                    drawCheckIconOnCharacter(index);
                    break;
                case 2:
                    div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
                    drawCheckIconOnCharacter(selectIdx[1]);
                    break;
            }


        }

        function drawCheckIconOnCharacter(index) {
            if ((index-1 < 0 ? params.data.length-1:index-1) == selectIdx[1]) div.find(".kidsCharacterList_area .kidsCharacter_div:eq(0) .kidsCharacter_select_img").addClass("select");
            if (index == selectIdx[1]) div.find(".kidsCharacterList_area .kidsCharacter_div:eq(1) .kidsCharacter_select_img").addClass("select");
            if ((index+1 > params.data.length-1 ? 0:index+1) == selectIdx[1]) div.find(".kidsCharacterList_area .kidsCharacter_div:eq(2) .kidsCharacter_select_img").addClass("select");

        }
    };

    kidsSubHome.popup.kidsCharacterSetting.prototype = new Layer();
    kidsSubHome.popup.kidsCharacterSetting.prototype.constructor = kidsSubHome.popup.kidsCharacterSetting;

    kidsSubHome.popup.kidsCharacterSetting.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsSubHome.popup.kidsCharacterSetting.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsCharacterSettingPopup"] = kidsSubHome.popup.kidsCharacterSetting;
})();