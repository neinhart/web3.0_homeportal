/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.home_editModeDelete = function editModeDeleteLayer(options) {
        var div, data, btnText = ["확인", "취소"], focusIdx = 0;
        Layer.call(this, options);

        function setFocus() {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame kidsPopup gniPopup editModeDelete");
            div.append(_$("<span/>", {class: "kidsPopup_title"}).text("선택항목 삭제"));
            div.append(_$("<span/>", {class: "text_white_center_45M"}));
            div.append(_$("<span/>", {class: "text_white_center_30L"}));
            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            focusIdx = 0;
            setFocus();
            log.printDbg(data);
            if (data.type == 0) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].itemName + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].itemName + "'");
                div.find(".text_white_center_30L").text("찜한 목록에서 삭제합니다");
            }
            else if (data.type == 1) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].title + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].title + "'");
                div.find(".text_white_center_30L").text("최근 시청 목록에서 삭제합니다");
            }
            else if (data.type == 2) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].title + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].title + "'");
                div.find(".text_white_center_30L").text("구매 목록에서 삭제합니다");
            }
            else if (data.type == 3) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].title + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].title + "'");
                div.find(".text_white_center_30L").text("'" + data.listName + "'에서 삭제합니다");
            }
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    focusIdx = HTool.indexMinus(focusIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    focusIdx = HTool.indexPlus(focusIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    data.callback(focusIdx == 0);
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    data.callback(false);
                    return true;
            }
        };
    };

    kidsSubHome.popup.home_editModeDelete.prototype = new Layer();
    kidsSubHome.popup.home_editModeDelete.prototype.constructor = kidsSubHome.popup.home_editModeDelete;

    kidsSubHome.popup.home_editModeDelete.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsSubHome.popup.home_editModeDelete.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsEditModeDeletePopup"] = kidsSubHome.popup.home_editModeDelete;
})();