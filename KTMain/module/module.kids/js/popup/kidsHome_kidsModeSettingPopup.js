/**
 * Created by Yun on 2017-02-20.
 */

(function () {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.kidsModeSetting = function kidsModeSettingPopup(options) {
        Layer.call(this, options);
        var div, btnText = ["저장", "취소"];
        var rdoText = ["ON", "OFF"];

        var focusIdx = 0;
        var focusRow = 0;
        var selectIdx = 0;

        this.init = function (cbCreate) {
            div = this.div;
            div.addClass("arrange_frame kidsPopup gniPopup kidsModeSetting");
            div.append(_$("<img/>", {class: "kidsPopup_decoration_img", src: modulePath + "resource/image/img_kidsmode_set_pop.png"}));
            div.append(_$("<div/>", {class: "kidsPopup_title"}).text("키즈모드 설정"));
            div.append(_$("<div/>", {class: "kidsPopup_subTitle_0 text_white_center_33M"}).html("키즈모드로 설정하시면 키즈/교육 채널과 콘텐츠만 이용할 수 있습니다"));
            div.append(_$("<div/>", {class: "kidsPopup_subTitle_1 text_gray_center_26M"}).html("※ 일반 설정은 동작하지 않습니다"));
            div.append(_$("<div/>", {class: "twoRdoBtn_area_0"}));
            div.find(".twoRdoBtn_area_0").append(_$("<span/>", {class: "rdoBtn_title"}).text("키즈모드 사용"));
            for (var idx = 0; idx < 2; idx++) {
                div.find(".twoRdoBtn_area_0").append(_$("<div/>", {class: "rdoBtn"}));
                div.find(".twoRdoBtn_area_0 .rdoBtn").eq(idx).append(_$("<img/>", {
                    class: "rdoBtn_bg",
                    src: modulePath + "resource/image/pop_btn_kids_w284.png"
                }));
                div.find(".twoRdoBtn_area_0 .rdoBtn").eq(idx).append(_$("<img/>", {
                    class: "rdoBtn_img",
                    src: modulePath + "resource/image/rdo_btn_d.png"
                }));
                div.find(".twoRdoBtn_area_0 .rdoBtn").eq(idx).append(_$("<span/>", {class: "rdoBtn_text"}).text(rdoText[idx]));
            }

            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }

            cbCreate(true);
        };

        this.show = function () {
            selectIdx = (KIDS_MODE_MANAGER.isKidsMode() ? 0 : 1);
            focusIdx = selectIdx;
            focusRow = 0;
            buttonFocusRefresh(focusIdx, focusRow);
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    focusRow = HTool.getIndex(focusRow, -1, 2);
                    focusIdx = (focusRow == 1)? 0 : selectIdx;
                    buttonFocusRefresh(focusIdx, focusRow);
                    return true;
                case KEY_CODE.DOWN:

                    focusRow = HTool.getIndex(focusRow, 1, 2);
                    focusIdx = (focusRow == 1)? 0 : selectIdx;
                    buttonFocusRefresh(focusIdx, focusRow);
                    return true;
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, -1, 2), focusRow);
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, 1, 2), focusRow);
                    return true;
                case KEY_CODE.CONTEXT :
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 1) {
                        if (focusIdx == 0) {
                            if (selectIdx == 0) {
                                if (!window.KIDS_MODE_MANAGER.isKidsMode()) {
                                    // KIDS_MODE_MANAGER.changeMode({
                                    //     kidsMode: window.OIPF_DEF.KIDS_MODE.ON,
                                    //     startKidsMode: window.OIPF_DEF.KIDS_MODE.ON
                                    // });
                                    LayerManager.historyBack();
                                    LayerManager.activateLayer({
                                        obj: {
                                            id: "KidsInputPasswordPopup",
                                            type: Layer.TYPE.POPUP,
                                            priority: Layer.PRIORITY.POPUP,
                                            params: {
                                                isKidsOn: true
                                            }
                                        },
                                        moduleId: "module.kids",
                                        new: true,
                                        visible: true
                                    });
                                } else {
                                    LayerManager.historyBack();
                                }
                            } else {
                                LayerManager.historyBack();
                                if (window.KIDS_MODE_MANAGER.isKidsMode()) {
                                    LayerManager.activateLayer({
                                        obj: {
                                            id: "KidsModeOffPopup",
                                            type: Layer.TYPE.POPUP,
                                            priority: Layer.PRIORITY.POPUP,
                                            params: {
                                            }
                                        },
                                        moduleId: "module.kids",
                                        new: true,
                                        visible: true
                                    });
                                }
                            }
                        } else {
                            LayerManager.historyBack();
                        }
                    } else {
                        selectIdx = focusIdx;
                        focusRow = 1;
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, focusRow);
                    }
                    return true;
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index, rowIndex) {
            div.find(".twoRdoBtn_area_0 .rdoBtn").removeClass("focus");
            div.find(".twoRdoBtn_area_0 .rdoBtn .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_d.png");
            div.find(".twoRdoBtn_area_0 .rdoBtn:eq(" + selectIdx + ") .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_select_d.png");
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            switch (rowIndex) {
                case 0:
                    div.find(".twoRdoBtn_area_0 .rdoBtn:eq(" + index + ")").addClass("focus");
                    if (index == 1 || selectIdx == 1) {
                        div.find(".kidsPopup_subTitle_1").addClass("disable");
                        div.find(".twoRdoBtn_area_1").addClass("disable");
                    }
                    break;
                case 1:
                    div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
                    if (selectIdx == 1) {
                        div.find(".kidsPopup_subTitle_1").addClass("disable");
                        div.find(".twoRdoBtn_area_1").addClass("disable");
                    }
                    break;
            }
        }
    };

    kidsSubHome.popup.kidsModeSetting.prototype = new Layer();
    kidsSubHome.popup.kidsModeSetting.prototype.constructor = kidsSubHome.popup.kidsModeSetting;

    kidsSubHome.popup.kidsModeSetting.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsSubHome.popup.kidsModeSetting.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code)
    };

    arrLayer["KidsModeSettingPopup"] = kidsSubHome.popup.kidsModeSetting;

})();