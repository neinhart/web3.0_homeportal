/**
 * Created by Yun on 2017-04-07.
 */

(function () {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.kidsBootingCheck = function kidsBootingCheckPopup(options) {
        Layer.call(this, options);
        var div, passwordCnt = 0;
        var focusRow = 0;
        var params;
        var authMgr = window.AuthManager;
        var wrongCnt = 0;
        var speechAdapter = oipfAdapter.speechRecognizerAdapter;

        this.init = function (cbCreate) {
            params = this.getParams();
            div = this.div;
            div.addClass("arrange_frame kidsPopup gniPopup kidsBootingCheck");
            createComponent();
            window.KidsCharacterManager.getKidsInfo(function(data, res) {
                if (res) {
                    if (data.data != null && data.data.imgUrl) {
                        window.KidsCharacterManager.setSelectCharacterData(data.data);
                        div.find(".popup_Character_img").attr("src", data.data.endImgUrl);
                    } else {
                        window.KidsCharacterManager.setSelectCharacterData("");
                        div.find(".popup_Character_img").attr("src", window.modulePath + "resource/image/character_default.png");
                    }
                } else {
                    window.KidsCharacterManager.setSelectCharacterData("");
                    div.find(".popup_Character_img").attr("src", window.modulePath + "resource/image/character_default.png");
                }
                cbCreate(true);
            });
        };

        function createComponent() {
            div.append(_$("<img/>", {class:"popup_Character_BG", src: window.modulePath + "resource/image/character_shine.png"}));
            div.append(_$("<img/>", {class:"popup_Character_img"}));
            div.append(_$("<div/>", {class: "kidsPopup_subTitle text_white_center_42M"}).html("키즈모드 시청제한 설정으로 종료된 경우<br>비밀번호를 입력하셔야 이용이 가능합니다"));
            div.append(_$("<div/>", {class: "kidsPopup_infoText text_gray_center_27L"}).html("olleh tv 비밀번호(성인인증)를 입력해 주세요"));
            div.append(_$("<div/>", {class: "kidsPopup_inputPasswordBG"}));
            div.append(_$("<div/>", {class: "passwordArea"}));
            div.find(".passwordArea").append(_$("<span/>", {class: "password"}).text("* * * *"));
            div.find(".passwordArea").append(_$("<span/>", {class: "password fill"}));
        }

        this.show = function () {
            focusRow = 0;
            wrongCnt = 0;
            passwordCnt = 0;
            authMgr.initPW();
            authMgr.resetPWCheckCount();
            window.isKidsPopupOpened = true;
            window.homeImpl.get(window.homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).navAdapter.stopAV();
            // AppServiceManager.changeService({nextServiceState: CONSTANT.SERVICE_STATE.KIDS_RESTRICTION});
            deleteNumber();
            speechAdapter.start(true);
            speechAdapter.addSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            speechAdapter.stop();
            speechAdapter.removeSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.DEL:
                    if (passwordCnt > 0) {
                        passwordCnt--;
                        deleteNumber();
                    }
                    return true;
                default:
                    if (keyCode >= KEY_CODE.NUM_0 && keyCode <= KEY_CODE.NUM_9 && focusRow == 0) {
                        if (passwordCnt < 4) passwordCnt ++;
                        fillPassword(keyCode - KEY_CODE.NUM_0);
                    }
                    return true;
            }
        };

        function deleteNumber() {
            authMgr.deletePW();
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
        }

        function getStarText(_passwordCnt) {
            var starText = "";
            if (_passwordCnt > 0) {
                for (var idx = 0 ; idx < _passwordCnt ; idx ++) {
                    if (_passwordCnt == 4 && idx == 3) starText += "*";
                    else starText += "* ";
                }
            }
            return starText;
        }

        function inputSpeechNumber (rec, input, mode) {
            if (rec && mode === oipfDef.SR_MODE.PIN) {
                passwordCnt = input.length > 4 ? 4 : input.length;
                div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
                if (authMgr.replacePW(input, 4)) checkPassword();
                log.printInfo("inputSpeechNumber::" + input);
            }
        }

        function fillPassword(num) {
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
            if (authMgr.inputPW(num, 4)) checkPassword();
        }

        function checkPassword() {
            authMgr.checkUserPW({
                type: window.AuthManager.AUTH_TYPE.AUTH_ADULT_PIN,
                callback: function(responseCode) {
                    log.printDbg("checkUserPW ResponseCode === " + responseCode);
                    if (responseCode == window.AuthManager.RESPONSE.CAS_AUTH_SUCCESS) {
                        window.homeImpl.get(window.homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).navAdapter.startAV();
                        var tmpKeyValue = KIDS_MODE_MANAGER.getKidsModeRestrictionKey();
                        if (tmpKeyValue == "true") {
                            window.KidsTimerCheckingManager.settingLimitWatchingText(null);
                            window.KidsTimerCheckingManager.resetTimer();
                            var tmp = window.ModuleManager.getModule("module.vod");
                            if (!tmp) {
                                window.ModuleManager.loadModule({
                                    moduleId: "module.vod",
                                    cbLoadModule: function (success) {
                                        if (success) window.ModuleManager.getModule("module.vod").execute({
                                            method: "setKidsCnt",
                                            params: 0
                                        });
                                    }
                                });
                            } else {
                                tmp.execute({method: "setKidsCnt", params: 0});
                            }
                        }
                        KIDS_MODE_MANAGER.setKidsModeRestrictionKey("false");
                        AppServiceManager.changeService({nextServiceState: CONSTANT.SERVICE_STATE.TV_VIEWING});
                        window.AuthManager.initPW();
                        window.isKidsPopupOpened = false;
                        LayerManager.historyBack();
                        LayerManager.activateLayer({
                            obj: {
                                id: "KidsHomeMain",
                                type: Layer.TYPE.NORMAL,
                                priority: Layer.PRIORITY.NORMAL,
                                linkage: false,
                                params: {
                                    data: MenuDataManager.getMenuData()[0]
                                }
                            },
                            moduleId: "module.kids",
                            visible: true,
                            new: true
                        });
                    }
                    else {
                        wrongCnt ++;
                        // if (wrongCnt < 3) {
                        window.AuthManager.initPW();
                        passwordCnt = 0;
                        deleteNumber();
                        window.showToast("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
                        // } else {
                        //
                        // }
                    }
                },
                loading: {
                    type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock: true,
                    callback: null,
                    on_off: false
                }
            });
        }
    };

    kidsSubHome.popup.kidsBootingCheck.prototype = new Layer();
    kidsSubHome.popup.kidsBootingCheck.prototype.constructor = kidsSubHome.popup.kidsBootingCheck;

    kidsSubHome.popup.kidsBootingCheck.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsSubHome.popup.kidsBootingCheck.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsBootingCheckPopup"] = kidsSubHome.popup.kidsBootingCheck;
})();