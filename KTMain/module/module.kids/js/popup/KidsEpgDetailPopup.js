/**
 * Created by Yun on 2017-04-21.
 */

(function () {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.kidsEpgDetail = function kidsEpgDetailPopup(options) {
        Layer.call(this, options);
        var div;
        var btnText_0 = ["시청", "닫기"];
        var btnText_1 = ["시청 예약", "닫기"];
        var btnText_2 = ["예약 취소", "닫기"];
        var data;
        var focusIdx = 0;
        var isReservation = false;
        var isShowReservation = false;

        this.init = function (cbCreate) {
            div = this.div;
            data = this.getParams().data;
            isShowReservation = data.is_show_reservation;
            if (window.reservationManager.isReserved(data.program) === 0) {
                isReservation = false;
            } else if (window.reservationManager.isReserved(data.program) === 1 || window.reservationManager.isReserved(data.program) === 2) {
                isReservation = true;
            }
            div.addClass("arrange_frame kidsPopup gniPopup kidsEpgDetailPopup");
            div.append(_$("<img/>", {class:"epgPopup_bg_l", src: window.modulePath + "resource/image/pop_bg_noti_h610_l.png"}))
            div.append(_$("<img/>", {class:"epgPopup_bg_m", src: window.modulePath + "resource/image/pop_bg_noti_h610_m.png"}))
            div.append(_$("<img/>", {class:"epgPopup_bg_r", src: window.modulePath + "resource/image/pop_bg_noti_h610_r.png"}))
            div.append(_$("<div/>", {class:"epgPopup_channelTitle_area"}));
            div.find(".epgPopup_channelTitle_area").append(_$("<span/>", {id:"epgTitle_channel_number"}));
            div.find(".epgPopup_channelTitle_area").append(_$("<span/>", {id:"epgTitle_channel_name"}));
            div.append(_$("<span/>", {class: "epgPopup_title_span"}));
            div.append(_$("<span/>", {class: "epgPopup_channelInfo_span"}));
            div.append(_$("<div/>", {class: "epgPopup_channelInfo_line"}));
            div.append(_$("<span/>", {class: "epgPopup_channelInfo_duration"}));

            div.append(_$("<span/>", {class: "epgPopup_description_area"}));

            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text((isShowReservation ? (isReservation ? btnText_2[i] : btnText_1[i]):btnText_0[i])));
            }
            setData();

            cbCreate(true);
        };

        function setData() {
            div.find("#epgTitle_channel_number").text(converterChannelNumber(data.channel.majorChannel + ""));
            div.find("#epgTitle_channel_name").text(data.channel.name);
            div.find(".epgPopup_title_span").text(data.program.name);
            div.find(".epgPopup_channelInfo_duration").text(data.program.duration / 60 + "분");
            div.find(".epgPopup_description_area").text(data.program.description);

            var tmp = new Date(data.program.startTime * 1000);
            var startTime = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
            tmp = new Date((data.program.startTime + data.program.duration) * 1000);
            var endTime = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
            var startDate = new Date(data.program.startTime * 1000);
            var showTimeStr = "(" + window.UTIL.transDay(startDate.getDay(), 'kor') + ") " + startTime + ' - ' + endTime;

            div.find(".epgPopup_channelInfo_span").text(showTimeStr);
        }

        function converterChannelNumber(value) {
            var tmpValue = "";
            for (var i = 0 ; i < (3-value.length) ; i ++) {
                tmpValue += "0";
            }
            tmpValue += value;
            return tmpValue;
        }

        this.show = function () {
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(key_code) {
            switch (key_code) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusIdx == 0) {
                        if (isShowReservation) {
                            data.btnType = 1;
                        } else {
                            data.btnType = 0;
                        }
                        this.getParams().callback(data);
                        LayerManager.historyBack();
                    } else {
                        data.btnType = 2;
                        this.getParams().callback(data);
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }
    };

    kidsSubHome.popup.kidsEpgDetail.prototype = new Layer();
    kidsSubHome.popup.kidsEpgDetail.prototype.constructor = kidsSubHome.popup.kidsEpgDetail;

    kidsSubHome.popup.kidsEpgDetail.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsSubHome.popup.kidsEpgDetail.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsEpgDetailPopup"] = kidsSubHome.popup.kidsEpgDetail;
})();