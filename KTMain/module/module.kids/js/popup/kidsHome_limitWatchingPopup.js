/**
 * 시청제한 설정 팝업
 * (키즈 -> 부모세상 -> 시청제한 설정)
 * Created by Yun on 2017-02-17.
 */

(function () {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.kidsLimitWatching = function kidsLimitWatchingPopup(options) {
        Layer.call(this, options);
        var div, btnText = ["저장", "취소"];
        var rdoText = ["제한 없음", "VOD 편수 제한", "시간 제한"];
        var limitTime = [{kor:"10분", eng:"10 minutes"}, {kor:"20분", eng:"20 minutes"}, {kor:"30분", eng:"30 minutes"}, {kor:"1시간", eng:"1 hour"}, {kor:"1시간 30분", eng:"1 hour 30 minutes"}, {kor:"2시간", eng:"2 hours"}, {kor:"2시간 30분", eng:"2 hours 30 minutes"}, {kor:"3시간", eng:"3 hours"}];
        var timeValue = [{hour:0, min:10}, {hour:0, min:20}, {hour:0, min:30}, {hour:1, min:0}, {hour:1, min:30}, {hour:2, min:0}, {hour:2, min:30}, {hour:3, min:0}];
        var dataLen = 0;
        var focusRow = 0;
        var focusIdx = 0;
        var selectIdx = [0, [0, 0, 0]];
        var language;
        var params;

        this.init = function (cbCreate) {
            params = this.getParams();
            language = params.langInfo;
            div = this.div;
            selectIdx[0] = window.KidsTimerCheckingManager.getSelectLimitIndex();
            for (var idx = 0 ; idx < 3; idx ++) {
                selectIdx[1][idx] = window.KidsTimerCheckingManager.getSelectLimitOptionIndex(idx);
            }
            div.addClass("arrange_frame kidsPopup gniPopup kidsLimitWatching");
            div.append(_$("<img/>", {
                class: "kidsPopup_decoration_img",
                src: modulePath + "resource/image/img_kidsmode_set_pop.png"
            }));
            div.append(_$("<div/>", {class: "kidsPopup_title"}).text("시청제한 설정"));
            div.append(_$("<div/>", {class: "kidsPopup_subTitle text_white_center_42M"}).text("자녀의 시청편수 또는 시청시간을 제한합니다"));
            div.append(_$("<div/>", {class: "kidsPopup_alertTitle text_gray_center_27L"}).text("제한된 편수/시간 전에 셋톱박스가 꺼지면 설정 내용은 초기화됩니다"));
            div.append(_$("<div/>", {class: "kidsPopup_bar_area"}));
            div.append(_$("<div/>", {class: "threeRdoBtn_area"}));
            for (var idx = 0; idx < 3; idx++) {
                div.find(".threeRdoBtn_area").append(_$("<div/>", {class: "rdoBtn"}));
                div.find(".threeRdoBtn_area .rdoBtn").eq(idx).append(_$("<img/>", {
                    class: "rdoBtn_bg",
                    src: modulePath + "resource/image/pop_btn_kids_w284.png"
                }));
                div.find(".threeRdoBtn_area .rdoBtn").eq(idx).append(_$("<img/>", {
                    class: "rdoBtn_img",
                    src: modulePath + "resource/image/rdo_btn_d.png"
                }));
                div.find(".threeRdoBtn_area .rdoBtn").eq(idx).append(_$("<span/>", {class: "rdoBtn_text"}).text(rdoText[idx]));
            }

            div.append(_$("<div/>", {class: "kidsPopup_optionSelect_area"}));
            div.find(".kidsPopup_optionSelect_area").append(_$("<img/>", {
                class: "optionSelect_arwLeft_img",
                src: modulePath + "resource/image/vod_end_ar_l.png"
            }));
            div.find(".kidsPopup_optionSelect_area").append(_$("<img/>", {
                class: "optionSelect_optionBG",
                src: modulePath + "resource/image/pop_btn_kids_w584.png"
            }));
            div.find(".kidsPopup_optionSelect_area").append(_$("<div/>", {class: "optionSelect_optionText"}).text(""));
            div.find(".kidsPopup_optionSelect_area").append(_$("<img/>", {
                class: "optionSelect_arwRight_img",
                src: modulePath + "resource/image/vod_end_ar_r.png"
            }));

            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function () {
            Layer.prototype.show.call(this);
            focusIdx = selectIdx[0];
            focusRow = 0;
            dataLen = 3;
            buttonFocusRefresh(focusIdx, focusRow);
            selectOptionsTextRefresh(focusIdx);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if (focusRow == 2 && selectIdx[0] == 0) {
                        focusIdx = 0;
                        focusRow = 0;
                        buttonFocusRefresh(focusIdx, focusRow);
                    } else {
                        focusIdx = focusRow == 0? 0 : (focusRow == 2?selectIdx[1][selectIdx[0]]:selectIdx[0]);
                        buttonFocusRefresh(focusIdx, focusRow = HTool.getIndex(focusRow, -1, 3));
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (focusRow == 0) {
                        selectIdx[focusRow] = focusIdx;
                        window.KidsTimerCheckingManager.setSelectLimitIndex(selectIdx[0]);
                        if (focusIdx == 0) {
                            focusIdx = 0;
                            focusRow = 2;
                        } else {
                            focusIdx = selectIdx[1][selectIdx[0]];
                            focusRow = 1;
                        }
                        buttonFocusRefresh(focusIdx, focusRow);
                    } else {
                        if (focusRow == 1) {
                            selectIdx[1][selectIdx[0]] = focusIdx;
                            focusIdx = 0;
                        }
                        else if (focusRow == 2) focusIdx = selectIdx[0];
                        buttonFocusRefresh(focusIdx, focusRow = HTool.getIndex(focusRow, 1, 3));
                    }
                    return true;
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, -1, dataLen), focusRow);
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, 1, dataLen), focusRow);
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 0) {
                        selectIdx[focusRow] = focusIdx;
                        if (focusIdx == 0) {
                            focusIdx = 0;
                            focusRow = 2;
                        } else {
                            focusIdx = selectIdx[1][selectIdx[0]];
                            focusRow = 1;
                        }
                        buttonFocusRefresh(focusIdx, focusRow);
                    } else if (focusRow == 1) {
                        selectIdx[1][selectIdx[0]] = focusIdx;
                        focusIdx = 0;
                        focusRow = 2;
                        buttonFocusRefresh(focusIdx, focusRow);
                    } else {
                        if (selectIdx[0] > 0 && focusIdx == 0) {
                            if (selectIdx[0] == 1) {
                                window.KidsTimerCheckingManager.settingLimitWatchingText(null);
                                window.KidsTimerCheckingManager.resetTimer();
                                var tmp = window.ModuleManager.getModule("module.vod");
                                if (!tmp) {
                                    window.ModuleManager.loadModule( {
                                        moduleId: "module.vod",
                                        cbLoadModule: function (success) {
                                            if (success) window.ModuleManager.getModule("module.vod").execute({method:"setKidsCnt", params: (selectIdx[1][1]+1)});
                                        }
                                    });
                                } else {
                                    tmp.execute({method:"setKidsCnt", params: (selectIdx[1][1]+1)});
                                }
                                if (language == "eng") {
                                    window.KidsTimerCheckingManager.settingLimitWatchingText(((selectIdx[1][1] + 1) + (selectIdx[1][1] > 0 ? " episodes" : " episode")));
                                } else {
                                    window.KidsTimerCheckingManager.settingLimitWatchingText(((selectIdx[1][1] + 1) + " 편만 보기"));
                                }
                            } else if (selectIdx[0] == 2) {
                                window.KidsTimerCheckingManager.settingTimerChecking(timeValue[selectIdx[1][selectIdx[0]]]);
                                window.KidsTimerCheckingManager.settingLimitWatchingText(language == "eng" ? limitTime[selectIdx[1][2]].eng : limitTime[selectIdx[1][2]].kor + "만 시청");
                                var tmp = window.ModuleManager.getModule("module.vod");
                                if (!tmp) {
                                    window.ModuleManager.loadModule( {
                                        moduleId: "module.vod",
                                        cbLoadModule: function (success) {
                                            if (success) window.ModuleManager.getModule("module.vod").execute({method:"setKidsCnt", params: 0});
                                        }
                                    });
                                } else {
                                    tmp.execute({method:"setKidsCnt", params: 0});
                                }
                            }
                            window.KidsTimerCheckingManager.setSelectLimitIndex(selectIdx[0]);
                            window.KidsTimerCheckingManager.setSelectLimitOptionIndex(selectIdx[0], selectIdx[1][selectIdx[0]]);
                            KIDS_MODE_MANAGER.setKidsModeRestrictionKey("true");
                        } else if (selectIdx[0] == 0 && focusIdx == 0) {
                            window.KidsTimerCheckingManager.setSelectLimitIndex(selectIdx[0]);
                            window.KidsTimerCheckingManager.setSelectLimitOptionIndex(selectIdx[0], selectIdx[1][selectIdx[0]]);
                            window.KidsTimerCheckingManager.settingLimitWatchingText(null);
                            window.KidsTimerCheckingManager.resetTimer();
                            var tmp = window.ModuleManager.getModule("module.vod");
                            if (!tmp) {
                                window.ModuleManager.loadModule( {
                                    moduleId: "module.vod",
                                    cbLoadModule: function (success) {
                                        if (success) window.ModuleManager.getModule("module.vod").execute({method:"setKidsCnt", params: 0});
                                    }
                                });
                            } else {
                                tmp.execute({method:"setKidsCnt", params: 0});
                            }
                            KIDS_MODE_MANAGER.setKidsModeRestrictionKey("false");
                        }
                        params.complete();
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };


        function buttonFocusRefresh(index, rowIndex) {
            div.find(".threeRdoBtn_area .rdoBtn").removeClass("focus");
            div.find(".threeRdoBtn_area .rdoBtn .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_d.png");
            div.find(".threeRdoBtn_area .rdoBtn:eq(" + selectIdx[0] + ") .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_select_d.png");
            div.find(".kidsPopup_optionSelect_area").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            switch (rowIndex) {
                case 0:
                    dataLen = 3;
                    div.find(".threeRdoBtn_area .rdoBtn:eq(" + index + ")").addClass("focus");
                    if (selectIdx[0] == index) {
                        div.find(".threeRdoBtn_area .rdoBtn:eq(" + index + ") .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_select_f.png");
                    } else {
                        div.find(".threeRdoBtn_area .rdoBtn:eq(" + index + ") .rdoBtn_img").attr("src", modulePath + "resource/image/rdo_btn_f.png");
                    }
                    selectOptionsTextRefresh(index);
                    break;
                case 1:
                    dataLen = selectIdx[0] == 0 ? 0 : (selectIdx[0] == 1 ? 10 : limitTime.length);
                    div.find(".kidsPopup_optionSelect_area").addClass("focus");
                    selectIdx[1][selectIdx[0]] = index;
                    selectOptionsTextRefresh(selectIdx[0]);
                    break;
                case 2:
                    dataLen = 2;
                    div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
                    break;
            }
        }

        function selectOptionsTextRefresh(index) {
            if (language == "eng") {
                div.find(".kidsPopup_optionSelect_area .optionSelect_optionText").text(index == 0 ? "" : (index == 1 ? ((selectIdx[1][1] + 1) + (selectIdx[1][1] > 0 ? " episodes" : " episode")) : limitTime[selectIdx[1][2]].eng));
            } else {
                div.find(".kidsPopup_optionSelect_area .optionSelect_optionText").text(index == 0 ? "" : (index == 1 ? ((selectIdx[1][1] + 1) + " 편만 보기") : limitTime[selectIdx[1][2]].kor));
            }
        }
    };

    kidsSubHome.popup.kidsLimitWatching.prototype = new Layer();
    kidsSubHome.popup.kidsLimitWatching.prototype.constructor = kidsSubHome.popup.kidsLimitWatching;

    kidsSubHome.popup.kidsLimitWatching.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsSubHome.popup.kidsLimitWatching.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsLimitWatchingPopup"] = kidsSubHome.popup.kidsLimitWatching;
})();