/**
 * Created by Yun on 2017-04-06.
 */

(function () {
    var kids_watchingClose_popup = function kidsWatchingClosePopup(options) {
        Layer.call(this, options);
        var _this = this, data, closeTimeout;
        var isCntEnd = false;
        var isSndEnd = false;
        var vodModule;
        var charData;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame kids kids_watchingClose_popup");
            this.div.append(_$("<img/>", {class: "kids_closeTitle_img", src: window.modulePath + "resource/image/kids_end_txt.png"}));
            this.div.append(_$("<img/>", {class: "kids_background_img_0", src: window.modulePath + "resource/image/character_shine.png"}));
            this.div.append(_$("<img/>", {class: "kids_background_img_1", src: window.modulePath + "resource/image/kids_end_planet.png"}));
            this.div.append(_$("<img/>", {class: "kids_character_img", src: window.modulePath + "resource/image/character_default.png"}));
            cbCreate(true);
        };

        function vodRequestPlayKidsSound() {
            vodModule = window.ModuleManager.getModule("module.vod");
            if (!vodModule) {
                window.ModuleManager.loadModule({
                    moduleId: "module.vod",
                    cbLoadModule: function (success) {
                        if (success) vodRequestPlayKidsSound();
                    }
                });
            } else {
                vodModule.execute({method: "requestPlayKidsSound", params: {
                    soundUrl: ((charData && charData.soundUrl) ? charData.soundUrl : null),
                    setVodEndTime: data.isVodEndTime,
                    onSoundEnded: function() {
                        isSndEnd = true;
                        log.printDbg("[KidsWatchingClose onSoundEnded] isCountCheck = " + isCntEnd + ", isSoundEndCheck = " + isSndEnd);
                        if (isCntEnd && isSndEnd) stbTurnOff();
                    }
                }});
            }
        }

        this.show = function() {
            isCntEnd = false;
            isSndEnd = false;
            charData = window.KidsCharacterManager.getSelectCharacterData();
            this.div.find(".kids_character_img").attr("src", ((charData && charData.soundUrl) ? charData.endImgUrl : window.modulePath + "resource/image/character_default.png"));
            data = _this.getParams();
            vodRequestPlayKidsSound();
            Layer.prototype.show.call(this);
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            closeTimeout = setTimeout(function() {
                isCntEnd = true;
                log.printDbg("[KidsWatchingClose Count] isCountCheck = " + isCntEnd + ", isSoundEndCheck = " + isSndEnd);
                if (isSndEnd && isCntEnd) stbTurnOff();
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }, 5000);
        };

        this.hide = function() {
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            Layer.prototype.hide.call(this);
        };

        function stbTurnOff() {
            window.KidsTimerCheckingManager.settingLimitWatchingText(null);
            window.KidsTimerCheckingManager.resetTimer();
            var tmp = window.ModuleManager.getModule("module.vod");
            if (!tmp) {
                window.ModuleManager.loadModule({
                    moduleId: "module.vod",
                    cbLoadModule: function (success) {
                        if (success) window.ModuleManager.getModule("module.vod").execute({
                            method: "setKidsCnt",
                            params: 0
                        });
                    }
                });
            } else {
                tmp.execute({method: "setKidsCnt", params: 0});
            }
            LayerManager.deactivateLayer({
                id: _this.id,
                remove: true
            });
            window.homeImpl.get(window.homeImpl.DEF_FRAMEWORK.POWER_MODE_MANAGER).changeStandbyMode();
        }
    };

    kids_watchingClose_popup.prototype = new Layer();
    kids_watchingClose_popup.prototype.constructor = kids_watchingClose_popup;

    kids_watchingClose_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kids_watchingClose_popup.prototype.handleKeyEvent = function() {
        return true;
    };

    arrLayer["KidsWatchingClose"] = kids_watchingClose_popup;
})();