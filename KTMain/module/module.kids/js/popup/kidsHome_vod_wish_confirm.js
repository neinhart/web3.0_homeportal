/**
 * Created by Lazuli on 2017-04-13.
 */
(function() {
    var kidsHome_vod_wish_confirm = function kidsVODWishConfirm(options) {
        Layer.call(this, options);
        var _this = this, curIdx = 0;
        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame kidsPopup wish_confirm");
            this.div.append(_$("<span/>", {class: "title"}).text("찜해제"));
            this.div.append(_$("<span/>", {class: "description"}).html("찜한 콘텐츠로 이미 등록되어 있습니다<br>해당 콘텐츠 찜을 해제 하시겠습니까?"));
            this.div.append(_$("<div/>", {class: "buttonArea"}));
            var menuArr = ["확인", "취소"];
            for (var i = 0; i < 2; i++) {
                _this.div.find(".buttonArea").append(_$(
                    "<div class='button'>" +
                    "<span class='text'>" + menuArr[i] + "</span>" +
                    "</div>"
                ));
            }
            cbCreate(true);
        };

        this.show = function() {
            setFocus(0);
            Layer.prototype.show.call(this);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        function setFocus(idx) {
            _this.div.find(".buttonArea .button.focus").removeClass("focus");
            _this.div.find(".buttonArea .button").eq(parseInt(idx) || curIdx).addClass("focus");
        }

        this.handleKeyEvent = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    curIdx = HTool.indexMinus(curIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    curIdx = HTool.indexPlus(curIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if (curIdx == 0) _this.getParams().callback();
                    LayerManager.deactivateLayer({id: _this.id, remove: true});
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: _this.id, remove: true});
                    return true;
            }
            return false;
        }
    };

    kidsHome_vod_wish_confirm.prototype = new Layer();
    kidsHome_vod_wish_confirm.prototype.constructor = kidsHome_vod_wish_confirm;

    kidsHome_vod_wish_confirm.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    arrLayer["KidsVODWishConfirm"] = kidsHome_vod_wish_confirm;
})();