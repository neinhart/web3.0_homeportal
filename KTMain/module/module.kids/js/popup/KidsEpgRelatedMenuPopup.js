/**
 * Created by Yun on 2017-04-03.
 */

(function () {
    kidsSubHome.popup = kidsSubHome.popup || {};
    kidsSubHome.popup.KidsEpgRelatedMenuPopup = function KidsEpgRelatedMenuPopup(options) {
        Layer.call(this, options);

        var log = window.log;
        var params = null;
        var contextMenu = null;
        var div = null;

        this.createView = function (cbCreate) {
            log.printDbg("create()");
            params = this.getParams();
            this.div.addClass("arrange_frame kids kids_epg_popup");
            div = this.div;
            contextMenu = params.contextM;
            div.append(contextMenu.getView());

            cbCreate(true);
        };

        this.show = function(options) {
            log.printDbg("show(" + log.stringify(options) + ")");
            Layer.prototype.show.call(this);
            setTimeout(function() {contextMenu.open();}, 150);
        };

        this.showView = function(options) {
            log.printDbg("show(" + log.stringify(options) + ")");
        };

        this.hideView = function(options) {
            log.printDbg("hide(" + log.stringify(options) + ")");
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;
            if (contextMenu.isOpen()) return contextMenu.onKeyAction(key_code);

            switch (key_code) {
                case KEY_CODE.UP:
                case KEY_CODE.DOWN:
                case KEY_CODE.OK:
                    consumed = true;
                    break;
                case KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KEY_CODE.LEFT:
                case KEY_CODE.BACK:
                case KEY_CODE.CONTEXT :
                    setTimeout(function() {
                        LayerManager.deactivateLayer({
                            id: "KidsEpgRelatedMenuPopup"
                        });
                    }, 600);
                    consumed = true;
                    break;
                case KEY_CODE.EXIT:
                    consumed = false;
                    break;
            }
            return consumed;
        };
    };

    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype = new Layer();
    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype.constructor = kidsSubHome.popup.KidsEpgRelatedMenuPopup;

    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.createView(cbCreate);
    };
    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype.show = function (options) {
        this.showView(options);
    };
    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype.hide = function (options) {
        this.hideView(options);
    };
    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype.destroy = function () {
        this.destroy();
    };
    kidsSubHome.popup.KidsEpgRelatedMenuPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsEpgRelatedMenuPopup"] = kidsSubHome.popup.KidsEpgRelatedMenuPopup;
})();