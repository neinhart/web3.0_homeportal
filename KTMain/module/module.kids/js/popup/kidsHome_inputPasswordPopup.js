/**
 * Created by Yun on 2017-02-16.
 */

(function () {
    // kidsSubHome.popup = kidsSubHome.popup || {};
    var kidsInputPassword = function kidsInputPasswordPopup(options) {
        Layer.call(this, options);
        var div, passwordCnt = 0;
        var focusRow = 0;
        var params;
        var authMgr = window.AuthManager;
        var wrongCnt = 0;
        var isKidsOn = false;
        var speechAdapter = oipfAdapter.speechRecognizerAdapter;

        this.init = function (cbCreate) {
            params = this.getParams();
            div = this.div;
            isKidsOn = params.isKidsOn;
            div.addClass("arrange_frame kidsPopup gniPopup kidsInputPassword");
            div.append(_$("<img/>", {class: "kidsPopup_decoration_img", src: modulePath + "resource/image/img_kidsmode_set_pop.png"}));
            div.append(_$("<div/>", {class: "kidsPopup_title"}).text("인증"));
            div.append(_$("<div/>", {class: "kidsPopup_subTitle text_white_center_42M"}).text("olleh tv 비밀번호(성인인증)를 입력하세요"));
            div.append(_$("<img/>", {class: "kidsPopup_inputPasswordBG_img", src: modulePath + "resource/image/kids_pw.png"}));
            div.append(_$("<div/>", {class: "passwordArea"}));
            div.find(".passwordArea").append(_$("<span/>", {class: "password"}).text("* * * *"));
            div.find(".passwordArea").append(_$("<span/>", {class: "password fill"}));
            div.append(_$("<div/>", {class: "oneBtnArea_280"}));
            div.find(".oneBtnArea_280").append(_$("<div/>", {class: "btn"}));
            div.find(".oneBtnArea_280 .btn").append(_$("<div/>", {class: "whiteBox"}));
            div.find(".oneBtnArea_280 .btn").append(_$("<span/>", {class: "btnText"}).text("취소"));
            authMgr.initPW();
            authMgr.resetPWCheckCount();

            cbCreate(true);
        };

        this.show = function () {
            focusRow = 0;
            passwordCnt = 0;
            window.AuthManager.initPW();
            window.AuthManager.resetPWCheckCount();
            buttonFocusRefresh(focusRow);
            deleteNumber();
            speechAdapter.start(true);
            speechAdapter.addSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            speechAdapter.stop();
            speechAdapter.removeSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.DEL:
                    if (focusRow == 0) {
                        if (passwordCnt > 0) {
                            passwordCnt--;
                            deleteNumber();
                        }
                    }
                    return true;
                case KEY_CODE.UP:
                    buttonFocusRefresh(focusRow = 0);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(focusRow = 1);
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 1) {
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    if (keyCode >= KEY_CODE.NUM_0 && keyCode <= KEY_CODE.NUM_9 && focusRow == 0) {
                        if (passwordCnt < 4) passwordCnt ++;
                        fillPassword(keyCode - KEY_CODE.NUM_0);
                        if (passwordCnt == 4) {

                        }
                        return true;
                    }
            }
        };

        function deleteNumber() {
            authMgr.deletePW();
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
        }

        function buttonFocusRefresh (index) {
            div.find(".kidsPopup_inputPasswordBG_img").removeClass("focus");
            div.find(".passwordArea").removeClass("focus");
            div.find(".oneBtnArea_280 .btn").removeClass("focus");
            if (index == 1) {
                div.find(".oneBtnArea_280 .btn").addClass("focus");
                speechAdapter.stop();
                speechAdapter.removeSpeechRecognizerListener(inputSpeechNumber);
            } else {
                div.find(".kidsPopup_inputPasswordBG_img").addClass("focus");
                div.find(".passwordArea").addClass("focus");
                speechAdapter.start(true);
                speechAdapter.addSpeechRecognizerListener(inputSpeechNumber);
            }
        }

        function getStarText(_passwordCnt) {
            var starText = "";
            if (_passwordCnt > 0) {
                for (var idx = 0 ; idx < _passwordCnt ; idx ++) {
                    if (_passwordCnt == 4 && idx == 3) starText += "*";
                    else starText += "* ";
                }
            }
            return starText;
        }

        function inputSpeechNumber (rec, input, mode) {
            if (rec && mode === oipfDef.SR_MODE.PIN) {
                passwordCnt = input.length > 4 ? 4 : input.length;
                div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
                if (authMgr.replacePW(input, 4)) checkPassword();
                log.printInfo("inputSpeechNumber::" + input);
            }
        }

        function fillPassword(num) {
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
            if (authMgr.inputPW(num, 4)) checkPassword();
        }

        function checkPassword() {
            authMgr.checkUserPW({
                type: window.AuthManager.AUTH_TYPE.AUTH_ADULT_PIN,
                callback: function(responseCode) {
                    log.printDbg("checkUserPW ResponseCode === " + responseCode);
                    switch (responseCode) {
                        case window.AuthManager.RESPONSE.CAS_AUTH_SUCCESS:
                            window.AuthManager.resetPWCheckCount();
                            passwordCnt = 0;
                            deleteNumber();
                            if (isKidsOn) {
                                KIDS_MODE_MANAGER.changeMode({
                                    kidsMode: window.OIPF_DEF.KIDS_MODE.ON,
                                    isKidsModule: true
                                });
                            } else {
                                LayerManager.historyBack();
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "KidsLimitWatchingPopup",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            langInfo: params.langInfo,
                                            complete: params.complete
                                        }
                                    },
                                    moduleId: "module.kids",
                                    new: true,
                                    visible: true
                                });
                            }
                            break;
                        case window.AuthManager.RESPONSE.CAS_AUTH_FAIL:
                            window.showToast("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
                            window.AuthManager.initPW();
                            passwordCnt = 0;
                            deleteNumber();
                            break;
                        case window.AuthManager.RESPONSE.GOTO_INIT:
                        case window.AuthManager.RESPONSE.CANCEL:
                            window.AuthManager.initPW();
                            passwordCnt = 0;
                            deleteNumber();
                            LayerManager.deactivateLayer({
                                id: "KidsInputPasswordPopup",
                                remove: true
                            });
                            break;
                    }
                },
                loading: {
                    type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock: true,
                    callback: null,
                    on_off: false
                }
            });
        }
    };

    kidsInputPassword.prototype = new Layer();
    kidsInputPassword.prototype.constructor = kidsInputPassword;

    kidsInputPassword.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    kidsInputPassword.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsInputPasswordPopup"] = kidsInputPassword;
})();