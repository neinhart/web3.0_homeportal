/**
 * Created by Lazuli on 2017-02-22.
 */

kidsSubHome = {
    HTTP: {
        CURATION: {
            LIVE_URL: "http://profile.ktipmedia.co.kr:7003/wasProfile",
            TEST_URL: "http://125.140.114.151:7002/wasProfile"
        },
        AMOC: {
            HTTP_URL: "http://webui.ktipmedia.co.kr:8080",
            HTTPS_URL: "https://webui.ktipmedia.co.kr",
            WAPI_URL:"http://wapi.ktipmedia.co.kr"
        },
        RECOMMEND: {
            LIVE_URL: "http://recommend.ktipmedia.co.kr:7002/",
            TEST_URL: "http://125.140.114.151:7002/"
        },
        KTPG: {
            LIVE_URL: "http://ktpay.kt.com:10088/webapi/json/stb/",
            TEST_URL: "http://221.148.188.212:10088/webapi/json/stb/"
        },
        SMLS: {
            LIVE_URL: "http://222.122.121.80:8080",
            TEST_URL: "http://203.255.241.154:8080"
        },
        KIDS: {
            // LIVE_URL: "http://homemodule.ktipmedia.co.kr/web3",
            // BMT_URL: "http://125.159.3.183:8080"
            // LIVE_URL: "https://125.159.2.19",
            // BMT_URL: "https://125.159.3.183"
            LIVE_URL: "https://web3api.ktipmedia.co.kr",
            BMT_URL: "https://web3apibmt.ktipmedia.co.kr"
        }
    }
};