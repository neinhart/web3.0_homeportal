/**
 * Created by Yun on 2017-02-22.
 */


window.KidsCharacterManager = (function () {
    var selCharData;
    var isCharSel = false;
    var isCallInfo = true;

    var apiUrl = window.DEF.RELEASE_MODE==="LIVE" ? kidsSubHome.HTTP.KIDS.LIVE_URL:  kidsSubHome.HTTP.KIDS.BMT_URL;

    function ajax(async, timeout, url, postData, callback) {
        return _$.ajax({
            type: "post",
            async: async,
            url: url,
            timeout: timeout,
            cache:false,
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(postData),
            success: function(result) {
                callback(result, true);
                log.printDbg("[HTTP] result = " + JSON.stringify(result));
            },error:function(request, status, error){
                log.printErr("[HTTP] request : " + JSON.stringify(request));
                log.printErr("[HTTP] status : " + JSON.stringify(status));
                log.printErr("[HTTP] error : " + JSON.stringify(error));
                callback(status, false);
            }
        });
    }

    function _getKidsList(callback) {
        log.printDbg(StorageManager.ps.load(StorageManager.KEY.OFFICE_CODE));
        var postData = {
            "saId": DEF.SAID,
            "officeCd": StorageManager.ps.load(StorageManager.KEY.OFFICE_CODE),
            "model": window.oipfAdapter.hwAdapter.getModelName()
            // "officeCd": "R00415",
            // "model": "BP-4000"
        };
        return ajax(true, 0, apiUrl + "/deploy-api/kids/list", postData, callback);
    }

    function _getKidsInfo(callback) {
        var postData = {
            "saId": DEF.SAID,
            "officeCd": StorageManager.ps.load(StorageManager.KEY.OFFICE_CODE),
            "model": window.oipfAdapter.hwAdapter.getModelName()
            // "officeCd": "R00415",
            // "model": "BP-4000"
        };
        return ajax(true, 5e3, apiUrl + "/deploy-api/kids/info", postData, callback);
    }

    function _setKidsSelectCharacter(characterId, callback) {
        var postData = {
            "saId": DEF.SAID,
            "officeCd": StorageManager.ps.load(StorageManager.KEY.OFFICE_CODE),
            "model": window.oipfAdapter.hwAdapter.getModelName(),
            // "officeCd": "R00415",
            // "model": "BP-4000",
            "characterId": characterId
        };
        return ajax(true, 0, apiUrl + "/deploy-api/kids/set", postData, callback);
    }

    return {
        setIsSelectCharacter: function(data) { isCharSel = data; },
        getIsSelectCharacter: function() { return isCharSel; },
        setSelectCharacterData: function(data) { selCharData = data; },
        getSelectCharacterData: function() { return selCharData; },
        setIsCallCharacterInfo: function(data) { isCallInfo = data; },
        getIsCallCharacterInfo: function() { return isCallInfo; },
        getKidsList: _getKidsList,
        getKidsInfo: _getKidsInfo,
        setKidsSelectCharacter: _setKidsSelectCharacter
    }
}());