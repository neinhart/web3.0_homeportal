/**
 * Created by ksk91_000 on 2016-11-08.
 */
window.RecentlyListManager = new function () {
    var recentlyList = [];
    var length;
    var isLoaded = false;
    var isKidsRecentlyLoad = false;
    var isResetting = false;
    var kidsSubHomeId = "";
    var isInit = false;

    var update = function (callback, catId) {
        kidsSubHome.amocManager.getViewHistoryNxt("saId=" + DEF.SAID + "&adultYn=N&opt=3&itemCnt=20&seriesTitleYn=Y&catId=" + catId + "&genreCd=13", function (result, contentsData) {
            if (result) {
                contentsData = contentsData.watchHistory;
                recentlyList = contentsData?(_$.isArray(contentsData) ? contentsData : [contentsData]):[];
                length = recentlyList.length;

                isLoaded = true;
                isInit = true;
            } else {
                // HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                recentlyList = [];
            }
            if (callback) callback(recentlyList);
        });
    };

    var addContents = function (object, linkTime) {
        recentlyList.push({
            imgUrl: object.imgUrl,
            contsName: object.contsName,
            prInfo: object.prInfo,
            catId: object.catId,
            contsId: object.contsId,
            contYn: object.contYn,
            runTime: object.runTime,
            wonYn: object.wonYn,
            linkTime: linkTime
        });
    };

    var removeContentsList = function (idxList, callback) {
        var result = true;
        var delCnt = idxList.length;
        for(var i=0; i<idxList.length; i++) {
            kidsSubHome.amocManager.setDelWatch(function (result, data) {
                if (result){
                    callbackFunc(data);
                    if (ModuleManager.getModule("module.kids")) ModuleManager.getModule("module.kids").execute({method: "kidsRecentlyListCheck"});
                }
                else callback({flag: 1, message: "시청목록 삭제 실패"});
            }, DEF.SAID, recentlyList[idxList[i]].contsId, recentlyList[idxList[i]].contYn)
        }

        function callbackFunc(data) {
            if(data.flag==1) result = false;
            if(--delCnt==0) {
                for(var i=idxList.length-1; i>=0; i--)
                    recentlyList.splice(idxList[i], 1);

                callback(result);
                isLoaded = false;
            }
        }
    };

    var removeContents = function (idx, callback) {
        kidsSubHome.amocManager.setDelWatch(function (result, data) {
            if (result){
                recentlyList.splice(idx, 1);
                callback(true);
            }
            else callback({flag: 1, message: "시청목록 삭제 실패"});
        }, DEF.SAID, recentlyList[idx].contsId, recentlyList[idx].contYn)
    };

    var setLinkTime = function (contsId, linkTime) {
        for(var i in recentlyList) {
            if(recentlyList[i].contsId==contsId) {
                recentlyList[i].linkTime = linkTime;
                return;
            }
        }
    };

    var getRecentlyData = function (callback, noUpdate, catId) {
        if (!isLoaded && !(noUpdate && isInit)) update(callback, catId);
        else callback(recentlyList);
    };

    return {
        setIsKidsRecentlyReLoad: function(data) { if (data) {isResetting = true;} isKidsRecentlyLoad = data; },
        getIsKidsRecentlyReLoad: function() { return isKidsRecentlyLoad; },
        setKidsSubHomeId: function(data) { kidsSubHomeId = data; },
        getKidsSubHomeId: function() { return kidsSubHomeId; },
        setIsResetting: function(data) { isResetting = data; },
        getIsResetting: function() { return isResetting; },
        update: update,
        addContents: addContents,
        removeContents: removeContents,
        removeContentsList: removeContentsList,
        getRecentlyData: getRecentlyData,
        setLinkTime: setLinkTime
    }
};