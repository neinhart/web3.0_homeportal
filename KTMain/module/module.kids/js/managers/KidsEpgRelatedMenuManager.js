/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgRelatedMenuManager</code>
 *
 * Full Epg / Mini Epg에서 제공하는 연관메뉴 UI를 제공한다.
 *  var related_menu_info = {
 *        left_menu_info : {
 *            "channel" : cur_channel,
 *            "is_ots_channel" : true/false,
 *            "is_show_favorited_channel" : true/false,
 *            "is_show_2ch" : true/false,
 *            "is_show_4ch" : true/false,
 *            "is_show_hit" : true/false
 *            "is_show_genre_option" : true/false
 *            "focus_option_index" : 0 / 1
 *            left_menu_callback_func : callback (parameter : menu_index , genre_option_index) // is_show_favorited_channel : 0 , detail : 1
 *                                                                      is_show_2ch : 2 , is_show_4ch : 3,
 *                                                                      is_show_hit : 4
 *                                                                      genre_option_index : 0 / 1
 *        },
 *        right_menu_info : {
 *            caller : 0 or 1 ,(KidsEpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER FULL_EPG,MINI_EPG)
 *            full_epg : {
 *                    top_genre : [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    middle_genrc : [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    bottom_genrc ; [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    focus_genrc : top_genrc/bottom_genrc/bottom_genrc
 *                    focus_index : 0 ~ (n-1),
 *                    fullepg_callback : callback(menuId)
 *            },
 *
 *            mini_epg : {
 *                    top_menu : {
 *                        is_show_top_menu : true/false , *
 *                        channel_genre : [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    },
 *                    middle_menu : {
 *                        caption : {
 *                            is_show_caption : true/false
 *                            is_dimmed : true/false
 *                        } ,
 *                        voice : {
 *                             is_show_voice : true/false
 *                             is_dimmed : true/false
 *                             voicelist : [],
 *                             voice_focus : focusIndex
 *                        },
 *                        bluetooth_voice_lang {
 *                             is_show_bluetooth_voice_lang : true/false
 *                             is_dimmed : true/false
 *                             bluetooth_voice_lang_list : [],
 *                             bluetooth_voice_lang_focus : focusIndex
 *                        },
 *                        bluetooth_listening  {
 *                             is_show_bluetooth_listening : true/false
 *                             is_dimmed : true/false
 *                             bluetooth_listening_list : []
 *                             bluetooth_listening_focus : focusIndex
 *                        }
 *                    },
 *
 *                    bottom_menu ; {
 *                        is_show_bottom_menu : true/false
 *                        app_list : [{name:북마크,value:BOOKMARK},{name:홈위젯,value:HOME_WIDJECT}]
 *                    },
 *                    miniepg_callback : callback(parameter : menu , index)
 *                            menu : top_menu/middle_menu/bottom_menu
 *                            index : 0 ~ (n-1)
 *            },
 *        }
 *      };
 *
 *
 * @author jjh1117
 * @since 2016-11-28
 */

"use strict";

window.KidsEpgRelatedMenuManager = (function () {

    var log = window.log;
    var util = window.UTIL;
    

    function _init() {
    }

    /**
     *
     * @param focusChannel : 포커스 채널 정보
     * @param leftMenuCallbackFunc : callback함수 parameter(index , genreoption_index)
     * @param isShowFavoritedChannel : 선호채널 등록/해제 버튼 노출 여부
     * @param isShowTwoChannel : 2채널 동시 시청 버튼 노출 여부
     * @param isShowFourChannel : 4채널 동시 시청 버튼 노출 여부
     * @param isShowHitChannel : 실시간 인기채널 버튼 노출 여부
     * @param isShowGenreOption : 장르 옵션 drop box 노출 여부
     * @param rightMenuCallbackFunc : callback함수 parameter(menuid)
     * @param focusMenuId : 편성표 메뉴ID
     * @private
     */
    function _showFullEpgRelatedMenu(focusChannel , leftMenuCallbackFunc , isShowFavoritedChannel , isShowTwoChannel ,
                                     isShowFourChannel , isShowHitChannel , isShowGenreOption , focusOptionIndex , rightMenuCallbackFunc , focusMenuId) {
        var topGenre = [];
        var middleGenre = [];
        var bottomGenre = [];

        var focusGenre = -1;
        var focusIndex = 0;


        // 채널 가이드 메뉴 가져오기
        var guideMenu = window.MenuDataManager.searchMenu({
            menuData: window.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id == window.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                    return true;
                }
            }
        })[0];


        if(guideMenu !== undefined && guideMenu !== null) {
            var childrenLength = guideMenu.children.length;

            /**
             * Top genre 구성
             */
            var obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST);
            if(obj !== null) {
                topGenre[topGenre.length] = obj;
            }

            obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL);
            if(obj !== null) {
                topGenre[topGenre.length] = obj;
            }

            if(window.CONSTANT.IS_OTS === true) {
                obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.SKYLIFE_UHD_CHANNEL);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }

                obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.MOVIE_CHOICE);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }

            }else {
                obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.UHD_CHANNEL);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }
            }

            obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.AUDIO_CHANNEL);
            if(obj !== null) {
                topGenre[topGenre.length] = obj;
            }

            for(var x=0;x<topGenre.length;x++) {
                if(topGenre[x].menuid === focusMenuId) {
                    focusGenre = KidsEpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.TOP_GENRE;
                    focusIndex = x;
                    break;
                }
            }

            /**
             * Middle genre 구성
             */
            if(window.CONSTANT.IS_OTS === true) {
                for(var i=0;i<childrenLength;i++) {
                    var menu = guideMenu.children[i];
                    if(menu.id === window.MenuDataManager.MENU_ID.PROGRAM_SEARCH) {
                        var genrechildrenLength = menu.children.length;
                        for(var j=0;j<genrechildrenLength;j++) {
                            var menuObject = _makeListObject(menu.children[j].name,menu.children[j].id);
                            if(menuObject !== null) {
                                middleGenre[middleGenre.length] = menuObject;
                            }
                        }
                        break;
                    }
                }
            }else {
                for(var i=0;i<childrenLength;i++) {
                    var menu = guideMenu.children[i];
                    if(menu.id === window.MenuDataManager.MENU_ID.GENRE_CHANNEL) {
                        var genrechildrenLength = menu.children.length;
                        for(var j=0;j<genrechildrenLength;j++) {
                            var menuObject = _makeListObject(menu.children[j].name,menu.children[j].id);

                            if(menuObject !== null) {
                                middleGenre[middleGenre.length] = menuObject;
                            }
                        }
                        break;
                    }
                }
            }

            if(focusGenre<0) {
                for(var x=0;x<middleGenre.length;x++) {
                    if(middleGenre[x].menuid === focusMenuId) {
                        focusGenre = KidsEpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.MIDDLE_GENRE;
                        focusIndex = x;
                        break;
                    }
                }
            }

            /**
             * Bottom genre 구성
             */
            var obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL);
            if(obj !== null) {
                bottomGenre[bottomGenre.length] = obj;
            }

            obj = _searchMenu(guideMenu,window.MenuDataManager.MENU_ID.TV_APP_CHANNEL);
            if(obj !== null) {
                bottomGenre[bottomGenre.length] = obj;
            }

            if(focusGenre<0) {
                for(var x=0;x<bottomGenre.length;x++) {
                    if(bottomGenre[x].menuid === focusMenuId) {
                        focusGenre = KidsEpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.BOTTOM_GENRE;
                        focusIndex = x;
                        break;
                    }
                }
            }
        }

        if (focusGenre < 0) focusGenre = 0;
        var leftMenuinfo = _makeLeftMenuInfo(focusChannel,leftMenuCallbackFunc , isShowFavoritedChannel,
            isShowTwoChannel , isShowFourChannel , isShowHitChannel , isShowGenreOption , focusOptionIndex);

        var fullepgMenuinfo = _makeFullEpgMenuInfo(topGenre , middleGenre,bottomGenre , focusGenre , focusIndex , rightMenuCallbackFunc) ;

        var rightMenuinfo = _makeRightMenuInfo(KidsEpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER.FULL_EPG , fullepgMenuinfo , {});


        var menuInfo = _makeMenuInfo(leftMenuinfo ,rightMenuinfo);

        // _activateRelatedMenu(menuInfo);
        LayerManager.activateLayer({
            obj: {
                id: "KidsEpgRelatedMenuPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    data : menuInfo
                }
            },
            moduleId: "module.kids",
            new: true,
            visible: true
        });
    }

    function _searchMenu(guideMenu , id) {
        var menuObject = null;
        if(guideMenu !== undefined && guideMenu !== null) {
            var childrenLength = guideMenu.children.length;
            for(var i=0;i<childrenLength;i++) {
                var menu = guideMenu.children[i];
                if(menu.id === id) {
                    menuObject = _makeListObject(menu.name,menu.id);
                    break;
                }
            }
        }
        return menuObject;
    }

    function _activateTwoChannel(callbackFuncHide , callbackFuncSwap) {

        var tempChannel = window.oipfAdapter.navAdapter.getCurrentChannel();
        if (tempChannel.isUHD === true ) {
            KTW.managers.service.SimpleMessageManager.showUHDChannelErrorNotiMessage();
            return;
        }

        LayerManager.activateLayer({
            obj: {
                id: "PipMultiViewPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                view : VIEW.popup.PipMultiViewPopup,
                params: {
                    data : {
                        callback_hide : callbackFuncHide,
                        callback_swap : callbackFuncSwap
                    }
                }
            },
            visible: true
        });
    }

    function _activateFourChannel() {
        log.printDbg("_activateFourChannel()");
        var targetChannel;
        var channelController =  window.oipfAdapter.navAdapter.getChannelControl(window.DEF.CONTROL.MAIN, true);

        var result = window.MenuDataManager.searchMenu({
            menuData: window.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id == window.MenuDataManager.MENU_ID.FOUR_CHANNEL_VIEW) {
                    return true;
                }
            }
        })[0];

        var toBeJumpedChannelLocator = "";
        if (result) {

            toBeJumpedChannelLocator = result.locator;
        }
        else {

            toBeJumpedChannelLocator = window.CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE;
        }

        if (toBeJumpedChannelLocator !== null && toBeJumpedChannelLocator !== undefined) {
            targetChannel = window.oipfAdapter.navAdapter.getChannelByTriplet(toBeJumpedChannelLocator);
            channelController.changeChannel(targetChannel);
        }
    }

    function _activateRealTimeChannel() {
        log.printDbg("_activateRealTimeChannel()");

        //var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.STITCHING);

        LayerManager.activateLayer({
            obj: {
                id: Layer.ID.STITCHING,
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                linkage: true,
                params: {
                    fromKey:true
                }
            },
            visible: true,
            cbActivate: function() {

            }
        });
    }

    function _activateFullEpgProgrammeDetailPopup (options, callbackFunc) {
        log.printDbg("_activateFullEpgProgrammeDetailPopup()");

        if (!options || !options.channel || !options.program) {
            return;
        }

        var startTime = options.program.startTime * 1000;
        var endTime = (options.program.startTime + options.program.duration) * 1000;
        var nowTime = (new Date()).getTime();

        options.is_show_reservation = true;
        if (startTime <= nowTime && endTime > nowTime) {
            options.is_show_reservation = false;
        }

        LayerManager.activateLayer({
            obj: {
                id: "KidsEpgDetailPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    data: options,
                    callback: callbackFunc
                }
            },
            moduleId: "module.kids",
            new: true,
            visible: true
        });
    }

    function _favoriteChannelOnOff(curChannel , callbackFunc) {
        window.favoriteChannelManager.favoriteChannelOnOff(curChannel , callbackFunc);
    }

    function _activateFullEpg() {
        /**
         *  Audio FullEpg 띄워야함.
         */
        LayerManager.activateLayer({
            obj: {
                id: Layer.ID.CHANNEL_GUIDE,
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                params: {
                    menuId: window.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST
                }
            },
            visible: true,
            cbActivate: function() {}
        });
    }

    function _activateFullFavoriteChannelEpg() {
        window.favoriteChannelManager.activateFavoriteChannelFullEpg();
    }

    function _activateRelatedMenu(param) {
        // LayerManager.activateLayer({
        //     obj: {
        //         id: "EpgRelatedMenuPopup",
        //         type: Layer.TYPE.POPUP,
        //         priority: Layer.PRIORITY.POPUP,
        //         view : VIEW.popup.EpgRelatedMenuPopup,
        //         params: {
        //             data : param
        //         }
        //     },
        //     visible: true
        // });
        LayerManager.activateLayer({
            obj: {
                id: "KidsEpgDetailPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    programData : param
                }
            },
            moduleId: "module.kids",
            new: true,
            visible: true
        });
    }

    function _deactivateRelatedMenu() {
        LayerManager.deactivateLayer({
            id: "KidsEpgRelatedMenuPopup"
        });
    }

    function _makeMenuInfo(leftMenuInfo , rightMenuInfo) {
        var menuInfo = {
            left_menu_info : leftMenuInfo ,
            right_menu_info : rightMenuInfo
        };

        return menuInfo;
    }

    function _makeLeftMenuInfo(focusChannel , leftMenuCallbackFunc , isShowFavoritedChannel , isShowTwoChannel ,
                               isShowFourChannel , isShowHitChannel , isShowGenreOption , focusOptionIndex) {
        var leftmenuinfo = {
            channel : focusChannel,
            is_ots_channel : window.CONSTANT.IS_OTS ,
            is_show_favorited_channel : (focusChannel === undefined || focusChannel === null) ? false : isShowFavoritedChannel === undefined ? true : isShowFavoritedChannel,
            is_show_2ch : (isShowTwoChannel === undefined  || isShowTwoChannel === null) ? true : isShowTwoChannel,
            is_show_4ch : (isShowFourChannel === undefined || isShowFourChannel === null) ? true : isShowFourChannel,
            is_show_hit : (isShowHitChannel === undefined || isShowHitChannel === null)? true : isShowHitChannel,
            is_show_genre_option : (isShowGenreOption === undefined || isShowGenreOption === null)? true : isShowGenreOption,
            focus_option_index : focusOptionIndex,
            left_menu_callback_func : leftMenuCallbackFunc
        };
        return leftmenuinfo;
    }

    function _makeRightMenuInfo(callerId , fullEpgMenuInfo , miniEpgMenuInfo) {
        var menuinfo = {
            caller : callerId,
            full_epg : fullEpgMenuInfo,
            mini_epg : miniEpgMenuInfo
        };
        return menuinfo;
    }

    function _makeFullEpgMenuInfo(topgenre , middlegenre,bottomgenre , focusGenre , focusIndex , callbackFunc) {
        var menuinfo = {
            top_genre: topgenre,
            middle_genrc: middlegenre,
            bottom_genrc: bottomgenre,
            focus_genrc: focusGenre,
            focus_index: focusIndex,
            fullepg_callback: callbackFunc
        };
        return menuinfo;
    }

    function _makeMiniEpgMenuInfo(topmenuinfo , middlemenuinfo , bottommenuinfo, callbackFunc) {
        var miniepginfo = {
            top_menu : topmenuinfo,
            middle_menu : middlemenuinfo,
            bottom_menu : bottommenuinfo,
            miniepg_callback : callbackFunc
        };
        return miniepginfo;
    }

    function _makeMiniEpgTopMenuInfo(isShowTopMenu , list) {
        var menuinfo = {
            is_show_top_menu : isShowTopMenu ,
            channel_genre : list
        };
        return menuinfo;
    }

    function _makeMiniEpgMiddleMenuInfo(captionmenu , voicemenu , bluetoothvoicemenu , bluetoothlisteningmenu) {
        var menuinfo = {
            caption : captionmenu ,
            voice : voicemenu,
            bluetooth_voice_lang : bluetoothvoicemenu,
            bluetooth_listening : bluetoothlisteningmenu
        };
        return menuinfo;
    }

    function _makeMiniEpgBottomMenuInfo(isShowBottomMenu , list) {
        var menuinfo = {
            is_show_bottom_menu : isShowBottomMenu,
            app_list : list
        };
        return menuinfo;
    }

    function _makeCaptionMenuInfo(isShowCaption , isDimmed) {
        var menuinfo = {
            is_show_caption : isShowCaption ,
            is_dimmed : isDimmed
        };
        return menuinfo;
    }

    function _makeVoiceMenuInfo(isShowVoice , isDimmed , list , focusIndex) {
        var menuinfo = {
            is_show_voice : isShowVoice ,
            is_dimmed : isDimmed,
            voicelist : list,
            voice_focus : focusIndex
        };
        return menuinfo;
    }

    function _makeBlueToothVoiceMenuInfo(isShowBlueToothVoice , isDimmed , list , focusIndex) {
        var menuinfo = {
            is_show_bluetooth_voice_lang : isShowBlueToothVoice ,
            is_dimmed : isDimmed ,
            bluetooth_voice_lang_list : list,
            bluetooth_voice_lang_focus : focusIndex
        };
        return menuinfo;
    }

    function _makeBlueToothListeningMenuInfo(isShowBlueToothListening , isDimmed , list , focusIndex) {
        /**
         * [{name:"혼자듣기" , value : 0} , {name:"같이듣기" , value : 1}]
         */
        var menuinfo = {
            is_show_bluetooth_listening : isShowBlueToothListening ,
            is_dimmed : isDimmed ,
            bluetooth_listening_list : list ,
            bluetooth_listening_focus : focusIndex
        };
        return menuinfo;
    }

    function _makeListObject(objectName , objectValue) {
        var listObject = {
            name: objectName,
            menuid: objectValue
        };
        return listObject;
    }

    return {
        init: _init,
        showFullEpgRelatedMenu: _showFullEpgRelatedMenu,
        activateRelatedMenu: _activateRelatedMenu,
        deactivateRelatedMenu: _deactivateRelatedMenu,
        makeMenuInfo: _makeMenuInfo,
        makeLeftMenuInfo: _makeLeftMenuInfo,
        makeRightMenuInfo: _makeRightMenuInfo,
        makeMiniEpgMenuInfo: _makeMiniEpgMenuInfo,
        makeMiniEpgTopMenuInfo: _makeMiniEpgTopMenuInfo,
        makeMiniEpgMiddleMenuInfo: _makeMiniEpgMiddleMenuInfo,
        makeMiniEpgBottomMenuInfo: _makeMiniEpgBottomMenuInfo,
        makeCaptionMenuInfo: _makeCaptionMenuInfo,
        makeVoiceMenuInfo: _makeVoiceMenuInfo,
        makeBlueToothVoiceMenuInfo: _makeBlueToothVoiceMenuInfo,
        makeBlueToothListeningMenuInfo: _makeBlueToothListeningMenuInfo,
        makeListObject: _makeListObject,
        searchMenu: _searchMenu,
        activateTwoChannel: _activateTwoChannel,
        activateFourChannel: _activateFourChannel,
        activateRealTimeChannel: _activateRealTimeChannel,
        favoriteChannelOnOff: _favoriteChannelOnOff,
        activateFullEpg: _activateFullEpg,
        activateFullFavoriteChannelEpg: _activateFullFavoriteChannelEpg,
        activateFullEpgProgrammeDetailPopup: _activateFullEpgProgrammeDetailPopup
    }


}());

//define constant.
Object.defineProperty(KidsEpgRelatedMenuManager, "MENU_INFO", {
    value : {
        LEFT_MENU : {
            FAVORITED : 0,
            DETAIL : 1,
            TWO_CH : 2,
            FOUR_CH : 3,
            HIT_CH : 4,
            GENRE_OPTION : 5,
            OPTION_TYPE : {
                SORT_START_TIME : 0,
                SORT_TEXT : 1
            }
        },
        RIGHT_MENU : {
            CALLER : {
                FULL_EPG  : 0,
                MINI_EPG : 1
            },
            FULL_EPG : {
                TOP_GENRE : 0,
                MIDDLE_GENRE : 1,
                BOTTOM_GENRE : 2
            },
            MINI_EPG : {
                TOP_MENU : 0,
                MIDDLE_MENU : 1,
                BOTTOM_MENU : 2
            },
            TOP_MENU : {
                TOTAL_CHANNEL : 0,
                F_CHANNEL : 1
            },
            MIDDLE_MENU : {
                CAPTION_MENU : 0,
                VOICE_MENU : 1,
                BLUE_TOOTH_VOICE_MENU : 2,
                BLUE_TOOTH_VOICE_LE_MENU : 3
            },
            BOTTOM_MENU : {
                BOOKMARK : 0,
                HOME_WIDGET : 1
            }
        }
    },
    writable : false,
    configurable : false
});


