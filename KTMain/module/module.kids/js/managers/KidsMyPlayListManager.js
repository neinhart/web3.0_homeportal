/**
 * Created by ksk91_000 on 2017-03-24.
 */
kidsSubHome.KidsMyPlayListManager = new function () {
    var playList;

    var getPlayItemList = function (callback, isKids) {
        if(playList) callback(true, filterKidsPlayList(isKids));
        else kidsSubHome.amocManager.getMyPlayItemList(function(result, data) {
            if(result) {
                playList = data.playItemList;
                if(playList==null) playList = [];
                if(!Array.isArray(playList)) playList = [playList];
                callback(true, filterKidsPlayList(isKids));
            } else callback(false);
        }, "Y");
    };

    var getKidsPlayItemList = function (callback) {
        getPlayItemList(function (res, list) {
            if(res) for (var i = 0; i < list.length; i++) if (list[i].isKidsYn == "Y") callback(true, list[i]);
            else callback(false);
        }, true);
    };

    function filterKidsPlayList(isKids) {
        if(!isKids) {
            var tmp = [];
            for(var i=0; i<playList.length; i++) {
                if(playList[i].isKidsYn=="N") tmp.push(playList[i]);
            } return tmp;
        }else return playList;
    }

    var addPlayItemList = function (itemNm, callback) {
        kidsSubHome.amocManager.setMyPlayItemList(function (res, data) {
            if(res && data.resCode==0) {
                playList.push({playListId: data.playListId, playListNm: itemNm, itemCnt:0, isKidsYn:"N"});
                if(callback) callback(true, playList);
            }else {
                callback(false)
            }
        }, "1", "",  itemNm);
    };

    var deletePlayItemList = function (playListId, callback) {
        kidsSubHome.amocManager.setMyPlayItemList(function (res, data) {
            if(res && data.resCode==0) {
                for(var i=0; i<playList.length; i++) {
                    if(playList[i].playListId===playListId) {
                        playList.splice(i, 1);
                        break;
                    }
                }

                if(callback) callback(true, playList);
            }else {
                callback(false)
            }
        }, "2", playListId,  "");
    };

    var editPlayItemList = function (playListId, listNm, callback) {
        kidsSubHome.amocManager.setMyPlayItemList(function (res, data) {
            if(res) {
                playList.find(function (list) { return list.playListId===playListId; }).playListNm = listNm;
                if(callback) callback(true, playList);
            }else {
                callback(false)
            }
        }, "3", playListId, listNm);
    };

    var addPlayItemDetList = function(callback, playListId, contsId, itemType, hdYn) {
        kidsSubHome.amocManager.setMyPlayItemDetList(function (res, data) {
            if(res){
                if(data.resCode==0){
                    var tmp = playList.find(function (list) { return list.playListId===playListId; });
                    showToast("'" + tmp.playListNm + "'에 추가 하였습니다");
                    tmp.isUpdated = false;
                    tmp.itemCnt++;
                    callback(true);
                } else if(data.resCode==1) {
                    showToast("개수 초과! 마이 플레이리스트 콘텐츠 등록은 최대 20개까지 가능합니다");
                    callback(false);
                }
            } else callback(false)
        }, 0, playListId, 1, contsId, "", itemType, hdYn);
    };

    var setMyPlayItemDetList = function(callback, playListId, itemCnt, contsId) {
        kidsSubHome.amocManager.setMyPlayItemDetList(function (res, response) {
            if(res && response.resCode==0) {
                var tmp = playList.find(function (list) { return list.playListId===playListId; });
                showToast("'" + tmp.playListNm + "'에서 삭제되었습니다");
                tmp.itemCnt-=itemCnt;
                callback(true);
            } else callback(false);
        }, 1, playListId, itemCnt, null, contsId);
    };

    var clearPlayListCache = function () {
        playList = null;
    };

    var getPlayListData = function(playListId) {
        return playList.find(function (list) { return list.playListId===playListId; });
    };

    var isRefresh = false;

    var setIsPlaylistRefresh = function (value) {
        isRefresh = value;
    };

    var getIsPlaylistRefresh = function () {
        return isRefresh;
    };

    return {
        getPlayItemList: getPlayItemList,
        getKidsPlayItemList : getKidsPlayItemList,
        addPlayItemList: addPlayItemList,
        deletePlayItemList: deletePlayItemList,
        editPlayItemList: editPlayItemList,
        addPlayItemDetList: addPlayItemDetList,
        setMyPlayItemDetList: setMyPlayItemDetList,
        clearPlayListCache: clearPlayListCache,
        getPlayListData: getPlayListData,
        setIsPlaylistRefresh : setIsPlaylistRefresh,
        getIsPlaylistRefresh : getIsPlaylistRefresh
    }
};