/**
 * Created by Yun on 2017-04-06.
 */

window.KidsTimerCheckingManager = (function () {
    var checkTimeout;
    var alreadyTimeout;

    var limitText = null;
    var isNotOff = false;

    var selLimitIdx = 0;
    var selLimitOp_idx = [0, 0, 0];
    var vodModule;
    var isVodPlaying;

    function _setSelectLimitIndex(value) {
        selLimitIdx = value;
    }

    function _setSelectLimitOptionIndex(index, value) {
        selLimitOp_idx[index] = value;
    }

    function _settingTimerChecking(data) {
        isNotOff = false;
        if (checkTimeout) {
            clearTimeout(checkTimeout);
            checkTimeout = setTimeout(function() {
                _endTimerChecking();
                clearTimeout(checkTimeout);
                checkTimeout = void 0;
            }, converterTimeValue(data) * 1000);
            clearTimeout(alreadyTimeout);
            alreadyTimeout = setTimeout(function() {
                if (!isNotOff) {
                    LayerManager.activateLayer({
                        obj: {
                            id: "KidsNotice",
                            type: Layer.TYPE.BACKGROUND,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                isTimeCheck: true
                            }
                        },
                        moduleId: "module.kids",
                        new: true,
                        visible: true
                    });
                }
                clearTimeout(alreadyTimeout);
                alreadyTimeout = void 0;
            }, (converterTimeValue(data) * 1000) - 300000);
        } else {
            checkTimeout = setTimeout(function() {
                _endTimerChecking();
                clearTimeout(checkTimeout);
                checkTimeout = void 0;
            }, converterTimeValue(data) * 1000);
            alreadyTimeout = setTimeout(function() {
                if (!isNotOff) {
                    LayerManager.activateLayer({
                        obj: {
                            id: "KidsNotice",
                            type: Layer.TYPE.BACKGROUND,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                isTimeCheck: true
                            }
                        },
                        moduleId: "module.kids",
                        new: true,
                        visible: true
                    });
                    clearTimeout(alreadyTimeout);
                    alreadyTimeout = void 0;
                }
            }, (converterTimeValue(data) * 1000) - 300000);
        }
    }

    function _resetTimer() {
        isNotOff = true;
        selLimitIdx = 0;
        selLimitOp_idx = [0, 0, 0];
        if (checkTimeout) {
            clearTimeout(checkTimeout);
            checkTimeout = void 0;
        }
        if (alreadyTimeout) {
            clearTimeout(alreadyTimeout);
            alreadyTimeout = void 0;
        }
        window.KidsTimerCheckingManager.settingLimitWatchingText(null);
    }

    function converterTimeValue(value) {
        var hour = value.hour * 3600;
        var min = value.min * 60;
        return hour+min;
    }

    function _endTimerChecking() {
        if (!isNotOff) {
            vodPlayCheck("isVODPlaying");
            LayerManager.activateLayer({
                obj: {
                    id: "KidsWatchingClose",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        isVodEndTime : (window.stateManager.isVODPlayingState() == true || isVodPlaying == "1")
                    }
                },
                moduleId: "module.kids",
                new: true,
                visible: true
            });
        }
    }

    function vodPlayCheck(_method) {
        vodModule = window.ModuleManager.getModule("module.vod");
        if (!vodModule) {
            window.ModuleManager.loadModule({
                moduleId: "module.vod",
                cbLoadModule: function (success) {
                    if (success) vodPlayCheck(_method);
                }
            });
        } else {
            isVodPlaying = vodModule.execute({method: _method, params: {}});
        }
    }

    return {
        setSelectLimitIndex: _setSelectLimitIndex,
        setSelectLimitOptionIndex: _setSelectLimitOptionIndex,
        getSelectLimitIndex: function() {return selLimitIdx;},
        getSelectLimitOptionIndex: function(index) {return selLimitOp_idx[index];},
        settingLimitWatchingText: function (value) { limitText = value; },
        getLimitWatchingText: function () {return limitText;},
        resetTimer: _resetTimer,
        settingTimerChecking: _settingTimerChecking,
        endTimerChecking: _endTimerChecking
    }
}());