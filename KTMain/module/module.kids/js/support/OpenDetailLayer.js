/**
 * Created by ksk91_000 on 2016-10-31.
 */
window.openDetailLayer = function(cateId, contsId, reqPathCd, contentsData, isKids, extraParams) {
    LayerManager.startLoading({preventKey:true});
    try {
        if (contentsData && contentsData.itemType == 1) cateId = contsId;
        var a = ModuleManager.getModule("module.vod");
        if (!a) {
            ModuleManager.loadModule({
                moduleId: "module.vod",
                cbLoadModule: function (success) {
                    if (success) openDetailLayer(cateId, contsId, reqPathCd, contentsData, isKids, extraParams);
                }
            });
            return;
        } else if (contentsData && !contentsData.subHomeItemType) {
            var params = {
                cat_id: cateId,
                const_id: contsId,
                req_cd: reqPathCd,
                cateInfo: contentsData,
                contsInfo: contentsData,
                isKids: isKids,
                callback: function() {
                    log.printDbg("[SK] callback stopLoading!!");
                    LayerManager.stopLoading();
                }
            };
            for(var key in extraParams) params[key] = extraParams[key];
            a.execute({
                method: "showDetailWithData",
                params: params
            });
        } else {
            var params = {
                cat_id: cateId,
                const_id: contsId,
                req_cd: reqPathCd,
                isKids: isKids,
                callback: function() {
                    log.printDbg("[SK] callback stopLoading!!");
                    LayerManager.stopLoading();
                }
            };
            for(var key in extraParams) params[key] = extraParams[key];
            a.execute({
                method: "showDetail",
                params: params
            });
        }
    } catch (e) {
        LayerManager.stopLoading();
    }
};