window.PosterFactory = new function() {
    this.getVerticalPoster = function(id, data) {
        var element = _$("<li id='" + id + "' class='content poster_vertical'>"+
            "<div class='content'>"+
            // "<img src='" + modulePath + "resource/image/posterList/poster_sdw_w210.png' class='posterSdw'>"+
            // "<img src='" + modulePath + "resource/img/line_poster_vodlist.png' class='posterImg'>"+
            "<img src='" + data.imgUrl + "' class='posterImg poster'>"+
            "<img src='" + modulePath + "resource/image/posterList/sdw_category_poster.png' class='previewSdw'>"+
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span>" + data.contsName + "</span></div>"+
            "<span class='posterDetail'>"+
            // "<span class='stars'>";
            // var i=0;
            // for(;i<data.rate;i++)
            //     html+="<img src='" + modulePath + "resource/img/img_rating_filled.png'>";
            // for(;i<5;i++)
            //     html+="<img src='" + modulePath + "resource/img/img_rating_gray.png'>";
            // html+="</span>"+
            (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo-0) + ".png' class='age'>"+
            "</span>"+
            "</div>"+
            "</li>");
        return element;
    };

    this.getVerticalPoster_more = function(id, data) {
        var html = _$("<li id='" + id + "' class='content poster_vertical more'>"+
            "<div class='content'>"+
            // "<img src='" + modulePath + "resource/image/posterList/poster_sdw_w210.png' class='posterSdw'>"+
            // "<img src='" + modulePath + "resource/img/line_poster_vodlist.png' class='posterImg'>"+
            "<div class='posterImgWrapper'>"+
            "<img src='" + data.imgUrl + "' class='posterImg poster'>"+
            "</div>"+
            "<div class='contentsData'>"+
            "<span class='moreViewTxt'>전체보기</span>"+
            "<span>"+
            "<span class='moreViewCnt'>" + data.moreCnt + "</span>"+
            "</span>"+
            "</div>"+
            "</div>");
        return html;
    };

    this.getBanner_Big_1 = function(id, data) {
        var html = "<li id = '" + id + "' class='content banner_big_1'>"+
            "<div class='content'>"+
            "<img src='" + modulePath + "resource/img/sdw_banner_genre_movie.png' class='posterSdw'>"+
//                            "<img src='" + modulePath + "resource/img/line_banner_movie_case2.png' class='posterImg'>"+
            "<img src='" + data.imgSrc + "' class='posterImg poster'>"+
            "<span class='posterTitle'>" + data.title + "</span>"+
            "<span class='posterDescription'>" + data.desc + "</span>"+
            "</div>"+
            "</li>"
        return html;
    }

    this.getBanner_Big_3 = function(id, data) {
        var html = "<li id = '" + id + "' class='content banner_big_3'>"+
            "<div class='content'>"+
            "<img src='" + modulePath + "resource/img/sdw_banner_genre_movie.png' class='posterSdw'>"+
            "<img src='" + modulePath + "resource/img/line_banner_movie_case2.png' class='posterImg'>"+
            "<img src='" + modulePath + "resource/img/sdw_category_poster.png' class='previewSdw'>"+
            "<img src='" + data.imgSrc + "' class='posterImg poster'>"+
            "<span class='posterTitle'>" + data.title + "</span>"+
            "<span class='posterDescription'>" + data.desc + "</span>"+
            "</div>"+
            "</li>"
        return html;
    }

    this.getBanner_Mini_1 = function(id, data) {
        var html = "<li id = '" + id + "' class='content banner_mini_1'>"+
            "<div class='content'>"+
            "<img src='" + modulePath + "resource/img/sdw_banner_genre_movie.png' class='posterSdw'>"+
//                            "<img src='" + modulePath + "resource/img/line_banner_movie_case2.png' class='posterImg'>"+
            "<img src='" + data.imgSrc + "' class='posterImg poster'>"+
            "<span class='posterTitle'>" + data.title + "</span>"+
            "<span class='posterDescription'>" + data.desc + "</span>"+
            "</div>"+
            "</li>"
        return html;
    }

}