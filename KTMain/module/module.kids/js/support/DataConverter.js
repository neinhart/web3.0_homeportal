/**
 * Created by ksk91_000 on 2016-12-05.
 */
window.DataConverter = new function () {
    function getView(parentValue, VCategoryObj, callback, _kidsArea, _viewMgr) {
        if (VCategoryObj.catType == "KidsSub") {
            if (VCategoryObj.isFirstKidsSubHome) {
                RecentlyListManager.setKidsSubHomeId(VCategoryObj.parent.id);
                RecentlyListManager.getRecentlyData(function(contentsData) {
                    var view = new kidsSubHome.view.KidsSubHomeView(VCategoryObj.id, contentsData);
                    view.title = HTool.getMenuName(VCategoryObj);
                    view.create(VCategoryObj);
                    callback(view);
                }, false, VCategoryObj.parent.id);
            }
            else {
                var view = new kidsSubHome.view.KidsSubHomeView(VCategoryObj.id);
                view.title = HTool.getMenuName(VCategoryObj);
                view.create(VCategoryObj);
                callback(view);
            }
        } else if (VCategoryObj.id == MenuDataManager.MENU_ID.KIDS_CHANNEL) {
            var view;
            if (window.CONSTANT.IS_OTS) {
                if (window.CONSTANT.IS_DCS) {
                    view = new KidsEpgViewDCS({
                        menuId: MenuDataManager.MENU_ID.KIDS_CHANNEL,
                        parent: parentValue,
                        kidsArea: _kidsArea,
                        categoryData: VCategoryObj,
                        viewManager: _viewMgr
                    });
                } else {
                    view = new KidsEpgViewOTS({
                        menuId: MenuDataManager.MENU_ID.KIDS_CHANNEL,
                        parent: parentValue,
                        kidsArea: _kidsArea,
                        categoryData: VCategoryObj,
                        viewManager: _viewMgr
                    });
                }
            } else {
                view = new KidsEpgViewOTV({
                    menuId: MenuDataManager.MENU_ID.KIDS_CHANNEL,
                    parent: parentValue,
                    kidsArea: _kidsArea,
                    categoryData: VCategoryObj,
                    viewManager: _viewMgr
                });
            }
            view.create();
            view.viewId = MenuDataManager.MENU_ID.KIDS_CHANNEL;
            callback(view);
        } else if (VCategoryObj.id == MenuDataManager.MENU_ID.KIDS_PLAY_LIST) {
            if (kidsSubHome.KidsMyPlayListManager.getIsPlaylistRefresh()) {
                kidsSubHome.KidsMyPlayListManager.clearPlayListCache();
            }
            var view = new kidsSubHome.view.KidsMyPlayListView(VCategoryObj.id);
            kidsSubHome.KidsMyPlayListManager.getKidsPlayItemList(function (result, data) {
                if (result) {
                    view.title = HTool.getMenuName(VCategoryObj);
                    view.create({id: data.playListId, name: data.playListNm, itemCnt: data.itemCnt});
                    callback(view);
                }
            });
        }else if(VCategoryObj.catType == "Rank" || VCategoryObj.catType == "RankW3") {
            var view = new kidsSubHome.view.KidsRankCategoryView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        } else if (VCategoryObj.children && VCategoryObj.listType == 4) { // 캐릭터 총집합 "10000000000000110407"
            var view = new kidsSubHome.view.CharacterListContentsView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create({categoryData: VCategoryObj, characterListData: VCategoryObj.children});
            callback(view);
        } else if (VCategoryObj.parent && VCategoryObj.parent.listType == 4) { // 캐릭터 총집합 하위 리스트 "10000000000000110407"
            if (VCategoryObj.children) {
                var view = new kidsSubHome.view.CharacterTheaterView(VCategoryObj.id);
                view.title = HTool.getMenuName(VCategoryObj);
                kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                    view.create({categoryData: VCategoryObj.children, dataList: response.itemDetailList, w3ListImg: VCategoryObj.w3ListImgUrl});
                    callback(view);
                }, VCategoryObj.id, 1, "A");
            } else {
                var view = new kidsSubHome.view.KidsCharacterContentsView(VCategoryObj.id);
                view.title = HTool.getMenuName(VCategoryObj);
                kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                    view.create({categoryData: VCategoryObj, dataList: response.itemDetailList});
                    callback(view);
                }, VCategoryObj.id, 1, "A");
            }
        } else if (VCategoryObj.catType == "PWorld") {
            var view = new kidsSubHome.view.ParentsWorldView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        } else if (VCategoryObj.children) {
            //카테고리 리스트
            var view = new kidsSubHome.view.categoryView(VCategoryObj.id);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        } else {
            //아이템 목록.
            if (VCategoryObj.itemType == 3 || VCategoryObj.itemType == 7 || VCategoryObj.itemType == 8) {
                var view = new kidsSubHome.view.AppContentsView(VCategoryObj.id);
                view.title = HTool.getMenuName(VCategoryObj);
                view.create(VCategoryObj);
                callback(view);
            } else {
                var view = new kidsSubHome.view.ContentsView(VCategoryObj.id);
                view.title = HTool.getMenuName(VCategoryObj);
                kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                    if (result) {
                        view.create({categoryData: VCategoryObj, dataList: response.itemDetailList});
                        callback(view);
                    }
                }, VCategoryObj.id, 1, 15);
            }
        }
    }
    return {
        getView:getView
    }
};