/**
 * Created by ksk91_000 on 2017-03-24.
 */
kidsSubHome.HomeShot = function() {
    var div = _$("<div/>", {class: "homeShot_area"});

    var arrHomeShotData = null;
    var curHomeShotIndex = 0;
    var homeShotTimer = null;

    /**
     * [dj.son] 키즈 화면에서는 default 홈샷을 노출해야 할때, 캐릭터 화면을 노출해야 함. 따라서 kidsCharShot 을 show / hide 하는 구문 추가
     */
    var kidsCharShot = null;

    div.append("" +
        "<div class='homeShot'>" +
        "<img class='home_shot'>" +
        "<img src='" + modulePath + "resource/image/btn_quickview.png' class='shortcut'>" +
        "</div>");
    // div.append("<div class='homeShot_bar'></div>");

    function clearHomeShotTimer () {
        clearTimeout(homeShotTimer);
        homeShotTimer = null;
    }

    function drawHomeShot () {
        if (arrHomeShotData.length === 0) {
            div.find(".homeShot").hide();
            div.find(".homeShot_bar").hide();

            if (kidsCharShot) {
                kidsCharShot.getView().show();
            }
        }
        else {
            var homeShot = arrHomeShotData[curHomeShotIndex];

            div.find(".homeShot_bar").show();
            div.find(".homeShot .home_shot").error(function () {
                div.find(".homeShot_bar").hide();

                if (kidsCharShot) {
                    kidsCharShot.getView().show();
                }
            }).attr("src", homeShot.hsImgUrl);
            div.find(".homeShot").show();

            if (kidsCharShot) {
                kidsCharShot.getView().hide();
            }
        }
    }

    function updateHomeShot () {
        drawHomeShot();

        if (arrHomeShotData.length > 0) {
            var homeShot = arrHomeShotData[curHomeShotIndex];

            var time = homeShot.hsDurationTime;

            homeShotTimer = setTimeout(function () {
                if (homeShot.hsType === HomeShotDataManager.TYPE.CATEGORY && homeShot.hsDisplayCnt !== "A") {
                    ++homeShot.hsRealDisplayCnt;

                    HomeShotDataManager.updateShowtCategoryHomeShot({
                        catId: "root",
                        transactionId: homeShot.transactionId,
                        hsRealDisplayCnt: homeShot.hsRealDisplayCnt
                    });

                    if (homeShot.hsDisplayCnt <= homeShot.hsRealDisplayCnt) {
                        arrHomeShotData.splice(curHomeShotIndex, 1);
                        --curHomeShotIndex;
                    }
                }

                ++curHomeShotIndex;

                if (curHomeShotIndex > arrHomeShotData.length - 1) {
                    curHomeShotIndex = 0;
                }

                if(arrHomeShotData.length > 1) {
                    updateHomeShot();
                }
            }, time * 1000);
        }
    }

    var setHomeShot = function (options) {
        log.printDbg("setHomeShot() " + log.stringify(options));

        if (!options || !options.bShow || !options.arrHomeShotData) {
            div.hide();
            clearHomeShotTimer();
        }
        else {
            div.show();

            arrHomeShotData = options.arrHomeShotData;
            curHomeShotIndex = 0;

            clearHomeShotTimer();

            updateHomeShot();
        }
    };

    var getView = function () {
        return div;
    };

    var execute = function () {
        var homeShot = arrHomeShotData[curHomeShotIndex];

        if (homeShot) {
            var options = {
                itemType: homeShot.hsTargetType,
                req_cd: homeShot.reqCd ? homeShot.reqCd : 25
            };

            switch (homeShot.hsTargetType) {
                case MENU_CONSTANT.ITEM.CATEGORY:  //카테고리
                    var menuData = MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(),
                        allSearch: false,
                        cbCondition: function (menu) {
                            if (menu.id == homeShot.hsTargetCatId) return true;
                        }
                    })[0];

                    if (!!menuData) {
                        MenuServiceManager.jumpMenu({menu: menuData});
                        // 카테고리 홈샷 로그 적재 추가
                        try {
                            NavLogMgr.collectJump(NLC.JUMP_START_CATE, NLC.JUMP_DEST_CATEGORY, menuData.id, "", "", "");
                        } catch(e) {}
                    }
                    else {
                        showToast("잘못된 접근입니다");
                    }
                    return true;
                case MENU_CONSTANT.ITEM.SERIES:  // 시리즈
                case MENU_CONSTANT.ITEM.CONTENT:  // 단편
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_CATE, homeShot.hsTargetCatId, homeShot.hsTargetAssetId, "25");
                    }
                    catch(e) {}

                    openDetailLayer(homeShot.hsTargetCatId, homeShot.hsTargetAssetId, "25");
                    return true;
                case MENU_CONSTANT.ITEM.INT_M:     //멀티캐스트
                case MENU_CONSTANT.ITEM.INT_U:    //유니캐스트 양방향 서비스
                    options.locator = homeShot.hsTargetLocator;
                    options.parameter = homeShot.hsTargetParameter;

                    MenuServiceManager.jumpByMenuData(options);

                    // 로그 적재 추가
                    try {
                        NavLogMgr.collectJump(NLC.JUMP_START_CATE, NLC.JUMP_DEST_INTERACTIVE,
                            "", "", "", options.locator);
                    } catch(e) {}

                    return true;
                case MENU_CONSTANT.ITEM.INT_W:    //웹뷰
                    options.locator = homeShot.hsTargetLocator;

                    MenuServiceManager.jumpByMenuData(options);
                    return true;
            }
        }

        return false;
    };

    function _setKidsCharShot (tmpKidsCharShot) {
        kidsCharShot = tmpKidsCharShot;
    }

    setHomeShot();

    return {
        getView: getView,
        setHomeShot: setHomeShot,
        execute: execute,
        setKidsCharShot: _setKidsCharShot
    }
};