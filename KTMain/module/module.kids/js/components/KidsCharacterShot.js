/**
 * Created by Yun on 2017-03-31.
 */

kidsSubHome.KidsCharacterShot = function () {
    var div = _$("<div/>", {class: "kidsCharacterShot_area"});
    var parent;

    div.append(_$("<img/>", {class: "kidsCharacterShot_BG_0", src: window.modulePath + "resource/image/object_island.png"}));
    /*div.append(_$("<img/>", {class: "kidsCharacterShot_BG_1", src: window.modulePath + "resource/image/object_island_3.png"}));*/
    div.append(_$("<img/>", {class: "kidsCharacterShot_character"}));
    var setKidsCharacterShot = function () {
        if (window.KidsCharacterManager.getIsCallCharacterInfo()) {
            window.KidsCharacterManager.getKidsInfo(function (data, result) {
                if (result) {
                    window.KidsCharacterManager.setIsCallCharacterInfo(false);
                    if (data.resultCd == 0) {
                        if (data.data != null && data.data.imgUrl) {
                            window.KidsCharacterManager.setSelectCharacterData(data.data);
                            KidsCharacterManager.setIsSelectCharacter(true);
                            parent.div.find(".kidsCharacterShot_character").attr("src", data.data.endImgUrl);
                        } else {
                            window.KidsCharacterManager.setSelectCharacterData("");
                            parent.div.find(".kidsCharacterShot_character").attr("src", window.modulePath + "resource/image/character_default.png");
                            KidsCharacterManager.setIsSelectCharacter(false);
                        }
                    } else {
                        window.KidsCharacterManager.setSelectCharacterData("");
                        parent.div.find(".kidsCharacterShot_character").attr("src", window.modulePath + "resource/image/character_default.png");
                        KidsCharacterManager.setIsSelectCharacter(false);
                    }
                } else {
                    window.KidsCharacterManager.setIsCallCharacterInfo(true);
                    window.KidsCharacterManager.setSelectCharacterData("");
                    parent.div.find(".kidsCharacterShot_character").attr("src", window.modulePath + "resource/image/character_default.png");
                    KidsCharacterManager.setIsSelectCharacter(false);
                }
            });
        } else {
            var tmpCharData = window.KidsCharacterManager.getSelectCharacterData();
            if (tmpCharData != null && tmpCharData.imgUrl) {
                KidsCharacterManager.setIsSelectCharacter(true);
                parent.div.find(".kidsCharacterShot_character").attr("src", tmpCharData.endImgUrl);
            } else {
                window.KidsCharacterManager.setSelectCharacterData("");
                parent.div.find(".kidsCharacterShot_character").attr("src", window.modulePath + "resource/image/character_default.png");
                KidsCharacterManager.setIsSelectCharacter(false);
            }
        }
    };

    var getView = function() {
        return div;
    };

    return {
        setParent: function(_parent) {parent = _parent;},
        getView: getView,
        setKidsCharacterShot: setKidsCharacterShot
    }
};