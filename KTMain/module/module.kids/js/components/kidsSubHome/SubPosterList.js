/**
 * Created by ksk91_000 on 2016-06-24.
 */

window.SubPosterList = function(menuId) {
    var componentId = menuId;
    var data = [];
    var length;
    var viewCnt, isMore;
    var posterType;

    var instance = this;
    var isShowTitle;
    var focus;
    var isFocus = false;
    var view = _$("<div/>", {id:menuId, class:"contentsList"});

    (function createDom() {
        view.append(_$("<span/>", {class:'componentTitle'}));
        view.append(_$("<ul/>", {class:'sub_posterList'}));
    })();

    this.setData = function(contentsData) {
        isFocus = false;
        view.find(".componentTitle").text(contentsData[0].recomTitle);
        titleUpCnt = Math.floor(UTIL.getTextLength(contentsData[0].recomTitle, "RixHead B", 27) /246);
        data = contentsData;
        length = contentsData.length;
        if(length<=0) return false;

        setPosterType(contentsData);

        for(var i=0; i<length && i<(isMore?viewCnt-1:viewCnt); i++) {
            var content = data[i];
            view.find("ul.sub_posterList").append(getPoster(componentId + "_" + i, content));
            if (HTool.isTrue(content.wonYn)) {
                view.find("img.isfreeIcon").show();
                view.find("span.isfree").hide();
            } else if (HTool.isFalse(content.wonYn)) {
                view.find("span.isfree").show();
                view.find("span.isfree").text("무료");
                view.find("img.isfreeIcon").hide();
            } else {
                view.find("img.isfreeIcon").hide();
                view.find("span.isfree").hide
            }
        } if(isMore) {
            data[viewCnt-1].moreCnt = length;
            view.find("ul.sub_posterList").append(getPoster_more(componentId + "_" + (viewCnt-1), data[viewCnt-1]));
        } return true;
    };

    function setPosterType(data) {
        switch (data[0].imgType-0) {
            case 0: // 세로형 포스터
            case 1: // 가로형 포스터
                posterType = "vertical";
                viewCnt = 5;
                isMore = data.length>5;
                break;
            case 2: //정사각 포스터
                posterType = "square";
                viewCnt = 4;
                isMore = data.length>4;
                break;
        }
    }

    this.getView = function() {
        return view;
    };

    this.titleHide = function() {
        view.find(".sub_posterList .content .contentsData").css("visibility", "hidden");
        view.find(".sub_posterList .content.more .contentsData").css("visibility", "inherit");
    };

    this.titleShow = function() {
        view.find(".sub_posterList .contentsData").css("visibility", "inherit");
    };

    var setFocus = function(index) {
        _$(".sub_posterList li.focus").removeClass("focus");
        _$("#" + componentId + "_" + index).addClass("focus");

        if(index<=titleUpCnt) view.find(".componentTitle").css("-webkit-transform", "translateY(-26px)");
        else view.find(".componentTitle").css("-webkit-transform", "");

        if (!isMore || focus<viewCnt-1) {
            var menuTxt = (KIDS_MODE_MANAGER.isKidsMode())? "옵션" : "찜<bar></bar>플레이리스트 추가";
            instance.parent.div.find(".relationBtn_area .relationBtn_text").html(menuTxt);
            //instance.parent.div.find(".relationBtn_area").css("left", (KIDS_MODE_MANAGER.isKidsMode() ? "1635px" : "1590px"));
        } else {
            instance.parent.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
            //instance.parent.div.find(".relationBtn_area").css("left", "1677px");
        }

        setTextAnimation(index);
    };

    function setTextAnimation(focus) {
        UTIL.clearAnimation(view.find(".textAnimating span"));
        view.find(".textAnimating").removeClass("textAnimating");
        void view[0].offsetWidth;
        if(UTIL.getTextLength(data[focus].itemName, "RixHead L", 30)>215) {
            var posterDiv = view.find("#" + componentId + "_" + focus + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.onKeyAction = function(keyCode) {
        if(!this.isParent && this.parent.contextMenu.isOpen()) return this.parent.contextMenu.onKeyAction(keyCode);
        switch(keyCode) {
            case KEY_CODE.LEFT:
                if (focus > 0) {
                    focus --;
                    setFocus(focus);
                    return true;
                } else {
                    this.blurred();
                    return false;
                }
                // focus = HTool.getIndex(focus, -1, Math.min(viewCnt, length));
            case KEY_CODE.RIGHT:
                if (focus+1 == Math.min(viewCnt, length)) return false;
                setFocus(++focus);
                return true;
            case KEY_CODE.ENTER:
                if (focus==viewCnt-1 && isMore) execute({method: "kidsShowMassList", dataList: data});
                else goToDetailLayer(data[focus]);
                return true;
            case KEY_CODE.CONTEXT:
                if (this.isParent) return false;
                if (focus==viewCnt-1 && isMore) return false;
                else {
                    this.parent.contextMenu.setVodInfo({
                        itemName: data[focus].contsName||data[focus].itemName,
                        imgUrl: data[focus].imgUrl,
                        contsId: data[focus].itemId,
                        itemType: data[focus].itemType,
                        cmbYn: data[focus].cmbYn,
                        resolCd: data[focus].resolCd,
                        prInfo: data[focus].prInfo,
                        isKids: true
                    });
                    this.parent.contextMenu.open();
                    return true;
                }
        } return false;
    };

    function goToDetailLayer(data) {
        switch (data.itemType-0) {
            case 0:
                var MenuDataManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.MENU_DATA_MANAGER);
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) {
                        if (menu.id == data.itemId) return true;
                    }
                })[0];
                if (menuData != undefined) {
                    MenuServiceManager.jumpMenu({menu: menuData});
                } else {
                    if(KIDS_MODE_MANAGER.isKidsMode()) KIDS_MODE_MANAGER.activateCanNotMoveMenuInfoPopup();
                    else showToast("잘못된 접근입니다");
                }
                break;
            case 1:    //시리즈
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null,data.itemId, "67");
                } catch(e) {}
                openDetailLayer(data.itemId, null, "67", "", true);
                break;
            case 2:    //컨텐츠
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.parentCatId, data.itemId, "67");
                } catch(e) {}
                openDetailLayer(null, data.itemId, "67", "", true);
                break;
            case 3:    //멀티캐스트 양방향 서비스
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                        data.parentCatId, data.itemId, "", data.locator);
                } catch(e) {}

                var nextState;
                if(data.locator == CONSTANT.APP_ID.MASHUP)
                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                else
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                window.AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        type: window.CONSTANT.APP_TYPE.MULTICAST,
                        param: data.locator,
                        ex_param: data.parameter
                    }
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                        data.parentCatId, data.itemId, "", data.locator);
                } catch(e) {}
                var nextState = stateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: data.id
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(data.locator);
                break;
        }
    }

    this.isLastFocused = function () { return focus === Math.min(viewCnt, length) - 1; };
    this.isFirstFocused = function () { return focus === 0; };

    this.focused = function (keyCode) {
        if(keyCode===KEY_CODE.BLUE) focus = Math.min(viewCnt, length) - 1;
        else focus = 0;
        isFocus = true;
        setFocus(focus);
        if (!isMore || focus<viewCnt-1) {
            var menuTxt = (KIDS_MODE_MANAGER.isKidsMode())? "옵션" : "찜<bar></bar>플레이리스트 추가";
            instance.parent.div.find(".relationBtn_area .relationBtn_text").html(menuTxt);
            //[sw.nam] css left 조절 할 필요가 없어보여 기존 코드 주석 처리
           // instance.parent.div.find(".relationBtn_area").css("left", (KIDS_MODE_MANAGER.isKidsMode() ? "1635px" : "1590px"));
        } else {

            instance.parent.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
            //[sw.nam] css left 조절 할 필요가 없어보여 기존 코드 주석 처리
           // instance.parent.div.find(".relationBtn_area").css("left", "1677px");
        }
        return true;
    };

    this.blurred = function () {
        isFocus = false;
        view.find(".componentTitle").css("-webkit-transform", "");
        _$(".sub_posterList li.focus").removeClass("focus");
        UTIL.clearAnimation(view.find(".textAnimating span"));
        view.find(".textAnimating").removeClass("textAnimating");
        instance.parent.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));

        //[sw.nam] 연관메뉴 의 left css 값을 조절할 필요가 없어보여 주석처리 함
        //instance.parent.div.find(".relationBtn_area").css("left", "1677px");
        // instance.parent.viewMgr.changeBackground(null, 0);
    };

    var getPoster = function(id, data) {
        var element = _$("<li id='" + id + "' class='content poster_" + posterType + "'>"+
            "<div class='content'>"+
            "<img src='" + data.imgUrl + "?w=210&h=300&quality=90" + "' class='posterImg poster'>"+
            "<div class='over_dim'/>"+
            "<div class='icon_area'/>" +
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span>" + (data.contsName||data.itemName) + "</span></div>"+
            "<span class='posterDetail'>"+
            "<span class='stars'/>" +
            (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/image/icon_age_list_" + UTIL.transPrInfo(data.prInfo) + ".png' class='age'>"+
            "</span>"+
            "</div>"+
            "</li>");
        element.find(".posterDetail").prepend(stars.getView(stars.TYPE.RED_20));
        stars.setRate(element.find(".rating_star_area"), data.mark);
        setIcon(element.find(".icon_area"), data);
        return element;
    };

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.newHot) {
            case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
            case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
            case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
            case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
            case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
        } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.isHd=="UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.isDvdYn=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        } if(UTIL.isLimitAge(data.prInfo)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    var getPoster_more = function(id, data) {
        var html = _$("<li id='" + id + "' class='content poster_" + posterType + " more'>"+
            "<div class='content'>"+
            // "<img src='" + modulePath + "resource/image/posterList/poster_sdw_w210.png' class='posterSdw'>"+
            // "<img src='" + modulePath + "resource/img/line_poster_vodlist.png' class='posterImg'>"+
            "<div class='posterImgWrapper'>"+
            "<img src='" + data.imgUrl + "' class='posterImg poster'>"+
            "</div>"+
            "<div class='contentsData'>"+
            "<span class='moreViewTxt'>전체보기</span>"+
            "<span>"+
            "<span class='moreViewCnt'>" + data.moreCnt + "</span>"+
            "</span>"+
            "</div>"+
            "</div>");
        return html;
    };

    this.setFocusIndex = function (index) {
        focus = index;
        setFocus(focus);
    };

    this.getFocusIndex = function () {
        return focus;
    };

    this.pause = function () {
        if (!isFocus) return;
        setFocus(focus);
        UTIL.clearAnimation(view.find(".textAnimating span"));
        view.find(".textAnimating").removeClass("textAnimating");
    };

    this.resume = function () {
        if (!isFocus) return;
        setTextAnimation(focus);
    };

    this.getHeight = function() {
        return 480;
    };

    this.getIsRecentlyList = function () {
        return false;
    };
};