/**
 * Created by ksk91_000 on 2016-12-07.
 * SubPosterList와 같은 Poster List로 현재 사용 안함 추후 제거 필요(2017/06/30)
 */
window.SubHomeMoreList = function() {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var isFocused = false;
    var lineVisibleTimeout;
    var posterType, lineCnt, pageCnt;

    this.create = function (cateId, imgType) {
        this.cateId = cateId;
        setPosterType(imgType);
        this.div = _$("<ul/>", {class: "contentsList " + posterType + "_poster_list"});
        for (var i = 0; i < lineCnt * (pageCnt * 2 + 2); i++)
            this.div.append(getPosterDiv());
    };

    function getPosterDiv() {
        var poster = _$("<li class='content poster_" + posterType + "'>" +
            "<div class='content'>" +
            "<img class='posterImg poster'>" +   // 포스터 이미지
            "<div class='previewSdw'/>" +
            "<div class='contentsData'>" +
            "<div class='posterTitle'><span></span></div>" + // 제목
            "<span class='posterDetail'>" +
            "<span class='isfree'></span>" + //유.무료
            "<img class='isfreeIcon'>" + //유.무료
            "<img class='age'>" +    //연령
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>" +
            "<div class='chart_area'/>" +
            "</div>" +
            "</li>");

        poster.find(".posterDetail").prepend(stars.getView(stars.TYPE.RED_20));
        return poster;
    }

    this.setData = function(_dataList, page) {
        var contList = this.div.find("li");
        if (_dataList) {
            dataList = _dataList;
            totalPage = Math.floor((dataList.length - 1) / (lineCnt * pageCnt)) + 1;
            this.indicator.setSize(totalPage, page);
        } else if (page == dpPage) return;

        dpPage = page;
        if (totalPage > 1) {
            //top Page
            var tmp = HTool.getIndex(page, -1, totalPage);
            for (var i = 0; i < lineCnt; i++) setItem(contList.eq(i), dataList[tmp * lineCnt * pageCnt + ((pageCnt - 1) * lineCnt) + i]);
            //2nd Page - current Page
            tmp = page;
            for (var i = 0; i < lineCnt * pageCnt; i++) setItem(contList.eq(i + lineCnt), dataList[tmp * lineCnt * pageCnt + i]);
            //bottom Page
            var tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0; i < lineCnt * pageCnt; i++) setItem(contList.eq(i + lineCnt * (page + 1)), dataList[tmp * lineCnt * pageCnt + i]);
            //bottom Page
            var tmp = HTool.getIndex(page, 2, totalPage);
            for (var i = 0; i < lineCnt; i++) setItem(contList.eq(i + lineCnt * (page * 2 + 1)), dataList[tmp * lineCnt * pageCnt + i]);
        } else {
            for (var i = 0; i < lineCnt * (pageCnt * 2 + 2); i++) setItem(contList.eq(i), dataList[i - lineCnt]);
        }
    };

    function setPosterType(imgType) {
        switch (imgType - 0) {
            case 0: // 세로형 포스터
                posterType = "vertical";
                lineCnt = 5;
                pageCnt = 2;
                break;
            case 1: // 가로형 포스터
                posterType = "horizontal";
                lineCnt = 4;
                pageCnt = 3;
                break;
            case 2: //정사각 포스터
                posterType = "square";
                lineCnt = 4;
                pageCnt = 2;
                break;
        }
    }

    function setItem(div, data) {
        if(data) {
            div.css("visibility", "inherit");
            div.attr("id", data.itemId);
            div.find(".posterImg.poster").attr("src", data.imgUrl + "?w=210&h=300&quality=90");
            div.find(".posterTitle span").text(data.itemName);
            if (HTool.isTrue(data.wonYn)) {
                div.find("img.isfreeIcon").show();
                div.find("span.isfree").hide();
            } else if (HTool.isFalse(data.wonYn)) {
                div.find("span.isfree").show();
                div.find("span.isfree").text("무료");
                div.find("img.isfreeIcon").hide();
            } else {
                div.find("span.isfree").hide();
                div.find("img.isfreeIcon").hide();
            }
            div.find("img.age").attr("src", modulePath + "resource/icon_age_list_" + UTIL.transPrInfo(data.prInfo) + ".png");
            stars.setRate(div.find(".rating_star_area"), data.mark);
        }else {
            div.css("visibility", "hidden");
            div.attr("id", null);
            div.find(".posterImg.poster").attr("src", null);
            div.find(".posterTitle span").text("");
            div.find("span.isfree").text("");
        }
    }

    this.changeFocus = function(amount) {
        var newFocus = focus;
        do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(dataList.length/lineCnt)*lineCnt);
        while(newFocus>=dataList.length);

        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        while(page!=Math.floor(newFocus/(lineCnt*2))) this.changePage((amount>0?1:-1));
        this.div.find("li#" + (dataList[focus].itemId ? dataList[focus].itemId : dataList[focus].contsId)).addClass('focus');

        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        void this.div[0].offsetWidth;
        if(UTIL.getTextLength(dataList[focus].itemName, "RixHead L", 30)>215) {
            var posterDiv = this.div.find("li#" + dataList[focus].itemId + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    };

    this.changePage = function(amount) {
        this.div.removeClass("slide_up slide_down");
        page = HTool.getIndex(page, amount, totalPage);
        this.setData(dataList, HTool.getIndex(page, amount>0?-1:0, totalPage));
        void this.div[0].offsetWidth;
        this.div.addClass("slide_" + (amount>0?'up':'down'));
        this.indicator.setPos(page);
    };

    this.setFocus = function(newFocus) {
        focus = newFocus;
        if(page!=Math.floor(newFocus/(lineCnt*2))) this.setPage(Math.floor(newFocus/(lineCnt*2)));
        this.div.find("li#" + (dataList[focus].itemId ? dataList[focus].itemId : dataList[focus].contsId)).addClass('focus');
    };

    this.setPage = function(newPage){
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(dataList, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
    };

    this.onKeyAction = function(keyCode) {
        if(this.indicator.isFocused()) return this.onKeyForIndicator(keyCode);
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-lineCnt);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(lineCnt);
                return true;
            case KEY_CODE.LEFT:
                if(focus%lineCnt!=0) this.changeFocus(-1);
                else if(totalPage>1) setIndicatorFocus(this, true);
                else LayerManager.historyBack();
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.ENTER:
                if(dataList[focus].itemType == 1) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, dataList[focus].itemId, null, "67");
                    } catch(e) {}
                    openDetailLayer(dataList[focus].itemId, null, "67", "", true);
                }else if(dataList[focus].itemType == 2) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, dataList[focus].itemId, "67");
                    } catch(e) {}
                    openDetailLayer(null, dataList[focus].itemId, "67", "", true);
                } return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.UP:
                this.changePage(-1);
                return true;
            case KEY_CODE.DOWN:
                this.changePage(1);
                return true;
            case KEY_CODE.LEFT:
                LayerManager.historyBack();
                return false;
            case KEY_CODE.RIGHT:
                setIndicatorFocus(this, false);
                return true;
        }
    };

    function setIndicatorFocus(that, focused) {
        if(focused) {
            that.indicator.focused();
            that.div.find("li.focus").removeClass('focus');
            // changeBackground(null, 0);
        }else {
            that.indicator.blurred();
            that.setFocus(page*lineCnt*2);
        }
    }

    this.setFocusIndex = function (index) {
        focus = index;
        this.setFocus(focus);
    };

    this.getFocusIndex = function () {
        return focus;
    };

    this.getIsRecentlyList = function () {
        return false;
    };

    this.focused = function() {
        this.setFocus(focus);
    };

    this.blurred = function () {
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        this.setData(dataList, page);
    };
};

SubHomeMoreList.prototype = new ContentsList();
SubHomeMoreList.prototype.constructor = SubHomeMoreList;