/**
 * Created by ksk91_000 on 2017-01-16.
 */
window.KidsMenuIcon = function () {

    var MAX_NUM = 5;

    var div = _$("<div/>", {class:"kids_menu_icon_area"});
    var focus = 0;
    var length;
    var eventList = [];

    var kidsShDataMenu = [];

    this.setData = function () {
        window.log.printDbg("setData()");

        kidsShDataMenu = [];
        var tmpMenu = MenuDataManager.getKidsSHDataMenu();

        if (tmpMenu && tmpMenu.children) {
            for (var i = 0; i < tmpMenu.children.length; i++) {
                if (i < MAX_NUM - 1) {
                    kidsShDataMenu.push(tmpMenu.children[i]);
                }
                else if (i === MAX_NUM - 1) {
                    if (window.CONSTANT.YOUTUBE_SUPPORT) {
                        for (var j = i; j < tmpMenu.children.length; j++) {
                            if (tmpMenu.children[j].catType === window.MENU_CONSTANT.CAT.YOUTUBE_KIDS) {
                                kidsShDataMenu.push(tmpMenu.children[j]);
                                break;
                            }
                        }

                        if (kidsShDataMenu.length < MAX_NUM) {
                            kidsShDataMenu.push(tmpMenu.children[i]);
                        }
                    }
                    else {
                        kidsShDataMenu.push(tmpMenu.children[i]);
                    }
                }
                else {
                    break;
                }
            }
        }

        for (var i = 0; i < kidsShDataMenu.length; i++) {
            div.append(getItem(kidsShDataMenu[i]));
        }
    };
    
    function getItem (data) {
        var url = window.CONSTANT.KIDS_SUBHOME_MENU_ICON.LIVE_URL;
        if (window.DEF.RELEASE_MODE !== "LIVE") {
            url = window.CONSTANT.KIDS_SUBHOME_MENU_ICON.BMT_URL;
        }

        // test
        // url = "./images/test/";

        url += data.id + ".png";

        var div = _$("<div/>", {class: "kids_menu_icon"});

        var wrapper = _$("<div/>", {class:"absolute_wrapper"});
        wrapper.append(_$("<img>", {src: modulePath + "resource/image/sub_menu_shadow.png", css: {position: "absolute"}}));
        wrapper.append(_$("<img>", {class:"kids_menu_icon_cristal", src: modulePath + "resource/image/kids_submenu_cristal.png", css: {position: "absolute"}}));
        wrapper.append(_$("<img>", {class:"kids_menu_icon_img", src: url, css: {position: "absolute"}}));
        wrapper.append(_$("<div>", {class:"kids_menu_icon_title_bg", css: {position: "absolute", "margin-left": -1, "margin-top": 204, width: 230, height: 100}}));
        wrapper.append(_$("<span>", {class:"kids_menu_icon_title", text: data.name, css: {
            position: "absolute", "margin-left": -1, "margin-top": 240, width: 230, height: 40, "font-size": 30, "text-align": "center"
        }}));

        div.append(wrapper);

        return div;
    }

    function setFocus (newFocus) {
        focus = newFocus;

        div.find(".kids_menu_icon.focus").removeClass("focus");
        div.find(".kids_menu_icon").eq(focus).addClass("focus");
    }

    this.onKeyAction = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT :
                if (focus === 0) {
                    return false;
                }

                setFocus(focus - 1);
                return true;
            case KEY_CODE.RIGHT :
                if (focus === kidsShDataMenu.length - 1) {
                    return false;
                }

                setFocus(Math.min(kidsShDataMenu.length - 1, focus + 1));
                return true;
            case KEY_CODE.ENTER:
                gotoLocator(kidsShDataMenu[focus]);
                return true;
        }
    };

    function gotoLocator (data) {
        switch (data.hsTargetType - 0) {
            case 0:
                var MenuDataManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.MENU_DATA_MANAGER);
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) {
                        if (menu.id == data.hsTargetId) return true;
                    }
                })[0];

                console.log(menuData);

                if (menuData) {
                    MenuServiceManager.jumpMenu({menu: menuData});
                }
                else {
                    if (KIDS_MODE_MANAGER.isKidsMode()) {
                        KIDS_MODE_MANAGER.activateCanNotMoveMenuInfoPopup();
                    }
                    else {
                        showToast("잘못된 접근입니다");
                    }
                }
                break;
            case 1:    //시리즈
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.hsTargetId, "67");
                } catch(e) {}
                openDetailLayer(data.hsTargetId, null, "67", "", true);
                break;
            case 2:    //컨텐츠
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.parent.id, data.hsTargetId, "67");
                } catch(e) {}
                openDetailLayer(null, data.hsTargetId, "67", "", true);
                break;
            case 3:    //멀티캐스트 양방향 서비스
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE, data.parent.id, data.hsTargetId, "", data.hsLocator);
                } catch(e) {}

                var nextState;
                if (data.hsLocator == CONSTANT.APP_ID.MASHUP) {
                    nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                }
                else {
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
                }

                window.AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        type: window.CONSTANT.APP_TYPE.MULTICAST,
                        param: data.hsLocator,
                        ex_param: data.parameter
                    }
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE, data.parent.id, data.hsTargetId, "", data.hsLocator);
                } catch(e) {}

                var nextState = stateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;

                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: data.hsLocator
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(data.hsLocator);
                break;
        }
    }

    this.isLastFocused = function () { return focus === kidsShDataMenu.length - 1; };
    this.isFirstFocused = function () { return focus === 0; };

    this.focused = function (keyCode) {
        if (keyCode === KEY_CODE.BLUE) {
            setFocus(kidsShDataMenu.length - 1);
        }
        else {
            setFocus(0);
        }

        return true;
    };

    this.blurred = function () {
        div.find(".focus").removeClass("focus");
    };

    this.getView = function () {
        return div;
    };

    this.getHeight = function () {
        return 400;
    };

    this.setFocusIndex = function (index) {
        focus = index;
        setFocus(focus);
    };

    this.getFocusIndex = function () {
        return focus;
    };

    this.getIsRecentlyList = function () {
        return false;
    };

    this.getDataLength = function () {
        return kidsShDataMenu.length;
    }
};