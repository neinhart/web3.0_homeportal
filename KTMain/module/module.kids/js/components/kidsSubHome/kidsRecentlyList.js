/**
 * Created by Yun on 2017-04-02.
 */

window.kidsRecentlyList = function (menuId) {
    var componentId = menuId;
    var data;
    var length;
    var isMore;
    var isFocused = false;
    var parent;

    var div = _$("<div/>", {id: menuId});

    var focus;
    var instance = this;

    var COMP_TITLE_HEIGHT = 56;

    /** Recently 내에서 키 입력등으로 이동할 수 있는 포커스의 최대 Index */
    var FOCUS_MAX_IDX = 5;


    this.update = function (contentsData) {
        data = contentsData;
        length = data.length;
        isMore = length > FOCUS_MAX_IDX;
        constructDom();

        instance.onKeyAction = data.length > 0 ? instance.onKeyForList : instance.onKeyForInfoTxt;

        if (isFocused) {
            if(data.length>0) {
                if(focus >= data.length) {
                    focus = data.length - 1;
                    focusing(focus);
                }
                else focusing(focus);
            }
            else div.find("#" + componentId).addClass("focus");
        }
    };

    var constructDom = function () {
        div.empty();
        if (data.length > 0) {
            div.append(_$("<span/>", {class: "componentTitle"}).html("<div></div>최근 시청 목록"));
            var list = _$("<ul/>", {id: componentId + "_contents", class: "sub_posterList"});
            div.append(list);

            for (var idx = 0 ; idx < length && idx < (isMore ? FOCUS_MAX_IDX-1 : FOCUS_MAX_IDX); idx ++) {
                list.append(getVerticalPoster(componentId + "_" + idx, data[idx]));
            }

            if (isMore) {
                data[FOCUS_MAX_IDX-1].moreCnt = length;
                list.append(window.PosterFactory.getVerticalPoster_more(componentId + "_" + (FOCUS_MAX_IDX-1), data[FOCUS_MAX_IDX-1]));
            }
            div.css("display", "block");
        } else {
            div.append(_$("<div/>", {id: componentId + "_noContents", class: "infoText_recently"}));
            div.css("display", "none");
        }
    };

    var getVerticalPoster = function (id, data) {
        var element = _$("<li id='" + id + "' class='content poster_vertical'>" +
            "<div class='content'>" +
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>"+
            "<img src='" + data.imgUrl + "?w=210&h=300&quality=90' class='posterImg poster'>" +
            "<div class='poster_remain_bg'></div>" +
            "<div class='poster_remain_bar'></div>" +
            "<div class='edit_select_btn'></div>" +
            "<div class='contentsData'>" +
            "<div class='posterTitle'><span>" + data.contsName + "</span></div>" +
            "<span class='posterDetail'>" +
            "<span class='stars'/>" +
            (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + window.modulePath + "resource/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo-0) + ".png' class='age'>"+
            "</span>"+
            "</div>" +
            "<div class='previewSdw'/>"+
            "<div class='icon_area'/>" +
            "</div>" +
            "</li>");
        element.find(".poster_remain_bar").css("width", Math.min(196, data.linkTime/(data.runtime*60) * 210) + "px");
        setIcon(element.find(".icon_area"), data);
        element.toggleClass("series", data.contYn=="N");
        return element;
    };

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.newHot) {
            case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
            case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
            case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
            case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
            case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
        } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.isHd=="UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.isDvdYn=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        } if(UTIL.isLimitAge(data.prInfo)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.setParent = function (value) {
        parent = value;
    };

    this.setDataRefresh = function(contentsData) {
        this.update(contentsData);
    };

    this.getDataLength = function () {
        return length;
    };

    this.getView = function () {
        return div;
    };

    this.isLastFocused = function () { if (data && data.length > 0) return focus === Math.min(5, length)-1; else return true; };
    this.isFirstFocused = function () { return focus === 0; };

    this.titleHide = function() {
        div.find(".sub_posterList .content .contentsData").css("visibility", "hidden");
        div.find(".sub_posterList .content.more .contentsData").css("visibility", "inherit");
    };

    this.titleShow = function() {
        div.find(".sub_posterList .contentsData").css("visibility", "inherit");
    };

    /**
     * 다른 View or component 등에서 RecentlyList로 포커스가 이동 되었을때(초기화)
     * @returns {boolean}
     */
    this.focused = function (keyCode) {
        div.removeClass("blurred");
        if (!data) return;
        if (data.length > 0) {
            parent.div.find(".relationBtn_area .relationBtn_text").text("목록에서 삭제");
            if(keyCode===KEY_CODE.BLUE) focus = Math.min(5, length) - 1;
            else focus = 0;
            focusing(focus);
            isFocused = true;
            return true;
        } else {
            parent.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
            div.find("#" + componentId).removeClass("focus");
            isFocused = false;
            return false;
        }
    };

    /**
     * 키 입력등으로 RecentlyList 내 에서 포커스가 이동 되었을 때
     * @param index
     */
    var focusing = function (index) {
        if(index==0) div.find(".componentTitle").css("-webkit-transform", "translateY(-26px)");
        else div.find(".componentTitle").css("-webkit-transform", "");
        _$("#" + componentId + "_" + index).addClass("focus");

        if (!isMore || focus < FOCUS_MAX_IDX-1) parent.div.find(".relationBtn_area .relationBtn_text").text("목록에서 삭제");
        else  parent.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));

        setTextAnimation(index);
    };

    function setTextAnimation(focus) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        if(UTIL.getTextLength(data[focus].contsName, "RixHead L", 30)>215) {
            var posterDiv = div.find("#" + componentId + "_" + focus + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    var unfocusing = function (index) {
        _$("#" + componentId + "_" + index).removeClass("focus");
    };

    this.onKeyForList = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT:
                if (focus > 0) {
                    unfocusing(focus);
                    focus--;
                    focusing(focus);
                    return true;
                } else return false;
            case KEY_CODE.RIGHT:
                if (focus < Math.min(FOCUS_MAX_IDX, length) - 1) {
                    unfocusing(focus);
                    focus ++;
                    focusing(focus);
                    return true;
                } return false;
            case KEY_CODE.ENTER:
                if(focus==FOCUS_MAX_IDX-1 && length>FOCUS_MAX_IDX) openMorePage();
                else {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data[focus].catId, data[focus].contsId, "02");
                    } catch(e) {}
                    // 171013 우리집 맞춤 TV에 최근시청목록 과 로직이 다른 결과로 동일하게 수정된 부분
                    // 권순관 대리님과 로직 및 시나리오 확인 필요
                    // openDetailLayer(data[focus].catId, data[focus].contsId, "02", "", true);
                    openDetailLayerImpl(data[focus]);
                }
                return true;
            case KEY_CODE.CONTEXT:
                if(focus == FOCUS_MAX_IDX-1 && isMore) return false;   // 전체보기 포커스 시 return false;
                else if(length<=FOCUS_MAX_IDX || focus!=FOCUS_MAX_IDX-1) {
                    var that = this;
                    LayerManager.activateLayer({
                        obj: {
                            id: "KidsEditModeDeletePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: false,
                            params: {
                                type: 1,
                                items: [{title:data[focus].contsName}],
                                callback: function (res) {
                                    if (res) {
                                        RecentlyListManager.removeContents(focus, function (result) {
                                            if (result === true) {
                                                showToast("최근 시청 목록에서 삭제되었습니다");
                                                RecentlyListManager.getRecentlyData(function(contentsData) {
                                                    that.update(contentsData);
                                                    if (contentsData.length == 0) {
                                                        RecentlyListManager.setIsResetting(true);
                                                        parent.resetting();
                                                    }
                                                    LayerManager.historyBack();
                                                });
                                            } else {
                                                LayerManager.historyBack();
                                            }
                                        });
                                    } else {
                                        LayerManager.historyBack();
                                    }
                                }
                            }
                        },
                        moduleId: "module.kids",
                        visible: true
                    });
                }
                return true;
        }
    };

    var openDetailLayerImpl = function(data) {
        LayerManager.startLoading({preventKey:true});
        if(data.contYn=="N") {
            if (data.catId == "N") {
                kidsSubHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=V&platformGubun=W", function (res, newData) {
                    if(res) {
                        if (!newData) {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET002});
                        } else if (newData.serviceYn == "Y") {
                            openDetailLayer(newData.catId, data.contsId, "02", "", true);
                        } else {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                        }
                    } else {
                        LayerManager.stopLoading();
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    }
                });
            } else openDetailLayer(data.contsId, data.contsId, "02", "", true);
        } else {
            kidsSubHome.amocManager.getContentW3(function (res, contData) {
                if(res) {
                    if(contData.contsId) openDetailLayer(contData.catId, data.contsId, "02", "", true, {contsInfo:contData});
                    else if(data.catId!=="N") openDetailLayer("N", data.contsId, "02");
                    else {
                        LayerManager.stopLoading();
                        HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                    }
                } else {
                    LayerManager.stopLoading();
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                }
            }, data.catId, data.contsId, DEF.SAID);
        }
    };

    function openMorePage() {
        // [si.mun] 기존에는 아래 주석이었던 layer( KidsSubhomeRecentlyList ) 를 호출하였음
        // 하지만, 18.03 형상부터 마이메뉴에 있는 최근시청 목록을 호출하도록 변경
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeRecentlyList",
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                params: {
                    menuName: "최근 시청 목록",
                    showModule: "Kids",
                    KidsRecentlyManager: window.RecentlyListManager,
                }
            },
            moduleId: "module.family_home",
            new: false,
            visible: true
        });
        //LayerManager.activateLayer({
        //    obj: {
        //        id: "KidsSubhomeRecentlyList",
        //        type: Layer.TYPE.NORMAL,
        //        priority: Layer.PRIORITY.NORMAL,
        //        linkage: false
        //    },
        //    moduleId: "module.kids",
        //    new: false,
        //    visible: true
        //})
    }

    this.onKeyForInfoTxt = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.ENTER:
                return true;
        } return false;
    };

    this.onKeyAction = function (keyCode) {
    };

    this.getHeight = function () {
        if (length > 0) return 480;
        else return 0;
    };

    this.pause = function () {
        if (!isFocused) return;
        _$("#kidsRecentlyList_contents li").removeClass("focus");
        focusing(focus);
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
    };

    this.resume = function () {
        if (!isFocused || data.length == 0) return;
        setTextAnimation(focus);
    };

    this.blurred = function () {
        // div.addClass("blurred");
        isFocused = false;
        parent.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        div.find(".componentTitle").css("-webkit-transform", "");
        _$("#kidsRecentlyList_contents li").removeClass("focus");
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        // parent.viewMgr.changeBackground(null, 0);
    };

    this.setFocusIndex = function (index) {
        _$("#kidsRecentlyList_contents li").removeClass("focus");
        focus = index;
        focusing(focus);
    };

    this.getFocusIndex = function () {
        return focus;
    };

    this.getIsRecentlyList = function () {
        return true;
    };
};