/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.RankingTitleList = function(parent, type) {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0;
    var itemCnt;
    var dpPage = -1;
    var sortGb;
    var isFocused = false;
    var isChangedPage = false;
    var reqPathCd = "18";
    var catData;

    var isLoading = false;
    var loadedCallback = null;
    var posterType = type || TitleList.TYPE.VERTICAL;

    this.create = function (cateId, cateData, _reqPathCd) {
        var typeClass = posterType === RankingTitleList.TYPE.SQUARE ? "square" : "";
        this.div = _$("<div/>", {class: "contentsList title_list rank_title_list " + typeClass});
        this.cateId = cateId;
        sortGb = cateData.sortGb;
        reqPathCd = _reqPathCd || reqPathCd;

        this.div.append(_$("<ul/>", {class:"item_list"}));
        var detArea = _$("<div/>", {class:"contents_detail_area"});
        detArea.append(_$("<div/>", {class:"posterArea"}));
        detArea.append(_$("<div/>", {class:"chart_area"}));
        detArea.append(_$("<div/>", {class:"star_area"}));
        detArea.append(_$("<div/>", {class:"info_area"}));
        detArea.append(_$("<div/>", {class:"synopsis_area"}));

        var posterArea = detArea.find(".posterArea");
        posterArea.append(_$("<img>", {class:"posterImg"}));
        posterArea.append(_$("<div/>", {class:"icon_area"}));

        this.div.append(detArea);
        createItemArea(this.div.find(".item_list"));
        this.div.append(_$("<div/>", {class: "focus_area"}));

        catData = HTool.transCateInfo(cateData);
    };

    function createItemArea(ul) {
        for(var i=0; i<10; i++) {
            var li = _$("<li/>", {class:"contents_item"});
            li.append(_$("<div/>", {class: "rank"}));
            li.append(_$("<div/>", {class: "chart_change"}));
            li.append(_$("<div/>", {class: "title"}).append("<div/>"));
            li.append(_$("<div/>", {class: "wonInfo_area"}));
            li.find(".wonInfo_area").append(_$("<div/>", {class: "isFree"}));
            li.find(".wonInfo_area").append(_$("<img/>", {class: "isFreeIcon"}));
            li.append(_$("<div/>", {class: "prInfo"}));
            ul.append(li);
        }
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        if(!data.chartOrder) switch(data.newHot) {
            case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
            case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
            case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
            case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
            case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
        } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.resolCd && data.resolCd.indexOf("UHD")>=0) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.isDvdYn=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        } if(UTIL.isLimitAge(data.prInfo)) {
            if (posterType === RankingTitleList.TYPE.SQUARE) {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked_music.png"}));
            } else {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
            }
        }
    }

    this.setData = function(_dataList, page, sortGb, _itemCnt) {
        var contList = this.div.find(".item_list li");
        if(_dataList) {
            dataList = _dataList;
            itemCnt = _itemCnt;
            totalPage = Math.floor((itemCnt - 1) / 10) + 1;
            if(isFocused && !this.indicator.isFocused()) this.setFocus(focus)
        } else if(dpPage==page) return;

        dpPage = page;
        for (var i = 0; i < 10; i++){
            setItem(contList.eq(i), page*10+i);
            setItem(contList.eq(i+10), HTool.getIndex(page, 1, totalPage)*10+i);
        }
        this.indicator.setSize(totalPage, page);
        if(isFocused && !this.indicator.isFocused()) this.setFocus(focus);
    };

    this.updateData = function (_dataList) {
        if(dpPage==page) dpPage = -1;
        this.setData(_dataList, page, sortGb, itemCnt);
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if(data) {
            div.attr("id", getDataListId(idx));
            div.find(".rank").text((idx+1));
            div.find(".title > div").text(data.itemName);
            // div.find(".isFree").text(data.wonYn=="Y"?"유료":"무료");
            if (HTool.isTrue(data.wonYn)) {
                div.find("img.isFreeIcon").show();
                div.find("div.isFree").hide();
            } else if (HTool.isFalse(data.wonYn)) {
                div.find("div.isFree").show();
                div.find("div.isFree").text("무료");
                div.find("img.isFreeIcon").hide();
            } else {
                div.find("img.isFreeIcon").hide();
                div.find("div.isFree").hide();
            }
            div.find(".prInfo").html(_$("<img>", {"src": modulePath + "resource/icon_age_txtlist_" + UTIL.transPrInfo(data.prInfo) + ".png"}));

            var chart_area = div.find(".chart_change");
            chart_area.removeClass("new still minus plus");
            switch (data.change) {
                case "N" : chart_area.text("").addClass("new"); break;
                case "0" : chart_area.text("").addClass("still"); break;
                default :
                    var sign = data.change.substr(0, 1);
                    var tmpClass = "";
                    if (sign == "-") tmpClass = "minus"; else if (sign == "+") tmpClass = "plus";
                    chart_area.text(data.change.substr(1, data.change.length - 1)).addClass(tmpClass);
                    break;
            }

            div.removeClass("hiddenItem");
        } else {
            div.attr("id", "");
            div.addClass("hiddenItem");
        }
    }

    this.changeFocus = function(amount) {
        focus = HTool.getIndex(focus, amount, itemCnt);
        this.div.find("li.focus").removeClass('focus');
        if(page!=Math.floor(focus/10)) this.changePage((amount>0?1:-1));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        this.div.find(".focus_area").css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");
        setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
        // this.parent.viewMgr.changeBackground(dataList[focus].imgUrl + "?w=259&h=370&quality=90", 500);

        if(focus%10+1>5 && !isLoading) {
            isLoading = true;
            this.parent.loadAddedContents(((page+1)*10+5), 10, function () {
                if(loadedCallback) loadedCallback();
                loadedCallback = null;
                isLoading = false;
            });
        }
    };

    this.changePage = function(amount) {
        var that = this;

        var newPage = HTool.getIndex(page, amount, totalPage);
        if(amount<0) this.parent.loadAddedContents((newPage*10), 10, function() { changePageImpl(that) }, true);
        else if(isLoading) {
            LayerManager.startLoading({preventKey: true});
            loadedCallback = function () { changePageImpl(that); LayerManager.stopLoading(); };
        } else this.parent.loadAddedContents((newPage*10)+5, 5*2, function() { changePageImpl(that) }, true);

        function changePageImpl() {
            that.setPage(newPage);
        }
    };

    this.setFocus = function(newFocus, forced) {
        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        if(forced || page!=Math.floor(newFocus/10)) this.setPage(Math.floor(newFocus/10));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");
        setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
        // this.parent.viewMgr.changeBackground(this.div.find("li#" + dataList[focus].itemId + " .posterImg").prop('src'), 500);
    };

    this.setPage = function(newPage) {
        this.div.find(".item_list").removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
    };

    function setFocusItem(div, data) {
        var imgUrl = data.imgUrl + "?w=259&h=370&quality=90";
        var defaultImg = "this.src='" + modulePath + "resource/image/default_poster.png'";
        if (posterType === RankingTitleList.TYPE.SQUARE) {
            imgUrl = data.squareImgUrl + "?w=250&h=250&quality=90";
            defaultImg = "this.src='" + modulePath + "resource/image/default_music2.png'";
        }
        div.find(".posterImg").attr({"src": imgUrl, onerror: defaultImg});
        div.find(".star_area").empty().append(stars.getView(stars.TYPE.RED_20)).append(data.runtime?("<span>" + data.runtime + "분</span>"):"");
        stars.setRate(div, data.mark);
        div.find(".info_area").html((data.overseerName?("<title>감독</title>" + data.overseerName + "<br>"):"") + (data.actor?("<title>출연</title>" + data.actor):""));
        div.find(".synopsis_area").text(data.synopsis);
        setIcon(div.find(".icon_area"), data);
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return true;
            case KEY_CODE.LEFT :
                return false;
            case KEY_CODE.RED:
                if(focus === 0) {
                    if(totalPage>1) this.changeFocus(-(((itemCnt-1)%10)+1));
                } else if(focus%10===0) {
                    if(totalPage<=1) return true;
                    this.changeFocus(-10);
                } else this.changeFocus((page*10) - focus);
                return true;
            case KEY_CODE.BLUE:
                if(focus === itemCnt-1) {
                    this.changeFocus(1);
                } else if(focus%10===9) {
                    if(totalPage<=1) return true;
                    if(page === totalPage-2) this.changeFocus(((itemCnt-1)%10)+1);
                    else this.changeFocus(10);
                } else this.changeFocus((Math.min(itemCnt-1, page*10+9)) - focus);
                return true;
            case KEY_CODE.ENTER:
                try {
                    NavLogMgr.collectKids(keyCode,  this.parent.viewId, this.parent.title, getDataId(focus), dataList[focus].itemName);
                } catch(e) {}
                openDetailLayer(this.cateId, getDataId(focus), reqPathCd, dataList[focus], true);
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function () {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN :
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.RIGHT :
            case KEY_CODE.ENTER:
                this.indicator.blurred();
                this.setFocus(isChangedPage ? page * 10 : focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.RED:
                this.indicator.blurred();
                this.changeFocus((page * 10) - focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.BLUE:
                this.indicator.blurred();
                this.changeFocus((Math.min(itemCnt - 1, page * 10 + 9)) - focus);
                isChangedPage = false;
                return true;
        }
    };
    
    this.getFocusedContents = function () {
        return dataList[focus];
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        return  ( dataList[dataIdx].itemId || dataList[dataIdx].contsId );
    }

    this.focused = function() {
        if(!dataList || itemCnt<=0) return false;
        this.setFocus(focus);
        this.div.find(".wonInfo_area").show();
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        this.div.removeClass("show_detail");
        this.div.find(".wonInfo_area").hide();
        this.div.find(".focus_area").hide();
        this.indicator.blurred();
        // this.parent.viewMgr.changeBackground(null, 0);
        // this.setData(dataList, page);
    };
};

RankingTitleList.prototype = new ContentsList();
RankingTitleList.prototype.constructor = RankingTitleList;
Object.defineProperty(RankingTitleList, "TYPE", {
    value : {
        /** 미리보기 포스터가 직사각형인 경우*/
        VERTICAL : 'VERTICAL',
        /** 미리보기 포스터가 정사각형인 경우*/
        SQUARE : 'SQUARE'
    },
    writable: false,
    configurable: false
});