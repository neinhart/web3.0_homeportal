/**
 * Created by Yun on 2017-02-23.
 */

window.KidsCharacterList = function () {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    const LINE_AMOUNT = 4;

    this.create = function (cateId) {
        this.cateId = cateId;
        this.div = _$("<ul/>", {class:"contentsList kids_character_list"});
        for (var i=0; i<LINE_AMOUNT*4; i++) {
            this.div.append(getCharacterDiv());
        }
    };

    function getCharacterDiv() {
        return _$("<li class='content kids_Character'>" +
            "<div class='kids_content'>" +
            "<div class='kids_characterBG_div'></div>" +
            "<img class='kids_character_img'>" +
            "<div class='kids_character_name_BG'></div>" +
            "<div class='kids_character_name'></div>" +
            "</div>" +
            "</li>"
        );
    }

    this.setData = function (_dataList, _page) {
        var contList = this.div.find("li");
        if (_dataList) {
            dataList = _dataList;
            totalPage = Math.floor((dataList.length-1)/8)+1;
        } else if (_page == dpPage) return;

        dpPage = _page;
        if (totalPage>1) {
            var tmp = _page;
            for (var i = 0 ; i < 8 ; i ++) setItem(contList.eq(i), dataList[tmp * 8 + i]);
            tmp = HTool.getIndex(_page, 1, totalPage);
            for (var i = 0 ; i < 8 ; i ++) setItem(contList.eq(i + 8), dataList[tmp * 8 + i]);
        } else {
            for (var i = 0 ; i < 16; i ++) setItem(contList.eq(i), dataList[i]);
        }
        // this.indicator.setSize(totalPage, page);
    };

    function setItem(div, data) {
        if (data) {
            div.css("visibility", "inherit");
            div.attr("id", data.id);
            div.find(".kids_character_img").attr("src", data.menuImgUrl);
            div.find(".kids_character_name").text(data.name);
        } else {
            div.css("visibility", "hidden");
            div.attr("id", null);
            div.find(".kids_character_name").text("");
        }
    }

    this.changeFocus = function (amount) {
        var newFocus = focus;
        do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(dataList.length/4)*4);
        while (newFocus>=dataList.length);

        focus = newFocus;
        this.div.find("li.focus").removeClass("focus");
        while(page!=Math.floor(newFocus/8)) this.changePage((amount>0?1:-1));
        this.div.find("li#" + dataList[focus].id).addClass("focus");
    };

    this.changePage = function (amount) {
        this.div.removeClass("slide_up slide_down");
        page = HTool.getIndex(page, amount, totalPage);
        this.setData(dataList, HTool.getIndex(page, amount>0?-1:0, totalPage));
        void this.div[0].offsetWidth;
        this.div.addClass("slide_" + (amount>0?"up":"down"));
        // this.indicator.setPos(page);
    };

    this.setFocus = function (newFocus) {
        focus = newFocus;
        if(page!=Math.floor(newFocus/8)) this.setPage(Math.floor(newFocus/8));
        this.div.find("li#" + dataList[focus].id).addClass("focus");
    };

    this.setPage = function (newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(dataList, page);
        void this.div[0].offsetWidth;
        // this.indicator.setPos(page);
    };

    this.onKeyAction = function (keyCode) {
        log.printDbg("onKeyAction() "+keyCode);
        switch(keyCode) {
            case KEY_CODE.UP:
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN:
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.LEFT:
                if (focus%LINE_AMOUNT != 0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.ENTER:
                return true;
            default:
                return false;
        }
    };

    this.focused = function () {
        this.setFocus(focus);
    };

    this.blurred = function () {
        this.div.find("li.focus").removeClass("focus");
        this.div.removeClass("slid_up slide_down");
        this.setData(dataList, page);
    };
};

KidsCharacterList.prototype = new ContentsList();
KidsCharacterList.prototype.constructor = KidsCharacterList;