/**
 * Created by Yun on 2017-03-10.
 */

window.TheaterVerticalPosterList = function() {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var lineVisibleTimeout;
    const LINE_AMOUNT = 5;

    var ITEM_NUMBER_OF_PER_PAGE = 10;

    this.create = function (cateId) {
        this.cateId = cateId;
        window.cateId = cateId;
        this.div = _$("<ul/>", {class:"contentsList vertical_poster_list theater"});
        for(var i=0; i<LINE_AMOUNT*LINE_AMOUNT; i++)
            this.div.append(getPosterDiv(Math.floor(i/LINE_AMOUNT)));
    };

    function getPosterDiv(lineNum) {
        var poster = _$("<li class='content poster_vertical line_" + lineNum + "'>"+
            "<div class='content'>"+
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>"+
            "<img class='posterImg poster'>"+   // 포스터 이미지
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span></span></div>"+ // 제목
            "<span class='posterDetail'>"+
            "<span class='isfree'></span>"+ //유.무료
            "<img class='isfreeIcon'>"+ //유.무료
            "<img class='age'>"+    //연령
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>"+
            "<div class='chart_area'/>"+
            "<div class='over_dim'/>"+
            "</div>"+
            "</li>");

        poster.find(".posterDetail").prepend(stars.getView(stars.TYPE.RED_20));
        return poster;
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        if(!data.chartOrder) switch(data.newHot) {
            case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
            case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
            case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
            case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
            case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
        } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.resolCd && data.resolCd.indexOf("UHD")>=0) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.isDvdYn=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        } if((KIDS_MODE_MANAGER.isKidsMode() ? Number(UTIL.transPrInfo(data.prInfo)) > 7 : UTIL.isLimitAge(data.prInfo))) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.setData = function(_dataList, page) {
        var contList = this.div.find("li");
        if (_dataList) {
            dataList = _dataList;
            totalPage = Math.floor((dataList.length-1)/ITEM_NUMBER_OF_PER_PAGE)+1;
        } else if (page==dpPage) return;

        dpPage = page;
        if (totalPage>1) {
            var tmp = page;
            for (var i = 0 ; i < ITEM_NUMBER_OF_PER_PAGE ; i++) setItem(contList.eq(i), tmp * ITEM_NUMBER_OF_PER_PAGE + i);
            tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0 ; i < ITEM_NUMBER_OF_PER_PAGE +5 ; i++) {
                setItem(contList.eq(i + ITEM_NUMBER_OF_PER_PAGE), tmp * ITEM_NUMBER_OF_PER_PAGE + i);
            }
        } else {
            for (var i = 0 ; i < ITEM_NUMBER_OF_PER_PAGE*2 ; i++) setItem(contList.eq(i), i);
        }
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if(data) {
            div.removeClass("hidden_cont");
            div.attr("id", getDataListId(idx));
            div.find(".posterImg.poster").attr("src", "");
            div.find(".posterImg.poster")[0].offsetWidth;
            div.find(".posterImg.poster").attr({"src": (data.imgUrl ? (data.imgUrl + "?w=210&h=310&quality=90") : (modulePath + "resource/image/default_poster.png")), onerror:"this.src='" + modulePath + "resource/image/default_poster.png'"});
            div.find(".posterTitle span").text(data.itemName || data.contsName);
            if (HTool.isTrue(data.wonYn)) {
                div.find("img.isfreeIcon").show();
                div.find("span.isfree").hide();
            } else if (HTool.isFalse(data.wonYn)) {
                div.find("span.isfree").show();
                div.find("span.isfree").text("무료");
                div.find("img.isfreeIcon").hide();
            } else {
                div.find("span.isfree").hide();
                div.find("img.isfreeIcon").hide();
            }
            div.find("img.age").attr("src", modulePath + "resource/icon_age_list_" + window.UTIL.transPrInfo(data.prInfo) + ".png");
            stars.setRate(div.find(".rating_star_area"), data.mark);
            setIcon(div.find(".icon_area"), data);
        }else {
            div.addClass("hidden_cont");
            div.attr("id", null);
            div.find(".posterImg.poster").attr("src", null);
            div.find(".posterTitle span").text("");
            div.find("span.isfree").text("");
            div.find(".chart_area").empty();
        }
    }

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        return  ( dataList[dataIdx].itemId || dataList[dataIdx].contsId );
    }


    this.changeFocus = function(amount) {
        var newFocus = focus;
        if(amount==1 && focus == dataList.length-1) newFocus = 0;
        else do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(dataList.length/LINE_AMOUNT)*LINE_AMOUNT);
        while(Math.floor(newFocus/LINE_AMOUNT)>Math.floor((dataList.length-1)/LINE_AMOUNT));
        if(newFocus>=dataList.length) newFocus = dataList.length-1;

        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        while(page!=Math.floor(newFocus/(LINE_AMOUNT*2))) this.changePage((amount>0?1:-1));
        if (dataList[focus]) {
            this.div.find("li#" + (getDataListId(focus))).addClass('focus');
            // this.parent.viewMgr.changeBackground(this.div.find("li#" + (dataList[focus].itemId || dataList[focus].contsId) + " .posterImg")[0].src, 500);

            setTextAnimation(this.div, focus);
        }
    };

    function setTextAnimation(div, focus) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        var tmpText = dataList[focus].itemName || dataList[focus].contsName;
        if(UTIL.getTextLength(tmpText, "RixHead L", 30)>210) {
            var posterDiv = div.find("li#" + (getDataListId(focus)) + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.changePage = function(amount) {
        if(totalPage==1) return;
        this.div.removeClass("slide_up slide_down");
        page = HTool.getIndex(page, amount, totalPage);
        this.setData(null, HTool.getIndex(page, amount>0?-1:0, totalPage));
        void this.div[0].offsetWidth;
        this.div.addClass("slide_" + (amount>0?'up':'down'));
        this.div.find(".hidden_line").removeClass("hidden_line");

        if(page == totalPage-1) setLineVisible(this.div.find(".line_" +(amount>0?"4":"2")), 0);
        if(amount>0) setLineVisible(this.div.find(".line_0,.line_1"), 500);
        else clearLineVisible();
    };

    this.setFocus = function(newFocus, forced) {
        if(!dataList[focus]) return;
        focus = newFocus;
        this.div.find(".hidden_line").removeClass("hidden_line");
        if(forced || page!=Math.floor(newFocus/(LINE_AMOUNT*2))) this.setPage(Math.floor(newFocus/(LINE_AMOUNT*2)));
        this.div.find("li#" + (getDataListId(focus))).addClass('focus');
        // this.parent.viewMgr.changeBackground(this.div.find("li#" + dataList[focus].itemId + " .posterImg")[0].src, 500);
        setTextAnimation(this.div, focus);
    };

    this.setPage = function(newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(dataList, page);
        void this.div[0].offsetWidth;
        this.div.find(".hidden_line").removeClass("hidden_line");
        if(page == totalPage-1) setLineVisible(this.div.find(".line_2"), 0);
    };

    function clearLineVisible() {
        if(lineVisibleTimeout) {
            clearTimeout(lineVisibleTimeout);
            lineVisibleTimeout = null;
        }
    }

    function setLineVisible(div, time) {
        clearLineVisible();

        if(time==0) div.addClass("hidden_line");
        else lineVisibleTimeout = setTimeout(function () {
            div.addClass("hidden_line");
        }, time);
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN:
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.LEFT:
                if(focus%LINE_AMOUNT!=0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.ENTER:
                gotoLocation(dataList[focus]);
                // openDetailLayer(this.cateId, dataList[focus].itemId, "00", dataList[focus]);
                return true;
            default:
                return false;
        }
    };

    function gotoLocation(data) {
        switch (data.itemType-0) {
            case 0:
                var vodModule = window.ModuleManager.getModule("module.vod");
                if(!vodModule) {
                    window.ModuleManager.loadModule({
                        moduleId: "module.vod",
                        cbLoadModule: function (success) { if (success) gotoLocation(data); }
                    }); return;
                }
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) { if(menu.id==data.itemId) return true; }
                })[0];
                vodModule.activateLayerByMenu({menu:menuData});
                break;
            case 1:    //시리즈
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.itemId, null);
                } catch(e) {}
                //TODO REQ_PATH_CD 입력 필요.
                openDetailLayer(data.itemId, null, "01", "", true);
                break;
            case 2:    //컨텐츠
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.itemId);
                } catch(e) {}
                //TODO REQ_PATH_CD 입력 필요.
                openDetailLayer(null, data.itemId, "01", "", true);
                break;
            case 3:    //멀티캐스트 양방향 서비스                                               
                var nextState;
                if(data.locator == CONSTANT.APP_ID.MASHUP)
                    nextState = stateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                else
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                window.AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        type: window.CONSTANT.APP_TYPE.MULTICAST,
                        param: data.locator,
                        ex_param: data.parameter
                    }
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                var nextState = stateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: data.itemId
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(data.locator);
                break;
                break;
        }
    }

    this.focused = function() {
        this.div.removeClass("slide_up slide_down");
        this.setFocus(focus);
    };

    this.blurred = function() {
        focus = 0;
        clearLineVisible();
        this.div.find("li.focus").removeClass('focus');
       //  this.div.removeClass("slide_up slide_down");
       //  this.div.find(".hidden_line").removeClass("hidden_line");
    };

    this.pause = function () {
        clearLineVisible();
        this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
    };

    this.resume = function () {
        this.setFocus(focus, true);
        setTextAnimation(this.div, focus);
    };
};

TheaterVerticalPosterList.prototype = new ContentsList();
TheaterVerticalPosterList.prototype.constructor = TheaterVerticalPosterList;