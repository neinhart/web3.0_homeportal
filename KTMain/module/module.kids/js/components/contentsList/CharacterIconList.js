/**
 * Created by Yun on 2017-06-19.
 */

window.CharacterIconList = function () {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var itemCnt;
    var isFocused = false;
    var sortGb;
    var buyType;
    var lineVisibleTimeout;
    var isAdultCate = false;
    var isChangedPage = false;
    var reqPathCd = "01";
    var catData;
    var loadedCallback = null;
    var isLoading = false;
    const LINE_AMOUNT = 4;

    this.create = function (cateId, _catData, _reqPathCd) {
        this.cateId = cateId;
        this.div = _$("<ul/>", {class: "contentsList kids_character_list"});
        for (var i = 0; i < LINE_AMOUNT * 5; i++)
            this.div.append(getCharacterDiv(Math.floor(i / LINE_AMOUNT)));

        sortGb = _catData.sortGb;
        buyType = _catData.buyType;
        reqPathCd = _reqPathCd || reqPathCd;

        (function () {
            var tmp = _catData;
            do {
                if (tmp.catType == "Adult") {
                    isAdultCate = true;
                    break;
                }
                tmp = tmp.parent;
            } while (!!tmp && !!tmp.parent)
        })();
    };

    function getCharacterDiv(lineNum) {
        return _$("<li class='content kids_Character line_" + lineNum + "'>" +
            "<div class='kids_content'>" +
            "<div class='kids_characterBG_div'></div>" +
            "<img class='kids_character_img'>" +
            "<div class='kids_character_name_BG'></div>" +
            "<div class='kids_character_name'></div>" +
            "</div>" +
            "</li>"
        );
    }

    this.setData = function (_dataList, page, _sortGb, _itemCnt) {
        var contList = this.div.find("li");
        if (_dataList) {
            dataList = _dataList;
            itemCnt = _itemCnt;
            totalPage = Math.ceil(itemCnt / (LINE_AMOUNT * 2));
            this.indicator.setSize(totalPage, page);
        } else if (page == dpPage) return;

        dpPage = page;
        if (totalPage > 1) {
            //top Page
            tmp = page;
            for (var i = 0; i < LINE_AMOUNT * 2; i++) setItem(contList.eq(i), tmp * LINE_AMOUNT * 2 + i);
            //2rd Page
            var tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0; i < LINE_AMOUNT * 2; i++) setItem(contList.eq(i + LINE_AMOUNT * 2), tmp * LINE_AMOUNT * 2 + i);
            //bottom Page
            var tmp = HTool.getIndex(page, 2, totalPage);
            for (var i = 0; i < LINE_AMOUNT; i++) setItem(contList.eq(i + LINE_AMOUNT * 4), tmp * LINE_AMOUNT * 2 + i);
            this.div.removeClass("singlePage");
        } else {
            for (var i = 0; i < LINE_AMOUNT * 5; i++) setItem(contList.eq(i), i);
            this.div.addClass("singlePage");
        }
        if (isFocused && !this.indicator.isFocused()) this.setFocus(focus);
    };

    this.updateData = function (_dataList) {
        dpPage = -1;
        dataList = _dataList;
        // this.setFocus(focus, true);
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if (data) {
            div.removeClass("hidden_cont");
            div.attr("id", getDataListId(idx));
            if (data.w3ListImgUrl) div.find(".kids_character_img").attr({"src": data.w3ListImgUrl, onerror:"this.src='" + modulePath + "resource/image/kids_cha_default2.png'"});
            else div.find(".kids_character_img").attr("src", modulePath + "resource/image/kids_cha_default2.png'");
            div.find(".kids_character_name").text(data.name);
        } else {
            div.addClass("hidden_cont");
            div.attr("id", null);
            div.find(".kids_character_name").text("");
        }
    }

    function setIndicatorFocus(that, focused) {
        if (focused) {
            that.indicator.focused();
            that.div.find("li.focus").removeClass('focus');
        } else {
            that.indicator.blurred();
            that.setFocus(isChangedPage ? page * LINE_AMOUNT * 2 : focus);
            isChangedPage = false;
        }
    }

    this.changeFocus = function (amount) {
        var newFocus = focus;
        // Set Focus
        if (amount == 1 && focus == itemCnt - 1) newFocus = 0;
        else do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(itemCnt / LINE_AMOUNT) * LINE_AMOUNT);
        while (Math.floor(newFocus / LINE_AMOUNT) > Math.floor((itemCnt - 1) / LINE_AMOUNT));
        if (newFocus >= itemCnt) newFocus = itemCnt - 1;
        focus = newFocus;
        // Set Page
        this.div.find("li.focus").removeClass('focus');
        if (page !== Math.floor(newFocus / (LINE_AMOUNT * 2))) this.changePage((amount > 0 ? 1 : -1));

        if (dataList[focus]) {
            this.div.find("li#" + getDataListId(focus)).addClass('focus');
            // Set Blur
            this.parent.viewMgr.changeBackground(this.div.find("li#" + getDataListId(focus) + " .posterImg").attr("src"), 500);
            //this.parent.viewMgr.setHomeShot(dataList[focus].hsTargetType, dataList[focus].hsTargetId, dataList[focus].w3HsImgUrl, dataList[focus].hsLocator, dataList[focus].hsLocator2, dataList[focus].hsKTCasLocator);
            // Focus Title Animation
            setTextAnimation(this.div, focus);
        }

        if (focus % (LINE_AMOUNT * 2) + 1 > LINE_AMOUNT && !isLoading) {
            isLoading = true;
            this.parent.loadAddedContents(((page + 1) * LINE_AMOUNT * 2 + LINE_AMOUNT), LINE_AMOUNT * 2, function () {
                if (loadedCallback) loadedCallback();
                loadedCallback = null;
                isLoading = false;
            });
        }
    };

    function setTextAnimation(div, focus) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        if (UTIL.getTextLength(dataList[focus].itemName, "RixHead L", 30) > 215) {
            var posterDiv = div.find("li#" + getDataListId(focus) + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.changePage = function (amount) {
        if (totalPage == 1) return;
        var that = this;
        page = HTool.getIndex(page, amount, totalPage);

        if (amount < 0) this.parent.loadAddedContents((page * LINE_AMOUNT * 2), LINE_AMOUNT * 2, function () {
            changePageImpl(that)
        }, true);
        else if (isLoading) {
            LayerManager.startLoading({preventKey: true});
            loadedCallback = function () {
                changePageImpl(that);
                LayerManager.stopLoading();
            };
        } else this.parent.loadAddedContents((page * LINE_AMOUNT * 2) + LINE_AMOUNT, LINE_AMOUNT * 2, function () {
            changePageImpl(that)
        }, true);

        function changePageImpl(that) {
            that.div.removeClass("slide_up slide_down");
            that.setData(null, HTool.getIndex(page, amount > 0 ? -1 : 0, totalPage));
            void that.div[0].offsetWidth;
            that.div.addClass("slide_" + (amount > 0 ? 'up' : 'down'));
            that.indicator.setPos(page);
            that.div.find(".hidden_line").removeClass("hidden_line");

            if (page == totalPage - 1) setLineVisible(that.div.find(".line_" + (amount > 0 ? "4" : "2")), 0);
            if (amount > 0) setLineVisible(that.div.find(".line_0,.line_1"), 500);
            else clearLineVisible();
        }
    };

    this.setFocus = function (newFocus, forced) {
        if (!dataList[newFocus]) return;
        focus = newFocus;
        if (forced || page != Math.floor(newFocus / (LINE_AMOUNT * 2))) this.setPage(Math.floor(newFocus / (LINE_AMOUNT * 2)));
        this.div.find("li.focus").removeClass('focus');
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        this.parent.viewMgr.changeBackground(this.div.find("li#" + getDataListId(focus) + " .posterImg").attr("src"), 500);
        //if (!forced) this.parent.viewMgr.setHomeShot(dataList[focus].hsTargetType, dataList[focus].hsTargetId, dataList[focus].w3HsImgUrl, dataList[focus].hsLocator, dataList[focus].hsLocator2, dataList[focus].hsKTCasLocator);
        setTextAnimation(this.div, focus);
    };

    this.setPage = function (newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
        this.div.find(".hidden_line").removeClass("hidden_line");
        if (page == totalPage - 1) setLineVisible(this.div.find(".line_2"), 0);
    };

    function clearLineVisible() {
        if (lineVisibleTimeout) {
            clearTimeout(lineVisibleTimeout);
            lineVisibleTimeout = null;
        }
    }

    function setLineVisible(div, time) {
        clearLineVisible();
        if (time == 0) div.addClass("hidden_line");
        else lineVisibleTimeout = setTimeout(function () {
            div.addClass("hidden_line");
        }, time);
    }

    this.onKeyAction = function (keyCode) {
        log.printDbg("onKeyAction" +keyCode);
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.RED:
                if (focus % (LINE_AMOUNT*2) === 0) {
                    if (totalPage<=1) return true;
                    if (focus == 0 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    this.changeFocus(-(LINE_AMOUNT*2));
                } else this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                if (focus === itemCnt-1) {
                    if (totalPage<=1) return true;
                    this.changeFocus(1);
                    this.changeFocus(7);
                } else if (focus%(LINE_AMOUNT*2) === LINE_AMOUNT*2-1) {
                    if (totalPage<=1) return true;
                    if (page==totalPage-2 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    else this.changeFocus(LINE_AMOUNT*2);
                } else this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
            case KEY_CODE.LEFT:
                if (focus % LINE_AMOUNT != 0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.ENTER:
                this.pause();
                LayerManager.activateLayer({
                    obj: {
                        id: "KidsSubHomeCharacter",
                        type: Layer.TYPE.NORMAL,
                        priority: Layer.PRIORITY.NORMAL,
                        linkage: false,
                        params: {menu: this.getFocusedContents()}
                    },
                    moduleId: "module.kids",
                    new: true,
                    visible: true
                });
                // return false;
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN:
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.LEFT:
                return false;
            case KEY_CODE.ENTER:
            case KEY_CODE.RIGHT:
                setIndicatorFocus(this, false);
                return true;
            case KEY_CODE.RED:
                setIndicatorFocus(this, false);
                this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                setIndicatorFocus(this, false);
                this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
        }
    };

    this.getFocusedContents = function () {
        return dataList[focus];
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        if(dataList[dataIdx]) return ( dataList[dataIdx].id || dataList[dataIdx].contsId );
        else return "";
    }

    this.focused = function() {
        if(!dataList || dataList.length<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        clearLineVisible();
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
        this.parent.viewMgr.changeBackground(null, 0);
    };

    this.pause = function () {
        // clearLineVisible();
        // this.setFocus(focus, true);
        // this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
    };

    this.resume = function () {
        setTextAnimation(this.div, focus);
    };
};

CharacterIconList.prototype = new ContentsList();
CharacterIconList.prototype.constructor = CharacterIconList;