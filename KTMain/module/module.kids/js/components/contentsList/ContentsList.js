/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.ContentsList = function () {
    this.div;
    this.cateId;
    this.indicator;
    this.parent;

    this.create = function(cateId) {
        this.div = _$("<div/>", {class:"contentsList"});
        this.cateId = cateId;
    };

    this.setData = function(data) {

    };

    this.onKeyAction = function(keyCode) {

    };

    this.focused = function() {

    };

    this.blurred = function() {

    };

    this.getView = function() {
        return this.div;
    };

    this.getFocusedContents = function() {
        return null;
    };

    this.setIndicator = function(_indicator) {
        this.indicator = _indicator;
    }
};