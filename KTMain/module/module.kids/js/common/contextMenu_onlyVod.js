/**
 * Created by ksk91_000 on 2017-03-25.
 */
var ContextMenu_OnlyVOD = function() {
    var div = _$("<div/>", {class: 'context_menu_area vod_only_context_menu'});
    var contextMenu = _$("<div/>", {class: 'context_area'});
    var vodArea = _$("<div/>", {class: "vod_info_area"});

    var vodInfo;
    var listener;
    var vodFocus = 0;
    var recentlyVod = [];
    var recentlyVodArea = [];

    var that = this;
    var openedDropBox;
    var isKidsOn = false;

    div.append(_$("<div/>", {class: 'background_dim'}));
    div.append(_$("<div/>", {class: 'left_arrow'}));
    div.append(contextMenu);

    (function() {
        contextMenu.append(vodArea);

        vodArea.html(
            "<div class='content_poster_div'>" +
                "<img class='content_poster'>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img class='poster_contextLock_img' src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>" +
            "<div class='btn'>찜하기</div>" +
            "<div class='btn'>플레이리스트 추가</div>" +
            "<div class='title recently_text'>최근 확인한 VOD</div>" +
            "<div class='no_content_text'><span>최근 확인한<br>VOD가 없습니다</span></div>" +
            "<div class='recently_content_notSupply'><span>키즈모드에서는<br>제공되지 않습니다</span></div>" +
            "<div class='recently_content'>" +
                "<div class='recently_content_imgWrapper'>" +
                "<img>" + "</div>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + window.modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>" +
            "<div class='recently_content'>" +
                "<div class='recently_content_imgWrapper'>" +
                "<img>" + "</div>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + window.modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>");
        recentlyVodArea[0] = vodArea.find(".recently_content").eq(0);
        recentlyVodArea[1] = vodArea.find(".recently_content").eq(1);
    })();

    this.open = function() {
        setFocus(0);
        div.addClass("activate");
        recentlyVod = JSON.parse(StorageManager.ms.load(StorageManager.KEY.RECENTLY_VOD)) || [];
        if (KIDS_MODE_MANAGER.isKidsMode()) {
            isKidsOn = true;
            div.find(".vod_info_area .btn:eq(0)").css("display", "none");
            div.find(".vod_info_area .recently_content_notSupply").css("display", "block");
            div.find(".vod_info_area .no_content_text").css("display", "none");
            div.find(".vod_info_area .recently_content").css("display", "none");
        } else {
            isKidsOn = false;
            div.find(".vod_info_area .btn:eq(0)").css("display", "block");
            div.find(".vod_info_area .recently_content_notSupply").css("display", "none");
            div.find(".vod_info_area .no_content_text").css("display", "block");
            div.find(".vod_info_area .recently_content").css("display", "block");
            setRecentlyContents();
        }
        div.find(".vod_info_area .content_poster_div .poster_contextLock_img").removeClass("show");
        if(UTIL.isLimitAge(vodInfo.prInfo)) {
            div.find(".vod_info_area .content_poster_div .poster_contextLock_img").addClass("show");
        }

        setFocus(isKidsOn ? 1 : 0);
    };

    this.close = function() {
        div.removeClass("activate");
    };

    this.isOpen = function() {
        return div.hasClass("activate");
    };

    this.setVodInfo = function(options) {
        vodInfo = options;
        contextMenu.find(".content_poster").attr("src", vodInfo.imgUrl);
        div.find(".content_poster_div .poster_contextLock_img").toggle(UTIL.isLimitAge(vodInfo.prInfo));
    };

    function setFocus(_focus) {
        vodFocus = _focus;
        vodArea.find(".focus").removeClass("focus");
        switch (_focus) {
            case 0:
            case 1:
                vodArea.find(".btn").eq(vodFocus).addClass("focus");
                break;
            case 2:
            case 3:
                recentlyVodArea[vodFocus-2].addClass("focus");
                recentlyVodArea[vodFocus-2].find(".shdw_area").addClass("focus");
                recentlyVodArea[vodFocus-2].find(".recently_content_imgWrapper").addClass("focus");
                break;
        }
    }

    function setRecentlyContents() {
        var i = 0, contsArea = div.find(".recently_content"), imgArea = div.find(".recently_content_imgWrapper > img");
        for (i = 0; i < recentlyVod.length; i++) {
            contsArea.eq(i).show().find(".icon_area").toggle(UTIL.isLimitAge(recentlyVod[i].prInfo));
            imgArea.eq(i).show().attr("src", recentlyVod[i].imgUrl);
        }
        for (; i < 2; i++) {
            contsArea.eq(i).hide();
        }
        if (recentlyVod.length == 0) {
            setFocus(0);
        } else if (vodFocus > recentlyVod.length + 1) {
            setFocus(vodFocus - 1);
        }
        div.find(".vod_info_area .no_content_text").css("display", (recentlyVod.length > 0 ? "none" : "block"));
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (isKidsOn) return true;
                setFocus(HTool.getIndex(vodFocus, -1, 2 + Math.min(2, recentlyVod.length)));
                return true;
            case KEY_CODE.DOWN:
                if (isKidsOn) return true;
                setFocus(HTool.getIndex(vodFocus, 1, 2 + Math.min(2, recentlyVod.length)));
                return true;
            case KEY_CODE.LEFT:
                this.close();
                return true;
            case KEY_CODE.RIGHT:
                return true;
            case KEY_CODE.RED:
                if (vodFocus == 2 || vodFocus == 3) {
                    recentlyVod.splice(vodFocus - 2, 1);
                    StorageManager.ms.save(StorageManager.KEY.RECENTLY_VOD, JSON.stringify(recentlyVod));
                    setRecentlyContents();
                }
                return true;
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
                this.close();
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
        }
    };

    function enterKeyAction() {
        if (vodFocus >= 0) {
            switch (vodFocus) {
                case 0:
                    addWishList(vodInfo.itemType, vodInfo.catId, vodInfo.contsId, (vodInfo.cmbYn == "Y" || vodInfo.cmbYn == true), vodInfo);
                    that.close();
                    return true;
                case 1:
                    // vodInfo.isKids = true;
                    LayerManager.activateLayer({
                        obj: {
                            id: "AddToMyPlayListPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: vodInfo
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    that.close();
                    return true;
                case 2:
                case 3:
                    that.close();
                    var vod = recentlyVod[vodFocus - 2];
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_CONTEXT, vod.catId, vod.contsId, "");
                    } catch(e) {}
                    openDetailLayer(vod.catId, vod.contsId, "", "", vod.isKids);
                    return true;
            }
        } else switch (menuList[focus].type) {
            case ContextMenu.ITEM_TYPE.BUTTON:
                listener(focus);
                return true;
            case ContextMenu.ITEM_TYPE.DROP_BOX:
                openedDropBox = menuList[focus].item;
                openedDropBox.open();
                return true;
        }

        function addWishList(itemType, catId, contsId, cmbYn, item) {
            if(itemType==1 || itemType==0) catId = contsId;
            try {
                kidsSubHome.amocManager.setPlayListNxt(1, itemType, contsId, catId, 1, (cmbYn === "Y" || cmbYn === true)? "Y" : "", function (result, response) {
                    if (result) {
                        if (response.reqCode == 0) {
                            // 정상등록
                            var familyHomeModule = ModuleManager.getModule("module.family_home");
                            if(familyHomeModule) familyHomeModule.execute({method: "setWishListFlag"});
                            showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다");
                        } else if (response.reqCode == 1) {
                            //삭제 확인 팝업 오픈
                            LayerManager.activateLayer({
                                obj: {
                                    id: "KidsVODWishConfirm",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage : true,
                                    params: {
                                        callback: function() {
                                            kidsSubHome.amocManager.setPlayListNxt(1, itemType, contsId, catId, 2, (cmbYn === "Y" || cmbYn === true)? "Y" : "", function (result, response) {
                                                if (result) {
                                                    if(response.reqCode == 0) {
                                                        //정상삭제
                                                        var familyHomeModule = ModuleManager.getModule("module.family_home");
                                                        if(familyHomeModule) familyHomeModule.execute({method: "setWishListFlag"});
                                                        showToast("VOD 찜을 해제하였습니다");
                                                    } else throw "networkError(4)";
                                                } else throw "networkError(3)";
                                            });
                                        }
                                    }
                                },
                                moduleId: "module.kids",
                                visible: true
                            });
                        } else if (response.reqCode == 3) throw "networkError(2)";
                    } else throw "networkError(1)";
                });
            } catch(e) {
                log.printErr(e);
            }
        }
    }

    this.removeFocus = function() {
        vodArea.find(".focus").removeClass("focus");
    };

    this.addFocus = function() {
        vodArea.find(".btn").eq(vodFocus).addClass("focus");
    };

    this.getView = function() {
        return div;
    };
};