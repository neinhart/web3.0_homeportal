/**
 * Created by ksk91_000 on 2017-03-25.
 */
var ContextMenu = function () {
    var div = _$("<div/>", {class:'context_area'});
    var listener;
    var menuList = [];
    var focus = 0;

    var openedDropBox;

    this.open = function () {
        div.addClass("activate");
        setFocus(focus);
    };

    this.close = function () {
        div.removeClass("activate");
    };

    this.isOpen = function () {
        return div.hasClass("activate");
    };

    this.addButton = function(title, classNm) {
        var item = _$("<div/>", {class:"btn " + (classNm?classNm:"")}).text(title);
        menuList.push({type: ContextMenu.ITEM_TYPE.BUTTON, item:item});
        div.append(item);
    };

    this.addSeparateLine = function (classNm) {
        var item = _$("<div/>", {class:"sep_line " + (classNm?classNm:"")});
        div.append(item);
    };

    this.addTitle = function (title, classNm) {
        var item = _$("<div/>", {class:"title " + (classNm?classNm:"")}).text(title);
        div.append(item);
    };

    this.addDropBox = function (options, focus, classNm) {
        var item = new dropBox(options, classNm);
        item.setSelectedIdx(focus||0);

        menuList.push({type:ContextMenu.ITEM_TYPE.DROP_BOX, item:item});
        div.append(item.getView());
    };

    this.setEventListener = function (eventListener) {
        listener = eventListener;
    };

    function setFocus(_focus) {
        menuList[focus].item.removeClass("focus");
        focus = _focus;
        menuList[focus].item.addClass("focus");
    }

    this.onKeyAction = function(keyCode) {
        if(openedDropBox) return openedDropBox.onKeyAction(keyCode);
        switch (keyCode) {
            case KEY_CODE.UP:
                setFocus(HTool.getIndex(focus, -1, menuList.length));
                return true;
            case KEY_CODE.DOWN:
                setFocus(HTool.getIndex(focus, 1, menuList.length));
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
            case KEY_CODE.LEFT:
                this.close();
                return true;
            case KEY_CODE.RIGHT:
                return true;
        }
    };

    function enterKeyAction() {
        switch (menuList[focus].type) {
            case ContextMenu.ITEM_TYPE.BUTTON:
                listener(focus);
                return true;
            case ContextMenu.ITEM_TYPE.DROP_BOX:
                openedDropBox = menuList[focus].item;
                openedDropBox.open();
                return true;
        }
    }

    this.removeFocus = function () {
        menuList[focus].item.removeClass("focus");
    };

    this.addFocus = function () {
        menuList[focus].item.addClass("focus");
    };

    this.getView = function () {
        return div;
    };

    var dropBox = function (options, classNm) {
        var selectedIdx;
        var indexList = [];
        var focusIdx = 0;
        var div = _$("<div/>", {class:"drop_box " + (classNm?classNm:"")}).html("<span class='text'></span><div class='options'></div>");

        (function init() {
            var optionArea = div.find(".options");
            for(var i=0; i<options.length; i++) {
                optionArea.append(_$("<div/>", {class: 'option'}).text(options[i]));
                indexList[i] = i;
            }
        })();

        function sortOption() {
            div.find(".checked").removeClass("checked");
            var optionArea = div.find(".options .option");
            var isSetOption = false;
            optionArea.eq(selectedIdx).addClass("checked").text(options[selectedIdx]);
            // for(var i=0; i<options.length; i++) {
            //     if(i==selectedIdx) {
            //         optionArea.eq(0).addClass("checked").text(options[i]);
            //         indexList[0] = i;
            //         isSetOption = true;
            //     }else {
            //         optionArea.eq(isSetOption?i:(i+1)).text(options[i]);
            //         indexList[isSetOption?i:(i+1)] = i;
            //     }
            // }
        }

        this.setSelectedIdx = function(index) {
            selectedIdx = index;
            div.find(".text").text(options[selectedIdx]);
            sortOption();
        };

        this.setFocus = function(focus) {
            div.find(".focus").removeClass("focus");
            focusIdx = focus;
            div.find(".option").eq(focus).addClass("focus");
        };

        this.open = function () {
            this.setFocus(selectedIdx);
            div.addClass("open");
        };

        this.close = function () {
            openedDropBox = null;
            div.removeClass("open");
        };

        this.onKeyAction = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    this.setFocus(HTool.getIndex(focusIdx, -1, options.length));
                    return true;
                case KEY_CODE.DOWN:
                    this.setFocus(HTool.getIndex(focusIdx, 1, options.length));
                    return true;
                case KEY_CODE.ENTER:
                    this.setSelectedIdx(indexList[focusIdx]);
                    this.close();
                    listener(focus, selectedIdx);
                    return true;
                case KEY_CODE.BACK:
                    this.close();
                    return true;
                case KEY_CODE.CONTEXT:
                case KEY_CODE.LEFT:
                    return true;
            }
        };

        this.addClass = function(classNm) { div.addClass(classNm) };
        this.removeClass = function(classNm) { div.removeClass(classNm) };

        this.getView = function () {
            return div;
        }
    }
};

Object.defineProperty(ContextMenu, "ITEM_TYPE", {
   value: {"BUTTON":0, "DROP_BOX": 1}
});