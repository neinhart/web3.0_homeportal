/**
 * Created by ksk91_000 on 2016-12-14.
 *
 [타입 설명]
 1. 리스트 형태의 좌측에 노출되는 Scroll

 9개 이상일 경우는 dot을 제거하고 최종 페이지 기준으로 균등하게 scroll
 9개 이하, 5개 이상일 경우는 전체 길이에서 dot/간격을 균등하게 배치
 4개이하일 경우는 짧은 스크롤을 적용 (방식은 상동)

 2.VOD(키즈) 시리즈 상세 Scroll

 4개 이하일 경우 전체 길이에서 dot/간격을 균등하게 배치
 4개 이상일 경우 dot을 제거하고 최종 페이지 기준으로 균등하게 scroll

 3. VOD(키즈) 상세시놉 디테일 좌측 Scroll
 3개 이하일 경우 전체 길이에서 dot/간격을 균등하게 배치
 3개 이상일 경우 dot을 제거하고 최종 페이지 기준으로 균등하게 scroll

 4. 채널가이드(오디오) 채널 리스트 스크롤

 6개 이하일 경우 전체 길이에서 dot/간격을 균등하게 배치
 6개 이상일 경우 dot을 제거하고 최종 페이지 기준으로 균등하게 scroll

 5.검색결과 리스트 Scroll

 4개 이하일 경우 전체 길이에서 dot/간격을 균등하게 배치
 4개 이상일 경우 dot을 제거하고 최종 페이지 기준으로 균등하게 scroll
 */
window.Indicator = function (type) {
    var div = _$("<div/>", {class:"pageIndicator"});
    var pageTransDiv = _$("<div/>", {class:"pageTransIcon"}).hide();
    var size = 1;
    var pos = 0;

    var prefList = [
        {totalSize:420, dotHidePivot:9},
        {totalSize:720, dotHidePivot:9},
        {totalSize:420, dotHidePivot:4},
        {totalSize:274, dotHidePivot:3},
        {totalSize:570, dotHidePivot:6},
        {totalSize:420, dotHidePivot:4},
        {totalSize:568, dotHidePivot:9}
    ];
    var curPref = prefList[type];

    var setSize = function (_size, pos) {
        size = _size;
        // [hw.boo] 스크롤 길이 고정으로 수정됨
        // if(type==1) curPref = prefList[size<5?0:1];
        div.find("#endCircle").text(size).css("-webkit-transform", "translateY(" + curPref.totalSize + "px)");
        div.find("#endCircle").removeClass("digit3 digit4").toggleClass("digit3", size>99).toggleClass("digit4", size>999);
        div.find(".baseLine").css("height", curPref.totalSize-38 + "px");
        setPos(pos||0);
        createDots(size-2);
        div.toggle(size>1);
        pageTransDiv.toggle(size>1);
    };

    var setPos = function (position) {
        pos = position;
        if(curPref.totalSize/(size-1)<38) {
            if(pos==0) div.find(".focusCircle").css("-webkit-transform", "translateY(0)").find(".focus_number").text(position + 1);
            else if(pos==size-1) div.find(".focusCircle").css("-webkit-transform", "translateY(" + curPref.totalSize + "px)").find(".focus_number").text(position + 1);
            else div.find(".focusCircle").css("-webkit-transform", "translateY(" + ((curPref.totalSize-76) / (size - 3) * (pos-1) + 38) + "px)").find(".focus_number").text(position + 1);
        }else {
            div.find(".focusCircle").css("-webkit-transform", "translateY(" + curPref.totalSize / (size - 1) * pos + "px)").find(".focus_number").text(position + 1);
        }

        div.find(".focusCircle").removeClass("digit3 digit4").toggleClass("digit3", pos>=99).toggleClass("digit4", pos>=999);
        div.find(".focusArrow_top").toggleClass("show", pos>0);
        div.find(".focusArrow_bottom").toggleClass("show", pos<size-1);
    };

    var getView = function() {
        return div;
    };

    var focused = function () {
        div.addClass("focus");
    };

    var blurred = function () {
        div.removeClass("focus");
    };

    var getPageTransView = function () {
        return pageTransDiv;
    };

    var togglePageTransIcon = function (isShow) {
        pageTransDiv.toggleClass("hidden", isShow);
    };

    function constructDiv() {
        div.append(_$("<div/>", {id:'startCircle', class:'baseCircle'}).text("1"));
        div.append(_$("<div/>", {class:'baseLine'}));
        div.append(_$("<div/>", {id:'endCircle', class:'baseCircle'}));
        div.append(_$("<div/>", {id:'dotArea'}));
        div.append(_$("<div/>", {class:'focusCircle'}));
        div.find(".focusCircle").append(_$("<div/>", {class:'focus_number'}));
        div.find(".focusCircle").append(_$("<div/>", {class:'focusArrow_top'}));
        div.find(".focusCircle").append(_$("<div/>", {class:'focusArrow_bottom'}));
    }

    function createDots(amount) {
        div.find("#dotArea").html("");
        if(amount<=0 || amount>curPref.dotHidePivot-2) return;
        for(var i=0; i<amount; i++) {
            div.find("#dotArea").append("<div class='dotCircle' style='top:" + (curPref.totalSize/(size-1)*(i+1)) + "px'>");
        }
    }

    constructDiv();
    return {
        setSize: setSize,
        setPos: setPos,
        getView: getView,
        focused: focused,
        blurred: blurred,
        isFocused: function () {
            return div.hasClass("focus");
        },
        getPageTransView: getPageTransView,
        togglePageTransIcon: togglePageTransIcon
    }
};