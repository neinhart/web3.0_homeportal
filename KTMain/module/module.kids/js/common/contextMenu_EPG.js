/**
 * Created by Yun on 2017-05-27.
 */

var ContextMenu_EPG = function () {
    var div = _$("<div/>", {class: "context_menu_area epg_context_menu"});
    var contextMenu = _$("<div/>", {class: "context_area epg_context_menu"});
    var epgArea = _$("<div/>", {class: "epg_info_area"});
    var contextArea = _$("<div/>", {class: "context_menu_area"});
    var contextMenuList = _$("<div/>", {class: "context_menu_area_list"});

    var epgInfo;
    var curChannel;
    var menuList = [];
    var leftFocusIdx = 0;
    var rightFocusIdx = 0;
    var isLeftFocus = true;
    var totalPage = 0;
    var curPage = 0;
    var that = this;
    var listener;
    var is_ots = false;

    div.append(_$("<div/>", {class:"background_dim"}));
    div.append(_$("<div/>", {class:"left_arrow"}));
    div.append(_$("<div/>", {class:"nextPage_title"}).text("장르별 채널"));
    div.append(_$("<img>", {class:"epgPage_up_icon", src: window.modulePath + "resource/image/arw_related_up.png"}));
    div.append(_$("<img>", {class:"epgPage_dw_icon", src: window.modulePath + "resource/image/arw_related_dw.png"}));
    div.append(contextMenu);

    (function () {
        contextArea.append(contextMenuList);
        contextMenu.append(epgArea).append(contextArea);
        if (window.CONSTANT.IS_OTS) {
            is_ots = true;
            epgArea.html("<div class='favorite_epg_area'>" +
                "<div class='title favorite_epg_text'></div>" +
                "<div class='btn favorite_btn'><span>선호 채널 등록</span><img class='favor_icon_img' src='" + window.modulePath + "resource/image/icon_favorite_d.png'></div>" +
                "<div class='btn detail_btn'><span>자세히</span></div>" +
                "</div>" +
                "<div class='epg4Ch_area'>" +
                "<div class='title epg4ch_text'>4채널 동시시청</div>" +
                "<div class='epg4ch_btn_area'>" +
                "<img class='epg4ch_sdw_img' src='" + window.modulePath + "resource/image/op_btn_ch_sdw.png'>" +
                "<img class='epg4ch_btn_img' src='" + window.modulePath + "resource/image/op_btn_4ch.png'>" +
                "</div>" +
                "</div>" +
                "<div class='realTimeChEpg_area'>" +
                "<div class='title realTimeChEpg_text'>실시간 인기채널</div>" +
                "<div class='realTimeChEpg_btn_area'>" +
                "<img class='realTimeChEpg_sdw_img' src='" + window.modulePath + "resource/image/op_btn_ch_sdw.png'>" +
                "<img class='realTimeChEpg_btn_img' src='" + window.modulePath + "resource/image/op_btn_popul.png'>" +
                "</div>" +
                "</div>");
        } else {
            is_ots = false;
            epgArea.html("<div class='favorite_epg_area'>" +
                "<div class='title favorite_epg_text'></div>" +
                "<img class='epgThumbnail_img_default' src='" + window.modulePath + "resource/image/default_thumb_kids.jpg'>" +
                "<img class='epgThumbnail_img'>" +
                "<div class='btn favorite_btn'><span>선호 채널 등록</span><img class='favor_icon_img' src='" + window.modulePath + "resource/image/icon_favorite_d.png'></div>" +
                "</div>" +
                "<div class='realTimeChEpg_area'>" +
                "<div class='title realTimeChEpg_text'>실시간 인기채널</div>" +
                "<div class='realTimeChEpg_btn_area'>" +
                "<img class='realTimeChEpg_sdw_img' src='" + window.modulePath + "resource/image/op_btn_ch_sdw.png'>" +
                "<img class='realTimeChEpg_btn_img' src='" + window.modulePath + "resource/image/op_btn_popul.png'>" +
                "</div>" +
                "</div>" +
                "<div class='epg4Ch_area'>" +
                "<div class='title epg4ch_text'>4채널 동시시청</div>" +
                "<div class='epg4ch_btn_area'>" +
                "<img class='epg4ch_sdw_img' src='" + window.modulePath + "resource/image/op_btn_ch_sdw.png'>" +
                "<img class='epg4ch_btn_img' src='" + window.modulePath + "resource/image/op_btn_4ch.png'>" +
                "</div>" +
                "</div>");
        }
    })();

    this.setCurrentChannel = function (data) {
        curChannel = data;
    };

    this.open = function () {
        div.addClass("activate");
        if (window.favoriteChannelManager.isFavoriteChannel(curChannel)) {
            div.find(".favorite_epg_area .btn.favorite_btn:eq(0) span").text("선호 채널 해제");
            div.find(".favorite_epg_area .btn.favorite_btn .favor_icon_img").attr("src", window.modulePath + "resource/image/icon_favorite.png");
        } else {
            div.find(".favorite_epg_area .btn.favorite_btn:eq(0) span").text("선호 채널 등록");
            div.find(".favorite_epg_area .btn.favorite_btn .favor_icon_img").attr("src", window.modulePath + "resource/image/icon_favorite_d.png");
        }
        if (!is_ots) div.find(".epgThumbnail_img").attr("src", "http://image.ktipmedia.co.kr/channel/CH_" + curChannel.data.sid + ".png").load(function() {
            _$(this).css("display", "block");
        }).on('error', function() {
            _$(this).css("display", "none");
        });
        div.find(".title.favorite_epg_text").text(curChannel.chNum + " " + curChannel.chName);
        leftFocusIdx = 0;
        rightFocusIdx = 0;
        isLeftFocus = true;
        totalPage = div.find(".context_menu_area_list").height() / div.find(".context_menu_area").height();
        div.find(".epgPage_up_icon").removeClass("show");
        div.find(".epgPage_dw_icon").removeClass("show");

        if(totalPage > 1 && menuList.length > 0) div.find(".epgPage_dw_icon").addClass("show");
        curPage = 0;
        div.find(".context_menu_area").css("margin-top", "52px");
        buttonFocusRefresh(leftFocusIdx, isLeftFocus);
        changePage(curPage);
    };

    this.close = function () {
        div.removeClass("activate");
    };

    this.isOpen = function () {
        return div.hasClass("activate");
    };

    function buttonFocusRefresh(index, isValue) {
        div.find(".epg_info_area .favorite_epg_area .btn").removeClass("focus");
        div.find(".epg_info_area .epg4Ch_area").removeClass("focus");
        div.find(".epg_info_area .realTimeChEpg_area").removeClass("focus");
        div.find(".epg_context_menu .context_menu_area .btn").removeClass("focus");
        if (isValue) {
            if (is_ots) {
                switch (index) {
                    case 0:
                    case 1:
                        div.find(".epg_info_area .favorite_epg_area .btn:eq(" + index + ")").addClass("focus");
                        break;
                    case 2:
                        div.find(".epg_info_area .realTimeChEpg_area").addClass("focus");
                        break;
                    case 3:
                        div.find(".epg_info_area .epg4Ch_area").addClass("focus");
                        break;
                }
            } else {
                switch (index) {
                    case 0:
                        div.find(".epg_info_area .favorite_epg_area .btn").addClass("focus");
                        break;
                    case 1:
                        div.find(".epg_info_area .realTimeChEpg_area").addClass("focus");
                        break;
                    case 2:
                        div.find(".epg_info_area .epg4Ch_area").addClass("focus");
                        break;
                }
            }
        } else {
            div.find(".epg_context_menu .context_menu_area .btn").eq(index).addClass("focus");
            if (div.find(".epg_context_menu .context_menu_area .btn").eq(index).position().top + (curPage * 892) > ((curPage+1) * 970)) {
                curPage ++;
            } else if (div.find(".epg_context_menu .context_menu_area .btn").eq(index).position().top + (curPage * 892) < (curPage * 970)) {
                curPage --;
            }
            changePage(curPage);
        }
    }

    function changePage(pageIdx) {
        if (pageIdx > 0) div.find(".nextPage_title").addClass("show");
        else div.find(".nextPage_title").removeClass("show");
        div.find(".nextPage_title").text(div.find(".context_menu_area_list ." +menuList[pageIdx*11].titleClass).text());
        div.find(".epg_context_menu .context_menu_area_list .btn").removeClass("show");
        div.find(".epgPage_up_icon").removeClass("show");
        div.find(".epgPage_dw_icon").removeClass("show");
        for (var idx = (pageIdx*11) ; idx < 11 + (pageIdx*11); idx ++) {
            div.find(".epg_context_menu .context_menu_area_list .btn:eq(" + idx + ")").addClass("show");
        };
        if (pageIdx == 0) {
            div.find(".epgPage_dw_icon").addClass("show");
            div.find(".context_menu_area").css("margin-top", "52px");
        } else {
            div.find(".epgPage_up_icon").addClass("show");
            div.find(".context_menu_area").css("margin-top", "94px");
        }
        div.find(".context_menu_area_list").css("-webkit-transform", "translateY(" + (-pageIdx * 892) + "px)");
    }

    this.addButton = function(title, classNm, titleClass) {
        var item = _$("<div/>", {class:"btn " + (classNm?classNm:"")}).text(title);
        item.append(_$("<div/>", {class:"btn_check_img"}));
        menuList.push({type: ContextMenu.ITEM_TYPE.BUTTON, item:item, titleClass: titleClass});
        contextMenuList.append(item);
    };

    this.addSeparateLine = function (classNm) {
        var item = _$("<div/>", {class:"sep_line " + (classNm?classNm:"")});
        contextMenuList.append(item);
    };

    this.addTitle = function (title, classNm) {
        var item = _$("<div/>", {class:"title " + (classNm?classNm:"")}).text(title);
        contextMenuList.append(item);
    };

    this.getMenuListLength = function() {
        return menuList.length;
    };

    this.setEventListener = function (eventListener) {
        listener = eventListener;
    };

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (isLeftFocus) {
                    leftFocusIdx = HTool.getIndex(leftFocusIdx, -1, (is_ots ? 4 : 3));
                    buttonFocusRefresh(leftFocusIdx, isLeftFocus);
                } else {
                    rightFocusIdx = HTool.getIndex(rightFocusIdx, -1, menuList.length);
                    buttonFocusRefresh(rightFocusIdx, isLeftFocus);
                }
                return true;
            case KEY_CODE.DOWN:
                if (isLeftFocus) {
                    leftFocusIdx = HTool.getIndex(leftFocusIdx, 1, (is_ots ? 4 : 3));
                    buttonFocusRefresh(leftFocusIdx, isLeftFocus);
                } else {
                    rightFocusIdx = HTool.getIndex(rightFocusIdx, 1, menuList.length);
                    buttonFocusRefresh(rightFocusIdx, isLeftFocus);
                }
                return true;
            case KEY_CODE.LEFT:
                var consume = false;
                if (!isLeftFocus) {
                    isLeftFocus = true;
                    buttonFocusRefresh(leftFocusIdx, isLeftFocus);
                    consume = true;
                } else {
                    this.close();
                    setTimeout(function() {
                        LayerManager.deactivateLayer({
                            id: "KidsEpgRelatedMenuPopup"
                        });
                    }, 600);
                }
                return consume;
            case KEY_CODE.RIGHT:
                if (menuList.length == 0) return true;
                isLeftFocus = false;
                buttonFocusRefresh(rightFocusIdx, isLeftFocus);
                return true;
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
                this.close();
                setTimeout(function() {
                    LayerManager.deactivateLayer({
                        id: "KidsEpgRelatedMenuPopup"
                    });
                }, 600);
                return true;
            case KEY_CODE.ENTER:
                listener((isLeftFocus ? leftFocusIdx : rightFocusIdx), isLeftFocus);
                return true;
            default:
                return false;
        }
    };

    this.getView = function () {
        return div;
    };
};