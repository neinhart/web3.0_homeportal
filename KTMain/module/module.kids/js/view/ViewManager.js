/**
 * Created by ksk91_000 on 2016-08-19.
 */
kidsSubHome.ViewManager = function () {
    var parent;
    var div = _$("<div/>", {class:'viewArea'});

    var instance = this;

    var currentCateView;
    var currentContView;
    var contentsChangeTimeout = null;
    var contentsLoadAjax = null;

    var ViewMap = {};
    var historyStack = [];
    var removableQueue = [];

    var MODE = {CATEGORY:0, CONTENTS:1, KIDS_LIST:2};
    var curLoadViewId;

    var homeShot = new kidsSubHome.HomeShot();
    var kidsCharShot = new kidsSubHome.KidsCharacterShot();
    var mode = MODE.CATEGORY;
    var curBgIdx = 0;
    var bg_timer;
    var emptyView = new View("emptyView");

    var oldInfo;
    var curInfo;

    div.append(homeShot.getView());
    div.append(kidsCharShot.getView());

    homeShot.setKidsCharShot(kidsCharShot);

    var cateKeyAction = function(keyCode) {
        var oldView = currentCateView.getCurrentContent();
        var oldCateView = currentCateView;
        if(currentCateView.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.UP:
            case KEY_CODE.DOWN:
            case KEY_CODE.RED:
            case KEY_CODE.BLUE:
                clearContentsAjax();
                if(currentContView.removal) {
                    removeView(currentContView);
                }
                var view = currentCateView.getCurrentContent();
                setContentsChangeTimeout(function() {
                    setCurrentContentData(view);
                }, 300);
                try {
                    oldInfo = oldCateView.getCateInfo();
                    var prevInfo = oldView ;
                    NavLogMgr.collectKids(keyCode, oldInfo.id, oldInfo.name, prevInfo.id, prevInfo.name);
                } catch(e) {}
                return true;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                contensChangeImmediately();
                try {
                    oldInfo = oldCateView.getCateInfo();
                    curInfo = oldCateView.getCurrentContent();
                    NavLogMgr.collectKids(keyCode, oldInfo.id, oldInfo.name, curInfo.id, curInfo.name);
                } catch(e) {}
                return true;
        }
    };

    var changeFocus = function(view) {
        if(getCurrentView() === view)
            return;

        switch (view.type) {
            case View.TYPE.CATEGORY:
                historyStack.push(getCurrentView());
                var focusMode = view.focused();
                if(focusMode == kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS) {
                    historyBack();
                    return;
                } else if(focusMode == kidsSubHome.ViewManager.FOCUS_MODE.BLUR_FOCUS) {
                    var oldCateView = currentCateView;
                    setCurrentCategory(view, true);
                    oldCateView.unfocused ? oldCateView.unfocused() : 0;
                    oldCateView.show();
                } else {
                    view.setPage(0);
                    view.setFocus(0);
                    // 포커스가 변경되었으므로 contents 에 설정할 뷰를 새로 갱신해야 함
                    setCurrentCategory(view);
                    changeTitle(view.title);
                }
                break;

            case View.TYPE.KIDS_LIST:
                historyStack.push(getCurrentView());
                var focusMode = view.focused();
                if(focusMode == kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS) {
                    historyBack();
                    return;
                } else if(focusMode == kidsSubHome.ViewManager.FOCUS_MODE.BLUR_FOCUS) {
                    var oldCateView = currentCateView;
                    setCurrentCategory(view, true);
                    oldCateView.unfocused?oldCateView.unfocused():0;
                    oldCateView.show();
                } else {
                    setCurrentCategory(view);
                }
                break;

            case View.TYPE.CONTENTS:
                currentCateView.unfocused();
                historyStack.push(getCurrentView());
                mode = MODE.CONTENTS;
                if(currentContView!==view) setCurrentContent(view);
                var focusMode = view.focused();
                if(focusMode == kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS) {
                    historyBack();
                    return;
                }else if(focusMode == kidsSubHome.ViewManager.FOCUS_MODE.BLUR_FOCUS) currentCateView.unfocused?currentCateView.unfocused():0;
                else {
                    if(currentContView.viewId == MenuDataManager.MENU_ID.KIDS_CHANNEL) {
                        // 2018.01.26 sw,nam
                        // 키즈 편성표의 경우 좌측 category 리스트 숨기도록 수정
                        currentCateView.hide();
                    }
                    changeTitle(view.title ? view.title : view.getTitle());
                }
                break;
        }
        var rmQidx = removableQueue.indexOf(view);
        if(rmQidx>=0) removableQueue.splice(rmQidx, 1);
    };

    var changeCategoryFocus = function(idx, isEnter) {
        if(idx<0) return;
        if(mode == MODE.CATEGORY) {
            currentCateView.setFocus(idx);
        } 
        setCurrentContentData(currentCateView.getCurrentContent(), function () {
            if(isEnter) changeFocus(currentContView)
        });
    }; this.changeCategoryFocus = changeCategoryFocus;

    var changeTitle = function(title) {
        if (UTIL.getTextLength(title, "RixHead B", 40, -1.2) > 360) {
            parent.div.find("#backgroundTitle").css("font-size", "38px");
            var newTitle = HTool.ellipsis(title, "RixHead B", 38, 360, "...", -1.2);
            parent.div.find("#backgroundTitle").text(newTitle);
        }
        else {
            parent.div.find("#backgroundTitle").css("font-size", "40px");
            parent.div.find("#backgroundTitle").text(title);
        }
    };

    var changeBackground = function (src, time) {

            //var bgArea = VBOBackground;
            //if(bg_timer) clearTimeout(bg_timer);
            //bg_timer = setTimeout(function () {
            //     if(src==null) {
            //         bgArea.find("#background_blur").css("display", "none");
            //         bgArea.find("#background_blur .bg_wrapper").css("opacity", "");
            //     }else {
            //         bgArea.find("#background_blur").css("display", "");
            //         var bg = bgArea.find("#background_blur .bg_wrapper");
            //         bg.eq(curBgIdx).css("opacity", 0);
            //         curBgIdx ^= 1;
            //         bg.find(".bg").eq(curBgIdx).css({"background-image": "url(" + src + ")"});
            //         bg.eq(curBgIdx).css("opacity", 1);
            //     } bg_timer = null;
            //}, time);
            //
            //

    };
    this.changeBackground = changeBackground;

    /**
     * [dj.son] HomeShotDataManager 적용
     *
     * - 사용 가능한 모든 홈샷 데이터를 모아 노출 (실제 노출 로직은 HomeShot 컴포넌트안에 있고, 여기는 그냥 데이터 세팅)
     * - 각 View 에서 HomeShot 호출하던것 제거하고 ViewManager 에서 일괄 호출하도록 수정
     * -- 단 CategoryView 에서는 성인인증이 걸릴 수가 있으므로, CategoryView 에서는 유지
     */
    var setHomeShot = function (options) {
        var bShow = false;
        var arrHomeShotData = null;

        if (options && options.menu) {

            /**
             * [dj.son] DataConverter 에 있는 조건문 참조
             */
            if (options.menu.catType === MENU_CONSTANT.CAT.KIDSSUB || options.menu.id === MenuDataManager.MENU_ID.KIDS_PLAY_LIST
                || options.menu.catType === MENU_CONSTANT.CAT.RANK || options.menu.catType === MENU_CONSTANT.CAT.RANKW3 || options.menu.catType === MENU_CONSTANT.CAT.PWORLD) {
                bShow = false;
            }
            else  if ((options.menu.children && options.menu.children.length > 0)
                || options.menu.itemType === MENU_CONSTANT.ITEM.INT_M || options.menu.itemType === MENU_CONSTANT.ITEM.INT_U || options.menu.itemType === MENU_CONSTANT.ITEM.INT_W) {

                bShow = true;
            }

            if (bShow) {
                arrHomeShotData = HomeShotDataManager.getCategoryHomeShot({
                    menu: options.menu
                });
            }
        }

        homeShot.setHomeShot({
            bShow: bShow,
            arrHomeShotData: arrHomeShotData
        });

        if (!bShow) {
            setKidsCharacterShot();
        }
    };
    this.setHomeShot = setHomeShot;

    //this.setHomeShotHide = function() {
    //    // homeShot.setHomeShot(null);
    //    homeShot.getView().hide();
    //    this.setHideCharacter();
    //};
    //
    //this.setHomeShotShow = function() {
    //    // div.find(".homeShot_area").css("display", "block");
    //    if(homeShot.getType() !== null)
    //        homeShot.getView().show();
    //    else
    //       this.setShowCharacter();
    //};

    this.setHideCharacter = function() {
        div.find(".kidsCharacterShot_area").css("display", "none");
    };

    this.setShowCharacter = function() {
        div.find(".kidsCharacterShot_area").css("display", "block");
    };

    this.setHideClippingArea = function() {
        parent.div.find("#kidsDefaultBg").removeClass("clipping");
        parent.div.find("#pig_area").css("display", "none");
        parent.div.find(".main_clock").css("display", "none");
    };

    this.setShowClippingArea = function() {
        parent.div.find("#kidsDefaultBg").addClass("clipping");
        parent.div.find("#pig_area").css("display", "block");
        parent.div.find(".main_clock").css("display", "block");
    };

    var setKidsCharacterShot = function () {
        kidsCharShot.setParent(parent);
        kidsCharShot.setKidsCharacterShot();
    }; this.setKidsCharacterShot = setKidsCharacterShot;

    var historyBack = function () {
        if (historyStack.length > 0) {
            // [hw.boo] 로드 중인 데이터 & 페이지 clear(WEBIIIHOME-3642)
            clearContentsAjax();
            clearContentsChangeTimeout();
            var oldView = getCurrentView();
            var currentView = historyStack.pop();
            addRemovableQueue(oldView);
            switch (currentView.type) {
                case View.TYPE.CATEGORY:
                    mode = MODE.CATEGORY;
                    setCurrentCategory(currentView);
                    setCurrentContentData(currentView.getCurrentContent());
                    break;
                case View.TYPE.KIDS_LIST:
                    mode = MODE.CATEGORY;
                    oldView.blurred();
                    setCurrentCategory(currentView);
                    setCurrentContentData(currentView.getCurrentContent());
                    break;
                case View.TYPE.CONTENTS:
                    mode = MODE.CONTENTS;
                    setCurrentContent(currentView);
                    currentView.hide();
                    currentView.focused();
                    break;
            } changeTitle(currentView.title);
            return true;
        } else {
            LayerManager.historyBack();
            return true;
        }
    };
    this.historyBack = historyBack;

    var addView = function(view) {
        ViewMap[(view.viewId?view.viewId:view.menuId)] = ViewMap[(view.viewId?view.viewId:view.menuId)]||(div.append(view.getView()), view);
        view.viewMgr = instance;
    };

    var removeView = function (view) {
        var qIdx = removableQueue.indexOf(view);
        if(qIdx>0) removableQueue.splice(qIdx, 1);
        view.getView().remove();
        delete ViewMap[view.viewId];
    };

    var setCurrentCategory = function (view, noChangePreview) {
        mode = MODE.CATEGORY;
        currentCateView?currentCateView.hide():0;
        currentCateView = view;
        addView(view);
        currentCateView.focused();
        currentCateView.show();
        changeTitle(view.title);
        if(!noChangePreview) {
            if (currentContView === view)
                currentContView = null;
            setCurrentContentData(currentCateView.getCurrentContent());
        }
    };

    var setCurrentContent = function (view, focus) {
        currentContView?currentContView.hide():0;
        currentContView = view;
        addRemovableQueue(currentContView);
        addView(currentContView);
        currentContView.blurred(true);
        currentContView.show();

        if(focus) {
            mode = MODE.CONTENTS;
            changeTitle(view.title);
            if(currentContView.focused() == kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS) historyBack();
        }

        /**
         * [dj.son] [WEBIIIHOME-3727] 렌더링으로 인해 백그라운드가 잠시 안보이는 이슈 수정
         * - 백그라운드 이미지 설정을 먼저 하도록 수정
         */

        /**
         * [sw.nam] [WEBIIIHOME-3727] 셋탑 부팅 후 편성표 카테고리 첫 진입 시 백그라운드 안보이는 이슈 ㅅ정
         * - 미리 이미지를 로딩해두고 visibility 조절하도록 수정
         */
        if (currentContView.menuId === MenuDataManager.MENU_ID.KIDS_CHANNEL) {
            // 키즈 편성표인 경우 bg_2d_kids 의 source 변경
            _$("#bg_2d_kids").css({visibility: "hidden"});
            _$("#bg_2d_3_kids_height").css({visibility: "inherit"});
        }
        else {
            _$("#bg_2d_kids").css({visibility: "inherit"});
            _$("#bg_2d_3_kids_height").css({visibility: "hidden"});
        }

        /**
         * [sjoh] 키즈의 경우, 홈샷을 출력하는 기준은 하위 항목을 카테고리로 가지거나, 하위 항목이 편성표인 경우에만 출력한다.
         */
        if (currentContView.type === View.TYPE.CATEGORY || currentContView.menuId === MenuDataManager.MENU_ID.KIDS_CHANNEL) {
            _$("#bg_kids").hide();

            //instance.setHomeShotShow();
            var categorys = currentCateView.getCategoryData();
            var targetMenu = null;

            for (var i = 0; i < categorys.length; i++) {
                if (currentContView.viewId === categorys[i].id) {
                    targetMenu = categorys[i];
                    break;
                }
            }
            instance.setHomeShot({menu: targetMenu});

            _$("#bg_kids_cloud_1").removeClass("moved");
            _$("#bg_kids_cloud_2").removeClass("moved");
        }
        else {
            _$("#bg_kids").show();

            //instance.setHomeShotHide();
            instance.setHomeShot();

            instance.setHideCharacter();
            _$("#bg_kids_cloud_1").addClass("moved");
            _$("#bg_kids_cloud_2").addClass("moved");
        }



        currentCateView.setRelationBtnPosition(currentContView.type === View.TYPE.CATEGORY, currentContView.viewId === MenuDataManager.MENU_ID.KIDS_PLAY_LIST);

    };

    var setCurrentContentData =function (viewData, callback) {
        log.printDbg("setCurrentContentData() "+viewData);
        LayerManager.stopLoading();
        if(ViewMap[viewData.id]) {
            LayerManager.stopLoading();
            setCurrentContent(ViewMap[viewData.id]);
            if(callback) callback(ViewMap[viewData.id]);
        } else {
            setCurrentContent(emptyView);
            LayerManager.startLoading();
            loadViewByData(viewData, function (view) {
                setCurrentContent(view);
                if (callback) callback(view);
                if (view.isLoaded()) LayerManager.stopLoading();
            });
        }
    };

    function addRemovableQueue(view) {
        if(view==emptyView || view==getCurrentView() || historyStack.indexOf(view)>=0 ||removableQueue.indexOf(view)>=0) return;
        if(removableQueue.length>=5) {
            var rmv = removableQueue[0];
            removableQueue.splice(0, 1);
            if(rmv!=view) {
                rmv.getView().remove();
                delete ViewMap[(rmv.viewId?rmv.viewId:rmv.menuId)];
            }
        } removableQueue.push(view);
    }

    var clearRemovableQueue = function() {
        if(removableQueue.length < 2) return;

        for(var i in removableQueue) {
            var rmv = removableQueue[i];
            rmv.destroy();
            delete ViewMap[rmv.viewId];
        }
        curLoadViewId = null;
    }; this.clearRemovableQueue = clearRemovableQueue;

    var getCurrentView = function() {
        if(mode==MODE.CATEGORY || mode==MODE.KIDS_LIST) return currentCateView;
        else if(mode==MODE.CONTENTS) return currentContView;
    };

    var setData = function(data, enterMenu) {
        log.printDbg("setData");
        LayerManager.startLoading({preventKey: true});
        if(ViewMap[data.id]) {
            if(ViewMap[data.id].type== View.TYPE.CATEGORY) setCurrentCategory(ViewMap[data.id], !!enterMenu);
            else if(ViewMap[data.id].type== View.TYPE.CONTENTS) setCurrentContent(ViewMap[data.id], true);
            if (ViewMap[data.id].isLoaded()) LayerManager.stopLoading();
        } else {
            loadViewByData(data, function (view) {
                if(view.type== View.TYPE.CATEGORY) setCurrentCategory(view, !!enterMenu);
                else if(view.type== View.TYPE.CONTENTS) setCurrentContent(view, true);
                if (ViewMap[data.id].isLoaded()) LayerManager.stopLoading();
            });
        }
    };

    function loadViewByData(data, callback) {
        if(curLoadViewId == data.id) return;
        curLoadViewId = data.id;
        contentsLoadAjax = DataConverter.getView(div, data, function(view) {
            callback(view);
        }, parent, instance);
    }

    function setContentsChangeTimeout(func, time) {
        // setCurrentContent(emptyView);
        // LayerManager.startLoading();
        clearContentsChangeTimeout();
        contentsChangeTimeout = setTimeout(func, time);
    }

    function clearContentsChangeTimeout() {
        if(contentsChangeTimeout) {
            clearTimeout(contentsChangeTimeout);
            contentsChangeTimeout = null;
        }
    }

    function clearContentsAjax() {
        if(contentsLoadAjax) {
            kidsSubHome.amocManager.ajaxStop(contentsLoadAjax);
            contentsLoadAjax = null;
            curLoadViewId = null;
        }
    }

    function contensChangeImmediately() {
        clearContentsChangeTimeout();
        setCurrentContentData(currentCateView.getCurrentContent(), changeFocus);
    }

    function getCurrentCategoryId() {
        return getCurrentView().viewId;
    }
    this.getCurrentCategoryId = getCurrentCategoryId;

    function getFocusCategoryInfo() {
        if (!currentCateView) return {};
        return currentCateView.getCurrentContent();
    }
    this.getFocusCategoryInfo = getFocusCategoryInfo;

    var pause = function () {
        getCurrentView().pause();
    }; this.pause = pause;

    var resume = function () {
        getCurrentView().resume();
    }; this.resume = resume;


    this.getParent = function () {
        return parent;
    };

    return {
        setParent: function (_parent) { parent = _parent; },
        setData: setData,
        setKidsCharacterShot : setKidsCharacterShot,
        onKeyAction: function (keyCode) {
            if(mode==MODE.CATEGORY) {
                if(cateKeyAction(keyCode)) return true;
            }else if(mode==MODE.CONTENTS){
                log.printDbg("onContentsKeyAction)");
                if (currentContView && currentContView.onKeyAction(keyCode)) return true;
            }

            if(keyCode==KEY_CODE.PLAY && homeShot.execute()) return true;
            return (keyCode == KEY_CODE.BACK || keyCode == KEY_CODE.LEFT) && historyBack();
        },
        resetting: function() {
            for(var id in ViewMap){
                if (ViewMap[id].hasOwnProperty("resetting")) ViewMap[id].resetting();
            }
        },
        changeBackground: changeBackground,
        changeCategoryFocus: changeCategoryFocus,
        clearRemovableQueue: clearRemovableQueue,
        setHomeShot:setHomeShot,
        getCurrentCategoryId:getCurrentCategoryId,
        getFocusCategoryInfo:getFocusCategoryInfo,
        pause: pause,
        resume: resume,
        getView: function() { return div; },
        destroy: function() {
            historyStack = [];
            removableQueue = [];
            curLoadViewId = null;

            for(var id in ViewMap) {
                if (ViewMap[id].div) {
                    ViewMap[id].div.remove();
                } else {
                    ViewMap[id].remove();
                }
                delete ViewMap[id];
            } ViewMap = {};
        }
    }
};
Object.defineProperty(kidsSubHome.ViewManager, "FOCUS_MODE", { value: { NO_FOCUS: 0, FOCUS:1, BLUR_FOCUS: 2 },writable:false });