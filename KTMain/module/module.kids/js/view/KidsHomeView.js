/**
 * Created by ksk91_000 on 2016-11-30.
 */
kidsSubHome.view.KidsHomeView = function(viewId) {
    View.call(this, viewId);
    var div = this.div;

    var id;
    var currentComp = 0;
    var compList = [];
    var kidsSubHomeData;
    var pageInfo = [{length:0, start:0, end:50}];

    this.create = function(data) {
        id = data.id;
        kidsSubHomeData = kidsSubHomeDataManager.getkidsSubHome(data.parent.id);
        (function createDiv() {
            div.attr("class", "kidsSubHomeView");
            div.append(_$("<div/>", {class:"indicator_area"}));
            div.append(_$("<div/>", {class:"contents_area"}));
            setComponents(div.find(".contents_area"), kidsSubHomeData);
        })();
    };

    function setComponents(div, data) {
        var root = _$("<div/>", {class:"components_area"});
        if(data.event) addComponent(root, new BigBanner(id + "banner_top"), data.event.list[id]);

        if(data.recommend) {
            var recomList = data.recommend.list[id];
            var tmp = [];
            for (var i = 0; i < recomList.length; i++) {
                if (!tmp[recomList[i].position]) tmp[recomList[i].position] = [];
                tmp[recomList[i].position].push(recomList[i]);
            }
            for(var i in tmp) {
                addComponent(root, new SubPosterList(id + "_recom_" + i), tmp[i]);
            }
        }

        if(data.event) addComponent(root, new MiniBanner(id + "banner_bottom"), data.event.list[id]);
        div.append(root);

        compList[currentComp].focused();
    }

    function addComponent(div, comp, data) {
        if(data) {
            if(comp.setData(data)) {
                div.append(comp.getView());
                compList.push(comp);
                addPageInfo(comp);
            }
        }
    }

    function addPageInfo(component) {
        var lastPageInfo = pageInfo[pageInfo.length-1];
        if(lastPageInfo.end - lastPageInfo.start < 1000 - component.getHeight()) {
            lastPageInfo.length++;
            lastPageInfo.end+=component.getHeight();
        } else {
            pageInfo[pageInfo.length] = {
                length: 1,
                start: lastPageInfo.end,
                end: lastPageInfo.end + component.getHeight()
            }
        }
    }

    function changeFocus(amount) {
        if(amount==0) return;
        div.find(".context_menu_indicator").text("");
        compList[currentComp].onBlured();
        currentComp=HTool.getIndex(currentComp, amount, compList.length);
        inPageIdx+=amount;
        syncPage();
        compList[currentComp].onFocused(amount);
    }

    function syncPage() {
        if(inPageIdx<0) {
            currentPage = HTool.getIndex(currentPage, -1, pageInfo.length);
            _$("#MyHomeMain #contents").css("-webkit-transform", "translateY(" + -pageInfo[currentPage].start + "px)");
            inPageIdx += pageInfo[currentPage].length;
        } else if(inPageIdx>pageInfo[currentPage].length-1) {
            inPageIdx -= pageInfo[currentPage].length;
            currentPage = HTool.getIndex(currentPage, 1, pageInfo.length);
            _$("#MyHomeMain #contents").css("-webkit-transform", "translateY(" + -pageInfo[currentPage].start + "px)");
        } else return;
        indicator.setPos(currentPage);
        syncPage();
    }

    function changePage(amount) {
        while(amount!=0) {
            if(amount>0) {
                currentComp=HTool.getIndex(currentComp, pageInfo[currentPage].length-inPageIdx, compList.length);
                inPageIdx = pageInfo[currentPage].length;
                amount--;
            }else if(amount<0) {
                currentComp=HTool.getIndex(currentComp, -1-inPageIdx, compList.length);
                inPageIdx = -1;
                amount++;
            } syncPage();
        }
    }

    this.onKeyAction = function(keyCode) {
    };

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.removeClass("blurred");
        // this.viewMgr.changeBackground(null, 0);
        return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.blurred =  function() {
        div.addClass("blurred");
    };
};
kidsSubHome.view.KidsHomeView.prototype = new View();
kidsSubHome.view.KidsHomeView.prototype.constructor = kidsSubHome.view.KidsHomeView;
kidsSubHome.view.KidsHomeView.prototype.type = View.TYPE.CONTENTS;