/**
 * Created by Yun on 2017-01-17.
 */


kidsSubHome.view.KidsSubHomeView = function (viewId, contentsData) {
    View.call(this, viewId);
    var div = this.div;

    var id;
    var curIdx = 0, inPageIdx = 0;
    var tmpCurIdx = 0;
    var tmpPosterIdx = 0;
    var tmpCompLen = 0;
    var curPage = 0;
    var compList = [];
    var pageInfo = [{length:0, start:0, end:30, compList:[]}];

    var indicator;
    //var homeShotIndex = 0;
    //var homeShotInterval;
    var kidsSubHomeData;
    var instance = this;
    var recentlyComp;

    var kidsMenuIconComp = null;
    var recentlyArrowArea = null;

    var cateData;
    var isLoadedChk = false;
    this.contextMenu;

    this.create = function (data) {
        isLoadedChk = false;
        id = data.id;
        cateData = data;

        /**
         * [dj.son] 키즈 메뉴 내 첫번째 KidsSubHome 메뉴에만 최근 시청 목록 표시하도록 수정
         */
        if (cateData.isFirstKidsSubHome) {

            kidsMenuIconComp = new KidsMenuIcon("KidsMenuIcon");
            //kidsMenuIconComp.setParent(instance);

            recentlyComp = new kidsRecentlyList("kidsRecentlyList");
            recentlyComp.setParent(instance);
        }

        kidsSubHomeData = SubHomeDataManager.getSubHome(data.parent.id);
        this.contextMenu = new ContextMenu_OnlyVOD();
        (function createDiv() {
            indicator = new Indicator(1);
            div.attr("class", "kidsSubHomeView");
            div.addClass("blurred");
            div.append(_$("<div/>", {class:"indicator_area"}));
            div.append(_$("<div/>", {class: "relationBtn_area"}));
            div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기")));
            div.append(_$("<div/>", {class: "contents_area"}));
            div.append(_$("<div/>", {class:"bottom_dim"}));

            if (kidsMenuIconComp) {
                recentlyArrowArea = _$("<div/>", {class:"recently_arrow_area", css: {position: "absolute", left: 1117, top: 974}});
                recentlyArrowArea.append(_$("<span/>", {text: "최근 시청 목록", css: {
                        position: "absolute", width: 181, height: 30, "font-size": 24, "letter-spacing": -1.2, "text-align": "center", color: "rgba(255, 255, 255, 0.7)"
                    }}));
                recentlyArrowArea.append(_$("<img/>", {src: window.modulePath + "resource/image/kids_btm_arrow.png", css: {position: "absolute", left: 75, top: 34}}));
                recentlyArrowArea.append(_$("<img/>", {src: window.modulePath + "resource/image/img_kidsmode_set_pop.png", css: {position: "absolute", left: 0, top: 60}}));

                recentlyArrowArea.hide();
                div.append(recentlyArrowArea);
            }

            if (kidsMenuIconComp) {
                kidsMenuIconComp.setData();
            }

            if (recentlyComp) {
                recentlyComp.setDataRefresh(contentsData);
            }

            setComponents(div.find(".contents_area"), kidsSubHomeData);

            div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());
            div.append(instance.contextMenu.getView());
        })();
    };

    function setComponents(_div, data) {
        var root = _$("<div/>", {class:"components_area"});
        if (data.event) addComponent(root, new BigBanner(id + "banner_top"), data.event.list[id]);

        if (kidsMenuIconComp && kidsMenuIconComp.getDataLength() > 0) {
            //kidsMenuIconComp.setParent(instance);
            compList.push(kidsMenuIconComp);
            root.append(kidsMenuIconComp.getView());
            addPageInfo(kidsMenuIconComp);
        }

        if (recentlyComp) {
            if (recentlyComp.getDataLength() > 0) {
                recentlyComp.setParent(instance);
                compList.push(recentlyComp);
                root.append(recentlyComp.getView());
                addPageInfo(recentlyComp);
            }
        }

        if (data.recommend && data.recommend.list[id]) {
            var recomList = data.recommend.list[id];
            var tmp = [];
            for (var idx = 0 ; idx < recomList.length; idx ++) {
                if (!tmp[recomList[idx].position]) tmp[recomList[idx].position] = [];
                tmp[recomList[idx].position].push(recomList[idx]);
            }
            for (var i in tmp) {
                addComponent(root, new SubPosterList(id + "_recom_" + i), tmp[i]);
            }
        }

        if (data.event) addComponent(root, new MiniBanner(id + "banner_bottom"), data.event.list[id]);
        _div.append(root);

        if (!div.hasClass("blurred")) compList[curIdx].focused();
    }

    function addComponent(div, comp, data) {
        if(data) {
            if(comp.setData(data)) {
                div.append(comp.getView());
                compList.push(comp);
                addPageInfo(comp);
                comp.parent = instance;
            } indicator.setSize(pageInfo.length);
        }
    }

    function addPageInfo(component) {
        var lastPageInfo = pageInfo[pageInfo.length-1];
        if(lastPageInfo.end - lastPageInfo.start < 970 - component.getHeight()) {
            lastPageInfo.length++;
            lastPageInfo.end+=component.getHeight();
            lastPageInfo.compList.push(component);
        } else {
            pageInfo[pageInfo.length] = {
                length: 1,
                start: lastPageInfo.end,
                end: lastPageInfo.end + component.getHeight(),
                compList: [component]
            }
        }
    }

    function changeFocus(amount, keyCode) {
        if(amount==0) return;
        div.find(".context_menu_indicator").text("");
        compList[curIdx].blurred();
        curIdx=HTool.getIndex(curIdx, amount, compList.length);
        inPageIdx+=amount;
        syncPage();
        // compList[curIdx].focused(keyCode);
        if (!compList[curIdx].focused(keyCode)) {
            changeFocus(amount);
        }

        if (recentlyArrowArea) {
            if (curPage === 0 && compList[1] === kidsMenuIconComp) {
                recentlyArrowArea.show();
            }
            else {
                recentlyArrowArea.hide();
            }
        }
    }

    function syncPage() {
        var componentsArea = div.find(".components_area");
        if(inPageIdx<0) {
            curPage = HTool.getIndex(curPage, -1, pageInfo.length);
            componentsArea.css("-webkit-transform", "translateY(" + (-pageInfo[curPage].start) + "px) translateZ(0)");
            inPageIdx += pageInfo[curPage].length;
        } else if(inPageIdx>pageInfo[curPage].length-1) {
            inPageIdx -= pageInfo[curPage].length;
            curPage = HTool.getIndex(curPage, 1, pageInfo.length);
            componentsArea.css("-webkit-transform", "translateY(" + (-pageInfo[curPage].start) + "px) translateZ(0)");
        } else return;
        div.find(".bottom_dim").css("display", (curPage > 0 ? "none" : "block"));
        indicator.setPos(curPage);
        div.find(".in_page_comp").removeClass("in_page_comp");
        for (var idx = 0 ; idx < pageInfo[curPage].length; idx ++) {
            if (compList[curIdx-inPageIdx+idx]) compList[curIdx-inPageIdx+idx].getView().addClass("in_page_comp");
        }
        processBannerScale();
        syncPage();
    }

    /**
     * Banner Scale 을 처리한다.
     * 페이지 전환 시 화면 하단에 다음 페이지 Banner 보이게 되어 Scale 을 줄임
     */
    function processBannerScale() {
        log.printDbg("called processBannerScale()");
        if (!compList || compList.length <= 0) {
            log.printDbg("processBannerScale() - return, invalid compList");
            return;
        }
        var length = compList.length;
        for (var i = 0; i < length; i++) {
            var compView = compList[i].getView();
            if (compView.hasClass("in_page_comp")) {
                compView.css("-webkit-transform", "scaleY(1)");
            } else {
                if (i === 1 && compList[0] instanceof BigBanner) {
                    // BigBanner 다음에 나오는 Banner 는 화면 상 보여줘야 되므로 Scale 조정 안함.
                    compView.css("-webkit-transform", "scaleY(1)");
                } else {
                    compView.css("-webkit-transform", "scaleY(0.8)");
                }
            }
        }
    }

    function changePage(amount) {
        while(amount!=0) {
            if(amount>0) {
                curIdx=HTool.getIndex(curIdx, pageInfo[curPage].length-inPageIdx, compList.length);
                inPageIdx = pageInfo[curPage].length;
                amount--;
            }else if(amount<0) {
                curIdx=HTool.getIndex(curIdx, -1-inPageIdx, compList.length);
                inPageIdx = -1;
                amount++;
            }
            syncPage();
        }
    }

    function getPagesFirstComponetIdx(page){
        var idx = 0;
        var j = (page < pageInfo.length)? page : pageInfo.length;
        for(var i=0; i<j; i++){
            idx += pageInfo[i].compList.length;
        }

        return idx;
    }


    this.onKeyAction = function(keyCode) {
        if(compList[curIdx].onKeyAction(keyCode)) return true;
        else
            switch (keyCode) {
                case KEY_CODE.UP: changeFocus(-1, keyCode); return true;
                case KEY_CODE.DOWN: changeFocus(1, keyCode); return true;
                case KEY_CODE.RED:
                    if (inPageIdx === 0) {
                        if (!compList[curIdx].isFirstFocused()) {
                            compList[curIdx].blurred();
                            compList[curIdx].focused(keyCode);
                            return true;
                        } else changeFocus(-1, keyCode);
                    } while(inPageIdx !== 0) {
                        changeFocus(-1, keyCode);
                    }
                    return true;
                case KEY_CODE.BLUE:
                    if (inPageIdx === pageInfo[curPage].length-1) {
                        if (!compList[curIdx].isLastFocused()) {
                            compList[curIdx].blurred();
                            compList[curIdx].focused(keyCode);
                            return true;
                        } else changeFocus(1, keyCode);
                    } while(inPageIdx !== pageInfo[curPage].length-1) {
                        changeFocus(1, keyCode);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    return false;
                case KEY_CODE.RIGHT:
                    changeFocus(1, keyCode);
                    return true;
                case KEY_CODE.ENTER:
                    return true;
                case KEY_CODE.CONTEXT:
                    openKidsModeSettingPopup();
                    return true;
            }
    };


    this.onKeyForIndicator = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                changePage(-1);
                return true;
            case KEY_CODE.DOWN:
                changePage(1);
                return true;
            case KEY_CODE.ENTER:
            case KEY_CODE.RIGHT :
                curIdx = getPagesFirstComponetIdx(curPage);
                inPageIdx = 0;
                indicator.blurred();
                compList[curIdx].focused();
                return true;
            case KEY_CODE.LEFT:
                this.viewMgr.historyBack();
                return true;
            case KEY_CODE.CONTEXT:
                openKidsModeSettingPopup();
                return true;
            case KEY_CODE.RED:
                indicator.blurred();
                if (inPageIdx === 0) {
                    compList[curIdx].focused(keyCode);
                }
                while (inPageIdx !== 0) changeFocus(-1, keyCode);
                return true;
            case KEY_CODE.BLUE:
                indicator.blurred();
                if (inPageIdx === pageInfo[curPage].length - 1) {
                    compList[curIdx].focused(keyCode);
                }
                while (inPageIdx !== pageInfo[curPage].length - 1) changeFocus(1, keyCode);
                return true;
        }
    };
    
    /**
     * KidsMode Setting (On / Off) Popup Open
     */
    function openKidsModeSettingPopup(){
        LayerManager.activateLayer({
            obj: {
                id: (KIDS_MODE_MANAGER.isKidsMode() ? "KidsModeOffPopup" : "KidsModeSettingPopup"),
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    complete:function() {
                        // settingInfoRefresh();
                    }
                }
            },
            moduleId: "module.kids",
            new: true,
            visible: true
        });
    }


    this.getView = function() {
        return div;
    };

    this.resetting = function () {
        if (recentlyComp) {
            if (RecentlyListManager.getIsResetting()) {
                if (RecentlyListManager.getIsKidsRecentlyReLoad()) {
                    RecentlyListManager.setIsKidsRecentlyReLoad(false);
                    RecentlyListManager.setIsResetting(false);
                    RecentlyListManager.update(function(data){
                        recentlyComp.setDataRefresh(data);
                        compList = [];
                        pageInfo = [{length:0, start:0, end:50, compList:[]}];
                        curIdx = 0;
                        curPage = 0;
                        inPageIdx = 0;
                        div.find(".components_area").remove();
                        setComponents(div.find(".contents_area"), kidsSubHomeData);
                        if (data.length == 0) {
                            while (curIdx != tmpCurIdx) changeFocus(-1);
                            void div[0].offsetWidth;
                            if (!div.hasClass("blurred") && !compList[curIdx].getIsRecentlyList()) compList[curIdx].setFocusIndex(tmpPosterIdx);
                            else if (compList[curIdx].getIsRecentlyList()) compList[curIdx].setFocusIndex(0);
                        } else {
                            if (tmpCurIdx != 0 && compList.length != tmpCompLen) tmpCurIdx ++;
                            while (curIdx != tmpCurIdx) changeFocus(1);
                            void div[0].offsetWidth;
                            if (!div.hasClass("blurred") && !compList[curIdx].getIsRecentlyList()) compList[curIdx].setFocusIndex(tmpPosterIdx);
                            else if (compList[curIdx].getIsRecentlyList()) compList[curIdx].setFocusIndex(0);
                        }
                        div.find(".components_area").css("transition", "-webkit-transform .5s");
                        if (!div.hasClass("blurred")) {
                            void div.find(".indicator_area").hide()[0].offsetWidth;
                            indicator.setSize(pageInfo.length, curPage);
                            div.find(".indicator_area").show();
                        }
                    }, RecentlyListManager.getKidsSubHomeId());
                } else {
                    RecentlyListManager.setIsResetting(false);
                    compList = [];
                    pageInfo = [{length:0, start:0, end:50, compList:[]}];
                    curIdx = 0;
                    curPage = 0;
                    inPageIdx = 0;
                    div.find(".components_area").remove();
                    setComponents(div.find(".contents_area"), kidsSubHomeData);
                    void div[0].offsetWidth;
                    div.find(".components_area").css("transition", "-webkit-transform .5s");
                    indicator.setSize(pageInfo.length, curPage);
                }
            }
        }
    };

    //function setHomeShot(index) {
    //    if(!kidsSubHomeData.homeShot) return;
    //    var homeShot = kidsSubHomeData.homeShot.list[id];
    //    instance.viewMgr.setHomeShot(homeShot[index].itemType, homeShot[index].itemId, homeShot[index].imgUrl, homeShot[index].locator, homeShot[index].locator2, null, homeShot[index].parameter);
    //}

    this.show = function () {
        View.prototype.show.apply(this, arguments);
        if(kidsSubHomeData.homeShot) instance.viewMgr.setHideCharacter();
        div.addClass("blurred");
        this.resetting();

        if (compList.length > 0) {
            compList[curIdx].blurred();
        }

        if (recentlyArrowArea) {
            if (curPage === 0 && compList[1] === kidsMenuIconComp) {
                recentlyArrowArea.show();
            }
            else {
                recentlyArrowArea.hide();
            }
        }

        //if (kidsSubHomeData.homeShot && kidsSubHomeData.homeShot.list[id] && kidsSubHomeData.homeShot.list[id].length > 0) {
        //    setHomeShot(homeShotIndex = HTool.getRandomRangeValue(0, kidsSubHomeData.homeShot.list[id].length));
        //
        //    if (!homeShotInterval) {
        //        homeShotInterval = setInterval(function () {
        //            if (!kidsSubHomeData.homeShot) return;
        //            setHomeShot(homeShotIndex = HTool.getIndex(homeShotIndex, 1, kidsSubHomeData.homeShot.list[id].length));
        //        }, 5000);
        //    }
        //} else instance.viewMgr.setHomeShot();
        div.show();
        isLoadedChk = true;
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
        //if(homeShotInterval) clearInterval(homeShotInterval);
        //homeShotInterval = null;
    };

    this.focused = function() {
        if (compList.length > 0) {
            div.find(".relationBtn_area").removeClass("blurred");
            div.find(".context_menu_indicator").text("");
            div.find(".components_area").css("transition", "-webkit-transform .5s");
            div.find(".bottom_dim").addClass("show");
            div.hide();
            div.removeClass("blurred");
            div.find(".context_menu_indicator").text("");
            indicator.blurred();
            compList[curIdx].focused();
            // compList[1].titleHide();
            // compList[2].titleHide();
            void div[0].offsetWidth;
            div.show();
            return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
        } else return kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS;
    };

    this.blurred =  function() {
        if (compList.length > 0) {
            div.addClass("blurred");
            div.find(".relationBtn_area").addClass("blurred");
            div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
            indicator.blurred();
            div.hide();
            compList[curIdx].blurred();
            while (curIdx != 0) changeFocus(1);
            void div[0].offsetWidth;
            div.show();
            div.find(".components_area").css("transition", "-webkit-transform 0s");
            div.find(".components_area").css("-webkit-transform", "");
            div.find(".bottom_dim").removeClass("show");
        } else {
            div.addClass("blurred");
        }
    };

    this.isLoaded = function () {
        return isLoadedChk;
    };

    this.pause = function () {
        tmpCurIdx = curIdx;
        tmpCompLen = compList.length;
        tmpPosterIdx = compList[curIdx].getFocusIndex();
        for (var idx = 0 ; idx < compList.length ; idx ++) {
            if (compList[idx].hasOwnProperty("pause")) compList[idx].pause();
        }
    };

    this.resume = function () {
        this.resetting();
        for (var idx = 0 ; idx < compList.length ; idx ++) {
            if (compList[idx].hasOwnProperty("resume")) compList[idx].resume();
        }
    };

};

kidsSubHome.view.KidsSubHomeView.prototype = new View();
kidsSubHome.view.KidsSubHomeView.prototype.constructor = kidsSubHome.view.ParentsWorldView;
kidsSubHome.view.KidsSubHomeView.prototype.type = View.TYPE.CONTENTS;