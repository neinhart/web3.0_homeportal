/**
 * Created by ksk91_000 on 2016-07-01.
 */
kidsSubHome.view = kidsSubHome.view||{};
var View = function(viewId) {
    this.div = _$("<div/>");
    this.viewId = viewId;
    this.div.attr({id: this.viewId});
    this.title = "";
    this.viewMgr;
};

Object.defineProperty(View, "TYPE", {
    value:{EMPTY:-1, CATEGORY:0, CONTENTS:1, KIDS_LIST:2},
    writable:!1
});

View.prototype = {
    type: View.TYPE.EMPTY,
    create: function () {
    },
    destroy: function () {
        this.div.remove();
    },
    show: function () {
        this.div.show();
    },
    hide: function () {
        this.div.hide();
    },
    focused: function() {
        this.div.removeClass("blurred");
        return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
    },
    blurred: function() {
        this.div.addClass("blurred");
    },
    pause: function() {
    },
    resume: function () {
    },
    onKeyAction: function(key_code) {
        if(key_code == KEY_CODE.ENTER) return true;
        return false;
    },
    getView: function() {
        return this.div;
    },
    getTitle: function() {
        return this.title;
    },
    isLoaded: function () {
        return true;
    },
    resetting: function () {

    },
    loadAddedContents: function (startIdx, amount, callback, showLoading) {
        if(callback) callback();
    }
};