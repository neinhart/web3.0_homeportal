/**
 * Created by ksk91_000 on 2016-06-30.
 */
kidsSubHome.view.categoryView = function(viewId) {
    View.call(this, viewId);

    var cateInfo;
    var categories;
    var isFocused = false;

    var focusIndex = 0;
    var pageIndex = 0;
    var NUMBER_OF_ITEM_PER_PAGE = 9;

    var pages = [];

    var div = this.div;
    var _this = this;
    var language;

    var focusedElement;

    var adultAuthBox;

    this.create = function(data) {
        cateInfo = data;
        categories = filteringByCateType(data.children);
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");

        (function init() {
            focusIndex = 0;
            pageIndex = 0;
            var html = "";
            // 카테고리 정보를 가지고 페이지 단위로 정보를 미리 생성
            makePages();

            // to make category area
            html += "<div id='menuArea' class='menuArea'>" +
                        makeCategoryItems(pages[pageIndex]) +
                    "</div>";

            // to make focus area
            html += "<div id='focusArea'>" +
                        makeFocusArea(pages[pageIndex][focusIndex]) +
                    "</div>";

            // to make page move guide
            if(categories.length > 9) {
                html += "<div class='category_bottom_arw'/>" +
                        "<div class='lr_arw_area'>" +
                            "<img src='" + modulePath + "resource/image/icon_pageup.png'>" +
                            "<img src='" + modulePath + "resource/image/icon_pagedown.png'>" +
                            "<span id='pageIndex'>00 / 00</span>" +
                        "</div>";
            }
            html+='<div class="leftArrow"/>';

            div.attr({class:"categoryArea"});
            div.html(html);
            div.append(_$("<div/>", {class:"previewArea"}));
            div.append(_$("<div/>", {class:"noticeArea"}));

            // to make relation button
            div.append(_$("<div/>", {class: "relationBtn_area"}));
            div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기")));

            makePreviewArea.call(_this, data);
            updateIndex();
            setFocusArrowVisibility(!!categories[0].w3ListImgUrl);      // 첫번쨰 항목이 텍스트 인 경우에만 화살표 출력
        })();
    };

    function makePages() {
        var page = [], size = 0;
        for(var i = 0; i < categories.length; i++) {
            // [sjoh] 사이즈가 9보다 큰경우가 되면 속성값을 변경해야 하므로 원본 데이터를 prototype 으로 가지는 새 오브젝트를
            // 생성하여 관리하도록 한다.
            var item = Object.create(categories[i]);
            if(size + Number.parseInt(item.w3MenuImgType || "01") > 9) {
                item.w3ListImgUrl = "";
                item.w3MenuImgType = "01";
            }
            page.push(item);
            size += Number.parseInt(item.w3MenuImgType || "01");
            if(size === 9) {
                pages.push(page);
                page = [];
                size = 0;
            }
        }
        if(page.length > 0)
            pages.push(page);
    }

    function makeFocusArea(data) {
        // [sjoh] 부팅 후 첫 진입시 첫번째 항목이 이미지 타입인경우 이미지를 비동기로 다운로드 하게 되므로
        // 높이정보를 알 수 없기 때문에 초기 포커스 높이를 설정한다.
        var itemType = data.menuImgUrl ? 'img' : 'text';
        var heightOffset = 22;
        var height = ((Number.parseInt(data.w3MenuImgType) || 1) * 94 - heightOffset) + "px";
        return  "<div class='textArea'>"+
                    "<div id='focus_border' style='height:" + height + "'>" +
                    "</div>" +
                    "<div class='rightArrow'>" +
                    "</div>" +
                "</div>";
    }

    function makeCategoryItems(page) {
        var html = "";

        for(var i = 0; i < page.length; i++) {
            var item = page[i];
            var itemType = item.w3ListImgUrl ? 'img' : 'text';
            if(itemType === 'text') {
                html += "<div class='text_item'>" +
                    "<span>" + getTitle(i) + "</span>" +
                    "</div>";
            } else {
                html += "<div class='image_item size" + (item.w3MenuImgType || "01") + "'>" +
                            "<img src='" + item.w3ListImgUrl + "'>" +
                        "</div>";
            }
        }

        return html;
    }

    function makePreviewArea(data) {
        var area = div.find(".previewArea");
        if(data.catType=="Adult"||data.catType=="Motel"){
            area.append(_$("<div/>", {class:"adult_auth_area"}));
            setAdultAuthArea.call(this, makePreview);
        } else {
            makePreview(area);
        }

        function makePreview(area) {
            for(var i = 0, size = 0; i < categories.length && i < 9 && size < 9; i++) {
                area.append(getCategoryPreview(categories[i]));
                size += Number.parseInt(pages[pageIndex][i].w3MenuImgType) || 1;
            }
        }
    }

    function getTitle(index) {
        var englishName = "", name = "";
        if(index instanceof window.parent.VItem) {
            englishName = index.englishItemName || index.name;
            name = index.name;
        } else {
            englishName = pages[pageIndex][index].englishItemName || pages[pageIndex][index].name;
            name = pages[pageIndex][index].name;
        }
        return (language === "eng") ? englishName : name;
    }

    function setAdultAuthArea(callback) {
        var that = this;
        adultAuthBox = new AdultAuthBox();
        adultAuthBox.create(function () {
            div.find(".adult_auth_area").detach();
            adultAuthBox = null;
            that.focused();
            callback();
            that.viewMgr.historyBack();
        });
        adultAuthBox.parent = this;
        div.find(".adult_auth_area").append(adultAuthBox.getView());
    }

    function getCategoryPreview(data) {
        var item = _$("<span/>", {class:"preview_item"});
        item.addClass("size" + (data.w3MenuImgType || "01"));
        var itemType = data.w3ListImgUrl ? 'img' : 'text';
        if(itemType === 'text')
            item.text(getTitle(data));
        else
            item.html("<img src='" + data.w3ListImgUrl + "'>");
        return item;
    }

    function filteringByCateType(dataList) {
        var list = [];
        for(var i=0; i<dataList.length; i++) {
            if(isUsedCatType(dataList[i].catType)) list.push(dataList[i]);
            if (dataList[i].catType == "KidsSub") {
                RecentlyListManager.setKidsSubHomeId(dataList[i].parent.id);
            }
        }
        return list;

        function isUsedCatType(catType) {
            return true;
        }
    }

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");
        setNoticeTextAnimation();
    };

    this.setPage = function (newPage, forced) {
        if(!forced && pageIndex === newPage)
            return;

        pageIndex = newPage;

        var menuArea = this.div.find('#menuArea');
        // to add new category items
        menuArea.html(makeCategoryItems(pages[pageIndex]))
    };

    this.setFocus = function(newFocus) {
        if(this.div.css("display") === "none")
            return;

        focusIndex = newFocus;

        UTIL.getNewCategoryIndex(pages, pageIndex, focusIndex, function (newFocusIdx, newPageIdx) {
            focusIndex = newFocusIdx;
            pageIndex = newPageIdx;
            _this.setPage(pageIndex, true);
        });

        // 포커스 아이템 변경
        var itemType = pages[pageIndex][focusIndex].w3ListImgUrl ? 'img' : 'text';
        if (focusedElement) {
            focusedElement.removeClass('focus');
            clearTextAnimation(focusedElement);
        }
        focusedElement = this.div.find('#menuArea div:eq('+focusIndex+')');
        focusedElement.addClass('focus');
        if (itemType === 'text') {
            setTextAnimation(focusedElement);
        }

        var focusArea = this.div.find('#focusArea');

        // [si.mun] DOM 에 categoryView가 append 되기 전엔
        // focusedElement.position().top 의 값은 0이 반환된다.
        // 때문에, 아래와 같이 top position 을 계산하는 로직을 삽입
        var top = 0;
        for(var i = 0; i < focusIndex; i++)
            top += parseInt(pages[pageIndex][i].w3MenuImgType || 1) * 94;

        focusArea.css("-webkit-transform", "translateY(" + top  + "px)");

        var heightOffset = 12;
        setFocusArrowVisibility(itemType === 'img');
        focusArea.find("#focus_border").css("height", (focusedElement.height() - heightOffset) + "px");
        setFocusImpl.apply(this);

        updateIndex(this);
    };

    function setFocusArrowVisibility(isVisible) {
        var arrow = div.find('#focusArea').find(".rightArrow")[0];
        arrow.hidden = isVisible;
    }

    this.changeFocus = function(amount) {
        if (amount === 0)
            return;

        if (categories.length <= 1)
            return;

        focusIndex += amount;

        if(focusIndex < 0) {
            moveToPreviousPage();
            focusIndex = pages[pageIndex].length - 1;
        } else if(focusIndex >= pages[pageIndex].length) {
            moveToNextPage();
            focusIndex = 0;
        }
        this.setFocus(focusIndex);
    };

    function moveToNextPage() {
        var newPage = pageIndex;
        if(++newPage >= pages.length)
            newPage = 0;
        _this.setPage(newPage);
    }

    function moveToPreviousPage() {
        var newPage = pageIndex;
        if(--newPage < 0)
            newPage = pages.length - 1;
        _this.setPage(newPage);
    }

    function updateIndex() {
        var index = '00' + (pageIndex + 1);
        var pageCount = '00' + (pages.length);
        div.find('#pageIndex').text(index.slice(-2) + ' / ' + pageCount.slice(-2));
    }

    function setFocusImpl() {
        var item = pages[pageIndex][focusIndex];
        if (!div.hasClass("blurred")) {
            if (item.notice){
                div.find(".noticeArea").html("<span>" + item.notice + "</span>");
                _this.div.find(".relationBtn_area").hide();
            }else{
                div.find(".noticeArea").empty();
                _this.div.find(".relationBtn_area").show();
            }
        }

        //if(isFocused &&
        //    item.catType !== "KidsSub" &&
        //    item.id != MenuDataManager.MENU_ID.KIDS_PLAY_LIST) {
        //    this.viewMgr.setHomeShot(item.hsTargetType, item.hsTargetId, item.w3HsImgUrl, item.hsLocator, item.hsLocator2, item.hsKTCasLocator);
        //}

        setNoticeTextAnimation();
    }

    function clearNoticeTextAnimation() {
        UTIL.clearAnimation(div.find(".noticeArea"));
        div.find(".noticeArea.textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
    }

    function setNoticeTextAnimation() {
        if (UTIL.getTextLength(categories[focusIndex].notice, "RixHead L", 30, -1.5) > 1770) {
            var d = div.find(".noticeArea");
            d.addClass("textAnimating");
            UTIL.startTextAnimation({ targetBox: d });
        }
    }

    function getCurrentContent() {
        return pages[pageIndex][focusIndex];
    }

    function setTextAnimation(element) {
        if (!element) {
            return;
        }
        if(UTIL.getTextLength(element.find("span").text(), "RixHead B", 42, -2.1) > 389) {
            element.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: element
            });
        }
    }

    function clearTextAnimation(element) {
        if (!element) {
            return;
        }
        UTIL.clearAnimation(element.find("span"));
        element.removeClass("textAnimating");
        void element[0].offsetWidth;
    }

    this.onKeyAction = function(keyCode) {
        if(adultAuthBox) return adultAuthBox.onKeyAction(keyCode);
        else switch(keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return false;

            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return false;

            case KEY_CODE.RED :
                if(focusIndex === 0) {
                    moveToPreviousPage();
                }
                this.setFocus(0);
                return false;

            case KEY_CODE.BLUE :
                if(focusIndex === pages[pageIndex].length - 1) {
                    moveToNextPage();
                }
                this.setFocus(pages[pageIndex].length - 1);
                return false;

            case KEY_CODE.CONTEXT:
                log.printDbg("focusIndex :"+focusIndex);
                log.printDbg("pageIndex :"+pageIndex);
                /// 2018.03.07 sw.nam
                // 전체 편성표에서 연관 메뉴 키 입력 시 키즈모드 ON/OFF 팝업이 뜨지 않는 이슈가 있었는데,
                // 분석 결과 focusIndex 가 페이지가 넘어가게 되면 0 으로 다시 초기화 되어
                // 전체 편성표 category 에 위치해 있더라도 categories[focusIndex] 가 1페이지에 있는 category 를 참조하면서
                // 아래 공지 확인 if문에 걸렸음.. 원래 있던 이슈였는데 여태 확인이 안됐었던것 같음
                // 그래서 페이지 index 를 곱해서 현재 포커스 index 를 구하는 방식으로 수정.. 현재 키즈 1-depth 카테고리 1페이지에 최대로 나타나는 갯수는 9개. GUI 수정되면 같이 수정해야 함.
                var currentFocusedIndex = focusIndex + (pageIndex* NUMBER_OF_ITEM_PER_PAGE);
                if(categories[currentFocusedIndex].notice !== null && categories[currentFocusedIndex].notice != "") return true;
                LayerManager.activateLayer({
                    obj: {
                        id: (KIDS_MODE_MANAGER.isKidsMode() ? "KidsModeOffPopup" : "KidsModeSettingPopup"),
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        params: {
                            complete:function() {
                                settingInfoRefresh();
                            }
                        }
                    },
                    moduleId: "module.kids",
                new: true,
                visible: true
                });
                return true;
        }
    };

    this.getCateInfo = function (key) {
        if(key === null)
            return cateInfo;
        else
            return cateInfo[key];
    };

    this.unfocused = function () {
        this.setFocus(focusIndex);
        div.addClass("unFocused");
        div.find(".relationBtn_area").hide();
        clearTextAnimation(focusedElement);
    };

    this.focused = function() {
        isFocused = true;
        this.div.find(".relationBtn_area").removeClass("blurred");
        this.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        //this.viewMgr.setHomeShot(categories[focusIndex].hsTargetType, categories[focusIndex].hsTargetId, categories[focusIndex].w3HsImgUrl, categories[focusIndex].hsLocator, categories[focusIndex].hsLocator2, categories[focusIndex].hsKTCasLocator);
        setNoticeTextAnimation();
        div.find(".relationBtn_area").show();

        if(adultAuthBox) {
            div.addClass("focused");
            return adultAuthBox.focused();
        } else {
            this.setFocus(focusIndex);
            div.removeClass("blurred unFocused focused");
            return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
        }
    };

    this.blurred = function() {
        isFocused = false;
        this.setFocus(0);
        this.div.find(".relationBtn_area").addClass("blurred");
        this.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        View.prototype.blurred.apply(this, arguments);
        clearNoticeTextAnimation();
        clearTextAnimation(focusedElement);
    };

    this.hide = function () {
        this.setFocus(focusIndex);
        View.prototype.hide.apply(this, arguments);
        clearNoticeTextAnimation();
        clearTextAnimation(focusedElement);
    };

    this.pause = function () {
        this.setFocus(focusIndex);
        clearNoticeTextAnimation();
        clearTextAnimation(focusedElement);
    };

    this.getCurrentContent = getCurrentContent;

    this.setRelationBtnPosition = function (isLeft, isKidsPlaylist) {
        if(isLeft) {
            _$('.relationBtn_area').removeClass('playlist');
            _$('.relationBtn_area').addClass('left');
        }
        else {
            if (isKidsPlaylist) {
                _$('.relationBtn_area').addClass('playlist');
            }
            else {
                _$('.relationBtn_area').removeClass('playlist');
            }
            _$('.relationBtn_area').removeClass('left');
        }
    }

    this.getCategoryData = function () {
        return categories;
    }
};

kidsSubHome.view.categoryView.prototype = new View();
kidsSubHome.view.categoryView.prototype.constructor = kidsSubHome.view.categoryView;
kidsSubHome.view.categoryView.prototype.type = View.TYPE.CATEGORY;