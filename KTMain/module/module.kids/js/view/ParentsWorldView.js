/**
 * Created by Yun on 2017-01-12.
 */

kidsSubHome.view.ParentsWorldView = function (viewId) {
    View.call(this, viewId);
    var div = this.div;
    var _this = this;
    var focusIdx = 0;
    var btnTitleArr = [{title:"키즈모드 설정", enTitle:"Kids Mode Setting", setOption:"OFF"}, {title:"시청제한 설정", enTitle:"Time Restrictions", setOption:"OFF"}, {title:"캐릭터 설정", enTitle:"Character Setting", setOption:""}];
    var pWorldData;
    var posterComp = [];
    var isBottomFocus = false;
    var language;

    this.create = function (data) {
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language == "kor");

        pWorldData = SubHomeDataManager.getSubHome(data.parent.id, viewId);
        div.attr("class", "parentsWorldView");
        div.addClass("blurred");
        div.append(_$("<div/>", {class: "contents_area"}));
        div.find(".contents_area").append(_$("<div/>", {id: "parents_watch_manager"}));
        div.find("#parents_watch_manager").append(_$("<div/>", {id: "parents_watch_manager_title_div"}));
        div.find("#parents_watch_manager_title_div").append(_$("<div/>", {id: "parents_watch_manager_title_bar", class:"parents_world_bar_style"}));
        div.find("#parents_watch_manager_title_div").append("<span id='parents_watch_manager_title_txt' class='parents_world_text_style'>시청관리</span>");
        div.find("#parents_watch_manager").append(_$("<div/>", {id: "parents_watch_manager_btn_area"}));
        for (var idx = 0 ; idx < 3; idx ++) {
            div.find("#parents_watch_manager_btn_area").append("<div class='parents_watch_manager_btn_div'>" +
                    "<div class='shadow' style ='position: absolute; left: -18px; top: -23px; width: 405px; height: 213px; '>" +
                        "<img class= 'shadow_l' style = 'position: relative; width: 41px; height: 213px;'>"+
                        "<img class= 'shadow_m' style = 'position: relative; width: 323px; height: 213px;'>"+
                        "<img class= 'shadow_r' style = 'position: relative; width: 41px; height: 213px;'>"+
                    "</div>" +
                    "<div class='parents_watch_manager_btn_bg'></div>" +
                    "<div class='parents_watch_manager_btn_innerBg'></div>" +
                    "<span class='parents_watch_manager_btn_title" + (language == "eng" ? " eng" : "") + "'>" + (language == "eng" ? btnTitleArr[idx].enTitle : btnTitleArr[idx].title) + "</span>" +
                    "<div class= 'parents_watch_manager_btn_subTitle'>" +
                      /*  "<img class= 'image' src= window.modulePath+'resource/image/kids_rbox_set.png'>"+*/
                        "<span class= 'text'>btnTitleArr[idx].setOption</span>" +
                    "</div>"+
                "</div>");
            if (idx == 2) {
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").eq(idx).append(_$("<img/>", {class:"parents_watch_manager_btn_charImg"}));
            }
        }
        div.find(".parents_watch_manager_btn_div .shadow_l").attr({src: window.modulePath + "resource/image/kids_set_box_sha_l.png"});
        div.find(".parents_watch_manager_btn_div .shadow_m").attr({src: window.modulePath + "resource/image/kids_set_box_sha_m.png"});
        div.find(".parents_watch_manager_btn_div .shadow_r").attr({src: window.modulePath + "resource/image/kids_set_box_sha_r.png"});
        /*div.find(".parents_watch_manager_btn_subTitle .image").attr({src: window.modulePath + "resource/image/kids_rbox_set.png"});*/


        //div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").append(_$("<img/>", {class:"parents_btn_sdw", src: window.modulePath + "resource/image/kids_btn_set_sdw.png"}));
        if (btnTitleArr[2].setOption == "OFF") {
            // 캐릭터 설정이 OFF 인경우 버튼 영역 활성화
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_title").css({top: "36px", width:"", height:"38px", "line-height":""});
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle").css({ visibility: "inherit"});
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle .text").text("OFF");

        } else {
            // 캐릭터 설정이 ON 인 경우 qjxms duddur ql ghkftjdghk
            if (language == "eng") {
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_title").css({top:"32px", width:"200px", height:"82px", "line-height": "41px"});
            } else {
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_title").css("top", "54px");
            }
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle").css({ visibility: "hidden"});
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle .text").text("");
        }
        div.find("#parents_watch_manager").append("<span id='parents_watch_manager_info'>" +
                "키즈모드 ON 설정 시 olleh tv를 켰을 때 키즈 메뉴로 시작하게 되며, 시청관리,<br>캐릭터 설정이 가능하고 어린이채널로만 구성된 Live 채널을 시청하게 됩니다" +
            "</span>");
        div.find(".contents_area").append(_$("<div/>", {id: "parent_education_guide"}));
        div.find("#parent_education_guide").append(_$("<div/>", {id: "parents_education_guide_title_div"}));
        // setData(testData);
        if (pWorldData.recommend.length > 0) {

            var list = _$("<ul/>", {id: "parents_education_guide_contentsList"});
            // div.find("#parents_education_guide_title_div").append(_$("<div/>", {id: "parents_education_guide_title_bar", class:"parents_world_bar_style"}));
            // div.find("#parents_education_guide_title_div").append("<span id='parents_education_guide_title_txt' class='parents_world_text_style'>자녀 교육을 위한 지침서</span>");
            var recomList = pWorldData.recommend;
            var tmp = [];
            for (var idx = 0 ; idx < recomList.length; idx ++) {
                if (!tmp[0]) tmp[0] = [];
                tmp[0].push(recomList[idx]);
            }
            for (var i in tmp) {
                addComponent(list, new SubPosterList(viewId + "_recom_" + i), tmp[i]);
            }
            div.find("#parent_education_guide").append(list);
            // div.find("#parent_education_guide .componentTitle").css("display", "none");
        }

        if (KIDS_MODE_MANAGER.isKidsMode()) {
            div.find(".parents_watch_manager_btn_div:eq(1)").css("opacity", "1.0");
            div.find(".parents_watch_manager_btn_div:eq(2)").css("opacity", "1.0");
        } else {
            div.find(".parents_watch_manager_btn_div:eq(1)").css("opacity", "0.3");
            div.find(".parents_watch_manager_btn_div:eq(2)").css("opacity", "0.3");
        }
    };
    function addComponent(div, comp, data) {
        if(data) {
            if(comp.setData(data)) {
                div.append(comp.getView());
                posterComp.push(comp);
                comp.parent = _this;
                comp.isParent = true;
            }
        }
    }

    // function setData(dataList) {
    //     var list = _$("<ul/>", {id: "parents_education_guide_contentsList"});
    //     div.find("#parent_education_guide").append(list);
    //     for (var idx = 0 ; idx < 5; idx ++) {
    //         list.append(PosterFactory.getVerticalPoster("parents_education_guide_contentsList_" + idx, {
    //             imgUrl: dataList[idx].imgUrl,
    //             contsName:dataList[idx].itemName,
    //             wonYn: dataList[idx].wonYn,
    //             prInfo: dataList[idx].prInfo
    //         }));
    //     }
    // }

    function changeFocus(amount) {
        log.printDbg("~~changeFocus~");
    }

    function changePage(amount) {
    }

    function setFocus(newFocus) {
        log.printDbg("~~setFocus~");
    }

    function setPage(newPage){
    }

    this.onKeyAction = function (keyCode) {
        if (isBottomFocus && posterComp[0].onKeyAction(keyCode)) return true;
        else
        switch (keyCode) {
            case KEY_CODE.UP:
                // 2017.08.31 이중 포커스 현상에 따른 포커스 초기화 수정
                focusIdx = 0;
                if (isBottomFocus) {
                    isBottomFocus = false;
                    buttonFocusRefresh(focusIdx);
                    div.find("#parents_watch_manager").addClass("focus");
                    div.find("#parent_education_guide").removeClass("focus");
                    posterComp[0].blurred();
                } else {
                    isBottomFocus = true;
                    div.find("#parents_watch_manager").removeClass("focus");
                    div.find("#parent_education_guide").addClass("focus");
                    div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");
                    posterComp[0].focused();
                }
                return true;
            case KEY_CODE.DOWN:
                // 2017.08.31 이중 포커스 현상에 따른 포커스 초기화 수정
                focusIdx = 0;
                if (!isBottomFocus) {
                    isBottomFocus = true;
                    div.find("#parents_watch_manager").removeClass("focus");
                    div.find("#parent_education_guide").addClass("focus");
                    div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");
                    posterComp[0].focused();
                } else {
                    isBottomFocus = false;
                    buttonFocusRefresh(focusIdx);
                    div.find("#parents_watch_manager").addClass("focus");
                    div.find("#parent_education_guide").removeClass("focus");
                    posterComp[0].blurred();
                }
                return true;
            case KEY_CODE.LEFT:
                if (focusIdx > 0) {
                    focusIdx --;
                    buttonFocusRefresh(focusIdx);
                } else this.viewMgr.historyBack();
                return true;
            case KEY_CODE.RIGHT:
                if (isBottomFocus) {
                    focusIdx = 0;
                    isBottomFocus = false;
                    buttonFocusRefresh(focusIdx);
                    div.find("#parents_watch_manager").addClass("focus");
                    div.find("#parent_education_guide").removeClass("focus");
                    posterComp[0].blurred();
                    return true;
                }
                if (!KIDS_MODE_MANAGER.isKidsMode()) {
                    isBottomFocus = true;
                    div.find("#parents_watch_manager").removeClass("focus");
                    div.find("#parent_education_guide").addClass("focus");
                    div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");
                    posterComp[0].focused();
                    return true;
                }
                if (focusIdx < 2) {
                    focusIdx ++;
                    buttonFocusRefresh(focusIdx);
                } else {
                    focusIdx = 0;
                    isBottomFocus = true;
                    div.find("#parents_watch_manager").removeClass("focus");
                    div.find("#parent_education_guide").addClass("focus");
                    div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");
                    posterComp[0].focused();
                }
                return true;
            case KEY_CODE.ENTER:
                if (focusIdx == 2) {
                    LayerManager.startLoading({preventKey: true});
                    KidsCharacterManager.getKidsList(function(data, result) {
                        LayerManager.stopLoading();
                        if (result) {
                            if (data.resultCd == 0) {
                                if (data.list.length == 0) {
                                    LayerManager.activateLayer({
                                        obj: {
                                            id: "kidsErrorPopup",
                                            type: Layer.TYPE.POPUP,
                                            priority: Layer.PRIORITY.POPUP,
                                            linkage: true,
                                            params: {
                                                title: "오류",
                                                message: (Array.isArray(data.resultMsg) ? data.resultMsg : [data.resultMsg]),
                                                button: "닫기",
                                                reboot: false
                                                // callback: _error
                                            }
                                        },
                                        moduleId: "module.kids",
                                        visible: true
                                    });
                                } else {
                                    LayerManager.activateLayer({
                                        obj: {
                                            id: "KidsCharacterSettingPopup",
                                            type: Layer.TYPE.POPUP,
                                            priority: Layer.PRIORITY.POPUP,
                                            linkage: true,
                                            params: {
                                                data: data.list,
                                                complete: function () {
                                                    settingInfoRefresh();
                                                    settingCharacterDataRefresh();
                                                    _this.viewMgr.setKidsCharacterShot();
                                                }
                                            }
                                        },
                                        moduleId: "module.kids",
                                        new: true,
                                        visible: true
                                    });
                                }
                            } else {
                                var tmpMessage = [];
                                if (data.resultCd == 500) {
                                    tmpMessage = ["서버 내부 오류"];
                                } else if (data.resultCd == 404) {
                                    tmpMessage = ["데이터를 찾을 수 없음"];
                                } else if (data.resultCd == 400) {
                                    tmpMessage = ["Request 오류"];
                                } else if (data.resultCd == 501) {
                                    tmpMessage = ["데이터베이스 오류"];
                                } else if (data.resultCd == 1001) {
                                    tmpMessage = ["framework 버전 오류"];
                                } else {
                                    tmpMessage = [data.resultMsg];
                                }
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "kidsErrorPopup",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            title: "오류",
                                            message: tmpMessage,
                                            button: "닫기",
                                            reboot: false
                                            // callback: _error
                                        }
                                    },
                                    moduleId: "module.kids",
                                    visible: true
                                });
                            }
                        } else {
                            LayerManager.activateLayer({
                                obj: {
                                    id: "kidsErrorPopup",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        title: "오류",
                                        message: ["네트워크 연결이 올바르지 않습니다", "인터넷 연결을 확인해 주세요"],
                                        button: "닫기",
                                        reboot: false
                                        // callback: _error
                                    }
                                },
                                moduleId: "module.kids",
                                visible: true
                            });
                        }
                    });
                } else {
                    LayerManager.activateLayer({
                        obj: {
                            id: focusIdx == 0 ? (KIDS_MODE_MANAGER.isKidsMode() ? "KidsModeOffPopup" : "KidsModeSettingPopup") : "KidsInputPasswordPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: {
                                langInfo: language,
                                complete:function() {
                                    settingInfoRefresh();
                                }
                            }
                        },
                        moduleId: "module.kids",
                        new: true,
                        visible: true
                    });
                }
                return true;
            default:
                return false;
        }
    };

    function buttonFocusRefresh(index) {
        focusIdx = index;
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(" + index + ")").addClass("focus");
    }

    function settingInfoRefresh() {
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(0) .parents_watch_manager_btn_subTitle .text").text(KIDS_MODE_MANAGER.isKidsMode() ? "ON" : "OFF");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(1) .parents_watch_manager_btn_subTitle .text").text(window.KidsTimerCheckingManager.getLimitWatchingText() != null ? window.KidsTimerCheckingManager.getLimitWatchingText() : "OFF");
        if (KIDS_MODE_MANAGER.isKidsMode()) {
            div.find("#parents_watch_manager_info").html("Tip! 캐릭터 설정시 리모컨의 별(*) 키를 누르시면<br>캐릭터가 나와서 부모님 대신 TV를 끌 수 있게 도와줍니다");
        } else {
            div.find("#parents_watch_manager_info").html("키즈모드 ON 설정 시 olleh tv를 켰을 때 키즈 메뉴로 시작하게 되며,<br>시청관리, 캐릭터 설정이 가능하고 어린이채널로만 구성된 Live 채널을 시청하게 됩니다");
        }
    }
    function settingCharacterDataRefresh() {
        // 케릭터 데이터 영역 업데이트
        var selData = KidsCharacterManager.getSelectCharacterData();
        if (KidsCharacterManager.getIsSelectCharacter()) {
            if (language == "eng") {
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_title").css({top:"32px", width:"200px", height:"82px", "line-height": "41px"});
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_charImg").addClass("eng");
            } else {
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_title").css("top", "54px");
                div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_charImg").removeClass("eng");
            }
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle").css({ visibility: "hidden"});
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle .text").text("");
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_charImg").attr("src", selData.imgUrl);


        } else {
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_title").css({top: "36px", width:"", height:"38px", "line-height":""});
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle").css({ visibility: "inherit"});
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(2) .parents_watch_manager_btn_subTitle .text").text("OFF");
            div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_charImg").attr("src", "");
        }
    }


    this.getView = function () {
        return div;
    };

    this.focused = function () {
        settingInfoRefresh();
        settingCharacterDataRefresh();
        isBottomFocus = false;
        focusIdx = 0;
        div.removeClass("blurred");
        div.find("#parents_watch_manager").removeClass("blurred");
        div.find("#parent_education_guide").removeClass("blurred");
        div.find("#parents_watch_manager_title_div").removeClass("blurred");
        div.find("#parents_education_guide_title_div").removeClass("blurred");
        div.find("#parents_watch_manager_info").removeClass("blurred");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("blurred");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_bg").removeClass("blurred");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div:eq(" + focusIdx + ")").addClass("focus");

        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_charImg").removeClass("blurred");
    };

    this.blurred = function () {
        posterComp[0].blurred();
        settingInfoRefresh();
        settingCharacterDataRefresh();
        div.addClass("blurred");
        div.find("#parents_watch_manager").addClass("blurred");
        div.find("#parent_education_guide").addClass("blurred");
        div.find("#parents_watch_manager_title_div").addClass("blurred");
        div.find("#parents_education_guide_title_div").addClass("blurred");
        div.find("#parents_watch_manager_info").addClass("blurred");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").addClass("blurred");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_bg").addClass("blurred");
        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_div").removeClass("focus");

        div.find("#parents_watch_manager_btn_area .parents_watch_manager_btn_charImg").addClass("blurred");
    };
};

kidsSubHome.view.ParentsWorldView.prototype = new View();
kidsSubHome.view.ParentsWorldView.prototype.constructor = kidsSubHome.view.ParentsWorldView;
kidsSubHome.view.ParentsWorldView.prototype.type = View.TYPE.CONTENTS;