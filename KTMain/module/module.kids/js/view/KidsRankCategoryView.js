/**
 * Created by Yun on 2017-04-19.
 */

kidsSubHome.view.KidsRankCategoryView = function(viewId) {
    View.call(this, viewId);
    var div = this.div;

    var catData;
    var dataList = [];
    var itemCnt;
    var posterListComp;
    var indicator;
    var isFocused = false;
    var type = 0;
    var viewType = 0;
    var contextMenu;
    var sortGb;
    var isLoaded = false;
    this.connerId;

    var instance = this;

    this.create = function(data) {
        catData = data;
        sortGb = catData.children[type].sortGb;
        div.attr("class", "contentsView");
        div.addClass("blurred");
        div.append(_$("<div/>", {class:"indicator_area"}));
        div.append(_$("<div/>", {class:"contents_area"}));
        div.append(_$("<div/>", {class:"bottom_dim"}));
        div.append(_$("<div/>", {class:"over_sdw_area"}));
        div.append(_$("<div/>", {class:"adult_auth_area"}));
        div.append(_$("<div/>", {class: 'order_code_text'}).text("일간 시청 순위"));
        div.append(_$("<div/>", {class: 'context_menu_indicator'}));
        kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
            if(result) createContentsView.call(instance, {categoryData: catData.children[type], dataList: response.itemDetailList});
            else LayerManager.stopLoading();
            isLoaded = true;
        }, catData.children[type].id, 1, 15);

        createContextMenu();
    };

    function createContextMenu() {
        contextMenu = new ContextMenu_VOD();
        contextMenu.addTitle("정렬옵션");
        contextMenu.addDropBox(["일간 시청 순위", "주간 시청 순위"]);
        contextMenu.addSeparateLine();

        var posterType = getPosterListType(catData.listType, catData.posterListType);
        viewType = posterType === -1 || posterType === -2 ? 1 : 0;
        contextMenu.addTitle("VOD 리스트 보기 방식");
        contextMenu.addDropBox(["포스터 보기", "타이틀 보기"], viewType);

        contextMenu.addSeparateLine();
        contextMenu.addTitle("키즈모드 설정");
        contextMenu.addButton("키즈모드 ON");

        contextMenu.setEventListener(contextMenuListener);
        div.append(contextMenu.getView());
    }

    function createContentsView(data) {
        setChartOrder(data.dataList);
        setOverSdwArea(data);
        var pt = getPosterListType(data.categoryData.listType, data.categoryData.posterListType);
        posterListComp = getPosterListComp(pt);
        posterListComp.create(data.categoryData.id, data.dataList);
        div.find(".contents_area").empty().append(posterListComp.getView());
        indicator = new Indicator(1);

        posterListComp.setIndicator(indicator);
        posterListComp.parent = this;
        dataList = data.dataList;
        itemCnt = data.dataList.length>0?data.dataList[0].webCatItemCnt-0:0;
        this.connerId = data.categoryData.connerId;
        setData(dataList, 0, sortGb, itemCnt);
        div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());

        if(isFocused) posterListComp.focused();
        else posterListComp.blurred();
        LayerManager.stopLoading();
    }

    function setChartOrder(data) {
        var order = 0;
        for(var i in data) {
            if(data[i].itemType==3||data[i].itemType==7||data[i].itemType==8) {
                data[i].chartOrder = undefined;
            }else {
                data[i].chartOrder = ++order;
            }
        }
    }

    /**
     * 포스터 리스트 타입을 반환한다.
     *
     * @param listType
     * @param posterListType
     * @returns {number}   -1 : 미리보기 포스터가 직사각형인 타이틀
     *                      -2 : 미리보기 포스터가 정사각형인 타이틀
     *                      1 : 직사각형 포스터
     *                      2 : 정사각형 포스터
     */
    function getPosterListType(listType, posterListType) {
        var listMode = StorageManager.ps.load(StorageManager.KEY.VOD_LIST_MODE);
        log.printDbg("getPosterListType() - listMode : " + listMode + ", listType : " + listType + ", posterListType : " + posterListType);
        var type = undefined;
        if (listMode === "poster") { // 포스터 보기
            type = posterListType;
        } else if (listMode === "text" || listMode === "list" || listType == 1) { // 타이틀 보기
            if (parseInt(posterListType) === 2) { // 미리보기 포스터가 정사각형인 경우
                type = -2;
            } else { // 미리보기 포스터가 직사각형인 경우
                type = -1;
            }
        } else {
            type = posterListType;
        }
        log.printDbg("getPosterListType() - result, type : " + type);
        return type;
    }

    function getPosterListComp(posterListType) {
        switch (parseInt(posterListType)) {
            case -1: viewType = 1; return new RankingTitleList(instance, RankingTitleList.TYPE.VERTICAL);// 미리보기 포스터가 직사각형인 경우
            case -2: viewType = 1; return new RankingTitleList(instance, RankingTitleList.TYPE.SQUARE);// 미리보기 포스터가 정사각형인 경우
            case 2: viewType = 0; return new SquarePosterList();
            default: viewType = 0; return new VerticalPosterList();
        }
    }

    function setOverSdwArea(data) {
        if(data.posterListType!=0) return;
        var d = div.find(".over_sdw_area");
        d.append("<div class='over_sdw_1'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        d.append("<div class='over_sdw_2'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        d.append("<div class='over_sdw_3'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
    }

    function setData(_dataList, page, _sortGb, itemCnt) {
        posterListComp.setData(_dataList, page, _sortGb, itemCnt);
    }

    function changeFocus(amount) {
        posterListComp.changeFocus(amount);
    }

    function changePage(amount) {
        posterListComp.changePage(amount);
    }

    function setFocus(newFocus) {
        posterListComp.setFocus(newFocus);
    }

    function setPage(newPage){
        posterListComp.setPage(newPage);
    }

    this.loadAddedContents = function (startIdx, amount, callback, showLoading) {
        if(startIdx>itemCnt-1 || (dataList[startIdx] && dataList[Math.min(startIdx+amount-1, itemCnt-1)])) callback();
        else {
            if(showLoading) LayerManager.startLoading({preventKey: true});
                kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                if(result) {
                    response.itemDetailList = response.itemDetailList ? (Array.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList]) : [];
                    for (var i = 0; i < amount; i++) dataList[startIdx + i] = response.itemDetailList[i];
                    posterListComp.updateData(dataList);
                    if (callback) callback();
                    if (showLoading) LayerManager.stopLoading();
                } else if (showLoading) LayerManager.stopLoading();
            }, catData.children[type].id, startIdx + 1, Math.min(itemCnt-startIdx+1, amount));
        }
    };

    function contextMenuListener(index, data) {
        sortGb = catData.children[type].sortGb;
        contextMenu.close();
        switch (index) {
            case 0:
                if(type==data) return;
                type = data;
                kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                    if(result){
                        div.find(".order_code_text").text(type==0?"일간 시청 순위":"주간 시청 순위");
                        createContentsView.call(instance, {categoryData: catData.children[type], dataList: response.itemDetailList});
                    }
                }, catData.children[type].id, 1, 15);
                break;
            case 1:
                if(viewType==data) return;
                viewType = data;
                StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, viewType==0?"poster":"list");
                instance.viewMgr.clearRemovableQueue();
                createContentsView.call(instance, {categoryData: catData.children[type], dataList: dataList});
                break;
        }
    }

    this.onKeyAction = function(keyCode) {
        if(contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        else if(keyCode==KEY_CODE.CONTEXT) {
            var data = posterListComp.getFocusedContents();
            contextMenu.setVodInfo({
                itemName: data.contsName||data.itemName,
                imgUrl: data.imgUrl,
                contsId: data.itemId||data.contsId,
                catId: catData.children[type].id,
                itemType: data.itemType,
                cmbYn: data.cmbYn,
                seriesType: data.seriesType,
                resolCd: data.resolCd,
                assetId: data.assetId,
                prInfo: data.prInfo,
                isKids: true
            });
            contextMenu.open();
            return true;
        } else return posterListComp.onKeyAction(keyCode);
    };

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.removeClass("blurred");
        if(posterListComp) posterListComp.focused();
        isFocused = true;
        div.find(".context_menu_indicator").html("찜 | 정렬");
        div.find(".order_code_text").show();
        div.find(".bottom_dim").addClass("show");
        return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.blurred =  function() {
        if(posterListComp) posterListComp.blurred();
        div.find(".context_menu_indicator").text("");
        div.find(".order_code_text").hide();
        isFocused = false;
        div.addClass("blurred");
        div.find(".bottom_dim").removeClass("show");
    };

    this.pause = function () {
        if(posterListComp.hasOwnProperty("pause")) posterListComp.pause();
    };

    this.resume = function () {
        if(posterListComp.hasOwnProperty("resume")) posterListComp.resume();
    };

    this.isLoaded = function () {
        return isLoaded;
    }
};
kidsSubHome.view.KidsRankCategoryView.prototype = new View();
kidsSubHome.view.KidsRankCategoryView.prototype.constructor = kidsSubHome.view.KidsRankCategoryView;
kidsSubHome.view.KidsRankCategoryView.prototype.type = View.TYPE.CONTENTS;