/**
 * Created by Yun on 2017-04-10.
 */

kidsSubHome.view.KidsMyPlayListView = function (viewId) {
    View.call(this, viewId);
    var instance = this;
    var playList = [];
    var indicator;
    var dom = this.div;
    var comp;

    var page = 0;
    var mainFocus = 0;
    var listFocus = 1;
    var menuFocus = 0;

    var playListId;
    var playListNm;
    var isLoaded = false;

    var pageMaxCnt = 5;

    var menuMaxCount = 5;

    this.itemCnt;

    this.create = function(data) {
        log.printDbg('create() - data : ' + data);
        playListId = data.id;
        playListNm = data.name;
        this.itemCnt = data.itemCnt;
        this.div.addClass("kidsMyList");
        this.div.append(_$("<div/>", {class: "relationBtn_area"}));
        this.div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text playlist"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "OFF" : "ON")));
        indicator = new Indicator(1);


        if (KTW.managers.service.ProductInfoManager.isMilitary()) {
            menuMaxCount = 4;
        }
    };

    function update() {
        log.printDbg('update() - isLoaded : ' + isLoaded + ', playList : ' + playList + ', playListRefresh : ' + (!kidsSubHome.KidsMyPlayListManager.getIsPlaylistRefresh()));
        var mpld = kidsSubHome.KidsMyPlayListManager.getPlayListData(playListId);

        if (isLoaded && playList && (!kidsSubHome.KidsMyPlayListManager.getIsPlaylistRefresh())) {
            listFocus = 0;
            //indicator.blurred();
            changeMainFocus(0);
            return;
        }
        kidsSubHome.amocManager.getMyPlayItemDetList(function (result, data) {
            if (result) {
                playList = data.playItemDetList;
                if (playList == null) playList = [];
                if (!Array.isArray(playList)) playList = [playList];
                instance.itemCnt = playList.length;
                mpld.isUpdated = true;
                kidsSubHome.KidsMyPlayListManager.setIsPlaylistRefresh(false);

                createDom();

                // [si.mun] 플레이 리스트 시나리오 정책 중
                // 콘텐츠 순서 변경 후에 재생버튼으로 포커스가 이동하는 시나리오가 존재함.
                // 이때 isLoaded 는 true 인 상태이므로 재생버튼으로 포커스가 이동해야함. (changeMainFocus(1) 수행)
                // 하지만, 외부에서 키즈 플레이리스트 추가 후 재 진입 시에도 changeMainFocus(1)을 수행하므로
                // blurred 상태임에도 불구하고 재생 버튼에 포커스가 위치한 상태가 되어버림.
                // 이를 방지하기 위해 blurred 상태가 아닐 때만 재생으로 포커스 이동하도록 수정
                if (isLoaded) {
                    if (!dom.hasClass("blurred")) {
                        changeMainFocus(1);
                    }
                } else {
                    changeMainFocus(0);
                }
                isLoaded = true;

                toggleAllCheckItem(true);
                setDisableButtonState();
                if (playList.length <= 0 && !dom.hasClass("blurred")) instance.viewMgr.historyBack();
            }
        }, playListId);
    }

    function createDom() {
        log.printDbg('createDom() - playList : ' + playList);
        // playList 갯수
        var playListCnt = playList ? playList.length : 0;

        if (playList && playListCnt > 0) {
            var totalCntArea = _$("<div/>", {class: "playlist_cnt"}).text("총 " + playListCnt + " 개");
            var contentsArea = _$("<div/>", {class: "contents_area"});
            var wp = _$("<div/>", {class: "overflow_wrapper"});
            contentsArea.append(wp);
            var contentList = _$("<div/>", {class: "content_list"});
            for (var i = 0; i < pageMaxCnt; i++)
                contentList.append(getItem());
            wp.append(contentList);
            wp.append(_$("<div/>", {class: "focus_area"}));

            dom.empty().append(totalCntArea);
            dom.append(contentsArea);
            dom.append(_$("<div/>", {class: "menu_area"}));
            dom.append(_$("<div/>", {class: "indicator_area"}));
            setMenuArea();
            setPage(0, true);

            indicator.setSize(Math.floor((playListCnt - 1) / pageMaxCnt) + 1);
            dom.find(".indicator_area").append(indicator.getView()).append(indicator.getPageTransView());
        } else {
            var contentsBox = _$("<div/>", {class: "contents_area noContents"});
            dom.empty().append(contentsBox);
            contentsBox.append(_$("<img>", {class: "msg_icon", src: window.modulePath + 'resource/image/icon_noresult_kids.png'}));
            contentsBox.append(_$("<span/>", {class: "main"}).text("등록된 콘텐츠가 없습니다"));
            contentsBox.append(_$("<span/>", {class: "sub"}).html("나만의 플레이리스트를 구성해 보세요<br>VOD선택 후 <icon/>옵션 키로<br>플레이리스트에 등록할 수 있습니다"));
        }
    }

    function setMenuArea() {
        var area = dom.find(".menu_area");
        var bg_area = _$("<div/>", {class:"bg_area"});
        bg_area.append(_$("<div/>", {class:"bg_top"}));
        bg_area.append(_$("<div/>", {class:"bg_mid"}));
        bg_area.append(_$("<div/>", {class:"bg_bottom"}));
        area.append(bg_area);

        var btn_area = _$("<div/>", {class:"btn_area"});
        btn_area.append(_$("<btn/>").text("전체 선택 해제"));
        btn_area.append(_$("<btn/>").text("재생"));

        if (!KTW.managers.service.ProductInfoManager.isMilitary()) {
            btn_area.append(_$("<btn/>").text("반복재생"));
        }

        btn_area.append(_$("<btn/>").text("삭제"));
        btn_area.append(_$("<sepline/>"));
        btn_area.append(_$("<btn/>").text("순서변경"));
        area.append(btn_area);
    }

    function getItem() {
        var item = _$("<div/>", {class: 'item_area'});
        var tmp = _$('<div>', {class: 'checkBoxArea'});
        tmp.append(_$('<div>', {class: 'checkBox'}));
        item.append(tmp);

        tmp = _$('<div>', {class: 'absoluteWrapper'});
        tmp.append(_$('<img>', {class: 'musicBg', src: modulePath + "resource/image/default_poster.png"}));
        tmp.append(_$('<img>', {class: 'poster'}));
        tmp.append(_$("<div/>", {class: "lock_icon"}).append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"})));
        item.append(_$('<div>', {class: 'posterArea'}).append(_$('<div>', {class: 'posterBox'}).append(tmp)));


        tmp = _$('<div>', {class: 'titleArea'});
        tmp.append(_$('<div>', {class: 'title'}).html("<span/>"));
        tmp.append(_$('<span>', {class: 'description'}));
        item.append(tmp);
        return item;
    }

    function setItem(div, itemData) {
        log.printDbg('setItem() - div : ' + div + ', itemData : ' + itemData);

        if(itemData) {
            div.find("img.poster").attr({
                "src": itemData.imgUrl + "?w=87&h=124&quality=90",
                onerror: "this.src='" + modulePath + "resource/image/default_poster.png'"
            });
            div.find(".lock_icon").toggle(UTIL.isLimitAge(itemData.prInfo));
            div.find(".titleArea .title span").text(itemData.contsName);
            div.find(".titleArea span.description").html(getBottomText(itemData.wonYn, itemData.prInfo));
            div.find(">*").css("visibility", "");
            div.toggleClass("checked", checkedList.indexOf(itemData)>=0);
        }else {
            div.find(">*").css("visibility", "hidden");
        }
    }

    function clearTextAnimation() {
        UTIL.clearAnimation(dom.find(".item_area .textAnimating span"));
        dom.find(".item_area .textAnimating").removeClass("textAnimating");
        void dom[0].offsetWidth;
    }

    function setTextAnimation(focus) {
        clearTextAnimation();

        if (!playList || playList.length <= 0) {
            return;
        }

        if(UTIL.getTextLength(playList[focus].contsName, "RixHead M", 33, -1.5)>495) {
            var posterDiv = dom.find(".item_area:eq(" + (focus%pageMaxCnt) + ") .title");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    function getBottomText(wonYN, prInfo) {
        return _$((HTool.isTrue(wonYN) ? "<img class='isfreeIcon'>" : (HTool.isFalse(wonYN) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/icon_age_list_" + ServerCodeAdapter.getPrInfo(prInfo - 0) + ".png' class='age'>");
    }

    function setListFocus(focus) {
        log.printDbg('setListFocus() - focus : ' + focus);

        listFocus = focus;
        setPage(Math.floor(focus/pageMaxCnt));
        dom.find(".item_area.focus").removeClass("focus");
        dom.find(".item_area").eq(focus%pageMaxCnt).addClass("focus");
        dom.find(".focus_area").css("-webkit-transform", "translateY(" + Math.floor(listFocus % pageMaxCnt * 166) + "px)").removeClass("hidden");
        setTextAnimation(focus);
    }

    function removeListFocus(clearFocus) {
        log.printDbg('removeListFocus() - clearFocus : ' + clearFocus);

        if (clearFocus) setListFocus(0);
        dom.find(".item_area.focus").removeClass("focus");
        dom.find(".focus_area").addClass("hidden");
    }

    function changeMenuFocus(amount) {
        log.printDbg('changeMenuFocus() - amount : ' + amount);

        var newFocus = HTool.getIndex(menuFocus, amount, menuMaxCount);
        if (dom.find(".menu_area btn").eq(newFocus).hasClass("disable")) {
            changeMenuFocus(amount+(amount/Math.abs(amount)));
        } else setMenuFocus(newFocus);
    }

    function setMenuFocus(focus) {
        menuFocus = focus==null?menuFocus:focus;
        dom.find(".menu_area btn.focus").removeClass("focus");
        dom.find(".menu_area btn").eq(menuFocus).addClass("focus");
    }

    function removeMenuFocus() {
        menuFocus = 0;
        dom.find(".menu_area btn.focus").removeClass("focus");
    }

    function setPage(_page, forced) {
        if(!forced && page == _page) return;
        page = _page;
        var itemArea = dom.find(".item_area");
        for (var i = 0; i < pageMaxCnt; i++) {
            setItem(itemArea.eq(i), playList[page*pageMaxCnt+i]);
        } indicator.setPos(page);
    }

    function changeMainFocus(mf) {
        switch (mf) {
            case 0:
                mainFocus = 0;
                removeMenuFocus();
                dom.find(".focus_area").removeClass("hidden");
                setListFocus(Math.min(listFocus, playList.length - 1));
                break;
            case 1:
                mainFocus = 1;
                removeListFocus();
                setMenuFocus(checkedList.length>0?1:0);
                UTIL.clearAnimation(dom.find(".item_area .textAnimating span"));
                dom.find(".item_area .textAnimating").removeClass("textAnimating");
                break;
        }
    }

    function toggleAllCheckItem(check) {
        if(check==null) check = checkedList.length != playList.length;

        if(check) {
            checkedList = [].concat(playList);
            dom.find(".item_area").addClass("checked");
        } else {
            checkedList = [];
            dom.find(".item_area").removeClass("checked");
        }
    }

    function setDisableButtonState() {
        var selectCnt = checkedList.length;
        var btnList = dom.find(".menu_area btn");
        btnList.eq(0).text(selectCnt == playList.length ? "전체 선택 해제" : "전체 선택");

        btnList.eq(1).toggleClass("disable", selectCnt==0);
        btnList.eq(2).toggleClass("disable", selectCnt==0);
        btnList.eq(3).toggleClass("disable", selectCnt==0);
    }

    function onKeyForList(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (playList.length<=2) break;

                if (listFocus === 0) { // 첫 번째 콘텐츠
                    setListFocus(playList.length - 1);
                } else {
                    setListFocus(listFocus - 1);
                }
                break;
            case KEY_CODE.DOWN:
                if (playList.length <= 2) break;

                if (listFocus === playList.length - 1) { // 마지막 콘텐츠
                    setListFocus(0);
                } else {
                    setListFocus(listFocus + 1);
                }
                break;
                break;
            case KEY_CODE.LEFT:
            case KEY_CODE.BACK:
                setListFocus(0);
                removeMenuFocus();
                instance.viewMgr.historyBack();
                break;
            case KEY_CODE.RIGHT:
                changeMainFocus(1); // 연관 메뉴로 focus 이동
                if (instance.itemCnt > pageMaxCnt) {
                    // 적/청색 page hidden
                    indicator.togglePageTransIcon(true);
                }
                break;
            case KEY_CODE.RED:
                if(listFocus===0) setListFocus(Math.floor((playList.length-1) / pageMaxCnt)*pageMaxCnt);
                else if(listFocus%pageMaxCnt===0) setListFocus(listFocus-pageMaxCnt);
                else setListFocus(page*pageMaxCnt);
                break;
            case KEY_CODE.BLUE:
                if(listFocus===playList.length-1) setListFocus(Math.min(pageMaxCnt - 1, playList.length-1));
                else if(listFocus%pageMaxCnt===pageMaxCnt - 1) setListFocus(Math.min(playList.length-1, listFocus+pageMaxCnt));
                else setListFocus(Math.min(playList.length-1, page*pageMaxCnt+pageMaxCnt - 1));
                break;
            case KEY_CODE.ENTER:
                if (checkedList.indexOf(playList[listFocus]) >= 0) {
                    checkedList.splice(checkedList.indexOf(playList[listFocus]), 1);
                    dom.find(".item_area").eq(listFocus % pageMaxCnt).removeClass("checked");
                } else {
                    checkedList.push(playList[listFocus]);
                    dom.find(".item_area").eq(listFocus % pageMaxCnt).addClass("checked");
                }
                setDisableButtonState();
                break;
            case KEY_CODE.CONTEXT:
                return true;
            default:
                return false;
        }
        return true;
    }

    function onKeyForMenu(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                changeMenuFocus(-1);
                break;
            case KEY_CODE.DOWN:
                changeMenuFocus(1);
                break;
            case KEY_CODE.LEFT:
                if (instance.itemCnt > pageMaxCnt) {
                    // 적/청색 page show
                    indicator.togglePageTransIcon(false);
                }
                changeMainFocus(0);
                break;
            case KEY_CODE.ENTER:
                enterKeyActionForMenu();
                break;
            case KEY_CODE.CONTEXT:
                return true;
            default: return false;
        } return true;
    }

    function enterKeyActionForMenu() {

        function activateKidsEditModeDeletePopup () {
            // 삭제
            var items = [], itemDiv = dom.find(".item_area");
            for (var i in checkedList)
                items.push({title: checkedList[i].contsName});

            if (items.length>0) LayerManager.activateLayer({
                obj : {
                    id: "KidsEditModeDeletePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        type: 3,
                        items: items,
                        listName: playListNm,
                        callback: function (res) {
                            if (res) {
                                var contId = "", itemDiv = dom.find(".item_area");
                                for (var i in checkedList) contId += checkedList[i].contsKey + "|";
                                kidsSubHome.KidsMyPlayListManager.setMyPlayItemDetList(function (res) {
                                    if (res) {
                                        playList = null;
                                        update();
                                    }
                                }, playListId, items.length, contId.substr(0, contId.length-1));
                            } LayerManager.historyBack();
                        }
                    }
                },
                moduleId: "module.kids",
                visible: true
            });
        }

        function activateKidsPlayListOrderChangePopup () {
            // 순서 변경
            LayerManager.activateLayer({
                obj: {
                    id: "KidsPlayListOrderChangePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        playListId: playListId,
                        playList: playList,
                        callback: function () {
                            playList = null;
                            update();
                        }
                    }
                },
                new: true,
                moduleId:"module.kids",
                visible: true
            });
        }

        switch (menuFocus) {
            case 0:
                toggleAllCheckItem();
                setDisableButtonState();
                break;
            case 1:
                // 재생
                requestPlayPlayList(false);
                break;
            case 2:
                if (KTW.managers.service.ProductInfoManager.isMilitary()) {
                    // 삭제
                    activateKidsEditModeDeletePopup();
                }
                else {
                    // 반복재생
                    requestPlayPlayList(true);
                }
                break;
            case 3:

                if (KTW.managers.service.ProductInfoManager.isMilitary()) {
                    // 순서 변경
                    activateKidsPlayListOrderChangePopup();
                }
                else {
                    // 삭제
                    activateKidsEditModeDeletePopup();
                }
                break;
            case 4:
                // 순서 변경
                activateKidsPlayListOrderChangePopup();
                break;
        }
    }

    function requestPlayPlayList(isLoop) {
        var a = ModuleManager.getModule("module.vod");
        if (!a) {
            ModuleManager.loadModule({
                moduleId: "module.vod",
                cbLoadModule: function (success) { if (success) requestPlayPlayList(isLoop); }
            }); return;
        } else {
            a.execute({
                method:"requestVODPlayList",
                params: {
                    playList: sortCheckedList(),
                    isLoop: isLoop,
                    req_cd: "46",
                    isKids: true
                }
            });
        }
    }

    function sortCheckedList() {
        var tmp = [];
        for(var i in playList) {
            if(checkedList.indexOf(playList[i])>=0) tmp.push(playList[i]);
        }
        // checkedList = tmp;
        return tmp;
    }

    this.onKeyAction = function (keyCode) {
        if (keyCode == KEY_CODE.CONTEXT) return true;
        //if (indicator.isFocused()) return onKeyForIndicator(keyCode);
        if (mainFocus == 0) return onKeyForList(keyCode);
        else if (mainFocus == 1) return onKeyForMenu(keyCode);
    };

    this.focused = function () {
        this.viewMgr.setHideCharacter();
        this.div.find(".relationBtn_area").removeClass("blurred");
        this.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "OFF" : "ON"));
        if (this.itemCnt > 0) {
            setListFocus(0);

            if (instance.itemCnt > pageMaxCnt) {
                // 적/청색 page show
                indicator.togglePageTransIcon(false);
            }
            View.prototype.focused.apply(this, arguments);
            return true;
        } else return false;
    };

    this.blurred = function () {
        //this.viewMgr.setHomeShotHide();
        //this.viewMgr.setShowCharacter();
        this.div.find(".relationBtn_area").addClass("blurred");
        this.div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "OFF" : "ON"));
        View.prototype.blurred.apply(this, arguments);
        //setMenuFocus(1);
        toggleAllCheckItem(true);
        setDisableButtonState();
        UTIL.clearAnimation(dom.find(".item_area .textAnimating span"));
        dom.find(".item_area .textAnimating").removeClass("textAnimating");
        setPage(0);
    };

    this.show = function () {
        View.prototype.show.apply(this, arguments);
        update();
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
        comp && comp.hide && comp.hide();
    };

    this.pause = function() {
        clearTextAnimation();
    };

    this.resume = function() {
        if (mainFocus == 0) setTextAnimation(listFocus);
    };

    this.getComponent = function () {
        return comp;
    };

};

kidsSubHome.view.KidsMyPlayListView.prototype = new View();
kidsSubHome.view.KidsMyPlayListView.prototype.constructor = kidsSubHome.view.KidsMyPlayListView;
kidsSubHome.view.KidsMyPlayListView.prototype.type = View.TYPE.CONTENTS;