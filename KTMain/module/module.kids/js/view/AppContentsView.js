/**
 * Created by ksk91_000 on 2016-11-30.
 */
kidsSubHome.view.AppContentsView = function(viewId) {
    View.call(this, viewId);
    var div = this.div;
    var adultComp;
    var catData;

    this.create = function(data) {
        div.attr("class", "appContentsView");
        div.append(_$("<div/>", {class:"contents_area"}));
        div.append(_$("<div/>", {class:"adult_auth_area"}));

        // to make relation button
        div.append(_$("<div/>", {class: "relationBtn_area"}));
        div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기")));

        catData = data;
        if(data.type === "Adult"||data.type === "Motel")
            setAdultAuthArea.call(this, data, createContentsView);
        else
            createContentsView.call(this, data);
    };

    function createContentsView(data) {
        var area = div.find(".contents_area");
        // var param = ((catData.id === MenuDataManager.MENU_ID.YOUTUBE || catData.id === MenuDataManager.MENU_ID.YOUTUBE_KIDS)?"":"?w=402&h=450&quality=90");
        area.append(_$("<img/>", {src: data.imgUrl, onerror:"this.src='" + modulePath + "resource/image/default_app_rec.png'"}));
        area.append(_$("<div/>", {class:"btn_go"}).text("바로가기"));
    }

    function setAdultAuthArea(data, callback) {
        var that = this;
        adultComp = new AdultAuthBox();
        adultComp.create(function () {
            div.find(".adult_auth_area").detach();
            adultComp = null;
            callback.call(that, data);
            that.viewMgr.historyBack();
        });

        adultComp.parent = this;
        div.find(".adult_auth_area").append(adultComp.getView());
    }

    function setData(_dataList, page) {
    }

    function changeFocus(amount) {
    }

    function changePage(amount) {
    }

    function setFocus(newFocus) {
    }

    function setPage(newPage){
    }

    this.onKeyAction = function(keyCode) {
        if(keyCode==KEY_CODE.LEFT) {
            this.viewMgr.historyBack();
            return true;
        }else if(keyCode==KEY_CODE.ENTER){
            goApplication();
            return true;
        }else if(keyCode==KEY_CODE.CONTEXT) {
            LayerManager.activateLayer({
                obj: {
                    id: (KIDS_MODE_MANAGER.isKidsMode() ? "KidsModeOffPopup" : "KidsModeSettingPopup"),
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        complete:function() {
                            settingInfoRefresh();
                        }
                    }
                },
                moduleId: "module.kids",
                new: true,
                visible: true
            });
            return true;
        }
    };

    function goApplication() {
        try {
            NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                catData.parent.id, catData.id, "", catData.locator);
        } catch(e) {}

        if(catData.id == MenuDataManager.MENU_ID.YOUTUBE) {
            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: CONSTANT.APP_TYPE.YOUTUBE,
                    param: catData.locator
                },
                visible: true
            });
        }else if(catData.id == MenuDataManager.MENU_ID.YOUTUBE_KIDS) {
            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: CONSTANT.APP_TYPE.YOUTUBE,
                    param: catData.locator + "|KIDS"
                },
                visible: true
            });
        }else switch (catData.itemType-0) {
            case 3:    //멀티캐스트 양방향 서비스
                if(!catData.locator) return;

                var nextState;
                var obj = {type:CONSTANT.APP_TYPE.MULTICAST};
                if(catData.locator.indexOf(".")>=0) {
                    if(catData.locator === CONSTANT.APP_ID.MASHUP) nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                    else nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
                    obj.param = catData.locator;
                    obj.ex_param = catData.parameter;
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
                }else {
                    obj.param = CONSTANT.APP_ID.MASHUP;
                    obj.ex_param = catData.locator;
                    nextState = stateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                }

                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: obj,
                    visible:true
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                var nextState = stateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: catData.id
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(catData.locator);
                break;
        }
    }

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.removeClass("blurred");
        div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        div.find(".relationBtn_area").show();
        return kidsSubHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
    };

    this.blurred =  function() {
        div.addClass("blurred");
       // div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        div.find(".relationBtn_area").hide();

    };
};
kidsSubHome.view.AppContentsView.prototype = new View();
kidsSubHome.view.AppContentsView.prototype.constructor = kidsSubHome.view.AppContentsView;
kidsSubHome.view.AppContentsView.prototype.type = View.TYPE.CONTENTS;