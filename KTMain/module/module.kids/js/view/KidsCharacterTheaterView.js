/**
 * Created by Yun on 2017-03-10.
 */


kidsSubHome.view.CharacterTheaterView = function (viewId) {
    View.call(this, viewId);
    var div = this.div;

    var dataIdx = 0;
    var focusIdx = 0;
    var isListFocus = false;
    var posterListComp;
    var indicator;
    var dataList = [];
    var cateData;
    var cateTotalPage;
    var cateCurPage;
    var isListRefresh = false;
    var isContents = false;
    var originData;

    this.create = function (data) {
        cateData = data.categoryData;
        cateTotalPage = Math.ceil(cateData.length/4);
        if (cateTotalPage == 1) cateTotalPage = 0;
        cateCurPage = 0;
        dataIdx = 0;
        focusIdx = 0;


        div.attr("class", "characterTheaterView");
        //sw.nam
        // 캐릭터 전용관 시나리오가 기존 키즈 컨셉하고 일관되지 않아서 따로 배경을 관리함.
        // backgroundTitle 추가
        this.div.html("<div id='kidsBackground'>" +
        "<span id='backgroundTitle'>키즈케릭터컨텐츠</span>"+
        "</div>" +
        "</div>");

        div.append(_$("<div/>", {class: "contents_area"}));
        div.find(".contents_area").append(_$("<img/>", {class: "characterDefault_BG", src:window.modulePath + "resource/image/kids_cha_bg.png"}));
        div.find(".contents_area").append(_$("<img/>", {class: "character_sdw_img", src:modulePath + "resource/image/kids_sdw_cha_1.png"}));
        if (data.w3ListImg) {
            div.find(".contents_area").append(_$("<img/>", {class: "selectCharacter_div", src: data.w3ListImg}).error(function() {_$(this).attr({src: modulePath + "resource/image/kids_cha_default.png"}).css({width:"182px", height:"184px", left:"63px", top:"56px"})}));
        } else {
            div.find(".contents_area").append(_$("<img/>", {class: "selectCharacter_div", src: modulePath + "resource/image/kids_cha_default.png"}).css({width:"182px", height:"184px", left:"63px", top:"56px"}));
        }
        div.find(".contents_area").append(_$("<div/>", {class: "characterTheater_category_area"}));
        div.find(".contents_area .characterTheater_category_area").append(_$("<img/>", {class: "characterTheater_category_arrow_img upArrow", src:modulePath + "resource/image/arw_menu_kids_up.png"}));
        for (var idx = 0 ; idx < (cateData.length > 5 ? 5 : cateData.length) ; idx ++) {
            div.find(".contents_area .characterTheater_category_area").append(setCategoryList(cateData[idx].name));
        }
        div.find(".contents_area .characterTheater_category_area").append(_$("<img/>", {class: "characterTheater_category_arrow_img downArrow", src:modulePath + "resource/image/arw_menu_kids_dw.png"}));

        div.find(".contents_area").append(_$("<div/>", {class: "characterTheater_noContents_area"}));
        div.find(".contents_area .characterTheater_noContents_area").append(_$("<img />", {class:"kidsNoContents_Img", src:window.modulePath + "resource/image/icon_noresult_kids.png"}));
        div.find(".contents_area .characterTheater_noContents_area").append(_$("<div/>", {class:"kidsNoContents_text"}).text("표시할 수 있는 콘텐츠가 없습니다"));
        div.find(".contents_area").append(_$("<div/>", {class: "characterTheater_vod_area"}));
        posterListComp = new TheaterVerticalPosterList();
        posterListComp.create(viewId);
        dataList = data.dataList;
        dataList = dataList?(_$.isArray(dataList)?dataList:[datList]):[];
        div.find(".contents_area").append(_$("<div/>", {class: "characterTheater_app_area"}));
        div.find(".contents_area .characterTheater_app_area").append(_$("<div/>", {class:"characterTheater_app_div"}));
        div.find(".contents_area .characterTheater_app_area .characterTheater_app_div").append(_$("<img/>", {class:"characterTheater_app_img"}));
        div.find(".contents_area .characterTheater_app_area .characterTheater_app_div").append(_$("<div/>", {class:"characterTheater_app_text"}).text("바로가기"));

        if (cateData.length > 5) {
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.upArrow").css("visibility", "hidden");
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.downArrow").css("visibility", "inherit");
        } else {
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img").css("visibility", "hidden");
        }

        div.append(_$("<div/>", {class:"indicator_area"}));
        div.append(_$("<div/>", {class:"relationBtn_area"}));
        div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기")));
        div.append(_$("<div/>", {class:"bottom_dim"}));
        div.append(_$("<div/>", {class:"over_sdw_area"}));
        div.append(_$("<div/>", {class:"adult_auth_area"}));

        originData = data;
        setBgTitleText();
        buttonFocusRefresh(focusIdx, false);
    };

    /**
     * 케릭터 컨텐츠 뷰 좌상단 타이틀 이름 setting
     */
    function setBgTitleText() {
        log.printDbg("setBgTitleText");
        window.originData = originData;
        div.find("#backgroundTitle").text(originData.name);
    }

    function setCategoryList(data) {
        return _$("<div class='characterTheater_category_btn'>" +
                "<div class='characterTheater_category_btn_text'>" + data + "</div>" +
            "</div>");
    }

    function buttonFocusRefresh(index, isSelect) {
        if (Math.floor(dataIdx / 5) != cateCurPage) categoryChangePage(Math.floor(dataIdx / 5));
        div.find(".characterTheater_category_btn").removeClass("unSelect");
        div.find(".characterTheater_category_btn").removeClass("select");
        div.find(".characterTheater_category_btn").removeClass("focus");
        div.find(".characterTheater_app_div").removeClass("focus");
        div.find(".characterTheater_category_btn:eq(" + focusIdx + ")").addClass("focus");
        div.find(".contents_area .characterTheater_noContents_area").removeClass("show");

        if (isSelect) {
            div.find(".characterTheater_category_btn").addClass("unSelect");
            div.find(".characterTheater_category_btn.focus").addClass("select");
            if (cateData[dataIdx].itemType == 3 || cateData[dataIdx].itemType == 7 || cateData[dataIdx].itemType == 8) {
                div.find(".characterTheater_app_div").addClass("focus");
            } else {
                posterListComp.focused();
            }
            isListRefresh = false;
        } else {
            div.find(".characterTheater_app_area").removeClass("show");
            div.find(".characterTheater_vod_area").removeClass("show");
            if (cateData[dataIdx].itemType == 3 || cateData[dataIdx].itemType == 7 || cateData[dataIdx].itemType == 8) {
                div.find(".contents_area .characterTheater_app_area .characterTheater_app_div .characterTheater_app_img").attr("src", cateData[dataIdx].imgUrl);
                div.find(".characterTheater_app_area").addClass("show");
                isContents = true;
            } else {
                if (isListRefresh) {
                    isContents = false;
                    div.find(".contents_area .characterTheater_vod_area").empty();
                    kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                        if (result) {
                            if (response.itemDetailList != null) {
                                var tmpList = _$.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList];
                                posterListComp.setData(tmpList, 0);
                                isContents = true;
                                div.find(".contents_area .characterTheater_vod_area").append(posterListComp.getView());
                            } else {
                                isContents = false;
                                div.find(".contents_area .characterTheater_noContents_area").addClass("show");
                            }
                        } else {
                            isContents = false;
                            div.find(".contents_area .characterTheater_noContents_area").addClass("show");
                        }
                    }, cateData[dataIdx].id, 0, "A");
                }
                isListRefresh = true;
                div.find(".characterTheater_vod_area").addClass("show");
                posterListComp.blurred();
            }
        }
    }

    function categoryChangePage(page) {
        cateCurPage = page;
        if (cateCurPage == 0) focusIdx = dataIdx;
        else if (focusIdx == 4) focusIdx = (cateTotalPage-1 != cateCurPage ? 3 : (cateData.length-1 - ((cateTotalPage-1)*5)));

        if (cateCurPage == 0) {
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.upArrow").css("visibility", "hidden");
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.downArrow").css("visibility", "inherit");
        } else if (cateCurPage == (cateTotalPage-1)) {
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.upArrow").css("visibility", "inherit");
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.downArrow").css("visibility", "hidden");
        } else {
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.upArrow").css("visibility", "inherit");
            div.find(".contents_area .characterTheater_category_area .characterTheater_category_arrow_img.downArrow").css("visibility", "inherit");
        }

        var tagIdx = 0;
        for (var idx = (cateCurPage * 5) ; idx < (cateCurPage * 5) + 5 ; idx ++) {
            if (cateData[idx]) {
                div.find(".contents_area .characterTheater_category_area .characterTheater_category_btn_text:eq(" + tagIdx + ")").text(cateData[idx].name);
                div.find(".contents_area .characterTheater_category_area .characterTheater_category_btn:eq(" + tagIdx + ")").css("display", "block");
            } else {
                div.find(".contents_area .characterTheater_category_area .characterTheater_category_btn_text:eq(" + tagIdx + ")").text("");
                div.find(".contents_area .characterTheater_category_area .characterTheater_category_btn:eq(" + tagIdx + ")").css("display", "none");
            }
            tagIdx ++;
        }
    }

    this.onKeyAction = function (keyCode) {
        if (isListFocus && posterListComp.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.LEFT:
                if (isListFocus) {
                    isListFocus = false;
                    buttonFocusRefresh(focusIdx, false);
                } else return false;
                return true;
            case KEY_CODE.RIGHT:
                if (isContents) {
                    isListFocus = true;
                    buttonFocusRefresh(focusIdx, true);
                }
                return true;
            case KEY_CODE.UP:
                dataIdx = HTool.getIndex(dataIdx, -1, cateData.length);
                focusIdx = HTool.getIndex(focusIdx, -1, (cateTotalPage > 0 ? 5 : cateData.length));
                buttonFocusRefresh(focusIdx, false);
                return true;
            case KEY_CODE.DOWN:
                dataIdx = HTool.getIndex(dataIdx, 1, cateData.length);
                focusIdx = HTool.getIndex(focusIdx, 1, (cateTotalPage > 0 ? 5 : cateData.length));
                buttonFocusRefresh(focusIdx, false);
                return true;
            case KEY_CODE.ENTER:
                if (isContents) {
                    isListFocus = true;
                    buttonFocusRefresh(focusIdx, true);
                }
                return true;
            default:
                return false;
        }
    };

    this.getView = function () {
        return div;
    };

    this.focused = function () {
        div.css({"visibility": "inherit", "display": "block"});

        // 2018.01.31 sw,nam
        // 캐릭터 전용 컨텐츠 뷰 배경 관리
        _$(".kidsSubhome_background").css({display:"block"});
        _$("#bg_kids").show();
        _$("#bg_2d_kids").hide();
        _$("#bg_kids_cloud_1").addClass("moved");
        _$("#bg_kids_cloud_2").addClass("moved");

        dataIdx = 0;
        focusIdx = 0;
        isListFocus = false;
        buttonFocusRefresh(focusIdx, false);
        return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.blurred = function () {
        posterListComp.blurred();
        div.find(".relationBtn_area").addClass("blurred");
        div.css({"visibility": "hidden", "display": "none"});
    };

    this.pause = function () {
        if(posterListComp.hasOwnProperty("pause")) posterListComp.pause();
    };

    this.resume = function () {
        if(posterListComp.hasOwnProperty("resume")) posterListComp.resume();
    };
};

kidsSubHome.view.CharacterTheaterView.prototype = new View();
kidsSubHome.view.CharacterTheaterView.prototype.constructor = kidsSubHome.view.CharacterTheaterView;
kidsSubHome.view.CharacterTheaterView.prototype.type = View.TYPE.CONTENTS;