/**
 * Created by Yun on 2017-06-12.
 */

kidsSubHome.view.KidsMassContentsView = function (viewId) {
    View.call(this, viewId);
    var div = this.div;

    var catData;
    var dataList = [];
    var posterListComp;
    var indicator;
    var isFocused = false;
    var isAdultAuthState = false;
    var originData;

    var sortOption;
    var viewOption;

    var instance = this;
    var contextMenu;
    var catId;
    var sortGb;
    this.adultAuthBox = false;
    this.connerId;

    this.create = function (data) {
        div.attr("class", "contentsView");
        div.append(_$("<div/>", {class: "indicator_area"}));
        div.append(_$("<div/>", {class:"relationBtn_area"}));
        div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).html("찜 | 플레이리스트 추가"));
        div.append(_$("<div/>", {class: "contents_area"}));
        div.append(_$("<div/>", {class: "bottom_div"}));
        div.append(_$("<img/>", {class: "dim_vod_list", src : modulePath + "resource/image/dim_vodlist_btm_kids.png"}));
        div.append(_$("<img/>", {class: "key_shadow", src : modulePath + "resource/image/key_shadow_kids.png"}));

        catId = data.categoryData.id;
        sortGb = data.categoryData.sortGb;
        originData = data;
        if(data.categoryData.catType=="Adult" && !AdultAuthorizedCheck.isAdultAuthorized())
            setAdultAuthArea.call(this, data, function(instance, data) {
                createContentsView.call(instance, data);
                createContextMenu();
            });
        else {
            createContentsView.call(this, data);
            createContextMenu();
        }
    };

    function createContextMenu() {
        contextMenu = new ContextMenu_OnlyVOD();
        div.append(contextMenu.getView());
    }

    function createContentsView(data) {
        if(data.dataList==null) data.dataList = [];
        if(!Array.isArray(data.dataList)) data.dataList = [data.dataList];

        setOverSdwArea(data);
        catData = data.categoryData;
        // var pt = getPosterListType(catData.listType, catData.posterListType);
        var pt = catData.posterListType; // [hw.boo] mass 경우는 포스트로만 노출 
        posterListComp = getPosterListComp(pt);
        posterListComp.create(catId, catData);
        div.find(".contents_area").empty().append(posterListComp.getView());
        indicator = new Indicator(1);

        posterListComp.setIndicator(indicator);
        posterListComp.parent = this;
        dataList = [].concat(data.dataList);
        this.connerId = data.categoryData.connerId;
        setData(dataList, 0, sortGb, dataList.length);
        div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());
    }

    function setAdultAuthArea(data, callback) {
        var that = this;
        that.adultAuthBox = true;
        isAdultAuthState = true;
        posterListComp = new AdultAuthBox();
        posterListComp.create(function () {
            div.find(".adult_auth_area").detach();
            callback.call(that, data);
            isAdultAuthState = false;
            that.viewMgr.reChangeFocus();
        });

        posterListComp.parent = this;
        div.find(".adult_auth_area").append(posterListComp.getView());
    }

    /**
     * 포스터 리스트 타입을 반환한다.
     *
     * @param listType
     * @param posterListType
     * @returns {number}   -1 : 미리보기 포스터가 직사각형인 타이틀
     *                      -2 : 미리보기 포스터가 정사각형인 타이틀
     *                      1 : 직사각형 포스터
     *                      2 : 정사각형 포스터
     */
    function getPosterListType(listType, posterListType) {
        var listMode = StorageManager.ps.load(StorageManager.KEY.VOD_LIST_MODE);
        log.printDbg("getPosterListType() - listMode : " + listMode + ", listType : " + listType + ", posterListType : " + posterListType);
        var type = undefined;
        if (listMode === "poster") { // 포스터 보기
            type = posterListType;
        } else if (listMode === "text" || listMode === "list" || listType == 1) { // 타이틀 보기
            if (parseInt(posterListType) === 2) { // 미리보기 포스터가 정사각형인 경우
                type = -2;
            } else { // 미리보기 포스터가 직사각형인 경우
                type = -1;
            }
        } else {
            type = posterListType;
        }
        log.printDbg("getPosterListType() - result, type : " + type);
        return type;
    }
    
    function getPosterListComp(data) {
        var comp = null;
        switch (parseInt(data)) {
            case -1: 
                viewOption = 1;
                comp = new TitleList(instance, TitleList.TYPE.VERTICAL);// 미리보기 포스터가 직사각형인 경우
                break;
            case -2: 
                viewOption = 1;
                comp = new TitleList(instance, TitleList.TYPE.SQUARE);// 미리보기 포스터가 정사각형인 경우
                break;
            case 2 : 
                viewOption = 0; 
                comp = new SquarePosterList();
                comp.setLineCount(6);
                break;
            default: 
                viewOption = 0;
                comp = new VerticalPosterList();
                comp.setLineCount(7);
                break;
        }
        return comp;
    }
    function setOverSdwArea(data) {
        if(data.categoryData.posterListType!=0) return;
        // var d = div.find(".over_sdw_area");
        // d.append("<div class='over_sdw_1'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        // d.append("<div class='over_sdw_2'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        // d.append("<div class='over_sdw_3'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
    }

    function setData(_dataList, page, _sortGb, itemCnt) {
        posterListComp.setData(_dataList, page, _sortGb, itemCnt);
    }

    function changeFocus(amount) {
        posterListComp.changeFocus(amount);
    }

    function changePage(amount) {
        posterListComp.changePage(amount);
    }

    function setFocus(newFocus) {
        posterListComp.setFocus(newFocus);
    }

    function setPage(newPage){
        posterListComp.setPage(newPage);
    }


    this.onKeyAction = function(keyCode) {
        if(contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        if(posterListComp.onKeyAction(keyCode)) return true;
        else if(keyCode == KEY_CODE.CONTEXT) {
            var data = posterListComp.getFocusedContents();
            if(data) contextMenu.setVodInfo({
                itemName: data.contsName||data.itemName,
                imgUrl: data.imgUrl,
                contsId: data.itemId||data.contsId,
                catId: catId,
                itemType: data.itemType,
                cmbYn: data.cmbYn,
                seriesType: data.seriesType,
                resolCd: data.resolCd,
                assetId: data.assetId,
                prInfo: data.prInfo
            });
            contextMenu.open();
            return true;
        } else return false;
    };

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.removeClass("blurred");
        if(!posterListComp.focused()) return kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        isFocused = true;
        if(this.adultAuthBox) {
            div.addClass("focused");
            return kidsSubHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        } else {
            this.viewMgr.setContextIndicator("찜 | 정렬");
            return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
        }
    };

    this.blurred =  function() {
        isFocused = false;
        div.addClass("blurred");
        div.removeClass("focused");
        this.viewMgr.setContextIndicator("");
    };
};

kidsSubHome.view.KidsMassContentsView.prototype = new View();
kidsSubHome.view.KidsMassContentsView.prototype.constructor = kidsSubHome.view.KidsMassContentsView;
kidsSubHome.view.KidsMassContentsView.prototype.type = View.TYPE.CONTENTS;