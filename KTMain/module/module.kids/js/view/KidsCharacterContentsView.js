/**
 * Created by Yun on 2017-04-26.
 *
 * 케릭터 친구들 -> 케릭터 리스트에서 케릭터 선택시 진입하는 화면
 */
kidsSubHome.view.KidsCharacterContentsView = function(viewId) {
    View.call(this, viewId);
    var div = this.div;

    var catData;
    var dataList = [];
    var posterListComp;
    var indicator;
    var isFocused = false;
    var isAdultAuthState = false;
    var originData;
    var itemCnt;

    var sortOption;
    var filterOption = [];
    var filterOptionData;
    var viewOption;

    var instance = this;
    var contextMenu;
    var catId;
    var type = 0;
    var filterCdList = "", filterOptList = "";
    this.connerId;

    this.create = function(data) {
        div.attr("class", "contentsView characterContentsView");
        div.addClass("blurred");

        // 2018.01.31 sw.nam
        // 캐릭터 전용관 시나리오가 기존 키즈 컨셉하고 일관되지 않아서 따로 배경을 관리함.
        // backgroundTitle 추가
        this.div.html("<div id='kidsBackground'>" +
        "<span id='backgroundTitle'>키즈케릭터컨텐츠</span>"+
        "</div>" +
        "</div>");

        // 2018.01.30 sw.nam
        // 케릭터 children 이 없는 경우에도 default 로 이미지 추가한다.
        div.append(_$("<img/>", {class: "characterDefault_BG", src:window.modulePath + "resource/image/kids_cha_bg.png"}));
        div.append(_$("<img/>", {class: "character_sdw_img", src:modulePath + "resource/image/kids_sdw_cha_1.png"}));
        if (data.w3ListImg) {
            div.append(_$("<img/>", {class: "selectCharacter_div", src: data.w3ListImg}).error(function() {_$(this).attr({src: modulePath + "resource/image/kids_cha_default.png"}).css({width:"182px", height:"184px", left:"213px", top:"156px"})}));
        } else {
            div.append(_$("<img/>", {class: "selectCharacter_div", src: modulePath + "resource/image/kids_cha_default.png"}).css({width:"182px", height:"184px", left:"213px", top:"156px"}));
        }

        div.append(_$("<div/>", {class:"indicator_area"}));
        div.append(_$("<div/>", {class:"relationBtn_area"}));
        div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기")));
        div.append(_$("<div/>", {class:"contents_area"}));
        div.append(_$("<div/>", {class:"bottom_dim"}));
        div.append(_$("<div/>", {class:"over_sdw_area"}));
        div.append(_$("<div/>", {class:"adult_auth_area"}));
        originData = data;
        sortGb = data.categoryData.sortGb;
        itemCnt = originData.dataList.length>0?originData.dataList[0].webCatItemCnt-0:0;

        catId = data.categoryData.id;
        if(data.type=="Adult"||data.type=="Motel") setAdultAuthArea.call(this, data, createContentsView);
        else createContentsView.call(this, data);


        setBgTitleText();
        createContextMenu();
    };

    /**
     * 케릭터 컨텐츠 뷰 좌상단 타이틀 이름 setting
     */
    function setBgTitleText() {
        log.printDbg("setBgTitleText");
        div.find("#backgroundTitle").text(originData.categoryData.name);
    }

    function createContextMenu() {
        filterOptionData = getFilter(catId);
        contextMenu = new ContextMenu_VOD();
        contextMenu.addTitle("정렬옵션");
        contextMenu.addDropBox(["최신순", "별점순", "가나다순"]);

        if(filterOptionData.length>0) {
            contextMenu.addSeparateLine();
            for (var i = 0; i < filterOptionData.length; i++) {
                contextMenu.addTitle(filterOptionData[i].filterText);
                contextMenu.addDropBox(filterOptionData[i].filterOptionTextList);
                filterOption[i] = 0;
            }contextMenu.addSeparateLine();
        }

        viewOption = getPosterListType(catData.listType, catData.posterListType)==-1?1:0;
        contextMenu.addTitle("VOD 리스트 보기 방식");
        contextMenu.addDropBox(["포스터 보기", "타이틀 보기"]);
        contextMenu.addSeparateLine();
        contextMenu.addTitle("키즈모드 설정");
        contextMenu.addButton("키즈모드 ON");

        div.append(contextMenu.getView());
        contextMenu.setEventListener(contextMenuListener)
    }

    function createContentsView(data, posterType) {
        if(data.dataList==null) data.dataList = [];
        if(!Array.isArray(data.dataList)) data.dataList = [data.dataList];

        setOverSdwArea(data);
        catData = data.categoryData;
        var pt = posterType==null?getPosterListType(catData.listType, catData.posterListType):posterType;
        posterListComp = getPosterListComp(pt);
        posterListComp.create(viewId, catData);
        div.find(".contents_area").empty().append(posterListComp.getView());
        indicator = new Indicator(1);

        posterListComp.setIndicator(indicator);
        posterListComp.parent = this;
        dataList = data.dataList;
        itemCnt = data.dataList.length>0?data.dataList[0].webCatItemCnt-0:0;
        this.connerId = data.categoryData.connerId;
        setData(dataList, 0, sortGb, itemCnt);
        div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());
        LayerManager.stopLoading();
    }

    function setAdultAuthArea(data, callback) {
        var that = this;
        isAdultAuthState = true;
        posterListComp = new AdultAuthBox();
        posterListComp.create(function () {
            div.find(".adult_auth_area").detach();
            isAdultAuthState = false;
            callback.call(that, data);
            this.viewMgr.historyBack();
        });

        posterListComp.parent = this;
        div.find(".adult_auth_area").append(posterListComp.getView());
    }

    function getPosterListType(listType, posterListType) {
        switch (StorageManager.ps.load(StorageManager.KEY.VOD_LIST_MODE)) {
            case "poster": return posterListType;
            case "text": case "list": return -1;
            default:
                if(listType==1) return -1;
                else return posterListType;
        }
    }

    function getPosterListComp(data) {
        switch (parseInt(data)) {
            case -1: viewOption = 1; return new TitleList();
            // case 1 : viewOption = 0; return new HorizontalPosterList();
            case 2 : viewOption = 0; return new SquarePosterList();
            default: viewOption = 0; return new VerticalPosterList();
        }
    }

    function setOverSdwArea(data) {
        if(data.categoryData.posterListType!=0) return;
        // var d = div.find(".over_sdw_area");
        // d.append("<div class='over_sdw_1'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        // d.append("<div class='over_sdw_2'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
        // d.append("<div class='over_sdw_3'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>");
    }

    function setData(_dataList, page, _sortGb, itemCnt) {
        posterListComp.setData(_dataList, page, _sortGb, itemCnt);
    }

    function changeFocus(amount) {
        posterListComp.changeFocus(amount);
    }

    function changePage(amount) {
        posterListComp.changePage(amount);
    }

    function setFocus(newFocus) {
        posterListComp.setFocus(newFocus);
    }

    function setPage(newPage){
        posterListComp.setPage(newPage);
    }

    function getFilter(id) {
        var MenuDataManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.MENU_DATA_MANAGER);

        var a = MenuDataManager.searchMenu({
            menuData: MenuDataManager.getMenuData(),
            allSearch: false,
            cbCondition: function (menu) { return menu.id == id; }
        })[0];
        if(!a) return[];

        do{
            var filter = SubHomeDataManager.getFilter(a.id);
            if(filter) return filter;
        }while(a=a.parent);
        return [];
    }

    this.loadAddedContents = function (startIdx, amount, callback, showLoading) {
        if(startIdx>itemCnt-1 || (dataList[startIdx] && dataList[Math.min(startIdx+amount-1, itemCnt-1)])) callback();
        else {
            if(showLoading) LayerManager.startLoading({preventKey: true});
            kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                if(result) {
                    response.itemDetailList = response.itemDetailList ? (Array.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList]) : [];
                    for (var i = 0; i < amount; i++) dataList[startIdx + i] = response.itemDetailList[i];
                    posterListComp.updateData(dataList);
                    if (callback) callback();
                    if (showLoading) LayerManager.stopLoading();
                }else if (showLoading) LayerManager.stopLoading();
            }, catId, startIdx + 1, Math.min(itemCnt-startIdx+1, amount), sortOption, filterCdList.substr(1, filterCdList.length-1), filterOptList.substr(1, filterOptList.length-1));
        }
    };

    function contextMenuListener(index, data) {
        contextMenu.close();
        if (index == 0) {
            if (sortOption == data) return;
            var oldOption = sortOption;
            sortOption = data;
            reloadData(function (res) {
                if(!res) {
                    sortOption = oldOption;
                    contextMenu.setDropBoxFocus(0, oldOption);
                }
            });
        } else if (index<=filterOptionData.length) {
            if(filterOption[index-1]==data) return;
            var oldOption = filterOption[index-1]||0;
            filterOption[index-1] = data;
            reloadData(function (res) {
                if(!res) {
                    filterOption[index-1] = oldOption;
                    contextMenu.setDropBoxFocus(index, oldOption);
                }
            });
        } else if(index == filterOptionData.length+1) {
            if(viewOption == data) return;
            viewOption = data;
            StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, viewOption==0?"poster":"list");
            createContentsView.call(instance, {categoryData: catData, dataList: dataList}, getPosterType());
            posterListComp.focused();
            instance.viewMgr.clearRemovableQueue();
        } contextMenu.close();

        function reloadData(callback) {
            filterCdList = "";
            filterOptList = "";
            for(var i in filterOptionData) {
                if(filterOptionData[i].filterOptionCdList[filterOption[i]]) {
                    filterCdList += "|" + filterOptionData[i].filterCd;
                    filterOptList += "|" + filterOptionData[i].filterOptionCdList[filterOption[i]];
                }
            }
            kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
                if(response.itemDetailList) {
                    createContentsView.call(instance, { categoryData: catData, dataList: response.itemDetailList }, getPosterType());
                    posterListComp.focused();
                    callback(true);
                }else {
                    showToast("선택한 옵션으로 표시할 수 있는 콘텐츠가 없습니다");
                    callback(false);
                }
            }, catId, 1, 15, sortOption, filterCdList.substr(1, filterCdList.length-1), filterOptList.substr(1, filterCdList.length-1));
        }

        function getPosterType() {
            return viewOption==0?catData.posterListType||0:-1
        }
    }

    function clearFilter() {
        if(sortOption!=0) {
            clearImpl();
            return true;
        } for(var i in filterOption) {
            if(filterOption[i]!=0) {
                clearImpl();
                return true;
            }
        }

        function clearImpl() {
            createContentsView.call(instance, originData);
            sortOption = 0;
            filterOption = [];
            filterCdList = "";
            filterOptList = "";
            contextMenu.clearDropBoxFocus();
            contextMenu.setDropBoxFocus(filterOptionData.length+1, viewOption);
        }
    }

    this.onKeyAction = function(keyCode) {
        log.printDbg("onKeyAction");
        if(contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        if(posterListComp.onKeyAction(keyCode)) return true;
        else if(keyCode == KEY_CODE.LEFT) {
            // 2018.01.31 sw.nam
            // 위 if문에서 left 입력 후 false 를 반환하게 되면 기존 포스터 리스트에서 blurred 되는 시나리오이나
            // 키즈 캐릭터 컨텐츠는 단일 layer 로 activate 되어있으므로 layer 를 deactivate 시켜준다.
            LayerManager.historyBack();
            return true;
        }
        else if(keyCode == KEY_CODE.CONTEXT) {
            var data = posterListComp.getFocusedContents();
            contextMenu.setVodInfo({
                itemName: data.contsName||data.itemName,
                imgUrl: data.imgUrl,
                contsId: data.itemId||data.contsId,
                catId: catId,
                itemType: data.itemType,
                cmbYn: data.cmbYn,
                seriesType: data.seriesType,
                resolCd: data.resolCd,
                assetId: data.assetId,
                isKids: true
            });
            contextMenu.open();
            return true;
        } else return false;
    };

    this.show = function () {
        log.printDbg("show()");
    };

    this.hide = function () {
        log.printDbg("hide()");

        div.css("display", "none");
    };

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.css("display", "block");
        div.removeClass("blurred");
        // div.find(".relationBtn_area").addClass("blurred");

        // 2018.01.31 sw,nam
        // 캐릭터 전용 컨텐츠 뷰 배경 관리
        _$(".kidsSubhome_background").css({display:"block"});
        _$("#bg_kids").show();
        _$("#bg_2d_kids").hide();
        _$("#bg_kids_cloud_1").addClass("moved");
        _$("#bg_kids_cloud_2").addClass("moved");

        div.find(".relationBtn_area .relationBtn_text").html((KIDS_MODE_MANAGER.isKidsMode())? "옵션" : "찜 | 정렬");
        if(!posterListComp.focused()) return kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        isFocused = true;
        if (dataList.length > 10) div.find(".bottom_dim").addClass("show");
        else div.find(".bottom_dim").removeClass("show");
        if(isAdultAuthState) return kidsSubHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        else return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.blurred =  function() {
        isFocused = false;
        div.find(".bottom_dim").removeClass("show");

/*        // 2018.01.31 sw,nam
        // 캐릭터 전용 컨텐츠 뷰 배경 관리
        _$("#bg_kids").hide();
        _$("#bg_2d_kids").show();
        _$("#bg_kids_cloud_1").removeClass("moved");
        _$("#bg_kids_cloud_2").removeClass("moved");*/

        if (!clearFilter()) posterListComp.blurred();
        div.addClass("blurred");
        // div.find(".relationBtn_area").removeClass("blurred");
        div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        div.css("display", "none");
        _$("#bg_2d_kids").show();
    };

    this.pause = function () {
        if(posterListComp.hasOwnProperty("pause")) posterListComp.pause();
    };

    this.resume = function () {
        if(posterListComp.hasOwnProperty("resume")) posterListComp.resume();
    };
};
kidsSubHome.view.KidsCharacterContentsView.prototype = new View();
kidsSubHome.view.KidsCharacterContentsView.prototype.constructor = kidsSubHome.view.KidsCharacterContentsView;
kidsSubHome.view.KidsCharacterContentsView.prototype.type = View.TYPE.CONTENTS;