/**
 * Created by Yun on 2017-06-19.
 */

kidsSubHome.view.CharacterListContentsView = function(viewId) {
    View.call(this, viewId);
    var div = this.div;
    
    var catData;
    var dataList = [];
    var characterListComp;
    var indicator;
    var isFocused = false;
    var originData;
    var itemCnt;
    
    var instance = this;
    
    this.create = function (data) {
        div.attr("class", "contentsView kidsCharacterListView");
        div.addClass("blurred");
        div.append(_$("<div/>", {class:"indicator_area"}));
        div.append(_$("<div/>", {class:"relationBtn_area"}));
        div.find(".relationBtn_area").append(_$("<span/>", {class:"relationBtn_text"}).text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기")));
        div.append(_$("<div/>", {class:"contents_area"}));
        div.append(_$("<div/>", {class:"bottom_dim"}));
        div.append(_$("<div/>", {class:"over_sdw_area"}));
        div.append(_$("<div/>", {class:"adult_auth_area"}));
        originData = data.characterListData;
        itemCnt = originData.length;

        catId = data.categoryData.id;
        createContentsView.call(this, data);
    };
    
    function createContentsView(data) {
        if(data.dataList==null) data.dataList = [];
        if(!Array.isArray(data.dataList)) data.dataList = [data.dataList];

        catData = data.categoryData;
        characterListComp = new CharacterIconList();
        characterListComp.create(catId, catData);
        div.find(".contents_area").empty().append(characterListComp.getView());
        indicator = new Indicator(1);

        characterListComp.setIndicator(indicator);
        characterListComp.parent = this;
        dataList = data.characterListData;
        this.connerId = data.categoryData.connerId;
        setData(dataList, 0, "", itemCnt);
        div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());
        LayerManager.stopLoading();
    }

    function setData(_dataList, page, _sortGb, itemCnt) {
        characterListComp.setData(_dataList, page, _sortGb, itemCnt);
    }

    function changeFocus(amount) {
        characterListComp.changeFocus(amount);
    }

    function changePage(amount) {
        characterListComp.changePage(amount);
    }

    function setFocus(newFocus) {
        characterListComp.setFocus(newFocus);
    }

    function setPage(newPage){
        characterListComp.setPage(newPage);
    }

    this.onKeyAction = function(keyCode) {
        log.printDbg("onkeyAction");
        if (characterListComp.onKeyAction(keyCode)) return true;
        else if (keyCode == KEY_CODE.CONTEXT) {
            LayerManager.activateLayer({
                obj: {
                    id: (KIDS_MODE_MANAGER.isKidsMode() ? "KidsModeOffPopup" : "KidsModeSettingPopup"),
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        complete:function() {
                            settingInfoRefresh();
                        }
                    }
                },
                moduleId: "module.kids",
                new: true,
                visible: true
            });
        }
        else return false;
    };

    this.focused = function() {
        div.removeClass("blurred");
        div.find(".relationBtn_area").removeClass("blurred");
        div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        if(!characterListComp.focused()) return kidsSubHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        isFocused = true;
        if (dataList.length > 10) div.find(".bottom_dim").addClass("show");
        else div.find(".bottom_dim").removeClass("show");
        return kidsSubHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.loadAddedContents = function (startIdx, amount, callback, showLoading) {
        if(originData[startIdx] && originData[Math.min(startIdx+amount-1, itemCnt-1)]) callback();
        else {
            // if(showLoading) LayerManager.startLoading({preventKey: true});
        //     kidsSubHome.amocManager.getItemDetlListW3(function (result, response) {
        //         if(result) {
        //             response.itemDetailList = response.itemDetailList ? (Array.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList]) : [];
        //             for (var i = 0; i < amount; i++) originData[startIdx + i] = response.itemDetailList[i];
        //             dataList = originData;
        //             characterListComp.updateData(originData);
                    if (callback) callback();
                    if (showLoading) LayerManager.stopLoading();
        //         }else if (showLoading) LayerManager.stopLoading();
        //     }, catId, startIdx + 1, Math.min(itemCnt, startIdx + amount + 1));
        }
    };

    this.getCurrentContent = function() {
        return characterListComp.getFocusedContents();
    };

    this.blurred =  function() {
        isFocused = false;
        div.find(".bottom_dim").removeClass("show");
        div.find(".contents_area").css("top", "79px");
        div.addClass("blurred");
        characterListComp.blurred();
        div.find(".relationBtn_area").addClass("blurred");
        div.find(".relationBtn_area .relationBtn_text").text("키즈모드 " + (KIDS_MODE_MANAGER.isKidsMode() ? "끄기" : "켜기"));
        //this.viewMgr.setHomeShot(catData.hsTargetType, catData.hsTargetId, catData.w3HsImgUrl, catData.hsLocator, catData.hsLocator2, catData.hsKTCasLocator);
    };

    this.pause = function () {
        if(characterListComp.hasOwnProperty("pause")) characterListComp.pause();
    };

    this.resume = function () {
        if(characterListComp.hasOwnProperty("resume")) characterListComp.resume();
    };
};

kidsSubHome.view.CharacterListContentsView.prototype = new View();
kidsSubHome.view.CharacterListContentsView.prototype.constructor = kidsSubHome.view.CharacterListContentsView;
kidsSubHome.view.CharacterListContentsView.prototype.type = View.TYPE.CONTENTS;