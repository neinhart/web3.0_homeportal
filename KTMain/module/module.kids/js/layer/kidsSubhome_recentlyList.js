/**
 * Created by Yun on 2017-05-26.
 */

(function () {
    kidsSubHome.layer = kidsSubHome.layer || {};
    kidsSubHome.layer.kidsSubhome_recentlyList = function KidsSubhomeRecentlyList(options) {
        Layer.call(this, options);
        var contentList = [];
        var focus = 0;
        var instance = this;
        var indicator;
        var curPage = 0;
        var focusMode = 0;
        var editMode = false;
        var editFocus = 0;

        var kidsCharShot = new kidsSubHome.KidsCharacterShot();

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame kidsHome kidsSubhome_recentlyList"});

            this.div.html("<div class='kidsBackground'>" +
                "<img id='kidsDefaultBg' src='module/module.kids/resource/image/bg_kids.jpg' alt='background' class='clipping'>"+
                "<span id='backgroundTitle'>최근 시청 목록</span>" +
                "<div id='pig_dim'></div>" +
                "<div class='main_clock'/>" +
                "<img id='default_homeshot' class='tv' alt='home_shot'>"+
                "</div>" +
                "<div id='contentsArea'></div>" +
                "<div id='indicatorArea'></div>" +
                "<div class='option_menu_area'><div class='option_menu_select_all btn'>전체 선택</div><div class='option_menu_delete btn'>삭제</div></div>" +
                "<div class='option_key_area'></div>" +
                "<div class='context_menu_indicator'>편집 모드</div>"
            );

            this.div.append(kidsCharShot.getView());
            kidsCharShot.setParent(this);

            indicator = new Indicator(1);
            cbCreate(true);
        };

        this.show = function (options) {
            Layer.prototype.show.call(this);
            kidsCharShot.setKidsCharacterShot();
            if (KIDS_MODE_MANAGER.isKidsMode()) this.div.find(".context_menu_indicator").css("display", "none");
            else this.div.find(".context_menu_indicator").css("display", "block");
            // setTimeout(function () {
                homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            // }, 100);
            IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            RecentlyListManager.getRecentlyData(function (data) {
                contentList = data;
                constructHTML();
                setFocus((options&&options.resume)?focus:0);
            });
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            UTIL.clearAnimation(instance.div.find(".textAnimating span"));
            instance.div.find(".textAnimating").removeClass("textAnimating");
            setFocusMode(0);
        };

        function setFocus(newFocus) {
            if(newFocus>=contentList.length) newFocus = contentList.length-1;
            setFocusMode(0);
            setPage(Math.floor(newFocus / 10));

            instance.div.find("#recently_item_" + focus).removeClass("focus");
            focus = newFocus;
            instance.div.find("#recently_item_" + focus).addClass("focus");

            instance.div.find(".context_menu_indicator").text("편집 모드" + (editMode?" 종료":""));
            setTextAnimation(focus);
        }

        function setTextAnimation(focus) {
            UTIL.clearAnimation(instance.div.find(".textAnimating span"));
            instance.div.find(".textAnimating").removeClass("textAnimating");
            void instance.div[0].offsetWidth;
            if(UTIL.getTextLength(contentList[focus].contsName, "RixHead L", 30)>215) {
                var posterDiv = instance.div.find("#recently_item_" + focus + " .posterTitle");
                posterDiv.addClass("textAnimating");
                UTIL.startTextAnimation({
                    targetBox: posterDiv
                });
            }
        }

        function setEditFocus(newFocus, isRightKey) {
            if(newFocus==1 && instance.div.find(".option_menu_area .btn").hasClass("disable")) {
                setEditFocus(0);
                return;
            }

            if(isRightKey && newFocus==0 && !(instance.div.find(".option_menu_area .btn").hasClass("disable"))) {
                setEditFocus(1);
                return;
            }

            setFocusMode(1);
            instance.div.find(".option_menu_area .btn").eq(editFocus).removeClass('focus');
            editFocus = newFocus;
            instance.div.find(".option_menu_area .btn").eq(editFocus).addClass('focus');
        }

        function setFocusMode(mode) {
            indicator.blurred();
            if(focusMode==mode) return;
            focusMode = mode;
            instance.div.find("#recently_item_" + focus).toggleClass("focus", mode==0);
            instance.div.find(".option_menu_area .btn").eq(editFocus).toggleClass('focus', mode==1);
        }

        function setPage(page) {
            curPage = page;
            instance.div.find("#listArea").css("-webkit-transform", 'translateY(-' + page * 908 + "px)");
            indicator.setPos(page);
        }

        function constructHTML() {
            var html = _$("<div/>", {id: 'listArea'});
            for (var i = 0; i < contentList.length; i++) {
                html.append(getVerticalPoster("recently_item_" + i, contentList[i]));
            }

            indicator.setSize(Math.floor((contentList.length-1)/10)+1);
            instance.div.find("#indicatorArea").append(indicator.getView());
            instance.div.find("#contentsArea").html(html);
            instance.div.toggleClass("singlePage", Math.ceil(contentList.length/10)==1);
        }

        function enterKeyAction() {
            if(focusMode == 0) {
                if(editMode) instance.div.find("#recently_item_" + focus).toggleClass("select");
                else {
                    var catId = contentList[focus].itemType==1?contentList[focus].contsId:contentList[focus].catId;
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, catId, contentList[focus].contsId, "01");
                    } catch(e) {}
                    openDetailLayerImpl(contentList[focus]);
                }
            } else if(editFocus == 0) {
                if(instance.div.find("li.content.select").length==contentList.length) instance.div.find("li.content").removeClass("select");
                else instance.div.find("li.content").addClass("select");
            } else if(editFocus == 1) {
                openConfirmPopup();
            } if(editMode) {
                instance.div.find(".option_menu_select_all").text(instance.div.find("li.content.select").length==contentList.length?"전체 선택 해제":"전체 선택");
                instance.div.find(".option_menu_delete").toggleClass("disable", instance.div.find("li.content.select").length==0);
            }
        }

        var openDetailLayerImpl = function(data) {
            LayerManager.startLoading({preventKey:true});
            if(data.contYn=="N") {
                if (data.catId == "N") {
                    kidsSubHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=V&platformGubun=W", function (res, newData) {
                        if(res) {
                            if (!newData) {
                                LayerManager.stopLoading();
                                HTool.openErrorPopup({message: ERROR_TEXT.ET002});
                            } else if (newData.serviceYn == "Y") {
                                openDetailLayer(newData.catId, data.contsId, "02", "", true);
                            } else {
                                LayerManager.stopLoading();
                                HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                            }
                        } else {
                            LayerManager.stopLoading();
                            extensionAdapter.notifySNMPError("VODE-00013");
                            HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                        }
                    });
                } else openDetailLayer(data.contsId, data.contsId, "02", "", true);
            } else {
                kidsSubHome.amocManager.getContentW3(function (res, contData) {
                    if(res) {
                        if(contData.contsId) openDetailLayer(contData.catId, data.contsId, "02", "", true, {contsInfo:contData});
                        else if(data.catId!=="N") openDetailLayer("N", data.contsId, "02");
                        else {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                        }
                    } else {
                        LayerManager.stopLoading();
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    }
                }, data.catId, data.contsId, DEF.SAID);
            }
        };

        this.onKeyAction = function(keyCode) {
            switch (keyCode) {
                // gyuh 2017-05-21
                case KEY_CODE.LEFT:
                    // if(indicator.isFocused()) return LayerManager.historyBack(), true;

                    if(indicator.isFocused()) {
                        if(editMode) {
                            editMode = false;
                            this.div.removeClass('editMode');
                            setFocus(focus);
                            instance.div.find(".context_menu_indicator").text("편집 모드");
                        }
                        LayerManager.historyBack();
                        return true;
                    }
                    else if(focusMode==0 && focus%5==0){
                        if(contentList.length>10) indicator.focused(), instance.div.find("#recently_item_" + focus).removeClass("focus");
                        else {
                            // return LayerManager.historyBack(), true;
                            if(editMode) {
                                editMode = false;
                                this.div.removeClass('editMode');
                                setFocus(focus);
                                instance.div.find(".context_menu_indicator").text("편집 모드");
                            }
                            LayerManager.historyBack();
                            return true
                        }
                        // gyuh 2017-05-21
                    } else if(editMode && focusMode == 1) setFocus(Math.min(focus - focus%5 + 4, contentList.length-1));
                    else setFocus(HTool.getIndex(focus, -1, contentList.length));
                    return true;
                case KEY_CODE.RIGHT:
                    if(indicator.isFocused()) setFocus(curPage*10), indicator.blurred();
                    else if(editMode && focusMode==1) return true;
                    else if(editMode && (focus==contentList.length-1||focus%5==4)) setEditFocus(editFocus, true);
                    else setFocus(HTool.getIndex(focus, 1, contentList.length));
                    return true;
                case KEY_CODE.UP :
                    if(indicator.isFocused()) setPage(HTool.getIndex(curPage, -1, Math.ceil(contentList.length/10)));
                    else if(editMode && focusMode==1) setEditFocus(editFocus^1);
                    else if(focus%5==focus) setFocus(Math.min(Math.floor(contentList.length/5)*5 + focus%5, contentList.length-1));
                    else setFocus(focus-5);
                    return true;
                case KEY_CODE.DOWN :
                    if(indicator.isFocused()) setPage(HTool.getIndex(curPage, 1, Math.ceil(contentList.length/10)));
                    else if(editMode && focusMode==1) setEditFocus(editFocus^1);
                    else if(Math.floor(focus/5)==Math.floor(contentList.length/5)) setFocus(focus%5);
                    else setFocus(Math.min(focus+5, contentList.length-1));
                    return true;
                case KEY_CODE.ENTER :
                    if (indicator.isFocused()) setFocus(curPage*10), indicator.blurred();
                    else enterKeyAction();
                    return true;
                case KEY_CODE.BACK :
                    if(editMode) {
                        editMode = false;
                        this.div.removeClass('editMode');
                        setFocus(focus);
                        instance.div.find(".context_menu_indicator").text("편집 모드");
                    }else LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    if (KIDS_MODE_MANAGER.isKidsMode()) return;
                    editMode^=true;
                    this.div.toggleClass('editMode', editMode);
                    !editMode?setFocus(focus):instance.div.find("li.content").removeClass("select");
                    instance.div.find(".context_menu_indicator").text("편집 모드" + (editMode?" 종료":""));
                    instance.div.find(".option_menu_delete").addClass("disable");
                    instance.div.find(".option_menu_select_all").text("전체 선택");
                    return true;
                default:
                    return false;
            }
        };

        var getVerticalPoster = function(id, data) {
            var element = _$("<li id='" + id + "' class='content poster_vertical'>" +
                "<div class='content'>" +
                "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>"+
                "<img src='" + data.imgUrl + "' class='posterImg poster'>" +
                "<div class='contentsData'>" +
                "<div class='posterTitle'><span>" + data.contsName + "</span></div>" +
                "<span class='posterDetail'>" +
                "<span class='stars'/>" +
                (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
                "<img src='" + modulePath + "resource/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo-0) + ".png' class='age'>"+
                "</span>" +
                "</div>" +
                "<div class='icon_area'/>" +
                "<div class='poster_remain_bg'></div>" +
                "<div class='poster_remain_bar'></div>" +
                "<div class='edit_select_btn'></div>" +
                "</div>" +
                "</li>");

            element.find(".poster_remain_bar").css("width", Math.min(210, data.linkTime/(data.runtime*60) * 210) + "px");
            setIcon(element.find(".icon_area"), data);
            element.toggleClass("series", data.contYn=="N");
            return element;
        };

        function setIcon(div, data) {
            div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
            switch(data.newHot) {
                case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
                case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
                case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
                case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
                case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
            } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
            } if(data.isHd=="UHD") {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
            } if(data.isDvdYn=="Y") {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
            } if(data.wEvtImageUrlW3) {
                div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
            } if(data.adultOnlyYn!="Y" && UTIL.isLimitAge(data.prInfo)) {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
            }
        }

        var openConfirmPopup = function() {
            var divList = instance.div.find("li.content");
            var deleteList = [];
            var deleteIdxList = [];

            for(var i=0; i<contentList.length; i++){
                if(divList.eq(i).hasClass("select")) {
                    deleteList.push({title: contentList[i].contsName});
                    deleteIdxList.push(i);
                }
            }

            LayerManager.activateLayer({
                obj: {
                    id: "KidsEditModeDeletePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        type: 1,
                        items: deleteList,
                        callback: function (res) {
                            if(res) RecentlyListManager.removeContentsList(deleteIdxList, function (result) {
                                if(result === true) {
                                    RecentlyListManager.getRecentlyData(function (data) {
                                        if(data.length>0) {
                                            showToast("최근 시청 목록에서 삭제되었습니다");
                                            contentList = data;
                                            constructHTML();
                                            setFocus(focus);
                                            editMode = false;
                                            instance.div.toggleClass('editMode', false);
                                        }else {
                                            editMode = false;
                                            instance.div.toggleClass('editMode', false);
                                            RecentlyListManager.setIsKidsRecentlyReLoad(true);
                                            RecentlyListManager.setIsResetting(true);
                                            LayerManager.deactivateLayer({id:instance.id});
                                        }
                                    });
                                } else {

                                }
                            }); LayerManager.historyBack();
                        }
                    }
                },
                moduleId: "module.kids",
                visible: true
            });
        };
    };

    kidsSubHome.layer.kidsSubhome_recentlyList.prototype = new Layer();
    kidsSubHome.layer.kidsSubhome_recentlyList.prototype.constructor = kidsSubHome.layer.kidsSubhome_recentlyList;

    kidsSubHome.layer.kidsSubhome_recentlyList.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    kidsSubHome.layer.kidsSubhome_recentlyList.prototype.handleKeyEvent = function (key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["KidsSubhomeRecentlyList"] = kidsSubHome.layer.kidsSubhome_recentlyList;
})();