/**
 * Created by Yun on 2017-01-09.
 */

(function () {
    kidsSubHome.layer = kidsSubHome.layer || {};
    kidsSubHome.layer.kidsHome_main = function KidsHomeMain(options) {
        Layer.call(this, options);
        var viewMgr;
        var menuData;
        var instance = this;
        // var resizeTimer = null;

        this.isJumpMenu = function () {
            var params = this.getParams();

            if (params && params.jump && params.enterMenu) {
                return true;
            }

            return false;
        };

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame kidsHome kidsHome_main"});
            this.div.html("<div id='kidsBackground'>" +
                    "<span id='backgroundTitle'>키즈</span>"+
                    "</div>" +
                "</div>");

            menuData = this.getParams().data;
            viewMgr = new kidsSubHome.ViewManager();
            this.div.append(viewMgr.getView());
            viewMgr.setParent(this);
            cbCreate(true);
        };

        this.controlKey = function (keyCode) {
            if (window.closeTimeout) {
                clearTimeout(window.closeTimeout);
                window.closeTimeout = null;
            }
            return viewMgr.onKeyAction(keyCode) || (keyCode == KEY_CODE.BACK && (LayerManager.historyBack(), true));
        };

        this.show = function (options) {
            window.isHide = false;
            Layer.prototype.show.apply(this, arguments);
            LayerManager.showVBOBackground("kidsSubHome_main_background");
            /*resizeTimer = setTimeout(function () {
                if (!window.isHide) {
                    homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
                    IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
                }
            }, 500);*/

            if (options && options.resume) {
                viewMgr.resume();
            } else if(this.getParams()) {
                var params = this.getParams();
                viewMgr.destroy();
                //if (params.enterMenu.id == MenuDataManager.MENU_ID.KIDS_PLAY_LIST) {
                //    var tmpPData = params.enterMenu.parent;
                //    viewMgr.setData(tmpPData);
                //    for (var idx = 0 ; idx < tmpPData.children.length ; idx ++) {
                //        if (tmpPData.children[idx].id == MenuDataManager.MENU_ID.KIDS_PLAY_LIST) {
                //            viewMgr.changeCategoryFocus(idx);
                //            viewMgr.pause();
                //            break
                //        }
                //    }
                //} else {
                //    viewMgr.setData(params.data, params.enterMenu);
                //}
                viewMgr.setData(params.data, params.enterMenu);
                if (params.focusIdx) viewMgr.changeCategoryFocus(params.focusIdx);
                else if (params.focusMenu) viewMgr.changeCategoryFocus(params.data.children.indexOf(params.focusMenu));
                else if (params.enterMenu) viewMgr.changeCategoryFocus(findIndex(params.data.children, params.enterMenu), true);
            }

            if (!options || !options.resume) {
                viewMgr.setKidsCharacterShot();
                // this.div.find("#backgroundTitle").text("키즈");
                if (window.KIDS_MODE_MANAGER.isKidsMode()) {
                    if (!window.isAutoExit) return;
                    if (!window.isEPGOpen) {
                        if (window.closeTimeout) {
                            clearTimeout(window.closeTimeout);
                            window.closeTimeout = null;
                        }
                        window.isAutoExit = false;
                        window.closeTimeout = setTimeout(function () {
                            clearTimeout(window.closeTimeout);
                            window.closeTimeout = null;
                            LayerManager.deactivateLayer({
                                id: instance.id
                            });
                        }, 15000);
                    }
                }
            }
            // LayerManager.hideVBOBackground();
        };

        function findIndex(root, target) {
            for(var i=0; i<root.length; i++) {
                if(root[i].id==target.id) return i;
            } return -1;
        }

        this.setCategoryFocusInit = function () {
            viewMgr.changeCategoryFocus(0);
        };

        this.getActivateCategoryId = function () {
            return viewMgr.getCurrentCategoryId();
        };

        this.getActivateFocusCategory = function () {
            return viewMgr.getFocusCategoryInfo();
        };

        this.hide = function (options) {
            window.isHide = true;
            // clearTimeout(resizeTimer);
            // resizeTimer = null;
            if(options && options.pause) viewMgr.pause();
            Layer.prototype.hide.apply(this, arguments);
            window.pipManager.deactivatePipLayer();
            if (!options || !options.pause) viewMgr.destroy();

            LayerManager.hideVBOBackground();
        };


        this.remove = function () {
            viewMgr.destroy();
            Layer.prototype.remove.apply(this, arguments);
        };
    };

    kidsSubHome.layer.kidsHome_main.prototype = new Layer();
    kidsSubHome.layer.kidsHome_main.prototype.constructor = kidsSubHome.layer.kidsHome_main;

    kidsSubHome.layer.kidsHome_main.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    kidsSubHome.layer.kidsHome_main.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsHomeMain"] = kidsSubHome.layer.kidsHome_main;
})();