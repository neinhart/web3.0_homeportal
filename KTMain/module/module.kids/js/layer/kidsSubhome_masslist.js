/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    kidsSubHome.layer = kidsSubHome.layer || {};
    kidsSubHome.layer.kidsSubhome_masslist = function HomeMain(options) {
        Layer.call(this, options);
        var dataList;
        var contentsView;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame kidsHome kidsHome_main"});
            var template = 
                "<div class='kidsSubhome_background'>" +
                "   <div id='bg_kids'></div>" +
                "   <img id='bg_kids_mountain' src='" + modulePath + "resource/image/bg_kids_mountain.png" + "'/>" +
                "   <img id='bg_kids_cloud_1' class='moved' src='" + modulePath + "resource/image/bg_kids_cloud_1.png" + "'/>" +
                "   <img id='bg_kids_cloud_2' class='moved' src='" + modulePath + "resource/image/bg_kids_cloud_2.png" + "'/>" +
                "</div>" +
                "<span id='backgroundTitle'></span>";
            this.div.append(_$(template.trim()));

            var params = this.getParams();
            if(!params || params.length<=0) {
                if(cbCreate) cbCreate(false);
                return;
            }

            dataList = params;
            contentsView = new kidsSubHome.view.KidsMassContentsView("kidsSubHome_masslist");
            contentsView.create({categoryData: {id:"N", listType:"poster", posterListType:dataList[0].imgType},dataList: dataList?(_$.isArray(dataList)?dataList:[dataList]):[]});
            contentsView.viewMgr = { changeBackground: changeBackground, historyBack: LayerManager.historyBack, setContextIndicator:function () {} };
            var div = contentsView.getView();
            this.div.find("#backgroundTitle").text(dataList[0].recomTitle);
            this.div.append(div);

            if(cbCreate) cbCreate(true);
        };

        var changeBackground = function (src, time) {
        };

        this.controlKey = function (keyCode) {
            return contentsView.onKeyAction(keyCode) || ((keyCode == KEY_CODE.BACK||keyCode==KEY_CODE.LEFT) && (LayerManager.historyBack(), true));
        };

        this.show = function () {
            Layer.prototype.show.apply(this, arguments);
            contentsView.focused();
        };

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
        }
    };

    kidsSubHome.layer.kidsSubhome_masslist.prototype = new Layer();
    kidsSubHome.layer.kidsSubhome_masslist.prototype.constructor = kidsSubHome.layer.kidsSubhome_masslist;

    //Override create function
    kidsSubHome.layer.kidsSubhome_masslist.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    kidsSubHome.layer.kidsSubhome_masslist.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["KidsSubHomeMassList"] = kidsSubHome.layer.kidsSubhome_masslist;
})();