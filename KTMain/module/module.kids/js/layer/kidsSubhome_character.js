'use strict';
/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * @fileoverview
 * @author hw.boo on 2018-01-31.
 * @varsion
 * @returns
 */
(function() {
    kidsSubHome.layer = kidsSubHome.layer || {};
    kidsSubHome.layer.kidsSubhome_character = function KidsSubHomeCharacter(options) {
        Layer.call(this, options);

        var view = null;
        var that = this;

        /**
         * [dj.son] [WEBIIIHOME-3752] 키즈 핫키 토글을 처리하기 위해 parameter 로 메뉴 데이터와 bHotKey 플래그를 받은 후 저장
         * - 이후 키즈 핫키 이벤트 발생시 현재 저장중인 메뉴 id 와 키즈 핫키 타겟 메뉴 id 를 비교 후 토글 수행
         * 
         * - 현재 subHome 에서 처리하고 있는 핫키 로직과 동일하게 처리 (bHotKey 여부 상관없이 현재 화면이 키즈 핫키 화면이면 토글 동작하도록 수정)
         */
        var menu = null;
        // var bHotKey = false;

        function checkKidsHotKey (options) {
            log.printDbg("checkKidsHotKey()");

            var bKidsHotKey = false;

            switch (options.keyCode) {
                case KEY_CODE.KIDS_SERVICE_HOT_KEY_1:
                case KEY_CODE.KIDS_SERVICE_HOT_KEY_2:
                case KEY_CODE.KIDS_SERVICE_HOT_KEY_3:
                case KEY_CODE.KIDS_SERVICE_HOT_KEY_4:

                    var kidsHotKeyMenu = MenuDataManager.getKidsHotKeyMenu();
                    var arrChildMenu = null;
                    if (kidsHotKeyMenu && kidsHotKeyMenu.children && kidsHotKeyMenu.children.length > 0) {
                        arrChildMenu = kidsHotKeyMenu.children;
                    }

                    var targetCategoryId = null;

                    if (options.keyCode === KEY_CODE.KIDS_SERVICE_HOT_KEY_1) {
                        targetCategoryId = "10000000000000207860";

                        if (arrChildMenu && arrChildMenu[0]) {
                            targetCategoryId = arrChildMenu[0].hsTargetId;
                        }

                    }
                    else if (options.keyCode === KEY_CODE.KIDS_SERVICE_HOT_KEY_2) {
                        targetCategoryId = "10000000000000110279";

                        if (arrChildMenu && arrChildMenu[1]) {
                            targetCategoryId = arrChildMenu[1].hsTargetId;
                        }
                    }
                    else if (options.keyCode === KEY_CODE.KIDS_SERVICE_HOT_KEY_3) {
                        targetCategoryId = "10000000000000196415";

                        if (arrChildMenu && arrChildMenu[2]) {
                            targetCategoryId = arrChildMenu[2].hsTargetId;
                        }
                    }
                    else if (options.keyCode === KEY_CODE.KIDS_SERVICE_HOT_KEY_4) {
                        targetCategoryId = "10000000000000212969";

                        if (arrChildMenu && arrChildMenu[3]) {
                            targetCategoryId = arrChildMenu[3].hsTargetId;
                        }
                    }

                    if (targetCategoryId && targetCategoryId === options.menu.id) {
                        bKidsHotKey = true;
                    }

                    break;
            }

            return bKidsHotKey;
        }

        this.create = function (cbCreate) {
            Layer.prototype.create.call(this);

            this.div.attr({class: "arrange_frame kidsHome kidsHome_main"});

            if (!view) {
                var params = this.getParams() || {};

                menu = params.menu;
                // bHotKey = params.bHotKey;

                // KidsCharacterTheaterView 또는 KidsCharacterContentsView 생성함
                DataConverter.getView(null, menu, function (v) {
                    view = v;
                    that.div.append(view.getView());
                    cbCreate(true);
                });
            }
        };

        this.show = function (options) {
            Layer.prototype.show.apply(this, arguments);

            if (!options || !options.resume) {
                if (view) {
                    view.focused();
                }
            }
            else {
                if (view) {
                    view.resume();
                }
            }
        };

        this.hide = function (options) {
            Layer.prototype.hide.apply(this, arguments);

            // bHotKey = false;

            if (!options || !options.pause) {
                menu = null;

                if (view) {
                    view.blurred();
                }
            }
            else {
                if (view) {
                    view.pause();
                }
            }
        };

        this.remove = function() {
            Layer.prototype.remove.apply(this, arguments);
            if (view) {
                view.destroy();
            }
            view = null;
        };

        this.handleKeyEvent = function(keyCode) {
            if (view) {
                var consumed = view.onKeyAction(keyCode);

                if (!consumed) {
                    if (menu) {
                        consumed = checkKidsHotKey({
                            keyCode: keyCode,
                            menu: menu
                        });

                        if (consumed) {
                            LayerManager.historyBack();
                        }
                    }
                }

                return consumed;
            }
            return false;
        }
    }

    kidsSubHome.layer.kidsSubhome_character.prototype = new Layer();
    kidsSubHome.layer.kidsSubhome_character.prototype.constructor = kidsSubHome.layer.kidsSubhome_character;
    arrLayer["KidsSubHomeCharacter"] = kidsSubHome.layer.kidsSubhome_character;
})();