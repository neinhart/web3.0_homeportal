/**
 * Created by ksk91_000 on 2017-03-27.
 */
subHome.HTTP = {
    AMOC: {
        HTTP_URL: "http://webui.ktipmedia.co.kr:8080",
        HTTPS_URL: "https://webui.ktipmedia.co.kr"
    },
    RECOMMEND: {
        LIVE_URL: "http://recommend.ktipmedia.co.kr:7002/",
        TEST_URL: "http://125.140.114.151:7002/"
    }
};