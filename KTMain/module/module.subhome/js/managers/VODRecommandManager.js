"use strict";
/**
 * @class 추천서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 *
 */
subHome.VodRecmManager = (function() {
    function getUrl() {
        if (DEF.RELEASE_MODE === CONSTANT.ENV.TEST) {
            return subHome.HTTP.RECOMMEND.TEST_URL;
        }
        else {
            return subHome.HTTP.RECOMMEND.LIVE_URL;
        }
    }

    function ajax(async, timeout, url, postData, /**Function*/callback) {
        return ajaxFactory.createAjax("VOD Recommand Manager", async, "post", "json", url, postData, true, -1, callback, null);
    }

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AJAX request stop!!");
            request.abort();
            request = null;
        }
    }

    var REQ_CODE = CONSTANT.STB_TYPE.ANDROID ? "G" : "E",
        SVC_CODE = CONSTANT.IS_OTS ? "OTS" : "OTV",
        STB_VER = CONSTANT.STB_TYPE.ANDROID ? "1.2" : (CONSTANT.IS_HDR ? "3.2" : "3.1");

    /**
     * 상세보기, 종료 시 추천 VOD
     * "ITEM_VOD2" : "ITEM_VOD", 라이브 VOD 추천
     */
    function _getRelatedContents(recType, recGbCode, said, product_list, consid, catid, hasAdult, itemCnt, callback) {
        log.printDbg("_getRelatedContents !!");
        var api = getUrl() + "wasRCS/recommend";
        var postData = {
            'CONTENT': "VOD",
            'REC_TYPE': recType,
            'REC_GB_CODE': recGbCode,
            'USER_ID': said,
            'PRODUCT_LIST': product_list,
            'REQ_DATE': HTool.getDateStr(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_ID': consid,
            'CAT_ID': catid,
            'CAT_TYPE': hasAdult == 'Y' ? "Adult" : "null",
            'ITEM_CNT': itemCnt,
            'CONS_RATE': "N", //필터링 옵션임. Y가 19세이상 필터링 (무조건 "N"으로 호출)
            'STB_VER': STB_VER
            // 'CONTENT': "VOD",
            // 'REC_TYPE': "ITEM_VOD",
            // 'REC_GB_CODE': "M",
            // 'USER_ID': "19732063690",
            // 'PRODUCT_LIST': "2S50",
            // 'REQ_DATE': HTool.getDateStr(),
            // 'REQ_CODE': "E",
            // 'SVC_CODE': "OTV",
            // 'CONS_ID': "SC200000000000014911",
            // 'CAT_ID': "10000000000000129035",
            // 'CAT_TYPE': "null",
            // 'ITEM_CNT': 21,
            // 'CONS_RATE': "N", //필터링 옵션임. Y가 19세이상 필터링 (무조건 "N"으로 호출)
            // 'STB_VER': "3.0"
        };
        return ajax(true, 0, api, postData, callback);
    }

    function _getCuration(said, rec_type, product_list, hasAdult, itemCnt, callback) {
        log.printDbg("_getCuration !!");

        var api = getUrl() + "wasRCS/curation";
        var postData = {
            'CONTENT': "VOD",
            'REC_TYPE': rec_type,
            'USER_ID': said,
            'PRODUCT_LIST': product_list,
            'REQ_DATE': HTool.getDateStr(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'MENU_MAX_CNT': 10,
            'MENU_MIN_CNT': 10,
            'ITEM_MAX_CNT': itemCnt,
            'ITEM_MIN_CNT': 10,
            'CONS_RATE': hasAdult, //필터링 옵션임. Y가 19세이상 필터링
            'STB_VER': STB_VER,
            'STB_mode': (window.KidsModeManager.isKidsMode() ? "Kids":"Normal")
        };

        return ajax(true, 0, api, postData, callback);
    }

    function _getTopContent(product_list, has_adult, item_cnt, callback) {
        var api = getUrl() + "wasRCS/getTopContent";
        var postData = {
            'CONTENT': "VOD",
            'REC_TYPE': 'TOP_MOVIE',
            'USER_ID': DEF.SAID,
            'PRODUCT_LIST': product_list,
            'REQ_DATE': HTool.getDateStr(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'ITEM_CNT': (item_cnt ? Math.min(item_cnt, 10) : 5), //max 10
            'CONS_RATE': has_adult, //필터링 옵션임. Y가 19세이상 필터링
            'STB_VER': STB_VER
        };

        return ajax(true, 0, api, postData, callback);
    }

    function _getRecommendMenu(recType, menuType, has_adult, callback) {
        var api = getUrl() + "wasRCS/getRecommendMenu";
        var postData = {
            'CONTENT': "VOD",
            'REC_TYPE': recType,
            'MENU_TYPE': menuType,
            'USER_ID': DEF.SAID,
            'PRODUCT_LIST': getPkgCode(),
            'REQ_DATE': HTool.getDateStr(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'MENU_MAX_CNT': 10,
            'MENU_MIN_CNT': 10,
            'ITEM_MAX_CNT': 20,
            'ITEM_MIN_CNT': 5,
            'CONS_RATE': has_adult, //필터링 옵션임. Y가 19세이상 필터링
            'STB_VER': STB_VER
        };

        return ajax(true, 0, api, postData, callback);
    }

    function getPkgCode() {
        var package_code;

        if (KTW.CONSTANT.IS_OTS === true) {
            package_code = "2S55";
        }
        else {
            package_code = "2P01";
        }

        try {
            var cust_env = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CUST_ENV));
            package_code = cust_env && cust_env.pkgCode;
        } catch(e) {
            log.printExec(e);
        }

        return package_code;
    }

    return {
        ajaxStop: _ajaxStop,
        getRelatedContents: _getRelatedContents,
        getCuration: _getCuration,
        getTopContent: _getTopContent,
        getRecommendMenu: _getRecommendMenu
    }

}());
