/**
 * Created by ksk91_000 on 2016-11-30.
 */
subHome.view.AppContentsView = function(viewId, parent) {
    View.call(this, viewId);
    var div = this.div;
    var adultComp;
    var catData;

    this.parent = parent

    this.create = function(data) {
        div.attr("class", "appContentsView");
        div.append(_$("<div/>", {class:"contents_area"}));
        div.append(_$("<div/>", {class:"adult_auth_area"}));

        catData = data;
        if(data.type=="Adult"||data.type=="Motel")
            setAdultAuthArea.call(this, data, createContentsView);
        else
            createContentsView.call(this, data);
    };

    function createContentsView(data) {
        var area = div.find(".contents_area");
        // var param = ((catData.id === MenuDataManager.MENU_ID.YOUTUBE || catData.id === MenuDataManager.MENU_ID.YOUTUBE_KIDS)?"":"?w=402&h=450&quality=90");
        area.append(_$("<img/>", {src: data.imgUrl, onerror:"this.src='" + modulePath + "resource/image/default_app_rec.png'"}));
        area.append(_$("<div/>", {class:"btn_go"}).text("바로가기"));
    }

    function setAdultAuthArea(data, callback) {
        var that = this;
        adultComp = new AdultAuthBox();
        adultComp.create(function () {
            div.find(".adult_auth_area").detach();
            adultComp = null;
            callback.call(that, data);
            this.viewMgr.historyBack();
        });

        adultComp.parent = this;
        div.find(".adult_auth_area").append(adultComp.getView());
    }

    function setData(_dataList, page) {
    }

    function changeFocus(amount) {
    }

    function changePage(amount) {
    }

    function setFocus(newFocus) {
    }

    function setPage(newPage){
    }

    this.onKeyAction = function(keyCode) {
        if(keyCode==KEY_CODE.LEFT) {
            this.viewMgr.historyBack();
            return true;
        }else if(keyCode==KEY_CODE.ENTER) {
            goApplication();
            return true;
        }else if(keyCode==KEY_CODE.CONTEXT) {
            return true;
        }
    };

    function goApplication() {
        try {
            NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                catData.parent.id, catData.id, "", catData.locator);
        } catch(e) {}

        if(catData.id == MenuDataManager.MENU_ID.TV_INTERNET) {
            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: CONSTANT.APP_TYPE.WEB_BROWSER,
                    param: catData.locator
                },
                visible: true
            });
        }else if (catData.id === MenuDataManager.MENU_ID.SKY_TOUCH) {
            // 2. 스카이 플러스 채널 가져오기
            var skyPlusChannel = oipfAdapter.navAdapter.getSkyPlusChannel();

            // 채널 튠
            var channelController = oipfAdapter.navAdapter.getChannelControl("main", true);
            channelController.changeChannel(skyPlusChannel);
        } else if(catData.id == MenuDataManager.MENU_ID.YOUTUBE) {
            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: CONSTANT.APP_TYPE.YOUTUBE,
                    param: catData.locator
                },
                visible: true
            });
        }else if(catData.id == MenuDataManager.MENU_ID.YOUTUBE_KIDS) {
            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: CONSTANT.APP_TYPE.YOUTUBE,
                    param: catData.locator + "|KIDS"
                },
                visible: true
            });
        }else if(catData.locator==CONSTANT.APP_ID.APPSTORE){
            var nextServiceState;
            if(StateManager.isTVViewingState()) nextServiceState = CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            else if(StateManager.isVODPlayingState()) nextServiceState = CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD;
            else nextServiceState = CONSTANT.SERVICE_STATE.OTHER_APP;

            AppServiceManager.changeService({
                nextServiceState: nextServiceState,
                obj: {
                    type:CONSTANT.APP_TYPE.MULTICAST,
                    param: CONSTANT.APP_ID.APPSTORE
                },
                visible:true
            });
        }else switch (catData.itemType-0) {
            case 3:    //멀티캐스트 양방향 서비스
                if(!catData.locator) return;

                var nextState;
                var obj = {type:CONSTANT.APP_TYPE.MULTICAST};
                if(catData.locator.indexOf(".")>=0) {
                    if(catData.locator === CONSTANT.APP_ID.MASHUP) nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                    else nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
                    obj.param = catData.locator;
                    obj.ex_param = catData.parameter;
                }else {
                    obj.param = CONSTANT.APP_ID.MASHUP;
                    obj.ex_param = catData.locator;
                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                }

                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: obj,
                    visible:true
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: catData.id
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(catData.locator);
                break;
        }
    }

    this.getView = function() {
        return div;
    };

    this.focused = function() {
        div.removeClass("blurred");
        return subHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
    };

    this.blurred =  function() {
        div.addClass("blurred");
    };
};
subHome.view.AppContentsView.prototype = new View();
subHome.view.AppContentsView.prototype.constructor = subHome.view.AppContentsView;
subHome.view.AppContentsView.prototype.type = View.TYPE.CONTENTS;