/**
 * Created by ksk91_000 on 2016-06-30.
 */
subHome.view.categoryView = function(viewId, parent) {
    View.call(this, viewId);

    var cateInfo;
    var categories;
    var isFocused = false;

    var focusIndex = 0;
    var pageIndex = 0;

    var pages = [];

    var div = this.div;
    var language;

    var _this = this;
    var focusedElement = null;

    this.parent = parent

    this.create = function(data) {
        cateInfo = data;
        categories = filteringByCateType(data.children);
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");

        (function init() {
            focusIndex = 0;
            pageIndex = 0;
            var html = "";
            // 카테고리 정보를 가지고 페이지 단위로 정보를 미리 생성
            makePages();

            // to make category area
            html += "<div id='menuArea' class='menuArea'>" +
                        makeCategoryItems(pages[pageIndex]) +
                    "</div>";

            // to make focus area
            html += "<div id='focusArea'>" +
                        makeFocusArea(pages[pageIndex][focusIndex]) +
                    "</div>";

            // to make page move guide
            if(categories.length > 9) {
                html += "<div class='category_bottom_arw'/>" +
                        "<div class='lr_arw_area'>" +
                            "<img src='" + modulePath + "resource/image/icon_pageup.png'>" +
                            "<img src='" + modulePath + "resource/image/icon_pagedown.png'>" +
                            "<span id='pageIndex'>00 / 00</span>" +
                        "</div>";
            }
            html+='<div class="leftArrow"/>';

            div.attr({class:"categoryArea"});
            div.html(html);
            div.append(_$("<div/>", {class:"previewArea"}));
            div.append(_$("<div/>", {class:"noticeArea"}));

            makePreviewArea();
            updateIndex();
            setFocusArrowVisibility(!!categories[0].w3ListImgUrl);      // 첫번쨰 항목이 텍스트 인 경우에만 화살표 출력
        })();
    };

    function makePages() {
        var page = [], size = 0;
        for(var i = 0; i < categories.length; i++) {
            // [sjoh] 사이즈가 9보다 큰경우가 되면 속성값을 변경해야 하므로 원본 데이터를 prototype 으로 가지는 새 오브젝트를
            // 생성하여 관리하도록 한다.
            var item = Object.create(categories[i]);
            if(size + Number.parseInt(item.w3MenuImgType || "01") > 9) {
                item.w3ListImgUrl = "";
                item.w3MenuImgType = "01";
            }
            page.push(item);
            size += Number.parseInt(item.w3MenuImgType || "01");
            if(size === 9) {
                pages.push(page);
                page = [];
                size = 0;
            }
        }
        if(page.length > 0)
            pages.push(page);
    }

    function makeFocusArea(data) {
        // [sjoh] 부팅 후 첫 진입시 첫번째 항목이 이미지 타입인경우 이미지를 비동기로 다운로드 하게 되므로
        // 높이정보를 알 수 없기 때문에 초기 포커스 높이를 설정한다.
        var itemType = data.menuImgUrl ? 'img' : 'text';
        var heightOffset = 22;
        var height = ((Number.parseInt(data.w3MenuImgType) || 1) * 94 - heightOffset) + "px";
        return  "<div class='textArea'>"+
                    "<div id='focus_border' style='height:" + height + "'>" +
                    "</div>" +
                    "<div class='rightArrow'>" +
                    "</div>" +
                "</div>";
    }

    function makeCategoryItems(page) {
        var html = "";

        for(var i = 0; i < page.length; i++) {
            var item = page[i];
            var itemType = item.w3ListImgUrl ? 'img' : 'text';

            if (itemType === 'text') {
                html += "<div class='text_item' index='" + i + "' data-voiceable-action='' data-voiceable-context='"+ parent.id +"'>" +
                    "<span>" + getTitle(i) + "</span>" +
                    "</div>";
            }
            else {
                html += "<div class='image_item size" + (item.w3MenuImgType || "01") + "'>" +
                            "<img src='" + item.w3ListImgUrl + "'>" +
                        "</div>";
            }
        }

        return html;
    }

    function makePreviewArea() {
        var area = div.find(".previewArea");
        for(var i = 0, size = 0; i < categories.length && i < 9 && size < 9; i++) {
            area.append(getCategoryPreview(categories[i]));
            size += Number.parseInt(pages[pageIndex][i].w3MenuImgType) || 1;
        }
    }

    function getTitle(index) {
        var englishName = "", name = "";
        if(index instanceof window.parent.VItem) {
            englishName = index.englishItemName || index.name;
            name = index.name;
        } else {
            englishName = pages[pageIndex][index].englishItemName || pages[pageIndex][index].name;
            name = pages[pageIndex][index].name;
        }
        return (language === "eng") ? englishName : name;
    }

    function getCategoryPreview(data) {
        var item = _$("<span/>", {class:"preview_item"});
        item.addClass("size" + (data.w3MenuImgType || "01"));
        var itemType = data.w3ListImgUrl ? 'img' : 'text';
        if(itemType === 'text')
            item.text(getTitle(data));
        else
            item.html("<img src='" + data.w3ListImgUrl + "'>");
        return item;
    }

    function filteringByCateType(dataList) {
        if(oipfAdapter.basicAdapter.getConfigText(oipfDef.CONFIG.KEY.ADULT_MENU_DISPLAY)=="true") return dataList;

        var list = [];
        for(var i = 0; i < dataList.length; i++) {
            if(isUsedCatType(dataList[i].catType))
                list.push(dataList[i]);
        }
        return list;

        function isUsedCatType(catType) {
            return catType!="Adult";
        }
    }

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");
        setNoticeAnimation();
    };

    this.setPage = function (newPage, forced) {
        if(!forced && pageIndex === newPage)
            return;

        pageIndex = newPage;

        var menuArea = this.div.find('#menuArea');
        // to add new category items
        menuArea.html(makeCategoryItems(pages[pageIndex]))
    };

    this.setFocus = function(newFocus) {
        // 한 화면에 category view가 2개 존재하는데, display 되지 않는 항목의 focus 를 처리 할 필요는 없음
        if(this.div.css("display") === "none")
            return;

        focusIndex = newFocus;

        // 점프로 메뉴 진입 시 포커스 인덱스 조정
        UTIL.getNewCategoryIndex(pages, pageIndex, focusIndex, function (newFocusIdx, newPageIdx) {
            focusIndex = newFocusIdx;
            pageIndex = newPageIdx;
            _this.setPage(pageIndex, true);
        });

        // 포커스 아이템 변경
        var itemType = pages[pageIndex][focusIndex].w3ListImgUrl ? 'img' : 'text';
        if (focusedElement) {
            focusedElement.removeClass('focus');
            clearFocusAnimation(focusedElement);
        }
        focusedElement = this.div.find('#menuArea div:eq('+focusIndex+')');
        focusedElement.addClass('focus');
        if (itemType === 'text') {
            setFocusAnimation(focusedElement);
        }

        // 포커스 바 위치 수정
        var focusArea = this.div.find('#focusArea');

        // [si.mun] DOM 에 categoryView가 append 되기 전엔
        // focusedElement.position().top 의 값은 0이 반환된다.
        // 때문에, 아래와 같이 top position 을 계산하는 로직을 삽입
        var top = 0;
        for(var i = 0; i < focusIndex; i++)
            top += parseInt(pages[pageIndex][i].w3MenuImgType || 1) * 94;

        focusArea.css("-webkit-transform", "translateY(" + top  + "px)");

        var heightOffset = 12;
        setFocusArrowVisibility(itemType === 'img');
        focusArea.find("#focus_border").css("height", (focusedElement.height() - heightOffset) + "px");
        setFocusImpl.apply(this);

        updateIndex(this);
    };

    function setFocusArrowVisibility(isVisible) {
        var arrow = div.find('#focusArea').find(".rightArrow")[0];
        arrow.hidden = isVisible;
    }

    this.changeFocus = function(amount) {
        if (amount === 0)
            return;

        if (categories.length <= 1)
            return;

        focusIndex += amount;

        if(focusIndex < 0) {
            moveToPreviousPage();
            focusIndex = pages[pageIndex].length - 1;
        } else if(focusIndex >= pages[pageIndex].length) {
            moveToNextPage();
            focusIndex = 0;
        }
        this.setFocus(focusIndex);
    };

    function moveToNextPage() {
        var newPage = pageIndex;
        if(++newPage >= pages.length)
            newPage = 0;
        _this.setPage(newPage);
    }

    function moveToPreviousPage() {
        var newPage = pageIndex;
        if(--newPage < 0)
            newPage = pages.length - 1;
        _this.setPage(newPage);
    }

    function updateIndex() {
        var index = '00' + (pageIndex + 1);
        var pageCount = '00' + (pages.length);
        div.find('#pageIndex').text(index.slice(-2) + ' / ' + pageCount.slice(-2));
    }

    function setFocusImpl() {
        var item = pages[pageIndex][focusIndex];
        if(item.notice)
            div.find(".noticeArea").html("<span>" + item.notice + "</span>");
        else
            div.find(".noticeArea").empty();

        /**
         * [dj.son] 성인인증 걸렸을 경우, 홈샷 미노출
         */
        if (isFocused) {
            if (item.catType=="Adult" && !AdultAuthorizedCheck.isAdultAuthorized()) {
                this.viewMgr.setHomeShot();
            }
            else {
                //this.viewMgr.setHomeShot({view: _this, menu: item});
                //this.viewMgr.setHomeShot(item.hsTargetType, item.hsTargetId, item.w3HsImgUrl, item.hsLocator, item.hsLocator2, item.hsKTCasLocator);
            }
        }

        setNoticeAnimation();
    }

    function clearNoticeAnimation() {
        UTIL.clearAnimation(div.find(".noticeArea"));
        div.find(".noticeArea.textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
    }

    function setNoticeAnimation() {
        if (UTIL.getTextLength(pages[pageIndex][focusIndex].notice, "RixHead L", 30, -1.5) > 1770) {
            var d = div.find(".noticeArea");
            d.addClass("textAnimating");
            UTIL.startTextAnimation({ targetBox: d });
        }
    }

    function clearFocusAnimation(element) {
        if (!element || !element.hasClass("textAnimating")) {
            return;
        }
        UTIL.clearAnimation(element.find("span"));
        element.removeClass("textAnimating");
        void element[0].offsetWidth;
    }

    function setFocusAnimation(element) {
        if (!element) {
            return;
        }
        if(UTIL.getTextLength(element.find("span").text(), "RixHead B", 42, -2.1) > 389) {
            element.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: element
            });
        }
    }

    function getCurrentContent() {
        return pages[pageIndex][focusIndex];
    }

    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return false;

            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return false;

            case KEY_CODE.RED :
                if(focusIndex === 0) {
                    moveToPreviousPage();
                }
                this.setFocus(0);
                return false;

            case KEY_CODE.BLUE :
                if(focusIndex === pages[pageIndex].length - 1) {
                    moveToNextPage();
                }
                this.setFocus(pages[pageIndex].length - 1)
                return false;

            case KEY_CODE.CONTEXT:
                return true;
        }
    };

    this.getCateInfo = function (key) {
        if(key === null)
            return cateInfo;
        else
            return cateInfo[key];
    };

    this.resume = function () {
        this.viewMgr.setCurrentContentData(getCurrentContent());
        this.focused();
        this.setFocus(focusIndex);
    };

    this.pause = function () {
        clearFocusAnimation(focusedElement);
        clearNoticeAnimation();
    };

    this.unfocused = function () {
        this.setFocus(focusIndex);
        div.addClass("unFocused");
        clearFocusAnimation(focusedElement);
        clearNoticeAnimation();
    };

    this.focused = function() {
        var item = pages[pageIndex][focusIndex];

        /**
         * [dj.son] 성인인증 걸렸을 경우, 홈샷 미노출
         */
        if (item.catType=="Adult" && !AdultAuthorizedCheck.isAdultAuthorized()) {
            this.viewMgr.setHomeShot();
        } else {
            //this.viewMgr.setHomeShot({view: _this, menu: item});
            //this.viewMgr.setHomeShot(item.hsTargetType, item.hsTargetId, item.w3HsImgUrl, item.hsLocator, item.hsLocator2, item.hsKTCasLocator);
        }

        isFocused = true;
        div.removeClass("blurred unFocused focused");
        this.setFocus(focusIndex);
        setNoticeAnimation();
        return subHome.ViewManager.FOCUS_MODE.FOCUS;
    };

    this.blurred = function() {
        isFocused = false;
        this.setFocus(0);
        View.prototype.blurred.apply(this, arguments);
        clearFocusAnimation(focusedElement);
        clearNoticeAnimation();
    };

    this.hide = function () {
        this.setFocus(focusIndex);
        View.prototype.hide.apply(this, arguments);
        clearFocusAnimation(focusedElement);
        clearNoticeAnimation();
    };

    this.pause = function () {
        this.setFocus(focusIndex);
        clearFocusAnimation(focusedElement);
        clearNoticeAnimation();
    };

    this.getCurrentContent = getCurrentContent;

    /**
     * 홈샷 노출을 위한 메뉴 데이터 비교용
     */
    this.getCategoryData = function () {
        return categories;
    };

    this.onNodeMatchListener = function (element) {
        log.printDbg("onNodeMatchListener()");

        var consumed = false;

        if (this.div.css("display") !== "none") {
            var el = _$(element);

            if (el.hasClass("text_item")) {
                if (this.viewMgr.getCurrentMode() === 1) {
                    this.viewMgr.historyBack();
                }

                focusIndex = Number(el.attr("index"));

                this.setFocus(focusIndex);

                this.viewMgr.changeContentView();
                // this.viewMgr.contensChangeImmediately();

                consumed = true;
            }
        }

        return consumed;
    };
};

subHome.view.categoryView.prototype = new View();
subHome.view.categoryView.prototype.constructor = subHome.view.categoryView;
subHome.view.categoryView.prototype.type = View.TYPE.CATEGORY;