/**
 * Created by ksk91_000 on 2016-06-30.
 */
subHome.view.recomCategoryView = function(viewId, parent) {
    View.call(this, viewId);

    var cateInfo;
    var categories;
    var topLength;
    var botLength;

    var focus = 0;
    var div = this.div;
    var adultFocus = false;

    this.parent = parent

    this.create = function(data) {
        cateInfo = data;
        categories = filteringByCateType(data.children);
        topLength = 0;//Math.min(2, Math.floor((categories.length-1)/2));
        botLength = Math.min(8, (categories.length-(topLength+1)));

        var that = this;
        (function init() {
            focus = getDefaultFocus();
            var html = "", i;
            html+="<div id='topArea' class='menuArea'>";
            html+="<span class='initialHiddenItem'>" + categories[HTool.getIndex(focus, -1-topLength, categories.length)].name + "</span>";
            for(i=0; i<topLength; i++) html+="<span>" + categories[HTool.getIndex(focus, i-topLength, categories.length)].name + "</span>";
            html+="</div>"+
                "<div id='focusArea'>" +
                "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>" +
                "<div class='page_text'/>"+
                "<div class='textArea'>"+
                "<span id='cate_focus_top'>" + categories[focus].name + "</span>"+
                "<span id='cate_focus_bottom'></span>"+
                "<div class='rightArrow'></div>" +
                "</div>"+
                "</div>"+
                "<div id='bottomArea' class='menuArea'>";
            for(i=0; i<botLength; i++)
                html+="<span>" + categories[HTool.getIndex(focus, i+1, categories.length)].name + "</span>";
            html+="<span class='initialHiddenItem'>" + categories[HTool.getIndex(focus, botLength+1, categories.length)] + "</span>";
            html+="</div>";
            html+="<span id='measureArea'></span>";

            div.attr({class:"categoryArea"});
            div.html(html);
            div.append(_$("<div/>", {class:"previewArea"}));
            div.append(_$("<div/>", {class:"noticeArea"}));
            makePreviewArea.call(that, data);

            setFocusWidth(categories[focus].name);
            div.find(".page_text").html("<b>" + (focus+1) + "</b>" + "/" + categories.length).toggle(categories.length>9);
            div.toggleClass("noAnimation", !!CONSTANT.STB_TYPE.ANDROID);
        })();
    };

    function getDefaultFocus() {
        for(var i=0; i<categories.length; i++) {
            if(categories[i].catType=="SubHome") return i
        } return 0;
    }

    function makePreviewArea(data) {
        var area = div.find(".previewArea");

        if((data.catType=="Adult"||data.catType=="Motel") && !AdultAuthorizedCheck.isAdultAuthorized()){
            area.append(_$("<div/>", {class:"adult_auth_area"}));
            setAdultAuthArea.call(this, makePreview);
        } else makePreview();

        function makePreview() {
            for(var i=0; i<categories.length&&i<7; i++) {
                area.append(getCategoryPreview(categories[i]));
            }
        }
    }

    function setAdultAuthArea(callback) {
        var that = this;
        that.adultAuthBox = new AdultAuthBox();
        that.adultAuthBox.create(function () {
            div.find(".adult_auth_area").detach();
            that.focused();
            callback();
            adultFocus = false;
            that.viewMgr.reChangeFocus();
        });
        adultFocus = true;
        that.adultAuthBox.parent = this;
        div.find(".adult_auth_area").append(that.adultAuthBox.getView());
    }

    function getCategoryPreview(data) {
        var cate = _$("<div/>", {class:"category_item"});
        var tmp = _$("<div/>", {class:"bg_area"});
        tmp.append(_$("<div/>", {class:"bg_top"}));
        tmp.append(_$("<div/>", {class:"bg_mid"}));
        tmp.append(_$("<div/>", {class:"bg_bottom"}));
        cate.append(tmp);

        tmp = _$("<div/>", {class:"title_area"});
        cate.append(tmp);

        tmp.append(_$("<span/>", {class:"title"}).text(data.name));
        if(data.children) {
            var subtitle = data.children[0].name;
            for(var i=1; i<data.children.length; i++) subtitle += " | " + data.children[i].name;
            tmp.append(_$("<span/>", {class:"sub_title"}).text(subtitle));
        } else {
            cate.append(_$("<div/>", {class:"poster_area"}).append(_$("<div/>", {class:"poster_wrapper"})));
            tmp = cate.find(".poster_wrapper");
            tmp.append(_$("<img>", {src:data.previewImgUrl1}));
            tmp.append(_$("<img>", {src:data.previewImgUrl2}));
            tmp.append(_$("<img>", {src:data.previewImgUrl3}));
        }

        tmp = null;
        return cate;
    }

    function filteringByCateType(dataList) {
        if(oipfAdapter.basicAdapter.getConfigText(oipfDef.CONFIG.KEY.ADULT_MENU_DISPLAY)=="true") return dataList;

        var list = [];
        for(var i=0; i<dataList.length; i++) {
            if(isUsedCatType(dataList[i].catType)) list.push(dataList[i]);
        }
        return list;

        function isUsedCatType(catType) {
            return catType!="Adult";
        }
    }

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        div.find('#focusArea').css("transition", "none");
        setFocusWidth(categories[focus].name);
        void div.find('#focusArea')[0].offsetWidth;
        div.find('#focusArea').css("transition", "");
    };

    this.setFocus = function(newFocus) {
        focus = newFocus;
        this.div.find('*').removeClass('slide_up slide_down fade_in fade_out');

        var i;
        var topChild = this.div.find('#topArea span');
        for(i=-1; i<topLength; i++)
            topChild[i+1].innerText = categories[HTool.getIndex(focus, i-topLength, categories.length)].name;
        this.div.find('#focusArea span').text(categories[focus].name);

        var botChild = this.div.find('#bottomArea span');
        for(i=0; i<botLength+1; i++)
            botChild[i].innerText = categories[HTool.getIndex(focus, i+1, categories.length)].name;

        setFocusWidth(categories[focus].name);
        setFocusImpl.apply(this);
    };

    this.changeFocus = function(amount) {
        if (amount == 0) return;

        focus = HTool.getIndex(focus, amount, categories.length);

        // 기존 애니메이션 관련 클래스 전부 제거.
        this.div.find('*').removeClass('slide_up slide_down fade_in fade_out');
        void this.div.find('#focusArea span')[0].offsetWidth;

        var i;

        // 상단영역
        if (topLength > 0) {
            this.div.find('#topArea').addClass('slide_' + (amount > 0 ? "up" : "down"));
            var topChild = this.div.find('#topArea span');
            for (i = 0; i < topLength + 1; i++) topChild.eq(i).text(categories[HTool.getIndex(focus, i - topLength - (amount > 0 ? 1 : 0), categories.length)].name);
            this.div.find("#topArea span").eq(topLength).addClass('fade_' + (amount > 0 ? "in" : "out"));
            this.div.find("#topArea span").eq(0).addClass('fade_' + (amount > 0 ? "out" : "in"));
        }

        //포커스 영역
        this.div.find('#focusArea span').addClass('slide_' + (amount>0?"up":"down"));
        this.div.find('#focusArea #cate_focus_top').text(categories[HTool.getIndex(focus, amount>0?-1:0, categories.length)].name).addClass('fade_' + (amount>0?"out":"in"));
        this.div.find('#focusArea #cate_focus_bottom').text(categories[HTool.getIndex(focus, amount>0?0:1, categories.length)].name).addClass('fade_' + (amount>0?"in":"out"));

        // 하단 영역
        if(botLength>0) {
            this.div.find('#bottomArea').addClass('slide_' + (amount > 0 ? "up" : "down"));
            var botChild = this.div.find('#bottomArea span');
            for (i = 0; i < botLength + 1; i++) botChild.eq(i).text(categories[HTool.getIndex(focus, i + (amount > 0 ? 0 : 1), categories.length)].name);
            this.div.find("#bottomArea span").eq(0).addClass('fade_' + (amount > 0 ? "out" : "in"));
            this.div.find("#bottomArea span").eq(botLength).addClass('fade_' + (amount > 0 ? "in" : "out"));
        }

        setFocusWidth(categories[focus].name);
        setFocusImpl.apply(this);
    };

    function setFocusImpl() {
        div.find(".page_text").html("<b>" + (focus+1) + "</b>" + "/" + categories.length).toggle(categories.length>9);
        div.find(".noticeArea").text(categories[focus].notice);
        //this.viewMgr.setHomeShot({view: this, menu: categories[focus]});
        //this.viewMgr.setHomeShot(categories[focus].hsTargetType, categories[focus].hsTargetId, categories[focus].w3HsImgUrl, categories[focus].hsLocator, categories[focus].hsLocator2, categories[focus].hsKTCasLocator);
    }

    function setFocusWidth(text) {
        div.find("#measureArea").text(text);
        div.find('#focusArea').css("width", Math.max(330, div.find("#measureArea")[0].offsetWidth + 66) + "px");
        div.find('#focusArea .sdw_mid').css("width", Math.max(155, div.find("#measureArea")[0].offsetWidth -109) + "px");
    }

    function getCurrentContent() {
        return categories[focus];
    }

    this.onKeyAction = function(keyCode) {
        if(adultFocus) return this.adultAuthBox.onKeyAction(keyCode);
        else switch(keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return false;
            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return false;
            case KEY_CODE.RED :
                if(focus%9 === 0) {
                    if(categories.length>9) {
                        var newPage = HTool.getIndex(page, -1, Math.ceil(categories.length / 9));
                        this.setFocus(newPage * 9);
                    } else return true;
                } else {
                    this.setFocus(page*9);
                } return false;
            case KEY_CODE.BLUE :
                if(focus === categories.length-1 || focus%9 === 8) {
                    if(categories.length>9) {
                        var newPage = HTool.getIndex(page, 1, Math.ceil(categories.length / 9));
                        this.setFocus(Math.min(categories.length - 1, newPage * 9 + 8));
                    } else return true;
                } else {
                    this.setFocus(Math.min(categories.length-1, page * 9 + 8));
                } return false;
            case KEY_CODE.CONTEXT:
                return true;
        }
    };

    this.unfocused = function () {
        div.addClass("unFocused");
    };

    this.focused = function() {
        //this.viewMgr.setHomeShot({view: this, menu: categories[focus]});
        //this.viewMgr.setHomeShot(categories[focus].hsTargetType, categories[focus].hsTargetId, categories[focus].w3HsImgUrl, categories[focus].hsLocator, categories[focus].hsLocator2, categories[focus].hsKTCasLocator);
        if(adultFocus) {
            div.addClass("focused");
            return this.adultAuthBox.focused();
        } else {
            div.removeClass("blurred unFocused focused");
            return subHome.ViewManager.FOCUS_MODE.FOCUS;
        }
    };

    this.blurred = function() {
        div.removeClass("focused");
        View.prototype.blurred.apply(this, arguments);
    };
    
    this.hide = function () {
        this.setFocus(focus);
        View.prototype.hide.apply(this, arguments);
        div.find('#focusArea').css("transition", "");
    };

    this.getCurrentContent = getCurrentContent;
};

subHome.view.recomCategoryView.prototype = new View();
subHome.view.recomCategoryView.prototype.constructor = subHome.view.recomCategoryView;
subHome.view.recomCategoryView.prototype.type = View.TYPE.CATEGORY;