/**
 * Created by ksk91_000 on 2016-11-30.
 */
subHome.view.SubHomeView = function(viewId, parent) {
    View.call(this, viewId);
    var div = this.div;

    var id;
    var curIdx = 0, inPageIdx = 0;
    var curPage = 0;
    var compList = [];
    var subHomeData;
    var pageInfo = [{length: 0, start: 0, end: 30}];

    var indicator;
    var homeShotIndex = 0;
    var homeShotInterval;
    var instance = this;
    this.contextMenu;

    this.create = function(data) {
        id = data.id;
        subHomeData = SubHomeDataManager.getSubHome(data.parent.id);
        this.contextMenu = new ContextMenu_OnlyVOD();
        (function createDiv() {
            indicator = new Indicator(1);

            div.attr("class", "subHomeView");
            div.append(_$("<div/>", {class: "indicator_area"}));
            div.append(_$("<div/>", {class: "contents_area"}));
            setComponents(div.find(".contents_area"), subHomeData);
            div.append(_$("<div/>", {class: "bottom_dim"}));

            div.find(".indicator_area").empty().append(indicator.getView()).append(indicator.getPageTransView());
            div.append(instance.contextMenu.getView());
        })();
    };

    function setComponents(div, data) {
        var root = _$("<div/>", {class: "components_area"});
        if (data.event) addComponent(root, new BigBanner(id + "banner_top"), data.event.list[id]);

        if (data.recommend) {
            var recomList = data.recommend.list[id];
            var tmp = [];
            for (var i = 0; i < recomList.length; i++) {
                if (!tmp[recomList[i].position]) tmp[recomList[i].position] = [];
                tmp[recomList[i].position].push(recomList[i]);
            }
            for (var i in tmp) {
                addComponent(root, new SubPosterList(id + "_recom_" + i), tmp[i]);
            }
        }

        if (data.event) addComponent(root, new MiniBanner(id + "banner_bottom"), data.event.list[id]);
        div.append(root);
    }

    function addComponent(div, comp, data) {
        if (data) {
            if (comp.setData(data)) {
                div.append(comp.getView());
                compList.push(comp);
                addPageInfo(comp);
                comp.parent = instance;
            }
            indicator.setSize(pageInfo.length);
        }
    }

    function addPageInfo(component) {
        var lastPageInfo = pageInfo[pageInfo.length - 1];
        if (lastPageInfo.end - lastPageInfo.start < 970 - component.getHeight()) {
            lastPageInfo.length++;
            lastPageInfo.end += component.getHeight();
        } else {
            pageInfo[pageInfo.length] = {
                length: 1,
                start: lastPageInfo.end,
                end: lastPageInfo.end + component.getHeight()
            }
        }
    }

    function changeFocus(amount, keyCode) {
        if (amount == 0) return;
        div.find(".context_menu_indicator_area").css("visibility", "hidden");
        div.find(".context_menu_indicator").text("");
        compList[curIdx].blurred();
        curIdx = HTool.getIndex(curIdx, amount, compList.length);
        inPageIdx += amount;
        syncPage();
        compList[curIdx].focused(keyCode);
    }

    function syncPage() {
        var componentsArea = div.find(".components_area");
        if (inPageIdx < 0) {
            curPage = HTool.getIndex(curPage, -1, pageInfo.length);
            componentsArea.css("-webkit-transform", "translateY(" + -pageInfo[curPage].start + "px) translateZ(0)");
            inPageIdx += pageInfo[curPage].length;
        } else if (inPageIdx > pageInfo[curPage].length - 1) {
            inPageIdx -= pageInfo[curPage].length;
            curPage = HTool.getIndex(curPage, 1, pageInfo.length);
            componentsArea.css("-webkit-transform", "translateY(" + -pageInfo[curPage].start + "px) translateZ(0)");
        } else return;

        indicator.setPos(curPage);
        div.find(".in_page_comp").removeClass("in_page_comp");
        for (var i = 0; i < pageInfo[curPage].length; i++) {
            compList[curIdx - inPageIdx + i].getView().addClass("in_page_comp");
        }
        processBannerScale();
        syncPage();
    }


    /**
     * Banner Scale 을 처리한다.
     * 페이지 전환 시 화면 하단에 다음 페이지 Banner 보이게 되어 Scale 을 줄임
     */
    function processBannerScale() {
        log.printDbg("called processBannerScale()");
        if (!compList || compList.length <= 0) {
            log.printDbg("processBannerScale() - return, invalid compList");
            return;
        }
        var length = compList.length;
        for (var i = 0; i < length; i++) {
            var compView = compList[i].getView();
            if (compView.hasClass("in_page_comp")) {
                compView.css("-webkit-transform", "scaleY(1)");
            } else {
                if (i === 1 && compList[0] instanceof BigBanner) {
                    // BigBanner 다음에 나오는 Banner 는 화면 상 보여줘야 되므로 Scale 조정 안함.
                    compView.css("-webkit-transform", "scaleY(1)");
                } else {
                    compView.css("-webkit-transform", "scaleY(0.8)");
                }
            }
        }
    }

    function changePage(amount) {
        while (amount != 0) {
            if (amount > 0) {
                curIdx = HTool.getIndex(curIdx, pageInfo[curPage].length - inPageIdx, compList.length);
                inPageIdx = pageInfo[curPage].length;
                amount--;
            } else if (amount < 0) {
                curIdx = HTool.getIndex(curIdx, -1 - inPageIdx, compList.length);
                inPageIdx = -1;
                amount++;
            }
            syncPage();
        }
    }

    this.onKeyAction = function(keyCode) {
        if (compList[curIdx].onKeyAction(keyCode)) {
            return true
        } else {
            switch (keyCode) {
                case KEY_CODE.UP:
                    changeFocus(-1, keyCode);
                    return true;
                case KEY_CODE.DOWN:
                    changeFocus(1, keyCode);
                    return true;
                case KEY_CODE.RED:
                    if (inPageIdx === 0) {
                        if (!compList[curIdx].isFirstFocused()) {
                            compList[curIdx].blurred();
                            compList[curIdx].focused(keyCode);
                            return true;
                        } else changeFocus(-1, keyCode);
                    }
                    while (inPageIdx !== 0) {
                        changeFocus(-1, keyCode);
                    }
                    return true;
                case KEY_CODE.BLUE:
                    if (inPageIdx === pageInfo[curPage].length - 1) {
                        if (!compList[curIdx].isLastFocused()) {
                            compList[curIdx].blurred();
                            compList[curIdx].focused(keyCode);
                            return true;
                        } else changeFocus(1, keyCode);
                    }
                    while (inPageIdx !== pageInfo[curPage].length - 1) {
                        changeFocus(1, keyCode);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    return false;
                case KEY_CODE.RIGHT:
                    changeFocus(1, keyCode);
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.CONTEXT:
                    return true;
            }
        }
    };

    this.onKeyForIndicator = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                changePage(-1);
                return true;
            case KEY_CODE.DOWN:
                changePage(1);
                return true;
            case KEY_CODE.ENTER:
            case KEY_CODE.RIGHT :
                indicator.blurred();
                compList[curIdx].focused();
                return true;
            case KEY_CODE.LEFT:
                this.viewMgr.historyBack();
                return true;
            case KEY_CODE.RED:
                indicator.blurred();
                if(inPageIdx===0) {
                    compList[curIdx].focused(keyCode);
                } while(inPageIdx!==0) changeFocus(-1, keyCode);
                return true;
            case KEY_CODE.BLUE:
                indicator.blurred();
                if(inPageIdx === pageInfo[curPage].length-1) {
                    compList[curIdx].focused(keyCode);
                } while(inPageIdx !== pageInfo[curPage].length-1) changeFocus(1, keyCode);
                return true;
        }
    };

    this.getView = function() {
        return div;
    };

    //function setHomeShot(index) {
    //    if (subHomeData.homeShot) {
    //        var homeShot = subHomeData.homeShot.list[id];
    //        instance.viewMgr.setHomeShot(homeShot[index].itemType, homeShot[index].itemId, homeShot[index].imgUrl, homeShot[index].locator, homeShot[index].locator2);
    //    } else instance.viewMgr.setHomeShot();
    //}

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        // 2017.08.07 Lazuli
        // 서브홈에 홈샷이 없는 경우에 대한 예외처리 누락으로 인해 서브홈에 진입이 안되는 이슈사항이 있었음
        // 홈샷이 존재하며, 홈샷 리스트에 ID 값이 일치하는 리스트가 있을 경우에만 홈샷 셋팅하도록 수정함
        //if (subHomeData.homeShot && subHomeData.homeShot.list[id] && subHomeData.homeShot.list[id].length > 0) {
        //    setHomeShot(homeShotIndex = HTool.getRandomRangeValue(0, subHomeData.homeShot.list[id].length));
        //    if (!homeShotInterval) {
        //        homeShotInterval = setInterval(function() {
        //            setHomeShot(homeShotIndex = HTool.getIndex(homeShotIndex, 1, subHomeData.homeShot.list[id].length));
        //        }, 5000);
        //    }
        //} else instance.viewMgr.setHomeShot();

        //instance.viewMgr.setHomeShot();
    };

    this.hide = function() {
        View.prototype.hide.apply(this, arguments);
        //if (homeShotInterval) clearInterval(homeShotInterval);
        //homeShotInterval = null;
    };

    this.focused = function() {
        if (compList.length > 0) {
            div.hide();
            div.removeClass("blurred");
            div.find(".context_menu_indicator_area").css("visibility", "hidden");
            div.find(".context_menu_indicator").text("");
            indicator.blurred();
            compList[curIdx].focused();
            void div[0].offsetWidth;
            div.show();
            return subHome.ViewManager.FOCUS_MODE.FOCUS;
        } else return subHome.ViewManager.FOCUS_MODE.NO_FOCUS;
    };

    this.blurred = function() {
        if (compList.length > 0) {
            this.viewMgr.setContextIndicator("");
            div.hide();
            div.addClass("blurred");
            while (curIdx != 0) changeFocus(1);
            compList[curIdx].blurred();
            void div[0].offsetWidth;
            div.show();
        } else {
            div.addClass("blurred");
        }
    };

    this.pause = function() {
        if (compList[curIdx] && compList[curIdx].hasOwnProperty("pause")) compList[curIdx].pause();
    };

    this.resume = function() {
        if (compList[curIdx] && compList[curIdx].hasOwnProperty("resume")) compList[curIdx].resume();
    };
};
subHome.view.SubHomeView.prototype = new View();
subHome.view.SubHomeView.prototype.constructor = subHome.view.SubHomeView;
subHome.view.SubHomeView.prototype.type = View.TYPE.CONTENTS;