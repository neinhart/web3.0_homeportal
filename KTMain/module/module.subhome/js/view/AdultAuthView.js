/**
 * Created by ksk91_000 on 2016-07-01.
 */
subHome.view.AdultAuthView = function(viewId) {
    View.call(this, viewId + "View");
    this.div.addClass("adultAuthViewView");
    var adultAuthBox = new AdultAuthBox();

    var that = this;
    adultAuthBox.create(function () {
        that.viewMgr.setAdultAuthCheck();
        that.viewMgr.reChangeFocus();
    });
    this.div.append(adultAuthBox.getView());
    adultAuthBox.parent = this;

    this.onKeyAction = function(keyCode) {
        if(adultAuthBox.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.LEFT:
                this.viewMgr.historyBack();
                return true;
        }
    };

    this.focused = function () {
        this.div.addClass("focused");
        return adultAuthBox.focused();
    };

    this.blurred = function () {
        this.div.removeClass("focused");
        return adultAuthBox.blurred();
    };

    this.pause = function () {
        adultAuthBox.pause();
    };

    this.resume = function () {
        adultAuthBox.resume();
    };

    this.show = function () {
        View.prototype.show.apply(this, arguments);
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
        adultAuthBox.blurred();
    };
};
subHome.view.AdultAuthView.prototype = new View();
subHome.view.AdultAuthView.prototype.constructor = subHome.view.AdultAuthView;
subHome.view.AdultAuthView.prototype.type = View.TYPE.CONTENTS;
