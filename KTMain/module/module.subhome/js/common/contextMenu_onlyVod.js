/**
 * Created by ksk91_000 on 2017-03-25.
 */
var ContextMenu_OnlyVOD = function () {
    var div = _$("<div/>", {class: 'context_menu_area vod_only_context_menu'});
    var contextMenu = _$("<div/>", {class:'context_area'});
    var vodArea = _$("<div/>", {class:"vod_info_area"});

    var vodInfo;
    var listener;
    var vodFocus = 0;
    var recentlyVod = [];
    var recentlyVodArea = [];

    var that = this;
    var btnType;

    div.append(_$("<div/>", {class:'background_dim'}));
    div.append(_$("<div/>", {class:'left_arrow'}));
    div.append(contextMenu);

    (function () {
        contextMenu.append(vodArea);

        vodArea.html(
            "<div class='poster_area'>" +
                "<img class='content_poster'>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>" +
            "<div class='btn'>찜하기</div>" +
            "<div class='btn'>플레이리스트 추가</div>" +
            "<div class='title recently_text'>최근 확인한 VOD</div>" +
            "<div class='no_content_text'>최근 확인한<br>VOD가<br>없습니다</div>" +
            "<div class='recently_content'>" +
                "<div class='recently_content_imgWrapper'>" +
                "<img>" + "</div>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>" +
            "<div class='recently_content'>" +
                "<div class='recently_content_imgWrapper'>" +
                "<img>" + "</div>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>");
        recentlyVodArea[0] = vodArea.find(".recently_content").eq(0);
        recentlyVodArea[1] = vodArea.find(".recently_content").eq(1);
    })();

    this.open = function () {
        initFocus(0);
        div.addClass("activate");
        recentlyVod = JSON.parse(StorageManager.ms.load(StorageManager.KEY.RECENTLY_VOD))||[];
        setRecentlyContents();
        pauseTextAnimation();
    };

    this.close = function () {
        div.removeClass("activate");
        resumeTextAnimating();
    };

    this.isOpen = function () {
        return div.hasClass("activate");
    };

    this.setVodInfo = function (options) {
        vodInfo = options;
        contextMenu.find(".content_poster").attr("src", vodInfo.imgUrl + "?w=168&h=240&quality=90");
        div.find(".icon_area").toggle(!AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(vodInfo.prInfo));

        if(options.itemType==3||options.itemType==7||options.itemType==8)
            btnType = ContextMenu_VOD.VOD_BTN_TYPE.NONE;
        else if(options.itemType==0)
            btnType = ContextMenu_VOD.VOD_BTN_TYPE.ONLY_WISH;
        else
            btnType = ContextMenu_VOD.VOD_BTN_TYPE.ALL;

        vodArea.find(".btn:eq(0)").toggleClass("hiddenItem", (btnType&1)===0);
        vodArea.find(".btn:eq(1)").toggleClass("hiddenItem", (btnType&2)===0);
    };

    function initFocus(_focus) {
        vodFocus = _focus||0;
        changeFocus(0);
    }

    function changeFocus(amount) {
        if(btnType===ContextMenu_VOD.VOD_BTN_TYPE.NONE && recentlyVod.length===0) return;
        vodFocus = HTool.getIndex(vodFocus, amount, 2 + Math.min(2, recentlyVod.length));
        if(vodFocus<2) {
            if((btnType&Math.pow(2,vodFocus))===0) return changeFocus(amount===0?1:amount);
        } else {
            if(recentlyVod.length<=vodFocus-2) return changeFocus(amount===0?1:amount);
        }

        vodArea.find(".focus").removeClass("focus");
        switch (vodFocus) {
            case 0: case 1: vodArea.find(".btn").eq(vodFocus).addClass("focus"); break;
            case 2: case 3:
                recentlyVodArea[vodFocus-2].addClass("focus");
                recentlyVodArea[vodFocus-2].find(".shdw_area").addClass("focus");
                recentlyVodArea[vodFocus-2].find(".recently_content_imgWrapper").addClass("focus");
                break;
        }
    }

    function setRecentlyContents() {
        var i=0, contsArea = contextMenu.find(".recently_content"), imgArea = contextMenu.find(".recently_content_imgWrapper > img");
        for(i=0; i<recentlyVod.length; i++) {
            contsArea.eq(i).show().find(".icon_area").toggle(!AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(recentlyVod[i].prInfo));
            imgArea.eq(i).attr("src", recentlyVod[i].imgUrl);
        }for(;i<2; i++) {
            contsArea.eq(i).hide();
        }
        if (recentlyVod.length == 0) {
            initFocus(0);
        } else if (vodFocus > recentlyVod.length + 1) {
            changeFocus(-1);
        }
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                changeFocus(-1);
                return true;
            case KEY_CODE.DOWN:
                changeFocus(1);
                return true;
            case KEY_CODE.LEFT:
                this.close();
                return true;
            case KEY_CODE.RIGHT:
                return true;
            case KEY_CODE.RED:
                if(vodFocus==2||vodFocus==3) {
                    recentlyVod.splice(vodFocus-2, 1);
                    StorageManager.ms.save(StorageManager.KEY.RECENTLY_VOD, JSON.stringify(recentlyVod));
                    setRecentlyContents();
                } return true;
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
                this.close();
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
        }
    };

    function enterKeyAction() {
        switch (vodFocus) {
            case 0:
                // addWishList(vodInfo.itemType, vodInfo.catId, vodInfo.contsId, vodInfo.resolCd);
                addWishListVodInfo(vodInfo);
                that.close();
                return true;
            case 1:
                LayerManager.activateLayer({
                    obj: {
                        id: "AddToMyPlayListPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        params : vodInfo
                    },
                    moduleId:"module.family_home",
                    visible: true
                });
                that.close();
                return true;
            case 2:
            case 3:
                that.close();
                var vod = recentlyVod[vodFocus-2];
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SEARCH, vod.catId, vod.contsId, "01");
                } catch(e) {}
                openDetailLayer(vod.catId, vod.contsId, "01", { isKids:vod.isKids });
                return true;
        }

        function addWishListVodInfo(_vodInfo) {
            var familyHomeModule = ModuleManager.getModule("module.family_home");
            if (familyHomeModule) {
                familyHomeModule.execute({
                    method: "addToWishList",
                    data: _vodInfo
                });
            } else addWishList(_vodInfo.itemType, _vodInfo.catId, _vodInfo.contsId, _vodInfo.cmbYn);
        }

        function addWishList(itemType, catId, contsId, cmbYn) {
            if(itemType==1 || itemType==0) catId = contsId;
            try {
                subHome.amocManager.setPlayListNxt(1, itemType, contsId, catId, 1, (cmbYn === "Y" || cmbYn === true)? "Y" : "", function (result, response) {
                    if (result) {
                        switch (response.reqCode) {
                            case "0": showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다"); break; // 찜 등록 성공
                            case "3": throw "networkError(2)"; break;
                            case "1":
                                // 이미 찜 등록되어 있음 -> 해제 확인 팝업 출력
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "VODWishConfirm",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            callback: function () {
                                                subHome.amocManager.setPlayListNxt(1, itemType, contsId, catId, 2, (cmbYn === "Y" || cmbYn === true)? "Y" : "", function (result, response) {
                                                    if (result) {
                                                        if(response.reqCode == 0)  showToast("VOD 찜을 해제하였습니다"); // 찜 해제 성공
                                                        else throw "networkError(4)";
                                                    } else throw "networkError(3)";
                                                });
                                            }
                                        }
                                    },
                                    moduleId: "module.subhome",
                                    visible: true
                                });
                                break;

                        }
                    } else throw "networkError(1)";
                });
            } catch(e) {
                log.printErr(e);
            }
        }
    }

    this.removeFocus = function () {
        vodArea.find(".focus").removeClass("focus");
    };

    this.addFocus = function () {
        vodArea.find(".btn").eq(vodFocus).addClass("focus");
    };

    this.getView = function () {
        return div;
    };

    function pauseTextAnimation() {
        var div = _$(".textAnimating").removeClass("textAnimating").addClass("pauseAnimationByCtxMenu");
        UTIL.clearAnimation(div.find("span"));
    }

    function resumeTextAnimating() {
        var div = _$(".pauseAnimationByCtxMenu").removeClass("pauseAnimationByCtxMenu").addClass("textAnimating");
        UTIL.startTextAnimation({
            targetBox: div
        })
    }
};

Object.defineProperty(ContextMenu_VOD, "VOD_BTN_TYPE", {
    value: { NONE : 0, ONLY_WISH: 1, ONLY_PL: 2, ALL : 3 },
    writable: false
});