/**
 * Created by ksk91_000 on 2016-12-05.
 */
var ViewFactory = new function () {
    function getView(VCategoryObj, callback, parent) {
        var ajax;

        if(VCategoryObj.catType == "SubHome") {
            //서브홈
            var view = new subHome.view.SubHomeView(VCategoryObj.id, parent);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        }else if(VCategoryObj.catType == "Rank" || VCategoryObj.catType == "RankW3") {
            //랭킹리스트
            var view = new subHome.view.RankCategoryView(VCategoryObj.id, parent);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        }else if(VCategoryObj.catType=="RECAT" || VCategoryObj.catType=="REPRD"){
            var tmp = VCategoryObj;
            while(tmp.parent) tmp = tmp.parent;
            ajax = subHome.VodRecmManager.getRecommendMenu(VCategoryObj.catType, VCategoryObj.catType=="RECAT"?tmp.id:VCategoryObj.connerId, StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT)==1?"N":"Y", function (res, data) {
                if(res) {
                    if(data.RESULT_TYPE=="ITEM") {
                        var view = new subHome.view.RecomContentsView(VCategoryObj.id, parent);
                        view.title = HTool.getMenuName(VCategoryObj);
                        view.create(data, VCategoryObj.catType=="RECAT"?"52":"53");
                        callback(view);
                    } else if (data.RESULT_TYPE=="MENU") {
                        var view = new subHome.view.RecomContentsView(VCategoryObj.id, parent);
                        view.title = HTool.getMenuName(VCategoryObj);
                        view.create(data, VCategoryObj.catType=="RECAT"?"52":"53");
                        callback(view);
                    }
                }
            });
        }else if(VCategoryObj.children) {
            var view = new subHome.view.categoryView(VCategoryObj.id, parent);
            view.title = HTool.getMenuName(VCategoryObj);
            view.create(VCategoryObj);
            callback(view);
        } else {
            if(VCategoryObj.itemType==3||VCategoryObj.itemType==7||VCategoryObj.itemType==8) {
                //양방향 서비스
                var view = new subHome.view.AppContentsView(VCategoryObj.id, parent);
                view.title = HTool.getMenuName(VCategoryObj);
                view.create(VCategoryObj);
                callback(view);
            }else {
                //아이템 목록.
                ajax = subHome.amocManager.getItemDetlListW3(function (result, response) {
                    //히든메뉴용
                    if(result) {
                        response.itemDetailList = response.itemDetailList ? (Array.isArray(response.itemDetailList) ? response.itemDetailList : [response.itemDetailList]) : [];
                        if (response.itemDetailList && response.itemDetailList[0] && response.itemDetailList[0].itemType == 0 && response.itemDetailList[0].catType != "PkgVod") {
                            if (response.itemDetailList[0].webCatItemCnt > 15) {
                                subHome.amocManager.getItemDetlListW3(function (result2, response2) {
                                    response.itemDetailList.concat(response2.itemDetailList);
                                    VCategoryObj.children = UTIL.getVCategoryList("ITEM_DETAIL", response.itemDetailList, VCategoryObj);
                                    var view = new subHome.view.categoryView(VCategoryObj.id, parent);
                                    view.title = HTool.getMenuName(VCategoryObj);
                                    view.create(VCategoryObj);
                                    callback(view);
                                }, VCategoryObj.id, 15, response.itemDetailList[0].webCatItemCnt - 15);
                            } else {
                                VCategoryObj.children = UTIL.getVCategoryList("ITEM_DETAIL", response.itemDetailList, VCategoryObj);
                                var view = new subHome.view.categoryView(VCategoryObj.id, parent);
                                view.title = HTool.getMenuName(VCategoryObj);
                                view.create(VCategoryObj);
                                callback(view);
                            }
                        } else {
                            var view = new subHome.view.ContentsView(VCategoryObj.id, parent);
                            view.title = HTool.getMenuName(VCategoryObj);
                            view.create({categoryData: VCategoryObj, dataList: response.itemDetailList});
                            callback(view);
                        }
                    } else if(response!="abort") {
                        var view = new subHome.view.ContentsView(VCategoryObj.id, parent);
                        view.title = HTool.getMenuName(VCategoryObj);
                        view.create({categoryData: VCategoryObj, dataList: []});
                        callback(view);
                    }
                }, VCategoryObj.id, 1, 15);
            }
        } return ajax;
    }

    return {
        getView:getView
    }
};