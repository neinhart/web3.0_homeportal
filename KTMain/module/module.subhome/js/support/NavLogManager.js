/**
 * Created by ninei on 2017. 6. 7..
 */

window.NavLogMgr = new function () {

    // System Variables
    var fwLogMgr = window.homeImpl.get(homeImpl.DEF_FRAMEWORK.USER_LOG_MANAGER);
    var fwLog = window.homeImpl.get(homeImpl.DEF_FRAMEWORK.LOG);
    var KEY_CODE = homeImpl.get(homeImpl.DEF_FRAMEWORK.KEY_CODE);

    function collectHelp(data) { fwLogMgr.collect(data); };
    function printErrLog(e) { fwLog.printWarn("[collect] " + e); }

    function printLog(data) {
        // console.error(">>>>>>>"); console.log(data);
    };

    /** * VOD 상세화면 Navigation */
    this.collectVODDetail = function (startAct, categoryID, contentsID, contentsName, bID, bValue) {
        try {
            categoryID = checkNull(categoryID);
            contentsID = checkNull(contentsID);
            contentsName = checkNull(contentsName);
            bID = checkNull(bID);
            bValue = checkNull(bValue);
            var data = {
                type: NLC.TYPE_VOD_DETAIL,
                act: startAct,
                catId: categoryID,
                contsId: contentsID,
                contsName: contentsName,
                buttonId: bID,
                buttonValue: bValue
            }
            collectLog(data);
        } catch (e) { printErrLog(e); }
    };
    /** * VOD 구매버튼 Navigation */
    this.collectVODDetailPerchase = function (startAct, categoryID, contentsID, contentsName, btnData) {
        try {
            var bID = getVODBuyBtnID(btnData);
            this.collectVODDetail(startAct, categoryID, contentsID, contentsName, bID, categoryID+"|"+contentsID);
        } catch (e) { printErrLog(e); }
    }
    /** * 장르별 서브홈 Navigation */
    this.collectSubHome = function (startAct, categoryID, categoryName, contentsID, contentsName) {
        try {
            categoryID = checkNull(categoryID);
            categoryName = checkNull(categoryName);
            contentsID = checkNull(contentsID);
            contentsName = checkNull(contentsName);
            var data = {
                type: NLC.TYPE_SH_MENU,
                act: startAct,
                catId: categoryID,
                catName: categoryName,
                contsId: contentsID,
                contsName: contentsName
            }
            collectLog(data);
        } catch (e) { printErrLog(e); }
    };
    /** * 키즈 서브홈 Navigation */
    this.collectKids = function (startAct, categoryID, categoryName, contentsID, contentsName) {
        try {
            categoryID = checkNull(categoryID);
            categoryName = checkNull(categoryName);
            contentsID = checkNull(contentsID);
            contentsName = checkNull(contentsName);
            var data = {
                type: NLC.TYPE_KIDS_MENU,
                act: startAct,
                catId: categoryID,
                catName: categoryName,
                contsId: contentsID,
                contsName: contentsName
            }
            collectLog(data);
        } catch (e) { printErrLog(e); }
    };
    /** * 우리집 맞춤 TV Navigation */
    this.collectFHMenu = function (startAct, mId, mName, contentsID, contentsName) {
        try {
            mId = checkNull(mId);
            mName = checkNull(mName);
            contentsID = checkNull(contentsID);
            contentsName = checkNull(contentsName);
            var data = {
                type: NLC.TYPE_FH_MENU,
                act: startAct,
                menuId: mId,
                menuName: mName,
                contsId: contentsID,
                contsName: contentsName
            }
            collectLog(data);
        } catch (e) { printErrLog(e); }
    }
    /**
     * JUMP 기능
     * “Appendix D” >> “Appendix E"
     * @param jumpStart 진입수단, 진입경로
     * @param jumpDest 진입유형, 목적지
     * @param categoryID 카테고리 ID
     * @param contensID 컨텐츠 ID
     * @param loc 진입 화면 유형이 양방향일 경우 locator 값
     * @param path VOD 진입 경로 (postMessage 에 의해 진입하는 경우)
     */
    this.collectJump = function (jumpStart, jumpDest, categoryID, contensID, path, loc) {
        try {
            categoryID = checkNull(categoryID);
            contensID = checkNull(contensID);
            path = checkNull(path);
            loc = checkNull(loc);
            var data = {
                type: NLC.TYPE_JUMP_TO,
                act: jumpStart, // 진입수단, 진입경로
                jumpType: jumpDest, // 진입유형, 목적지
                catId: categoryID,
                contsId: contensID,
                locator: loc, // 진입 화면 유형이 양방향일 경우 locator 값
                reqPathCd: path // VOD 진입 경로 (postMessage 진입)

            }
            collectLog(data);
        } catch (e) { printErrLog(e); }
    };
    /** * VOD 점프 시나리오 처리 */
    this.collectJumpVOD = function (jumpStart, cateId, contsId) {
        try { this.collectJump(jumpStart, NLC.JUMP_DEST_DETAIL, cateId, contsId);
        } catch (e) { printErrLog(e); }
    }

    function getVODBuyBtnID (btn) {
        try {
            switch (btn.type) {
                case ButtonObject.TYPE.NORMAL: return NLC.DETAIL_BUY_SHORT;
                case ButtonObject.TYPE.DVD: return NLC.DETAIL_BUY_COLLECT;
                case ButtonObject.TYPE.SERIES: return NLC.DETAIL_BUY_SERIES;
                case ButtonObject.TYPE.CORNER: return NLC.DETAIL_BUY_MONTHLY;
                case ButtonObject.TYPE.PACKAGE:
                    if (!!btn.buyData.seriesData) return NLC.DETAIL_BUY_SERIES_PACKAGE;
                    else return NLC.DETAIL_BUY_PACKAGE;
                case ButtonObject.TYPE.VODPLUS: return NLC.DETAIL_SPOT;
                // NLC.DETAIL_SPOT
                // NLC.DETAIL_SHOPING
                default: return NLC.DETAIL_INTERACTIVE;
            }
        } catch(e) { printErrLog(e); }
    }

    function collectLog(data) {
        collectHelp(data);
        printLog("Req Collect =======");
        printLog(data);
        printLog("===================");
    };

    function checkNull(target) { if (!target) target = ""; return target; };
};

window.NLC = {
    TYPE_HOT_KEY: "HOT_KEY",                                // 리모컨핫키
    TYPE_HOME_MENU: "HOME_MENU",                            // 홈메뉴 navigation
    TYPE_VOD_DETAIL: "VOD_DETAIL",                          // VOD 상세화면 navigation
    TYPE_JUMP_TO: "JUMP_TO",                                // JUMP 기능
    TYP_ETC_MENU: "ETC_MENU",                               // 기타메뉴 진입
    TYPE_MENU_UPDATE: "MENU_UPDATE",                        // 메뉴 업데이트
    TYPE_FH_MENU: "FH_MENU",                                // 우리집 맞춤 TV
    TYPE_SH_MENU: "SH_MENU",                                // 장르별 Sub Home
    TYPE_KIDS_MENU: "KIDS_MENU",                            // 키즈 Sub Home
    // VOD 상세화면
    DETAIL_BUY_SHORT: "DETAIL_BUY_SHORT",                   // 단편
    DETAIL_BUY_COLLECT: "DETAIL_BUY_COLLECT",               // 소장용
    DETAIL_BUY_PACKAGE: "DETAIL_BUY_PACKAGE",               // 패키지
    DETAIL_BUY_MONTHLY: "DETAIL_BUY_MONTHLY",               // 월정액
    DETAIL_BUY_SERIES: "DETAIL_BUY_SERIES",                 // 시리즈
    DETAIL_BUY_SERIES_PACKAGE: "DETAIL_BUY_SERIES_PACKAGE", // 시리즈/패키지
    DETAIL_SERIES_LIST: "DETAIL_SERIES_LIST",               // 시리즈 회차 리스트
    DETAIL_PACKAGE_LIST: "DETAIL_PACKAGE_LIST",             // 패키지 컨텐츠 리스트
    DETAIL_SYNOP: "DETAIL_SYNOP",                           // 줄거리더보기
    DETAIL_NOTICE: "DETAIL_NOTICE",                         // 공지사항더보기
    DETAIL_SPOT: "DETAIL_SPOT",                             // 홈포털플러스

    // 2018.02.28 sw.nam
    // VOD 상세 버튼 코드 추가
    DETAIL_VOD_PLUS : "DETAIL_VOD_PLUS",                    // VOD 플러스 (업셀링)
    DETAIL_VOD_PPT : "DETAIL_VOD_PPT",                      // 기간정액

    DETAIL_SHOPING: "DETAIL_SHOPING",	                    // 쇼핑아이템
    DETAIL_INTERACTIVE: "DETAIL_INTERACTIVE",               // 양방향
    DETAIL_RECOMMEND_TAB: "DETAIL_RECOMMEND_TAB",           // 함께많이본영상 탭
    DETAIL_RECOMMEND: "DETAIL_RECOMMEND",                   // 함께많이본영상
    DETAIL_CHARA_SEARCH_TAB: "DETAIL_CHARA_SEARCH_TAB",     // 인물검색 탭
    DETAIL_CHARA_SEARCH: "DETAIL_CHARA_SEARCH",             // 인물검색
    DETAIL_PREVIEW_TAB: "DETAIL_PREVIEW_TAB",               // 예고편/부가영상 탭
    DETAIL_PREVIEW: "DETAIL_PREVIEW",                       // 예고편/부가영상
    DETAIL_STILLCUT: "DETAIL_STILLCUT",                     // 스틸컷
    DETAIL_RATING_TAB: "DETAIL_RATING_TAB",                 // 평점더보기 탭
    DETAIL_RATING: "DETAIL_RATING",                         // 별점평가하기
    DETAIL_SYNOPL_TAB: "DETAIL_SYNOPL_TAB",                 // 상세줄거리더보기 탭
    DETAIL_SYNOPL: "DETAIL_SYNOPL",                         // 상세줄거리더보기
    // 점프 기능 진입 코드표, Appendix D.
    JUMP_START_CATE: "JUMP_CATE_HOMESHOT",                  // 홈메뉴 카테고리 홈샷
    JUMP_START_PROMO: "JUMP_PROMO_CHANNEL",                 // 프로모 채널 바로가기
    JUMP_START_NOTICE: "JUMP_NOTICE",                       // 공지사항
    JUMP_START_SEARCH: "JUMP_SEARCH",                       // 검색
    JUMP_START_POST_MSG: "JUMP_POST_MSG",                   // postMessage
    JUMP_START_CONTEXT: "JUMP_CONTEXT",                     // 연관메뉴 (설정, 편성표, 북마크, 홈위젯 등)
    JUMP_START_RELATED_CONTENTS: "JUMP_RELATED_CONTENTS",   // 연관정보/VOD, 함께 많이 본 영상 등
    JUMP_START_MAIN_RECOMMEND: "JUMP_MAIN_RECOMMEND",       // 메인 화면 1 depth 좌측 메뉴별 추천 콘텐츠
    JUMP_START_SUBHOME: "JUMP_SUBHOME",                     // 서브홈 화면에 편성된 프로모션, 매스 추천 콘텐츠
    // 점프 기능 TYPE 코드표, Appendix E.
    JUMP_DEST_CATEGORY: "CATEGORY_JUMP",                    // 홈메뉴 카테고리 노출
    JUMP_DEST_DETAIL: "DETAIL_JUMP",                        // VOD 상세화면 노출
    JUMP_DEST_INTERACTIVE: "INTERACTIVE_JUMP",              // 양방향 실행
    JUMP_DEST_SEARCH: "SEARCH_JUMP",                        // 검색화면 노출
    JUMP_DEST_ETC: "ETC_JUMP",                              // 우리집 맞춤 TV/설정/채널가이드 메뉴 노출
    JUMP_DEST_PIP: "PIP_JUMP",                              // 2채널 동시시청 실행 (by 연관메뉴)
    JUMP_DEST_EPG: "EPG_JUMP",                              // 편성표 화면 노출 (by 연관메뉴)

    // 우리집 맞춤 TV 메뉴 ID 코드표
    MENU00700 : { id: "MENU00700", name: "알림박스"},
    // 마이메뉴1
        MENU00608: { id:"MENU00608", name: "공지사항"},
        MENU00710: { id: "MENU00710", name: "알림"},
        MENU00711: { id: "MENU00711", name: "이벤트"},
    T000000000008: { id: "T000000000008", name: "TV쿠폰"},
    T000000000009: { id: "T000000000009", name: "KT 맴버십"},
    T000000000010: { id: "T000000000010", name: "TV포인트"},
    T000000000011: { id: "T000000000011", name: "콘텐츠이용권"},
    T000000000012: { id: "T000000000012", name: "VOD 선물함"},
    // 최근시청목록
    MENU00601: { id: "MENU00601", name: "최근 시청 목록"},
    // 찜한목록
    MENU01200: { id: "MENU01200", name: "찜한 VOD 목록"},
    MENU07003: { id: "MENU07003", name: "당신을 위한 VOD 추천"},
        MENU07004: { id: "MENU07004", name: "나의 취향 알아보기"},
        MENU07005: { id: "MENU07005", name: "내가 평가한 콘텐츠"}
}

// l  type: 로그 유형 (첨부 문서 Appendix A. 로그 Type 코드표 참조)
// l  act : 진입 방법
//     n  진입 키코드 (첨부 문서 Appendix B. 리모컨 키 코드표 참조)
//     n  진입 수단(첨부 문서 Appendix E. 점프 기능 Type 코드표 참조)
//     n  메뉴 update (첨부 문서 Appendix F. 메뉴 업데이트 코드표 참조)
// l  catId : 상위 카테고리 id
// l  catName: 상위 카테고리 name
// l  contsId : 컨텐츠 id (카테고리 id, 시리즈 id, 대표화질 id)
// l  contsName : 컨텐츠 name (카테고리 name, 시리즈 name, 단편 name)
// l  jumpType: 진입 화면 유형 (첨부 문서 Appendix D. 점프 기능 진입 코드표 참조)
// l  locator : 진입 화면 유형이 양방향일 경우 locator 값
// l  reqPathCd: VOD 진입 경로 (postMessage 에 의해 진입하는 경우)
// l  buttonId: 버튼 구분 값 (Appendix C 참조)
// l  buttonValue: 버튼 데이터 값
// l  menuId: 메뉴 ID
// l  menuName : 메뉴 명