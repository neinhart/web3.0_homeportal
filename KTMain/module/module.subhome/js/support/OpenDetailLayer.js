/**
 * Created by ksk91_000 on 2016-10-31.
 *
 * @param cateId 카테고리 아이디
 * @param contsId 컨텐츠 아이디
 * @param reqPathCd 진입경로
 * @param exParam 추가 파라미터
 *      contentsData: 컨텐츠 데이터
 *      catData: 카테고리 데이터
 *      vodPlayListData: 플레이리스트 데이터. 있으면 플레이리스트로 재생
 *      sortGb: 이어보기 설정값
 *      isAdultCate: 성인카테고리에서 진입했는지 여부
 *      isKids: 키즈 카테고리에서 진입했는지 여부
 *      changeContentListener: 재생중인 컨텐츠가 바뀔 때 호출될 리스너 함수.
 */
window.openDetailLayer = function(cateId, contsId, reqPathCd, exParam){
    LayerManager.startLoading({preventKey:true});
    try {
        if (!exParam) exParam = {};
        if (exParam.contentsData && exParam.contentsData.itemType == 1) cateId = contsId;
        var a = ModuleManager.getModule("module.vod");
        if (!a) {
            ModuleManager.loadModule({
                moduleId: "module.vod",
                cbLoadModule: function (success) {
                    if (success) openDetailLayer(cateId, contsId, reqPathCd, exParam);
                }
            });
            return;
        } else if (exParam.contentsData && !exParam.contentsData.subHomeItemType) {
            a.execute({
                method: "showDetailWithData",
                params: {
                    cat_id: cateId,
                    const_id: contsId,
                    req_cd: reqPathCd,
                    cateInfo: exParam.contentsData,//exParam.catData,
                    contsInfo: exParam.contentsData,
                    playListData: exParam.vodPlayListData,
                    sortGb: exParam.sortGb,
                    isAdultCate: exParam.isAdultCate,
                    isKids: exParam.isKids,
                    changeContentListener: exParam.changeContentListener,
                    callback: function() {
                        LayerManager.stopLoading();
                    }
                }
            });
        } else a.execute({
            method: "showDetail",
            params: {
                cat_id: cateId,
                const_id: contsId,
                req_cd: reqPathCd,
                playListData: exParam.vodPlayListData,
                sortGb: exParam.sortGb,
                isAdultCate: exParam.isAdultCate,
                isKids: exParam.isKids,
                callback: function() {
                    LayerManager.stopLoading();
                }
            }
        });
    } catch(e) {
        LayerManager.stopLoading();
    }
};