/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    subHome.layer = subHome.layer || {};
    subHome.layer.subhome_masslist = function HomeMain(options) {
        Layer.call(this, options);
        var dataList;
        var contentsView;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame subhome subhome_main"});
            var template =
                "<div class='background'></div>" +
                "<span id='backgroundTitle'></span>";
            this.div.append(_$(template.trim()));

            var params = this.getParams();
            if(!params || params.length<=0) {
                if(cbCreate) cbCreate(false);
                return;
            }

            dataList = params;
            dataList = dataList?(_$.isArray(dataList)?dataList:[dataList]):[];
            for(var i=0; i<dataList.length; i++) dataList[i].squareImgUrl = dataList[i].imgUrl;

            contentsView = new subHome.view.MassContentsView("subhome_masslist");
            contentsView.create({categoryData: {id:"N", listType:"poster", posterListType:dataList[0].imgType},dataList: dataList?(_$.isArray(dataList)?dataList:[dataList]):[]});
            contentsView.viewMgr = { historyBack: LayerManager.historyBack, setContextIndicator:function () {} };
            var div = contentsView.getView();
            if (UTIL.getTextLength(dataList[0].recomTitle, "RixHead B", 40, -1.2) > 360) {
                this.div.find("#backgroundTitle").css("font-size", "38px");
                var newTitle = HTool.ellipsis(dataList[0].recomTitle, "RixHead B", 38, 360, "...", -1.2);
                this.div.find("#backgroundTitle").text(newTitle);
            }
            else {
                this.div.find("#backgroundTitle").css("font-size", "40px");
                this.div.find("#backgroundTitle").text(dataList[0].recomTitle);
            }
            this.div.append(div);

            var context = _$("<div/>", {class:"context_menu_indicator_area"});
            context.css("visibility", "inherit");
            context.append(_$("<div/>", {class:"context_menu_indicator"}).text("찜 | 플레이리스트 추가"));
            this.div.append(context);
            if(cbCreate) cbCreate(true);
        };

        this.controlKey = function (keyCode) {
            return contentsView.onKeyAction(keyCode) || ((keyCode == KEY_CODE.BACK||keyCode==KEY_CODE.LEFT)&& (LayerManager.historyBack(), true));
        };

        this.show = function () {
            Layer.prototype.show.apply(this, arguments);
            contentsView.focused();
        };

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
            LayerManager.hideVBOBackground();
        };
    };

    subHome.layer.subhome_masslist.prototype = new Layer();
    subHome.layer.subhome_masslist.prototype.constructor = subHome.layer.subhome_masslist;

    //Override create function
    subHome.layer.subhome_masslist.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    subHome.layer.subhome_masslist.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["SubHomeMassList"] = subHome.layer.subhome_masslist;
})();