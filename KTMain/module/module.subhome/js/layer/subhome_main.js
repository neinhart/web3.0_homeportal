/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    subHome.layer = subHome.layer || {};
    subHome.layer.subhome_main = function HomeMain(options) {
        Layer.call(this, options);

        var _this = this;

        var viewMgr;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame subhome subhome_main"});
            this.div.html('<div id="background">' +
                // '<div style="background: -webkit-linear-gradient(left, black 10%, rgba(0,0,0,.7));width: 1920px;height: 1080px;position: fixed"/>' +
                '<span id="backgroundTitle"></span>' +
                '</div>');

            viewMgr = new subHome.ViewManager();
            this.div.append(viewMgr.getView());
            viewMgr.setParent(this);
            cbCreate(true);
        };

        this.controlKey = function (keyCode) {
            return viewMgr.onKeyAction(keyCode) || (keyCode == KEY_CODE.BACK && (LayerManager.historyBack(), true));
        };

        this.show = function (options) {
            Layer.prototype.show.apply(this, arguments);
            // IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);

            LayerManager.showVBOBackground("subHome_main_background");
            if(options && options.resume) {
                viewMgr.resume();
            } else if(this.getParams()) {
                var params = this.getParams();
                viewMgr.destroy();
                viewMgr.setData(params.data, params.enterMenu);
                if (params.focusIdx) viewMgr.changeCategoryFocus(params.focusIdx);
                else if (params.focusMenu) viewMgr.changeCategoryFocus(findIndex(params.data.children, params.focusMenu));
                else if (params.enterMenu) viewMgr.changeCategoryFocus(findIndex(params.data.children, params.enterMenu), true);
            }

            KTW.managers.service.VoiceableManager.addNodeMatchListener(onNodeMatchListener);
        };

        function onNodeMatchListener (element) {
            var layer = LayerManager.getLastLayerInStack();
            if (layer.id !== _this.id) {
                return;
            }

            if (viewMgr) {
                viewMgr.onNodeMatchListener(element)
            }
        }

        function findIndex(root, target) {
            for(var i=0; i<root.length; i++) {
                if(root[i].id==target.id) return i;
            } return -1;
        }

        this.getActivateCategoryId = function () {
            return viewMgr.getCurrentCategoryId();
        };

        this.hide = function (options) {
            if(options && options.pause) viewMgr.pause();
            else {
                /**
                 * [dj.son] [WEBIIIHOME-3882] timer clear
                 */
                viewMgr.clearContentsChangeTimeout();

                AdultAuthorizedCheck.setAdultAuthorized(false, "subhome");
                LayerManager.stopLoading();
            }

            Layer.prototype.hide.apply(this, arguments);
            LayerManager.hideVBOBackground();

            KTW.managers.service.VoiceableManager.removeNodeMatchListener(onNodeMatchListener);
        };

        this.remove = function () {
            viewMgr.destroy();
            AdultAuthorizedCheck.setAdultAuthorized(false, "subhome");
            Layer.prototype.remove.apply(this, arguments);
        }
    };

    subHome.layer.subhome_main.prototype = new Layer();
    subHome.layer.subhome_main.prototype.constructor = subHome.layer.subhome_main;

    //Override create function
    subHome.layer.subhome_main.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    subHome.layer.subhome_main.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["SubHomeMain"] = subHome.layer.subhome_main;
})();