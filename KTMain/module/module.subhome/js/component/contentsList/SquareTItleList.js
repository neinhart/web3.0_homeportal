/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.SquarePosterList = function() {
    ContentsList.apply(this, arguments);
    var catData;
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var isFocused = false;
    var reqPathCd;
    const LINE_AMOUNT = 4;

    this.create = function (cateId, _catData, _reqPathCd) {
        this.cateId = cateId;
        this.div = _$("<ul/>", {class:"contentsList square_poster_list"});
        for(var i=0; i<LINE_AMOUNT*6; i++)
            this.div.append(getPosterDiv(Math.floor(i/LINE_AMOUNT)));

        reqPathCd = _reqPathCd||reqPathCd;
        catData = HTool.transCateInfo(_catData);
    };

    function getPosterDiv(lineNum) {
        return _$("<li class='content poster_square line_" + lineNum + "'>"+
            "<div class='content'>"+
            "<img class='posterImg poster'>"+   // 포스터 이미지
            "<div class='previewSdw'/>"+
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span></span></div>"+ // 제목
            "</div>"+
            "<div class='icon_area'/>"+
            "<div class='chart_area'/>"+
            "<div class='over_dim'/>"+
            "</li>");
    }

    function addChartOrder(div, data, idx) {
        div.empty();
        if(data.chartOrder) {
            div.append(_$("<div/>", {class: "chart_icon"}).text((idx+1)));
            div.append(_$("<div/>", {class: "chart_change_area"}));
            var chart_area = div.find(".chart_change_area");
            switch (data.change) {
                case "N" :
                    chart_area.append(_$("<div/>", {class: "chart_change_icon new"}));
                    break;
                case "0" :
                    chart_area.append(_$("<div/>", {class: "chart_change_icon still"}));
                    break;
                default :
                    var sign = data.change.substr(0, 1);
                    var tmpClass = "";
                    if (sign == "-") tmpClass = "minus"; else if (sign == "+") tmpClass = "plus";
                    chart_area.append(_$("<div/>", {class: "chart_change_icon " + tmpClass}).text(data.change.substr(1, data.change.length - 1)));
                    break;
            }
        }
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        if(!data.chartOrder) switch(data.newHot) {
            case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
            case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
            case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
            case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
            case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
        } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.resolCd && data.resolCd.indexOf("UHD")>=0) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.isDvdYn=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        } if(UTIL.isLimitAge(data.prInfo)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.setData = function(_dataList, page) {
        var contList = this.div.find("li");
        if(_dataList) {
            dataList = _dataList;
            totalPage = Math.ceil(dataList.length/(LINE_AMOUNT*2));
            this.indicator.setSize(totalPage, page);
            if(isFocused) this.setFocus(focus);
        } else if(dpPage==page) return;

        dpPage = page;
        if(totalPage>1) {
            //top Page
            var tmp = HTool.getIndex(page, -1, totalPage);
            for (var i = 0; i < LINE_AMOUNT; i++) setItem(contList.eq(i), tmp * LINE_AMOUNT*2 + LINE_AMOUNT + i);
            //2nd Page - current Page
            tmp = page;
            for (var i = 0; i < LINE_AMOUNT*2; i++) setItem(contList.eq(i + LINE_AMOUNT), tmp * LINE_AMOUNT*2 + i);
            //3rd Page
            var tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0; i < LINE_AMOUNT*2; i++) setItem(contList.eq(i + LINE_AMOUNT*3), tmp * LINE_AMOUNT*2 + i);
            //bottom Page
            var tmp = HTool.getIndex(page, 2, totalPage);
            for (var i = 0; i < LINE_AMOUNT; i++) setItem(contList.eq(i + LINE_AMOUNT*5), tmp * LINE_AMOUNT*2 + i);
        } else {
            for (var i = 0; i < LINE_AMOUNT*6; i++) setItem(contList.eq(i), i-LINE_AMOUNT);
        }
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if(data) {
            div.css("visibility", "inherit");
            div.attr("id", getDataListId(idx));
            div.find(".posterImg.poster").attr("src", "");
            div.find(".posterImg.poster")[0].offsetWidth;
            div.find(".posterImg.poster").attr("src", data.wEvtSquareImageUrlW3);
            div.find(".posterTitle span").text(data.itemName);
            // div.find("span.isfree").text(data.wonYn=="Y" ? "유료" : "무료");
            addChartOrder(div.find(".chart_area"), data, idx);
            setIcon(div.find(".icon_area"), data);
        }else {
            div.css("visibility", "hidden");
            div.attr("id", null);
            div.find(".posterImg.poster").attr("src", null);
            div.find(".posterTitle span").text("");
            // div.find("span.isfree").text("");
            div.find(".chart_area").detach();
        }
    }

    function setIndicatorFocus(that, focused) {
        if(focused) {
            that.indicator.focused();
            that.div.find("li.focus").removeClass('focus');
        }else {
            that.indicator.blurred();
            that.setFocus(page*LINE_AMOUNT*2);
        }
    }

    this.changeFocus = function(amount) {
        var newFocus = focus;
        do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(dataList.length/LINE_AMOUNT)*LINE_AMOUNT);
        while(Math.floor(newFocus/LINE_AMOUNT)>Math.floor((dataList.length-1)/LINE_AMOUNT));
        if(newFocus>=dataList.length) newFocus = dataList.length-1;

        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        while(page!=Math.floor(newFocus/(LINE_AMOUNT*2))) this.changePage((amount>0?1:-1));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
    };

    this.changePage = function(amount) {
        if(totalPage==1) return;
        this.div.removeClass("slide_up slide_down");
        page = HTool.getIndex(page, amount, totalPage);
        this.setData(null, HTool.getIndex(page, amount>0?-1:0, totalPage));
        void this.div[0].offsetWidth;
        this.div.addClass("slide_" + (amount>0?'up':'down'));
        this.indicator.setPos(page);
    };

    this.setFocus = function(newFocus) {
        if(!dataList[focus]) return;
        focus = newFocus;
        if(page!=Math.floor(newFocus/(LINE_AMOUNT*2))) this.setPage(Math.floor(newFocus/(LINE_AMOUNT*2)));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
    };

    this.setPage = function(newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
    };

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.LEFT:
                if(focus%LINE_AMOUNT!=0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.ENTER:
                dataList[focus].connerId = this.parent.connerId;
                try {
                    NavLogMgr.collectSubHome(keyCode, this.parent.viewId, this.parent.title,
                        getDataId(focus), dataList[focus].itemName);
                } catch(e) {}
                openDetailLayer(this.cateId, getDataId(focus), reqPathCd, {
                    contentsData: dataList[focus],
                    catData: catData,
                    changeContentListener: function (contsId) { that.onChangedContent(contsId); }
                });
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.UP:
                this.changePage(-1);
                return true;
            case KEY_CODE.DOWN:
                this.changePage(1);
                return true;
            case KEY_CODE.LEFT:
                return false;
            case KEY_CODE.RIGHT:
                setIndicatorFocus(this, false);
                return true;
            case KEY_CODE.RED:
                setIndicatorFocus(this, false);
                this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                setIndicatorFocus(this, false);
                this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
        }
    };

    this.getFocusedContents = function () {
        return dataList[focus];
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        return  ( dataList[dataIdx].itemId || dataList[dataIdx].contsId );
    }

    this.focused = function() {
        if(!dataList || dataList.length<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        this.indicator.blurred();
        this.setFocus(0);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
    };
};

SquarePosterList.prototype = new ContentsList();
SquarePosterList.prototype.constructor = SquarePosterList;