/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.RecomTitleList = function() {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var itemCnt;
    var reqPathCd;
    var isFocused = false;
    var isChangedPage = false;

    this.create = function (starType, _reqPathCd) {
        this.div = _$("<div/>", {class:"contentsList title_list"});

        this.div.append(_$("<ul/>", {class:"item_list"}));
        var detArea = _$("<div/>", {class:"contents_detail_area"});
        detArea.append(_$("<div/>", {class:"posterArea"}));
        detArea.append(_$("<div/>", {class:"star_area"}));
        detArea.append(_$("<div/>", {class:"info_area"}));
        detArea.append(_$("<div/>", {class:"synopsis_area"}));

        var posterArea = detArea.find(".posterArea");
        posterArea.append(_$("<img>", {class:"posterImg"}));
        posterArea.append(_$("<div/>", {class:"icon_area"}));

        reqPathCd = _reqPathCd;

        this.div.append(detArea);
        createItemArea(this.div.find(".item_list"));
        this.div.append(_$("<div/>", {class: "focus_area"}));
    };

    function createItemArea(ul) {
        for(var i=0; i<12; i++) {
            var li = _$("<li/>", {class:"contents_item"});
            li.append(_$("<div/>", {class: "title"}).append("<div/>"));
            li.append(_$("<div/>", {class: "wonInfo_area"}));
            li.find(".wonInfo_area").append(_$("<div/>", {class: "isFree"}));
            li.find(".wonInfo_area").append(_$("<img/>", {class: "isFreeIcon"}));
            li.append(_$("<div/>", {class: "prInfo"}));
            ul.append(li);
        }
    }

    this.setData = function(_dataList, page) {
        var contList = this.div.find(".item_list li");
        if(_dataList) {
            dataList = _dataList;
            itemCnt = _dataList.length;
            totalPage = Math.floor((itemCnt - 1) / 10) + 1;
            if(isFocused && !this.indicator.isFocused()) this.setFocus(focus)
        } else if(dpPage==page) return;

        dpPage = page;

        for (var i = 0; i < 12; i++) setItem(contList.eq(i), page*10+i);
        // for (var i = 0; i < 10; i++) setItem(contList.eq(i+10), dataList[HTool.getIndex(page, 1, totalPage)*10+i]);
        this.indicator.setSize(totalPage, page);
        if(isFocused && !this.indicator.isFocused()) this.setFocus(focus);
    };

    this.updateData = function (_dataList) {
        if(dpPage==page) dpPage = -1;
        this.setData(_dataList, page);
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if(data) {
            div.attr("id", getDataListId(idx));
            div.find(".title > div").text(data.ITEM_NAME);
            // div.find(".isFree").text(data.wonYn=="Y"?"유료":"무료");
            if (HTool.isTrue(data.WON_YN)) {
                div.find("img.isFreeIcon").show();
                div.find("div.isFree").hide();
            } else if (HTool.isFalse(data.WON_YN)) {
                div.find("div.isFree").show();
                div.find("div.isFree").text("무료");
                div.find("img.isFreeIcon").hide();
            } else {
                div.find("div.isFree").hide();
                div.find("img.isFreeIcon").hide();
            }
            div.find(".prInfo").html(_$("<img>", {"src": modulePath + "resource/image/icon_age_txtlist_" + UTIL.transPrInfo(data.RATING) + ".png"}));

            div.removeClass("hiddenItem");
        } else {
            div.attr("id", "");
            div.addClass("hiddenItem");
        }
    }

    this.changeFocus = function(amount) {
        focus = HTool.getIndex(focus, amount, itemCnt);
        this.div.find("li.focus").removeClass('focus');
        if(page!=Math.floor(focus/10)) this.changePage((amount>0?1:-1));
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");

        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
    };

    this.changePage = function(amount) {
        var newPage = HTool.getIndex(page, amount, totalPage);
        this.setPage(newPage);
    };

    this.setFocus = function(newFocus, forced) {
        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        if(forced || page!=Math.floor(newFocus/10)) this.setPage(Math.floor(newFocus/10));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");
        setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
    };

    this.setPage = function(newPage) {
        this.div.find(".item_list").removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
    };

    function setFocusItem(div, data) {
        div.find(".posterImg").attr({src: data.IMG_URL + "?w=259&h=370&quality=90", onerror:"this.src='" + modulePath + "resource/image/default_poster.png'"});
        div.find(".star_area").empty().append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(div, data.OLLEH_RATING);
        if(!!data.KEYWORD) {
            var keywordList = data.KEYWORD.split(", ");
            var html = "<title>추천</title><br>";
            for(var i=0; i<keywordList.length && i<3; i++) {
                html+=keywordList[i] + "<br>";
            } div.find(".info_area").html(html);
        }
        setIcon(div.find(".icon_area"), data);
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.NEW_HOT) {
            case "NEW": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
        } if(data.HDR_YN === "Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.IS_HD === "UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.SMART_DVD_YN === "Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(UTIL.isLimitAge(data.RATING)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return true;
            case KEY_CODE.LEFT :
                return false;
            case KEY_CODE.RED:
                if (focus === 0) {
                    if (totalPage > 1) this.changeFocus(-(((itemCnt - 1) % 10) + 1));
                } else if (focus % 10 === 0) {
                    if (totalPage <= 1) return true;
                    this.changeFocus(-10);
                } else this.changeFocus((page * 10) - focus);
                return true;
            case KEY_CODE.BLUE:
                if (focus === itemCnt - 1) {
                    if (totalPage > 1) this.changeFocus(10);
                } else if (focus % 10 === 9) {
                    if (totalPage <= 1) return true;
                    if (page === totalPage - 2) this.changeFocus(((itemCnt - 1) % 10) + 1);
                    else this.changeFocus(10);
                } else this.changeFocus((Math.min(itemCnt - 1, page * 10 + 9)) - focus);
                return true;
            case KEY_CODE.ENTER:
                goToDetailLayer(dataList[focus]);
                return true;
            default:
                return false;
        }
    };
    
    this.onKeyForIndicator = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN :
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.RIGHT :
            case KEY_CODE.ENTER:
                this.indicator.blurred();
                this.setFocus(isChangedPage?page*10:focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.RED:
                this.indicator.blurred();
                this.changeFocus((page*10) - focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.BLUE:
                this.indicator.blurred();
                this.changeFocus((Math.min(itemCnt-1, page*10+9)) - focus);
                isChangedPage = false;
                return true;
        }
    };

    function goToDetailLayer(data) {
        if(data.ITEM_TYPE == 1) {
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.ITEM_ID, "", reqPathCd);
            } catch(e) {}
            openDetailLayer(data.ITEM_ID, "", reqPathCd);
        }else if(data.ITEM_TYPE == 2 || data.ITEM_TYPE == 4) {
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.PARENT_CAT_ID, data.ITEM_ID, reqPathCd);
            } catch(e) {}
            openDetailLayer(data.PARENT_CAT_ID, data.ITEM_ID, reqPathCd);
        }
    }

    this.getFocusedContents = function () {
        return dataList[focus];
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        if(dataList[dataIdx]) return ( dataList[dataIdx].itemId || dataList[dataIdx].contsId );
        else return "";
    }

    this.focused = function() {
        if(!dataList || itemCnt<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        this.div.removeClass("show_detail");
        this.div.find(".focus_area").hide();
        // this.setData(dataList, page);
    };
};

RecomTitleList.prototype = new ContentsList();
RecomTitleList.prototype.constructor = RecomTitleList;