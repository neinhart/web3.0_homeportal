/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.NoContentsList = function () {
    this.div;
    this.cateId;
    this.sortGb;
    this.indicator;
    this.parent;

    this.create = function() {
        this.div = _$("<div/>", {class:"contentsList noContents"});
        var contentArea = _$("<div/>", {class: "no_content_area"});
        contentArea.html("<img src='" + modulePath +"resource/image/icon_noresult.png'><span>표시할 수 있는 콘텐츠가 없습니다</span>");
        this.div.append(contentArea);
    };

    this.setData = function(data) {

    };

    this.onKeyAction = function(keyCode) {

    };

    this.focused = function() {
        return subHome.ViewManager.FOCUS_MODE.NO_FOCUS;
    };

    this.blurred = function() {

    };

    this.getView = function() {
        return this.div;
    };

    this.getFocusedContents = function() {
        return null;
    };

    this.setIndicator = function(_indicator) {}
};