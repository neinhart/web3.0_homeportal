/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.RecomVerticalPosterList = function() {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var itemCnt;
    var isFocused = false;
    var lineVisibleTimeout;
    var div;
    var reqPathCd;
    const LINE_AMOUNT = 5;

    this.create = function (starType, _reqPathCd) {
        this.div = _$("<ul/>", {class:"contentsList vertical_poster_list"});
        div = this.div;
        reqPathCd = _reqPathCd;
        for(var i=0; i<LINE_AMOUNT*6; i++) {
            this.div.append(getPosterDiv((i%5>2?"right":"left") +  " line_" + Math.floor(i/5)));
        }
    };

    function getPosterDiv(addedClass) {
        var poster = _$("<li class='content poster_vertical " + addedClass + "'>"+
            "<div class='content'>"+
            "<img class='posterImg poster'>"+   // 포스터 이미지
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span/></div>"+ // 제목
            "<span class='posterDetail'>"+
            "<span class='isfree'></span>"+ //유.무료
            "<img class='isfreeIcon'>"+ //유.무료
            "<img class='age'>"+    //연령
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>"+
            "<div class='recom_area'><div class='bg_area'><div class='bg_left'/><div class='bg_mid'/><div class='bg_right'/></div>" +
            "<div class='text_area'><div class='title'>추천 키워드</div><div class='keyword'/></div>" +
            "</div>"+
            "<div class='over_dim'/>"+
            "</li>");

        poster.find(".posterDetail").prepend(stars.getView(stars.TYPE.RED_20));
        return poster;
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.NEW_HOT) {
            case "NEW": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
        } if(data.HDR_YN=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.IS_HD == "UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.SMART_DVD_YN=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(UTIL.isLimitAge(data.RATING)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.setData = function(_dataList, page) {
        var contList = this.div.find("li");
        if(_dataList) {
            dataList = _dataList;
            itemCnt = dataList.length;
            totalPage = Math.ceil(itemCnt/(LINE_AMOUNT*2));
            this.indicator.setSize(totalPage, page);
        } else if(page==dpPage) return;

        dpPage = page;
        if(totalPage>1) {
            //top Page
            tmp = page;
            for (var i = 0; i < LINE_AMOUNT*2; i++) setItem(contList.eq(i), tmp * LINE_AMOUNT*2 + i);
            //2rd Page
            var tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0; i < LINE_AMOUNT*2; i++) setItem(contList.eq(i + LINE_AMOUNT*2), tmp * LINE_AMOUNT*2 + i);
            //bottom Page
            var tmp = HTool.getIndex(page, 2, totalPage);
            for (var i = 0; i < LINE_AMOUNT; i++) setItem(contList.eq(i + LINE_AMOUNT*4), tmp * LINE_AMOUNT*2 + i);
            this.div.removeClass("singlePage");
        } else {
            for (var i = 0; i < LINE_AMOUNT*5; i++) setItem(contList.eq(i), i);
            this.div.addClass("singlePage");
        } if(isFocused && !this.indicator.isFocused()) this.setFocus(focus);
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if(data) {
            div.removeClass("hidden_cont");
            div.attr("id", getDataListId(idx));
            div.find(".posterImg.poster").attr("src", "");
            div.find(".posterImg.poster")[0].offsetWidth;
            div.find(".posterImg.poster").attr({src: data.IMG_URL+"?w=210&h=300&quality=90", onerror:"this.src='" + modulePath + "resource/image/default_poster.png'"});;
            div.find(".posterTitle span").text(data.ITEM_NAME);
            if (HTool.isTrue(data.WON_YN)) {
                div.find("img.isfreeIcon").show();
                div.find("span.isfree").hide();
            } else if (HTool.isFalse(data.WON_YN)) {
                div.find("span.isfree").show();
                div.find("span.isfree").text("무료");
                div.find("img.isfreeIcon").hide();
            } else {
                div.find("img.isfreeIcon").hide();
                div.find("span.isfree").hide();
            }
            div.find("img.age").attr("src", modulePath + "resource/image/icon_age_list_" + UTIL.transPrInfo(data.RATING) + ".png");
            if(!!data.KEYWORD) div.find(".recom_area").css("visibility", "inherit");
            else div.find(".recom_area").css("visibility", "hidden");
            div.find(".recom_area").css("width", getKeywordWidth(data.KEYWORD)+189 + "px");
            div.find(".recom_area .bg_mid").css("width", getKeywordWidth(data.KEYWORD)+112 + "px");
            div.find(".recom_area .keyword").html(data.KEYWORD.replace(/, /g,"<bar/>"));
            stars.setRate(div.find(".rating_star_area"), data.OLLEH_RATING);
            setIcon(div.find(".icon_area"), data);
        } else {
            div.addClass("hidden_cont");
            div.attr("id", null);
            div.find(".posterImg.poster").attr("src", null);
            div.find(".posterTitle span").text("");
            div.find("img.isfreeIcon").hide();
            div.find("span.isfree").hide();
            div.find(".chart_area").empty();
        }
    }

    function getKeywordWidth(keyword) {
        var length = 0;
        var keywords = keyword.split(", ");
        for(var i=0 ;i<keywords.length; i++) {
            length+=UTIL.getTextLength(keywords[i], "RixHead L", 27)+38;
        } return length-38;
    }

    function setIndicatorFocus(that, focused) {
        if(focused) {
            that.indicator.focused();
            that.div.find("li.focus").removeClass('focus');
        } else {
            that.indicator.blurred();
            that.setFocus(page*LINE_AMOUNT*2);
        }
    }

    this.changeFocus = function(amount) {
        var newFocus = focus;
        // Set Focus
        if(amount==1 && focus == itemCnt-1) newFocus = 0;
        else do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(itemCnt/LINE_AMOUNT)*LINE_AMOUNT);
        while(Math.floor(newFocus/LINE_AMOUNT)>Math.floor((itemCnt-1)/LINE_AMOUNT));
        if(newFocus>=itemCnt) newFocus = itemCnt-1;
        focus = newFocus;

        this.div.find("li.focus").removeClass('focus');
        while(page!=Math.floor(newFocus/10)) this.changePage((amount>0?1:-1));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        setTextAnimation(this.div, focus);
    };

    function clearTextAnimation() {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
    }

    function setTextAnimation(div, focus) {
        clearTextAnimation();
        if(UTIL.getTextLength(dataList[focus].ITEM_NAME, "RixHead L", 30)>210) {
            var posterDiv = div.find("li#" + getDataListId(focus) + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.changePage = function(amount) {
        if(totalPage==1) return;
        this.div.removeClass("slide_up slide_down");
        page = HTool.getIndex(page, amount, totalPage);
        this.setData(dataList, HTool.getIndex(page, amount>0?-1:0, totalPage));
        void this.div[0].offsetWidth;
        this.div.addClass("slide_" + (amount>0?'up':'down'));
        this.indicator.setPos(page);
        this.div.find(".hidden_line").removeClass("hidden_line");

        if(page == totalPage-1) setLineVisible(this.div.find(".line_" +(amount>0?"4":"2")), 0);
        if(amount>0) setLineVisible(this.div.find(".line_0,.line_1"), 500);
        else clearLineVisible();
    };

    this.setFocus = function(newFocus, forced) {
        if(!dataList[newFocus]) return;
        focus = newFocus;
        if(forced || page!=Math.floor(newFocus/(LINE_AMOUNT*2))) this.setPage(Math.floor(newFocus/(LINE_AMOUNT*2)));
        this.div.find("li.focus").removeClass('focus');
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        setTextAnimation(this.div, focus);
    };

    this.setPage = function(newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
        this.div.find(".hidden_line").removeClass("hidden_line");
        if(page == totalPage-1) setLineVisible(this.div.find(".line_2"), 0);
    };

    function clearLineVisible() {
        if(lineVisibleTimeout) {
            clearTimeout(lineVisibleTimeout);
            lineVisibleTimeout = null;
        }
    }

    function setLineVisible(div, time) {
        clearLineVisible();
        if(time==0) div.addClass("hidden_line");
        else lineVisibleTimeout = setTimeout(function () {
            div.addClass("hidden_line");
        }, time);
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.LEFT:
                if(focus%LINE_AMOUNT!=0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.RED:
                if(focus%(LINE_AMOUNT*2)===0) {
                    if(totalPage<=1) return true;
                    if(focus==0 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    this.changeFocus(-(LINE_AMOUNT*2));
                } else this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                if(focus === itemCnt-1) {
                    if(totalPage<=1) return true;
                    this.changeFocus(1);
                    this.changeFocus(LINE_AMOUNT * 2 - 1);
                } else if(focus%(LINE_AMOUNT*2)===LINE_AMOUNT*2-1) {
                    if(totalPage<=1) return true;
                    if(page==totalPage-2 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    else this.changeFocus(LINE_AMOUNT*2);
                } else this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
            case KEY_CODE.ENTER:
                goToDetailLayer(dataList[focus]);
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.LEFT:
                return false;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                setIndicatorFocus(this, false);
                return true;
            case KEY_CODE.UP:
                this.changePage(-1);
                return true;
            case KEY_CODE.DOWN:
                this.changePage(1);
                return true;
            case KEY_CODE.RED:
                setIndicatorFocus(this, false);
                this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                setIndicatorFocus(this, false);
                this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
        }
    };

    function goToDetailLayer(data) {
        if(data.ITEM_TYPE == 1) {
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.ITEM_ID, "", reqPathCd);
            } catch(e) {}
            openDetailLayer(data.ITEM_ID, "", reqPathCd);
        }else if(data.ITEM_TYPE == 2 || data.ITEM_TYPE == 4) {
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.PARENT_CAT_ID, data.ITEM_ID, reqPathCd);
            } catch(e) {}
            openDetailLayer(data.PARENT_CAT_ID, data.ITEM_ID, reqPathCd);
        }
    }

    this.getFocusedContents = function () {
        return dataList[focus];
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        if(dataList[dataIdx]) return dataList[dataIdx].ITEM_ID;
        else return "";
    }

    this.focused = function() {
        if(!dataList || dataList.length<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        clearLineVisible();
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
    };

    this.pause = function () {
        clearLineVisible();
        this.div.removeClass("slide_up slide_down");
        UTIL.clearAnimation(this.div.find(".textAnimating span"));
        this.div.find(".textAnimating").removeClass("textAnimating");
    };

    this.resume = function () {
        this.setFocus(focus, true);
        if(!this.indicator.isFocused()) setTextAnimation(this.div, focus);
        else setIndicatorFocus(this, true);
    };
};

RecomVerticalPosterList.prototype = new ContentsList();
RecomVerticalPosterList.prototype.constructor = RecomVerticalPosterList;