/**
 * Created by ksk91_000 on 2016-12-07.
 */
window.TitleList = function(parent, type) {
    ContentsList.apply(this, arguments);
    var dataList;
    var totalPage;
    var focus = 0, page = 0;
    var itemCnt;
    var dpPage = -1;
    var sortGb;
    var buyType;
    var isFocused = false;
    var isChangedPage = false;
    var isAdultCate;
    var ageLimit;
    var reqPathCd = "01";
    var catData;

    var isLoading = false;
    var loadedCallback = null;
    var posterType = type || TitleList.TYPE.VERTICAL;

    this.create = function (cateId, cateData, _reqPathCd) {
        var typeClass = posterType === TitleList.TYPE.SQUARE ? "square" : "";
        this.div = _$("<div/>", {class: "contentsList title_list " + typeClass});
        this.cateId = cateId;
        sortGb = cateData.sortGb;
        buyType = cateData.buyType;
        reqPathCd = _reqPathCd||reqPathCd;

        this.div.append(_$("<ul/>", {class:"item_list"}));
        var detArea = _$("<div/>", {class:"contents_detail_area"});
        detArea.append(_$("<div/>", {class:"posterArea"}));
        detArea.append(_$("<div/>", {class:"star_area"}));
        detArea.append(_$("<div/>", {class:"info_area"}));
        detArea.append(_$("<div/>", {class:"synopsis_area"}));

        var posterArea = detArea.find(".posterArea");
        posterArea.append(_$("<img>", {class:"posterImg"}));
        posterArea.append(_$("<div/>", {class:"icon_area"}));

        this.div.append(detArea);
        createItemArea(this.div.find(".item_list"));
        this.div.append(_$("<div/>", {class: "focus_area"}));

        isAdultCate = cateData.catType=="Adult";
        catData = HTool.transCateInfo(cateData);
    };

    function createItemArea(ul) {
        for(var i=0; i<10; i++) {
            var li = _$("<li/>", {class:"contents_item"});
            li.append(_$("<div/>", {class: "title"}).append("<div/>"));
            li.append(_$("<div/>", {class: "wonInfo_area"}));
            li.find(".wonInfo_area").append(_$("<div/>", {class: "isFree"}));
            li.find(".wonInfo_area").append(_$("<img/>", {class: "isFreeIcon"}));
            li.append(_$("<div/>", {class: "prInfo"}));
            ul.append(li);
        }
    }

    this.setData = function(_dataList, page, sortGb, _itemCnt) {
        var contList = this.div.find(".item_list li");
        if(_dataList) {
            dataList = _dataList;
            itemCnt = _itemCnt;
            totalPage = Math.floor((itemCnt - 1) / 10) + 1;
            ageLimit = AuthManager.getPR();
            if(isFocused && !this.indicator.isFocused()) this.setFocus(focus)
        } else if(dpPage==page) return;

        dpPage = page;

        for (var i = 0; i < 10; i++) setItem(contList.eq(i), page*10+i);
        // for (var i = 0; i < 10; i++) setItem(contList.eq(i+10), dataList[HTool.getIndex(page, 1, totalPage)*10+i]);
        this.indicator.setSize(totalPage, page);
        if(isFocused && !this.indicator.isFocused()) this.setFocus(focus);
    };

    this.updateData = function (_dataList) {
        if(dpPage==page) dpPage = -1;
        this.setData(_dataList, page, sortGb, itemCnt);
    };

    function setItem(div, idx) {
        var data = dataList[idx];
        if(data) {
            div.attr("id", getDataListId(idx));
            div.find(".title > div").text(data.itemName);
            // div.find(".isFree").text(data.wonYn=="Y"?"유료":"무료");
            if (HTool.isTrue(data.wonYn)) {
                div.find("img.isFreeIcon").show();
                div.find("div.isFree").hide();
            } else if (HTool.isFalse(data.wonYn)) {
                div.find("div.isFree").show();
                div.find("div.isFree").text("무료");
                div.find("img.isFreeIcon").hide();
            } else {
                div.find("div.isFree").hide();
                div.find("img.isFreeIcon").hide();
            }
            div.find(".prInfo").html(_$("<img>", {"src": modulePath + "resource/image/icon_age_txtlist_" + UTIL.transPrInfo(data.prInfo) + ".png"}));

            div.removeClass("hiddenItem");
        } else {
            div.attr("id", "");
            div.addClass("hiddenItem");
        }
    }

    this.changeFocus = function(amount) {
        var that = this;
        var oldFocus = focus;

        focus = HTool.getIndex(focus, amount, itemCnt);
        this.div.find("li.focus").removeClass('focus');
        if(page!=Math.floor(focus/10)) this.changePage((amount>0?1:-1), function () { that.setFocus(oldFocus); });
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");

        if(dataList[focus]) {
            this.div.find("li#" + getDataListId(focus)).addClass('focus');
            setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
        }

        if(focus%10+1>5 && !isLoading) {
            isLoading = true;
            this.parent.loadAddedContents(((page+1)*10+5), 10, function (res) {
                if(loadedCallback) loadedCallback(res);
                loadedCallback = null;
                isLoading = false;
            });
        }
    };

    this.changePage = function(amount, errorCallback) {
        var that = this;

        var newPage = HTool.getIndex(page, amount, totalPage);
        if(amount<0) this.parent.loadAddedContents((newPage*10), 10, function(res) { if(res) changePageImpl(that); else if(errorCallback) errorCallback(); }, true);
        else if(isLoading) {
            LayerManager.startLoading({preventKey: true});
            loadedCallback = function (res) { if(res) changePageImpl(that); else if(errorCallback) errorCallback(); LayerManager.stopLoading(); };
        } else this.parent.loadAddedContents((newPage*10)+5, 5*2, function(res) { if(res) changePageImpl(that); else if(errorCallback) errorCallback(); }, true);

        function changePageImpl() {
            that.setPage(newPage);
        }
    };

    this.setFocus = function(newFocus, forced) {
        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        if(forced || page!=Math.floor(newFocus/10)) this.setPage(Math.floor(newFocus/10));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");
        setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
    };

    this.setPage = function(newPage) {
        this.div.find(".item_list").removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
    };

    function setFocusItem(div, data) {
        div.find(".posterImg").attr({src: ""});
        void div.find(".posterImg")[0].offsetWidth;
        var imgUrl = data.imgUrl + "?w=259&h=370&quality=90";
        var defaultImg = "this.src='" + modulePath + "resource/image/default_poster.png'";
        if (posterType === TitleList.TYPE.SQUARE) {
            imgUrl = data.squareImgUrl + "?w=250&h=250&quality=90";
            defaultImg = "this.src='" + modulePath + "resource/image/default_music2.png'";
        }
        div.find(".posterImg").attr({src: imgUrl, onerror: defaultImg});
        div.find(".star_area").empty().append(stars.getView(stars.TYPE.RED_20)).append(data.runtime?("<span>" + data.runtime + "분</span>"):"");
        stars.setRate(div, data.mark);
        div.find(".info_area").html((data.overseerName?("<title>감독</title>" + data.overseerName + "<br>"):"") + (data.actor?("<title>출연</title>" + data.actor):""));
        div.find(".synopsis_area").text(data.synopsis);
        setIcon(div.find(".icon_area"), data);
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        if(!data.chartOrder) switch(data.newHot) {
            case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
            case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
            case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
            case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
            case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
        } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.resolCd && data.resolCd.indexOf("UHD")>=0) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.isDvdYn=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        } if(!isAdultCate && isLimitAge(data.prInfo)) {
            if (posterType === TitleList.TYPE.SQUARE) {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked_music.png"}));
            } else {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
            }
        }
    }

    function isLimitAge(a) {
        var b, c = !1;
        return b = ageLimit, "all" != UTIL.transPrInfo(a) && 0 != b && Number(UTIL.transPrInfo(a)) >= Number(b) && (c = !0), c
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return true;
            case KEY_CODE.LEFT :
                return false;
            case KEY_CODE.RED:
                if (focus === 0) {
                    if (totalPage > 1) this.changeFocus(-(((itemCnt - 1) % 10) + 1));
                } else if (focus % 10 === 0) {
                    if (totalPage <= 1) return true;
                    this.changeFocus(-10);
                } else this.changeFocus((page * 10) - focus);
                return true;
            case KEY_CODE.BLUE:
                if (focus === itemCnt - 1) {
                    if (totalPage > 1) this.changeFocus(10);
                } else if (focus % 10 === 9) {
                    if (totalPage <= 1) return true;
                    if (page === totalPage - 2) this.changeFocus(((itemCnt - 1) % 10) + 1);
                    else this.changeFocus(10);
                } else this.changeFocus((Math.min(itemCnt - 1, page * 10 + 9)) - focus);
                return true;
            case KEY_CODE.ENTER:
                dataList[focus].connerId = this.parent.connerId;
                if (dataList[focus].itemType == 2) {
                    dataList[focus].buyType = dataList[focus].buyType || buyType;
                    dataList[focus].catId = this.cateId;
                }
                try {
                    NavLogMgr.collectSubHome(keyCode, this.parent.viewId, this.parent.title,
                        getDataId(focus), dataList[focus].itemName);
                } catch (e) {
                }

                if (parent != undefined) {
                    var emptyData = parent.getEmptyData();
                    var playListData = {
                        list: (sortGb ? dataList : []),
                        startIdx: emptyData.startIdx,
                        amount: emptyData.lastIdx - emptyData.startIdx
                    };
                } else {
                    var playListData = {
                        list: (sortGb ? dataList : []),
                    };
                }

                var that = this;
                openDetailLayer(this.cateId, getDataId(focus), reqPathCd, {
                    contentsData: dataList[focus],
                    catData: catData,
                    vodPlayListData: playListData,
                    sortGb: sortGb,
                    isAdultCate: isAdultCate,
                    changeContentListener: function (contsId) {
                        that.onChangedContent(contsId);
                    }
                });
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN :
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.RIGHT :
            case KEY_CODE.ENTER:
                this.indicator.blurred();
                this.setFocus(isChangedPage?page*10:focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.RED:
                this.indicator.blurred();
                this.changeFocus((page*10) - focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.BLUE:
                this.indicator.blurred();
                this.changeFocus((Math.min(itemCnt-1, page*10+9)) - focus);
                isChangedPage = false;
                return true;
        }
    };

    this.getFocusedContents = function () {
        return dataList[focus];
    };

    this.onChangedContent = function (contsId) {
        for(var i in dataList) {
            if(dataList[i].itemId === contsId || dataList[i].contsId === contsId) {
                return this.setFocus(i-0);
            }
        }
    };

    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        if(dataList[dataIdx]) return ( dataList[dataIdx].itemId || dataList[dataIdx].contsId );
        else return "";
    }

    this.focused = function() {
        if(!dataList || itemCnt<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        isFocused = false;
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        this.div.removeClass("show_detail");
        this.div.find(".focus_area").hide();
        // this.setData(dataList, page);
    };
};

TitleList.prototype = new ContentsList();
TitleList.prototype.constructor = TitleList;
Object.defineProperty(TitleList, "TYPE", {
    value : {
        /** 미리보기 포스터가 직사각형인 경우*/
        VERTICAL : 'VERTICAL',
        /** 미리보기 포스터가 정사각형인 경우*/
        SQUARE : 'SQUARE'
    },
    writable: false,
    configurable: false
});