/**
 * Created by ksk91_000 on 2017-01-16.
 */
window.MiniBanner = function () {
    var div = _$("<div/>", {class:"banner_mini"});
    var focus = 0;
    var length;
    var eventList = [];

    this.setData = function(data) {
        for(var i=0; i<data.length; i++) if(data[i].templateType.charAt(0) == "M") eventList[data[i].templateLoc-1] = data[i];
        if(eventList.length===0) return false;
        switch(eventList[0].templateType) {
            case "M1":
                div.addClass("type_1");
                div.append(getItem("banner_mini_1", eventList[0]));
                length = 1;
                break;
            case "M2":
                div.addClass("type_2");
                div.append(getItem("banner_mini_2", eventList[0]));
                div.append(getItem("banner_mini_2", eventList[1]));
                length = 2;
                break;
            case "M3":
                div.addClass("type_3");
                div.append(getItem("banner_mini_3", eventList[0]));
                div.append(getItem("banner_mini_p", eventList[1]));
                div.append(getItem("banner_mini_p", eventList[2]));
                length = 3;
                break;
            case "M4":
                div.addClass("type_4");
                div.append(getItem("banner_mini_4", eventList[0]));
                div.append(getItem("banner_mini_p", eventList[1]));
                div.append(getItem("banner_mini_p", eventList[2]));
                div.append(getItem("banner_mini_p", eventList[3]));
                length = 4;
                break;
            default: return false;
        }return true;
    };

    function getItem(_class, _data) {
        var div = _$("<div/>", {class: _class + " banner_item"});
        if (_class === "banner_mini_2") { // 타입 2의 경우만 그림자 추가
            div.append(getShadowDiv());
        }
        div.append(_$("<div/>", {class:"absolute_wrapper"}).append(_$("<img>", {src:_data.imgUrl, onerror:"this.src='" + COMMON_IMAGE.BANNER_DEFAULT + getDefaultImageUrl(_class) + "';this.onerror=''"})));
        return div;
    }

    function getShadowDiv() {
        var shadow =
            "<div class='banner_sdw'>" +
            "   <img class='banner_sdw_l' src='" + modulePath + "resource/image/minibanner_focus_w580_shadow_l.png" + "'/>" +
            "   <img class='banner_sdw_m' src='" + modulePath + "resource/image/minibanner_focus_w580_shadow_m.png" + "'/>" +
            "   <img class='banner_sdw_r' src='" + modulePath + "resource/image/minibanner_focus_w580_shadow_r.png" + "'/>" +
            "</div>";
        return _$(shadow.trim());
    }

    function getDefaultImageUrl(_class) {
        switch (_class) {
            case "banner_mini_1": return "default_recommend_mini_w1194.png";
            case "banner_mini_2": return "default_recommend_mini_w590.png";
            case "banner_mini_3": return "default_recommend_mini_w702.png";
            case "banner_mini_4": return "default_recommend_mini_w456.png";
            case "banner_mini_p": return "default_recommend_mini_w210.png";
        }
    }

    function setFocus(newFocus) {
        focus = newFocus;
        div.find(".banner_item.focus").removeClass("focus");
        div.find(".banner_item").eq(focus).addClass("focus");
    }

    this.onKeyAction = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT :
                if(focus==0) return false;
                else setFocus(focus-1);
                return true;
            case KEY_CODE.RIGHT :
                if(focus==length-1) return false;
                setFocus(focus+1);
                return true;
            case KEY_CODE.ENTER:
                gotoLocator(eventList[focus]);
                return true;
        }
    };

    function gotoLocator(data) {
        switch (data.itemType-0) {
            case 0:
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) {
                        if (menu.id == data.itemId) return true;
                    }
                })[0];
                if (!!menuData) MenuServiceManager.jumpMenu({menu: menuData});
                else showToast("잘못된 접근입니다");
                break;
            case 1:    //시리즈
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.itemId, "67");
                } catch(e) {}
                openDetailLayer(data.itemId, null, "67");
                break;
            case 2:    //컨텐츠
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.parentCatId, data.itemId, "67");
                } catch(e) {}
                openDetailLayer(null, data.itemId, "67");
                break;
            case 3:    //멀티캐스트 양방향 서비스

                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                        data.parentCatId, data.itemId, "", data.locator);
                } catch(e) {}

                var nextState;
                if(data.locator == CONSTANT.APP_ID.MASHUP)
                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                else
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: data.locator,
                        ex_param: data.parameter
                    }
                });
                break;
            case 7:    //유니캐스트 양방향 서비스

                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                        data.parentCatId, data.itemId, "", data.locator);
                } catch(e) {}

                var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: data.id
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(data.locator);
                break;
        }
    }

    this.isLastFocused = function () { return focus === length-1; };
    this.isFirstFocused = function () { return focus === 0; };

    this.focused = function (keyCode) {
        if(keyCode===KEY_CODE.BLUE) setFocus(Math.min(5, length) - 1);
        else setFocus(0);
    };

    this.blurred = function () {
        div.find(".focus").removeClass("focus");
    };

    this.getView = function () {
        return div;
    };

    this.getHeight = function () {
        return 294 + 70;
    }
};