"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId);
 * posterDm.ajaxMenu(createPoster, failAjax);
 *
 */
var AmocManager = (function () {
    //var usageId;
    var api;
    var data;

    var AMOC_SERVER_HTTP_URL = AMOC_URL[0];
    var AMOC_SERVER_HTTPS_URL = AMOC_URL[1];

    var log = Log;
    var IS_RELEASE = ((KTW_DEF.UI_VERSION == "0.01.000") ? false : true);

    var DEV_MID = "OTVBIZ1001";
    var RELEASE_MID = "OTVBIZ1001";
    /**
     * ajax를 이용하여 json 데이터 수집<br>
     * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
     * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    var ajax = function (async, timeout, url, postData, /**Function*/callback, reqType, param) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAX request url+postData:" + url + "\n[" + log.stringify(postData) + "]");
        }

        if(postData instanceof Object) {
            if(!postData) {
                postData = {};
            }
            postData.WMOCKey = KTW_DEF.AMOC.AUTH_KEY;
        } else {
            postData = postData ? postData + "&WMOCKey=" + KTW_DEF.AMOC.AUTH_KEY : "WMOCKey=" + KTW_DEF.AMOC.AUTH_KEY;
        }

        return AjaxFactory.createAjax("AMOC Manager", async, (reqType ? reqType : "post"), "json", url, postData, false, timeout, callback, param);
    };

    var ajaxData = function (async, timeout, url, postData, /**Function*/callback) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAXDATA request url+postData:" + url + "\n[" + log.stringify(postData) + "]");
        }
        postData = postData ? postData : new Object();
        postData.WMOCKey = KTW_DEF.AMOC.AUTH_KEY;

        return AjaxFactory.createAjax("AMOC Manager [DATA]", async, "post", "json", url, postData, false, timeout, callback, null);
    };


    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AMOC AJAX request stop!!");
            try {
                request.abort();
            } catch (e) {
                log.printExec(e.message);
            }

            request = null;
        }
    }

    function _getProductInfo(callback, pkgCode, param , buyTypeYn) {
        api = "/amoc-api/product/info";
        data = "saId=" + KTW_DEF.SAID + "&pkgCode=" + pkgCode + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, null, param);
    }



    function _getChannelInfo(callback, serviceId, param , buyTypeYn) {
        api = "/amoc-api/channel/channel-info";
        data = "saId=" + KTW_DEF.SAID + "&serviceId=" + serviceId + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }


    function _usedMobilePay(callback, itemCode , itemName , authAmount , mobileNumber,  period , catId , ltFlag, param) {
        log.printDbg("_usedMobilePay() itemCode : " + itemCode + " , itemName : " + itemName + " , authAmount : " + authAmount + " , mobileNumber : " 
            + mobileNumber + " , period : " + period + " , catId : "+ catId + " , ltFlag : " + ltFlag);
        var version = "0100";
        var mid = DEV_MID;
        var orderDate = Util.getFormatDate(new Date(), "yyyyMMddHHmmss");
        var userId  = KTW_DEF.SAID;
        var pkgYn = "H";
        var userIp = KTW_DEF.IP;
        var saleYn = "N";
        var vatYn = "Y";
        var appCd = "H";
        var reqPathCd = "50";
        var hdYn = "";
        var trgtContsId = "";
        var trgtCatId = "";
        var pushPkgYn = "4";
        var pushLtFlag = ltFlag;
        var pushHdYn = "0";

        api = "/amoc-pgcpn-api/vod/buy/use-moble-pay";
        data = "version=" + version + "&mid=" + mid + "&orderDate=" + orderDate + "&userId=" + userId
            + "&itemCode=" + itemCode + "&itemName=" + itemName  + "&userIp=" + userIp  + "&authAmount=" + authAmount
            + "&mobileNumber=" + mobileNumber + "&pkgYn=" + pkgYn
            + "&period=" + period + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd
            + "&ltFlag=" + ltFlag  + "&hdYn=" + hdYn  + "&trgtContsId=" + trgtContsId
            + "&trgtCatId=" + trgtCatId + "&pushPkgYn=" + pushPkgYn  + "&pushLtFlag=" + pushLtFlag
            + "&pushHdYn=" + pushHdYn + "&saleYn=" + saleYn  + "&vatYn=" + vatYn;


        return ajax(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback, undefined/*req_type*/, param);
    }


    return {
        ajaxStop            : _ajaxStop,
        getChannelInfo      : _getChannelInfo,
        getProductInfo      : _getProductInfo,
        usedMobilePay      : _usedMobilePay
    }
}());