/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelPaymentImpl</code>
 *
 * 채널 미가입 및 OTS PPV 관련 외부 연동
 * @author jjh1117
 * @since 2017-02-07
 */

"use strict";

var ChannelPaymentImpl = (function() {
    var log = Log;
    var callbackFunc = null;

    var pkgData = null;
    var pkgDataList = [];

    var additionalProductSelectIndex = -1;

    var isBizChannelInfo = false;
    var buyTypeYn = "Y";

    function _getChannelInfo(params) {
        pkgData = null;
        pkgDataList = [];
        callbackFunc = null;

        additionalProductSelectIndex = -1;
        isBizChannelInfo = false;

        var custEnv = StorageManager.ps.load(StorageManager.KEY.CUST_ENV);
        if(custEnv !== null) {
            buyTypeYn = JSON.parse(custEnv).buyTypeYn;
        }else {
            buyTypeYn = "Y";
        }

        if(params !== undefined && params !== null) {
            var ch = params.ch;
            var isDVBChannel = params.is_dvb_channel;
            callbackFunc = params.callback;
            LayerManager.startLoading({
                preventKey: true
            });

            var param = {
                serviceId : ch.sid
            };

            if(KTW_ENV.IS_OTS === true) {
                LayerManager.stopLoading();
                _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_JOIN_FAIL , null , _showErrorPopupCallback);
                return;
            }

            if(KTW_ENV.IS_BIZ === true && buyTypeYn === "Y") {
                LayerManager.stopLoading();
                _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_JOIN_FAIL , null , _showErrorPopupCallback);
                return;
            }

            AmocManager.getChannelInfo(_callbackFuncChannelInfo, ch.sid , param ,  buyTypeYn);
        }
    }

    function _getCPMSSearchInfo(params) {
        pkgData = null;
        pkgDataList = [];
        callbackFunc = null;
        additionalProductSelectIndex = -1;
        if(params !== undefined && params !== null) {
            var ch = params.ch;
            callbackFunc = params.callback;

            var smartCardID = params.smart_card_id;
            LayerManager.startLoading({
                preventKey: true
            });
            IMSManager.searchInfo(_callbackFuncCPMSSearchInfo, ch.sid , smartCardID);
        }
    }

    function _showCPMSPurchasePopup(params) {
        callbackFunc = null;
        if(params !== undefined && params !== null) {
            pkgDataList[0] = params.cpmsInfo;
            callbackFunc = params.callback;
            _showPurchasePopup(false , false , true , isBizChannelInfo , callbackFunc);
        }
    }

    function _searchOTSOppInfo(params) {
        if(params !== undefined && params !== null) {
            var callback = params.callback;
            var chNo = params.ch_num;
            var smartCardID = params.smart_card_id;

            OppvManager.searchOppInfo(callback , chNo , smartCardID);
        }
    }


    function _searchOTSContractInfo(params) {
        if(params !== undefined && params !== null) {
            var callback = params.callback;
            var smartCardID = params.smart_card_id;

            OppvManager.searchContractInfo(callback , smartCardID);
        }
    }

    function _registOTSOppv(params) {
        if(params !== undefined && params !== null) {
            var callback = params.callback;
            var smartCardID = params.smart_card_id;
            var ppvSvcId = params.ppvSvcId;
            var svcOpenDh = params.svcOpenDh;
            var svcCloseDh = params.svcCloseDh;
            var usgAmt = params.usgAmt;
            var svcMthCd = params.svcMthCd;
            var juminBizNo = params.juminBizNo;
            var telNo = params.telNo;
            var etpsType = params.etpsType;
            var orderIpAddr = params.orderIpAddr;
            var parameters = params.parameters;

            OppvManager.registOppv(callback, smartCardID, ppvSvcId, svcOpenDh, svcCloseDh, usgAmt, svcMthCd, juminBizNo, telNo, etpsType, orderIpAddr, parameters);
        }
    }



    /**
     * OTV 채널 channel info 얻어오는 callback
     * @param result : AMOC 연동 성공여부
     * @param data : AMOC API getChanelInfo Response
     * @private
     */
    function _callbackFuncChannelInfo(result, data , param) {
        log.printDbg('_callbackFuncChannelInfo() result: ' + result + ', data: ' + data + ', param: ' + log.stringify(param));
        if(result === true) {
            // 부가 상품 가입
            // 기본상품과 부가상품 모두 구매 가능하면 부가상품의 우선순위가 더 높아서 먼저 처리
            if(buyTypeYn === "Y") {
                if (data.addPkgYn === "Y") {
                    if (data.addPkgId == null) {
                        log.printDbg('_callbackChInfo() addPkgId is null!!');
                        result = false;
                    }
                    else {
                        var pkg_list = data.addPkgId.split("|");
                        if (pkg_list.length > 1) {
                            // 상품 선택 팝업 호출 (캐치온, 캐치온디맨드)
                            // 이후에 상품을 선택하면 단일 상품과 같이 일반적인 구매 과정으로 진입
                            for (var i=0; i<pkg_list.length; i++) {
                                var params = {
                                    "length" : pkg_list.length,
                                    "idx" : i,
                                    "pkgCode" : pkg_list[i],
                                    serviceId : param.serviceId
                                };
                                AmocManager.getProductInfo(_callbackFuncPackageInfo, pkg_list[i], params , buyTypeYn);
                            }
                        }
                        else {
                            var params = { "pkgCode" : data.addPkgId , serviceId : param.serviceId};
                            AmocManager.getProductInfo(_callbackFuncPackageInfo, data.addPkgId, params , buyTypeYn);
                        }
                    }
                }
                // 기본 상품 upselling
                else if (data.upSelYn === "Y") {
                    if (data.upSelId == null) {
                        log.printWarn('_callbackChInfo() upSelId is null!!');
                        result = false;
                    }
                    else {
                        var pkg_list = data.upSelId.split("|");
                        for (var i=0; i<pkg_list.length; i++) {
                            var params = {
                                "length": pkg_list.length,
                                "idx": i,
                                "pkgCode": pkg_list[i]
                                , serviceId : param.serviceId};
                            AmocManager.getProductInfo(_callbackFuncPackageInfo, pkg_list[i], params , buyTypeYn);
                        }
                    }
                }else {
                    // 20180307
                    // 오류 팝업 노출 해야 함.
                    LayerManager.stopLoading();
                    _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_INFOMATION_FAIL , null , _showErrorPopupCallback);
                }
            }else {
                if (data.pptYn === "Y") {
                    isBizChannelInfo = true;
                    if (data.pptId == null) {
                        log.printDbg('_callbackChInfo() pptId is null!!');
                        result = false;
                    }
                    else {
                        var pkg_list = data.pptId.split("|");
                        if (pkg_list.length > 1) {
                            // 상품 선택 팝업 호출 (캐치온, 캐치온디맨드)
                            // 이후에 상품을 선택하면 단일 상품과 같이 일반적인 구매 과정으로 진입
                            for (var i=0; i<pkg_list.length; i++) {
                                var params = {
                                    "length" : pkg_list.length,
                                    "idx" : i,
                                    "pkgCode" : pkg_list[i]
                                    , serviceId : param.serviceId};
                                AmocManager.getProductInfo(_callbackFuncPackageInfo, pkg_list[i], params , buyTypeYn);
                            }
                        }
                        else {
                            var params = { "pkgCode" : data.pptId , serviceId : param.serviceId };
                            AmocManager.getProductInfo(_callbackFuncPackageInfo, data.pptId, params , buyTypeYn);
                        }
                    }
                }
                else {
                    // 구매가능 상품이 없는 경우 안내팝업 호출
                    LayerManager.stopLoading();
                    _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_INFOMATION_FAIL , null , _showErrorPopupCallback);
                }
            }

        }
        else {
            log.printDbg('_callbackChInfo() failed');
            if (data !== "abort") {
                /**
                 * CEMS로 전달
                 */
                OipfAdadapter.extensionAdapter.notifySNMPError("VODE-00013");
            }
            LayerManager.stopLoading();
            _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_INFOMATION_FAIL , null , _showErrorPopupCallback);
        }
    }

    function _showErrorPopupCallback(key) {
        
        if(key === undefined) {
            if(callbackFunc !== null) {
                callbackFunc();
            }
        }else {
            if(key === KTW_KEY_CODE.BACK || key === KTW_KEY_CODE.EXIT) {
                if(callbackFunc !== null) {
                    callbackFunc();
                }
                return true;
            }
        }
        
    }

    /**
     * OTV 채널 개별 상품 정보(channel package info) 얻어오는 callback
     * @param result : AMOC 연동 성공 여부
     * @param data : 개별 상품 정보 Response
     * @param param
     * @private
     */
    function _callbackFuncPackageInfo(result, data, param) {
        log.printDbg('_callbackPkgInfo() result: ' + result + ', data: ' + data + ', param: ' + log.stringify(param));

        LayerManager.stopLoading();
        if (!result) {
            if (data !== "abort") {
                /**
                 * result 값이 false 로 오고 결과가 timeout 등의 이유 일 경우 CEMS 리포트
                 */
                OipfAdadapter.extensionAdapter.notifySNMPError("VODE-00013");
            }
        }

        if (result === true) {
            var pkg_type = Number(data.pkgGubun);
            data.pkgCode = param.pkgCode;
            data.serviceId = param.serviceId;
            // 기본 상품
            if (pkg_type === 1) {
                // 순서 보정
                pkgDataList[param.idx] = data;
                if (param.length === pkgDataList.length) {
                    // 나중에 호출한 response가 먼저 오면 이전에 null 항목이 있어도 length는 같아지므로
                    // null check도 해준다
                    var hasEmpty = false;
                    for (var i=0; i<param.length; i++) {
                        if (pkgDataList[i] == null) {
                            hasEmpty = true;
                        }
                    }
                    if (hasEmpty === false) {
                        /**
                         * 업셀링 상품 가입 팝업 노출
                         */
                        _showPurchasePopup(true,true,true , isBizChannelInfo , callbackFunc);
                    }
                }
            }
            // 부가 상품
            else if (pkg_type === 2 && data.ossYn === "Y") {
                if (param.length == null) {
                    /**
                     * 채널 정보에서 부가상품이 한개만 존재 하는 경우에 진입
                     */
                    pkgDataList[0] = data;
                    if(data.contractYn == "Y"){
                        /**
                         * 약정 선택이 존재 하는 경우
                         */

                        log.printDbg("_callbackPkgInfo() call focus Choice Monthly");
                        _showPurchasePopup(false , false , true, isBizChannelInfo , callbackFunc);
                    }else{
                        log.printDbg("_callbackPkgInfo() call focus Monthly Guide");
                        _showPurchasePopup(false , false , false , isBizChannelInfo , callbackFunc);
                    }

                    /**
                     * biz 상품 관련 테스트 코드
                     */
                    // data.vatProductPrice = "1000|2000|3000";
                    // isBizChannelInfo = true;
                    // pkgDataList[0] = data;
                    // pkgDataList[1] = data;
                    // _showAddionalProductSelectPopup(_callbackAdditionalProductSelectPopup , 0 , isBizChannelInfo);

                }
                else {
                    /**
                     * 채널 정보에서 부가상품이 여러개 존재 하는 경우에 진입
                     * 해당 Case에 대해 검증 할 방법 찾아야 함.
                     */
                    pkgDataList[param.idx] = data;
                    if (param.length === pkgDataList.length) {
                        var hasEmpty = false;
                        for (var i=0; i<param.length; i++) {
                            if (pkgDataList[i] == null) {
                                hasEmpty = true;
                            }
                        }
                        if (hasEmpty === false) {
                            /**
                             *  부가상품 선택 팝업 노출
                             */
                            _showAddionalProductSelectPopup(_callbackAdditionalProductSelectPopup , 0 , isBizChannelInfo);
                        }
                    }
                }
            }
            // 카테고리 PPT 상품
            else if (pkg_type === 3) {
                // N/A
            }
            // 채널 PPT 상품
            else if (pkg_type === 4) {
                if (param.length == null) {
                    /**
                     * 채널 정보에서 PPT상품이 한개만 존재 하는 경우에 진입
                     */
                    pkgDataList[0] = data;

                    var vatProductPrice = data.vatProductPrice.split("|");

                    if (vatProductPrice.length > 1) {
                        /**
                         * 기간이 1개 이상 인 경우
                         */
                        log.printDbg("_callbackPkgInfo() call focus Choice Monthly");
                        _showPurchasePopup(false , false , true, isBizChannelInfo , callbackFunc);
                    }else {
                        _showPurchasePopup(false , false , false , isBizChannelInfo , callbackFunc);
                    }
                }
                else {
                    /**
                     * 채널 정보에서 부가상품이 여러개 존재 하는 경우에 진입
                     * 해당 Case에 대해 검증 할 방법 찾아야 함.
                     */
                    pkgDataList[param.idx] = data;
                    if (param.length === pkgDataList.length) {
                        var hasEmpty = false;
                        for (var i=0; i<param.length; i++) {
                            if (pkgDataList[i] == null) {
                                hasEmpty = true;
                            }
                        }
                        if (hasEmpty === false) {
                            /**
                             *  PPT상품 선택 팝업 노출
                             */
                            _showAddionalProductSelectPopup(_callbackAdditionalProductSelectPopup , 0 , isBizChannelInfo);
                        }
                    }
                }
            }
        }
        else {
            log.printDbg('_callbackFuncPackageInfo() failed');

            _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_INFOMATION_FAIL , null , _showErrorPopupCallback);
        }
    }

    function _callbackAdditionalProductSelectPopup(result , selectIndex) {
        LayerManager.deactivateLayer({
            id: PAYMENT_CHANNEL_PURCHASE_ADDITIONAL_PRODUCT_SELECT_POPUP
        });

        log.printDbg('_callbackAdditionalProductSelectPopup() result : ' + result + " , selectIndex : " + selectIndex);
        if(result === true) {
            additionalProductSelectIndex = selectIndex;

            var data = pkgDataList[additionalProductSelectIndex];
            if(data.contractYn == "Y"){
                /**
                 * 약정 선택이 존재 하는 경우
                 */
                log.printDbg("_callbackPkgInfo() call focus Choice Monthly");
                _showPurchasePopup(false , false , true, isBizChannelInfo , callbackFunc , additionalProductSelectIndex);
            }else{
                log.printDbg("_callbackPkgInfo() call focus Monthly Guide");
                _showPurchasePopup(false , false , false , isBizChannelInfo , callbackFunc , additionalProductSelectIndex);
            }
        }else {
            _showErrorPopupCallback();
        }
    }

    /**
     * OTS 미가입 채널 관련 CPMS 연동 부분 시작
     */

    /**
     *  CPMS 연동 Callback Function
     *  OTS 미가입채널 가입정보 얻어오는 callback
     */
    function _callbackFuncCPMSSearchInfo(result, data) {
        log.printDbg('_callbackFuncCPMSSearchInfo() result: ' + result + " , data : " + log.stringify(data));

        if (result === true && data !== null && data.data != null && data.data.type === "ch") {
            log.printDbg('_callbackFuncCPMSSearchInfo() data: ' + log.stringify(data));
            if(callbackFunc !== null) {
                callbackFunc(data.data);
            }
        }
        else if (result === true) {
            log.printDbg("[_callbackFuncCPMSSearchInfo] failed!");
            var descArray = null;
            if(data != null && data.message !== undefined && data.message != null && data.message !== ""){
                log.printDbg("[_callbackFuncCPMSSearchInfo] cpms server response errmsg : " + data.message);
                descArray = [];
                var split_1 = data.message.split("\n");
                for (var i = 0; i < split_1.length; i++) {
                    descArray[descArray.length] = split_1[i];
                }

                if(descArray.length>0) {
                    _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_SERVER_ERROR_MESSAGE , descArray , _showErrorPopupCallback);
                }
            }else {
                _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_OTS_UPSELLING , null , _showErrorPopupCallback);
            }
        }else {
            _showErrorPopup(ChannelPayment.DATA.ERROR.ERROR_INFOMATION_FAIL , null , _showErrorPopupCallback);
        }


    }

    function _callbackFuncBackKey(callbackFunc) {
        log.printDbg("_callbackFuncBackKey() additionalProductSelectIndex : " + additionalProductSelectIndex);
        if(additionalProductSelectIndex<0) {
            if(callbackFunc !== undefined && callbackFunc !== null) {
                callbackFunc(false);
            }
        }else {
            _showAddionalProductSelectPopup(_callbackAdditionalProductSelectPopup , additionalProductSelectIndex , isBizChannelInfo);
        }
    }

    function _showPurchasePopup(isUpselling , isChoiceProduct , isChoiceContract , isBiz , callbackFunc , selectAdditionalProductIndex) {
        var params = {
            is_upselling : isUpselling,
            is_choice_product : isChoiceProduct,
            is_choice_contract : isChoiceContract,
            is_biz : isBiz,
            package_list : pkgDataList,
            callback : callbackFunc,
            select_additional_product_index : selectAdditionalProductIndex === undefined ? -1 : selectAdditionalProductIndex,
            callback_back_key : _callbackFuncBackKey
        };

        LayerManager.activateLayer({
            obj: {
                id: PAYMENT_CHANNEL_PURCHASE_POPUP,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    data : params
                }
            },
            moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
            visible: true
        });
    }


    function _showAddionalProductSelectPopup(callbackFunc , selectIndex , isBiz) {
        var params = {
            additional_product_list : pkgDataList,
            callback : callbackFunc,
            select_product_index : selectIndex , // 채널 결제 팝업에서 이전 키 눌렀을 때 다시 부가상품 선택 팝업이 노출 되어야 함.
            is_biz : isBiz
        };

        LayerManager.activateLayer({
            obj: {
                id: PAYMENT_CHANNEL_PURCHASE_ADDITIONAL_PRODUCT_SELECT_POPUP,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    data : params
                }
            },
            moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
            visible: true
        });
    }


    /**
     * errorType [0:가입 실패 , 1 : 가입정보 못 얻어 왔을 때 , 2 : OTS  UPSelling 상품인 경우 , 3 : 출력메세지 리스트
     */
    function _showErrorPopup(errorType , descArray , callbackFunc) {
        var isCalled = false;

        var popupData = {
            arrMessage: [],
            arrButton: [{id: "ok", name: "확인"}],
            cbAction: function (buttonId) {
                var consumed = false;
                if (!isCalled) {
                    isCalled = true;

                    if(buttonId === "ok") {
                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                        });
                        if(callbackFunc !== undefined && callbackFunc !== null) {
                            callbackFunc();
                        }
                    }else {
                        if(callbackFunc !== undefined && callbackFunc !== null) {
                            consumed = callbackFunc(buttonId);
                            if(consumed === true) {
                                LayerManager.deactivateLayer({
                                    id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                                });
                            }
                        }
                    }
                }
                return consumed;
            }
        };

        if(errorType === 0) {
            /**
             * 가입 실패 한 경우
             */
            popupData.arrMessage.push({
                type: "Title",
                message: ["알림"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_45",
                message: ["가입한 상품에서는 구매할 수 없습니다"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_30_DARK",
                message: ["문의:국번 없이 100번"],
                cssObj: {}
            });

        }else if(errorType === 1) {
            /**
             * 가입 관련 정보가 없는 경우
             */
            popupData.arrMessage.push({
                type: "Title",
                message: ["안내"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_45",
                message: ["죄송합니다. 지금 리모컨 주문이 되지 않습니다"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_33",
                message: ["잠시 후에 주문해 주세요"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_30_DARK",
                message: ["문의:국번 없이 100번"],
                cssObj: {}
            });

        }else if(errorType === 2) {
            /**
             * OTS UPSELLING 인경우
             */
            popupData.arrMessage.push({
                type: "Title",
                message: ["가입 신청"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_45",
                message: ["시청하시려면 상품 변경이 필요합니다"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_30_DARK",
                message: ["문의:국번 없이 100번"],
                cssObj: {}
            });
        }else if(errorType === 3) {
            popupData.arrMessage.push({
                type: "Title",
                message: ["안내"],
                cssObj: {}
            });

            if(descArray !== undefined && descArray !== null && descArray.length>0 ) {
                popupData.arrMessage.push({
                    type: "MSG_45",
                    message: descArray,
                    cssObj: {}
                });
            }else {
                return;
            }
        }else if(errorType === 4) {
            popupData.arrMessage.push({
                type: "Title",
                message: ["휴대폰 결제"],
                cssObj: {}
            });
        }


        LayerManager.activateLayer({
            obj: {
                id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    data : popupData
                }
            },
            moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
            visible: true
        });

    }

    return {
        getChannelInfo: _getChannelInfo,
        getCPMSSearchInfo : _getCPMSSearchInfo,
        showCPMSPurchasePopup : _showCPMSPurchasePopup,
        searchOTSOppInfo : _searchOTSOppInfo,
        searchOTSContractInfo : _searchOTSContractInfo,
        registOTSOppv : _registOTSOppv

    }
}());