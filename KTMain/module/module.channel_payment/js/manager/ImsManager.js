"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * 
 */
var IMSManager = (function(){
	var log = Log;
	var groupId = "400";
    
	/**
	 * ajax를 이용하여 json 데이터 수집<br>
	 * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
	 * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
	var ajax = function(async, timeout, url, /**Function*/callback){
		return AjaxFactory.createAjax("IMSManager", async, "get", "json", url, undefined, undefined, timeout, callback);
	}
	
	function _ajaxStop(request){
	    if(request){
	        log.printDbg("IMS AJAX request stop!!");
	        request.abort();
	        request = null;
	    }
	} 

	function _searchInfo(callback, sourceId, scId){
    	var api = "/api/v1/stb/chinfovat/" + groupId + "/" + scId + "/" + sourceId;
        return ajax(true, 0, IMS_URL[0]+api, callback);
    }
    

    
    return {
    	ajaxStop: _ajaxStop,
    	searchInfo: _searchInfo,
    };
}());