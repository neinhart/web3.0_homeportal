/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>PurchaseChannelPopup</code>
 * params : {
 *    is_upselling : true/false ,
 *    is_choice_product = true/false,
 *    is_choice_contract = true/false,
 *    package_list : []
 * }
 * 미가입 채널에 대한 부가상품 가입 또는 Upselling 가입 팝업
 * @author jjh1117
 * @since 2017. 02. 03.
 */

"use strict";

(function() {
    var channelPurchasePopup = function ChannelPurchasePopup(options) {
        Layer.call(this, options);

        var util = Util;
        var log = Log;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var selectUpsellingProductView = null;
        var selectMonthlyView = null;
        var termsAgreeView = null;
        var mobilePhoneAuthView = null;
        var pinInputView = null;

        /**
         * 현재 포커스 위치
         * @type {number} 1 : 상품정보 , 2 : 약정기간 or 가입안내 , 3 : 가입안내 or 휴대폰 인증, 4 : 구매인증
         */
        var focusStep = 2;

        var unfocusStep1 = {};
        var unfocusStep2 = {};
        var unfocusStep3 = {};
        var unfocusStep4 = {};

        var focusStep1Div = null;
        var focusStep2Div = null;
        var focusStep3Div = null;
        var focusStep4Div = null;

        var unfocusTopPosition = [];

        var isUpselling = false;
        var isChoiceProduct = false;
        var isChoiceContract = false;

        var selectAdditionalProductIndex = -1;

        var callbackFunc = null;

        var callbackBackKey = null;

        var purchaseTitle = "";

        var step1Title = ["구매 상품" , "상품 선택"];

        var selectProduct = null;

        var pkgList = null;

        var descStr = null;

        var initshow = false;

        var channelPurchasePopupResourcePath = resourcePath + "channelpurchasepopup/";

        var isBiz = false;
        var serviceId = null;

        this.init = function(cbCreate) {
            params = this.getParams();

            if(params !== null) {
                isUpselling = params.data.is_upselling;
                isChoiceProduct = params.data.is_choice_product;
                isChoiceContract = params.data.is_choice_contract;
                isBiz = params.data.is_biz;
                pkgList =  params.data.package_list;
                callbackFunc = params.data.callback;
                selectAdditionalProductIndex = params.data.select_additional_product_index;

                callbackBackKey = params.data.callback_back_key;
                log.printDbg("create() isUpselling : " + isUpselling + " , isChoiceProduct : " + isChoiceProduct + " , isChoiceContract : " + isChoiceContract + " , selectAdditionalProductIndex : " + selectAdditionalProductIndex + " , isBiz : " + isBiz);


                var custEnv = StorageManager.ps.load(StorageManager.KEY.CUST_ENV);
                log.printDbg("create() custEnv : " + custEnv);
                var oldProdCd = JSON.parse(custEnv).pkgCode;
                log.printDbg("create() oldProdCd : " + oldProdCd);

            }

            if(isUpselling === true) {
                focusStep = 1;
                purchaseTitle = "가입하기";
            }else {
                focusStep = 2;
                if(isBiz === true) {
                    purchaseTitle = "결제하기";
                }else {
                    purchaseTitle = "가입하기";
                }

                if(isChoiceProduct === false) {
                    if(selectAdditionalProductIndex<0) {
                        if(pkgList !== null && pkgList.length>0) {
                            selectProduct = pkgList[0];
                        }
                    }else {
                        selectProduct = pkgList[selectAdditionalProductIndex];
                    }
                }

                if(isChoiceContract === false) {
                    focusStep = 3;
                }
            }

            div = this.div;

            _createView(div , this);

            if(cbCreate) cbCreate(true);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            _showView();
        };

        this.hideView = function(options) {
            log.printDbg("hideView()");
            _hideView();
        };


        this.isProductUpselling = function() {
            return isUpselling;
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey() keyCode : " + key_code);
            var consumed = false;
            if(key_code === KTW_KEY_CODE.EXIT || key_code === KTW_KEY_CODE.BACK) {
                if(key_code === KTW_KEY_CODE.BACK) {
                    /**
                     * activate 와 deactivate를 할 경우 순서가 매우 중요함.
                     * deactivate를 꼮 먼저 해야 함.
                     */
                    LayerManager.deactivateLayer({
                        id: PAYMENT_CHANNEL_PURCHASE_POPUP
                    });

                    if(callbackBackKey !== null) {
                        callbackBackKey(callbackFunc);
                    }
                }else {
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    LayerManager.deactivateLayer({
                        id: PAYMENT_CHANNEL_PURCHASE_POPUP
                    });
                }
                consumed = true;
            }else {
                if(focusStep === 1) {
                    consumed = selectUpsellingProductView.controlKey(key_code);
                }else if(focusStep === 2) {
                    if(isUpselling === true) {
                        consumed = termsAgreeView.controlKey(key_code);
                    }else {
                        consumed = selectMonthlyView.controlKey(key_code);
                    }
                }else if(focusStep === 3) {
                    if(isUpselling === true) {
                        consumed = mobilePhoneAuthView.controlKey(key_code);
                    }else {
                        consumed = termsAgreeView.controlKey(key_code);
                    }
                }else if(focusStep === 4) {
                    consumed = pinInputView.controlKey(key_code);
                }
            }

            return consumed;
        };

        this.goNextStep= function() {
            log.printDbg("goNextStep()");
            if(focusStep === 1) {
                var selectUpsellingProduct = selectUpsellingProductView.getSelectProductString();
                var length = util.getTextLength(selectUpsellingProduct , "RixHead M" , 27,-1.35);
                unfocusStep1.step_title.text("상품 선택 :");
                unfocusStep1.step_title.css({width:122});
                unfocusStep1.step_comment_div.css({width:(length+19*2)});
                unfocusStep1.step_comment_div.css({left:182});
                unfocusStep1.step_comment_text.css({width:(length+19*2)});
                unfocusStep1.step_comment_text.text(selectUpsellingProduct);
                unfocusStep1.step_comment_div.css("display" , "");
                if(isUpselling === true) {
                    unfocusStep1.step_comment1_text.css("display" , "");
                }

                selectProduct = pkgList[selectUpsellingProductView.getSelectProductIndex()];
            }else if(focusStep === 2) {
                if(isUpselling === true) {
                    if(termsAgreeView.getIsTermsAgree() === true) {
                        var length = util.getTextLength("약관 동의함" , "RixHead M" , 27,-1.35);
                        unfocusStep2.step_title.text("가입 안내 :");
                        unfocusStep2.step_comment_div.css({width:(length+19*2)});
                        unfocusStep2.step_comment_text.css({width:(length+19*2)});
                        unfocusStep2.step_comment_text.text("약관 동의함");
                        unfocusStep2.step_comment_div.css("display" , "");
                        unfocusStep2.step_comment_div.css({left:182});
                    }else {
                        unfocusStep2.step_title.text("가입 안내");
                        unfocusStep2.step_comment_div.css("display" , "none");
                    }
                }else {
                    var selectOption = selectMonthlyView.getSelectOptionString();

                    var length = util.getTextLength(selectOption , "RixHead M" , 27,-1.35);
                    if(isBiz === true) {
                        unfocusStep2.step_title.text("시청 기간 :");
                    }else {
                        unfocusStep2.step_title.text("약정 기간 :");
                    }
                    unfocusStep2.step_comment_div.css({width:(length+19*2)});
                    unfocusStep2.step_comment_text.css({width:(length+19*2)});
                    unfocusStep2.step_comment_text.text(selectOption);
                    unfocusStep2.step_comment_div.css("display" , "");
                    unfocusStep2.step_comment_div.css({left:182});
                }

            }else if(focusStep === 3) {
                if(isUpselling === true) {
                    if(mobilePhoneAuthView.getIsMobilePhoneAuth()=== true) {
                        var length = util.getTextLength("휴대폰 인증 완료" , "RixHead M" , 27, -1.35);
                        unfocusStep3.step_title.text("휴대폰 인증 :");
                        unfocusStep3.step_title.css({width:148});

                        unfocusStep3.step_comment_div.css({width:(length+19*2)});
                        unfocusStep3.step_comment_text.css({width:(length+19*2)});
                        unfocusStep3.step_comment_text.text("휴대폰 인증 완료");
                        unfocusStep3.step_comment_div.css("display" , "");
                        unfocusStep3.step_comment_div.css({left:206});
                    }else {
                        unfocusStep3.step_title.text(",휴대폰 인증");
                        unfocusStep3.step_comment_div.css("display" , "none");
                    }
                }else {
                    if(termsAgreeView.getIsTermsAgree() === true) {
                        var length = util.getTextLength("약관 동의함" , "RixHead M" , 27,-1.35);
                        unfocusStep3.step_title.text("가입 안내 :");
                        unfocusStep3.step_comment_div.css({width:(length+19*2)});
                        unfocusStep3.step_comment_text.css({width:(length+19*2)});
                        unfocusStep3.step_comment_text.text("약관 동의함");
                        unfocusStep3.step_comment_div.css("display" , "");
                        unfocusStep3.step_comment_div.css({left:182});
                    }else {
                        unfocusStep3.step_title.text("가입 안내");
                        unfocusStep3.step_comment_div.css("display" , "none");
                    }
                }
            }


            focusStep++;
            if(focusStep>4) {
                focusStep = 4;
            }
            _showFocusStep();
        };


        this.goPrevStep= function() {
            log.printDbg("goPrevStep()");

            focusStep--;
            if(isUpselling === true) {
                // if(focusStep === 2) {
                //     termsAgreeView.setIsTermsAgree(false);
                // }
                if(focusStep<1) {
                    focusStep = 1;
                }
            }else {
                if(focusStep<2) {
                    focusStep = 2;
                }

                if(focusStep === 2) {
                    if(isChoiceContract === false) {
                        focusStep = 3;
                    }
                }
            }
            _showFocusStep();
        };

        this.showTermsAgreeDetail = function () {
            var params = {
                terms_desc : descStr,
                callback : _callbackTermsAgreeDetail
            };

            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_TERMS_AGREE_DETAIL_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : params
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });


        };


        this.purchaseChannel = function(pin) {
            log.printDbg("purchaseChannel() pin : " + pin);
            log.printDbg("purchaseChannel() isUpselling : " + isUpselling);

            if(KTW_ENV.IS_OTS === true){
                var selectIndex = selectMonthlyView.getSelectOptionIndex();

                log.printDbg("purchaseChannel() scid : " + selectProduct.details[selectProduct.detailIndex].scid);
                log.printDbg("purchaseChannel() code : " + selectProduct.details[selectProduct.detailIndex].code);
                log.printDbg("purchaseChannel() ctcode : " + selectProduct.details[selectProduct.detailIndex].contract[selectIndex].ctcode);
                LayerManager.startLoading({
                    preventKey: true
                });

                OppvManager.requestOptionalVodUpselling(_optionalUpsellingOTS,
                    selectProduct.details[selectProduct.detailIndex].scid,
                    selectProduct.details[selectProduct.detailIndex].code, null,
                    selectProduct.details[selectProduct.detailIndex].contract[selectIndex].ctcode);

            }else {
                if(isUpselling === true) {
                    log.printDbg("purchaseChannel() OTV UPSELLING getPackageCode : " + _getPackageCode());
                    var now = new Date().getTime();
                    now = Math.floor(now/1000);
                    log.printDbg("purchaseChannel() now : " + now);

                    HDSManager.onlineChangeProdbPIN(pin,
                        _getPackageCode(),
                        now , _responseHDS);

                    var params = {
                        product_name : selectUpsellingProductView.getSelectUpSellingPkgName()
                    };

                    LayerManager.activateLayer({
                        obj: {
                            id: PAYMENT_CHANNEL_UPGRADE_PROCESSING_POPUP,
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                data : params
                            }
                        },
                        moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                        visible: true
                    });

                }else {
                    LayerManager.startLoading({
                        preventKey: true
                    });
                    var now = new Date().getTime();
                    now = Math.floor(now/1000);
                    log.printDbg("purchaseChannel() now : " + now);

                    log.printDbg("purchaseChannel() OTV 부가상품 온라인 가입 isChoiceContract : " + isChoiceContract);
                    if(isChoiceContract === true) {
                        var selectIndex = selectMonthlyView.getSelectOptionIndex();

                        log.printDbg("purchaseChannel() selectMonthlyView.getSelectOptionIndex() : " + selectIndex);
                        log.printDbg("purchaseChannel() select contractInfo  : " + log.stringify(selectProduct.contractInfo[selectIndex]));
                        log.printDbg("purchaseChannel() getPackageCode : " + _getPackageCode());
                        log.printDbg("purchaseChannel() contractYn : " + selectProduct.contractYn);
                        log.printDbg("purchaseChannel() contractCd : " + selectProduct.contractInfo[selectIndex].contractCd);
                        HDSManager.onlineChVodMSVCbPIN(pin,
                            _getPackageCode(),
                            _responseHDS ,
                            selectProduct.contractYn,
                            selectProduct.contractInfo[selectIndex].contractCd , now );
                    }else {
                        log.printDbg("purchaseChannel() getPackageCode : " + _getPackageCode());
                        HDSManager.onlineChVodSvcReg(pin, _getPackageCode(), now , _responseHDS);
                    }

                }

            }
        };

        this.closePurchasePopup = function() {
            log.printDbg("closePurchasePopup()");
            if(callbackFunc !== null) {
                callbackFunc(false);
            }

            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_POPUP
            });

        };
        this.tvPayPurchase = function(selectbizOptionIndex) {
            log.printDbg("tvPayPurchase() selectbizOptionIndex : " + selectbizOptionIndex);
            var authAmount = "";
            var optionsPeriod = "";

            var periodList =  selectProduct.period.split("|");
            var vatProductPriceList = selectProduct.vatProductPrice.split("|");

            
            if(isChoiceContract === true) {
                var idx = selectMonthlyView.getSelectOptionIndex();
                optionsPeriod = periodList[idx];
                authAmount = vatProductPriceList[idx];

            }else {
                optionsPeriod = periodList[0];
                authAmount = vatProductPriceList[0];
            }




            var orderDate = Util.getFormatDate(new Date(), "yyyyMMddHHmmss");
            var said  = KTW_DEF.SAID;
            var contentId = _getPackageCode();
            var contentName = selectProduct.productName;
            var userIp = KTW_DEF.IP;
            var amount = authAmount;
            var MobileNumber = "";
            var pkgYn = "H";
            var period = optionsPeriod;
            var categoryId = selectProduct.serviceId;
            var appCd = "H";
            var reqPathCd = "50";
            var ltFlag = selectProduct.ltFlag;
            var hdYn = "";
            var buyType = 101;
            if(selectbizOptionIndex ===1) {
                buyType = 103;
            }
            var trgtContsId = "";
            var trgtCatId = "";
            var pushPkgYn = "4";
            var pushLtFlag = ltFlag;
            var pushHdYn = "0";
            var saleYn = "N";
            var vatYn = "Y";
            var purchaseType = "PAY";
            if(selectbizOptionIndex ===1) {
                purchaseType = "POINT";
            }


            var appandData = orderDate + "|" + said + "|"
                            + contentId + "|" + contentName + "|"
                            + userIp + "|" + amount + "|"
                + MobileNumber + "|" + pkgYn + "|"
                + period + "|" + categoryId + "|"
                + appCd + "|" + reqPathCd + "|"
                + ltFlag + "|" + hdYn + "|"
                + buyType + "|" + trgtContsId + "|"
                + trgtCatId + "|" + pushPkgYn + "|"
                + pushLtFlag + "|" + pushHdYn + "|"
                + saleYn + "|" + vatYn + "|"
                + purchaseType ;


            var options = {
                amount : authAmount,
                goodsName : selectProduct.productName,
                period : optionsPeriod,
                appendData : appandData,
                callback : _callbacktvPayPurchase
            };


            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_POPUP
            });

            if(tvPayManager === undefined || tvPayManager === null) {
                log.printDbg("tvPayPurchase() tvPayManager : " + tvPayManager);
            }else {
                tvPayManager.makeBHPayment(options);
            }

        };
        
        this.bizMobilePurchase = function() {
            log.printDbg("bizMobilePurchase()");

            var authAmount = "";
            var period = "";

            var periodList =  selectProduct.period.split("|");
            var vatProductPriceList = selectProduct.vatProductPrice.split("|");

            if(isChoiceContract === true) {
                var idx = selectMonthlyView.getSelectOptionIndex();
                period = periodList[idx];
                authAmount = vatProductPriceList[idx];

            }else {
                period = periodList[0];
                authAmount = vatProductPriceList[0];
            }

            var params = {
                product_name  : selectProduct.productName,
                product_price : authAmount,
                period : period,
                callback : _callbackBizMobilePurchase
            };

            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        data : params
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });



        };

        function _getPackageCode() {
            var pkgCode = null;
            log.printDbg("_getPackageCode()");
            if(selectProduct !== undefined && selectProduct !== null &&
                selectProduct.pkgCode !== undefined && selectProduct.pkgCode !== null) {
                pkgCode = selectProduct.pkgCode;

            }

            log.printDbg("_getPackageCode() return pkgCode : " + pkgCode);
            return pkgCode;
        }

        function _responseHDS(success , xhr , error_message) {
            LayerManager.stopLoading();

            log.printDbg("_responseHDS() success : " + success);

            if(isUpselling === true) {
                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_UPGRADE_PROCESSING_POPUP
                });
            }

            try {
                if (success) {
                    var x = xhr.responseXML.getElementsByTagName("string");
                    var startIndex = x[0].textContent.indexOf("returnVal^True");

                    if (startIndex >= 0) {

                        if(isUpselling === false) {
                            LayerManager.deactivateLayer({
                                id: PAYMENT_CHANNEL_PURCHASE_POPUP
                            });

                            /**
                             * OTV 부가상품 가입 완료 후 pkgCode update 처리 함.
                             */
                            AppServiceManager.updatePkgInfo(_callbackUpdatePkgInfo);
                        }else {
                            LayerManager.deactivateLayer({
                                id: PAYMENT_CHANNEL_PURCHASE_POPUP
                            });

                            if(callbackFunc !== null) {
                                callbackFunc(true , isUpselling);
                            }
                        }

                    } else {
                        var messageIndex = x[0].textContent.indexOf("returnDESC^") + 11 ;
                        var message = x[0].textContent.substring(messageIndex);
                        //message = "이 전용 메뉴의 콘텐츠는 15요금제 이상\n고객님께 무료로 제공됩니다. \n요금제를 업그레이드하셔서\n더 많은 채널과 다양한 유료 VOD를 즐기세요.\n문의 : 국번 없이 100번";
                        //message = "고객님은 다회선 가입 고객으로\n요금제 변경 후 업그레이드가 가능합니다.\n다른 단말에서도 처리가 안되실 경우\n고객센터로 문의 부탁드립니다.\n문의 : 국번 없이 100번";
                        // //  message = "현재 시스템 점검 중으로 상품변경이 되지 않습니다.\n잠시 후 다시 이용해 주세요.";

                        message = message.replace(/\\n/g,'\n');
                        if(message !== null) {
                            var hdsMessageArray = message.split("\n");
                            _showErrorPopup(hdsMessageArray , false);
                        }else {
                            _showErrorPopup(undefined , false);
                        }
                    }
                }else {
                    if(error_message !== undefined && error_message !== null) {
                        if(error_message.error === true) {
                            _showErrorPopup(["서비스가 일시적으로 원활하지 않습니다", "네트워크 상태를 확인해주세요" , "동일 현상 반복 시 고객센터로 문의해" , "주시기 바랍니다" , "" , "문의) 국번없이 100번 (VODE-00016)"] , true);
                        }else {
                            if(callbackFunc !== null) {
                                callbackFunc(false);
                            }
                            LayerManager.deactivateLayer({
                                id: PAYMENT_CHANNEL_PURCHASE_POPUP
                            });
                        }
                    }else {
                        if(callbackFunc !== null) {
                            callbackFunc(false);
                        }
                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_POPUP
                        });
                    }
                }
            } catch (error) {
                log.printErr("catching error:" + error);

                // R6 CEMS
                // 서번 단절 및 예외사항인 경우이기 때문에 CEMS 리포트
                OipfAdadapter.extensionAdapter.notifySNMPError("VODE-00013");

                _showErrorPopup(undefined , false);
            }
        }

        function _optionalUpsellingOTS(success, data){
            log.printDbg("_optionalUpsellingOTS success:" + success + " data:" + log.stringify(data));
            LayerManager.stopLoading();

            if(success === true && data !== undefined && data !== null && data.rsltCd !== undefined && data.rsltCd !== null && data.rsltCd == '00') {
                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_PURCHASE_POPUP
                });

                var params = {
                    callback : _callbackPurchaseSuccessPopup
                };

                LayerManager.activateLayer({
                    obj: {
                        id: PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP,
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            data : params
                        }
                    },
                    moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                    visible: true
                });

            } else {
                _showErrorPopup(undefined , false);
            }
        }

        this.showUpsellingProductDetail = function (productName , ImgUrl) {
            var params = {
                product_name : productName,
                img : ImgUrl ,
                callback :_callbackUpsellingProductDetail
            };

            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_UPSELLING_PRODUCT_DETAIL_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : params
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });



        };




        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "channelpayment_arrange_frame"});

            var topPos = 0;
            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });

            topPos = 220;
            var unfocusStep1Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step1_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 6+93+2
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep1Div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 6+93, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep1Div
            });

            unfocusStep1.root_div = unfocusStep1Div;

            focusStep1Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_step1_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 378,
                        display:"none"
                    }
                },
                parent: popup_container_div
            });
            if(isUpselling === true) {
                selectUpsellingProductView = new ChannelPayment.ChannelPurchase.view.SelectUpsellingProductView(_this);
                selectUpsellingProductView.setParentDiv(focusStep1Div);
                selectUpsellingProductView.create();
            }

            topPos+=(6+93+2);
            topPos-=2;

            var unfocusStep2Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step2_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 2+93+2
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep2Div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 2+93, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep2Div
            });

            unfocusStep2.root_div = unfocusStep2Div;

            if(isUpselling === true) {
                focusStep2Div = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_step2_div",
                        css: {
                            position: "absolute",
                            left: 359, top: topPos-6, width: 1200, height: 416,
                            display:"none"
                        }
                    },
                    parent: popup_container_div
                });

                termsAgreeView = new ChannelPayment.ChannelPurchase.view.TermsAgreeView(_this);
                termsAgreeView.setParentDiv(focusStep2Div);
                termsAgreeView.setStepNumber("2");
                termsAgreeView.create();
            }else {
                focusStep2Div = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_step2_div",
                        css: {
                            position: "absolute",
                            left: 359, top: topPos-6, width: 1200, height: 294,
                            display:"none"
                        }
                    },
                    parent: popup_container_div
                });
                
                if(isChoiceContract === true) {
                    selectMonthlyView = new ChannelPayment.ChannelPurchase.view.SelectMonthlyView(_this);
                    var contractInfoList = [];
                    if(KTW_ENV.IS_OTS === true) {
                        var length = selectProduct.details[selectProduct.detailIndex].contract.length;
                        if(length !== undefined) {

                            for(var xx = 0; xx<length;xx++) {
                                contractInfoList[contractInfoList.length] = selectProduct.details[selectProduct.detailIndex].contract[xx];
                            }
                        }else {
                            contractInfoList[contractInfoList.length] = selectProduct.details[selectProduct.detailIndex].contract;
                        }
                    }else {

                        if(selectProduct.pkgGubun !== 4 && selectProduct.contractInfo !== null) {
                            var length = selectProduct.contractInfo.length;
                            if(length !== undefined) {
                                for(var xx = 0; xx<length;xx++) {
                                    contractInfoList[contractInfoList.length] = selectProduct.contractInfo[xx];
                                }
                            }else {
                                contractInfoList[contractInfoList.length] = selectProduct.contractInfo;
                            }
                        }
                    }

                    if(isBiz === true) {
                        selectMonthlyView.setBizPeriodInfo(selectProduct.period , selectProduct.vatProductPrice , isBiz);
                    }else {
                        selectMonthlyView.setContractInfo(contractInfoList);
                    }

                    selectMonthlyView.setParentDiv(focusStep2Div);
                    selectMonthlyView.create();
                }

            }

            topPos+=(2+93+2);
            topPos-=2;

            var unfocusStep3Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step3_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 2+93+2
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            var top_line = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep3Div
            });

            var bottom_line = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 2+93, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep3Div
            });

            unfocusStep3.root_div = unfocusStep3Div;
            unfocusStep3.top_line = top_line;
            unfocusStep3.bottom_line = bottom_line;


            if(isUpselling === true) {
                focusStep3Div = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_step3_div",
                        css: {
                            position: "absolute",
                            left: 359, top: topPos-6, width: 1200, height: 289,
                            display:"none"
                        }
                    },
                    parent: popup_container_div
                });
            }else {
                focusStep3Div = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_step3_div",
                        css: {
                            position: "absolute",
                            left: 359, top: topPos-6, width: 1200, height: 416,
                            display:"none"
                        }
                    },
                    parent: popup_container_div
                });
            }

            if(isUpselling === true) {
                mobilePhoneAuthView = new ChannelPayment.ChannelPurchase.view.MobilePhoneAuthView(_this);
                mobilePhoneAuthView.setParentDiv(focusStep3Div);
                mobilePhoneAuthView.create();
            }else {
                termsAgreeView = new ChannelPayment.ChannelPurchase.view.TermsAgreeView(_this);
                termsAgreeView.setParentDiv(focusStep3Div);
                termsAgreeView.setStepNumber("3");
                termsAgreeView.create();
            }

            topPos+=(2+93+2);
            topPos-=2;

            var unfocusStep4Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "unfocus_step4_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos, width: 1200, height: 2+93+6
                    }
                },
                parent: popup_container_div
            });

            unfocusTopPosition[unfocusTopPosition.length] = topPos;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 2,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep4Div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 2+93, width: 1200, height: 6,
                        "background-color": "rgba(67, 67, 67, 0.8)"
                    }
                },
                parent: unfocusStep4Div
            });

            unfocusStep4.root_div = unfocusStep4Div;

            focusStep4Div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_step4_div",
                    css: {
                        position: "absolute",
                        left: 359, top: topPos-6, width: 1200, height: isBiz === true ? 473 : 280,
                        display:"none"
                    }
                },
                parent: popup_container_div
            });

            pinInputView = new ChannelPayment.ChannelPurchase.view.PinInputView(_this , isBiz);
            pinInputView.setParentDiv(focusStep4Div);
            pinInputView.create();


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 0 , top: 164 , width: KTW_ENV.RESOLUTION.WIDTH, height: 34,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:"","letter-spacing":-1.65
                    }
                },
                text: purchaseTitle,
                parent: popup_container_div
            });

            _createUnFocusStep1DivElement();
            _createUnFocusStep2DivElement();
            _createUnFocusStep3DivElement();
            _createUnFocusStep4DivElement();
        }

        function _createUnFocusStep1DivElement(){

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 5 , top: 29 , width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45 , "text-align": "left"
                    }
                },
                text: "1",
                parent: unfocusStep1.root_div
            });


            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 61 , top: 38 , width: 120, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: unfocusStep1.root_div
            });

            var commentDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 178, top: 32, height: 40,
                        "background-color": "rgba(70, 70, 70, 0.8)",
                        display:"none"
                    }
                },
                parent: unfocusStep1.root_div
            });

            var textComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_comment",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 0 , top: 7 , height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: commentDiv
            });

            var textUpsellingComment = null;
            if(isUpselling === true) {
                textUpsellingComment = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_upselling_comment",
                        class: "channelpayment_font_m",
                        css: {
                            position: "absolute", left: 600 , top: 37 , width: 600, height: 28,
                            color: "rgba(255, 255, 255, 0.3)", "font-size": 26 , "text-align": "right","letter-spacing":-1.3
                        }
                    },
                    text: (KTW_ENV.IS_OTS === true && KTW_DEF.IS_VAT_PRICE === false) ? "3년 약정 기준 (부가세별도)" : "3년 약정 기준 (부가세 포함)",
                    parent: unfocusStep1.root_div
                });
            }

            unfocusStep1.step_title = textStepTitle;
            unfocusStep1.step_comment_div = commentDiv;
            unfocusStep1.step_comment_text = textComment;
            if(isUpselling === true) {
                unfocusStep1.step_comment1_text = textUpsellingComment;

            }
        }

        function _createUnFocusStep2DivElement(){
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 5 , top: 22 , width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45 , "text-align": "left"
                    }
                },
                text: "2",
                parent: unfocusStep2.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 61 , top: 33 , width: 120, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: unfocusStep2.root_div
            });

            var commentDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 178, top: 26, height: 40,
                        "background-color": "rgba(70, 70, 70, 0.8)",
                        display:"none"
                    }
                },
                parent: unfocusStep2.root_div
            });

            var textComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_comment",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 0 , top: 7 , height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: commentDiv
            });

            unfocusStep2.step_title = textStepTitle;
            unfocusStep2.step_comment_div = commentDiv;
            unfocusStep2.step_comment_text = textComment;
        }

        function _createUnFocusStep3DivElement(){
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 5 , top: 22 , width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45 , "text-align": "left"
                    }
                },
                text: "3",
                parent: unfocusStep3.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 61 , top: 33 , width: 145, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: unfocusStep3.root_div
            });

            var commentDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 178, top: 26, height: 40,
                        "background-color": "rgba(70, 70, 70, 0.8)",
                        display:"none"
                    }
                },
                parent: unfocusStep3.root_div
            });

            var textComment = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_comment",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 0 , top: 7 , height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: commentDiv
            });

            unfocusStep3.step_title = textStepTitle;
            unfocusStep3.step_comment_div = commentDiv;
            unfocusStep3.step_comment_text = textComment;
        }

        function _createUnFocusStep4DivElement(){
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 5 , top: 22 , width: 30, height: 47,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 45 , "text-align": "left"
                    }
                },
                text: "4",
                parent: unfocusStep4.root_div
            });

            var textStepTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 61 , top: 33 , width: 120, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: isBiz === true ? "결제 옵션" : "구매 인증",
                parent: unfocusStep4.root_div
            });


            unfocusStep4.step_title = textStepTitle;

            if(isBiz === false) {
                var tempdiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute",
                            left: 901, top: 18, width:300,height: 62 , opacity: 0.3
                        }
                    },
                    parent: unfocusStep4.root_div
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "password_box_bg_img" ,
                        src: resourcePath +"inputbox_unfocus/pop_pw_box_w300.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 300,
                            height: 62
                        }
                    },
                    parent: tempdiv
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "step_title",
                        class: "channelpayment_font_m",
                        css: {
                            position: "absolute", left: 0 , top: 19 , width: 300, height: 42,
                            color: "rgba(255, 255, 255, 1)", "font-size": 42 , "text-align": "center","letter-spacing":-2.1
                        }
                    },
                    text: "* * * *",
                    parent: tempdiv
                });
            }
        }

        /**
         * 2017.11.17 sw.nam
         * 가입 팝업 에서 나가기 키 입력 시 hide가 호출 되지 않아 구매 인증팝업 PIN 입력 창에 포커스 된 상태에서
         * 음성 핀 UI 를 띄운 후에 나가기 키로 화면을 벗어난 경우 speechRecognizer 의  stop이 호출되지 않는 이슈가 발생 (WEBIIIHOME-3516)
         * 구매 인증 팝업에서 나가기 키를 누르는 경우 pinInputView 의 hide 가 호출되도록 수정
         * @param options
         */
        function _hideView() {
            log.printDbg("_hideView()");
            pinInputView.hide();
        }

        function _showView() {
            log.printDbg("_showView()");

            if(initshow === false) {
                initshow = true;
                if(isUpselling === true) {
                    unfocusStep1.step_title.text("상품 선택");
                    unfocusStep1.step_title.css({width:122});
                    unfocusStep1.step_comment_div.css("display" , "none");

                    unfocusStep2.step_title.text("가입 안내");
                    unfocusStep2.step_comment_div.css("display" , "none");

                    unfocusStep3.step_title.text("휴대폰 인증");
                    unfocusStep3.step_comment_div.css("display" , "none");

                    unfocusStep4.step_title.text("구매인증");

                }else {
                    unfocusStep1.step_title.text("구매 상품");
                    unfocusStep1.step_comment_div.css("display" , "none");

                    if(isBiz === true) {
                        unfocusStep2.step_title.text("시청 기간");
                    }else {
                        unfocusStep2.step_title.text("약정 기간");
                    }
                    unfocusStep2.step_comment_div.css("display" , "none");

                    unfocusStep3.step_title.text("가입 안내");
                    unfocusStep3.step_comment_div.css("display" , "none");

                    if(isBiz === true) {
                        unfocusStep4.step_title.text("결제 옵션");
                    }else {
                        unfocusStep4.step_title.text("구매 인증");
                    }

                }
                if(isUpselling === false) {
                    if(selectProduct !== null) {
                        var productName = "";
                        if(KTW_ENV.IS_OTS === true) {
                            productName = selectProduct.details[selectProduct.detailIndex].name;
                        }else {
                            if(isBiz === true) {
                                /**
                                 * 가격 표시 하기 어려움.(확인 필요함)
                                 */
                                productName = selectProduct.productName;

                            }else {
                                productName = selectProduct.productName;
                            }
                        }

                        var length = util.getTextLength(productName , "RixHead M" , 27,-1.35);
                        unfocusStep1.step_title.text("구매 상품 :");
                        unfocusStep1.step_comment_div.css({width:(length+19*2)});
                        unfocusStep1.step_comment_div.css({left:182});
                        unfocusStep1.step_comment_text.css({width:(length+19*2)});
                        unfocusStep1.step_comment_text.text(productName);
                        unfocusStep1.step_comment_div.css("display" , "");
                    }else {
                        unfocusStep1.step_title.text("구매 상품");
                        unfocusStep1.step_comment_div.css("display" , "none");
                    }


                    if(isChoiceContract === true) {
                        unfocusStep2.step_title.text("약정 기간");
                        unfocusStep2.step_comment_div.css("display" , "none");
                    }else {
                        if(isBiz === true) {
                            unfocusStep2.step_title.text("시청 기간 :");

                            var tempstr = null;
                            var tempperiod = null;
                            var period = Number(selectProduct.period);
                            if(period<24) {
                                tempperiod =  period + "시간";
                            }else {
                                var temp = Math.ceil(period / 24) ;
                                tempperiod =  temp + "일";
                            }

                            tempstr = tempperiod + " | " + util.setComma(Number(selectProduct.vatProductPrice))+"원";

                            length = util.getTextLength(tempstr , "RixHead M" , 27,-1.35);

                            unfocusStep2.step_comment_div.css({width:(length+19*2)});
                            unfocusStep2.step_comment_div.css({left:182});
                            unfocusStep2.step_comment_text.css({width:(length+19*2)});
                            unfocusStep2.step_comment_text.text(tempstr);
                            unfocusStep2.step_comment_div.css("display" , "");

                        }else {
                            length = util.getTextLength("약정 없음" , "RixHead M" , 27,-1.35);
                            unfocusStep2.step_title.text("약정 기간 :");
                            unfocusStep2.step_comment_div.css({width:(length+19*2)});
                            unfocusStep2.step_comment_div.css({left:182});
                            unfocusStep2.step_comment_text.css({width:(length+19*2)});
                            unfocusStep2.step_comment_text.text("약정 없음");
                            unfocusStep2.step_comment_div.css("display" , "");

                        }
                    }
                }

                _showFocusStep();
            }
        }

        function _showFocusStep() {

            focusStep1Div.css("display" , "none");
            focusStep2Div.css("display" , "none");
            focusStep3Div.css("display" , "none");
            focusStep4Div.css("display" , "none");

            /**
             * 음성 핀 초기화
             */
            pinInputView.hide();

            if(focusStep === 1) {
                unfocusStep1.root_div.css({top:unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display" , "none");

                focusStep1Div.css("display" , "");

                var selectIndex = selectUpsellingProductView.getSelectProductIndex();
                //
                // if(selectIndex <0) {
                //     selectIndex = 0;
                // }

                selectUpsellingProductView.setPackageList(pkgList , selectIndex);
                selectUpsellingProductView.show();

                var tempY = unfocusTopPosition[0];
                tempY += 378;
                tempY-=2;
                unfocusStep2.root_div.css({top:tempY});
                unfocusStep2.root_div.css("display" , "");

                tempY+=(2+93+2);
                tempY-=2;
                unfocusStep3.root_div.css({top:tempY});
                unfocusStep3.root_div.css("display" , "");

                tempY+=(2+93+2);
                tempY-=2;
                unfocusStep4.root_div.css({top:tempY});
                unfocusStep4.root_div.css("display" , "");


            }else if(focusStep === 2) {
                unfocusStep1.root_div.css({top:unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display" , "");

                unfocusStep2.root_div.css({top:unfocusTopPosition[1]});
                unfocusStep2.root_div.css("display" , "none");

                focusStep2Div.css("display" , "");

                if(isUpselling === true) {
                    descStr = null;

                    if(selectProduct !== undefined && selectProduct !== null && selectProduct.upPkgInfoW3 !== undefined && selectProduct.upPkgInfoW3 !== null) {
                        descStr = selectProduct.upPkgInfoW3.trim();
                    }

                    //descStr = "① 본 상품 가입 시 미드나잇 채널을 무제한 이용할 수 있습니다.\r\n② 가입 후 매월 자동 청구됩니다.\r\n③ 사용요금은 일할 청구되며, 당일 가입/해지 시 당월 내 재가입 불가합니다.\r\n④ 약정가입자는 만료일 이전 해지 시 할인반환금이 청구되며, 약정기간 종료 시 무약정으로 자동 전환됩니다.\r\n⑤ 본 상품은 표기/광고된 내용과 다를 시, 가입일로부터 3개월 이내 청약 철회할 수 있습니다.\r\n※ 상세 : 홈페이지(tv.olleh.com)";

                    // if(descStr !== null) {
                    //     descStr = descStr.replace(/\r/g,''); //엔터 제거
                    // }
                    termsAgreeView.setTermsDesc(descStr);

                    termsAgreeView.show();
                }else {
                    selectMonthlyView.show();
                }

                for(var i=0;i<unfocusTopPosition.length;i++) {
                    log.printDbg("unfocusTopPosition["+ i + "] : " + unfocusTopPosition[i]);
                }

                var tempY = unfocusTopPosition[focusStep-1];
                if(isUpselling === true) {
                    tempY += 416;
                }else {
                    tempY += 294;
                }
                tempY -= 6;
                unfocusStep3.root_div.css({top:tempY});
                log.printDbg("unfocusStep3 TOP : " + tempY);
                unfocusStep3.root_div.css("display" , "");

                tempY+=(2+93+2);
                tempY-=2;
                unfocusStep4.root_div.css({top:tempY});
                log.printDbg("unfocusStep4 TOP : " + tempY);
                unfocusStep4.root_div.css("display" , "");
            }else if(focusStep === 3) {
                unfocusStep1.root_div.css({top:unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display" , "");

                unfocusStep2.root_div.css({top:unfocusTopPosition[1]});
                unfocusStep2.root_div.css("display" , "");

                unfocusStep3.root_div.css({top:unfocusTopPosition[2]});
                unfocusStep3.root_div.css("display" , "none");

                focusStep3Div.css("display" , "");

                if(isUpselling === true) {

                    // if(mobilePhoneAuthView.getIsMobilePhoneAuth() === true) {
                    //     unfocusStep3.top_line.css({"background-color": "rgb(209, 67, 63)"});
                    //     unfocusStep3.bottom_line.css({"background-color": "rgb(209, 67, 63)"});
                    //
                    // }else {
                    //     unfocusStep3.top_line.css({"background-color": "rgba(67, 67, 67, 0.8)"});
                    //     unfocusStep3.bottom_line.css({"background-color": "rgba(67, 67, 67, 0.8)"});
                    // }

                    mobilePhoneAuthView.show();
                }else {
                    descStr = null;
                    if(KTW_ENV.IS_OTS === true) {
                        descStr = selectProduct.clause.trim();
                    }else {
                        log.printDbg("_showFocusStep() isChoiceContract : " + isChoiceContract);
                        if(isChoiceContract === true) {
                            if(isBiz === true) {
                                if(selectProduct !== undefined && selectProduct !== null && selectProduct.pkgDesc !== undefined && selectProduct.pkgDesc !== null) {
                                    descStr = selectProduct.pkgDesc;
                                    log.printDbg("_showFocusStep() biz selectProduct.contractInfo[selectIndex].pkgDesc : " + descStr);
                                }
                            }else {
                                if(selectProduct !== undefined && selectProduct !== null
                                    && selectProduct.contractInfo !== undefined && selectProduct.contractInfo !== null && selectProduct.contractInfo.length>0) {
                                    var selectIndex = selectMonthlyView.getSelectOptionIndex();
                                    log.printDbg("_showFocusStep() selectMonthlyView.getSelectOptionIndex() : " + selectIndex);
                                    if(selectProduct.contractInfo[selectIndex] !== undefined && selectProduct.contractInfo[selectIndex] !== null) {
                                        descStr = selectProduct.contractInfo[selectIndex].contractDescW3;
                                        log.printDbg("_showFocusStep() selectProduct.contractInfo[selectIndex].contractDescW3 : " + descStr);
                                    }
                                }
                            }
                        }else {
                            if(isBiz === true) {
                                if(selectProduct !== undefined && selectProduct !== null && selectProduct.pkgDesc !== undefined && selectProduct.pkgDesc !== null) {
                                    descStr = selectProduct.pkgDesc;
                                    log.printDbg("_showFocusStep() biz selectProduct.contractInfo[selectIndex].pkgDesc : " + descStr);
                                }

                            }else {
                                if(selectProduct !== undefined && selectProduct !== null && selectProduct.pkgDescW3 !== undefined && selectProduct.pkgDescW3 !== null) {
                                    descStr = selectProduct.pkgDescW3;
                                    log.printDbg("_showFocusStep() selectProduct.contractInfo[selectIndex].pkgDescW3 : " + descStr);
                                }

                            }
                        }
                    }

                    // if(descStr !== null) {
                    //     descStr = descStr.replace(/\r/g,''); //엔터 제거
                    // }
                    //descStr = "① 본 상품 가입 시 미드나잇 채널을 무제한 이용할 수 있습니다.\r\n② 가입 후 매월 자동 청구됩니다.\r\n③ 사용요금은 일할 청구되며, 당일 가입/해지 시 당월 내 재가입 불가합니다.\r\n④ 약정가입자는 만료일 이전 해지 시 할인반환금이 청구되며, 약정기간 종료 시 무약정으로 자동 전환됩니다.\r\n⑤ 본 상품은 표기/광고된 내용과 다를 시, 가입일로부터 3개월 이내 청약 철회할 수 있습니다.\r\n※ 상세 : 홈페이지(tv.olleh.com)";

                    termsAgreeView.setTermsDesc(descStr);
                    termsAgreeView.show();
                }


                var tempY = unfocusTopPosition[focusStep-1];
                if(isUpselling === true) {
                    if(mobilePhoneAuthView.getIsMobilePhoneAuth()) {
                        tempY += 147;
                    }else {
                        tempY += 289;
                    }
                }else {
                    tempY += 416;
                }
                tempY -= 6;
                unfocusStep4.root_div.css({top:tempY});
                unfocusStep4.root_div.css("display" , "");
            }else if(focusStep === 4) {
                unfocusStep1.root_div.css({top:unfocusTopPosition[0]});
                unfocusStep1.root_div.css("display" , "");

                unfocusStep2.root_div.css({top:unfocusTopPosition[1]});
                unfocusStep2.root_div.css("display" , "");

                unfocusStep3.root_div.css({top:unfocusTopPosition[2]});
                unfocusStep3.root_div.css("display" , "");


                unfocusStep4.root_div.css({top:unfocusTopPosition[3]});
                unfocusStep4.root_div.css("display" , "none");

                focusStep4Div.css("display" , "");


                var price = "";
                if(isUpselling === true) {
                    price = selectProduct.vatUpPrice;
                }else {
                    if(isBiz === true) {
                        if(isChoiceContract === true) {
                            var selectIndex = selectMonthlyView.getSelectOptionIndex();

                            var vatProductPriceList = selectProduct.vatProductPrice.split("|");

                            if(vatProductPriceList !== null && vatProductPriceList.length>0) {
                                price = vatProductPriceList[selectIndex];
                            }
                        }else {
                            var vatProductPriceList = selectProduct.vatProductPrice.split("|");

                            if(vatProductPriceList !== null && vatProductPriceList.length>0) {
                                price = vatProductPriceList[0];
                            }
                        }

                    }else {
                        if(isChoiceContract === true) {
                            var selectIndex = selectMonthlyView.getSelectOptionIndex();
                            if(KTW_ENV.IS_OTS){
                                /**
                                 * jjh1117 2018/01/11 이 부분은 추후 연동 규격를 전달 받아 다시 적용 해야 함.
                                 * OTS는 아직 vat 포함된 data가 아님.
                                 */
                                price = selectProduct.details[selectProduct.detailIndex].contract[selectIndex].pricevat;
                            }else {
                                price =  selectProduct.contractInfo[selectIndex].vatContractPrice;
                            }
                        }else {
                            price = selectProduct.vatProductPrice;
                        }
                    }
                }

                pinInputView.setBuyPrice(price);
                pinInputView.show();
            }
        }

        function _callbacktvPayPurchase(resultCode, resultMessage) {
            log.printDbg("_callbacktvPayPurchase() resultCode : " + resultCode + " , resultMessage : " + resultMessage);
            // LayerManager.deactivateLayer({
            //     id: PAYMENT_CHANNEL_PURCHASE_POPUP
            // });

            // if(resultCode === "0") {
            //     /**
            //      * OTV 부가상품 가입 완료 후 pkgCode update 처리 함.
            //      */
            //     AppServiceManager.updatePkgInfo(_callbackUpdatePkgInfo);
            // }

            // _callbackUpdatePkgInfo();

        }
        
        function _callbackBizMobilePurchase(ret , mobileNumber) {
            if(ret === false) {
                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
                });

                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_PURCHASE_POPUP
                });
            }else {
                LayerManager.startLoading({
                    preventKey: true
                });

                var itemCode = _getPackageCode();
                var itemName = selectProduct.productName;
                var purchaseMobileNumber = mobileNumber;
                var pkgYn = "H";

                var authAmount = "";
                var period = "";
                var catId = selectProduct.serviceId;
                var ltFlag = selectProduct.ltFlag;

                var periodList =  selectProduct.period.split("|");
                var vatProductPriceList = selectProduct.vatProductPrice.split("|");

                if(isChoiceContract === true) {
                    var idx = selectMonthlyView.getSelectOptionIndex();
                    period = periodList[idx];
                    authAmount = vatProductPriceList[idx];

                }else {
                    period = periodList[0];
                    authAmount = vatProductPriceList[0];
                }

                AmocManager.usedMobilePay(_callbackAMOCMobilePay , itemCode ,itemName ,authAmount, purchaseMobileNumber,period,catId , ltFlag );
            }
        }

        function _callbackAMOCMobilePay(result, data) {
            LayerManager.stopLoading();

            if(result === true) {
                if(data === null) {
                    LayerManager.deactivateLayer({
                        id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
                    });

                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }

                    LayerManager.deactivateLayer({
                        id: PAYMENT_CHANNEL_PURCHASE_POPUP
                    });
                }else {
                    if(data.responseCode === "0000") {
                        _showBizMobilePurchaseSMSSendSuccess();
                    }else if(data.responseCode === "R8100") {
                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
                        });
                        
                        
                        _showErrorPopup(undefined , false);
                    }else if(data.responseCode === "R8101") {
                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
                        });
                        
                        _showErrorPopup(undefined , false);
                    }else {
                        if(data.detailResponseMessage !== null) {
                            var msgList = data.responseMessage.split("\n");
                            if(msgList.length>2) {

                            }else {
                                msgList = data.responseMessage.split("\\n");
                            }

                            LayerManager.deactivateLayer({
                                id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
                            });
                            _showErrorPopup(msgList , false);
                        }
                    }
                }

            }else {
                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
                });

                if(callbackFunc !== null) {
                    callbackFunc(false);
                }

                LayerManager.deactivateLayer({
                    id: PAYMENT_CHANNEL_PURCHASE_POPUP
                });
            }
        }


        function _showBizMobilePurchaseSMSSendSuccess() {
            var isCalled = false;


            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP
            });

            if(callbackFunc !== null) {
                callbackFunc(false);
            }

            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_POPUP
            });

            var popupData = {
                arrMessage: [],
                arrButton: [{id: "ok", name: "확인"}],
                isShowSaid : false,
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                        });


                    }
                }
            };

            popupData.arrMessage.push({
                type: "Title",
                message: ["휴대폰 결제"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_38",
                message: ["문자가 전송되었습니다" , "휴대폰에서 결제를 진행해 주세요"],
                cssObj: {}
            });



            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : popupData
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });

        }



        function _callbackTermsAgreeDetail(ret) {
            if(ret === true) {
                termsAgreeView.setIsTermsAgree(true);
            }
            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_TERMS_AGREE_DETAIL_POPUP
            });
        }

        function _showErrorPopup(message , isShowSaid) {
            var isCalled = false;

            if(callbackFunc !== null) {
                callbackFunc(false);
            }

            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_POPUP
            });

            var popupData = {
                arrMessage: [],
                arrButton: [{id: "ok", name: "확인"}],
                isShowSaid : isShowSaid,
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP
                        });

                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_MOBILE_PHONE_AUTH_POPUP
                        });

                        LayerManager.deactivateLayer({
                            id: PAYMENT_CHANNEL_PURCHASE_POPUP
                        });
                    }
                }
            };

            popupData.arrMessage.push({
                type: "Title",
                message: ["알림"],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_42",
                message: message === undefined ? ["가입한 상품에서는 구매할 수 없습니다"] : message,
                cssObj: {}
            });

            if(message === undefined ) {
                popupData.arrMessage.push({
                    type: "MSG_30_DARK",
                    message: ["문의:국번 없이 100번"],
                    cssObj: {}
                });
            }


            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : popupData
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });

        }

        /**
         * OTV 부가상품 가입 완료 후 pkgCode update 처리 함.
         */
        function _callbackUpdatePkgInfo() {
            var params = {
                callback : _callbackPurchaseSuccessPopup
            };
            LayerManager.activateLayer({
                obj: {
                    id: PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data : params
                    }
                },
                moduleId: Module.ID.MODULE_CHANNEL_PAYMENT,
                visible: true
            });
        }


        function _callbackPurchaseSuccessPopup(ret) {
            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_SUCCESS_POPUP
            });

            if(callbackFunc !== null) {
                callbackFunc(true , isUpselling);
            }
        }

        function _callbackUpsellingProductDetail() {

            LayerManager.deactivateLayer({
                id: PAYMENT_CHANNEL_PURCHASE_UPSELLING_PRODUCT_DETAIL_POPUP
            });
        }

    };

    channelPurchasePopup.prototype = new Layer();
    channelPurchasePopup.prototype.constructor = channelPurchasePopup;
    channelPurchasePopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    channelPurchasePopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    channelPurchasePopup.prototype.hide = function(options) {
        Layer.prototype.hide.call(this);
        this.hideView(options);
    };

    channelPurchasePopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_POPUP] = channelPurchasePopup;
})();