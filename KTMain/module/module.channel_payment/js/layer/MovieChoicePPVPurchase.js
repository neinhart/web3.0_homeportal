/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    var movieChoicePPVPurchase = function movieChoicePPVPurchase(options) {
        Layer.call(this, options);
        var div;
        var optionList = [];
        var focus = 0;
        var backgroundDiv = null;

        this.init = function(cbCreate) {
            div = this.div;

            backgroundDiv = Util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width : 1920 ,
                        height : 1080,
                        "background-color": "rgba(255,255,255,1)",
                        display : ""
                    }
                },
                parent: div
            });

            if(cbCreate) cbCreate(true);


        };

        this.showView = function(options) {
//            backgroundDiv.css("display" , "");
            console.log("movieChoicePPVPurchase.showView()]");
        };



        this.controlKey = function(keyCode) {
//            if(optionList[focus].onKeyAction(keyCode)) return true;
            switch(keyCode) {
                case KTW_KEY_CODE.DOWN:
                    return true;
                case KTW_KEY_CODE.UP:
                    return true;
                case KTW_KEY_CODE.BACK:
                    LayerManager.deactivateLayer({
                        id: "MovieChoicePPVPurchase",
                        remove: true
                    });


                    LayerManager.activateLayer({
                        obj: {
                            id: Layer.ID.MINI_EPG,
                            type: Layer.TYPE.TRIGGER,
                            priority: Layer.PRIORITY.NORMAL,
                            linkage: true,
                            params: {
                                fromKey:true
                            }
                        },
                        clear_normal: true,
                        visible: true,
                        cbActivate: function() {

                        }
                    });
                    return true;
            }
        };
    };

    movieChoicePPVPurchase.prototype = new Layer();
    movieChoicePPVPurchase.prototype.constructor = movieChoicePPVPurchase;

    //Override create function
    movieChoicePPVPurchase.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    movieChoicePPVPurchase.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    movieChoicePPVPurchase.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_MOVICECHOICE_PPV_PURCHASE] = movieChoicePPVPurchase;
})();