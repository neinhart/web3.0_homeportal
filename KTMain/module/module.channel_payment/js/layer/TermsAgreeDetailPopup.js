/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>TermsAgreeDetailPopup</code>
 * params : {
 *  terms_desc :
 *  callback : 약관동의 시 전달 할 callbackfunc
 * }
 * 약관 상세 팝업
 * @author jjh1117
 * @since 2016. 12. 20.
 */

"use strict";

(function() {
    var termsAgreeDetailPopup = function TermsAgreeDetailPopup(options) {
        Layer.call(this, options);

        var util = Util;
        var log = Log;

        var FOCUS_TERMS_AGREE = 0;
        var FOCUS_CANCEL = 1;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var termsAgreeDesc = "";
        var callbackFunc = null;


        var btnTermsAgreeOk = null;
        var btnTermsAgreeCancel = null;

        var textDesc = null;

        var scrollBarBackground = null;
        var scrollBar = null;

        var focusIndex = 0;

        var pageTotal = 0;
        var scrollBarTop = 0;
        var pageIndex = -1;

        var initShow = false;

        var termsAgreeDetailPopupResourcePath = resourcePath + "termsagreedetailpopup/";

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();
            if(params !== null) {
                termsAgreeDesc = params.data.terms_desc;
                callbackFunc = params.data.callback;
            }
            div = this.div;

            _createView(div , this);
            if(cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("showView()");
            _showView();

        };

        this.hideView = function() {
            log.printDbg("hideView()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroyView()");
        };


        this.controlKey = function(key_code) {
            var consumed = false;

            switch (key_code) {
                case KTW_KEY_CODE.UP:
                    if(pageTotal>1) {
                        pageIndex--;
                        if(pageIndex<0) {
                            pageIndex = 0;
                        }
                        textDesc.css({top:0-(423*pageIndex),height:(423*(pageIndex+1))});

                        scrollBar.css({top:(320 + (scrollBarTop*pageIndex))});
                    }

                    consumed = true;
                    break;
                case KTW_KEY_CODE.DOWN:
                    if(pageTotal>1) {
                        pageIndex++;
                        if(pageTotal<=pageIndex) {
                            pageIndex = pageTotal - 1;
                        }

                        textDesc.css({top:0-(423*pageIndex),height:(423*(pageIndex+1))});
                        scrollBar.css({top:(320 + (scrollBarTop*pageIndex))});
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.RIGHT:
                    focusIndex++;
                    if(focusIndex>1) {
                        focusIndex = 0;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    _keyOk();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT:
                    focusIndex--;
                    if(focusIndex<0) {
                         focusIndex = 1;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.BACK:
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.EXIT:
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    break;
            }
            return consumed;
        };


        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "channelpayment_arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });



            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 390, top: 266, width: 1140, height: 510,
                        "background-color": "rgba(48, 47, 47 , 1)" , opacity : 0.8
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "channelpayment_font_b" ,
                    css: {
                        position: "absolute", left: 0, top: 193, width: KTW_ENV.RESOLUTION.WIDTH , height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33,"text-align" : "center","letter-spacing":-1.65
                    }
                },
                text: "약관 상세",
                parent: popup_container_div
            });

            _createBtnElement();

            var tempDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 460 , top: 338 , width: 1000, height: 423
                    }
                },
                parent: popup_container_div
            });

            textDesc = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_desc",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 0 , top: 0 , width: 1000,
                        color: "rgba(255, 255, 255, 1)", "font-size": 26 , "text-align": "left",
                        "white-space":"pre-line", "line-height": 1.5 ,"letter-spacing":-1.3
                    }
                },
                text: "",
                parent: tempDiv
            });


            scrollBarBackground = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1478, top: 316, width: 3, height: 409,
                        "background-color": "rgba(255, 255, 255, 0.14)"
                    }
                },
                parent: popup_container_div
            });


            scrollBar = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1477, top: 316, width: 5, height: 0,
                        "background-color": "rgba(170, 170, 170, 1)"
                    }
                },
                parent: popup_container_div
            });
        }

        function _createBtnElement() {
            var leftStart = 673;
            for(var i=0;i<2;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "btn_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 836, width: 280, height: 62
                        }
                    },
                    parent: popup_container_div
                });

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + "pop_btn_w280.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 280,
                            height: 62
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "channelpayment_font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center","letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var focusText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "channelpayment_font_m" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center","letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    default_text : defaultText,
                    focus_text : focusText
                };
                if(i === FOCUS_TERMS_AGREE) {
                    btnTermsAgreeOk = btnobj;
                    btnTermsAgreeOk.default_text.text("동의");
                    btnTermsAgreeOk.focus_text.text("동의");
                }else {
                    btnTermsAgreeCancel = btnobj;
                    btnTermsAgreeCancel.default_text.text("닫기");
                    btnTermsAgreeCancel.focus_text.text("닫기");
                }

                leftStart+=280;
                leftStart+=12;
            }
        }

        function _showView() {
            log.printDbg("_showView()");
            if(initShow === false) {
                initShow = true;
                if(termsAgreeDesc !== undefined && termsAgreeDesc !== null) {
                    termsAgreeDesc = termsAgreeDesc.replace(/\r/g,''); //엔터 제거
                }

                textDesc.text(termsAgreeDesc);


                setTimeout(function(){
                    //dom 에 add 된 후 처리되어야 할것 같아 timeout 으로 처리.
                    var heightStr = textDesc.css("height");
                    var contentsHeight = Number(heightStr.substring(0, heightStr.indexOf("px")));

                    pageTotal = Math.ceil(contentsHeight/423);
                    scrollBarTop = Math.floor(423/pageTotal);

                    if (pageTotal > 1) {
                        scrollBarBackground.css("display", "");
                        scrollBar.css("display", "");
                        if(scrollBarTop>0) {
                            scrollBar.css({height:scrollBarTop});
                            pageIndex = 0;
                        }else {
                            scrollBarBackground.css("display", "none");
                            scrollBar.css("display", "none");
                        }

                    }else {
                        scrollBarBackground.css("display", "none");
                        scrollBar.css("display", "none");
                    }
                }, 20);
                _focusUpdate();

            }

        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");

            btnTermsAgreeOk.default_btn.css("display" , "");
            btnTermsAgreeOk.focus_btn.css("display" , "none");
            btnTermsAgreeOk.default_text.css("display" , "");
            btnTermsAgreeOk.focus_text.css("display" , "none");

            btnTermsAgreeCancel.default_btn.css("display" , "");
            btnTermsAgreeCancel.focus_btn.css("display" , "none");
            btnTermsAgreeCancel.default_text.css("display" , "");
            btnTermsAgreeCancel.focus_text.css("display" , "none");

            if(focusIndex === FOCUS_TERMS_AGREE) {
                btnTermsAgreeOk.default_btn.css("display" , "none");
                btnTermsAgreeOk.focus_btn.css("display" , "");
                btnTermsAgreeOk.default_text.css("display" , "none");
                btnTermsAgreeOk.focus_text.css("display" , "");
            }else if(focusIndex === FOCUS_CANCEL) {
                btnTermsAgreeCancel.default_btn.css("display" , "none");
                btnTermsAgreeCancel.focus_btn.css("display" , "");
                btnTermsAgreeCancel.default_text.css("display" , "none");
                btnTermsAgreeCancel.focus_text.css("display" , "");
            }
        }

        function _keyOk() {
            log.printDbg("_keyOk()");
            if(focusIndex === FOCUS_TERMS_AGREE) {
                if(callbackFunc !== null) {
                    callbackFunc(true);
                }
            }else {
                if(callbackFunc !== null) {
                    callbackFunc(false);
                }
            }

        }

        function _hideView() {
            log.printDbg("_hideView()");

        }
    };

    termsAgreeDetailPopup.prototype = new Layer();
    termsAgreeDetailPopup.prototype.constructor = termsAgreeDetailPopup;

    termsAgreeDetailPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    termsAgreeDetailPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    termsAgreeDetailPopup.prototype.hide = function() {
        this.hideView();
    };

    termsAgreeDetailPopup.prototype.destroy = function() {
        this.destroyView();
    };

    termsAgreeDetailPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_TERMS_AGREE_DETAIL_POPUP] = termsAgreeDetailPopup;

})();