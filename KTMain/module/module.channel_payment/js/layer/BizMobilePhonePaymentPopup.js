/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>BizMobilePhonePaymentPopup</code>
 * params : {
 *    telNo :
 *    birth :
 *    gender :
 *    nation :
 *    uname :
 *    callback :
 * }
 * 휴대폰 인증 팝업
 * @author jjh1117
 * @since 2016. 12. 21.
 */

"use strict";

(function() {
    var bizmobilePhonePaymentPopup = function BizMobilePhonePaymentPopup(options) {
        Layer.call(this, options);

        var util = Util;
        var log = Log;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var FOCUS_MOBILE_FIRST_NUMBER = 0;
        var FOCUS_MOBILE_MIDDLE_NUMBER = 1;
        var FOCUS_MOBILE_LAST_NUMBER = 2;
        var FOCUS_OK = 3;
        var FOCUS_CANCEL = 4;



        var initShow = false;

        var callbackFunc = null;

        var rootDiv = null;

        var focusIndex = 0;


        var mobileNumber = [];

        var mobileNumberInputString = ["000","0000","0000"];

        var btnOk = null;
        var btnCancel = null;

        var productName = null;
        var productPrice = null;
        var period = null;

        var bizmobilePhonePaymentPopupResourcePath = resourcePath + "bizmobilephonepaymentpopup/";

        var current_year = new Date();

        this.init = function(cbCreate) {

            params = this.getParams();

            if(params !== null) {
                productName = params.data.product_name;
                productPrice = params.data.product_price;
                period = params.data.period;
                callbackFunc = params.data.callback;
            }

            div = this.div;
            _createView(div);
            focusIndex = 0;

            if(cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };


        this.controlKey = function(key_code) {
            var consumed = false;
            switch (key_code) {
                case KTW_KEY_CODE.BACK:
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.EXIT:
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex++;
                        _focusUpdate();
                    }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if(focusIndex === FOCUS_OK) {
                            //checkEmptyElement
                            var isEmptyElement = false;
                            if(mobileNumberInputString[0] === "" || mobileNumberInputString[0].length<3) {
                                focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                                isEmptyElement = true;
                            }else if(mobileNumberInputString[1] === "" || mobileNumberInputString[1].length<4) {
                                focusIndex = FOCUS_MOBILE_MIDDLE_NUMBER;
                                isEmptyElement = true;
                            }else if(mobileNumberInputString[2] === "" || mobileNumberInputString[2].length<4) {
                                focusIndex = FOCUS_MOBILE_LAST_NUMBER;
                                isEmptyElement = true;
                            }

                            if(isEmptyElement) {
                                _focusUpdate();
                                return true;
                            }else {
                                if(callbackFunc !== null) {
                                    callbackFunc(true , mobileNumberInputString[0] + mobileNumberInputString[1] + mobileNumberInputString[2]);
                                }
                            }

                        }else if(focusIndex === FOCUS_CANCEL) {
                            if(callbackFunc !== null) {
                                callbackFunc(false);
                            }
                        }

                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT:
                    if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        if(mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER].length<=0){
                            focusIndex--;
                            if(focusIndex<FOCUS_MOBILE_FIRST_NUMBER) {
                                focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                            }
                        }else {
                            mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER] = mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER].substring(0,mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER].length-1);
                            mobileNumber[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].default_text.text(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER]);
                        }
                    }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if(focusIndex === FOCUS_CANCEL) {
                            focusIndex = FOCUS_OK;
                        }
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.RIGHT:
                    if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex++;
                        if(focusIndex>FOCUS_MOBILE_LAST_NUMBER) {
                            focusIndex = FOCUS_MOBILE_LAST_NUMBER;
                        }
                    }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if(focusIndex === FOCUS_OK) {
                            focusIndex = FOCUS_CANCEL;
                        }
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.UP:
                    if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.DOWN:
                    if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex = FOCUS_OK;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
            }

            if(consumed === false) {
                var number = util.keyCodeToNumber(key_code);
                if (number >= 0) {
                    if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        var maxlength = 0;
                        if((focusIndex - FOCUS_MOBILE_FIRST_NUMBER) === 0) {
                            maxlength = 3;
                        }else {
                            maxlength = 4;
                        }
                        if(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].length>=maxlength) {
                            mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER] = ""+ number;
                        }else {
                            mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER] += number;
                        }

                        mobileNumber[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].default_text.text(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER]);

                        if(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].length>=maxlength) {
                            focusIndex++;
                            if(focusIndex>FOCUS_MOBILE_LAST_NUMBER) {
                                focusIndex = FOCUS_OK;
                            }
                            _focusUpdate();
                        }
                    }
                    consumed = true;
                }
            }

            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "channelpayment_arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 0 , top: 202 , width: KTW_ENV.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center","letter-spacing":-1.65,
                        display:""
                    }
                },
                text: "휴대폰 결제",
                parent: popup_container_div
            });


            rootDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "pw_root_div",
                    css: {
                        position: "absolute",
                        left: 485, top: 429, width: 915, height: 282
                    }
                },
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_l_img",
                    src: bizmobilePhonePaymentPopupResourcePath + "pop_bg_pwreset_phone_l.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 29, height: 282
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_m_img",
                    src: bizmobilePhonePaymentPopupResourcePath + "pop_bg_pwreset_phone_m.png",
                    css: {
                        position: "absolute", left: 29, top: 0, width: (915-29-29) , height: 282
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_r_img",
                    src: bizmobilePhonePaymentPopupResourcePath + "pop_bg_pwreset_phone_r.png",
                    css: {
                        position: "absolute", left: 29 + (915-29-29), top: 0, width: 29, height: 282
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "channel_name",
                    class: "channelpayment_font_m cut",
                    css: {
                        position: "absolute", left: 0 , top: 278 , width: 1920, height: 54,
                        color: "rgba(255, 255, 255, 1)", "font-size": 52 , "text-align": "center",
                        display:"","letter-spacing":-2.6
                    }
                },
                text: productName,
                parent: popup_container_div
            });


            var productInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 353, width: 1920, height: 35,
                        "text-align": "center", "font-size": 30, "letter-spacing": -1.5
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 0.4)"
                    }
                },
                text: "가격 : ",
                parent: productInfoDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: util.setComma(Number(productPrice)) + "원",
                parent: productInfoDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead M", "font-size": 30 , color: "rgba(255, 255, 255, 0.3)" , "margin-left" : 12 , "margin-right" : 12
                    }
                },
                text: "|",
                parent: productInfoDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 0.4)"
                    }
                },
                text: "기간 : ",
                parent: productInfoDiv
            });


            var periodData = Number(period);
            var tempstr = "";
            if(periodData<24) {
                tempstr =  period + "시간 시청 가능";
            }else {
                var temp = Math.ceil(period / 24) ;
                tempstr =  temp + "일 간 시청 가능";
            }

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: tempstr,
                parent: productInfoDiv
            });



            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "channelpayment_font_l cut",
                    css: {
                        position: "absolute", left: 0 , top: (481-429) , width: 915, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center",
                        display:"","letter-spacing":-1.5
                    }
                },
                text: "스마트폰에서만 결제 가능합니다",
                parent: rootDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "channelpayment_font_l cut",
                    css: {
                        position: "absolute", left: 0 , top: (481-429) + 35 , width: 915, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center",
                        display:"","letter-spacing":-1.5
                    }
                },
                text: "결제할 휴대폰 번호를 입력해 주세요",
                parent: rootDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "channelpayment_font_l cut",
                    css: {
                        position: "absolute", left: (595-485) , top: (612-429) , width: 150, height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "left",
                        display:"","letter-spacing":-1.5
                    }
                },
                text: "휴대폰 번호",
                parent: rootDiv
            });



            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 922-485, top: 624-429, width:12, height: 4,
                        "background-color": "rgba(255, 255, 255, 0.55)"
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1110-485, top: 624-429, width:12, height: 4,
                        "background-color": "rgba(255, 255, 255, 0.55)"
                    }
                },
                parent: rootDiv
            });


            var leftStart = (755-485);

            for(var i=0;i<3;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "mobile_number_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: (591-429), width: 158, height: 70
                        }
                    },
                    parent: rootDiv
                });


                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath+"inputbox_unfocus/pop_input_box_w158.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 158,
                            height: 70,
                            opacity:0.3
                        }
                    },
                    parent: btnDiv
                });


                var defaultTitle = "";
                if(i=== 0) {
                    defaultTitle = "000";
                }else {
                    defaultTitle = "0000";
                }

                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "channelpayment_font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 20, width: 158 , height: 35,
                            color: "rgba(255, 255, 255, 01)", "font-size": 33,"text-align" : "center","letter-spacing":-1.65
                        }
                    },
                    text: defaultTitle,
                    parent: btnDiv
                });


                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    default_text : defaultText
                };

                mobileNumber[mobileNumber.length] = btnobj;

                leftStart+=158;
                leftStart+=30;
            }


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 0 , top: 734 , width: KTW_ENV.RESOLUTION.WIDTH, height: 26,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 24 , "text-align": "center","letter-spacing":-1.2,
                        display:""
                    }
                },
                text: "휴대폰 요금청구서에는 소액결제로 표기",
                parent: popup_container_div
            });

            _createBtnElement();
        }



        function _createBtnElement() {
            var leftStart = 673;
            for(var i=0;i<2;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "btn_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 820, width: 280, height: 62
                        }
                    },
                    parent: popup_container_div
                });

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + "pop_btn_w280.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 280,
                            height: 62
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "channelpayment_font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center","letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var focusText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "channelpayment_font_m" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center","letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    default_text : defaultText,
                    focus_text : focusText
                };


                if(i === 0) {
                    btnOk = btnobj;
                    btnOk.default_text.text("문자전송");
                    btnOk.focus_text.text("문자전송");
                }else {
                    btnCancel = btnobj;
                    btnCancel.default_text.text("취소");
                    btnCancel.focus_text.text("취소");
                }

                leftStart+=280;
                leftStart+=12;
            }
        }





        function _resetFocusBtn() {

            btnOk.default_btn.css("display" , "");
            btnOk.default_text.css("display" , "");
            btnOk.focus_btn.css("display" , "none");
            btnOk.focus_text.css("display" , "none");


            btnCancel.default_btn.css("display" , "");
            btnCancel.default_text.css("display" , "");
            btnCancel.focus_btn.css("display" , "none");
            btnCancel.focus_text.css("display" , "none");

        }

        function _focusUpdate() {
            _resetFocusBtn();

            if(focusIndex === FOCUS_MOBILE_FIRST_NUMBER){
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        mobileNumber[i].default_text.text("");
                    }
                }
                mobileNumber[0].default_btn.css({opacity: 1});
                mobileNumber[0].default_btn.attr("src" , resourcePath + "mobilephoneauthpopup/" + "pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

            }else if(focusIndex === FOCUS_MOBILE_MIDDLE_NUMBER){
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        mobileNumber[i].default_text.text("");
                    }
                }
                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 1});
                mobileNumber[1].default_btn.attr("src" , resourcePath + "mobilephoneauthpopup/" + "pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");
            }else if(focusIndex === FOCUS_MOBILE_LAST_NUMBER){
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        mobileNumber[i].default_text.text("");
                    }
                }
                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 1});
                mobileNumber[2].default_btn.attr("src" , resourcePath + "mobilephoneauthpopup/" + "pop_input_box_w158.png");
            }else if(focusIndex === FOCUS_OK){
                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                btnOk.default_btn.css("display" , "none");
                btnOk.default_text.css("display" , "none");
                btnOk.focus_btn.css("display" , "");
                btnOk.focus_text.css("display" , "");
            }else if(focusIndex === FOCUS_CANCEL){
                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , resourcePath+"inputbox_unfocus/pop_input_box_w158.png");

                btnCancel.default_btn.css("display" , "none");
                btnCancel.default_text.css("display" , "none");
                btnCancel.focus_btn.css("display" , "");
                btnCancel.focus_text.css("display" , "");
            }
        }





        function _showView() {
            log.printDbg("_showView()");
            mobileNumberInputString = ["000","0000","0000"];
            if(initShow === false) {
                initShow = true;
                _focusUpdate();
            }
        }



        function _keyOk() {
            log.printDbg("_keyOk()");

        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

    };

    bizmobilePhonePaymentPopup.prototype = new Layer();
    bizmobilePhonePaymentPopup.prototype.constructor = bizmobilePhonePaymentPopup;

    bizmobilePhonePaymentPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    bizmobilePhonePaymentPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };

    bizmobilePhonePaymentPopup.prototype.hide = function(options) {
        Layer.prototype.hide.call(this);
        this.hideView(options);
    };

    bizmobilePhonePaymentPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_BIZ_MOBILE_PHONE_PAYMENT_POPUP] = bizmobilePhonePaymentPopup;

})();