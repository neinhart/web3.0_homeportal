/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AdditionalProductSelectPopup</code>
 * params : {
 *    product_name :
 *    img :
 *    callback :
 * }
 * 업셀링 상품 상세 정보 팝업
 * @author jjh1117
 * @since 2016. 12. 30.
 */

"use strict";

(function() {
    var additionalProductSelectPopup = function AdditionalProductSelectPopup(options) {
        Layer.call(this, options);

        var util = Util;
        var log = Log;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var title = "상품 안내";

        var callbackFunc = null;
        var buttonDiv = null;
        var buttonFocusDiv = null;
        var buttonText = null;

        var additionalProductList = null;

        var DISPLAY_MAX_COUNT = 6;

        var displayProductCount = 0;

        var additionalProductDivList = [];

        var focusIndex = 0;
        var focusIndexY = 0;

        var isBiz = false;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();

            if(params !== null) {
                additionalProductList = params.data.additional_product_list;
                // showImageName = params.data.img;
                callbackFunc = params.data.callback;
                focusIndex = params.data.select_product_index;
                isBiz = params.data.is_biz;
            }

            div = this.div;
            _createView(div);

            if(cbCreate) cbCreate(true);
        };

        this.showView = function() {
            log.printDbg("show()");
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };


        this.controlKey = function(key_code) {
            log.printDbg("controlKey() key_code : " + key_code);
            var consumed = false;
            switch (key_code) {
                case KTW_KEY_CODE.UP:
                    if(focusIndexY === 1) {
                        _unFocusBtnView();
                        focusIndexY = 0;
                        focusIndex = 0;
                        _foucsProductView();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.DOWN:
                    if(focusIndexY === 0) {
                        _unFocusProductView();
                        focusIndexY = 1;
                        _foucsBtnView();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT:
                case KTW_KEY_CODE.RIGHT:
                    if(focusIndexY === 0) {
                        _unFocusProductView();
                        if(key_code === KTW_KEY_CODE.LEFT) {
                            focusIndex--;
                        }else {
                            focusIndex++;
                        }

                        if(focusIndex<0) {
                            focusIndex = displayProductCount-1;
                        }else {
                            if(focusIndex>=displayProductCount) {
                                focusIndex = 0;
                            }
                        }
                        _foucsProductView();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.BACK :
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    if(focusIndexY === 0) {
                        if(callbackFunc !== null) {
                            callbackFunc(true , focusIndex);
                        }
                    }else {
                        if(callbackFunc !== null) {
                            callbackFunc(false);
                        }
                    }
                    consumed = true;
                    break;
            }

            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "channelpayment_arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 0 , top: 262 , width: KTW_ENV.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:"","letter-spacing":-1.65
                    }
                },
                text: title,
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 0 , top: 333 , width: KTW_ENV.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(255, 255, 255, 1)", "font-size": 33 , "text-align": "center",
                        display:"","letter-spacing":-1.65
                    }
                },
                text: "국내 개봉 최신 영화와 미드를 즐길 수 있습니다",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 0 , top: 376 , width: KTW_ENV.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(255, 255, 255, 1)", "font-size": 33 , "text-align": "center",
                        display:"","letter-spacing":-1.65
                    }
                },
                text: "가입 상품 유형을 선택하세요",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "main_desc",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 0 , top: 703 , width: KTW_ENV.RESOLUTION.WIDTH, height: 29,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 27 , "text-align": "center",
                        display:"","letter-spacing":-1.35
                    }
                },
                text: (KTW_ENV.IS_OTS === true && KTW_DEF.IS_VAT_PRICE === false) ? "※ 부가세는 별도 청구됩니다" : "※ 부가세 포함된 가격입니다",
                parent: popup_container_div
            });



            buttonDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 820, top: 766, width: 280, height: 62,
                        border: "solid 2px rgba(190, 190, 190, 1)" , opacity:.5 , "box-sizing" : "border-box"
                    }
                },
                parent: popup_container_div
            });

            buttonFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 820, top: 766, width: 280, height: 62,
                        "background-color": "rgba(210, 51, 47, 1)" , display: "none"
                    }
                },
                parent: popup_container_div
            });

            buttonText = util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 820, top: (766 + 17), width: 280, height: 35, "text-align": "center",
                        "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead L"

                    }
                },
                text: "취소",
                parent: popup_container_div
            });



            if(additionalProductList !== undefined && additionalProductList !== null && additionalProductList.length>0) {
                if(additionalProductList.length>DISPLAY_MAX_COUNT) {
                    displayProductCount = DISPLAY_MAX_COUNT;
                }else {
                    displayProductCount = additionalProductList.length;
                }

                var totalWidth = (displayProductCount*255) + ((displayProductCount-1)*12);
                var startLeft = (KTW_ENV.RESOLUTION.WIDTH - totalWidth) / 2;
                for(var i=0;i<displayProductCount;i++) {

                    var rootDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            css: {
                                position: "absolute", left: startLeft, top: 442, width: 255, height: 228,
                            }
                        },
                        parent: popup_container_div
                    });

                    var defaultDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            css: {
                                position: "absolute", left: 0, top: 0, width: 255, height: 228,
                                "background-color": "rgba(47, 47, 47, 1)" ,
                                border: "solid 2px rgba(77, 77, 77, 1)" , "box-sizing" : "border-box"
                            }
                        },
                        parent: rootDiv
                    });
                    
                    var focusDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class : "channelpayment_focus_red_border_box" ,
                            css: {
                                position: "absolute", left: 0, top: 0, width: 255, height: 228,
                                "background-color": "rgba(47, 47, 47, 1)" ,  display: "none"
                            }
                        },
                        parent: rootDiv
                    });


                    var joinbtnDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            css: {
                                position: "absolute", left: 15, top: 162, width: 225, height: 51,
                                "background-color": "rgba(73, 73, 73, 1)"
                            }
                        },
                        parent: rootDiv
                    });

                    util.makeElement({
                        tag: "<span />",
                        attrs: {
                            css: {
                                position: "absolute", left: 0, top: 12, width: 225, height: 29, "text-align": "center",
                                "font-size": 27, "letter-spacing": -1.35, color: "rgba(255, 255, 255, 0.5)", "font-family": "RixHead M"

                            }
                        },
                        text: "가입신청",
                        parent: joinbtnDiv
                    });



                    var additionalProductName = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "channelpayment_font_l channelpayment_cut",
                            css: {
                                position: "absolute", left: 10 , top: 44 , width: 235, height: 39,
                                color: "rgba(255, 255, 255, 1)", "font-size": 37 , "text-align": "center","letter-spacing":-1.85,
                                display:""
                            }
                        },
                        text: additionalProductList[i].productTitle,
                        parent: rootDiv
                    });


                    var textPrice = "";

                    var priceList = null;
                    if(KTW_ENV.IS_OTS === true && KTW_DEF.IS_VAT_PRICE === false) {
                        priceList = additionalProductList[i].productPrice.split('|');
                    }else {
                        priceList = additionalProductList[i].vatProductPrice.split('|');
                    }

                    if(isBiz === true) {
                        if(priceList !== null && priceList.length>1) {
                            textPrice = "(선택기간)";
                        }else {
                            textPrice = util.setComma(priceList[0]) + "원";
                        }
                    }else {
                        if(priceList !== null) {
                            textPrice ="월 " + util.setComma(priceList[0]) + "원";
                        }
                    }

                    var additionalProductPrice = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "channelpayment_font_m channelpayment_cut",
                            css: {
                                position: "absolute", left: 0 , top: 94 , width: 255, height: 26,
                                color: "rgba(255, 255, 255, 1)", "font-size": 24 , "text-align": "center",
                                display:"","letter-spacing":-1.2
                            }
                        },
                        text:textPrice,
                        parent: rootDiv
                    });



                    startLeft += 255;
                    startLeft+= 12;

                    additionalProductDivList[additionalProductDivList.length] = {
                        root_div : rootDiv,
                        default_div : defaultDiv,
                        focus_div : focusDiv,
                        join_btn_div : joinbtnDiv,
                        product_name : additionalProductName,
                        product_price : additionalProductPrice
                    };
                }
            }
        }



        function _showView() {
            log.printDbg("_showView()");
            _foucsProductView();
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _foucsProductView() {
            additionalProductDivList[focusIndex].default_div.css("display" , "none");
            additionalProductDivList[focusIndex].focus_div.css("display" , "");

            additionalProductDivList[focusIndex].join_btn_div.css( {"background-color": "rgba(210, 51, 47, 1)"});
            additionalProductDivList[focusIndex].join_btn_div.children().css({ color: "rgba(255, 255, 255, 1)"});

            additionalProductDivList[focusIndex].product_name.css({ color: "rgba(255, 255, 255, 1)"});
            additionalProductDivList[focusIndex].product_price.css({ color: "rgba(255, 255, 255, 1)"});
        }

        function _unFocusProductView() {
            additionalProductDivList[focusIndex].default_div.css("display" , "");
            additionalProductDivList[focusIndex].focus_div.css("display" , "none");
            additionalProductDivList[focusIndex].join_btn_div.css( {"background-color": "rgba(73, 73, 73, 1)"});
            additionalProductDivList[focusIndex].join_btn_div.children().css({ color: "rgba(255, 255, 255, 0.5)"});

            additionalProductDivList[focusIndex].product_name.css({ color: "rgba(255, 255, 255, 1)"});
            additionalProductDivList[focusIndex].product_price.css({ color: "rgba(255, 255, 255, 1)"});
        }


        function _foucsBtnView() {
            buttonFocusDiv.css("display" , "");
            buttonDiv.css("display" , "none");
            buttonText.css({ color: "rgba(255, 255, 255, 1)"});
        }

        function _unFocusBtnView() {
            buttonFocusDiv.css("display" , "none");
            buttonDiv.css("display" , "");
            buttonText.css({ color: "rgba(255, 255, 255, 0.7)"});
        }

    };

    additionalProductSelectPopup.prototype = new Layer();
    additionalProductSelectPopup.prototype.constructor = additionalProductSelectPopup;

    additionalProductSelectPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    additionalProductSelectPopup.prototype.show = function(options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };
    additionalProductSelectPopup.prototype.hide = function() {
        this.hideView();
    };
    additionalProductSelectPopup.prototype.destroy = function() {
        this.destroyView();
    };
    additionalProductSelectPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_ADDITIONAL_PRODUCT_SELECT_POPUP] = additionalProductSelectPopup;

})();