/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>BasicPopup.js</code>
 *
 * @author dj.son
 * @since 2016-10-04
 *
 * 부모 Layer 의 params.data 를 확인해서 UI 를 구성하며, params.data 의 내용은 아래와 같다.
 *
 * [arrMessage]
 * arrMessage 는 해당 팝업에 보여줄 타이틀, 메세지 등 text Data 에 대한 기술이다.
 * arrMessage 의 각 item 들은 모두 type 을 가지는데, 해당 type 에 따라 정해진 css 값을 가지게 된다.
 * 만약 cssObj 가 존재하면, 정해진 css 값 + cssObj 값이 적용된다.
 * 각 item 의 message 는 UI 상 한줄의 text 를 의미, 즉 같은 type 의 text 가 여러 줄일 수도 있다.
 *
 * [arrButton]
 * arrButton 은 해당 팝업에 보여줄 버튼에 대한 기술이다.
 * 각 item 은 id 와 name 으로 이루어져 있으며, UI 에는 name 이 노출된다.
 *
 * [cbAction]
 * cbAction 은 버튼이 눌렸을때 혹은 View 가 종료되었을때 호출되는 함수이다.
 * - 버튼이 눌렸을때는 button id 를 넘겨준다.
 * - 버튼이 없을때는 keyCode 를 넘겨주고, consume 처리 여부를 return 값으로 받는다.
 * - View 가 종료될때는 parameter 가 null 로 호출이 된다.
 *
 * ex)
 * var popupData = {
 *      arrMessage: [{
 *          type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
 *          message: ["이동식 디스크 연결"],
		    cssObj: {}
 *      }, {
 *          type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
 *          message: ["해당 콘텐츠의 정보가 없습니다", "편성에서 변경/삭제된 콘텐츠입니다"],
		    cssObj: {}
 *      }, ...],
 *
 *      arrButton: [{id: "ok", name: "확인"}, {id: "cancle", name: "취소"}, ...],
 *      isShowSaid : trua/false
 *
 *      cbAction: function (id) {...}
 * }
 * KTW.ui.LayerManager.activateLayer({
 *      obj: {
 *          id: "",
 *          type: "",
 *          priority: "",
 *          view: KTW.ui.view.popup.BasicPopup,
 *          params: {
 *              data: popupData
 *          }
 *      },
 *      ...
 * });
 */

"use strict";

(function() {

    var log =Log;
    var util =Util;

    var MSG_TYPE = {
        TITLE: "Title",

        MSG_52: "MSG_52",
        MSG_48: "MSG_48",
        MSG_45: "MSG_45",
        MSG_42: "MSG_42",
        MSG_40: "MSG_40",
        MSG_38: "MSG_38",
        MSG_33: "MSG_33",
        MSG_32: "MSG_32",

        MSG_30: "MSG_30",
        MSG_30_DARK: "MSG_30_DARK",
        MSG_28: "MSG_28"
    };

    var COMMON_CSS = {
        position: "absolute", left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: 50, "text-align": "center",
        "font-size": 38, "letter-spacing": -1.9, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M"
    };
    var BASIC_CSS = [];
    BASIC_CSS[MSG_TYPE.TITLE] = _$.extend({}, COMMON_CSS, {
        height: 74, "font-size": 33, "letter-spacing": -1.65, color: "rgba(221, 175, 120, 1)", "font-family": "RixHead B"
    });
    BASIC_CSS[MSG_TYPE.MSG_52] = _$.extend({}, COMMON_CSS, { height: 70, "font-size": 52, "letter-spacing": -2.6 });
    BASIC_CSS[MSG_TYPE.MSG_48] = _$.extend({}, COMMON_CSS, { height: 70, "font-size": 48, "letter-spacing": -2.4 });
    BASIC_CSS[MSG_TYPE.MSG_45] = _$.extend({}, COMMON_CSS, { height: 65, "font-size": 45, "letter-spacing": -2.25 });
    BASIC_CSS[MSG_TYPE.MSG_42] = _$.extend({}, COMMON_CSS, { height: 55, "font-size": 42, "letter-spacing": -2.1 });
    BASIC_CSS[MSG_TYPE.MSG_40] = _$.extend({}, COMMON_CSS, { height: 50, "font-size": 40 , "letter-spacing": -2.0 });
    BASIC_CSS[MSG_TYPE.MSG_38] = _$.extend({}, COMMON_CSS, { height: 50, "font-size": 38, "letter-spacing": -1.9 });
    BASIC_CSS[MSG_TYPE.MSG_33] = _$.extend({}, COMMON_CSS, { height: 45, "font-size": 33, "letter-spacing": -1.5 });
    BASIC_CSS[MSG_TYPE.MSG_32] = _$.extend({}, COMMON_CSS, { height: 45, "font-size": 32, "letter-spacing": -1.5 });
    BASIC_CSS[MSG_TYPE.MSG_30] = _$.extend({}, COMMON_CSS, { height: 45, "font-size": 30, "letter-spacing": -1.5, "font-family": "RixHead L" });
    BASIC_CSS[MSG_TYPE.MSG_30_DARK] = _$.extend({}, COMMON_CSS, { height: 45, "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.5)", "font-family": "RixHead L" });
    BASIC_CSS[MSG_TYPE.MSG_28] = _$.extend({}, COMMON_CSS, { height: 40, "font-size": 28, "letter-spacing": -1.4, "font-family": "RixHead L" });


    function getMessageHeight (type, isReal) {
        var height = 0;

        switch (type) {
            case MSG_TYPE.TITLE:
                height = !isReal ? 74 : 33;
                break;
            case MSG_TYPE.MSG_52:
                height = !isReal ? 70 : 52;
                break;
            case MSG_TYPE.MSG_48:
                height = !isReal ? 70 : 48;
                break;
            case MSG_TYPE.MSG_45:
                height = !isReal ? 65 : 45;
                break;
            case MSG_TYPE.MSG_42:
                height = !isReal ? 55 : 42;
                break;
            case MSG_TYPE.MSG_40:
                height = !isReal ? 62 : 40;
                break;
            case MSG_TYPE.MSG_38:
                height = !isReal ? 50 : 38;
                break;
            case MSG_TYPE.MSG_33:
                height = !isReal ? 45 : 33;
                break;
            case MSG_TYPE.MSG_32:
                height = !isReal ? 45 : 32;
                break;
            case MSG_TYPE.MSG_30:
                height = !isReal ? 45 : 30;
                break;
            case MSG_TYPE.MSG_28:
                height = !isReal ? 40 : 28;
                break;
        }

        return height;
    }

    function createElement (_this) {
        log.printDbg("createElement()");

        var parentDiv = _this.div;
        var data = _this.data;
        var totalHeight  = 0;
        var height = 0;
        var cssObj = {};

        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 0, width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                    "background-color": "rgba(0, 0, 0, 0.9)"
                }
            },
            parent: parentDiv
        });

        var containerDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible"
                }
            },
            parent: parentDiv
        });

        // title
        var length = data.arrMessage.length;
        for (var i = 0; i < length; i++) {
            var msgData = data.arrMessage[i];

            cssObj = _$.extend({}, BASIC_CSS[msgData.type], msgData.cssObj);

            height = getMessageHeight(msgData.type);

            for (var j = 0; j < msgData.message.length; j++) {
                cssObj.top = totalHeight;
                cssObj.height = height;

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: cssObj
                    },
                    text: msgData.message[j],
                    parent: containerDiv
                });

                // totalHeight 변경
                totalHeight += height;
            }
        }

        if (data.arrButton && data.arrButton.length > 0) {
            // text & button 간 간격 조정
            var lastType = data.arrMessage[data.arrMessage.length - 1].type;
            totalHeight = totalHeight - (getMessageHeight(lastType) - getMessageHeight(lastType, true)) + 80;

            // button
            length = data.arrButton.length;

            var buttonLeft = 820;
            if (length > 1) {
                buttonLeft = (1920 - (294 * length)) / 2;
            }

            for (var i = 0; i < length; i++) {
                var buttonData = data.arrButton[i];

                var button = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "basic_popup_button",
                        css: {
                            position: "absolute", left: buttonLeft, top: totalHeight, width: 280, height: 62
                        }
                    },
                    parent: containerDiv
                });
                
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280, height: 35, "text-align": "center",
                            "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead M"

                        }
                    },
                    text: buttonData.name,
                    parent: button
                });

                buttonLeft += 294;
            }

            totalHeight += 62;
        }

        containerDiv.css({
            top: (1080 - totalHeight) / 2
        });


        if(data.isShowSaid && data.isShowSaid === true) {
            var saidTop = ((1080 - totalHeight) / 2)  + totalHeight + 216;
            if (data.arrButton && data.arrButton.length > 0) {
                saidTop-=62;
            }

            if(saidTop<1050) {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: {
                            position: "absolute", left: 0, top: saidTop, width: KTW_ENV.RESOLUTION.WIDTH, height: 24, "text-align": "center",
                            "font-size": 22, "letter-spacing": -1.1, color: "rgba(255, 255, 255, 0.3)", "font-family": "RixHead L"
                        }
                    },
                    text: "SAID : " + "****" + KTW_DEF.SAID.substring(4),
                    parent: parentDiv
                });
            }

        }
    }

    function setButtonFocus (_this) {
        var arrButton = _this.div.find("[name=basic_popup_button]");
        var focusButton = _$(arrButton[_this.focusIdx]);

        arrButton.css({ "background-color": "rgba(1, 1, 1, 0.5)", border: "solid 2px rgba(190, 190, 190, 0.5)", width: 276, height: 58 });
        arrButton.find("span").css({ color: "rgba(255, 255, 255, 0.7)", top: 15, "font-family": "RixHead L" });


        focusButton.css({ "background-color": "rgba(210, 51, 47, 1)", border: "", width: 280, height: 62 });
        focusButton.find("span").css({ color: "rgba(255, 255, 255, 1)", top: 17, "font-family": "RixHead L" });
    }


    var basicPopup = function BasicPopup(options) {
        Layer.call(this, options);

        this.data = null;
        this.focusIdx = -1;
        this.div = null;
        this.params = null;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            this.params = this.getParams();
            if(this.params !== null) {
                this.data = this.params.data;
            }
            this.div = this.div;

            this.div.attr({class: "channelpayment_arrange_frame"});
            createElement(this);
            if(cbCreate) cbCreate(true);
        };


        this.showView = function (options) {
            if (!options || !options.resume) {
                if (this.data.arrButton && this.data.arrButton.length > 0) {
                    this.focusIdx = 0;

                    setButtonFocus(this);
                }
            }
        };

        this.hideView = function (options) {
            if (!options || !options.pause) {
                if (this.data.cbAction) {
                    this.data.cbAction();
                }
            }
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey() keyCode : " + keyCode);
            var consumed = false;
            if (this.data.arrButton && this.data.arrButton.length > 0) {
                switch (keyCode) {
                    case KTW_KEY_CODE.LEFT:
                        if (this.focusIdx > -1) {
                            this.focusIdx = --this.focusIdx < 0 ? this.data.arrButton.length - 1 : this.focusIdx;
                            setButtonFocus(this);
                            consumed = true;
                        }
                        break;
                    case KTW_KEY_CODE.RIGHT:
                        if (this.focusIdx > -1) {
                            this.focusIdx = ++this.focusIdx > this.data.arrButton.length - 1 ? 0 : this.focusIdx;
                            setButtonFocus(this);
                            consumed = true;
                        }
                        break;
                    case KTW_KEY_CODE.OK:
                        if (this.focusIdx > -1) {
                            if (this.data.cbAction) {
                                this.data.cbAction(this.data.arrButton[this.focusIdx].id);
                            }
                            consumed = true;
                        }
                        break;
                    case KTW_KEY_CODE.BACK:
                        if (this.data.cbAction) {
                            this.data.cbAction(keyCode);
                        }
                        consumed = true;
                        break;
                }

            }else {
                if (this.data.cbAction) {
                    consumed = this.data.cbAction(keyCode);
                }
            }

            return consumed;
        };
    };

    basicPopup.prototype =  new Layer();
    basicPopup.prototype.constructor = basicPopup;
    basicPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    basicPopup.prototype.show = function (options) {
        Layer.prototype.show.call(this);
        this.showView(options);
    };
    basicPopup.prototype.hide = function () {
        this.hideView();
    };
    basicPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer[PAYMENT_CHANNEL_PURCHASE_BASIC_POPUP] = basicPopup;
})();

