/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>TermsAgreeView</code>
 *
 * 약관동의 STEP#2 or STEP#3 View
 * @author jjh1117
 * @since 2016-12-16
 */

"use strict";

(function() {
    ChannelPayment.ChannelPurchase.view.TermsAgreeView = function(parent) {

        var log = Log;
        var util = Util;

        var FOCUS_TERMS_DETAIL = 0;
        var FOCUS_TERMS_AGREE = 1;

        var parent = parent;
        var div = null;

        /**
         * 약관 동의 여부
         */
        var isTermsAgree = false;

        var focusIndex = FOCUS_TERMS_DETAIL;

        /**
         * 약관상세보기 버튼
        */
        var btnTermsDetail = null;
        /**
         * 약관동의 체크 버튼
         */
        var btnTermsAgree = null;

        /**
         * 약관 정보 Element
         */
        var textDesc = null;

        /**
         * 약관 정보
         */
        var termsDesc = null;

        var stepNumber = "3";

        var channelPurchasePopupResourcePath = resourcePath + "channelpurchasepopup/";

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        /**
         * 약관 동의 여부
         */
        this.getIsTermsAgree = function() {
            return isTermsAgree;
        };

        /**
         * 약관 동의 여부
         */
        this.setIsTermsAgree = function(termsAgree) {
            isTermsAgree = termsAgree;
            setTimeout(function(){
                if(isTermsAgree === true) {
                    parent.goNextStep();
                }
            } , 10);
        };

        this.setTermsDesc = function(desc) {
            termsDesc = desc;
        };
        this.setStepNumber = function(stepnum) {
            stepNumber = stepnum;
        };

        this.getDiv = function () {
            return div;
        };

        this.create = function () {
            log.printDbg("create()");
            _createElement();
        };

        this.show = function (options) {
            log.printDbg("show()");
            focusIndex = FOCUS_TERMS_AGREE;
            textDesc.text(termsDesc);
            _focusUpdate();
        };

        this.hide = function (options) {
            log.printDbg("hide()");

            div.css("display", "none");
            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW_KEY_CODE.UP:
                    parent.goPrevStep();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.DOWN:
                    if(isTermsAgree === true) {
                        parent.goNextStep();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT:
                    focusIndex--;
                    if(focusIndex<FOCUS_TERMS_DETAIL) {
                        focusIndex = FOCUS_TERMS_AGREE;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.RIGHT:
                    focusIndex++;
                    if(focusIndex>FOCUS_TERMS_AGREE) {
                        focusIndex = FOCUS_TERMS_DETAIL;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    if(focusIndex === FOCUS_TERMS_DETAIL) {
                        parent.showTermsAgreeDetail();

                    }else if(focusIndex === FOCUS_TERMS_AGREE) {
                        if(isTermsAgree === true) {
                            isTermsAgree = false;
                            _focusUpdate();
                        }else {
                            isTermsAgree = true;
                            parent.goNextStep();

                        }
                    }
                    consumed = true;
                    break;
            }

            return consumed;
        };

        function _createElement() {
            log.printDbg("_createElement()");

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 416-6, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });



            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: channelPurchasePopupResourcePath + "pay_terms_bg.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 96,
                        width: 1200,
                        height: 214
                    }
                },
                parent: div
            });

            // util.makeElement({
            //     tag: "<div />",
            //     attrs: {
            //         id: "",
            //         css: {
            //             position: "absolute",
            //             left: 0, top: 96, width: 1200, height: 2,
            //             "background-color": "rgba(67, 67, 67, 0.8)"
            //         }
            //     },
            //     parent: div
            // });
            //
            // util.makeElement({
            //     tag: "<div />",
            //     attrs: {
            //         id: "",
            //         css: {
            //             position: "absolute",
            //             left: 0, top: 302, width: 1200, height: 2,
            //             "background-color": "rgba(67, 67, 67, 0.8)"
            //         }
            //     },
            //     parent: div
            // });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 1 , top: 25 , width: 40, height: 59,
                        color: "rgba(255, 96, 108, 0.5)", "font-size": 57 , "text-align": "left"
                    }
                },
                text: stepNumber,
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "terms_agree_comment1",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 60 , top: 39 , width: 350, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "가입 안내를 확인해 주세요",
                parent: div
            });


            textDesc = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_desc",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 46 , top: 536-408 , width: (1200-(46*2)), height: 140,
                        color: "rgba(255, 255, 255, 1)", "font-size": 24 , "text-align": "left",
                        "white-space":"pre-line", "line-height": 1.5 ,  display: "-webkit-box",
                        overflow:"hidden" , "-webkit-line-clamp":4,"text-overflow":"ellipsis","letter-spacing":-1.2
                    }
                },
                text: "",
                parent: div
            });



            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "terms_agree_comment2",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 4 , top: 342 , width: 480, height: 29,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: parent.isProductUpselling() === true ? "" : "월정액 가입 시에는 약관 동의가 필요합니다",
                parent: div
            });


            var btnDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_1",
                    css: {
                        position: "absolute",
                        left: 792, top: 324, width:198, height: 62
                    }
                },
                parent: div
            });


            var defaultImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "btn_1_default_img" ,
                    src: channelPurchasePopupResourcePath + "btn_pay_w198.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 198,
                        height: 62,
                    }
                },
                parent: btnDiv1
            });

            var btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_1_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:198, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv1
            });

            var btnDefaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_1_default_text",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 198, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "약관 상세보기",
                parent: btnDiv1
            });

            var btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_1_focus_text",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 198, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "약관 상세보기",
                parent: btnDiv1
            });

            var btn = {
                btn_root_div : btnDiv1,
                btn_default_img : defaultImg,
                btn_focus_div : btnFocusDiv,
                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            } ;

            btnTermsDetail = btn;


            var btnDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_2",
                    css: {
                        position: "absolute",
                        left: 1002, top: 324, width:198, height: 62
                    }
                },
                parent: div
            });


            defaultImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "btn_2_default_img" ,
                    src: channelPurchasePopupResourcePath + "btn_pay_w198.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 198,
                        height: 62,
                    }
                },
                parent: btnDiv2
            });

            btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_2_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:198, height: 62,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv2
            });


            var btnCheckedImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "btn_2_checked_img" ,
                    src: channelPurchasePopupResourcePath + "pop_cb_dim_uncheck.png",
                    css: {
                        position: "absolute",
                        left: 15 ,
                        top: 12,
                        width: 48,
                        height: 40,
                    }
                },
                parent: btnDiv2
            });

            btnDefaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_2_default_text",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left: 63 , top: 18 , width: 135, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "약관동의",
                parent: btnDiv2
            });

            btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_2_focus_text",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 63 , top: 18 , width: 135, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                    }
                },
                text: "약관동의",
                parent: btnDiv2
            });

            btn = {
                btn_root_div : btnDiv2,
                btn_default_img : defaultImg,
                btn_focus_div : btnFocusDiv,
                btn_checked_img : btnCheckedImg,
                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            };

            btnTermsAgree = btn;
        }


        function _resetFocusAll() {
            log.printDbg("_resetFocusAll()");

            btnTermsDetail.btn_default_img.css("display" , "");
            btnTermsDetail.btn_focus_div.css("display" , "none");
            btnTermsDetail.btn_default_text.css("display" , "");
            btnTermsDetail.btn_focus_text.css("display" , "none");

            btnTermsAgree.btn_default_img.css("display" , "");
            btnTermsAgree.btn_focus_div.css("display" , "none");
            btnTermsAgree.btn_default_text.css("display" , "");
            btnTermsAgree.btn_focus_text.css("display" , "none");

            if(isTermsAgree === true) {
                btnTermsAgree.btn_checked_img.attr("src", channelPurchasePopupResourcePath + "pop_cb_dim_check_red.png");

                btnTermsAgree.btn_default_text.css({left:63});
                btnTermsAgree.btn_focus_text.css({left:63});

            }else {
                btnTermsAgree.btn_checked_img.attr("src", channelPurchasePopupResourcePath + "pop_cb_dim_uncheck.png");
                btnTermsAgree.btn_default_text.css({left:59});
                btnTermsAgree.btn_focus_text.css({left:59});
            }
        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");
            _resetFocusAll();

            if(focusIndex === FOCUS_TERMS_DETAIL) {
                btnTermsDetail.btn_default_img.css("display" , "none");
                btnTermsDetail.btn_focus_div.css("display" , "");
                btnTermsDetail.btn_default_text.css("display" , "none");
                btnTermsDetail.btn_focus_text.css("display" , "");
            }else {
                btnTermsAgree.btn_default_img.css("display" , "none");
                btnTermsAgree.btn_focus_div.css("display" , "");
                btnTermsAgree.btn_default_text.css("display" , "none");
                btnTermsAgree.btn_focus_text.css("display" , "");

                if(isTermsAgree === true) {
                    btnTermsAgree.btn_checked_img.attr("src", channelPurchasePopupResourcePath + "pop_cb_foc_check_red.png");

                    btnTermsAgree.btn_default_text.css({left:63});
                    btnTermsAgree.btn_focus_text.css({left:63});
                }else {
                    btnTermsAgree.btn_checked_img.attr("src", channelPurchasePopupResourcePath +  "pop_cb_foc_uncheck.png");

                    btnTermsAgree.btn_default_text.css({left:59});
                    btnTermsAgree.btn_focus_text.css({left:59});
                }
            }


        }
    }
})();