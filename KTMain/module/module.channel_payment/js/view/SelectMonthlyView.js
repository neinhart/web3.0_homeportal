/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SelectMonthlyView</code>
 *
 * 약정 선택 STEP#2 View
 * @author jjh1117
 * @since 2016-12-14
 */

"use strict";

(function() {
    ChannelPayment.ChannelPurchase.view.SelectMonthlyView = function(parent) {

        var log = Log;
        var util = Util;

        var parent = parent;
        var div = null;
        var titleSpan = null;
        var descSpan = null;

        /**
         * 약정 정보 List
         */
        var contractInfo = [];

        /**
         * 약정 GUI Group Element List
         */
        var optionList = [];
        var focusIndex = 0;
        var showOptionCount = 0;

        /**
         * 선택한 약정 Index
         */
        var selectIndex = -1;

        var periodList = null;
        var vatProductPriceList = null;
        var isBiz = false;

        var channelPurchasePopupResourcePath = resourcePath + "channelpurchasepopup/";
        
        

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        /**
         * 약정 정보 리스트
         * @param Info
         */
        this.setContractInfo  = function(Info) {
            contractInfo = Info;
            
        };

        this.setBizPeriodInfo = function(period , vatProductPrice , is_biz) {
            log.printDbg("setBizPeriodInfo()");
            periodList =  period.split("|");
            vatProductPriceList = vatProductPrice.split("|");
            isBiz = is_biz;
        }

        /**
         * 선택한 약정 Index
         * @returns {number}
         */
        this.getSelectOptionIndex = function() {
            return selectIndex;
        };

        /**
         * 선택한 약정 정보 String
         * 다음 Step으로 이동 시 현재 Step 문구 표시를 위함.
         * @returns {string}
         */
        this.getSelectOptionString = function() {
            var monthlyName = "";
            var periodStr =  "";

            if(isBiz === true) {
                var tempstr = null;
                var tempperiod = null;
                var period = Number(periodList[selectIndex]);
                if(period<24) {
                    tempperiod =  periodList[selectIndex] + "시간";
                }else {
                    var temp = Math.ceil(period / 24) ;
                    tempperiod =  temp + "일";
                }
                
                tempstr = util.setComma(Number(vatProductPriceList[selectIndex]));
                return tempperiod + " | " + tempstr+"원";
            }else {
                if (KTW_ENV.IS_OTS){
                    periodStr = contractInfo[selectIndex].period.toString();
                }else{
                    periodStr = contractInfo[selectIndex].contractPeriod;
                }

                if(periodStr === "0") {
                    monthlyName = "무약정";
                }else {
                    var year = parseInt(Number(periodStr) / 12);
                    var month = Number(periodStr) - year * 12;
                    if (year > 0 && month > 0)
                        monthlyName = year + "년 " + month + "개월";
                    else if (year == 0) {
                        monthlyName = month + "개월";
                    }
                    else {
                        monthlyName = year + "년";
                    }
                }

                var price = "";
                if(KTW_ENV.IS_OTS) {
                    /**
                     * jjh1117 2018/01/11
                     * OTS는 아직 vat 연동 data가 아님 추후 이 부분 적용해야 함.
                     */
                    price = contractInfo[selectIndex].pricevat;
                }else {
                    price =  contractInfo[selectIndex].vatContractPrice;
                }

                var tempstr = util.setComma(Number(price));


                return monthlyName + " | 월 " + tempstr+"원";

            }
        };

        this.getDiv = function () {
            return div;
        };

        this.create = function () {
            log.printDbg("create()");
            _createElement();
        };


        this.show = function (options) {
            log.printDbg("show()");

            if(selectIndex<0) {
                focusIndex = 0;
            }else {
                focusIndex = selectIndex;
            }

            if(isBiz === true) {
                titleSpan.text("시청 기간을 선택해주세요");
                descSpan.css("display" , "none");
                _showBizPeriodInfo();
            }else {
                titleSpan.text("약정 기간을 선택해주세요");
                descSpan.css("display" , "none");
                _showContractInfo();
            }


            _focusUpdate();
        };

        this.hide = function (options) {
            log.printDbg("hide()");

            div.css("display", "none");
            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey() showOptionCount : " + showOptionCount + " , keyCode : " + keyCode);

            var consumed = false;

            switch (keyCode) {
                case KTW_KEY_CODE.UP:
                    /**
                     * 이전 STEP 이동함
                     */
                    //parent.goPrevStep();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.DOWN:
                    /**
                     * 선택한 약정이 있는 경우 다음 Step으로 이동
                     */
                    if(selectIndex>=0) {
                        parent.goNextStep();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT:
                    focusIndex--;
                    if(focusIndex<0) {
                        focusIndex =showOptionCount-1;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.RIGHT:
                    focusIndex++;
                    if(focusIndex>(showOptionCount-1)) {
                        focusIndex = 0;
                    }
                    _focusUpdate();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    /**
                     * 약정 선택 후 다음 Step 이동함
                     */
                    selectIndex = focusIndex;
                    parent.goNextStep();
                    consumed = true;
                    break;
            }

            return consumed;
        };


        function _createElement() {
            log.printDbg("_createElement()");

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 294-6, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 1 , top: 25 , width: 40, height: 59,
                        color: "rgba(255, 96, 108, 0.5)", "font-size": 57 , "text-align": "left"
                    }
                },
                text: "2",
                parent: div
            });


            titleSpan = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 60 , top: 39 , width: 320, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "약정 기간을 선택해주세요",
                parent: div
            });

            descSpan = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_desc",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 600 , top: 40 , width: 600, height: 28,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 26 , "text-align": "right","letter-spacing":-1.3
                    }
                },
                text:  "약정 기간 종료 시 무약정으로 자동 전환됩니다",
                parent: div
            });

            for(var i=0;i<4;i++) {
                var option = _createOptionElement(i);
                optionList[optionList.length] = option;
                option.root_div.css("display" , "none");
            }

        }

        /**
         * 약정 GUI Group Element 생성
         * @param optionIndex
         * @returns {{}}
         * @private
         */
        function _createOptionElement(optionIndex) {
            log.printDbg("_createOptionElement() optionIndex : " + optionIndex);
            var option = {};

            var optionDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: (292*optionIndex) + (11*optionIndex), top: 96, width: 292, height: 168 , "background-color": "rgba(55,55,55,1)"
                    }
                },
                parent: div
            });

            var defaultOptionImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "img_default_option" ,
                    src: channelPurchasePopupResourcePath + "pop_op_w292.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 292,
                        height: 168
                    }
                },
                parent: optionDiv
            });

            var focusOptionDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    class : "channelpayment_focus_red_border_box" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 292, height: 168
                    }
                },
                parent: optionDiv
            });

            var radioBtnImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "img_radio_btn" ,
                    src: channelPurchasePopupResourcePath + "rdo_btn_d.png",
                    css: {
                        position: "absolute",
                        left: 127 ,
                        top: 22,
                        width: 38,
                        height: 38
                    }
                },
                parent: optionDiv
            });

            var textDefaultBizPeriodName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_default_biz_period_name",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", left :0 ,top: 79 ,  width : 292 , height: 30,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 28 , "text-align": "center","letter-spacing":-1.4
                    }
                },
                text: "",
                parent: optionDiv
            });


            var textDefaultMonthlyName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_default_monthly_name",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", top: 79 , height: 30,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 28 , "text-align": "left","letter-spacing":-1.4
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textDefaultDiscount = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_default_discount",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", top: 81 , height: 26,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 24 , "text-align": "left","letter-spacing":-1.2
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textFocusBizPeriodName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_focus_biz_period_name",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left :0 ,top: 79 ,  width : 292 , height: 30,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textFocusMonthlyName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_focus_monthly_name",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", top: 79 , height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textFocusDiscount = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_focus_discount",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", top: 81 , height: 26,
                        color: "rgba(189, 60, 60, 1)", "font-size": 24 , "text-align": "left","letter-spacing":-1.2
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textDefaultPriceMonthUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_default_price_month_unit",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", top: 120 , height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "월",
                parent: optionDiv
            });

            var textDefaultPrice = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_default_price",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", top: 114 , height: 38,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 36 , "text-align": "left","letter-spacing":-1.8
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textDefaultPriceUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_default_price_unit",
                    class: "channelpayment_font_l",
                    css: {
                        position: "absolute", top: 120 , height: 29,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35
                    }
                },
                text: "원",
                parent: optionDiv
            });

            var textFocusPriceMonthUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_focus_price_month_unit",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", top: 120 , height: 30,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 28 , "text-align": "left","letter-spacing":-1.4
                    }
                },
                text: "월",
                parent: optionDiv
            });


            var textFocusPrice = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_focus_price",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", top: 114 , height: 40,
                        color: "rgba(255, 255, 255, 1)", "font-size": 38 , "text-align": "left","letter-spacing":-1.9
                    }
                },
                text: "",
                parent: optionDiv
            });

            var textFocusPriceUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_focus_price_unit",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", top: 120 , height: 30,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 28 , "text-align": "left","letter-spacing":-1.4
                    }
                },
                text: "원",
                parent: optionDiv
            });


            option.root_div = optionDiv;
            option.default_option_img = defaultOptionImg;
            option.focus_option_div = focusOptionDiv;
            option.radio_btn = radioBtnImg;

            option.text_default_biz_period_name = textDefaultBizPeriodName;
            option.text_default_monthly_name = textDefaultMonthlyName;
            option.text_default_discount = textDefaultDiscount;

            option.text_focus_biz_period_name = textFocusBizPeriodName;
            option.text_focus_monthly_name = textFocusMonthlyName;
            option.text_focus_discount = textFocusDiscount;

            option.text_default_price_month_unit = textDefaultPriceMonthUnit;
            option.text_default_price = textDefaultPrice;
            option.text_default_price_unit = textDefaultPriceUnit;

            option.text_focus_price_month_unit = textFocusPriceMonthUnit;
            option.text_focus_price = textFocusPrice;
            option.text_focus_price_unit = textFocusPriceUnit;


            return option;

        }

        function _showBizPeriodInfo() {
            log.printDbg("_showContractInfo()");
            var periodName = "";
            var price = 0;

            if(vatProductPriceList !== null && vatProductPriceList.length>0 && periodList !== null && periodList.length>0 && periodList.length === vatProductPriceList.length) {
                var len = 0;
                if(periodList.length>4) {
                    len = 4;
                }else {
                    len = periodList.length;
                }

                showOptionCount = len;

                for(var i=0;i<len;i++) {
                    var period = Number(periodList[i]);
                    if(period<24) {
                        periodName =  periodList[i] + "시간";
                    }else {
                        var temp = Math.floor(period / 24) ;
                        periodName =  temp + "일";
                    }
                    
                    
                    price =  vatProductPriceList[i];
                    _showBizOption(i , periodName, Number(price));
                }
            }

        }

        function _showContractInfo() {
            log.printDbg("_showContractInfo()");
            var monthlyName = "";
            var discountRate = -1;
            var price = 0;

            if(contractInfo !== null && contractInfo.length>0) {
                var len = 0;
                if(contractInfo.length>4) {
                    len = 4;
                }else {
                    len = contractInfo.length;
                }

                showOptionCount = len;
                for(var i=0;i<len;i++) {
                    var periodStr =  "";
                    if (KTW_ENV.IS_OTS){
                        periodStr = contractInfo[i].period.toString();
                    }else{
                        periodStr = contractInfo[i].contractPeriod;
                    }

                    if(periodStr === "0") {
                        monthlyName = "무약정";
                    }else {
                        var year = parseInt(Number(periodStr) / 12);
                        var month = Number(periodStr) - year * 12;
                        if (year > 0 && month > 0)
                            monthlyName = year + "년 " + month + "개월";
                        else if (year == 0) {
                            monthlyName = month + "개월";
                        }
                        else {
                            monthlyName = year + "년";
                        }
                    }

                    if(KTW_ENV.IS_OTS) {
                        discountRate = contractInfo[i].disrate;
                    }else {
                        discountRate = contractInfo[i].contractDiscountRate;
                    }

                    if(KTW_ENV.IS_OTS) {
                        /**
                         * jjh1117 2018/01/11
                         * OTS vat 연동 data가 아님
                         */
                        price = contractInfo[i].pricevat;
                    }else {
                        price =  contractInfo[i].vatContractPrice;
                    }
                    _showOption(i , monthlyName, discountRate , Number(price));
                }
            }
        }



        function _resetFocusAll() {
            log.printDbg("_resetFocusAll()");

            for(var i=0;i<showOptionCount;i++) {
                var option = optionList[i];
                option.default_option_img.css("display" , "none");
                option.focus_option_div.css("display" , "none");

                if(selectIndex === i) {
                    option.radio_btn.attr("src", channelPurchasePopupResourcePath + "rdo_btn_select_d.png");
                }else {
                    option.radio_btn.attr("src", channelPurchasePopupResourcePath + "rdo_btn_d.png");
                }

                option.text_default_biz_period_name.css("display", "");
                if(isBiz === true) {
                    option.text_default_monthly_name.css("display", "none");
                    option.text_default_discount.css("display", "none");
                }else {
                    option.text_default_monthly_name.css("display", "");
                    option.text_default_discount.css("display", "");
                }
                option.text_focus_biz_period_name.css("display", "none");
                option.text_focus_monthly_name.css("display", "none");
                option.text_focus_discount.css("display", "none");

                if(isBiz === false) {
                    if(option.is_show_text_discount === true) {
                        option.text_default_discount.css("display", "");
                    }else {
                        option.text_default_discount.css("display", "none");
                    }
                }

                option.text_default_price_month_unit.css("display", "");
                option.text_default_price.css("display", "");
                option.text_default_price_unit.css("display", "");

                option.text_focus_price_month_unit.css("display", "none");
                option.text_focus_price.css("display", "none");
                option.text_focus_price_unit.css("display", "none");
            }
        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");
            _resetFocusAll();

            var option = optionList[focusIndex];

            option.default_option_img.css("display" , "none");
            option.focus_option_div.css("display" , "");

            if(selectIndex === focusIndex) {
                option.radio_btn.attr("src", channelPurchasePopupResourcePath + "rdo_btn_select_d.png");
            }else {
                option.radio_btn.attr("src", channelPurchasePopupResourcePath + "rdo_btn_d.png");
            }

            option.text_default_biz_period_name.css("display" , "none");
            option.text_default_monthly_name.css("display" , "none");
            option.text_default_discount.css("display" , "none");

            option.text_focus_biz_period_name.css("display" , "");
            option.text_focus_monthly_name.css("display" , "");
            option.text_focus_discount.css("display" , "");

            if(option.is_show_text_discount === false) {
                option.text_focus_discount.css("display" , "none");
            }


            option.text_default_price_month_unit.css("display", "none");
            option.text_default_price.css("display", "none");
            option.text_default_price_unit.css("display", "none");

            option.text_focus_price_month_unit.css("display", "");
            option.text_focus_price.css("display", "");
            option.text_focus_price_unit.css("display", "");
        }

        function _showBizOption(optionIndex , periodName, price) {
            log.printDbg("_showBizOption() optionIndex : " + optionIndex + " , periodName : " + periodName + " , price : " + price);
            var option = optionList[optionIndex];

            option.root_div.css("display" , "");


            var monthUnitDefaultLength = 0;
            var monthUnitFocusLength = 0;
            var priceUnitDefaultLength = 0;
            var priceUnitFocusLength = 0;
            var priceDefaultLength = 0;
            var priceFocusLength = 0;

            option.text_default_biz_period_name.text(periodName);
            option.text_focus_biz_period_name.text(periodName);


            monthUnitDefaultLength = util.getTextLength("월" , "RixHead L" , 27,-1.35);
            monthUnitFocusLength = util.getTextLength("월" , "RixHead M" , 28,-1.4);
            var tempstr = util.setComma(price);
            option.text_default_price.text(tempstr);
            option.text_focus_price.text(tempstr);

            priceDefaultLength = util.getTextLength(tempstr , "RixHead L" , 36,-1.8);
            priceFocusLength = util.getTextLength(tempstr , "RixHead B" , 38 , -1.9);

            priceUnitDefaultLength = util.getTextLength("원" , "RixHead L" , 27 , -1.35);
            priceUnitFocusLength = util.getTextLength("원" , "RixHead M" , 28 , -1.4);


            var totalLength = monthUnitDefaultLength + priceDefaultLength + priceUnitDefaultLength;
            var leftPos = (292-totalLength) / 2;
            option.text_default_price_month_unit.css({left:leftPos , width:monthUnitDefaultLength});
            leftPos+=monthUnitDefaultLength;
            option.text_default_price.css({left:leftPos , width:priceDefaultLength});
            leftPos+=priceDefaultLength;
            option.text_default_price_unit.css({left:leftPos , width:priceUnitDefaultLength});


            totalLength = monthUnitFocusLength + priceFocusLength + priceUnitFocusLength;
            leftPos = (292-totalLength) / 2;
            option.text_focus_price_month_unit.css({left:leftPos , width:monthUnitFocusLength});
            leftPos+=monthUnitFocusLength;
            option.text_focus_price.css({left:leftPos , width:priceFocusLength});
            leftPos+=priceFocusLength;
            option.text_focus_price_unit.css({left:leftPos , width:priceUnitFocusLength});
        }


        function _showOption(optionIndex , monthlyName, discountRate , price) {
            log.printDbg("_showOption() optionIndex : " + optionIndex + " , monthlyName : " + monthlyName + " , discountRate : " + discountRate + " , price : " + price);
            var option = optionList[optionIndex];

            option.root_div.css("display" , "");

            var monthlyNameDefaultLength = 0;
            var monthlyNameFocusLength = 0;
            var discountRateDefaultLength = 0;
            var discountRateFocusLength = 0;

            var monthUnitDefaultLength = 0;
            var monthUnitFocusLength = 0;
            var priceUnitDefaultLength = 0;
            var priceUnitFocusLength = 0;
            var priceDefaultLength = 0;
            var priceFocusLength = 0;


            if(monthlyName !== "무약정") {
                option.is_show_text_discount = true;
                var temp = "(" + discountRate + "% 할인)";
                discountRateDefaultLength = util.getTextLength(temp , "RixHead L" , 24 , -1.2);
                discountRateFocusLength = util.getTextLength(temp , "RixHead M" , 24 , -1.2);
                option.text_default_discount.text(temp);
                option.text_focus_discount.text(temp);
            }else {
                option.is_show_text_discount = false;
            }
            monthlyNameDefaultLength = util.getTextLength(monthlyName , "RixHead L" , 28, -1.4);
            monthlyNameFocusLength = util.getTextLength(monthlyName , "RixHead M" , 30 , -1.5);
            option.text_default_monthly_name.text(monthlyName);
            option.text_focus_monthly_name.text(monthlyName);

            var totalLength = monthlyNameDefaultLength + discountRateDefaultLength;

            var leftPos = (292-totalLength) / 2;
            option.text_default_monthly_name.css({left:leftPos , width:monthlyNameDefaultLength});
            if(option.is_show_text_discount === true) {
                leftPos+=monthlyNameDefaultLength;
                option.text_default_discount.css({left:leftPos , width:discountRateDefaultLength});
                option.text_default_discount.css("display" , "");
            }else {
                option.text_default_discount.css("display" , "none");
            }

            totalLength = monthlyNameFocusLength + discountRateFocusLength;

            var leftPos = (292-totalLength) / 2;
            option.text_focus_monthly_name.css({left:leftPos , width:monthlyNameFocusLength});
            if(option.is_show_text_discount === true) {
                leftPos+=monthlyNameFocusLength;
                option.text_focus_discount.css({left:leftPos , width:discountRateFocusLength});
                option.text_focus_discount.css("display" , "");
            }else {
                option.text_default_discount.css("display" , "none");
            }

            monthUnitDefaultLength = util.getTextLength("월" , "RixHead L" , 27,-1.35);
            monthUnitFocusLength = util.getTextLength("월" , "RixHead M" , 28,-1.4);
            var tempstr = util.setComma(price);
            option.text_default_price.text(tempstr);
            option.text_focus_price.text(tempstr);

            priceDefaultLength = util.getTextLength(tempstr , "RixHead L" , 36,-1.8);
            priceFocusLength = util.getTextLength(tempstr , "RixHead B" , 38 , -1.9);

            priceUnitDefaultLength = util.getTextLength("원" , "RixHead L" , 27 , -1.35);
            priceUnitFocusLength = util.getTextLength("원" , "RixHead M" , 28 , -1.4);


            totalLength = monthUnitDefaultLength + priceDefaultLength + priceUnitDefaultLength;
            leftPos = (292-totalLength) / 2;
            option.text_default_price_month_unit.css({left:leftPos , width:monthUnitDefaultLength});
            leftPos+=monthUnitDefaultLength;
            option.text_default_price.css({left:leftPos , width:priceDefaultLength});
            leftPos+=priceDefaultLength;
            option.text_default_price_unit.css({left:leftPos , width:priceUnitDefaultLength});


            totalLength = monthUnitFocusLength + priceFocusLength + priceUnitFocusLength;
            leftPos = (292-totalLength) / 2;
            option.text_focus_price_month_unit.css({left:leftPos , width:monthUnitFocusLength});
            leftPos+=monthUnitFocusLength;
            option.text_focus_price.css({left:leftPos , width:priceFocusLength});
            leftPos+=priceFocusLength;
            option.text_focus_price_unit.css({left:leftPos , width:priceUnitFocusLength});
        }

    }
})();