/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SelectUpsellingProductView</code>
 *
 * 미가입 채널 UPSelling 선택 STEP#1 View
 * @author jjh1117
 * @since 2016-12-20
 */

"use strict";

(function() {
    ChannelPayment.ChannelPurchase.view.SelectUpsellingProductView = function(parent) {

        var log = Log;
        var util = Util;


        var parent = parent;
        var div = null;

        var selectProductIndex = -1;
        var focusIndxX = 0;
        var focusIndxY = 0;
        var productPkgList = [];
        
        var maxProductCount = 0;

        var upsellingProductList = [];

        var channelPurchasePopupResourcePath = resourcePath + "channelpurchasepopup/";

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        /**
         * UPSelling 상품 정보
         * @param packagelist
         */
        this.setPackageList  = function(packagelist , selectIndex) {
            log.printDbg("setPackageList()");
            productPkgList = packagelist;

            if(productPkgList !== undefined && productPkgList !== null) {
                selectProductIndex =  selectIndex;
                
                log.printDbg("setPackageList() length : " + productPkgList.length);

                for(var i=0;i<productPkgList.length;i++) {
                    log.printDbg("productPkgList[" + i + "] : " + log.stringify(productPkgList[i]));
                }
            }
        };

        /**
         * 선택한 상품 Index
         * @returns {number}
         */
        this.getSelectProductIndex = function() {
            return selectProductIndex;
        };

        /**
         * 선택한 상품 정보 String
         * @returns {string}
         */
        this.getSelectProductString = function() {
            var productName = "";
            var priceString = "";
            if(selectProductIndex >= 0) {
                productName = productPkgList[selectProductIndex].upPkgName;
                
                if(KTW_ENV.IS_OTS === true && KTW_DEF.IS_VAT_PRICE === false) {
                    priceString = util.setComma(Number(productPkgList[selectProductIndex].upPrice));
                }else {
                    priceString = util.setComma(Number(productPkgList[selectProductIndex].vatUpPrice));
                }

                return productName + " | 월 " + priceString+"원";
            }else {
               return "";
            }
        };
        
        this.getSelectUpSellingPkgName = function() {
            var pkgName = "";
            if(selectProductIndex >= 0) {
                pkgName = productPkgList[selectProductIndex].upPkgName;
                return pkgName;
            }else {
                return "";
            }
        }


        this.getDiv = function () {
            return div;
        };

        this.create = function () {
            log.printDbg("create()");
            _createElement();
        };


        this.show = function (options) {
            log.printDbg("show()");

            _hideAll();

            if(productPkgList !== undefined && productPkgList !== null && productPkgList.length>0) {
                log.printDbg("show() length : " + productPkgList.length);

                maxProductCount = 0;
                if(productPkgList.length>3) {
                    maxProductCount = 3;
                }else {
                    maxProductCount = productPkgList.length;
                }
                for(var i=0;i<maxProductCount;i++) {
                    upsellingProductList[i].product_img.attr("src", productPkgList[i].vatW3ImgUrl1);
                    
                    upsellingProductList[i].product_img.css("display" , "");

                    upsellingProductList[i].focus_div.css("display" , "none");
                    if(selectProductIndex === i) {
                        upsellingProductList[i].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_select_d.png");
                    }else {
                        upsellingProductList[i].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_d.png");
                    }
                    upsellingProductList[i].radio_img.css("display" , "");

                    upsellingProductList[i].channel_confirm_default_div.css("display" , "");
                    upsellingProductList[i].channel_confirm_focus_div.css("display" , "none");
                }
            }

            if(selectProductIndex <0) {
                focusIndxX = 0;
            }else {
                focusIndxX = selectProductIndex;
            }
            focusIndxY = 0;
            
            _focusBtn();
            
        };

        this.hide = function (options) {
            log.printDbg("hide()");

            div.css("display", "none");
            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey() focusIndxX : " + focusIndxX + " , focusIndxY : " + focusIndxY + " , keyCode : " + keyCode);

            var consumed = false;

            switch (keyCode) {
                case KTW_KEY_CODE.UP:
                    if(focusIndxY === 1) {
                        focusIndxY = 0;
                        _focusBtn();
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.DOWN:
                    if(focusIndxY === 0) {
                        focusIndxY = 1;
                        _focusBtn();
                    }else {
                        if(selectProductIndex>=0) {
                            parent.goNextStep();
                        }
                    }
                    consumed = true;
                    break;
                case KTW_KEY_CODE.LEFT:
                    _unfocusBtn();
                    focusIndxY = 0;
                    focusIndxX--;
                    if(focusIndxX<0) {
                        focusIndxX =maxProductCount-1;
                    }
                    _focusBtn();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.RIGHT:
                    _unfocusBtn();
                    focusIndxY = 0;
                    focusIndxX++;
                    if(focusIndxX>(maxProductCount-1)) {
                        focusIndxX = 0;
                    }
                    _focusBtn();
                    consumed = true;
                    break;
                case KTW_KEY_CODE.OK:
                case KTW_KEY_CODE.ENTER:
                    if(focusIndxY === 1) {
                        /**
                         * 주요채널 확인
                         */
                        /**
                         * 추후 w3ImgUrl2로 바꿔야 함.
                         */
                        if(productPkgList !== null && productPkgList.length>0) {
                            parent.showUpsellingProductDetail(productPkgList[focusIndxX].upPkgName , productPkgList[focusIndxX].w3ImgUrl2);
                        }
                    }else {
                        /**
                         * 업셀링 상품 선택 후 다음 Step 이동
                         */
                        selectProductIndex = focusIndxX;
                        parent.goNextStep();
                    }
                    consumed = true;
                    break;
            }

            return consumed;
        };

        function _createElement() {
            log.printDbg("_createElement()");

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 378-6, width: 1200, height: 6,
                        "background-color": "rgba(128, 128, 128, 1)"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_number",
                    class: "channelpayment_font_b",
                    css: {
                        position: "absolute", left: 1 , top: 25 , width: 40, height: 59,
                        color: "rgba(255, 96, 108, 0.5)", "font-size": 57 , "text-align": "left"
                    }
                },
                text: "1",
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 60 , top: 39 , width: 320, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left","letter-spacing":-1.5
                    }
                },
                text: "상품을 선택하세요",
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "step_title",
                    class: "channelpayment_font_m",
                    css: {
                        position: "absolute", left: 600 , top: 40 , width: 600, height: 28,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 26 , "text-align": "right","letter-spacing":-1.3
                    }
                },
                text: (KTW_ENV.IS_OTS === true && KTW_DEF.IS_VAT_PRICE === false) ? "3년 약정 기준 (부가세 별도)" : "3년 약정 기준 (부가세 포함)",
                parent: div
            });

            for(var i=0;i<3;i++) {
                var productDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute",
                            left: 0 + (i*404), top: 85, width: 392, height: 252
                        }
                    },
                    parent: div
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_product" ,
                        src: channelPurchasePopupResourcePath + "upselling_none.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 392,
                            height: 252
                        }
                    },
                    parent: productDiv
                });



                var focusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_product",
                        css: {
                            position: "absolute",
                            left: 0 + (i*404), top: 85, width: 392, height: 169, border:"solid 6px rgba(210,52,47,1)" , "box-sizing" : "border-box"
                        }
                    },
                    parent: div
                });

                var productImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "img_product" ,
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 392,
                            height: 252
                        }
                    },
                    parent: productDiv
                });




                var radioBtnImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "img_radio_btn" ,
                        src: channelPurchasePopupResourcePath + "rdo_btn_d.png",
                        css: {
                            position: "absolute",
                            left: 177 ,
                            top: 22,
                            width: 38,
                            height: 38
                        }
                    },
                    parent: productDiv
                });

                var channeldefaultConfirmDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute",
                            left: 182, top: 178, width: 198, height: 62,
                            "background-color": "rgba(62, 61, 60, 1)" , border:"solid 2px rgba(93,93,93,1)" , "box-sizing" : "border-box"
                        }
                    },
                    parent: productDiv
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "channelpayment_font_l",
                        css: {
                            position: "absolute", left:0 , top: 18 , width:198, height: 29,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                        }
                    },
                    text: "주요채널 확인",
                    parent: channeldefaultConfirmDiv
                });

                var channelfocusConfirmDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute",
                            left: 182, top: 178, width: 198, height: 62,
                            "background-color": "rgba(210, 51, 47, 1)"
                        }
                    },
                    parent: productDiv
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "channelpayment_font_m",
                        css: {
                            position: "absolute", left:0 , top: 18 , width:198, height: 29,
                            color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "center","letter-spacing":-1.35
                        }
                    },
                    text: "주요채널 확인",
                    parent: channelfocusConfirmDiv
                });


                /**
                 * w3ImgUrl1 : Web 3.0 , wImgUrl1 : Web 2.0 ,  aImgUrl1 : ACAP , cImgUrl1 : Cloud
                 * productPkgList[i].w3ImgUrl1
                 */
                var upsellingProduct = {
                    productdiv : productDiv,
                    product_img : productImg,
                    focus_div : focusDiv,
                    radio_img : radioBtnImg,
                    channel_confirm_default_div : channeldefaultConfirmDiv,
                    channel_confirm_focus_div : channelfocusConfirmDiv
                };
                upsellingProductList[upsellingProductList.length] = upsellingProduct;
            }
        }

        function _hideAll() {
            if(upsellingProductList !== undefined && upsellingProductList !== null && upsellingProductList.length>0) {
                for(var i=0;i<upsellingProductList.length;i++) {
                    upsellingProductList[i].productdiv.css("display" , "");
                    upsellingProductList[i].product_img.css("display" , "none");
                    upsellingProductList[i].focus_div.css("display" , "none");
                    upsellingProductList[i].radio_img.css("display" , "none");
                    upsellingProductList[i].channel_confirm_default_div.css("display" , "none");
                    upsellingProductList[i].channel_confirm_focus_div.css("display" , "none");
                }
            }
        }

        function _unfocusBtn() {
            if(focusIndxX>=0) {
                if(upsellingProductList !== undefined && upsellingProductList !== null && upsellingProductList.length>0) {
                    upsellingProductList[focusIndxX].focus_div.css("display" , "none");
                    if(selectProductIndex === focusIndxX) {
                        upsellingProductList[focusIndxX].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_select_d.png");
                    }else {
                        upsellingProductList[focusIndxX].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_d.png");
                    }

                    upsellingProductList[focusIndxX].channel_confirm_default_div.css("display" , "");
                    upsellingProductList[focusIndxX].channel_confirm_focus_div.css("display" , "none");
                }
            }
        }
        
        function _focusBtn() {
            if(focusIndxX>=0) {
                if(upsellingProductList !== undefined && upsellingProductList !== null && upsellingProductList.length>0) {

                    if(focusIndxY ===0) {
                        upsellingProductList[focusIndxX].focus_div.css("display" , "");

                        if(selectProductIndex === focusIndxX) {
                            upsellingProductList[focusIndxX].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_select_d.png");
                        }else {
                            upsellingProductList[focusIndxX].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_d.png");
                        }
                        upsellingProductList[focusIndxX].radio_img.css("display" , "");

                        upsellingProductList[focusIndxX].channel_confirm_default_div.css("display" , "");
                        upsellingProductList[focusIndxX].channel_confirm_focus_div.css("display" , "none");
                    }else {
                        upsellingProductList[focusIndxX].focus_div.css("display" , "none");

                        if(selectProductIndex === focusIndxX) {
                            upsellingProductList[focusIndxX].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_select_d.png");
                        }else {
                            upsellingProductList[focusIndxX].radio_img.attr("src", channelPurchasePopupResourcePath + "rdo_btn_d.png");
                        }
                        upsellingProductList[focusIndxX].radio_img.css("display" , "");
                        upsellingProductList[focusIndxX].channel_confirm_default_div.css("display" , "none");
                        upsellingProductList[focusIndxX].channel_confirm_focus_div.css("display" , "");

                    }
                }
            }
        }

        function _resetFocusAll() {
            log.printDbg("_resetFocusAll()");

        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");
            _resetFocusAll();
        }
    }
})();