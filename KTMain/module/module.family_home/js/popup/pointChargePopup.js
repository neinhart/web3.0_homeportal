/**
 * 쿠폰, 포인트 등록/충전
 * (우리집 맞춤 TV > 쿠폰, 포인트 등록/충전)
 * Created by si.mun on 2018-01-26.
 */

(function() {
    myHome.popup = myHome.popup||{};
    myHome.popup.coupon_charge_popup = function CouponChargePopup(options) {
        Layer.call(this, options);

        var div = _$("<div/>", {class: "popup_contents"});
        var focus, btnFocus = false;
        var instance = this;
        var menuNames = [];

        this.init = function(cbCreate) {
            // UI가 월정액 모아보기(monthlypayPopup.js)와 매우 유사하여 그대로 class 를 가져다 씀
            this.div.attr({class: "arrange_frame myhome gniPopup monthlypayPopup coupon_charge_popup"});
            menuNames = this.getParams().menuNames;

            this.div.append(div);
            div.append(_$(
                "<div class='popup_title'>쿠폰, 포인트 등록/충전</div>" +
                "<div class='itemList'></div>" +
                "<div class='popup_button'>닫기</div>"
            ));

            addItems(cbCreate);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.show.call(this);
        };

        function addItems(cbCreate) {
            HTool.getPoint(false, function (data) {
                var itemList = instance.div.find('.itemList');
                var titleList = getTitleList();

                for(var i=0; i<4; i++) {
                    itemList.append(getItem());
                }

                //KT 멤버십 포인트 Setting
                if(data.UsrDiv == "R") {
                    setData({
                        div: itemList.find('.item:eq(0)'),
                        title: titleList[0],
                        amount: data.StarPoint,
                        measure: "P",
                        btnText: "조회",
                    });
                } else {
                    setData({
                        div: itemList.find('.item:eq(0)'),
                        title: titleList[0],
                        amount: "미가입",
                        measure: "",
                        btnText: "조회",
                    });
                }

                //TV 포인트 Setting
                if(data.TvPointYn == "Y") {
                    setData({
                        div: itemList.find('.item:eq(1)'),
                        title: titleList[1],
                        amount: data.TvPoint,
                        measure: "P",
                        btnText: "충전/관리",
                    });
                } else {
                    setData({
                        div: itemList.find('.item:eq(1)'),
                        title: titleList[1],
                        amount: "미가입",
                        measure: "",
                        btnText: "충전/관리",
                    });
                }

                // TV 쿠폰
                setData({
                    div: itemList.find('.item:eq(2)'),
                    title: titleList[2],
                    amount: (data.TvMoney || data.KtCoupon)? ((data.TvMoney-0) + (data.KtCoupon-0)) : 0,
                    measure: "원",
                    btnText: "구매/등록",
                });

                //콘텐츠 이용권 Setting
                setData({
                    div: itemList.find('.item:eq(3)'),
                    title: titleList[3],
                    amount: data.DetPoint2 ? data.DetPoint2 : 0,
                    measure: "개",
                    btnText: "구매/등록",
                });

                setFocus(2);

                if (cbCreate) {
                    LayerManager.stopLoading();
                    cbCreate(true);
                }
            });
        }

        function getItem() {
            var item = _$('<div/>', {class:'item'});
            item.html(
                '<div class="focus_red"><div class="black_bg"></div></div>' +
                '<div class="contents_Area">' +
                    '<div class="title_wrapper">' +
                        '<div class="title"></div>' +
                        '<div class="amount_area">' +
                            '<span class="amount_text"></span>' +
                            '<span class="amount_unit"></span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="btn">' +
                    '</div>' +
                '</div>');
            return item;
        }

        function setData(obj) {
            var div = obj.div;
            var title = obj.title;
            var amount = obj.amount;
            var measure = obj.measure;
            var btnText = obj.btnText;

            div.find('.title').text(title); // KT 멤버십, TV 포인트 등
            div.find('.amount_text').text(UTIL.formatComma(amount)); // 가격
            div.find('.amount_unit').text(measure); // 단위
            div.find('.btn').text(btnText); // 버튼 이름

            // [si.mun] UI 작업을 위한 TEST
            //var sampleMoney = ["45000", "102500", "98000", "0"];
            //
            //var testTitle =  ["KT 멤버십", "TV포인트", "TV쿠폰", "콘텐츠이용권"];
            //var testMoney = [
            //    sampleMoney[0],
            //    sampleMoney[1],
            //    sampleMoney[2],
            //    sampleMoney[3]
            //];
            //var testUnit = ["P", "점", "원", "개"];
            //var testButton = ["조회", "조회", "구매/등록", "구매/등록"];
            //
            //var items = div.find(".itemList .item");
            //
            //for (var i = 0; i < 4; ++i) {
            //    items.eq(i).find('.title').text(testTitle[i]); // KT 멤버십, TV 포인트 등
            //    items.eq(i).find('.amount_text').text(HTool.addComma(testMoney[i])); // 가격
            //    items.eq(i).find('.amount_unit').text(testUnit[i]); // 단위
            //    items.eq(i).find('.btn').text(testButton[i]); // 버튼 이름
            //}
            // TEST END
        }

        function changeFocus(amount) {
            focus += amount;

            if (focus < 0) {
                focus = 3;
            } else if (focus > 3) {
                focus = 0;
            }
            setFocus(focus);
        }

        function setFocus(_focus) {
            div.find(".itemList .item.focus").removeClass("focus");
            focus = _focus;
            div.find(".itemList .item").eq(_focus).addClass("focus");
        }

        function getLanguage() {
            var language = MenuDataManager.getCurrentMenuLanguage();
            0 == UTIL.isValidVariable(language) && (language = "kor");
            if(language == "kor") {
                return "kor";
            } else {
                return "eng";
            }
        }

        function getTitleList() {
            var language = getLanguage();
            var titleList = [];
            switch (language) {
                case "kor" :
                    titleList = [
                        menuNames[0].name,
                        menuNames[1].name,
                        menuNames[2].name,
                        menuNames[3].name
                    ];
                    break;
                case "eng" :
                    titleList = [
                        menuNames[0].englishItemName,
                        menuNames[1].englishItemName,
                        menuNames[2].englishItemName,
                        menuNames[3].englishItemName
                    ];
                    break;
            }
            return titleList;
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                //gyuh 2017-05-21
                case KEY_CODE.LEFT:
                    if(!btnFocus) {
                        changeFocus(-1);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(!btnFocus) {
                        changeFocus(1);
                    }
                    return true;
                case KEY_CODE.UP:
                    btnFocus = false;
                    div.find(".popup_button").removeClass("focus");
                    setFocus(focus);
                    return true;
                case KEY_CODE.DOWN:
                    btnFocus = true;
                    div.find(".popup_button").addClass("focus");
                    div.find(".itemList .item.focus").removeClass("focus");
                    return true;
                // gyuh 2017-05-21
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.ENTER:
                    if(btnFocus) {
                        LayerManager.deactivateLayer({id: this.id});
                    }else {
                        AppServiceManager.changeService({
                            nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                            obj: {
                                type: CONSTANT.APP_TYPE.MULTICAST,
                                param: HTool.getPointLocator(focus),
                            }
                        });
                    }
                    return true;
            }
        };
    };

    myHome.popup.coupon_charge_popup.prototype = new Layer();
    myHome.popup.coupon_charge_popup.prototype.constructor = myHome.popup.coupon_charge_popup;

    //Override create function
    myHome.popup.coupon_charge_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.popup.coupon_charge_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["CouponChargePopup"] = myHome.popup.coupon_charge_popup;
})();