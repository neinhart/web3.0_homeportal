/**
 * Created by ksk91_000 on 2016-08-11.
 */

(function() {
    myHome.popup = myHome.popup||{};
    myHome.popup.home_cugList = function Constructor(options) {
        Layer.call(this, options);

        var div = _$("<div/>", {class: "popup_contents"});
        var data = [];
        var page, maxPage;
        var focus, btnFocus = false;

        this.init = function() {
            this.div.attr({class: "arrange_frame myhome gniPopup monthlypayPopup"});

            this.div.append(div);
            div.append(_$(
                "<div class='popup_title'>CUG 모아보기</div>" +
                "<div class='itemList'></div>" +
                "<div class='page_indicator'></div>" +
                "<div class='popup_button'>닫기</div>"
            ));

            addItems(this.getParams());
            setFocus(0);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.show.call(this);
        };

        function addItems(data) {
            var itemList = div.find('.itemList');
            for(var i=0; i<4; i++) itemList.append(getItem());
            setData(data);
        }

        function getItem() {
            var item = _$('<div/>', {class:'item'});
                item.html('<div class="focus_red"><div class="black_bg"></div></div>' +
                    //'<div class="bg_area"><div class="bg_left"/><div class="bg_mid"/><div class="bg_right"/></div>' +
                    '<div class="contents_Area"><div class="title_wrapper"><div class="title"></div></div><div class="btn"></div>' +
                    '</div>');
            return item;
        }

        function setData(_data, _page) {
            if(_data){
                data = _data;
                maxPage = Math.ceil(_data.length/4);
            }
            page = _page==null?0:_page;
            var items = div.find(".itemList .item");
            for(var i=0; i<4; i++) {
                setItem(items.eq(i), data[page*4+i]);
            } div.find(".page_indicator").text((page+1) + "/" + maxPage);

            div.find(".itemList").toggleClass("rightArw", page<maxPage-1);
            div.find(".itemList").toggleClass("leftArw", page!=0 && maxPage>1);

            function setItem(div, data) {
                if(data) {
                    div.find(".title").text(data.cugName);
                    div.find(".btn").text("바로가기");
                    div.show();
                }else {
                    div.find(".title").text("");
                    div.find(".btn").text("");
                    div.hide();
                }
            }
        }

        function changeFocus(amount) {
            focus += amount;
            while(focus<0) {
                changePage(-1);
                if(page==maxPage-1) focus = data.length%4-1;
                else focus += 4;
            }if(page*4+focus>=data.length) {
                setPage(0);
                focus-=(data.length%4);
            }while(focus>=4 || page*4+focus>=data.length) {
                changePage(1);
                focus -= 4;
            }setFocus(focus);
        }

        function changePage(amount) {
            setPage(HTool.getIndex(page, amount, maxPage));
        }

        function setFocus(_focus) {
            div.find(".itemList .item.focus").removeClass("focus");
            focus = _focus;
            div.find(".itemList .item").eq(_focus).addClass("focus");
        }

        function setPage(_page) {
            setData(null, _page);
        }

        var goToCategory = function (catId) {
            var menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) { if(menu.id==catId) return true; }
            })[0];
            if(menuData != undefined) {
                MenuServiceManager.jumpMenu({menu: menuData});
            } else {
                showToast("잘못된 접근입니다");
            }
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(!btnFocus) changeFocus(-1);
                    return true;
                case KEY_CODE.RIGHT:
                    if(!btnFocus) changeFocus(1);
                    return true;
                case KEY_CODE.UP:
                    if(btnFocus) {
                        btnFocus = false;
                        div.find(".popup_button").removeClass("focus");
                        setFocus(focus);
                    } return true;
                case KEY_CODE.DOWN:
                    if(!btnFocus) {
                        btnFocus = true;
                        div.find(".popup_button").addClass("focus");
                        div.find(".itemList .item.focus").removeClass("focus");
                    } return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.ENTER:
                    if(btnFocus) {
                        LayerManager.deactivateLayer({id: this.id});
                    }else {
                        switch (data[focus].linkType - 0) {
                            case 0:
                                var nextState;
                                var obj = {type: CONSTANT.APP_TYPE.MULTICAST};
                                if (data[focus].linkInfo.indexOf(".") >= 0) {
                                    obj.param = data[focus].linkInfo;
                                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                                } else {
                                    obj.param = CONSTANT.APP_ID.MASHUP;
                                    obj.ex_param = data[focus].linkInfo;
                                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                                }

                                AppServiceManager.changeService({
                                    nextServiceState: nextState,
                                    obj: obj
                                });
                                break;
                            case 1:
                                goToCategory(data[focus].linkInfo);
                                break;
                            case 2:
                                //미사용
                                break;
                            case 3:
                                var nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                                AppServiceManager.changeService({
                                    nextServiceState: nextState,
                                    obj: {
                                        type: CONSTANT.APP_TYPE.UNICAST,
                                        param: data.linkInfo
                                    }
                                });
                                break;
                        }
                        return true;
                    }
            }
        }
    };

    myHome.popup.home_cugList.prototype = new Layer();
    myHome.popup.home_cugList.prototype.constructor = myHome.popup.home_cugList;

    //Override create function
    myHome.popup.home_cugList.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    myHome.popup.home_cugList.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeCUGListPopup"] = myHome.popup.home_cugList;
})();