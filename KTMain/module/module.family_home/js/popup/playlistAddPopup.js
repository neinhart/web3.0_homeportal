/**
 * 마이 플레이리스트 추가 팝업
 * (우리집 맞춤 TV > 마이 플레이리스트 > 연관메뉴 > 플레이 리스트 추가)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.playlistAddPopup = function PlayListAddPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var osk = new OSKModule(10);
        var ime = new IMEModule();
        var focus = 2;
        var callback;
        var secondTextTimeout;
        var curText = "";

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup playlist_add_popup"});

            div.append(_$("<title/>").text("마이 플레이리스트"));
            div.append(_$("<span/>", {class:"text1"}).html("원하는 플레이리스트 명을 입력하세요<br>플레이리스트 명은 10자까지 등록 가능합니다"));
            div.append(_$("<div/>", {class:"contents_box"}));
            div.append(_$("<div/>", {class:"btn_area"}));

            makeContentsBox();
            makeButtonArea();

            this.div.append(div);
            setFocus(focus);
            if(cbCreate) cbCreate(true);
        };

        function makeContentsBox() {
            var box = div.find(".contents_box");
            box.append(_$("<div/>", {class:"osk_input_area"}));
            var osk_area = box.find(".osk_input_area");
            osk_area.append(_$("<div class='bg_area'><div class='bg_left'/><div class='bg_mid'/><div class='bg_right'/></div>"));
            osk_area.append(_$("<div/>", {class: "input_box"}));
            osk_area.append(osk.getView(div.find(".osk_input_area .input_box")));
            osk.setOnTextChangeListener(onTextChangeListener);
            osk.setOverTextListener(onOverTextListener);
            osk.setOnCompleteInputListener(onCompleteInputListener);
            osk.init();
        }

        function makeButtonArea() {
            var d = div.find(".btn_area");
            d.append(_$("<btn/>", {class:"btn_1"}).text("확인"));
            d.append(_$("<btn/>", {class:"btn_2"}).text("취소"));
        }

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
            callback = this.getParams().callback;
            osk.setText(myHome.MyPlayListManager.getNewPlayListName());
        };

        this.controlKey = function(keyCode) {
            var res;
            switch (focus) {
                case 0: res = onKeyForInputBox(keyCode);
                    break;
                case 1: res = onKeyForOSK(keyCode);
                    break;
                case 2: case 3: res = onKeyForButtonArea(keyCode);
                break;
            }

            if((keyCode == KEY_CODE.RED||keyCode == KEY_CODE.DEL) && (focus == 0 || focus == 1)) {
                if(curText.length < 10) chageSecondText();

                if(compareText != null && compareText != curText) {
                    chageSecondText();
                }
            }

            return res;
        };

        function onKeyForInputBox(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP: setFocus(2); return true;
                case KEY_CODE.DOWN: setFocus(1, 1); return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RED:
                case KEY_CODE.DEL:
                    osk.controlKey(KEY_CODE.RED);
                    ime.setText(osk.getText());
                    return true;
                case KEY_CODE.BLUE:
                case KEY_CODE.RIGHT:
                    osk.controlKey(KEY_CODE.BLUE);
                    ime.setText(osk.getText());
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.PLAY:
                    setFocus(2);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    osk.setText(ime.onKeyDown(keyCode));
                    return true;
            }
        }

        function onKeyForOSK(keyCode) {
            if(osk.controlKey(keyCode)) {
                if(keyCode == KEY_CODE.ENTER) {
                    if(curText.length < 10) chageSecondText();

                    if(compareText != null && compareText != curText) {
                        chageSecondText();
                    }
                }
                return true;
            }

            switch (keyCode) {
                case KEY_CODE.UP: setFocus(0); return true;
                case KEY_CODE.DOWN: setFocus(2); return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.PLAY:
                    setFocus(2);
                    return true;
            }
        }
        
        function chageSecondText(text) {
            if(secondTextTimeout) {
                clearTimeout(secondTextTimeout);
                secondTextTimeout = null;
            }

            if(!inputedSpChar){
                if(text) div.find(".text1").html("원하는 플레이리스트 명을 입력하세요<br>" + text);
                else  {
                    div.find(".text1").html("원하는 플레이리스트 명을 입력하세요<br>플레이리스트 명은 10자까지 등록 가능합니다");
                    compareText = null;
                }
            }else{
                div.find(".text1").html("플레이리스트 명으로 특수문자 입력이 불가합니다<br>다른 이름을 등록해 주세요");
                inputedSpChar = false;
            }
        }

        function checkTextSpace(_text) {
            var text = encodeURI(_text);
            var spaceCompare = '%C2%A0';
            for(var i = 0; i < 10 ;i++) {
                if(text == spaceCompare) return true;
                spaceCompare += '%C2%A0';
            }
            return false;
        }

        var compareText = null;

        function onKeyForButtonArea(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP: setFocus(1, 3); return true;
                case KEY_CODE.DOWN: setFocus(0); return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    setFocus(focus==2?3:2);
                    return true;
                case KEY_CODE.ENTER:
                    if(focus==2) {
                        if(curText.trim().length==0) {
                            chageSecondText();
                            div.find(".text1").html("입력된 플레이리스트 명이 없습니다<br>다른 이름을 등록해 주세요");
                        }else myHome.MyPlayListManager.addPlayItemList(curText, function (res, data) {
                            if(res==2) {
                                compareText = curText;
                                chageSecondText();
                                div.find(".text1").html("이미 존재하는 플레이리스트 명 입니다<br>다른 이름을 등록해 주세요");
                            } else if(res==1) {
                                compareText = null;
                                callback(res, data);
                                showToast("'" + curText +  "' 로 저장되었습니다");
                                LayerManager.historyBack();
                            } else if(!res) {
                                //통신장애 처리
                            }
                        });
                    }else if(focus==3) LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        }


        var inputedSpChar = false;
        function onTextChangeListener(text) {
            if(text=="" && !inputedSpChar) div.find(".text1").html("원하는 플레이리스트 명을 입력하세요<br>플레이리스트 명은 10자까지 등록 가능합니다");

            if([")", "?", "!", "%", "^", ":", ";", "~", "♥", "(", "@", "/", ".", ","].indexOf(text.substr(text.length-1, 1))>-1) {
                inputedSpChar = true;
                osk.controlKey(KEY_CODE.RED);
                ime.setText(osk.getText());
            }else {
                curText = text;
                div.find(".osk_input_area .input_box").html(HTool.addSpace(text));
            }
        }

        function onOverTextListener() {
            chageSecondText("플레이리스트 명은 10자를 초과하여 입력할 수 없습니다");
            secondTextTimeout = setTimeout(function () {
                chageSecondText();
            }, 5000);
        }

        function onCompleteInputListener() {
            setFocus(2);
        }

        function onChangeModeListener(index) {
            osk.changeCharMode(index)
        }

        var setFocus = function(_focus, focusY) {
            div.find(".focus").removeClass("focus");
            osk.blurred();
            focus = _focus;
            switch (focus) {
                case 0: // 인풋박스
                    div.find(".contents_box .input_box").addClass("focus blinker");
                    ime.init(osk.getText(), osk.getCharMode(), onChangeModeListener);
                    break;
                case 1: // OSK영역
                    div.find(".contents_box .input_box").addClass("blinker");
                    osk.focused(focusY);
                    break;
                case 2: // 버튼영역
                case 3:
                    div.find(".contents_box .input_box").removeClass("blinker");
                    setButtonFocus(focus-2);
                    break;
            }
        };

        function setButtonFocus(btnFocus) {
            div.find(".btn_area btn").removeClass("focus");
            div.find(".btn_area btn").eq(btnFocus).addClass("focus");
        }
    };

    myHome.popup.playlistAddPopup.prototype = new Layer();
    myHome.popup.playlistAddPopup.constructor = myHome.layer.playlistAddPopup;

    myHome.popup.playlistAddPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.playlistAddPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["playlistAddPopup"] = myHome.popup.playlistAddPopup;
})();