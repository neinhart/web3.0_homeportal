/**
 * 별점 평가 완료 팝업
 * (우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기 > 나의 취향 알아보기 > 별점 평가 더하기 > 완료)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_rating_complete_popup = function RatingCompletePopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus = 0;
        var params;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup rating_complete_popup"});
            params = this.getParams();

            div.append(_$("<title/>").text("평가 완료"));
            div.append(_$("<span/>", {class:"text1"}).html("총 " + params.count + "개 콘텐츠를 평가했습니다"));
            div.append(_$("<span/>", {class:"text2"}).html("콘텐츠 평가를 많이 할수록 추천이 정확해 집니다"));
            div.append(_$("<div/>", {class:"btn_area"}));

            div.find(".btn_area").append(_$("<btn/>").text("확인").addClass("focus"));
            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    LayerManager.startLoading();
                    if(params.callback) params.callback(true);
                    return true;
            }
        };
    };

    myHome.popup.home_rating_complete_popup.prototype = new Layer();
    myHome.popup.home_rating_complete_popup.constructor = myHome.layer.home_rating_complete_popup;

    myHome.popup.home_rating_complete_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_rating_complete_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeRatingCompletePopup"] = myHome.popup.home_rating_complete_popup;
})();