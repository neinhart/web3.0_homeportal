/**
 * 마이플레이 리스트 추가 (VOD를 플레이리스트에 추가)
 * Created by skkwon on 2017-03-30.
 *
 * params - [itemType 1 (시리즈)]
 *          catId       카테고리 아이디
 *          contsId     컨텐츠 아이디
 *          itemType	아이템 타입(1 입력)
 *          cmbYn	    통합편성 여부
 *          resolCd	    통합편성이면 '|'으로 구분하여 입력
 *          seriesType	시리즈 타입 입력
 *
 *
 *          [itemType 1, seriesType 02 (MCID)]
 *          catId	    카테고리 아이디
 *          contsId	    컨텐츠 아이디
 *          itemType	아이템 타입(1 입력)
 *          cmbYn	    통합편성 여부
 *          resolCd	    통합편성이면 |으로 구분하여 입력
 *          seriesType	시리즈 타입 입력
 *          optionSet	옵션데이터 (선택)
 *
 *
 *          [itemType 2(단편)]
 *          catId	    카테고리 아이디
 *          contsId	    컨텐츠 아이디
 *          cmbYn	    통합편성 여부
 *          itemType	아이템 타입(2 입력)
 *          isDvdYn	    소장여부 (default N)
 *          resolCd	    화질값
 *          assetId	    화질별 어셋 아이디
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.addToMyPlayListPopup = function AddToMyPlayListPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var data;
        var optionList = [], playList;
        var OPTION_TYPE = {SINGLE: 0, SERIES: 1, DVD: 2};
        var focus = [0, 0, 0];
        var select = [-1, -1, 0];
        var curPage = [0, 0, 0];
        var totalPage = [0, 0, 0];
        var index = 2;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup add_to_playlist_popup"});

            data = this.getParams();
            div.append(_$("<title/>").text("마이 플레이리스트 추가"));
            div.append(_$("<div/>", {class:"content_name"}));
            div.append(_$("<div/>", {class:"content_area"}));
            div.append(_$("<div/>", {class:"kidsMode_noti_text"}).text("*키즈모드 ON 설정 시 키즈 플레이리스트 콘텐츠만 이용 가능합니다"));

            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("취소")));
            makeContentArea();
            this.div.append(div);

            focus = [0, 0, 0];
            myHome.MyPlayListManager.clearPlayListCache();
            if(KidsModeManager.isKidsMode()) {
                div.find(".kidsMode_noti_text").css("display", "block");
                myHome.MyPlayListManager.getKidsPlayItemList(function (res, listData) {
                    if (res) {
                        playList = [listData];
                        setPlayListArea(0);
                        setData(data, cbCreate);
                    }
                });
            }else {
                div.find(".kidsMode_noti_text").css("display", (data.isKids ? "block" : "none"));
                myHome.MyPlayListManager.getPlayItemList(function (res, listData) {
                    if (res) {
                        playList = listData;
                        if(data.isKids) restructurePlayListForOnKids();
                        setPlayListArea(0);
                        setData(data, cbCreate);
                    }
                }, data.isKids);
            }
        };

        function restructurePlayListForOnKids(){
            // playList
            var tempList = [];
            var kidsIdx = 0;
            if(playList){
                for(var i =0; i<playList.length; i++){
                    if(playList[i].isKidsYn == "Y"){
                        tempList[0] = playList[i];
                        kidsIdx = i;
                        break;
                    }
                }

                for(var i=0; i<playList.length; i++){
                    if(i != kidsIdx) tempList[tempList.length] = playList[i];
                }

                playList = tempList;
            }
        }
        
        function makeContentArea() {
            var area = div.find(".content_area");
            var productArea = _$("<div/>", {class:"productArea"});
            productArea.append("<div class='title_area'><div class='index_number'>1</div><div class='text'>추가하려는 상품을 선택해 주세요</div></div>");
            productArea.append(
                "<div class='option_area'>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span>단편</span>" +
                    "</div>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span>시리즈</span>" +
                    "</div>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span>소장</span>" +
                    "</div>" +
                "</div>");

            var resolutionArea = _$("<div/>", {class:"resolutionArea"});
            resolutionArea.append("<div class='title_area'><div class='index_number'>2</div><div class='text'>추가하려는 화질을 선택해 주세요</div></div>");
            resolutionArea.append(
                "<div class='option_area'>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span></span>" +
                    "</div>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span></span>" +
                    "</div>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span></span>" +
                    "</div>" +
                    "<div class='option'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<span></span>" +
                    "</div>" +
                "</div>");

            var listArea = _$("<div/>", {class:"listArea"});
            listArea.append("<div class='title_area'><div class='index_number'>3</div><div class='text'>추가하려는 플레이리스트를 선택해 주세요</div></div>");
            listArea.append(
                "<div class='option_area'>" +
                    "<div class='option btn'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<div class='title'/>" +
                        "<div class='amount'/>" +
                    "</div>" +
                    "<div class='option btn'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<div class='title'/>" +
                        "<div class='amount'/>" +
                    "</div>" +
                    "<div class='option btn'>" +
                        "<div class='focus_red'><div class='black_bg'></div></div>" +
                        "<div class='title'/>" +
                        "<div class='amount'/>" +
                    "</div>" +
                "</div>");
            listArea.append("<div class='index_area'>");

            area.append("<div class='sep_line'/>").append(productArea).append("<div class='sep_line'/>").append(resolutionArea).append("<div class='sep_line'/>").append(listArea).append("<div class='sep_line'/>");
        }

        function setContentArea() {
            setProductArea();
            arrangeResolution();
            setResolutionArea((curPage[1] = Math.floor((focus[1] = select[1] = getResolutionFocus())/4)));
            syncFocusUI();
        }

        function getResolutionFocus() {
            for(var i=0; i<optionList[focus[0]].length; i++) {
                if(optionList[focus[0]][i].resolName==="HD") return i ;
            } return 0;
        }

        function setProductArea() {
            var options = div.find(".productArea .option_area .option");
            options.removeClass("select focus");
            options.hide();
            var prdtCnt = 0;
            for(var i=2; i>=0; i--) {
                if(optionList[i]) options.eq(i).show(), prdtCnt++, focus[0] = i;
            }

            options.eq(focus[0]).addClass("select");
            select[0] = focus[0];
        }

        function setResolutionArea(page) {
            var i=0, options = div.find(".resolutionArea .option_area .option");
            options.removeClass("select");
            for(var j=page*4; i<4&&j<optionList[focus[0]].length; i++,j++) {
                if(j==select[1]) options.eq(i).addClass("select");
                //options.eq(i).text(optionList[focus[0]][j].title);
                options.eq(i).find("span").text(optionList[focus[0]][j].title);
                options.eq(i).show();
            } for(;i<4; i++) {
                options.eq(i).hide();
            } div.find(".resolutionArea .option_area").toggleClass("has_left", page>0).toggleClass("has_right", (page+1)*4<optionList[focus[0]].length);
        }

        function setPlayListArea(page) {
            var i=0, options = div.find(".listArea .option_area .option");
            for(var j=page*3; i<3&&j<playList.length; i++,j++) {
                options.eq(i).find(".title").html(HTool.addSpace(playList[j].playListNm));
                options.eq(i).find(".amount").text("(" + playList[j].itemCnt + ")");
                options.eq(i).show();
            } for(;i<3; i++) {
                options.eq(i).hide();
            }div.find(".listArea .index_area").text((page+1) + "/" + Math.ceil(playList.length/3));
            div.find(".listArea .option_area").toggleClass("has_left", page>0).toggleClass("has_right", (page+1)*3<playList.length);
        }

        function syncFocusUI() {
            div.find(".focus").removeClass("focus");
            if(Math.floor(focus[1] / 4) !== curPage[1]) setResolutionArea(curPage[1] = Math.floor(focus[1]/4));
            if(Math.floor(focus[2] / 3) !== curPage[2]) setPlayListArea(curPage[2] = Math.floor(focus[2]/3));
            var focusedArea;
            switch(index) {
                case 0:
                    focusedArea = div.find(".content_area .productArea").addClass("focus");
                    focusedArea.find(".option").eq(focus[index]).addClass("focus");
                    break;
                case 1:
                    focusedArea = div.find(".content_area .resolutionArea").addClass("focus");
                    focusedArea.find(".option").eq(focus[index]%4).addClass("focus");
                    break;
                case 2:
                    focusedArea = div.find(".content_area .listArea").addClass("focus");
                    focusedArea.find(".option").eq(focus[index]%3).addClass("focus");
                    break;
                case 3:
                    div.find(".btn_area btn").addClass("focus");
                    break;
            } if(focusedArea) {
                focusedArea.prev().addClass("focus");
                focusedArea.next().addClass("focus");
            }
        }

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        function setData(data, callback) {
            try {
                div.find(".content_name").text("'" + data.itemName + "'");
                if (data.itemType == 2 || data.itemType == 4) setDataForContent(data, callback);
                else if (data.itemType == 1) setDataForSeries(data, callback);
                else if (callback) callback(false);
            } catch (e) {
                if(callback) callback(false);
            }
        }

        function setDataForContent(data, callback) {
            if(!data.assetId && data.cmbYn=="N") data.assetId = data.contsId;
            if(data.assetId) {
                setData(data);
            } else {
                myHome.amocManager.getContentW3(function (result, data) {
                    if(result) setData(data);
                    else if(callback) callback(false);
                }, data.catId?data.catId:"N", data.contsId, DEF.SAID, true);
            }

            function setData(data) {
                var type = (data.isDvdYn&&data.isDvdYn=="Y")?OPTION_TYPE.DVD:OPTION_TYPE.SINGLE;
                optionList[type] = [];
                var resolutionList = data.resolCd.split("|");
                var assetList = data.assetId.split("|");
                for(var i=0 ;i<resolutionList.length; i++) {
                    if (resolutionList[i] != "")
                        optionList[type].push({
                            title: getTitle(data, resolutionList[i]),
                            hdYn: "",
                            contsId: assetList[i],
                            resolName: getResolName(data, resolutionList[i]),
                            subtitleDubbed: data.subtitleDubbed
                        });
                }
                setContentArea();
                callback(true);
            }
        }

        function setDataForSeries(data, callback) {
            if(!data.contsId) data.contsId = data.catId;
            if(data.seriesType) {
                setData(data);
            } else {
                myHome.amocManager.getCateInfoW3(function (result, catData) {
                    if(result) {
                        data.seriesType = catData.seriesType;
                        data.contsId = catData.catId;
                        data.resolCd = catData.resolCd;
                        setData(data);
                    } else if(callback) callback(false);
                }, data.contsId, DEF.SAID, true);
            }

            function setData(data) {
                if(data.seriesType==="02") setDataForMCID(data, callback);
                else {
                    optionList[OPTION_TYPE.SERIES] = [];
                    var resolutionList = data.resolCd.split("|");
                    for(var i=0 ;i<resolutionList.length; i++) {
                        if(resolutionList[i].trim() != ""){
                            optionList[OPTION_TYPE.SERIES].push({
                                title: getTitle(data, resolutionList[i]),
                                hdYn: ((data.cmbYn==="Y")?getHdYnCd(resolutionList[i]):""),
                                contsId: data.contsId,
                                resolName: getResolName(data, resolutionList[i]),
                                subtitleDubbed: data.subtitleDubbed
                            });
                        }
                    }
                    setContentArea();
                    callback(true);
                }
            }
        }

        function arrangeResolution() {
            for(var i in optionList) sortByResolution(optionList[i]);

            function sortByResolution(list) {
                var resolOrder = ["일반", "SD", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];
                list.sort(function(a, b) {
                    var resol = resolOrder.indexOf(a.resolName) - resolOrder.indexOf(b.resolName);
                    if (resol != 0) return resol;
                    var subtitle = (a.subtitleDubbed ? a.subtitleDubbed - 0 : -1) - (b.subtitleDubbed ? b.subtitleDubbed - 0 : -1);
                    if (subtitle != 0) return subtitle;
                    return 0;
                });
            }
        }

        function setDataForMCID(data, callback) {
            if(data.optionSet) {
                for(var i in data.optionSet) {
                    var type = data.optionSet[i].type==1?OPTION_TYPE.DVD:OPTION_TYPE.SINGLE;
                    optionList[type] = optionList[type]||[];
                    for(var j=0 ;j<data.optionSet[i].optionList.length; j++) {
                        var option = data.optionSet[i].optionList[j];
                        optionList[type].push({
                            title : getTitle(option.data, option.resolCd),
                            hdYn: "",
                            contsId: option.assetId,
                            resolName: getResolName(option, option.resolCd),
                            subtitleDubbed: option.subtitleDubbed
                        });
                    }
                } setContentArea();
                callback(true);
            }else (data.cmbYn=="Y"?myHome.amocManager.getCateContNxtW3:myHome.amocManager.getCateContExtW3)(function (result, response) {
                if(result) {
                    var contList = response.seriesContsList || response.cmbSeriesContsList;
                    if(!Array.isArray(contList)) contList = [contList];
                    var applySaleOption = false;
                    for(var i in contList) {
                        if(contList[i].saleOptionYn==="Y") {
                            applySaleOption = true;
                            break;
                        }
                    } for(var i in contList) {
                        if(applySaleOption && contList[i].saleOptionYn==="N") continue;
                        var type = contList[i].isDvdYn=="Y"?OPTION_TYPE.DVD:OPTION_TYPE.SINGLE;
                        optionList[type] = optionList[type]||[];
                        var resolutionList = contList[i].resolCd.split("|");
                        var assetList = (contList[i].assetId||contList[i].contsId).split("|");
                        resolutionList.reverse();
                        assetList.reverse();
                        for(var j=0 ;j<resolutionList.length; j++) {
                            if(resolutionList[j].trim() != ""){
                                optionList[type].push({
                                    // title: getTitle(data, resolutionList[j]),
                                    title: getTitle(contList[i], resolutionList[j]),
                                    hdYn: "",
                                    contsId: assetList[j],
                                    resolName: getResolName(contList[i], resolutionList[j]),
                                    subtitleDubbed: contList[i].subtitleDubbed
                                })
                            }
                        }
                    }
                    setContentArea();
                    callback(true);
                } else callback(false);
            }, data.contsId, DEF.SAID);
        }

        function getResolName(data, resolCd) {
            if(data.isHdrYn === "Y") return "HDR";
            else if(data.dimension === "3D") return "3D";
            else if(data.screenFormat === 2) return "WIDE";
            else if(resolCd==="SD") return "일반";
            else return resolCd;
        }

        function getTitle(data, resolCd) {
            var resolName = getResolName(data, resolCd);

            if(data.subtitleDubbed=="0") resolName+=" 자막";
            else if(data.subtitleDubbed=="1") resolName+=" 더빙";

            return resolName;
        }

        function getHdYnCd(resolCd) {
            switch (resolCd) {
                case "HD" : return "Y";
                case "SD" : return "N";
                case "FHD" : return "F";
                case "UHD" : return "U";
            }
        }

        this.controlKey = function(keyCode) {
            if(onKeyAction[index](keyCode)) return true;
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };

        var onKeyAction = [
            function (keyCode) {
                switch (keyCode) {
                    case KEY_CODE.LEFT:
                        do { focus[0] = HTool.getIndex(focus[0], -1, 3); } while (!optionList[focus[0]]);
                        setResolutionArea((curPage[1] = Math.floor((focus[1] = select[1] = getResolutionFocus())/4)));
                        syncFocusUI();
                        return true;
                    case KEY_CODE.RIGHT:
                        do { focus[0] = HTool.getIndex(focus[0], 1, 3); } while (!optionList[focus[0]]);
                        select[1] = -1;
                        setResolutionArea((curPage[1] = Math.floor((focus[1] = select[1] = getResolutionFocus())/4)));
                        syncFocusUI();
                        return true;
                    case KEY_CODE.UP:
                        return true;
                    case KEY_CODE.ENTER:
                        focus[1] = 0;
                        select[0] = focus[0];
                        div.find(".productArea .option_area .option").removeClass("select").eq(focus[0]).addClass("select");
                    case KEY_CODE.DOWN:
                        if (select[0] > -1) {
                            index = 1;
                            focus[0] = select[0];
                            focus[1] = select[1]===-1?0:select[1];
                            setResolutionArea((curPage[1] = Math.floor((focus[1] = select[1] = getResolutionFocus())/4)));
                            syncFocusUI();
                        } return true;
                }
            },
            function (keyCode) {
                switch (keyCode) {
                    case KEY_CODE.LEFT:
                        focus[1] = HTool.getIndex(focus[1], -1, optionList[focus[0]].length);
                        syncFocusUI();
                        return true;
                    case KEY_CODE.RIGHT:
                        focus[1] = HTool.getIndex(focus[1], 1, optionList[focus[0]].length);
                        syncFocusUI();
                        return true;
                    case KEY_CODE.UP:
                        focus[1] = Math.max(select[1], 0);
                        index = 0;
                        syncFocusUI();
                        return true;
                    case KEY_CODE.ENTER:
                        select[1] = focus[1];
                        div.find(".resolutionArea .option_area .option").removeClass("select").eq(focus[1]%4).addClass("select");
                    case KEY_CODE.DOWN:
                        if (select[1] > -1) {
                            focus[1] = select[1];
                            // focus[2] = 0;
                            index = 2;
                            syncFocusUI();
                        } return true;
                }
            },
            function (keyCode) {
                switch (keyCode) {
                    case KEY_CODE.LEFT:
                        focus[2] = HTool.getIndex(focus[2], -1, playList.length);
                        syncFocusUI();
                        return true;
                    case KEY_CODE.RIGHT:
                        focus[2] = HTool.getIndex(focus[2], 1, playList.length);
                        syncFocusUI();
                        return true;
                    case KEY_CODE.UP:
                        index = 1;
                        syncFocusUI();
                        return true;
                    case KEY_CODE.ENTER:
                        addMyPlayList();
                        return true;
                    case KEY_CODE.DOWN:
                        index = 3;
                        syncFocusUI();
                        return true;
                }
            },
            function (keyCode) {
                switch (keyCode) {
                    case KEY_CODE.LEFT:
                    case KEY_CODE.RIGHT:
                    case KEY_CODE.DOWN:
                        return true;
                    case KEY_CODE.UP:
                        index = 2;
                        syncFocusUI();
                        return true;
                    case KEY_CODE.ENTER:
                        LayerManager.historyBack();
                        return true;
                }
            }
        ];

        function addMyPlayList() {
            var itemType, hdYn;
            switch (select[0]) {
                case 0 :  itemType = 2; hdYn = optionList[select[0]][select[1]].hdYn; break; // 단편
                case 1 :  itemType = 1; hdYn = optionList[select[0]][select[1]].hdYn; break; // 시리즈
                case 2 :  itemType = 2; hdYn = optionList[select[0]][select[1]].hdYn; break; // 소장
            }
            myHome.MyPlayListManager.addPlayItemDetList(function(res){
                if(res) {
                    if (ModuleManager.getModule("module.kids")) ModuleManager.getModule("module.kids").execute({method: "kidsPlayListRefresh"});
                    LayerManager.historyBack();
                }
            }, playList[Math.floor(focus[2] % 3)+((curPage[2]) * 3)].playListId, optionList[select[0]][select[1]].contsId, itemType, hdYn);
        }
    };

    myHome.popup.addToMyPlayListPopup.prototype = new Layer();
    myHome.popup.addToMyPlayListPopup.constructor = myHome.popup.addToMyPlayListPopup;

    myHome.popup.addToMyPlayListPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.addToMyPlayListPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["AddToMyPlayListPopup"] = myHome.popup.addToMyPlayListPopup;
})();