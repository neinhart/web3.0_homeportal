/**
 * 성인 인증 팝업
 *
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.adultAuthPopup = function AdultAuthPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus = 0;
        var callback;
        var speechAdapter = oipfAdapter.speechRecognizerAdapter;
        var authMgr = AuthManager;
        var starString = "";

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup adult_auth_popup"});

            div.append(_$("<title/>").text("인증"));
            div.append(_$("<span/>", {class:"text1"}).html("olleh tv 비밀번호(성인인증)를<br>입력해 주세요"));
            div.append(_$("<div/>", {class:"input_box"}).append("<div class='back'>****</div><div class='front'/>"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("취소")));

            this.div.append(div);
            setFocus(focus);
            AuthManager.resetPWCheckCount();
            callback = this.getParams().callback;
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            if(this.getParams()) div.find(".text1").html((this.getParams().type||"olleh tv 비밀번호(성인인증)") + "를<br>입력해 주세요");
            else div.find(".text1").html("olleh tv 비밀번호(성인인증)를<br>입력해 주세요");
            authMgr.initPW();
            speechAdapter.addSpeechRecognizerListener(inputSpeechNumber);
            if(focus===0) speechAdapter.start(true);
            clearNumber();

            Layer.prototype.show.apply(this, arguments);
        };

        this.hide = function () {
            speechAdapter.stop();
            speechAdapter.removeSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (focus) {
                case 0: return onKeyForInputBox(keyCode);
                case 1: return onKeyForButtonArea(keyCode);
            }
        };

        function onKeyForInputBox(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    return true;
                case KEY_CODE.DOWN:
                    setFocus(1);
                    return true;
                case KEY_CODE.ENTER:
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    inputNumber(keyCode - KEY_CODE.NUM_0);
                    return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.DEL:
                    deleteNumber();
                    return true;
            }
        }

        function onKeyForButtonArea(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    setFocus(0);
                    return true;
                case KEY_CODE.DOWN:
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    return true;
            }
        }

        var inputNumber = function (num) {
            if(starString.length<4) starString += "*";
            div.find(".input_box .front").text(starString);
            if(authMgr.inputPW(num, 4)) checkPassword();
        };

        var inputSpeechNumber = function(rec, input, mode) {
            if (rec && mode === oipfDef.SR_MODE.PIN) {
                starString = "****".substr(0, input.length);
                div.find(".input_box .front").text(starString);
                if(authMgr.replacePW(input, 4)) checkPassword();
                log.printInfo("inputSpeechNumber::" + input);
            }
        };

        var deleteNumber = function () {
            if(starString.length>0) {
                authMgr.deletePW();
                starString = starString.substr(0, starString.length-1);
                div.find(".input_box .front").text(starString);
            }
        };

        var clearNumber = function () {
            authMgr.initPW();
            starString = "";
            div.find(".input_box .front").text(starString);
        };

        function checkPassword() {
            authMgr.checkUserPW({
                type: AuthManager.AUTH_TYPE.AUTH_ADULT_PIN,
                callback: function(responseCode) {
                    log.printDbg("checkUserPW ResponseCode === " + responseCode);
                    if (responseCode == AuthManager.RESPONSE.CAS_AUTH_SUCCESS) callback();
                    else {
                        div.find(".text1").html("비밀번호가 일치하지 않습니다<br>다시 입력해 주세요");
                        clearNumber();
                    }
                },
                loading: {
                    type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock: true,
                    callback: null,
                    on_off: false
                }
            })
        }

        var setFocus = function(_focus) {
            div.find(".focus").removeClass("focus");
            focus = _focus;
            switch (focus) {
                case 0: // 인풋박스
                    div.find(".input_box").addClass("focus");
                    speechAdapter.start(true);
                    break;
                case 1:
                    div.find(".btn_area btn").addClass("focus");
                    speechAdapter.stop();
                    break;
            }
        };
    };

    myHome.popup.adultAuthPopup.prototype = new Layer();
    myHome.popup.adultAuthPopup.constructor = myHome.popup.adultAuthPopup;

    myHome.popup.adultAuthPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.adultAuthPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["adultAuthPopup"] = myHome.popup.adultAuthPopup;
})();