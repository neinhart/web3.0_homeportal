/**
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.singleTextPopup = function SingleTextPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var data;

        this.init = function (cbCreate) {
            data = this.getParams();
            this.div.attr({class: "arrange_frame myhome gniPopup single_text_popup"});

            if(data.btnText) {
                div.append(_$("<title/>"));
                div.append(_$("<span/>", {class:"text1"}));
                div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").addClass("focus")));
            } else {
                div.append(_$("<span/>", {class:"process"}));
            }

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            if(data.btnText) {
                div.find("title").text(data.title);
                div.find("btn").html(data.btnText);
                div.find(".text1").html(data.content);
            } else {
                div.find(".process").html(data.content);
            }

            Layer.prototype.show.apply(this, arguments);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    if(div.find("btn")) {
                        LayerManager.deactivateLayer({ id: this.id });
                        if(data.callback) data.callback(true);
                    }
                    return true;
            }
        };
    };

    myHome.popup.singleTextPopup.prototype = new Layer();
    myHome.popup.singleTextPopup.constructor = myHome.layer.singleTextPopup;

    myHome.popup.singleTextPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.singleTextPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["SingleTextPopup"] = myHome.popup.singleTextPopup;
})();