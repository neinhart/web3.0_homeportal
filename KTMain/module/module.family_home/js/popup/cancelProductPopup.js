/**
 * 월정액 해지 신청 팝업
 * (우리집 맞춤 TV > 마이메뉴 > 가입정보 > 월정액 상품 해지)
 * Created by Taesang on 2016-12-30.
 */

(function() {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_cancelProduct = function cancelProductLayer(options) {
        var speechAdapter = oipfAdapter.speechRecognizerAdapter;
        var div, data, passwordCnt = 0, focusRow = 0, focusIdx = 0, btnText = ["확인", "취소"];
        Layer.call(this, options);

        var isShowingPopup = false;

        function renderPassword() {
            var starText = "";
            if (passwordCnt > 0) {
                div.find(".inputBox .inputText").removeClass("focus");
                div.find(".inputBox .password").addClass("focus");
                for (var i = 0; i < passwordCnt; i++) {
                    if (passwordCnt == 4 && i == 3) starText += "*";
                    else starText += "* "
                }
            } else {
                div.find(".inputBox .password").removeClass("focus");
                div.find(".inputBox .inputText").addClass("focus");
                starText = "";
            }
            div.find(".inputBox .password.fill").text(starText);
            starText = null;
        }

        function fillPassword(_password) {
            if (passwordCnt >= 4) return;
            passwordCnt++;
            renderPassword();
            if (AuthManager.inputPW(_password, 4)) {
                focusRow = 1;
                focusIdx = 0;
                setFocus();
            }
        }

        var inputSpeechNumber = function(rec, input, mode) {
            if (rec && mode === oipfDef.SR_MODE.PIN) {
                passwordCnt = input.length;
                renderPassword();
                if(AuthManager.replacePW(input, 4)) {
                    focusRow = 1;
                    focusIdx = 0;
                    setFocus();
                }
            }
        };

        function deletePassword() {
            if (passwordCnt <= 0) return;
            passwordCnt--;
            AuthManager.deletePW();
            renderPassword();
        }

        function checkPassword() {
            AuthManager.checkUserPW({
                type: AuthManager.AUTH_TYPE.AUTH_BUY_PIN,
                callback: function(responseCode) {
                    log.printDbg("checkUserPW ResponseCode === " + responseCode);
                    if (responseCode == AuthManager.RESPONSE.HDS_BUY_AUTH_SUCCESS) cancelProduct();
                    else {
                        div.find(".inputBox .inputText").text("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
                        passwordCnt = focusRow = 0;
                        AuthManager.initPW();

                        /**
                         * 2018.01.30 sw.nam
                         * 가입 해지 시 비밀번호 입력 창에서 3회 이상 틀린 후에 비밀번호 초기화 설정 메뉴로 JUMP 되었을 떄 팝업 창이 닫히는데,
                         * 이때 닫히면서 호출되는 hide 함수보다 callback 함수가 늦게 호출되면서  setFocus 내부에 있는 speechAdapter.start() 함수가 호출됨.
                         * 그래서 음성 PIN 입력 창이 아님에도 불구하고 음성 PIN 입력 UI 가 노출되는 이슈가 있음
                         * 기존에 layermanager 의 isShowing() 함수를 이용해 수정하려 했으나 isShowing 이 제대로 동작을? 안해서 로컬flag 를 이용해 이슈 수정
                         * this.hide 가 호출 된 경우에는 callback 이 불려도 setFocus() 함수를 실행하지 않도록 수정 WEBIIIHOME-3627
                         */
                        if(isShowingPopup) {
                            setFocus();
                        }
                    }
                },
                loading: {
                    type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock: true,
                    callback: null,
                    on_off: false
                }
            });
        }

        function setFocus() {
            if (focusRow == 0) {
                div.find(".inputBox").addClass("focus");
                div.find(".twoBtnArea_280 .btn").removeClass("focus");
                speechAdapter.start(true);
            } else {
                div.find(".inputBox").removeClass("focus");
                div.find(".twoBtnArea_280 .btn").removeClass("focus");
                // if(passwordCnt == 0) focusIdx = 1;
                div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
                speechAdapter.stop();
            }
            if (passwordCnt > 0) {
                div.find(".inputBox .password").addClass("focus");
                div.find(".inputBox .inputText").removeClass("focus");
            } else {
                if (focusRow != 0) div.find(".inputBox .inputText").text("구매인증 번호 4자리를 입력해 주세요");
                div.find(".inputBox .password").removeClass("focus");
                div.find(".inputBox .inputText").addClass("focus");
            }
        }

        function cancelProduct() {
            LayerManager.startLoading({preventKey: true});
            homeImpl.get(homeImpl.DEF_FRAMEWORK.HDS_MANAGER).onlineChVodSvcCncl(AuthManager.getPW(), data.pkgCode, function(a, b) {
                LayerManager.stopLoading();
                LayerManager.historyBack();
                if (a) {
                    var response = b.responseXML.getElementsByTagName("string")[0].textContent;
                    log.printDbg("returnVal = " + response.split('|')[0].split('^')[1].toLowerCase());
                    if (response.split('|')[0].split('^')[1].toLowerCase() === "true") {
                        // 성공
                        LayerManager.activateLayer({
                            obj: {
                                id: "CancelProductComplete",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {}
                            },
                            moduleId: "module.family_home",
                            new: true,
                            visible: true
                        });
                    } else {
                        // 실패
                        var errorMsg = response.split('|')[1].split('^')[1];
                        log.printDbg("returnDESC = " + errorMsg);
                        var hdsMessageArray = errorMsg.replace(/\\n/g, '\n').split("\n");
                        HTool.openErrorPopup({message: hdsMessageArray, title:"해지 신청"});
                    }
                } else {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET0013, reboot: true});
                }
            });
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome gniPopup cancelProduct");
            div.append(_$("<span/>", {class: "popup_title"}).text("해지 신청"));
            div.append(_$("<span/>", {class: "text_white_center_45M"}));
            div.append(_$("<span/>", {class: "greyInfoText price"}).text("가격 :"));
            div.append(_$("<span/>", {class: "greyInfoText date"}).text("가입 날짜 :"));
            div.append(_$("<span/>", {class: "whiteInfoText price"}));
            div.append(_$("<span/>", {class: "whiteInfoText date"}));
            div.append(_$("<div/>", {class: "inputBox"}));
            div.find(".inputBox").append(_$("<span/>", {class: "inputText"}).text("구매인증 번호 4자리를 입력해 주세요"));
            div.find(".inputBox").append(_$("<span/>", {class: "password"}).text("* * * *"));
            div.find(".inputBox").append(_$("<span/>", {class: "password fill"}));
            div.append(_$("<span/>", {class: "description"}).html("해지 상품 요금 문의 : 국번 없이 100번")); //※상품 해지에 따른 요금 정보는 고객센터로 문의 바랍니다" + "<br>" + "(국번 없이 100번)"));
            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            log.printDbg(data);
            focusRow = focusIdx = passwordCnt = 0;
            AuthManager.resetPWCheckCount();
            AuthManager.initPW();
            setFocus();
            div.find(".text_white_center_45M").text("'" + data.pkgName + "'");
            div.find(".whiteInfoText.price").text("월 " + HTool.addComma(data.vatPrice) + "원");
            div.find(".whiteInfoText.date").text(HTool.getDateDotFormat(data.regDate));
            speechAdapter.addSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.show.call(this);

            isShowingPopup = true;
        };

        this.hide = function() {
            speechAdapter.stop();
            speechAdapter.removeSpeechRecognizerListener(inputSpeechNumber);
            Layer.prototype.hide.call(this);

            isShowingPopup = false;
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (focusRow == 1) {
                        focusIdx = HTool.indexMinus(focusIdx, 2);
                        setFocus();
                    }
                    else {
                        deletePassword();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow == 1) {
                        focusIdx = HTool.indexPlus(focusIdx, 2);
                        setFocus();
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 1) {
                        if (focusIdx == 0) {
                            checkPassword();
                        } else {
                            LayerManager.historyBack();
                        }
                    }
                    return true;
                case KEY_CODE.UP:
                    focusRow = 0;
                    setFocus();
                    return true;
                case KEY_CODE.DOWN:
                    focusRow = 1;
                    focusIdx = 0;
                    setFocus();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    if (keyCode >= KEY_CODE.NUM_0 && keyCode <= KEY_CODE.NUM_9) {
                        if (focusRow == 0) fillPassword(keyCode - KEY_CODE.NUM_0);
                        return true;
                    }
            }
        };
    };

    myHome.popup.home_cancelProduct.prototype = new Layer();
    myHome.popup.home_cancelProduct.prototype.constructor = myHome.popup.home_cancelProduct;

    myHome.popup.home_cancelProduct.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_cancelProduct.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["CancelProductPopup"] = myHome.popup.home_cancelProduct;
})();