/**
 *  Copyright (c) 2018 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>otmp_masterUserChangePopup</code>
 *
 * @author sw.nam
 * @since 2018-02-07
 *
 * 마스터 변경 확인 팝업
 */

(function() {
    var otmp_changeMaster = function otmpChangeMasterPopup(options) {
        Layer.call(this, options);
        var _this = this, div, focusIdx;
        var params;

        function setFocus() {
            div.find(".button_area .button").removeClass("focus");
            div.find(".button_area .button").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {

            params = _this.getParams();
            if(!params) {
                return;
            }
            div = this.div;
            div.addClass("arrange_frame myhome otmp_changeMaster");
            div.append(_$("<span/>", {class: "popupTitle"}).text(params.title));
            div.append(_$("<span/>", {class: "description1"}).text(params.mainText));
            div.append(_$("<div/>", {class: "box"}));

            div.append(_$("<img class='dwArrowIcon' style = 'position: absolute; left: 942px; top:487px; width: 36px; height: 20px;'>"));

            div.append(_$("<span/>", {class: "description3"}).html(params.subText));
            div.append(_$("<div/>", {class: "button_area"}));

            div.find(".box").append(_$("<div class = 'boxItem1' style = 'margin-top: 30px; display: block; margin-left: auto; margin-right: auto; width: 180px;'></div>"));
            div.find(".box").append(_$("<div class = 'boxItem2' style = 'margin-top: 140px; display: block; margin-left: auto; margin-right: auto; width: 180px;'></div>"));
            div.find(".boxItem1").append(_$("<img/>", {class: "fromNickImage"}));
            div.find(".boxItem1").append(_$("<span/>", {class: "fromNick"}).text(params.fromNick));


            div.find(".boxItem2").append(_$("<img/>", {class: "toNickImage"}));
            div.find(".boxItem2").append(_$("<span/>", {class: "toNick"}).text(params.toNick));



            div.find(".dwArrowIcon").attr({ src : modulePath+"resource/image/arw_pop_menu_dw.png" });
            div.find(".fromNickImage").attr({ src : modulePath+"resource/otmpImage/icon_master_no.png" });
            div.find(".toNickImage").attr({ src : modulePath+"resource/otmpImage/icon_master.png" });

            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));

            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);

            var nickWidth, imageWidth, sum;

            nickWidth = div.find(".fromNick").width();
            imageWidth = div.find(".fromNickImage").width();
            sum = nickWidth+imageWidth + 16;
            div.find(".boxItem1").width(sum);

            nickWidth = div.find(".toNick").width();
            imageWidth = div.find(".toNickImage").width();
            sum = nickWidth+imageWidth + 18;
            div.find(".boxItem2").width(sum);

            focusIdx = 0;
            setFocus();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    return true;
                case KEY_CODE.DOWN:
                    return true;
                case KEY_CODE.LEFT:
                    setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    if(focusIdx == 0) {
                        params.callback(true)
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_changeMaster.prototype = new Layer();
    otmp_changeMaster.prototype.constructor = otmp_changeMaster;

    otmp_changeMaster.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_changeMaster.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPChangeMaster"] = otmp_changeMaster;
})();