/**
 * Created by Lazuli on 2017-10-17.
 *
 * 휴대폰 인증 팝업 (번호 입력하고 통신사 정하고 인증 번호 받고 ..)
 */
(function() {
    var otmp_mobileAuth = function otmpMobileAuthPopup(options) {
        Layer.call(this, options);
        var _this = this, div, getParamsData, focusIdx, focusRow, focusDrop;
        var dropBoxOpened = false;
        var selectedCorp = -1;
        var phoneNumber = ["000", "0000", "0000"], totalPhoneNumber, hasPhoneNumber = [false, false, false], authNumber = "";

        function nextStep() {
            focusRow++;
            setFocus(focusIdx = 0);
            resetPhoneNumber(focusRow == 1);
        }

        function setFocus() {
            div.find(".corpArea .corpBtn").removeClass("focus");
            div.find(".phoneArea .phoneInput").removeClass("focus");
            div.find(".authArea .smsBtn").removeClass("focus");
            div.find(".authArea .pwInput").removeClass("focus");
            div.find(".button_area .button").removeClass("focus");
            if (focusRow == 0) div.find(".corpArea .corpBtn").eq(focusIdx).addClass("focus");
            else if (focusRow == 1) div.find(".phoneArea .phoneInput").eq(focusIdx).addClass("focus");
            else if (focusRow == 2) {
                if (focusIdx == 0) div.find(".authArea .smsBtn").addClass("focus");
                else div.find(".authArea .pwInput").addClass("focus");
            }
            else if (focusRow == 3) div.find(".button_area .button").eq(focusIdx).addClass("focus");
        }

        // 통신사 버튼 생성
        function makeCorpButton() {
            var corpList = ["KT", "SKT", "LG U+", "알뜰폰"];
            div.find(".bgBox").append(_$("<div/>", {class: "corpArea"}));
            for (var i = 0; i < 4; i++) {
                if (i == 3) div.find(".bgBox .corpArea").append(_$("<div class='corpBtn dropBtn'><span class='corpText'>" + corpList[i] + "</span><div class='optionIcon dropArw'></div></div>"));
                else div.find(".bgBox .corpArea").append(_$("<div class='corpBtn'><span class='corpText'>" + corpList[i] + "</span><div class='optionIcon'></div></div>"));
            }
        }

        function makeDropBox() {
            var corpList = ["KT 알뜰폰", "SKT 알뜰폰", "LG U+ 알뜰폰", "알뜰폰 사업자 확인"];
            div.find(".bgBox").append(_$("<div/>", {class: "dropboxArea"}));
            div.find(".bgBox .dropboxArea").append(_$("<div/>", {class: "options"}));
            for (var i = 0; i < 4; i++) {
                div.find(".bgBox .dropboxArea .options").append(_$("<div class='option'><span class='optionText'>" + corpList[i] + "</span><div class='selectedIcon'></div></div>"));
            }
        }

        // 통신사 선택
        function selectCorp(noNextStep) {
            if (focusIdx < 3) {
                selectedCorp = focusIdx;
                div.find(".corpArea .corpBtn .optionIcon").removeClass("show");
                div.find(".corpArea .corpBtn .optionIcon").eq(selectedCorp).addClass("show");
                div.find(".bgBox .dropboxArea .options .option .selectedIcon").removeClass("show");
                div.find(".corpArea .corpBtn.dropBtn .corpText").text("알뜰폰");
                div.find(".corpArea .corpBtn.dropBtn .optionIcon").addClass("dropArw");
                if (!noNextStep) nextStep();
            } else openDropBox();
        }

        function selectDropCorp() {
            selectedCorp = focusDrop + 3;
            div.find(".corpArea .corpBtn .optionIcon").removeClass("show");
            div.find(".bgBox .dropboxArea .options .option .selectedIcon").removeClass("show");
            div.find(".bgBox .dropboxArea .options .option .selectedIcon").eq(focusDrop).addClass("show");
            div.find(".corpArea .corpBtn.dropBtn .corpText").text(div.find(".bgBox .dropboxArea .options .option .optionText").eq(focusDrop).text());
            div.find(".corpArea .corpBtn.dropBtn .optionIcon").removeClass("dropArw");
            div.find(".corpArea .corpBtn.dropBtn .optionIcon").addClass("show");
            closeDropBox();
            nextStep();
        }

        function setFocusDropBox() {
            div.find(".bgBox .dropboxArea .options .option").removeClass("focus");
            div.find(".bgBox .dropboxArea .options .option").eq(focusDrop).addClass("focus");
        }

        function openDropBox() {
            dropBoxOpened = true;
            div.find(".bgBox .boxText").css("opacity", ".3");
            div.find(".bgBox .phoneArea").css("opacity", ".3");
            div.find(".bgBox .authArea").css("opacity", ".3");
            div.find(".corpArea .corpBtn.dropBtn").hide();
            div.find(".bgBox .dropboxArea").show();
            if (selectedCorp > 2) setFocusDropBox(focusDrop = selectedCorp - 3);
            else setFocusDropBox(focusDrop = 0);
        }

        function closeDropBox() {
            dropBoxOpened = false;
            div.find(".bgBox .boxText").css("opacity", "1");
            div.find(".bgBox .phoneArea").css("opacity", "1");
            div.find(".bgBox .authArea").css("opacity", "1");
            div.find(".corpArea .corpBtn.dropBtn").show();
            div.find(".bgBox .dropboxArea").hide();
        }

        // 휴대폰 번호 영역 생성
        function makePhoneArea() {
            div.find(".bgBox").append(_$("<div/>", {class: "phoneArea"}));
            div.find(".bgBox .phoneArea").append(_$("<div/>", {class: "phoneInput"}).text(phoneNumber[0]));
            div.find(".bgBox .phoneArea").append(_$("<div/>", {class: "phoneHyphen"}));
            div.find(".bgBox .phoneArea").append(_$("<div/>", {class: "phoneInput"}).text(phoneNumber[1]));
            div.find(".bgBox .phoneArea").append(_$("<div/>", {class: "phoneHyphen"}));
            div.find(".bgBox .phoneArea").append(_$("<div/>", {class: "phoneInput"}).text(phoneNumber[2]));
        }

        function resetPhoneNumber(_reset) {
            for (var i = 0; i < 3; i++) {
                if (i == 0) {
                    if (phoneNumber[0].length == 0 || !hasPhoneNumber[0]) {
                        phoneNumber[0] = _reset ? "" : "000";
                        div.find(".phoneArea .phoneInput").eq(0).text(phoneNumber[0]);
                    }
                }
                else if (phoneNumber[i].length == 0 || !hasPhoneNumber[i]) {
                    phoneNumber[i] = _reset ? "" : "0000";
                    div.find(".phoneArea .phoneInput").eq(i).text(phoneNumber[i]);
                }
            }
        }

        function inputPhoneNumber(_phoneNum) {
            log.printDbg("inputPhoneNumber >> " + _phoneNum);
            if (focusIdx == 0) {
                if (phoneNumber[focusIdx].length == 3) phoneNumber[focusIdx] = "";
                phoneNumber[focusIdx] += _phoneNum;
                div.find(".phoneArea .phoneInput").eq(focusIdx).text(phoneNumber[focusIdx]);
                hasPhoneNumber[focusIdx] = true;
                if (phoneNumber[focusIdx].length == 3) setFocus(focusIdx++);
            } else {
                if (phoneNumber[focusIdx].length == 4) phoneNumber[focusIdx] = "";
                phoneNumber[focusIdx] += _phoneNum;
                div.find(".phoneArea .phoneInput").eq(focusIdx).text(phoneNumber[focusIdx]);
                hasPhoneNumber[focusIdx] = true;
                if (phoneNumber[focusIdx].length == 4) {
                    if (focusIdx == 1) setFocus(focusIdx = 2);
                    else nextStep();
                }
            }
        }

        function deletePhoneNumber() {
            if (phoneNumber[focusIdx].length == 0) {
                hasPhoneNumber[focusIdx] = false;
                if (focusIdx != 0) setFocus(focusIdx -= 1);
            } else {
                phoneNumber[focusIdx] = phoneNumber[focusIdx].substring(0, phoneNumber[focusIdx].length - 1);
                div.find(".phoneArea .phoneInput").eq(focusIdx).text(phoneNumber[focusIdx]);
            }
        }

        // 인증번호 영역 생성
        function makeAuthArea() {
            div.find(".bgBox").append(_$("<div/>", {class: "authArea"}));
            div.find(".bgBox .authArea").append(_$("<div/>", {class: "smsBtn"}).text("인증번호 받기"));
            div.find(".bgBox .authArea").append(_$("<div/>", {class: "pwInput"}).text("* * * * * *"));
            div.find(".bgBox .authArea .pwInput").append(_$("<div/>", {class: "pwUserInput"}));
        }

        function inputAuthNumber(_authNum) {
            log.printDbg("inputAuthNumber >> " + _authNum);
            if (authNumber.length == 6) authNumber = "";
            authNumber += _authNum;
            var starStr = "";
            for (var i = 0; i < authNumber.length; i++) {
                starStr += "* ";
            }
            div.find(".authArea .pwInput .pwUserInput").text(starStr);
            if (authNumber.length == 6) nextStep();
        }

        function deleteAuthNumber() {
            if (authNumber.length == 0) setFocus(focusIdx = 0);
            else {
                authNumber = authNumber.substring(0, authNumber.length - 1);
                var starStr = "";
                for (var i = 0; i < authNumber.length; i++) {
                    starStr += "* ";
                }
                div.find(".authArea .pwInput .pwUserInput").text(starStr);
            }
        }

        function checkUserInfo() {
            var checked = true;
            for (var i = 0; i < 3; i++) {
                if (phoneNumber[i].length == 0) {
                    focusRow = 1;
                    setFocus(focusIdx = 0);
                    checked = false;
                    break;
                }
            }
            if (authNumber.length == 0) {
                focusRow = 2;
                setFocus(focusIdx = 1);
                checked = false;
            }
            return checked;
        }

        function getCorpName(_corpNum) {
            if (_corpNum == 0) return "KTF";
            else if (_corpNum == 1) return "SKT";
            else if (_corpNum == 2) return "LGT";
            else if (_corpNum == 3) return "KTM";
            else if (_corpNum == 4) return "SKM";
            else if (_corpNum == 5) return "LGM";
        }

        function isTruePhoneNumber(_phoneNum) {
            // 휴대폰 번호 유효성 검사
            var isTruePhoneNumber = /(01[0|1|6|9|7])(\d{3}|\d{4})(\d{4}$)/g;
            if (!isTruePhoneNumber.test(_phoneNum)) {
                console.error("PhoneNumber Error");
                LayerManager.activateLayer({
                    obj: {
                        id: "OTMPBasicPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            title: "알림",
                            message: "입력하신 전화번호가 유효하지 않습니다<br>다시 입력해주세요",
                            buttons: [{
                                name: "확인", callback: function() {
                                    focusRow = 0;
                                    setFocus(focusIdx = 0);
                                    for (var i = 0; i < 3; i++) {
                                        phoneNumber[i] = "";
                                        hasPhoneNumber[i] = false;
                                    }
                                    totalPhoneNumber = "";
                                    resetPhoneNumber();
                                    selectCorp(true);
                                    LayerManager.deactivateLayer({id: "OTMPBasicPopup", onlyTarget: true});
                                }
                            }, {name: "취소", callback: "close"}]
                        }
                    },
                    moduleId: "module.family_home",
                    visible: true
                });
                return false;
            }
            return true;
        }

        function requestAuthNumber() {
            totalPhoneNumber = phoneNumber[0] + phoneNumber[1] + phoneNumber[2];
            if (!isTruePhoneNumber(totalPhoneNumber)) return;
            window.OTMPManager.requestAuthNumber(function(result, errType) {
                if (result || errType == "SMS") {
                    // 인증번호 요청 성공 후 InputBox로 포커스 이동 시켜줌
                    // 인증번호가 틀렸을 경우에도 포커스 이동
                    // 이 때 기존에 입력되어 있던 인증번호 삭제함
                    focusRow = 2;
                    setFocus(focusIdx = 1);
                    div.find(".authArea .pwInput .pwUserInput").text(authNumber = "");
                } else if (errType == "PHONE") {
                    //번호가 안맞는 경우
                    focusRow = 0;
                    setFocus(focusIdx = 0);
                    for (var i = 0; i < 3; i++) {
                        phoneNumber[i] = "";
                        hasPhoneNumber[i] = false;
                    }
                    totalPhoneNumber = "";
                    resetPhoneNumber();
                    selectCorp(true);
                }
            }, getCorpName(selectedCorp), totalPhoneNumber);
        }

        // SMS 인증
        function smsVerify() {
            totalPhoneNumber = phoneNumber[0] + phoneNumber[1] + phoneNumber[2];
            if (!isTruePhoneNumber(totalPhoneNumber)) return;
            window.OTMPManager.smsVerify(function(result, errType) {
                if (result) LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                else {
                    if (errType == "PHONE") {
                        focusRow = 0;
                        setFocus(focusIdx = 0);
                        for (var i = 0; i < 3; i++) {
                            phoneNumber[i] = "";
                            hasPhoneNumber[i] = false;
                        }
                        totalPhoneNumber = "";
                        resetPhoneNumber();
                        selectCorp(true);
                    } else if (errType == "SMS") {
                        focusRow = 2;
                        setFocus(focusIdx = 1);
                        div.find(".authArea .pwInput .pwUserInput").text(authNumber = "");
                    }
                }
            }, authNumber);
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome otmp_mobileAuth");
            div.append(_$("<span/>", {class: "popupTitle"}).text("휴대폰 인증"));
            div.append(_$("<span/>", {class: "description"}).text("올레 tv 모바일 연결을 위해서는 휴대폰 인증이 필요합니다"));
            div.append(_$("<div/>", {class: "bgBoxSide left"}));
            div.append(_$("<div/>", {class: "bgBoxSide right"}));
            div.append(_$("<div/>", {class: "bgBox"}));
            div.find(".bgBox").append(_$("<span/>", {class: "boxText"}).text("휴대폰 번호"));
            div.find(".bgBox").append(_$("<span/>", {class: "boxText auth"}).text("인증번호"));
            makeCorpButton();
            makePhoneArea();
            makeAuthArea();
            makeDropBox();
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            getParamsData = _this.getParams();
            focusIdx = focusRow = focusDrop = 0;
            setFocus();
            selectCorp(true);
        };

        function onKeyActionForDropBox(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    setFocusDropBox(focusDrop = HTool.getIndex(focusDrop, -1, 4));
                    return true;
                case KEY_CODE.DOWN:
                    setFocusDropBox(focusDrop = HTool.getIndex(focusDrop, 1, 4));
                    return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                    if (focusDrop < 3) {
                        selectDropCorp();
                    } else LayerManager.activateLayer({
                        obj: {
                            id: "OTMPLicenseList",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: null
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    closeDropBox();
                    return true;
            }
        }

        this.controlKey = function(keyCode) {
            if (dropBoxOpened) return onKeyActionForDropBox(keyCode);
            switch (keyCode) {
                case KEY_CODE.UP:
                    focusRow--;
                    if (focusRow < 0) focusRow = 0;
                    resetPhoneNumber(focusRow == 1);
                    setFocus(focusIdx = 0);
                    return true;
                case KEY_CODE.DOWN:
                    focusRow++;
                    if (focusRow > 3) focusRow = 3;
                    resetPhoneNumber(focusRow == 1);
                    setFocus(focusIdx = 0);
                    return true;
                case KEY_CODE.LEFT:
                    if (focusRow == 0) setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 4));
                    else if (focusRow == 1) deletePhoneNumber();
                    else if (focusRow == 2) deleteAuthNumber();
                    else if (focusRow == 3) setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow == 0) setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 4));
                    else if (focusRow == 1 && focusIdx < 2) setFocus(focusIdx += 1);
                    else if (focusRow == 2) setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    else if (focusRow == 3) setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 0) selectCorp();
                    else if (focusRow == 1) {
                        if (focusIdx < 2) setFocus(focusIdx++);
                        else nextStep();
                    }
                    else if (focusRow == 2) {
                        // 인증 번호 받기
                        if (focusIdx == 0) requestAuthNumber();
                        else nextStep();
                    }
                    else {
                        if (focusIdx == 0) {
                            // 휴대폰 번호 및 인증번호 검증
                            if (checkUserInfo()) smsVerify();
                        } else LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    if (focusRow == 1) inputPhoneNumber(keyCode - KEY_CODE.NUM_0);
                    else if (focusRow == 2 && focusIdx == 1) inputAuthNumber(keyCode - KEY_CODE.NUM_0);
                    return true;
            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_mobileAuth.prototype = new Layer();
    otmp_mobileAuth.prototype.constructor = otmp_mobileAuth;

    otmp_mobileAuth.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_mobileAuth.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPMobileAuth"] = otmp_mobileAuth;
})();