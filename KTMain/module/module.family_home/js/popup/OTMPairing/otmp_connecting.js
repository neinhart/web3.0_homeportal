/**
 * Created by Lazuli on 2017-10-17.
 */
(function() {
    var otmp_connecting = function otmpConnectingPopup(options) {
        Layer.call(this, options);
        var _this = this, div, getParamsData, focusIdx, focusRow;

        function getConnectingBtn(_btnText) {
            var element = _$(
            "<div class='connectingBtnDiv' style='position: relative; width: 255px; height: 228px; margin-right: 12px; float: left;'>" +
                "<div class='connectingBtn'></div>" +
                "<span class='btnName'>" + _btnText + "</span>" +
                "<div class='sellectBtn'>" +
                        "<span class='sellectBtnText'>선택</span>" +
                "</div>" +
            "</div>");
            return element;
        }

        function setFocus() {
            div.find(".connectingBtnArea .connectingBtnDiv").removeClass("focus");
            div.find(".button_area .button").removeClass("focus");
            if (focusRow == 0) div.find(".connectingBtnArea .connectingBtnDiv").eq(focusIdx).addClass("focus");
            else div.find(".button_area .button").addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome otmp_connecting");
            div.append(_$("<span/>", {class: "popupTitle"}).text("올레 tv 모바일 연결"));
            div.append(_$("<span/>", {class: "description1"}).text("연결방법을 선택해 주세요"));
            div.append(_$("<span/>", {class: "description2"}).html("※ 올레 tv 모바일 고객 및 아이폰/패드 고객님은<br>사용 중인 SNS/KT 아이디로 연결해 주세요"));
            div.append(_$("<div/>", {class: "connectingBtnArea"}));
            div.find(".connectingBtnArea").append(getConnectingBtn("휴대폰번호<br>연결하기"));
            div.find(".connectingBtnArea").append(getConnectingBtn("SNS/KT아이디<br>연결하기"));
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            getParamsData = _this.getParams();
            focusIdx = focusRow = 0;
            setFocus();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    focusRow = 0;
                    setFocus(focusIdx = 0);
                    return true;
                case KEY_CODE.DOWN:
                    focusRow = 1;
                    setFocus();
                    return true;
                case KEY_CODE.LEFT:
                    if (focusRow == 0) setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow == 0) setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    if (focusRow == 0) {
                        if (focusIdx == 0) {
                            LayerManager.activateLayer({
                                obj: {
                                    id: "OTMPMobileAuth",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: null
                                },
                                moduleId: "module.family_home",
                                visible: true
                            });
                        } else {
                            var menuData = MenuDataManager.searchMenu({
                                menuData: MenuDataManager.getMenuData(),
                                allSearch: false,
                                cbCondition: function(menu) { if (menu.id == "T0000000000000050140") return true; }
                            });
                            var connectingLocator, exParam = "";
                            if (menuData != null && menuData.length > 0 && menuData[0].itemType == 3) {
                                connectingLocator = menuData[0].locator;
                                exParam = menuData[0].parameter;
                            }
                            else connectingLocator = "1.23F.23F";
                            AppServiceManager.changeService({
                                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                                obj: {
                                    type: CONSTANT.APP_TYPE.MULTICAST,
                                    param: connectingLocator,
                                    ex_param: exParam
                                }
                            });
                        }
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_connecting.prototype = new Layer();
    otmp_connecting.prototype.constructor = otmp_connecting;

    otmp_connecting.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_connecting.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPConnecting"] = otmp_connecting;
})();