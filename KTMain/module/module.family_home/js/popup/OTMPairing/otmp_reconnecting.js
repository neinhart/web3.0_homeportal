/**
 * Created by Lazuli on 2017-10-17.
 *
 * 올레티비 모바일 재 연결 팝업
 *
 */
(function() {
    var otmp_reconnecting = function otmpReconnecting(options) {
        Layer.call(this, options);
        var _this = this, div, focusIdx;
        var params;
        var isKTPhoneCorp = false;

        var focusIdx = 0;
        var timer;
        var authNumber = ""; // 인증번호
        var phoneCorp = "";

        function setFocus(idx) {
            focusIdx = idx;
            log.printDbg("setFocus()");

            div.find(".authArea .textArea").removeClass("focus");
            div.find(".authArea .inputArea").removeClass("focus");

            div.find(".button_area .button").removeClass("focus");

            if(!isKTPhoneCorp) {
                // 휴대폰 인증이 필요한 경우만 포커스 사용
                switch (idx) {
                    case 0: // 인증번호 받기
                        div.find(".authArea .textArea").addClass("focus");
                        break;
                    case 1: // 인증번호 입력 창
                        div.find(".authArea .inputArea").addClass("focus");
                        break;
                    case 2: // 확인 key
                        div.find(".button_area .button").eq(0).addClass("focus");
                        break;
                    case 3: // 취소 key
                        div.find(".button_area .button").eq(1).addClass("focus");
                        break;
                }
            }
        }

        function inputPassword() {
            log.printDbg("inputPassword()");
                if(focusIdx !== 1)
                return;
        }

        this.init = function(cbCreate) {

            params = _this.getParams();
            log.printDbg("params = "+JSON.stringify(params));

            if(params && params.selectedHistItem.phone_corp ) {
                // KT 통신사 휴대폰인 경우(SKT 를 피하기 위한 예외처리 추가)
               if(params.selectedHistItem.phone_corp.indexOf("KT") > -1 && params.selectedHistItem.phone_corp.indexOf("SKT") < 0 ) {
                    isKTPhoneCorp = true;
               }else {
                   isKTPhoneCorp = false;
               }
                phoneCorp = params.selectedHistItem.phone_corp;
            }else {
                // 2018.03.05 sw.nam
                // id로? 로그인 한 경우에는 KT 통신사 정보가 없다.
                isKTPhoneCorp = true;
                phoneCorp = "";
            }

            div = this.div;
            div.addClass("arrange_frame myhome otmp_reconnecting");
            div.append(_$("<span/>", {class: "popupTitle"}).text(isKTPhoneCorp? "재연결" : "재연결 인증"));
            div.append(_$("<span/>", {class: "description1"}).html(isKTPhoneCorp? "올레 tv 모바일 재연결 중입니다.잠시만 기다려 주세요" : "올레 tv 모바일 연결을 위해서<br>휴대폰 인증이 필요합니다"));
            div.append(_$("<div/>", {class: "box"}));
            div.append(_$("<div/>", {class: "phoneNumberArea"}));
           // div.append(_$("<span/>", {class: "description3"}).html(params.subText));

            // isKTPhoneCorp 여부에 따라 어떤 view 를 만들지 결정.
            if(!isKTPhoneCorp) {

                // 2018.03.08 sw.nam
                // 재 연결 팝업 문구 수정으로 KT 외 핸드폰 인증 시 전체적으로 top-margin을 추가한다
                div.find(".box").css({"margin-top" : "38px"});
                div.find(".phoneNumberArea").css({"margin-top" : "38px"});

                div.find(".phoneNumberArea").append(_$("<span class='corp' style='color: rgba(255,255,255,0.7); font-size: 33px; margin-left: 800px; margin-right: 23px; letter-spacing: -1.65px; float:left'></span>"));
                div.find(".phoneNumberArea").append(_$("<div class='bar' style = ' width:2px; height: 26px; background-color: white; opacity: 0.2; margin-top: 5px; margin-right: 23px; float:left'></div>"));
                div.find(".phoneNumberArea").append(_$("<span class='number' style = 'color: white; font-size: 33px; letter-spacing: -1.65px; float:left'></span>"));

                div.find(".corp").text(params.selectedHistItem.phone_corp);
                div.find(".number").text(params.selectedHistItem.nick);

                div.append(_$("<div class='authArea' style='position: absolute; left: 691px; top: 647px; '></div>"));
                div.find(".authArea").append(_$("<div class='textBoxBg textArea' style='position: absolute; left: 0px; top: 0px; width: 240px; height: 70px;'></div>" +
                                                "<span class='text textArea' style ='position: absolute; width: 240px; height: 70px; line-height: 70px; color: white; font-size: 28px; letter-spacing: -1.4px; text-align: center'>인증번호 받기</span>"));

                div.find(".authArea").append(_$("<div class='inputBoxBg inputArea' style='position: absolute; left: 254px; width: 284px; height: 70px; box-sizing: border-box;'></div>" +
                                                "<span class ='pwTextBG inputArea' style='position: absolute; left: 254px; width: 284px; height: 70px; line-height: 82px; color: white; opacity: 0.2; letter-spacing: -2.1px; font-size: 41px; text-align: center;'>* * * * * *</span>"+
                                                "<span class ='pwText inputArea' style='position: absolute; left: 316px; width: 284px; height: 70px; line-height: 82px; color: white; letter-spacing: -2.1px; font-size: 41px;'></span>"));


                div.append(_$("<div/>", {class: "button_area"}));
                div.find(".button_area").css({"margin-top" : "38px"});
                div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
                div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));

            }else {
                div.find(".phoneNumberArea").text(params.selectedHistItem.nick);
            }

            cbCreate(true);
        };

        this.show = function() {

            Layer.prototype.show.call(this);

                        //focusIdx = 0;
                        //setFocus();
            if(isKTPhoneCorp) {
                    var isTimeout = false;
                    var loadingData = {
                        boxSize : {
                        left: 1,
                        top: 150,
                        width: KTW.CONSTANT.RESOLUTION.WIDTH,
                        height: 1310
                    },
                    timeout: 5 * 1000
                };
                 LayerManager.startLoading(loadingData);

                // 재연결
                requestReconnecting();

            }else {
                // 타 통신사 문자 인증 로직 수행
                // 문자 인증에 필요한 정보.
                // 1. 휴대폰 통신사
                // 2. 휴대폰 번호
                // 3. 서비스 키
                // 4. SAID
                // 5. 히스토리 페어링 여부 (재연결 로직이니 반드시 필요)
                // 6. 페어링 히스토리
                // 오브젝트 examples. )
 /*                           nick: "딸기_KT",
                                pairing_hist_id: "asflkajflakaskj",
                                "suid": "suid",
                                master_yn: "N",
                                "phone_corp": "KTF",
                                "phone_no": "01057284850"*/

                // 1. 인증번호 받기 포커스
                // 인증번호 받기 에 포커스 가게 하면 된다.
                setFocus(0);

            }


        };

        this.controlKey = function(keyCode) {
            if(isKTPhoneCorp) {
                switch (keyCode) {
                    case KEY_CODE.EXIT:
                    case KEY_CODE.BACK:
                        LayerManager.stopLoading();
                        LayerManager.deactivateLayer({id: this.id});
                        break;
                    default:
                        return true;
                }
            }
            switch (keyCode) {
                case KEY_CODE.UP:
                    var originIdx = focusIdx;
                    focusIdx = focusIdx -2;
                    if(focusIdx < 0) {
                        focusIdx = originIdx + 2;
                    }
                    setFocus(focusIdx);
                    return true;
                case KEY_CODE.DOWN:
                    focusIdx = focusIdx +2;
                    if(focusIdx > 3) {
                        focusIdx = focusIdx % 4;
                    }
                    setFocus(focusIdx);
                    return true;
                case KEY_CODE.LEFT:

                    if(focusIdx == 0) {
                        focusIdx = 1;
                    }else if(focusIdx == 1) {
                        deleteAuthNumber();
                        return true;
                    }else if(focusIdx == 2) {
                        focusIdx = 3;
                    }else if(focusIdx == 3) {
                        focusIdx = 2;
                    }
                    setFocus(focusIdx);

                    return true;
                case KEY_CODE.RIGHT:

                    if(focusIdx == 0) {
                        focusIdx =1 ;
                    }else if(focusIdx == 1) {
                        return true;
                    }else if(focusIdx == 2) {
                        focusIdx = 3;
                    }else if(focusIdx == 3) {
                        focusIdx = 2;
                    }
                    setFocus(focusIdx);
                    return true;
                case KEY_CODE.ENTER:
                    switch (focusIdx) {
                        case 0:
                            // 인증번호 요청
                            requestAuthNumber();
                            break;
                        case 1:
                            focusIdx++;
                            setFocus(focusIdx);
                            break;
                        case 2:
                         // 확인
                            smsVerify();
                            break;
                        case 3:
                            LayerManager.deactivateLayer({ id: this.id});
                            break;
                    }

                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({ id: this.id});
                    return true;

                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    if (focusIdx == 1) inputAuthNumber(keyCode - KEY_CODE.NUM_0);
                    return true;
            }
        };

        this.hide = function() {
            log.printDbg("hide()");
            Layer.prototype.hide.call(this);

        };

        function inputAuthNumber(_authNum) {
            log.printDbg("inputAuthNumber >> " + _authNum);
            if (authNumber.length == 6) authNumber = "";
            authNumber += _authNum;
            var starStr = "";
            for (var i = 0; i < authNumber.length; i++) {
                starStr += "* ";
            }
            div.find(".authArea .pwText").text(starStr);
            if (authNumber.length == 6) {
                // 확인 키로 이동
                setFocus(2);
            }
        }

        function deleteAuthNumber() {
            if (authNumber.length == 0) {
                // 인증번호가 모두 삭제 된 경우 인증번호 받기 창으로 이동
                setFocus(0);
            }
            else {
                authNumber = authNumber.substring(0, authNumber.length - 1);
                var starStr = "";
                for (var i = 0; i < authNumber.length; i++) {
                    starStr += "* ";
                }
                div.find(".authArea .pwText").text(starStr);
            }
        }

        /**
         * 휴대폰 인증번호 요청
         */
        function requestAuthNumber() {
            log.printDbg("requestAuthNumber()");

            var phoneNumber = params.selectedHistItem.phone_no;
            var phoneCorp = params.selectedHistItem.phone_corp;
            var suid = params.selectedHistItem.suid; // TODO 필수값인가 ?
            var pairingHistId = params.selectedHistItem.pairing_hist_id;
        //   pairing_hist_id

            // 유효성 검사
            if(!phoneNumber && phoneNumber == null && phoneNumber == undefined ) {
                log.printDbg("there's no phoneNumber");
                return;
            }
            if(!phoneCorp && phoneCorp == null && phoneCorp == undefined ) {
                log.printDbg("there's no phoneCorp");
                return;
            }
            if(!pairingHistId && pairingHistId == null && pairingHistId == undefined ) {
                log.printDbg("there's no pairingHistId");
                return;
            }


            // callback,phoneCorp,phoneNumber,
            window.OTMPManager.requestAuthNumber(function(result, errType) {
                log.printDbg("result = "+result+ " errorType = "+errType);

                if (result || errType == "SMS") {
                    // 인증번호 요청 성공 후 InputBox로 포커스 이동 시켜줌
                    // 인증번호가 틀렸을 경우에도 포커스 이동
                    // 이 때 기존에 입력되어 있던 인증번호 삭제함
                    div.find(".authArea .pwText").text("");
                    setFocus(1);


                } else {
                    // 인증번호 요청 단계에서 에러난 경우
                    setFocus(0);
                }
            }, phoneCorp, phoneNumber,suid, pairingHistId);
        }


        /**
         * 인증번호 확인 요청
         */
        function smsVerify() {
            log.printDbg("smsVerify()");

            var number = authNumber;

            if(!number || number.length < 6 || number == null || number == "") {
                // 번호 제대로 입력하도록 포커스 다시 주고
                setFocus(1);
                return;
            }
            window.OTMPManager.smsVerify(function(result, errType) {
                log.printDbg("result = "+result+ " errorType = "+errType);
                if (result) {
                    params.callback(true);
                    LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                }
                else {
                    if(errType == "SMS") {
                        // 인증번호가 잘못된 경우
                        // 인증번호를 초기화 하고 재 입력하도록 한다.
                        setFocus(1);
                        authNumber = "";
                       // resetPhoneNumber();

                    }else {
                        //TODO
                        log.printDbg("Other error");
                    }
                }
            }, authNumber);
        }

        function requestReconnecting() {
            log.printDbg("requestReconnecting()");

            window.OTMPManager.makePairingHistory(function(result) {
                if(result) {
                  //  clearTimer();
                    LayerManager.stopLoading();
                    LayerManager.deactivateLayer({ id: "OTMPReconnecting",onlyTarget: true});
                   log.printDbg("this.id = "+this.id);
                    params.callback(true);

                }else {
                  //  clearTimer();
                    LayerManager.stopLoading();
                    LayerManager.deactivateLayer({ id: "OTMPReconnecting",onlyTarget: true});
                    params.callback(false);
                }

            },params.selectedHistItem.suid,params.selectedHistItem.pairing_hist_id);
        }

    };

    otmp_reconnecting.prototype = new Layer();
    otmp_reconnecting.prototype.constructor = otmp_reconnecting;

    otmp_reconnecting.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_reconnecting.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPReconnecting"] = otmp_reconnecting;
})();