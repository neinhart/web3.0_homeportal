/**
 * Created by Lazuli on 2017-10-17.
 *
 * otmp 연결 해제 및 개정 삭제 팝업
 *
 */
(function() {
    var otmp_disconnect = function otmpDisconnect(options) {
        Layer.call(this, options);
        var _this = this, div, focusIdx;
        var params;

        function setFocus() {
            div.find(".button_area .button").removeClass("focus");
            div.find(".button_area .button").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {

            params = _this.getParams();
            if(!params) {
                return;
            }
            div = this.div;
            div.addClass("arrange_frame myhome otmp_disconnect");
            div.append(_$("<span/>", {class: "popupTitle"}).text(params.title));
            div.append(_$("<span/>", {class: "description1"}).text(params.mainText));
            div.append(_$("<div/>", {class: "box"}));
            div.append(_$("<span/>", {class: "description2"}).text(params.nickName));
            div.append(_$("<span/>", {class: "description3"}).html(params.subText));
            div.append(_$("<div/>", {class: "button_area"}));

            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);

            focusIdx = 0;
            setFocus();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    return true;
                case KEY_CODE.DOWN:
                    return true;
                case KEY_CODE.LEFT:
                    setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    if(focusIdx == 0) {
                        params.callback(true);
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_disconnect.prototype = new Layer();
    otmp_disconnect.prototype.constructor = otmp_disconnect;

    otmp_disconnect.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_disconnect.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPDisconnect"] = otmp_disconnect;
})();