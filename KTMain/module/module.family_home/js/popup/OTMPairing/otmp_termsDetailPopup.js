/**
 * 서비스 이용약관 상세 팝업
 * Created by LeeBaeng on 2017-10-19.
 */

(function() {
    var otmp_termsDetailPopup = function otmpTermsDetailPopup(options){
        Layer.call(this, options);
        var _this = this;
        var div = _$("<div/>", {class: "popup_contents"});
        var data;
        var maxPage, page = 0;

        this.init = function (cbCreate){
            this.div.attr({class: "arrange_frame myhome gniPopup otmp_termsDetailPopup"});
            div.append(_$("<div/>", {class:"title"}));

            div.append(_$("<div/>", {class:"contents_box"}).append("" +
                "<div class='background'/>" +
                "<div class='contents'><span/></div>" +
                "<div class='scroll_bar'><div class='bar'/></div>" +
                "<div class='measureArea'/>")
            );

            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("닫기")));
            div.find(".btn_area btn").addClass("focus");

            var param = this.getParams();
            setData(param.title, param.stplt_text_list);
            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        function setData(title, stplt_text_list) {
            div.find(".title").text(title);

            var stpltList = stplt_text_list;
            stpltList = stpltList.sort(function(a, b) {return (parseInt(a.sort_odrg) > parseInt(b.sort_odrg)) ? 1 : ((parseInt(b.sort_odrg) > parseInt(a.sort_odrg)) ? -1 : 0);});
            var desc = "";
            for(var i=0; i<stpltList.length; i++){
                if(i == stpltList.length-1) {
                    desc += (stpltList[i].title != null && stpltList[i].title.length > 0)? stpltList[i].title + ((stpltList[i].sbst != null && stpltList[i].sbst.length > 0)? "\r\n" : "") : "";
                    desc += (stpltList[i].sbst != null && stpltList[i].sbst.length > 0)? stpltList[i].sbst : "";
                }else{
                    desc += getSbst(stpltList[i].title, stpltList[i].sbst);
                }
            }
            // desc = desc.replace(/\r\n/g, '<br>');
            // desc = desc.replace(/\t/g, '    ');
            // desc = desc.replace(/\n/g, '<br>');
            desc = HTool.convertTextToHtml(desc);
            div.find(".contents_box .contents span").html(desc);
            div.find(".contents_box .measureArea").html(desc);
        }

        function getSbst(title, sbstr) {
            var returnStr = (title != null && title.length > 0)? title + "\r\n" : "";
            returnStr += (sbstr != null && sbstr.length > 0)? sbstr + "\r\n\r\n" : "\r\n";
            return returnStr;
        }

        function setPage(_page) {
            page = _page;
            div.find(".contents span").css("-webkit-transform", "translateY(" + -414*page + "px)");
            div.find(".scroll_bar .bar").css("-webkit-transform", "translateY(" + 414/maxPage*page + "px)");
        }

        this.show = function(){
            Layer.prototype.show.apply(this, arguments);

            maxPage = Math.ceil(div.find(".contents_box .measureArea")[0].offsetHeight/414);
            div.find(".scroll_bar .bar").css("height", 414/maxPage + "px");
            div.find(".scroll_bar").toggle(maxPage>1);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    setPage(Math.max(0, page-1));
                    return true;
                case KEY_CODE.DOWN:
                    setPage(Math.min(maxPage-1, page+1));
                    return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_termsDetailPopup.prototype = new Layer();
    otmp_termsDetailPopup.prototype.constructor = otmp_termsDetailPopup;

    otmp_termsDetailPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_termsDetailPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPTermsDetail"] = otmp_termsDetailPopup;
})();