/**
 * Created by Taesang on 2017-01-20.
 */
(function() {
    var otmp_basicPopup = function otmpBasicPopup(options) {
        Layer.call(this, options);
        var _this = this, div, getParams, buttonLength = 0, curIdx = 0;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame myhome otmp_basicPopup");
            this.div.append(_$("<div/>", {class: "baiscPopup"}));
            div = this.div.find(".baiscPopup");
            div.append(_$("<span/>", {class: "popupTitle"}));
            div.append(_$("<span/>", {class: "popupText"}));
            div.append(_$("<span/>", {class: "popupSubText"}));
            div.append(_$("<div/>", {class: "button_area"}));

            cbCreate(true);
        };

        function makeButton() {
            buttonLength = getParams.buttons.length;
            div.find(".button_area").empty();
            for (var i = 0; i < buttonLength; i++) {
                div.find(".button_area").append(_$("<div class='button'><div class='buttonText'>" + getParams.buttons[i].name + "</div></div>"));
            }
        }

        function makePopup() {
            div.find(".popupTitle").text(getParams.title);
            div.find(".popupText").html(getParams.message);
            if(getParams.subMessage) {
                div.find(".popupSubText").html(getParams.subMessage);
                div.find(".popupSubText").css({display: "block"});
            }else {
                div.find(".popupSubText").css({display: "none"});
            }

        }

        function setFocus(_idx) {
            if (buttonLength > 0) {
                div.find(".button").removeClass("focus");
                div.find(".button").eq(_idx != null ? _idx : curIdx).addClass("focus");
            }
        }

        this.show = function() {
            Layer.prototype.show.call(this);
            getParams = _this.getParams();
            makePopup();
            makeButton();
            setFocus(0);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    setFocus(curIdx = HTool.indexMinus(curIdx, buttonLength));
                    return true;
                case KEY_CODE.RIGHT:
                    setFocus(curIdx = HTool.indexPlus(curIdx, buttonLength));
                    return true;
                case KEY_CODE.ENTER:
                    if (getParams.buttons[curIdx].callback) {
                        if (getParams.buttons[curIdx].callback == "close") LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                        else getParams.buttons[curIdx].callback();
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_basicPopup.prototype = new Layer();
    otmp_basicPopup.prototype.constructor = otmp_basicPopup;

    otmp_basicPopup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_basicPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPBasicPopup"] = otmp_basicPopup;
})();