/**
 * Created by Lazuli on 2017-10-17.
 */
(function() {
    var otmp_terms = function otmpTermsPopup(options) {
        Layer.call(this, options);
        var _this = this, div, originalData, focusIdx, focusRow, popupMode;
        var termsList = [];

        function setFocus() {
            if (focusRow == termsList.length + 1 && !checkAgreeState() && focusIdx == 0) focusIdx = 1;
            div.find(".button_area .button").removeClass("focus");
            div.find(".termsList .terms .termsBtnArea .termsBtn").removeClass("focus");
            div.find(".agreeArea .termsBtn").removeClass("focus");
            if (focusRow < termsList.length) {
                div.find(".termsList .terms").eq(focusRow).find(".termsBtn").eq(focusIdx).addClass("focus");
            } else {
                if (focusRow == termsList.length) div.find(".agreeArea .termsBtn").addClass("focus");
                else if (focusRow == termsList.length + 1) div.find(".button_area .button").eq(focusIdx).addClass("focus");
            }
        }

        function selectTermsBtn(_isAllAgree) {
            if (_isAllAgree) {
                div.find(".termsList .termsBtn.checkBtn").toggleClass("checked", !div.find(".agreeArea .termsBtn").hasClass("checked"));
                div.find(".agreeArea .termsBtn").toggleClass("checked");
            } else {
                if (focusIdx == 1) {
                    div.find(".termsList .terms").eq(focusRow).find(".termsBtn.checkBtn").toggleClass("checked");
                    if (div.find(".termsList .termsBtn.checkBtn.checked").length == termsList.length) div.find(".agreeArea .termsBtn").addClass("checked");
                    else div.find(".agreeArea .termsBtn").removeClass("checked");
                } else {
                    window.OTMPManager.showDetailTermsPopup(termsList[focusRow].stplt_no, popupMode == "otv" ? "1" : "2", termsList[focusRow].stplt_nm);
                }
            }
            if (checkAgreeState() && _isAllAgree) {
                // 전체동의 버튼 선택 시 확인 버튼으로 자동 이동함 (전체 동의 해제 시에는 포커스 유지)
                focusRow++;
                focusIdx = 0;
                setFocus();
            }
        }

        function parseForSend(_agreeList) {
            var tempList = [];
            for (var i = 0; i < _agreeList.length; i++) {
                tempList.push({
                    stplt_no: _agreeList[i].stplt_no,
                    sel_agree_yn: _agreeList[i].sel_agree_yn ? "Y" : "N"
                });
            }
            return tempList;
        }

        function checkAgreeState() {
            var allAgreeYn = true;
            for (var i = 0; i < termsList.length; i++) {
                termsList[i].sel_agree_yn = div.find(".termsList .terms").eq(i).find(".termsBtn.checkBtn").hasClass("checked");
                if (termsList[i].mand_stplt_yn == "Y" && !termsList[i].sel_agree_yn) {
                    allAgreeYn = false;
                    break;
                }
            }
            div.find(".button_area .button").eq(0).toggleClass("dim", !allAgreeYn);
            return allAgreeYn;
        }

        function allAgreeComplete() {
            if (checkAgreeState()) {
                if (popupMode == "otv") {
                    originalData.termsData.otv_list = termsList;
                    if (originalData.mandAgreeYn == "N" && originalData.termsData.otm_list != null && originalData.termsData.otm_list.length > 0) {
                        setPopup({
                            termsKind: "otm",
                            termsList: originalData.termsData.otm_list
                        });
                    } else {
                        // 최종 약관 동의 진행
                        window.OTMPManager.makePairing(function(result) {
                            if (result) LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                        }, parseForSend(originalData.termsData.otv_list), null);
                    }
                } else {
                    originalData.otm_list = termsList;
                    // 최종 약관 동의 진행
                    window.OTMPManager.makePairing(function(result) {
                        if (result) LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                    }, parseForSend(originalData.termsData.otv_list), parseForSend(originalData.termsData.otm_list));
                }
            }
        }

        function getBtnElement(_btnName, _isCheckBtn) {
            var btnClass = _isCheckBtn ? "termsBtn checkBtn" : "termsBtn";
            var element = _$("<div class='" + btnClass + "'>" +
                "<span class='btnName'>" + _btnName + "</span>" +
                (_isCheckBtn ? "<div class='checkBox'/>" : "") +
                "</div>");
            return element;
        }

        function makeTermsList() {
            // 약관 노출 순서대로 정렬
            termsList = termsList.sort(function(a, b) {return (parseInt(a.stplt_show_odrg) > parseInt(b.stplt_show_odrg)) ? 1 : ((parseInt(b.stplt_show_odrg) > parseInt(a.stplt_show_odrg)) ? -1 : 0);});
            div.find(".termsArea .termsList").empty();
            div.find(".agreeArea").empty();
            for (var i = 0; i < termsList.length; i++) {
                var termsName = termsList[i].stplt_nm + (termsList[i].mand_stplt_yn == "Y" ? " (필수)" : " (선택)");
                div.find(".termsArea .termsList").append(_$("<div class='terms'><span class='termsName'>" + termsName + "</span><div class='termsBtnArea'></div></div>"));
            }
            div.find(".termsArea .termsList .terms .termsBtnArea").append(getBtnElement("약관 상세보기"));
            div.find(".termsArea .termsList .terms .termsBtnArea").append(getBtnElement("약관 동의", true));
            div.find(".agreeArea").append(_$("<span/>", {class: "agreeText"}).text(popupMode == "otv" ? "올레 tv 연결을 위한 서비스 약관에 동의해주세요" : "올레 tv와의 연결을 위해 올레 tv 모바일 서비스 이용약관에 동의해주세요"));
            div.find(".agreeArea").append(getBtnElement("전체 동의", true));
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome otmp_terms");
            div.append(_$("<span/>", {class: "popupTitle"}));
            div.append(_$("<div/>", {class: "termsArea"}));
            div.find(".termsArea").append(_$("<div/>", {class: "termsList"}));
            div.find(".termsArea").append(_$("<div/>", {class: "agreeArea"}));
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));
            cbCreate(true);
        };

        function setPopup(_popupData) {
            popupMode = _popupData.termsKind;
            div.find(".popupTitle").text(popupMode == "otv" ? "올레 tv 약관 동의" : "올레 tv 모바일 약관 동의");
            termsList = _popupData.termsList;
            makeTermsList();
            checkAgreeState();
            focusRow = termsList.length;
            focusIdx = 0;
            setFocus();
        }

        this.show = function(options) {
            Layer.prototype.show.call(this);
            if (!options || !options.resume) {
                originalData = _this.getParams();
                setPopup({
                    termsKind: "otv",
                    termsList: originalData.termsData.otv_list
                });
            }
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if (focusRow == termsList.length) focusIdx = 1;
                    focusRow--;
                    if (focusRow < 0) focusRow = 0;
                    setFocus();
                    return true;
                case KEY_CODE.DOWN:
                    // 전체동의나 확인,취소 버튼으로 포커스가 내려갈 때 디폴트포커스를 0번째로 정의함
                    if (focusRow == termsList.length - 1 || focusRow == termsList.length) focusIdx = 0;
                    focusRow++;
                    if (focusRow > termsList.length + 1) focusRow = termsList.length + 1;
                    setFocus();
                    return true;
                case KEY_CODE.LEFT:
                    if (focusRow != termsList.length) setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow != termsList.length) setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == termsList.length) selectTermsBtn(true);
                    else if (focusRow == termsList.length + 1) {
                        focusIdx == 0 ? allAgreeComplete() : LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    } else if (focusRow < termsList.length) selectTermsBtn();
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_terms.prototype = new Layer();
    otmp_terms.prototype.constructor = otmp_terms;

    otmp_terms.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_terms.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPTerms"] = otmp_terms;
})();