/**
 * Created by Lazuli on 2017-10-17.
 */
(function() {
    var otmp_connectingComplete = function otmpConnectingComplete(options) {
        Layer.call(this, options);
        var _this = this, div, focusIdx;

        function setFocus() {
            div.find(".button_area .button").removeClass("focus");
            div.find(".button_area .button").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome otmp_connectingComplete");
            div.append(_$("<span/>", {class: "popupTitle"}).text("올레 tv 모바일 연결"));
            div.append(_$("<span/>", {class: "description1"}).text("올레 tv 모바일과 올레 tv 연결이 완료되었습니다"));
            div.append(_$("<span/>", {class: "description2"}).html("고객님께 발송된 메시지에서 올레 tv 모바일을 실행 후<br>등록한 전화번호로 로그인하시면 서비스 이용이 가능합니다"));
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            focusIdx = 0;
            setFocus();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    return true;
                case KEY_CODE.DOWN:
                    return true;
                case KEY_CODE.LEFT:
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_connectingComplete.prototype = new Layer();
    otmp_connectingComplete.prototype.constructor = otmp_connectingComplete;

    otmp_connectingComplete.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_connectingComplete.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPConnectingComplete"] = otmp_connectingComplete;
})();