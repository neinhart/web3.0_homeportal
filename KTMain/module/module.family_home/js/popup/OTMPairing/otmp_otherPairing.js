/**
 * Created by Lazuli on 2017-10-17.
 */
(function() {
    var otmp_otherPairing = function otmpOtherPairing(options) {
        Layer.call(this, options);
        var _this = this, div, focusIdx;

        function setFocus() {
            div.find(".button_area .button").removeClass("focus");
            div.find(".button_area .button").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome otmp_otherPairing");
            div.append(_$("<span/>", {class: "popupTitle"}).text("알림"));
            div.append(_$("<span/>", {class: "description1"}).html("이 계정은 이미 다른 올레 tv와 연결되어 있습니다"));
            div.append(_$("<span/>", {class: "description2"}).html("기존 올레 tv와 연결을 끊고<br>현재 이용중인 올레 tv와 다시 연결하시겠습니까?"));
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>확인</span></div>"));
            div.find(".button_area").append(_$("<div class='button'><span class='buttonText'>취소</span></div>"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            focusIdx = 0;
            setFocus();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    return true;
                case KEY_CODE.DOWN:
                    return true;
                case KEY_CODE.LEFT:
                    setFocus(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    setFocus(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusIdx == 0) _this.getParams().callback();
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    otmp_otherPairing.prototype = new Layer();
    otmp_otherPairing.prototype.constructor = otmp_otherPairing;

    otmp_otherPairing.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    otmp_otherPairing.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["OTMPOtherPairing"] = otmp_otherPairing;
})();