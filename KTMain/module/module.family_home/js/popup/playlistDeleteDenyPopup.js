/**
 * 플레이 리스트 삭제 거부 팝업?
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.playlistDeleteDenyPopup = function PlayListDeleteDenyPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup playlist_delete_deny_popup"});

            div.append(_$("<title/>").text("알림"));
            div.append(_$("<span/>", {class:"text1"}).html("해당 플레이리스트는 삭제할 수 없습니다"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("확인").addClass("focus")));

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    myHome.popup.playlistDeleteDenyPopup.prototype = new Layer();
    myHome.popup.playlistDeleteDenyPopup.constructor = myHome.popup.playlistDeleteDenyPopup;

    myHome.popup.playlistDeleteDenyPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.playlistDeleteDenyPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["playlistDeleteDenyPopup"] = myHome.popup.playlistDeleteDenyPopup;
})();