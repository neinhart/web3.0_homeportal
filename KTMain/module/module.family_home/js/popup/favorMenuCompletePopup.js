/**
 * 현재 등록된 메뉴 확인/변경 팝업 (즐겨찾기)
 * (우리집 맞춤 TV > 즐겨 찾는 메뉴 > 편집 > 연관메뉴(저장 및 편집))
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.favorMenuCompletePopup = function FavorMenuCompletePopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var editMode = false;
        var focus = 0;
        var menuFocus = 0;
        var btnFocus = 0;
        var pickIndex = -1;
        var curSelectMenu = [];
        var callback;
        var limitL = 0, limitR = 0;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup favor_menu_complete_popup"});

            div.append(_$("<title/>").text("현재 등록된 메뉴 확인/변경"));
            div.append(_$("<div/>", {class:"content_area"}));
            div.append(_$("<div/>", {class:"bottom_text_area"}));
            div.append(_$("<div/>", {class:"button_area"}).append("<btn class='ok'>저장</btn><btn class='cancel'>취소</btn>"));
            div.append(_$("<div/>", {class:"context_menu_indicator"}).text("순서변경"));

            div.find(".bottom_text_area").html("체크를 해제하면 즐겨 찾는 메뉴에서 삭제됩니다<br>순서 변경을 원하는 메뉴에서 <div class='icon'/>키를 누르면 순서 변경이 가능합니다");

            var param = this.getParams();
            curSelectMenu = param.menuList;
            callback = param.callback;

            makeContentsArea();
            setFocus(1, 0);
            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        function makeContentsArea() {
            limitL = 0; limitR = 0;
            var area = _$("<div/>", {class:"absoluteWrapper"});
            for(var i=0; i<6; i++) {
                if(curSelectMenu[i]) {
                    if(curSelectMenu[i].menuType!=500)
                        area.append(
                            "<div class='item position_" + i + "'>" +
                                "<div class='focus_red'><div class='black_bg'></div></div>" +
                                "<div class='checkBox'/>" +
                                "<div class='title'>" + HTool.getTitle(curSelectMenu[i].menuNameKor, curSelectMenu[i].menuNameEng) + "</div>" +
                                "<div class='subTitle'>" + HTool.getTitle(curSelectMenu[i].parentNameKor, curSelectMenu[i].parentNameEng) + "</div>" +
                            "</div>");
                    else {
                        area.append("<div class='item noCheck position_" + i + "'><div class='title'>" + HTool.getTitle(curSelectMenu[i].menuNameKor, curSelectMenu[i].menuNameEng) + "</div><div class='subTitle'>" + HTool.getTitle(curSelectMenu[i].parentNameKor, curSelectMenu[i].parentNameEng) + "</div></div>");
                        limitL++;
                    }limitR=i;
                } else {
                    area.append("<div class='item position_" + i + " noItem'><div class='title'>미등록</div></div>");
                }
            }
            div.find(".content_area").append(area);
            div.find(".context_menu_indicator").toggle(div.find(".item div.checkBox").length>1);
        }

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        function setEditMode(_editMode) {
            if(div.find(".item div.checkBox").length<=1) return;
            editMode = _editMode;
            if(editMode) {
                setFocus(0, menuFocus);
                pickIndex = menuFocus;
                div.find(".content_area .item.position_" + pickIndex).addClass("pick").toggleClass("noLeft", pickIndex==limitL).toggleClass("noRight", pickIndex==limitR);
                div.find("title").text("순서 변경");
                div.find(".bottom_text_area").html("좌/우 방향키를 누르시면 순서 변경이 되며<br>확인키를 누르시면 해당 순서에 메뉴가 위치합니다");
                div.find(".button_area btn.ok").hide();
            } else {
                pickIndex = -1;
                div.find("title").text("현재 등록된 메뉴 확인/변경");
                div.find(".content_area .item.pick").removeClass("pick");
                div.find(".bottom_text_area").html("체크를 해제하면 즐겨 찾는 메뉴에서 삭제됩니다<br>순서 변경을 원하는 메뉴에서 <div class='icon'/>키를 누르면 순서 변경이 가능합니다");
                div.find(".button_area btn.ok").show();
            }
        }

        function saveMenuList() {
            var saveList = [];
            for(var i=0; i<curSelectMenu.length; i++) {
                if(curSelectMenu[i].menuType!=500 && !div.find(".content_area .item.position_" + i).hasClass("unChecked")) {
                    curSelectMenu[i].menuOrder = saveList.length;
                    saveList.push(curSelectMenu[i]);
                }
            } myHome.web3Manager.setFavoriteMenuAdd(saveList, function (result, data) {
                if(callback) callback(result?data.resultCd==0:false, saveList);
                LayerManager.historyBack();
            });
        }

        function setFocus(mf, sf) {
            if(mf===0 && limitL>limitR) return;
            div.find(".focus").removeClass("focus");
            if(focus==0 && mf==1) sf = 0;
            focus = mf;
            switch (focus) {
                case 0:
                    menuFocus = Math.min(Math.max(limitL, sf), limitR);
                    div.find(".content_area .item.position_" + menuFocus).addClass("focus");
                    if(curSelectMenu.length > 1 && !editMode) {
                        if(div.find(".content_area .item.position_" + menuFocus).hasClass("unChecked"))
                            div.find(".context_menu_indicator").hide();
                        else
                            div.find(".context_menu_indicator").show();
                    }
                    else div.find(".context_menu_indicator").hide();
                    break;
                case 1:
                    btnFocus = editMode?1:sf;
                    div.find(".button_area btn").eq(btnFocus).addClass("focus");
                    div.find(".context_menu_indicator").hide();
                    break;
            }
        }

        function moveElement(amount) {
            if(menuFocus+amount<limitL) return;
            if(menuFocus+amount>limitR) return;
            var ee = div.find(".content_area .item.position_" + menuFocus);
            var er = div.find(".content_area .item.position_" + (menuFocus+amount));
            ee.removeClass("position_" + menuFocus).addClass("position_" + (menuFocus+amount));
            er.removeClass("position_" + (menuFocus+amount)).addClass("position_" + menuFocus);
            ee.toggleClass("noLeft", menuFocus+amount==limitL).toggleClass("noRight", menuFocus+amount==limitR);
            menuFocus = menuFocus+amount;
        }

        function swapObject(a, b) {
            var tmp = curSelectMenu.splice(a, 1)[0];
            curSelectMenu.splice(b, 0, tmp);
        }

        function onKeyForNormalState(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if(focus==1) {
                        setFocus(0, menuFocus%3+3);
                    } else {
                        if(menuFocus>=3) setFocus(0, menuFocus-3);
                    } return true;
                case KEY_CODE.DOWN:
                    if(focus==0) {
                        if(menuFocus>=3 || limitR<3) setFocus(1, btnFocus);
                        else setFocus(0, menuFocus+3);
                    } return true;
                case KEY_CODE.RIGHT:
                    if(focus==1){
                        setFocus(1, btnFocus^1);
                    } else {
                        setFocus(0, menuFocus+1);
                    } return true;
                case KEY_CODE.LEFT:
                    if(focus==1){
                        setFocus(1, btnFocus^1);
                    } else {
                        setFocus(0, menuFocus-1);
                    } return true;
                case KEY_CODE.ENTER:
                    if(focus==0) {
                        if(menuFocus>=limitL) div.find(".content_area .item.position_" + menuFocus).toggleClass("unChecked")
                        if(div.find(".content_area .item.position_" + menuFocus).hasClass("unChecked"))
                            div.find(".context_menu_indicator").hide();
                        else
                            div.find(".context_menu_indicator").show();
                    }else {
                        if(btnFocus==0) saveMenuList();
                        else if (btnFocus==1) LayerManager.historyBack();
                    }return true;
                case KEY_CODE.EXIT:
                    if(callback) callback(false);
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    // div.find(".context_menu_indicator").hide();
                    if(focus == 0 && curSelectMenu.length > 1 && !div.find(".content_area .item.position_" + menuFocus).hasClass("unChecked"))
                        setEditMode(true);
                    return true;
            }
        }

        function onKeyForEditState(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if(focus==1) setFocus(0, menuFocus);
                    return true;
                case KEY_CODE.DOWN:
                    if(focus==0) setFocus(1, 1);
                    return true;
                case KEY_CODE.RIGHT:
                    if(focus == 0) moveElement(1);
                    return true;
                case KEY_CODE.LEFT:
                    if(focus == 0) moveElement(-1);
                    return true;
                case KEY_CODE.ENTER:
                    if(focus==0) {
                        div.find(".context_menu_indicator").show();
                        swapObject(pickIndex, menuFocus);
                        setEditMode(false);
                    }else if(focus==1) {
                        LayerManager.historyBack();
                    } return true;
                case KEY_CODE.EXIT:
                    if(callback) callback(false);
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        }

        this.controlKey = function(keyCode) {
            if(editMode) return onKeyForEditState(keyCode);
            else return onKeyForNormalState(keyCode);
        };
    };

    myHome.popup.favorMenuCompletePopup.prototype = new Layer();
    myHome.popup.favorMenuCompletePopup.constructor = myHome.layer.favorMenuCompletePopup;

    myHome.popup.favorMenuCompletePopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.favorMenuCompletePopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeFavorMenuCompletePopup"] = myHome.popup.favorMenuCompletePopup;
})();