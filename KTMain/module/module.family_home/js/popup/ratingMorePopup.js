/**
 * 별점 평가 미완료 팝업(별점 평가 컨텐츠 수가 총 20개 이하일 때 안내 팝업)
 * 우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기 > 나의 취향 알아보기 > 나의 취향 알아보기(버튼) > 완료
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_rating_more_popup = function RatingMorePopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus = 0;
        var params;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup rating_more_popup"});
            params = this.getParams();

            div.append(_$("<title/>").text("평가 미완료"));
            div.append(_$("<span/>", {class:"text1"}).html("콘텐츠 평가를 계속 하시겠습니까?"));
            div.append(_$("<span/>", {class:"text2"}).html("현재 " + params.count + "개 콘텐츠를 평가했습니다<br>최소 20개 영화를 평가해야 정확한 분석 결과와 추천 콘텐츠를 제공받을 수 있습니다"));
            div.append(_$("<div/>", {class:"btn_area"}));

            div.find(".btn_area").append(_$("<btn/>").text("평가 계속하기"));
            div.find(".btn_area").append(_$("<btn/>").text("다음에 하기"));
            setFocus(focus);
            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        function setFocus(_focus) {
            focus = _focus;
            div.find(".btn_area btn").removeClass("focus").eq(focus).addClass("focus");
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    setFocus(focus^1);
                    return true;
                case KEY_CODE.ENTER:
                    if(focus==0) {
                        LayerManager.deactivateLayer({id:this.id});
                        LayerManager.activateLayer({
                            obj: {
                                id: "MyHomeRatingPopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                params: { callback: params.callback }
                            },
                            moduleId: "module.family_home",
                            visible: true
                        }); return true;
                    }
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    if(params.callback) params.callback(false);
                    return true;
            }
        };
    };

    myHome.popup.home_rating_more_popup.prototype = new Layer();
    myHome.popup.home_rating_more_popup.constructor = myHome.layer.home_rating_more_popup;

    myHome.popup.home_rating_more_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_rating_more_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeRatingMorePopup"] = myHome.popup.home_rating_more_popup;
})();