/**
 * 플레이 리스트 추가 거부 팝업 - 플레이 리스트 최대 개수(9개) 초과 하여 생성 할 경우
 * (우리집 맞춤 TV > 마이 플레이리스트 > 연관메뉴 > 플레이 리스트 추가)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.playlistAddDenyPopup = function PlayListAddDenyPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup playlist_add_deny_popup"});

            div.append(_$("<title/>").text("알림"));
            div.append(_$("<span/>", {class:"text1"}).html("플레이리스트는 최대 9개 까지 추가 가능합니다"));
            div.append(_$("<span/>", {class:"text2"}).html("새로운 플레이리스트를 추가 하시려면 이전 폴더 삭제 후 추가해 주세요"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("확인").addClass("focus")));

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    myHome.popup.playlistAddDenyPopup.prototype = new Layer();
    myHome.popup.playlistAddDenyPopup.constructor = myHome.popup.playlistAddDenyPopup;

    myHome.popup.playlistAddDenyPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.playlistAddDenyPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["playlistAddDenyPopup"] = myHome.popup.playlistAddDenyPopup;
})();