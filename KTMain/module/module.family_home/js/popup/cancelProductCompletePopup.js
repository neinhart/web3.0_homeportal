/**
 * 월정액 해지 신청 완료 팝업
 * (우리집 맞춤 TV > 마이메뉴 > 가입정보 > 월정액 상품 해지 > 비밀번호 입력 > 확인)
 * Created by Taesang on 2016-12-30.
 */

(function() {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_cancelProductComplete = function cancelProductCompletePopup(options) {
        var div;
        Layer.call(this, options);

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome gniPopup cancelProductComplete");
            div.append(_$("<span/>", {class: "popup_title"}).text("해지 신청"));
            div.append(_$("<span/>", {class: "text_white_center_45M"}).text("가입 해지 신청이 접수되었습니다"));
            div.append(_$("<span/>", {class: "text_white_center_30M"}).text("해지 상품 요금 문의 : 국번 없이 100번"));
            div.append(_$("<div/>", {class: "whiteBox"}));
            div.find(".whiteBox").append(_$("<span/>", {class: "btnText"}).text("확인"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    myHome.popup.home_cancelProductComplete.prototype = new Layer();
    myHome.popup.home_cancelProductComplete.prototype.constructor = myHome.popup.home_cancelProductComplete;

    myHome.popup.home_cancelProductComplete.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_cancelProductComplete.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["CancelProductComplete"] = myHome.popup.home_cancelProductComplete;
})();