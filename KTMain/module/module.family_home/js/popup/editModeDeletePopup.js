/**
 * 선택항목 삭제 팝업
 * (찜한 목록 삭제, 최근 시청 목록 삭제, 구매 목록 삭제)
 * Created by Taesang on 2016-12-30.
 */

(function() {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_editModeDelete = function editModeDeleteLayer(options) {
        var div, data, btnText = ["확인", "취소"], focusIdx = 0;
        Layer.call(this, options);

        function setFocus() {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome gniPopup editModeDelete");
            div.append(_$("<span/>", {class: "popup_title"}).text("선택항목 삭제"));
            div.append(_$("<span/>", {class: "text_white_center_45M"}));
            div.append(_$("<span/>", {class: "text_white_center_30L"}));
            div.append(_$("<span/>", {class: "text_white_center_30M"}));
            div.append(_$("<span/>", {class: "text_center_28L"}).text("(목록에서 삭제해도 서비스 해지 전까지 시청 가능합니다)"));
            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            div.removeClass("isDvd");
            data = this.getParams();
            focusIdx = 0;
            setFocus();
            log.printDbg(data);
            if (data.type == 0) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].itemName + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].itemName + "'");
                div.find(".text_white_center_30L").text("찜한 목록에서 삭제합니다");
            }
            else if (data.type == 1) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].title + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].title + "'");
                div.find(".text_white_center_30L").text("최근 시청 목록에서 삭제합니다");
            }
            else if (data.type == 2) {
                var title = data.items[0].contsName;
                if (data.items[0].adultOnlyYn == "Y") title = data.items[0].contsName.substr(0, 1) + "******************************************************".substr(0, data.items[0].contsName.length);
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + title + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + title + "'");
                div.find(".text_white_center_30L").text("구매 목록에서 삭제합니다");
                for (var i = 0; i < data.items.length; i++) {
                    if (data.items[i].isDvdYn == "Y") {
                        div.addClass("isDvd");
                        div.find(".text_white_center_30L").html("소장용 VOD는 구매 목록과<br>마이 소장용VOD 목록에서 삭제됩니다");
                        break;
                    }
                }
            }
            else if (data.type == 3) {
                if (data.items.length > 1) div.find(".text_white_center_45M").text("'" + data.items[0].title + "' 외 " + (data.items.length - 1) + "개");
                else div.find(".text_white_center_45M").text("'" + data.items[0].title + "'");
                div.find(".text_white_center_30L").text("'" + data.listName + "'에서 삭제합니다");
            }
            else if (data.type = 4) {
                div.find(".popup_title").text("찜해제");
                div.find(".text_white_center_45M").html("찜한 콘텐츠로 이미 등록되어 있습니다");
                div.find(".text_white_center_30L").html("해당 콘텐츠 찜을 해제 하시겠습니까?");

                //div.find(".popup_title").addClass("multi_line");
                //div.find(".text_white_center_30M").addClass("multi_line");
                //div.find(".twoBtnArea_280").addClass("multi_line");
                //div.find(".popup_title").text("찜해제");
                //div.find(".text_white_center_30M").html("찜한 콘텐츠로 이미 등록되어 있습니다<br>해당 콘텐츠 찜을 해제 하시겠습니까?");
            }
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    focusIdx = HTool.indexMinus(focusIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    focusIdx = HTool.indexPlus(focusIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    data.callback(focusIdx == 0);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    data.callback(false);
                    return true;
            }
        };
    };

    myHome.popup.home_editModeDelete.prototype = new Layer();
    myHome.popup.home_editModeDelete.prototype.constructor = myHome.popup.home_editModeDelete;

    myHome.popup.home_editModeDelete.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_editModeDelete.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["EditModeDeletePopup"] = myHome.popup.home_editModeDelete;
})();