/**
 * 결제수단 확인 팝업
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.paymentMethodPopup = function PaymentMethodPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup payment_method_popup"});

            div.append(_$("<title/>").text("결제수단 확인"));
            div.append(_$("<div/>", {class:"content_area"}).html("<div class='contents_box'><div class='contentsName'/><div class='price'/><div class='method_area'/></div>"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("확인")));

            this.div.append(div);

            setData(this.getParams(), cbCreate);
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        function setData(data, callback) {
            var title = data.contsName;
            if (data.adultOnlyYn == "Y") title = data.contsName.substr(0, 1) + "******************************************************".substr(0, data.contsName.length);
            div.find(".contentsName").text("'" + title + "'");
            div.find(".price").text(HTool.addComma(data.buyingPrice));
            myHome.amocManager.getBuyDetInfo(data.buyId, data.buyingDate, data.expireDate, function(res, data) {
                if(res && data && data.cpUseCdList.length>0) {
                    var area = div.find(".method_area");
                    var cpUseCdList = data.cpUseCdList.split("|");
                    var cpUsePrList = data.cpUsePrList.split("|");

                    // 정렬을 위해 데이터를 가공한다
                    // CdList 와 PrList 를 묶어서 관리
                    var cpList = [];
                    for (var i = 0; i < cpUseCdList.length; i++) {
                        cpList[i] = {
                            cpUseCd : cpUseCdList[i],
                            cpUsePr : cpUsePrList[i]
                        }
                    };

                    if (cpList.length >= 2) {
                        // 2개 이상의 결제 수단으로 결제한 경우 cpUseCdList 정렬 해야함.
                        // 이러한 경우는 혼합결제 뿐이다.
                        // 일반결제 N > 할인권 34 > 멤버십 C > kt제공TV쿠폰 30 > TV포인트 3 > TV쿠폰 2 or 31
                        // 이용권은 단독으로 사용되기 때문에 고려하지 않음
                        var order = {
                            'N': 1,
                            '34': 2,
                            'C': 3,
                            "30": 4,
                            "3": 5,
                            "2": 6,
                            "31": 7
                        };

                        cpList.sort(function(a, b) {
                            return order[a.cpUseCd] < order[b.cpUseCd] ? -1 : 1;
                        })
                    }

                   for (var i = 0; i < cpList.length; i++) {
                       var html = "<div class='method'><div class='method_code'>" + getUseCodeText(cpList[i].cpUseCd);

                       // 멤버십, TV 포인트만 P로 표시하고 나머지는 원으로 표시
                       if (cpList[i].cpUseCd == "C" || cpList[i].cpUseCd == "3") {
                           html += "</div><div class='method_membership'>" + HTool.addComma(cpList[i].cpUsePr) + "</div>";
                       } else {
                           html += "</div><div class='method_price'>" + HTool.addComma(cpList[i].cpUsePr) + "</div>";
                       }

                       area.append(html);
                    }
                    div.css("margin-top", 300 - (33 * cpList.length) + "px");
                }else {
                    HTool.openErrorPopup({message: ["결제수단 확인 중 오류가 발생했습니다", "잠시 후 다시 시도해 주세요"]});
                } if (callback) callback(res);
            });

        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };

        function getUseCodeText(code) {
            switch (code) {
                case "N": return "일반 결제";
                case "1": return "선불권";
                case "2": return "TV쿠폰";
                case "3": return "TV포인트";
                case "4": return "휴대폰 결제";
                case "9": return "콘텐츠 이용권";
                case "A": return "신한카드 포인트";
                case "B": return "BC카드 포인트";
                // case "C": return "올레클럽별";
                case "C": return "KT 멤버십";
                case "D": return "KTF 포인트";
                case "15": return "신용카드";
                case "IC": return "IC 카드";
                case "TC": return "TV 앱카드";
                case "TE": return "TV 간편결제";
                case "KP": return "카카오페이";
                case "IT": return "IC 즉시이체";
                case "H": return "혼합결제";
                case "14": return "TV포인트";
                case "30": return "TV쿠폰 (KT 제공)";
                case "31": return "TV쿠폰";
                case "32": return "콘텐츠 이용권";
                case "33": return "콘텐츠 이용권";
                case "34": return "콘텐츠할인권";
                default: return "미정의 결제수단";
            }
        }

    };

    myHome.popup.paymentMethodPopup.prototype = new Layer();
    myHome.popup.paymentMethodPopup.constructor = myHome.popup.paymentMethodPopup;

    myHome.popup.paymentMethodPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.paymentMethodPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PaymentMethodPopup"] = myHome.popup.paymentMethodPopup;
})();