/**
 * 나의 취향 알아보기 팝업
 * (우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기 > 나의 취향 알아보기 > 나의 취향 알아보기(버튼))
 * Created by ksk91_000 on 2016-08-11.
 */

(function() {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_rating_popup = function Constructor(options) {
        Layer.call(this, options);

        var div, ul, myRatingCnt = 0, contsIdx = 0, currentRow = 0, itemList, showUl, curPage = 0;
        var rateObjList;
        var rateString = ["안봤어요", "최악이에요", "괜히 봤어요", "재미없어요", "별로에요", "아쉬워요", "보통이에요", "괜찮아요", "재미있어요", "정말좋아요", "최고에요"];
        var adultYn;
        var noLeft = true;
        var callback;
        var instance = this;

        this.init = function(cbCreate) {
            rateObjList = new rateObjectList();
            div = this.div;
            div.addClass("arrange_frame myhome rating_popup");
            div.append(_$("<span/>", {class: "title_area"}).text("나의 취향 알아보기"));
            div.append(_$("<span/>", {class: "description_area"}));
            div.append(_$("<div/>", {class: "contents_area"}));
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div/>", {class: "button"}));
            div.find(".button_area").append(_$("<div/>", {class: "whiteBox"}));
            div.find(".button_area").append(_$("<span/>", {class: "buttonText"}).text("완료"));
            div.append(_$("<span/>", {class: "title_area"}).text("나의 취향 알아보기"));
            // 팝업 배경
            div.find(".contents_area").append(_$("<div/>", {class: "background"}));
            //div.find(".contents_area").append(_$("<div/>", {class: "background l"}));
            //div.find(".contents_area").append(_$("<div/>", {class: "background m"}));
            //div.find(".contents_area").append(_$("<div/>", {class: "background r"}));
            // div.find(".contents_area").append(_$("<div/>", {class: "background"}));
            // 평가 수치
            div.find(".contents_area").append(_$("<div/>", {class: "status"}));
            div.find(".contents_area .status").append(_$("<div/>", {class: "icon start"}));
            div.find(".contents_area .status .icon.start").append(_$("<span/>", {class: "count start"}).text("0"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "icon bronze"}));
            div.find(".contents_area .status .icon.bronze").append(_$("<span/>", {class: "count bronze"}).text("50"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "icon silver"}));
            div.find(".contents_area .status .icon.silver").append(_$("<span/>", {class: "count silver"}).text("100"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "icon gold"}));
            div.find(".contents_area .status .icon.gold").append(_$("<span/>", {class: "count gold"}).text("200"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "line"}).css("left", "67px"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "line"}).css("left", "308px"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "line"}).css("left", "549px"));
            div.find(".contents_area .status").append(_$("<div/>", {class: "filled_line"}));
            div.find(".contents_area .status").append(_$("<div/>", {class: "filled_icon"}));
            // VOD 리스트
            div.find(".contents_area").append(_$("<div/>", {class: "contents"}));
            div.find(".contents_area .contents").append(_$("<ul/>", {class: "contents_list left"}));
            div.find(".contents_area .contents").append(_$("<ul/>", {class: "contents_list right"}));
            div.find(".contents_area").append(_$("<div/>", {class: "arrow_left", style: "display:none"}));
            div.find(".contents_area").append(_$("<div/>", {class: "arrow_right"}));

            div.append(_$("<div/>", {class: 'context_menu_indicator'}).text("찜하기"));

            adultYn = MyPreferenceManager.isNoAdult();
            callback = this.getParams().callback;

            cbCreate(true);
        };

        this.show = function(options) {
            if(!options || !options.resume) {
                div.css("visibility", "hidden");
                Layer.prototype.show.call(this);
                getRatingContent();
                contsIdx = currentRow = curPage = 0;
            } else {
                Layer.prototype.show.call(this);
            }
        };

        // this.hide = function() {
        //     Layer.prototype.show.call(this);
        // };

        var keyBlock = false;
        this.controlKey = function(keyCode) {
            if (keyBlock) return true;
            switch (keyCode) {
                case KEY_CODE.UP:
                    if (currentRow == 0) return;
                    else currentRow = 0;
                    setFocusRow();
                    return true;
                case KEY_CODE.DOWN:
                    if (currentRow == 2) return;
                    else currentRow++;
                    setFocusRow();
                    return true;
                case KEY_CODE.LEFT:
                    if (currentRow == 0) {
                        if (contsIdx > 0) {
                            if (curPage * 5 > contsIdx - 1) {
                                if (!noLeft) movePrevPage();
                                else return true;
                            }
                            //showUl.find("li").removeClass("focus");
                            contsIdx--;
                            setFocusIdx();
                        }
                    } else if (currentRow == 1) {
                        itemList[contsIdx].rate = Math.max(0, itemList[contsIdx].rate ? itemList[contsIdx].rate - 0.5 : 0);
                        setStarRate(contsIdx);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (currentRow == 0) {
                        if (contsIdx < itemList.length - 1) {
                            //showUl.find("li").removeClass("focus");
                            contsIdx++;
                            if ((curPage + 1) * 5 == contsIdx) {
                                moveNextPage(setFocusIdx);
                            } else {
                                setFocusIdx();
                            }
                        }
                    } else if (currentRow == 1) {
                        itemList[contsIdx].rate = Math.min(5, itemList[contsIdx].rate != null ? itemList[contsIdx].rate + 0.5 : 3);
                        setStarRate(contsIdx);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (currentRow == 2) {
                        if(rateObjList.getList().length == 0) { LayerManager.historyBack(); return true; }
                        keyBlock = true;
                        // if( (myRatingCnt < 20) && (myRatingCnt + rateObjList.getList().length) >= 20) {
                        //     LayerManager.activateLayer({
                        //         obj: {
                        //             id: "SingleTextPopup",
                        //             type: Layer.TYPE.POPUP,
                        //             priority: Layer.PRIORITY.POPUP,
                        //             linkage: false,
                        //             params: {
                        //                 title: "알림",
                        //                 content: "평가를 바탕으로 맞춤 메뉴를 구성 중입니다"
                        //             }
                        //         },
                        //         moduleId: "module.family_home",
                        //         visible: true
                        //     });
                        //     LayerManager.startLoading();
                        //     setTimeout(function () {
                        //         makeRatingInfo();
                        //     }, 1500);
                        // } else {
                            makeRatingInfo();
                        // }
                    } else if (currentRow == 1) {
                        currentRow = 0;
                        setFocusRow();
                    } else if (currentRow == 0) {
                        currentRow = 1;
                        setFocusRow();
                    }
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    // if(rateObjList.getList().length > 0) myHome.curationManager.makePreference(adultYn, false, rateObjList.getList(), function() { MyPreferenceManager.setUpdateFlag(); });
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    if (currentRow == 0 || currentRow == 1) {
                        var item = itemList[contsIdx];
                        WishListManager.addContents((parseInt(item.ITEM_TYPE) == 4 ? "2" : item.ITEM_TYPE), parseInt(item.ITEM_TYPE) == 1 ? item.CAT_ID : item.ITEM_ID, item.CAT_ID, (item.CMB_YN == "Y"), item.IS_HD, null, true);
                    }
                    return true;
            }
        };

        function makeRatingInfo() {
            myHome.curationManager.makePreference(adultYn, true, rateObjList.getList(), function(res) {
                if(res) {
                    // LayerManager.deactivateLayer({id: "SingleTextPopup"});
                    // LayerManager.stopLoading();
                    keyBlock = false;

                    // try {
                    //     var viewId = LayerManager.getLastLayerInStack({
                    //         key: "type",
                    //         value: Layer.TYPE.NORMAL
                    //     }).getViewMgr().getCurrentMenuData().getViewId();
                    //     if (viewId == "myRatingContent") {
                            if (myRatingCnt + rateObjList.getList().length >= 20) {
                                MyPreferenceManager.setUpdateFlag();
                                // MyPreferenceManager.update(null, true);
                                // callback(true);
                            }
                    //     }
                    // } catch (e) {
                    //     log.printErr(e);
                    // }

                    LayerManager.historyBack();

                    LayerManager.activateLayer({
                        obj: {
                            id: ((myRatingCnt + rateObjList.getList().length) < 20) ? "MyHomeRatingMorePopup" : "MyHomeRatingCompletePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: {callback: callback, count: myRatingCnt + rateObjList.getList().length}
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                } else {
                    // LayerManager.deactivateLayer({id: "SingleTextPopup"});
                    // LayerManager.stopLoading();
                    HTool.openErrorPopup({message: ["오류가 발생했습니다", "잠시 후 다시 시도 해 주세요"]});
                }
            });
        };

        function openNoRatingContentPopup() {
            LayerManager.activateLayer({
                obj: {
                    id: "SingleTextPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        title: "알림",
                        content: "평가할 콘텐츠 목록을 생성하지 못했습니다<br>잠시 후 다시 시도해 주세요",//responseData.RESULT_MSG,
                        btnText: "확인"
                    }
                },
                moduleId: "module.family_home",
                visible: true
            });
        };

        function getRatingContent() {
            LayerManager.startLoading();
            myHome.curationManager.getRatingContent(adultYn, function(result, responseData) {
                if(result && responseData.RESULT_CODE == 200) {
                    myRatingCnt = responseData.MY_ITEM_CNT;
                    itemList = responseData.ITEM_LIST;
                    makeContentsList(itemList);
                    setFocusIdx(0);

                    setRatingCount(myRatingCnt);

                    div.css("visibility", "inherit");

                    LayerManager.stopLoading();
                    // Layer.prototype.show.call(instance);

                } else {
                    div.css("visibility", "inherit");
                    LayerManager.stopLoading();
                    LayerManager.deactivateLayer({id: "MyHomeRatingPopup"});
                    openNoRatingContentPopup();
                }
            });
        }

        function setRatingCount(count) {
            div.find(".description_area").text("총 " + count + "편 평가! 평가를 많이 할수록 선호도 분석과 추천이 정확해 집니다");
            div.find(".contents_area .status .filled_line").empty();
            if (count > 200) {
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({left: "0px"}));
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({left: "241px"}));
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({left: "482px"}));
                div.find(".contents_area .status .filled_icon").hide();
            } else if (count > 100) {
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({left: "0px"}));
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({left: "241px"}));
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({
                    left: "482px",
                    width: (175 * (count - 100) / 100) + "px"
                }));
                div.find(".contents_area .status .filled_icon").text(count).css("-webkit-transform", "translateX(" + (482 + (175 * (count - 100) / 100)) + "px)").toggle(count != 200);
            } else if (count > 50) {
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({left: "0px"}));
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({
                    left: "241px",
                    width: (175 * (count - 50) / 50) + "px"
                }));
                div.find(".contents_area .status .filled_icon").text(count).css("-webkit-transform", "translateX(" + (241 + (175 * (count - 50) / 50)) + "px)").toggle(count != 100);
            } else {
                div.find(".contents_area .status .filled_line").append(_$("<div/>", {class: "fill"}).css({
                    left: "0px",
                    width: (175 * count / 50) + "px"
                }));
                div.find(".contents_area .status .filled_icon").text(count).css("-webkit-transform", "translateX(" + (175 * count / 50) + "px)").toggle(count != 50);
            }
        }

        function getVerticalPoster(id, data) {
            var element = _$("<li id='" + id + "' class='content poster_vertical'>"
                + "<div class='content'>"
                + "<div class='focus_red'><div class='black_bg'></div></div>"
                + "<img src='" + data.IMG_URL + "?w=210&h=300&quality=90' class='posterImg' onerror='this.src=\"" + modulePath + "resource/image/default_poster.png\"'>"
                + "</div>"
                + "<div class='rating_area whiteStarBox'>"
                + "<div class='box_area'>"
                + "<div class='rating_box'></div>"
                + "<div class='whiteBox_area'>"
                + "<div class='focus_red'><div class='black_bg'></div></div>"
                + "<div class='whiteArrow l'></div>"
                + "<div class='whiteArrow r'></div>"
                + "</div>"
                + "</div>"
                + "<div class='star_area'>"
                /*
                + "<div class='star'></div>"
                + "<div class='star'></div>"
                + "<div class='star'></div>"
                + "<div class='star'></div>"
                + "<div class='star'></div>"
                */
                + "</div>"
                + "<span class='posterTitle'>" + data.ITEM_NAME + "</span>"
                + "</div>"
                + "</li>");

            element.find(".star_area").prepend(stars.getView(stars.TYPE.YELLOW_26));

            element.find(".rating_star_area").addClass("popup");

            stars.setRate(element.find(".star_area"), 0);
            return element;
        }

        function getVerticalEmptyPoster() {
            var element = _$("<li id='" + id + "' class='content poster_vertical'>"
                + "<div class='content'>"
                + "<img src='' class='posterImg'>"
                + "</div>"
                + "</li>");
            return element;
        }

        function makeContentsList(data) {
            ul = new Array(2);
            ul[0] = div.find(".contents_area .contents .contents_list.left");
            ul[1] = div.find(".contents_area .contents .contents_list.right");
            ul[0].empty();
            ul[1].empty();
            var i, j, length = parseInt(data.ITEM_CNT);

            if (length <= 5) {
                for (i = 0; i < length; i++) ul[0].append(getVerticalPoster(data[i].ITEM_ID, data[i]));
                for (i = 0; i < 5 - length; i++) ul[0].append(getVerticalEmptyPoster());
            } else if (length <= 10) {
                for (i = 0; i < 5; i++) ul[0].append(getVerticalPoster(data[i].ITEM_ID, data[i]));
                for (j = 5; j < length; j++) ul[1].append(getVerticalPoster(data[j].ITEM_ID, data[j]));
                for (j = 0; j < 10 - length; j++) ul[1].append(getVerticalEmptyPoster());
            } else {
                for (i = 0; i < 5; i++) ul[0].append(getVerticalPoster(data[i].ITEM_ID, data[i]));
                for (j = 5; j < 10; j++) ul[1].append(getVerticalPoster(data[j].ITEM_ID, data[j]));
            }
            showUl = ul[0];
        }

        function setListData(ulNum, page) {
            for (var i = 0; i < 5; i++) {
                if (itemList[(page * 5) + i]) {
                    ul[ulNum].find("li")[i].id = itemList[(page * 5) + i].ITEM_ID;
                    ul[ulNum].find(".posterImg")[i].src = "";
                    void ul[ulNum].find(".posterImg")[i].offsetWidth;
                    ul[ulNum].find(".posterImg")[i].src = itemList[(page * 5) + i].IMG_URL;
                    ul[ulNum].find(".posterTitle").eq(i).text(itemList[(page * 5) + i].ITEM_NAME);
                    if (ul[ulNum].find("li")[i].style.display == "none") ul[ulNum].find("li").eq(i).show();
                    setStarRate((page * 5) + i, true, ulNum);
                } else {
                    ul[ulNum].find("li")[i].id = "";
                    ul[ulNum].find(".posterImg")[i].src = "";
                    ul[ulNum].find(".posterTitle").eq(i).text("");
                    ul[ulNum].find("li").eq(i).hide();
                    stars.setRate(ul[ulNum].find("li").eq(i).find(".star_area"), 0);
                    // ul[ulNum].find("li").eq(i).find(".star_area .star").removeClass("half filled");
                }
            }
        }

        function makeNextPage() {
            if (curPage >= 2) {
                for (var i = 0; i < 5; i++) delete itemList[(curPage - 2) * 5 + i]
            }

            div.find(".contents_area .arrow_left").show();
            if (ul[0].hasClass("moveLeft")) {
                div.find(".contents_area .contents_list").css("transition", "-webkit-transform 0.0s");
                ul[0].toggleClass("moveLeft");
                ul[1].toggleClass("moveLeft");
                setListData(0, curPage);
                curPage++;
                if (curPage > Math.floor((itemList.length - 1) / 5)) curPage = 0;
                setListData(1, curPage);
                void div.find(".contents_area .contents_list")[0].offsetWidth;
                div.find(".contents_area .contents_list").css("transition", "-webkit-transform 0.5s");
                ul[0].toggleClass("moveLeft");
                ul[1].toggleClass("moveLeft");
            } else {
                div.find(".contents_area .contents_list").css("transition", "-webkit-transform 0.5s");
                ul[0].toggleClass("moveLeft");
                ul[1].toggleClass("moveLeft");
                curPage++;
                if (curPage > Math.floor((itemList.length - 1) / 5)) curPage = 0;
            }
            showUl = ul[1];
        };

        function moveNextPage(callback) {
            noLeft = false;

            console.log("GYUH.moveNextPage(): " + ((curPage + 2) * 5) +":" + itemList.length );
            if ((curPage + 2) * 5 >= itemList.length) {
                myHome.curationManager.getRatingContent(adultYn, function(result, responseData) {
                    if(responseData.RESULT_CODE == 200) {
                        itemList = itemList.concat(responseData.ITEM_LIST);
                        makeNextPage();
                    } else {
                        openNoRatingContentPopup();
                    } if(callback) callback();
                });
            } else {
                makeNextPage();
                if(callback) callback();
            }
        }

        function movePrevPage() {
            noLeft = true;
            div.find(".contents_area .arrow_left").hide();
            if (ul[0].hasClass("moveLeft")) {
                ul[0].toggleClass("moveLeft");
                ul[1].toggleClass("moveLeft");
                curPage--;
                if (curPage < 0) curPage = Math.floor((itemList.length - 1) / 5);
            } else {
                div.find(".contents_area .contents_list").css("transition", "-webkit-transform 0.0s");
                ul[0].toggleClass("moveLeft");
                ul[1].toggleClass("moveLeft");
                setListData(1, curPage);
                curPage--;
                if (curPage < 0) curPage = Math.floor((itemList.length - 1) / 5);
                setListData(0, curPage);
                void div.find(".contents_area .contents_list")[0].offsetWidth;
                div.find(".contents_area .contents_list").css("transition", "-webkit-transform 0.5s");
                ul[0].toggleClass("moveLeft");
                ul[1].toggleClass("moveLeft");
            }
            showUl = ul[0];
        }

        function setFocusIdx(idx) {
            showUl.find("li.focus .content.focus").removeClass("focus");
            showUl.find("li.focus .rating_area.focus").removeClass("focus");
            showUl.find("li").removeClass("focus");
            showUl.find("li").eq(idx || contsIdx % 5).addClass("focus");
            showUl.find("li").eq(idx || contsIdx % 5).find(".content").addClass("focus");
        }

        function setStarRate(contsIdx, noTitle, ulNum) {
            var ui = ulNum == null ? showUl : ul[ulNum];
            if (itemList[contsIdx].rate) {
                // ui.find("li").eq(contsIdx % 5).find(".star_area .star").removeClass("half filled").eq(Math.ceil(itemList[contsIdx].rate) - 1).addClass("half");
                // ui.find("li").eq(contsIdx % 5).find(".star_area .star").slice(0, Math.floor(itemList[contsIdx].rate)).removeClass("half").addClass("filled");

                stars.setRate(ui.find("li").eq(contsIdx % 5).find(".star_area"), itemList[contsIdx].rate);
                if (!noTitle) ui.find("li").eq(contsIdx % 5).find(".posterTitle").text(rateString[itemList[contsIdx].rate * 2]);
            } else {
                // ui.find("li").eq(contsIdx % 5).find(".star_area .star").removeClass("half filled");
                stars.setRate(ui.find("li").eq(contsIdx % 5).find(".star_area"), 0);
                if (!noTitle) ui.find("li").eq(contsIdx % 5).find(".posterTitle").text(rateString[0]);
            }
        }

        function setFocusRow() {
            if (currentRow == 0) {
                rateObjList.setRate(itemList[contsIdx].ITEM_ID, itemList[contsIdx].rate);
                showUl.find("li").eq(contsIdx % 5).addClass("focus");
                showUl.find("li").eq(contsIdx % 5).find(".content").addClass("focus");
                showUl.find("li").eq(contsIdx % 5).find(".rating_area").removeClass("focus");
                showUl.find("li").eq(contsIdx % 5).find(".posterTitle").text(itemList[contsIdx].ITEM_NAME);
                div.find(".button_area.focus").removeClass("focus");
                div.find(".context_menu_indicator").show();
            } else if (currentRow == 1) {
                showUl.find("li").eq(contsIdx % 5).find(".content").removeClass("focus");
                showUl.find("li").eq(contsIdx % 5).find(".rating_area").addClass("focus");
                if (itemList[contsIdx].rate == null) {
                    itemList[contsIdx].rate = 3;
                    setStarRate(contsIdx);
                }
                showUl.find("li").eq(contsIdx % 5).find(".posterTitle").text(rateString[itemList[contsIdx].rate * 2]);
                div.find(".button_area.focus").removeClass("focus");
                div.find(".context_menu_indicator").show();
            } else if (currentRow == 2) {
                rateObjList.setRate(itemList[contsIdx].ITEM_ID, itemList[contsIdx].rate);
                showUl.find("li").eq(contsIdx % 5).removeClass("focus");
                showUl.find("li").eq(contsIdx % 5).find(".content").removeClass("focus");
                showUl.find("li").eq(contsIdx % 5).find(".rating_area").removeClass("focus");
                div.find(".button_area").addClass("focus");
                div.find(".context_menu_indicator").hide();
            }
        }

        var rateObject = function(_id) {
            this.CONS_ID = _id;
            this.RATING;
            this.setRate = function(_rate) {
                this.RATING = _rate;
                return this;
            };
        };

        var rateObjectList = function() {
            var list = [];
            this.setRate = function(id, rate) {
                var idx = this.getIndex(id);
                if(rate==0) {
                    if (idx >= 0) list.splice(idx, 1);
                }else {
                    if (idx >= 0) list[idx].setRate(rate);
                    else if (rate) list.push(new rateObject(id).setRate(rate));
                } setRatingCount(myRatingCnt + list.length);
            };
            this.getIndex = function(id) {
                for (var i in list) {
                    if (list[i].CONS_ID == id) return i;
                }
                return -1;
            };
            this.getList = function() {
                return list;
            }
        };
    };

    myHome.popup.home_rating_popup.prototype = new Layer();
    myHome.popup.home_rating_popup.prototype.constructor = myHome.popup.home_rating_popup;

    myHome.popup.home_rating_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_rating_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeRatingPopup"] = myHome.popup.home_rating_popup;
})();