/**
 * 즐겨 찾는 메뉴 편집 팝업
 * (우리집 맞춤 TV > 즐겨 찾는 메뉴 > 편집)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.favorMenuEditPopup = function FavorMenuEditPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus = 0;
        var page = 0;
        var menuData;
        var historyStack = [];
        var curSelectMenu = [];
        var language;
        var callback;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup favor_menu_edit_popup"});

            div.append(_$("<title/>").text("즐겨 찾는 메뉴 편집"));
            div.append(_$("<div/>", {class:"content_area"}));
            div.append(_$("<div/>", {class:"bottom_text_area"}));
            div.append(_$("<div/>", {class:"context_menu_indicator"}).text("저장 및 편집"));

            div.find(".bottom_text_area").html("※ 최대 등록 가능한 메뉴는 6개입니다<br>월정액 상품, CUG 상품에 가입하시면 우선 등록되어 표시됩니다");

            var params = this.getParams();
            callback = params.callback;
            var that = this;
            FavorMenuManager.getFavorMenuData(function (favorMenuData) {
                try {
                    curSelectMenu = favorMenuData ? [].concat(favorMenuData) : [];

                    setCurSelectMenuData(params.monthlyData, params.cugData?params.cugData.cugList:[]);
                    makeContentArea(div.find(".content_area"));

                    language = MenuDataManager.getCurrentMenuLanguage();
                    0 == UTIL.isValidVariable(language) && (language = "kor");

                    setCurrentMenuArea();
                    that.div.append(div);
                    if (cbCreate) cbCreate(true);
                } catch(e) {
                    if (cbCreate) cbCreate(false);
                }
            })

        };

        function setCurSelectMenuData(monthlyData, cugData) {
            if(cugData!=null && cugData.length>0) {
                if(cugData.length==1) curSelectMenu.splice(0, 0, {
                    menuType: 500,
                    menuNameKor: cugData[0].cugName,
                    parentNameKor: "바로가기"
                });
                else curSelectMenu.splice(0, 0, {
                    menuType: 500,
                    menuNameKor: "내가 가입한<br>CUG 모아보기",
                    parentNameKor: ""
                });
            }if(monthlyData.length>0) {
                if(monthlyData.length==1) curSelectMenu.splice(0, 0, {
                    menuType: 500,
                    menuNameKor: monthlyData[0].productName,
                    parentNameKor: "바로가기"
                });
                else curSelectMenu.splice(0, 0, {
                    menuType: 500,
                    menuNameKor: "내가 가입한<br>월정액 모아보기",
                    parentNameKor: ""
                });
            }
        }

        function makeContentArea(div) {
            div.append(_$("<div/>", {class:"left_depth_area"}));
            div.append(_$("<div/>", {class:"right_depth_area"}));
            div.append(_$("<div/>", {class:"cur_menu_area"}));
            div.append(_$("<div/>", {class:"cur_menu_text_area"}).append("<div class='text1'>현재 등록된 메뉴</div><div class='number_area'/>"));

            div.append(_$("<div/>", {class:"left_arrow"}));
            div.append(_$("<div/>", {class:"top_arrow"}));
            div.append(_$("<div/>", {class:"bottom_arrow"}));
            makeCurMenuArea();
            setLeftDepth(filteringCategory(MenuDataManager.getMenuData()), 0, 0);
        }

        function filteringCategory(menuData) {
            var md = [];
            for(var i=0; i<menuData.length; i++) {
                if(isUsableMenu(menuData[i])) {
                    if(menuData[i].id == "MENU00000") md.unshift(menuData[i]);
                    else md.push(menuData[i]);
                }
            } return md;

            function isUsableMenu(menuData) {
                if(menuData.parent==null && menuData.catType=="SubHome") return false;
                else if(menuData.catType == "AppGm") return false;
                else return true;
            }
        }

        function makeCurMenuArea() {
            var d = div.find(".cur_menu_area");
            d.append("<div class=item><div class='title'/><div class='subtitle'/><div class='noItemIcon'/></div>");
            d.append("<div class=item><div class='title'/><div class='subtitle'/><div class='noItemIcon'/></div>");
            d.append("<div class=item><div class='title'/><div class='subtitle'/><div class='noItemIcon'/></div>");
            d.append("<div class=item><div class='title'/><div class='subtitle'/><div class='noItemIcon'/></div>");
            d.append("<div class=item><div class='title'/><div class='subtitle'/><div class='noItemIcon'/></div>");
            d.append("<div class=item><div class='title'/><div class='subtitle'/><div class='noItemIcon'/></div>");
        }

        function setLeftDepth(_data, _page, _focus) {
            menuData = filteringCategory(_data);
            page = _page;
            focus = _focus;

            var list = div.find(".left_depth_area");
            list.empty();

            var i = page*8, j= 0, item;

            for(;i<menuData.length && j<8; i++,j++) {
                if(menuData[i].catType=="Adult") menuData.splice(i, 1);
                if(!menuData[i]) break;

                //var item = _$("<div/>", {class:"item" + (isSelected(menuData[i].id)?" select":"")}).text(HTool.getMenuName(menuData[i]));
                item = _$("<div/>", {class:"item" + (isSelected(menuData[i].id)?" select":"")});
                item.append("<span>" + HTool.getMenuName(menuData[i]) + "</span>");
                item.append("<div class='focus_red'><div class='black_bg'></div></div>");

                if(isSelected(menuData[i].id)) {
                    item.addClass("select");
                }
                item.toggleClass("leaf", !menuData[i].children || menuData[i].children.length<=0 || menuData[i].catType=="Rank" || menuData[i].catType=="RankW3");
                list.append(item);
            }for(;j<8;j++){
                item = _$("<div/>", {class:"item noItem"});
                item.append("<span></span>");
                item.append("<div class='focus_red'><div class='black_bg'></div></div>");
                list.append(item);
            }for(;i<menuData.length; i++) if(menuData[i].catType=="Adult") menuData.splice(i, 1);

            div.find(".left_depth_area .item").eq(focus).addClass("focus");
            div.find(".left_arrow").toggle(!!menuData[0].parent);
            div.find(".top_arrow").toggle(page>0);
            div.find(".bottom_arrow").toggle(page<Math.ceil(menuData.length/8)-1);
            setRightDepth();
        }

        function setRightDepth() {
            var data = menuData[focus+page*8].children?filteringCategory(menuData[focus+page*8].children):null;
            var list = div.find(".right_depth_area");
            if(menuData[focus+page*8].catType=="Rank" || menuData[focus+page*8].catType == "RankW3") data = null;

            list.empty();
            if(!data || data.length<=0) {
                list.append(_$("<div/>", {class:"noItem"}).html(
                    "하위 메뉴가 없습니다<br>즐겨 찾는 메뉴 편집을<br>완료하시려면<br><div class='icon'/> 키를 눌러주세요"
                ));
            }else {
                for (var i = 0; i < 8 && i < data.length; i++) {
                    if(data[i].catType=="Adult") data.splice(i, 1);
                    list.append(_$("<div/>", {class: "item" + ((data[i] && isSelected(data[i].id))?" select":"")}).text(HTool.getMenuName(data[i])));
                }
            }
        }

        function setCurrentMenuArea() {
            var length = Math.min(6, curSelectMenu.length);
            div.find(".cur_menu_text_area .number_area").html("<white>" + length + "</white>/6");
            var d = div.find(".cur_menu_area .item");
            var i=0;
            for(;i<length; i++) {
                d.eq(i).removeClass("noItem");
                d.eq(i).find(".title").html(language=="kor"?curSelectMenu[i].menuNameKor:curSelectMenu[i].menuNameEng||curSelectMenu[i].menuNameKor);
                d.eq(i).find(".subtitle").html(language=="kor"?curSelectMenu[i].parentNameKor:curSelectMenu[i].parentNameEng||curSelectMenu[i].parentNameKor);
            }for(;i<6;i++) {
                d.eq(i).addClass("noItem");
            }
        }

        function changeFocus(amount) {
            if(focus+amount+page*8<0) {
                return setFocus(menuData.length-1);
            } else if(focus+amount+page*8>=menuData.length) {
                return setFocus(0);
            }

            if(focus+amount>7) {
                changePage(Math.ceil(Math.abs(amount)/7));
            } else if(focus+amount<0) {
                changePage(-Math.ceil(Math.abs(amount)/7));
            }

            focus= (focus+amount+8)%8;
            div.find(".left_depth_area .item.focus").removeClass("focus");
            div.find(".left_depth_area .item").eq(focus).addClass("focus");
            setRightDepth();
        }

        function changePage(amount) {
            page += amount;
            var i=0, list = div.find(".left_depth_area .item");
            for(var j=page*8 ;j< menuData.length && i<8; i++, j++) {
                list.eq(i).removeClass("noItem").toggleClass("select", isSelected(menuData[j].id)).find("span").text(HTool.getMenuName(menuData[j]));
                list.eq(i).toggleClass("leaf", !menuData[j].children || menuData[j].children.length<=0 || menuData[j].catType=="Rank" || menuData[j].catType=="RankW3");
            }for(;i<8; i++) {
                list.eq(i).addClass("noItem");
            }
            div.find(".top_arrow").toggle(page>0);
            div.find(".bottom_arrow").toggle(page<Math.ceil(menuData.length/8)-1);
        }

        function setFocus(_focus) {
            if(page!==Math.floor(_focus/8)) setPage(Math.floor(_focus/8));
            focus = _focus%8;

            div.find(".left_depth_area .item.focus").removeClass("focus");
            div.find(".left_depth_area .item").eq(focus).addClass("focus");
            setRightDepth();
        }

        function setPage(_page) {
            page = _page;
            var i=0, list = div.find(".left_depth_area .item");
            for(var j=page*8 ;j< menuData.length && i<8; i++, j++) {
                list.eq(i).removeClass("noItem").toggleClass("select", isSelected(menuData[j].id)).find("span").text(HTool.getMenuName(menuData[j]));
                list.eq(i).toggleClass("leaf", !menuData[j].children || menuData[j].children.length<=0 || menuData[j].catType=="Rank" || menuData[j].catType=="RankW3");
            }for(;i<8; i++) {
                list.eq(i).addClass("noItem");
            }
            div.find(".top_arrow").toggle(page>0);
            div.find(".bottom_arrow").toggle(page<Math.ceil(menuData.length/8)-1);
        }

        function intoDepth() {
            if(menuData[focus+page*8].catType=="Rank" || menuData[focus+page*8].catType == "RankW3") return;
            if(menuData[focus+page*8].children && menuData[focus+page*8].children.length>0) {
                historyStack.push({menuData : menuData, focus : focus , page : page});
                setLeftDepth(_$.extend([], menuData[focus+page*8].children), 0, 0);
            }
        }

        function uptoDepth() {
            if(historyStack.length>0) {
                var d = historyStack.pop();
                setLeftDepth(d.menuData, d.page, d.focus);
            }
        }

        function toggleCurSelectMenu() {
            if(div.find(".left_depth_area .item").eq(focus).hasClass("select")) {
                div.find(".left_depth_area .item").eq(focus).removeClass("select");
                curSelectMenu.splice(getSelectIndex(menuData[focus+page*8].id), 1);
            } else {
                if(curSelectMenu.length>=6) return showToast("개수 초과! 즐겨 찾는 메뉴는 6개까지 등록 가능합니다");

                div.find(".left_depth_area .item").eq(focus).addClass("select");
                curSelectMenu.push({
                    menuNameKor: menuData[focus + page*8].name,
                    parentNameKor: menuData[focus + page*8].parent?menuData[focus + page*8].parent.name:"바로가기",
                    menuNameEng: menuData[focus + page*8].englishItemName,
                    parentNameEng: menuData[focus + page*8].parent?menuData[focus + page*8].parent.englishItemName:"바로가기",
                    menuId: menuData[focus + page*8].id,
                    menuOrder: curSelectMenu.length,
                    menuType: menuData[focus + page*8].itemType
                });
            } setCurrentMenuArea();
        }

        function isSelected(id) {
            return getSelectIndex(id)>=0;
        }

        function getSelectIndex(id) {
            for(var i=0; i<curSelectMenu.length; i++) {
                if(curSelectMenu[i].menuId==id) return i;
            }return -1;
        }

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    changeFocus(-1);
                    return true;
                case KEY_CODE.DOWN:
                    changeFocus(1);
                    return true;
                case KEY_CODE.RIGHT:
                    intoDepth();
                    return true;
                case KEY_CODE.LEFT:
                    uptoDepth();
                    return true;
                case KEY_CODE.ENTER:
                    toggleCurSelectMenu();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    LayerManager.activateLayer({
                        obj: {
                            id: "MyHomeFavorMenuCompletePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: {
                                menuList: curSelectMenu,
                                callback: function (result, data) {
                                    if(result) FavorMenuManager.setFavorMenuData(data);
                                    LayerManager.historyBack();
                                    if(callback) callback(result, data);
                                }
                            }
                        },
                        moduleId:"module.family_home",
                        visible: true
                    }); return true;
            }
        };
    };

    myHome.popup.favorMenuEditPopup.prototype = new Layer();
    myHome.popup.favorMenuEditPopup.constructor = myHome.layer.favorMenuEditPopup;

    myHome.popup.favorMenuEditPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.favorMenuEditPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeFavorMenuEditPopup"] = myHome.popup.favorMenuEditPopup;
})();