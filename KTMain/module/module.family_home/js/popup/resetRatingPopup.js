/**
 * 나의 취향 알아보기 초기화 확인 팝업(별점 평가 초기화)
 * (우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기 > 나의 취향 알아보기 > 맞춤 추천 초기화)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.resetRatingPopup = function ResetRatingPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus;
        var callback;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup reset_rating_popup"});

            div.append(_$("<title/>").text("초기화"));
            div.append(_$("<span/>", {class:"text1"}).html("초기화 하시겠습니까?"));
            div.append(_$("<span/>", {class:"text2"}).html("나만의 맞춤 추천 결과를 초기화하면<br>모든 선호도 입력 정보가 삭제됩니다"));
            div.append(_$("<div/>", {class:"btn_area"}));
            setButtonArea();
            setFocus(0);

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        function setButtonArea() {
            var btnArea = div.find(".btn_area");
            btnArea.append(_$("<div/>", {class:"btn ok"}).text("확인"));
            btnArea.append(_$("<div/>", {class:"btn cancel"}).text("취소"));
        }

        function setFocus(index) {
            div.find(".btn.focus").removeClass("focus");
            focus = index;
            div.find(".btn:eq(" + focus + ")").addClass("focus");
        }

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
            if(this.getParams())
                callback = this.getParams().callback;
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    setFocus(focus^1);
                    return true;
                case KEY_CODE.ENTER:
                    if(callback && focus==0) callback(true);
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    myHome.popup.resetRatingPopup.prototype = new Layer();
    myHome.popup.resetRatingPopup.constructor = myHome.layer.resetRatingPopup;

    myHome.popup.resetRatingPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.resetRatingPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeResetRatingPopup"] = myHome.popup.resetRatingPopup;
})();