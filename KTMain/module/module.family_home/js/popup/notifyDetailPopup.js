/**
 * 공지사항 자세히 보기 팝업
 * (우리집 TV > 알림박스 > 공지사항 > 자세히보기)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.notifyDetailPopup = function NotifyDetailPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var focus = 0;
        var data;
        var maxPage, page = 0;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup notify_detail_popup"});

            div.append(_$("<div/>", {class:"title"}));
            div.append(_$("<div/>", {class:"contents_box"}).append("<div class='background'><div class='bg_left'/><div class='bg_mid'/><div class='bg_right'/></div><div class='textArea'><span/></div><div class='scroll_bar'><div class='bar'/></div><div class='measureArea'/>"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("닫기")));

            setData(this.getParams().data);
            this.div.append(div);
            setFocus(focus);
            if(cbCreate) cbCreate(true);
        };

        function setData(_data) {
            data = _data;
            div.find(".title").text(data.title);
            var desc = HTool.convertTextToHtml(data.desc, "\r\n");
            // var desc = data.desc.replace(/\r\n/g, '<br>');
            div.find(".contents_box .textArea span").html(desc);
            div.find(".contents_box .measureArea").html(desc);
            if(data.link_type!=0 && data.link_type!=99) div.find(".btn_area").prepend(_$("<btn/>").text("바로보기"));
        }

        function setPage(_page) {
            page = _page;
            div.find(".textArea span").css("-webkit-transform", "translateY(" + -414*page + "px)");
            div.find(".scroll_bar .bar").css("-webkit-transform", "translateY(" + 414/maxPage*page + "px)");
        }

        this.show = function () {
            Layer.prototype.show.apply(this, arguments);

            maxPage = Math.ceil(div.find(".contents_box .measureArea")[0].offsetHeight/414);
            div.find(".scroll_bar .bar").css("height", 414/maxPage + "px");
            div.find(".scroll_bar").toggle(maxPage>1);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    setPage(Math.max(0, page-1));
                    return true;
                case KEY_CODE.DOWN:
                    setPage(Math.min(maxPage-1, page+1));
                    return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    if(data.link_type!=0 && data.link_type!=99) setFocus(focus^1);
                    return true;
                case KEY_CODE.ENTER:
                    if(focus==0) {
                        moveProcess();
                    }else LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };

        function moveProcess() {
            switch (data.link_type-0) {
                case 0:
                case 99:
                    break;
                case 11:    //VOD 컨텐트
                    /* 성인콘텐츠일 경우, 성인인증 팝업 띄움 */
                    // data.link_data2 = "MBXH5009SGL150000100";
                    if(data.link_data1 == undefined || data.link_data1.length < 5) {
                        data.link_data1 = "N";
                    }

                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.link_data1, data.link_data2);
                    } catch(e) {}
                    openDetailLayer(data.link_data1, data.link_data2, "01");

                    break;
                case 12:    //시리즈 카테고리
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.link_data1);
                    } catch(e) {}
                    openDetailLayer(data.link_data1, data.link_data1, "01");

                    break;
                case 13:    //일반 카테고리
                    var menuData = MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(),
                        allSearch: false,
                        cbCondition: function (menu) { if(menu.id===data.link_data1) return true; }
                    })[0];
                    if(menuData != undefined) {
                        MenuServiceManager.jumpMenu({menu: menuData});
                    } else {
                        showToast("잘못된 접근입니다");
                    } break;
                case 21:    //바운드 어플
                    AppServiceManager.changeService({
                        nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                        obj :{
                            type: CONSTANT.APP_TYPE.MULTICAST,
                            param: data.link_data1
                        }
                    });
                    break;
                case 22:    //언바운드 어플
                    var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                    AppServiceManager.changeService({
                        nextServiceState: nextState,
                        obj: {
                            type: CONSTANT.APP_TYPE.MULTICAST,
                            param: CONSTANT.APP_ID.MASHUP,
                            ex_param: data.link_data1
                        }
                    });
                    break;
                case 31:    //일반 채널
                    var channelObj = oipfAdapter.navAdapter.getChannelBySID(parseInt(data.link_data1));
                    if(!channelObj) return;
                    if(StateManager.isVODPlayingState()) {
                        getVodModule(function (vodModule) {
                            vodModule.execute({
                                method: "stopVodWithChannelSelection",
                                params:{
                                    channelObj: channelObj
                                }
                            })
                        })
                    } else {
                        AppServiceManager.changeService({
                            nextServiceState: CONSTANT.SERVICE_STATE.TV_VIEWING,
                            obj: {
                                channel: channelObj,
                                tune: "true"
                            },
                            forced: true
                        });
                    }
                    break;
            }
        }

        function getVodModule(callback) {
            var vodModule = ModuleManager.getModule("module.vod");
            if(!vodModule) ModuleManager.loadModule( {
                moduleId: "module.vod",
                cbLoadModule: function (success) {
                    if (success) callback(ModuleManager.getModule("module.vod"));
                }
            }); else {
                callback(vodModule);
            }
        }

        var setFocus = function(_focus) {
            div.find(".btn_area .focus").removeClass("focus");
            if(data.link_type == 0 || data.link_type == 99) {
                focus = 1;
                div.find(".btn_area btn").addClass("focus");
            }else {
                focus = _focus;
                div.find(".btn_area btn").eq(focus).addClass("focus");
            }
        };
    };

    myHome.popup.notifyDetailPopup.prototype = new Layer();
    myHome.popup.notifyDetailPopup.constructor = myHome.layer.notifyDetailPopup;

    myHome.popup.notifyDetailPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.notifyDetailPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeNotifyDetailPopup"] = myHome.popup.notifyDetailPopup;
})();