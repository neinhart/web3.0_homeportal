/**
 * 마이 소장용VOD 구매 내역이 없을 때 표시되는 팝업?
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.noCloudPopup = function NoCloudPopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup no_cloud_popup"});

            div.append(_$("<title/>").text("마이 소장용VOD"));
            div.append(_$("<span/>", {class:"text1"}).html("구매한 내역이 없습니다"));
            div.append(_$("<div/>", {class:"btn_area"}).append(_$("<btn/>").text("확인").addClass("focus")));

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    myHome.popup.noCloudPopup.prototype = new Layer();
    myHome.popup.noCloudPopup.constructor = myHome.layer.noCloudPopup;

    myHome.popup.noCloudPopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.noCloudPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeNoCloudPopup"] = myHome.popup.noCloudPopup;
})();