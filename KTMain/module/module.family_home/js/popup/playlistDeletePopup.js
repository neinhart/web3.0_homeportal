/**
 * 마이 플레이리스트 삭제 팝업
 * (우리집 맞춤 TV > 마이 플레이리스트 > 연관메뉴 > 플레이 리스트 삭제)
 * Created by Taesang on 2016-12-30.
 */

(function() {
    myHome.popup = myHome.popup || {};
    myHome.popup.home_playlistDelete = function playlistDeleteLayer(options) {
        var div, data, callback, btnText = ["확인", "취소"], focusIdx = 0;
        Layer.call(this, options);

        function setFocus() {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame myhome gniPopup playlistDelete");
            div.append(_$("<span/>", {class: "popup_title"}).text("마이 플레이리스트 삭제"));
            div.append(_$("<span/>", {class: "text_white_center_45M"}));
            div.append(_$("<span/>", {class: "text_white_center_30L"}).html("플레이리스트를 삭제합니다" + "<br>" + "플레이리스트 삭제 시 모든 콘텐츠도 삭제됩니다"));
            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams().data;
            callback = this.getParams().callback;
            focusIdx = 0;
            setFocus();
            log.printDbg(data);
            div.find(".text_white_center_45M").text("'" + data.title + "'");
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    focusIdx = HTool.indexMinus(focusIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    focusIdx = HTool.indexPlus(focusIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if(focusIdx==0) {
                        myHome.MyPlayListManager.deletePlayItemList(data.info.playListId, function (res, data) {
                            if(res) {
                                callback(res, data);
                                showToast("삭제되었습니다");
                                LayerManager.historyBack();
                            }else {
                                //통신장애 처리
                            }
                        })
                    } else {
                        callback(false);
                        LayerManager.historyBack();
                    } return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    callback(false);
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    myHome.popup.home_playlistDelete.prototype = new Layer();
    myHome.popup.home_playlistDelete.prototype.constructor = myHome.popup.home_playlistDelete;

    myHome.popup.home_playlistDelete.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.home_playlistDelete.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["playlistDeletePopup"] = myHome.popup.home_playlistDelete;
})();