/**
 * 마이 플레이리스트 내 컨텐츠 순서 변경 팝업
 * (우리집 맞춤 TV > 마이 플레이리스트 > 순서변경)
 * Created by skkwon on 2017-01-18.
 */

(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.playlistOrderChangePopup = function PlayListOrderChangePopup(options) {
        Layer.call(this, options);
        var div = _$("<div/>", {class: "popup_contents"});
        var playListId;
        var callback;
        var playList;
        var originPL;

        var focusMode = 0;
        var itemFocus = 0;
        var btnFocus = 0;
        var isEditMode = false;
        var scrollHeight;


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome gniPopup playlist_order_change_popup"});
            var param = this.getParams();
            originPL = param.playList;
            playList = [].concat(originPL);
            playListId = param.playListId;
            callback = param.callback;
            scrollHeight = 787/(Math.ceil(playList.length/10));

            var mainArea = _$("<div/>", {class:"main_area"});
            mainArea.append(_$("<div/>", {class:"bg_area"}).append("<div class='bg_left'/><div class='bg_mid'/><div class='bg_right'/>"));
            mainArea.append(_$("<div/>", {class:"content_area"}));
            mainArea.append(_$("<div/>", {class:"focus_line"}));
            mainArea.append(_$("<div/>", {class:"scroll_bar"}).append(_$("<div/>", {class:"scroll_indicator"})));
            mainArea.append(_$("<div/>", {class:"btn_area"}));
            div.append(mainArea);

            setContentArea();
            setButtonArea();

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        function setContentArea() {
            var area = div.find(".content_area");
            var itemList = _$("<div class='item_list'/>");
            for(var i=0; i<playList.length; i++) {
                var itemArea = _$("<div class='item'><span>" + playList[i].contsName + "</span><div class='info_area'><img src='" + modulePath + "resource/image/icon/icon_age_txtlist_" + UTIL.transPrInfo(playList[i].prInfo) +".png'>" + "</div></div>")
                if(HTool.isTrue(playList[i].wonYn)) itemArea.find(".info_area").prepend("<img class='wonIcon' src='" + modulePath + "resource/image/icon/icon_buy_txtlist.png'>");
                else if(HTool.isFalse(playList[i].wonYn)) itemArea.find(".info_area").prepend("무료");
                else itemArea.find(".info_area").prepend("<img class='wonIcon' src='" + modulePath + "resource/image/icon/icon_buy_txtlist.png'>").addClass("noWonYn");
                itemList.append(itemArea);
            }
            div.find(".scroll_indicator").toggle(playList.length>=10);
            area.empty();
            area.append(itemList);
        }

        function setButtonArea() {
            var area = div.find(".btn_area");
            area.append(_$("<div/>", {class: "text"}).html("순서를 변경할 VOD를<br>선택하세요"));
            area.append(_$("<div/>", {class: "button"}).text("저장"));
            area.append(_$("<div/>", {class: "button"}).text("취소"));
        }

        this.show = function() {
            setItemFocus(0);
            Layer.prototype.show.apply(this, arguments);
        };

        function setItemFocus(focus) {
            itemFocus = focus;
            div.find(".focus_line").css("-webkit-transform", "translateY(" + (itemFocus%10 * 78) + "px)");
            div.find(".item_list").css("-webkit-transform", "translateY(" + -(Math.floor(itemFocus/10) * 779) + "px)");
            div.find(".scroll_indicator").css({"height": scrollHeight + "px", "-webkit-transform": "translateY(" + (Math.floor(itemFocus/10))*scrollHeight + "px)" });
            div.find(".content_area .item.focus").removeClass("focus");
            div.find(".content_area .item").eq(itemFocus).addClass("focus");
        }

        this.onKeyForEditMode = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if(itemFocus==0) {
                        var tmp = playList.splice(0, 1);
                        playList.push(tmp[0]);
                        setContentArea();
                        setItemFocus(playList.length-1);
                    } else {
                        var tmp = playList.splice(itemFocus, 1);
                        playList.splice(itemFocus-1, 0, tmp[0]);
                        setContentArea();
                        setItemFocus(itemFocus-1);
                    } return true;
                case KEY_CODE.DOWN:
                    if(itemFocus==playList.length-1) {
                        var tmp = playList.splice(playList.length-1, 1);
                        playList.splice(0, 0, tmp[0]);
                        setContentArea();
                        setItemFocus(0);
                    } else {
                        var tmp = playList.splice(itemFocus, 1);
                        playList.splice(itemFocus+1, 0, tmp[0]);
                        setContentArea();
                        setItemFocus(itemFocus+1);
                    } return true;
                case KEY_CODE.RIGHT:
                    this.div.removeClass("editMode");
                    div.find(".btn_area .text").html("순서를 변경할 VOD를<br>선택하세요");
                    isEditMode = false;
                    this.controlKey(keyCode);
                    return true;
                case KEY_CODE.ENTER:
                    this.div.removeClass("editMode");
                    div.find(".btn_area .text").html("순서를 변경할 VOD를<br>선택하세요");
                    isEditMode = false;
                    return true;
            }
        };

        this.onKeyForItemArea = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    setItemFocus(HTool.getIndex(itemFocus, -1, playList.length));
                    return true;
                case KEY_CODE.DOWN:
                    setItemFocus(HTool.getIndex(itemFocus, 1, playList.length));
                    return true;
                case KEY_CODE.RIGHT:
                    div.find(".focus_line").hide();
                    div.find(".content_area .item.focus").removeClass("focus");
                    focusMode = 1;
                    div.find(".btn_area .button").eq(btnFocus).addClass("focus");
                    return true;
                case KEY_CODE.ENTER:
                    isEditMode = true;
                    div.find(".btn_area .text").html("원하는 위치로 이동 후<br>확인키를 누르세요");
                    this.div.addClass("editMode");
                    return true;
            }
        };

        this.onKeyForButtonArea = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    div.find(".btn_area .button.focus").removeClass("focus");
                    btnFocus = HTool.getIndex(btnFocus, -1, 2);
                    div.find(".btn_area .button").eq(btnFocus).addClass("focus");
                    return true;
                case KEY_CODE.DOWN:
                    div.find(".btn_area .button.focus").removeClass("focus");
                    btnFocus = HTool.getIndex(btnFocus, 1, 2);
                    div.find(".btn_area .button").eq(btnFocus).addClass("focus");
                    return true;
                case KEY_CODE.LEFT:
                    focusMode = 0;
                    div.find(".btn_area .button.focus").removeClass("focus");
                    div.find(".focus_line").show();
                    setItemFocus(itemFocus);
                    return true;
                case KEY_CODE.ENTER:
                    if(btnFocus==0) sendOrderList();
                    else LayerManager.deactivateLayer({id: this.id});
                    return true;

            }
        };

        this.controlKey = function(keyCode) {
            if(keyCode===KEY_CODE.EXIT) {
                LayerManager.historyBack();
                return true;
            } else if(focusMode==0) {
                if(isEditMode) return this.onKeyForEditMode(keyCode);
                else return this.onKeyForItemArea(keyCode);
            } else if(focusMode==1) return this.onKeyForButtonArea(keyCode);
        };

        function sendOrderList() {
            var contsKeyList = "";
            var pContsKeyList = "";

            for(var i=0; i<playList.length; i++) {
                if(playList[i-1] != originPL[originPL.indexOf(playList[i])-1]) {
                    contsKeyList += playList[i].contsKey + "|";
                    pContsKeyList += (playList[i-1]?playList[i-1].contsKey:"TOP") + "|"
                }
            }

            myHome.amocManager.sortMyPlayItemDetList(function (result, data) {
                if(result && data.resCode==0) {
                    callback();
                    LayerManager.historyBack();
                }
            },playListId, contsKeyList.substr(0, contsKeyList.length-1), pContsKeyList.substr(0, pContsKeyList.length-1));
        }
    };

    myHome.popup.playlistOrderChangePopup.prototype = new Layer();
    myHome.popup.playlistOrderChangePopup.constructor = myHome.layer.playlistOrderChangePopup;

    myHome.popup.playlistOrderChangePopup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.playlistOrderChangePopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomePlayListOrderChangePopup"] = myHome.popup.playlistOrderChangePopup;
})();