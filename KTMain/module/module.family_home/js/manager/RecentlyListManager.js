/**
 * Created by ksk91_000 on 2016-11-08.
 */
window.RecentlyListManager = new function() {
    var recentlyList = [];
    var isLoaded = false;
    var isInit = false;

    var dvdList = [];

    var update = function(callback) {
        myHome.amocManager.getViewHistoryNxt("saId=" + DEF.SAID + "&adultYn=N&opt=3&itemCnt=20&seriesTitleYn=Y", function(result, contentsData) {
            if (result && !!contentsData) {
                contentsData = contentsData.watchHistory;
                recentlyList = contentsData ? (_$.isArray(contentsData) ? contentsData : [contentsData]) : [];

                isInit = true;
                isLoaded = true;
            } else {
                // HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                extensionAdapter.notifySNMPError("VODE-00013");
                recentlyList = [];
            }
            if (callback) callback(recentlyList);
        });
    };

    var addContents = function(object, linkTime, onlyUpdate) {
        if (!isLoaded) return;
        if (object.adultOnlyYn == "Y") return;

        var hasData = false;
        for (var i = recentlyList.length - 1; i >= 0; i--) {
            if (recentlyList[i].contsId == object.contsId) {
                recentlyList.splice(i, 1);
                hasData = true;
            } else if (recentlyList[i].contYn == "N" && (recentlyList[i].catId == object.catId || (!!recentlyList[i].cCSersId) && recentlyList[i].cCSersId == object.cCSersId)) {
                recentlyList.splice(i, 1);
                hasData = true;
            }
        } if(onlyUpdate && !hasData) return;

        if (recentlyList.length == 20) {
            recentlyList.splice(19, 1);
        }

        recentlyList.unshift({
            imgUrl: object.imgUrl,
            contsName: object.contsName,
            prInfo: object.prInfo,
            catId: object.catId,
            contsId: object.contsId,
            contYn: object.contYn,
            runTime: object.runTime,
            wonYn: object.wonYn,
            cCSersId: object.cCSersId,
            linkTime: linkTime,
            seriesYn: object.contYn === "Y" ? "N" : "Y"
        });
    };

    var removeContentsList = function(idxList, callback) {
        var itemDeleteLength = idxList.length;
        var itemDeleteCount = 0;

        var contsIdList = "";
        var contYnList = "";
        var count = 0;
        for (var i = itemDeleteCount; i < 5 && i < itemDeleteLength; i++) {
            contsIdList += (((contsIdList.length > 0) ? "|" : "") + recentlyList[idxList[i]].contsId);
            contYnList += (((contYnList.length > 0) ? "|" : "") + recentlyList[idxList[i]].contYn);
            count++;
        }

        deleteChtckedItemsImpl(contsIdList, contYnList, count, callbackFunc);

        function deleteChtckedItemsImpl(_contsIdList, _contYnList, _count, callback) {
            console.log("[RecentlyListManager] deleteChtckedItemsImpl : " + arguments);
            LayerManager.startLoading();
            myHome.amocManager.setDelWatch(function(result, data) {
                if (result) {
                    callback(true, data, _count);
                } else {
                    callback(false);
                }
            }, DEF.SAID, _contsIdList, _contYnList)
        }

        function callbackFunc(res, data, _count) {
            if(res) {
                if(data.flag == 0) {
                    itemDeleteCount += _count;
                    if(itemDeleteCount < itemDeleteLength) {
                        var contsIdList = "";
                        var contYnList = "";
                        var count = 0;
                        for (var i = itemDeleteCount; i < itemDeleteCount + 5 && i < itemDeleteLength; i++) {
                            contsIdList += (((contsIdList.length > 0) ? "|" : "") + recentlyList[idxList[i]].contsId);
                            contYnList += (((contYnList.length > 0) ? "|" : "") + recentlyList[idxList[i]].contYn);
                            count++;
                        }
                        deleteChtckedItemsImpl(contsIdList, contYnList, count, callbackFunc);
                    } else if(itemDeleteCount == itemDeleteLength) {
                        // 키즈시청목록 reload를 위해 flag 전달.
                        if (ModuleManager.getModule("module.kids")) ModuleManager.getModule("module.kids").execute({method: "kidsRecentlyListCheck"});
                        for (var i = idxList.length - 1; i >= 0; i--) recentlyList.splice(idxList[i], 1);
                        LayerManager.stopLoading();
                        callback(true);
                        isLoaded = false;
                    }
                } else {
                    LayerManager.stopLoading();
                    openErrorPopUp(itemDeleteCount);
                    callback(false);
                    isLoaded = false;
                }
            } else {
                LayerManager.stopLoading();
                openErrorPopUp(itemDeleteCount);
                callback(false);
                isLoaded = false;
            }
        }
    };

    var removeContents = function(idx, callback) {
        myHome.amocManager.setDelWatch(function(result, data) {
            if (result) {
                if (data.flag == 0) {
                    recentlyList.splice(idx, 1);
                    if (ModuleManager.getModule("module.kids")) ModuleManager.getModule("module.kids").execute({method: "kidsRecentlyListCheck"});
                    callback(true);
                    isLoaded = false;
                } else {
                    HTool.openErrorPopup({message: data.errorMsg.split("\r\n")});
                    callback(false);
                }
            } else {
                openErrorPopUp(0);
                callback(false);
            }
        }, DEF.SAID, recentlyList[idx].contsId, recentlyList[idx].contYn)
    };

    function openErrorPopUp(itemDeleteCnt) {
        if(itemDeleteCnt > 0) {
            HTool.openErrorPopup({message: ["일부 콘텐츠를 삭제하지 못했습니다","잠시 후 다시 시도해 주세요"]});
            isLoaded = false;
        } else {
            HTool.openErrorPopup({message: ["삭제하지 못했습니다", "잠시 후 다시 시도해 주세요"], title:"알림", button:"확인"});
        }
    }


    var setLinkTime = function(contsId, linkTime) {
        for (var i in recentlyList) {
            if (recentlyList[i].contsId == contsId) {
                recentlyList[i].linkTime = linkTime;
                return;
            }
        }
    };

    var getRecentlyData = function(callback, noUpdate) {
        if (!isLoaded && !(noUpdate && isInit)) update(callback);
        else callback(recentlyList);
    };

    var getMyDvdData = function(callback) {
        myHome.amocManager.getBuyHistoryNxt("saId=" + DEF.SAID + "&adultYn=A&opt=3&itemCnt=20&deletedItemYn=Y&dvdYn=Y&pkgVodYn=Y", function(result, _data) {
            if (result) {
                if (_data.buyHistory == null) callback(true, []);
                else {
                    dvdList = Array.isArray(_data.buyHistory) ? _data.buyHistory : [_data.buyHistory];
                    callback(true, dvdList);
                }
            } else callback(false)
        });
    };

    var destroy = function() {
        StateManager.removeServiceStateListener(stateChangeListener);
    };

    var stateChangeListener = function(options) {
        if (options.nextServiceState == CONSTANT.SERVICE_STATE.STANDBY) {
            isLoaded = false;
            recentlyList = [];
        }
    };

    StateManager.addServiceStateListener(stateChangeListener);
    return {
        update: update,
        addContents: addContents,
        removeContents: removeContents,
        removeContentsList: removeContentsList,
        getRecentlyData: getRecentlyData,
        getMyDvdData: getMyDvdData,
        setLinkTime: setLinkTime,
        destroy: destroy
    }
};