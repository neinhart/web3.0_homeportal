/**
 * Created by ksk91_000 on 2016-11-08.
 */
window.WishListManager = new function() {
    var data = [];
    var length;
    var isLoaded = false;
    var triggerList = [];

    var update = function(callback, callTrigger) {
        myHome.amocManager.getPlayListNxt((oipfAdapter.basicAdapter.getConfigText(oipfDef.CONFIG.KEY.ADULT_MENU_DISPLAY)==="true"?"A":"N"), function(result, contentsData) {
            if (result) {
                contentsData = contentsData.playList;
                data = contentsData ? (_$.isArray(contentsData) ? contentsData : [contentsData]) : [];
                length = data.length;
                filteringAdultContent(data);

                isLoaded = true;
                if (callTrigger) for (var i in triggerList) triggerList[i].call();
            } else {
                data = [];
                extensionAdapter.notifySNMPError("VODE-00013");
                // HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
            }
            if (callback) callback(data);
        });
    };

    function filteringAdultContent(dataList) {
        for (var i in dataList) {
            var data = dataList[i];
            if (data.adultOnlyYn == "Y") {
                data.itemName = data.itemName.substr(0, 1) + "******************************************************".substr(0, data.itemName.length);
                data.imgUrl = modulePath + "resource/image/default_19plus.png";
            }
        }
    }

    var addContents = function(itemType, itemId, categoryId, cmbYn, resolCd, callback, isShowToast) {
        if(itemType==1 || itemType==0) categoryId = itemId;
        var isHd = cmbYn ? getHdYn(resolCd) : "";
        myHome.amocManager.setPlayListNxt(1, itemType, itemId, categoryId, 1, isHd, function(result, response) {
            if (result && response) {
                if (response.reqCode == 0) {
                    // 정상등록
                    setClearFlag();
                    if (callback) callback(true);
                    if (isShowToast) showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다");
                } else if (response.reqCode == 1) {
                    //삭제
                    // removeContents([{itemId: itemId}], callback, isShowToast);
                    openConfirmPopup([{itemId: itemId}], callback, isShowToast);
                } else if (response.reqCode == 3) {
                    if (callback) callback(false);
                    if (isShowToast) showToast("VOD 찜을 하지못했습니다. 잠시후 다시 해주세요");
                    throw "networkError(2)";
                }
            } else {
                if (callback) callback(false);
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                throw "networkError(1)";
            }
        });
    };

    var openConfirmPopup = function (itemId, callback, isShowToast) {
        LayerManager.activateLayer({
            obj: {
                id: "EditModeDeletePopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: false,
                params: {
                    type: 4,
                    items: itemId,
                    callback: function (res) {
                        if (res) {
                            WishListManager.removeContents(itemId, function (result) {
                                if (result) showToast("VOD찜을 해제하였습니다");
                            }, false);
                        } LayerManager.historyBack();
                    }
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    };

    var removeContents = function(deleteList, callback, isShowToast) {
        var itemIds = deleteList[0].itemId;
        for (var i = 1; i < deleteList.length; i++) itemIds += "|" + deleteList[i].itemId;
        myHome.amocManager.setPlayListNxt(deleteList.length, "", itemIds, "", 2, "", function(result, resData) {
            if (result) {
                if (resData.reqCode == 0) {
                    for (var i = deleteList.length - 1; i >= 0; i--) data.splice(data.indexOf(deleteList[i]), 1);
                    for (var i in triggerList) triggerList[i].call();
                    setClearFlag();
                    if (callback) callback(true);
                    if (isShowToast) showToast("VOD 찜을 해제하였습니다");
                } else if(resData.reqCode == 2) {
                    for (var i = deleteList.length - 1; i >= 0; i--) data.splice(data.indexOf(deleteList[i]), 1);
                    for (var i in triggerList) triggerList[i].call();
                    setClearFlag();
                    if (callback) callback(false);
                    HTool.openErrorPopup({title:"알림", message: ["이미 삭제된 콘텐츠입니다"], noShowSAID: true});
                } else {
                    log.printDbg(resData);
                    if (callback) callback(false);
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    throw "networkError(4)";
                }
            } else throw "networkError(3)";
        });
    };

    var getWishListData = function(callback, noUpdate) {
        if (!noUpdate && !isLoaded) update(callback);
        else callback(data);
    };

    var setClearFlag = function() {
        // if(triggerList.length > 0) update(null, true);
        // else
        isLoaded = false;
    };

    var addUpdateTrigger = function(trigger) {
        if (triggerList.indexOf(trigger) >= 0) return;
        triggerList.push(trigger);
    };

    var removeUpdateTrigger = function(trigger) {
        var index = triggerList.indexOf(trigger);
        if (index >= 0) triggerList.splice(index, 1);
    };

    function getHdYn(resolCd) {
        switch (resolCd) {
            case "HD" :
                return "Y";
            case "SD" :
                return "N";
            case "FHD" :
                return "F";
            case "UHD" :
                return "U";
            default:
                return "";
        }
    }

    var destroy = function() {
        StateManager.removeServiceStateListener(stateChangeListener);
    };

    var stateChangeListener = function(options) {
        if (options.nextServiceState == CONSTANT.SERVICE_STATE.STANDBY) {
            isLoaded = false;
            data = [];
        }
    };

    StateManager.addServiceStateListener(stateChangeListener);
    return {
        update: update,
        addContents: addContents,
        removeContents: removeContents,
        getWishListData: getWishListData,
        setClearFlag: setClearFlag,
        addUpdateTrigger: addUpdateTrigger,
        removeUpdateTrigger: removeUpdateTrigger,
        destroy: destroy
    }
};