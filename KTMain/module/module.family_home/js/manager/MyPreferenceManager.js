/**
 * Created by ksk91_000 on 2016-11-25.
 */
window.MyPreferenceManager = new function () {
    var preference;
    var hasPreference;
    var isLoaded = false;

    var adult_limit;

    var totalCount = 0;
    var dataOnlyload = false;

    var _isNoAudult = function () {
        return StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT)!=1;
    };

    var _update = function (callback, _dataOnlyload) {
        if(_dataOnlyload) dataOnlyload = true;
        else dataOnlyload = false;

         myHome.curationManager.getMyPreference(_isNoAudult(), function (result, data) {
            if (result) {
                if(!dataOnlyload) isLoaded = true;
                preference = data;
                if (data.RESULT_CODE == 200) {
                    hasPreference = true;
                    totalCount = data.TOTAL_ITEM_CNT;
                    if (callback) callback(data);
                } else {
                    hasPreference = false;
                    totalCount = 0;
                    if (callback) callback(data);
                }
            } else {
                if(callback) callback(null);
            }
        });
    };

    var _getTotalCount = function () {
        return totalCount;
    };

    var _getMyPreference = function (callback) {
        if(dataOnlyload) {
            isLoaded = true;
            dataOnlyload = false;
        }

        if (!isLoaded) _update(callback, false);
        else callback(preference);
        // else setTimeout(function() {callback(preference)}, 0);
    };

    var _initMyPrefence = function (callback) {
        myHome.curationManager.initMyPreference(_isNoAudult(), function(res, data) {
            if(res && data.RESULT_CODE==200) {
                RatingContentsManager.setUpdateFlag();
                hasPreference = false;
                preference = null;
                totalCount = 0;
                callback(true);
            } else {
                callback(false);
            }
        });
    };

    var _setUpdateFlag = function () {
        isLoaded = false;
        preference = null;
        totalCount = 0;
        RatingContentsManager.setUpdateFlag();
    };

    var _hasPreference = function () {
        return hasPreference || !isLoaded;
    };

    var _hasUpdate = function () {
        return !isLoaded;
    };

    return {
        update: _update,
        getMyPreference: _getMyPreference,
        setUpdateFlag: _setUpdateFlag,
        hasPreference: _hasPreference,
        hasUpdate: _hasUpdate,
        isNoAdult: _isNoAudult,
        getTotalCount: _getTotalCount,
        initMyPreference: _initMyPrefence
    }
};