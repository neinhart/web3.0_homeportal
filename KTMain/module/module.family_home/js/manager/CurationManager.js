/**
 * Created by ksk91_000 on 2016-11-25.
 */
window.CurationManager = new function () {
    var listData;
    var menuData = [];
    var expireDate;
    var isLoaded = false;

    var update = function (callback) {
        myHome.VodRecmManager.getCuration(KTW.SAID, "CURATION", getPkgCode(), MyPreferenceManager.isNoAdult()?"Y":"N", 20, function (result, data) {
            try {
                if (result) {
                    var menuList = [];
                    var childObj = {};
                    for (var item in data.ITEM_LIST) {
                        item = data.ITEM_LIST[item];
                        childObj[item.MENU_ID] = childObj[item.MENU_ID] || [];
                        childObj[item.MENU_ID].push(item);
                    }
                    for (var menu in data.MENU_LIST) {
                        menu = data.MENU_LIST[menu];
                        menu.child = childObj[menu.MENU_ID];
                        menuList.push(new ViewData(menu.MENU_ID, myHome.view.ContentsView, menu, menu.MENU_NAME, menu.MENU_ID));
                    }
                    menuData = menuList;
                    listData = childObj[menuList[0].viewId];
                    expireDate = new Date();
                    var hours = expireDate.getHours();
                    expireDate.setHours(hours + 6);
                    isLoaded = true;

                    callback();
                } else {
                    listData = null;
                    isLoaded = false;
                    callback();
                }
            } catch(e) {
                log.printErr(e);
                listData = null;
                isLoaded = false;
                callback();
            }
        });

        function getPkgCode() {
            var baseCd = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
            var pkgList = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);

            return  baseCd + (pkgList?("," + pkgList):"");
        }
    };

    var getCurationData = function (callback) {
        if (!isLoaded || isExpire()) update(function () { callback(menuData); });
        else setTimeout(function() {callback(menuData)}, 0);
    };

    var getFirstList = function (callback) {
        if (!isLoaded || isExpire()) update(function () { callback(listData); });
        else setTimeout(function() {callback(listData)}, 0);
    };

    var expire = function () {
        isLoaded = false;
    };

    function isExpire() {
        return expireDate<new Date();
    }

    return {
        update: update,
        getCurationData: getCurationData,
        getFirstList: getFirstList,
        expire: expire
    }
};