if(!myHome) var myHome = {};

myHome.NetManager = new function() {
    var INSTANCE = this;

    // Ajax URL Request (GET)
    // 'callback' 파라메터가 있는경우는 서버가 비동기 호출되며, callback함수로 결과 값을 리턴한다.
    // 'callback' 파라메터가 없는경우는 서버가 동기 호출 되며, 함수 수행이 끝나고 호출한곳으로 결과값을 리턴한다.
    this.ajaxRequest = function(targetUrl, callback) {
        var result = null;
        _$.ajax({
            type: "get",
            async: callback ? true : false,
            url: targetUrl,
            crossDomain: true,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            contentEncoding: "euc-kr",
            dataType: "json",
            success: function(data) {
                result = data;
                if (callback != null)
                    callback(result);
            },
            error: function(e, state) {
                log.printErr(e);
                result = state;
                if (callback != null)
                    callback(result);
            }
        });
        return result;
    };
    
    this.getCategoryData = function(cateID, callback) {
        _$.getJSON(modulePath + "resource/data/" + cateID + ".json", callback)
        // return this.ajaxRequest(modulePath + "resource/data/" + cateID + ".json");
    }
}