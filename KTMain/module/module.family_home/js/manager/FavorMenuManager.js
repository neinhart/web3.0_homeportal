/**
 * Created by User on 2017-07-18.
 */
var FavorMenuManager = new function () {
    var favorMenuData = [];
    var isLoaded = false;
    var isLoading = false;

    function update(callback) {
        isLoading = true;
        myHome.web3Manager.getFavoriteMenuInfo(function (result, data) {
            if(result && data.resultCd==0) {
                favorMenuData = data.list || [];
                isLoaded = true;
            } else {
                favorMenuData = [];
                isLoaded = false;
            }

            isLoading = false;
            if(callback) callback(favorMenuData);
        });
    }

    var _getFavorMenuData = function (callback) {
        if(isLoaded) callback(favorMenuData);
        else update(callback);
    };

    var _setFavorMenuData = function (data, updateFlag) {
        favorMenuData = data;
        if(updateFlag) isLoaded = false;
    };

    var _destroy = function() {
        StateManager.removeServiceStateListener(stateChangeListener);
    };

    var stateChangeListener = function(options) {
        if (options.nextServiceState === CONSTANT.SERVICE_STATE.STANDBY) {
            isLoaded = false;
            favorMenuData = [];
        }
    };

    StateManager.addServiceStateListener(stateChangeListener);
    return {
        getFavorMenuData: _getFavorMenuData,
        setFavorMenuData: _setFavorMenuData,
        destroy: _destroy
    }
};