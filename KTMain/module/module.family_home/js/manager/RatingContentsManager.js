/**
 * Created by ksk91_000 on 2016-11-25.
 */
window.RatingContentsManager = new function () {
    var list = [];
    var isLoaded = false;
    var isLoading = false;

    var adult_limit;
    var totalCount = 0;
    var dataOnlyload = false;
    var updateForce = false;

    var _getAdultLimit = function () {
        return MyPreferenceManager.isNoAdult();
    };

    var update = function (callback, _dataOnlyload) {
        if(_dataOnlyload) dataOnlyload = true;
        else dataOnlyload = false;

        if((!isLoading) || updateForce) {
            isLoading = true;
            myHome.curationManager.getMyRatingContent(_getAdultLimit(), 200, "D", function (result, data) {
                if (result) {
                    if(!dataOnlyload) isLoaded = true;
                    if (data.RESULT_CODE == 200) {
                        list = data.ITEM_LIST;
                        totalCount = list.length;
                        if(callback) callback(list);
                    } else {
                        list = [];
                        totalCount = 0;
                        if(callback) callback(list);
                    }
                    for (var i in list) {
                        if(!!list[i].CONT_RATING) {
                            list[i].OLLEH_RATING = list[i].RATING;
                            list[i].RATING = list[i].CONT_RATING;
                            delete list[i].CONT_RATING;
                        }
                    }isLoading = false;
                }

                updateForce = false;
            });
        }else {
            isLoading = false;
            dataOnlyload = false;
            if(callback) callback([]);
        }
    };

    var _getTotalCount = function () {
        return totalCount;
    };

    var getRatingContents = function (callback) {
        if(dataOnlyload) {
            isLoaded = true;
            dataOnlyload = false;
        }

        if (!isLoaded) update(callback);
        else setTimeout(function() {callback(list)}, 0);
    };

    var setUpdateFlag = function (_updateForce) {
        list = null;
        totalCount  = 0;
        isLoaded = false;
        if(_updateForce) updateForce = true;
        else updateForce = false;
    };

    var hasUpdate = function () {
        return !isLoaded;
    };


    return {
        update: update,
        getRatingContents: getRatingContents,
        setUpdateFlag: setUpdateFlag,
        hasUpdate: hasUpdate,
        getTotalCount: _getTotalCount

    }
};