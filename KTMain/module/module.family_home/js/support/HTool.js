// Strict Mode On (엄격모드)
"use strict";
"use warning";

/**
 * @author Lazuli
 * 2014.07.30
 */

window.HTool = new function() {

    // 숫자 컴마 찍기
    this.addComma = function(src) {
        src = src + "";
        var temp_str = src.replace(/,/g, "");

        for (var i = 0, retValue = String(), stop = temp_str.length; i < stop; i++) {
            retValue = ((i % 3) == 0) && i != 0 ? temp_str.charAt((stop - i) - 1) + "," + retValue : temp_str.charAt((stop - i) - 1) + retValue;
        }

        return retValue;
    };

    // YYYYMMDD
    this.getDateDotFormat = function(date) {
        return date.substring(0, 4) + "." + date.substring(4, 6) + "." + date.substring(6, 8);
    };

    // 숫자앞에 0 붙이기 
    // n : 숫자, digits : 원하는 자리수
    this.leadingZero = function(n, digits) {
        var zero = "";
        n = n.toString();

        if (n.length < digits) {
            for (var i = 0; i < digits - n.length; i++) {
                zero += "0";
            }
        }
        return zero + n;
    };

    // 배열에서 특정 스트링을 가진 배열 값을 삭제.
    this.removeArrayString = function(arr, removeStr) {
        if (HTool.inArray(removeStr, arr)) {
            var index = arr.indexOf(removeStr);
            arr.splice(index, 1);
        }
    };

    // 스트링 Array 에 특정 스트링이 있는지 검사 (Boolean)
    this.inArray = function(str, arry) {
        if (Object.prototype.toString.call(str) != '[object String]')
            return false;
        if (Object.prototype.toString.call(arry) != '[object Array]')
            return false;

        for (var ii = 0, al = arry.length; ii < al; ii++)
            if (arry[ii] == str)
                return true;

        return false;
    };

    // OBJECT 배열에서 특정 ITEM(Key)의 값을 검색해서 일치하는 OBJECT를 모두 반환한다.
    // var objArr = [{"ID": "AAA", "NM":"유태상"},{"ID":"BBB","NM":"지니프릭스"}, {"ID":"BBB","NM":"스튜디오B"}];
    // HTool.searchJsonObjectArray(objArr, "ID", "BBB");
    // 결과값 : [{"ID":"BBB","NM":"지니프릭스"}, {"ID":"BBB","NM":"스튜디오B"}]
    this.searchJsonObjectArray = function(objArray, searchKey, searchData) {
        if (Object.prototype.toString.call(objArray) != '[object Array]')
            return [];
        if (objArray.length == 0)
            return [];
        if (objArray[0][searchKey] == null)
            return [];
        if (Object.prototype.toString.call(searchData) == '[object String]') {
            var t = searchData;
            searchData = [];
            searchData[0] = t;
        }
        var result = [];
        var idx = 0;
        for (var ii = 0, ol = objArray.length; ii < ol; ii++) {
            if (HTool.inArray(objArray[ii][searchKey], searchData))
                result[idx++] = objArray[ii];
        }
        return result;
    };

    // OBJECT 배열에서 특정 ITEM(Key)의 값을 검색해서 일치하는 OBJECT를 제거한다.
    // var objArr = [{"ID": "AAA", "NM":"유태상"},{"ID":"BBB","NM":"지니프릭스"}, {"ID":"BBB","NM":"스튜디오B"}];
    // HTool.removeJsonObjectArray(objArr, "ID", "BBB");
    // 결과값 : [{"ID": "AAA", "NM":"유태상"}]
    this.removeJsonObjectArray = function(objArray, searchKey, searchData) {
        if (Object.prototype.toString.call(objArray) != '[object Array]')
            return null;
        if (objArray.length == 0)
            return null;
        if (objArray[0][searchKey] == null)
            return null;
        if (Object.prototype.toString.call(searchData) == '[object String]') {
            var t = searchData;
            searchData = [];
            searchData[0] = t;
        }
        var result = [];
        var idx = 0;
        for (var ii = 0, ol = objArray.length; ii < ol; ii++) {
            if (!HTool.inArray(objArray[ii][searchKey], searchData))
                result[idx++] = objArray[ii];
        }
        return result;
    };

    // 불러올 리소스가 같은 이름 패턴으로 여러개 일때 url 셋팅해주는 함수
    this.getURLs = function(root, name, type, length) {
        if (arguments.length != 4) log.printErr("[HMF] getURLs Arguments Error");
        var urlArr = [];
        var url = "";

        for (var i = 0; i < length; i++) {
            url = root + name + i + type;
            urlArr.push(url);
        }

        return urlArr;
    };

    // 글자를 한개씩 분리하여 배열에 저장
    this.toCharArray = function(str) {
        if (Object.prototype.toString.call(str) != "[object String]") {
            log.printErr("Not String !!");
        }
        var result = [];
        for (var i = 0, sl = str.length; i < sl; i++) {
            result.push(str.substring(i, i + 1));
        }
        return result;
    };

    // 글자를 원하는 개수만큼 분리하여 배열에 저장
    this.toCharArrayWithLength = function(str, len) {
        if (Object.prototype.toString.call(str) != "[object String]") {
            log.printErr("Not String !!");
        }
        if (isNaN(len)) len = Number(len);
        var arr = [];
        for (var i = 0; i < str.length; i += len) {
            arr.push(str.substring(i, i + len));
        }
        return arr;
    };

    this.getRandomRangeValue = function(first, last) {
        return Math.floor((Math.random() * (last - first)) + first);
    };

    this.toCharArrayWithWidth = function(text, maxWidth) {
        var words = text.split("");
        var lines = [];
        var currentLine = words[0];
        for (var i = 1; i < words.length; i++) {
            var word = words[i];
            var width = g.measureText(currentLine + word).width;
            if (width < maxWidth) {
                currentLine += word;
            } else {
                lines.push(currentLine);
                currentLine = word;
            }
            word = null;
            width = null;
        }
        lines.push(currentLine);
        words = null;
        currentLine = null;
        return lines;
    };

    /**
     * Text문장을 html문으로 변환 하여 반환한다.
     * ======================================
     * \n -> <br>로 변경,
     * <br> 외 태그는 무효함,
     * text로 입력된 <,>,&,",ⓒ,®,™ 등을 html상으로 표현될 수 있도록 html 예약어로 변경해 준다. )
     * ======================================
     * @param txt : 변환하고자 하는 텍스트
     * @param crtxt : 캐리지 리턴 텍스트(미입력시 기본값 사용)
     */
    this.convertTextToHtml = function (txt, crtxt) {
        txt = txt.replace(/&/g, '&amp');
        txt = txt.replace(/</g, '&lt');
        txt = txt.replace(/>/g, '&gt');
        txt = txt.replace(/"/g, '&quot');
        txt = txt.replace(/ⓒ/g, '&copy');
        txt = txt.replace(/®/g, '&reg');
        txt = txt.replace(/™/g, '&trade');
        txt = txt.replace(/™/g, '&trade');
        if(crtxt) txt = txt.replace(new RegExp(crtxt, "g"), '<br>');
        else{
            txt = txt.replace(/\r\n/g, '<br>');
            txt = txt.replace(/\n/g, '<br>');
        }

        return txt;
    };

    // 2차원 배열 만들기
    this.make2DArray = function(num, num2) {
        var arr = new Array(num);
        for (var i = 0; i < num; i++) {
            if (num2 == null)
                arr[i] = [];
            else if (num2 > 0) {
                arr[i] = new Array(num2);
            }
        }
        return arr;
    };

    this.indexPlus = function(curIdx, maxIndex) {
        return (curIdx + 1) % maxIndex;
    };

    this.indexMinus = function(curIdx, maxIndex) {
        return (curIdx - 1 + maxIndex) % maxIndex;
    };

    /**
     * 연산된 인덱스값 반환. 방향키나 OK키로 depth 만큼 이동되는 메뉴, 기타 인덱스 연산등에서 사용
     * @param curIdx 현재 인덱스
     * @param depth 이동(키) 값
     * @param len 전체 메뉴길이
     * @return 연산된 인덱스 값
     */
    this.getIndex = function(curIdx, depth, len) {
        curIdx += depth;
        if (curIdx < 0)
            curIdx += len;
        else if (curIdx >= len)
            curIdx -= len;
        return curIdx;
    };

    /**
     * 현재 인덱스를 제외한 숫자를 랜덤으로 선택
     * @param CurIndex 현재 인덱스
     * @param length 인덱스 길이
     * @return 현재 인덱스를 제외한 랜덤숫자
     */
    this.getAnotherIndex = function(CurIndex, length) {
        return (CurIndex + new Random().nextInt(length - 1) + 1) % length;
    };

    /**
     * 한글 String 마지막 글자의 받침여부 확인
     * @param txt 받침 여부를 확인할 문자열 (마지막글자가 한글이어야함).
     * @return boolean 마지막 글자의 받침여부 (한글이 아닌경우 false)
     */
    this.haveBatchim = function(txt) {
        txt = txt.toString();
        var code = txt.charCodeAt(txt.length - 1) - 44032;

        // 한글이 아닐 때
        if (code < 0 || code > 11171) return false;

        // 한글일 때
        return !(code % 28 == 0);
    };

    this.getBoolean = function(str) {
        if (!str) return false;
        return "TtYy1".indexOf(str.charAt(0)) > -1;
    };

    this.isTrue = function(str) {
        // if (!str) return false;
        // return "TtYy1".indexOf(str.charAt(0)) > -1;
        return str==="Y" || str==="y";
    };

    this.isFalse = function(str) {
        // if (!str) return false;
        // return "FfNn0".indexOf(str.charAt(0)) > -1;
        return str==="N" || str==="n";
    };

    // yyyyMMddHHmmSS
    this.compareTime = function(curDate, endDate) {
        var cd = dateConvert(curDate);
        var ed = dateConvert(endDate);

        return Number(ed) - Number(cd);
    };

    // 밀리초를 시간으로 표시 (hh:mm:ss) 
    this.msecToTime = function(msec) {
        var totalSec = parseInt(msec / 1000);
        var totalMin = parseInt(totalSec / 60);

        var nHour = parseInt(totalMin / 60);
        var nMin = totalMin % 60;
        var nSec = totalSec % 60;

        return {
            "hour": nHour,
            "min": nMin,
            "sec": nSec
        };
    };

    // yyyyMMddHHmmSS
    var dateConvert = function(date) {
        var ret = new Date();
        date = date.toString();
        ret.setFullYear(date.substring(0, 4) - 0);
        ret.setMonth(date.substring(4, 6) - 0);
        ret.setDate(date.substring(6, 8) - 0);
        ret.setHours(date.substring(8, 10) - 0);
        ret.setMinutes(date.substring(10, 12) - 0);
        ret.setSeconds(date.substring(12, 14) - 0);
        return Date.parse(ret);
    };

    this.getDateStr = function() {
        var date = new Date();
        return HTool.leadingZero(date.getFullYear(), 4) +
            HTool.leadingZero(date.getMonth() + 1, 2) +
            HTool.leadingZero(date.getDate(), 2) +
            HTool.leadingZero(date.getHours(), 2) +
            HTool.leadingZero(date.getMinutes(), 2) +
            HTool.leadingZero(date.getSeconds(), 2);
    };

    this.getDateStrWithMilliSeconds = function() {
        var date = new Date();
        return HTool.leadingZero(date.getFullYear(), 4) +
            HTool.leadingZero(date.getMonth() + 1, 2) +
            HTool.leadingZero(date.getDate(), 2) +
            HTool.leadingZero(date.getHours(), 2) +
            HTool.leadingZero(date.getMinutes(), 2) +
            HTool.leadingZero(date.getSeconds(), 2) +
            HTool.leadingZero(date.getMilliseconds(), 2);
    };

    // ObjectArray 정렬
    this.sortingObjectArray = function(objArr, value, sort) {
        if (!sort || sort == 0) {
            objArr.sort(function(a, b) {
                if (!isNaN(a[value]) && !isNaN(b[value])) return Number(a[value]) < Number(b[value]) ? -1 : Number(a[value]) > Number(b[value]) ? 1 : 0;
                else return a[value] < b[value] ? -1 : a[value] > b[value] ? 1 : 0;
            })
        } else {
            objArr.sort(function(a, b) {
                if (!isNaN(a[value]) && !isNaN(b[value])) return Number(a[value]) < Number(b[value]) ? 1 : Number(a[value]) > Number(b[value]) ? -1 : 0;
                else return a[value] < b[value] ? 1 : a[value] > b[value] ? -1 : 0;
            })
        }
    };

    /**
     * ObjectArray를 복사 (서로 참조 안함)
     * @param {ObjectArray} array
     * @returns {ObjectArray} 복사된 ObjectArray
     */
    this.cloneObjectArray = function(array) {
        return JSON.parse(JSON.stringify(array));
    };

    /**
     * Array를 복사 (서로 참조 안함)
     * @param {Array} array
     * @returns {Array} 복사된 Array
     */
    this.cloneArray = function(array) {
        return array.slice(0);
    };

    /**
     * 유효한 이미지인지 확인
     * @param {image} img
     * @returns {Boolean}
     */
    this.isImage = function(img) {
        if (Object.prototype.toString.call(img) == "[object HTMLImageElement]") {
            if (img.src == "") return false;
            if (!img.complete) return false;
            if (!img.naturalWidth) return false;
        } else {
            return false;
        }
        return true;
    };

    /**
     * html에 텍스트를 넣을 때, 스페이스가 무시되지 않도록 &nbsp; 로 변환한다.
     * .text()로 입력하지 말고 .html로 입력해야 한다.
     * @param {string} str 변환할 String
     * @returns {string} 변환된 String
     */
    this.addSpace = function (str) {
        return str.replace(/ /g, "&nbsp;");
    };

    this.getTitle =function (korName, engName) {
        var language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");
        if(language!="kor") return engName||korName;
        else return korName;
    };

    this.getMenuName = function(vitem) {
        if(vitem) return this.getTitle(vitem.name, vitem.englishItemName);
    };

    this.getNoticeData = function () {
        var originData = MenuDataManager.getNoticeData();
        var viewingData = [];
        var curDateStr = HTool.getDateStr();
        // 노출시점에 해당하지 않는 데이터 필터링
        for(var i=0; i<originData.length; i++) {
            if(curDateStr <= originData[i].end_time && curDateStr >= originData[i].start_time) viewingData.push(originData[i]);
        } return viewingData;
    };

    this.getMyHomeMenuName = function() {
        var myHomeMenuData = MenuDataManager.searchMenu({
            menuData: MenuDataManager.getMenuData(),
            allSearch: false,
            cbCondition: function (menu) { if(!menu.parent &&menu.catType=="SubHome") return true; }
        })[0];
        if(myHomeMenuData&&myHomeMenuData.name) return myHomeMenuData.name;
        else return "마이메뉴";
    };

    this.openErrorPopup = function(params) {
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeError",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: params.title||"오류",
                    message: params.message,
                    button: params.button||"닫기",
                    reboot: params.reboot,
                    showSAID: params.showSAID,
                    callback: params.callback
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    };


    this.getPoint = function (_useCached, _callback) {
        var cacheData;
        var parseData;

        if (_useCached) { // 포인트 조회 시, cache 된 data를 사용
            var cacheData = StorageManager.ps.load(StorageManager.KEY.STB_POINT_CACHE);

            try {
                parseData = JSON.parse(cacheData);
                if(!(parseData instanceof Object)) cacheData = null;
            } catch(e) {
                cacheData = null;
            }
        }

        if(!cacheData || JSON.parse(cacheData).ReqFlag===true) {
            myHome.PointManager.getPoint('1', function(res, _pointData) {
                if (res) {
//                var sampleData = ["10000",
//                        "2000",
//                        "1000",
//                        "1000",
//                        "4"];
//
//                var testOptionData = [
//                    {
//                        // 맴버십
//                        settlWayCd: 'ST',
//                        blncAmt: sampleData[0],
//                        settlRate: '20',
//                        mbrTypeCd: 'R'
//                    },
//                    {
//                        // 티비 포인트
//                        settlWayCd: 'TP',
//                        blncAmt: sampleData[1],
//                        tvPointYn: 'Y'
//                    },
//                    {
//                        // 티비 쿠폰 (프로모션)
//                        settlWayCd: 'CP',
//                        blncAmt: sampleData[2]
//                    },
//                    {
//                        // 티비 쿠폰
//                        settlWayCd: 'CS',
//                        blncAmt: sampleData[3]
//                    },
//                    {
//                        // 콘텐츠 이용
//                        settlWayCd: 'CU',
//                        blncAmt: sampleData[4]
//                    }
//                ]

                    // 캐시 데이터 형태로 저장
                    var dataForCache = {};
                    var optionData = _pointData.settlWayList ? _pointData.settlWayList : null;
                    var _data;
                    dataForCache.PointAmount = _pointData.totBlncAmt ? _pointData.totBlncAmt : 0;
//                optionData = testOptionData; // for test

                    for (var i = 0; i < optionData.length; i++) {
                        _data = optionData[i];
                        if (_data.settlWayCd === "ST") {
                            //KT 맴버십
                            dataForCache.StarPoint = _data.blncAmt;
                            dataForCache.UsrDiv = _data.mbrTypeCd;
                            dataForCache.MembershipRate = _data.settlRate;
                        } else if (_data.settlWayCd === "CP") {
                            // TV 쿠폰 (KT제공)
                            dataForCache.KtCoupon = _data.blncAmt;
                        } else if (_data.settlWayCd === "TP") {
                            // TV 포인트
                            dataForCache.TvPoint = _data.blncAmt;
                            dataForCache.TvPointYn = _data.tvPointYn;
                        } else if (_data.settlWayCd === "CS") {
                            // TV 쿠폰
                            dataForCache.TvMoney = _data.blncAmt;
                        } else if (_data.settlWayCd === "CU") {
                            // 이용권 개수
                            dataForCache.NumOfCoupon = _data.blncAmt;
                        }
                    }

                    dataForCache.ReqFlag = false;
                    dataForCache.UpdateTime = new Date().getTime();

                    StorageManager.ps.save(StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(dataForCache));
                    _callback(dataForCache);
                }
                else {
                    var pointData = {
                        StarPoint: 0,
                        KtCoupon: 0,
                        TvPoint: 0,
                        TvPointYn: "Y",
                        TvMoney: 0,
                        NumOfCoupon: 0
                    };

                    _callback(pointData);
                }
            });
        }
        else {
            _callback(parseData);
        }
    }

    this.getPointLocator = function (idx) {
        switch (idx) {
            case 0: //KT멤버십
                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.STAR_POINT.LOCATOR.LIVE;
                else return CONSTANT.STAR_POINT.LOCATOR.BMT;
            case 1: //TV포인트
                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.TV_POINT.LOCATOR.LIVE;
                else return CONSTANT.TV_POINT.LOCATOR.BMT;
            case 2: //TV쿠폰
                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.TV_COUPON.LOCATOR.LIVE;
                else return CONSTANT.TV_COUPON.LOCATOR.BMT;
            case 3: //콘텐츠 이용권
                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.CONTENT_COUPON.LOCATOR.LIVE;
                else return CONSTANT.CONTENT_COUPON.LOCATOR.BMT;
        }
    }

};

// Java 의 ArrayList 와 동일한 메소드 구현
var ArrayList = function() {
    this.array = new Array();
};
ArrayList.prototype.add = function() {
    if (arguments.length == 1) this.array[this.array.length] = arguments[0];
    else if (arguments.length == 2) this.array.splice(arguments[1], 0, arguments[0]);
    else log.printErr("[HMF][ArrayList] Add Error - Wrong Arguments");
}
ArrayList.prototype.size = function() {
    return this.array.length;
};
ArrayList.prototype.get = function(index) {
    return this.array[index];
};
ArrayList.prototype.clear = function() {
    this.array = [];
};
ArrayList.prototype.remove = function(obj) {
    for (var i = this.array.length - 1; i >= 0; i--) {
        if (this.array[i] == obj) {
            this.array.splice(i, 1);
        }
    }
};
ArrayList.prototype.removeIndex = function(index) {
    this.array.splice(index, 1);
};
ArrayList.prototype.removeAll = function() {
    this.array = [];
};
ArrayList.prototype.contains = function(obj) {
    return $.inArray(obj, this.array) != -1;
};
ArrayList.prototype.isEmpty = function() {
    return this.array.size <= 0;
};
ArrayList.prototype.getArray = function() {
    return this.array;
};

// Java 의 Random 함수와 동일한 메소드 구현
var Random = function() {
};
Random.prototype.nextInt = function(_num) {
    return Math.floor(Math.random() * _num);
};

