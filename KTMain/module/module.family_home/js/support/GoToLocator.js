/**
 * Created by ksk91_000 on 2017-04-18.
 */
window.gotoLocator = function(data, reqPathCd) {
    if(!data) return;
    switch (data.itemType-0) {
        case 0:
            var subHomeModule = ModuleManager.getModule("module.subhome");
            if(!subHomeModule) {
                LayerManager.startLoading();
                ModuleManager.loadModule({
                    moduleId: "module.subhome",
                    cbLoadModule: function (success) { if (success) gotoLocator(data); }
                }); return;
            }
            var menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) { if(menu.id==data.itemId) return true; }
            });
            subHomeModule.activateLayerByMenu({menu:menuData[0]});
            break;
        case 1:    //시리즈
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.itemId, null, reqPathCd);
            } catch(e) {}
            openDetailLayer(data.itemId, null, reqPathCd);
            break;
        case 2:    //컨텐츠
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.itemId, reqPathCd);
            } catch(e) {}
            openDetailLayer(null, data.itemId, reqPathCd);
            break;
        case 3:    //멀티캐스트 양방향 서비스                               
            var nextState;
            if(data.locator == CONSTANT.APP_ID.MASHUP)
                nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            else
                nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

            AppServiceManager.changeService({
                nextServiceState: nextState,
                obj: {
                    type: CONSTANT.APP_TYPE.MULTICAST,
                    param: data.locator,
                    ex_param: data.parameter
                }
            });
            break;
        case 7:    //유니캐스트 양방향 서비스
            var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            AppServiceManager.changeService({
                nextServiceState: nextState,
                obj :{
                    type: CONSTANT.APP_TYPE.UNICAST,
                    param: data.itemId
                }
            });
            break;
        case 8:    //웹뷰
            AppServiceManager.startChildApp(data.locator);
            break;
    }
}