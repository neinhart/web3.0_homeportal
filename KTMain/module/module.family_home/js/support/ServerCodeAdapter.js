/**
 * Created by ksk91_000 on 2016-06-27.
 */

window.ServerCodeAdapter = new function () {
    this.getPrInfo = function(prInfo) {
        //gyuh 시청등급 데이터가 비어서 올때 처리
        if(prInfo == "") {
            return "all";
        }
        switch(prInfo) {
            case 1: return "all";
            case 2: return 7;
            case 3: return 12;
            case 4: return 15;
            case 5: return 19;
        }
    }
}