/**
 * Created by ksk91_000 on 2016-10-31.
 */
window.openAdultAuthPopup = function(callback, autoClose, type) {
    if(AdultAuthorizedCheck.isAdultAuthorized()) {
        callback();
    }else LayerManager.activateLayer({
        obj: {
            id: "adultAuthPopup",
            type: Layer.TYPE.POPUP,
            priority: Layer.PRIORITY.POPUP,
            params: {
                type: type,
                callback: function () {
                    if (autoClose){
                        LayerManager.deactivateLayer({
                            id: "adultAuthPopup",
                            remove: true
                        });
                    }if(callback) callback();
                }
            }
        },
        moduleId: "module.family_home",
        visible: true
    });
};