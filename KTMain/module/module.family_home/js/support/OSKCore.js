OSKCore = (function(){
    //초성 자음
    var initial = [ 12593, 12594, 12596, 12599, 12600, 
                    12601, 12609, 12610, 12611, 12613, 
                    12614, 12615, 12616, 12617, 12618, 
                    12619, 12620, 12621, 12622 ];
    //모음
    var vowel = [ 12623, 12624, 12625, 12626, 12627,
                    12628, 12629, 12630, 12631, 12632,
                    12633, 12634, 12635, 12636, 12637,
                    12638, 12639, 12640, 12641, 12642,
                    12643 ];
    //종성 자음
    var finale = [ 0, 12593, 12594, 12595, 12596, 
                   12597, 12598, 12599, 12601, 12602, 
                   12603, 12604, 12605, 12606, 12607, 
                   12608, 12609, 12610, 12612, 12613, 
                   12614, 12615, 12616, 12618, 12619, 
                   12620, 12621, 12622 ];
    
    var dMedial = [ 0, 0, 0, 0, 0, 
                    0, 0, 0, 0, 800, 
                    801, 820, 0, 0, 1304, 
                    1305, 1320, 0, 0, 1820 ];
    var dFinale = [ 0, 0, 0, 119, 0, 
                    422, 427, 0, 0, 801, 
                    816, 817, 819, 825, 826, 
                    827, 0, 0, 1719, 0, 1919 ];
    
    var shiftJung = [12623, 12625, 12627, 12629, 12632, 12637];//ㅏ,ㅑ,ㅓ,ㅕ,ㅘ,ㅝ
    var shiftJungFinale = [1, 3, 5, 7, 10, 15];
    var SBase = 44032; //가
    var LBase = 4352; //ㄱ
    var VBase = 12623; //ㅏ
    var TBase = 4519;
    var LCount = 19; //초성 자음 개수, initial 
    var VCount = 21; //중성 갯수
    var TCount = 28; //자음 받침 개수, finale
    var NCount = 588; //21*28
    var SCount = 11172; //한글 조합 총 개수
    var PChar = 12643; //ㅣ
    
    function insertAtCaret(inputBox, chr) {
        inputBox.val(inputBox.val() + chr);
    }
    
    function deleteAtCaret(inputbox) {
        var num = inputbox.val() === "double"?2:1;
        var deleted = inputbox.val().length === 0?'':inputbox.val()[inputbox.val().length - num];
        inputbox.val(inputbox.val().substring(0, inputbox.val().length - num));
        return deleted;
    }

    function composeHangul (components) {
        if (components.length === 0) {
            return "";
        }
        var prev = components.charCodeAt(0);
        var prev_str = String.fromCharCode(prev);

        var next, k, l, j, f, c, m;
        for ( var i = 1; i < components.length; i++) {
            next = components.charCodeAt(i); 
            
            if(next === PChar){//모음 ㅣ가 붙는 경우
                var result = searchJungSung(prev);
                if(result.result){//받침 없는 경우
                    k = _$.inArray( result.jungChar,shiftJung);//ㅏ,ㅑ,ㅓ,ㅕ,ㅘ,ㅝ -> ㅐ,ㅒ,ㅔ,ㅖ,ㅙ,ㅞ
                    if(k !== -1){
                        var word = SBase + result.cho*NCount + shiftJungFinale[k]*TCount;
                        return String.fromCharCode(word);
                    }
                }
            }

            k = _$.inArray(prev,initial);
            if (k !== -1) {//자음만 있는 경우
                l = next - VBase;
                if (0 <= l && l < VCount) { //모음인 경우
                    prev = SBase + (k * VCount + l) * TCount;
                    prev_str = prev_str.slice(0, prev_str.length - 1) + String.fromCharCode(prev);
                    continue;
                }
            }
            
            m = prev - SBase; // 글자에서 '가'를 뺀다.
            if (0 <= m && m < 11145 && (m % TCount) === 0) { //한글이면서 받침이 없는 경우
                f = _$.inArray(next,finale);
                if (f !== -1) { //현재 문자가 받침인 경우
                    prev += f;//받침을 더한다
                    prev_str = prev_str.slice(0, prev_str.length - 1) + String.fromCharCode(prev);
                    continue;
                }
                //받침이 아닌 경우
                l = (m % NCount) / TCount; //받침 번호
                j = _$.inArray((l * 100) + (next - VBase),dMedial);
                if (j > 0) {
                    prev += (j - l) * TCount;
                    prev_str = prev_str.slice(0, prev_str.length - 1) + String.fromCharCode(prev);
                    continue;
                }
            }
            if (0 <= m && m < 11172 && (m % TCount) != 0) {
                f = m % TCount;
                l = next - VBase;
                if (0 <= l && l < VCount) {
                    k = _$.inArray(finale[f],initial);
                    if (0 <= k && k < LCount) {
                        prev_str = prev_str.slice(0, prev_str.length - 1) + String.fromCharCode(prev - f);
                        prev = SBase + (k * VCount + l) * TCount;
                        prev_str = prev_str + String.fromCharCode(prev);
                        continue;
                    }
                    if (f < dFinale.length && dFinale[f] != 0) {
                        prev_str = prev_str.slice(0, prev_str.length - 1) + String.fromCharCode(prev - f + Math.floor(dFinale[f] / 100));
                        prev = SBase + (_$.inArray(  finale[(dFinale[f] % 100)],initial) * VCount + l) * TCount;
                        prev_str = prev_str + String.fromCharCode(prev);
                        continue;
                    }
                }
                c = _$.inArray((f * 100) + _$.inArray(next,finale),dFinale);
                if (c > 0) {
                    prev = prev + c - f;
                    prev_str = prev_str.slice(0, prev_str.length - 1) + String.fromCharCode(prev);
                    continue;
                }
            }
            prev = next;
            prev_str = prev_str + String.fromCharCode(next);
        }
        return prev_str;
    }
    function decomposeHangul (c) {
        var h = c.length;
        var l = "";
        var d, k, j, e, f;
        for ( var g = 0; g < h; g++) {
            var d = c.charCodeAt(g);
            k = d - SBase;
            if (k < 0 || k >= SCount) {
                l = l + String.fromCharCode(d);
                continue;
            }
            j = initial[Math.floor(k / NCount)];
            e = VBase + (k % NCount) / TCount;
            f = finale[k % TCount];
            l = l + String.fromCharCode(j, e);
            if (f != 0) {
                /*
                 * 자음 : 유니코드에서 지원되는 자음 조합은 총 30개로 다음 표와 같다.
                            12593 ㄱ 12599 ㄷ 12605 ㄽ 12611 ㅃ 12617 ㅉ
                            12594 ㄲ 12600 ㄸ 12606 ㄾ 12612 ㅄ 12618 ㅊ
                            12595 ㄳ 12601 ㄹ 12607 ㄿ 12613 ㅅ 12619 ㅋ
                            12596 ㄴ 12602 ㄺ 12608 ㅀ 12614 ㅆ 12620 ㅌ
                            12597 ㄵ 12603 ㄻ 12609 ㅁ 12615 ㅇ 12621 ㅍ
                            12598 ㄶ 12604 ㄼ 12610 ㅂ 12616 ㅈ 12622 ㅎ
                    위의 내용중에서 KT OSK특성상 단/쌍자음 문자 입력을 고려해서 아래와 같이
                    한글자음을 쪼개서 return하게 수정함
                 */
                switch (f) {
                    case 12595 :    //ㄳ
                        l = l + String.fromCharCode(12593)+String.fromCharCode(12613);
                        break;
                    case 12597 :    // ㄵ
                        l = l + String.fromCharCode(12596)+String.fromCharCode(12616);
                        break;
                    case 12602 :    // ㄺ
                        l = l + String.fromCharCode(12601)+String.fromCharCode(12593);
                        break;
                    case 12604 :    // ㄼ
                        l = l + String.fromCharCode(12601)+String.fromCharCode(12610);
                        break;
                    case 12605 :    // ㄽ
                        l = l + String.fromCharCode(12601)+String.fromCharCode(12613);
                        break;
                    case 12612 :    // ㅄ
                        l = l + String.fromCharCode(12610)+String.fromCharCode(12613);
                        break;
                    default:
                        l = l + String.fromCharCode(f);
                        break;
                }
            }
        }
        return l;
    }
    
    function searchJungSung (chr_code){
        var result = new Object();
        var uni = chr_code - SBase;
        var jong = uni % TCount;
        if(jong > 0){ //받침 있음
            result.result = false;
            return result;
        }
        //받침 없음
        var jung = (uni/TCount)%VCount;
        var cho = parseInt(uni/TCount/VCount);
        var jungChar = vowel[jung]
        result.result = true;
        result.jungChar = jungChar;
        result.cho = cho;
        result.jung = jung;
        return result;
    }

    return {
        insertAtCaret:insertAtCaret,
        deleteAtCaret:deleteAtCaret,
        composeHangul:composeHangul,
        decomposeHangul:decomposeHangul
    };
})();

