/**
 * Created by ksk91_000 on 2016-11-25.
 */
window.ElementStack = function () {
    var stack = [];

    /**
     * 마지막에 push된 엘리먼트 아래에 엘리먼트를 추가한다.
     * @param element
     */
    var append = function (element) {
        if(stack.length<=0) stack.push(element);
        else stack.get(stack.length-1).append(element);
    };

    /**
     * 엘리먼트를 스택에 넣는다.
     * 이 뒤로 append 혹은 push를 하면 새로 추가된 element 하위에 붙는다.
     * @param element
     */
    var push = function (element) {
        stack.push(element);
    };

    /**
     * 한단계 위로 올라갈 때 호출.
     */
    var pop = function () {
        var element = stack.pop()
        append(element);
    };

    /**
     * 모든 오브젝트 추가가 끝나면 호출.
     */
    var end = function() {
        while(stack.length<=1) pop();
        var element = stack.pop()
        append(element);
        return element;
    };

    return {
        append: append,
        push: push,
        pop: pop,
        end: end
    }
};