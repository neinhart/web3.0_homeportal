/**
 * Created by ksk91_000 on 2016-10-31.
 */
window.openDetailLayer = function(cateId, contsId, reqPathCd, extraParams) {

    var a = ModuleManager.getModule("module.vod");
    if(!a) {
        LayerManager.startLoading();
        ModuleManager.loadModule( {
            moduleId: "module.vod",
            cbLoadModule: function (success) {
                if (success) openDetailLayer(cateId, contsId, reqPathCd, extraParams);
            }
        });
        return;
    }

    var params = {
        cat_id : cateId,
        const_id : contsId,
        req_cd :reqPathCd
    };

    for(var key in extraParams) params[key] = extraParams[key];
    LayerManager.stopLoading();

    a.execute({
        method: "showDetail",
        params: params
    })
};

window.openSearchLayer = function (schword) {
    var a = ModuleManager.getModule("module.search");
    if(!a) {
        ModuleManager.loadModule({
            moduleId: "module.search",
            cbLoadModule: function (success) {
                if(success) openSearchLayer(schword);
            }
        });
        return;
    }a.execute({
        method: "searchVOD",
        params: {
            srchword: schword
        }
    });
};