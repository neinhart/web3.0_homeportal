/**
 * Created by ksk91_000 on 2017-06-12.
 */
window.OpenByMenuObject = function (options) {
    if(!options.menu.parent && options.menu.catType=="SubHome") {
        var mainLayer = LayerManager.getLastLayerInStack({key: "id", value: "MyHomeMain"});
        if(options.bHotKey && mainLayer && mainLayer.isShowing()) {
            LayerManager.deactivateLayer({ id: "MyHomeMain" });
        } else LayerManager.activateLayer({
            obj: {
                id: "MyHomeMain",
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                params : {
                    menuData: options.menu,
                    focusIdx: options.focus
                }
            },
            moduleId:"module.family_home",
            clear_normal: options.jump,
            visible: true,
            skipRequestShow: options.skipRequestShow
        });
    } else {
        var layerId = "", params = {menuName : HTool.getMenuName(options.menu)}, isNew = false;
        switch (options.menu.id) {
            case "MENU00700":   //알림박스
                layerId = "MyHomeMailbox";
                break;
            case "MENU00608":  //공지사항
                params.focusIdx = 2;
                layerId = "MyHomeMailbox";
                break;
            case "MENU00710":  //알림
                params.focusIdx = 0;
                layerId = "MyHomeMailbox";
                break;
            case "MENU00711":  //이벤트
                params.focusIdx = 1;
                layerId = "MyHomeMailbox";
                break;
            case "MENU00601":  // 최근 시청목록
                RecentlyListManager.getRecentlyData(function (data) {
                    if(data.length==0) {
                        options.menu = getHomeMainData();
                        options.focus = 2;
                        OpenByMenuObject(options);
                    } else {
                        layerId = "MyHomeRecentlyList";
                        activateLayer();
                    }
                });
                return;
            case "MENU01200":  // 찜한 목록
                WishListManager.getWishListData(function (data) {
                    if(data.length==0) {
                        options.menu = getHomeMainData();
                        options.focus = 7;
                        OpenByMenuObject(options);
                    }else {
                        layerId = "MyHomeWishList";
                        activateLayer();
                    }
                });
                return;
            case "T000000000013":  // 즐겨찾는 메뉴
                LayerManager.activateLayer({
                    obj: {
                        id: "favorMenuPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: false,
                    },
                    moduleId: "module.family_home",
                    visible: true
                });
                return;
            case "INT00300":  // 마이메뉴 2
                options.menu = getHomeMainData();
                options.focus = 5;
                OpenByMenuObject(options);
                return;
            case "MENU00610": // 마이소장용VOD
                isNew = true;
                layerId = "MyHomeCloud";
                break;
            case "MENU00603": // 마이 플레이리스트
                layerId = "MyHomeMyPlayList";
                break;
            case "MENU00602": // 구매 내역 보기
                isNew = true;
                layerId = "MyHomeBuyList";
                break;
            case "MENU00611": // VOD구매목록
                params.focusIdx = 0;
                isNew = true;
                layerId = "MyHomeBuyList";
                break;
            case "MENU00609": // 모바일 공유목록
                params.focusIdx = 1;
                isNew = true;
                layerId = "MyHomeBuyList";
                break;
            case "MENU00612": // 무비초이스 구매목록
                params.focusIdx = 2;
                isNew = true;
                layerId = "MyHomeBuyList";
                break;
            case "MENU01400": // 가입정보"
                layerId = "MyHomeMyProduct";
                break;
            case "MENU07003":// 당신을 위한 VOD 추천
            case "MENU07004":// 나의 취향 알아보기
            case "MENU07005":// 내가 평가한 콘텐츠
                isNew = true;
                layerId = "MyHomeForYou";
                params.targetId = options.menu.id;
                break;
            default:
                if(options.menu.parent && options.menu.parent.catType==="SubHome") {
                    var mainData = options.menu.parent;
                    for(var i=0; i<mainData.children.length; i++) {
                        if(mainData.children[i].id === options.menu.id)  {
                            options.menu = mainData;
                            options.focus = i;
                            OpenByMenuObject(options);
                            return;
                        }
                    } return;
                } else if(options.menu.parent && options.menu.parent.catType==="MyMenu2") {
                    //마이메뉴 2 이하 편성메뉴
                    if(options.menu instanceof window.parent.VCategory) {
                        layerId = "MyHomeHelp";
                        params.catId = options.menu.id;
                    } else {
                        var mainData = options.menu.parent;
                        for(var i=0; i<mainData.children.length; i++) {
                            if(mainData.children[i].id === options.menu.id)  {
                                options.menu = getHomeMainData();
                                options.focus = i;
                                OpenByMenuObject(options);
                                return;
                            }
                        }
                    } return;
                } else return;
                break;
        }

        function activateLayer() {
            var layers = LayerManager.getLayers(layerId);
            for (var i = 0; i < layers.length; i++) {
                if (layers[i].isShowing() && layers[i].hasOwnProperty("hotKeyProcess") && layers[i].hotKeyProcess(options.keyCode, options.menu)) {
                    return;
                }
            }

            LayerManager.activateLayer({
                obj: {
                    id: layerId,
                    type: Layer.TYPE.NORMAL,
                    priority: Layer.PRIORITY.NORMAL,
                    linkage: false,
                    params: params
                },
                moduleId: "module.family_home",
                new: isNew,
                clear_normal: options.jump,
                visible: true
            });
        }

        function getHomeMainData() {
            return MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) { if (menu.catType == "SubHome" && !menu.parent) return true; }
            })[0];
        }

        activateLayer();
    }
};

window.goToMainLayer = function (focusIdx) {
    LayerManager.activateLayer({
        obj: {
            id: "MyHomeMain",
            type: Layer.TYPE.NORMAL,
            priority: Layer.PRIORITY.NORMAL,
            params : {
                menuData: MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(),
                        allSearch: false,
                        cbCondition: function (menu) { if (menu.catType == "SubHome" && !menu.parent) return true; }
                    })[0],
                focusIdx: focusIdx
            }
        },
        moduleId:"module.family_home",
        visible: true
    });
};