window.PosterFactory = new function() {
    this.getVerticalPoster = function(id, data) {
        var element = _$("<li id='" + id + "' class='content poster_vertical'>" +
            "<div class='content'>" +
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>" +
            "<img src='" + data.imgUrl + "?w=210&h=300&quality=90' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/image/default_poster.png\"'>"+
            "<div class='previewSdw'/>" +
            "<div class='contentsData'>" +
            "<div class='posterTitle'><span>" + data.contsName + "</span></div>" +
            "<span class='posterDetail'>" +
            "<span class='stars'/>" +
            (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/image/icon/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo - 0) + ".png' class='age'>" +
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>" +
            "</div>" +
            "</li>");

        element.find(".stars").append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(element.find(".stars"), data.mark);
        setIcon(element.find(".icon_area"), data);
        return element;
    };

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch (data.newHot) {
            case "N":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"}));
                break;
            case "U":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"}));
                break;
            case "X":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"}));
                break;
            case "B":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"}));
                break;
            case "R":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"}));
                break;
        }
        if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        }
        if (data.resolCd && data.resolCd.indexOf("UHD") >= 0) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        }
        if (data.isDvdYn == "Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        }
        if (data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        }
        if (data.adultOnlyYn != "Y" && UTIL.isLimitAge(data.prInfo)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.getVerticalPoster_more = function(id, data) {
        var html = _$("<li id='" + id + "' class='content poster_vertical more'>" +
            "<div class='content'>" +
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>" +
            "<div class='posterImgWrapper'>" +
            "<img src='" + data.imgUrl + "?w=196&h=280&quality=90' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/image/default_poster.png\"'>"+
            "</div>" +
            "<div class='contentsData'>" +
            "<span class='moreViewTxt'>전체보기</span>" +
            "<span>" +
            "<span class='moreViewCnt'>" + data.moreCnt + "</span>" +
            "</span>" +
            "</div>" +
            "</div>");
        return html;
    };

    this.getBanner_Big_1 = function(id, data) {
        var html = "<li id = '" + id + "' class='content banner_big_1'>" +
            "<div class='content'>" +
            "<img src='" + modulePath + "resource/img/sdw_banner_genre_movie.png' class='posterSdw'>" +
            "<img src='" + data.imgSrc + "' class='posterImg poster'>" +
            "<span class='posterTitle'>" + data.title + "</span>" +
            "<span class='posterDescription'>" + data.desc + "</span>" +
            "</div>" +
            "</li>"
        return html;
    }

    this.getBanner_Big_3 = function(id, data) {
        var html = "<li id = '" + id + "' class='content banner_big_3'>" +
            "<div class='content'>" +
            "<img src='" + modulePath + "resource/img/sdw_banner_genre_movie.png' class='posterSdw'>" +
            "<img src='" + modulePath + "resource/img/line_banner_movie_case2.png' class='posterImg'>" +
            "<img src='" + modulePath + "resource/img/sdw_category_poster.png' class='previewSdw'>" +
            "<img src='" + data.imgSrc + "' class='posterImg poster'>" +
            "<span class='posterTitle'>" + data.title + "</span>" +
            "<span class='posterDescription'>" + data.desc + "</span>" +
            "</div>" +
            "</li>"
        return html;
    }

    this.getBanner_Mini_1 = function(id, data) {
        var html = "<li id = '" + id + "' class='content banner_mini_1'>" +
            "<div class='content'>" +
            "<img src='" + modulePath + "resource/img/sdw_banner_genre_movie.png' class='posterSdw'>" +
            "<img src='" + data.imgSrc + "' class='posterImg poster'>" +
            "<span class='posterTitle'>" + data.title + "</span>" +
            "<span class='posterDescription'>" + data.desc + "</span>" +
            "</div>" +
            "</li>"
        return html;
    }

}