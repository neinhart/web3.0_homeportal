/**
 * Created by ksk91_000 on 2017-01-17.
 */
window.OSKModule = function (maxLength) {
    var div = _$("<div/>", {class: "OSKArea"});

    var focusX = 0;
    var focusY = 3;
    var doubleFocus = 0;
    var BTN_LEN = 0;

    var currentTextArray;
    var currentText;
    var inputLength;
    var inputMaxLen = maxLength||15;

    var inputBox;

    var focusOSK = 0;
    var onTextChagneListener;
    var onOverTextListener;
    var onCompleteInputListener;

    var arrKorCon = [["ㄱ", "ㄲ"], ["ㄴ"], ["ㄷ", "ㄸ"], ["ㄹ"], ["ㅁ"], ["ㅂ", "ㅃ"], ["ㅅ", "ㅆ"], ["ㅇ"], ["ㅈ", "ㅉ"], ["ㅊ"], ["ㅋ"], ["ㅌ"], ["ㅍ"], ["ㅎ"]];
    var arrKorVow = ["ㅏ", "ㅑ", "ㅓ", "ㅕ", "ㅗ", "ㅛ", "ㅜ", "ㅠ", "ㅡ", "ㅣ", "ㅐ", "ㅔ", "ㅒ", "ㅖ"];
    var arrEng_Cap = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    var arrEng = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    var arrNum = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
    var arrSpcText = [")", "?", "!", "%", "^", ":", ";", "~", "♥", "("];

    this.init = function (cbCreate) {
        currentTextArray = new Array();
        inputLength = 0;
        focusOSK = 0;
        focusX = 0;
        setComponent();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    this.focused = function (_focusY) {
        if(_focusY==null) focusY = 3;
        else focusY = _focusY;
        buttonFocusRefresh(focusX, focusY);
    };

    this.blurred = function () {
        div.find("#oskKorFocus_firstArea img").removeClass("focus");
        div.find("#oskKorFocus_secondArea img").removeClass("focus");
        div.find("#oskCapEngFocus_firstArea img").removeClass("focus");
        div.find("#oskCapEngFocus_secondArea img").removeClass("focus");
        div.find("#oskEngFocus_firstArea img").removeClass("focus");
        div.find("#oskEngFocus_secondArea img").removeClass("focus");
        div.find("#oskNumFocus_firstArea img").removeClass("focus");
        div.find("#oskNumFocus_secondArea img").removeClass("focus");
        div.find("#thirdArea img").removeClass("focus");
    };

    this.controlKey = function (key_code) {
        switch (key_code) {
            case KEY_CODE.UP:
                if (focusY == 3) {
                    if (focusOSK == 0) {
                        focusX = (focusX == 0 ? 0 : (focusX == 1 ? 5 : (focusX == 2 ? 8 : 11)));
                    } else if (focusOSK == 3) {
                        focusX = (focusX == 0 ? 0 : (focusX == 1 ? 3 : (focusX == 2 ? 6 : 9)));
                    } else {
                        focusX = (focusX == 0 ? 0 : (focusX == 1 ? 4 : (focusX == 2 ? 7 : 10)));
                    }
                }
                if (focusY == 1) {
                    if(focusOSK == 0 && arrKorCon[focusX].length == 2 && doubleFocus < 1) {
                        buttonDoubleFocusRefresh(focusX, doubleFocus);
                        doubleFocus++;
                    } else return false;
                } else buttonFocusRefresh(focusX, focusY-1);
                return true;
            case KEY_CODE.DOWN:
                if (focusY == 2) {
                    if (focusOSK == 0) {
                        focusX = ((focusX >= 0 && focusX < 5) ? 0 : (focusX < 8 ? 1 : (focusX < 11 ? 2 : 3)));
                    } else if (focusOSK == 3) {
                        focusX = ((focusX >= 0 && focusX < 3)?0:(focusX < 6?1:(focusX < 9?2:3)));
                    } else {
                        focusX = ((focusX >= 0 && focusX < 4)?0:(focusX < 7?1:(focusX < 10?2:3)));
                    }
                } if (focusY == 1 && arrKorCon[focusX].length == 2) {
                    if (doubleFocus > 0) {
                        buttonDoubleFocusRefresh(focusX, doubleFocus);
                        doubleFocus--;
                    } else {
                        buttonFocusRefresh(focusX, focusY+1);
                    }
                } else if (focusY==3) return false;
                else buttonFocusRefresh(focusX, HTool.indexPlus(focusY, 4));
                return true;
            case KEY_CODE.LEFT:
                buttonFocusRefresh(HTool.getIndex(focusX, -1, BTN_LEN), focusY);
                return true;
            case KEY_CODE.RIGHT:
                buttonFocusRefresh(HTool.getIndex(focusX, 1, BTN_LEN), focusY);
                return true;
            case KEY_CODE.ENTER:
                return enterKorKeyboard();
            case KEY_CODE.RED:
            case KEY_CODE.DEL:
                deleteInputText();
                return true;
            case KEY_CODE.BLUE:
                sumTextString(" ");
                return true;
            case KEY_CODE.PLAY:
                if(onCompleteInputListener) onCompleteInputListener();
                return true;
            case KEY_CODE.CHANGE_CHAR:
            case 122:
                changeOSKBoardImage(HTool.getIndex(focusOSK, 1, 4));
                buttonFocusRefresh(focusX=0, focusY=1);
                return true;
            default:
                return false;
        }
    };

    function enterKorKeyboard() {
        switch (focusY) {
            case 0:
                if(onCompleteInputListener) onCompleteInputListener();
                return true;
            case 1:
                switch (focusOSK) {
                    case 0:
                        if (arrKorCon[focusX].length == 2) {
                            sumTextString(arrKorCon[focusX][doubleFocus]);
                        } else {
                            sumTextString(arrKorCon[focusX][0]);
                        }
                        break;
                    case 1:
                        sumTextString(arrEng[focusX]);
                        break;
                    case 2:
                        sumTextString(arrEng_Cap[focusX]);
                        break;
                    case 3:
                        sumTextString( arrNum[focusX]);
                        break;
                }
                return true;
            case 2:
                switch (focusOSK) {
                    case 0:
                        sumTextString(arrKorVow[focusX]);
                        break;
                    case 1:
                        sumTextString(arrEng[focusX + 13]);
                        break;
                    case 2:
                        sumTextString(arrEng_Cap[focusX + 13]);
                        break;
                    case 3:
                        sumTextString(arrSpcText[focusX]);
                        break;
                }
                return true;
            case 3:
                switch (focusX) {
                    case 0:
                        if (inputLength == 0) return false;
                        else if(onCompleteInputListener) onCompleteInputListener();
                        break;
                    case 1:
                        deleteInputText();
                        break;
                    case 2:
                        sumTextString('\u00A0');
                        break;
                    case 3:
                        changeOSKBoardImage(HTool.getIndex(focusOSK, 1, 4));
                        break;
                }
                return true;
        }
    }

    function changeOSKBoardImage(index) {
        focusOSK = index;
        switch (index) {
            case 0:
                div.find("#oskBoard_img").attr("src", modulePath + "resource/image/osk/osk_home_kor_h.png");
                break;
            case 1:
                div.find("#oskBoard_img").attr("src", modulePath + "resource/image/osk/osk_home_eng_s_h.png");
                break;
            case 2:
                div.find("#oskBoard_img").attr("src", modulePath + "resource/image/osk/osk_home_eng_l_h.png");
                break;
            case 3:
                div.find("#oskBoard_img").attr("src", modulePath + "resource/image/osk/osk_home_num_h.png");
                break;
        }
    }

    function buttonDoubleFocusRefresh(index_X, index_Y) {
        if (index_Y == 0) {
            div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").attr("src", modulePath + "resource/image/osk/key_kor_" + (index_X + 1) + "_2_h.png");
        } else {
            div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").attr("src", modulePath + "resource/image/osk/key_kor_" + (index_X + 1) + "_1_h.png");
        }
    }

    function buttonFocusRefresh(index_X, index_Y) {
        focusY = index_Y;
        div.find("#oskKorFocus_firstArea img").removeClass("focus");
        div.find("#oskKorFocus_secondArea img").removeClass("focus");
        div.find("#oskCapEngFocus_firstArea img").removeClass("focus");
        div.find("#oskCapEngFocus_secondArea img").removeClass("focus");
        div.find("#oskEngFocus_firstArea img").removeClass("focus");
        div.find("#oskEngFocus_secondArea img").removeClass("focus");
        div.find("#oskNumFocus_firstArea img").removeClass("focus");
        div.find("#oskNumFocus_secondArea img").removeClass("focus");
        div.find("#thirdArea img").removeClass("focus");
        switch (index_Y) {
            case 0:
                break;
            case 1:
                switch (focusOSK) {
                    case 0:
                        focusX = index_X;
                        BTN_LEN = arrKorVow.length;
                        if (arrKorCon[focusX].length == 2) {
                            doubleFocus = 0;
                            div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").attr("src", modulePath + "resource/image/osk/key_kor_" + (index_X + 1) + "_1_h.png");
                        }
                        div.find("#oskKorFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                    case 1:
                        focusX = index_X;
                        BTN_LEN = 13;
                        div.find("#oskEngFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                    case 2:
                        focusX = index_X;
                        BTN_LEN = 13;
                        div.find("#oskCapEngFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                    case 3:
                        focusX = index_X;
                        BTN_LEN = arrNum.length;
                        div.find("#oskNumFocus_firstArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                }
                break;
            case 2:
                switch (focusOSK) {
                    case 0:
                        focusX = index_X;
                        BTN_LEN = arrKorVow.length;
                        div.find("#oskKorFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                    case 1:
                        focusX = index_X;
                        BTN_LEN = 13;
                        div.find("#oskEngFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                    case 2:
                        focusX = index_X;
                        BTN_LEN = 13;
                        div.find("#oskCapEngFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                    case 3:
                        focusX = index_X;
                        BTN_LEN = arrSpcText.length;
                        div.find("#oskNumFocus_secondArea img:eq(" + index_X + ")").addClass("focus");
                        break;
                }
                break;
            case 3:
                focusX = index_X;
                BTN_LEN = 4;
                div.find("#thirdArea img:eq(" + index_X + ")").addClass("focus");
                break;
        }
    }

    function setComponent() {
        div.append("<div id='IMEBoardArea'>" +
            "<img id='oskBoard_img' src='" + modulePath + "resource/image/osk/osk_home_kor_h.png'>" +
            "<img id='defaultBtn_img' src='" + modulePath + "resource/image/osk/osk_btn_default.png'>" +
            "<div id='firstArea'>" +
            "<div id='oskKorFocus_firstArea'></div>" +
            "<div id='oskCapEngFocus_firstArea'></div>" +
            "<div id='oskEngFocus_firstArea'></div>" +
            "<div id='oskNumFocus_firstArea'></div>" +
            "</div>" +
            "<div id='secondArea'>" +
            "<div id='oskKorFocus_secondArea'></div>" +
            "<div id='oskCapEngFocus_secondArea'></div>" +
            "<div id='oskEngFocus_secondArea'></div>" +
            "<div id='oskNumFocus_secondArea'></div>" +
            "</div>" +
            "<div id='thirdArea'>" +
            "<img id='enterBtn_f_img' class='focusBtn' src='" + modulePath + "resource/image/osk/key_enter_h.png'>" +
            "<img id='deleteBtn_f_img' class='focusBtn' src='" + modulePath + "resource/image/osk/key_delete_h.png'>" +
            "<img id='spaceBtn_f_img' class='focusBtn' src='" + modulePath + "resource/image/osk/key_space_h.png'>" +
            "<img id='oskChangeBtn_f_img' class='focusBtn' src='" + modulePath + "resource/image/osk/key_change_h.png'>" +
            "</div>" +
            "</div>");
        setOSKFocusComponent();
    }

    function setOSKFocusComponent() {
        var html = "";
        for (var idx = 1; idx < 15; idx++) {
            if (arrKorCon[idx - 1].length == 2) {
                html += "<img class='korFocus_img' src='" + modulePath + "resource/image/osk/key_kor_" + idx + "_1_h.png'>";
            } else {
                html += "<img class='korFocus_img' src='" + modulePath + "resource/image/osk/key_kor_" + idx + "_h.png'>";
            }
        }
        div.find("#oskKorFocus_firstArea").append(html);
        html = "";
        for (var idx = 15; idx < 29; idx++) {
            html += "<img class='korFocus_img' src='" + modulePath + "resource/image/osk/key_kor_" + idx + "_h.png'>";
        }
        div.find("#oskKorFocus_secondArea").append(html);
        html = "";
        for (var idx = 0; idx < 13; idx++) {
            html += "<img class='capEngFocus_img' src='" + modulePath + "resource/image/osk/key_eng_l_" + (idx + 1) + "_h.png'>";
        }
        div.find("#oskCapEngFocus_firstArea").append(html);
        html = "";
        for (var idx = 13; idx < arrEng_Cap.length; idx++) {
            html += "<img class='capEngFocus_img' src='" + modulePath + "resource/image/osk/key_eng_l_" + (idx + 1) + "_h.png'>";
        }
        div.find("#oskCapEngFocus_secondArea").append(html);
        html = "";
        for (var idx = 0; idx < 13; idx++) {
            html += "<img class='engFocus_img' src='" + modulePath + "resource/image/osk/key_eng_s_" + (idx + 1) + "_h.png'>";
        }
        div.find("#oskEngFocus_firstArea").append(html);
        html = "";
        for (var idx = 13; idx < arrEng.length; idx++) {
            html += "<img class='engFocus_img' src='" + modulePath + "resource/image/osk/key_eng_s_" + (idx + 1) + "_h.png'>";
        }
        div.find("#oskEngFocus_secondArea").append(html);

        html = "";
        for (var idx = 1; idx < arrNum.length; idx++) {
            html += "<img class='numFocus_img' src='" + modulePath + "resource/image/osk/key_num_" + idx + "_h.png'>";
        }
        html += "<img class='numFocus_img' src='" + modulePath + "resource/image/osk/key_num_0_h.png'>";
        div.find("#oskNumFocus_firstArea").append(html);
        html = "";
        for (var idx = 0; idx < arrSpcText.length; idx++) {
            html += "<img class='numFocus_img' src='" + modulePath + "resource/image/osk/key_num_" + (idx + 11) + "_h.png'>";
        }
        div.find("#oskNumFocus_secondArea").append(html);
    }

    // 한글 조합
    function sumTextString(arrayPush) {
        var sumTextStr = "";

        if (currentText.length >= inputMaxLen) {
            if(onOverTextListener) onOverTextListener();
            return currentText;
        }

        var components = OSKCore.deleteAtCaret(inputBox) + arrayPush;
        var compound = OSKCore.composeHangul(components);
        OSKCore.insertAtCaret(inputBox, compound);
        sumTextStr = inputBox.val();

        currentText = sumTextStr;
        onTextChagneListener(currentText);
        return currentText;
    }

    function deleteInputText() {
        if (!currentText || currentText.length==0) {
            return "";
        }

        if (inputLength > inputMaxLen) {
            inputLength = inputMaxLen;
        } return deleteWordTextString();
    }

    function deleteWordTextString() {
        // 한글자씩 지우기
        if (!currentText || currentText.length==0) {
            return "";
        }

        var compound = OSKCore.deleteAtCaret(inputBox);
        var components = OSKCore.decomposeHangul(compound);
        if (components.length > 1) OSKCore.insertAtCaret(inputBox, OSKCore.composeHangul(components.slice(0, -1)));

        currentText = inputBox.val();
        onTextChagneListener(currentText);
        return currentText;
    }

    this.setText = function (text) {
        if (text.length > inputMaxLen) {
            if(onOverTextListener) onOverTextListener();
        } else {
            inputBox.val("");
            var sumTextStr = "";
            var components = OSKCore.deleteAtCaret(inputBox) + text;
            var compound = OSKCore.composeHangul(components);
            OSKCore.insertAtCaret(inputBox, compound);
            currentText = inputBox.val();
        } onTextChagneListener(currentText);
    };

    this.getText = function () {
        return currentText;
    };

    this.getView = function(components) {
        inputBox = components;
        return div;
    };

    this.getCharMode = function () {
        return focusOSK;
    };

    this.changeCharMode = function (mode) {
        changeOSKBoardImage(mode);
    };

    this.setOnTextChangeListener = function(_onTextChagneListener) {
        onTextChagneListener = _onTextChagneListener;
    };

    this.setOverTextListener = function(_onOverTextListener) {
        onOverTextListener = _onOverTextListener;
    };

    this.setOnCompleteInputListener = function (_onCompleteInputListener) {
        onCompleteInputListener = _onCompleteInputListener;
    }
};