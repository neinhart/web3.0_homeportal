/**
 * 찜한 목록 (우리집 맞춤 Tv -> 찜한 목록 -> 전체보기)
 * Created by ksk91_000 on 2016-11-08.
 */
(function() {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_wishList = function HomeWishList(options) {
        Layer.call(this, options);
        var contentList = [];
        var focus = 0;
        var instance = this;
        var indicator;
        var curPage = 0;

        // 0 : 콘텐츠 focus, 1 : contextMenu focus
        var focusMode = 0;

        // false : Non 편집,  true : 편집 모드
        var editMode = false;

        var editFocus = 0;
        var contextMenu;
        var isGanada = false;

        // 한 page 당 표시 되는 poster의 최대 갯수
        var pageMaxCnt = 10;

        // 한 줄에 표시할 수 있는 poster의 갯수
        var rowItemCnt = pageMaxCnt / 2;

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_recentlyList"});

            this.div.html("<div class='background'>" +
                "<span id='backgroundTitle'>찜한 목록</span>" +
                "</div>" +
                "<div id='contentsArea'></div>" +
                "<div id='indicatorArea'></div>" +
                "<div class='context_menu_indicator' style='z-index: 102'>찜 해제</div>" +
                "<div id='sdw_mask'></div>"
            );

            contextMenu = new ContextMenu_editMode();
            contextMenu.addTitle("정렬 옵션");
            contextMenu.addDropBox(["최근 등록순", "가나다순"]);
            contextMenu.addSeparateLine();
            contextMenu.addButton("편집 모드", "option_menu_current_mode");
            contextMenu.addButton("전체 선택", "option_menu_select_all");
            contextMenu.setEventListener(contextMenuEventListener);
            // 전체 선택 버튼 disable
            contextMenu.setDisable(2, false);
            contextMenu.open();
            this.div.append(contextMenu.getView());

            indicator = new Indicator(1);
            cbCreate(true);
        };

        this.show = function (options) {
            editMode = false;
            this.div.toggleClass('editMode', editMode);
            if(this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);
            Layer.prototype.show.call(this);

            LayerManager.showVBOBackground("myHome_main_background");
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            WishListManager.getWishListData(function (data) {
                contentList = data;
                contextMenu.setDropBoxFocus(2, isGanada?1:0);
                if(isGanada) contextMenuEventListener(2, 1);
                checkChgFlag(function () {
                    constructHTML();
                    setFocus((options && options.resume) ? focus : 0);
                });
            });
        };

        this.hide = function(options) {
            Layer.prototype.hide.call(this);
            //mainclock.hide();
            LayerManager.hideVBOBackground();
            //setFocusMode(0);
            resetFocus();

            UTIL.clearAnimation(instance.div.find(".textAnimating span"));
            instance.div.find(".textAnimating").removeClass("textAnimating");
            if(!options || !options.pause) isGanada = false;
        };

        function resetFocus() {
            focusMode = 0;
            editMode = false;
            editFocus = 0;
            focus = 0;

            // contextMenu focus 해제
            contextMenu.removeFocus();
            contextMenu.clearDropBoxFocus();

            // contextMenu text 초기화
            instance.div.find(".option_menu_select_all").text("전체 선택");
            instance.div.find(".option_menu_current_mode").text("편집 모드");

            // 콘텐츠 focus, select 해제
            instance.div.find("li.content").removeClass("select");
            instance.div.find("li.content").removeClass("focus");
        }

        function checkChgFlag(callback) {
            var chgList = [];
            for(var i in contentList)
                if(contentList[i].chgFlag=="D") chgList.push(contentList[i]);

            if(chgList.length>0) {
                WishListManager.removeContents(chgList, function (res) {
                    if(res) {
                        WishListManager.getWishListData(function (data) {
                            contentList = data;
                            contextMenu.setDropBoxFocus(2, isGanada?1:0);
                            if(isGanada) contextMenuEventListener(2, 1);
                            showAlert("편성 정보 삭제로 인해 " + chgList.length + "개의 콘텐츠가 자동 삭제되었습니다", "찜한 목록 삭제 안내");
                            callback();
                        });
                    } callback();
                })
            } else callback();
        }

        function setFocus(newFocus) {
            if(newFocus>=contentList.length) newFocus = contentList.length-1;
            setFocusMode(0);
            setPage(Math.floor(newFocus / pageMaxCnt));

            instance.div.find("#recently_item_" + focus).removeClass("focus");
            focus = newFocus;
            instance.div.find("#recently_item_" + focus).addClass("focus");

            if(contentList[focus]) changeBackground(contentList[focus].imgUrl + "?w=196&h=280&quality=90", 500);
            setTextAnimation(focus);
        }

        function setTextAnimation(focus) {
            UTIL.clearAnimation(instance.div.find(".textAnimating span"));
            instance.div.find(".textAnimating").removeClass("textAnimating");
            void instance.div[0].offsetWidth;
            if(UTIL.getTextLength(contentList[focus].itemName, "RixHead L", 30)>215) {
                var posterDiv = instance.div.find("#recently_item_" + focus + " .posterTitle");
                posterDiv.addClass("textAnimating");
                UTIL.startTextAnimation({
                    targetBox: posterDiv
                });
            }
        }

        var bg_timer, curBgIdx = 0;
        var changeBackground = function (src, time) {
            var bgArea = VBOBackground;
            if(bg_timer) clearTimeout(bg_timer);
            bg_timer = setTimeout(function () {
                if(src==null) {
                    bgArea.find("#background_blur").css("display", "none");
                    bgArea.find("#background_blur .bg_wrapper").css("opacity", "");
                }else {
                    bgArea.find("#background_blur").css("display", "");
                    var bg = bgArea.find("#background_blur .bg_wrapper");
                    bg.eq(curBgIdx).css("opacity", 0);
                    curBgIdx ^= 1;
                    bg.find(".bg").eq(curBgIdx).css({"background-image": "url(" + src + ")"});
                    bg.eq(curBgIdx).css("opacity", 1);
                } bg_timer = null;
            }, time);
        };

        // mode : 0 -> 콘텐츠 focus
        // mode : 1 -> context focus
        function setFocusMode(mode) {
            if(focusMode==mode) return;

            var selectedCnt = instance.div.find("li.content.select").length;

            focusMode = mode;
            instance.div.find("#recently_item_" + focus).toggleClass("focus", mode==0);
            if(mode==0) {
                contextMenu.removeFocus();

                // 콘텐츠에 focus가 위치 시 찜 해제 버튼 show
                instance.div.find(".context_menu_indicator").show();
                // 적/청색 page show
                indicator.togglePageTransIcon(false);
            } else {
                if(selectedCnt > 0) {
                    contextMenu.addFocus(1);
                } else {
                    contextMenu.addFocus(0);
                }

                // context에 focus가 된 경우에
                // 선택 된 콘텐츠 수에 따라 찜 해제 버튼 show/hide
                if (selectedCnt > 0) {
                    instance.div.find(".context_menu_indicator").show();
                } else {
                    instance.div.find(".context_menu_indicator").hide();
                }

                // 적/청색 hide
                indicator.togglePageTransIcon(true);
            }
        }

        var scrollTimer;

        function setPage(page) {
            var isPageLoad = false;
            if(curPage != page) isPageLoad = true;

            curPage = page;
            instance.div.find("#listArea").css("-webkit-transform", 'translateY(-' + page * 908 + "px)");
            indicator.setPos(page);
            if(isPageLoad) {
                instance.div.find(".content.poster_vertical").css("visibility", "");

                if(scrollTimer) clearTimeout(scrollTimer);
                scrollTimer = setTimeout(function () {
                    for (var i = 0; i < page * pageMaxCnt; i++) instance.div.find("#recently_item_" + i).css("visibility", "hidden");
                }, 300);
            } else {
                for (var i = 0; i < page * pageMaxCnt; i++) instance.div.find("#recently_item_" + i).css("visibility", "hidden");
            }
        }

        function constructHTML() {
            var html = _$("<div/>", {id: 'listArea'});
            for (var i = 0; i < contentList.length; i++) {
                html.append(getVerticalPoster("recently_item_" + i, contentList[i]));
            }

            indicator.setSize(Math.floor((contentList.length-1)/pageMaxCnt)+1);
            instance.div.find("#indicatorArea").append(indicator.getView()).append(indicator.getPageTransView());
            instance.div.find("#contentsArea").html(html);
            instance.div.toggleClass("singlePage", Math.ceil(contentList.length/pageMaxCnt)==1);
        }

        function enterKeyAction() {
            if(focusMode == 0) {
                if(editMode) {
                    instance.div.find("#recently_item_" + focus).toggleClass("select");
                } else {
                    if(contentList[focus].chgFlag=="D") {
                        WishListManager.removeContents([contentList[focus]], function () {
                            showAlert("편성정보 변경으로 인해 삭제된 콘텐츠 입니다", "찜한 목록 삭제 안내");
                        });
                    }else {
                        if(contentList[focus].adultOnlyYn=="Y") {
                            openAdultAuthPopup(function () {
                                openDetailLayerImpl(contentList[focus], true);
                            }, true, "성인인증 비밀번호")
                        } else {
                            openDetailLayerImpl(contentList[focus], false);
                        }
                    }
                }
            } else if(editFocus == 0) {
                if(instance.div.find("li.content.select").length==contentList.length) {
                    instance.div.find("li.content").removeClass("select");
                } else {
                    instance.div.find("li.content").addClass("select");
                }
            } else if(editFocus == 1) {
                // [si.mun] editFocus 를 수정하는 곳이 없는데.. 이게 왜있는 지 모르겠음
                //var deleteList = [];
                //var tmp = instance.div.find("li.content");
                //for(var i=0; i<tmp.length; i++) {
                //    if(tmp.eq(i).hasClass("select")) deleteList.push(contentList[i]);
                //} openConfirmPopup(deleteList);
            }

            if(editMode) {
                instance.div.find(".option_menu_select_all").text(instance.div.find("li.content.select").length==contentList.length?"전체 선택 해제":"전체 선택");
            }

            // 선택 된 콘텐츠 수에 따라 찜 해제 버튼 show/hide
            if (instance.div.find("li.content.select").length > 0) {
                instance.div.find(".context_menu_indicator").show();
            } else {
                instance.div.find(".context_menu_indicator").hide();
            }

        }

        this.onKeyAction = function(keyCode) {
            if(focusMode==1) { // contextMenu focus
                if(contextMenu.onKeyAction(keyCode)) return true;
                else if(keyCode==KEY_CODE.LEFT){
                    setFocus(Math.min(focus - focus%rowItemCnt + rowItemCnt - 1, contentList.length-1));
                    return true;
                } else if(keyCode==KEY_CODE.BACK) {
                    return LayerManager.historyBack(), true;
                } else if(keyCode==KEY_CODE.CONTEXT) { // 찜 해제
                    openConfirmPopup();
                    return true;
                }
            } switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(focusMode==0 && focus%rowItemCnt==0){ // 콘텐츠 focus && 최 자측 focus
                        instance.div.find("#recently_item_" + focus).removeClass("focus");
                        return LayerManager.historyBack(), true;
                    } else {
                        setFocus(HTool.getIndex(focus, -1, contentList.length));
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (focus==contentList.length-1||focus%rowItemCnt==rowItemCnt - 1) { // 최우측 콘텐츠
                        setFocusMode(1);
                    } else {
                        setFocus(HTool.getIndex(focus, 1, contentList.length));
                    }
                    return true;
                case KEY_CODE.UP :
                    if ((contentList.length % rowItemCnt != 0) && focus % rowItemCnt == focus) setFocus(Math.min(Math.floor(contentList.length / rowItemCnt) * rowItemCnt + focus % rowItemCnt, contentList.length - 1));
                    else if (contentList.length % rowItemCnt == 0 && focus < rowItemCnt && contentList.length > rowItemCnt) setFocus((contentList.length - rowItemCnt) + (focus % rowItemCnt));
                    else if (contentList.length > rowItemCnt)setFocus(focus-rowItemCnt);
                    return true;
                case KEY_CODE.DOWN :
                    if ((contentList.length % rowItemCnt != 0) && Math.floor(focus / rowItemCnt) == Math.floor(contentList.length / rowItemCnt)) setFocus(focus % rowItemCnt);
                    else if (contentList.length % rowItemCnt == 0 && contentList.length > rowItemCnt && focus >= contentList.length - rowItemCnt && focus < contentList.length) setFocus(focus % rowItemCnt);
                    else if (contentList.length > rowItemCnt)setFocus(Math.min(focus+rowItemCnt, contentList.length-1));
                    return true;
                case KEY_CODE.RED:
                    if(editMode && focusMode == 1) return true;

                    if(focus%pageMaxCnt === 0) setFocus(HTool.getIndex(curPage, -1, Math.ceil(contentList.length / pageMaxCnt))*pageMaxCnt);
                    else setFocus(curPage*pageMaxCnt);
                    return true;
                case KEY_CODE.BLUE:
                    if(editMode && focusMode == 1) return true;

                    if(focus === contentList.length-1) setFocus(Math.min(contentList.length-1, pageMaxCnt - 1));
                    else if(focus%pageMaxCnt === pageMaxCnt - 1) setFocus(Math.min(contentList.length-1, HTool.getIndex(curPage, 1, Math.ceil(contentList.length / pageMaxCnt))*pageMaxCnt + pageMaxCnt - 1));
                    else setFocus(Math.min(curPage*pageMaxCnt+pageMaxCnt - 1));
                    return true;
                case KEY_CODE.ENTER :
                    enterKeyAction();
                    return true;
                case KEY_CODE.BACK :
                    return LayerManager.historyBack(), true;
                case KEY_CODE.CONTEXT:
                    openConfirmPopup();
                    return true;
                default:
                    return false;
            }
        };

        /**
         * 연관메뉴(편집모드) 이벤트 리스너
         * @param index Menu Index(Button, Dropbox 등)
         * @param data Event Data(Dropbox Select Index 등)
         */
        function contextMenuEventListener(index, data) {
            if(index == 0) { // 최근 등록 순 dropBox
                toggleEditMode(false);
                if(data==0) {
                    isGanada = false;
                    WishListManager.getWishListData(function (data) {
                        contentList = data;
                        checkChgFlag(function () {
                            constructHTML();
                            setFocus((options&&options.resume)?focus:0);
                        });
                    });
                } else if(data==1) {
                    isGanada = true;
                    contentList = [].concat(contentList);
                    contentList.sort(function (a, b) { return a.itemName > b.itemName ? 1 : -1; });
                    checkChgFlag(function () {
                        constructHTML();
                        setFocus((options&&options.resume)?focus:0);
                    });
                }
            } else if(index == 1) { // 편집 모드 ON/OFF
                if (editMode) {
                    // 편집 모드 해제
                    toggleEditMode(false);
                    instance.div.find(".context_menu_indicator").hide();
                } else {
                    // 편집 모드
                    toggleEditMode(true);
                }
            } else if(index==2){ // 전체 선택
                var selectedCnt = instance.div.find("li.content.select").length;

                if(selectedCnt == contentList.length) {
                    instance.div.find("li.content").removeClass("select");
                    instance.div.find(".option_menu_select_all").text("전체 선택");
                    instance.div.find(".context_menu_indicator").hide();
                } else {
                    instance.div.find("li.content").addClass("select");
                    instance.div.find(".option_menu_select_all").text("전체 선택 해제");
                    instance.div.find(".context_menu_indicator").show();
                }
            }
        }

        function openConfirmPopup() {
            var deleteList = [];

            // 1. 편집 모드 일 땐 select class를 보고 목록에 추가
            // 2. 편집 모드가 아닌 경우, 콘텐츠에 focus가 위치하여도 찜 해제가 가능 하므로
            //    deleteList 에 현재 focus 가 위치한 콘텐츠만 push
            if (editMode) {
                var tmp = instance.div.find("li.content");
                for(var i=0; i<tmp.length; i++) {
                    if(tmp.eq(i).hasClass("select")) deleteList.push(contentList[i]);
                }
            } else {
                deleteList.push(contentList[focus]);
            }

            LayerManager.activateLayer({
                obj: {
                    id:"EditModeDeletePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        type: 0,
                        items: deleteList,
                        callback: function(res) {
                            if(res) {
                                WishListManager.removeContents(deleteList, function (result) {
                                    WishListManager.getWishListData(function (data) {
                                        if(data.length>0) {
                                            contentList = data;
                                            contextMenu.setDropBoxFocus(2, isGanada?1:0);
                                            if(isGanada) contextMenuEventListener(2, 1);
                                            checkChgFlag(function () {
                                                showToast("찜한 목록에서 삭제되었습니다");
                                                constructHTML();
                                                // focus = curPage * 10;
                                                focus = 0;
                                                setFocus(focus);
                                                toggleEditMode(false);
                                            });
                                        }else LayerManager.deactivateLayer({id:instance.id});
                                    }, true);
                                });
                            } LayerManager.historyBack();
                        }
                    }
                },
                moduleId: "module.family_home",
                visible: true
            });
        }

        var toggleEditMode = function(mode) {
            editMode = mode!=null?mode:editMode^true;

            // editMode toggle
            instance.div.toggleClass('editMode', editMode);

            if(editMode) { // 편집 모드 ON
                // 전체 선택 disable 해제
                contextMenu.setDisable(2, true);
                instance.div.find(".option_menu_current_mode").text("편집 모드 종료");
            } else { // 편집 모드 OFF
                // 전체 선택 disable
                contextMenu.setDisable(2, false);
                instance.div.find(".option_menu_current_mode").text("편집 모드");
            }

            // 모드가 바뀔 때 마다 select 콘텐츠 해제 및 전체선택으로 text 변경
            instance.div.find("li.content.select").removeClass("select");
            instance.div.find(".option_menu_select_all").text("전체 선택");
        };

        function openDetailLayerImpl(item, authComplete) {
            try { NavLogMgr.collectFHMenu(NLC.JUMP_START_SUBHOME, NLC.MENU01200.id, NLC.MENU01200.name, item.itemId, item.itemName); } catch(e) {}

            var catId = item.itemType == 2 ? item.categoryId:item.itemId;
            if(!catId || catId==="N" || catId==="X") {
                openDetailLayer(catId, item.itemId, "16", {isAuthorizedContent: authComplete});
            }else myHome.amocManager.getCateInfoW3(function (res, catData) {
                if(res) {
                    if(!catData.catId) {
                        myHome.amocManager.getCategoryId("contsId=" + catId + "&saId=" + DEF.SAID + "&path=V&platformGubun=W", function (res, newData) {
                            if(res) {
                                if(newData.serviceYn=="Y") openDetailLayer(newData.categoryId, item.itemId, "16", {isAuthorizedContent: authComplete});
                                else HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                            } else {
                                extensionAdapter.notifySNMPError("VODE-00013");
                                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                            }
                        });
                    } else {
                        if(catData.catType!=="PkgVod" && catData.seriesYn!=="Y") {
                            myHome.amocManager.getContentW3(function (res, contData) {
                                if(res) {
                                    // if(contData.contsId) openDetailLayer(item.itemId, item.itemId, "16", {cateInfo:catData, contsInfo: contData});
                                    // 2017.10.13 Lazuli
                                    // 카테고리ID를 넘겨야되는데 itemId를 넘기고있어서 찜목록에서 진입한 컨텐츠들 중 연관메뉴의 최근 확인한 VOD에서 진입이 안되는 오류가 있음
                                    // (최근 확인한 VOD에 같은 컨텐츠가 2개가 되는 경우도 생김)
                                    // 다른 페이지의 openDetailLayerImpl 함수 참고해서 카테고리ID를 넘기도록 수정함
                                    if(contData.contsId) openDetailLayer(catId, item.itemId, "16", {cateInfo:catData, contsInfo: contData, isAuthorizedContent: authComplete});
                                    else openDetailLayer("N", item.itemId, "16", {isAuthorizedContent: authComplete});
                                } else {
                                    extensionAdapter.notifySNMPError("VODE-00013");
                                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                                }
                            }, catId, item.itemId, DEF.SAID);
                        } else {
                            openDetailLayer(catId, item.itemId, "16", {cateInfo:catData, isAuthorizedContent: authComplete});
                        }
                    }
                } else {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                }
            }, catId, DEF.SAID);
        }

        var getVerticalPoster = function(id, data) {
            var element = _$("<li id='" + id + "' class='content poster_vertical'>"+
                "<div class='content'>"+
                "<div class='focus_red'><div class='black_bg'></div></div>" +
                "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>"+
                "<img src='" + data.imgUrl + "?w=210&h=300&quality=90' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/image/default_poster.png\"'>"+
                "<div class='previewSdw'/>"+
                "<div class='contentsData'>"+
                "<div class='posterTitle'><span>" + data.itemName + "</span></div>"+
                "<span class='posterDetail'>"+
                "<span class='stars'/>"+
                (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
                "<img src='" + modulePath + "resource/image/icon/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo-0) + ".png' class='age'>"+
                "</span>"+
                "</div>"+
                "<div class='icon_area'/>" +
                "<div class='edit_select_btn'></div>" +
                "</div>"+
                "</li>");

            element.find(".stars").append(stars.getView(stars.TYPE.RED_20));
            stars.setRate(element, data.mark);
            setIcon(element.find(".icon_area"), data);
            return element;
        };

        function setIcon(div, data) {
            div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
            switch(data.newHot) {
                case "N": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
                case "U": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"})); break;
                case "X": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"})); break;
                case "B": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"})); break;
                case "R": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"})); break;
            } if(data.isHdrYn=="Y" && CONSTANT.IS_HDR) {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
            } if(data.resolCd.indexOf("UHD")>=0) {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
            } if(data.isDvdYn=="Y") {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
            } if(data.wEvtImageUrlW3) {
                div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
            } if(data.adultOnlyYn!="Y" && UTIL.isLimitAge(data.prInfo)) {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
            }
        }

    };

    myHome.layer.home_wishList.prototype = new Layer();
    myHome.layer.home_wishList.prototype.constructor = myHome.layer.home_wishList;

    //Override create function
    myHome.layer.home_wishList.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_wishList.prototype.handleKeyEvent = function(key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["MyHomeWishList"] = myHome.layer.home_wishList;
})();