/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_main = function HomeMain(options) {
        Layer.call(this, options);
        var INSTANCE = this;
        var currentComp = 0;
        var compList = [];
        var recomList = [];
        var pageInfo = [{length: 0, start: 0, end: 50}];
        var currentPage = 0;
        var menuData;

        var inPageIdx = 0;

        var indicator;
        //var homeShot;
        var subHomeData;
        //var homeShotInterval, homeShotIndex = 0;
        var viewingPageTimeout;
        var subHomeVersion;

        // [si.mun] myMenu_pg 만 따로 컴포넌트를 정의한다.
        // 그 이유는 포인트 갱신을 위해 update 를 수행해야하기 때문
        var myMenuPoint;

        this.contextMenu;

        this.init = function() {
            try {
                this.div.attr({class: "arrange_frame myhome"});

                this.div.html("<div class='background'>" +
                //"<img id='defaultBg' src='" + COMMON_IMAGE.BG_DEFAULT + "' alt='background' class='clipping'>" +
                //"<div id='bgCanvasContainer' class='clipping'></div>" +
                "<span id='backgroundTitle'>마이메뉴</span>" +
                    //"<div id='pig_dim'></div>" +
                "</div>" +
                //"<div id='commonArea'>" +
                //"<div id='homeShot'>" +
                "<div class='context_menu_indicator'></div>" +
                //"</div>" +
                //"</div>" +
                "<div id='indicatorArea'></div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "<div id='sdw_mask'></div>" +
                "</div>");

                indicator = new Indicator(1);
                this.div.find("#indicatorArea").append(indicator.getView()).append(indicator.getPageTransView());
                //homeShot = new component.HomeShot();
                //this.div.find("#homeShot").append(homeShot.getView());

                menuData = this.getParams().menuData;
                subHomeData = SubHomeDataManager.getSubHome("VC", menuData.id);
                subHomeVersion = SubHomeDataManager.getSubHomeVersion();
                this.contextMenu = new ContextMenu_OnlyVOD();
                this.div.append(this.contextMenu.getView());
                createComponent();
            } catch (e) {
                console.log(e);
            }
        };

        this.remove = function() {
            removeComponents();
            Layer.prototype.remove.apply(this, arguments);
        };

        //function setHomeShot(index) {
        //    var homeShotData = subHomeData.homeShot;
        //    homeShot.setHomeShot(homeShotData[index].itemType, homeShotData[index].itemId, homeShotData[index].imgUrl, homeShotData[index].locator, homeShotData[index].locator2, null, homeShotData[index].parameter);
        //}

        this.show = function(options) {
            if (!options || !options.resume) {
                this.div.hide();
                initFocus();
                setFocus(this.getParams().focusIdx || 1);
                void this.div[0].offsetWidth;
                this.div.show();
            }

            Layer.prototype.show.call(this);
            LayerManager.showVBOBackground("myHome_main_background");

            updateComponents(options);
            showComponents(options);

            if (options && options.resume) resumeTextAnimating();
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);

            // 2017.08.27 Lazuli
            // 홈샷 없을 경우 디폴트 홈샷 노출 하도록 수정
            //if (subHomeData.homeShot && subHomeData.homeShot.length > 0) {
            //    setHomeShot(homeShotIndex = HTool.getIndex(homeShotIndex, 1, subHomeData.homeShot.length));
            //    if (!homeShotInterval) {
            //        homeShotInterval = setInterval(function() {
            //            setHomeShot(homeShotIndex = HTool.getIndex(homeShotIndex, 1, subHomeData.homeShot.length));
            //        }, 5000);
            //    }
            //} else homeShot.setHomeShot();
            this.div.find("#backgroundTitle").text(HTool.getMenuName(menuData));
            log.printDbg("show HomeMain");
        };

        this.hide = function(options) {
            Layer.prototype.hide.apply(this, arguments);
            LayerManager.hideVBOBackground();

            hideComponents(options);

            if (options && options.pause) pauseTextAnimation();
            else {
                var div = _$(".textAnimating").removeClass("textAnimating");
                UTIL.clearAnimation(div.find("span"));
            }

            // 2017/07/06 - lyllyl
            // 우리집 맞춤 TV 이탈 후 재진입시 재호출 없도록 수정함
            // 외부에서 찜하거나 찜 해제 했을 때 setFlag함.
            // if(!options || !options.pause) {
            //     WishListManager.setClearFlag();
            // }

            //if (homeShotInterval) clearInterval(homeShotInterval);
            //homeShotInterval = null;
            this.contextMenu.close();

            log.printDbg("hide HomeMain");
        };

        function pauseTextAnimation() {
            var div = INSTANCE.div.find("#contents .textAnimating").removeClass("textAnimating").addClass("pauseAnimationByMyHomeMain");
            UTIL.clearAnimation(div.find("span"));
        }

        function resumeTextAnimating() {
            var div = INSTANCE.div.find("#contents .pauseAnimationByMyHomeMain").removeClass("pauseAnimationByMyHomeMain").addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: div
            });
        }

        function updateComponents(options) {
            updateSubHomeData();
            for (var i = 0; i < compList.length; i++) {
                if (compList[i].hasOwnProperty("update")) {
                    compList[i].update({
                        resume: options && options.resume,
                        updatePageInfo: updatePageInfo
                    });
                }
            }

            // [si.mun] myMenuPoint component만 별도로 update 처리
            if (myMenuPoint && myMenuPoint.hasOwnProperty("update")) {
                myMenuPoint.update({
                    resume: options && options.resume,
                    updatePageInfo: updatePageInfo
                });
            }
        }

        function removeComponents() {
            for (var i = 0; i < compList.length; i++) {
                if (compList[i].hasOwnProperty("remove")) {
                    compList[i].remove();
                }
            }

            // [si.mun] myMenuPoint component만 별도로 remove 처리
            if (myMenuPoint && myMenuPoint.hasOwnProperty("remove")) {
                myMenuPoint.remove();
            }
        }

        function hideComponents(options) {
            for (var i = 0; i < compList.length; i++) {
                if (compList[i].hasOwnProperty("hide")) {
                    compList[i].hide(options);
                }
            }

            // [si.mun] myMenuPoint component만 별도로 hide 처리
            if (myMenuPoint && myMenuPoint.hasOwnProperty("hide")) {
                myMenuPoint.hide(options);
            }
        }

        function showComponents(options) {
            for (var i = 0; i < compList.length; i++) {
                if (compList[i].hasOwnProperty("show")) {
                    compList[i].show(options);
                }
            }

            // [si.mun] myMenuPoint component만 별도로 show 처리
            if (myMenuPoint && myMenuPoint.hasOwnProperty("show")) {
                myMenuPoint.show(options);
            }
        }

        function updateSubHomeData() {
            var newSubHomeVersion = SubHomeDataManager.getSubHomeVersion();
            if(subHomeVersion!==newSubHomeVersion) {
                // 서브홈데이터의 버전이 다르면 추천영역 모두 삭제 후 새로 생성하여 붙임
                subHomeData = SubHomeDataManager.getSubHome("VC", menuData.id);
                subHomeVersion = newSubHomeVersion;
                for(var i in recomList) {
                    if(recomList[i].hasOwnProperty("remove")) recomList[i].remove();
                    recomList[i].getView().remove();
                    compList.splice(compList.indexOf(recomList[i]), 1);
                } recomList = [];
                makeRecommendData();
                updatePageInfo();
                indicator.setSize(pageInfo.length, currentPage);
                compList[currentComp].onFocused(0);
            }
        }

        function updatePageInfo() {
            pageInfo = [{length: 0, start: 0, end: 50}];
            for (var i = 0; i < compList.length; i++) {
                addPageInfo(compList[i]);
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");

            // Biz 상품인 경우 myMenu_pg 컴포넌트 미생성
            if (!KTW.CONSTANT.IS_BIZ) {
                // [si.mun] 기존에 포인트 component 가 고정된 자리에 위치되도록 변경되었음
                // 때문에 for loop 내에서 생성하는 것이 loop 이전에 무조건 생성하도록 변경
                myMenuPoint = new myMenu_pg('myMenu_pg');
                INSTANCE.div.find("#contentsArea").prepend(myMenuPoint.getView());
                myMenuPoint.parent = INSTANCE;
            }

            for (var i in menuData.children) {
                //최근 시청 목록
                if (menuData.children[i].id === "MENU00601") {
                    addComponent(new recentlyList("recentlyList", menuData.children[i]));
                }
                //찜한 목록
                else if (menuData.children[i].id === "MENU01200") {
                    addComponent(new wishList("wishList", menuData.children[i]));

                    // [si.mun] 18.05월 GUI 형상에서 찜한목록, 추천 데이터, 당신을 위한 VOD 추천 순으로
                    // component 가 생성되는 것으로 변경이 되어 아래 위치에 추가하였음.
                    makeRecommendData();
                }
                //당신을 위한 VOD 추천
                else if (menuData.children[i].id === "MENU07003") {
                    addComponent(new component.foryou('foryou', menuData.children[i]));
                }
                // 마이 메뉴 2
                else if (menuData.children[i].catType == "MyMenu2") {
                    addComponent(new myMenu('myMenu', menuData.children[i]));
                }
            }

            indicator.setSize(pageInfo.length, currentPage);
            compList[currentComp].onFocused(0);
        }

        // 추천영역 (Mass추천 및 배너 영역 생성)
        function makeRecommendData() {
            if (subHomeData.recommend) {
                var recomDataList = subHomeData.recommend;
                var tmp = [];

                for (var i = 0; i < recomDataList.length; i++) {
                    if (!tmp[recomDataList[i].position]) tmp[recomDataList[i].position] = [];
                    tmp[recomDataList[i].position].push(recomDataList[i]);
                }
                for (var i in tmp) {
                    var d = new SubPosterList("VC_recom_" + i);
                    if (d.setData(tmp[i])) {
                        addComponent(d);
                        recomList.push(d);
                    }
                }
            }
        }

        function addComponent(component) {
            INSTANCE.div.find("#contents").append(component.getView());
            compList.push(component);
            addPageInfo(component);
            component.parent = INSTANCE;
        }

        function addPageInfo(component) {
            var lastPageInfo = pageInfo[pageInfo.length - 1];
            if (lastPageInfo.end - lastPageInfo.start < 1000 - component.getHeight()) {
                lastPageInfo.length++;
                lastPageInfo.end += component.getHeight();
            } else {
                pageInfo[pageInfo.length] = {
                    length: 1,
                    start: lastPageInfo.end,
                    end: lastPageInfo.end + component.getHeight()
                }
            }
        }

        this.controlKey = function(key_code) {
            if (!compList[currentComp]) return false;
            /*if (indicator.isFocused()) {
                switch (key_code) {
                    case KEY_CODE.UP:
                        changePage(-1);
                        return true;
                    case KEY_CODE.DOWN:
                        changePage(1);
                        return true;
                    case KEY_CODE.ENTER:
                    case KEY_CODE.RIGHT :
                        indicator.blurred();
                        compList[currentComp].onFocused();
                        return true;
                    case KEY_CODE.LEFT:
                        LayerManager.historyBack();
                        return true;
                    case KEY_CODE.PLAY:
                        return homeShot.execute();
                    case KEY_CODE.RED:
                        indicator.blurred();
                        if(inPageIdx===0) {
                            compList[currentComp].onFocused(key_code);
                        } while(inPageIdx!==0) changeFocus(-1, key_code);
                        return true;
                    case KEY_CODE.BLUE:
                        indicator.blurred();
                        if(inPageIdx === pageInfo[currentPage].length-1) {
                            compList[currentComp].onFocused(key_code);
                        } while(inPageIdx !== pageInfo[currentPage].length-1) changeFocus(1, key_code);
                        return true;
                }
            } else */if (compList[currentComp].onKeyAction(key_code)) {
                return true;
            } else switch (key_code) {
                case KEY_CODE.UP:
                    changeFocus(-1, key_code);
                    return true;
                case KEY_CODE.DOWN:
                    changeFocus(1, key_code);
                    return true;
                case KEY_CODE.RED:
                    if(inPageIdx===0) {
                        if(!compList[currentComp].isFirstFocused()){
                            compList[currentComp].onBlured();
                            compList[currentComp].onFocused(key_code);
                            return true;
                        } else changeFocus(-1, key_code);
                    } while(inPageIdx!==0) changeFocus(-1, key_code);
                    return true;
                case KEY_CODE.BLUE:
                    if(inPageIdx === pageInfo[currentPage].length-1) {
                        if(!compList[currentComp].isLastFocused()){
                            compList[currentComp].onBlured();
                            compList[currentComp].onFocused(key_code);
                            return true;
                        } else changeFocus(1, key_code);
                    } while(inPageIdx !== pageInfo[currentPage].length-1) changeFocus(1, key_code);
                    return true;
                case KEY_CODE.LEFT:
                    LayerManager.historyBack();
                    //_$("#MyHomeMain .context_menu_indicator").text("");
                    //compList[currentComp].onBlured();
                    //indicator.focused();
                    return true;
                case KEY_CODE.RIGHT:
                    changeFocus(1, key_code);
                    return true;
                case KEY_CODE.CONTEXT:
                    return true;
                //case KEY_CODE.PLAY:
                //    return homeShot.execute();
            }
        };

        function initFocus() {
            if (!compList || !compList[currentComp]) return;
            compList[currentComp].onBlured();
            currentComp = 1;
            currentPage = 0;
            inPageIdx = 1;
            _$("#MyHomeMain #contents").css("-webkit-transform", "translateY(0px)");
            _$("#MyHomeMain .context_menu_indicator").text("");
            indicator.setPos(0);
            //indicator.blurred();
            compList[currentComp].onFocused();
            setViewingPage(0);
        }

        function setFocus(newFocus) {
            while (currentComp !== newFocus) {
                changeFocus(1);
            }
        }

        function changeFocus(amount, keyCode) {
            if (amount == 0) return;
            INSTANCE.div.find(".context_menu_indicator").text("");
            compList[currentComp].onBlured();
            currentComp = HTool.getIndex(currentComp, amount, compList.length);
            inPageIdx += amount;
            syncPage();
            compList[currentComp].onFocused(keyCode);
        }

        function syncPage() {
            var oldPage = currentPage;
            if (inPageIdx < 0) {
                currentPage = HTool.getIndex(currentPage, -1, pageInfo.length);
                _$("#MyHomeMain #contents").css("-webkit-transform", "translateY(" + -pageInfo[currentPage].start + "px)");
                inPageIdx += pageInfo[currentPage].length;
                setViewingPage(oldPage > currentPage ? 0 : 300);//500)
            } else if (inPageIdx > pageInfo[currentPage].length - 1) {
                inPageIdx -= pageInfo[currentPage].length;
                currentPage = HTool.getIndex(currentPage, 1, pageInfo.length);
                _$("#MyHomeMain #contents").css("-webkit-transform", "translateY(" + -pageInfo[currentPage].start + "px)");
                setViewingPage(oldPage > currentPage ? 0 : 300);//500)
            } else return;
            indicator.setPos(currentPage);
            syncPage();
        }

        function setViewingPage(timer) {
            if (viewingPageTimeout) {
                clearTimeout(viewingPageTimeout);
                viewingPageTimeout = null;
            }

            viewingPageTimeout = setTimeout(function() {
                var pageIdx = 0;
                var compIdx = 0;
                for (var i = 0; i < compList.length; i++) {
                    if (compIdx >= pageInfo[pageIdx].length) {
                        compIdx = 0;
                        pageIdx++;
                    }

                    if (pageIdx < currentPage) {
                        compList[i].getView().css("visibility", "hidden");
                    } else {
                        compList[i].getView().css("visibility", "");
                    }
                    compIdx++;
                }
            }, timer);
        }

        function changePage(amount) {
            while (amount != 0) {
                if (amount > 0) {
                    currentComp = HTool.getIndex(currentComp, pageInfo[currentPage].length - inPageIdx, compList.length);
                    inPageIdx = pageInfo[currentPage].length;
                    amount--;
                } else if (amount < 0) {
                    currentComp = HTool.getIndex(currentComp, -1 - inPageIdx, compList.length);
                    inPageIdx = -1;
                    amount++;
                }
                syncPage();
            } if(inPageIdx!==0) {
                currentComp = currentComp-inPageIdx;
                inPageIdx = 0;
            }
        }

        function getPageTopIdx(page) {
            var idx = 0;
            for (var i = 0; i < page - 1; i++) {
                idx += pageInfo[i].length;
            }
            return idx + 1;
        }

    };

    myHome.layer.home_main.prototype = new Layer();
    myHome.layer.home_main.prototype.constructor = myHome.layer.home_main;

    //Override create function
    myHome.layer.home_main.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    myHome.layer.home_main.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeMain"] = myHome.layer.home_main;
})();