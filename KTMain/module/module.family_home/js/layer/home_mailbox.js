
/**
 * 알림박스
 * (우리집 맞춤 TV > 알림박스)
 * Created by ksk91_000 on 2016-08-30.
 */
(function() {
    myHome.layer = myHome.layer||{};
    myHome.layer.home_mailBox = function MyHomeMailbox(options) {
        Layer.call(this, options);

        var cateData;
        var viewMgr = new myHome.ViewManager();
        var instance = this;

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_mailBox"});

            this.div.html("<div class='background'>" +
                //"<img id='defaultBg' src='" + COMMON_IMAGE.BG_DEFAULT + "' alt='background' class='clipping'>"+
                "<div id='defaultBg' alt='background' class='category'>" +
                "<span id='backgroundTitle'>공지/알림</span>"+
                "</div>"
            );

            cbCreate(true);
        };

        // function updateView(that, callback) {
        function updateView(focusIdx) {
            LayerManager.startLoading();
            myHome.wsSmartPushManager.msgBoxList(function (result, data) {

                /*  편성 없을 시 테스트용 데이터 */
                /*
                 data = {
                 "result": "200",
                 "result_msg": "SUCCESS",
                 "ts_id": "1477889458",
                 "notice_list": [
                 {
                 "req_id": 9,
                 "event_cd": "NG",
                 "start_dt": "20161022140000",
                 "req_nm":"ggyy올레tv 신규고객 감사쿠폰 적립안내",
                 "req_cont":"ggyy가입감사 기념으로 tv쿠폰을 적립해 드리오니 많은 이용 바랍니다. 많은 이용 바랍니다.",
                 "comment":"ggyy유효기간: 12/31까지",
                 "img_path":" http://test/test.jpg ",
                 "action_type":"MOVE_TO_CH",
                 "action_ots":707
                 },
                 {
                 "req_id": 8,
                 "event_cd": "NC",
                 "start_dt": "20161021103000",
                 "req_nm":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔",
                 "req_cont":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십",
                 "comment":"유효기간: 12/31까지",
                 "img_path":" http://test/test.jpg ",
                 "action_type":"PLAY_VOD",
                 "action_value":"MZ4G90B8SGL150000100"
                 },
                     {
                         "req_id": 8,
                         "event_cd": "NC",
                         "start_dt": "20161021103000",
                         "req_nm":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔",
                         "req_cont":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십",
                         "comment":"유효기간: 12/31까지",
                         "img_path":" http://test/test.jpg ",
                         "action_type":"PLAY_VOD",
                         "action_value":"MZ4G90B8SGL150000100"
                     },
                     {
                         "req_id": 8,
                         "event_cd": "NC",
                         "start_dt": "20161021103000",
                         "req_nm":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔",
                         "req_cont":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십",
                         "comment":"유효기간: 12/31까지",
                         "img_path":" http://test/test.jpg ",
                         "action_type":"PLAY_VOD",
                         "action_value":"MZ4G90B8SGL150000100"
                     },
                 {
                 "req_id": 4,
                 "event_cd": "NG",
                 "start_dt": "20161020150000",
                 "req_nm":"어벤져스 에이지오브 울트론",
                 "req_cont":"마블 최강 히어로들을 모두만나보세요.",
                 "comment":"",
                 "img_path":" http://test/test.jpg ",
                 "action_type":"SHOW_ALARM"
                 }
                 ],
                 "event_list": [
                 {
                 "req_id": 7,
                 "event_cd": "EA",
                 "start_dt": "20161024140000",
                 "req_nm":"ggyy<럭키>구매 시 5천원 증정!",
                 "req_cont":"ggyy동시개봉 <럭키> 구매 후 이벤트에 응모하시면 K쇼핑 쿠폰증정",
                 "comment":"ggyy유효기간: 12/31까지",
                 "img_path":" http://blog.jinbo.net/attach/615/200937431.jpg ",
                 "action_type":"SHOW_CONTENTDETAIL",
                 "action_value":"MZ4G90B8SGL150000100"
                 },
                     {
                         "req_id": 7,
                         "event_cd": "EA",
                         "start_dt": "20161024140000",
                         "req_nm":"ggyy<럭키>구매 시 5천원 증정!",
                         "req_cont":"ggyy동시개봉 <럭키> 구매 후 이벤트에 응모하시면 K쇼핑 쿠폰증정",
                         "comment":"ggyy유효기간: 12/31까지",
                         "img_path":" http://blog.jinbo.net/attach/615/200937431.jpg ",
                         "action_type":"SHOW_CONTENTDETAIL",
                         "action_value":"MZ4G90B8SGL150000100"
                     },
                 {
                 "req_id": 6,
                 "event_cd": "EB",
                 "start_dt": "20161012140000",
                 "req_nm":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔",
                 "req_cont":"일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십",
                 "comment":"지상파/KBS/MBC 등 월정액 해당",
                 "img_path":" http://blog.jinbo.net/attach/615/200937431.jpg ",
                 "action_type":"RUN_BOUND",
                 "action_value":"131"
                 },
                 {
                 "req_id": 5,
                 "event_cd": "EC",
                 "start_dt": "20161012140000",
                 "req_nm":"<어벤져스2> 예약구매 이벤트!",
                 "req_cont":"<어벤져스2> 예약구매 후 응모한 고객에게 예매권을 드려요!",
                 "comment":"출시 예정일: 12/23",
                 "img_path":" http://blog.jinbo.net/attach/615/200937431.jpg ",
                 "action_type":"MOVE_TO_CH",
                 "action_otv":15
                 },
                 {
                 "req_id": 2,
                 "event_cd": "ED",
                 "start_dt": "20161012140000",
                 "req_nm":"Clip 제휴 이벤트",
                 "req_cont":"Clip을 다운 받으시면 미쓰와이프 무료 시청 쿠폰을 드려요!",
                 "comment":"(9/10 ~ 9/16) 매일 300명",
                 "img_path":" http://blog.jinbo.net/attach/615/200937431.jpg ",
                 "action_type":"RUN_PUSH_APP",
                 "action_value":"2"
                 }
                 ]
                 };
                 */


                if(result && data.result==200) {
                    cateData = [
                        new ViewData("notifyList", myHome.view.SingleCompView, {component: new component.notifyList('notifyListComp', HTool.getNoticeData())}, "공지사항", "MENU00608"),
                        new ViewData("noticeList", myHome.view.SingleCompView, {component: new component.noticeList('noticeListComp', data.notice_list)}, "알림", "MENU00710"),
                        new ViewData("eventList", myHome.view.SingleCompView, {component: new component.eventList('eventListComp', data.event_list)}, "이벤트", "MENU00711")
                    ];
                } else {
                    cateData = [
                        new ViewData("notifyList", myHome.view.SingleCompView, {component: new component.notifyList('notifyListComp', HTool.getNoticeData())}, "공지사항", "MENU00608"),
                        new ViewData("noticeList", myHome.view.SingleCompView, {component: new component.noticeList('noticeListComp', [])}, "알림", "MENU00710"),
                        new ViewData("eventList", myHome.view.SingleCompView, {component: new component.eventList('eventListComp', [])}, "이벤트", "MENU00711")
                    ];
                }

                viewMgr.setParent(instance.div); //that.div);
                viewMgr.setData(new CategoryViewData("categoryList", cateData, "알림박스", "MENU00700"));
                /*that*/ instance.div.append(viewMgr.getView());
                if(focusIdx!=null) viewMgr.changeCategoryFocus(focusIdx, true);

                instance.div.find("#categoryArea").append("<div id='sdw_mask'></div>");
                LayerManager.stopLoading();
            });
        }

        this.show = function(options) {
            if(this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);

            if(options && options.resume) viewMgr.resume();
            if (!options || !options.resume) {
                viewMgr.destroy();
                viewMgr.goToRoot();
            }

            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            if (!options || !options.resume) {
                var that = this;
                // updateView(this, function () { Layer.prototype.show.call(that); });
                updateView(this.getParams() && this.getParams().focusIdx);
            } //else Layer.prototype.show.call(this);

            Layer.prototype.show.call(this);
            LayerManager.showVBOBackground("myHome_main_background");
            log.printDbg("show HomeMailbox");
        };

        this.hide = function(options) {
            if(options && options.pause) viewMgr.pause();
            Layer.prototype.hide.call(this);
            if(!options || !options.pause) {
                viewMgr.destroy();
                viewMgr.goToRoot();
            }

            /**
             * [dj.son] hide 될때 background 도 hide 되도록 수정
             */
            LayerManager.hideVBOBackground("myHome_main_background");

            log.printDbg("hide HomeMailbox");
        };

        this.onKeyAction = function(keyCode) {
            return viewMgr.onKeyAction(keyCode);
        };
    };

    myHome.layer.home_mailBox.prototype = new Layer();
    myHome.layer.home_mailBox.prototype.constructor = myHome.layer.home_mailBox;

    //Override create function
    myHome.layer.home_mailBox.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_mailBox.prototype.handleKeyEvent = function(key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["MyHomeMailbox"] = myHome.layer.home_mailBox;
})();