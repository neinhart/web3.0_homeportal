
/**
 * 마이 플레이리스트
 * (우리집 맞춤 TV > 마이 플레이리스트)
 * Created by ksk91_000 on 2016-09-05.
 */
(function() {
    myHome.layer = myHome.layer||{};
    myHome.layer.home_myPlayList = function HomeMyPlayList(options) {
        Layer.call(this, options);
        var cateData;
        var viewMgr = new myHome.ViewManager();

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_myPlayList"});

            this.div.html("<div class='background'>" +
                "<div id='defaultBg' alt='background' class='category'>" +
                "<span id='backgroundTitle'>마이 플레이리스트</span>" +
                "</div>");

            viewMgr.setParent(this.div);
            this.div.append(viewMgr.getView());
            // update(cbCreate);
            if(cbCreate) cbCreate(true);
        };

        this.show = function (options) {
            Layer.prototype.show.apply(this, arguments);
            LayerManager.showVBOBackground("myHome_main_background");
            //oipfAdapter.basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);

            if(this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);
            if(!options || !options.resume || myHome.MyPlayListManager.hasUpdate()) update(options&&options.resume);
            else viewMgr.resume();
        };

        this.hide = function (options) {
            if(options && options.pause) viewMgr.pause();
            Layer.prototype.hide.apply(this, arguments);
            LayerManager.hideVBOBackground();
            log.printDbg("hide HomeMain");
        };

        var update = function(isResume, callback) {
            LayerManager.startLoading({preventKey:true});
            var prevIdx = 0;
            if(isResume) prevIdx = viewMgr.getCurrentCategoryIndex();
            myHome.MyPlayListManager.getPlayItemList(function (result, data) {
                if(result) parseData(data, function() {
                    if(isResume) {
                        // resume으로 갱신되는 경우 갱신 전 인덱스로 진입되도록 함.
                        viewMgr.changeCategoryFocus(prevIdx);
                        viewMgr.reChangeFocus(true);
                    } if(callback) callback(true);
                });
                else if(callback) callback(false);
                LayerManager.stopLoading();
            }, true);
        };

        function parseData(data, callback) {
            for (var i = 0, cateData = []; i < data.length; i++) {
                var vd = new ViewData("myPlayList_" + i, myHome.view.MyPlayListView, {id: data[i].playListId, name: data[i].playListNm, itemCnt: data[i].itemCnt, info: data[i]}, data[i].playListNm, data[i].playListId);
                vd.info = data[i];
                cateData.push(vd);
            }

            viewMgr.destroy();
            viewMgr.setData(new ViewData("myPlayList", myHome.view.myPlayListCategoryView, { data: cateData, updateCb: update }, "마이 플레이리스트", "MENU00603"));
            if(callback) callback();
        }

        this.controlKey = function(key_code) {
            if(viewMgr.onKeyAction(key_code)) return true;
            else if(key_code===KEY_CODE.LEFT) {
                LayerManager.historyBack();
                return true;
            } else return false;
        };
    };

    myHome.layer.home_myPlayList.prototype = new Layer();
    myHome.layer.home_myPlayList.prototype.constructor = myHome.layer.home_myPlayList;

    //Override create function
    myHome.layer.home_myPlayList.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_myPlayList.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeMyPlayList"] = myHome.layer.home_myPlayList;
})();