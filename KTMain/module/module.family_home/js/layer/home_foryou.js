/**
 * Created by ksk91_000 on 2016-08-09.
 */
(function() {
    myHome.layer = myHome.layer||{};
    myHome.layer.home_foryou = function HomeCloud(options) {
        Layer.call(this, options);
        var viewMgr;

        this.getViewMgr = function () {
          return viewMgr;
        };

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_foryou"});
            viewMgr = new myHome.ViewManager();

            this.div.html("<div class='background'>" +
                "<div id='defaultBg' alt='background' class='category'>" +
                '<span id="backgroundTitle">당신을 위한 VOD 추천</span>' +
                '</div>');

            this.div.append(viewMgr.getView());
            viewMgr.setParent(this.div);

            var params = this.getParams();
            CurationManager.getCurationData(function (data) {
                var viewDataList = [new ViewData("foryou", myHome.view.ForYouView, null, "나의 취향 알아보기", "MENU07004")].concat(data);
                viewDataList.push(new ViewData("myRatingContent", myHome.view.MyRatingContentView, null, "내가 평가한 콘텐츠", "MENU07005"));

                viewMgr.setData(new ViewData("categoryView", myHome.view.forYouCategoryView, viewDataList, "당신을 위한 VOD 추천", "MENU07003"));

                // [si.mun] home_foryou layer가 DOM에 append 되기 전에
                // changeCategoryFocus -> ForYouCategoryView.setFocus -> setTextAnimation 이 실행되는 데
                // DOM 에 append 가 되지 않았으므로 util의 startTextAnimation 이 실행되지 않는다.
                // 이렇게 되면 categoryView에 첫 진입 시, title이 길어도 textAnimation 이 동작하지 않으므로
                // home_foryou layer를 DOM에 먼저 append 한 뒤, viewMgr.changeCategoryFocus 를 수행하도록 위치 변경
                if(cbCreate != undefined) cbCreate(true);

                if(params && params.targetId) {
                    switch (params.targetId) {
                        case "MENU07004": viewMgr.changeCategoryFocus(0, true); break;
                        case "MENU07005": viewMgr.changeCategoryFocus(viewDataList.length-1, true); break;
                    }
                }
                // [si.mun] focusIdx 값이 0으로 전달되면 else if 로직을 타지 않아 changeCategoryFocus 가 동작하지 않는다.
                // 때문에 null, undefined로 비교하도록 수정
                else if(params.focusIdx !== null && params.focusIdx !== undefined) {
                    viewMgr.changeCategoryFocus(params.focusIdx);
                }

                //if(cbCreate != undefined) cbCreate(true);
            });
        };

        this.show = function(options) {
            if(this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);
            Layer.prototype.show.call(this);
            LayerManager.showVBOBackground("myHome_main_background");
            if(options && options.resume) viewMgr.resume();
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
        };

        this.hide = function (options) {
            if(options && options.pause) viewMgr.pause();
            Layer.prototype.hide.call(this);
            LayerManager.hideVBOBackground();
        };

        this.remove = function () {
            Layer.prototype.remove.call(this);
            viewMgr.destroy();
        };

        this.controlKey = function(keyCode) {
            return viewMgr.onKeyAction(keyCode) || (keyCode == KEY_CODE.BACK && (LayerManager.historyBack(), true));
        };

        this.hotKeyProcess = function (keyCode, menuData) {
            if(keyCode===KEY_CODE.RECOMMEND) {
                if(viewMgr.getCurrentView().id==menuData.id) {
                    LayerManager.clearNormalLayer();
                    return true;
                } else return false;
            } else {
                return false;
            }
        };

        this.title = "당신을 위한 VOD 추천"
    };

    myHome.layer.home_foryou.prototype = new Layer();
    myHome.layer.home_foryou.prototype.constructor = myHome.layer.home_foryou;

    //Override create function
    myHome.layer.home_foryou.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_foryou.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeForYou"] = myHome.layer.home_foryou;
})();