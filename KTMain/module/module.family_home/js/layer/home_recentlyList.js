/**
 * 최근 시청 목록 (우리집 맞춤 Tv -> 최근 시청 목록 -> 전체보기)
 * Created by ksk91_000 on 2016-11-08.
 */
(function() {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_recentlyList = function HomeRecentlyList(options) {
        Layer.call(this, options);
        var contentList = [];
        var focus = 0;
        var instance = this;
        var indicator;
        var curPage = 0;

        // 0 : content focus, 1 : context focus
        var focusMode = 0;

        // 0 : 편집 모드 X, 1 : 편집 모드 O
        var editMode = false;

        // 편집 모드에서 focus
        // 0 : 편집 모드 ON/OFF
        // 1 : 전체 선택 / 해제
        var editFocus = 0;

        // 한 page 당 표시 되는 poster의 최대 갯수
        var pageMaxCnt = 10;

        // 한 줄에 표시할 수 있는 poster의 갯수
        var rowItemCnt = pageMaxCnt / 2;

        // 외부에서 최근시청 목록을 호출 시 해당 모듈의 이름이 저장 됨
        // (ex. 키즈모드에서 호출 시 showModule = "Kids" 가 된다.)
        var showModule;

        // 외부에서 최근시청 목록을 호출 시 showModule을 보고 모듈의 recentlyListManager를 저장
        // showModule 이 없다면 기본적으로 마이메뉴의 recentlyListManager 를 사용하도록 구현
        var usedRecentlyListManager;

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_recentlyList"});

            indicator = new Indicator(1);

            this.div.html("<div class='background'>" +
                            "<span id='backgroundTitle'>최근 시청 목록</span>" +
                        "</div>" +
                        "<div id='contentsArea'></div>" +
                        "<div id='indicatorArea'></div>" +
                        "<div class='related_menu'>" +
                            "<div class='menu_area'>" +
                                "<div class='bg_area'>" +
                                    "<div class='bg_top'/>" +
                                    "<div class='bg_mid'/>" +
                                    "<div class='bg_bottom'/>" +
                                "</div>" +
                                "<div class='btn_area'>" +
                                    "<btn class='option_menu_current_mode'>편집 모드</btn>" +
                                    "<btn class='option_menu_select_all'>전체 선택</btn>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                        "<div class='context_menu_indicator'>목록에서 삭제</div>" +
                        "<div id='sdw_mask'></div>"
            );
            cbCreate(true);
        };

        this.show = function(options) {
            var params = this.getParams();

            editMode = false;
            this.div.toggleClass('editMode', editMode);
            this.div.find("._menu_area").addClass("activate");
            Layer.prototype.show.call(this);
            LayerManager.showVBOBackground("myHome_main_background");
            usedRecentlyListManager = window.RecentlyListManager;

            if(params) {
                this.div.find("#backgroundTitle").text(params.menuName);

                // [si.mun] 키즈 모듈에서 최근 시청 목록 호출 시
                // 아래 로직을 수행하고 with_kids class 를 추가한다.
                showModule = params.showModule;
                if (showModule && showModule === "Kids") {
                    usedRecentlyListManager = params.KidsRecentlyManager;
                    this.div.addClass("with_kids");

                    // [si.mun]
                    // 1. 마이메뉴의 VBOBackground hide
                    // 2. 키즈모드의 배경 show
                    // 3. 키즈 category의 배경 hide
                    // hideVBO는 VBObacground를 모두 hide 시키므로 1~2번의 순서가 바뀌면 안됨
                    LayerManager.hideVBOBackground();
                    LayerManager.showVBOBackground("kidsSubHome_main_background");
                    _$("#bg_2d_kids").hide();
                }
            }
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            usedRecentlyListManager.getRecentlyData(function(data) {
                contentList = data;
                constructHTML();
                setFocus((options && options.resume) ? focus : 0);
            });
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);

            LayerManager.hideVBOBackground();

            // [si.mun] 키즈 모듈에서 최근 시청 목록 호출 후
            // 종료 시 아래 로직을 수행하고 with_kids class 를 제거한다.
            // 그리고 this.show 에서 bg_2d_kids 를 hide 하였는 데, 이를 다시 show
            if (showModule === "Kids") {
                this.div.removeClass("with_kids");
                showModule = null;
                _$("#bg_2d_kids").show();
            }

            UTIL.clearAnimation(instance.div.find(".textAnimating span"));
            instance.div.find(".textAnimating").removeClass("textAnimating");
            resetFocus();
            //setFocusMode(0);
        };

        function setFocus(newFocus) {
            if (newFocus >= contentList.length) newFocus = contentList.length - 1;
            setPage(Math.floor(newFocus / pageMaxCnt));

            instance.div.find("#recently_item_" + focus).removeClass("focus");
            focus = newFocus;
            instance.div.find("#recently_item_" + focus).addClass("focus");

            if (contentList[focus]) changeBackground(contentList[focus].imgUrl + "?w=196&h=280&quality=90", 500);
            //instance.div.find(".context_menu_indicator").text("편집 모드" + (editMode ? " 종료" : ""));
            setTextAnimation(focus);
        }

        function setTextAnimation(focus) {
            UTIL.clearAnimation(instance.div.find(".textAnimating span"));
            instance.div.find(".textAnimating").removeClass("textAnimating");
            void instance.div[0].offsetWidth;
            if (UTIL.getTextLength(contentList[focus].contsName, "RixHead L", 30) > 215) {
                var posterDiv = instance.div.find("#recently_item_" + focus + " .posterTitle");
                posterDiv.addClass("textAnimating");
                UTIL.startTextAnimation({
                    targetBox: posterDiv
                });
            }
        }

        function resetFocus() {
            // Menu 숨김
            instance.div.find(".menu_area").removeClass("activate");

            // Menu 초기화
            instance.div.find(".option_menu_current_mode").text("편집 모드");
            instance.div.find(".option_menu_select_all").text("전체 선택").addClass("disable");
            instance.div.find(".menu_area btn").eq(editFocus).removeClass('focus');

            // 콘텐츠 focus, select 해제
            instance.div.find("li.content").removeClass("select");
            instance.div.find("li.content").removeClass("focus");

            // 편집 모드 해제
            editMode = false;

            // 포커스 초기화
            focus = 0;
            focusMode = 0;
            editFocus = 0;
        }

        var bg_timer, curBgIdx = 0;
        var changeBackground = function(src, time) {
            var bgArea = VBOBackground;
            if (bg_timer) clearTimeout(bg_timer);
            bg_timer = setTimeout(function() {
                if (src == null) {
                    bgArea.find("#background_blur").css("display", "none");
                    bgArea.find("#background_blur .bg_wrapper").css("opacity", "");
                } else {
                    bgArea.find("#background_blur").css("display", "");
                    var bg = bgArea.find("#background_blur .bg_wrapper");
                    bg.eq(curBgIdx).css("opacity", 0);
                    curBgIdx ^= 1;
                    bg.find(".bg").eq(curBgIdx).css({"background-image": "url(" + src + ")"});
                    bg.eq(curBgIdx).css("opacity", 1);
                }
                bg_timer = null;
            }, time);
        };

        function setEditFocus(newFocus) {
            // 전체 선택, 해제 버튼이 disable 일 땐 아무런 동작을 하지 않는다.
            if (instance.div.find(".menu_area btn").eq(1).hasClass("disable")) {
                return ;
            }

            // 버튼 focus 변경
            instance.div.find(".menu_area btn").eq(editFocus).removeClass('focus');
            instance.div.find(".menu_area btn").eq(newFocus).addClass('focus');
            editFocus = newFocus;
        }

        function setFocusMode(mode) {
            if (focusMode == mode) return;

            if (mode === 0) { // 콘텐츠에 focus 위치 시
                //instance.div.find("#recently_item_" + focus).removeClass("focus");
                //instance.div.find(".context_area .btn").eq(editFocus).removeClass("focus");
            } else { // context에 focus 위치 시
                //instance.div.find("#recently_item_" + focus).addClass("focus");
                //instance.div.find(".context_area .btn").eq(editFocus).addClass("focus");
            }

            focusMode = mode;
            instance.div.find("#recently_item_" + focus).toggleClass("focus", mode == 0);
            instance.div.find(".menu_area btn").eq(editFocus).toggleClass('focus', mode == 1);
        }

        var scrollTimer;

        function setPage(page) {
            var isPageLoad = false;
            if(curPage != page) isPageLoad = true;

            curPage = page;
            instance.div.find("#listArea").css("-webkit-transform", 'translateY(-' + page * 908 + "px)");
            indicator.setPos(page);
            if(isPageLoad) {
                instance.div.find(".content.poster_vertical").css("visibility", "");

                if (scrollTimer) clearTimeout(scrollTimer);
                scrollTimer = setTimeout(function () {
                    for (var i = 0; i < page * pageMaxCnt; i++) instance.div.find("#recently_item_" + i).css("visibility", "hidden");
                }, 300);
            } else {
                for (var i = 0; i < page * pageMaxCnt; i++) instance.div.find("#recently_item_" + i).css("visibility", "hidden");
            }
        }

        function constructHTML() {
            var html = _$("<div/>", {id: 'listArea'});
            for (var i = 0; i < contentList.length; i++) {
                html.append(getVerticalPoster("recently_item_" + i, contentList[i]));
            }

            indicator.setSize(Math.floor((contentList.length - 1) / pageMaxCnt) + 1);
            instance.div.find("#indicatorArea").append(indicator.getView()).append(indicator.getPageTransView());
            instance.div.find("#contentsArea").html(html);
            instance.div.toggleClass("singlePage", Math.ceil(contentList.length / pageMaxCnt) == 1);
        }

        function enterKeyAction() {
            var selectAll = instance.div.find(".option_menu_select_all");

            if (focusMode == 0) { // 콘텐츠에 focus
                if (editMode) { // 편집 모드
                    instance.div.find("#recently_item_" + focus).toggleClass("select");

                    var selectedCnt = instance.div.find("li.content.select").length;

                    if (selectedCnt === contentList.length) { // 모두 선택 된 경우
                        selectAll.text("전체 선택 해제");
                    } else { // 일부만 선택 된 경우 || 1개도 선택되지 않은 경우
                        selectAll.text("전체 선택");
                    }

                    instance.div.find(".context_menu_indicator").show();
                } else { // 상세 진입
                    var catId = contentList[focus].itemType == 1 ? contentList[focus].contsId : contentList[focus].catId;
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, catId, contentList[focus].contsId, "02");
                    } catch(e) {}
                    openDetailLayerImpl(contentList[focus]);
                }
            } else { // contextMenu focus
                if (editMode) { // 편집 모드 일 때
                    if (editFocus === 0) { // 편집 모드 버튼(이곳을 수행하는 것은 편집 모드 O -> 편집 모드 X 시나리오)
                        editMode = false;
                        instance.div.removeClass('editMode');
                        instance.div.find(".option_menu_current_mode").text("편집 모드");
                        instance.div.find(".option_menu_select_all").addClass("disable");

                        // select 해제
                        instance.div.find("li.content").removeClass("select");

                        // 전체선택 버튼 텍스트 변경
                        selectAll.text("전체 선택");

                        // 목록에서 삭제 버튼 hide
                        instance.div.find(".context_menu_indicator").hide();
                    } else { // 전체 선택 버튼( 전체 선택, 전체 선택 해제 toggle )
                        var contents = instance.div.find("li.content");
                        var selectedList = instance.div.find("li.content.select");

                        // 전체 선택 해제 -> 전체 상태로 변경
                        // enterKeyAction() 으로 들어온 순간은 전체선택 해제 상태이기 때문이다.
                        if (contents.length !== selectedList.length) {
                            contents.addClass("select");
                            selectAll.text("전체 선택 해제");

                            // 목록에서 삭제 버튼 show
                            instance.div.find(".context_menu_indicator").show();
                        } else { // 전체 선택 -> 전체 선택 해제로 상태 변경
                            contents.removeClass("select");
                            selectAll.text("전체 선택");
                            instance.div.find(".context_menu_indicator").hide();
                        }
                    }
                } else { // 편집모드가 아닐 때
                    if (editFocus === 0) { // 편집 모드 버튼
                        instance.div.addClass('editMode');
                        instance.div.find(".option_menu_current_mode").text("편집 모드 해제");
                        instance.div.find(".option_menu_select_all").removeClass("disable");
                    }
                    // 편집 모드로 전환
                    editMode = true;
                }
            }
        }

        this.onKeyAction = function(keyCode) {
            switch (keyCode) {
                // gyuh 2017-05-21
                case KEY_CODE.LEFT:
                    if (focusMode == 1) { // 편집모드 && context focus
                        setFocusMode(0); // 콘텐츠로 focus
                        setFocus(Math.min(focus - focus % rowItemCnt + rowItemCnt - 1, contentList.length - 1));

                        // 적/청색 page non-hidden(show)
                        indicator.togglePageTransIcon(false);
                        instance.div.find(".context_menu_indicator").show();
                    } else if (focus % rowItemCnt == 0) { // 리스트 최자측
                        // resetFocus();
                        return LayerManager.historyBack(), true;
                    } else { // 콘텐츠
                        setFocus(HTool.getIndex(focus, -1, contentList.length));
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (focus === contentList.length - 1 ||
                        focus % rowItemCnt == rowItemCnt - 1) { // 리스트 최우측
                        setFocusMode(1);
                        setEditFocus(editFocus); // context 내에서 btn focus 설정

                        // contextMenu 로 focus가 이동 시, 선택 된 콘텐츠가 없으면 목록에서 삭제 hide
                        if (instance.div.find("li.content.select").length === 0) {
                            instance.div.find(".context_menu_indicator").hide();
                        }

                        // 적/청색 page hidden
                        indicator.togglePageTransIcon(true);
                    } else { // 콘텐츠
                        setFocus(HTool.getIndex(focus, 1, contentList.length));
                    }
                    return true;
                case KEY_CODE.UP :
                    if (focusMode === 1) { // contextMenu 에 focus 위치
                        setEditFocus(editFocus ^ 1);
                    } else if ((contentList.length % rowItemCnt != 0) && focus % rowItemCnt == focus) setFocus(Math.min(Math.floor(contentList.length / rowItemCnt) * rowItemCnt + focus % rowItemCnt, contentList.length - 1));
                    else if (contentList.length % rowItemCnt == 0 && focus < rowItemCnt && contentList.length > rowItemCnt) setFocus((contentList.length - rowItemCnt) + (focus % rowItemCnt));
                    else if (contentList.length > rowItemCnt) setFocus(focus - rowItemCnt);
                    return true;
                case KEY_CODE.DOWN :
                    if (focusMode === 1) { // contextMenu 에 focus 위치
                        setEditFocus(editFocus ^ 1);
                    } else if ((contentList.length % rowItemCnt != 0) && Math.floor(focus / rowItemCnt) == Math.floor(contentList.length / rowItemCnt)) setFocus(focus % rowItemCnt);
                    else if (contentList.length % rowItemCnt == 0 && contentList.length > rowItemCnt && focus >= contentList.length - rowItemCnt && focus < contentList.length) setFocus(focus % rowItemCnt);
                    else if (contentList.length > rowItemCnt) setFocus(Math.min(focus + rowItemCnt, contentList.length - 1));
                    return true;
                case KEY_CODE.RED:
                    if (focusMode === 0) { // 콘텐츠에 focus
                        if (focus % pageMaxCnt === 0) { // 0번째 index focus
                            setFocus(HTool.getIndex(curPage, -1, Math.ceil(contentList.length / pageMaxCnt))*pageMaxCnt);
                        } else { // 0번째 index가 아닌 곳에 focus
                            setFocus(curPage * pageMaxCnt);
                        }
                    }
                    return true;
                case KEY_CODE.BLUE:
                    if (focusMode === 0) { // 콘텐츠에 focus
                        if (focus % pageMaxCnt === pageMaxCnt - 1) { // 현재 포커스가 해당 page의 마지막 index에 위치
                            setFocus(Math.min(contentList.length-1, HTool.getIndex(curPage, 1, Math.ceil(contentList.length / pageMaxCnt))*pageMaxCnt + pageMaxCnt - 1));
                        } else if (focus === contentList.length - 1) { // 마지막 page에 마지막 index에 위치
                            setFocus(Math.min(contentList.length-1, pageMaxCnt - 1));
                        } else { // 중간 index에 위치한 경우
                            setFocus(Math.min(curPage * pageMaxCnt + pageMaxCnt - 1));
                        }
                    }
                    return true;
                case KEY_CODE.ENTER :
                    enterKeyAction();
                    return true;
                case KEY_CODE.BACK :
                    return LayerManager.historyBack(), true;
                case KEY_CODE.CONTEXT:
                    openConfirmPopup();
                    return true;
                default:
                    return false;
            }
        };

        var openDetailLayerImpl = function(data) {
            LayerManager.startLoading({preventKey:true});
            if(data.contYn=="N") { // 시리즈
                if (data.catId == "N") {
                    myHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=V&platformGubun=W", function (res, newData) {
                        if(res) {
                            if (!newData) {
                                LayerManager.stopLoading();
                                HTool.openErrorPopup({message: ERROR_TEXT.ET002});
                            } else if (newData.serviceYn == "Y") {
                                openDetailLayer(newData.catId, data.contsId, "02");
                            } else {
                                LayerManager.stopLoading();
                                HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                            }
                        } else {
                            LayerManager.stopLoading();
                            extensionAdapter.notifySNMPError("VODE-00013");
                            HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                        }
                    });
                } else openDetailLayer(data.contsId, data.contsId, "02");
            } else { //컨텐츠
                myHome.amocManager.getContentW3(function (res, contData) {
                    if(res) {
                        if(contData.contsId) openDetailLayer(contData.catId, data.contsId, "02", {contsInfo:contData});
                        else if(data.catId!=="N") openDetailLayer("N", data.contsId, "02");
                        else {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                        }
                    } else {
                        LayerManager.stopLoading();
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    }
                }, data.catId, data.contsId, DEF.SAID);
            }
        };

        var getVerticalPoster = function(id, data) {
            var element = _$("<li id='" + id + "' class='content poster_vertical'>" +
                "<div class='content'>" +
                "<div class='focus_red'><div class='black_bg'></div></div>" +
                "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>" +
                "<img src='" + data.imgUrl + "?w=196&h=280&quality=90' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/image/default_poster.png\"'>"+
                "<div class='contentsData'>" +
                "<div class='posterTitle'><span>" + data.contsName + "</span></div>" +
                "<span class='posterDetail'>" +
                "<span class='stars'/>" +
                (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
                "<img src='" + modulePath + "resource/image/icon/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo - 0) + ".png' class='age'>" +
                "</span>" +
                "</div>" +
                "<div class='icon_area'/>" +
                "<div class='poster_remain_bg'></div>" +
                "<div class='poster_remain_bar'></div>" +
                "<div class='edit_select_btn'></div>" +
                "</div>" +
                "</li>");

            element.find(".poster_remain_bar").css("width", Math.min(196, data.linkTime / (parseInt(data.runTime || data.runtime) * 60) * 196) + "px");
            setIcon(element.find(".icon_area"), data);
            element.toggleClass("series", data.contYn == "N");
            return element;
        };

        function setIcon(div, data) {
            div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
            switch (data.newHot) {
                case "N":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"}));
                    break;
                case "U":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"}));
                    break;
                case "X":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"}));
                    break;
                case "B":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"}));
                    break;
                case "R":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"}));
                    break;
            }
            if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
            }
            if (data.isHd == "UHD") {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
            }
            if (data.isDvdYn == "Y") {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
            }
            if (data.wEvtImageUrlW3) {
                div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
            }
            if (data.adultOnlyYn != "Y" && UTIL.isLimitAge(data.prInfo)) {
                div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
            }
        }

        var openConfirmPopup = function() {
            var divList = instance.div.find("li.content");
            var deleteList = [];
            var deleteIdxList = [];
            var selectedCnt = instance.div.find("li.content.select").length;

            // 삭제할 콘텐츠 유/무 여부
            var isDeleted = false;

            // 1. focus가 콘텐츠에 위치
            //    - 체크 여부에 상관없이 무조건 삭제 팝업 노출
            // 2. focus가 버튼 영역에 위치
            //    - 편집모드 여부와 선택된 콘텐츠 갯수를 확인하여 삭제팝업 노출
            //      (편집모드를 확인하는 이유는 편집모드가 아니고, 버튼영역에 focus가 위치하면 연관메뉴 키가 노출되지 않기 때문)
            if (focusMode === 0 || (editMode && selectedCnt !== 0)) { // 콘텐츠에 focus가 위치 시에
                isDeleted = true;
            }

            if (isDeleted) {
                if (selectedCnt === 0) { // 선택된 콘텐츠가 없으면 focus가 위치한 콘텐츠 삭제
                    deleteList.push({title: contentList[focus].contsName});
                    deleteIdxList.push(focus);
                } else {
                    for (var i = 0; i < contentList.length; i++) {
                        if (divList.eq(i).hasClass("select")) {
                            deleteList.push({title: contentList[i].contsName});
                            deleteIdxList.push(i);
                        }
                    }
                }
            }

            LayerManager.activateLayer({
                obj: {
                    id: "EditModeDeletePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        type: 1,
                        items: deleteList,
                        callback: function(res) {
                            LayerManager.historyBack();
                            if (res) usedRecentlyListManager.removeContentsList(deleteIdxList, function(result) {
                                if (result === true) {
                                    usedRecentlyListManager.getRecentlyData(function(data) {
                                        if (data.length > 0) {
                                            contentList = data;
                                            constructHTML();
                                            // focus = curPage * 10;
                                            curPage = 0;

                                            // 편집 모드인 경우 리스트 재정렬 및 포커스 reset
                                            if (editMode) {
                                                focus = 0;
                                            }
                                            setFocus(focus);
                                            editMode = false;
                                            instance.div.toggleClass('editMode', false);
                                            //instance.div.find(".context_menu_indicator").text("편집 모드" + (editMode ? " 종료" : ""));
                                        } else LayerManager.deactivateLayer({id: instance.id});
                                        showToast("최근 시청 목록에서 삭제되었습니다");
                                    });
                                }
                            });
                        }
                    }
                },
                moduleId: "module.family_home",
                visible: true
            });
        };
    };

    myHome.layer.home_recentlyList.prototype = new Layer();
    myHome.layer.home_recentlyList.prototype.constructor = myHome.layer.home_recentlyList;

    //Override create function
    myHome.layer.home_recentlyList.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_recentlyList.prototype.handleKeyEvent = function(key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["MyHomeRecentlyList"] = myHome.layer.home_recentlyList;
})();