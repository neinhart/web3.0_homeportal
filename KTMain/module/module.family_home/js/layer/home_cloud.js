/**
 * 마이 소장용VOD
 * (우리집 맞춤 TV > 마이메뉴 > 마이 소장용VOD)
 * Created by ksk91_000 on 2016-08-09.
 */
(function () {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_cloud = function HomeCloud(options) {
        Layer.call(this, options);
        var data;
        var focus = 0;
        var page = 0;
        var maxPage;
        var indicator;
        var _this = this;

        var posterMaxCnt = 5;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_cloud"});

            indicator = new Indicator(7);
            this.div.html(
                "<div class='background'>" +
                '<img id="defaultBg" src="' + modulePath + 'resource/image/cloud/iframe_myvod_default.jpg" alt="background">' +
                '<div id="bgContainer"></div>' +
                '<img id="dvdBgCover" src="' + modulePath + 'resource/image/cloud/mask_cloud.png">' +
                '<span id="backgroundTitle">마이 소장용VOD</span>' +
                '<span id="backgroundDescription">가입 기간 내 무제한 시청 가능하며, 올레 tv 모바일 앱에서도 시청할 수 있습니다</span>' +
                '</div>' +
                '<div id="DescriptionArea">' +
                '<span id="contentTitle"/>' +
                '<span id="contentPrInfo"/>' +
                '<span id="contentInfo">' +
                '<span id="ollehText">olleh tv 별점</span>' +
                '<span id="contentRate"/>' +
                '<span id="contentMark"/>' +
                '</span>' +
                '</div>' +
                '<div id="ListArea">' +
                '<div id="indicator"></div>' +
                '<div id="vodList"></div>' +
                '</div>');
            this.div.find("#contentRate").append(stars.getView(stars.TYPE.RED_29));
            this.div.find("#indicator").append(indicator.getView()).append(indicator.getPageTransView());

            LayerManager.startLoading({preventKey: true});
            myHome.amocManager.getBuyHistoryNxt("saId=" + DEF.SAID + "&adultYn=A&opt=1&itemCnt=A&deletedItemYn=Y&dvdYn=Y&pkgVodYn=Y", function (result, _data) {
                LayerManager.stopLoading();
                if (result) {
                    data = [];
                    if (_data.buyHistory == null) _data = [];
                    else _data = Array.isArray(_data.buyHistory) ? _data.buyHistory : [_data.buyHistory];
                    for (var i = 0; i < _data.length; i++) if (_data[i].viewYn == "Y") data.push(_data[i]);
                    if (data.length <= 0) {
                        if (cbCreate) cbCreate(false);
                        LayerManager.activateLayer({
                            obj: {
                                id: "MyHomeNoCloudPopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP
                            },
                            moduleId: "module.family_home",
                            visible: true
                        });
                    } else {
                        maxPage = Math.ceil(data.length / posterMaxCnt);
                        indicator.setSize(maxPage);
                        constructHTML();
                        setFocus(focus);
                        if (cbCreate) cbCreate(true);
                    }
                } else callback(false)
            });
        };

        var constructHTML = function () {
            var html = _$("<ul/>");
            for (var i = 0; i < data.length; i++) {
                html.append(getPosterItem(i, data[i]));
            }

            _this.div.find("#vodList").empty().append(html);
        };

        var getPosterItem = function (index, data) {
            var html;
            html = "<li id='content_" + index + "'>" +
                "<div class='imgWrapper'>";
            html += "<img src='" + data.imgUrl + "' class='posterImg'>" +
                "<img src='" + modulePath + "resource/image/cloud/cover_img_w273.png' class='dvdCoverImg'>";
            html += "<div class='icon_area'/>";
            html += "</div>" + "</li>";
            var div = _$(html);
            setIcon(div.find(".icon_area"), data);

            return div;
        };

        function setIcon(div, data) {
            div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
            switch (data.newHot) {
                case "N":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"}));
                    break;
                case "U":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"}));
                    break;
                case "X":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"}));
                    break;
                case "B":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"}));
                    break;
                case "R":
                    div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"}));
                    break;
            }
            var posterIconCount = 0;
            if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
                posterIconCount++;
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
            }
            if (data.isHd == "UHD") {
                posterIconCount++;
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
            }
            if (data.isDvdYn == "Y" && posterIconCount < 2) {
                div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
            }
        }

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT :
                    if (focus % posterMaxCnt == 0) {
                        LayerManager.historyBack();
                        return true;
                    } else setFocus(focus - 1);
                    return true;
                case KEY_CODE.RIGHT :
                    setFocus(HTool.getIndex(focus, 1, data.length));
                    return true;
                case KEY_CODE.UP :
                    if (HTool.getIndex(focus, (-1 * posterMaxCnt), maxPage * posterMaxCnt) > data.length - 1) setFocus(data.length - 1);
                    else setFocus(HTool.getIndex(focus, (-1 * posterMaxCnt), maxPage * posterMaxCnt));
                    return true;
                case KEY_CODE.DOWN :
                    if (HTool.getIndex(focus, posterMaxCnt, maxPage * posterMaxCnt) > data.length - 1) setFocus(data.length - 1);
                    else setFocus(HTool.getIndex(focus, posterMaxCnt, maxPage * posterMaxCnt));
                    return true;
                case KEY_CODE.RED:
                    if (focus % posterMaxCnt === 0) return this.controlKey(KEY_CODE.UP);
                    else setFocus(focus - (focus % posterMaxCnt));
                    return true;
                case KEY_CODE.BLUE:
                    if (maxPage <= 1) return true;
                    if (focus === data.length - 1) setFocus(posterMaxCnt - 1);
                    else if (focus % posterMaxCnt === posterMaxCnt - 1) return this.controlKey(KEY_CODE.DOWN);
                    else setFocus(Math.min(data.length - 1, page * posterMaxCnt + (posterMaxCnt - 1)));
                    return true;
                case KEY_CODE.ENTER :
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data[focus].catId, data[focus].itemType == 1 ? data[focus].catId : data[focus].contsId, "03");
                    } catch (e) {
                    }
                    selectItem(data[focus]);
                    return true;
            }
            return false;
        };

        function selectItem(data) {
            if (data.adultOnlyYn == "Y") {
                openAdultAuthPopup(function () {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.catId, data.contsId, "03");
                    } catch (e) {
                    }
                    openDetailLayerImpl(data, true);
                }, true);
            } else {
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.catId, data.contsId, "03");
                } catch (e) {
                }
                openDetailLayerImpl(data, false);
            }
        }

        var openDetailLayerImpl = function (data, authComplete) {
            LayerManager.startLoading({preventKey: true});
            if (data.pkgYn == "K") {
                ModuleManager.getModuleForced("module.vod", function (module) {
                    module.execute({method: "showDetailForPkgProdCd", params: {prdtCd: data.contsId, req_cd: "03"}});
                });
            } else if (data.pkgYn == "Y" || data.seriesYn == "Y") {
                if (data.catId == "N") {
                    myHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=B&platformGubun=W", function (res, data2) {
                        if (res) {
                            if (data2.serviceYn == "Y" || isTimeRemain(data.duration)) openDetailLayer(data2.catId, data2.catId, "03", {isAuthorizedContent: authComplete});
                            else {
                                LayerManager.stopLoading();
                                HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                            }
                        } else {
                            LayerManager.stopLoading();
                            extensionAdapter.notifySNMPError("VODE-00013");
                            HTool.openErrorPopup({
                                message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                                reboot: true
                            });
                        }
                    });
                } else if (data.pkgYn == "Y") {
                    openDetailLayer(data.contsId, data.contsId, "03", {isAuthorizedContent: authComplete});
                } else {
                    openDetailLayer(data.catId, data.contsId, "03", {isAuthorizedContent: authComplete});
                }
            } else {
                myHome.amocManager.getContentW3(function (res, contData) {
                    if (res) {
                        if (contData.contsId) openDetailLayer(contData.catId, data.contsId, "03", {
                            contsInfo: contData,
                            isAuthorizedContent: authComplete,
                            noCateForced: true
                        });
                        else if (data.catId && data.catId !== "N") data.catId = "N", openDetailLayerImpl(data, authComplete);
                        else if (isTimeRemain(data.duration)) openDetailLayer("X", data.contsId, "03", {
                            isAuthorizedContent: authComplete,
                            noCateForced: true
                        });
                        else {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                        }
                    } else {
                        LayerManager.stopLoading();
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({
                            message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                            reboot: true
                        });
                    }
                }, data.catId || "N", data.contsId, DEF.SAID);
            }
        };

        var setFocus = function (newFocus) {
            _this.div.find("#content_" + focus).removeClass('focus');
            focus = newFocus;
            _this.div.find("#content_" + focus).addClass('focus');
            _this.div.find('#contentTitle').text(data[focus].contsName);
            tryChangeBg(data[focus].dvdBImgUrl);
            setRate(data[focus].mark); // 별점(star)
            setPrInfo(data[focus].prInfo); // 연령 아이콘
            setPage(Math.floor(focus / posterMaxCnt));
        };

        var setPage = function (newPage) {
            if (page == newPage) return;
            page = newPage;
            // 페이지 이동 간격 : 419 + (margin-bottom)
            _this.div.find("#vodList").css("-webkit-transform", "translateY(-" + 458 * page + "px)");
            indicator.setPos(page);
        };

        var setPrInfo = function (prInfo) {
            _this.div.find("#contentPrInfo").html("<img src='" + modulePath + "resource/image/icon/icon_age_detail_" + ServerCodeAdapter.getPrInfo(prInfo - 0) + ".png' class='age'>");
        };

        function setRate(count) {
            var view = _this.div.find("#contentRate .rating_star_area");
            stars.setRate(view, count)

            var numberArea = _this.div.find("#contentMark");
            numberArea.empty();

            if (count != null && Math.ceil(count) > 0) {
                // 별점 숫자 표기
                var numberSrc = [];
                numberSrc.push(modulePath + "resource/image/rating/number/rating_vod_number_" + (count.split(".")[0] || 0) + ".png");
                numberSrc.push(modulePath + "resource/image/rating/number/rating_vod_number_" + (count.split(".")[1] || 0) + ".png");
                numberArea.append(_$("<div/>", {class: "ratingNumber"}).css("background-image", "url(" + numberSrc[0] + ")"));
                numberArea.append(_$("<div/>", {class: "ratingNumber dot"}));
                numberArea.append(_$("<div/>", {class: "ratingNumber"}).css("background-image", "url(" + numberSrc[1] + ")"));
            }
        }

        function isTimeRemain(str) {
            if (str == "PERM") return true;
            if (str.indexOf("-") >= 0) return false;
            return true;
        }

        var changeBgTimeout = null;

        function tryChangeBg(imgUrl) {
            if (changeBgTimeout) {
                clearTimeout(changeBgTimeout);
                changeBgTimeout = null;
            }

            changeBgTimeout = setTimeout(function () {
                changeBg(imgUrl);
            }, 500);
        }

        function changeBg(imgUrl) {
            var bgContainer = _this.div.find('#bgContainer')[0];
            var newBg = document.createElement('img');
            var oldBg = bgContainer.lastChild;
            newBg.src = imgUrl;
            bgContainer.appendChild(newBg);

            function transitionEnd() {
                newBg.removeEventListener('webkitTransitionEnd', transitionEnd);
                while (bgContainer.childNodes.length > 1) {
                    bgContainer.removeChild(bgContainer.firstChild);
                }
            }

            webkitRequestAnimationFrame(function () {
                newBg.addEventListener('webkitTransitionEnd', transitionEnd);
                setTimeout(function () {
                    if (!imgUrl && oldBg) oldBg.style.opacity = 0;
                    newBg.style.opacity = 1;
                    _this.div.toggleClass("hasImgBg", !!imgUrl);
                }, 100);
            });
        }

        this.show = function () {
            if (this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);
            Layer.prototype.show.call(this);
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };
    };

    myHome.layer.home_cloud.prototype = new Layer();
    myHome.layer.home_cloud.prototype.constructor = myHome.layer.home_cloud;

    //Override create function
    myHome.layer.home_cloud.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_cloud.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeCloud"] = myHome.layer.home_cloud;
})();