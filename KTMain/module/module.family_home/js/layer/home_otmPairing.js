/**
 *  Copyright (c) 2018 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>home_otmPairing</code>
 *
 * @author sw.nam
 * @since 2018-01-06
 *
 * OTM(올레 TV 모바일) Pairing Layer
 *
 */

(function() {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_otmPairing = function OtmPairing(options) {
        Layer.call(this, options);

        var cateData;
        var mainclock;
        var instance = this;
        var util = window.UTIL;

        var storageManager = window.StorageManager;

        // 전체 화면 view 관리
        var otmManageView;

        // 리스트 영역, 우측 버튼 영역, 연관메뉴 키 및 기타 text 영역관리
        var otmListArea;
        var rightMenuArea;
        var subArea;
        var contextKeyArea;

        // 현재 연결중인 영역
        var isConnectingListArea;
        var itemIsConnecting = [];

        // 연결되었었던 영역 ( 히스토리 영역 )
        var wasConnectedListArea;
        var itemWasConnected = [];

        var OTMPManager = window.OTMPManager;

        var menuType = 0;
        var rightBtnIdx = 0;
        var listFocusIdx = 0;

        var listBtnIdx = 1;

        var MENU_TYPE = {
            LIST_MENU: 0,
            RIGHT_MENU: 1
        };

        var currentPage =0;
        var maxPage;

        // 최대 연결 가능한 otm 갯수
        var MAX_NUMBER_OF_CONNECTABLE_MOBILES = 3;

        // 리스트 관리용 변수
        var isConnectingList = [];
        var wasConnectedList = [];

        var scrollArea;
        var scroll;
        var colorKeyArea;

        var indicator;

        // 2018.01.08 [sw.nam] 페어링 API 가 현재 개발되지 않아 임시 리스트로 사용하기 위한 테스트 플래그
        var TEST_FLAG = false;

        // 2018.03.04 sw.nam
        // 초기 화면 노출 시 응답에 의한 처리 떄문에 UI가 잘못 보여짐을 방지하기 위한 초기 flag
        var isFirstShowing = false;

        var PAIRING_INFO = {
            code: "S0000",
            message: "성공",
            data: {
                pairing_list: [
                    {
                        nick: "사과",
                        pairing_id: "123123",
                        suid: "1111",
                        master_yn: "Y",
                        "phone_corp": "KTF",
                        "phone_no": "01099998888"
                    },
                    {
                        nick: "바나나",
                        pairing_id: "234567",
                        suid: "222",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01085928592"
                    },
                    {
                        nick: "토마토",
                        pairing_id: "345678",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01088256425"
                    }
                ]
            }
        };
        var PAIRING_HISTORY_INFO = {
            code: "S0000",
            message: "성공",
            data: {
                pairing_hist_list: [
                    {
                        nick: "딸기_KT",
                        pairing_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01057284850"
                    },
                    {
                        nick: "복숭아_SKT",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "SKT",
                        "phone_no": "01098173425"
                    },
                    {
                        nick: "메론_LGT",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "LGT",
                        "phone_no": "01088599992"
                    },
                    {
                        nick: "수박_SKT",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "SKT",
                        "phone_no": "01075924849"
                    },
                    {
                        nick: "포도_KTF",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "77520581929"
                    },
                    {
                        nick: "참외_KTF",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01000000000"
                    },
                    {
                        nick: "오렌지_KTF",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01000000000"
                    },
                    {
                        nick: "귤_KTF",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "1234567777"
                    },
                    {
                        nick: "낑깡_KTF",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01012345678"
                    },
                    {
                        nick: "홍시_KTF",
                        paring_hist_id: "asflkajflakaskj",
                        "suid": "suid",
                        master_yn: "N",
                        "phone_corp": "KTF",
                        "phone_no": "01041234242"
                    },
                ]
            }

        };

        var pairingInfoResult;

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_otmPairing"});

            this.div.html("<div class='background'>" +
                "<div id='defaultBg' style = 'position: absolute; left:0px; top: 0px; width: 1920px; height: 1080px; background-color: rgba(0,0,0,0.9);' alt='background' ></div>" +
                "<span id='backgroundTitle'>올레 tv 모바일 연결 관리</span>" +
                "<div id='indicator' style = 'position: absolute; left: 1351px; top: 123px;' ></div>" +
                "</div>"
            );

            // create otm pairing view
            createOtmElement(this.div);
            this.div.find("#indicator").append(indicator.getView());
            this.div.find(".rightMenuDescriptionTxt").html("마스터 계정은 모바일<br>시청을 지원하는 콘텐츠를<br>자유롭게 이어볼 수 있습니다." +
                "<br>마스터 계정이 아닌 경우<br>월정액 이어보기와 일부<br>콘텐츠에 대한 이어보기가<br>제한됩니다");

            cbCreate(true);

            // seamless message listener 등록



        };

        function pairingStateChangedListener(options) {
            log.printDbg("pairingStateChangedListener() "+options);
            getAllPairingInfoByPushMessage();
        }

        function createOtmElement(parentDiv) {
            log.printDbg("createOtmElement()");

            otmManageView = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "otmManageView",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 1920, height: 1080  // width: KTW_ENV.RESOLUTION.WIDTH, height: KTW_ENV.RESOLUTION.HEIGHT,
                    }
                },
                parent: parentDiv
            });

            // 좌측 리스트 영역 (연결된 + 히스토리 영역 포함)
            otmListArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "otmListArea",
                    css: {
                        position: "absolute", left: 133, top: 121, width: 1168, height: 838, overflow: "hidden"
                    }
                },
                parent: otmManageView
            });


            // 현재 연결된 리스트 관리 영역

            // 연결된 리스트가 없는 경우
            //util.makeElenet
            //연결 리스트
            isConnectingListArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "isConnectingListArea",
                    css: {
                        position: "relative", left: 0, top: 0
                    }
                },
                parent: otmListArea
            });
            // 아이템 위에 붙는 text 및 보더
            var descArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "descArea",
                    css: {
                        position: "relative", left: 0, top: 0, width: 1168, height: 38
                    }
                },
                parent: isConnectingListArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "box",
                    css: {
                        position: "absolute", width: 1168, height: "inherit", "border-bottom" : "solid 5px rgba(178,177,177,0.25)", "box-sizing": "border-box"
                    }
                },
                parent: descArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "numText",
                    css: {
                        position: "absolute",left:0, top: 0, width: 150, height: 25, "font-size": 22, "font-family" : "RixHead L", "letter-spacing" : -1.1, color : "rgba(255,255,255,0.3)"
                    }
                },
                text: "총 n개",
                parent: descArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "totalNumText",
                    css: {
                        position: "absolute", right: 0, top: 0, width: 200, height: 25, "font-size": 22, "font-family" : "RixHead L", "letter-spacing" : -1.1, color : "rgba(255,255,255,0.3)"
                    }
                },
                text: "최대 3개 동시연결 가능",
                parent: descArea
            });

            ////////////////////////
            // 연결된 리스트가 없는 경우
            var noItem = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "noItem",
                    css: {
                        position: "relative", left: 0, width: 1168, height: 160
                    }
                },
                parent: isConnectingListArea
            });

            var itemBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "itemBox",
                    css: {
                        position: "absolute", left: 0, top: 0, width: "inherit", height: "inherit", "box-sizing": "border-box", "border-bottom": "solid 5px rgba(178,177,177,0.25)"
                    }
                },
                parent: noItem
            });

            var textArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "textArea",
                    css: {
                        position: "absolute", left: 0, top: 64, width: 1000, height: 35
                    }
                },
                parent: noItem
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "text",
                    css: {
                        position: "absolute", left: 0, top: 0, height: 35, "margin-right": 13, "font-size": 33, color: "white", opacity: 0.5, "letter-spacing" : -1.65
                    }
                },
                text: "현재 연결된 계정이 없습니다",
                parent: textArea
            });


            // 히스토리 영역
            wasConnectedListArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "wasConnectedListArea",
                    css: {
                        position: "relative", left: 0, top:0
                    }
                },
                parent: otmListArea
            });

            // 아이템 위에 붙는 text 및 보더
            var descArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "descArea",
                    css: {
                        position: "relative", left: 0, top: 0, width: 1168, height: 160
                    }
                },
                parent: wasConnectedListArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "box",
                    css: {
                        position: "absolute", width: "inherit", height: "inherit", "border-bottom" : "solid 5px rgba(178,177,177,0.25)", "box-sizing" : "border-box"
                    }
                },
                parent: descArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "numText",
                    css: {
                        position: "absolute",left:0, bottom: 12, width: 500, height: 25, "font-size": 22, "font-family" : "RixHead L", "letter-spacing" : -1.1, color : "rgba(255,255,255,0.3)"
                    }
                },
                text: "최근 연결한 계정 / 총 n개",
                parent: descArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "description",
                    css: {
                        position: "absolute",right:0, bottom: 12,height: 25, "font-size": 22, "font-family" : "RixHead L", "letter-spacing" : -1.1, color : "rgba(255,255,255,0.3)"
                    }
                },
                text: "계정 이력은 6개월 후 자동 삭제",
                parent: descArea
            });

            //연결 이력이 없는 경우 나오는 box 생성
            ////////////////////////
            // 연결된 리스트가 없는 경우
            var noItem = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "noItem",
                    css: {
                        position: "relative", left: 0, width: 1168, height: 160
                    }
                },
                parent: wasConnectedListArea
            });

            var itemBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "itemBox",
                    css: {
                        position: "absolute", left: 0, top: 0, width: "inherit", height: "inherit", "box-sizing": "border-box", "border-bottom": "solid 5px rgba(178,177,177,0.25)"
                    }
                },
                parent: noItem
            });

            var textArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "textArea",
                    css: {
                        position: "absolute", left: 0, top: 64, width: 1000, height: 35
                    }
                },
                parent: noItem
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "text",
                    css: {
                        position: "absolute", left: 0, top: 0, height: 35, "margin-right": 13, "font-size": 33, color: "white", opacity: 0.5, "letter-spacing" : -1.65
                    }
                },
                text: "최근 연결한 계정이 없습니다",
                parent: textArea
            });

            for (var i = 0; i < 5 ; i++) {
                itemWasConnected[i] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "item",
                        css: {
                            position: "relative", left: 0, width: 1168, height: 160
                        }
                    },
                    parent: wasConnectedListArea
                });

                var listBox = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "itemBox",
                        css: {
                            position: "absolute", left: 0, top: 0, width: "inherit", height: "inherit", "box-sizing": "border-box"
                        }
                    },
                    parent: itemWasConnected[i]
                });

                var nickArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "nickArea",
                        css: {
                            position: "absolute", left: 0, top: 64, width: 500, height: 35
                        }
                    },
                    parent: itemWasConnected[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "name",
                        css: {
                            position: "absolute", left: 0, top: 0, height: 35, "margin-right": 13, "font-size": 33
                        }
                    },
                    text: "이름 없음",
                    parent: nickArea
                });
                var disConnectButtonArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "connectingBtnArea",
                        css: {
                            position: "absolute", left: 685 + 248, top: 46, width: 248, height: 64
                        }
                    },
                    parent: itemWasConnected[i]
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "btnBg",
                        css: {
                            position: "absolute", width: 235, height: 64
                        }
                    },
                    parent: disConnectButtonArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "btnTxt",
                        css: {
                            position: "absolute", top: 18, width: 235, "text-align": "center", "font-size": 30, "letter-spacing": -1.5, "line-height": "32px"
                        }
                    },
                    text: "재연결",
                    parent: disConnectButtonArea
                });

                //포커스 영역
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focusArea",
                        css: {
                            position: "absolute", top: -6, width: 1168, height: 6, "background-color" : "rgb(210,51,47)"
                        }
                    },
                    parent: itemWasConnected[i]
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focusArea",
                        css: {
                            position: "absolute", top: 154, width: 1168, height: 6, "background-color" : "rgb(210,51,47)"
                        }
                    },
                    parent: itemWasConnected[i]
                });
            }

            // 우측 버튼 박스 영역
            rightMenuArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenuArea",
                    css: {
                        position: "absolute", left: 1433, top: 87, width: 420, height: 907
                    }
                },
                parent: otmManageView
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "boxImg_top",
                    src: modulePath + "/resource/image/myplaylist/set_bg_btn_t.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 418, height: 66
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "boxImg_middle",
                    src: modulePath + "/resource/image/myplaylist/set_bg_btn.png",
                    css: {
                        position: "absolute", left: 0, top: 66, width: 418, height: 775
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "boxImg_bottom",
                    src: modulePath + "/resource/image/myplaylist/set_bg_btn_b.png",
                    css: {
                        position: "absolute", left: 0, top: 66 + 775, width: 418, height: 66
                    }
                },
                parent: rightMenuArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "rightMenuDescriptionTxt",
                    css: {
                        position: "absolute", left: 39, top: 79, width: 290, "line-height": "33px", "font-size": 26, "font-family": "RixHead L", "letter-spacing": -1.3, color: "rgba(255,255,255,0.4)"
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "rightMenuDescriptionTxt_2",
                    css: {
                        position: "absolute", left: 39, top: 344, width: 276, "line-height": "31px", "font-size": 24, "font-family": "RixHead L", "letter-spacing": -1.2, color: "rgba(255,255,255,0.4)"
                    }
                },
                text: "※올레 tv 모바일 서비스 해지 시 목록에서 자동 삭제 됩니다",
                parent: rightMenuArea
            });

            var boxBtnArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenuBoxArea",
                    css: {
                        position: "absolute", left: 39, top: 688, width: 282, height: 170 //149
                    }

                },
                parent: rightMenuArea
            });

            // 우측 메뉴 버튼 영역 생성
            var txt = ["계정 추가", "취소"];
            var btn;
            for (var i = 0; i < txt.length; i++) {
                btn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "btnDiv",
                        css: {
                            position: "relative", width: 282, height: 79
                        }
                    },
                    parent: boxBtnArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "btnBg",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 282, height: 66
                        }
                    },
                    parent: btn
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "btnTxt",
                        css: {
                            position: "absolute", left: 0, top: 19, width: 282, "text-align": "center", "font-size": 30, "letter-spacing": -1.5, color: "rgb(255,255,255)"
                        }
                    },
                    text: txt[i],
                    parent: btn
                });
            }

            contextKeyArea =  util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "keyDeleteId",
                    css: {
                        position: "absolute", left: 1648, top: 986, width: 300, height: 45
                    }
                },
                parent: otmManageView
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: modulePath + "resource/image/icon_option_related.png",
                    css: {
                        position: "absolute", width: 33, height: 27
                    }
                },
                parent: contextKeyArea
            });
            util.makeElement({
               tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 45, top: 2, "font-size" : 26, "font-family" : "RixHead L", "letter-spacing" : -1.3, color : "rgba(255,255,255,0.3)"
                    }
                },
                text: "계정 삭제",
                parent: contextKeyArea
            });


            //스크롤 영역

            indicator = new Indicator(1);

            colorKeyArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "colorKey",
                    css: {
                        position: "absolute", left: 0, top: 0,width: 1920, height: 1080
                    }
                },
                parent: otmManageView
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: modulePath + "resource/image/icon_pageup.png",
                    css: {
                        position: "absolute", left: 1343, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: modulePath + "resource/image/icon_pagedown.png",
                    css: {
                        position: "absolute", left: 1371, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "keyText",
                    css: {
                        position: "absolute", left: 1343, top: 939, width: 60, height: 25, "letter-spacing" : -1.1,
                        "font-size" : 22, color: "rgba(255,255,255,0.77)", "font-family" : "RixHead L"
                    }
                },
                text: "페이지",
                parent: colorKeyArea
            });

        }



        /** sw.nam
         * 현재 연결된 리스트는 연결 개수에 따라 리스트 전체 구조(네비게이션 관련) 가 바뀌므로, pairing info 를 가져올 때 마다 지운 뒤 엘레먼트를 새로 생성한다.
         */
        function makeIsConnectingArea() {
            log.printDbg("makeIsConnectingArea()");
            // 현재 연결중인 아이템 리스트 element 생성

            //리스트 초기화
            instance.div.find(".isConnectingListArea .item").remove();
            itemIsConnecting = [];

            if(!isConnectingList) {
                instance.div.find(".isConnectingListArea .descArea .numText").text("총 0개");
                return ;
            }else {
                instance.div.find(".isConnectingListArea .descArea .numText").text("총 "+isConnectingList.length+"개");
            }
            for (var i = 0; i < isConnectingList.length; i++) {
                itemIsConnecting[i] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "item",
                        css: {
                            position: "relative", left: 0, width: 1168, height: 160
                        }
                    },
                    parent: isConnectingListArea
                });

                var itemBox = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "itemBox",
                        css: {
                            position: "absolute", left: 0, top: 0, width: "inherit", height: "inherit", "box-sizing": "border-box"
                        }
                    },
                    parent: itemIsConnecting[i]
                });

                var nickArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "nickArea",
                        css: {
                            position: "absolute", left: 0, top: 64, width: 1000, height: 35
                        }
                    },
                    parent: itemIsConnecting[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "name",
                        css: {
                            position: "absolute", left: 0, top: 0, height: 35, "margin-right": 13, "font-size": 33
                        }
                    },
                    text: "000***0000",
                    parent: nickArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: modulePath + "resource/otmpImage/icon_master.png",
                        class: "icon",
                        css: {
                            position: "absolute", left: 696, width: 31, height: 24
                        }
                    },
                    parent: nickArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "masterTxt",
                        css: {
                            position: "absolute", left: 741, width: 170, height: 33, "font-size": 30, "letter-spacing": -1.5
                        }
                    },
                    text: "마스터 이용 중",
                    parent: nickArea
                });

                var masterBtnArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "masterBtnArea",
                        css: {
                            position: "absolute", left: 685, top: 46, width: 248, height: 64
                        }
                    },
                    parent: itemIsConnecting[i]
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "btnBg",
                        css: {
                            position: "absolute", width: 235, height: 64
                        }
                    },
                    parent: masterBtnArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "btnTxt",
                        css: {
                            position: "absolute", top: 18, width: 235, "text-align": "center", "font-size": 30, "letter-spacing": -1.5, "line-height": "32px"
                        }
                    },
                    text: "마스터로 변경",
                    parent: masterBtnArea
                });

                var disConnectButtonArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "connectingBtnArea",
                        css: {
                            position: "absolute", left: 685 + 248, top: 46, width: 248, height: 64
                        }
                    },
                    parent: itemIsConnecting[i]
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "btnBg",
                        css: {
                            position: "absolute", width: 235, height: 64
                        }
                    },
                    parent: disConnectButtonArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "btnTxt",
                        css: {
                            position: "absolute", top: 18, width: 235, "text-align": "center", "font-size": 30, "letter-spacing": -1.5, "line-height": "32px"
                        }
                    },
                    text: "연결 끊기",
                    parent: disConnectButtonArea
                });

                //포커스 영역
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focusArea",
                        css: {
                            position: "absolute", top: -6, width: 1168, height: 6, "background-color" : "rgb(210,51,47)"
                        }
                    },
                    parent: itemIsConnecting[i]
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focusArea",
                        css: {
                            position: "absolute", top: 154, width: 1168, height: 6, "background-color" : "rgb(210,51,47)"
                        }
                    },
                    parent: itemIsConnecting[i]
                });

            }
        }


        /**
         * 2018.03.04 sw.nam
         * 페어링 리스트 & 히스토리 리스트 요청 시 응답 처리를 위해 loading dialog 가 필요하다.
         * show 가 불리는 상황은 2가지 상황
         * 1. 처음 화면에 진입 할 때
         *  -> 무조건 getPairing& history list를 호출하는 상황이므로 , 타임아웃(2초) 정도 두고 로딩 바 돌린다.
         *  -> 응답을 받으면 stop loadinglayer 호출 (cbPariringList.. 여기다가 )
         *
         * 2. resume 으로 불릴 때. -> popup 이 닫히면서도 resume 이 불리는지 확인 해봐야 함.
         * -> 신규 계정 추가, 연결되어있던 계정 삭제, 재 연결 후 다시 현재 화면으로 돌아 올 떄,
         * @param options
         */
        this.show = function(options) {
            log.printDbg("show()");
           // mainclock.show(this.div.find(".clock_area"));


            if (this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);

            if (!options || !options.resume) {
                // OTM 페어링 & 히스토리 리스트 받아오기 (테스트 버전인 경우 임의의 리스트를 받아온다
                if(TEST_FLAG) {

                    var PAIRING_INFO = {
                        code: "S0000",
                        message: "성공",
                        data: {
                            pairing_list: [
                                {
                                    nick: "사과",
                                    pairing_id: "123123",
                                    suid: "1111",
                                    master_yn: "Y",
                                    "phone_corp": "KTF",
                                    "phone_no": "01099998888"
                                },
                                {
                                    nick: "바나나",
                                    pairing_id: "234567",
                                    suid: "222",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01085928592"
                                },
                                {
                                    nick: "토마토",
                                    pairing_id: "345678",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01088256425"
                                }
                            ]
                        }
                    };
                    var PAIRING_HISTORY_INFO = {
                        code: "S0000",
                        message: "성공",
                        data: {
                            pairing_hist_list: [
                                {
                                    nick: "딸기_KT",
                                    pairing_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01057284850"
                                },
                                {
                                    nick: "복숭아_SKT",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "SKT",
                                    "phone_no": "01098173425"
                                },
                                {
                                    nick: "메론_LGT",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "LGT",
                                    "phone_no": "01088599992"
                                },
                                {
                                    nick: "수박_SKT",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "SKT",
                                    "phone_no": "01075924849"
                                },
                                {
                                    nick: "포도_KTF",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "77520581929"
                                },
                                {
                                    nick: "참외_KTF",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01000000000"
                                },
                                {
                                    nick: "오렌지_KTF",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01000000000"
                                },
                                {
                                    nick: "귤_KTF",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "1234567777"
                                },
                                {
                                    nick: "낑깡_KTF",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01012345678"
                                },
                                {
                                    nick: "홍시_KTF",
                                    paring_hist_id: "asflkajflakaskj",
                                    "suid": "suid",
                                    master_yn: "N",
                                    "phone_corp": "KTF",
                                    "phone_no": "01041234242"
                                },
                            ]
                        }

                    };

                    isConnectingList = [];//PAIRING_INFO.data.pairing_list;
                    wasConnectedList = [];
                    log.printDbg("isConnectingList = "+JSON.stringify(isConnectingList));
                    log.printDbg("wasConnectedList = "+JSON.stringify(wasConnectedList));
                    for(var i =0 ; i < PAIRING_INFO.data.pairing_list.length; i++) {
                        isConnectingList[isConnectingList.length] = PAIRING_INFO.data.pairing_list[i];
                    }
                    for(var i =0 ; i < PAIRING_HISTORY_INFO.data.pairing_hist_list.length; i++) {
                        wasConnectedList[wasConnectedList.length] = PAIRING_HISTORY_INFO.data.pairing_hist_list[i];
                    }
                    log.printDbg("PAIRING_INFO = "+JSON.stringify(PAIRING_INFO.data.pairing_list));
                    log.printDbg("isConnectingList = "+JSON.stringify(isConnectingList));
                    log.printDbg("PAIRING_HISTORY_INFO = "+JSON.stringify(PAIRING_HISTORY_INFO.data.pairing_hist_list));
                    log.printDbg("wasConnectedList = "+JSON.stringify(wasConnectedList));
                    listFocusIdx = 0;
                    listBtnIdx = 1;
                    setCurrentPage(listFocusIdx);

                }else {
                    // 첫 화면..
                    OTMPManager.activateLoadingForMessage(null);
                    // 첫 화면 로딩 시 페어링 리스트를 받아와야 하므로 초기 화면만 hidden 처리 한다.
                    otmListArea.css({visibility: "hidden"});
                    isFirstShowing = true;
                }

                listFocusIdx = 0;
                rightBtnIdx = 0;
                setFocus(menuType,rightBtnIdx);
                getAllPairingInfo();

            }

            OTMPManager.addPairingStateListener(pairingStateChangedListener);
            Layer.prototype.show.call(this);
        };

        this.hide = function(options) {
            Layer.prototype.hide.call(this);
            OTMPManager.removePairingStateListener(pairingStateChangedListener);
        };

        this.onKeyAction = function(keyCode) {
            log.printDbg("onKeyAction() " + keyCode);
            switch (keyCode) {
                case KEY_CODE.UP :
                    var idx;
                    if (menuType == MENU_TYPE.LIST_MENU) {
                        listFocusIdx--;
                        if (listFocusIdx < 0) {
                            listFocusIdx = isConnectingList.length + wasConnectedList.length - 1;
                        }
                        setCurrentPage(listFocusIdx);
                        idx = listFocusIdx
                    } else if (menuType == MENU_TYPE.RIGHT_MENU) {
                        rightBtnIdx--;
                        if (rightBtnIdx < 0) {
                            rightBtnIdx = 1;
                        }
                        idx = rightBtnIdx;
                    }
                    setFocus(menuType, idx);
                    setListBtnFocus(listFocusIdx, listBtnIdx);
                    break;
                case KEY_CODE.RED :
                    if(isConnectingList.length + wasConnectedList.length < 1) {
                        return ;
                    }
                    if(menuType == MENU_TYPE.RIGHT_MENU) {
                        // 우측 메뉴에서 넘어올 때
                        if(currentPage == 0) {
                            // 현재 페이지가 0번째라면
                            listFocusIdx = 0;
                        }else {
                            var itemNum; // 첫번째 페이지 item 갯수
                            if(isConnectingList.length < 1) {
                                // 연결된게 없으면 3개의 히스토리 내역만 첫 페이지에 존재
                                itemNum = 3;
                            }else {
                                // 하나라도 연결된 게 있으면 첫페이지에는 4개의 아이템을 표시
                                itemNum = 4;
                            }
                            listFocusIdx = listFocusIdx - ((listFocusIdx - itemNum) % 5);// + itemNum;
                            log.printDbg("listFOcusIdx = "+listFocusIdx );
                        }
                        menuType = MENU_TYPE.LIST_MENU;
                        setFocus(menuType, listFocusIdx);
                        setListBtnFocus(listFocusIdx, listBtnIdx);
                        return;
                    }
                    // 연결 갯수에 따라 첫 페이지에 표시되는 목록의 갯수가 매번 다르기 때문에
                    // 분리해서 따로 처리를 해 주어야 함.

                    if(maxPage > 1) {
                        // 2페이지 이상 존재하는 경우
                        if(currentPage == 0 && listFocusIdx !== 0) {
                            // 첫번째 페이지 이면서 0번째 index  에 위치하지 않은 경우
                            // 현재 인덱스의 가장 위로 이동 (0번째 index)
                            listFocusIdx = 0;
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);
                            return;

                        }else if(currentPage == 0 && listFocusIdx == 0) {
                            //첫 페이지의 가장 최상위에 위치한 경우.
                            // 마지막 페이지의 가장 최상위로 위치시킨다.
                            var itemNum; // 첫번째 페이지 item 갯수
                            if(isConnectingList.length < 1) {
                                // 연결된게 없으면 3개의 히스토리 내역만 첫 페이지에 존재
                                itemNum = 3;
                            }else {
                                // 하나라도 연결된 게 있으면 첫페이지에는 4개의 아이템을 표시
                                itemNum = 4;
                            }
                            // 먼저 최하위로 포커스를 맞추고
                            log.printDbg("listFocusIdx = "+listFocusIdx);
                            listFocusIdx = isConnectingList.length + wasConnectedList.length -1;
                            listFocusIdx = listFocusIdx - ((listFocusIdx - itemNum) % 5);
                            log.printDbg("listFocusIdx = "+listFocusIdx);
                            setCurrentPage(listFocusIdx);
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);

                        }else if(currentPage !==0) {
                            // 첫번째 페이지가 아닌 경우
                            var itemNum; // 첫번째 페이지 item 갯수
                            if(isConnectingList.length < 1) {
                                // 연결된게 없으면 3개의 히스토리 내역만 첫 페이지에 존재
                                itemNum = 3;
                            }else {
                                // 하나라도 연결된 게 있으면 첫페이지에는 4개의 아이템을 표시
                                itemNum = 4;
                            }
                            // 현재 페이지의 가장 최상위로 위치시킨다.
                            // but , 이미 현재 페이지의 가장 최 상위에 있다면.
                            if((listFocusIdx -itemNum) % 5 == 0) {
                                // 위 페이지의 제일 상위 idx로 위치시킨다.
                                listFocusIdx = listFocusIdx - 5;
                                if(listFocusIdx < 0) {
                                    listFocusIdx = 0;
                                }
                                setCurrentPage(listFocusIdx);
                            }else {
                                listFocusIdx = listFocusIdx - ((listFocusIdx - itemNum) % 5);// + itemNum;
                            }
                            log.printDbg("listFocusIdx = "+listFocusIdx);
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);
                        }
                    }else {
                        // 1페이지만 있는 경우
                        // 현재 인덱스의 가장 위로 이동 (0번째 index)
                        listFocusIdx = 0;
                        setFocus(menuType, listFocusIdx);
                        setListBtnFocus(listFocusIdx, listBtnIdx);
                        return;
                    }
                    break;
                case KEY_CODE.DOWN :
                    var idx;
                    if (menuType == MENU_TYPE.LIST_MENU) {
                        listFocusIdx++;
                        if (listFocusIdx > (isConnectingList.length + wasConnectedList.length) - 1) {
                            listFocusIdx = 0;
                        }
                        setCurrentPage(listFocusIdx);
                        idx = listFocusIdx;
                    } else if (menuType == MENU_TYPE.RIGHT_MENU) {
                        rightBtnIdx++;
                        if (rightBtnIdx > 1) {
                            rightBtnIdx = 0;
                        }
                        idx = rightBtnIdx
                    }
                    setFocus(menuType, idx);
                    setListBtnFocus(listFocusIdx, listBtnIdx);
                    break;
                case KEY_CODE.BLUE:
                    if(isConnectingList.length + wasConnectedList.length < 1) {
                        return ;
                    }
                    if(menuType == MENU_TYPE.RIGHT_MENU) {
                        // 우측 메뉴에서 넘어올 때
                        if(currentPage == 0) {
                            // 현재 페이지가 0번째라면
                            var itemNum;
                            if(isConnectingList.length < 1) {
                                // 연결 된게 없는 경우
                                if(wasConnectedList.length > 3) {
                                    itemNum = 3;
                                }else {
                                    itemNum = wasConnectedList.length;
                                }
                            }else {
                                // 1개 이상 연결 된 경우
                                if(isConnectingList.length + wasConnectedList.length > 4) {
                                    itemNum = 4;
                                }else {
                                    itemNum = isConnectingList.length + wasConnectedList.length;
                                }

                            }
                            listFocusIdx = itemNum-1;
                            log.printDbg("listFocusIdx = "+listFocusIdx);
                            menuType = MENU_TYPE.LIST_MENU;
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);

                        }else {
                            // 0번째 페이지가 아닌 경우
                            var itemNum; // 첫번째 페이지 item 갯수
                            if(isConnectingList.length < 1) {
                                // 연결된게 없으면 3개의 히스토리 내역만 첫 페이지에 존재
                                itemNum = 3;
                            }else {
                                // 하나라도 연결된 게 있으면 첫페이지에는 4개의 아이템을 표시
                                itemNum = 4;
                            }
                            listFocusIdx = listFocusIdx + ( 4 - ((listFocusIdx - itemNum) % 5));// + itemNum;
                            if(listFocusIdx > isConnectingList.length + wasConnectedList.length -1) {
                                listFocusIdx = isConnectingList.length + wasConnectedList.length -1;
                            }
                            log.printDbg("listFocusIdx = "+listFocusIdx);
                        }
                        menuType = MENU_TYPE.LIST_MENU;
                        setFocus(menuType, listFocusIdx);
                        setListBtnFocus(listFocusIdx, listBtnIdx);
                        return;
                    }
                    // 연결 갯수에 따라 첫 페이지에 표시되는 목록의 갯수가 매번 다르기 때문에
                    // 분리해서 따로 처리를 해 주어야 함.

                    if(maxPage > 1) {
                        // 2페이지 이상 존재하는 경우
                        var itemNum;
                        if(isConnectingList.length < 1) {
                            itemNum = 3;
                        }else {
                            itemNum = 4;
                        }

                        if(currentPage == 0 && listFocusIdx !== itemNum-1) {
                            // 현재 인덱스의 가장 마지막으로 이동 (itemNum-1번째 index)
                            // 첫번째 페이지이 면서 가장 아래 index  에 위치하지 않은 경우
                            listFocusIdx = itemNum-1;
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);
                            return;

                        }else if(currentPage == 0 && listFocusIdx == itemNum-1) {
                            //첫 페이지의 가장 마지막 인덱스에 에 위치한 경우.
                            // 다음 페이지(2페이지)로 이동시킨다.
                            listFocusIdx = listFocusIdx + 5;
                            if(listFocusIdx > isConnectingList.length + wasConnectedList.length -1) {
                                // 두번째 페이지가 마지막 페이지라면.
                                listFocusIdx = isConnectingList.length + wasConnectedList.length - 1;
                            }
                            setCurrentPage(listFocusIdx);
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);
                        }else if(currentPage !==0) {
                            // 첫번째 페이지가 아닌 경우

                            // 현재 페이지의 가장 마지막으로 위치시킨다.
                            // but , 이미 현재 페이지의 가장 마지막에  있다면.
                            if((listFocusIdx - itemNum) % 5 == 4) {
                                // 다음 페이지의 가장 하위 idx로 위치시킨다.
                                listFocusIdx = listFocusIdx + 5;
                                if(listFocusIdx > isConnectingList.length + wasConnectedList.length -1) {
                                    // 마지막 페이지 예외처리
                                    listFocusIdx = isConnectingList.length + wasConnectedList.length - 1;
                                }
                                setCurrentPage(listFocusIdx);
                            }else if(listFocusIdx == isConnectingList.length + wasConnectedList.length - 1) {
                                //  현재 포커스된 idx 가 마지막 인덱스인 경우
                                // 첫번째 페이지의 마지막 인덱스로 이동 시킨다.
                                listFocusIdx = itemNum -1;
                                setCurrentPage(listFocusIdx);
                            } else {
                                listFocusIdx = listFocusIdx + ( 4 - (listFocusIdx - itemNum) % 5)// + itemNum;
                                if(listFocusIdx > isConnectingList.length + wasConnectedList.length -1) {
                                    // 마지막 페이지 예외처리
                                    listFocusIdx = isConnectingList.length + wasConnectedList.length - 1;
                                }
                            }
                            setFocus(menuType, listFocusIdx);
                            setListBtnFocus(listFocusIdx, listBtnIdx);
                        }
                    }else {
                        // 1페이지만 있는 경우
                        // 현재 인덱스의 가장 마지막으로 이동 (itemNum-1 번쨰 idx)
                        listFocusIdx = isConnectingList.length + wasConnectedList.length -1;
                        setFocus(menuType, listFocusIdx);
                        setListBtnFocus(listFocusIdx, listBtnIdx);
                         return;
                    }
                    break;
                case KEY_CODE.LEFT :
                    var idx;
                    if (menuType == MENU_TYPE.RIGHT_MENU) {
                        if(isConnectingList.length + wasConnectedList.length < 1) {
                            LayerManager.deactivateLayer({id: this.id});
                            return true;
                        }
                        menuType = MENU_TYPE.LIST_MENU;

                        rightBtnIdx = 0;
                       // listFocusIdx = 0;
                        listBtnIdx = 1;
                        idx = listFocusIdx;
                        setCurrentPage(listFocusIdx);
                        setFocus(menuType, idx);
                    } else if(menuType == MENU_TYPE.LIST_MENU) {
                        // 현재 페이지의 위치에 따라 나눠줘야 함
                        if(listBtnIdx == 1) {
                            // 연결중인 영역과 히스토리 영역 구분
                            log.printDbg("listFocus idx = "+listFocusIdx);
                            if(instance.div.find(".item:eq("+listFocusIdx+") div").hasClass("masterBtnArea")) {
                                // 연결 중 영역인 경우
                                // 현재 마스터라면 나가기, 마스터가 아니면 마스터 변경 키로 이동.
                                if(instance.div.find(".item:eq("+listFocusIdx+")").hasClass("isMaster")) {
                                    LayerManager.deactivateLayer({id: this.id});
                                    return true;
                                }else {
                                    listBtnIdx = 0;
                                }
                            }else {
                                // 히스토리 영역인 경우
                                LayerManager.deactivateLayer({id: this.id});
                                return true;
                            }
                        }else {
                            LayerManager.deactivateLayer({id: this.id});
                            return true;
                        }
                    }
                    setListBtnFocus(listFocusIdx,listBtnIdx);
                    break;
                case KEY_CODE.RIGHT :
                    var idx;
                    if (menuType == MENU_TYPE.RIGHT_MENU) {
                        return true;
                    } else if(menuType == MENU_TYPE.LIST_MENU) {
                        if(listBtnIdx == 0 ) {
                            listBtnIdx = 1;
                            setListBtnFocus(listFocusIdx,listBtnIdx);
                        }else {
                            menuType = MENU_TYPE.RIGHT_MENU;
                            rightBtnIdx = 0;
                            idx = rightBtnIdx;
                            setFocus(menuType, idx);
                            setListBtnFocus(listFocusIdx,listBtnIdx);

                        }
                    }

                    break;
                case KEY_CODE.OK :
                    if (menuType == MENU_TYPE.LIST_MENU) {
                        //연결 끊기 or 재연결 or 마스터 변경
                        if (listFocusIdx < isConnectingList.length) {
                            if(listBtnIdx == 0) {
                                // 마스터 변경 팝업 호출
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "OTMPChangeMaster",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            title: "마스터 변경",
                                            mainText: "마스터 계정을 변경하시겠습니까?",
                                            fromNick: isConnectingList[0].nick,
                                            toNick: isConnectingList[listFocusIdx].nick,
                                            subText: "마스터 계정은 모바일 시청을 지원하는 콘텐츠를 자유롭게 이어볼 수 있습니다.<br>" +
                                                     "마스터 계정이 아닌 경우 월정액 이어보기와 일부 콘텐츠에 대한 이어보기가 제한됩니다",
                                            callback : function(result) {
                                                if (result) {
                                                    // 마스터 변경
                                                    if(TEST_FLAG) {

                                                        if(isConnectingList != null) {
                                                            for(var i = 0; i < isConnectingList.length; i ++) {
                                                                isConnectingList[i].master_yn = "N";
                                                            }
                                                            isConnectingList[listFocusIdx].master_yn = "Y";
                                                            isConnectingList.sort(function(a,b) {
                                                                return a.master_yn > b.master_yn ? -1 : a.master_yn < b.master_yn ? 1: 0;
                                                            });
                                                        }

                                                        var ttl = "변경 완료";
                                                        var msg = "마스터 계정이 변경되었습니다";

                                                        activateBasicPopupLayer(ttl,msg);
                                                        getAllPairingInfo();
                                                    }else {
                                                        OTMPManager.setPairingMaster(function(result) {
                                                            if(result) {
                                                                requestPairingInfoToServer();
                                                            }
                                                        },isConnectingList[listFocusIdx].pairing_id,isConnectingList[listFocusIdx].suid)
                                                    }

                                                }
                                            }
                                        }
                                    },
                                    moduleId: "module.family_home",
                                    visible: true
                                });


                            }else {
                                //연결 끊기
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "OTMPDisconnect",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            title: "연결 끊기",
                                            mainText: "올레 TV 모바일과 연결을 끊으시겠습니까?",
                                            nickName: isConnectingList[listFocusIdx].nick,
                                            subText: "연결을 끊으면 올레 tv 모바일에서<br>구매한 VOD와 월정액 상품을 올레 tv에서 이용할 수 없습니다",
                                            callback : function(result) {
                                                if (result) {
                                                    // 연결 해제 (기존 연결 기기, 연결 내역에 남김
                                                    // 현재 포커스 된 기기의 연결을 해지한다.
                                                    var isHistory = true;
                                                    if(TEST_FLAG) {
                                                        var ttl = "연결 끊기 완료";
                                                        var msg = "올레 tv 모바일 연결 끊기가 완료되었습니다";
                                                        //// 팝업 노출
                                                        activateBasicPopupLayer(ttl,msg);

                                                        wasConnectedList.push(isConnectingList[listFocusIdx]);
                                                        isConnectingList.splice(listFocusIdx,1);
                                                        log.printDbg("isConnectingList splice = "+JSON.stringify(isConnectingList));
                                                        if(isConnectingList.length > 0) {
                                                            for(var i = 0; i < isConnectingList.length; i ++) {
                                                                isConnectingList[i].master_yn = "N";
                                                            }
                                                            isConnectingList[0].master_yn = "Y";
                                                        }
                                                        getAllPairingInfo();
                                                    }else {
                                                        OTMPManager.removePairing(function(result) {
                                                            if(result) {
                                                                // 삭제 완료했으므로 리스트 업데이트 (심리스 푸시 메시지로 처리하므로 아래 로직은 주석)
                                                                // 심리스 메시지 올 떄 까지 로딩 activate
                                                              //  OTMPManager.activateLoadingForMassage(null);
                                                            }
                                                        },false,false,isHistory,isConnectingList[listFocusIdx].pairing_id);
                                                    }
                                                }
                                            }
                                        }

                                    },
                                    moduleId: "module.family_home",
                                    visible: true
                                });
                            }

                        } else {
                            //재연결
                            log.printDbg("reconnect");
                            // 현재 연결된 기기가 최대(3개) 인 경우 연결 해지 팝업을 띄운다.
                            if (isConnectingList.length >= 3) {
                                var ttl = "알림";
                                var msg = "계정은 최대 3개까지 등록 가능합니다<br>기존 계정의 연결을 끊은 후 재시도해주세요";

                                OTMPManager.openBasicPopup(ttl,msg, [{name: "확인", callback: "close"}]);

                            }else {
                                var selectedHistPairing = wasConnectedList[listFocusIdx - isConnectingList.length];
                                /// 재 연결 팝업 호출
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "OTMPReconnecting",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            selectedHistItem: selectedHistPairing,
                                            callback: function(result) {
                                                if(result) {
                                                    if(TEST_FLAG) {
                                                        // 팝업 띄운다.
                                                        var title = "연결 완료";
                                                        var msg = "사용 중이신 올레 tv 모바일과<br>올레 tv가 연결되었습니다";
                                                        var subMsg = "마스터 계정은 모바일 시청을 지원하는 콘텐츠를 자유롭게 이어볼 수 있습니다.<br>" +
                                                            "마스터 계정이 아닌 경우 월정액 이어보기와 일부 콘텐츠에 대한 이어보기가 제한됩니다";
                                                        OTMPManager.openBasicPopup(title, msg, [{
                                                            name: "확인",
                                                            callback: "close"
                                                        }], subMsg);

                                                        //UI 정리
                                                        var length = isConnectingList.length;

                                                        isConnectingList.push(wasConnectedList[listFocusIdx - isConnectingList.length]);
                                                        wasConnectedList.splice(listFocusIdx - length, 1);
                                                        if (isConnectingList.length > 0) {
                                                            for (var i = 0; i < isConnectingList.length; i++) {
                                                                isConnectingList[i].master_yn = "N";
                                                            }
                                                            isConnectingList[0].master_yn = "Y";
                                                        }
                                                        getAllPairingInfo();
                                                    }else {
                                                        // 심리스 푸쉬 메시지 대기
                                                       // OTMPManager.activateLoadingForMassage(null);
                                                    }
                                                }else {

                                                }
                                            }
                                        }
                                    },
                                    moduleId: "module.family_home",
                                    visible: true
                                });


                 /*                  // 재연결할 히스토리 목록의 suid와 hist_id를 넘겨줘야 함
                                 OTMPManager.makePairingHistory(function(result) {
                                 if(result) {
                                 getAllPairingInfo();
                                 }
                                 }, selectedHistPairing.suid, selectedHistPairing.pairing_hist_id);*/
                            }
                        }

                    } else if (menuType == MENU_TYPE.RIGHT_MENU) {
                        if (rightBtnIdx == 0) {

                            if (isConnectingList.length >= 3) {
                                var ttl = "알림";
                                var msg = "계정은 최대 3개까지 등록 가능합니다<br>기존 계정의 연결을 끊은 후 재시도해주세요";
                                OTMPManager.openBasicPopup(ttl,msg, [{name: "확인", callback: "close"}]);
                            }else {
                                // 계정 추가
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "OTMPConnecting",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true
                                    },
                                    moduleId: "module.family_home",
                                    visible: true
                                });
                            }

                        } else {
                            // 나가기
                            LayerManager.deactivateLayer({id: this.id});
                            return true;
                        }
                    }
                    return true;
                    break;
                case KEY_CODE.CONTEXT :
                    if (menuType == MENU_TYPE.LIST_MENU) {
                        // 계정 삭제
                        if (listFocusIdx < isConnectingList.length) {
                            // 연결 끊고 계정 삭제 (완전 삭제)
                            log.printDbg("deletePairing");
                            var selectedPairingItem = isConnectingList[listFocusIdx];
                            LayerManager.activateLayer({
                                obj: {
                                    id: "OTMPDisconnect",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        title: "계정 삭제",
                                        mainText: "올레 TV 모바일 연결 내역에서 영구적으로 삭제하시겠습니까?",
                                        nickName: selectedPairingItem.nick,
                                        subText: "계정을 삭제하면 올레 tv 모바일에서 구매한<br>VOD와 월정액 상품을 올레 tv에서 이용할 수 없습니다<br>" +
                                        "또한 최근 연결한 목록에서도 삭제되어 추후 페어링 시 재인증이 필요합니다",
                                        callback: function (result) {
                                            if(result) {
                                                if(TEST_FLAG) {

                                                    ttl = "연결 삭제 완료";
                                                    msg = "올레 tv에서 해당 계정이 삭제되었습니다";
                                                    activateBasicPopupLayer(ttl,msg);
                                                    isConnectingList.splice(listFocusIdx,1);
                                                    log.printDbg("isConnectingList splice = "+JSON.stringify(isConnectingList));
                                                    var len = isConnectingList.length;
                                                    if(isConnectingList && len && len > 0) {
                                                        isConnectingList[0].master_yn = "Y";
                                                    }
                                                    getAllPairingInfo();
                                                }else {
                                                    OTMPManager.removePairing(function(result) {
                                                        // result : 결과값 리턴함, 필요한 경우에만 사용 (일반적인 결과 팝업은 OTMPManager 에서 호출 )
                                                        // 예를 들어, 팝업이 아닌 화면상에 무언가를 갱신해줘야 하는 경우에는 여기에서 갱신하면 될 듯
                                                        if(result) {
                                                            // 페어링 리스트만 다시 불러온다.(푸시 메시지로 처리)
                                                          //  requestPairingInfoToServer();
                                                        }
                                                    }, true,false,false,selectedPairingItem.pairing_id);
                                                }
                                            }
                                        }
                                    }
                                },
                                moduleId: "module.family_home",
                                visible: true
                            });

                        } else {
                            // 히스토리 삭제
                            log.printDbg("deletePairingHistory");
                            var selectedHistPairing = wasConnectedList[listFocusIdx - isConnectingList.length];

                            LayerManager.activateLayer({
                                obj: {
                                    id: "OTMPDisconnect",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        title: "계정 삭제",
                                        mainText: "올레 TV 모바일 연결 내역에서 영구적으로 삭제하시겠습니까?",
                                        nickName: selectedHistPairing.nick,
                                        subText: "계정 삭제 시 올레 tv 모바일의<br>무료 영화 / 실시간 TV / VOD 이어보기 혜택을 받을 수 없습니다.<br>" +
                                        "또한 최근 연결한 목록에서도 삭제되어 추후 페어링 시 재인증이 필요합니다",
                                        callback: function (result) {
                                            if(result) {

                                                if(TEST_FLAG) {
                                                    ttl = "연결 삭제 완료";
                                                    msg = "올레 tv에서 해당 계정이 삭제되었습니다";
                                                    activateBasicPopupLayer(ttl,msg);
                                                    wasConnectedList.splice(listFocusIdx - isConnectingList.length,1);
                                                    //getAllPairingInfo();
                                                    requestPairingHistoryToServer();

                                                }else {
                                                    OTMPManager.removePairingHistory(function(result) {
                                                        if(result) {
                                                            requestPairingHistoryToServer();
                                                        }

                                                    }, selectedHistPairing.suid, selectedHistPairing.pairing_hist_id);
                                                }
                                            }
                                        }
                                    }
                                },
                                moduleId: "module.family_home",
                                visible: true
                            });
                        }
                    }
                    break;
                default :
                    break;

                    return true;
            }
            return false;
        };

        /**
         * 포커스 이동 (리스트 영역 버튼 제외)
         * @param menuType
         * @param focusIdx
         */
        function setFocus(menuType, focusIdx) {
            log.printDbg("setFocus()");
            instance.div.find(".item").removeClass("focus");
            instance.div.find(".rightMenuArea .btnDiv").removeClass("focus");

            var tmpFocus;
            if (menuType == MENU_TYPE.LIST_MENU) {
                if(currentPage == 0) {
                    instance.div.find(".item:eq(" + focusIdx + ")").addClass("focus");
                }else {
                    // 1페이지 이상인 경우
                    if(isConnectingList.length < 1) {
                        // 연결된게 없으면 첫 페이지에 표현 가능한 개수는 3개
                        // 3보다 큰 경우..
                        tmpFocus = Math.floor((focusIdx -3) % 5) + isConnectingList.length;
                    }else {
                        // 연결된게 하나라도 있으면 첫 페이지에 표현 가능한 개수는 4개
                        tmpFocus = Math.floor((focusIdx - 4) % 5) + isConnectingList.length;
                    }
                    log.printDbg("tmpFocus = "+tmpFocus);
                    instance.div.find(".item:eq("  +tmpFocus+ ")").addClass("focus");
                }
                // 계정 삭제 키는 리스트 영역에서만 보이도록
                contextKeyArea.css({visibility: "inherit"});

            } else if (menuType == MENU_TYPE.RIGHT_MENU) {

                instance.div.find(".rightMenuArea .btnDiv:eq(" + focusIdx + ")").addClass("focus");

                // 계정 삭제 키는 리스트 영역에서만 보이도록
                contextKeyArea.css({visibility: "hidden"});
            }
        }

        /**
         * 리스트 버튼 포커스 이동
         * @param focusIdx 포커스된 장치
         * @param buttonIdx 포커스 할 버튼 idx
         */
        function setListBtnFocus(focusIdx,buttonIdx) {
            log.printDbg("setListBtnFocus");
            log.printDbg("btnIdx == "+buttonIdx);
            log.printDbg("focusidx = "+focusIdx);
            listFocusIdx = focusIdx;
            listBtnIdx = buttonIdx;

            var tmpFocus;

            instance.div.find(".item .masterBtnArea").removeClass("focus");
            instance.div.find(".item .connectingBtnArea").removeClass("focus");

            if(menuType == MENU_TYPE.RIGHT_MENU) {
                // 우측 메뉴 영역에선 동작하지 않는다.
                return;
            }

            // 연결 리스트 유무에 따라 현재 페이지 인덱스의 위치가 달라지므로 현재 페이지를 구분한다.
            if(currentPage == 0) {
                // 첫번째 페이지인 경우
                tmpFocus = focusIdx;
            }else {
                // 1페이지 이상인 경우
                if(isConnectingList.length < 1) {
                    // 연결된게 없으면, 첫 페이지에 표현 가능한 갯수는 3개
                    tmpFocus = Math.floor((focusIdx -3) % 5) + isConnectingList.length;
                }else {
                    // 연결된게 하나라도 있으면 첫 페이지에 표현 가능한 개수는 4개
                    tmpFocus = Math.floor((focusIdx - 4) % 5) + isConnectingList.length;
                }
            }

            if(listBtnIdx == 0) {
                // 연결중인 영역과 히스토리 영역을 구분한다.
                if(instance.div.find(".item:eq("+ tmpFocus +") div").hasClass("masterBtnArea")) {
                    // 연결중 영역 인 경우 , 현재 마스터인지 아닌지 구분한다.
                    if(instance.div.find(".item:eq("+tmpFocus+")").hasClass("isMaster")) {
                        // 현재 마스터인 경우 연결 끊기 버튼으로 이동
                        listBtnIdx = 1;
                        instance.div.find(".item:eq(" + tmpFocus + ") .connectingBtnArea").addClass("focus");
                    }else {
                        // 현재 마스터가 아닌 경우, 마스터 버튼으로 포커스
                        instance.div.find(".item:eq(" + tmpFocus + ") .masterBtnArea").addClass("focus");
                    }
                }else {
                    // 히스토리 영역
                    listBtnIdx = 1;
                    instance.div.find(".item:eq(" + tmpFocus + ") .connectingBtnArea").addClass("focus");
                }
            }else {
                log.printDbg("xxxxxx");
                instance.div.find(".item:eq(" + tmpFocus + ") .connectingBtnArea").addClass("focus");
            }

        }

        /**
         * 화면상의 list UI 업데이트 (현재 연결된 목록 + 히스토리 목록)
         */
        function updateListView() {
            log.printDbg("updateListView()");

            instance.div.find(".item").css({display: "none"});
            instance.div.find(".item").removeClass("focus");
            instance.div.find(".item").removeClass("isMaster");
            instance.div.find(".item").removeClass("isLast");

            // 페어링 되어있는 갯수만큼 display 값 조절
            var targetDevice = null;

            if(currentPage == 0) {
                isConnectingListArea.css({display: "block"});

                // 첫번째 페이지의 경우만 연결된 리스트가 노출이 된다.
                if(isConnectingList.length > 0) {
                    for (var i = 0; i < isConnectingList.length; i++) {
                        targetDevice = itemIsConnecting[i];
                        targetDevice.css({display: "block"});
                        targetDevice.find(".nickArea .name").text(isConnectingList[i].nick);
                        // 마스터 노드인 경우 master class 추가
                        if (isConnectingList[i].master_yn == "Y") {
                            itemIsConnecting[i].addClass("isMaster");
                        }else {
                            itemIsConnecting[i].removeClass("isMaster");

                        }
                        if( i == isConnectingList.length-1) {
                            // 마지막 노드인 경우 bottom-border 의 굵기를 늘림
                            itemIsConnecting[i].addClass("isLast");
                        }
                    }
                    isConnectingListArea.find(".noItem").css({ display: "none"});
                }else {
                    isConnectingListArea.find(".noItem").css({ display: "block"});
                }

            }else {
                isConnectingListArea.css({display: "none"});
            }
            var forLoopLength ;
            var index =0;
            // 히스토리 아이템 갯수가 0개 이상인 경우만
            if(wasConnectedList.length > 0) {
                // 현재 페이지가 0페이지이면 최대 노출가능한 히스토리 리스트 갯수는 3개, 2페이지 부터는 5개 이므로
                // 현재 페이지가 몇페이지 인지부터 구분
                if(currentPage == 0) {
                    //첫번째 페이지인 경우, 표현가능한 최대 히스토리 갯수는 3개,
                    //현재 연결된 아이템의 갯수가 몇개 인지에 에 따라, 화면에 그릴 히스토리 갯수가 달라진다.
                    if(isConnectingList.length < 2) {
                        // 1개 혹은 0개
                        if(wasConnectedList.length < 3) {
                            forLoopLength = wasConnectedList.length;
                        }else {
                            forLoopLength = 3;
                        }
                    }else if(isConnectingList.length == 2) {
                        if(wasConnectedList.length < 2) {
                            forLoopLength = wasConnectedList.length;
                        }else {
                            forLoopLength = 2;
                        }
                    }else if(isConnectingList.length == 3) {
                        forLoopLength = 1;
                    }

                    wasConnectedListArea.find(".descArea").css({height: 160});
                } else {
                    // 2페이지 부터는 현재 연결된 장치 갯수에 따라 경우의 수가 나뉘므로
                    // 해당 상황에 맞는 리스트 정리가 필요함
                    if(isConnectingList.length < 2) {
                        // 현재 연결된 장치가 없거나 1개만 있는 경우, 1페이지에 히스토리 리스트가 3개 표현됨
                        index = 3;
                    }else if(isConnectingList.length == 2) {
                        // 현재 연결된 장치가 2개인 경우 , 1페이징 히스토리 아이템 2개 표시
                        index = 2;
                    }else {
                        // 현재 연결된 장치가 3개인 경우 , 1페이지에 히스토리 아이템 1개 표시
                        index = 1;
                    }
                    // 현재 페이지에 표현할 히스토리 아이템 갯수 계산
                    log.printDbg("wasConnectedList length  = "+wasConnectedList.length);
                    if(wasConnectedList.length - (index + (5* (currentPage-1))) < 5) {
                        forLoopLength = wasConnectedList.length - (index +(5*(currentPage-1)));
                        // loop 가 중간에 끝나는 경우(마지막 페이지)
                    }else {
                        forLoopLength = 5;
                    }
                    wasConnectedListArea.find(".descArea").css({ height: 37 });
                }
                log.printDbg("index = "+index);
                log.printDbg("currentPage = "+currentPage);
                log.printDbg("forLoopLength = "+forLoopLength);
                for (var i = 0; i < forLoopLength; i++) {
                    log.printDbg("itemWasConnected "+i+"= "+itemWasConnected[i]);
                    targetDevice = itemWasConnected[i];
                    targetDevice.css({display: "block"});
                    if(currentPage == 0) {
                        targetDevice = targetDevice.find(".nickArea .name").text(wasConnectedList[i].nick);
                    }else {
                        // i + 이미 표시된 장치 + 5 * (currentPage-1)
                        targetDevice.find(".nickArea .name").text(wasConnectedList[i+index+ (5*(currentPage-1)) ].nick);
                    }
                }
                // 최근 연결한계정
                wasConnectedListArea.find(".descArea .numText").text("최근 연결한 계정 / 총 "+wasConnectedList.length+"개");
                wasConnectedListArea.find(".noItem").css({ display: "none"});

                // 현재 보여지는 페이지의 가장 마지막 historyItem box의 굵기를 조절
                //2018.03.16 sw.nam
                // 리스트 추가 및 삭제 등으로 인해 페이지 전환이 일어나야 하는 경우
                // ex) 연결된 계정 0 개 , 히스토리 개정 4개 존재 할 때 2페이지에 있는 히스토리 계정 연결 시도 후
                // 로직 상 forLoopLength  값이 0 혹은 음수가 나올수 있으므로 그 경우 히스토리의 가장 마지막 리스트를 가리키도록 예외처리 추가(	WEBIIIHOME-3710)
                if(forLoopLength < 1) {
                    forLoopLength = wasConnectedList.length;
                }
                itemWasConnected[forLoopLength-1].addClass("isLast");
            }else {
                wasConnectedListArea.find(".descArea .numText").text("최근 연결한 계정 / 총 0개");
                wasConnectedListArea.find(".noItem").css({ display: "block"});

            }

            if(isConnectingList.length + wasConnectedList.length < 1) {
                setFocus(MENU_TYPE.RIGHT_MENU,0);
            }

            // 연결된 리스트 및 히스토리 내역이 없는 경우 계정 삭제 키 숨기기
            if(isConnectingList.length + wasConnectedList.length < 1) {
                contextKeyArea.css({visibility: "hidden"});
            }else {
                contextKeyArea.css({visibility: "inherit"});
            }

            if(TEST_FLAG) {
                log.printDbg("PAIRING_INFO = "+JSON.stringify(PAIRING_INFO.data.pairing_list));
                log.printDbg("isConnectingList = "+JSON.stringify(isConnectingList));
                log.printDbg("PAIRING_HISTORY_INFO = "+JSON.stringify(PAIRING_HISTORY_INFO.data.pairing_hist_list));
                log.printDbg("wasConnectedList = "+JSON.stringify(wasConnectedList));
            }

            setMaxPage();

        };

        /**
         * 페어링 리스트 및 히스토리 리스트 조회
         */
        function getAllPairingInfo() {
            log.printDbg("getAllPairingInfo()");

            if (TEST_FLAG) {
                // 테스트 리스트 삽입
                if (isConnectingList.length > 0 || wasConnectedList.length > 0) {
                    menuType = MENU_TYPE.LIST_MENU;
                } else {
                    menuType = MENU_TYPE.RIGHT_MENU;
                }

                makeIsConnectingArea();
             //   setFocus(menuType, listFocusIdx);

                // 관리 화면의 리스트 업데이트
                updateListView();

                listFocusIdx = 0;
                listBtnIdx = 1;
                setFocus(menuType, listFocusIdx);
                setListBtnFocus(listFocusIdx,listBtnIdx);

                setCurrentPage(listFocusIdx);

            } else {
                //현재 페어링 된 장치 정보를 불러온다.
                // 히스토리보다 페어링 장치에 우선되어 리스트가 정렬되어야 하므로 히스토리를 먼저 호출한다.
                requestPairingHistoryToServer();
                requestPairingInfoToServer();

            }

        }

        /**
         * push Message 에 의한 페어링 정보를 업데이트 한다.
         */
        function getAllPairingInfoByPushMessage() {
            getHistoryList();
            getPairingLIst();
        }

        /**
         * 서버 연동 없이 페어링 리스트만 호출 & UI 업데이트
         */
        function getPairingLIst() {
            log.printDbg("getPairingLIst()");
            isConnectingList = OTMPManager.getPairingList();
            if(!isConnectingList) {
                isConnectingList = [];
            }

            makeIsConnectingArea();

            // 리스트 업데이트
            updateListView();

            if(isConnectingList.length + wasConnectedList.length < 1) {
                menuType = MENU_TYPE.RIGHT_MENU;
                listFocusIdx = 0;
                rightBtnIdx = 0;
                setFocus(menuType,rightBtnIdx);
            }else {
                menuType = MENU_TYPE.LIST_MENU;
                listFocusIdx = 0;
                listBtnIdx = 1;
                setFocus(menuType, listFocusIdx);
                setListBtnFocus(listFocusIdx,listBtnIdx);
                setCurrentPage(listFocusIdx)
            }

        }

        /**
         * 서버 연동 없이 히스토리 리스트만 호출 & UI 업데이트
         */
        function getHistoryList() {
            log.printDbg("getHistoryList()");
            wasConnectedList = OTMPManager.getPairingHistList();
            if(!wasConnectedList) {
                wasConnectedList = [];
            }

            // 리스트 업데이트
            updateListView();

            if(isConnectingList.length + wasConnectedList.length < 1) {
                menuType = MENU_TYPE.RIGHT_MENU;
                listFocusIdx = 0;
                rightBtnIdx = 0;
                setFocus(menuType,rightBtnIdx);
            }else {
                menuType = MENU_TYPE.LIST_MENU;
                listBtnIdx = 1;
                if(listFocusIdx > isConnectingList.length + wasConnectedList.length -1) {
                    listFocusIdx = isConnectingList.length + wasConnectedList.length -1;
                }
                setFocus(menuType, listFocusIdx);
                setListBtnFocus(listFocusIdx,listBtnIdx);
                setCurrentPage(listFocusIdx);
            }

            // sw.nam
            // 히스토리영역만 refresh 되는 경우(히스토리 삭제)는 심리스 푸시가 오지 않으므로
            // 로딩과 상관없이 stopLoading 함수를 호출하도록 한다.(layerManager 내에서 로딩 바가 돌고 있는 경우만 stop 시키도록 알아서 처리하므로)
            // 별도의 예외처리를 하지 않아도 상관 없음
            LayerManager.stopLoading();
            if(isFirstShowing) {
                isFirstShowing = false;
                otmListArea.css({visibility: "inherit"});
            }

        }
        function requestPairingInfoToServer() {
            log.printDbg("requestPairingInfoToServer()");
            OTMPManager.getPairingInfo(cbGetPairingInfo,false)
        }

        function requestPairingHistoryToServer() {
            log.printDbg("requestPairingHistoryToServer()");
            OTMPManager.getPairingHistory(cbGetPairingHistory);
        }

        /**
         * 페어링 리스트 콜백
         * 초기 진입 시, 마스터 변경, 계정 추가, 계정 삭제 시 불림
         * @param result
         * @param list
         */
        function cbGetPairingInfo(result, list) {
            isConnectingList = list;
            log.printDbg("cbGetPairingInfo " + JSON.stringify(result));
            if(!isConnectingList) {
                isConnectingList = [];
            }


            // 연결시마다 계속 변하므로 엘레먼트를 다시 만든다.
            makeIsConnectingArea();

            // 리스트 업데이트
            updateListView();

            if(isConnectingList.length + wasConnectedList.length < 1) {
                menuType = MENU_TYPE.RIGHT_MENU;
                listFocusIdx = 0;
                rightBtnIdx = 0;
                setFocus(menuType,rightBtnIdx);
            }else {
                menuType = MENU_TYPE.LIST_MENU;
                listFocusIdx = 0;
                listBtnIdx = 1;
                setFocus(menuType, listFocusIdx);
                setListBtnFocus(listFocusIdx,listBtnIdx);
                setCurrentPage(listFocusIdx)
            }

        }

        function cbGetPairingHistory(result, list) {
            wasConnectedList = list;
            log.printDbg("cbGetPairingHistory " + JSON.stringify(result));

            if(!wasConnectedList) {
                wasConnectedList = [];
            }
            // 리스트 업데이트
            updateListView();

            if(isConnectingList.length + wasConnectedList.length < 1) {
                menuType = MENU_TYPE.RIGHT_MENU;
                listFocusIdx = 0;
                rightBtnIdx = 0;
                setFocus(menuType,rightBtnIdx);
            }else {
                menuType = MENU_TYPE.LIST_MENU;
                listBtnIdx = 1;
                if(listFocusIdx > isConnectingList.length + wasConnectedList.length -1) {
                    listFocusIdx = isConnectingList.length + wasConnectedList.length -1;
                }
                setFocus(menuType, listFocusIdx);
                setListBtnFocus(listFocusIdx,listBtnIdx);
                setCurrentPage(listFocusIdx);
            }

            // sw.nam
            // 히스토리영역만 refresh 되는 경우(히스토리 삭제)는 심리스 푸시가 오지 않으므로
            // 로딩과 상관없이 stopLoading 함수를 호출하도록 한다.(layerManager 내에서 로딩 바가 돌고 있는 경우만 stop 시키도록 알아서 처리하므로)
            // 별도의 예외처리를 하지 않아도 상관 없음
            LayerManager.stopLoading();
            if(isFirstShowing) {
                isFirstShowing = false;
                otmListArea.css({visibility: "inherit"});
            }

        }

        /**
         * 최대 페이지 개수를 구한다.
         */
        function setMaxPage() {
            log.printDbg("setMaxPage()");
            if(isConnectingList.length < 1) {
                // 연결된게 없으면.. 첫페이지는 3개만 표현 가능
                if (wasConnectedList.length <= 3) {
                    // 히스토리가 3개 이하라면 페이지는 1
                    maxPage = 1;
                } else {
                    // 히스토리가 3개보다 크면.. 3개를 뺀 나머지를 가지고 5로 나눠서 페이지를 구하고, 거기에 1을 더한다.
                    maxPage = 1 + Math.ceil((wasConnectedList.length - 3) / 5);
                }
            }else {
                // 연결된게 하나라도 있으면, 첫페이지는 4개 표현 가능
                if(isConnectingList.length + wasConnectedList.length <= 4) {
                    // 두 리스트의 합이 4개 이하라면 페이지는 1개
                    maxPage =1;
                }else {
                    // 히스토리가 4개 이상이라면.. 4개를 뺀 나머지를 가지고 5로 나눈 페이지 갯수에 +1을 더한다.
                    maxPage = 1 + Math.ceil((wasConnectedList.length -4) / 5);
                }
            }

            if(maxPage > 1) {
                colorKeyArea.css({ visibility: "inherit"});
            }else {
                colorKeyArea.css({ visibility: "hidden"});
            }
            log.printDbg("maxPage = "+maxPage);
            indicator.setSize(maxPage);
        }

        /**
         * 현재 페이지를 구한다.
          * @param idx 포커스된 index
         */
        function setCurrentPage(idx) {
            log.printDbg("setCurrentPage() "+idx);

            var prevPage = currentPage;

            var indexingVal;
            if(isConnectingList.length < 1) {
                // 연결된 리스트가 없는경우, 인덱스가 3보다 작으면 1페이지이다.
                //3보다 큰 경우.. 3개를 뺀 나머지를 가지고 5로 나눈 페이지 갯수에 1을 더한다.
                indexingVal = 3;
            }else {
                // 연결된 리스트가 있는경우, 첫페이지에 들어갈 수 있는 리스트 의 갯수는 4개이므로 인덱스가 4보다 작으면 무조건 1페이지 이다
                indexingVal = 4;
                // 4보다 큰 경우 .. 4를 뺀 나머지를 가지고 5로 나눈 페이지 갯수에 1을 더한다.
            }

            if (idx < indexingVal) {
                currentPage = 0;
            } else {
                currentPage = Math.floor((idx - indexingVal) / 5) + 1;
            }
            if(prevPage !== currentPage) {
                updateListView();
            }
            indicator.setPos(currentPage);
        };

        function getMaxPage() {
            return maxPage;
        };

        function getCurrentPage() {
            return currentPage;
        };

        /**
         * gui test 용 popup 호출 함수
         * @param title
         * @param msg
         */
        function activateBasicPopupLayer(title,msg) {
            log.printDbg("activateBasicPopupLayer");

            LayerManager.activateLayer({
                 obj: {
                         id: "OTMPBasicPopup",
                         type: Layer.TYPE.POPUP,
                         priority: Layer.PRIORITY.POPUP,
                         linkage: true,
                         params: {
                             title: title,
                             message: msg,
                             buttons: [{
                             name: "확인", callback: "close"
                             }]
                         }
                     },
                     moduleId: "module.family_home",
                     visible: true
                 });
        }
    };

    myHome.layer.home_otmPairing.prototype = new Layer();
    myHome.layer.home_otmPairing.prototype.constructor = myHome.layer.home_mailBox;

    //Override create function
    myHome.layer.home_otmPairing.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_otmPairing.prototype.handleKeyEvent = function(key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["MyHomeOtmPairing"] = myHome.layer.home_otmPairing;
})();