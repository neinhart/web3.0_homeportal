/**
 * 올레 tv 100배 즐기기
 * (우리집 맞춤 tv > 마이메뉴 > 올레 tv 100배 즐기기)
 * Created by ksk91_000 on 2016-08-30.
 */
(function() {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_help = function HomeHelp(options) {
        Layer.call(this, options);
        var contentList = [];
        var instance = this;
        var catId;
        var mainclock;
        var viewMgr;

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_help"});

            catId = this.getParams().catId;
            var menuName = this.getParams().menuName;

            this.div.html("<div class='background'>" +
                //"<img id='defaultBg' src='" + COMMON_IMAGE.BG_DEFAULT + "' alt='background' class='clipping'>" +
                "<span id='backgroundTitle'>" + this.getParams().menuName + "</span>" +
                "<div id='pig_dim'></div>" +
                "<div class='main_clock'/>" +
                "<img id='default_homeshot' src='" + COMMON_IMAGE.HOMESHOT_DEFAULT + "' alt='home_shot'>"+
                "</div>");

            viewMgr = new myHome.ViewManager();
            this.div.append(viewMgr.getView());
            mainclock = new ClockComp({parentDiv:this.div.find(".clock_area")});

            myHome.amocManager.getItemDetlListW3(function(result, data) {
                if(result) {
                    contentList = data.itemDetailList;
                    contentList = contentList==null?[]:Array.isArray(contentList)?contentList:[contentList];

                    viewMgr.setParent(instance.div);
                    if(contentList[0].itemType==0) {
                        var dataList = [];
                        for(var i in contentList) dataList.push(new ViewData(contentList[i].itemId, myHome.view.HelpListView, null, contentList[i].itemName, contentList[i].itemId));
                        viewMgr.setData(new CategoryViewData(catId, dataList, menuName, catId));
                    }else {
                        viewMgr.setData(new ViewData(catId, myHome.view.HelpListView, contentList, menuName, catId));
                    }
                } if(cbCreate) cbCreate(result);
            }, catId, 1, 20, DEF.SAID);
        };

        this.show = function(options) {
            Layer.prototype.show.call(this);
            mainclock.show(this.div.find(".clock_area"));
            if(options && options.resume) viewMgr.resume();
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
        };

        this.hide = function(options) {
            mainclock.hide();
            if(options && options.pause) viewMgr.pause();
            Layer.prototype.hide.call(this);
        };

        this.onKeyAction = function(keyCode) {
            return viewMgr.onKeyAction(keyCode)
        };
    };

    myHome.layer.home_help.prototype = new Layer();
    myHome.layer.home_help.prototype.constructor = myHome.layer.home_help;

    //Override create function
    myHome.layer.home_help.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    myHome.layer.home_help.prototype.handleKeyEvent = function(key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["MyHomeHelp"] = myHome.layer.home_help;
})();