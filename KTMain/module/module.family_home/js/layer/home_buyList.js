
/**
 * 구매내역 보기 (우리집 맞춤 TV > 구매내역 보기)
 *  └ components : buyList.js(VOD 구매목록), buyList_otn.js(모바일 공유목록), buyList_ots.js(무비초이스 구매목록 : OTS전용 구매목록)
 * Created by ksk91_000 on 2016-07-27.
 */
(function() {
    myHome.layer = myHome.layer||{};
    myHome.layer.home_buyList = function HomeBuyList(options) {
        Layer.call(this, options);
        var cateData = [];
        var view_cate;
        var INSTANCE = this;

        var viewMgr = new myHome.ViewManager();
        var mainclock;

        this.init = function() {
            this.div.attr({class: "arrange_frame myhome home_buyList"});

            cateData.push(new ViewData("vodBuyList", myHome.view.SingleCompView, {component: new component.buyList('vodBuyList', 'v')}, "VOD 구매목록", "MENU00611", true));
            cateData.push(new ViewData("mobileBuyList", myHome.view.SingleCompView, {component: new component.buyList_otn('mobileBuyList')}, "모바일 공유목록", "MENU00609", true));
            if(CONSTANT.IS_OTS) cateData.push(new ViewData("movieChoiceBuyList", myHome.view.SingleCompView, {component: new component.buyList_ots('movieChoiceBuyList')}, "무비초이스 구매목록", "MENU00612", true));

            this.div.html("<div class='background category'>" +
                "<div id='defaultBg' alt='background' class='category'>"+
                "<span id='backgroundTitle'>구매내역 보기</span>"+
                //"<div id='pig_dim'></div>" +
                //"<div class='clock_area'/>"+
                //"<img id='default_homeshot' src='" + COMMON_IMAGE.HOMESHOT_DEFAULT + "' alt='home_shot'>"+
                "</div>");


            viewMgr.setParent(this.div);
            view_cate = new CategoryViewData("buyList", cateData, "구매내역 보기");
            viewMgr.setData(view_cate);
            view_cate = view_cate.getView();
            this.div.append(viewMgr.getView());
            //mainclock = new ClockComp({parentDiv:this.div.find(".clock_area")});

            var params = this.getParams();
            if(params && params.focusIdx!=null) viewMgr.changeCategoryFocus(params.focusIdx);
        };

        this.show = function(options) {
            Layer.prototype.show.call(this);
            LayerManager.showVBOBackground("myHome_main_background");

            if(options && options.resume) viewMgr.resume();
            if(this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);

            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            log.printDbg("show BuyList");
        };

        this.hide = function(options) {
            if(options && options.pause) viewMgr.pause();
            Layer.prototype.hide.call(this);
            LayerManager.showVBOBackground();

            AdultAuthorizedCheck.setAdultAuthorized(false, "buyList");
            log.printDbg("hide BuyList");
        };

        this.remove = function () {
            viewMgr.destroy();
            Layer.prototype.remove.call(this);
        };

        this.controlKey = function(key_code) {
            if(viewMgr.onKeyAction(key_code)){
                return true;
            } else if(key_code==KEY_CODE.BACK) {
                LayerManager.historyBack();
                return true;
            }return false;
        };
    };

    myHome.layer.home_buyList.prototype = new Layer();
    myHome.layer.home_buyList.prototype.constructor = myHome.layer.home_buyList;

    //Override create function
    myHome.layer.home_buyList.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    myHome.layer.home_buyList.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MyHomeBuyList"] = myHome.layer.home_buyList;
})();