/**
 * 가입 정보
 * (우리집 맞춤 TV > 가입 정보)
 * Created by ksk91_000 on 2016-08-30.
 */
(function () {
    myHome.layer = myHome.layer || {};
    myHome.layer.home_myProduct = function HomeMyProduct(options) {
        Layer.call(this, options);
        var pkgList = [];
        var focus = 0;
        var page;
        var indicator;
        var instance = this;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame myhome home_myProduct"});

            this.div.html("<div class='background'>" +
                //"<img id='defaultBg' src='" + COMMON_IMAGE.BG_DEFAULT + "' alt='background' class='clipping'>" +
                "<span id='backgroundTitle'>가입 정보</span>" +
                //"<div id='pig_dim'></div>" +
                //"<img id='default_homeshot' src='" + COMMON_IMAGE.HOMESHOT_DEFAULT + "' alt='home_shot'>"+
                "</div>" +
                "<div class='listArea'>"
            );

            indicator = new Indicator(1);
            this.div.append(indicator.getView()).append(indicator.getPageTransView());

            if (cbCreate) cbCreate();
        };

        function setFocus(newFocus) {
            instance.div.find("#item_" + pkgList[focus].code).removeClass("focus");
            focus = newFocus;
            setPage(Math.floor(focus / 3));
            instance.div.find("#item_" + pkgList[focus].code).addClass("focus");
            instance.div.find(".focus_line").css("-webkit-transform", "translateY(" + (230 * (focus % 3) + (focus % 3 === 0 ? 0 : 1)) + "px)");
        }

        function setPage(newPage) {
            if (page !== newPage)
                page = newPage;
            indicator.setPos(page);
            for (var i = 0; i < 3; i++) {
                if (!!pkgList[i + page * 3]) setItem(instance.div.find(".item:eq(" + i + ")").removeClass("hiddenItem"), pkgList[i + page * 3]);
                else instance.div.find(".item:eq(" + i + ")").addClass("hiddenItem");
            }
        }

        function makePkgListData(data) {
            var list = data.pkgList;
            list = list == null ? [] : _$.isArray(list) ? list : [list];
            pkgList = [];
            for (var i = 0; i < list.length; i++) {
                var tmp = {};
                tmp.originData = list[i];
                switch (parseInt(list[i].pkgClass)) {
                    case 1:
                        tmp.code = list[i].pkgCode;
                        tmp.title = "기본 상품: " + list[i].pkgName;
                        tmp.infoList = [
                            "<color>가격: </color>월 " + HTool.addComma(list[i].vatPrice) + "원",
                            "<color>가입 날짜: </color>" + HTool.getDateDotFormat(list[i].regDate),
                        ];
                        break;
                    default:
                        tmp.code = list[i].pkgCode;
                        tmp.title = (list[i].pkgClass == 2 ? "부가 상품: " : "월정액 상품: ") + list[i].pkgName;
                        tmp.infoList = [
                            "<color>가격: </color>월 " + HTool.addComma(list[i].vatPrice) + "원",
                            "<color>가입 날짜: </color>" + HTool.getDateDotFormat(list[i].regDate)
                        ];
                        if (HTool.getBoolean(list[i].contractYn)) {
                            tmp.infoList.push("<color>약정: </color>" + list[i].contractPeriod + "개월");
                            // [170612 SK] 채널 PPT의 경우에만 만료날짜 내려오므로 표기하지 않는 것으로 정리
                            // tmp.infoList.push("<color>만료 날짜: </color>" + HTool.getDateDotFormat(list[i].contractExpireDate));
                        }
                        tmp.isOsc = HTool.getBoolean(list[i].oscYn);
                }
                pkgList.push(tmp);
            }
            indicator.setSize(Math.ceil(pkgList.length / 3));
        }

        function constructHTML() {
            function makeItem() {
                var html = "<tr class='item'>" +
                    "<td colspan='2'>" +
                    "<span id='productName'></span>" +
                    "<div class='info_left'/>" +
                    "<div class='info_right'/>";
                html += "</td>";
                html += "<td class='btnCell'>" +
                    "<div class='btn focus' style='display: none'>해지</div>" +
                    "</td>";
                html += "</tr>";
                return _$(html);
            }

            instance.div.find(".listArea").empty();
            var html = instance.div.find(".listArea");
            html.append("<span id='description'>상품 가격은 할인 적용 전 금액이므로 실 납부 금액과 다를 수 있습니다.<br>가입 상품은 별도 해지 신청하지 않으면 자동 연장되며 일부 상품은 1개월 이내 해지 시, 1개월 요금이 청구됩니다</span>");
            html.append(_$("<div/>", {class: "focus_line"}));
            html.append(_$("<div/>", {class: "view_area"}).append(_$("<table/>")));
            var table = html.find(".view_area table");
            for (var i = 0; i < 3; i++) table.append(makeItem());
            if (pkgList.length == 0) {
                instance.div.find(".focus_line").hide();
            }
            if (pkgList.length !== 0) {
                setFocus(getDefaultFocus());
            }
        }

        function setItem(div, data) {
            div.attr("id", "item_" + data.code);
            div.find("#productName").text(data.title);
            div.find(".btnCell .btn").toggle(!!data.isOsc);
            div.find(".info_left,.info_right").empty();
            for (var i = 0; i < data.infoList.length; i++) {
                div.find(".info_" + (i < 2 ? "left" : "right")).append("<span id='productInfo'>" + data.infoList[i] + "</span>");
            }
        }

        function getDefaultFocus() {
            for (var i = 0; i < pkgList.length; i++)
                if (pkgList[i].isOsc) return i;
            return 0;
        }

        this.onKeyAction = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP :
                    setFocus(HTool.getIndex(focus, -1, pkgList.length));
                    return true;
                case KEY_CODE.DOWN :
                    setFocus(HTool.getIndex(focus, 1, pkgList.length));
                    return true;
                case KEY_CODE.RED:
                    if (focus % 3 === 0) setFocus(HTool.getIndex(page, -1, Math.ceil(pkgList.length / 3)) * 3);
                    else setFocus(page * 3);
                    return true;
                case KEY_CODE.BLUE:
                    if (focus % 3 === 2 || focus === pkgList.length - 1) setFocus(Math.min(pkgList.length - 1, HTool.getIndex(page, 1, Math.ceil(pkgList.length / 3)) * 3 + 2));
                    else setFocus(Math.min(pkgList.length - 1, page * 3 + 2));
                    return true;
                case KEY_CODE.LEFT:
                    focus = 0;
                    page = 0;
                    instance.div.find(".focus_line").show();
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER :
                    if (pkgList[focus].isOsc) {
                        LayerManager.activateLayer({
                            obj: {
                                id: "CancelProductPopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: pkgList[focus].originData
                            },
                            moduleId: "module.family_home",
                            new: true,
                            visible: true
                        });
                    }
                    return true;
            }
        };

        this.show = function (options) {
            if (this.getParams()) this.div.find("#backgroundTitle").text(this.getParams().menuName);
            if (options && options.resume) {
                Layer.prototype.show.call(this);
                LayerManager.showVBOBackground("myHome_main_background");
                //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
                //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
                return;
            }

            focus = 0;
            page = 0;
            indicator.blurred();
            indicator.setPos(0);
            instance.div.find(".focus_line").show();

            Layer.prototype.show.call(this);
            LayerManager.showVBOBackground("myHome_main_background");
            //homeImpl.get(homeImpl.DEF_FRAMEWORK.OIPF_ADAPTER).basicAdapter.resizeScreen(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //IframeManager.changeIframe(CONSTANT.SUBHOME_PIG_STYLE.LEFT, CONSTANT.SUBHOME_PIG_STYLE.TOP, CONSTANT.SUBHOME_PIG_STYLE.WIDTH, CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);

            if (this.div.find(".listArea"))
                this.div.find(".listArea").empty();
            LayerManager.startLoading({preventKey: true});
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            myHome.amocManager.getCustPkgList("saId=" + DEF.SAID, custEnv ? custEnv.buyTypeYn : "Y", function (result, data) {
                makePkgListData(data);
                constructHTML();
                LayerManager.stopLoading();
            }, false);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            LayerManager.showVBOBackground();
        };
    };

    myHome.layer.home_myProduct.prototype = new Layer();
    myHome.layer.home_myProduct.prototype.constructor = myHome.layer.home_myProduct;

    //Override create function
    myHome.layer.home_myProduct.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init();
        cbCreate(true);
    };

    myHome.layer.home_myProduct.prototype.handleKeyEvent = function (key_code) {
        return this.onKeyAction(key_code);
    };

    arrLayer["MyHomeMyProduct"] = myHome.layer.home_myProduct;
})();