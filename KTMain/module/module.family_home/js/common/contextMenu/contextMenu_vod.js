/**
 * Created by ksk91_000 on 2017-03-25.
 */
var ContextMenu_VOD = function() {
    var div = _$("<div/>", {class: 'context_menu_area vod_context_menu'});
    var contextMenu = _$("<div/>", {class: 'context_area vod_context_menu'});
    var vodArea = _$("<div/>", {class: "vod_info_area"});
    var contextArea = _$("<div/>", {class: "context_menu_area"});

    var vodInfo;
    var listener;
    var menuList = [];
    var vodFocus = 0;
    var focus = 0;
    var isVodFocus = false;
    var recentlyVod = [];
    var recentlyVodArea = [];
    var that = this;
    var btnType;

    var openedDropBox;

    div.append(_$("<div/>", {class: 'background_dim'}));
    div.append(_$("<div/>", {class: 'left_arrow'}));
    div.append(contextMenu);

    (function() {
        contextMenu.append(vodArea).append(contextArea);
        vodArea.html(
            "<div class='poster_area'>" +
                "<img class='content_poster'>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>" +
            "<div class='btn'>찜하기</div>" +
            "<div class='btn'>플레이리스트 추가</div>" +
            "<div class='title recently_text'>최근 확인한 VOD</div>" +
            "<div class='no_content_text'>최근 확인한<br>VOD가 없습니다</div>" +
            "<div class='recently_content'>" +
                "<div class='recently_content_imgWrapper'>" +
                "<img>" + "</div>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                "<img src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>" +
            "<div class='recently_content'>" +
                "<div class='recently_content_imgWrapper'>" +
                "<img>" + "</div>" +
                "<div class='shdw_area shdw_bg_l'/>" +
                "<div class='shdw_area shdw_bg'/>" +
                "<div class='shdw_area shdw_bg_r'/>" +
                "<div class='icon_area'>" +
                    "<img src='" + modulePath + "resource/image/icon/img_vod_locked.png'>" +
                "</div>" +
            "</div>");
        recentlyVodArea[0] = vodArea.find(".recently_content").eq(0);
        recentlyVodArea[1] = vodArea.find(".recently_content").eq(1);
    })();

    this.open = function() {
        div.addClass("activate");
        recentlyVod = JSON.parse(StorageManager.ms.load(StorageManager.KEY.RECENTLY_VOD)) || [];
        setRecentlyContents();
        setFocus(0);
        initVodFocus(0);
        pauseTextAnimation();
    };

    this.close = function() {
        div.removeClass("activate");
        resumeTextAnimating();
    };

    this.isOpen = function() {
        return div.hasClass("activate");
    };

    this.addButton = function(title, classNm) {
        var item = _$("<div/>", {class: "btn " + (classNm ? classNm : "")}).text(title);
        menuList.push({type: ContextMenu.ITEM_TYPE.BUTTON, item: item});
        contextArea.append(item);
    };

    this.addSeparateLine = function(classNm) {
        var item = _$("<div/>", {class: "sep_line " + (classNm ? classNm : "")});
        contextArea.append(item);
    };

    this.addTitle = function(title, classNm) {
        var item = _$("<div/>", {class: "title " + (classNm ? classNm : "")}).text(title);
        contextArea.append(item);
    };

    this.addDropBox = function(options, focus, classNm) {
        var item = new dropBox(options, classNm);
        menuList.push({type: ContextMenu.ITEM_TYPE.DROP_BOX, item: item});

        item.setSelectedIdx(focus || 0);
        contextArea.append(item.getView());
    };

    this.setEventListener = function(eventListener) {
        listener = eventListener;
    };

    this.setVodInfo = function(options) {
        vodInfo = options;
        contextMenu.find(".content_poster").attr("src", vodInfo.imgUrl + "?w=168&h=240&quality=90");
        div.find(".icon_area").toggle(!AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(vodInfo.prInfo));

        if(options.itemType==3||options.itemType==7||options.itemType==8)
            btnType = ContextMenu_VOD.VOD_BTN_TYPE.NONE;
        else if(options.itemType==0)
            btnType = ContextMenu_VOD.VOD_BTN_TYPE.ONLY_WISH;
        else
            btnType = ContextMenu_VOD.VOD_BTN_TYPE.ALL;

        vodArea.find(".btn:eq(0)").toggleClass("hiddenItem", (btnType&1)===0);
        vodArea.find(".btn:eq(1)").toggleClass("hiddenItem", (btnType&2)===0);
    };

    var isVisibleVodInfo = true;

    this.setVisibleVodInfo = function (visible) {
        isVisibleVodInfo = visible;
        if(visible) {
            div.find(".vod_info_area").css("visibility", "inherit");
            div.removeClass("no_recent");
        } else {
            div.find(".vod_info_area").css("visibility", "hidden");
            div.addClass("no_recent");
        }
    }

    function setFocus(_focus) {
        isVodFocus = false;
        vodArea.find(".focus").removeClass("focus");
        menuList[focus].item.removeClass("focus");
        focus = _focus;
        menuList[focus].item.addClass("focus");
    }

    function initVodFocus(focus) {
        vodFocus = focus||0;
        changeVodFocus(0);
    }

    function changeVodFocus(amount) {
        if(btnType===ContextMenu_VOD.VOD_BTN_TYPE.NONE && recentlyVod.length===0) return setFocus(focus);
        vodFocus = HTool.getIndex(vodFocus, amount, 2 + Math.min(2, recentlyVod.length));
        if(vodFocus<2) {
            if((btnType&Math.pow(2,vodFocus))===0) return changeVodFocus(amount===0?1:amount);
        } else {
            if(recentlyVod.length<=vodFocus-2) return changeVodFocus(amount===0?1:amount);
        }

        isVodFocus = true;
        menuList[focus].item.removeClass("focus");
        vodArea.find(".focus").removeClass("focus");
        switch (vodFocus) {
            case 0: case 1: vodArea.find(".btn").eq(vodFocus).addClass("focus"); break;
            case 2: case 3:
                recentlyVodArea[vodFocus-2].addClass("focus");
                recentlyVodArea[vodFocus-2].find(".shdw_area").addClass("focus");
                recentlyVodArea[vodFocus-2].find(".recently_content_imgWrapper").addClass("focus");
                break;
        }
    }

    function setRecentlyContents() {
        var i = 0, contsArea = contextMenu.find(".recently_content"), imgArea = contextMenu.find(".recently_content_imgWrapper > img");
        for (i = 0; i < recentlyVod.length; i++) {
            contsArea.eq(i).show().find(".icon_area").toggle(!AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(recentlyVod[i].prInfo));
            imgArea.eq(i).attr("src", recentlyVod[i].imgUrl);
        }
        for (; i < 2; i++) {
            contsArea.eq(i).hide();
        }
        if (recentlyVod.length == 0) {
            initVodFocus(0);
        } else if (vodFocus > recentlyVod.length + 1) {
            changeVodFocus(-1);
        }
    }

    this.onKeyAction = function(keyCode) {
        if (openedDropBox) return openedDropBox.onKeyAction(keyCode);
        switch (keyCode) {
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
                this.close();
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
            default:
                if (isVodFocus) return onKeyForVodMenu.call(this, keyCode);
                else return onKeyForContextMenu.call(this, keyCode);
        }
    };

    function onKeyForVodMenu(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                changeVodFocus(-1);
                return true;
            case KEY_CODE.DOWN:
                changeVodFocus(1);
                return true;
            case KEY_CODE.RIGHT:
                setFocus(focus);
                return true;
            case KEY_CODE.LEFT:
                this.close();
                return true;
            case KEY_CODE.RED:
                if (vodFocus == 2 || vodFocus == 3) {
                    recentlyVod.splice(vodFocus - 2, 1);
                    StorageManager.ms.save(StorageManager.KEY.RECENTLY_VOD, JSON.stringify(recentlyVod));
                    setRecentlyContents();
                }
                return true;
        }
    }

    function onKeyForContextMenu(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                setFocus(HTool.getIndex(focus, -1, menuList.length));
                return true;
            case KEY_CODE.DOWN:
                setFocus(HTool.getIndex(focus, 1, menuList.length));
                return true;
            case KEY_CODE.LEFT:
                if(btnType===ContextMenu_VOD.VOD_BTN_TYPE.NONE && recentlyVod.length===0) this.close();
                else changeVodFocus(0);
                return true;
            case KEY_CODE.RIGHT:
                return true;
        }
    }

    function enterKeyAction() {
        if (isVodFocus) {
            switch (vodFocus) {
                case 0:
                    // addWishList(vodInfo.itemType, vodInfo.catId, vodInfo.contsId, vodInfo.cmbYn);
                    addWishListVodInfo(vodInfo);
                    that.close();
                    return true;
                case 1:
                    LayerManager.activateLayer({
                        obj: {
                            id: "AddToMyPlayListPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: vodInfo
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    that.close();
                    return true;
                case 2:
                case 3:
                    that.close();
                    var vod = recentlyVod[vodFocus - 2];
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SEARCH, vod.catId, vod.contsId, "01");
                    } catch(e) {}
                    openDetailLayer(vod.catId, vod.contsId, "01", {isKids:vod.isKids});
                    return true;
            }
        } else switch (menuList[focus].type) {
            case ContextMenu.ITEM_TYPE.BUTTON:
                listener(focus);
                return true;
            case ContextMenu.ITEM_TYPE.DROP_BOX:
                openedDropBox = menuList[focus].item;
                openedDropBox.open();
                return true;
        }

        function addWishListVodInfo(_vodInfo) {
            var familyHomeModule = ModuleManager.getModule("module.family_home");
            if (familyHomeModule) {
                familyHomeModule.execute({
                    method: "addToWishList",
                    data: _vodInfo
                });
            } else addWishList(_vodInfo.itemType, _vodInfo.catId, _vodInfo.contsId, _vodInfo.cmbYn);
        }

        function addWishList(itemType, catId, contsId, cmbYn) {
            if(itemType==1 || itemType==0) catId = contsId;
            try {
                subHome.amocManager.setPlayListNxt(1, itemType, contsId, catId, 1, (cmbYn === "Y" || cmbYn === true)? "Y" : "", function (result, response) {
                    if (result) {
                        switch (response.reqCode) {
                            case "0": showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다"); break; // 찜 등록 성공
                            case "3": throw "networkError(2)"; break;
                            case "1":
                                // 이미 찜 등록되어 있음 -> 해제 확인 팝업 출력
                                LayerManager.activateLayer({
                                    obj: {
                                        id: "VODWishConfirm",
                                        type: Layer.TYPE.POPUP,
                                        priority: Layer.PRIORITY.POPUP,
                                        linkage: true,
                                        params: {
                                            callback: function () {
                                                subHome.amocManager.setPlayListNxt(1, itemType, contsId, "", 2, "", function (result, response) {
                                                    if (result) {
                                                        if(response.reqCode == 0)  showToast("VOD 찜을 해제하였습니다"); // 찜 해제 성공
                                                        else throw "networkError(4)";
                                                    } else throw "networkError(3)";
                                                });
                                            }
                                        }
                                    },
                                    moduleId: "module.subhome",
                                    visible: true
                                });
                                break;

                        }
                    } else throw "networkError(1)";
                });
            } catch(e) {
                log.printErr(e);
            }
        }
    }

    this.setDropBoxFocus = function(index, focus) {
        if (menuList[index].type === ContextMenu.ITEM_TYPE.DROP_BOX) {
            menuList[index].item.setSelectedIdx(focus);
        }
    };

    this.clearDropBoxFocus = function() {
        for (var index in menuList) {
            if (menuList[index].type === ContextMenu.ITEM_TYPE.DROP_BOX) {
                menuList[index].item.setSelectedIdx(0);
            }
        }
    };

    this.removeFocus = function() {
        menuList[focus].item.removeClass("focus");
    };

    this.addFocus = function() {
        menuList[focus].item.addClass("focus");
    };

    this.getView = function() {
        return div;
    };

    function pauseTextAnimation() {
        var div = _$(".textAnimating").removeClass("textAnimating").addClass("pauseAnimationByCtxMenu");
        UTIL.clearAnimation(div.find("span"));
    }

    function resumeTextAnimating() {
        var div = _$(".pauseAnimationByCtxMenu").removeClass("pauseAnimationByCtxMenu").addClass("textAnimating");
        UTIL.startTextAnimation({
            targetBox: div
        })
    }

    var dropBox = function(options, classNm) {
        var selectedIdx;
        var indexList = [];
        var focusIdx = 0;
        var div = _$("<div/>", {class: "drop_box " + (classNm ? classNm : "")}).html("<span class='text'></span><div class='options'></div>");
        var page = 0;

        (function init() {
            var optionArea = div.find(".options");
            for (var i = 0; i < options.length && i < 5; i++) {
                optionArea.append(_$("<div/>", {class: 'option'}).text(options[i]));
                indexList[i] = i;
            } optionArea.append("<div class='focus_line'/>");
            if(options.length>5) optionArea.append("<div class='scroll_bg'><div class='scroll_bar'/></div>");
            div.find(".scroll_bar").css("height", 317 / Math.ceil(options.length / 5) + "px");
        })();

        function sortOption() {
            var isSetOption = false;
            for (var i = 0; i < options.length; i++) {
                if (i == selectedIdx) {
                    indexList[0] = i;
                    isSetOption = true;
                } else {
                    indexList[isSetOption ? i : (i + 1)] = i;
                }
            }
            setPage(0);
        }

        function setPage(_page) {
            page = _page;
            div.find(".checked").removeClass("checked");
            var optionArea = div.find(".options .option");
            var i = 0;
            for (; i < (options.length - page * 5) && i < 5; i++) {
                // if((page*5)+i==0) optionArea.eq(i).addClass("checked");
                if ((page * 5) + i == selectedIdx) optionArea.eq(i).addClass("checked");
                optionArea.eq(i).text(options[(page * 5) + i]);
            }
            for (; i < 5; i++) {
                optionArea.eq(i).text("");
            }
            div.find(".scroll_bar").css("top", (317 / Math.ceil(options.length / 5) * page) + "px");
        }

        this.setSelectedIdx = function(index) {
            selectedIdx = index;
            div.find(".text").text(options[selectedIdx]);
            // sortOption();
            // setPage(0);
            setPage(Math.floor(selectedIdx / 5));
        };

        this.setFocus = function(focus) {
            div.find(".focus").removeClass("focus");
            focusIdx = focus;
            if (Math.floor(focusIdx / 5) != page) setPage(Math.floor(focusIdx / 5));
            div.find(".option").eq(focus % 5).addClass("focus");
            div.find(".focus_line").css("-webkit-transform", "translateY(" + (63*(focusIdx%5)) + "px)");
        };

        this.open = function() {
            this.setFocus(selectedIdx);
            div.addClass("open");
        };

        this.close = function() {
            openedDropBox = null;
            div.removeClass("open");
        };

        this.onKeyAction = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    this.setFocus(HTool.getIndex(focusIdx, -1, options.length));
                    return true;
                case KEY_CODE.DOWN:
                    this.setFocus(HTool.getIndex(focusIdx, 1, options.length));
                    return true;
                case KEY_CODE.ENTER:
                    this.setSelectedIdx(focusIdx);
                    this.close();
                    listener(focus, selectedIdx);
                    return true;
                case KEY_CODE.BACK:
                    this.close();
                    return true;
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    return true;
            }
        };

        this.addClass = function(classNm) { div.addClass(classNm) };
        this.removeClass = function(classNm) { div.removeClass(classNm) };

        this.getView = function() {
            return div;
        }
    }
};

Object.defineProperty(ContextMenu_VOD, "VOD_BTN_TYPE", {
    value: { NONE : 0, ONLY_WISH: 1, ONLY_PL: 2, ALL : 3 },
    writable: false
});