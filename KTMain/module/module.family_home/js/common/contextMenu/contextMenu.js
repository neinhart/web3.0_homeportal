/**
 * Created by ksk91_000 on 2017-03-25.
 */
var ContextMenu = function () {
    var div = _$("<div/>", {class: 'related_menu type2'});
    var contextMenu = _$("<div/>", {class:'menu_area'});
    var btnArea = _$("<div/>", {class:'btn_area'});
    var listener;
    var menuList = [];
    var focus = 0;

    var openedDropBox;

    contextMenu.append(btnArea);

    div.append(_$("<div/>", {class:'background_dim'}));
    div.append(_$("<div/>", {class:'left_arrow'}));
    div.append(contextMenu);

    this.open = function () {
        div.addClass("activate");
        setFocus(focus);
        pauseTextAnimation();
    };

    this.close = function () {
        div.removeClass("activate");
        resumeTextAnimating();
    };

    this.isOpen = function () {
        return div.hasClass("activate");
    };

    this.addButton = function(title, classNm) {
        var item = _$("<btn/>", {class:"btn " + (classNm?classNm:"")}).text(title);
        menuList.push({type: ContextMenu.ITEM_TYPE.BUTTON, item:item});
        btnArea.append(item);
    };

    this.addSeparateLine = function (classNm) {
        var item = _$("<sepline/>", {class:"sep_line " + (classNm?classNm:"")});
        btnArea.append(item);
    };

    this.addTitle = function (title, classNm) {
        var item = _$("<div/>", {class:"title " + (classNm?classNm:"")}).text(title);
        btnArea.append(item);
    };

    this.addText = function (text, classNm) {
        var item = _$("<div/>", {class:"text " + (classNm?classNm:"")}).html(text);
        btnArea.append(item);
    };

    this.addDropBox = function (options, focus, classNm) {
        var item = new dropBox(options, classNm);
        item.setSelectedIdx(focus||0);

        menuList.push({type:ContextMenu.ITEM_TYPE.DROP_BOX, item:item});
        btnArea.append(item.getView());
    };

    this.setEventListener = function (eventListener) {
        listener = eventListener;
    };

    function setFocus(_focus) {
        menuList[focus].item.removeClass("focus");
        focus = _focus;
        menuList[focus].item.addClass("focus");
    }

    function changeFocus(amount) {
        var newFocus = HTool.getIndex(focus, amount, menuList.length);
        while(menuList[newFocus].item.hasClass("disable"))
            newFocus = HTool.getIndex(newFocus, amount>0?1:-1, menuList.length);
        setFocus(newFocus);
    }

    this.onKeyAction = function(keyCode) {
        if(openedDropBox) return openedDropBox.onKeyAction(keyCode);
        switch (keyCode) {
            case KEY_CODE.UP:
                changeFocus(-1);
                return true;
            case KEY_CODE.DOWN:
                changeFocus(1);
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
            case KEY_CODE.LEFT:
                this.close();
                return true;
            case KEY_CODE.RIGHT:
                return true;
        }
    };

    function enterKeyAction() {
        switch (menuList[focus].type) {
            case ContextMenu.ITEM_TYPE.BUTTON:
                listener(focus);
                return true;
            case ContextMenu.ITEM_TYPE.DROP_BOX:
                openedDropBox = menuList[focus].item;
                openedDropBox.open();
                return true;
        }
    }

    this.setDisable = function(index, enable) {
        menuList[index].item.toggleClass("disable", !enable)
    };

    this.setDropBoxFocus = function (index, focus) {
        if(menuList[index].type === ContextMenu.ITEM_TYPE.DROP_BOX){
            menuList[index].item.setSelectedIdx(focus);
        }
    };

    this.removeFocus = function () {
        menuList[focus].item.removeClass("focus");
    };

    this.addFocus = function (newFocus) {
        if(newFocus && !menuList[newFocus].hasClass("disable")) focus = newFocus;
        menuList[focus].item.addClass("focus");
    };

    this.getView = function () {
        return div;
    };

    function pauseTextAnimation() {
        var div = _$(".textAnimating").removeClass("textAnimating").addClass("pauseAnimationByCtxMenu");
        UTIL.clearAnimation(div.find("span"));
    }

    function resumeTextAnimating() {
        var div = _$(".pauseAnimationByCtxMenu").removeClass("pauseAnimationByCtxMenu").addClass("textAnimating");
        UTIL.startTextAnimation({
            targetBox: div
        })
    }

    var dropBox = function (options, classNm) {
        var selectedIdx;
        var indexList = [];
        var focusIdx = 0;
        var div = _$("<div/>", {class:"drop_box " + (classNm?classNm:"")}).html("<span class='text'></span><div class='options'></div>");
        var page = 0;

        (function init() {
            var optionArea = div.find(".options");
            for(var i=0; i<options.length && i<5; i++) {
                optionArea.append(_$("<div/>", {class: 'option'}).text(options[i]));
                indexList[i] = i;
            } optionArea.append("<div class='focus_line'/>");
            if(options.length>5) optionArea.append("<div class='scroll_bg'><div class='scroll_bar'/></div>");
            div.find(".scroll_bar").css("height", 317/Math.ceil(options.length/5) + "px");
        })();

        function sortOption() {
            var isSetOption = false;
            for(var i=0; i<options.length; i++) {
                if(i==selectedIdx) {
                    indexList[0] = i;
                    isSetOption = true;
                }else {
                    indexList[isSetOption?i:(i+1)] = i;
                }
            } setPage(0);
        }

        function setPage(_page) {
            page = _page;
            div.find(".checked").removeClass("checked");
            var optionArea = div.find(".options .option");
            var i=0;
            for(; i<(options.length - page*5)&&i<5; i++) {
                // if((page*5)+i==0) optionArea.eq(i).addClass("checked");
                if((page*5)+i==selectedIdx) optionArea.eq(i).addClass("checked");
                optionArea.eq(i).text(options[indexList[(page*5)+i]]);
            } for(; i<5; i++) {
                optionArea.eq(i).text("");
            } div.find(".scroll_bar").css("top", (317/Math.ceil(options.length/5)*page) + "px");
        }

        this.setSelectedIdx = function(index) {
            selectedIdx = index;
            div.find(".text").text(options[selectedIdx]);
            // sortOption();
            // setPage(0);
            setPage(Math.floor(selectedIdx/5));
        };

        this.setFocus = function(focus) {
            div.find(".focus").removeClass("focus");
            focusIdx = focus;
            if(Math.floor(focusIdx/5)!=page) setPage(Math.floor(focusIdx/5));
            div.find(".option").eq(focus%5).addClass("focus");
            div.find(".focus_line").css("-webkit-transform", "translateY(" + (63*(focusIdx%5)) + "px)");
        };

        this.open = function () {
            this.setFocus(selectedIdx);
            div.addClass("open");
        };

        this.close = function () {
            openedDropBox = null;
            div.removeClass("open");
        };

        this.onKeyAction = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    this.setFocus(HTool.getIndex(focusIdx, -1, options.length));
                    return true;
                case KEY_CODE.DOWN:
                    this.setFocus(HTool.getIndex(focusIdx, 1, options.length));
                    return true;
                case KEY_CODE.ENTER:
                    this.setSelectedIdx(indexList[focusIdx]);
                    this.close();
                    listener(focus, selectedIdx);
                    return true;
                case KEY_CODE.BACK:
                    this.close();
                    return true;
                case KEY_CODE.CONTEXT:
                case KEY_CODE.LEFT:
                    return true;
            }
        };

        this.addClass = function(classNm) { div.addClass(classNm) };
        this.removeClass = function(classNm) { div.removeClass(classNm) };
        this.hasClass = function(classNm) { return div.hasClass(classNm) };
        this.toggleClass = function(classNm, flag) { div.toggleClass(classNm, flag) };

        this.getView = function () {
            return div;
        }
    }
};

Object.defineProperty(ContextMenu, "ITEM_TYPE", {
   value: {"BUTTON":0, "DROP_BOX": 1}
});