/**.
 * VerticalPosterList등 포스터 하단에 올레 별점 표시용
 * (우리집 맞춤 TV>추천>VOD 포스터 하단 별점)
 * Created by lyllyl on 2017. 1. 5..
 */
window.stars = new function() {
    var setRate = function(div, rate) {
        var normal_icons = div.find(".star_icon .normal_img");
        var focus_icons = div.find(".star_icon .focus_img");
        var type = getType(div);
        var typeTxt = getTypeText(type);
        var i = 0;
        for(;i<Math.floor(rate); i++) {
            normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_filled_" + typeTxt + ".png");
            focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_filled_" + typeTxt + ".png");
        }
        var dotRate = Math.round(rate % 1 *10);
        if(dotRate>=1) {
            if(type>3) {
                if (rate % 1 < 0.25) {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_empty_" + getTypeText(type%4) + ".png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_empty_" + getTypeText(type%4) + "_f.png");
                } else if (rate % 1 > 0.75) {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_filled_" + typeTxt + ".png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_filled_" + typeTxt + ".png");
                } else {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + ".png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_f.png");
                }
            }else {
                if (dotRate >= 9) {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_filled_" + typeTxt + ".png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_filled_" + typeTxt + ".png");
                } else if (dotRate >= 7) {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_4.png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_4_f.png");
                } else if (dotRate >= 5) {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_3.png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_3_f.png");
                } else if (dotRate >= 3) {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_2.png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_2_f.png");
                } else {
                    normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_1.png");
                    focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_half_" + typeTxt + "_1_f.png");
                }
            } i++;
        }for(;i<5;i++) {
            normal_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_empty_" + getTypeText(type%4) + ".png");
            focus_icons.eq(i).attr("src", COMMON_IMAGE.STAR_POINT_PATH + "img_rating_empty_" + getTypeText(type%4) + "_f.png");
        }
    };

    function getView(type) {
        var div = _$("<div/>", {class:"rating_star_area type" + type});

        div.append(_$("<div/>", {class:"star_icon"}).append("<img class='normal_img'><img class='focus_img'>"));
        div.append(_$("<div/>", {class:"star_icon"}).append("<img class='normal_img'><img class='focus_img'>"));
        div.append(_$("<div/>", {class:"star_icon"}).append("<img class='normal_img'><img class='focus_img'>"));
        div.append(_$("<div/>", {class:"star_icon"}).append("<img class='normal_img'><img class='focus_img'>"));
        div.append(_$("<div/>", {class:"star_icon"}).append("<img class='normal_img'><img class='focus_img'>"));

        return div;
    }

    function getTypeText(type, onlySize) {
        switch (type) {
            case stars.TYPE.RED_20: return "20";
            case stars.TYPE.RED_26: return "26";
            case stars.TYPE.RED_29: return "29";
            case stars.TYPE.YELLOW_20: return "y_20";
            case stars.TYPE.YELLOW_26: return "y_26";
            case stars.TYPE.YELLOW_29: return "y_29";
            case stars.TYPE.YELLOW_36: return "y_36";
            case 3: return "36";
        }
    }

    function getType(div) {
        var tmp = div;
        if(!div.hasClass("rating_star_area")) tmp = div.find(".rating_star_area");

        if(tmp.hasClass("type0")) return stars.TYPE.RED_20;
        if(tmp.hasClass("type1")) return stars.TYPE.RED_26;
        if(tmp.hasClass("type2")) return stars.TYPE.RED_29;
        if(tmp.hasClass("type4")) return stars.TYPE.YELLOW_20;
        if(tmp.hasClass("type5")) return stars.TYPE.YELLOW_26;
        if(tmp.hasClass("type6")) return stars.TYPE.YELLOW_29;
        if(tmp.hasClass("type7")) return stars.TYPE.YELLOW_36;
    }

    return {
        setRate: setRate,
        getView: getView
    }
};

Object.defineProperty(stars, "TYPE", {
    value:{
        RED_20: 0, RED_26: 1, RED_29: 2, YELLOW_20: 4, YELLOW_26: 5, YELLOW_29: 6, YELLOW_36:7
    },
    writable:!1
});
