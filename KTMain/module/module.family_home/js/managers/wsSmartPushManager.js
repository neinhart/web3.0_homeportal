"use strict";

/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
myHome.wsSmartPushManager = (function(){
    var LIVE_URL = myHome.HTTP.SMART_PUSH.LIVE_URL;
    var TEST_URL = myHome.HTTP.SMART_PUSH.TEST_URL;
    var SERVICE_URL = DEF.RELEASE_MODE==="LIVE"?LIVE_URL:TEST_URL;

    var modelName = oipfAdapter.hwAdapter.getModelName();
    var dongCd = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV)).dongCd;

    var AjaxFactory = homeImpl.get(homeImpl.DEF_FRAMEWORK.AJAX_FACTORY);

    var ajax = function(async, timeout, url, postData, /**Function*/callback, parameters){
        return  AjaxFactory.createAjax("SmartPushManager", async, "get", "json", url, postData, undefined, timeout, callback, parameters);
    };
    
    function _ajaxStop(request){
        if(request) {
            log.printDbg("SmartPushManager AJAX request stop!!");
            request.abort();
            request = null;
        }
    };

    function _msgBoxNew(callback, last_date) {
        var api;
        var data;

        api = "/msgBox/msgBoxNew";
        data = "said=" + DEF.SAID + "&stb_type=" + (CONSTANT.IS_OTS?"OTS":"OTV") + "&home_version=" + DEF.UI_VERSION + "&stb_mv_gb=3&stb_model=" + modelName + "&last_date=" + last_date;
        // data = "said=TT170111001&stb_type=OTV&home_version=6.0.2.2016.08.06&stb_mv_gb=3&stb_model=GX-KT600EJ&last_date=0";

        return ajax(true, 0, SERVICE_URL + api, data, callback);
    }

    function _msgBoxList(callback) {
        var api;
        var data;

        api = "/msgBox/msgBoxList";
        data = "said=" + DEF.SAID + "&stb_type=" + (CONSTANT.IS_OTS?"OTS":"OTV") + "&home_version=" + DEF.UI_VERSION + "&stb_mv_gb=3&stb_model=" + modelName;
        // data = "said=TT170111001&stb_type=OTV&home_version=6.0.2.2016.08.06&stb_mv_gb=3&stb_model=GX-KT600EJ"; // 알림, 이벤트 모두 있는 경우
        // data = "said=TT170111004&stb_type=OTV&home_version=6.0.2.2016.08.06&stb_mv_gb=3&stb_model=GX-KT600EJ"; // 알림, 이벤트 모두 없는 경우
        return ajax(true, 0, SERVICE_URL + api, data, callback);
    }
    
    return {
        ajaxStop:_ajaxStop,
        msgBoxNew:_msgBoxNew,
        msgBoxList:_msgBoxList
    };
}());