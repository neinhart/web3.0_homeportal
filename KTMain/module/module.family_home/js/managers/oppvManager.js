"use strict";

/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
myHome.oppvManager = (function(){
    var usageId;
    var api;
    var data;
    
    function getUrl() {
    	if (DEF.RELEASE_MODE === "LIVE" || DEF.RELEASE_MODE === "BMT") {
    		return myHome.HTTP.SKY_RP_SERVER.LIVE_URL;
    	} else {
    		return myHome.HTTP.SKY_RP_SERVER.TEST_URL;
    	}
    }
    
	/**
	 * ajax를 이용하여 json 데이터 수집<br>
	 * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
	 * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
    var ajax = function(async, timeout, url, postData, /**Function*/callback, parameters){
        return  ajaxFactory.createAjax("oppvManager", async, "post", "json", url, postData, undefined, timeout, callback, parameters);
    };
    
    function _ajaxStop(request){
        if(request){
            KTW.utils.Log.printDbg("OPPV AJAX request stop!!");
            request.abort();
            request = null;
        }
    };

    function _searchOppInfo(callback, chlNo, scId) {
        var obj;
        log.printDbg("searchOppvInfo !!!");
        log.printDbg("chlNo === " + chlNo);
        log.printDbg("scId === " + scId);
        
        api = "/rpjson/GatewayRelay";
        
        obj = new Object();
        
        obj.method = "searchOppvInfo";
        obj.chlNo = "" + chlNo;
        obj.scId = scId;
        
        log.printDbg(JSON.stringify(obj));
        
        return ajax(true, 0, getUrl() + api, JSON.stringify(obj), callback);
    }
    
    function _registOppv(callback, scId, ppvSvcId, svcOpenDh, svcCloseDh, usgAmt, svcMthCd, juminBizNo, telNo, etpsType, orderIpAddr, parameters) {
        var obj;
        
        log.printDbg("registOppv !!!");
        log.printDbg("scId ===== " + scId);
        log.printDbg("ppvSvcId ===== " + ppvSvcId);
        log.printDbg("svcOpenDh ===== " + svcOpenDh);
        log.printDbg("svcCloseDh ===== " + svcCloseDh);
        log.printDbg("usgAmt ===== " + usgAmt);
        log.printDbg("svcMthCd ===== " + svcMthCd);
        log.printDbg("juminBizNo ===== " + juminBizNo);
        log.printDbg("telNo ===== " + telNo);
        log.printDbg("etpsType ===== " + etpsType);
        log.printDbg("orderIpAddr ===== " + orderIpAddr);
        log.printDbg("scId ===== " + scId);

        api = "/rpjson/GatewayRelay";
        obj = new Object();
        obj.method = "registOppv";
        obj.scId = scId;
        obj.ppvSvcId = ppvSvcId;
        obj.svcOpenDh = svcOpenDh;
        obj.svcCloseDh = svcCloseDh;
        obj.usgAmt = usgAmt;
        obj.svcMthCd = svcMthCd;
        obj.juminBizNo = juminBizNo;
        obj.telNo = telNo;
        obj.etpsType = etpsType;
        obj.orderIpAddr = orderIpAddr;
        log.printDbg(JSON.stringify(obj));
        
        return ajax(true, 0, getUrl() + api, JSON.stringify(obj), callback, parameters);
    }
    
    function _searchContractInfo(callback, scId) {
        var obj;
        log.printDbg("searchContractInfo !!!");
        log.printDbg("scId === " + scId);
    	
        api = "/rpjson/GatewayRelay";
        obj = new Object();
        obj.method = "searchContractInfo";
        obj.scId = scId;
        log.printDbg(JSON.stringify(obj));

        return ajax(true, 0, getUrl() + api, JSON.stringify(obj), callback);
    }
    
    function _searchHistory(callback, scId) {
        var obj;
        
        log.printDbg("searchHistory !!!");
        log.printDbg("scId === " + scId);
    	
        api = "/rpjson/GatewayRelay";
        obj = new Object();
        obj.method = "searchHistory";
        obj.scId = scId;
        log.printDbg(JSON.stringify(obj));

        return ajax(true, 0, getUrl() + api, JSON.stringify(obj), callback);
    }
    
    function _requestOptionalVodUpselling(callback, scId, prdtCd, regrUserId, vTermCntrctTypeCd) {
        var obj;
        
        log.printDbg("requestOptionalVodUpselling !!!");
        log.printDbg("scId === " + scId);
        log.printDbg("prdtCd === " + prdtCd);
        log.printDbg("regrUserId === " + regrUserId);
        log.printDbg("vTermCntrctTypeCd === " + vTermCntrctTypeCd);
    	
    	regrUserId = "KTHOME";
        api = "/rpjson/HomePortalUpselling";
        obj = new Object();
        obj.method = "requestOptionalVodUpselling";
        obj.scId = scId;
        obj.prdtCd = prdtCd;
        obj.regrUserId = regrUserId;
        obj.vTermCntrctTypeCd = vTermCntrctTypeCd;
        log.printDbg(JSON.stringify(obj));

        return ajax(true, 0, getUrl() + api, JSON.stringify(obj), callback);
    }
    
    return {
        ajaxStop:_ajaxStop,
        searchOppInfo:_searchOppInfo,
        registOppv:_registOppv,
        searchContractInfo:_searchContractInfo,
        searchHistory:_searchHistory,
        requestOptionalVodUpselling:_requestOptionalVodUpselling
    };
    
}());