"use strict";
/**
 * @class 추천서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * 
 */
myHome.VodRecmManager = (function(){
	function getUrl(){
	    //return "http://webrecom2.ktipmedia.co.kr:7002/";//old-live

        // app.properties 관련 내용 삭제
        // getProperty('TOPCONTENT_TEST') 가 어떤 역활을 하는지 모르겠음
        //if(KTW.utils.util.getProperty('TOPCONTENT_TEST') || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.TEST) {
        if (DEF.RELEASE_MODE === CONSTANT.ENV.TEST) {
	        return myHome.HTTP.RECOMMEND.TEST_URL;
	    }
	    else {
	        return myHome.HTTP.RECOMMEND.LIVE_URL;
	    }
		//return "http://125.140.114.151:7002/";//test. getTopContent 는 여기서만 동작.
	}

    /**
     * 2017.06.15 dhlee
     * STB 유형별로 version 정보를 반환한다.
     *
     * @returns {string}
     */
    function getVersion() {
		if(CONSTANT.STB_TYPE.ANDROID === true) {
			return "1.2";
		}else {
			if (CONSTANT.IS_HDR) {
				return "3.2";
			} else {
				return "3.1";
			}
		}
    }
	
	function ajax(async, timeout, url, postData, /**Function*/callback) {
		//(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
		return ajaxFactory.createAjax("VOD Recommand Manager", async, "post", "json", url, postData, true, 0, callback, null);
	}
	
	function _ajaxStop(request){
	    if(request){
	        log.printDbg("AJAX request stop!!");
	        request.abort();
	        request = null;
	    }
	};
	
	/**
	 * 상세보기, 종료 시 추천 VOD
	 * "ITEM_VOD2" : "ITEM_VOD", 라이브 VOD 추천 
	 */
	function _getRelatedContents(recType, recGbCode, said, product_list, consid, catid, hasAdult, itemCnt, callback) {
		log.printDbg("_getRelatedContents !!");
		
		//var api = VODRECM_URL + "wasRCS/get_contents_by_default";
		var api = getUrl() + "wasRCS/recommend";
		var postData = {
					'CONTENT':"VOD",
					'REC_TYPE':recType,
					'REC_GB_CODE': recGbCode,	
				 	'USER_ID':said,
				 	'PRODUCT_LIST':product_list,
				 	'REQ_DATE':UTIL.getCurrentDateTime(),
				 	'REQ_CODE':(CONSTANT.STB_TYPE.ANDROID === true ? "G" : "E"),
				 	'SVC_CODE':(CONSTANT.IS_OTS === true ? "OTS" : "OTV"),
				 	'CONS_ID':consid,
				 	'CAT_ID':catid,
				 	'CAT_TYPE':hasAdult == 'Y'?"Adult":"null",
				 	'ITEM_CNT':itemCnt,
				 	'CONS_RATE':"N", //필터링 옵션임. Y가 19세이상 필터링 (무조건 "N"으로 호출)
                    'STB_VER': getVersion()
				 	//'STB_VER': KTW.CONSTANT.IS_UHD === true ? "1.1" : "1.0"
		};
		
		return ajax(true,0,api,postData, callback);
	};
	
	function _getCuration(said, rec_type, product_list, hasAdult, itemCnt, callback) {
		log.printDbg("_getCuration !!");
		
		var api = getUrl() + "wasRCS/curation";
		var postData = {
					'CONTENT':"VOD",
					'REC_TYPE':rec_type,
				 	'USER_ID':said,
				 	'PRODUCT_LIST':product_list,
				 	'REQ_DATE':UTIL.getCurrentDateTime(),
				 	'REQ_CODE':"E",
				 	'SVC_CODE':(CONSTANT.IS_OTS === true ? "OTS" : "OTV"),
				 	'MENU_MAX_CNT':10,
				 	'MENU_MIN_CNT':10,
				 	'ITEM_MAX_CNT':itemCnt,
				 	'ITEM_MIN_CNT':10,
				 	'CONS_RATE':hasAdult, //필터링 옵션임. Y가 19세이상 필터링
				 	//'STB_VER': KTW.CONSTANT.IS_UHD === true ? "1.1" : "1.0"
                    'STB_VER': getVersion(),
					'STB_mode': (window.KidsModeManager.isKidsMode() ? "Kids":"Normal")
		};
		
		return ajax(true,0,api,postData, callback);
	};
	
	function _getTopContent(product_list, has_adult, item_cnt, callback) {
		var api = getUrl() + "wasRCS/getTopContent";
		var postData = {
					'CONTENT':"VOD",
					'REC_TYPE':'TOP_MOVIE',
				 	'USER_ID':DEF.SAID,
				 	'PRODUCT_LIST':product_list,
				 	'REQ_DATE':UTIL.getCurrentDateTime(),
				 	'REQ_CODE':"E",
				 	'SVC_CODE':(CONSTANT.IS_OTS === true ? "OTS" : "OTV"),
				 	'ITEM_CNT':(item_cnt ? Math.min(item_cnt,10) : 5), //max 10
				 	'CONS_RATE':has_adult, //필터링 옵션임. Y가 19세이상 필터링
				 	//'STB_VER': KTW.CONSTANT.IS_UHD === true ? "1.1" : "1.0"
                    'STB_VER': getVersion()
		};
		
		return ajax(true,0,api,postData, callback);
	}
	
	return {
		ajaxStop: _ajaxStop,
		getRelatedContents: _getRelatedContents,
		getCuration: _getCuration,
		getTopContent:_getTopContent
	}	
	
}());
