"use strict";
/**
 * 추천 고도화 API
 */
myHome.curationManager = (function() {

    function getUrl() {
        if (DEF.RELEASE_MODE === CONSTANT.ENV.BMT || DEF.RELEASE_MODE === CONSTANT.ENV.LIVE) {
            return myHome.HTTP.CURATION.LIVE_URL;
        } else {
            return myHome.HTTP.CURATION.TEST_URL
        }
    }

    function getPkgCode() {
        var baseCd = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
        var pkgList = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);

        return  baseCd + (pkgList?("," + pkgList):"");
    }

    /**
     * ajax를 이용하여 json 데이터 수집<br>
     * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
     * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    function ajax(url, post_data, /**Function*/callback, params) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return ajaxFactory.createAjax("curationManager", true, "post", "json", url, post_data, true, -1, callback, params);
    };

    function _ajaxStop(request) {
        ajaxFactory.stopAjax(request);
    };

    var REQ_CODE = CONSTANT.STB_TYPE.ANDROID ? "G" : "E",
        SVC_CODE = CONSTANT.IS_OTS ? "OTS" : "OTV",
        STB_VER = CONSTANT.STB_TYPE.ANDROID ? "1.2" : (CONSTANT.IS_HDR ? "3.2" : "3.1");

    /**
     * no_adult -> true -> 성인 미노출
     * is_save -> true -> save else cancel
     * content_list -> 대상 list [content id:rating, content id:rating, content id:rating...] max 값 명시 안되어 있음.
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     */
    function makePreference(no_adult, is_save, content_list, callback, params) {
        var api = "/makePreference";

        var svc_code = SVC_CODE;
        var cons_rate = (no_adult === true ? "Y" : "N");
        var stb_ver = STB_VER;
        var req_type = is_save === true ? "S" : "C";

        var post_data = "CONTENT=VOD&USER_ID=" + DEF.SAID + "&PRODUCT_LIST=" + getPkgCode() + "&REQ_CODE=E&SVC_CODE=" + svc_code + "&CONS_RATE=" + cons_rate + "&STB_VER=" + stb_ver + "&REQ_TYPE=" + req_type;

        for (var i = 0; i < content_list.length; i++) {
            post_data += "&CONS_LIST=" + content_list[i].CONS_ID + ":" + (content_list[i].RATING ? content_list[i].RATING : 0);
        }

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     */
    function getMyPreference(no_adult, callback, params) {
        var api = "/getMyPreference";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     */
    function getRatingContent(no_adult, callback, params) {
        var api = "/getRatingContent";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     *
     * max_count : 결과 최대 개수 (기본 100)
     * sort_type : 'D' 등록일수 'R' 별점 순 (기본 'D')
     */
    function getMyRatingContent(no_adult, max_count, sort_type, callback, params) {
        var api = "/getMyRatingContent";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER,
            'MAX_ITEM_CNT': max_count ? max_count : 100,
            'SORTING': sort_type ? sort_type : "D",
            'PAGE': 1,
            'ITEM_NUM': 200
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     */
    function initMyPreference(no_adult, callback, params) {
        var api = "/initMyPreference";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     * content_id -> 추천 제외 ID.
     * is_save -> 저장 / 취소
     */
    function saveFilteringContent(no_adult, content_id, is_save, callback, params) {
        var api = "/saveFilteringContent";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER,
            'CONS_ID': content_id,
            'REQ_TYPE': is_save === true ? "S" : "C"
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     * content_id -> 컨텐츠 ID.
     * rating -> 별점 점수 (0~5)
     * req_type -> 'V' : 상세 / 'P' : VOD 시청 종료 / 'M' : 마이메뉴
     */
    function saveRating(no_adult, content_id, rating, req_type, callback, params) {
        var api = "/saveRating";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER,
            'CONS_ID': content_id,
            'REQ_TYPE': req_type,
            'RATING': rating
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     * content_id - 컨텐츠 ID.
     */
    function getMyRating(no_adult, content_id, callback, params) {
        var api = "/getMyRating";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            /*SU_ID: "", optional 필요한지 여부 확인 필요. mook_t */
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': (no_adult === true ? "Y" : "N"),
            'STB_VER': STB_VER,
            'CONS_ID': content_id
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    return {
        'makePreference': makePreference,
        'getMyPreference': getMyPreference,
        'getRatingContent': getRatingContent,
        'getMyRatingContent': getMyRatingContent,
        'initMyPreference': initMyPreference,
        'saveFilteringContent': saveFilteringContent,
        'saveRating': saveRating,
        'getMyRating': getMyRating,
        'ajaxStop': _ajaxStop,
    };
}());