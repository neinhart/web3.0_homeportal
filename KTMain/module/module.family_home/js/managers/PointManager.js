/**
 * Created by kh.kim2 on 2018-01-22.
 */
"use strict";
/**
 * @class TV포인트 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 *
 */
myHome.PointManager = (function(){

    function getUrl() {
        if (DEF.RELEASE_MODE === CONSTANT.ENV.BMT || DEF.RELEASE_MODE === CONSTANT.ENV.LIVE) {
            return myHome.HTTP.LUPIN.LIVE_URL;
        } else {
            return myHome.HTTP.LUPIN.TEST_URL
        }
    }

    /**
     * ajax를 이용하여 데이터 수집<br>
     * 응답은 json 데이터로 오지만 요청헤더 contentType은 text로 보내야한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    function ajax(async, timeout, url, postData, /**Function*/callback) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return ajaxFactory.createAjax("PointManager", async, "post", "json", url, postData, true, -1, callback, null);
    };

    function ajax2(async, timeout, url, postData, /**Function*/callback) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return ajaxFactory.createAjaxServerTimeout("PointManager", async, "post", "json", url, postData, true, 6000, callback, null);
    };

    function _ajaxStop(request){
        if(request){
            KTW.utils.Log.printDbg("AJAX request stop!!");
            request.abort();
        }
    };

    function getIP(){
        var configuration = KTW.oipf.oipfObjectFactory.createConfigurationObject();
        KTW.utils.Log.printDbg("ip:" + configuration.localSystem.networkInterfaces[0].ipAddress);
        return configuration.localSystem.networkInterfaces[0].ipAddress;
    }

    /**
     *
     * @param point_type : "1": myMenu 에서, 이용권 포함 호출
     *                     "9": 결제 진행에서 호출, 포인트, 쿠폰 만 호출
     * @param callback
     * @returns {*}
     * @private
     */
    // Lupin 서버 신규 API
    function _getPoint(point_type, callback) {
        var postData = {
            settlIdTypeCd: "7", // 결제 아이디 타입
            settlId      : KTW.SAID, // 결제 아이디
            storId       : "KTPGMTV002", // 가맹점 ID (현재는 사용하지 않음)
            retvTypeCd   : point_type? point_type : "9", // 조회 유형 코드
            ipadr        : getIP(), // IP address
            adtnInfo     : "" // 부가 정보
        };

        KTW.utils.Log.printDbg("getTvPoint");
        return ajax2(true, 0, getUrl()+"retvPoint.json", postData, callback);
    }

    return {
        ajaxStop: _ajaxStop,
        getPoint: _getPoint
    };
}());