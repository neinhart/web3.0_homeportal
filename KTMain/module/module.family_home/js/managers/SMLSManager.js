"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
myHome.SMLSManager = (function(){
    var api;
    var data;
    var AjaxFactory = homeImpl.get(homeImpl.DEF_FRAMEWORK.AJAX_FACTORY);

    function getUrl(){
        if (DEF.RELEASE_MODE === CONSTANT.ENV.BMT || DEF.RELEASE_MODE === CONSTANT.ENV.LIVE) {
			return myHome.HTTP.SMLS.LIVE_URL;
		}else{
			return myHome.HTTP.SMLS.TEST_URL;
		}
    }
    /*
    function getHDSUrl() {
    	var url;
    	if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            url = "http://usbsso.megatvdnp.co.kr/"; //개발용 [라이브: ktpay.kt.com]
        }else{
            url = "https://125.147.35.170/"; //test.
        }
    	//http://usbsso.megatvdnp.co.kr/HDSollehtvnowSDP/HDSollehtvnowSDP.asmx?op=ReqMSaidSDP
    	return url;
    }
    */
	/**
	 * ajax를 이용하여 json 데이터 수집<br>
	 * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
	 * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
	var ajax = function(async, timeout, url, postData, /**Function*/callback, parameters){
		//(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
		return AjaxFactory.createAjax("Seamless", async, "post", "json", url, postData, undefined, timeout, callback, parameters);
	};
	
	function _ajaxStop(request){
	    if(request){
	        KTW.utils.Log.printDbg("SeamLess AJAX request stop!!");
	        request.abort();
	        request = null;
	    }
	};
	
    function _setOTVPassedTime(callback, saId, contsId, stopTime){
        api = "/vod/setOTVPassedTime";
        data = "saId="+saId+"&contsId="+contsId+"&stopTime="+stopTime;
        return ajax(true,0,getUrl()+api,data,callback);
    };
    
    function _recvBuyItemInfo(callback, saId, itemId, buyDT, buyEndDT, itemType, buyAmount, buyTypeCd, ltermYN, refundYN, viewYN, sersId, resolCd){
        api = "/vod/recvBuyItemInfo";
        data = "saId="+saId+"&itemId="+itemId+"&buyDT="+buyDT+"&buyEndDT="+buyEndDT+"&itemType="+itemType+
        			"&buyAmount="+buyAmount+"&buyTypeCd="+buyTypeCd+"&ltermYN="+ltermYN+"&refundYN="+refundYN+"&viewYN="+viewYN+
                    "&sersId="+sersId+"&resolCd="+resolCd;
        return ajax(true,0,getUrl()+api,data,callback);
    };
    
    function _checkBuyShareItemExist(callback, saId, contId, systemFlag, parameters){ 
        api = "/vod/checkBuyShareItemExist";
        data = "saId="+saId+"&contId="+contId+"&systemFlag="+systemFlag;
        return ajax(true,0,getUrl()+api,data,callback,parameters);
    };
    
    function _getOTNBuyShareHistoryList(callback, otvSaId){
        api = "/vod/getOTNBuyShareHistoryList";
        var uhd = KTW.CONSTANT.IS_UHD === true ? 'Y' : 'N';
        // taxYn (부가세 가격) 파라미터 추가
        data = "otvSaId="+otvSaId + "&uhdYn=" + uhd + "&taxYn=Y";
        return ajax(true,0,getUrl()+api,data,callback);
    };
    
    // OTV 어플에 페어링된 가입자 정보 전달
    function _getMadePairingInfoList(callback, otvSaId) {
    	api = '/vod/getMadePairingInfoList';
    	data = "otvSaId=" + otvSaId;
    	return ajax(true, 0, getUrl() + api, data , callback);
    };
    
	// otnSaid가 페어링 되어 있는지 체크하는 API
    function _checkExistPairingInfo(callback, otvSaId) {
    	api = '/vod/checkExistPairingInfo';
    	data = "otvSaId=" + otvSaId;
    	///vod/ checkExistPairingInfo?otnSaId=otv000001
    	return ajax(true, 0, getUrl() + api, data , callback);
    }
    
    // 페어링 삭제 요청'TT150319017', 'TT130426916', 'ilikeyou82', '3893316')
    function _deleteMadeParingInfo(callback, otvSaId, otnSaId) {
    	api = "/vod/deleteMadePairingInfo";
    	data = "otvSaId=" + otvSaId + "&otnSaId=" + otnSaId;
    	return ajax(true, 0, getUrl() + api, data , callback);
    }
    
    function _test(ip, port){
    	data = "otvSaId=1111&otnSaId=2222";
    	return ajax(true, 0, "http://"+ ip + ":1717/ipstb/rtcontrol", data , function(){});
    }
    
    function _test2(ip, port){
    	data = "otvSaId=1111&otnSaId=2222";
    	return ajax(true, 0, "http://"+ ip + ":1717/ipstb/test", data , function(){});
    }
    
    /*
    // 가입자 페어링 
    function _reqMakePairingInfo(callback, otvSaId, otnSaId, otnId, otnIdAliasNm) {
    	api = "/vod/reqMakePairingInfo";
    	// dowdow test 아래 said 바꾸어야함.
    	data = "otvSaid=" + otvSaId + "&otnSaId=" + otnSaId + "&otnId="+ otnId + "&otnIdAliasNm=" + otnIdAliasNm;//kt234567
    	return ajax(true, 0, getUrl() + api, data, callback);
    		
    };
    
    // 가입자 인증
    function _reqMSaidSDP(callback, ollehId, password) {
    	var url = "http://usbsso.megatvdnp.co.kr/HDSollehtvnowSDP/HDSollehtvnowSDP.asmx";//op=ReqMSaidSDP
        var soapBody = '<?xml version="1.0" encoding="utf-8"?>'+
                   '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                      '<soap:Body>'+
                        '<ReqMSaidSDP xmlns="http://tempuri.org/">'+
                          '<oInfo>'+
                            '<ID>'+ollehId+'</ID>'+
                            '<PWD>'+password+'</PWD>'+
                            '<MODEL>'+'AIP-100'+'</MODEL>'+
                          '</oInfo>'+
                        '</ReqMSaidSDP>'+
                      '</soap:Body>'+
                   '</soap:Envelope>';
        $.ajax({
            type: "post",
            url: url,
            contentType: "text/xml",
            data: soapBody,
            dataType: "xml",
            processData: false,
            success: function( data, status, req, xml, xmlHttpRequest, responseXML ){
            	console.dir(data);
//                var myObj = new Array();
//                var rcode = $(req.responseXML).find('ReqMSaidSDPResult').find('RCode').text();
//                var rmsg = $(req.responseXML).find('ReqMSaidSDPResult').find('RMsg').text();
//                if (rcode === 'SHD0000100') { // 성공
//                	var otnSaid = $(req.responseXML).find('ReqMSaidSDPResult').find('QSaid').each(function() {
//                    	myObj.push($(this).text());
//                    })
//                    console.log(myObj);
//                    callback(myObj);	
//                }
//                else {
//                	
//                }
            	callback(req);
                                
            },
            error: function( data, status, req){
                console.log( "ERROR", req.responseText );
                // 에러처리.
                
                // req.responseText
                // status 
            }
        });
    }
    
    // ukey:5134167b1d11f245db7ac71253c95c07127e7ce300ddb56879b980777d1462d90518b7a469fbcfff8680442e478f4dde
    // sms 본인확인
    function _reqSmsQook(callback, id, rnum, ttype, mobile, ukey) {
    	var url = "http://usbsso.megatvdnp.co.kr/HDSollehtvnowSDP/HDSollehtvnowSDP.asmx";//?op=ReqMSaidSDP
        var soapBody = '<?xml version="1.0" encoding="utf-8"?>'+
                   '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                      '<soap:Body>'+
                        '<ReqSmsQook xmlns="http://tempuri.org/">'+
                          '<oInfo>'+
                            '<ID>'+ollehId+'</ID>'+
                            '<PWD>'+password+'</PWD>'+
                            '<MODEL>'+'AIP-100'+'</MODEL>'+
                          '</oInfo>'+
                        '</ReqMSaidSDP>'+
                      '</soap:Body>'+
                   '</soap:Envelope>';
        $.ajax({
            type: "post",
            url: url,
            contentType: "text/xml",
            data: soapBody,
            dataType: "xml",
            processData: false,
            success: function( data, status, req, xml, xmlHttpRequest, responseXML ){
            	console.dir(data);
                var myObj = new Array();
                var otnSaid = $(req.responseXML).find('ReqMSaidSDPResult').find('QSaid').each(function() {
                	myObj.push($(this).text());
                })
                console.log(myObj);
                callback(myObj);                
            },
            error: function( data, status, req){
                console.log( "ERROR", arguments );
                // req.responseText
                // status 
            }
        });
    }
	*/
    return {
    	ajaxStop:_ajaxStop,
    	setOTVPassedTime:_setOTVPassedTime,
    	recvBuyItemInfo:_recvBuyItemInfo,
    	checkBuyShareItemExist:_checkBuyShareItemExist,
    	getOTNBuyShareHistoryList:_getOTNBuyShareHistoryList,
    	checkExistPairingInfo:_checkExistPairingInfo,
    	getMadePairingInfoList:_getMadePairingInfoList,
    	deleteMadeParingInfo:_deleteMadeParingInfo,
    	test:_test,
    	test2:_test2
//    	reqMSaidSDP:_reqMSaidSDP,
//    	getMadePairingInfoList:_getMadePairingInfoList,
    	
    };
}());
