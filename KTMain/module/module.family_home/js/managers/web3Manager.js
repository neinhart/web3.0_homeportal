"use strict";
myHome.web3Manager = (function () {
    var usageId;
    var api;
    var data;

    var officeCd;
    var model;

    var IS_RELEASE = DEF.RELEASE_MODE=="LIVE";

    function getOfficeCode() {
        if(!officeCd) officeCd = StorageManager.ps.load(StorageManager.KEY.OFFICE_CODE);
        return officeCd;
    }

    function getModelName() {
        if(!model) model = oipfAdapter.hwAdapter.getModelName();
        return model;
    }

    function getUrl() {
        if(IS_RELEASE) return myHome.HTTP.WEB_3.LIVE_URL;
        else return myHome.HTTP.WEB_3.BMT_URL;
    }

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AMOC AJAX request stop!!");
            try {
                request.abort();
            } catch (e) {
                log.printExec(e.message);
            }

            request = null;
        }
    }

    function _getKidsList(callback) {
        api = "/deploy-api/kids/list";
        data = {
            saId: DEF.SAID,
            officeCd: getOfficeCode(),
            model: getModelName()
        };

        // return ajax(true, 0, myHome.HTTP.WEB_3.URL + api, data, callback);
        return request(callback, getUrl() + api, data);
    }

    function _getFavoriteMenuInfo(callback) {
        api = "/deploy-api/favoriteMenu/info";
        data = {
            saId: DEF.SAID,
            officeCd: getOfficeCode(),
            model: getModelName()
        };
        return request(callback, getUrl() + api, data);
    }

    function _setFavoriteMenuAdd(list, callback) {
        api = "/deploy-api/favoriteMenu/add";
        data = {
            saId: DEF.SAID,
            officeCd: getOfficeCode(),
            model: getModelName(),
            list: list
        };
        return request(callback, getUrl() + api, data);
    }

    function request(callback, url, data, async, parameters) {
        try {
            var paramData = data;
            return _$.ajax({
                url: url,
                type: "post",
                dataType: 'json',
                async: async == undefined ? true : async,
                contentType: "application/json; charset=utf-8",
                timeout: 5e3,
                data: JSON.stringify(paramData),
                crossOrigin: false,
                success: function (data) {
                    log.printDbg("[HTTP] ajax success [Web3.0Manager] ###########");
                    log.printLongDbg("[HTTP] result = " + JSON.stringify(data));
                    callback(true, data, parameters);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    log.printErr("[HTTP] Error return [Web3.0Manager] ###########");
                    log.printErr("[HTTP] request : ", JSON.stringify(XMLHttpRequest));
                    log.printErr("[HTTP] status : ", JSON.stringify(textStatus));
                    log.printErr("[HTTP] error", JSON.stringify(errorThrown));
                    callback(false, XMLHttpRequest, parameters);
                }
            });
        } catch(exception) {
            log.printErr("[HTTP] catch exception [Web3.0Manager] ###########");
            log.printErr("[HTTP] exception",JSON.stringify(exception));

            callback(false, exception, parameters);

            return null;
        }
    }

    return {
        ajaxStop            : _ajaxStop,
        getKidsList         : _getKidsList,
        setFavoriteMenuAdd  : _setFavoriteMenuAdd,
        getFavoriteMenuInfo : _getFavoriteMenuInfo
    }
}());
