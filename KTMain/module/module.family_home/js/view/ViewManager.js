/**
 * Created by ksk91_000 on 2016-08-19.
 */
myHome.ViewManager = function () {
    var parent;
    var div = _$("<div/>", {class:'viewArea'});

    var currentCateViewData;
    var currentContViewData;
    var ViewDataMap = {};
    var historyStack = [];
    var contentsChangeTimeout = null;
    var adultAuthViewData = new ViewData("adultAuthView", myHome.view.AdultAuthView, null, "");
    var adultAuthClear = false;

    var bg_timer, curBgIdx = 0;
    var emptyViewData = new ViewData("emptyView", View);

    var MODE = {CATEGORY:0, CONTENTS:1};
    var mode = MODE.CATEGORY;

    var oldInfo;
    var curInfo;

    div.append(_$("<div/>", {class:"context_menu_indicator"}));

    //var showDefalutBackground = function (isShow) {
    //
    //    /**
    //     * [dj.son] [WEBIIIHOME-3673] Layer 가 null 일 경우 예외처리 추가
    //     */
    //    var lastLayer = LayerManager.getLastLayerInStack({key: "type", value: Layer.TYPE.NORMAL});
    //
    //    if(lastLayer && lastLayer.id == "MyHomeForYou") {
    //        // [si.mun] 아래 코드를 주석처리 하지 않으면
    //        // 내가 평가한 콘텐츠 카테고리 > 우측 키 > 콘텐츠로 포커스가 변경이 됨
    //        // 이때 isShow 는 false로 전달이 되면서 #defaultBg를 display: none 상태로 변경한다.
    //        // 그런데, MyHomeForYou layer의 defaultBg는 category 영역을 draw 하는 tag 이므로
    //        // display: none 처리를 하면 안된다. 때문에 주석처리를 하였음
    //        // 결과적으로, showDefalutBackground 함수는 어떠한 역할도 하지 않게 되기 때문에
    //        // 주석처리하였음.
    //
    //        //if(isShow) parent.find("#defaultBg").css("display", "");
    //        //else parent.find("#defaultBg").css("display", "none");
    //    }
    //};

    var cateKeyAction = function(keyCode) {
        if(currentCateViewData.view.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.UP:
            case KEY_CODE.DOWN:
            case KEY_CODE.RED:
            case KEY_CODE.BLUE:
                setContentsChangeTimeout(function() {
                    setCurrentContent(currentCateViewData.view.getCurrentContent())
                }, 300);
                try {
                    oldInfo = oldCateView.getCateInfo();
                    var prevInfo = oldView ;
                    NavLogMgr.collectFHMenu(keyCode, oldInfo.id, oldInfo.name, prevInfo.id, prevInfo.name);
                } catch(e) {}
                return true;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                contensChangeImmediately();
                changeFocus(currentContViewData);
                try {
                    if(getCurrentView() != adultAuthViewData) {
                        oldInfo = currentCateViewData;
                        curInfo = currentContViewData;
                        NavLogMgr.collectFHMenu(keyCode, oldInfo.id, oldInfo.title, curInfo.id, curInfo.title);
                    }
                } catch(e) {}
                return true;
        }
    };

    var reloadView = function() {
        contensChangeImmediately();
        changeFocus(currentContViewData);
    };

    var changeFocus = function(viewData) {
        var focusMode;
        switch (viewData.getView().type) {
            case View.TYPE.CATEGORY:
                if(getCurrentView()) {
                    historyStack.push(getCurrentView());
                }
                setCurrentCategory(viewData);
                return true;
            case View.TYPE.CONTENTS:
                // 카테고리 unfocus
                currentCateViewData.view.unfocused();

                // 현재 view가 존재하면 push
                if(getCurrentView()) {
                    historyStack.push(getCurrentView());
                }
                mode = MODE.CONTENTS;

                if(currentContViewData !== viewData) {
                    setCurrentContent(viewData);
                }
                focusMode = viewData.view.focused();

                if(focusMode == myHome.ViewManager.FOCUS_MODE.NO_FOCUS) {
                    historyBack();
                } else if(focusMode == myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS) {
                    if (currentCateViewData && currentCateViewData.view.unfocused) {
                        currentCateViewData.view.unfocused();
                    }
                } else {
                    if(currentCateViewData) {
                        //currentCateViewData.view.hide();
                    }
                    //showDefalutBackground(false);
                    changeTitle(viewData.title);
                }

                if(focusMode == myHome.ViewManager.FOCUS_MODE.NO_FOCUS && viewData.viewId == 'myRatingContent') {
                    setTimeout(reloadView, 30);
                } return focusMode;
        }
    };

    var changeCategoryFocus = function(idx, isEnter) {
        if(idx<0) return;
        if(mode == MODE.CATEGORY) {
            currentCateViewData.view.setFocus(idx);
        } setCurrentContent(currentCateViewData.view.getCurrentContent());
        if(isEnter && changeFocus(currentContViewData)) historyStack = [];
    };

    var changeTitle = function(title) {
        parent.find("#backgroundTitle").text(title);
    };

    var changeBackground = function (src, time) {
        var bgArea = VBOBackground;
        if(bg_timer) clearTimeout(bg_timer);
        bg_timer = setTimeout(function () {
            if(src==null) {
                bgArea.find("#background_blur").css("display", "none");
                bgArea.find("#background_blur .bg_wrapper").css("opacity", "");
            }else {
                bgArea.find("#background_blur").css("display", "");
                var bg = bgArea.find("#background_blur .bg_wrapper");
                bg.eq(curBgIdx).css("opacity", 0);
                curBgIdx ^= 1;
                bg.find(".bg").eq(curBgIdx).css({"background-image": "url(" + src + ")"});
                bg.eq(curBgIdx).css("opacity", 1);
            } bg_timer = null;
        }, time);
    };


    var historyBack = function () {
        if (historyStack.length > 0) {
            //showDefalutBackground(true);
            if(getCurrentView() == adultAuthViewData) {
                getCurrentView().view.hide();
            } else {
                switch(getCurrentView().getView().type) {
                    case View.TYPE.CONTENTS:
                        getCurrentView().view.blurred();
                        break;
                    case View.TYPE.CATEGORY:
                        getCurrentView().view.hide();
                        break;
                }
            }

            var currentView = historyStack.pop();
            switch (currentView.getView().type) {
                case View.TYPE.CATEGORY:
                    mode = MODE.CATEGORY;
                    setCurrentCategory(currentView);
                    setCurrentContent(currentView.view.getCurrentContent());
                    break;
                case View.TYPE.CONTENTS:
                    mode = MODE.CONTENTS;
                    setCurrentContent(currentView);
                    currentCateViewData.view.hide();
                    currentView.focused();
                    break;
            } changeTitle(currentView.title);
            return true;
        } else {
            LayerManager.historyBack();
            return true;
        };
    };

    // [si.mun] VOD 구매목록에서 성인 인증 후 focus 변경을 위해 사용
    var reChangeFocus = function (noback) {
        if(!noback) historyBack();
        changeFocus(currentContViewData);
        LayerManager.stopLoading();
    };

    var addView = function(viewData) {
        ViewDataMap[viewData.viewId] = ViewDataMap[viewData.viewId]||(div.append(viewData.getView().getView()), viewData);
        viewData.getView().viewMgr = instance;
    };

    var setCurrentCategory = function (categoryViewData) {
        mode = MODE.CATEGORY;
        currentCateViewData?currentCateViewData.view.hide():0;
        currentCateViewData = categoryViewData;
        addView(categoryViewData);
        currentCateViewData.view.focused();
        currentCateViewData.view.show();
        setCurrentContent(categoryViewData.view.getCurrentContent());
    };

    var setCurrentContent = function (contentsViewData) {
        currentContViewData ? currentContViewData.view.hide() : 0;
        if(contentsViewData.hasToAdultCheck && !adultAuthClear) currentContViewData = adultAuthViewData;
        else currentContViewData = contentsViewData;

        addView(currentContViewData);

        // [si.mun] WEB 3.0 18.05월 형상부터 blurred 형태가 아닌, 포커스가 preview 영역에
        // 곧바로 이동하도록 수정되었으므로, 없어도 괜찮을 것 같음.
        // 18.01.23 -> blurred 로직을 삭제하기보다 점차 수정하기 위해 있으면 blurred를 수행하도록 변경
        //if (currentContViewData.view.blurred) {
            currentContViewData.view.blurred();
        //}

        currentContViewData.view.show();
    };

    var getCurrentView = function() {
        if(mode==MODE.CATEGORY) return currentCateViewData;
        else if(mode==MODE.CONTENTS) return currentContViewData;
    };

    var setData = function(data) {
        changeFocus(data)
    };

    function setContentsChangeTimeout(func, time) {
        setCurrentContent(emptyViewData);
        LayerManager.startLoading();
        if(contentsChangeTimeout) {
            clearTimeout(contentsChangeTimeout);
            contentsChangeTimeout = null;
        } contentsChangeTimeout = setTimeout(function() {
            func();
            LayerManager.stopLoading();
        }, time);
    }
    
    function  contensChangeImmediately() {
        if(contentsChangeTimeout) {
            LayerManager.stopLoading();
            clearTimeout(contentsChangeTimeout);
            contentsChangeTimeout = null;
            setCurrentContent(currentCateViewData.view.getCurrentContent());
        }
    }

    var setContextIndicator = function (text) {
        div.find(".context_menu_indicator").html(text);
    };

    var clearView = function () {
        for(var id in ViewDataMap) {
            if(historyStack.indexOf(ViewDataMap[id])<0 && currentContViewData.viewId != id && currentCateViewData.viewId != id) {
                ViewDataMap[id].destroy();
                delete ViewDataMap[id];
            }
        }
    };

    var pause = function () {
        if(getCurrentView().view) getCurrentView().view.pause();
    };

    var resume = function () {
        if(getCurrentView().view) getCurrentView().view.resume();
    };

    var instance = {
        setParent: function (_parent) { parent = _parent; },
        getParent: function() { return parent; },
        setData: setData,
        setFocus: changeFocus,
        changeCategoryFocus: changeCategoryFocus,
        onKeyAction: function (keyCode) {
            if(mode==MODE.CATEGORY) {
                if(cateKeyAction(keyCode)) return true;
            }else if(mode==MODE.CONTENTS){
                if (currentContViewData && currentContViewData.view.onKeyAction(keyCode)) return true;
            }

            return (keyCode == KEY_CODE.BACK || keyCode == KEY_CODE.LEFT) && historyBack();
        },
        getView: function() { return div; },
        destroy: function() {
            if(currentCateViewData&&currentCateViewData.view) currentCateViewData.view.hide();
            if(currentContViewData&&currentContViewData.view) currentContViewData.view.hide();
            for(var id in ViewDataMap) ViewDataMap[id].destroy();
            ViewDataMap = {};
            historyStack = [];
            currentCateViewData = null;
            currentContViewData = null;
        },
        historyBack: historyBack,
        reChangeFocus: reChangeFocus,
        changeTitle: changeTitle,
        changeBackground: changeBackground,
        setContextIndicator: setContextIndicator,
        getCurrentView: getCurrentView,
        getCurrentMenuData: function () { return currentContViewData; },
        getCurrentCategoryIndex: function () { return currentCateViewData.view?currentCateViewData.view.getCurrentIndex():0 },
        updateMenuData: function (callback) { if(currentCateViewData.view.updateMenuData) currentCateViewData.view.updateMenuData(callback); },
        goToRoot: function () {
            while(historyStack.length > 0) {
                historyBack();
            }
        },
        setAdultAuthCheck: function () { adultAuthClear = true; },
        clearView: clearView,
        pause: pause,
        resume: resume
    };
    return instance;
};
Object.defineProperty(myHome.ViewManager, "FOCUS_MODE", { value: { NO_FOCUS: 0, FOCUS:1, BLUR_FOCUS: 2 },writable:false });