/**
 * 컨텐츠 뷰
 * (우리집 맞춤 TV > 알림박스 > 우측 컨텐츠 리스트(포스터) 등 ?)
 * Created by ksk91_000 on 2016-11-30.
 */
myHome.view.ContentsView = function(viewId, star) {
    View.call(this, viewId);
    var div = this.div;

    var dataList = [];
    var posterListComp;
    var indicator;
    var originData;
    var catData;
    var isFocused = false;

    var instance = this;
    var contextMenu;
    var sortOption = 0;
    var ollehFilter = 0, priceFilter = 0;
    var viewOption = 0;
    var ViewId = viewId;

    this.create = function(data) {
        (function createDiv() {
            div.attr("class", "contentsView");
            div.append(_$("<div/>", {class:"indicator_area"}));
            div.append(_$("<div/>", {class:"contents_area"}));
            div.append(_$("<div/>", {class:"bottom_dim"}));
            div.append(_$("<div/>", {class:"over_sdw_area"}));
        })();

        indicator = new Indicator(1);
        catData = data;
        originData = data.child?(Array.isArray(data.child)?(data.child):[data.child]):[];
        createContentsView(originData);
        createContextMenu();
        // this.div.append(_$("<div/>", {class: "indicator_area"}).append(indicator.getView()));
        div.find(".indicator_area").append(indicator.getView()).append(indicator.getPageTransView());
    };

    function createContextMenu() {
        contextMenu = new ContextMenu_VOD();
        contextMenu.addTitle("정렬옵션");
        contextMenu.addDropBox(["최신순", "별점순"]);

        contextMenu.addSeparateLine();
        contextMenu.addTitle("olleh 별점");
        contextMenu.addDropBox(["전체", "4.5 이상", "4.0 이상", "3.0 이상", "2.0 이상"]);
        contextMenu.addTitle("가격");
        contextMenu.addDropBox(["전체", "유료", "무료"]);
        contextMenu.addSeparateLine();

        viewOption = getPosterListType();
        contextMenu.addTitle("VOD 보기 옵션");
        contextMenu.addDropBox(["포스터 보기", "타이틀 보기"], viewOption);

        div.append(contextMenu.getView());
        contextMenu.setEventListener(contextMenuListener)
    }

    function createContentsView(data, posterType) {
        var pt = posterType==null?getPosterListType():posterType;
        posterListComp = getPosterListComp(pt);
        posterListComp.create(star||stars.TYPE.RED_20);
        div.find(".contents_area").empty().append(posterListComp.getView());

        posterListComp.setIndicator(indicator);
        posterListComp.parent = instance;
        dataList = data.dataList;
        setData(data, 0);
        if(isFocused) posterListComp.focused();
    }

    this.setContentsView = function(data, posterType) {
        createContentsView(data, posterType);
    };

    function getPosterListType() {
        switch (StorageManager.ps.load(StorageManager.KEY.VOD_LIST_MODE)) {
            case "poster": return 0;
            case "text": case "list": return 1;
            default: return 0;
        }
    }

    function getPosterListComp(data) {
        switch (parseInt(data)) {
            case 1: viewOption = 1; return new component.TitleList();
            default: viewOption = 0; return new component.VerticalPosterList();
        }
    }

    function setData(_dataList, page) {
        posterListComp.setData(_dataList, page);
    }

    this.changeFocus = function(amount) {
        posterListComp.changeFocus(amount)
    };

    this.changePage = function(amount) {
       posterListComp.changePage(amount)
    };

    this.setFocus = function(newFocus, forced) {
        posterListComp.setFocus(newFocus, forced);
    };

    this.setPage = function(newPage) {
        posterListComp.setPage(newPage);
    };

    this.onKeyAction = function(keyCode) {
        if (contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        if(posterListComp.onKeyAction(keyCode)) return true;
        else if(keyCode == KEY_CODE.CONTEXT) {
            var data = posterListComp.getFocusedContents();
            if(data) contextMenu.setVodInfo({
                    itemName: data.ITEM_NAME,
                    imgUrl: data.IMG_URL,
                    contsId: data.ITEM_ID,
                    catId: data.PARENT_CAT_ID,
                    itemType: data.ITEM_TYPE,
                    cmbYn: data.CMB_YN,
                    resolCd: data.IS_HD,
                    prInfo: data.RATING
                });
            contextMenu.open();
            return true;
        } else return false;
    };

    function contextMenuListener(index, data) {
        contextMenu.close();
        var oldData;
        switch (index) {
            case 0:
                if (sortOption == data) return;
                oldData = sortOption;
                sortOption = data;
                if(!filterImpl()){
                    sortOption = oldData;
                    return false;
                };
                break;
            case 1:
                if (ollehFilter == data) return;
                oldData = ollehFilter;
                ollehFilter = data;
                if(!filterImpl()){
                    ollehFilter = oldData;
                    return false;
                };
                break;
            case 2:
                if (priceFilter == data) return;
                oldData = priceFilter;
                priceFilter = data;
                if(!filterImpl()){
                    priceFilter = oldData;
                    return false;
                };
                break;
            case 3:
                if(viewOption == data) return;
                viewOption = data;
                StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, viewOption==0?"poster":"list");
                instance.viewMgr.clearView();
                filterImpl();
                break;
        }
        return true;
    }

    function filterImpl() {
        var dataList = originData.slice(0);
        if (ollehFilter != 0) {
            var tmp = [];
            var ollehPivot = ([0, 4.5, 4, 3, 2])[ollehFilter];
            for (var i = 0; i < dataList.length; i++) {
                if (dataList[i].OLLEH_RATING - 0 >= ollehPivot)
                    tmp.push(dataList[i]);
            }
            dataList = tmp;
        }
        if (priceFilter != 0) {
            var tmp = [];
            var pricePivot = priceFilter == 1 ? "Y" : "N";
            for (var i = 0; i < dataList.length; i++) {
                if (dataList[i].WON_YN == pricePivot)
                    tmp.push(dataList[i]);
            }
            dataList = tmp;
        }
        if (sortOption == 1) {
            dataList.sort(function(a, b) {
                return b.OLLEH_RATING - 0 - a.OLLEH_RATING;
            });
        } if(dataList.length==0) {
            showToast("선택한 옵션으로 표시할 수 있는 콘텐츠가 없습니다");
            return false;
        } else createContentsView(dataList, viewOption);

        return true;
    }

    function clearFilter() {
        if(sortOption!=0) {
            clearImpl();
            return true;
        } if(ollehFilter !=0) {
            clearImpl();
            return true;
        } if(priceFilter !=0) {
            clearImpl();
            return true;
        }

        function clearImpl() {
            createContentsView.call(instance, originData);
            sortOption = 0;
            ollehFilter = 0;
            priceFilter = 0;
            filterImpl();
            contextMenu.clearDropBoxFocus();
            contextMenu.setDropBoxFocus(3, viewOption);
        }
    }

    this.getView = function() {
        return div;
    };

    this.isIndicatorFocused = function() {
      return indicator.isFocused();
    };

    this.getFocusedContents = function () {
        return posterListComp.getFocusedContents();
    };

    this.focused = function() {
        isFocused = true;
        div.removeClass("blurred");
        posterListComp.focused();
        this.viewMgr.setContextIndicator("찜 | 정렬");
        return true;
    };

    this.blurred = function() {
        isFocused = false;
        div.addClass("blurred");

        if(!clearFilter()) posterListComp.blurred();
        this.viewMgr.setContextIndicator("");
    };

    this.pause = function () {
        if(posterListComp && posterListComp.hasOwnProperty("pause"))
            posterListComp.pause();
    };

    this.resume = function () {
        if(posterListComp && posterListComp.hasOwnProperty("resume"))
            posterListComp.resume();
    };
};
myHome.view.ContentsView.prototype = new View();
myHome.view.ContentsView.prototype.constructor = myHome.view.ContentsView;
myHome.view.ContentsView.prototype.type = View.TYPE.CONTENTS;