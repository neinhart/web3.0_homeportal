/**
 * 나의 취향 알아보기 화면
 * 우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기 > 나의 취향 알아보기(카테고리)
 * Created by ksk91_000 on 2016-11-23.
 */
myHome.view.ForYouView = function(viewId) {
    View.call(this, viewId + "View");
    var colorList = [this.Color(149, 169, 232), this.Color(62, 201, 196), this.Color(234, 146, 100)];
    var graphs = [];
    var activate;
    var div;
    var isFocused = false;
    var trophyRate;
    var personList;
    var genreList;
    var contextMenu;
    var instance = this;
    var isLoaded = false;

    var focus = {main: 0, sub: 0};

    this.create = function() {
        div = this.div;
    };

    this.reloadView = function() {
        isLoaded = false;
        MyPreferenceManager.update(function(data) {
            activate = (data != null && data.RESULT_CODE == 200);
            updateView(data);
            isLoaded = true;
            // if (isFocused) div.find("#" + viewId).addClass('focus');
        });
    };

    function init(callback) {
        LayerManager.stopLoading();
        setLoadingView();
        isLoaded = false;
        MyPreferenceManager.getMyPreference(function(data) {
            activate = (data != null && data.RESULT_CODE == 200);
            updateView(data);
            isLoaded = true;
            if (callback) callback();
            if (isFocused) instance.viewMgr.reChangeFocus();
        });
    }

    function setLoadingView() {
        div.empty();
        div.append(_$("<div id='" + viewId + "' class='noPref'>" +
            "<div class='no_content_area'>" +
            "<div id='spinner'>" +
            "<div id='circle_0' class='spin_circle'/>" +
            "<div id='circle_1' class='spin_circle'/>" +
            "<div id='circle_2' class='spin_circle'/>" +
            "<div id='circle_3' class='spin_circle'/>" +
            "<div id='circle_4' class='spin_circle'/>" +
            "<div id='circle_5' class='spin_circle'/>" +
            "</div>" +
            "<span class='loading_text'>평가를 바탕으로 맞춤 메뉴를 구성 중입니다</span>" +
            "</div>" +
            "</div>"));
    }

    function updateView(data) {
        if (activate) {
            setData(div, data);
        } else {
            div.empty();
            div.append(_$("<div id='" + viewId + "' class='noPref'>" +
                "<div class='no_content_area'>" +
                "<img src='" + modulePath + "resource/image/icon_noresult.png'>" +
                "<span class='text1'>오늘도 볼만한 콘텐츠를 찾다가<br>시간 다 보내셨나요?</span>" +
                "<span class='text2'>별점을 평가하고 내 취향을 알아보세요<br>당신을 위한 VOD를 추천해 드립니다.</span>" +
                "<div class='rateBtn'>나의 취향 알아보기</div>" +
                "</div></div>"));
        }
    }

    function setData(div, data) {
        div.empty();
        var trophyType;
        if (data.TOTAL_ITEM_CNT < 50) trophyType = "silver_0";
        else if (data.TOTAL_ITEM_CNT < 100) trophyType = "bronze";
        else if (data.TOTAL_ITEM_CNT < 200) trophyType = "silver";
        else trophyType = "gold";
        trophyRate = data.TOTAL_ITEM_CNT < 100 ? ((data.TOTAL_ITEM_CNT - 50) * 2) : (data.TOTAL_ITEM_CNT < 200 ? (data.TOTAL_ITEM_CNT - 100) : 100);
        div.append(_$("<div id='" + viewId + "'>" +
            "<div class='top_area'>" +
            "<div id='trophyArea'>" +
            "<img src='" + modulePath + "resource/image/foryou/trophy_" + trophyType + ".png'>" +
            "<span class='" + trophyType + "Txt'>" + data.TOTAL_ITEM_CNT + "</span>" +
            "<img src='" + modulePath + "resource/image/foryou/trophy_fill_" + (trophyType == "silver_0" ? "silver" : trophyType) + ".png' class='trophyFill'>" +
            "</div>" +
            "<div id='rateBtn'>별점 평가 더하기</div>" +
            "<div id='mentArea'>" +
            "<span class='ment1'></span>" +
            "<span class='ment2'>" + getMent2String(data.TOTAL_ITEM_CNT) + "</span>" +
            "</div>" +
            "<div id='graphArea'>" +
            "<div id='graphWrapper0' class='graphWrapper'>" +
            "<span></span>" +
            "</div>" +
            "<div id='graphWrapper1' class='graphWrapper'>" +
            "<span></span>" +
            "</div>" +
            "<div id='graphWrapper2' class='graphWrapper'>" +
            "<span></span>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='bottom_area'></div>" +
            "<div class='context_menu_indicator'></div>" +
            "</div>"));

        // 2017.08.08 Lazuli
        // 평가한 개수에 따라 트로피 면적이 채워지는 로직 추가
        var percentage = 100;
        switch (trophyType) {
            case "silver_0":
                percentage = (50 - data.TOTAL_ITEM_CNT) / 50 * 100;
                break;
            case "bronze":
                percentage = (50 - (data.TOTAL_ITEM_CNT - 50)) / 50 * 100;
                break;
            case "silver":
                percentage = (100 - (data.TOTAL_ITEM_CNT - 100));
                break;
            case "gold":
                percentage = 0;
                break;
        }
        div.find("#" + viewId + " .trophyFill").css("-webkit-clip-path", "polygon(0 " + percentage + "%, 100%" + percentage + "%, 100% 100%, 0% 100%)");

        for (var i = 0; i < 3; i++) {
            if (data.GENRE_CNT > i) {
                var genreData = data.GENRE_LIST[i];
                graphs[i] = new Graph(genreData.PREFERENCE / 100, colorList[i]);
                div.find('#graphWrapper' + i).append(graphs[i].getCanvas());
                div.find('#graphWrapper' + i + " span").text(genreData.GENRE_NAME + " " + genreData.ITEM_CNT + "편");
            }
        }
        (function setMent1() {
            var text = "당신은 ";
            if (data.GENRE_CNT > 1) {
                text += data.GENRE_LIST[0].GENRE_NAME;
                text += HTool.haveBatchim(data.GENRE_LIST[0].GENRE_NAME) ? "과 " : "와 ";
            }
            if (data.GENRE_CNT > 1) {
                text += data.GENRE_LIST[Math.min(data.GENRE_CNT - 1, 1)].GENRE_NAME;
                text += HTool.haveBatchim(data.GENRE_LIST[Math.min(data.GENRE_CNT - 1, 1)].GENRE_NAME) ? "을" : "를";
            }
            div.find(".ment1").text(text + " 즐기는 타입입니다");
        })();

        setBottomList(div.find(".bottom_area"), data);
        setContextMenu();
        setFocus(0, 0);
        for (var i = 0; i < graphs.length; i++) graphs[i].startAnimation();
    }

    function getMent2String(cnt) {
        if (cnt < 50) return "총 " + cnt + "편 평가! 50편에 도전하여 레벨을 높이세요";
        else if (cnt < 100) return "총 " + cnt + "편 평가! 100편에 도전하여 레벨을 높이세요";
        else if (cnt < 200) return "총 " + cnt + "편 평가! 200편에 도전하여 레벨을 높이세요";
        else return "총 " + cnt + "편 평가! 별점을 추가할수록 추천은 정확해집니다";
    }

    function setBottomList(div, data) {
        personList = data.PERSON_LIST;

        // 선호 감독 & 배우
        var tmp = _$("<div/>", {class: "bottom_card person_area"});
        tmp.append(_$("<div/>", {class: 'title'}).text("선호 감독 & 배우"));
        for (var i = 0; i < 4 && i < data.PERSON_CNT; i++) {
            tmp.append(_$("<div/>", {class: 'item'}).text(data.PERSON_LIST[i].PERSON_NAME));
        }
        div.append(tmp);

        // 장르
        var tmpList = filterGenreItem(data);
        genreList = [];
        for (var i in tmpList) {
            // 추천 장르 title
            tmp = _$("<div/>", {class: "bottom_card genre_poster_area"});
            tmp.append(_$("<div/>", {class: 'title'}).text(tmpList[i][0].GENRE_NAME + " 추천"));

            // 포스터, 콘텐츠 제목 등등
            // [si.mun] WEB 3.0 5월 고도화에서 장르별 콘텐츠는 1개씩 제공하므로
            // 1번만 수행해도 된다.
            if (tmpList[i].length > 0) {
                tmp.append(_$("<div/>", {class: 'item'}).html(
                    "<div class='poster_area'>" +
                        "<img src='" + tmpList[i][0].IMG_URL + "?w=196&h=280&quality=90'>" +
                        (window.UTIL.isLimitAge(tmpList[i][0].CONT_RATING) ? ("<img class='poster_lock_img' src='" + window.modulePath + "resource/image/icon/img_vod_locked.png'>") : "") +
                    "</div>" +

                    "<div class='text_area'>" +
                        "<div class='title'>" +
                            "<span>" + tmpList[i][0].ITEM_NAME + "</span>" +
                        "</div>" +
                        "<div class='info_area'>" +
                            (HTool.isTrue(tmpList[i][0].WON_YN) ? "<img class='wonYnIcon'>" : (HTool.isFalse(tmpList[i][0].WON_YN) ? ("<span class='wonYn'>" + "무료" + "</span>") : "")) +
                            "<div class='prIcon pr_" + tmpList[i][0].CONT_RATING + "'></div>" +
                        "</div>" +
                    "</div>" +
                    "<div class='focus_red'><div class='black_bg'></div></div>"
                ));
            }

            // 2018-03-16 TaeSang. 별점 정보 노출 하지 않음 (데이터도 안내려오고있는 상태)
            // tmp.find(".info_area").prepend(stars.getView(stars.TYPE.RED_20));
            // stars.setRate(tmp.find(".rating_star_area"), tmpList[i][0].RATING ? tmpList[i][0].RATING : "0");

            //for (var j = 0; j < 2 && j < tmpList[i].length; j++) {
            //    tmp.append(_$("<div/>", {class: 'item'}).html(
            //        "<div class='poster_area'>" +
            //            "<img src='" + tmpList[i][j].IMG_URL + "?w=196&h=280&quality=90'>" +
            //            (window.UTIL.isLimitAge(tmpList[i][j].CONT_RATING) ? ("<img class='poster_lock_img' src='" + window.modulePath + "resource/image/icon/img_vod_locked.png'>") : "") +
            //        "</div>" +
            //
            //        "<div class='text_area'>" +
            //            "<div class='title'>" +
            //                "<span>" + tmpList[i][j].ITEM_NAME + "</span>" +
            //            "</div>" +
            //            "<div class='info_area'>" +
            //                (HTool.isTrue(tmpList[i][j].WON_YN) ? "<img class='wonYnIcon'>" : (HTool.isFalse(tmpList[i][j].WON_YN) ? ("<span class='wonYn'>" + "무료" + "</span>") : "")) +
            //                "<div class='prIcon pr_" + tmpList[i][j].CONT_RATING + "'></div>" +
            //            "</div>" +
            //        "</div>"
            //    ));
            //}

            div.append(tmp);
            genreList.push(tmpList[i]);
        }
        div.append(_$("<div/>", {class: "focus_line"}).append("<div class='focus_line_top'/><div class='focus_line_bottom'/>"));
    }

    function filterGenreItem(data) {
        if (!data.ITEM_LIST) return;

        var list = [];
        for (var i = 0; i < data.ITEM_LIST.length; i++) {
            var item = data.ITEM_LIST[i];
            if (!list[item.GENRE_NAME]) list[item.GENRE_NAME] = [];
            list[item.GENRE_NAME].push(item);
        }
        return list;
    }

    function setContextMenu() {
        contextMenu = new ContextMenu();
        contextMenu.addButton("맞춤 추천 초기화");
        contextMenu.addText("주의!! 추천을 위해 수집한 모든<br>데이터를 삭제합니다");
        contextMenu.addSeparateLine();
        // var alFocus = StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT);
        var alFocus = MyPreferenceManager.isNoAdult()?0:1;
        contextMenu.addTitle("19세 이상 콘텐츠 추천");
        contextMenu.addDropBox(["추천에서 제외", "추천에 포함"], alFocus - 0);
        contextMenu.setEventListener(function(index, data) {
            contextMenu.close();
            switch (index) {
                case 0:
                    LayerManager.activateLayer({
                        obj: {
                            id: "MyHomeResetRatingPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                callback: function(res) {
                                    if (res) {
                                        MyPreferenceManager.initMyPreference(function(res) {
                                            if (res) {
                                                showToast("맞춤 추천 결과가 초기화되었습니다");
                                                instance.viewMgr.updateMenuData(function () {
                                                    init(instance.viewMgr.historyBack);
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    break;
                case 1:
                    StorageManager.ps.save(StorageManager.KEY.RECOMMEND_ADULT_LIMIT, data);
                    MyPreferenceManager.update(function(data) {
                        if (data.RESULT_CODE == 200) {
                            activate = true;
                            updateView(data);
                            instance.focused();
                            // if (callback) callback();
                        } else {
                            activate = false;
                            updateView(data)
                            // if (callback) callback();
                        }
                        if (isFocused) div.find("#" + viewId).addClass('focus');
                    });
                    break;
            }
        });
        div.append(contextMenu.getView());
    }

    function setFocus(_focus, _subFocus) {
        focus.main = _focus;
        focus.sub = _subFocus;
        div.find(".focus").removeClass("focus");
        if (isFocused) {
            clearTextAnimation();
            switch (_focus) {
                // 별점 평가 더하기 버튼
                case 0:
                    div.find("#rateBtn").addClass("focus");
                    div.find(".focus_line").css({"-webkit-transform": "translate(0px, 120px)", "opacity": "0"});
                    break;
                // 선호 감독 & 배우
                case 1:
                    div.find(".bottom_area .person_area .item").eq(_subFocus).addClass("focus");
                    div.find(".focus_line .focus_line_bottom").css("-webkit-transform", "translateY(70px)");
                    div.find(".focus_line").css({"-webkit-transform": "translate(0px, " + (75 + 85 * _subFocus) + "px)", "opacity": ".99"});
                    break;
                case 2:
                case 3:
                case 4:
                    div.find(".bottom_area .genre_poster_area").eq(_focus - 2).find(".item").eq(_subFocus).addClass("focus");
                    div.find(".focus_line").css({"opacity": "0"});
                    //div.find(".focus_line .focus_line_bottom").css("-webkit-transform", "translateY(156px)");
                    //div.find(".focus_line").css({
                    //    "-webkit-transform": "translate(" + (468 * _focus - 422) + "px, " + (128 + 156 * _subFocus) + "px)",
                    //    "opacity": ".99"
                    //});
                    setTextAnimation(_focus, _subFocus);
                    break;
            }
        }
    }

    function setTextAnimation(mainFocus, subFocus) {
        clearTextAnimation();
        if (UTIL.getTextLength(genreList[mainFocus - 2][subFocus].ITEM_NAME, "RixHead M", 30) > 244) {
            var posterDiv = div.find(".bottom_area .genre_poster_area").eq(mainFocus - 2).find(".item").eq(subFocus).find(".title");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    function clearTextAnimation() {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
    }

    this.onKeyAction = function(keyCode) {
        if (activate) {
            return onKeyForActivateState(keyCode);
        } else {
            if (keyCode == KEY_CODE.LEFT) {
                instance.viewMgr.historyBack();
                return true;
            } else if (keyCode == KEY_CODE.ENTER) {
                openRatingPopup();
                return true;
            }
        }
    };

    function onKeyForActivateState(keyCode) {
        if (contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        switch (keyCode) {
            case KEY_CODE.UP:
                if (focus.main != 0) {
                    if (focus.sub > 0) setFocus(focus.main, focus.sub - 1);
                    else setFocus(0, 0);
                }
                return true;
            case KEY_CODE.DOWN:
                if (focus.main == 0) setFocus(1, 0);
                else if (focus.main == 1) setFocus(1, HTool.getIndex(focus.sub, 1, Math.min(4, personList.length)));
                else if (genreList[focus.main - 2][1]) setFocus(focus.main, 0);
                //else if (genreList[focus.main - 2][1]) setFocus(focus.main, focus.sub ^ 1);
                return true;
            case KEY_CODE.RIGHT:
                if (focus.main > 0 && focus.main < 4) {
                    if (genreList[focus.main - 1][0]) setFocus(focus.main + 1, 0);
                } else if (focus.main == 4) {
                    setFocus(1, 0);
                }
                return true;
            case KEY_CODE.LEFT:
                if (focus.main > 2) {
                    if (genreList[focus.main - 3][0]) setFocus(focus.main - 1, 0);
                }
                else if (focus.main == 2) setFocus(focus.main - 1, 0);
                else if (focus.main == 1 || focus.main == 0) instance.viewMgr.historyBack();

                //else if (focus.main == 1) setFocus(Math.min(3, genreList.length) + 1, 0);
                //else if (focus.main == 0) instance.viewMgr.historyBack();
                return true;
            case KEY_CODE.ENTER:
                if (focus.main == 0) openRatingPopup();
                else if (focus.main == 1) openSearchLayer(personList[focus.sub].PERSON_NAME);
                else {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, genreList[focus.main - 2][focus.sub].CAT_ID, genreList[focus.main - 2][focus.sub].ITEM_ID, "33");
                    } catch (e) {
                    }
                    openDetailLayer(genreList[focus.main - 2][focus.sub].CAT_ID, genreList[focus.main - 2][focus.sub].ITEM_ID, "33");
                }
                return true;
            case KEY_CODE.CONTEXT:
                contextMenu.open(1);
                return true;
        }
    }

    function openRatingPopup(responseData) {
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeRatingPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    callback: function(result) {
                        if (result) {
                            instance.viewMgr.updateMenuData(function () {
                                init(instance.viewMgr.reChangeFocus);
                            });
                        }
                    },
                    responseData: responseData
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    }

    this.focused = function() {
        if (!isLoaded) return myHome.ViewManager.FOCUS_MODE.NO_FOCUS;

        //View.prototype.focused.apply(this, arguments);

        this.viewMgr.changeBackground(null, 0);
        this.div.find("#" + viewId).addClass('focus');
        isFocused = true;
        if (activate) {
            setFocus(0, 0);

            _$(".foryou_def_homeShow").hide();
            //instance.viewMgr.getParent().find("#categoryArea").css("visibility", "hidden");
            div.find(".context_menu_indicator").text("연관 메뉴");
            return myHome.ViewManager.FOCUS_MODE.FOCUS;
        } else {
            return myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        }
    };

    this.blurred = function() {
        //View.prototype.blurred.apply(this, arguments);

        isFocused = false;
        clearTextAnimation();
        this.div.find(".focus").removeClass('focus');
        _$(".foryou_def_homeShow").show();
        div.find(".focus_line").css("opacity", "0");
        div.find(".context_menu_indicator").text("");
    };

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        for (var i = 0; i < graphs.length; i++) graphs[i].endAnimation();

        if (MyPreferenceManager.hasUpdate() || !isLoaded) {
            if (MyPreferenceManager.getTotalCount() >= 20) {
                activate = true;
                init();
            } else {
                activate = false;
                init();
            }
        }
    };

    this.hide = function() {
        View.prototype.hide.apply(this, arguments);
        clearTextAnimation();
    };

    this.pause = function() {
        clearTextAnimation();
    };

    this.resume = function() {
        if(activate) setFocus(focus.main, focus.sub);
    };

    this.getComponent = function() {
        return component;
    };

    var Graph = this.Graph;
};
myHome.view.ForYouView.prototype = new View();
myHome.view.ForYouView.prototype.constructor = myHome.view.ForYouView;
myHome.view.ForYouView.prototype.type = View.TYPE.CONTENTS;

myHome.view.ForYouView.prototype.Color = function(r, g, b) {
    return {
        colorText: "rgb(" + r + "," + g + "," + b + ")",
        alphaText: "rgba(" + r + "," + g + "," + b + ",.2)"
    }
};

myHome.view.ForYouView.prototype.Graph = function(rate, color) {
    var canvas;
    var canvasCtx;
    var animated = false;
    var interval;

    function init() {
        canvas = document.createElement('canvas');
        canvas.width = 210;
        canvas.height = 210;
        canvasCtx = canvas.getContext('2d');
        renderArc(canvasCtx, 0, color);
    }

    function renderArc(ctx, rate, color) {
        ctx.clearRect(0, 0, 210, 210);
        ctx.lineWidth = 5;
        ctx.strokeStyle = color.alphaText;
        ctx.beginPath();
        ctx.arc(105, 105, 105 - 2.5, 1.5 * Math.PI, 2 * Math.PI * rate - 0.5 * Math.PI, true);
        ctx.stroke();

        if (rate > 0) {
            ctx.strokeStyle = color.colorText;
            ctx.beginPath();
            ctx.arc(105, 105, 105 - 2.5, 1.5 * Math.PI, 2 * Math.PI * rate - 0.5 * Math.PI);
            ctx.stroke();
        }

        ctx.font = "62px RixHead L";
        ctx.fillStyle = "rgba(255,255,255,.9)";
        ctx.textAlign = "center";
        ctx.fillText(Math.floor(rate * 100) + "", 87.5, 117);
        ctx.font = "38px RixHead L";
        ctx.fillText("%", 145, 114);
    }

    this.startAnimation = function() {
        if (animated) return;
        animated = true;
        var count = 0;
        interval = setInterval(function() {
            renderArc(canvasCtx, rate * count++ / 20, color);
            if (count > 20) {
                clearInterval(interval);
                interval = null;
            }
        }, 50);
    };

    this.endAnimation = function() {
        if (interval) clearInterval(interval);
        interval = null;
        renderArc(canvasCtx, rate, color);
    };

    this.getCanvas = function() {
        return canvas;
    };

    init();
};
