/**
 * 나의 취향 알아보기 페이지(평가한 컨텐츠가 없거나 20개 이하일 경우)
 * (우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기 > 나의 취향 알아보기(카테고리))
 * Created by ksk91_000 on 2016-11-23.
 */
myHome.view.MyRatingContentView = function(viewId) {
    View.call(this, viewId + "View");
    var activate;
    var div;
    var contView;

    var originData;

    var contextMenu;
    var viewOption = 0;
    var sortOption = 0;
    var ollehFilter=0, priceFilter=0;
    var instance = this;
    var isLoaded = false;
    var isFocused = false;

    this.create = function() {
        div = this.div;
    };

    this.reloadView = function () {
        isLoaded = false;
        LayerManager.startLoading();
        RatingContentsManager.getRatingContents(function (data) {
            // data = [];
            if(data.length>0) {
                div.empty();
                originData = data;
                // setRatingCnt(data);
                setRatingCnt(originData);
                setData(div, originData);
                // activate = true;
                createContextMenu();
            } else {
                div.empty();
                div.append(_$("<div id='" + viewId + "' class='noPref'>" +
                    "<div class='no_content_area'>" +
                    "<img src='" + modulePath + "resource/image/icon_noresult.png'>" +
                    "<span class='text1'>오늘도 볼만한 콘텐츠를 찾다가<br>시간 다 보내셨나요?</span>" +
                    "<span class='text2'>별점을 평가하고 내 취향을 알아보세요<br>당신을 위한 VOD를 추천해 드립니다.</span>" +
                    "<div class='rateBtn'>나의 취향 알아보기</div>" +
                    "</div></div>" ));
                activate = false;
            }

            isLoaded = true;
            LayerManager.stopLoading();
            if(isFocused) instance.viewMgr.reChangeFocus();
            // if(callback) callback();
        });
    };

    function init(callback) {
        isLoaded = false;
        LayerManager.startLoading();
        RatingContentsManager.getRatingContents(function (data) {
            if(data && data.length>0) {
               div.empty();
               originData = data;
               // setRatingCnt(data);
               setRatingCnt(originData);
               setData(div, originData);
               activate = true;
               createContextMenu();
            } else {
               div.empty();
               div.append(_$("<div id='" + viewId + "' class='noPref'>" +
                   "<div class='no_content_area'>" +
                   "<img src='" + modulePath + "resource/image/icon_noresult.png'>" +
                   "<span class='text1'>오늘도 볼만한 콘텐츠를 찾다가<br>시간 다 보내셨나요?</span>" +
                   "<span class='text2'>별점을 평가하고 내 취향을 알아보세요<br>당신을 위한 VOD를 추천해 드립니다.</span>" +
                   "<div class='rateBtn'>나의 취향 알아보기</div>" +
                   "</div></div>" ));
               activate = false;
            }
            isLoaded = true;
            if(callback) callback();
            LayerManager.stopLoading();
            if(isFocused) instance.viewMgr.reChangeFocus();
        });
    }

    function createContextMenu() {
        contextMenu = new ContextMenu_VOD();
        contextMenu.addTitle("정렬옵션");
        contextMenu.addDropBox(["최근 평가순", "가나다순"]);

        contextMenu.addSeparateLine();
        contextMenu.addTitle("olleh 별점");
        contextMenu.addDropBox(["전체", "5.0 이상", "4.5 이상", "4.0 이상", "3.0 이상"]);
        contextMenu.addTitle("가격");
        contextMenu.addDropBox(["전체", "유료", "무료"]);
        contextMenu.addSeparateLine();

        viewOption = getPosterListType();
        contextMenu.addTitle("VOD 보기 옵션");
        contextMenu.addDropBox(["포스터 보기", "타이틀 보기"], viewOption);

        div.append(contextMenu.getView());
        contextMenu.setEventListener(contextMenuListener)
    }

    function contextMenuListener(index, data) {
        contextMenu.close();
        var oldData;
        switch (index) {
            case 0:
                if (sortOption == data) return;
                oldData = sortOption;
                sortOption = data;
                if(!filterImpl()){
                    sortOption = oldData;
                    return false;
                };
                break;
            case 1:
                if (ollehFilter == data) return;
                oldData = ollehFilter;
                ollehFilter = data;
                if(!filterImpl()){
                    ollehFilter = oldData;
                    return false;
                };
                break;
            case 2:
                if (priceFilter == data) return;
                oldData = priceFilter;
                priceFilter = data;
                if(!filterImpl()){
                    priceFilter = oldData;
                    return false;
                };
                break;
            case 3:
                if(viewOption == data) return;
                viewOption = data;
                StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, viewOption==0?"poster":"list");
                instance.viewMgr.clearView();
                filterImpl();
                break;
        }
        return true;
    }

    function filterImpl() {
        var dataList = originData.slice(0);
        if (ollehFilter != 0) {
            var tmp = [];
            var ollehPivot = ([0, 5, 4.5, 4, 3])[ollehFilter];
            for (var i = 0; i < dataList.length; i++) {
                if (dataList[i].OLLEH_RATING - 0 >= ollehPivot)
                    tmp.push(dataList[i]);
            }
            dataList = tmp;
        }
        if (priceFilter != 0) {
            var tmp = [];
            var pricePivot = priceFilter == 1 ? "Y" : "N";
            for (var i = 0; i < dataList.length; i++) {
                if (dataList[i].WON_YN == pricePivot)
                    tmp.push(dataList[i]);
            }
            dataList = tmp;
        }
        if (sortOption == 1) {
            dataList.sort(function(a, b) {
                return b.ITEM_NAME > a.ITEM_NAME ? -1:1;
            });
        } if(dataList.length==0) {
            showToast("선택한 옵션으로 표시할 수 있는 콘텐츠가 없습니다");
            return false;
        } else contView.setContentsView(dataList, viewOption);

        return true;
    }

    /**
     * [dj.son] [WEBIIIHOME-3647] 내가 평가한 컨텐츠 리스트 화면에서 빠져나올때, 필터값 reset 하도록 수정
     */
    function clearFilter () {
        if (sortOption!=0) {
            clearImpl();
            return true;
        }
        if (ollehFilter !=0) {
            clearImpl();
            return true;
        }
        if (priceFilter !=0) {
            clearImpl();
            return true;
        }

        function clearImpl() {
            sortOption = 0;
            ollehFilter = 0;
            priceFilter = 0;
            filterImpl();
            contextMenu.clearDropBoxFocus();
            contextMenu.setDropBoxFocus(3, viewOption);
        }
    }

    function getPosterListType() {
        switch (StorageManager.ps.load(StorageManager.KEY.VOD_LIST_MODE)) {
            case "poster": return 0;
            case "text": case "list": return 1;
            default: return 0;
        }
    }

    function setRatingCnt(data) {
        log.printInfo("setRatingCnt!!!" + !!data[0].CONT_RATING);
        for(var i=0; i<data.length; i++) {
            if(data[i].CONT_RATING) {
                data[i].OLLEH_RATING = data[i].RATING;
                data[i].RATING = data[i].CONT_RATING;
                delete data[i].CONT_RATING;
            }
        }
    }

    function setData(div, data) {
        if(contView) {
            contView.div.remove();
            contView.destroy();
        }

        contView = new myHome.view.ContentsView("MyRatingContView", stars.TYPE.YELLOW_20);

        contView.create({child: data});
        contView.viewMgr = instance.viewMgr;
        div.append(contView.div);
    }

    this.onKeyAction = function(keyCode) {
        if(activate) {
            return onKeyForActivateState(keyCode);
        } else {
            if(keyCode==KEY_CODE.LEFT) {
                this.viewMgr.historyBack();
                return true;
            }else if(keyCode==KEY_CODE.ENTER) {
                openRatingPopup();
                return true;
            }
        }
    };

    function onKeyForActivateState(keyCode) {
        if(contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        if(keyCode==KEY_CODE.CONTEXT) {
            var data = contView.getFocusedContents();
            if(data) contextMenu.setVodInfo({
                itemName: data.ITEM_NAME,
                imgUrl: data.IMG_URL,
                contsId: data.ITEM_ID,
                catId: data.PARENT_CAT_ID,
                itemType: data.ITEM_TYPE,
                cmbYn: data.CMB_YN,
                resolCd: data.IS_HD,
                prInfo: data.RATING
            });

            if(contView.isIndicatorFocused()) {
                contextMenu.setVisibleVodInfo(false);
            } else {
                contextMenu.setVisibleVodInfo(true);
            }
            contextMenu.open();
        }else return contView.onKeyAction(keyCode);
    }

    function openRatingPopup() {
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeRatingPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: { callback: function () {
                    instance.viewMgr.updateMenuData(function () {
                        init(instance.viewMgr.reChangeFocus);
                    });

                }}
            },
            moduleId:"module.family_home",
            visible: true
        });
    }

    this.focused = function () {
        if(!isLoaded) return myHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        View.prototype.focused.apply(this, arguments);
        this.div.find("#" + viewId).addClass('focus');
        isFocused = true;

        if(activate) {
            return contView.focused();
        } else {
            return myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        }
    };

    this.blurred = function () {
        View.prototype.blurred.apply(this, arguments);
        this.div.find("#" + viewId).removeClass('focus');
        isFocused = false;

        if (activate) {
            clearFilter();

            contView.blurred();
        }
    };

    this.show = function () {
        View.prototype.show.apply(this, arguments);
        if(RatingContentsManager.hasUpdate() || !isLoaded) {
            div.empty();
            LayerManager.startLoading();
            init(function () { LayerManager.stopLoading(); });
        }
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
    };

    this.pause = function () {
        if(contView && contView.hasOwnProperty("pause"))
            contView.pause();
    };

    this.resume = function () {
        if(contView && contView.hasOwnProperty("resume"))
            contView.resume();
    };
};
myHome.view.MyRatingContentView.prototype = new View();
myHome.view.MyRatingContentView.prototype.constructor = myHome.view.MyRatingContentView;
myHome.view.MyRatingContentView.prototype.type = View.TYPE.CONTENTS;