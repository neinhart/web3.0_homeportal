/**
 * 마이 플레이리스트 뷰
 * (우리집 맞춤 TV > 마이 플레이리스트 > 우측 컨텐츠 리스트(포스터) 등)
 * Created by ksk91_000 on 2016-07-01.
 */
myHome.view.MyPlayListView = function(viewId) {
    View.call(this, viewId);
    var instance = this;
    var playList = [];
    var indicator;
    var dom = this.div;
    var comp;

    var page = 0;
    var mainFocus = 0;
    var listFocus = 1;
    var menuFocus = 0;
    var menuFocusList = []; // ["AllItemCheck", "Play", "Repeat", "Delete", "Order"]

    var playListId;
    var playListNm;
    var isKidsPlayList;
    var isLoaded = false;
    var checkedList = [];
    var contentListDom;

    // 한 page 당 표시 되는 poster의 최대 갯수
    var pageMaxCnt = 5;

    this.itemCnt;

    this.create = function(data) {
        log.printDbg('create() - data : ' + data);
        playListId = data.id;
        playListNm = data.name;
        this.itemCnt = data.itemCnt;
        isKidsPlayList = data.info.isKidsYn==="Y";
        this.div.addClass("myplist");
        indicator = new Indicator(1);
    };

    function update() {
        log.printDbg('update()');

        var mpld = myHome.MyPlayListManager.getPlayListData(playListId);

        if (isLoaded && playList && (!mpld || mpld.isUpdated)) {
            listFocus = 0;
            //listFocus = 1;
            //indicator.blurred();
            //changeMainFocus(1);
            return;
        }

        // 1. 마이플레이 리스트 첫 진입
        // 2. 플레이 리스트에서 콘텐츠 삭제, 순서 변경 후에 아래 로직이 호출된다.
        myHome.amocManager.getMyPlayItemDetList(function(result, data) {
            if (result) {
                playList = data.playItemDetList;
                if (playList == null) playList = [];
                if (!Array.isArray(playList)) playList = [playList];
                instance.itemCnt = playList.length;

                // 성인 콘텐츠인 경우 콘텐츠명 숨김 처리
                for(var i in playList) {
                    if (playList[i].adultOnlyYn == "Y") {
                        playList[i].contsName = playList[i].contsName.substr(0, 1) + "******************************************************".substr(0, playList[i].contsName.length);
                    }
                }

                createDom();
                // 첫번째 콘텐츠로 focus
                //setListFocus(0);
                //changeMainFocus(0);

                //setMenuFocus(1);

                // Default 로 모든 콘텐츠에 선택되어야 한다.
                toggleAllCheckItem(true);

                // 메뉴 btn disable
                setDisableButtonState();

                // 콘텐츠 삭제, 순서 변경 후엔 재생에 포커스가 위치하여야 하며 아래 로직을 수행한다.
                // 첫 진입 시 아래 로직을 수행하지 않기 위해 isLoaded 와 mpld.isUpdated 를 마지막에 수행하도록 함
                // 1. 콘텐츠 포커스 삭제
                // 2. 콘텐츠 포커스 초기화
                // 3. 메뉴 포커스 재생으로 설정
                // 4. 적/청색 indicator hide
                if (isLoaded && mpld.isUpdated) {
                    removeListFocus();
                    listFocus = 0;
                    setMenuFocus(1);
                }

                if (instance.itemCnt > pageMaxCnt) {
                    // 적/청색 page hide
                    indicator.togglePageTransIcon(true);
                }

                mpld.isUpdated = true;
                isLoaded = true;

                /**
                 * [dj.son] [WEBIIIHOME-3830] 예외처리 추가
                 */
                if (dom.hasClass("blurred")) {
                    removeMenuFocus();
                }
                else {
                    if (playList.length <= 0) {
                        instance.viewMgr.historyBack();
                    }
                }


                // else getOllehPoint();
            }
        }, playListId);


    }

    function createDom() {
        // playList 갯수
        var playListCnt = playList ? playList.length : 0;

        if (playList && playListCnt > 0) {
            var totalCntArea = _$("<div/>", {class: "playlist_cnt"}).text("총 " + playListCnt + "개");
            var contentsArea = _$("<div/>", {class: "contents_area"});
            var wp = _$("<div/>", {class: "overflow_wrapper"});
            contentsArea.append(wp);
            var contentList = _$("<div/>", {class: "content_list"});
            for (var i = 0; i < pageMaxCnt; i++)
                contentList.append(getItem());
            wp.append(contentList);
            wp.append(_$("<div/>", {class: "focus_area hidden"}));

            dom.empty().append(totalCntArea);
            dom.append(contentsArea);
            var related_menu = _$("<div/>", {class: "related_menu"});
            related_menu.append(_$("<div/>", {class: "menu_area"}));
            dom.append(related_menu);
            dom.append(_$("<div/>", {class: "indicator_area"}));
            setMenuArea();
            setPage(0, true);

            indicator.setSize(Math.floor((playListCnt - 1) / pageMaxCnt) + 1);
            dom.find(".indicator_area").append(indicator.getView()).append(indicator.getPageTransView());
        } else {
            var contentsBox = _$("<div/>", {class: "contents_area noContents"});
            var noContArea = _$("<div/>", {class: "no_content_area"});
            contentsBox.append(noContArea);
            dom.empty().append(contentsBox);
            noContArea.append(_$("<img>", {class: "msg_icon", src: modulePath + 'resource/image/icon_noresult.png'}));
            noContArea.append(_$("<span/>", {class: "main"}).text("등록된 콘텐츠가 없습니다"));
            noContArea.append(_$("<span/>", {class: "sub"}).html("나만의 플레이리스트를 구성해 보세요<br>VOD선택 후 <icon/>옵션키로<br>플레이리스트에 등록할 수 있습니다"));
        }
    }

    function setMenuArea() {
        var area = dom.find(".menu_area");
        var bg_area = _$("<div/>", {class: "bg_area"});
        bg_area.append(_$("<div/>", {class: "bg_top"}));
        bg_area.append(_$("<div/>", {class: "bg_mid"}));
        bg_area.append(_$("<div/>", {class: "bg_bottom"}));
        area.append(bg_area);

        var btn_area = _$("<div/>", {class: "btn_area"});
        btn_area.append(_$("<btn/>").text("전체 선택 해제"));
        btn_area.append(_$("<btn/>").text("재생"));
        // [kh.kim] 국방 상품에서는 반복재생 기능 제공하지 않음
        if (!KTW.managers.service.ProductInfoManager.isMilitary()) {
            btn_area.append(_$("<btn/>").text("반복재생"));
        }
        btn_area.append(_$("<btn/>").text("삭제"));
        btn_area.append(_$("<sepline/>"));
        btn_area.append(_$("<btn/>").text("순서 변경"));
        area.append(btn_area);

        // [kh.kim] 국방 상품에서는 반복재생 기능 제공하지 않음
        if (!KTW.managers.service.ProductInfoManager.isMilitary()) {
            menuFocusList = ["AllItemCheck", "Play", "Repeat", "Delete", "Order"];
        }
        else {
            menuFocusList = ["AllItemCheck", "Play", "Delete", "Order"];
        }
    }

    function getItem() {
        var item = _$("<div/>", {class: 'item_area'});
        var tmp = _$('<div>', {class: 'checkBoxArea'});
        tmp.append(_$('<div>', {class: 'checkBox'}));
        item.append(tmp);

        tmp = _$('<div>', {class: 'absoluteWrapper'});
        tmp.append(_$('<img>', {class: 'musicBg', src: modulePath + "resource/image/myplaylist/cd_img_w87.png"}));
        tmp.append(_$('<img>', {class: 'poster'}));
        tmp.append(_$("<div/>", {class: "lock_icon"}).append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"})));
        item.append(_$('<div>', {class: 'posterArea'}).append(_$('<div>', {class: 'posterBox'}).append(tmp)));

        tmp = _$('<div>', {class: 'titleArea'});
        tmp.append(_$('<div>', {class: 'title'}).html("<span/>"));
        tmp.append(_$('<span>', {class: 'description'}));

        item.append(tmp);

        // item.find(".description").prepend(stars.getView(0));

        return item;
    }

    function setItem(div, itemData) {
        if (itemData) {
            var posterUrl = itemData.imgUrl + "?w=87&h=124&quality=90";
            if (itemData.adultOnlyYn == "Y") {
                // 성인 전용인 경우 성인19 디폴트 이미지 표시
                posterUrl = myHome.CONSTANT.IMAGE_PATH + "/image/default_19plus.png"
            }

            div.find("img.poster").attr({
                "src": posterUrl,
                onerror: "this.src='" + modulePath + "resource/image/default_poster.png'"
            });

            // 성인 전용인 경우에는 lock 표시하지 않음
            div.find(".lock_icon").toggle(UTIL.isLimitAge(itemData.prInfo) && itemData.adultOnlyYn != "Y");
            div.find(".titleArea .title span").text(itemData.contsName);
            var bottonText = getBottomText(itemData.wonYn, itemData.prInfo);

            div.find(".titleArea span.description").html(getBottomText(itemData.wonYn, itemData.prInfo));
            // div.find(".rating_star_area.type0.my_list").html(stars.getStarIcon());

            div.toggleClass("checked", checkedList.indexOf(itemData) >= 0);

            div.find(">*").css("visibility", "");

            // div.find(".rating_star_area.type0").hide();
        } else {
            div.find(">*").css("visibility", "hidden");
        }
    }

    function clearTextAnimation() {
        UTIL.clearAnimation(dom.find(".item_area .textAnimating span"));
        dom.find(".item_area .textAnimating").removeClass("textAnimating");
        void dom[0].offsetWidth;
    }

    function setTextAnimation(focus) {
        clearTextAnimation();

        if (playList && playList[focus] && UTIL.getTextLength(playList[focus].contsName, "RixHead M", 33) > 320) {
            var posterDiv = dom.find(".item_area:eq(" + (focus % pageMaxCnt) + ") .title");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    function getBottomText(wonYN, prInfo) {
        return _$((HTool.isTrue(wonYN) ? "<img class='isfreeIcon'>" : (HTool.isFalse(wonYN) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/image/icon/icon_age_list_" + ServerCodeAdapter.getPrInfo(prInfo - 0) + ".png' class='age'>");
    }

    function setListFocus(focus) {
        listFocus = focus;
        setPage(Math.floor(focus / pageMaxCnt));
        // dom.find(".item_area.focus").find(".rating_star_area.type0").hide();
        dom.find(".item_area.focus").removeClass("focus");

        dom.find(".item_area").eq(focus % pageMaxCnt).addClass("focus");
        // dom.find(".item_area.focus").find(".rating_star_area.type0").show();

        dom.find(".focus_area").css("-webkit-transform", "translateY(" + (listFocus % pageMaxCnt * 166) + "px)").removeClass("hidden");
        setTextAnimation(focus);
    }

    function removeListFocus(clearFocus) {
        if (clearFocus) setListFocus(0);
        dom.find(".item_area.focus").removeClass("focus");
        dom.find(".focus_area").addClass("hidden");
    }

    function changeMenuFocus(amount) {
        var newFocus = HTool.getIndex(menuFocus, amount, menuFocusList.length);
        if (dom.find(".menu_area btn").eq(newFocus).hasClass("disable")) {
            changeMenuFocus(amount + (amount / Math.abs(amount)));
        } else setMenuFocus(newFocus);
    }

    function setMenuFocus(focus) {
        menuFocus = focus == null ? menuFocus : focus;
        dom.find(".menu_area btn.focus").removeClass("focus");
        dom.find(".menu_area btn").eq(menuFocus).addClass("focus");
    }

    function removeMenuFocus() {
        menuFocus = 0;
        dom.find(".menu_area btn.focus").removeClass("focus");
    }

    function setPage(_page, forced) {
        if (!forced && page == _page) return;
        page = _page;
        var itemArea = dom.find(".item_area");
        for (var i = 0; i < pageMaxCnt; i++) {
            setItem(itemArea.eq(i), playList[page * pageMaxCnt + i]);
        }

        indicator.setPos(page);
    }

    function resetFocus() {
        // 초기 포커스는 콘텐츠로 초기화
        mainFocus = 0;

        // 첫번째 page로 강제 초기화
        setPage(0, true);

        // 모든 포스터 checke
        toggleAllCheckItem(true);

        // 포스터 List focus 제거
        removeListFocus();

        // 메뉴 btn focus 제거
        removeMenuFocus();

        // 메뉴 btn disable
        setDisableButtonState();

        if (instance.itemCnt > pageMaxCnt) {
            // 적/청색 page hidden
            indicator.togglePageTransIcon(true);
        }
    }

    function changeMainFocus(mf) {
        log.printDbg('changeMainFocus() - mf :' + mf);
        switch (mf) {
            case 0:
                mainFocus = 0;
                removeMenuFocus();
                dom.find(".focus_area").removeClass("hidden");
                setListFocus(Math.min(listFocus, playList.length - 1));
                break;
            case 1:
                mainFocus = 1;
                removeListFocus();
                setMenuFocus(checkedList.length > 0 ? 1 : 0);
                UTIL.clearAnimation(dom.find(".item_area. textAnimating span"));
                dom.find(".item_area. textAnimating").removeClass("textAnimating");
                break;
        }
    }

    function toggleAllCheckItem(check) {
        if (check == null) check = checkedList.length != playList.length;

        if (check) {
            checkedList = [].concat(playList);
            dom.find(".item_area").addClass("checked");
        } else {
            checkedList = [];
            dom.find(".item_area").removeClass("checked");
        }
    }

    function setDisableButtonState() {
        var selectCnt = checkedList.length;
        var btnList = dom.find(".menu_area btn");
        btnList.eq(0).text(selectCnt == playList.length ? "전체 선택 해제" : "전체 선택");

        btnList.eq(1).toggleClass("disable", selectCnt == 0);
        btnList.eq(2).toggleClass("disable", selectCnt == 0);
        btnList.eq(3).toggleClass("disable", selectCnt == 0);
    }

    function onKeyForList(keyCode) {
        log.printDbg('onKeyForList() - keyCode :' + keyCode);
        switch (keyCode) {
            case KEY_CODE.UP:
                if (playList.length <= 1) break;

                if (listFocus === 0) { // 첫 번째 콘텐츠
                    setListFocus(playList.length - 1);
                } else {
                    setListFocus(listFocus - 1);
                }
                break;
            case KEY_CODE.DOWN:
                if (playList.length <= 1) break;

                if (listFocus === playList.length - 1) { // 마지막 콘텐츠
                    setListFocus(0);
                } else {
                    setListFocus(listFocus + 1);
                }
                break;
            case KEY_CODE.LEFT:
            case KEY_CODE.BACK:
                resetFocus();
                instance.viewMgr.historyBack();
                break;
            case KEY_CODE.RIGHT:
                changeMainFocus(1); // 연관 메뉴로 focus 이동

                if (instance.itemCnt > pageMaxCnt) {
                    // 적/청색 page hidden
                    indicator.togglePageTransIcon(true);
                }

                break;
            case KEY_CODE.RED:
                if(listFocus===0) setListFocus(Math.floor((playList.length-1) / pageMaxCnt)*pageMaxCnt);
                else if(listFocus%pageMaxCnt===0) setListFocus(listFocus-pageMaxCnt);
                else setListFocus(page*pageMaxCnt);
                break;
            case KEY_CODE.BLUE:
                if(listFocus===playList.length-1) setListFocus(Math.min(pageMaxCnt - 1, playList.length-1));
                else if(listFocus%pageMaxCnt===pageMaxCnt - 1) setListFocus(Math.min(playList.length-1, listFocus+pageMaxCnt));
                else setListFocus(Math.min(playList.length-1, page*pageMaxCnt+pageMaxCnt - 1));
                break;
            case KEY_CODE.ENTER:
                if (checkedList.indexOf(playList[listFocus]) >= 0) {
                    checkedList.splice(checkedList.indexOf(playList[listFocus]), 1);
                    dom.find(".item_area").eq(listFocus % pageMaxCnt).removeClass("checked");
                } else {
                    checkedList.push(playList[listFocus]);
                    dom.find(".item_area").eq(listFocus % pageMaxCnt).addClass("checked");
                }
                setDisableButtonState();
                break;
            case KEY_CODE.CONTEXT:
                return true;
            default:
                return false;
        }
        return true;
    }

    function onKeyForMenu(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                changeMenuFocus(-1);
                break;
            case KEY_CODE.DOWN:
                changeMenuFocus(1);
                break;
            case KEY_CODE.LEFT:
                if (instance.itemCnt > pageMaxCnt) {
                    // 적/청색 page show
                    indicator.togglePageTransIcon(false);
                }
                changeMainFocus(0);
                break;
            case KEY_CODE.BACK:
                resetFocus();
                instance.viewMgr.historyBack();
                break;
            case KEY_CODE.ENTER:
                enterKeyActionForMenu();
                break;
            case KEY_CODE.CONTEXT:
                return true;
            default:
                return false;
        }
        return true;
    }

    function enterKeyActionForMenu() {
        log.printDbg('enterKeyActionForMenu() - menuFocus :' + menuFocus);
        switch (menuFocusList[menuFocus]) {
            case "AllItemCheck":
                toggleAllCheckItem();
                setDisableButtonState();
                break;
            case "Play":
                //재생
                requestPlayPlayList(false);
                break;
            case "Repeat":
                //반복재생
                requestPlayPlayList(true);
                break;
            case "Delete":
                //삭제
                var items = [], itemDiv = dom.find(".item_area");
                for (var i in checkedList)
                    items.push({title: checkedList[i].contsName});

                if (items.length > 0) LayerManager.activateLayer({
                    obj: {
                        id: "EditModeDeletePopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: false,
                        params: {
                            type: 3,
                            items: items,
                            listName: playListNm,
                            callback: function(res) {
                                if (res) {
                                    var contId = "", itemDiv = dom.find(".item_area");
                                    for (var i in checkedList) contId += checkedList[i].contsKey + "|";
                                    myHome.MyPlayListManager.setMyPlayItemDetList(function(res) {
                                        if (res) {
                                            playList = null;
                                            update();
                                        }
                                    }, playListId, items.length, contId.substr(0, contId.length - 1));
                                }
                                LayerManager.historyBack();
                            }
                        }
                    },
                    moduleId: "module.family_home",
                    visible: true
                });
                break;
            case "Order":
                // 순서 변경
                LayerManager.activateLayer({
                    obj: {
                        id: "MyHomePlayListOrderChangePopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: false,
                        params: {
                            playListId: playListId,
                            playList: playList,
                            callback: function() {
                                playList = null;
                                update();
                            }
                        }
                    },
                    new: true,
                    moduleId: "module.family_home",
                    visible: true
                });

                break;
        }
    }

    function requestPlayPlayList(isLoop) {
        log.printDbg('requestPlayPlayList() - isLoop :' + isLoop);
        var a = ModuleManager.getModule("module.vod");
        if (!a) {
            ModuleManager.loadModule({
                moduleId: "module.vod",
                cbLoadModule: function(success) { if (success) requestPlayPlayList(isLoop); }
            });
            return;
        } else {
            a.execute({
                method: "requestVODPlayList",
                params: {
                    playList: sortCheckedList(),
                    isLoop: isLoop,
                    req_cd: "46",
                    isKids: isKidsPlayList
                }
            })
        }
    }

    function sortCheckedList() {
        var tmp = [];
        for (var i in playList) {
            if (checkedList.indexOf(playList[i]) >= 0) tmp.push(playList[i]);
        }
        // checkedList = tmp;
        return tmp;
    }

    //function onKeyForIndicator(keyCode) {
    //    switch (keyCode) {
    //        case KEY_CODE.UP:
    //            setPage(HTool.getIndex(page, -1, Math.ceil(playList.length / 10)));
    //            listFocus = page * 10;
    //            return true;
    //        case KEY_CODE.DOWN:
    //            setPage(HTool.getIndex(page, 1, Math.ceil(playList.length / 10)));
    //            listFocus = page * 10;
    //            return true;
    //        case KEY_CODE.LEFT:
    //            //indicator.blurred();
    //            instance.viewMgr.historyBack();
    //            return true;
    //        case KEY_CODE.RIGHT:
    //        case KEY_CODE.ENTER:
    //            //indicator.blurred();
    //            setListFocus(listFocus);
    //            return true;
    //        case KEY_CODE.CONTEXT:
    //            return true;
    //        case KEY_CODE.RED:
    //            //indicator.blurred();
    //            setListFocus(page*10);
    //            return true;
    //        case KEY_CODE.BLUE:
    //            //indicator.blurred();
    //            setListFocus(Math.min(playList.length-1, page*10+9));
    //            return true;
    //    }
    //}

    this.onKeyAction = function(keyCode) {
        log.printDbg('onKeyAction() - keyCode :' + keyCode);
        //if (indicator.isFocused()) return onKeyForIndicator(keyCode);
        if (mainFocus == 0) return onKeyForList(keyCode);
        else if (mainFocus == 1) return onKeyForMenu(keyCode);
    };

    this.focused = function() {
        log.printDbg('focused()');

        if (this.itemCnt > 0) {
            /**
             * [dj.son] [WEBIIIHOME-3830] 예외처리 추가
             */
            if (!playList || playList.length <= 0) {
                return 0;
            }

            View.prototype.focused.apply(this, arguments);

            // 첫번째 콘텐츠로 focus 이동
            setListFocus(0);

            if (instance.itemCnt > pageMaxCnt) {
                // 적/청색 page show
                indicator.togglePageTransIcon(false);
            }

            // contentListDom.find(".rating_star_area.type0").show();
            _$(".mypList_def_homeShow").hide();
            return true;
        } else return false;
    };

    this.blurred = function() {
        // dom.find(".contents_area").css("-webkit-transform", "");

        View.prototype.blurred.apply(this, arguments);
        //_$(".mypList_def_homeShow").show();
        //setMenuFocus(1);
        //toggleAllCheckItem(true);
        //setDisableButtonState();
        //UTIL.clearAnimation(dom.find(".item_area. textAnimating span"));
        //dom.find(".item_area. textAnimating").removeClass("textAnimating");
        //setPage(0)
        //if (this.itemCnt > 0) {
        //     dom.find(".rating_star_area.type0").hide();
        //}
    };

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        update();
    };

    this.hide = function() {
        View.prototype.hide.apply(this, arguments);
        comp && comp.hide && comp.hide();
    };

    this.pause = function() {
        clearTextAnimation();
    };

    this.resume = function() {
        if (mainFocus == 0) setTextAnimation(listFocus);
    };

    this.getComponent = function() {
        return comp;
    }
};
myHome.view.MyPlayListView.prototype = new View();
myHome.view.MyPlayListView.prototype.constructor = myHome.view.MyPlayListView;
myHome.view.MyPlayListView.prototype.type = View.TYPE.CONTENTS;
