/**
 * 마이 플레이리스트 카테고리 뷰
 * (우리집 맞춤 TV > 마이 플레이리스트 > 좌측 카테고리 리스트 등)
 * Created by ksk91_000 on 2017-02-06.
 */
myHome.view.myPlayListCategoryView = function() {
    View.call(this, "categoryView");

    var categories;
    var contextMenu = new ContextMenu();
    var kidsContextMenu = new ContextMenu();

    var updateCb;
    var focusIndex = 0;
    var pageIndex = 0;

    var pages = [];

    var div = this.div;
    var language;

    var _this = this;
    var focusedElement = null;

    this.create = function(data) {
        updateCb = data.updateCb;
        categories = data.data;

        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");

        (function init() {
            focusIndex = 0;
            pageIndex = 0;
            var html = "";

            makePages();

            // to make category area
            html += "<div id='menuArea' class='menuArea'>" +
                        makeCategoryItems(pages[pageIndex]) +
                    "</div>";

            // to make focus area
            html += "<div id='focusArea'>" +
                        makeFocusArea(pages[pageIndex][focusIndex]) +
                    "</div>";

            // to make page move guide
            if(categories.length > 9) {
                html += "<div class='category_bottom_arw'/>" +
                        "<div class='lr_arw_area'>" +
                            "<img src='" + modulePath + "resource/image/icon_pageup.png'>" +
                            "<img src='" + modulePath + "resource/image/icon_pagedown.png'>" +
                            "<span id='pageIndex'>00 / 00</span>" +
                        "</div>";
            }
            html+='<div class="leftArrow"/>';
            html+="<div class='context_menu_indicator'>플레이리스트 편집</div>";

            div.attr({id:"categoryArea"});
            div.html(html);

            createContextMenu();
            updateIndex();
            setFocusArrowVisibility(!!categories[0].w3ListImgUrl);      // 첫번쨰 항목이 텍스트 인 경우에만 화살표 출력
        })();
    };

    function makePages() {
        var page = [], size = 0;
        for(var i = 0; i < categories.length; i++) {
            var item = Object.create(categories[i]);
            if(size + Number.parseInt(item.w3MenuImgType || "01") > 9) {
                item.w3ListImgUrl = "";
                item.w3MenuImgType = "01";
            }
            page.push(item);
            size += Number.parseInt(item.w3MenuImgType || "01");
            if(size === 9) {
                pages.push(page);
                page = [];
                size = 0;
            }
        }
        if(page.length > 0)
            pages.push(page);
    }

    function makeFocusArea(data) {
        var heightOffset = 22;
        var height = ((Number.parseInt(data.w3MenuImgType) || 1) * 94 - heightOffset) + "px";
        return  "<div class='textArea'>"+
                    "<div id='focus_border' style='height:" + height + "'>" +
                    "</div>" +
                    "<div class='rightArrow'>" +
                    "</div>" +
                "</div>";
    }

    function makeCategoryItems(page) {
        var html = "";

        for(var i = 0; i < page.length; i++) {
            var item = page[i];
            var itemType = item.w3ListImgUrl ? 'img' : 'text';
            if(itemType === 'text') {
                html += "<div class='text_item'>" +
                    "<span>" + getTitle(i) + "</span>" +
                    "</div>";
            } else {
                html += "<div class='image_item size" + (item.w3MenuImgType || "01") + "'>" +
                            "<img src='" + item.w3ListImgUrl + "'>" +
                        "</div>";
            }
        }

        return html;
    }

    function getTitle(index) {
        var englishName = "", name = "";
        if(index instanceof window.parent.VItem) {
            englishName = index.englishItemName || index.title;
            name = index.title;
        } else {
            englishName = pages[pageIndex][index].englishItemName || pages[pageIndex][index].title;
            name = pages[pageIndex][index].title;
        }
        return (language === "eng") ? englishName : name;
    }

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");
    };

    this.setPage = function (newPage, forced) {
        if(!forced && pageIndex === newPage)
            return;

        pageIndex = newPage;

        var menuArea = this.div.find('#menuArea');
        // to add new category items
        menuArea.html(makeCategoryItems(pages[pageIndex]))
    };

    this.setFocus = function(newFocus) {
        if(this.div.css("display") === "none")
            return;

        focusIndex = newFocus;

        UTIL.getNewCategoryIndex(pages, pageIndex, focusIndex, function (newFocusIdx, newPageIdx) {
            focusIndex = newFocusIdx;
            pageIndex = newPageIdx;
            _this.setPage(pageIndex, true);
        });

        // 포커스 아이템 변경
        var itemType = pages[pageIndex][focusIndex].w3ListImgUrl ? 'img' : 'text';
        if (focusedElement) {
            focusedElement.removeClass('focus');
            clearFocusAnimation(focusedElement);
        }
        focusedElement = this.div.find('#menuArea div:eq('+focusIndex+')');
        focusedElement.addClass('focus');
        if (itemType === 'text') {
            setFocusAnimation(focusedElement);
        }

        // 포커스 바 위치 수정
        var focusArea = this.div.find('#focusArea');

        // [si.mun] DOM 에 categoryView가 append 되기 전엔
        // focusedElement.position().top 의 값은 0이 반환된다.
        // 때문에, 아래와 같이 top position 을 계산하는 로직을 삽입
        var top = 0;
        for(var i = 0; i < focusIndex; i++)
            top += parseInt(pages[pageIndex][i].w3MenuImgType || 1) * 94;

        focusArea.css("-webkit-transform", "translateY(" + top  + "px)");

        var heightOffset = 12;
        setFocusArrowVisibility(itemType === 'img');
        focusArea.find("#focus_border").css("height", (focusedElement.height() - heightOffset) + "px");

        updateIndex(this);


        if(categories[focusIndex].info.isKidsYn==="Y") {
            div.find(".context_menu_indicator").text('플레이리스트 추가');
        } else {
            div.find(".context_menu_indicator").text('플레이리스트 편집');
        }
    };

    function setFocusArrowVisibility(isVisible) {
        var arrow = div.find('#focusArea').find(".rightArrow")[0];
        arrow.hidden = isVisible;
    }

    this.changeFocus = function(amount) {
        if (amount == 0)
            return;

        if (categories.length <= 1)
            return;

        focusIndex += amount;

        if(focusIndex < 0) {
            moveToPreviousPage();
            focusIndex = pages[pageIndex].length - 1;
        } else if(focusIndex >= pages[pageIndex].length) {
            moveToNextPage();
            focusIndex = 0;
        }
        this.setFocus(focusIndex);
    };

    function moveToNextPage() {
        var newPage = pageIndex;
        if(++newPage >= pages.length)
            newPage = 0;
        _this.setPage(newPage);
    }

    function moveToPreviousPage() {
        var newPage = pageIndex;
        if(--newPage < 0)
            newPage = pages.length - 1;
        _this.setPage(newPage);
    }

    function updateIndex() {
        var index = '00' + (pageIndex + 1);
        var pageCount = '00' + (pages.length);
        div.find('#pageIndex').text(index.slice(-2) + ' / ' + pageCount.slice(-2));
    }

    function clearFocusAnimation(element) {
        if (!element || !element.hasClass("textAnimating")) {
            return;
        }
        UTIL.clearAnimation(element.find("span"));
        element.removeClass("textAnimating");
        void element[0].offsetWidth;
    }

    function setFocusAnimation(element) {
        if (!element) {
            return;
        }
        if(UTIL.getTextLength(element.find("span").text(), "RixHead B", 42, -2.1) > 389) {
            element.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: element
            });
        }
    }

    function getCurrentContent() {
        return pages[pageIndex][focusIndex];
    }


    function getCurrentIndex() {
        return focusIndex;
    }

    function createContextMenu() {
        contextMenu.addButton("플레이리스트 추가");
        contextMenu.addSeparateLine();
        contextMenu.addTitle("", "playListNm");
        contextMenu.addButton("플레이리스트 명 변경");
        contextMenu.addButton("플레이리스트 삭제");
        contextMenu.setEventListener(contextMenuListener);

        kidsContextMenu.addButton("플레이리스트 추가");
        kidsContextMenu.setEventListener(contextMenuListener);
        div.append(_$("<div/>", {class:"contextMenuArea"}).append(contextMenu.getView()));
        div.append(_$("<div/>", {class:"contextMenuArea"}).append(kidsContextMenu.getView()));
    }

    function contextMenuListener(index) {
        contextMenu.close();
        kidsContextMenu.close();
        var layerId = "";
        switch (index) {
            case 0:
                if (categories.length >= 9) layerId = "playlistAddDenyPopup";
                else layerId = "playlistAddPopup";
                break;
            case 1:
                layerId = "playlistEditPopup";
                break;
            case 2:
                if (categories.length <= 2) layerId = "playlistDeleteDenyPopup";
                else layerId = "playlistDeletePopup";
                break;
        }

        LayerManager.activateLayer({
            obj: {
                id: layerId,
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    data: pages[pageIndex][focusIndex],
                    callback: function (res) {
                        if(res) {
                            updateCb();
                        }
                    }
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    }


    this.onKeyAction = function(keyCode) {
        if(contextMenu.isOpen()) return contextMenu.onKeyAction(keyCode);
        if(kidsContextMenu.isOpen()) return kidsContextMenu.onKeyAction(keyCode);
        switch(keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return false;

            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return false;
            case KEY_CODE.CONTEXT:
                if(categories[focusIndex].info.isKidsYn==="Y") {
                    kidsContextMenu.getView().find(".playListNm").html(HTool.addSpace(categories[focusIndex].title));
                    kidsContextMenu.open();
                } else {
                    contextMenu.getView().find(".playListNm").html(HTool.addSpace(categories[focusIndex].title));
                    contextMenu.open();
                } return true;
            case KEY_CODE.RED :
            case KEY_CODE.BLUE :
                // [kh.kim] PlayList 카테고리에서는 페이지 up,down 동작 하지 않음
                return true;
            default:
                return false;
        }
    };

    this.unfocused = function() {
        this.setFocus(focusIndex);
        div.addClass("unFocused");
        div.find(".context_menu_indicator").text("");
        clearFocusAnimation(focusedElement);
    };

    this.focused = function() {
        div.removeClass("unFocused");
        this.div.css('opacity', 1);
        this.setFocus(focusIndex);
        return true;
    };

    this.blurred =  function() {
        this.div.css('opacity', 0);
        clearFocusAnimation(focusedElement);
    };

    this.pause = function () {
        this.setFocus(focusIndex);
        clearFocusAnimation(focusedElement);
    };

    this.getCurrentContent = getCurrentContent;
    this.getCurrentIndex = getCurrentIndex;
};

myHome.view.myPlayListCategoryView.prototype = new View();
myHome.view.myPlayListCategoryView.prototype.constructor = myHome.view.myPlayListCategoryView;
myHome.view.myPlayListCategoryView.prototype.type = View.TYPE.CATEGORY;