/**
 * 당신을 위한 VOD 추천의 카테고리 뷰
 * (우리집 맞춤 TV > 당신을 위한 VOD 추천 > 나의 취향 알아보기)
 * Created by ksk91_000 on 2016-06-30.
 */
myHome.view.forYouCategoryView = function() {
    View.call(this, "categoryView");

    var categories;
    var contextMenu;

    var focusIndex = 0;
    var pageIndex = 0;
    var pages = [];
    var div = this.div;
    var _this = this;

    var focusedElement;

    this.create = function(data) {
        categories = data;
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");

        (function init() {
            focusIndex = 0;
            pageIndex = 0;
            var html = "";

            makePages();

            // to make category area
            html += "<div id='menuArea' class='menuArea'>" +
                        makeCategoryItems(pages[pageIndex]) +
                    "</div>";

            // to make focus area
            html += "<div id='focusArea'>" +
                        makeFocusArea(pages[pageIndex][focusIndex]) +
                    "</div>";

            // to make page move guide
            if(categories.length > 9) {
                html += "<div class='category_bottom_arw'/>" +
                        "<div class='lr_arw_area'>" +
                            "<img src='" + modulePath + "resource/image/icon_pageup.png'>" +
                            "<img src='" + modulePath + "resource/image/icon_pagedown.png'>" +
                            "<span id='pageIndex'>00 / 00</span>" +
                        "</div>";
            }
            html+='<div class="leftArrow"/>';

            div.attr({id:"categoryArea"});
            div.html(html);

            createContextMenu();
            updateIndex();
            setFocusArrowVisibility(!!categories[0].w3ListImgUrl);      // 첫번쨰 항목이 텍스트 인 경우에만 화살표 출력
        })();
    };

    function makePages() {
        var page = [], size = 0;
        for(var i = 0; i < categories.length; i++) {
            var item = Object.create(categories[i]);
            if(size + Number.parseInt(item.w3MenuImgType || "01") > 9) {
                item.w3ListImgUrl = "";
                item.w3MenuImgType = "01";
            }
            page.push(item);
            size += Number.parseInt(item.w3MenuImgType || "01");
            if(size === 9) {
                pages.push(page);
                page = [];
                size = 0;
            }
        }
        if(page.length > 0)
            pages.push(page);
    }

    function makeFocusArea(data) {
        var heightOffset = 22;
        var height = ((Number.parseInt(data.w3MenuImgType) || 1) * 94 - heightOffset) + "px";
        return  "<div class='textArea'>"+
                    "<div id='focus_border' style='height:" + height + "'>" +
                    "</div>" +
                    "<div class='rightArrow'>" +
                    "</div>" +
                "</div>";
    }

    function makeCategoryItems(page) {
        var html = "";

        for(var i = 0; i < page.length; i++) {
            var item = page[i];
            var itemType = item.w3ListImgUrl ? 'img' : 'text';
            if(itemType === 'text') {
                html += "<div class='text_item'>" +
                    "<span>" + getTitle(i) + "</span>" +
                    "</div>";
            } else {
                html += "<div class='image_item size" + (item.w3MenuImgType || "01") + "'>" +
                            "<img src='" + item.w3ListImgUrl + "'>" +
                        "</div>";
            }
        }

        return html;
    }

    function getTitle(index) {
        var englishName = "", name = "";
        if(index instanceof window.parent.VItem) {
            englishName = index.englishItemName || index.title;
            name = index.title;
        } else {
            englishName = pages[pageIndex][index].englishItemName || pages[pageIndex][index].title;
            name = pages[pageIndex][index].title;
        }
        return (language === "eng") ? englishName : name;
    }
    function createContextMenu() {
        contextMenu = new ContextMenu();

        var adult_limit = MyPreferenceManager.isNoAdult()?0:1;
        contextMenu.addTitle("19세 이상 콘텐츠 추천");
        contextMenu.addDropBox(["추천에서 제외", "추천에 포함"], adult_limit);

        div.append(contextMenu.getView());
        contextMenu.setEventListener(function (index, data) {
            contextMenu.close();
            if(index==0) {
                StorageManager.ps.save(StorageManager.KEY.RECOMMEND_ADULT_LIMIT, data);
                _this.viewMgr.getCurrentMenuData().getView().reloadView();
                _this.viewMgr.clearView();
            }
        });
    }

    this.updateMenuData = function (callback) {
        var that = this;
        CurationManager.expire();
        CurationManager.getCurationData(function (data) {
            var tmpFocus = 0;
            if(focusIndex==0) tmpFocus = 0;
            else if(focusIndex==categories.length-1) tmpFocus = data.length+1;
            else tmpFocus = 1;
            categories.splice.apply(categories, [1, categories.length-2].concat(data));
            that.create(categories);
            that.setFocus(tmpFocus);
            that.viewMgr.clearView();
            if(callback) callback();
        });
    };

    this.show = function() {
        View.prototype.show.apply(this, arguments);
        language = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(language) && (language = "kor");
        // 나의 취향 알아보기 및 내가 평가한 콘텐츠의 경우 19세 등급제한 출력
        var focusedMenuItem = pages[pageIndex][focusIndex];
        if(focusedMenuItem === MenuDataManager.MENU_ID.MY_CURATION ||
            focusedMenuItem === MenuDataManager.MENU_ID.MY_RECOMMEND_ONLY_MINE)
            div.find(".context_menu_indicator2").text("19세 등급 제한");
        else
            div.find(".context_menu_indicator2").text("");
    };

    this.setPage = function (newPage, forced) {
        if(!forced && pageIndex === newPage)
            return;

        pageIndex = newPage;

        var menuArea = this.div.find('#menuArea');
        // to add new category items
        menuArea.html(makeCategoryItems(pages[pageIndex]))
    };

    this.setFocus = function(newFocus) {
        if(this.div.css("display") === "none")
            return;

        focusIndex = newFocus;

        UTIL.getNewCategoryIndex(pages, pageIndex, focusIndex, function (newFocusIdx, newPageIdx) {
            focusIndex = newFocusIdx;
            pageIndex = newPageIdx;
            _this.setPage(pageIndex, true);
        });

        // 포커스 아이템 변경
        var itemType = pages[pageIndex][focusIndex].w3ListImgUrl ? 'img' : 'text';
        if (focusedElement) {
            focusedElement.removeClass('focus');
            clearFocusAnimation(focusedElement);
        }
        focusedElement = this.div.find('#menuArea div:eq('+focusIndex+')');
        focusedElement.addClass('focus');
        if (itemType === 'text') {
            setFocusAnimation(focusedElement);
        }

        var focusArea = this.div.find('#focusArea');

        // [si.mun] DOM 에 categoryView가 append 되기 전엔
        // focusedElement.position().top 의 값은 0이 반환된다.
        // 때문에, 아래와 같이 top position 을 계산하는 로직을 삽입
        var top = 0;
        for(var i = 0; i < focusIndex; i++)
            top += parseInt(pages[pageIndex][i].w3MenuImgType || 1) * 94;

        focusArea.css("-webkit-transform", "translateY(" + top  + "px)");

        var heightOffset = 12;
        setFocusArrowVisibility(itemType === 'img');
        focusArea.find("#focus_border").css("height", (focusedElement.height() - heightOffset) + "px");

        updateIndex(this);
    };

    function setFocusArrowVisibility(isVisible) {
        var arrow = div.find('#focusArea').find(".rightArrow")[0];
        arrow.hidden = isVisible;
    }

    this.changeFocus = function(amount) {
        if (amount == 0)
            return;

        if (categories.length <= 1)
            return;

        focusIndex += amount;

        if(focusIndex < 0) {
            moveToPreviousPage();
            focusIndex = pages[pageIndex].length - 1;
        } else if(focusIndex >= pages[pageIndex].length) {
            moveToNextPage();
            focusIndex = 0;
        }
        this.setFocus(focusIndex);
    };

    function moveToNextPage() {
        var newPage = pageIndex;
        if(++newPage >= pages.length)
            newPage = 0;
        _this.setPage(newPage);
    }

    function moveToPreviousPage() {
        var newPage = pageIndex;
        if(--newPage < 0)
            newPage = pages.length - 1;
        _this.setPage(newPage);
    }

    function updateIndex() {
        var index = '00' + (pageIndex + 1);
        var pageCount = '00' + (pages.length);
        div.find('#pageIndex').text(index.slice(-2) + ' / ' + pageCount.slice(-2));
    }

    function clearFocusAnimation(element) {
        if (!element || !element.hasClass("textAnimating")) {
            return;
        }
        UTIL.clearAnimation(element.find("span"));
        element.removeClass("textAnimating");
        void element[0].offsetWidth;
    }

    function setFocusAnimation(element) {
        if (!element) {
            return;
        }
        if(UTIL.getTextLength(element.find("span").text(), "RixHead B", 42, -2.1) > 389) {
            element.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: element
            });
        }
    }

    function getCurrentContent() {
        return pages[pageIndex][focusIndex];
    }


    function getCurrentIndex() {
        return focusIndex;
    }

    this.onKeyAction = function(keyCode) {
        if(contextMenu.isOpen())
            return contextMenu.onKeyAction(keyCode);

        switch(keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return false;

            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return false;

            case KEY_CODE.RED :
                if(focusIndex === 0) {
                    moveToPreviousPage();
                }
                this.setFocus(0);
                return false;

            case KEY_CODE.BLUE :
                if(focusIndex === pages[pageIndex].length - 1) {
                    moveToNextPage();
                }
                this.setFocus(pages[pageIndex].length - 1)
                return false;

            case KEY_CODE.CONTEXT:
                if(focusIndex==0 || focusIndex == (categories.length-1)) {
                    // var focus_context = StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT);
                    // var focus_context;
                    // if(focus == 0) focus_context = MyPreferenceManager.getAdultLimit();
                    // else focus_context = RatingContentsManager.getAdultLimit();
                    //
                    // contextMenu.setDropBoxFocus(0, focus_context-0);
                    contextMenu.open();
                }
                return true;

            default:
                return false;
        }
    };

    this.unfocused = function() {
        this.setFocus(focusIndex);
        div.addClass("unFocused");
        div.find(".context_menu_indicator2").text("");
        clearFocusAnimation(focusedElement);
    };

    this.focused = function() {
        div.removeClass("unFocused");
        this.div.css('opacity', 1);
        this.setFocus(focusIndex);
        this.viewMgr.changeBackground(null, 0);
        return true;
    };

    this.blurred =  function() {
        this.div.css('opacity', 0);
        clearFocusAnimation(focusedElement);
    };

    this.pause = function () {
        this.setFocus(focusIndex);
        clearFocusAnimation(focusedElement);
    };

    this.getCurrentContent = getCurrentContent;
    this.getCurrentIndex = getCurrentIndex;
};

myHome.view.forYouCategoryView.prototype = new View();
myHome.view.forYouCategoryView.prototype.constructor = myHome.view.forYouCategoryView;
myHome.view.forYouCategoryView.prototype.type = View.TYPE.CATEGORY;