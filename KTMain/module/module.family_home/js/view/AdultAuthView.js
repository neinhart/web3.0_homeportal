/**
 * Created by ksk91_000 on 2016-07-01.
 */
myHome.view.AdultAuthView = function(viewId) {
    View.call(this, viewId + "View");
    this.div.addClass("adultAuthViewView");
    var adultAuthBox = new component.AdultAuthBox();

    this.create = function() {
        var that = this;
        adultAuthBox.create(function () {
            that.viewMgr.setAdultAuthCheck();
            that.viewMgr.reChangeFocus();
        });
        this.div.append(adultAuthBox.getView());
        this.div.append(_$("<img>", {class:'tip_icon', src:modulePath + "resource/image/icon_tips.png"}));
        this.div.append(_$("<div/>", {class:'tip_text'}).html("'TV에 내폰 연결'(ch.742) 메뉴에서 올레 tv 모바일앱과 올레 tv를 연결하시면<br>구매한 VOD를 폰에서도 추가 결제 없이 시청하실 수 있습니다"));
    };

    this.onKeyAction = function(keyCode) {
        if(adultAuthBox.onKeyAction(keyCode)) return true;
        switch (keyCode) {
            case KEY_CODE.LEFT:
                this.viewMgr.historyBack();
                return true;
        }
    };

    this.focused = function () {
        this.div.addClass("focused");
        return adultAuthBox.focused();
    };

    this.blurred = function () {
        this.div.removeClass("focused");
        return adultAuthBox.blurred();
    };

    this.pause = function () {
        adultAuthBox.pause();
    };

    this.resume = function () {
        adultAuthBox.resume();
    };

    this.show = function () {
        View.prototype.show.apply(this, arguments);
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
        adultAuthBox.blurred();
    };
};
myHome.view.AdultAuthView.prototype = new View();
myHome.view.AdultAuthView.prototype.constructor = myHome.view.AdultAuthView;
myHome.view.AdultAuthView.prototype.type = View.TYPE.CONTENTS;
