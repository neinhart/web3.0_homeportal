/**
 * Created by ksk91_000 on 2016-07-01.
 */
myHome.view.HelpListView = function(viewId) {
    View.call(this, viewId + "View");
    var instance = this;
    var contentList;
    var focus = 0, curPage = 0;
    var indicator;
    var catId = viewId;

    this.create = function(data) {
        if (data) this.setData(data);
        else myHome.amocManager.getItemDetlListW3(function (result, data) {
            if (result) {
                contentList = data.itemDetailList;
                contentList = contentList == null ? [] : Array.isArray(contentList) ? contentList : [contentList];
                instance.setData(contentList);
            }
        }, catId, 1, 20, DEF.SAID);
    };

    function constructHTML() {
        function makeItem(index) {
            var html = "<tr id='item_" + index + "'>" +
                "<td class='imgArea'>" +
                "<img>"+
                "</td>" +
                "<td>" +
                "<span class='title'/>" +
                "<span class='description'/>" +
                "</td>";
            html += "</tr>";
            return _$(html);
        }

        var html = _$("<div/>", {id: 'listArea'});
        var table = _$("<table/>");
        html.append(table);
        for (var i = 0; i < 6; i++) {
            table.append(makeItem(i));
        } html.append(_$("<div/>", {class:"focus_line"}));

        instance.div.append(html);
        indicator.setSize(Math.ceil(contentList.length/4));
    }

    function setItem(div, item) {
        if(item) {
            div.removeClass("hiddenItem");
            div.find("img").attr("src", item.imgUrl + "?w=119&170&quality=90");
            div.find(".title").text(item.itemName);
            div.find(".description").text(item.synopsis);
        }else div.addClass("hiddenItem");
    }

    this.setData = function(_contentList) {
        contentList = _contentList;
        indicator = new Indicator(1);
        constructHTML();
        instance.div.append(indicator.getView()).append(indicator.getPageTransView());
        setFocus(focus, true);
    };

    function setFocus(newFocus, forced) {
        if (forced || Math.floor(focus / 4) != Math.floor(newFocus / 4)) setPage(Math.floor(newFocus / 4));

        instance.div.find("tr.focus").removeClass("focus");
        focus = newFocus;
        instance.div.find("#item_" + (focus - curPage*4)).addClass("focus");
        instance.div.find(".focus_line").css("-webkit-transform", "translateY(" + (193*(focus%4)) + "px)");
    }

    function setPage(page) {
        curPage = page;
        for(var i=0; i<6; i++) setItem(instance.div.find("#item_" + i), contentList[page*4 + i]);
        indicator.setPos(page);
    }

    this.onKeyAction = function(keyCode) {
        if(indicator.isFocused()) return onKeyForIndicator(keyCode);
        switch (keyCode) {
            case KEY_CODE.UP :
                setFocus(HTool.getIndex(focus, -1, contentList.length));
                return true;
            case KEY_CODE.DOWN :
                setFocus(HTool.getIndex(focus, 1, contentList.length));
                return true;
            case KEY_CODE.LEFT:
                if(contentList.length>4) {
                    instance.div.find("tr.focus").removeClass("focus");
                    indicator.focused();
                    instance.div.find(".focus_line").hide();
                    return true;
                } else return false;
            case KEY_CODE.ENTER :
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, catId, contentList[focus].itemId, "01");
                } catch(e) {}
                openDetailLayer(catId, contentList[focus].itemId, "01");
                return true;
            default:
                return false;
        }
    };

    function onKeyForIndicator(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                setPage(HTool.getIndex(curPage, -1, Math.ceil(contentList.length/4)));
                return true;
            case KEY_CODE.DOWN:
                setPage(HTool.getIndex(curPage, 1, Math.ceil(contentList.length/4)));
                return true;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                setFocus(curPage*4);
                indicator.blurred();
                instance.div.find(".focus_line").show();
                return true;
            case KEY_CODE.LEFT:
            case KEY_CODE.BACK:
                return false;
            case KEY_CODE.RED:
                setFocus(curPage*4);
                indicator.blurred();
                instance.div.find(".focus_line").show();
                return true;
            case KEY_CODE.BLUE:
                setFocus(Math.min(contentList.length/4, curPage*4+3));
                indicator.blurred();
                instance.div.find(".focus_line").show();
                return true;
        }
    }

    this.focused = function () {
        View.prototype.focused.apply(this, arguments);
        if(contentList != undefined) setFocus(0);
        return true;
    };

    this.blurred = function () {
        View.prototype.blurred.apply(this, arguments);
        instance.div.find("tr.focus").removeClass("focus");
    };

    this.show = function () {
        if(contentList != undefined) {
            setFocus(focus);
            indicator.blurred();
            instance.div.find(".focus_line").show();
        }
        View.prototype.show.apply(this, arguments);
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
    };
};
myHome.view.HelpListView.prototype = new View();
myHome.view.HelpListView.prototype.constructor = myHome.view.HelpListView;
myHome.view.HelpListView.prototype.type = View.TYPE.CONTENTS;
