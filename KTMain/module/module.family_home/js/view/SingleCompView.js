/**
 * Created by ksk91_000 on 2016-07-01.
 */
myHome.view.SingleCompView = function(viewId) {
    View.call(this, viewId + "View");
    var comp;

    this.create = function(data) {
        if(data.component){
            comp = data.component;
            comp.parent = this;
            this.div.append(comp.getView());
        }
    };

    this.onKeyAction = function(keyCode) {
        return comp&&comp.onKeyAction&&comp.onKeyAction(keyCode);
    };

    this.focused = function () {
        var ret = comp&&comp.focused&&comp.focused();
        switch (ret) {
            case myHome.ViewManager.FOCUS_MODE.NO_FOCUS:
                break;
            case myHome.ViewManager.FOCUS_MODE.FOCUS:
            case true:
                View.prototype.focused.apply(this, arguments);
                break;
            case myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS:
                break;
        } return ret?ret:!comp.focused;
    };

    this.blurred = function () {
        View.prototype.blurred.apply(this, arguments);
        comp&&comp.blurred&&comp.blurred();
    };

    this.show = function () {
        View.prototype.show.apply(this, arguments);
        comp&&comp.show&&comp.show();
    };

    this.hide = function () {
        View.prototype.hide.apply(this, arguments);
        comp&&comp.hide&&comp.hide();
    };

    this.getComponent = function() {
        return comp;
    }
};
myHome.view.SingleCompView.prototype = new View();
myHome.view.SingleCompView.prototype.constructor = myHome.view.SingleCompView;
myHome.view.SingleCompView.prototype.type = View.TYPE.CONTENTS;
