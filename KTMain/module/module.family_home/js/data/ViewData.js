/**
 * Created by ksk91_000 on 2016-11-21.
 */
window.ViewData = function(viewId, viewClass, data, title, menuId, adultMenu) {
    this.viewId = viewId;
    this.viewClass = viewClass||View;
    this.data = data;
    this.title = title;
    this.id = menuId;
    this.view;
    this.hasToAdultCheck = !!adultMenu;
};

ViewData.prototype.getView = function() {
    if(!this.view) {
        this.view = new this.viewClass(this.viewId);
        this.view.create(this.data);
    }

    return this.view;
};

ViewData.prototype.destroy = function () {
    this.view.getView().remove();
    this.view = null;
};

ViewData.prototype.getViewId = function () {
    return this.viewId;
}