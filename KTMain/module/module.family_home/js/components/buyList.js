/**
 * VOD 구매목록 (우리집 맞춤 TV > 구매내역 보기 > VOD 구매목록)
 * Created by ksk91_000 on 2016-07-01.
 */
if (!component) var component = {};

component.buyList = function (id, type) {
    var componentId = id;
    var listData = [];
    var focus = 0;
    var page = 0;
    var editMode = false;
    var isEditFocus = false;
    var indicator = new Indicator(1);
    var authAdultBox;
    var contextMenu;
    var dvdYn = "A";
    var deletedItemYn = "Y";
    var hasDvd = false;
    var checkedList = [];
    var isLoaded = false;
    var isLoading = false;
    var isFocused = false;
    var isLoadWating = false;
    var originData;

    var instance = this;

    var pageMaxCnt = 5;

    var dom = _$("<div id='" + componentId + "' class='buyList'>" +
        "<div id='amountTxt'>총 0개</div>" +
        "<div id='addedText'>전체 구매 내역</div>" +
        "<div class='item_area'/>" +
        "<div class='context_menu_indicator' style='display:none'>목록에서 삭제</div>" +
        "</div>");
    dom.append(indicator.getView()).append(indicator.getPageTransView());
    makeContextMenu();
    // authAdultBox = new component.AdultAuthBox();

    this.getView = function () {
        return dom;
    };

    this.update = function (callback) {
        if (isLoading) return;
        isLoading = true;
        var that = this;
        LayerManager.startLoading({preventKey: true});
        myHome.amocManager.getBuyHistoryNxt("saId=" + DEF.SAID + "&adultYn=A&opt=3&itemCnt=A&deletedItemYn=" + deletedItemYn + "&dvdYn=" + dvdYn + "&pkgVodYn=Y", function (result, data) {
            if (result) {
                listData = Array.isArray(data.buyHistory) ? data.buyHistory : (data.buyHistory ? [data.buyHistory] : []);
                hasDvd = false;
                for (var i in listData) if (listData[i].isDvdYn == "Y") {
                    hasDvd = true;
                    break;
                }
                updateView(that.parent.viewMgr);
                isEditFocus = false;
                //toggleEditMode(true);

                // contextMenu는 기존과 다르게 항상 open 되어 있어야 한다.
                contextMenu.open();
                //contextMenu.setDisable(3, hasDvd);

                // 소장용 VOD가 존재하지 않으면 필터옵션 disable
                contextMenu.setDisable(0, hasDvd);
                // 전체 선택, 결제수단 확인 disable
                contextMenu.setDisable(2, false);
                contextMenu.setDisable(3, false);

                isLoaded = true;
                if (isFocused) instance.focused();
                else instance.blurred();
                LayerManager.stopLoading();
                isLoading = false;
                if (callback) callback();
            } else {
                listData = [];
                updateView(that.parent.viewMgr);
                LayerManager.stopLoading();
                // [skkwon 17/11/20] Web2.0에서 팝업을 띄우지 않고 있다고 하여 뺌. (WEBIIIHOME-3529)
                // HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                // extensionAdapter.notifySNMPError("VODE-00013");
                isLoaded = true;
                isLoading = false;
                if (callback) callback();
            }
        });
    };

    function updateView(viewMgr) {
        dom.find(".item_area").empty();
        if (authAdultBox) {
            authAdultBox.create(function () {
                authAdultBox = null;
                dom.find("#" + componentId).removeClass("blurred");
                updateView(viewMgr);
                viewMgr.reChangeFocus();
            });
            dom.find(".item_area").append(authAdultBox.getView());
            dom.find(".item_area").append(_$("<div/>", {class: 'tip_text'}).html("'TV에 내폰 연결'(ch.742) 메뉴에서 올레 tv 모바일앱과 올레 tv를 연결하시면<br>구매한 VOD를 폰에서도 추가 결제 없이 시청하실 수 있습니다"));
        } else if (listData.length <= 0) {
            dom.find(".item_area").append(getNoDataView());
        } else {
            dom.find(".item_area").append("<div class='focus_line'/><table class='item_list'/>");
            dom.find("#amountTxt").text("총 " + listData.length + "개");
            indicator.setSize(Math.ceil(listData.length / pageMaxCnt));

            for (var i = 0; i < pageMaxCnt; i++) {
                dom.find(".item_list").append(getItemView());
            }
            changePage(0);
            setFocus(0);
        }
    }

    function getNoDataView() {
        var div = _$("<div/>", {class: 'noItemView'});
        div.append(_$("<img>", {src: modulePath + "resource/image/icon_noresult.png"}));
        div.append(_$("<span>", {class: "text"}).text("최근 구매한 목록이 없습니다"));

        return div;
    }

    function getItemView() {
        // var iconTagPath = '';
        // if(isDvd) {
        //     iconTagPath = "<img src='" + modulePath + "resource/image/icon/icon_tag_possess2.png" + "'>";
        // } else {
        //     if(isHd == 'HD' || isHd == 'UHD') {
        //         iconTagPath = "<img src='" + modulePath + "resource/image/icon/icon_tag_" + isHd.toLowerCase() +".png" + "'>";
        //     }
        // }

        var html = "<tr>" +
            "<td class='checkBox'/>" +
            "<td class='posterImg'>" +
            "<img class='poster'>" +
            "<div class='lock_icon'><img src='" + modulePath + "resource/image/icon/img_vod_locked.png'></div>" +
            "</td>" +
            "<td class='buyInfo'>" +
            "<span class='Title'><span/></span>" +
            "<span class='info'>" +
            "<span class='price'></span>" +
            "<span class='date'></span>" +
            "<div class='expire'>" +
            "<span class='remain'></span>" +
            "<span class='remain_detail'></span>" +
            "</div>" +
            "</span></td>" +
            //"<td class='rightArea'>" +
            //"<span class='remain'></span>"+
            //"</td>" +
            "</tr>";
        return _$(html);
    }

    function setItem(div, title, imgUrl, price, date, remainTime, expireDate, isDvd, isAdult, isViewYn, isHd, prInfo, isChecked) {
        div.find(".posterImg .poster").attr("src", "");
        void div[0].offsetWidth;
        if (isAdult) {
            title = title.substr(0, 1) + "******************************************************".substr(0, title.length);
            imgUrl = modulePath + "resource/image/default_19plus.png";
        } else {
            imgUrl = imgUrl + "?w=87&h=124&quality=90";
        }
        div.toggleClass('checked', isChecked);
        div.find(".posterImg .poster").attr("src", imgUrl);
        div.find(".posterImg .lock_icon").toggle(UTIL.isLimitAge(prInfo));
        div.find(".Title span").text(title);
        div.find(".Title").toggleClass("uhd", isHd == "UHD").toggleClass("hd", isHd == "HD").toggleClass("dvd", isDvd).toggleClass("delete", !isViewYn);

        // if(isDvd) {
        //     div.find(".Title img").attr("src", modulePath + "resource/image/icon/icon_tag_possess2.png").show();
        // } else if(isHd == 'HD' || isHd == 'UHD') {
        //     div.find(".Title img").attr("src", modulePath + "resource/image/icon/icon_tag_" + isHd.toLowerCase() +".png").show();
        // } else {
        //     div.find(".Title img").hide();
        // }

        var info = div.find(".info");
        info.find(".price").html(HTool.addComma(price) + "원 <co>l</co> ");
        info.find(".date").html(parseDateString(date));

        // [si.mun] 18.05 월 형상부터 남은 시간 외에도 만료기간도 표시해주는 것으로 변경되었다.
        // 때문에 getRemainTime() 으로부터 전달받은 object의 needExpireDate가 true일 때, 만료기간도 표시해주도록 수정
        var remainTimeObj = getRemainTime(remainTime);
        info.find(".remain").html(remainTimeObj.remainTime);

        if (remainTimeObj.needExpireDate) {
            info.find(".remain_detail").html(getExpireDate(expireDate));
        } else {
            info.find(".remain_detail").empty();
        }

        //div.find(".info").html(HTool.addComma(price) + "원 <co>l</co> " + parseDateString(date) + " <co>l</co> " + getRemainTime(remainTime));
        //div.find(".remain").html(getRemainTime(remainTime));
    }

    function makeContextMenu() {
        contextMenu = new ContextMenu_editMode();
        contextMenu.addTitle("필터 옵션");
        contextMenu.addDropBox(["전체보기", "소장용 보기"]);
        contextMenu.addSeparateLine();
        contextMenu.addButton("편집 모드", "option_menu_current_mode");
        contextMenu.addButton("전체 선택", "all_checked");
        contextMenu.addButton("결제수단 확인");
        dom.append(contextMenu.getView());
        contextMenu.setEventListener(contextMenuListener);

        //contextMenu = new ContextMenu_editMode();
        //contextMenu.addButton("전체 선택", "all_checked");
        //contextMenu.addButton("삭제");
        //contextMenu.addButton("결제수단 확인");
        //contextMenu.addSeparateLine();
        //contextMenu.addTitle("필터 옵션");
        //contextMenu.addDropBox(["전체보기", "소장용 보기"]);
        //dom.append(contextMenu.getView());
        //contextMenu.setEventListener(contextMenuListener);
    }

    function setFocus(newFocus) {
        removeCurrentFocus();
        focus = newFocus;
        showCurrentFocus();
    }

    function removeCurrentFocus() {
        dom.find(".focus_line").hide();
        dom.find("table tr").eq(focus % pageMaxCnt).removeClass('focus');
    }

    function showCurrentFocus() {
        dom.find("table tr").eq(focus % pageMaxCnt).addClass('focus');
        if (Math.floor(focus / pageMaxCnt) != page) changePage(Math.floor(focus / pageMaxCnt));
        dom.find(".focus_line").css("-webkit-transform", "translateY(" + focus % pageMaxCnt * 170 + "px)");
        dom.find(".focus_line").show();
    }

    function changePage(newPage) {
        page = newPage;
        indicator.setPos(page);

        for (var i = 0; i < pageMaxCnt; i++) {
            var idx = i + (page * pageMaxCnt);

            if (listData[idx])
                setItem(dom.find("table tr").eq(i).removeClass("hiddenItem"), listData[idx].contsName, listData[idx].imgUrl, listData[idx].buyingPrice, listData[idx].buyingDate, listData[idx].duration, listData[idx].expireDate,
                    listData[idx].isDvdYn == "Y", listData[idx].adultOnlyYn == "Y", listData[idx].viewYn == 'Y', listData[idx].isHd, listData[idx].prInfo, checkedList.indexOf(listData[idx]) >= 0);
            else dom.find("table tr").eq(i).addClass("hiddenItem")
        }
    }

    function setToOriginData(removeFocus) {
        if (originData) {
            listData = [].concat(originData);
            hasDvd = false;
            for (var i in listData) if (listData[i].isDvdYn == "Y") {
                hasDvd = true;
                break;
            }
            updateView(instance.parent.viewMgr);
            toggleEditMode(false);
            //contextMenu.setDisable(3, hasDvd);
            contextMenu.setDisable(0, hasDvd);
            contextMenu.clearDropBoxFocus();
            dvdYn = "A";
            deletedItemYn = "Y";
            dom.find("#addedText").text("전체 구매 내역");
            //contextMenu.setDisable(0, true);
            contextMenu.setDisable(2, false);
            contextMenu.setDisable(3, false);

            if (removeFocus) removeCurrentFocus();
        }
    }

    function showEditModeDeletePopup() {
        var items = getSelectedItem();

        if (items.length > 0) {
            LayerManager.activateLayer({
                obj: {
                    id: "EditModeDeletePopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        type: 2,
                        items: items,
                        callback: function (res) {
                            LayerManager.historyBack();
                            if (res) {
                                deleteCheckedItems2(items);
                            }
                        }
                    }
                },
                moduleId: "module.family_home",
                visible: true
            });
        }
    }

    this.show = function () {
        if (!isLoaded) {
            this.update(function () {
                originData = [].concat(listData);
                if (isLoadWating) instance.parent.viewMgr.reChangeFocus(true);
            });
        }
    };

    this.focused = function () {
        isFocused = true;
        isLoadWating = false;
        if (!isLoaded) {
            isLoadWating = true;
            return;
        }
        if (authAdultBox) {
            dom.addClass("focused");
            return myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        } else if (listData.length <= 0) {
            return myHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        } else {
            setFocus(focus);
            //dom.find(".context_menu_indicator").show();
            return myHome.ViewManager.FOCUS_MODE.FOCUS;
        }
    };

    this.blurred = function () {
        isFocused = false;
        dom.removeClass("focused");
        dom.find(".context_menu_indicator").hide();
        dom.find("tr.focus").removeClass("focus");
        setToOriginData(true);
    };


    this.onKeyAction = function (keyCode) {
        if (authAdultBox) return authAdultBox.onKeyAction(keyCode);
        else if (checkHiddenKey(keyCode)) {
            log.printDbg("hiddenKey Activate!!");
            restoreBuyList();
            return true;
        } else if (keyCode >= KEY_CODE.NUM_0 && keyCode <= KEY_CODE.NUM_9) {
            //히든키 동작을 위해 DCA키 block함.
            return true;
        } else if (isEditFocus) {
            if (contextMenu.onKeyAction(keyCode)) {
                return true;
            } else if (keyCode == KEY_CODE.LEFT) {
                // focus : contextMenu -> 콘텐츠로 변경
                toggleEditFocus(false);
                // 적/청색 page show
                indicator.togglePageTransIcon(false);
                return true;
            } else if (keyCode == KEY_CODE.BACK) {
                removeCurrentFocus();
                toggleEditFocus(false);
                instance.parent.viewMgr.historyBack();
                return true;
            } else if (keyCode == KEY_CODE.CONTEXT) {
                //toggleEditFocus(false);
                showEditModeDeletePopup();
                return true;
            }
        } else switch (keyCode) {
            case KEY_CODE.UP:
                setFocus(HTool.getIndex(focus, -1, listData.length));
                return true;
            case KEY_CODE.DOWN:
                setFocus(HTool.getIndex(focus, 1, listData.length));
                return true;
            case KEY_CODE.RED:
                if (focus % pageMaxCnt === 0) setFocus(HTool.getIndex(page, -1, Math.ceil(listData.length / pageMaxCnt)) * pageMaxCnt);
                else setFocus(page * pageMaxCnt);
                return true;
            case KEY_CODE.BLUE:
                if (focus % pageMaxCnt === pageMaxCnt - 1 || focus === listData.length - 1) setFocus(Math.min(listData.length - 1, HTool.getIndex(page, 1, Math.ceil(listData.length / pageMaxCnt)) * pageMaxCnt + pageMaxCnt - 1));
                else setFocus(Math.min(listData.length - 1, page * pageMaxCnt + pageMaxCnt - 1));
                return true;
            case KEY_CODE.ENTER:
                if (editMode) {
                    var clIdx = checkedList.indexOf(listData[focus]);
                    if (clIdx >= 0) checkedList.splice(clIdx, 1);
                    else checkedList.push(listData[focus]);
                    dom.find("table tr").eq(focus % pageMaxCnt).toggleClass('checked', clIdx < 0);
                    checkItemImpl();
                } else {
                    selectItem(listData[focus]);
                }
                return true;
            case KEY_CODE.LEFT:
                removeCurrentFocus();
                toggleEditMode(false);
                instance.parent.viewMgr.historyBack();
                return true;
            case KEY_CODE.RIGHT:
                toggleEditFocus(true);
                // 적/청색 page hide
                indicator.togglePageTransIcon(true);
                return true;
            case KEY_CODE.CONTEXT:
                if (deletedItemYn == "Y") {
                    // [WEBIIIHOME-3838] 복원된 리스트에서는 제공하지 않음
                    showEditModeDeletePopup();
                }
                return true;
            case KEY_CODE.BACK:
                if (editMode) {
                    toggleEditMode(false);
                    return false;
                }
                setFocus(0);
                return false;
        }
    };

    function selectItem(data) {
        if (data.adultOnlyYn == "Y") {
            openAdultAuthPopup(function () {
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.catId, data.contsId, "03");
                } catch (e) {
                }
                openDetailLayerImpl(data, true);
            }, true);
        } else {
            try {
                NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.catId, data.contsId, "03");
            } catch (e) {
            }

            if (data.pkgYn == "C") { // VOD 기간정액 카테고리 이동
                jumpToCategory(data);
            }
            else {
                openDetailLayerImpl(data, false);
            }
        }
    }

    var jumpToCategory = function (data) {
        if (data.catId == null || data.catId.length < 2) {
            // 카테고리 ID가 없는 경우 getCategoryId에 기간정액 상품 코드를 전달하여 카테고리ID를 얻어온다
            myHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=B&platformGubun=W", function (res, newData) {
                if (res) {
                    data.catId = newData.catId;
                    var menuData = MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(),
                        allSearch: false,
                        cbCondition: function (menu) {
                            if (menu.id == data.catId) return true;
                        }
                    })[0];
                    if (menuData != undefined) {
                        MenuServiceManager.jumpMenu({menu: menuData});
                    } else {
                        showToast("잘못된 접근입니다");
                    }
                } else {
                    HTool.openErrorPopup({message: ERROR_TEXT.ET004, reboot: false});
                }
            });
        } else {
            var menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) {
                    if (menu.id == data.catId) return true;
                }
            })[0];
            if (menuData != undefined) {
                MenuServiceManager.jumpMenu({menu: menuData});
            } else {
                showToast("잘못된 접근입니다");
            }
        }
    }

    var openDetailLayerImpl = function (data, authComplete) {
        LayerManager.startLoading({preventKey: true});
        if (data.pkgYn == "K") {
            ModuleManager.getModuleForced("module.vod", function (module) {
                module.execute({method: "showDetailForPkgProdCd", params: {prdtCd: data.contsId, req_cd: "03"}});
            });
        } else if (data.pkgYn == "Y" || data.seriesYn == "Y") {
            if (data.catId == "N") {
                myHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=B&platformGubun=W", function (res, data2) {
                    if (res) {
                        if (data2.serviceYn == "Y" || isTimeRemain(data.duration)) openDetailLayer(data2.catId, data2.catId, "03", {isAuthorizedContent: authComplete});
                        else {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                        }
                    } else {
                        LayerManager.stopLoading();
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({
                            message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                            reboot: true
                        });
                    }
                });
            } else if (data.pkgYn == "Y") {
                openDetailLayer(data.contsId, data.contsId, "03", {isAuthorizedContent: authComplete});
            } else {
                openDetailLayer(data.catId, data.contsId, "03", {isAuthorizedContent: authComplete});
            }
        } else {
            myHome.amocManager.getContentW3(function (res, contData) {
                if (res) {
                    if (contData.contsId) openDetailLayer(contData.catId, data.contsId, "03", {
                        contsInfo: contData,
                        isAuthorizedContent: authComplete,
                        noCateForced: true
                    });
                    else if (data.catId && data.catId !== "N") data.catId = "N", openDetailLayerImpl(data, authComplete);
                    else if (isTimeRemain(data.duration)) openDetailLayer("X", data.contsId, "03", {
                        isAuthorizedContent: authComplete,
                        noCateForced: true
                    });
                    else {
                        LayerManager.stopLoading();
                        HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                    }
                } else {
                    LayerManager.stopLoading();
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                }
            }, data.catId || "N", data.contsId, DEF.SAID);
        }
    };

    function contextMenuListener(index, data) {
        switch (index) {
            case 0: // 필터 옵션
                if (data == 0) {
                    dvdYn = "A";
                    dom.find("#addedText").text("전체 구매 내역");
                } else if (data == 1) {
                    dvdYn = "Y";
                    dom.find("#addedText").text("소장용 구매 내역");
                }
                contextMenu.removeFocus();
                instance.update();
                break;
            case 1: // 편집 모드 ON/OFF
                if (editMode) {
                    // 편집 모드 OFF
                    toggleEditMode(false);
                    // 전체 선택 disable
                    contextMenu.setDisable(2, false);
                } else {
                    // 편집모드 ON
                    toggleEditMode(true);
                    // 전체 선택 disable 해제
                    contextMenu.setDisable(2, true);
                }
                break;
            case 2: // 전체 선택/해제
                if (checkedList.length == listData.length) {
                    dom.find("table tr").removeClass('checked');
                    checkedList = [];
                } else {
                    dom.find("table tr").addClass('checked');
                    checkedList = listData ? [].concat(listData) : [];
                }
                checkItemImpl();
                break;
            case 3:
                LayerManager.activateLayer({
                    obj: {
                        id: "PaymentMethodPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        params: getSelectedItem()[0]
                    },
                    moduleId: "module.family_home",
                    visible: true
                });
                break;
        }


        //switch (index) {
        //    case 0:
        //        if(checkedList.length==listData.length) {
        //            dom.find("table tr").removeClass('checked');
        //            checkedList = [];
        //        } else {
        //            dom.find("table tr").addClass('checked');
        //            checkedList = listData?[].concat(listData):[];
        //        }checkItemImpl();
        //        break;
        //    case 1:
        //        var items = getSelectedItem();
        //        LayerManager.activateLayer({
        //            obj: {
        //                id: "EditModeDeletePopup",
        //                type: Layer.TYPE.POPUP,
        //                priority: Layer.PRIORITY.POPUP,
        //                linkage: false,
        //                params: {
        //                    type: 2,
        //                    items: items,
        //                    callback: function (res) {
        //                        LayerManager.historyBack();
        //                        if (res) deleteCheckedItems2(items);
        //
        //                    }
        //                }
        //            },
        //            moduleId: "module.family_home",
        //            visible: true
        //        });
        //        break;
        //    case 2:
        //        LayerManager.activateLayer({
        //            obj: {
        //                id: "PaymentMethodPopup",
        //                type: Layer.TYPE.POPUP,
        //                priority: Layer.PRIORITY.POPUP,
        //                params : getSelectedItem()[0]
        //            },
        //            moduleId:"module.family_home",
        //            visible: true
        //        });
        //        break;
        //    case 3:
        //        if(data==0) {
        //            dvdYn = "A";
        //            dom.find("#addedText").text("전체 구매 내역");
        //        } else if(data==1) {
        //            dvdYn = "Y";
        //            dom.find("#addedText").text("소장용 구매 내역");
        //        }
        //        instance.update();
        //        break;
        //}
    }

    var itemDeleteCount = 0;
    var itemDeleteLength = 0;
    var itemDeleteList = [];
    var deleteAlldata = false;

    function deleteCheckedItems2(items) {
        itemDeleteList = items;
        itemDeleteLength = items.length;

        if (listData.length == itemDeleteLength)
            deleteAlldata = true;

        LayerManager.startLoading();
        // 2017.08.07 Lazuli
        // itemDeleteCount를 초기화해주지 않아, 2개 삭제 후 1개 삭제 시도 시
        // itemDeleteCount 값이 1로 들어가면서 스크립트 오류 발생
        // 초기화 로직 추가
        itemDeleteCount = 0;

        deleteItemList(itemDeleteList);
    }

    function deleteItemList(deleteList, callback) {
        var buyIdList = "";
        var buyingDateList = "";
        var expireDateList = "";

        var deletedLength = 0;
        for (var i = 0; i < 5 && i < deleteList.length; i++) {
            buyIdList += ((i > 0 ? "|" : "") + deleteList[i].buyId);
            buyingDateList += ((i > 0 ? "|" : "") + deleteList[i].buyingDate);
            expireDateList += ((i > 0 ? "|" : "") + deleteList[i].expireDate);
            deletedLength++;
        }

        var reqData = {
            buyId: buyIdList,
            buyingDate: buyingDateList,
            expireDate: expireDateList
        };

        callDeleteItem(reqData, callback_listener);
    }

    function openErrprPopUp() {
        var contentHtml;
        if (itemDeleteCount > 0) {
            // showToast("일부콘텐츠를 삭제하지 못했습니다");
            contentHtml = "일부 콘텐츠를 삭제하지 못했습니다<br>잠시 후 다시 시도해 주세요";
        } else {
            // showToast("삭제하지 못했습니다");
            contentHtml = "삭제하지 못했습니다<br>잠시 후 다시 시도해 주세요";
        }
        updateView(instance.parent.viewMgr);
        hasDvd = false;
        for (var i in listData) if (listData[i].isDvdYn == "Y") {
            hasDvd = true;
            break;
        }
        toggleEditMode(false);
        //contextMenu.setDisable(3, hasDvd);
        contextMenu.setDisable(0, hasDvd);
        LayerManager.stopLoading();
        LayerManager.activateLayer({
            obj: {
                id: "SingleTextPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: false,
                params: {
                    title: "알림",
                    content: contentHtml,
                    btnText: "확인"
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    }

    function callback_listener(res, data, _deleteIdList) {
        window.log.printDbg("[buyList] callback_listener res = " + res);
        if (res) {
            var deleteIdList = _deleteIdList.split("|");
            removeItemFromList(deleteIdList);

            if (data.flag == '0') {
                itemDeleteCount += deleteIdList.length;
                if (itemDeleteCount < itemDeleteLength) {
                    var buyIdList = "";
                    var buyingDateList = "";
                    var expireDateList = "";

                    var deletedLength = 0;
                    for (var i = itemDeleteCount; i < (itemDeleteCount + 5) && i < itemDeleteList.length; i++) {
                        buyIdList += ((i > itemDeleteCount ? "|" : "") + itemDeleteList[i].buyId);
                        buyingDateList += ((i > itemDeleteCount ? "|" : "") + itemDeleteList[i].buyingDate);
                        expireDateList += ((i > itemDeleteCount ? "|" : "") + itemDeleteList[i].expireDate);
                        deletedLength++;
                    }

                    var reqData = {
                        buyId: buyIdList,
                        buyingDate: buyingDateList,
                        expireDate: expireDateList
                    };

                    callDeleteItem(reqData, callback_listener);
                } else {//if(itemDeleteCount == itemDeleteLength) {
                    LayerManager.stopLoading();
                    showToast(data.message);
                    updateView(instance.parent.viewMgr);
                    hasDvd = false;
                    for (var i in listData) if (listData[i].isDvdYn == "Y") {
                        hasDvd = true;
                        break;
                    }
                    toggleEditMode(false);
                    toggleEditFocus(false);
                    //contextMenu.setDisable(3, hasDvd);
                    contextMenu.setDisable(0, hasDvd);
                    contextMenu.setDisable(2, false);
                    contextMenu.setDisable(3, false);
                    if (deleteAlldata) {
                        deleteAlldata = false;
                        instance.parent.viewMgr.historyBack();
                    }
                }
            } else {
                openErrprPopUp();
            }
        } else {
            openErrprPopUp();
        }
    }

    function callDeleteItem(reqData, callback) {
        myHome.amocManager.setDelBuy("saId=" + DEF.SAID + "&buyId=" + reqData.buyId + "&buyingDate=" + reqData.buyingDate + "&expireDate=" + reqData.expireDate, function (res, data) {
//            if(res) {
//                listData.splice(getIndex(listData, item), 1);
//                originData.splice(getIndex(originData, item), 1);
//            }
            callback.apply(null, [res, data, reqData.buyId]);
        });
    }

    function removeItemFromList(_buyIdList) {
        for (var i in _buyIdList) {
            listData.splice(getIndex(listData, _buyIdList[i]), 1);
            originData.splice(getIndex(originData, _buyIdList[i]), 1);
        }
    }

    function getIndex(list, buyId) {
        for (var i = 0; i < list.length; i++) if (list[i].buyId === buyId) return i;
        return -1;
    }

    function getSelectedItem() {
        return checkedList;
    }

    function toggleEditMode(mode) {
        if (mode == null) mode = !editMode;
        dom.toggleClass("editMode", mode);
        editMode = dom.hasClass("editMode");
        //isEditFocus = mode;
        if (mode) {
            //contextMenu.open();
            //contextMenu.setDisable(2, checkedList.length==1);
            // 결제수단 확인은 하나의 항목만 체크가 되어야만 활성화 됨
            contextMenu.setDisable(3, checkedList.length == 1);
            if (deletedItemYn == 'Y') {
                //contextMenu.setDisable(1, checkedList.length>0);
            }

            dom.find(".option_menu_current_mode").text("편집 모드 종료");
            //dom.find(".context_menu_indicator").text("편집 모드 종료");
        } else {
            checkedList = [];
            dom.find("table tr").removeClass('checked');
            dom.find(".option_menu_current_mode").text("편집 모드");
            dom.find(".all_checked").text("전체 선택");
            //contextMenu.close();
            //dom.find(".context_menu_indicator").text("편집 모드");
        }

        // 편집모드를 toggle 하는 것은 contextMenu 에 focus가 위치할 때만 가능하므로 항상 hide
        dom.find(".context_menu_indicator").hide();
        //toggleEditFocus(false);
    }


    function toggleEditFocus(_isEditFocus) {
        isEditFocus = _isEditFocus;
        if (isEditFocus) {

            if (checkedList.length > 0) {
                dom.find(".context_menu_indicator").show();
            } else {
                //if(deletedItemYn != "Y") {
                //    contextMenu.addFocus(2);
                //} else {
                //    contextMenu.addFocus(0);
                //}

                dom.find(".context_menu_indicator").hide();
            }
            contextMenu.addFocus(0);
            removeCurrentFocus();
        } else {
            contextMenu.removeFocus();
            showCurrentFocus();

            // 콘텐츠로 focus가 이동 될 때, checkList 유무에 따라 목록에서 삭제 키 노출여부 결정
            if (checkedList.length > 0) {
                dom.find(".context_menu_indicator").show();
            } else {
                dom.find(".context_menu_indicator").hide();
            }

            // setFocus(focus);
        }
    }

    function checkItemImpl() {
        if (checkedList.length == listData.length) { // 모두 선택 된 경우
            dom.find(".all_checked").text("전체 선택 해제");
            dom.find(".context_menu_indicator").show();
        } else if (checkedList.length > 0) { // 일부만 선택 된 경우
            dom.find(".all_checked").text("전체 선택");
            dom.find(".context_menu_indicator").show();
        } else { // 1개도 선택되지 않은 경우
            dom.find(".all_checked").text("전체 선택");
            dom.find(".context_menu_indicator").hide();
        }
        //contextMenu.setDisable(2, checkedList.length==1);
        // 결제 수단 확인은 1개의 항목이 선택된 경우에만 활성화가 됨
        contextMenu.setDisable(3, checkedList.length == 1);

        // contextMenu.setDisable(1, dom.find("table tr.checked").length>0);
        if (deletedItemYn == 'A') {
            // [WEBIIIHOME-3838] 복원된 리스트에서는 제공하지 않음
            dom.find(".context_menu_indicator").hide();
            // contextMenu.setDisable(1, checkedList.length>0);
        }
    }

    function restoreBuyList() {
        deletedItemYn = "A";
        //contextMenu.setDisable(0, false);
        //contextMenu.setDisable(1, false);
        //contextMenu.setDisable(2, false);
        contextMenu.setDisable(2, false);
        //contextMenu.setDisable(1, false);
        contextMenu.setDisable(3, false);

        instance.update();
    }

    var hiddenKeyList = "";

    function checkHiddenKey(key_code) {
        if (key_code == KEY_CODE.ENTER) {
            if (hiddenKeyList === "14789632") return true;
        } else if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9) {
            hiddenKeyList += (key_code - KEY_CODE.NUM_0) + "";
            if (hiddenKeyList.length > 8) hiddenKeyList = hiddenKeyList.substr(1, 8);
        } else {
            hiddenKeyList = "";
        }
        return false;
    }

    function isTimeRemain(str) {
        if (str == "PERM") return true;
        if (str.indexOf("-") >= 0) return false;
        return true;
    }

    function getRemainTime(str) {
        if (str == "PERM") {
            return {
                remainTime: "해지 전까지",
                needExpireDate: false,
            };
        }

        var a = str.split('D'), b, c;

        if (str.indexOf("-") >= 0) {
            return {
                remainTime: "시청기간 만료",
                needExpireDate: false,
            };
        } else if (a[0] >= 365) {
            return {
                remainTime: "해지 전까지",
                needExpireDate: false,
            };
        } else if (a[0] > 1) {
            return {
                remainTime: a[0] + "일 남음",
                needExpireDate: true,
            };
        } else if (b = a[1].split('H'), a[0] == 1 || b[0] > 0) {
            return {
                remainTime: ((a[0] * 24) + (b[0] - 0)) + "시간 남음",
                needExpireDate: true,
            }
        } else {
            c = b[1].split('M');
            if (c[0] == 0) { // duration 이 59초 이하일 땐 0D0H0M 으로 내려오는 데, 이런 경우는 1분으로 처리
                return {
                    remainTime: "1분 남음",
                    needExpireDate: true,
                }
            } else {
                return {
                    remainTime: c[0] + "분 남음",
                    needExpireDate: true,
                };
            }
        }
    }

    function getExpireDate(expireDate) {
        var month = expireDate.substring(4, 6) + "월";
        var day = " " + expireDate.substring(6, 8) + "일";
        var hour = " " + expireDate.substring(8, 10) + ":";
        var minute = expireDate.substring(10, 12) + "까지";

        return (" (" + month + day + hour + minute + ")");
    }

    function parseDateString(str) {
        return str.substr(0, 4) + "." + str.substr(4, 2) + "." + str.substr(6, 2);
    }
};