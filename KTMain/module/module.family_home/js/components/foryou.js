/**
 * 당신을 위한 VOD 추천
 * (우리집 맞춤 TV > 당신을 위한 VOD 추천)
 * Created by ksk91_000 on 2016-07-06.
 */
if(!component) var component = {};

component.foryou = function(menuId, _menuData) {
    var componentId = menuId;
    var listData = [];

    var div = _$("<div/>", {id: menuId});

    var focus;
    var type = 0; // 0: 리스트, 1: 데이터 없음.
    var length;
    var isMore;
    var itemCnt;
    var limitAge;
    var isFocused = false;

    var maxPosterCnt = 7;

    this.update = function () {
        div.find(".componentTitle").html("<div/>" + HTool.getMenuName(_menuData));
        var newAge = AuthManager.getPR();
        var that = this;
        CurationManager.getFirstList(function (data) {
            if(listData!==data || limitAge != newAge) {
                limitAge = newAge;
                if(data) {
                    type = 0;
                    listData = data;
                    length = listData.length;
                    isMore = length > maxPosterCnt - 1;
                    itemCnt = isMore ? maxPosterCnt : length + 1;
                    constructDom();
                    that.onKeyAction = that.onKeyForList;
                    if(isFocused) focusing(focus);
                } else {
                    type = 1;
                    constructDom();
                    that.onKeyAction = that.onKeyForInfoTxt;
                    if(isFocused) div.find("#" + componentId).addClass("focus");
                }
            }
        });
    };

    this.init = function() {
        var that = this;
        type = 0;
        limitAge = AuthManager.getPR();
        createNoDataDom();

        CurationManager.getFirstList(function (data) {
            if(data) {
                type = 0;
                listData = data;
                length = listData.length;
                isMore = length > maxPosterCnt - 1;
                itemCnt = isMore ? maxPosterCnt : length + 1;
                constructDom();
                that.onKeyAction = that.onKeyForList;
                if(isFocused) focusing(focus);
            } else {
                type = 1;
                constructDom();
                that.onKeyAction = that.onKeyForInfoTxt;
                if(isFocused) div.find("#" + componentId).addClass("focus");
            }
        });
    };

    var constructDom = function () {
        div.empty();
        div.append(_$("<span/>", {class: "componentTitle"}).html("<div></div>당신을 위한 VOD 추천"));
        if (type==0) {
            var list = _$("<ul/>", {id: componentId + "_contents", class: "sub_posterList"});
            div.append(list);

            list.append(_$("<li id='my_liking' class='content poster_vertical'>"+
            "<div class='content'>"+
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            // "<img src='" + modulePath + "resource/image/contentsList/poster_sdw_w210.png' class='posterSdw'>"+
            "<img src='" + modulePath + "resource/image/main/my_liking.png' class='posterImg poster'>" +
            "</div></li>"));

            for (var i = 1; i < length && i < (isMore ? maxPosterCnt - 1 : maxPosterCnt); i++) {
                list.append(getVerticalPoster(componentId + "_" + i, listData[i-1]));
            } if (isMore) {
                // TaeSang. listData[3]에서 listData[5]로 수정함 (5번째 자리에 놓여질 포스터가 블러처리되어 보여져야 됨)
                list.append(PosterFactory.getVerticalPoster_more(componentId + "_" + (maxPosterCnt - 1), {imgUrl:listData[5].IMG_URL, moreCnt:length}));
            }
        } else {
            createNoDataDom();
        }
    };

    function createNoDataDom() {
        div.append(_$("<div/>", {id: componentId, class: "infoText_foryou"}));
        div.find("#" + componentId).append(_$("<img>", {src: modulePath + 'resource/image/img_myhome_no_liking.png'}));
        div.find("#" + componentId).append(_$("<span>", {class: 'text_1'}).text("평가한 콘텐츠가 없습니다"));
        div.find("#" + componentId).append(_$("<span>", {class: 'text_2'}).html("별점을 평가하고 내 취향을 알아보세요<br>당신을 위한 VOD를 추천해 드립니다"));
        div.find("#" + componentId).append(_$("<div/>", {class: 'dim'}).text("나의 취향 알아보기"));
    }

    var getVerticalPoster = function(id, data) {
        var element = _$("<li id='" + id + "' class='content poster_vertical'>"+
            "<div class='content'>"+
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>"+
            "<img src='" + data.IMG_URL + "?w=196&h=280&quality=90' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/image/default_poster.png\"'>"+
            "<div class='previewSdw'/>"+
            "<div class='icon_area'/>"+
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span>" + data.ITEM_NAME + "</span></div>"+
            "<span class='posterDetail'>"+
            "<span class='stars'/>"+
            (HTool.isTrue(data.WON_YN) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.WON_YN) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/image/icon/icon_age_list_" + (data.RATING-0||"all") + ".png' class='age'>"+
            "</span>"+
            "</div>"+
            "</li>");

        element.find(".stars").append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(element.find(".stars"), data.OLLEH_RATING);
        setIcon(element.find(".icon_area"), data);
        return element;
    };

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.NEW_HOT) {
            case "NEW": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
        } if(data.HDR_YN=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.IS_HD == "UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.SMART_DVD_YN=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(UTIL.isLimitAge(data.RATING)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.getView = function () {
        return div;
    };

    this.isLastFocused = function () { return focus === itemCnt-1; };
    this.isFirstFocused = function () { return focus === 0; }

    this.onFocused = function (keyCode) {
        isFocused = true;
        if(type==0) {
            if(keyCode===KEY_CODE.BLUE) focus = itemCnt-1;
            else focus = 0;
            focusing(focus);
        } else {
            div.find("#" + componentId).addClass("focus");
        }
    };

    this.onBlured = function () {
        isFocused = false;
        if(type==0) {
            unfocusing(focus);
        } else {
            div.find("#" + componentId).removeClass("focus");
        } div.find(".componentTitle").css("-webkit-transform", "");
    };

    var focusing = function (index) {
        div.find("ul.sub_posterList li").eq(index).addClass("focus");
        if(index==0) {
            div.find("#my_liking .posterImg").attr('src', modulePath + "resource/image/main/my_liking_f.png");
            _$("#MyHomeMain .context_menu_indicator").text("");
        } else if(isMore && index==maxPosterCnt - 1){
            _$("#MyHomeMain .context_menu_indicator").text("");
        } else {
            /**
             * [dj.son] [WEBIIIHOME-3659] 우리집TV> 당신을 위한 VOD추천> 우측에 VOD 컨텐츠로 포커스 이동한경우> 연관메뉴명 변경 필요 (연관메뉴 => 찜 | 플레이리스트 추가)
             */
            _$("#MyHomeMain .context_menu_indicator").html("찜<bar/>플레이리스트 추가");
            //_$("#MyHomeMain .context_menu_indicator").html("연관 메뉴");
        }

        if(index==0||index==1) div.find(".componentTitle").css("-webkit-transform", "translateY(-13px)");
        else div.find(".componentTitle").css("-webkit-transform", "");
        setTextAnimation(index);
    };

    var unfocusing = function (index) {
        div.find("ul.sub_posterList li").eq(index).removeClass("focus");
        if(index==0) div.find("#my_liking .posterImg").attr('src', modulePath + "resource/image/main/my_liking.png");
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
    };

    function setTextAnimation(focus) {
        UTIL.clearAnimation(div.find(".textAnimating.posterTitle span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        if(listData[focus-1] == undefined) return ;

        if(UTIL.getTextLength(listData[focus-1].ITEM_NAME, "RixHead L", 30)>201) {
            var posterDiv = div.find("#" + componentId + "_" + focus + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.onKeyForList = function (keyCode) {
        if(this.parent.contextMenu.isOpen()) return this.parent.contextMenu.onKeyAction(keyCode);
        switch (keyCode) {
            case KEY_CODE.LEFT:
                if(focus>0) {
                    unfocusing(focus);
                    focus = HTool.getIndex(focus, -1, Math.min(maxPosterCnt, length));
                    focusing(focus);
                    return true;
                }else return false;
            case KEY_CODE.RIGHT:
                if(focus<itemCnt-1) {
                    unfocusing(focus);
                    focus = HTool.getIndex(focus, 1, Math.min(maxPosterCnt, length));
                    focusing(focus);
                    return true;
                }else return false;
            case KEY_CODE.ENTER:
                if(focus==0) {
                    try { // 나의 취향 알아보기
                        NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU07004.id, NLC.MENU07004.name, "", "");
                    } catch(e) {}
                    openMorePage(0);
                }
                else if(focus==maxPosterCnt - 1 && isMore) {
                    try { // 젠체보기는 메뉴 ID가 없어 상위메뉴인 당신을 위한 VOD 추천 메뉴 ID로 임시매핑
                        NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU07003.id, "전체보기", "", "");
                    } catch(e) {}
                    openMorePage(1);
                }
                else {
                    goToDetailLayer(listData[focus-1]);
                }
                return true;
            case KEY_CODE.CONTEXT:
                if(!(focus==0 || (isMore && focus==maxPosterCnt - 1))) {
                    this.parent.contextMenu.setVodInfo({
                        itemName: listData[focus-1].ITEM_NAME,
                        imgUrl: listData[focus-1].IMG_URL,
                        contsId: listData[focus-1].ITEM_ID,
                        catId: listData[focus-1].PARENT_CAT_ID,
                        itemType: listData[focus-1].ITEM_TYPE,
                        cmbYn: listData[focus-1].CMB_YN,
                        resolCd: listData[focus-1].IS_HD
                    });
                    this.parent.contextMenu.open();
                } return true;
        } return false;
    };

    function goToDetailLayer(data) {
        if(data.ITEM_TYPE == 1 || data.ITEM_TYPE == 0) {
            try {
                NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU07003.id, NLC.MENU07003.name, data.ITEM_ID, data.ITEM_NAME);
            } catch(e) {}
            openDetailLayer(data.ITEM_ID, "", "33");
        }else if(data.ITEM_TYPE == 2 || data.ITEM_TYPE == 4) {
            try {
                NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU07003.id, NLC.MENU07003.name, data.ITEM_ID, data.ITEM_NAME);
            } catch(e) {}
            openDetailLayer(data.PARENT_CAT_ID, data.ITEM_ID, "33");
        }
    }

    this.onKeyForInfoTxt = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.ENTER:
                try {
                    NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU07004.id, NLC.MENU07004.name, "", "");
                } catch(e) {}
                openLikingPopup();
                return true;
        }
    };

    function openLikingPopup() {
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeRatingPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {}
            },
            moduleId: "module.family_home",
            new: true,
            visible: true
        });
    }

    function openMorePage(focusIdx) {
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeForYou",
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                linkage: false,
                params: {
                    menuName: HTool.getMenuName(_menuData),
                    focusIdx: focusIdx
                }
            },
            moduleId: "module.family_home",
            new: true,
            visible: true
        })
    }

    this.onKeyAction = function (keyCode) {

    };

    this.getHeight = function () {
        return myHome.CONSTANT.COMP_TITLE_HEIGHT + myHome.CONSTANT.POSTER_HEIGHT - 50;
        //if (length > 0) return COMP_TITLE_HEIGHT + 431;
        //else return COMP_TITLE_HEIGHT + 435;
    };

    this.init();
};