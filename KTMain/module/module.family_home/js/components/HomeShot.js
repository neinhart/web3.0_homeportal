/**
 * Created by ksk91_000 on 2017-03-24.
 */
if(!component) var component = {};
component.HomeShot = function() {
    var div = _$("<div/>", {class: "homeShot_area"});
    var type;
    var targetId;
    var locator;
    var locator2;
    var casLocator;
    var parameter;

    div.append("" +
        "<div class='homeShot'>" +
        "<img class='home_shot'>" +
        "<div class='shortcut'>바로보기</div>" +
        "</div>" +
        "<div class='default_homeShot'><img src='" + COMMON_IMAGE.HOMESHOT_DEFAULT + "'></div>");
    div.append("<div class='homeShot_bar'></div>");

    var setHomeShot = function (_type, _targetId, _imgUrl, _locator1, _locator2, _casLocator, _parameter) {
        if(_type==null || _type==""){
            type = null;
            div.find(".homeShot").hide();
            div.find(".default_homeShot").show();
            div.find(".homeShot_bar").hide();
        } else {
            type = _type;
            targetId = _targetId;
            locator = _locator1;
            locator2 = _locator2;
            casLocator = _casLocator;
            parameter = _parameter;

            div.find(".homeShot_bar").show();
            div.find(".homeShot img").error(function () {
                div.find(".homeShot_bar").hide();
            }).attr("src", _imgUrl);
            div.find(".homeShot").show();
            div.find(".default_homeShot").hide();
        }
    };

    var getView = function () {
        return div;
    };

    var execute = function () {
        if(type == null) return false;

        switch(type-0) {
            case 0:  //카테고리
                var MenuDataManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.MENU_DATA_MANAGER);
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) {
                        if (menu.id == targetId) return true;
                    }
                })[0];
                if (!!menuData) {
                    MenuServiceManager.jumpMenu({menu: menuData});
                    // 카테고리 홈샷 로그 적재 추가
                    try {
                        NavLogMgr.collectJump(NLC.JUMP_START_CATE, NLC.JUMP_DEST_CATEGORY, menuData.id, "", "", "");
                    } catch(e) {}

                }
                else showToast("잘못된 접근입니다");
                return true;
            case 1:  // 시리즈
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null,targetId, "25");
                } catch(e) {}
                openDetailLayer(targetId, null, "25");
                return true;
            case 2:  // 단편
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, targetId, "25");
                } catch(e) {}
                openDetailLayer(null, targetId, "25");
                return true;
            case 3:     //멀티캐스트
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_CATE, NLC.JUMP_DEST_INTERACTIVE,
                        "", "", "", options.locator);
                } catch(e) {}

                var nextState;
                if(locator == CONSTANT.APP_ID.MASHUP)
                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                else
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                AppServiceManager.changeService({
                    nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: locator,
                        ex_param: parameter
                    }
                });
                return true;
            case 7:    //유니캐스트 양방향 서비스
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_CATE, NLC.JUMP_DEST_INTERACTIVE,
                        "", "", "", options.locator);
                } catch(e) {}

                var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: targetId
                    }
                });
                return true;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(locator);
                return true;
        } return false;
    };

    return {
        getView: getView,
        setHomeShot: setHomeShot,
        execute: execute
    }
};