/**
 * Created by ksk91_000 on 2016-12-07.
 */
if(!window.component) window.component = {};
component.VerticalPosterList = function() {
    ContentsList.apply(this, arguments);
    var dataList;
    var itemCnt;
    var totalPage;
    var focus = 0, page = 0, dpPage = -1;
    var isFocused = false;
    var lineVisibleTimeout;
    var isAdultCate;
    var isChangedPage = false;
    var ageLimit;
    var starType;
    var reqPathCd = "33";
    const LINE_AMOUNT = 5;

    this.create = function (star, _reqPathCd) {
        starType = star||stars.TYPE.RED_20;
        reqPathCd = _reqPathCd||reqPathCd;
        this.div = _$("<ul/>", {class:"contentsList vertical_poster_list"});
        for(var i=0; i<LINE_AMOUNT*5; i++)
            this.div.append(getPosterDiv(Math.floor(i/LINE_AMOUNT)));
    };

    function getPosterDiv(lineNum) {
        var poster = _$("<li class='content poster_vertical line_" + lineNum + "'>"+
            "<div class='content'>"+
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>"+
            "<img class='posterImg poster'>"+   // 포스터 이미지
            "<div class='contentsData'>"+
            "<div class='posterTitle'><span/></div>"+ // 제목
            "<span class='posterDetail'>"+
            "<span class='isfree'></span>"+ //유.무료
            "<img class='isfreeIcon'>"+ //유.무료
            "<img class='age'>"+    //연령
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>"+
            "<div class='chart_area'/>"+
            "<div class='over_dim'/>"+
            "</div>"+
            "</li>");

        poster.find(".posterDetail").prepend(stars.getView(starType));
        return poster;
    }

    function addChartOrder(div, data) {
        div.empty();
        if(data.chartOrder) {
            div.append(_$("<div/>", {class: "chart_icon"}).text(data.chartOrder));
            div.append(_$("<div/>", {class: "chart_change_area"}));
            var chart_area = div.find(".chart_change_area");
            switch (data.change) {
                case "N" :
                    chart_area.append(_$("<div/>", {class: "chart_change_icon new"}));
                    break;
                case "0" :
                    chart_area.append(_$("<div/>", {class: "chart_change_icon still"}));
                    break;
                default :
                    var sign = data.change.substr(0, 1);
                    var tmpClass = "";
                    if (sign == "-") tmpClass = "minus"; else if (sign == "+") tmpClass = "plus";
                    chart_area.append(_$("<div/>", {class: "chart_change_icon " + tmpClass}).text(data.change.substr(1, data.change.length - 1)));
                    break;
            }
        }
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.NEW_HOT) {
            case "NEW": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
        } if(data.HDR_YN=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.IS_HD == "UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.SMART_DVD_YN=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(UTIL.isLimitAge(data.RATING)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }
    
    function isLimitAge(a) {
        var b, c = !1;
        return b = ageLimit, "all" != UTIL.transPrInfo(a) && 0 != b && Number(UTIL.transPrInfo(a)) >= Number(b) && (c = !0), c
    }

    this.setData = function(_dataList, page) {
        console.log("setData!!!!!");
        var contList = this.div.find("li");
        if(_dataList) {
            dataList = _dataList;
            itemCnt = _dataList.length;
            totalPage = Math.ceil(itemCnt/(LINE_AMOUNT*2));
            ageLimit = AuthManager.getPR();
            this.indicator.setSize(totalPage, page);
            if(isFocused) this.setFocus(focus);
        } else if(page==dpPage) return;

        dpPage = page;
        if(totalPage>1) {
            //top Page
            tmp = page;
            for (var i = 0; i < LINE_AMOUNT*2; i++) setItem(contList.eq(i), dataList[tmp * LINE_AMOUNT*2 + i], tmp * LINE_AMOUNT*2 + i);
            //2rd Page
            var tmp = HTool.getIndex(page, 1, totalPage);
            for (var i = 0; i < LINE_AMOUNT*2; i++) setItem(contList.eq(i + LINE_AMOUNT*2), dataList[tmp * LINE_AMOUNT*2 + i], tmp * LINE_AMOUNT*2 + i);
            //bottom Page
            var tmp = HTool.getIndex(page, 2, totalPage);
            for (var i = 0; i < LINE_AMOUNT; i++) setItem(contList.eq(i + LINE_AMOUNT*4), dataList[tmp * LINE_AMOUNT*2 + i], tmp * LINE_AMOUNT*2 + i);
            this.div.removeClass("singlePage");
        } else {
            for (var i = 0; i < LINE_AMOUNT*5; i++) setItem(contList.eq(i), dataList[i], i);
            this.div.addClass("singlePage");
        }
    };

    function setItem(div, data, index) {
        if (data) {
            div.removeClass("hidden_cont");
            div.attr("id", getDataListId(index));
            div.find(".posterImg.poster").attr("src", "");
            void div[0].offsetWidth;

            div.find(".posterImg.poster").attr({src: data.IMG_URL + "?w=210&h=300&quality=90", onerror: "this.src='" + modulePath + "resource/image/default_poster.png'"});
            div.find(".posterTitle span").text(data.ITEM_NAME);
            if (HTool.isTrue(data.WON_YN)) {
                div.find("img.isfreeIcon").show();
                div.find("span.isfree").hide();
            } else if (HTool.isFalse(data.WON_YN)) {
                div.find("span.isfree").show();
                div.find("span.isfree").text("무료");
                div.find("img.isfreeIcon").hide();
            } else {
                div.find("img.isfreeIcon").hide();
                div.find("span.isfree").hide();
            }

            if(data.CONT_RATING == undefined) {
                div.find("img.age").attr("src", modulePath + "resource/image/icon/icon_age_list_" + UTIL.transPrInfo(data.RATING) + ".png");
            } else {
                div.find("img.age").attr("src", modulePath + "resource/image/icon/icon_age_list_" + UTIL.transPrInfo(data.CONT_RATING) + ".png");
            }

            stars.setRate(div.find(".rating_star_area"), data.OLLEH_RATING);
            setIcon(div.find(".icon_area"), data);
        } else {
            div.addClass("hidden_cont");
            div.attr("id", null);
            div.find(".posterImg.poster").attr("src", null);
            div.find(".posterTitle span").text("");
            div.find("img.isfreeIcon").hide();
            div.find("span.isfree").hide();
        }
    }

    function setIndicatorFocus(that, focused) {
        if(focused) {
            that.indicator.focused();
            that.div.find("li.focus").removeClass('focus');
            that.parent.viewMgr.changeBackground(null, 0);
        } else {
            that.indicator.blurred();
            that.setFocus(isChangedPage?page*LINE_AMOUNT*2:focus);
            isChangedPage = false;
        }
    }

    this.changeFocus = function(amount) {
        var newFocus = focus;
        if(amount==1 && focus == dataList.length-1) newFocus = 0;
        else do newFocus = HTool.getIndex(newFocus, amount, Math.ceil(dataList.length/LINE_AMOUNT)*LINE_AMOUNT);
        while(Math.floor(newFocus/LINE_AMOUNT)>Math.floor((dataList.length-1)/LINE_AMOUNT));
        if(newFocus>=dataList.length) newFocus = dataList.length-1;

        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        while(page!=Math.floor(newFocus/(LINE_AMOUNT*2))) this.changePage((amount>0?1:-1));
        this.div.find("li#" + getDataListId(focus)).addClass('focus');

        this.parent.viewMgr.changeBackground(this.div.find("li#" + getDataListId(focus) + " .posterImg")[0].src, 500);
        setTextAnimation(this.div, focus);
    };

    function clearTextAnimation(div) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
    }

    function setTextAnimation(div, focus) {
        clearTextAnimation(div);

        if(UTIL.getTextLength(dataList[focus].ITEM_NAME, "RixHead L", 30)>215) {
            var posterDiv = div.find("li#" + getDataListId(focus) + " .posterTitle").first();
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.changePage = function(amount) {
        if(totalPage==1) return;
        this.div.removeClass("slide_up slide_down");
        page = HTool.getIndex(page, amount, totalPage);
        this.setData(null, HTool.getIndex(page, amount>0?-1:0, totalPage));
        void this.div[0].offsetWidth;
        this.div.addClass("slide_" + (amount>0?'up':'down'));
        this.indicator.setPos(page);
        this.div.find(".hidden_line").removeClass("hidden_line");

        if(page == totalPage-1) setLineVisible(this.div.find(".line_" +(amount>0?"4":"2")), 0);
        if(amount>0) setLineVisible(this.div.find(".line_0,.line_1"), 500);
        else clearLineVisible();
    };

    this.setFocus = function(newFocus, forced) {
        if(!dataList[focus]) return;
        console.log("setFocus!!!!!");
        focus = newFocus;
        if(forced || page!=Math.floor(newFocus/(LINE_AMOUNT*2))) {
            this.setPage(Math.floor(newFocus / (LINE_AMOUNT * 2)));
            this.div.find("li.focus").removeClass('focus');
        }
        this.div.find("li#" + getDataListId(focus)).addClass('focus');
        this.parent.viewMgr.changeBackground(this.div.find("li#" + getDataListId(focus) + " .posterImg")[0].src, 500);
        setTextAnimation(this.div, focus);
    };

    this.setPage = function(newPage) {
        this.div.removeClass("slide_up slide_down");
        page = newPage;
        this.setData(dataList, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
        this.div.find(".hidden_line").removeClass("hidden_line");
        if(page == totalPage-1) setLineVisible(this.div.find(".line_2"), 0);
    };

    function clearLineVisible() {
        if(lineVisibleTimeout) {
            clearTimeout(lineVisibleTimeout);
            lineVisibleTimeout = null;
        }
    }

    function setLineVisible(div, time) {
        clearLineVisible();

        if(time==0) div.addClass("hidden_line");
        else lineVisibleTimeout = setTimeout(function () {
            div.addClass("hidden_line");
        }, time);
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-LINE_AMOUNT);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(LINE_AMOUNT);
                return true;
            case KEY_CODE.LEFT:
                if(focus%LINE_AMOUNT!=0) this.changeFocus(-1);
                else return false;
                return true;
            case KEY_CODE.RIGHT:
                this.changeFocus(1);
                return true;
            case KEY_CODE.RED:
                if(focus%(LINE_AMOUNT*2)===0) {
                    if(totalPage<=1) return true;
                    if(focus==0 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    this.changeFocus(-(LINE_AMOUNT*2));
                } else this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                if(focus === itemCnt-1) {
                    if(totalPage<=1) return true;
                    this.changeFocus(1);
                    this.changeFocus(LINE_AMOUNT * 2 - 1);
                } else if(focus%(LINE_AMOUNT*2)===LINE_AMOUNT*2-1) {
                    if(totalPage<=1) return true;
                    if(page==totalPage-2 && itemCnt%(LINE_AMOUNT*2)<=LINE_AMOUNT) this.changeFocus(LINE_AMOUNT);
                    else this.changeFocus(LINE_AMOUNT*2);
                } else this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
            case KEY_CODE.ENTER:
                if (dataList[focus].ITEM_TYPE == 1) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, dataList[focus].ITEM_ID, "", reqPathCd);
                    } catch(e) {}
                    openDetailLayer(dataList[focus].ITEM_ID, "", reqPathCd);
                } else if (dataList[focus].ITEM_TYPE == 2 || dataList[focus].ITEM_TYPE == 4) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, dataList[focus].PARENT_CAT_ID, dataList[focus].ITEM_ID, reqPathCd);
                    } catch(e) {}
                    openDetailLayer(dataList[focus].PARENT_CAT_ID, dataList[focus].ITEM_ID, reqPathCd);
                }
                this.setFocus(focus, true);
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.UP:
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN:
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.LEFT:
                return false;
            case KEY_CODE.ENTER:
            case KEY_CODE.RIGHT:
                setIndicatorFocus(this, false);
                return true;
            case KEY_CODE.RED:
                setIndicatorFocus(this, false);
                this.changeFocus((page*LINE_AMOUNT*2) - focus);
                return true;
            case KEY_CODE.BLUE:
                setIndicatorFocus(this, false);
                this.changeFocus((Math.min(itemCnt-1, page*LINE_AMOUNT*2 + (LINE_AMOUNT*2-1))) - focus);
                return true;
        }
    };

    this.getFocusedContents = function () {
        return dataList[focus];
    };


    function getDataListId(dataIdx) {
        return getDataId(dataIdx) + "_" + dataIdx;
    }

    function getDataId(dataIdx) {
        if(dataList[dataIdx]) return ( dataList[dataIdx].ITEM_ID );
        else return "";
    }

    this.focused = function() {
        if(isFocused) return;
        if(!dataList || dataList.length<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        if(!isFocused) return;
        isFocused = false;
        this.indicator.blurred();
        clearLineVisible();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        this.parent.viewMgr.changeBackground(null, 0);
        clearTextAnimation(this.div);
    };

    this.pause = function () {
        clearLineVisible();
        if(this.indicator.isFocused()) focus = isChangedPage?page*LINE_AMOUNT*2:focus;
        this.setFocus(focus, true);
        this.div.removeClass("slide_up slide_down");
        clearTextAnimation(this.div);
        this.div.find(".textAnimating").removeClass("textAnimating");
        if(this.indicator.isFocused()) setIndicatorFocus(this, true);
    };

    this.resume = function () {
        if(!this.indicator.isFocused())
            setTextAnimation(this.div, focus);
    }
};

component.VerticalPosterList.prototype = new ContentsList();
component.VerticalPosterList.prototype.constructor = component.VerticalPosterList;