/**
 * Created by ksk91_000 on 2016-12-07.
 */
if(!window.component) window.component = {};
component.TitleList = function() {
    ContentsList.apply(this, arguments);
    var dataList;
    var itemCnt;
    var totalPage;
    var focus = 0, page = 0;
    var dpPage = -1;
    var isFocused = false;
    var isChangedPage = false;
    var starType;
    var ageLimit;
    var reqPathCd = "33";

    this.create = function (star, _reqPathCd) {
        this.div = _$("<div/>", {class:"contentsList title_list"});
        this.div.append(_$("<ul/>", {class:"top_line list_line"}));

        this.div.append(_$("<ul/>", {class:"item_list"}));
        var detArea = _$("<div/>", {class:"contents_detail_area"});
        detArea.append(_$("<div/>", {class:"posterArea"}));
        detArea.append(_$("<div/>", {class:"star_area"}));
        detArea.append(_$("<div/>", {class:"info_area"}));
        detArea.append(_$("<div/>", {class:"synopsis_area"}));

        var posterArea = detArea.find(".posterArea");
        posterArea.append(_$("<img>", {class:"posterImg"}));
        posterArea.append(_$("<div/>", {class:"icon_area"}));

        this.div.append(detArea);
        createItemArea(this.div.find(".item_list"));
        this.div.append(_$("<div/>", {class: "focus_area"}));
        this.div.append(_$("<ul/>", {class:"bottom_line list_line"}));
        starType = star||stars.TYPE.RED_20;
        reqPathCd = _reqPathCd||reqPathCd;
    };

    function createItemArea(ul) {
        for(var i=0; i<10; i++) {
            var li = _$("<li/>", {class:"contents_item"});
            li.append(_$("<div/>", {class: "title"}).append("<div/>"));
            li.append(_$("<div/>", {class: "wonInfo_area"}));
            li.find(".wonInfo_area").append(_$("<div/>", {class: "isFree"}));
            li.find(".wonInfo_area").append(_$("<img/>", {class: "isFreeIcon"}));
            li.append(_$("<div/>", {class: "prInfo"}));
            ul.append(li);
        }
    }

    this.setData = function(_dataList, page) {
        var contList = this.div.find(".item_list li");
        if(_dataList) {
            dataList = _dataList;
            itemCnt = _dataList.length;
            totalPage = Math.floor((itemCnt - 1) / 10) + 1;
            ageLimit = AuthManager.getPR();
            if(isFocused) this.setFocus(focus)
        } else if(dpPage==page) return;

        dpPage = page;

        for (var i = 0; i < 10; i++) setItem(contList.eq(i), dataList[page*10+i]);
        // for (var i = 0; i < 10; i++) setItem(contList.eq(i+10), dataList[HTool.getIndex(page, 1, totalPage)*10+i]);
        this.indicator.setSize(totalPage, page);
    };

    function setItem(div, data) {
        if(data) {
            div.attr("id", data.ITEM_ID);
            div.find(".title > div").text(data.ITEM_NAME);
            if (HTool.isTrue(data.WON_YN)) {
                div.find("img.isFreeIcon").show();
                div.find("div.isFree").hide();
            } else if (HTool.isFalse(data.WON_YN)) {
                div.find("div.isFree").show();
                div.find("div.isFree").text("무료");
                div.find("img.isFreeIcon").hide();
            } else {
                div.find("img.isFreeIcon").hide();
                div.find("div.isFree").hide();
            }
            div.find(".prInfo").html(_$("<img>", {"src": modulePath + "resource/image/icon/icon_age_txtlist_" + UTIL.transPrInfo(data.RATING) + ".png"}));

            div.removeClass("hiddenItem");
        } else {
            div.attr("id", "");
            div.addClass("hiddenItem");
        }
    }

    this.changeFocus = function(amount) {
        focus = HTool.getIndex(focus, amount, dataList.length);
        this.div.find("li.focus").removeClass('focus');
        while(page!=Math.floor(focus/10)) this.changePage((amount>0?1:-1));
        this.div.find("li#" + dataList[focus].ITEM_ID).addClass('focus');
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");
        var bgSrc = setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
        this.parent.viewMgr.changeBackground(bgSrc, 500);
        // this.parent.viewMgr.changeBackground(this.div.find("li#" + dataList[focus].ITEM_ID + " .posterImg").prop('src'), 500);
        // this.parent.viewMgr.changeBackground(this.div.find(".contents_detail_area.posterArea.posterImg").prop('src'), 500);
    };

    this.changePage = function(amount) {
        // this.div.find(".item_list").removeClass("slide_up slide_down");
        // page = HTool.getIndex(page, amount, totalPage);
        // this.setData(null, HTool.getIndex(page, amount>0?-1:0, totalPage));
        // void this.div[0].offsetWidth;
        // this.div.find(".item_list").addClass("slide_" + (amount>0?'up':'down'));
        // this.indicator.setPos(page);
        this.setPage(HTool.getIndex(page, amount, totalPage));
    };

    this.setFocus = function(newFocus, forced) {
        focus = newFocus;
        this.div.find("li.focus").removeClass('focus');
        if(forced || page!=Math.floor(newFocus/10)) this.setPage(Math.floor(newFocus/10));
        this.div.find("li#" + dataList[focus].ITEM_ID).addClass('focus');
        this.div.find(".focus_area").show().css("-webkit-transform", "translateY(" + 83*(focus%10) + "px)");
        this.div.addClass("show_detail");
        var bgSrc = setFocusItem(this.div.find(".contents_detail_area"), dataList[focus]);
        // this.parent.viewMgr.changeBackground(this.div.find("li#" + dataList[focus].ITEM_ID + " .posterImg").prop('src'), 500);
        // this.parent.viewMgr.changeBackground(this.div.find(".contents_detail_area.posterArea.posterImg").prop('src'), 500);
        this.parent.viewMgr.changeBackground(bgSrc, 500);
    };

    this.setPage = function(newPage) {
        this.div.find(".item_list").removeClass("slide_up slide_down");
        page = newPage;
        this.setData(null, page);
        void this.div[0].offsetWidth;
        this.indicator.setPos(page);
    };

    function setFocusItem(div, data) {
        var bgImgSrc = data.IMG_URL + "?w=259&h=370&quality=90";
        div.find(".posterImg").attr({src: data.IMG_URL + "?w=259&h=370&quality=90", onerror:"this.src='" + modulePath + "resource/image/default_poster.png'"});
        div.find(".star_area").empty().append(stars.getView(starType));
        stars.setRate(div, data.OLLEH_RATING);
        if (data.overseerName || data.actor) {
            div.find(".info_area").html((data.overseerName?("<title>감독</title>" + data.overseerName + "<br>"):"") + (data.actor?("<title>출연</title>" + data.actor):""));
        }
        if (data.synopsis) {
            div.find(".synopsis_area").text(data.synopsis);
        }
        setIcon(div.find(".icon_area"), data);
        return bgImgSrc;
    }

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch(data.NEW_HOT) {
            case "NEW": div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"})); break;
        } if(data.HDR_YN=="Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        } if(data.IS_HD == "UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        } if(data.SMART_DVD_YN=="Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        } if(UTIL.isLimitAge(data.RATING)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    function isLimitAge(a) {
        var b, c = !1;
        return b = ageLimit, "all" != UTIL.transPrInfo(a) && 0 != b && Number(UTIL.transPrInfo(a)) >= Number(b) && (c = !0), c
    }

    this.onKeyAction = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changeFocus(-1);
                return true;
            case KEY_CODE.DOWN :
                this.changeFocus(1);
                return true;
            case KEY_CODE.LEFT :
                return false;
            case KEY_CODE.RED:
                if(focus === 0) {
                    if(totalPage>1) this.changeFocus(-(((itemCnt-1)%10)+1));
                } else if(focus%10===0) {
                    if(totalPage<=1) return true;
                    this.changeFocus(-10);
                } else this.changeFocus((page*10) - focus);
                return true;
            case KEY_CODE.BLUE:
                if(focus === itemCnt-1) {
                    if(totalPage>1) this.changeFocus(10);
                } else if(focus%10===9) {
                    if(totalPage<=1) return true;
                    if(page === totalPage-2) this.changeFocus(((itemCnt-1)%10)+1);
                    else this.changeFocus(10);
                } else this.changeFocus((Math.min(itemCnt-1, page*10+9)) - focus);
                return true;
            case KEY_CODE.ENTER:
                if (dataList[focus].ITEM_TYPE == 1) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, dataList[focus].ITEM_ID, "", reqPathCd);
                    } catch(e) {}
                    openDetailLayer(dataList[focus].ITEM_ID, "", reqPathCd);
                } else if (dataList[focus].ITEM_TYPE == 2 || dataList[focus].ITEM_TYPE == 4) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, dataList[focus].PARENT_CAT_ID, dataList[focus].ITEM_ID, reqPathCd);
                    } catch(e) {}
                    openDetailLayer(dataList[focus].PARENT_CAT_ID, dataList[focus].ITEM_ID, reqPathCd);
                } return true;
                return true;
            default:
                return false;
        }
    };

    this.onKeyForIndicator = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP :
                this.changePage(-1);
                isChangedPage = true;
                return true;
            case KEY_CODE.DOWN :
                this.changePage(1);
                isChangedPage = true;
                return true;
            case KEY_CODE.RIGHT :
            case KEY_CODE.ENTER:
                this.indicator.blurred();
                this.setFocus(isChangedPage ? page * 10 : focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.RED:
                this.indicator.blurred();
                this.changeFocus((page * 10) - focus);
                isChangedPage = false;
                return true;
            case KEY_CODE.BLUE:
                this.indicator.blurred();
                this.changeFocus((Math.min(itemCnt - 1, page * 10 + 9)) - focus);
                isChangedPage = false;
                return true;
        }
    };


    this.getFocusedContents = function () {
        return dataList[focus];
    };

    this.focused = function() {
        if(isFocused) return;
        if(!dataList || dataList.length<=0) return false;
        this.setFocus(focus);
        isFocused = true;
        return true;
    };

    this.blurred = function () {
        if(!isFocused) return;
        isFocused = false;
        this.indicator.blurred();
        this.setFocus(0, true);
        this.div.find("li.focus").removeClass('focus');
        this.div.removeClass("slide_up slide_down");
        this.div.removeClass("show_detail");
        this.div.find(".focus_area").hide();
        this.parent.viewMgr.changeBackground(null, 0);
        // this.setData(dataList, page);
    };
};

component.TitleList.prototype = new ContentsList();
component.TitleList.prototype.constructor = component.TitleList;