window.myMenu = function(menuId, menuData) {
    var id = menuId;
    var focus;
    var div = _$("<div/>", {id: id, class:'myMenu'});
    var language;
    var menuDataVersion = MenuDataManager.getMenuVersion();

    // 하드 코딩 된 MENU ID
    var MENU_ID = MenuDataManager.MENU_ID;

    // [si.mun] 편성되는 메뉴들은 MENU ID가 따로 존재하지 않아서,
    // 각 메뉴에 맞는 image 파일 이름들을 알아 낼 방법이 존재하지 않음.
    // 그래서 w3_menu.js 에 있는 MENU_ID 를 하드 코딩 해놓음.
    var MAKED_MENU_ID = {
        MOBILE_PAIRING  : "T0000000000000050140", // TV에 내폰 연결
        ALARM           : "T0000000000000004062", // 일정 알리미
        BOOK_MARK       : "T0000000000000004070", // 북마크
        MY_GALLERY      : "T0000000000000004078", // 마이 갤러리
        GIFT_BOX        : "T0000000000000004091", // VOD 선물함
    };

    var titleList = [];
    titleList[MENU_ID.PURCHASED_LIST_MENU]      = {name:"구매내역<br>보기",          englishItemName: "Purchased"};
    titleList[MENU_ID.MY_PLAY_LIST]             = {name:"마이<br>플레이리스트",      englishItemName: "My<br>Playlist"};
    titleList[MENU_ID.MY_VOD_COLLECTION]        = {name:"마이<br>소장용VOD",         englishItemName: "My VOD<br>Collection"};
    titleList[MENU_ID.SUBSCRIPTION_INFOMATION]  = {name:"가입정보",                  englishItemName: "Package<br>Info"};
    titleList[MENU_ID.COUPON_POINT]             = {name:"쿠폰, 포인트<br>등록/충전", englishItemName: "Coupons<br> & Points"};

    titleList[MAKED_MENU_ID.MOBILE_PAIRING]     = {name:"TV에<br>내 폰 연결",        englishItemName: "Mobile<br>Pairing"}; // TV에 내폰 연결
    titleList[MAKED_MENU_ID.ALARM]              = {name:"일정<br>알리미",            englishItemName: "Alarm"}; // 일정 알리미
    titleList[MAKED_MENU_ID.BOOK_MARK]          = {name:"북마크<br>보관함",          englishItemName: "Bookmark"}; // 북마크
    titleList[MAKED_MENU_ID.MY_GALLERY]         = {name:"마이<br>갤러리",            englishItemName: "My<br>Gallery"}; // 마이 갤러리
    titleList[MAKED_MENU_ID.GIFT_BOX]           = {name:"VOD<br>선물함",             englishItemName: "Gift box"}; // VOD 선물함

    var maxRowItemCnt = 10;

    (function constructDom() {
        //div.append("<span class='componentTitle'><div></div>마이메뉴</span>");
        div.append("<ul class='focusingAnimatable menuList'/>");

        for(var i=0; i<menuData.children.length; i++) {
            div.find(".menuList").append(getItem(i, menuData.children[i]));
        }
    })();

    this.getView = function() {
        return div;
    };

    this.update = function() {
        var hasUpdate = updateMenuData();
        hasUpdate = updateLanguage() || hasUpdate;
        if(hasUpdate) {
            //div.find(".componentTitle").html("<div/>" + HTool.getMenuName(menuData));

            div.find(".menuList").empty();
            for (var i = 0; i < menuData.children.length; i++) {
                div.find(".menuList").append(getItem(i, menuData.children[i]));
            }
        }
    };

    function updateLanguage() {
        var l = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(l) && (l = "kor");
        if(language==l) return false;
        language = l;
        return true;
    }

    function updateMenuData() {
        var newMenuDataVersion = MenuDataManager.getMenuVersion();
        if(menuDataVersion !== newMenuDataVersion) {
            menuDataVersion = newMenuDataVersion;
            menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) {
                    if (menu.parent && !menu.parent.parent && menu.parent.catType == "SubHome" && menu.catType == "MyMenu2") return true;
                }
            })[0];
            return true;
        } else return false;
    }

    function setItemName() {
    }

    //TODO 편성대로 변경 필요
    function getItem(idx, menuData) {
        var className = 'menuItem';

        // [si.mun] Biz 상품인 경우 쿠폰/포인트 충전, TV에 내폰 연결 비활성화
        if (KTW.CONSTANT.IS_BIZ) {
            if (menuData.id === MAKED_MENU_ID.MOBILE_PAIRING || menuData.id === MENU_ID.COUPON_POINT) {
                className += " disabled";
            }
        }

        var html="<li id="+ id + "_" + idx + " class='" + className + "'>" +
            "<div class='itemArea'>" +
            "<img class='basic_img' src='" + (modulePath + "resource/image/mymenu/" + getImageName(menuData.id) + ".png") + "'>" +
            "<img class='focus_img' src='" + (modulePath + "resource/image/mymenu/" + getImageName(menuData.id) + "_f.png") + "'>" +
            "<span class='item_title'><p>" + ((titleList[menuData.id]&&titleList[menuData.id].name)?HTool.getMenuName(titleList[menuData.id]):HTool.getMenuName(menuData)) + "</p></span>" +
            "</div>"+
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            "<div class='sdw_area'></div>" +
            "</li>";
        return html;
    }

    /**
     * [si.mun] MENU ID에 따라 지정 된 Image name 반환,
     * @param menuId
     * @returns imageName : image name (string)
     */
    function getImageName (menuId) {
        var imageName;

        switch (menuId) {
            case MENU_ID.PURCHASED_LIST_MENU : // 구매내역
                imageName = "my_menu_s_3";
                break;
            case MENU_ID.MY_VOD_COLLECTION : // 마이 소장용
                imageName = "my_menu_s_1";
                break;
            case MENU_ID.MY_PLAY_LIST : // 마이 플레이 리스트
                imageName = "my_menu_s_2";
                break;
            case MENU_ID.COUPON_POINT : // 쿠폰, 포인트 등록/충전
                imageName = "my_menu_s_p";
                break;
            case MENU_ID.SUBSCRIPTION_INFOMATION : // 가입 정보
                imageName = "my_menu_s_4";
                break;
            case MAKED_MENU_ID.MOBILE_PAIRING : // TV 에 내폰 연결
                imageName = "my_menu_s_5";
                break;
            case MAKED_MENU_ID.ALARM : // 일정 알리미
                imageName = "my_menu_s_6";
                break;
            case MAKED_MENU_ID.BOOK_MARK : // 북마크 보관함
                imageName = "my_menu_s_7";
                break;
            case MAKED_MENU_ID.MY_GALLERY : // 마이 갤러리
                imageName = "my_menu_s_8";
                break;
            case MAKED_MENU_ID.GIFT_BOX : // VOD 선물함
                imageName = "my_menu_s_9";
                break;
            default : // 그 외에 편성으로 내려오는 메뉴들
                imageName = "my_menu_s_default";
                break;
        }
        return imageName;
    }

    this.isLastFocused = function () { return focus === menuData.children.length-1; };
    this.isFirstFocused = function () { return focus === 0; }

    this.onFocused = function(fromKey) {
        if(fromKey===KEY_CODE.UP) focus = Math.floor((menuData.children.length-1)/maxRowItemCnt)*maxRowItemCnt;
        else if(fromKey===KEY_CODE.BLUE) focus = menuData.children.length-1;
        else focus = 0;
        focusing(focus);
    };

    this.onBlured = function() {
        unfocusing(focus);
        div.find(".componentTitle").css("-webkit-transform", "");
    };

    var focusing = function(index) {
        div.find("li#" + id + "_" + index).addClass("focus");
        if(index==0) div.find(".componentTitle").css("-webkit-transform", "translateY(-12px)");
        else div.find(".componentTitle").css("-webkit-transform", "");
    };

    var unfocusing = function(index) {
        div.find("li#" + id + "_" + index).removeClass("focus");
    };

    this.onKeyAction = function(keyCode) {
        var tempNextFocus = 0;

        var keyAction = { isUsed : true };
        switch(keyCode) {
            case KEY_CODE.LEFT:
                if(focus%maxRowItemCnt>0) {
                    unfocusing(focus);
                    tempNextFocus = getNewFocus(focus-1, keyCode);

                    if (tempNextFocus >= 0) {
                        focus = tempNextFocus;
                        focusing(focus);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            case KEY_CODE.RIGHT:
                if(focus<menuData.children.length-1) {
                    unfocusing(focus);
                    tempNextFocus = getNewFocus(focus + 1, keyCode);

                    if (tempNextFocus >= 0) {
                        focus = tempNextFocus;
                        focusing(focus);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            case KEY_CODE.UP:
                if(focus>=maxRowItemCnt) {
                    unfocusing(focus);

                    tempNextFocus = getNewFocus(focus - maxRowItemCnt, keyCode);

                    if (tempNextFocus >= 0) {
                        focus = tempNextFocus;
                        focusing(focus);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            case KEY_CODE.DOWN:
                // 2줄 이상일 때
                if(Math.floor(focus/maxRowItemCnt)<Math.floor((menuData.children.length-1)/maxRowItemCnt)) {
                    unfocusing(focus);
                    if(focus+maxRowItemCnt<menuData.children.length) { // 아래에 메뉴가 있다면
                        focus = focus+maxRowItemCnt;
                    } else { // 아래에 메뉴가 없으면 마지막으로
                        focus = menuData.children.length-1;
                    }
                    focusing(focus);
                    return true;
                } else return false;
            case KEY_CODE.ENTER:
                enterKeyAction(menuData.children[focus]);
                return true;
        } return false;
    };

    function getNewFocus(nextFocus, keyCode) {
        var nextItemIsDisabled = div.find("li#" + id + "_" + nextFocus).hasClass("disabled");

        // [si.mun] Biz 상품에선 일부 메뉴가 disabled 되어있으므로 확인 필요
        if (KTW.CONSTANT.IS_BIZ && nextItemIsDisabled) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (nextFocus - 1 % maxRowItemCnt >= 0) {
                        return nextFocus - 1;
                    }
                    break;
                case KEY_CODE.RIGHT:
                    if (nextFocus + 1 < menuData.children.length) {
                        return nextFocus + 1;
                    }
                    break;
                case KEY_CODE.UP:
                    if (nextFocus - maxRowItemCnt < menuData.children.length) {
                        return nextFocus - maxRowItemCnt;
                    }
                    break;
            }
            return -1;
        }
        return nextFocus;
    }

    function enterKeyAction(menuData) {
        try {
            NavLogMgr.collectFHMenu(KEY_CODE.ENTER, menuData.id, menuData.name, "", "");
        } catch(e) {}
        if(menuData instanceof window.parent.VMenu) {
            switch (menuData.id) {
                case MENU_ID.MY_VOD_COLLECTION :
                    //마이 소장용VOD
                    openAdultAuthPopup(function () {
                        LayerManager.activateLayer({
                            obj: {
                                id: "MyHomeCloud",
                                type: Layer.TYPE.NORMAL,
                                priority: Layer.PRIORITY.NORMAL,
                                params: {
                                    menuName: HTool.getMenuName(menuData)
                                }
                            },
                            moduleId: "module.family_home",
                            new: true,
                            visible: true
                        });
                    }, true);
                    break;
                case MENU_ID.MY_PLAY_LIST :
                    //마이 플레이리스트
                    LayerManager.activateLayer({
                        obj: {
                            id: "MyHomeMyPlayList",
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            params: {
                                menuName: HTool.getMenuName(menuData)
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    break;
                case MENU_ID.PURCHASED_LIST_MENU:
                    //구매내역 보기
                    LayerManager.activateLayer({
                        obj: {
                            id: "MyHomeBuyList",
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            params: {
                                menuName: HTool.getMenuName(menuData)
                            }
                        },
                        moduleId: "module.family_home",
                        new: true,
                        visible: true
                    });
                    break;
                case MENU_ID.SUBSCRIPTION_INFOMATION:
                    //가입정보
                    LayerManager.activateLayer({
                        obj: {
                            id: "MyHomeMyProduct",
                            type: Layer.TYPE.NORMAL,
                            priority: Layer.PRIORITY.NORMAL,
                            params: {
                                menuName: HTool.getMenuName(menuData)
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    break;
                case MENU_ID.COUPON_POINT:
                    // 쿠폰, 포인트 등록/충전
                    LayerManager.startLoading({preventKey: true});
                    LayerManager.activateLayer({
                        obj: {
                            id: "CouponChargePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: {
                                menuNames: [
                                    { name: menuData.children[1].name, englishItemName: menuData.children[1].templateName },
                                    { name: menuData.children[2].name, englishItemName: menuData.children[2].templateName },
                                    { name: menuData.children[0].name, englishItemName: menuData.children[0].templateName },
                                    { name: menuData.children[3].name, englishItemName: menuData.children[3].templateName },
                                ],
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    break;
            }
        } else if(menuData instanceof window.parent.VInteractive) {
            goToLocator(menuData);
        } else if(menuData instanceof window.parent.VCategory) {
            //home_help.js
            LayerManager.activateLayer({
                obj: {
                    id: "MyHomeHelp",
                    type: Layer.TYPE.NORMAL,
                    priority: Layer.PRIORITY.NORMAL,
                    params: {
                        catId: menuData.id,
                        menuName: HTool.getMenuName(menuData)
                    }
                },
                moduleId: "module.family_home",
                visible: true
            });
        }
    }

    function goToLocator(data) {
        switch (data.itemType-0) {
            case 0:
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) { if(menu.id==data.itemId) return true; }
                });
                if(menuData != undefined) {
                    MenuServiceManager.jumpMenu({menu: menuData});
                } else {
                    showToast("잘못된 접근입니다");
                }
                break;
            case 1:    //시리즈
                //TODO REQ_PATH_CD 입력 필요.
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, data.itemId, null, "01");
                } catch(e) {}
                openDetailLayer(data.itemId, null, "01");
                break;
            case 2:    //컨텐츠
                //TODO REQ_PATH_CD 입력 필요.
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.itemId, "01");
                } catch(e) {}
                openDetailLayer(null, data.itemId, "01");
                break;
            case 3:    //멀티캐스트 양방향 서비스
                var nextState;
                if(data.locator == CONSTANT.APP_ID.MASHUP)
                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                else
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: data.locator,
                        ex_param: data.parameter
                    }
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: data.itemId
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(data.locator);
                break;
                break;
        }
    }

    this.getHeight = function() {
        // 170 : 한 메뉴의 height(height + margin-bottom)
        //  60 : component margin-bottom
        return 170*(Math.ceil(menuData.children.length/maxRowItemCnt)) + 60;
    }
};