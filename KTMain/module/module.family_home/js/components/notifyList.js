/**
 * Created by ksk91_000 on 2016-06-30.
 */
if(!component) var component = {};

component.notifyList = function(id, data) {
    var componentId = id;
    var dom = _$("<div/>", {id:componentId});
    var maxPage;
    var itemList = [];
    var focus, page, dispPage;
    var indicator = new Indicator(1);

    function createDom() {
        var div = _$("<div/>", {class: "content_area"});
        var wrapper = _$("<div/>", {class: "scrollWrapper"});
        for (var i = 0; i < 6 ; i++) wrapper.append(getItemView());
        div.append(wrapper);
        dom.append(_$("<div/>", {class: "focus_line"}));
        dom.append(div);
        dom.append(indicator.getView()).append(indicator.getPageTransView());;
    }

    this.setData = function(data) {
        data = getFilteredViewingData(data);
        if (data.length > 0) {
            createDom();
            sortNotify(data);
            maxPage = Math.ceil(data.length/4);
            dom.attr({class:"notify mailbox"});
            for(var i=0; i<data.length; i++)
                itemList.push(new Item(data[i]));
            page = 0;
            indicator.setSize(maxPage, page);
            setPageData(0);
            setFocus(0);
            dom.find(".page_bottom").toggle(maxPage>1);
        } else {
            dom.empty();
            dom.attr({class:"noNotice mailbox"});
            dom.html("<div class='no_item_area'><img src='" + modulePath +"resource/image/icon_noresult.png'><span>공지사항이 없습니다</span></div>");
        }
    };

    function getFilteredViewingData(data) {
        var viewingData = [];
        var curDateStr = HTool.getDateStr();
        // 노출시점에 해당하지 않는 데이터 필터링
        for(var i=0; i<data.length; i++) {
            if(curDateStr <= data[i].end_time && curDateStr >= data[i].start_time) viewingData.push(data[i]);
        } return viewingData;
    }

    function sortNotify(data) {
        if(data[0].order!=null) data.sort(function (a, b) { return a.order-b.order; });
        else data.sort(function (a, b) { return b.start_time-a.start_time; });
    }

    function setPageData(page) {
        if(dispPage==page) return;
        var j = 0;
        for(var i=page*4; i<itemList.length && j<6; i++, j++) {
            itemList[i].setDiv(dom.find(".item").eq(j).removeClass("hiddenItem"), i+1);
        }for(; j<6; j++) {
            dom.find(".item").eq(j).addClass("hiddenItem");
        } dispPage = page;
    }

    this.getView = function() {
        return dom;
    };

    var getItemView = function() {
        return "<div class='item'><div class='index'><span></span></div>"+
            "<div class='content'>"+
            "<span class='title'></span>"+
            "<span class='desc'></span>"+
            "</div>"+
            "<div class='btnArea'><div class='date'/></div>" +
            "</div>";
    };

    function getDateStr(dateStr) {
        return dateStr.substr(0, 4) + "." + dateStr.substr(4, 2) + "." + dateStr.substr(6,2);
    }

    function changeFocus(amount) {
        setFocus(HTool.getIndex(focus, amount, itemList.length));
    }

    function setFocus(_focus) {
        dom.find(".item.focus").removeClass("focus");
        focus = _focus;
        dom.find(".item").eq(focus%4).addClass("focus");
        dom.find(".focus_line").css("-webkit-transform", "translateY(" + ((focus%4)*210) + "px)");
        setPage(Math.floor(focus/4));
    }

    function changePage(amount) {
        if(page + amount < 0) setPage(maxPage - 1);
        else if(page + amount == maxPage) setPage(0);
        else setPage(page + amount);
    }

    function setPage(_page) {
        page = _page;
        setPageData(page);
        indicator.setPos(page);
    }

    this.onKeyAction = function(keyCode) {
        //if(indicator.isFocused()) return onKeyForIndicator(keyCode);
        switch(keyCode) {
            case KEY_CODE.LEFT:
                dom.find(".item.focus").removeClass("focus");
                dom.find(".focus_line").hide();
                return false;
                //if(maxPage>1) {
                //    //indicator.focused();
                //    dom.find(".item.focus").removeClass("focus");
                //    dom.find(".focus_line").hide();
                //    return true;
                //}else return false;
            case KEY_CODE.RIGHT:
                return true;
            case KEY_CODE.UP:
                changeFocus(-1);
                return true;
            case KEY_CODE.DOWN:
                changeFocus(1);
                return true;
            case KEY_CODE.RED:
                if (maxPage === 1) {
                    return true;
                }

                if(focus%4===0) setFocus(HTool.getIndex(page, -1, maxPage)*4);
                else setFocus(page*4);
                //if(indicator.isFocused()) changePage(HTool.getIndex(page, -1, maxPage));
                //else if(focus%4===0) setFocus(HTool.getIndex(page, -1, maxPage)*4);
                //else setFocus(page*4);
                return true;
            case KEY_CODE.BLUE:
                if (maxPage === 1) {
                    return true;
                }

                if(focus%4===3 || focus===itemList.length-1) setFocus(Math.min(itemList.length-1, HTool.getIndex(page, 1, maxPage)*4 + 3));
                else setFocus(Math.min(itemList.length-1, page*4+3));
                //if(indicator.isFocused()) changePage(HTool.getIndex(page, 1, maxPage));
                //else if(focus%4===3 || focus===itemList.length-1) setFocus(Math.min(itemList.length-1, HTool.getIndex(page, 1, maxPage)*4 + 3));
                //else setFocus(Math.min(itemList.length-1, page*4+3));
                return true;
            case KEY_CODE.ENTER:
                itemList[focus].enterKeyAction();
                return true;
        }
    };

    this.focused = function() {
        if (data.length > 0){
            setFocus(page*4);
            return true;
        } else return false;
    };

    this.blurred = function() {
        //indicator.blurred();
        dom.find(".focus_line").show();
        if (data.length > 0) {
            dom.find(".item.focus").removeClass("focus");
            setPage(0);
        }
    };

    var Item = function (data) {
        this.getData = function() { return data };
        this.setDiv = function (div, index) {
            if (index < 10) {
                div.find(".index span").text("0" + index);
            } else {
                div.find(".index span").text(index);
            }

            div.find(".title").text(data.title);
            div.find(".desc").text(data.desc);
            div.find(".date").text(getDateStr(data.start_time));
        };
        this.enterKeyAction = function () {
            LayerManager.activateLayer({
                obj: {
                    id: "MyHomeNotifyDetailPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: { data: data }
                },
                moduleId: "module.family_home",
                visible: true
            });
        }
    };

    this.setData(data);
}