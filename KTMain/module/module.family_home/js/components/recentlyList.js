/**
 * 최근 시청 목록 (우리집 맞춤 Tv -> 최근 시청 목록(컴포넌트))
 * Created by ksk91_000 on 2016-10-07.
 */
window.recentlyList = function(menuId, menuData) {
    var componentId = menuId;
    var data;
    var length;
    var isMore;
    var isFocused = false;

    var div = _$("<div/>", {id: menuId});

    var focus = 0;
    var type = 0; // 0: 리스트, 1: 데이터 없음.
    var instance = this;

    var maxPosterCnt = 7;

    this.update = function(options) {
        RecentlyListManager.getRecentlyData(function(contentsData) {
            data = contentsData;
            length = data.length;
            isMore = length > maxPosterCnt;
            constructDom();

            instance.onKeyAction = data.length > 0 ? instance.onKeyForList : instance.onKeyForInfoTxt;

            if (isFocused) {
                if (data.length > 0) {
                    if (focus >= data.length) focus = data.length - 1;
                    focusing(focus);
                }
                else div.find("#" + componentId).addClass("focus");
            }
        }, options && options.resume);
    };

    var constructDom = function() {
        div.empty();
        div.append(_$("<span/>", {class: "componentTitle"}).html("<div/>" + HTool.getMenuName(menuData)));
        if (data.length > 0) {
            var list = _$("<ul/>", {id: componentId + "_contents", class: "sub_posterList"});
            div.append(list);

            for (var i = 0; i < length && i < (isMore ? maxPosterCnt - 1 : maxPosterCnt); i++) {
                list.append(getVerticalPoster(componentId + "_" + i, data[i]));
            }
            if (isMore) {
                data[6].moreCnt = length;
                list.append(PosterFactory.getVerticalPoster_more(componentId + "_" + (maxPosterCnt - 1), data[maxPosterCnt - 1]));
            }
        } else {
            makeNoContentArea();
        }
    };

    function makeNoContentArea() {
        div.append(_$("<div/>", {id: componentId, class: "infoText_recently"}));
        div.find("#" + componentId).append(_$("<span>", {class: 'text_1'}).text("최근 시청 콘텐츠가 없습니다"));
        div.find("#" + componentId).append(_$("<span>", {class: 'text_2'}).text("올레 tv의 다양한 콘텐츠를 즐겨보세요~"));
        div.find("#" + componentId).append(_$("<img>", {src: modulePath + 'resource/image/img_myhome_no_content.png'}));
        div.find("#" + componentId).append(_$("<div/>", {class: 'dim'}));
        div.find(".dim").text("최신 영화 바로가기");
    }

    var getVerticalPoster = function(id, data) {
        var element = _$("<li id='" + id + "' class='content poster_vertical'>" +
            "<div class='content'>" +
            "<div class='focus_red'><div class='black_bg'></div></div>" +
            "<div class='sdw_area'><div class='sdw_left'/><div class='sdw_mid'/><div class='sdw_right'/></div>" +
            "<img src='" + data.imgUrl + "?w=196&h=280&quality=90' class='posterImg poster' onerror=this.src='" + modulePath + "resource/image/default_poster.png'>" +
            "<div class='edit_select_btn'></div>" +
            "<div class='contentsData'>" +
            "<div class='posterTitle'><span>" + data.contsName + "</span></div>" +
            "<span class='posterDetail'>" +
            "<span class='stars'/>" +
            (HTool.isTrue(data.wonYn) ? "<img class='isfreeIcon'>" : (HTool.isFalse(data.wonYn) ? ("<span class='isfree'>" + "무료" + "</span>") : "")) +
            "<img src='" + modulePath + "resource/image/icon/icon_age_list_" + ServerCodeAdapter.getPrInfo(data.prInfo - 0) + ".png' class='age'>" +
            "</span>" +
            "</div>" +
            "<div class='icon_area'/>" +
            "<div class='poster_remain_bg'></div>" +
            "<div class='poster_remain_bar'></div>" +
            "</div>" +
            "</li>");

        element.find(".poster_remain_bar").css("width", Math.min(196, data.linkTime / (parseInt(data.runTime || data.runtime) * 60) * 196) + "px");
        setIcon(element.find(".icon_area"), data);
        element.toggleClass("series", data.contYn == "N");
        return element;
    };

    function setIcon(div, data) {
        div.html("<div class='left_icon_area'/><div class='right_icon_area'/><div class='bottom_tag_area'/><div class='lock_icon'/>");
        switch (data.newHot) {
            case "N":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_new.png"}));
                break;
            case "U":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_update.png"}));
                break;
            case "X":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_monopoly.png"}));
                break;
            case "B":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_discount.png"}));
                break;
            case "R":
                div.find(".left_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/icon_flag_recom.png"}));
                break;
        }
        if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_hdr.png"}));
        }
        if (data.isHd == "UHD") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_uhd.png"}));
        }
        if (data.isDvdYn == "Y") {
            div.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/image/icon/tag_poster_mine.png"}));
        }
        if (data.wEvtImageUrlW3) {
            div.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
        }
        if (data.adultOnlyYn != "Y" && UTIL.isLimitAge(data.prInfo)) {
            div.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/image/icon/img_vod_locked.png"}));
        }
    }

    this.getView = function() {
        return div;
    };

    this.isLastFocused = function () { if (data && data.length > 0) return focus === Math.min(maxPosterCnt, length)-1; else return true; };
    this.isFirstFocused = function () { return focus === 0; };

    this.onFocused = function(keyCode) {
        if (data && data.length > 0) {
            if(keyCode===KEY_CODE.BLUE) focus = Math.min(maxPosterCnt, length) - 1;
            else focus = 0;
            focusing(focus);
        } else {
            div.find("#" + componentId).addClass("focus");
            _$("#MyHomeMain .context_menu_indicator").text("");
        }
        isFocused = true;
    };

    this.onBlured = function() {
        isFocused = false;
        if (data && data.length > 0) {
            div.find(".componentTitle").css("-webkit-transform", "");
            div.find(".context_menu_indicator").hide();
            unfocusing(focus);
        } else {
            div.find("#" + componentId).removeClass("focus");
        }
    };

    var focusing = function(index) {
        _$("#" + componentId + "_" + index).addClass("focus");
        if (isMore && focus == maxPosterCnt - 1) _$("#MyHomeMain .context_menu_indicator").text("");
        else _$("#MyHomeMain .context_menu_indicator").text("목록에서 삭제");
        if (index == 0) div.find(".componentTitle").css("-webkit-transform", "translateY(-13px)");
        else div.find(".componentTitle").css("-webkit-transform", "");
        setTextAnimation(index);
    };

    var unfocusing = function(index) {
        _$("#" + componentId + "_" + index).removeClass("focus");
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
    };

    function setTextAnimation(focus) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        if (UTIL.getTextLength(data[focus].contsName, "RixHead L", 30) > 201) {
            var posterDiv = div.find("#" + componentId + "_" + focus + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.onKeyForList = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT:
                if (focus > 0) {
                    unfocusing(focus);
                    focus--;
                    focusing(focus);
                    return true;
                } else return false;
            case KEY_CODE.RIGHT:
                if (focus < Math.min(maxPosterCnt, length) - 1) {
                    unfocusing(focus);
                    focus++;
                    focusing(focus);
                    return true;
                }
                return false;
            case KEY_CODE.ENTER:
                if (focus == maxPosterCnt - 1 && length > maxPosterCnt) {
                    openMorePage();
                } else {
                    try {
                        NavLogMgr.collectFHMenu(NLC.JUMP_START_SUBHOME, NLC.MENU00601.id, NLC.MENU00601.name,
                            data[focus].contsId, data[focus].contsName);
                    } catch(e) {}
                    openDetailLayerImpl(data[focus]);
                }
                return true;
            case KEY_CODE.CONTEXT:
                if (length <= maxPosterCnt || focus != maxPosterCnt - 1) {
                    var that = this;
                    LayerManager.activateLayer({
                        obj: {
                            id: "EditModeDeletePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: false,
                            params: {
                                type: 1,
                                items: [{title: data[focus].contsName}],
                                callback: function(res) {
                                    if (res) {
                                        RecentlyListManager.removeContents(focus, function(result) {
                                            if (result === true) {
                                                showToast("최근 시청 목록에서 삭제되었습니다");
                                                that.update();
                                            }
                                        });
                                    }
                                    LayerManager.historyBack();
                                }
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                }
                return true;
        }
        return false;
    };

    this.onKeyForInfoTxt = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.ENTER:
                try {
                    NavLogMgr.collectFHMenu(keyCode, NLC.MENU00601.id, "최신영화 바로가기", "", "");
                } catch(e) {}
                gotoLocator(SubHomeDataManager.getMyMenu().find(function(a) { return a.position == 1; }));
                return true;
        }
        return false;
    };

    var openDetailLayerImpl = function(data) {
        LayerManager.startLoading({preventKey:true});
        if(data.contYn=="N") {
            if (data.catId == "N") {
                myHome.amocManager.getCategoryId("contsId=" + data.contsId + "&saId=" + DEF.SAID + "&path=V&platformGubun=W", function (res, newData) {
                    if(res) {
                        if (!newData) {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET002});
                        } else if (newData.serviceYn == "Y") {
                            openDetailLayer(newData.catId, data.contsId, "02");
                        } else {
                            LayerManager.stopLoading();
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                        }
                    } else {
                        LayerManager.stopLoading();
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    }
                });
            } else openDetailLayer(data.contsId, data.contsId, "02");
        } else {
            myHome.amocManager.getContentW3(function (res, contData) {
                if(res) {
                    if(contData.contsId) openDetailLayer(contData.catId, data.contsId, "02", {contsInfo:contData});
                    else if(data.catId!=="N") openDetailLayer("N", data.contsId, "02");
                    else {
                        LayerManager.stopLoading();
                        HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                    }
                } else {
                    LayerManager.stopLoading();
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                }
            }, data.catId, data.contsId, DEF.SAID);
        }
    };

    function openMorePage() {
        try {
            NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU00601.id, "전체보기", "", "");
        } catch(e) {}
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeRecentlyList",
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                linkage: false,
                params: { menuName:HTool.getMenuName(menuData) }
            },
            moduleId: "module.family_home",
            new: false,
            visible: true
        })
    }

    this.onKeyAction = function(keyCode) {
    };

    this.getHeight = function() {
        //if (length > 0) return COMP_TITLE_HEIGHT + 411;
        //else return COMP_TITLE_HEIGHT + 415;
        return myHome.CONSTANT.COMP_TITLE_HEIGHT + myHome.CONSTANT.POSTER_HEIGHT;
    };

    makeNoContentArea();
};