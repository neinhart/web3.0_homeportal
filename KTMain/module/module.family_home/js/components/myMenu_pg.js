//window.myMenu_pg = function(menuId, menuData) {
//    var instance = this;
//
//    var id = menuId;
//    var msgAmount = 0;
//
//    var focus;
//    var dom = _$("<div/>", {id:id, class:'myMenu_pg'});
//    var language;
//    var Message_last_dt, noticeVersion;
//    var hasUpdateMail;
//
//    this.init = function() {
//        var list = _$("<ul/>", {class:'menuList'});
//        dom.html(list);
//        list.append(getItem(0, 1, 0, "개", "", "새로운 <br>알림/이벤트가<br>없습니다")); //우편함
//        list.append(getItem(1, 2, 0, "원", "구매/등록"));           //TV쿠폰
//        list.append(getItem(2, 3, 0, "P", "충전/관리"));            //TV포인트
//        list.append(getItem(3, 4, 0, "점", "조회"));                //KT멤버십 포인트
//        list.append(getItem(4, 5, 0, "개", "구매/등록"));           //콘텐츠 이용권
//    };
//
//    this.update = function() {
//        var cacheData = StorageManager.ps.load(StorageManager.KEY.STB_POINT_CACHE);
//        var parseData;
//        try {
//            parseData = JSON.parse(cacheData);
//            if(!(parseData instanceof Object)) cacheData = null;
//        } catch(e) {
//            cacheData = null;
//        }
//
//        var l = MenuDataManager.getCurrentMenuLanguage();
//        0 == UTIL.isValidVariable(l) && (l = "kor");
//        language = l;
//
//        if(!cacheData || JSON.parse(cacheData).ReqFlag===true) {
//            KTW.managers.http.PointManager.getTvPoint('1', function(res, data) {
//                data.ReqFlag = false;
//                data.UpdateTime = new Date().getTime();
//                StorageManager.ps.save(StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
//                instance.setData(data);
//            });
//        } else instance.setData(parseData);
//
//        noticeVersion = MenuDataManager.getNoticeVersion();
//        var last_dt = StorageManager.ps.load(StorageManager.KEY.INBOX_LAST_READ_TIME);
//        if(!last_dt || last_dt=="undefined") {
//            StorageManager.ps.save(StorageManager.KEY.INBOX_LAST_READ_TIME, "0|0");
//            last_dt = ["0", "0"];
//        } else {
//            last_dt = last_dt.split("|");
//        }
//
//        myHome.wsSmartPushManager.msgBoxNew(function (res, data) {
//            if(data.last_dt && data.last_dt!=="") {
//                Message_last_dt = data.last_dt;
//                setItem(0, 0, "개", "", "새로운<br>알림/이벤트를<br>확인하세요", true);
//                hasUpdateMail = true;
//            } else if(noticeVersion!==last_dt[1] && HTool.getNoticeData().length>0) {
//                setItem(0, 0, "개", "", "새로운 공지를<br>확인하세요", true);
//                Message_last_dt = last_dt[0];
//                hasUpdateMail = true;
//            } else {
//                setItem(0, 0, "개", "", "새로운 <br>알림/이벤트가<br>없습니다", false);
//                Message_last_dt = last_dt[0];
//            }
//        }, last_dt[0]||"0");
//
//    };
//
//    this.getView = function() {
//        return dom;
//    };
//
//    this.setData = function(data) {
//        //TV쿠폰 Setting.
//        setItem(1, data.TvMoney?data.TvMoney:0, "원", "구매/등록");
//        //TV포인트 Setting
//        if(data.TvPointYn=="Y") setItem(2, data.TvPoint, "P", "충전/관리");
//        else setItem(2, "미가입", "", "충전/관리");
//        //KT멤버십 포인트 Setting
//        if(data.UsrDiv=="R") setItem(3, data.StarPoint, "점", "조회");
//        else setItem(3, "미가입", "", "조회");
//        //콘텐츠 이용권 Setting
//        setItem(4, data.DetPoint2?data.DetPoint2:0, "개", "구매/등록");
//    };
//
//    function setItem(idx, amount, measure, btnTxt, basicText, isNew) {
//        var item = dom.find("#" + id + "_" + idx);
//        item.find('.amountText').text(addComma(amount));
//        item.find('.measureText').text(measure);
//        item.find('.btnText').text(btnTxt);
//        item.find(".basicText").html(basicText);
//        item.toggleClass("new", !!isNew);
//        item.find(".baseBox").attr("src", modulePath + "resource/image/main/my_menu_box_" + (idx+1) + (language=="eng"?"_eng":"") + ".png");
//        item.find(".focusBox").attr("src", modulePath + "resource/image/main/my_menu_box_" + (idx+1) + (language=="eng"?"_eng":"") + "_f.png");
//    }
//
//    function getItem(idx, imgIdx, amount, measure, btnTxt, msg) {
//        var html="<li id="+ id + "_" + idx + " class='menuItem'><div class='text_area'>";
//        if(msg && amount<=0) {
//            html+="<span class='basicText'>" + msg + "</span>";
//        }else {
//            html+="<span class='amountArea'>"+
//            "<span class='amountText'>" + addComma(amount) + "</span>"+
//            "<span class='measureText'>" + measure + "</span>"+
//            "</span>";
//        }if(btnTxt) {
//            html+="<span class='btnText'>" + btnTxt + "</span>"
//        }
//        html+= "<img src='" + modulePath + "resource/image/main/my_menu_box_" + (idx+1) + (language=="eng"?"_eng":"") + ".png' class='baseBox'>" +
//        "<img src='" + modulePath + "resource/image/main/my_menu_new.png' class='baseNew newIcon'>" +
//        "<img src='" + modulePath + "resource/image/main/my_menu_box_" + (idx+1) + (language=="eng"?"_eng":"") + "_f.png' class='focusBox'>"+
//        "<img src='" + modulePath + "resource/image/main/my_menu_new_f.png' class='focusNew newIcon'>" +
//        "</div>" +
//        "<div class='focus_line'/>" +
//        "</li>";
//        return html;
//    }
//
//    function addComma(num){
//        num = String(num);
//        return num.replace(/(\d)(?=(?:\d{3})+(?!\d))/g,"$1,");
//    }
//
//    this.isLastFocused = function () { return focus === 4; };
//    this.isFirstFocused = function () { return focus === 0; };
//
//    this.onFocused = function(keyCode) {
//        if(keyCode===KEY_CODE.BLUE) focus = 4;
//        else focus = 0;
//        focusing(focus);
//    };
//
//    this.onBlured = function() {
//        unfocusing(focus);
//    };
//
//    var focusing = function(index) {
//        _$("#" + id + "_" + index).addClass("focus")
//    };
//
//    var unfocusing = function(index) {
//        _$("#" + id + "_" + index).removeClass("focus")
//    };
//
//    this.onKeyAction = function(keyCode) {
//        switch(keyCode) {
//            case KEY_CODE.LEFT:
//                if(focus>0) {
//                    unfocusing(focus);
//                    focus = HTool.getIndex(focus, -1, 5);
//                    focusing(focus);
//                    return true;
//                } return false;
//            case KEY_CODE.RIGHT:
//                if(focus<4) {
//                    unfocusing(focus);
//                    focus = HTool.getIndex(focus, 1, 5);
//                    focusing(focus);
//                    return true;
//                } return false;
//            case KEY_CODE.ENTER:
//                if(focus==0) {
//                    try {
//                        NavLogMgr.collectFHMenu(keyCode, NLC.MENU00700.id, NLC.MENU00700.name, "", "");
//                    } catch(e) {}
//                    if(hasUpdateMail) {
//                        StorageManager.ps.save(StorageManager.KEY.INBOX_LAST_READ_TIME, Message_last_dt + "|" + noticeVersion);
//                        hasUpdateMail = false;
//                    }
//
//                    LayerManager.activateLayer({
//                        obj: {
//                            id: "MyHomeMailbox",
//                            type: Layer.TYPE.NORMAL,
//                            priority: Layer.PRIORITY.NORMAL,
//                            params: {
//                                menuName: HTool.getMenuName(menuData.children?menuData.children[0]:null)
//                            }
//                        },
//                        moduleId: "module.family_home",
//                        visible: true
//                    });
//                } else {
//                    try {
//                        if (focus == 1) NavLogMgr.collectFHMenu(keyCode, NLC.T000000000008.id, NLC.T000000000008.name, "", "");
//                        else if (focus == 2) NavLogMgr.collectFHMenu(keyCode, NLC.T000000000010.id, NLC.T000000000010.name, "", "");
//                        else if (focus == 3) NavLogMgr.collectFHMenu(keyCode, NLC.T000000000009.id, NLC.T000000000009.name, "", "");
//                        else if (focus == 4) NavLogMgr.collectFHMenu(keyCode, NLC.T000000000011.id, NLC.T000000000011.name, "", "");
//                    } catch(e) {}
//
//                    AppServiceManager.changeService({
//                        nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
//                        obj: {
//                            type: CONSTANT.APP_TYPE.MULTICAST,
//                            param: getLocator(focus)
//                        }
//                    });
//                } return true;
//        } return false;
//    };
//
//    function getLocator(focus) {
//        switch (focus) {
//            case 1: //TV쿠폰
//                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.TV_COUPON.LOCATOR.LIVE;
//                else return CONSTANT.TV_COUPON.LOCATOR.BMT;
//            case 2: //TV포인트
//                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.TV_POINT.LOCATOR.LIVE;
//                else return CONSTANT.TV_POINT.LOCATOR.BMT;
//            case 3: //kT멤버십
//                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.STAR_POINT.LOCATOR.LIVE;
//                else return CONSTANT.STAR_POINT.LOCATOR.BMT;
//            case 4: //콘텐츠 이용권
//                if(KTW.RELEASE_MODE=="LIVE") return CONSTANT.CONTENT_COUPON.LOCATOR.LIVE;
//                else return CONSTANT.CONTENT_COUPON.LOCATOR.BMT;
//        }
//
//    }
//
//    this.getHeight = function() {
//        return 244;
//    };
//
//    this.init();
//};

window.myMenu_pg = function (menuId, menuData) {
    var instance = this;

    var id = menuId;
    var focus;
    var dom = _$("<div/>", {id: id, class: 'myMenu_pg'});
    var language;

    this.init = function () {
        var list = _$("<ul/>", {class: 'menuList'});
        dom.html(list);
        list.append(getItem(0, 0, "P", language == "eng" ? "Membership" : "KT 멤버십")); // KT 멤버쉽
        list.append(getItem(1, 0, "P", language == "eng" ? "TV Point" : "TV포인트"));  // TV 포인트
        list.append(getItem(2, 0, "원", language == "eng" ? "TV Coupon" : "TV쿠폰"));   // TV 쿠폰
        list.append(getItem(3, 0, "개", language == "eng" ? "VOD Voucher" : "콘텐츠이용권")); // 콘텐츠 이용권
    };

    this.update = function () {
        console.log("update()");

        var l = MenuDataManager.getCurrentMenuLanguage();
        0 == UTIL.isValidVariable(l) && (l = "kor");
        language = l;

        _$("<ul/>", {class: 'menuList'}).empty();
        instance.init();

        HTool.getPoint(true, function (data) {
            instance.setData(data);
        });
    };

    this.getView = function () {
        return dom;
    };

    this.setData = function (data) {
        //KT멤버십 포인트 Setting
        if (data.UsrDiv == "R") {
            setItem(0, data.StarPoint, "P", "조회");
        }
        else {
            setItem(0, "미가입", "", "조회");
        }

        //TV포인트 Setting
        if (data.TvPointYn == "Y") {
            setItem(1, data.TvPoint, "P", "충전/관리");
        }
        else {
            setItem(1, "미가입", "", "충전/관리");
        }

        // TV 쿠폰
        setItem(2, (data.TvMoney || data.KtCoupon) ? ((data.TvMoney - 0) + (data.KtCoupon - 0)) : 0, "원", "구매/등록");

        //콘텐츠 이용권 Setting
        setItem(3, data.NumOfCoupon ? data.NumOfCoupon : 0, "개", "구매/등록");
    };

    function setItem(idx, amount, measure) {
        var item = dom.find("#" + id + "_" + idx);
        item.find('.amountText').text(addComma(amount));
        item.find('.measureText').text(measure);
    }

    function getItem(idx, amount, measure, msg) {
        var html = "<li id=" + id + "_" + idx + " class='menuItem'>" + "<span class='bar'></span>" + "<span class='measureText'>" + measure + "</span>" + "<span class='amountText'>" + addComma(amount) + "</span>" + "<span class='basicText'>" + msg + "</span>" + "</li>";
        return html;
    }

    function addComma(num) {
        num = String(num);
        return num.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, "$1,");
    }

    this.isLastFocused = function () {
        return focus === 4;
    };
    this.isFirstFocused = function () {
        return focus === 0;
    };

    this.onFocused = function (keyCode) {
        if (keyCode === KEY_CODE.BLUE) focus = 4;
        else focus = 0;
        focusing(focus);
    };

    this.onBlured = function () {
        unfocusing(focus);
    };

    var focusing = function (index) {
        _$("#" + id + "_" + index).addClass("focus")
    };

    var unfocusing = function (index) {
        _$("#" + id + "_" + index).removeClass("focus")
    };

    // [si.mun] myMenu_pg 는 key 를 전달 받지 않는다.
    // 때문에 무조건 false를 return
    this.onKeyAction = function (keyCode) {
        return false;
    };

    // [si.mun] myMenu_pg 는 key 를 전달 받지 않기 때문에
    // height 의 크기는 중요하지 않다.
    this.getHeight = function () {
        return 0;
    };

    this.init();
};