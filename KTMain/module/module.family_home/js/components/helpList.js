/**
 * Created by ksk91_000 on 2017-05-05.
 */
if(!component) var component = {};

component.helpList = function(menuId) {
    var componentId = menuId;
    var div = _$("<div/>", {id:componentId});
    var contentList;
    var focus = 0, curPage = 0;
    var indicator = new Indicator(1);

    this.create = function(data) {
        this.setData(data);
    };

    function constructHTML() {
        function makeItem(item) {
            var html = "<tr id='item_" + item.itemId + "'>" +
                "<td class='imgArea'>" +
                "<img src='" + item.imgUrl + "'>" +
                "</td>" +
                "<td>" +
                "<span class='title'>" + item.itemName + "</span>" +
                "<span class='description'>" + item.synopsis + "</span>" +
                "</td>";
            html += "</tr>";
            return _$(html);
        }

        var html = _$("<div/>", {id: 'listArea'});
        var table;
        for (var i = 0; i < contentList.length; i++) {
            if (i % 4 == 0) html.append(table = _$("<table/>"));
            table.append(makeItem(contentList[i]));
        }

        div.append(html);
        indicator.setSize(Math.ceil(contentList.length/4));
    }

    this.setData = function(_contentList) {
        contentList = _contentList;
        indicator = new Indicator(1);
        constructHTML();
        div.append(indicator.getView()).append(indicator.getPageTransView());
        setFocus(focus);
    };

    function setFocus(newFocus) {
        if (Math.floor(focus / 4) != Math.floor(newFocus / 4)) setPage(Math.floor(newFocus / 4));

        div.find("#item_" + contentList[focus].itemId).removeClass("focus");
        focus = newFocus;
        div.find("#item_" + contentList[focus].itemId).addClass("focus");
    }

    function setPage(page) {
        curPage = page;
        div.find("#listArea").css("-webkit-transform", 'translateY(-' + page * 977 + "px)");
        indicator.setPos(page);
    }

    this.getView = function() {
        return div;
    };

    this.onKeyAction = function(keyCode) {
        if(indicator.isFocused()) return onKeyForIndicator(keyCode);
        switch (keyCode) {
            case KEY_CODE.UP :
                setFocus(HTool.getIndex(focus, -1, contentList.length));
                return true;
            case KEY_CODE.DOWN :
                setFocus(HTool.getIndex(focus, 1, contentList.length));
                return true;
            case KEY_CODE.RED :
                if(focus%4===0) setFocus(HTool.getIndex(curPage, -1, Math.ceil(contentList.length/4))*4);
                else setFocus(curPage*5);
                return true;
            case KEY_CODE.BLUE :
                if(focus%4===3 || focus===contentList.length-1) setFocus(Math.min(contentList.length-1, HTool.getIndex(curPage, 1, Math.ceil(contentList.length/4))*4 + 3));
                else setFocus(Math.min(contentList.length-1, curPage*4+3));
                return true;
            case KEY_CODE.LEFT:
                if(contentList.length>4) {
                    div.find("#item_" + contentList[focus].itemId).removeClass("focus");
                    indicator.focused();
                } else {
                    LayerManager.historyBack();
                }return true;
            case KEY_CODE.ENTER :
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, catId, contentList[focus].itemId, "01");
                } catch(e) {}
                openDetailLayer(catId, contentList[focus].itemId, "01");
                return true;
            case KEY_CODE.BACK :
                LayerManager.historyBack();
                return true;
            default:
                return false;
        }
    };

    function onKeyForIndicator(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                setPage(HTool.getIndex(curPage, -1, Math.ceil(contentList.length/4)));
                return true;
            case KEY_CODE.DOWN:
                setPage(HTool.getIndex(curPage, 1, Math.ceil(contentList.length/4)));
                return true;
            case KEY_CODE.RIGHT:
            case KEY_CODE.ENTER:
                setFocus(curPage*4);
                indicator.blurred();
                return true;
            case KEY_CODE.LEFT:
            case KEY_CODE.BACK:
                LayerManager.historyBack();
                return true;
            case KEY_CODE.BLUE:
                setFocus(curPage*4);
                indicator.blurred();
                return true;
            case KEY_CODE.RED:
                setFocus(Math.min(contentList.length-1, curPage*4+3));
                indicator.blurred();
                return true;
        }
    }

    this.focused = function() {
        div.removeClass("blurred");
    };

    this.blurred = function() {
        div.addClass("blurred");
    };
};