/**
 * Created by ksk91_000 on 2016-06-30.
 */
if(!component) var component = {};

component.noticeList = function(id, data) {
    var componentId = id;
    var dom = _$("<div/>", {id:componentId});
    var maxPage;
    var itemList = [];
    var focus, page, dispPage;
    var indicator = new Indicator(1);

    function createDom() {
        var div = _$("<div/>", {class: "content_area"});
        var wrapper = _$("<div/>", {class: "scrollWrapper"});
        for (var i = 0; i < 6 ; i++) wrapper.append(getItemView());
        div.append(wrapper);
        dom.append(_$("<div/>", {class: "focus_line"}));
        dom.append(div);
        dom.append(indicator.getView()).append(indicator.getPageTransView());
    }

    this.setData = function(data) {
        if (data.length > 0) {
            createDom();
            maxPage = Math.ceil(data.length/4);
            dom.attr({class:"noticeList mailbox"});
            for(var i=0; i<data.length; i++)
                itemList.push(new Item(data[i]));
            page = 0;
            indicator.setSize(maxPage, page);
            setPageData(0);
            setFocus(0);
        } else {
            dom.empty();
            dom.attr({class:"noNotice mailbox"});
            dom.html("<div class='no_item_area'><img src='" + modulePath +"resource/image/icon_noresult.png'><span>알림이 없습니다</span></div>");
        }
    };

    function setPageData(page) {
        if(dispPage==page) return;
        var j = 0;
        for(var i=page*4; i<itemList.length && j<6; i++, j++) {
            itemList[i].setDiv(dom.find(".item").eq(j).removeClass("hiddenItem"), i+1);
        }for(; j<6; j++) {
            dom.find(".item").eq(j).addClass("hiddenItem");
        }
        dispPage = page;
    }

    this.getView = function() {
        return dom;
    };

    var getItemView = function() {
        return "<div class='item'><div class='index'><span></span></div>"+
            "<div class='content'>"+
            "<span class='mainTitle'></span>"+
            "<span class='subTitle'></span>"+
            "<span class='extraDesc'>" +
                "<span class='remain_text'></span>" +
                "<span class='remain_date'></span>" +
            "</span>"+
            "</div>"+
            "<div class='btnArea'><div class='date'/><div class='btn'>바로가기</div></div>" +
            "</div>";
    };

    function getDateStr(dateStr) {
        return dateStr.substr(0, 4) + "." + dateStr.substr(4, 2) + "." + dateStr.substr(6,2);
    }

    function changeFocus(amount) {
        setFocus(HTool.getIndex(focus, amount, itemList.length));
    }

    function setFocus(_focus) {
        dom.find(".item.focus").removeClass("focus");
        focus = _focus;
        dom.find(".item").eq(focus%4).addClass("focus");
        dom.find(".focus_line").css("-webkit-transform", "translateY(" + ((focus%4)*210) + "px)");
        setPage(Math.floor(focus/4));
    }

    function changePage(amount) {
        if(page + amount < 0) setPage(maxPage - 1);
        else if(page + amount == maxPage) setPage(0);
        else setPage(page + amount);
    }

    function setPage(_page) {
        page = _page;
        setPageData(page);
        indicator.setPos(page);
    }

    this.onKeyAction = function(keyCode) {
        //if(indicator.isFocused()) return onKeyForIndicator(keyCode);
        switch(keyCode) {
            case KEY_CODE.LEFT:
                dom.find(".item.focus").removeClass("focus");
                dom.find(".focus_line").hide();
                return false;
                //if(maxPage>1) {
                //    indicator.focused();
                //    dom.find(".item.focus").removeClass("focus");
                //    dom.find(".focus_line").hide();
                //    return true;
                //}else return false;
            case KEY_CODE.RIGHT:
                return true;
            case KEY_CODE.UP:
                changeFocus(-1);
                return true;
            case KEY_CODE.DOWN:
                changeFocus(1);
                return true;
            case KEY_CODE.RED:
                if (maxPage === 1) {
                    return true;
                }

                if(focus%4===0) setFocus(HTool.getIndex(page, -1, maxPage)*4);
                else setFocus(page*4);
                //if(indicator.isFocused()) changePage(HTool.getIndex(page, -1, maxPage));
                //else if(focus%4===0) setFocus(HTool.getIndex(page, -1, maxPage)*4);
                //else setFocus(page*4);
                return true;
            case KEY_CODE.BLUE:
                if (maxPage === 1) {
                    return true;
                }

                if(focus%4===3 || focus===itemList.length-1) setFocus(Math.min(itemList.length-1, HTool.getIndex(page, 1, maxPage)*4 + 3));
                else setFocus(Math.min(itemList.length-1, page*4+3));
                //if(indicator.isFocused()) changePage(HTool.getIndex(page, 1, maxPage));
                //else if(focus%4===3 || focus===itemList.length-1) setFocus(Math.min(itemList.length-1, HTool.getIndex(page, 1, maxPage)*4 + 3));
                //else setFocus(Math.min(itemList.length-1, page*4+3));
                return true;
            case KEY_CODE.ENTER:
                itemList[focus].enterKeyAction();
                return true;
        }
    };

    this.focused = function() {
        if (data.length > 0){
            setFocus(page*4);
            return true;
        } else return false;
    };

    this.blurred = function() {
        //indicator.blurred();
        dom.find(".focus_line").show();
        if (data.length > 0) {
            dom.find(".item.focus").removeClass("focus");
            setPage(0);
        }
    };
    
    function hasAction(actionType) {
        switch (actionType) {
            case "MOVE_TO_CH":
            case "RUN_BOUND":
            case "RUN_PUSH_APP":
            case "SHOW_CONTENTDETAIL":
            case "PLAY_VOD":
            case "SHOW_CATEGORY":
                return true;
            default:
                return false;
        }
    }

    var Item = function (data) {
        this.getData = function() { return data };
        this.setDiv = function (div, index) {
            if (index < 10) {
                div.find(".index span").text("0" + index);
            } else {
                div.find(".index span").text(index);
            }
            div.find(".mainTitle").text(data.req_nm);
            div.find(".subTitle").text(data.req_cont);

            if (data && data.comment && data.comment.length === 0) { // data.comment 가 빈 문자열 일 때
                div.find(".remain_text").text("");
                div.find(".remain_date").text("");
            } else {
                var parseStrIdx = data.comment.indexOf(":");

                if (parseStrIdx < 0) { // ":" 이 존재하지 않으면
                    div.find(".remain_text").text(data.comment);
                    div.find(".remain_date").text("");
                } else { // ":" 이 존재한다면
                    var comment = data.comment.split(':');
                    div.find(".remain_text").text(comment[0] + " : ");
                    div.find(".remain_date").text(comment[1]);
                }
            }

            div.find(".date").text(getDateStr(data.start_dt));
            div.find(".btn").toggle(hasAction(data.action_type));
        };
        this.enterKeyAction = function () {
            switch (data.action_type) {
                case "MOVE_TO_CH":  //채널로 이동
                    goToChannelNum(CONSTANT.IS_OTS?data.action_ots:data.action_otv);
                    break;
                case "SHOW_ALARM": //알람목록만 보여줌
                    break;
                case "RUN_BOUND":  //바운드 앱 구동 (바운드 채널로 이동)
                    goToDataChannel(data.action_value);
                    break;
                case "RUN_PUSH_APP": //차일드 앱 호출
                    MessageManager.sendMessage({
                        'method': 'spc_runApp',
                        'to': CONSTANT.APP_ID.SMART_PUSH,
                        'from': CONSTANT.APP_ID.HOME,
                        'req_id': data.action_value
                    }); break;
                case "SHOW_CONTENTDETAIL":  //VOD상세로 이동
                    /* 성인콘텐츠일 경우, 성인인증 팝업 띄움 */
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, null, data.action_value, "01");
                    } catch(e) {}
                    openDetailLayer(null, data.action_value, "01");
                    break;
                case "PLAY_VOD":  //VOD를 바로 재생
                    getVodModule(function (vodModule) {
                        vodModule.execute({
                            method: "watchContent",
                            params: {
                                callback : null,
                                cat_id : null,
                                const_id : data.action_value,
                                req_cd: "01"
                            }
                        });
                    });
                    break;
                case "SHOW_CATEGORY": //카테고리로 이동
                    var menuData = MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(),
                        allSearch: false,
                        cbCondition: function (menu) { if(menu.id==target) return true; }
                    })[0];
                    if(menuData != undefined) {
                        MenuServiceManager.jumpMenu({menu: menuData});
                    } else {
                        showToast("잘못된 접근입니다");
                    }
                    break;
            }
        }
    };

    function getVodModule(callback) {
        var vodModule = ModuleManager.getModule("module.vod");
        if(!vodModule) ModuleManager.loadModule( {
            moduleId: "module.vod",
            cbLoadModule: function (success) {
                if (success) callback(ModuleManager.getModule("module.vod"));
            }
        }); else {
            callback(vodModule);
        }
    }

    function goToChannelNum(number) {
        var channelObj = oipfAdapter.navAdapter.getChannelByNum(number-0);
        if(!channelObj) return;
        if(StateManager.isVODPlayingState()) {
            getVodModule(function (vodModule) {
                vodModule.execute({
                    method: "stopVodWithChannelSelection",
                    params:{
                        channelObj: channelObj
                    }
                })
            })
        } else {
            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.TV_VIEWING,
                obj: {
                    channel: channelObj,
                    tune: "true"
                },
                forced: true
            });
        }
    }

    function goToDataChannel(number) {
        var channelObj = oipfAdapter.navAdapter.getChannelByNumOnAll(number-0);

        AppServiceManager.changeService({
            nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
            obj: {
                type: CONSTANT.APP_TYPE.DATA_CHANNEL,
                param: channelObj
            }
        });
    }

    this.setData(data);
};