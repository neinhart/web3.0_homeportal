window.promo_welcome = function(componentId) {
    var id = componentId;

    this.getView = function() {
        var html= "<div id='" + id + "' class='promo_welcome'>" +
                "<img src='resource/image/img_myhome_no_promo.png'>"+
                "<div class='dim'>VOD 이용 방법 보기</div>"
            "</div>";
        return html;
    }

    this.isLastFocused = function () { return true; };
    this.isFirstFocused = function () { return true; };

    this.onFocused = function() {
        focusing();
    };

    this.onBlured = function() {
        unfocusing();
    };

    var focusing = function() {
        document.getElementById(id).classList.add("focus");
    };

    var unfocusing = function() {
        document.getElementById(id).classList.remove("focus");
    };

    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.ENTER:
                gotoLocator(SubHomeDataManager.getMyMenu().find(function (a) { return a.position==0; }));
                return true;
        } return false;
    };

    this.getHeight = function() {
        return 600;
    };
}