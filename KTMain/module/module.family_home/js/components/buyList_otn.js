/**
 * 모바일 공유목록 (우리집 맞춤 TV > 구매내역 보기 > 모바일 공유목록)
 * Created by ksk91_000 on 2016-07-01.
 */
if (!component) var component = {};

component.buyList_otn = function (id) {
    var componentId = id;
    var listData;
    var cnt = 0;
    var focus = 0;
    var page = 0;
    var indicator = new Indicator(1);
    var authAdultBox;
    var isPaired;
    var isLoaded = false;
    var isLoading = false;
    var isFocused = false;
    var isLoadWating = false;

    var instance = this;
    var NUMBEROFCONTENTINPAGE = 5;// 페이지에 노출되어야할 컨텐츠 수

    var dom = _$("<div id='" + componentId + "' class='buyList'>" +
        "<div id='amountTxt'>총 0건</div>" +
        "<div class='item_area'>" +
            "<div class='focus_line'/>" +
            "<table class='item_list'/>" +
        "</div>" +
    "</div>");
    dom.append(indicator.getView()).append(indicator.getPageTransView());
    dom.append("<div id='info_text'>※ 모바일 공유목록은 TV에서 삭제할 수 없습니다</div>");

    this.getView = function () {
        return dom;
    };

    this.update = function (callback) {
        if (isLoading) {
            return;
        }

        isLoading = true;

        var res = StorageManager.ps.load(StorageManager.KEY.OTN_PARING);

        isPaired = res == 'Y';

        if (isPaired) {
            LayerManager.startLoading({preventKey: true});
            myHome.SMLSManager.getOTNBuyShareHistoryList(function (result, data) {
                if (result) {
                    listData = data.item;
                    cnt = parseInt(data.ITEM_COUNT);
                } else {
                    listData = [];
                    cnt = 0;
                }

                updateView();
                isLoaded = true;
                isLoading = false;
                LayerManager.stopLoading();

                if (callback) {
                    callback();
                }
            }, DEF.SAID);
        } else {
            console.log("4th isPaired: " + isPaired);
            updateView();
            isLoaded = true;
            isLoading = false;
            LayerManager.stopLoading();

            if (callback) {
                callback();
            }
        }
    };

    function updateView() {
        if (cnt === 0) {
            dom.addClass("noItem");
        } else {
            dom.removeClass("noItem");
        }

        dom.find(".item_area").empty();
        if (authAdultBox) {
            authAdultBox.create(function () {
                authAdultBox = null;
                dom.find("#" + componentId).removeClass("blurred");
                updateView();
                instance.parent.viewMgr.reChangeFocus();
            });
            dom.find(".item_area").append(authAdultBox.getView());
            dom.find(".item_area").append(_$("<div/>", {class: 'tip_text'}).html("'TV에 내폰 연결'(ch.742) 메뉴에서 올레 tv 모바일앱과 올레 tv를 연결하시면<br>구매한 VOD를 폰에서도 추가 결제 없이 시청하실 수 있습니다"));
        } else {
            // gyuh 2017-05-21
            if (cnt <= 0) {
                if (isPaired) {
                    dom.find(".item_area").append(getNoDataView("모바일 공유 목록이 없습니다"));
                } else {
                    dom.find(".item_area").append(getNoDataView("마이메뉴 > TV에 내 폰 연결을 설정하면 TV와 모바일에서<br>olleh tv 콘텐츠를 이용할 수 있습니다"));
                }
            } else {
                dom.find(".item_area").append("<div class='focus_line'/><table class='item_list'/>");
                dom.find("#amountTxt").text("총 " + cnt + "개");
                indicator.setSize(Math.ceil(cnt / 5));

                for (var i = 0; i < NUMBEROFCONTENTINPAGE; i++) {
                    dom.find(".item_list").append(getItemView());
                }

                dom.find(".rightArea").addClass("otn");
                changePage(0);
                setFocus(0);
            }
        }

        if (isFocused) {
            instance.focused();
        } else {
            instance.blurred();
        }
    }

    function getNoDataView(text) {
        var div = _$("<div/>", {class: 'noItemView'});
        div.append(
            _$("<img>", {src: modulePath + "resource/image/icon_noresult.png"}),
            _$("<span>", {class: "text"}).html(text));

        return div;
    }

    function getItemView() {
        var html = "<tr>" +
            "<td>" +
            "<div class='Title'>" +
            "<span/>" +
            "</div>" +
            "<div class='info'/>" +
            "<div class='remain'/>" +
            "</td>" +
            "<td class='rightArea'>" +
            "<span class='phoneNum'/>" +
            "</td>" +
            "</tr>";

        return _$(html);
    }

    function setItemView(div, title, price, date, remainTime, saId, resol_cd, adult_yn) {
        if (adult_yn == 'Y') {
            title = title.substr(0, 1) + "******************************************************".substr(0, title.length);
        }

        div.find(".Title").removeClass("hd uhd").toggleClass("hd", resol_cd == "HD").toggleClass("uhd", resol_cd == "UHD").find("span").text(title);
        div.find(".info").html(HTool.addComma(price) + "원 <co>l</co> " + parseDateString(date));
        div.find(".remain").text(getRemainTime(remainTime));
        div.find(".phoneNum").text(makeSecretId(saId));
        div.removeClass("hiddenItem");
    }

    function setFocus(newFocus) {
        dom.find("table tr").eq(focus % 5).removeClass('focus');
        focus = newFocus;
        dom.find("table tr").eq(focus % 5).addClass('focus');

        if (Math.floor(focus / 5) != page) {
            changePage(Math.floor(focus / 5));
        }

        dom.find(".focus_line").css("-webkit-transform", "translateY(" + focus % 5 * 170 + "px)");
    }

    function changePage(newPage) {
        page = newPage;
        indicator.setPos(page);

        for (var i = 0; i < NUMBEROFCONTENTINPAGE; i++) {
            var idx = i + (page * 5);

            if (listData[idx]) {
                setItemView(dom.find("table tr").eq(i),
                    listData[idx].ITEM_NM,
                    listData[idx].BUY_AMOUNT,
                    listData[idx].BUY_DT,
                    listData[idx].EFCT_DAY,
                    listData[idx].SA_ID,
                    listData[idx].RESOL_CD,
                    listData[idx].ADULT_YN);
            } else {
                dom.find("table tr").eq(i).addClass("hiddenItem")
            }
        }
    }

    this.show = function () {
        var res = StorageManager.ps.load(StorageManager.KEY.OTN_PARING);
        if (isPaired && res == "N") {
            isLoaded = false;
        } else if (!isPaired && res == "Y") {
            isLoaded = false;
        }

        if (!isLoaded) {
            this.update(function () {
                if (isLoadWating) {
                    instance.parent.viewMgr.reChangeFocus(true);
                }
            });
        }
    };

    this.focused = function () {
        isFocused = true;
        isLoadWating = false;

        if (!isLoaded) {
            isLoadWating = true;
            return;
        }
        if (authAdultBox) {
            dom.addClass("focused");
            return myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        } else if (cnt <= 0) return myHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        else {
            setFocus(focus);
            return myHome.ViewManager.FOCUS_MODE.FOCUS;
        }
    };

    this.blurred = function () {
        isFocused = false;
        setFocus(0);
        dom.removeClass("focused");
        dom.find("tr.focus").removeClass("focus");
    };

    this.onKeyAction = function (keyCode) {
        if (authAdultBox) return authAdultBox.onKeyAction(keyCode);
        else switch (keyCode) {
            case KEY_CODE.UP:
                setFocus(HTool.getIndex(focus, -1, listData.length));
                return true;
            case KEY_CODE.DOWN:
                setFocus(HTool.getIndex(focus, 1, listData.length));
                return true;
            case KEY_CODE.RED:
                if (indicator.isFocused()) {
                    indicator.blurred();
                    setFocus(page * 5);
                    dom.find(".focus_line").show();
                } else if (focus % 5 === 0) {
                    setFocus(HTool.getIndex(page, -1, Math.ceil(listData.length / 5)) * 5);
                } else {
                    setFocus(page * 5);
                }
                return true;
            case KEY_CODE.BLUE:
                if (indicator.isFocused()) {
                    indicator.blurred();
                    setFocus(Math.min(listData.length - 1, page * 5 + 4));
                    dom.find(".focus_line").show();
                } else if (focus % 5 === 4 || focus === listData.length - 1) {
                    setFocus(Math.min(listData.length - 1, HTool.getIndex(page, 1, Math.ceil(listData.length / 5)) * 5 + 4));
                } else {
                    setFocus(Math.min(listData.length - 1, page * 5 + 4));
                }
                return true;
            case KEY_CODE.ENTER:
                if (!indicator.isFocused()) {
                    try {
                        NavLogMgr.collectJumpVOD(NLC.JUMP_START_SUBHOME, "N", listData[focus].ITEM_ID, "24");
                    } catch (e) {
                    }
                    openDetailLayerImpl(listData[focus]);
                } else {
                    this.onKeyAction(KEY_CODE.RIGHT);
                }
                return true;
            case KEY_CODE.LEFT:
                if (indicator.isFocused() || listData.length <= 5) {
                    setFocus(0);
                    //indicator.blurred();
                    dom.find(".focus_line").show();
                    return false;
                }
            case KEY_CODE.BACK:
                setFocus(0);
                return false;
        }
    };

    function openDetailLayerImpl(data) {
        if (data.SERS_YN === "Y") {
            var seriesId = data.PKG_YN === "Y" ? data.ITEM_ID : data.SERIES_ID;
            LayerManager.startLoading();
            getCateInfo(function (res, data2) {
                if (res && !!data2 && data2.catId) {
                    LayerManager.stopLoading();
                    openDetailLayer(data2.catId, data.ITEM_ID, "24", {cateInfo: data2, useUseYn: !isRemainTime(data)});
                } else {
                    myHome.amocManager.exchangeItemId(function (res, data3) {
                        LayerManager.stopLoading();
                        // 남은 기간이 종료된 경우 useYn값에 따라 노출/미노출을 결정.
                        // 남은기간이 있으면 무조건 노출
                        openDetailLayer(data3.vrCatId, data.ITEM_ID, "24", {useUseYn: !isRemainTime(data)});
                    }, seriesId, 1, "Y", data.RESOL_CD, DEF.SAID);
                }
            }, data.VR_CTGR_ID, DEF.SAID);

            function getCateInfo(callback, vr_ctgr_id, said) {
                if (!vr_ctgr_id || vr_ctgr_id.length == 1) {
                    callback(true, {});
                } else {
                    myHome.amocManager.getCateInfoW3(callback, vr_ctgr_id, said);
                }
            }
        } else {
            openDetailLayer("N", data.ITEM_ID, "24", {forced: isRemainTime(data), noCateForced: true});
        }
    }

    function isRemainTime(data) {
        return (data.EFCT_DAY || "-").indexOf("-") < 0;
    }

    function getRemainTime(str) {
        if(str=="PERM") {
            return "해지 전까지";
        }

        var a = str.split('D'), b, c;

        if (a[0] < 0 || str.substr(0, 1) == "-") {
            return "시청기간 만료";
        } else if (a[0] >= 365) {
            return "해지 전까지";
        } else if (a[0] > 1) {
            return a[0] + "일 남음";
        } else if (b = a[1].split('H'), a[0] == 1 || b[0] > 0) {
            return ((a[0]*24)+(b[0]-0)) + "시간 남음";
        } else {
            c = b[1].split('M');
            if (c[0] == 0) { // duration 이 59초 이하일 땐 0D0H0M 으로 내려오는 데, 이런 경우는 1분으로 처리
                return "1분 남음";
            } else {
                return c[0] + "분 남음";
            }
        }
    }

    function makeSecretId(id) {
        id = decodeURIComponent(id);
        if (id.substr(0, 2) == "01" && (id.length == 11 || id.length == 10)) {
            return id.substr(0, id.length - 6) + "***" + id.substr(id.length - 3, 3);
        } else if (id.length > 3) {
            return id.substr(0, id.length - 3) + "***";
        } else {
            return "***";
        }
    }

    function parseDateString(str) {
        return str.substr(0, 4) + "." + str.substr(4, 2) + "." + str.substr(6, 2);
    }
};