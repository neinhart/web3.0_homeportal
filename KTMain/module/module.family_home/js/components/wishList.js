/**
 * 찜 목록 (우리집 맞춤 TV > 찜한목록(컴포넌트))
 * Created by ksk91_000 on 2016-10-07.
 */
window.wishList = function (menuId, menuData) {
    var componentId = menuId;
    var data;
    var length;
    var isMore;
    var isFocused = false;
    var instance = this;

    var div = _$("<div/>", {id: menuId});

    var focus;
    var type = 0; // 0: 리스트, 1: 데이터 없음.

    var maxPosterCnt = 7;

    this.update = function (options) {
        WishListManager.getWishListData(function (contentsData) {
            data = contentsData?(_$.isArray(contentsData) ? contentsData : [contentsData]):[];
            updateImpl();
        }, options&&options.resume);

        function updateImpl() {
            length = data.length;
            isMore = length > maxPosterCnt;
            constructDom();

            instance.onKeyAction = data.length > 0 ? instance.onKeyForList : instance.onKeyForInfoTxt;
            if(isFocused) {
                if(focus>data.length-1) focus = isMore?maxPosterCnt - 1:data.length-1;
                if(data.length>0) focusing(focus);
                else div.find("#" + componentId).addClass("focus");
            }
        }
    };
1
    var constructDom = function () {
        div.empty();
        div.append(_$("<span/>", {class: "componentTitle"}).html("<div/>" + HTool.getMenuName(menuData)));
        if (data.length > 0) {
            var list = _$("<ul/>", {id: componentId + "_contents", class: "sub_posterList"});
            div.append(list);

            for (var i = 0; i < length && i < (isMore ? maxPosterCnt - 1 : maxPosterCnt); i++) {
                list.append(PosterFactory.getVerticalPoster(componentId + "_" + i, {
                    imgUrl: data[i].imgUrl,
                    contsName: data[i].itemName,
                    wonYn: data[i].wonYn,
                    prInfo: data[i].prInfo,
                    resolCd: data[i].resolCd,
                    newHot: data[i].newHot,
                    adultOnlyYn: data[i].adultOnlyYn,
                    isHdrYn: data[i].isHdrYn,
                    wEvtImageUrlW: data[i].wEvtImageUrlW,
                    isDvdYn: data[i].isDvdYn,
                    mark: data[i].mark
                }));
            }
            if (isMore) {
                list.append(PosterFactory.getVerticalPoster_more(componentId + "_" + (maxPosterCnt - 1), {
                    imgUrl: data[i].imgUrl,
                    moreCnt: length
                }));
            }
        } else {
            makeNoContentArea();
        }
    };

    function makeNoContentArea() {
        div.append(_$("<div/>", {id: componentId, class: "infoText_wish"}));
        div.find("#" + componentId).append(_$("<span>", {class: 'text_1'}).text("찜한 목록이 없습니다"));
        div.find("#" + componentId).append(_$("<span>", {class: 'text_2'}).text("볼만한 콘텐츠를 찜 해보세요"));
        div.find("#" + componentId).append(_$("<img>", {src: modulePath + 'resource/image/img_myhome_no_pin.png'}));
        div.find("#" + componentId).append(_$("<div/>", {class: 'dim'}));
        div.find(".dim").text("무료영화관 바로가기");
    }

    this.getView = function () {
        return div;
    };

    this.isLastFocused = function () { if(data && data.length>0) return focus === Math.min(maxPosterCnt, length)-1; else return true; };
    this.isFirstFocused = function () {
        /**
         * [dj.son] [WEBIIIHOME-3652] data 가 없을 경우 항상 true 를 리턴하도록 수정
         */
        var result = false;

        if (data && data.length > 0) {
            if (focus === 0) {
                result = true;
            }
        }
        else {
            result = true;
        }

        return result;
    };

    this.onFocused = function (keyCode) {
        if(data && data.length>0) {
            if(keyCode===KEY_CODE.BLUE) focus = Math.min(maxPosterCnt, length) - 1;
            else focus = 0;
            focusing(focus);
        } else {
            div.find("#" + componentId).addClass("focus");
            _$("#MyHomeMain .context_menu_indicator").text("");
        } isFocused = true;
    };

    this.onBlured = function () {
        isFocused = false;
        if(data && data.length>0) {
            unfocusing(focus);
            div.find(".componentTitle").css("-webkit-transform", "");
        } else {
            div.find("#" + componentId).removeClass("focus");
        }
    };

    var focusing = function (index) {
        _$("#" + componentId + "_" + index).addClass("focus");
        if(isMore && index==(maxPosterCnt - 1)) _$("#MyHomeMain .context_menu_indicator").text("");
        else _$("#MyHomeMain .context_menu_indicator").text("찜해제");
        if(index==0) div.find(".componentTitle").css("-webkit-transform", "translateY(-13px)");
        else div.find(".componentTitle").css("-webkit-transform", "");
        setTextAnimation(index);
    };

    var unfocusing = function (index) {
        _$("#" + componentId + "_" + index).removeClass("focus");
        UTIL.clearAnimation(div.find(".textAnimating span"))
        div.find(".textAnimating").removeClass("textAnimating");
    };

    function setTextAnimation(focus) {
        UTIL.clearAnimation(div.find(".textAnimating span"));
        div.find(".textAnimating").removeClass("textAnimating");
        void div[0].offsetWidth;
        if(UTIL.getTextLength(data[focus].itemName, "RixHead L", 30)>215) {
            var posterDiv = div.find("#" + componentId + "_" + focus + " .posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.onKeyForList = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT:
                if(focus>0) {
                    unfocusing(focus);
                    focus = HTool.getIndex(focus, -1, Math.min(maxPosterCnt, length));
                    focusing(focus);
                    return true;
                }else return false;
            case KEY_CODE.RIGHT:
                if(focus<Math.min(maxPosterCnt, length)-1) {
                    unfocusing(focus);
                    focus = HTool.getIndex(focus, 1, Math.min(maxPosterCnt, length));
                    focusing(focus);
                    return true;
                } else return false;
            case KEY_CODE.ENTER:
                if(focus==(maxPosterCnt - 1) && length>maxPosterCnt) openMorePage();
                else {
                    if(data[focus].chgFlag == "D") {
                        WishListManager.removeContents([data[focus]], function (res) {
                            if(res) showAlert("편성 정보 삭제로 인해<br>콘텐츠가 자동 삭제되었습니다", "찜한 목록 삭제 안내");
                        });
                    }else {
                        if(data[focus].adultOnlyYn=="Y") {
                            // [WEBIIIHOME-3772] 성인 전용 콘텐츠인 경우 인증 팝업에 성인인증 비밀번호 문구 표시
                            openAdultAuthPopup(function () { openDetailLayerImpl(data[focus], true); }, true, "성인인증 비밀번호")
                        } else {
                            openDetailLayerImpl(data[focus], false);
                        }
                    }
                }return true;
            case KEY_CODE.CONTEXT:
                if(length<=maxPosterCnt || focus!=(maxPosterCnt - 1)) {
                    var that = this;
                    LayerManager.activateLayer({
                        obj: {
                            id: "EditModeDeletePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: false,
                            params: {
                                type: 0,
                                items: [data[focus]],
                                callback: function (res) {
                                    if (res) {
                                        WishListManager.removeContents([data[focus]], function (result) {
                                            if (result) showToast("VOD찜을 해제하였습니다");
                                        }, false);
                                    } LayerManager.historyBack();
                                }
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                } return true;
        } return false;
    };

    this.onKeyForInfoTxt = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.ENTER:
                try {
                    NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU01200.id, "무료영화관 바로가기", "", "");
                } catch(e) {}
                gotoLocator(SubHomeDataManager.getMyMenu().find(function (a) { return a.position==2; }));
                return true;
        }
        return false;
    };

    function openDetailLayerImpl(item, authComplete) {
        try { NavLogMgr.collectFHMenu(NLC.JUMP_START_SUBHOME, NLC.MENU01200.id, NLC.MENU01200.name, item.itemId, item.itemName); } catch(e) {}

        var catId = item.itemType == 2 ? item.categoryId:item.itemId;
        if(!catId || catId==="N" || catId==="X") {
            openDetailLayer(catId, item.itemId, "16", {isAuthorizedContent: authComplete});
        } else myHome.amocManager.getCateInfoW3(function (res, catData) {
            if(res) {
                if(!catData.catId) {
                    myHome.amocManager.getCategoryId("contsId=" + catId + "&saId=" + DEF.SAID + "&path=V&platformGubun=W", function (res, newData) {
                        if(res) {
                            if(newData.serviceYn=="Y"&&newData.categoryId) openDetailLayer(newData.categoryId, item.itemId, "16", {isAuthorizedContent: authComplete});
                            else HTool.openErrorPopup({message: ERROR_TEXT.ET004, title:"알림", button:"확인"});
                        } else {
                            extensionAdapter.notifySNMPError("VODE-00013");
                            HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                        }
                    });
                } else {
                    if(catData.catType!=="PkgVod"&&catData.seriesYn!=="Y") {
                        myHome.amocManager.getContentW3(function (res, contData) {
                            if(res) {
                                // if(contData.contsId) openDetailLayer(item.itemId, item.itemId, "16", {cateInfo:catData, contsInfo: contData});
                                // 2017.10.13 Lazuli
                                // 카테고리ID를 넘겨야되는데 itemId를 넘기고있어서 찜목록에서 진입한 컨텐츠들 중 연관메뉴의 최근 확인한 VOD에서 진입이 안되는 오류가 있음
                                // (최근 확인한 VOD에 같은 컨텐츠가 2개가 되는 경우도 생김)
                                // 다른 페이지의 openDetailLayerImpl 함수 참고해서 카테고리ID를 넘기도록 수정함
                                if(contData.contsId) openDetailLayer(catId, item.itemId, "16", {cateInfo:catData, contsInfo: contData, isAuthorizedContent: authComplete});
                                else openDetailLayer("N", item.itemId, "16", {isAuthorizedContent: authComplete});
                            } else {
                                extensionAdapter.notifySNMPError("VODE-00013");
                                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                            }
                        }, catId, item.itemId, DEF.SAID);
                    } else {
                        openDetailLayer(catId, item.itemId, "16", {cateInfo:catData, isAuthorizedContent: authComplete});
                    }
                }
            } else {
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
            }
        }, catId, DEF.SAID);
    }

    function openMorePage() {
        try {
            NavLogMgr.collectFHMenu(KEY_CODE.ENTER, NLC.MENU01200.id, "전체보기", "", "");
        } catch(e) {}
        LayerManager.activateLayer({
            obj: {
                id: "MyHomeWishList",
                type: Layer.TYPE.NORMAL,
                priority: Layer.PRIORITY.NORMAL,
                linkage: false,
                params: {menuName: HTool.getMenuName(menuData)}
            },
            moduleId: "module.family_home",
            new: false,
            visible: true
        })
    }

    this.show = function () {
        WishListManager.addUpdateTrigger(this.update);
    };

    this.hide = function () {
        WishListManager.removeUpdateTrigger(this.update);
    };

    this.onKeyAction = function (keyCode) {};

    this.getHeight = function () {
        return myHome.CONSTANT.COMP_TITLE_HEIGHT + myHome.CONSTANT.POSTER_HEIGHT - 50;
        //if (length > 0) return COMP_TITLE_HEIGHT + 431;
        //else return COMP_TITLE_HEIGHT + 435;
    };

    makeNoContentArea();
};