/**
 * 즐겨 찾는 메뉴
 * (우리집 맞춤 TV > 즐겨 찾는 메뉴)
 * Created by ksk91_000 on 2016-06-14.
 */
(function () {
    myHome.popup = myHome.popup || {};
    myHome.popup.favorMenu_popup = function favorMenuPopup(options) {
        Layer.call(this, options);
        var view = _$("<div/>", {class: "popup_contents"});

        var itemList = [];

        // 즐겨찾는 메뉴 유/무
        var noItem = false;

        var instance = this;

        var monthlyData;
        var cugData;
        var isLoaded = false;
        var deleteListIndex;

        var focus = 0;
        var btnFocus = 0;

        var isBtnFocus = false;

        this.init = function (cbCreate) {
            log.printDbg('init()');

            this.div.attr({class: "arrange_frame myhome gniPopup favorMenu"});
            this.div.append(view);

            view.append(_$("<div class='popup_title'>즐겨 찾는 메뉴</div>" + "<div class='itemList'>" + "<ul></ul>" + "</div>" + "<div class='button_area'>" + "<btn class='ok'>편집</btn>" + "<btn class='cancel'>닫기</btn>" + "</div>"));

            //view.addClass("arrange_frame myhome gniPopup favorMenu");
            //view.append("<span class='componentTitle'><div></div>즐겨 찾는 메뉴</span>");
            //view.append(_$("<div/>", {class:"favorMenu"}));
            //view.find(".favorMenu").append(_$("<ul/>"));

            if (cbCreate) {
                cbCreate(true);
            }
        };

        this.update = function () {
            isLoaded = false;
            this.show();
        }

        this.show = function () {
            log.printDbg('show()');
            Layer.prototype.show.call(this);

            if (isLoaded) {
                return;
            }

            cugData = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_CUG_INFO));
            myHome.amocManager.getMyCornerListW3(function (result, data) {
                unfocusBtn();
                itemList = [];
                if (result) {
                    monthlyData = data.cornerList;
                    var monthlyItem = new MonthlyPayItem(monthlyData);
                    if (monthlyItem.isAvailable()) itemList.push(monthlyItem);
                }

                if (cugData && cugData.cugList) itemList.push(new CUGItem(cugData));
                var menuList;

                FavorMenuManager.getFavorMenuData(function (favorMenuData) {
                    menuList = favorMenuData;
                    for (var i in menuList) {
                        if (itemList.length >= 6) break;
                        itemList.push(new Item(menuList[i]));
                    }

                    // var prevStateIsNoItem = noItem;
                    // if (itemList.length == 0) {
                    //     noItem = true;
                    //     itemList.push(new NoItemItem());
                    // }
                    // else noItem = false;

                    noItem = false;
                    // [si.mun] 여기까지 진행 되었을 때 itemList 가 비어있다면 즐겨찾는 메뉴가 존재하지 않습니다. 팝업 호출
                    if (itemList && itemList.length === 0) {
                        showNoFavorMenu();
                        setBtnFocus(0);
                        noItem = true;
                        return;
                    }

                    //itemList.splice(3, 0, new EditItem());
                    //height = 130*(Math.floor((itemList.length-1)/4)+1);

                    //view.find("div.favorMenu").css("height", height + "px");
                    var list = instance.div.find("ul");
                    list.empty();

                    // 메뉴가 있으면 메뉴 append
                    for (var i = 0; i < itemList.length; i++) {
                        list.append(itemList[i].getView());
                    }

                    // 메뉴가 없다면 빈 item append
                    for (var i = 6 - itemList.length; i > 0; --i) {
                        list.append((new emptyItem()).getView());
                    }

                    // 2017.10.13 Lazuli
                    // 즐겨찾는메뉴가 없는 상태에서 즐겨찾는 메뉴를 2개 이상 추가하게되면 noItem은 false고 현재 focus는 1이므로 최종적으로 focus가 1로 셋팅됨
                    // 이전 상태 여부를 체크하여, focus를 셋팅하도록 수정
                    //if (prevStateIsNoItem) focus = noItem?1:0;
                    //else focus = noItem?1:Math.max(0, Math.min(itemList.length-1, (focus||0)));

                    focus = 0;
                    focusing(focus);
                    isLoaded = true;
                    //if(isFocused) focusing(focus);
                    //if(cbUpdate) cbUpdate();
                });
            });
        };

        this.hide = function () {
            log.printDbg('hide()');
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function (keyCode) {
            var tempFocus = 0;

            log.printDbg('controlKey() - keyCode : ' + keyCode);
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (isBtnFocus) {
                        setBtnFocus(btnFocus ^ 1);
                    }
                    else if (!noItem) {
                        if (focus >= 0 && focus <= 2) { // 첫 행
                            tempFocus = HTool.getIndex(focus, -1, itemList.length >= 3 ? 3 : itemList.length);
                        }
                        else { // 두번 째 행
                            tempFocus = HTool.getIndex(focus, -1, itemList.length);
                            if (tempFocus < 3) {
                                tempFocus = itemList.length - 1;
                            }
                        }
                        setFocus(tempFocus);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (isBtnFocus) {
                        setBtnFocus(btnFocus ^ 1);
                    }
                    else if (!noItem) {
                        if (focus >= 0 && focus <= 2) { // 첫 행
                            tempFocus = HTool.getIndex(focus, 1, itemList.length >= 3 ? 3 : itemList.length);
                        }
                        else {
                            tempFocus = HTool.getIndex(focus, 1, itemList.length);
                            if (tempFocus === 0) {
                                tempFocus = 3;
                            }
                        }
                        setFocus(tempFocus);
                    }
                    return true;
                case KEY_CODE.UP:
                    if (!noItem) {
                        if (isBtnFocus) {
                            unfocusBtn();
                            tempFocus = focus;
                        }
                        else {
                            if (focus <= 2) { // 첫행에선 버튼으로 포커스가 이동하지 않도록
                                return true;
                            }
                            else {
                                tempFocus = focus % 3;
                            }
                        }
                        setFocus(tempFocus);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (!noItem) {
                        if (!isBtnFocus) { // 메뉴에 포커스가 되어있을 때만 포커스 변경
                            if (itemList.length <= 3) { // 첫 행에만 메뉴가 존재
                                setBtnFocus(btnFocus);
                                unfocusing(focus);
                            }
                            else {
                                if (focus >= 3) { // 두번 째 행에 포커스
                                    setBtnFocus(btnFocus);
                                    unfocusing(focus);
                                }
                                else {
                                    tempFocus = focus + 3 >= itemList.length ? itemList.length - 1 : focus + 3;
                                    setFocus(tempFocus);
                                }
                            }
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (isBtnFocus) {
                        if (btnFocus === 0) {
                            openEditModePopup();
                        }
                        else {
                            LayerManager.historyBack();
                        }
                    }
                    else {
                        itemList[focus].enterKeyAction();
                    }
                    return true;
            }
            return false;
        };

        function setFocus(focusIdx) {
            unfocusing(focus);
            focus = focusIdx;
            focusing(focus);
        }

        function focusing(index) {
            log.printDbg('focusing() - index : ' + index);

            if (itemList && itemList.length > 0 && itemList[index]) {
                itemList[index].toggle(true);
            }
        }

        function unfocusing(index) {
            log.printDbg('unfocusing() - index : ' + index);

            if (itemList && itemList[index]) itemList[index].toggle(false);
        };

        function setBtnFocus(btnIdx) {
            unfocusBtn();
            focusBtn(btnIdx);
            btnFocus = btnIdx;
        }

        function unfocusBtn() {
            view.find(".button_area btn.focus").removeClass("focus");
            isBtnFocus = false;
        }

        function focusBtn(btnIdx) {
            view.find(".button_area btn:eq(" + btnIdx + ")").addClass("focus");
            isBtnFocus = true;
        }


        function goToCategory(catId) {
            log.printDbg('goToCategory() - catId : ' + catId);

            var menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(), allSearch: false, cbCondition: function (menu) {
                    if (menu.id === catId) return true;
                }
            })[0];
            if (menuData != undefined) {
                MenuServiceManager.jumpMenu({menu: menuData});
            }
            else {
                openCancelMenuDataConfirmPopup(getCancelMenuData(), "DELTE_MODE");
            }
        };

        function getAvailableMonthlyData() {
            var tmp = [];
            for (var i in monthlyData) {
                if (monthlyData[i].connerYn == "Y" && !!monthlyData[i].catId) tmp.push(monthlyData[i]);
            }
            log.printDbg('getAvailableMonthlyData() - tmp : ' + tmp);

            return tmp;
        }

        function getCancelMenuData() {
            log.printDbg('getCancelMenuData()');

            // itemList
            var count = 0;
            deleteListIndex = [];

            for (var i = 0; i < itemList.length; i++) {
                if (itemList[i].getData() != null && itemList[i].getData().menuId != null) {
                    var menuData = MenuDataManager.searchMenu({
                        menuData: MenuDataManager.getMenuData(), allSearch: false, cbCondition: function (menu) {
                            if (menu.id == itemList[i].getData().menuId) {
                                return true;
                            }
                        }
                    })[0];

                    if (menuData == undefined) {
                        deleteListIndex[count] = (itemList[i].getData().menuId);
                        count++;
                    }
                }
            }

            return count;
        };

        function deleteItem() {
            log.printDbg('deleteItem()');

            FavorMenuManager.getFavorMenuData(function (data) {
                var menuList = data;

                for (var i = 0; i < deleteListIndex.length; i++) {
                    for (var j = 0; j < menuList.length; j++) {
                        if (deleteListIndex[i] == menuList[j].menuId) {
                            menuList.splice(j, 1);
                        }
                    }
                }

                FavorMenuManager.setFavorMenuData(menuList);
                instance.update();

                myHome.web3Manager.setFavoriteMenuAdd(menuList, function (result, data) {
                    if (!result || data.resultCd != 0) {
                        showToast("즐겨찾기 저장에 실패했습니다");
                    }
                });
            });
        };

        function openCancelMenuDataConfirmPopup(cancelCount, actionType) {
            log.printDbg('openCancelMenuDataConfirmPopup() - cancelCount : ' + cancelCount + ', actionType : ' + actionType);
            LayerManager.activateLayer({
                obj: {
                    id: "SingleTextPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        title: "알림", content: "편성 정보 변경으로 인해 " + cancelCount + "개의 " + "<br>" + "메뉴가 즐겨찾는 메뉴에서 삭제됩니다",//responseData.RESULT_MSG,
                        btnText: "확인", callback: function (res) {
                            if (res) {
                                if (actionType == "EDIT_MODE") {
                                    deleteItem();
                                    openEditModePopup();
                                }
                                else {
                                    deleteItem();
                                }
                            }
                        }
                    }
                }, moduleId: "module.family_home", visible: true
            });
        };

        function openEditModePopup() {
            log.printDbg('openEditModePopup()');

            LayerManager.activateLayer({
                obj: {
                    id: "MyHomeFavorMenuEditPopup", type: Layer.TYPE.POPUP, priority: Layer.PRIORITY.POPUP, params: {
                        monthlyData: getAvailableMonthlyData(), cugData: cugData, callback: function (result) {
                            if (result) {
                                instance.update();
                            }
                        }
                    }
                }, moduleId: "module.family_home", visible: true
            });
        };

        function showNoFavorMenu() {
            var itemList = instance.div.find("ul");
            var emptyHtml = "<div class='no_item_area'>" + "<span>즐겨 찾는 메뉴가 없습니다</span>" + "</div>";

            itemList.empty();
            itemList.append(emptyHtml);
        }

        var Item = function (itemData) {
            var data = itemData;
            var li = _$("<li/>", {class: "menuItem"});
            li.append(_$("<div/>", {class: 'basicBox'}));
            //li.append(_$("<div/>", {class:'focusBox'}));
            li.append(_$("<span/>", {class: "title"}).text(HTool.getTitle(data.menuNameKor, data.menuNameEng)));
            li.append(_$("<span/>", {class: "subTitle"}).text(HTool.getTitle(data.parentNameKor, data.parentNameEng)));
            li.append(_$("<div class='focus_red'><div class='black_bg'></div></div>"));

            function getView() {
                return li;
            }

            function getData() {
                return data;
            }

            function toggle(focus) {
                li.toggleClass("focus", focus);
            }

            function enterKeyAction() {
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_CATEGORY, data.menuId, "", "", "");
                } catch (e) {
                }
                goToCategory(data.menuId);
            }

            return {
                getView: getView, getData: getData, toggle: toggle, enterKeyAction: enterKeyAction
            }
        };

        var NoItemItem = function () {
            var li = _$("<li/>", {class: "noItemItem"});
            li.append(_$("<div/>", {class: 'basicBox'}));
            li.append(_$("<span/>", {class: "title"}).text("즐겨 찾는 메뉴가 없습니다"));

            function getView() {
                return li;
            }

            function getData() {
                return null;
            }

            return {
                getView: getView, getData: getData
            }
        };

        var MonthlyPayItem = function (itemData) {
            monthlyData = itemData == null ? [] : (Array.isArray(itemData) ? itemData : [itemData]);
            var data = getAvailableMonthlyData();
            var cnt = data.length;
            if (cnt > 0) {
                var li = _$("<li/>", {class: "menuItem"});
                li.append(_$("<div/>", {class: 'basicBox'}));
                //li.append(_$("<div/>", {class: 'focusBox'}));
                if (cnt > 1) li.append(_$("<span/>", {class: "title"}).html("내가 가입한<br>월정액 모아보기"));
                else {
                    li.append(_$("<span/>", {class: "title"}).text(data[0].productName));
                    li.append(_$("<span/>", {class: "subTitle"}).text("바로가기"));
                }
                li.append(_$("<div class='focus_red'><div class='black_bg'></div></div>"));
            }

            function getView() {
                return li;
            }

            function getData() {
                return data;
            }

            function toggle(focus) {
                li.toggleClass("focus", focus);
            }

            function enterKeyAction() {
                if (data.length > 1) {
                    // [WEBIIIHOME-3781] Biz 상품에서는 가입한 월정액 모아보기 선택 시 알림 팝업 표시.
                    // 멀티룸패키지에서 월정액 가입을 막고 있기 때문에.. 멀티룸패키지인 경우에도 알림팝업이 뜨도록 처리함.
                    if (KTW.managers.service.ProductInfoManager.isBiz() || KTW.managers.service.ProductInfoManager.isMultiroomPackage()) {
                        HTool.openErrorPopup({message: ERROR_TEXT.ET005, title: "알림", button: "확인"});
                    }
                    else {
                        LayerManager.activateLayer({
                            obj: {
                                id: "MyHomeMonthlypayPopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                params: monthlyData
                            }, moduleId: "module.family_home", visible: true
                        });
                    }
                }
                else {
                    try {
                        NavLogMgr.collectFHMenu(KEY_CODE.ENTER, data[0].catId, data[0].productName, "", "");
                    } catch (e) {
                    }
                    goToCategory(data[0].catId);
                }
            }

            return {
                getView: getView,
                getData: getData,
                toggle: toggle,
                enterKeyAction: enterKeyAction,
                isAvailable: function () {
                    return cnt > 0;
                }
            }
        };

        var CUGItem = function (itemData) {
            var data = itemData.cugList;
            data = data == null ? [] : (Array.isArray(data) ? data : [data]);

            var li = _$("<li/>", {class: "menuItem"});
            li.append(_$("<div/>", {class: 'basicBox'}));
            //li.append(_$("<div/>", {class: 'focusBox'}));
            if (data.length > 1) li.append(_$("<span/>", {class: "title"}).html("내가 가입한<br>CUG 모아보기"));
            else {
                li.append(_$("<span/>", {class: "title"}).text(data[0].cugName));
                li.append(_$("<span/>", {class: "subTitle"}).text("바로가기"));
            }
            li.append(_$("<div class='focus_red'><div class='black_bg'></div></div>"));

            function getView() {
                return li;
            }

            function getData() {
                return data;
            }

            function toggle(focus) {
                li.toggleClass("focus", focus);
            }

            function enterKeyAction() {
                if (data.length > 1) {
                    LayerManager.activateLayer({
                        obj: {
                            id: "MyHomeCUGListPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: data
                        }, moduleId: "module.family_home", visible: true
                    });
                }
                else {
                    switch (data[0].linkType - 0) {
                        case 0:
                            var nextState;
                            var obj = {type: CONSTANT.APP_TYPE.MULTICAST};
                            if (data[0].linkInfo.indexOf(".") >= 0) {
                                obj.param = data[0].linkInfo
                                nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
                            }
                            else {
                                obj.param = CONSTANT.APP_ID.MASHUP;
                                obj.ex_param = data[0].linkInfo;
                                nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                            }

                            AppServiceManager.changeService({
                                nextServiceState: nextState, obj: obj
                            });
                            return;
                        case 1:
                            goToCategory(data[0].linkInfo);
                            return;
                        case 2:
                            //미사용
                            return;
                        case 3:
                            var nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                            AppServiceManager.changeService({
                                nextServiceState: nextState, obj: {
                                    type: CONSTANT.APP_TYPE.UNICAST, param: data.linkInfo
                                }
                            });
                            return;
                    }
                }
            }

            return {
                getView: getView, getData: getData, toggle: toggle, enterKeyAction: enterKeyAction
            }
        };

        var emptyItem = function () {
            var li = _$("<li/>", {class: "menuItem"});
            li.append(_$("<div/>", {class: 'basicBox empty'}));

            function getView() {
                return li;
            }

            //function getData() { return data; }
            //function toggle(focus) { li.toggleClass("focus", focus); }
            //function enterKeyAction() {
            //    try {
            //        NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_CATEGORY, data.menuId, "", "", "");
            //    } catch(e) {}
            //    goToCategory(data.menuId);
            //}

            return {
                getView: getView, //getData: getData,
                //toggle: toggle,
                //enterKeyAction: enterKeyAction
            }
        };
    }

    myHome.popup.favorMenu_popup.prototype = new Layer();
    myHome.popup.favorMenu_popup.prototype.constructor = myHome.popup.favorMenu_popup;

    myHome.popup.favorMenu_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    myHome.popup.favorMenu_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["favorMenuPopup"] = myHome.popup.favorMenu_popup;
})();


//window.favorMenu = function(componentId, cbUpdate, menuData) {
//    var id = componentId;
//    var itemList = [];
//    var focus = 0;
//    var view = _$("<div/>", {id:id});
//    var height = 132;
//    var noItem = false;
//    var instance = this;
//
//    var monthlyData;
//    var cugData;
//    var isFocused = false;
//
//    (function createDom() {
//        view.append("<span class='componentTitle'><div></div>즐겨 찾는 메뉴</span>");
//        view.append(_$("<div/>", {class:"favorMenu"}));
//        view.find(".favorMenu").append(_$("<ul/>"));
//    })();
//
//    this.update = function () {
//        this.updateView();
//    };
//
//    this.updateView = function () {
//        view.find(".componentTitle").html("<div/>" + HTool.getMenuName(menuData));
//        cugData =  JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_CUG_INFO));
//        myHome.amocManager.getMyCornerListW3(function (result, data) {
//            itemList = [];
//            if(result) {
//                monthlyData = data.cornerList;
//                var monthlyItem = new MonthlyPayItem(monthlyData);
//                if(monthlyItem.isAvailable()) itemList.push(monthlyItem);
//            }
//
//            if(cugData && cugData.cugList) itemList.push(new CUGItem(cugData));
//            var menuList;
//
//            FavorMenuManager.getFavorMenuData(function (favorMenuData) {
//                menuList = favorMenuData;
//                for(var i in menuList) {
//                    if(itemList.length>=6) break;
//                    itemList.push(new Item(menuList[i]));
//                }
//
//                var prevStateIsNoItem = noItem;
//                if(itemList.length==0) {
//                    noItem = true;
//                    itemList.push(new NoItemItem());
//                } else noItem = false;
//
//                itemList.splice(3, 0, new EditItem());
//                height = 130*(Math.floor((itemList.length-1)/4)+1);
//
//                view.find("div.favorMenu").css("height", height + "px");
//                var list = view.find(".favorMenu ul");
//                list.empty();
//                for(var i=0; i<itemList.length; i++) list.append(itemList[i].getView());
//                // 2017.10.13 Lazuli
//                // 즐겨찾는메뉴가 없는 상태에서 즐겨찾는 메뉴를 2개 이상 추가하게되면 noItem은 false고 현재 focus는 1이므로 최종적으로 focus가 1로 셋팅됨
//                // 이전 상태 여부를 체크하여, focus를 셋팅하도록 수정
//                if (prevStateIsNoItem) focus = noItem?1:0;
//                else focus = noItem?1:Math.max(0, Math.min(itemList.length-1, (focus||0)));
//                if(isFocused) focusing(focus);
//                if(cbUpdate) cbUpdate();
//            });
//        });
//    };
//
//    this.getView = function() {
//        return view;
//    };
//
//    this.getHeight = function() {
//        return myHome.CONSTANT.COMP_TITLE_HEIGHT + height + 50;
//    };
//
//    this.isLastFocused = function () { return focus === itemList.length-1; };
//    this.isFirstFocused = function () { return focus === 0; }
//
//    this.onFocused = function(keyCode) {
//        if(noItem) {
//            focus = 1;
//        } else {
//            if(keyCode===KEY_CODE.UP) focus = Math.floor((itemList.length - 1) / 4) * 4;
//            else if(keyCode===KEY_CODE.BLUE) focus = itemList.length-1;
//            else focus = 0;
//        } focusing(focus);
//        isFocused = true;
//    };
//
//    this.onBlured = function() {
//        isFocused = false;
//        unfocusing(focus);
//        view.find(".componentTitle").css("-webkit-transform", "");
//    };
//
//    var focusing = function(index) {
//        if(itemList && itemList[index]) {
//            itemList[index].toggle(true);
//            if(index==0) view.find(".componentTitle").css("-webkit-transform", "translateY(-5px)");
//            else view.find(".componentTitle").css("-webkit-transform", "");
//        }
//    };
//
//    var unfocusing = function(index) {
//        if(itemList && itemList[index])
//            itemList[index].toggle(false);
//    };
//
//    this.onKeyAction = function(keyCode) {
//        switch(keyCode) {
//            case KEY_CODE.LEFT:
//                if(!noItem) {
//                    if(focus%4==0) return false;
//                    setFocus(focus-1);
//                    return true;
//                } break;
//            case KEY_CODE.RIGHT:
//                if(!noItem) {
//                    if(focus>=itemList.length-1) return false;
//                    setFocus(focus+1);
//                    return true;
//                } return false;
//            case KEY_CODE.UP:
//                if (itemList.length > 4 && focus >= 4) {
//                    setFocus(focus-4);
//                    return true;
//                } else return false;
//            case KEY_CODE.DOWN:
//                if (itemList.length > focus+4) {
//                    setFocus(Math.min(itemList.length, focus + 4));
//                    return true;
//                }else if(itemList.length > 4 && focus < 4){
//                    setFocus(itemList.length-1);
//                    return true;
//                }else return false;
//            case KEY_CODE.ENTER:
//                itemList[focus].enterKeyAction();
//                return true;
//        } return false;
//    };
//
//    function setFocus(focusIdx){
//        unfocusing(focus);
//        focus = focusIdx;
//        focusing(focus);
//    }
//
//    var goToCategory = function (catId) {
//        var menuData = MenuDataManager.searchMenu({
//            menuData: MenuDataManager.getMenuData(),
//            allSearch: false,
//            cbCondition: function (menu) { if(menu.id===catId) return true; }
//        })[0];
//        if(menuData != undefined) {
//            MenuServiceManager.jumpMenu({menu: menuData});
//        } else {
//            openCancelMenuDataConfirmPopup(getCancelMenuData(), "DELTE_MODE");
//        }
//    };
//
//    function getAvailableMonthlyData() {
//        var tmp = [];
//        for(var i in monthlyData) {
//            if(monthlyData[i].connerYn=="Y" && !!monthlyData[i].catId) tmp.push(monthlyData[i]);
//        } return tmp;
//    }
//
//    var deleteListIndex;
//
//    function getCancelMenuData() {
//        // itemList
//        var count = 0;
//        deleteListIndex = [];
//
//        for(var i=0; i < itemList.length ;i++) {
//            if(itemList[i].getData() != null && itemList[i].getData().menuId != null) {
//                var menuData = MenuDataManager.searchMenu({
//                    menuData: MenuDataManager.getMenuData(),
//                    allSearch: false,
//                    cbCondition: function (menu) {
//                        if (menu.id == itemList[i].getData().menuId) {
//                            return true;
//                        }
//                    }
//                })[0];
//
//                if(menuData == undefined) {
//                    deleteListIndex[count] = (itemList[i].getData().menuId);
//                    count++;
//                }
//            }
//        }
//
//        return count;
//    };
//
//    function deleteItem() {
//        FavorMenuManager.getFavorMenuData(function (data) {
//            var menuList = data;
//
//            for(var i=0;i<deleteListIndex.length;i++) {
//                for(var j=0;j<menuList.length;j++) {
//                    if(deleteListIndex[i] == menuList[j].menuId) {
//                        menuList.splice(j, 1);
//                    }
//                }
//            }
//
//            FavorMenuManager.setFavorMenuData(menuList);
//            instance.update();
//
//            myHome.web3Manager.setFavoriteMenuAdd(menuList, function (result, data) {
//                if(!result || data.resultCd!=0) {
//                    showToast("즐겨찾기 저장에 실패했습니다");
//                }
//            });
//        });
//    };
//
//    function openCancelMenuDataConfirmPopup(cancelCount, actionType) {
//        LayerManager.activateLayer({
//            obj: {
//                id: "SingleTextPopup",
//                type: Layer.TYPE.POPUP,
//                priority: Layer.PRIORITY.POPUP,
//                linkage: false,
//                params: {
//                    title: "알림",
//                    content: "편성 정보 변경으로 인해 "+ cancelCount+ "개의 " + "<br>" + "메뉴가 즐겨찾는 메뉴에서 삭제됩니다",//responseData.RESULT_MSG,
//                    btnText: "확인",
//                    callback: function (res) {
//                        if (res) {
//                            if(actionType ==  "EDIT_MODE") {
//                                deleteItem();
//                                openEditModePopup();
//                            } else {
//                                deleteItem();
//                            }
//                        }
//                    }
//                }
//            },
//            moduleId: "module.family_home",
//            visible: true
//        });
//    };
//
//    var Item = function (itemData) {
//        var data = itemData;
//        var li = _$("<li/>", {class: "menuItem"});
//        li.append(_$("<div/>", {class:'basicBox'}));
//        li.append(_$("<div/>", {class:'focusBox'}));
//        li.append(_$("<span/>", {class:"title"}).text(HTool.getTitle(data.menuNameKor, data.menuNameEng)));
//        li.append(_$("<span/>", {class:"subTitle"}).text(HTool.getTitle(data.parentNameKor, data.parentNameEng)));
//        li.append(_$("<div/>", {class:"focus_line"}));
//
//        function getView() { return li; }
//        function getData() { return data; }
//        function toggle(focus) { li.toggleClass("focus", focus); }
//        function enterKeyAction() {
//            try {
//                NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_CATEGORY, data.menuId, "", "", "");
//            } catch(e) {}
//            goToCategory(data.menuId);
//        }
//
//        return {
//            getView: getView,
//            getData: getData,
//            toggle: toggle,
//            enterKeyAction: enterKeyAction
//        }
//    };
//
//    var NoItemItem = function () {
//        var li = _$("<li/>", {class: "noItemItem"});
//        li.append(_$("<div/>", {class:'basicBox'}));
//        li.append(_$("<span/>", {class:"title"}).text("즐겨 찾는 메뉴가 없습니다"));
//
//        function getView() { return li; }
//        function getData() { return null; }
//
//        return {
//            getView: getView,
//            getData: getData
//        }
//    };
//
//    var MonthlyPayItem = function (itemData) {
//        monthlyData = itemData==null?[]:(Array.isArray(itemData)?itemData:[itemData]);
//        var data = getAvailableMonthlyData();
//        var cnt = data.length;
//        if (cnt>0) {
//            var li = _$("<li/>", {class: "menuItem"});
//            li.append(_$("<div/>", {class:'basicBox'}));
//            li.append(_$("<div/>", {class: 'focusBox'}));
//            if (cnt > 1)
//                li.append(_$("<span/>", {class: "title"}).html("내가 가입한<br>월정액 모아보기"));
//            else {
//                li.append(_$("<span/>", {class: "title"}).text(data[0].productName));
//                li.append(_$("<span/>", {class: "subTitle"}).text("바로가기"));
//            } li.append(_$("<div/>", {class:"focus_line"}));
//        }
//
//        function getView() { return li; }
//        function getData() { return data; }
//        function toggle(focus) { li.toggleClass("focus", focus); }
//        function enterKeyAction() {
//            if(data.length>1) {
//                LayerManager.activateLayer({
//                    obj: {
//                        id: "MyHomeMonthlypayPopup",
//                        type: Layer.TYPE.POPUP,
//                        priority: Layer.PRIORITY.POPUP,
//                        params: monthlyData
//                    },
//                    moduleId: "module.family_home",
//                    visible: true
//                });
//            }else {
//                try {
//                    NavLogMgr.collectFHMenu(KEY_CODE.ENTER, data[0].catId, data[0].productName, "", "");
//                } catch(e) {}
//                goToCategory(data[0].catId);
//            }
//        }
//
//        return {
//            getView: getView,
//            getData: getData,
//            toggle: toggle,
//            enterKeyAction: enterKeyAction,
//            isAvailable: function () {
//                return cnt>0;
//            }
//        }
//    };
//
//    var CUGItem = function (itemData) {
//        var data = itemData.cugList;
//        data = data==null?[]:(Array.isArray(data)?data:[data]);
//
//        var li = _$("<li/>", {class: "menuItem"});
//        li.append(_$("<div/>", {class:'basicBox'}));
//        li.append(_$("<div/>", {class: 'focusBox'}));
//        if (data.length > 1)
//            li.append(_$("<span/>", {class: "title"}).html("내가 가입한<br>CUG 모아보기"));
//        else {
//            li.append(_$("<span/>", {class: "title"}).text(data[0].cugName));
//            li.append(_$("<span/>", {class: "subTitle"}).text("바로가기"));
//        } li.append(_$("<div/>", {class:"focus_line"}));
//
//        function getView() { return li; }
//        function getData() { return data; }
//        function toggle(focus) { li.toggleClass("focus", focus); }
//        function enterKeyAction() {
//            if(data.length>1) {
//                LayerManager.activateLayer({
//                    obj: {
//                        id: "MyHomeCUGListPopup",
//                        type: Layer.TYPE.POPUP,
//                        priority: Layer.PRIORITY.POPUP,
//                        params: data
//                    },
//                    moduleId: "module.family_home",
//                    visible: true
//                });
//            }else {
//                switch (data[0].linkType-0) {
//                    case 0:
//                        var nextState;
//                        var obj = {type:CONSTANT.APP_TYPE.MULTICAST};
//                        if(data[0].linkInfo.indexOf(".")>=0) {
//                            obj.param = data[0].linkInfo
//                            nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
//                        }else {
//                            obj.param = CONSTANT.APP_ID.MASHUP;
//                            obj.ex_param = data[0].linkInfo;
//                            nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
//                        }
//
//                        AppServiceManager.changeService({
//                            nextServiceState: nextState,
//                            obj: obj
//                        });return;
//                    case 1:
//                        goToCategory(data[0].linkInfo);
//                        return;
//                    case 2:
//                        //미사용
//                        return;
//                    case 3:
//                        var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
//                        AppServiceManager.changeService({
//                            nextServiceState: nextState,
//                            obj :{
//                                type: CONSTANT.APP_TYPE.UNICAST,
//                                param: data.linkInfo
//                            }
//                        }); return;
//                }
//            }
//        }
//
//        return {
//            getView: getView,
//            getData: getData,
//            toggle: toggle,
//            enterKeyAction: enterKeyAction
//        }
//    };
//
//    function openEditModePopup() {
//        LayerManager.activateLayer({
//            obj: {
//                id: "MyHomeFavorMenuEditPopup",
//                type: Layer.TYPE.POPUP,
//                priority: Layer.PRIORITY.POPUP,
//                params: {
//                    monthlyData :getAvailableMonthlyData(),
//                    cugData: cugData,
//                    callback: function (result) {
//                        if(result) instance.updateView();
//                    }
//                }
//            },
//            moduleId:"module.family_home",
//            visible: true
//        });
//    };
//
//    var EditItem = function () {
//        var li = _$("<li/>", {class: "editItem"});
//        li.append(_$("<div/>", {class:'basicBox'}));
//        li.append(_$("<div/>", {class:'focusBox'}));
//        li.append(_$("<div/>", {class:'icon'}));
//        li.append(_$("<span/>", {class:"subTitle"}).text("편집"));
//        li.append(_$("<div/>", {class:"focus_line"}));
//
//        function getView() { return li; }
//        function getData() { return null; }
//        function toggle(focus) { li.toggleClass("focus", focus); }
//        function enterKeyAction() {
//            var cancelCount = getCancelMenuData();
//            if(cancelCount > 0) {
//                openCancelMenuDataConfirmPopup(cancelCount, "EDIT_MODE");
//                return;
//            }
//            openEditModePopup();
//        }
//
//        return {
//            getView: getView,
//            getData: getData,
//            toggle: toggle,
//            enterKeyAction: enterKeyAction
//        }
//    };
//};