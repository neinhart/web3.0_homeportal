window.contentsList = function(code, data) {
    var contentsData = data.items;
    var focus = 0;
    var currentPage = 0;
    var currentPageLength = 6;
    var lastPage = 0;
    var pageIDList = [];
    var listenerID;
    var categoryCode = code;
    var posterType = data.posterType;
    
    this.init = function() {
        document.getElementById("contents").innerHTML += "<div id='contentsList_" + categoryCode + "' class='contentslist'></div>";

        lastPage = Math.ceil(contentsData.length/10);
        currentPageLength = lastPage-1 == currentPage?contentsData.length%10:10;
        currentPage = 0;
        focus = 0;

        pageIDList = [];
        createPageData(0, getPageInfo(HTool.getIndex(currentPage, -2, lastPage), contentsData));
        createPageData(1, getPageInfo(HTool.getIndex(currentPage, -1, lastPage), contentsData));
        createPageData(2, getPageInfo(currentPage, getPageInfo(currentPage, contentsData)));
        createPageData(3, getPageInfo(HTool.getIndex(currentPage, 1, lastPage), contentsData));
        createPageData(4, getPageInfo(HTool.getIndex(currentPage, 2, lastPage), contentsData));
    }
    
    this.show = function() {
        if(!document.getElementById("contentsList_" + categoryCode)) this.init();
        
        document.getElementById("contentsList_" + categoryCode).classList.remove("hidden"); 
    }
    
    this.hide = function() {
        document.getElementById("contentsList_" + categoryCode).classList.add("hidden"); 
    }
    
    this.onFocus = function() {
        focus = 0;
        focusItem(focus);
        Indicator.createIndicator(lastPage, currentPage);
    }
    
    this.onBlurred = function() {
        unfocusItem(focus);
        focus = 0;
    }
    
    var getPageInfo = function(page, data) {
        var firstIndex = page*10;
        var lastIndex = page*10 + 10;
        var data = data.slice(firstIndex, lastIndex);
        data.page = page; 
        return data;
    }
    
    var createPageData = function(page, data) {
        var random_num = Math.floor(Math.random() * 99999999);
        var html = "<ul id=\"page_" + random_num + "\" class=\"posterPage page" + page + " focusingAnimatable pageAnimatable\">" + getPageHtml(data, random_num)+"</ul>";
        document.getElementById("contentsList_" + categoryCode).innerHTML += html;
        document.getElementById("contentsArea").classList.add("preview");
        pageIDList.push(random_num);
    }
            
    var getPageHtml = function(data, divId) {
        var html = "";
        for(var i=0; i<data.length; i++) {
            html+=ContentsFactory.getContents('posterDiv_' + divId + '_' + i, data[i], posterType);
        } return html;
    }
    
    var pageUp = function() {
        currentPage = HTool.getIndex(currentPage, -1, lastPage);
        unfocusItem(focus);
        
        document.getElementById("page_" + pageIDList[0]).className="posterPage page1 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[1]).className="posterPage page2 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[2]).className="posterPage page3 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[3]).className="posterPage page4 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[4]).className="posterPage page0 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[4]).innerHTML = getPageHtml(getPageInfo(HTool.getIndex(currentPage, -2, lastPage), contentsData), pageIDList[4]);
        pageIDList.unshift(pageIDList.pop());
        
        currentPageLength = lastPage-1 == currentPage?contentsData.length%10:10;
        focus = Math.min(currentPageLength-1, currentPageLength>5?focus+5:focus);
        Indicator.changePage(currentPage);
        
        focusItem(focus);
    }
    
    var pageDown = function() {
        currentPage = HTool.getIndex(currentPage, 1, lastPage);
        unfocusItem(focus);
        
        document.getElementById("page_" + pageIDList[0]).className="posterPage page4 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[1]).className="posterPage page0 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[2]).className="posterPage page1 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[3]).className="posterPage page2 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[4]).className="posterPage page3 focusingAnimatable pageAnimatable";
        document.getElementById("page_" + pageIDList[0]).innerHTML = getPageHtml(getPageInfo(HTool.getIndex(currentPage, 2, lastPage), contentsData), pageIDList[0]);
        pageIDList.push(pageIDList[0]); pageIDList.shift();
        log.printDbg(pageIDList);
        
        currentPageLength = lastPage-1 == currentPage?contentsData.length%10:10;
        focus = Math.min(currentPageLength-1, focus>=5?focus-5:focus);
        Indicator.changePage(currentPage);
        focusItem(focus);
    }
    
    var focusItem = function(newFocus) {
//        document.getElementById("posterDiv_" + pageIDList[2] + "_" +  newFocus).addEventListener('webkitTransitionEnd', changeBg, false);
        document.getElementById("posterDiv_" + pageIDList[2] + "_" +  newFocus).classList.add("focus");
    }
    
    var unfocusItem = function(focus) {
//        document.getElementById("posterDiv_" + pageIDList[2] + "_" + focus).removeEventListener('webkitTransitionEnd', changeBg);
        document.getElementById("posterDiv_" + pageIDList[2] + "_" + focus).classList.remove('focus');
        log.printDbg(pageIDList[2]);
    }
    
    var changeFocus = function(newFocus) {
        unfocusItem(focus);
        focus = newFocus;
        focusItem(focus);
    }
    
    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.LEFT:
                var newFocus = focus-1;
                if(newFocus<0) newFocus = Math.min(currentPageLength, 4);
                else if(newFocus==4) newFocus = currentPageLength-1;
                changeFocus(newFocus);
                break;
            case KEY_CODE.RIGHT:
                var newFocus = focus+1;
                if(newFocus>=currentPageLength) newFocus = currentPageLength>5?5:0;
                else if(newFocus==5) newFocus = 0;
                changeFocus(newFocus);
                break;
            case KEY_CODE.DOWN:
                if(currentPageLength<=5 || focus>=5) {
                    pageDown();
                } else {
                    changeFocus(Math.min(focus+5, currentPageLength-1));
                } break;
            case KEY_CODE.UP:
                if(focus<5) {
                    pageUp();
                } else {
                    changeFocus(focus-5);
                } break;
        }
    }
}
    