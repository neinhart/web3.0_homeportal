/**
 * 무비초이스 구매 목록 (우리집 맞춤 TV > 구매내역 보기 > 무비초이스 구매 목록)
 * Created by ksk91_000 on 2016-07-01.
 */
if (!component) var component = {};

component.buyList_ots = function (id) {
    var componentId = id;
    var listData;
    var cnt = 0;
    var focus = 0;
    var page = 0;
    var indicator = new Indicator(1);
    var authAdultBox;
    var isLoaded = false;
    var isLoading = false;
    var isFocused = false;
    var isLoadWating = false;

    var instance = this;
    var NUMBEROFCONTENTINPAGE = 5;// 페이지에 노출되어야할 컨텐츠 수

    var dom = _$("<div id='" + componentId + "' class='buyList'>" +
        "<div id='amountTxt'>총 0건</div>" +
        "<div class='item_area'>" +
            "<div class='focus_line'/>" +
            "<table class='item_list'/>" +
        "</div>" +
    "</div>");
    dom.append(indicator.getView()).append(indicator.getPageTransView());

    this.getView = function () {
        return dom;
    };

    this.update = function (callback) {
        if (isLoading) {
            return;
        }

        isLoading = true;

        LayerManager.startLoading({preventKey: true});

        myHome.oppvManager.searchHistory(function (result, data) {
            try {
                if (result && data.custPpvList[0].rsltCd === "") {
                    listData = data.custPpvList;
                } else {
                    listData = [];
                }
            } catch (e) {
                listData = [];
            }

            cnt = listData.length;

            updateView();
            isLoaded = true;
            isLoading = false;
            LayerManager.stopLoading();

            if (callback) {
                callback();
            }
        }, DEF.SMARTCARD_ID);
    };

    function updateView() {
        if (cnt === 0) {
            dom.addClass("noItem");
        } else {
            dom.removeClass("noItem");
        }

        dom.find(".item_area").empty();

        if (authAdultBox) {
            authAdultBox.create(function () {
                authAdultBox = null;
                dom.find("#" + componentId).removeClass("blurred");
                updateView();
                instance.parent.viewMgr.reChangeFocus();
            });
            dom.find(".item_area").append(authAdultBox.getView());
            dom.find(".item_area").append(_$("<div/>", {class: 'tip_text'}).html("'TV에 내폰 연결'(ch.742) 메뉴에서 올레 tv 모바일앱과 올레 tv를 연결하시면<br>구매한 VOD를 폰에서도 추가 결제 없이 시청하실 수 있습니다"));
        } else if (cnt <= 0) {
            dom.find(".item_area").append(getNoDataView());
        } else {
            dom.find(".item_area").append("<div class='focus_line'/><table class='item_list'/>");
            dom.find("#amountTxt").text("총 " + cnt + "개");
            indicator.setSize(Math.ceil(cnt / 5));

            for (var i = 0; i < NUMBEROFCONTENTINPAGE; i++) {
                dom.find(".item_list").append(getItemView());
            }

            dom.find(".rightArea").addClass("otn");
            changePage(0);
            setFocus(0);
        }

        if (isFocused) {
            instance.focused();
        } else {
            instance.blurred();
        }
    }

    function getNoDataView() {
        var div = _$("<div/>", {class: 'noItemView'});
        div.append(
            _$("<img>", {src: modulePath + "resource/image/icon_noresult.png"}),
            _$("<span>", {class: "text"}).text("최근 구매한 목록이 없습니다"));

        return div;
    }

    function getItemView() {
        var html = "<tr>" +
            "<td>" +
            "<div class='Title'></div>" +
            "<div class='info'></div></td>" +
            "</td>" +
            "</tr>";
        return _$(html);
    }

    function setItemView(div, title, price, date) {
        div.find(".Title").text(title);
        div.find(".info").html(HTool.addComma(price) + "원 <co>l</co> " + parseDateString(date));
        div.removeClass("hiddenItem");
    }

    function setFocus(newFocus) {
        dom.find("table tr").eq(focus % 5).removeClass('focus');
        focus = newFocus;
        dom.find("table tr").eq(focus % 5).addClass('focus');

        if (Math.floor(focus / 5) != page) {
            changePage(Math.floor(focus / 5));
        }
        dom.find(".focus_line").css("-webkit-transform", "translateY(" + focus % 5 * 170 + "px)");
    }

    function changePage(newPage) {
        page = newPage;
        indicator.setPos(page);
        for (var i = 0; i < NUMBEROFCONTENTINPAGE; i++) {
            var idx = i + (page * 5);

            if (listData[idx]) {
                setItemView(dom.find("table tr").eq(i),
                    listData[idx].pgmNm, // 프로그램명
                    listData[idx].usgAmt, // 이용금액
                    listData[idx].recvDh // 접수일지
                );
            } else {
                dom.find("table tr").eq(i).addClass("hiddenItem")
            }
        }
    }

    this.show = function () {
        if (!isLoaded) this.update(function () {
            if (isLoadWating) {
                instance.parent.viewMgr.reChangeFocus(true);
            }
        });
    };

    this.focused = function () {
        isFocused = true;
        isLoadWating = false;
        if (!isLoaded) {
            isLoadWating = true;
            return;
        }
        if (authAdultBox) {
            dom.addClass("focused");
            return myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
        } else if (!listData || cnt <= 0) {
            return myHome.ViewManager.FOCUS_MODE.NO_FOCUS;
        } else {
            setFocus(focus);
            return myHome.ViewManager.FOCUS_MODE.FOCUS;
        }
    };

    this.blurred = function () {
        isFocused = false;
        setFocus(0);
        dom.removeClass("focused");
        dom.find("tr.focus").removeClass("focus");
    };

    this.onKeyAction = function (keyCode) {
        if (authAdultBox) return authAdultBox.onKeyAction(keyCode);
        else switch (keyCode) {
            case KEY_CODE.UP:
                setFocus(HTool.getIndex(focus, -1, cnt));
                return true;
            case KEY_CODE.DOWN:
                setFocus(HTool.getIndex(focus, 1, cnt));
                return true;
            case KEY_CODE.RED:
                if (indicator.isFocused()) {
                    indicator.blurred();
                    setFocus(page * 5);
                    dom.find(".focus_line").show();
                } else if (focus % 5 === 0) {
                    setFocus(HTool.getIndex(page, -1, Math.ceil(cnt / 5)) * 5);
                } else {
                    setFocus(page * 5);
                }
                return true;
            case KEY_CODE.BLUE:
                if (indicator.isFocused()) {
                    indicator.blurred();
                    setFocus(Math.min(cnt - 1, page * 5 + 4));
                    dom.find(".focus_line").show();
                }
                else if (focus % 5 === 4 || focus === cnt - 1) {
                    setFocus(Math.min(cnt - 1, HTool.getIndex(page, 1, Math.ceil(cnt / 5)) * 5 + 4));
                } else {
                    setFocus(Math.min(cnt - 1, page * 5 + 4));
                }
                return true;
            case KEY_CODE.ENTER:
                return true;
            case KEY_CODE.LEFT:
                if (indicator.isFocused() || cnt <= 5) {
                    setFocus(0);
                    indicator.blurred();
                    dom.find(".focus_line").show();
                    return false;
                }
            case KEY_CODE.BACK:
                setFocus(0);
                return false;
        }
    };

    function parseDateString(str) {
        return str.substr(0, 4) + "." + str.substr(4, 2) + "." + str.substr(6, 2);
    }
};