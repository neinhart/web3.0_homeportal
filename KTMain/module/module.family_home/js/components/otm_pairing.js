if(!component) var component = {};

component.otm_pairing = function(componentId) {
    var id = componentId;
    var isPaired = false;
    var div = _$("<div>", {id: id, class: "otm_paring_area"});

    div.append("<div class='absolute_wrapper'>" +
        "<div class='bg_area'>" +
        "<div class='default_bg'/><div class='focus_bg'/>" +
        "</div>" +
        "<div class='btn_area'>" +
        "<div class='show_icon'/>" +
        "<span/>" +
        "<div class='button'/>" +
        "</div>" +
        "<span class='description_text'>올레 tv에서 구매한 콘텐츠를 올레 tv 모바일에서도 시청 가능합니다</span>" +
        "</div>");

    // otm 관련 메시지 리스너 설정.
    StorageManager.ps.addStorageDataChangeListener(onStorageDataChange);

    this.getView = function() {
        return div;
    };

    this.update = function () {
        update();
    };

    function update() {
        isPaired = StorageManager.ps.load(StorageManager.KEY.OTN_PARING)==="Y";
        div.toggleClass("paired", isPaired).toggleClass("english", HTool.getTitle("kor", "eng")==="eng");
        div.find(".btn_area span").text(HTool.getTitle("올레 tv 모바일", "olleh tv mobile"));
        div.find(".btn_area .button").text(isPaired?HTool.getTitle("연결 관리", "Setting"):HTool.getTitle("연결하기", "Connect"));
    }

    this.isLastFocused = function () { return true; };
    this.isFirstFocused = function () { return true; };

    this.onFocused = function() {
        focusing();
    };

    this.onBlured = function() {
        unfocusing();
    };

    var focusing = function() {
        div.addClass("focus");
    };

    var unfocusing = function() {
        div.removeClass("focus");
    };

    this.onKeyAction = function(keyCode) {
        switch(keyCode) {
            case KEY_CODE.ENTER:
                if(isPaired) {
                    window.OTMPManager.getParingInfo();
                } else {
                    LayerManager.activateLayer({
                        obj: {
                            id: "OTMPConnecting",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true
                        },
                        moduleId:"module.family_home",
                        visible: true
                    });
                } return true;
        } return false;
    };

    this.remove = function () {
        // 메시지 리스너 삭제
        StorageManager.ps.removeStorageDataChangeListener(onStorageDataChange);
    };

    function onStorageDataChange(key) {
        if (key === StorageManager.KEY.OTN_PARING) update();
    }

    this.getHeight = function() {
        return 75;
    };
};