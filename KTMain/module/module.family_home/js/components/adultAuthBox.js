/**
 * Created by ksk91_000 on 2016-12-07.
 */
if(!window.component) window.component = {};
component.AdultAuthBox = function () {
    var starString = "";
    var callback;
    var speechAdapter = oipfAdapter.speechRecognizerAdapter;
    var that = this;

    this.create = function(_callback) {
        this.div = _$("<div/>", {class:"adult_auth_box"});
        var contentArea = _$("<div/>", {class:"no_content_area"});

        contentArea.append(_$("<img>", {src: modulePath + "resource/image/icon_warning.png"}));
        // contentArea.append(_$("<span>", {class: "text1"}).text("성인 인증 후 확인이 가능합니다"));
        contentArea.append(_$("<span>", {class: "text2"}).html("olleh tv 비밀번호(성인인증)를<br>입력해 주세요"));
        contentArea.append(_$("<span>", {class: "input_box"}).html("<div class='box'>****</div><div class='star_txt'/>"));

        this.div.append(contentArea);
        AuthManager.initPW();
        AuthManager.resetPWCheckCount();
        starString = "";

        callback = _callback;
    };

    this.setData = function(data) {

    };

    this.onKeyAction = function(keyCode) {
        if(keyCode==KEY_CODE.BACK) {
            return false;
        } else if(keyCode==KEY_CODE.LEFT || keyCode==KEY_CODE.DEL) {
            return this.deleteNumber();
        } else if((keyCode>=KEY_CODE.NUM_0 && keyCode<=KEY_CODE.NUM_9)) {
            this.inputNumber(keyCode-KEY_CODE.NUM_0);
            return true;
        } else if(keyCode==KEY_CODE.RIGHT) {
            return true;
        } else if(keyCode==KEY_CODE.ENTER) {
            this.checkPassword();
            return true;
        }
    };

    this.inputNumber = function (num) {
        if(starString.length>=4) return;
        starString += "*";
        this.div.find(".input_box .star_txt").text(starString);
        if(AuthManager.inputPW(num, 4)) {
            this.checkPassword();
        }
    };

    this.checkPassword = function () {
        AuthManager.checkUserPW({
            type: AuthManager.AUTH_TYPE.AUTH_ADULT_PIN,
            callback: function(responseCode) {
                log.printDbg("checkUserPW ResponseCode === " + responseCode);
                if (responseCode == AuthManager.RESPONSE.CAS_AUTH_SUCCESS) callback();
                else {
                    that.div.find(".text2").html("비밀번호가 일치하지 않습니다<br>다시 입력해 주세요");
                    AuthManager.initPW();
                    starString = "";
                    that.div.find(".input_box .star_txt").text(starString);
                }
            },
            loading: {
                type: KTW.ui.view.LoadingDialog.TYPE.CENTER,
                lock: true,
                callback: null,
                on_off: false
            }
        })
    }

    this.inputSpeechNumber = function (rec, input, mode) {
        if (rec && mode === oipfDef.SR_MODE.PIN) {
            starString = "****".substr(0, input.length);
            that.div.find(".input_box .star_txt").text(starString);
            if(AuthManager.replacePW(input, 4)) that.checkPassword();
            log.printInfo("inputSpeechNumber::" + input);
        }
    };

    this.deleteNumber = function () {
        if(starString.length>0) {
            AuthManager.deletePW();
            starString = starString.substr(0, starString.length-1);
            this.div.find(".input_box .star_txt").text(starString);
            return true;
        } else return false;
    };

    this.clearNumber = function () {
        starString = "";
        AuthManager.initPW();
        this.div.find(".input_box .star_txt").text(starString);
    };

    this.focused = function() {
        speechAdapter.start(true);
        speechAdapter.addSpeechRecognizerListener(this.inputSpeechNumber);
        return myHome.ViewManager.FOCUS_MODE.BLUR_FOCUS;
    };

    this.blurred = function() {
        speechAdapter.stop();
        speechAdapter.removeSpeechRecognizerListener(this.inputSpeechNumber);
        AuthManager.resetPWCheckCount();
        this.clearNumber();
    };

    this.pause = function () {
        speechAdapter.stop();
        speechAdapter.removeSpeechRecognizerListener(this.inputSpeechNumber);
    };

    this.resume = function () {
        speechAdapter.start(true);
        speechAdapter.addSpeechRecognizerListener(this.inputSpeechNumber);
    };

    this.getView = function() {
        return this.div;
    };

    this.setIndicator = function(_indicator) {
        this.indicator = _indicator;
    }
};