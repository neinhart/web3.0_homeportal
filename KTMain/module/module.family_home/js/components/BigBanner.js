/**
 * Created by ksk91_000 on 2017-01-16.
 */
window.BigBanner = function () {
    var div = _$("<div/>", {class:"banner_big"});
    var focus = 0;
    var length;
    var eventList = [];

    this.setData = function(data) {
        for(var i=0; i<data.length; i++) if(data[i].templateType.charAt(0) == "B") eventList[data[i].templateLoc-1] = data[i];
        if(eventList.length===0) return false;
        switch(eventList[0].templateType) {
            case "B1":
                div.addClass("b1");
                div.append(getItem("banner_big_1", eventList[0]));
                length = 1;
                break;
            case "B2":
                div.addClass("b2");
                div.append(getItem("banner_big_2_1", eventList[0]));
                div.append(getItem("banner_big_2_2", eventList[1]));
                length = 2;
                break;
            case "B3":
                div.addClass("b3");
                div.append(getItem("banner_big_3", eventList[0]));
                div.append(getItem("banner_big_3", eventList[1]));
                div.append(getItem("banner_big_3", eventList[2]));
                length = 3;
                break;
            case "B4":
                div.addClass("b4");
                div.append(getItem("banner_big_4", eventList[0]));
                div.append(getItem("banner_big_4", eventList[1]));
                length = 2;
                break;
            default: return false;
        }return true;
    };

    function getItem(_class, _data) {
        var div = _$("<div/>", {class: _class + " banner_item"});
        div.append(_$("<div/>", {class:"absolute_wrapper"}).append(_$("<img>", {src:_data.imgUrl, onerror:"this.src='" + COMMON_IMAGE.BANNER_DEFAULT + getDefaultImageUrl(_class) + "';this.onerror=''"})));
        return div;
    }

    function getDefaultImageUrl(_class) {
        switch (_class) {
            case "banner_big_1": return "default_recommend_big_w1194.png";
            case "banner_big_2_1": return "default_recommend_big_w702.png";
            case "banner_big_2_2": return "default_recommend_big_w465.png";
            case "banner_big_3": return "default_recommend_big_w390.png";
            case "banner_big_4": return "default_recommend_big_w590.png";
        }
    }

    function setFocus(newFocus) {
        focus = newFocus;
        div.find(".banner_item.focus").removeClass("focus");
        div.find(".banner_item").eq(focus).addClass("focus");
    }

    this.onKeyAction = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT :
                if(focus==0) return false;
                else setFocus(focus-1);
                return true;
            case KEY_CODE.RIGHT :
                if(focus==length-1) return false;
                setFocus(focus+1);
                return true;
            case KEY_CODE.ENTER:
                gotoLocator(eventList[focus]);
                return true;
        }
    };

    function gotoLocator(data) {
        switch (data.itemType-0) {
            case 0:
                var menuData = MenuDataManager.searchMenu({
                    menuData: MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) {
                        if (menu.id == data.itemId) return true;
                    }
                })[0];
                if (!!menuData) MenuServiceManager.jumpMenu({menu: menuData});
                else showToast("잘못된 접근입니다");
                break;
            case 1:    //시리즈
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_CONTEXT, null, data.itemId,  "67");
                } catch(e) {}
                openDetailLayer(data.itemId, null, "67");
                break;
            case 2:    //컨텐츠
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_CONTEXT, data.catId, data.itemId, "67");
                } catch(e) {}
                openDetailLayer(null, data.itemId, "67");
                break;
            case 3:    //멀티캐스트 양방향 서비스

                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                        data.parentCatId, data.itemId, "", data.locator);
                } catch(e) {}


                var nextState;
                if(data.locator == CONSTANT.APP_ID.MASHUP)
                    nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                else
                    nextState = CONSTANT.SERVICE_STATE.OTHER_APP;

                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        type: CONSTANT.APP_TYPE.MULTICAST,
                        param: data.locator,
                        ex_param: data.parameter
                    }
                });
                break;
            case 7:    //유니캐스트 양방향 서비스
                try {
                    NavLogMgr.collectJump(NLC.JUMP_START_SUBHOME, NLC.JUMP_DEST_INTERACTIVE,
                        data.parentCatId, data.itemId, "", data.locator);
                } catch(e) {}


                var nextState = StateManager.isVODPlayingState()?CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD:CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj :{
                        type: CONSTANT.APP_TYPE.UNICAST,
                        param: data.id
                    }
                });
                break;
            case 8:    //웹뷰
                AppServiceManager.startChildApp(data.locator);
                break;
        }
    }

    this.onFocused = function (keyCode) {
        switch(keyCode) {
            case KEY_CODE.BLUE: setFocus(length-1);
            default:setFocus(0);
        }
    };

    this.onBlured = function () {
        div.find(".focus").removeClass("focus");
    };

    this.isLastFocused = function () { return focus === length-1; };
    this.isFirstFocused = function () { return focus === 0; };

    this.getView = function () {
        return div;
    };

    this.getHeight = function () {
        return 483+70;
    }
};