/**
 * Created by ksk91_000 on 2017-03-27.
 */
myHome.HTTP = {
    CURATION: {
        LIVE_URL: "http://profile.ktipmedia.co.kr:7003/wasProfile",
        TEST_URL: "http://125.140.114.151:7002/wasProfile"
    }, AMOC: {
        HTTP_URL: "http://webui.ktipmedia.co.kr:8080",
        HTTPS_URL: "https://webui.ktipmedia.co.kr"
    }, WEB_3: {
        // BMT_URL: "https://125.159.3.183",
        // LIVE_URL: "https://125.159.2.19"
        BMT_URL: "https://web3apibmt.ktipmedia.co.kr",
        LIVE_URL: "https://web3api.ktipmedia.co.kr"
    }, SMART_PUSH: {
        LIVE_URL: "http://mashup.ktipmedia.co.kr:80",
        TEST_URL: "http://211.113.46.215:80"
    }, SMLS: {
        LIVE_URL: "http://222.122.121.80:8080",
        TEST_URL: "http://203.255.241.154:8080"
    }, SKY_RP_SERVER: {
        LIVE_URL:"http://220.73.134.62",
        TEST_URL:"http://220.73.134.30"
    }, RECOMMEND: {
        LIVE_URL: "http://recommend.ktipmedia.co.kr:7002/",
        TEST_URL: "http://125.140.114.151:7002/"
    }, KOL: {
        LIVE_URL: "https://pairing.ktipmedia.co.kr",
        TEST_URL: "http://tb.pairing.kt.com"

    },
    LUPIN: {
        LIVE_URL: "https://ktpay.kt.com/adaptor/ktpg/dcb/",
        TEST_URL: "http://etbips.olleh.com:10088/adaptor/ktpg/dcb/"
    }
};

myHome.CONSTANT = {
    COMP_TITLE_HEIGHT: 35,
    POSTER_HEIGHT: 400,
}