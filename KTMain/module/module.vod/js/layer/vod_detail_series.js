/**
 * Created by Yun on 2016-10-25.
 */


(function () {
    var vod_detail_series_layer = function vodDetailSeriesLayer(options) {
        Layer.call(this, options);

        var _this = this;
        var sortGb; // 이어보기 sort정보 (정순, 역순)

        var contsId;
        var cateId; // 카테고리 ID
        var cateData; // 카테고리 정보
        var btnList = []; // 버튼 리스트
        var wishObjectList = []; // (시리즈)
        var thumbnailList = []; // (시리즈) 썸네일 리스트
        var btnFocusIdx = 0;  // 버튼 포커스
        var rowIdx = 2;
        var bottomMenu = null; // 연관 메뉴
        var contextMenu = null; // 하단 추천 메뉴
        var reqPathCd; // 진입코드

        var connerYn; // 월정액 유무
        var isDetailSyn = false; // 더보기 버튼 유무
        var isNotice = false; // 공지 유무
        var isNoticeUnFocus = false; // 공지 버튼 포커스 유무
        var isConnerChk = false; // 월정액 체크 여부
        var isConnerBuyChk = false; // 월정액 구매 여부
        var isVodPptBuyChk = false;
        var isAdultCate = false; // 성인카테고리
        var isAuthorizedContent = false; // 성인 인증된 컨텐츠인지 확인
        var isAuthComplete = false; // 인증 완료 여부
        var isSingle = false; // 버튼타입 단편
        var isKidsCate = false; // 키즈카테고리 진입 유무
        var savedSeamlessCode = "1"; // 심리스 코드 저장
        var pointData; // 포인트 정보

        var seriesData;
        var totalContents;
        var hdr_yn;
        var btnLen = 0;
        var btnCurSection = 0;
        var curPage = 0;
        var totalPage = 0;
        var episodeFocusedIdx = 0;

        var isEpisodeBtnFocus = false; // 에피소드 영역 포커스 여부
        var isEpisodeListFocused = false;
        var isPageFocused = false;
        var isThumbnailShow = false;
        var indicator = null;
        var isSeriesBuyChk = false;
        var isSeriesLongChk = false;
        var isMovedPages = false;

        var timeoutBtnData = null; // 하단 추천 메뉴 타임아웃
        var targetEpisode = null;
        var optionList = [];
        var contextMenuBtnIdxList = [];
        var previewBtnList = [];
        var previewData = null;

        this.init = function (cbCreate) {
            this.div.addClass("arrange_frame vod_detail_series_layer vod");
            this.div.css("background", "black");

            cateId = this.getParams().cat_id;
            contsId = this.getParams().const_id;
            cateData = _$.extend({}, this.getParams().cateInfo);
            reqPathCd = this.getParams().req_cd;

            isAdultCate = this.getParams().isAdultCate;
            isAuthorizedContent = isAdultCate || this.getParams().isAuthorizedContent;
            isKidsCate = this.getParams().isKids || KidsModeManager.isKidsMode();
            isEpisodeBtnFocus = this.getParams().isBtnFocus;
            isSeriesLongChk = false;
            isThumbnailShow = cateData.thumbListYn == "Y";
            isEpisodeListFocused = true;
            isPageFocused = false;

            btnFocusIdx = 0;
            btnCurSection = 0;
            btnList = [];
            thumbnailList = [];

            var tmpFlag = cateData.ltFlag.split("|");
            for (var idx = 0; idx < tmpFlag.length; idx++) {
                if (tmpFlag[idx] == 2 || tmpFlag[idx] == 1) {
                    isSeriesLongChk = true;
                }
            }

            if (isKidsCate) {
                this.div.addClass("kids");
            }

            sortGb = cateData.sortGb;
            bottomMenu = new DetailBottom();
            indicator = new Indicator(1);

            var getCateCont = cateData.cmbYn == "N" ? VODAmocManager.getCateContExtW3 : VODAmocManager.getCateContNxtW3;
            getCateCont.call(null, function (result, data) {
                if (result) {
                    optionList = [];
                    seriesData = cateData.cmbYn == "N" ? data.seriesContsList : data.cmbSeriesContsList;
                    if (!seriesData) {
                        // 편성된 에피소드가 없는 경우
                        HTool.openErrorPopup({message: ERROR_TEXT.ET007});

                        if (cbCreate) {
                            cbCreate(false);
                        }
                        return;
                    }

                    if (Object.prototype.toString.call(seriesData) != '[object Array]') {
                        seriesData = [seriesData];
                    }

                    log.printDbg("test Value ===== " + Object.prototype.toString.call(seriesData));

                    totalContents = seriesData.length;
                    totalPage = Math.floor((totalContents - 1) / (isThumbnailShow ? 6 : 9));
                    curPage = 0;
                    indicator.setSize(totalPage + 1);

                    constructDom();

                    var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                    if (custEnv.perYn == "Y") {
                        VODAmocManager.getItemPerInfo(function (result, data) {
                            if (!result) {
                                extensionAdapter.notifySNMPError("VODE-00013");
                            }

                            log.printDbg("Watching Data = " + cateId + ", " + contsId + " || " + data.rcntContsId);

                            setContentsData(cbCreate, data);
                        }, cateId);
                    }
                    else {
                        setContentsData(cbCreate);
                    }
                }
                else {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});

                    if (cbCreate) {
                        cbCreate(false);
                    }
                }
            }, cateId, DEF.SAID);
        };

        /** 데이터 주입 */
        function setContentsData(cbCreate, data) {
            if (cateId == contsId) {
                if (data && data.rcntContsId) {
                    for (var idx = 0; idx < totalContents; idx++) {
                        if (seriesData[idx].contsId == data.rcntContsId) {
                            episodeFocusedIdx = idx;
                            break;
                        }
                    }
                }
                else {
                    if (seriesData[0].seriesEndYn == "Y") {
                        episodeFocusedIdx = 0;
                    }
                    else {
                        episodeFocusedIdx = totalContents - 1;
                    }
                }
            }
            else {
                if (contsId) {
                    for (var idx = 0; idx < totalContents; idx++) {
                        if (seriesData[idx].contsId == contsId) {
                            episodeFocusedIdx = idx;
                            break;
                        }
                    }
                }
                else {
                    if (data && data.rcntContsId) {
                        for (var idx = 0; idx < totalContents; idx++) {
                            if (seriesData[idx].contsId == data.rcntContsId) {
                                episodeFocusedIdx = idx;
                                break;
                            }
                        }
                    }
                    else {
                        if (seriesData[0].seriesEndYn == "Y") {
                            episodeFocusedIdx = 0;
                        }
                        else {
                            episodeFocusedIdx = totalContents - 1;
                        }
                    }
                }
            }

            hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";
            createContextMenu();
            changePage(Math.floor(episodeFocusedIdx / (isThumbnailShow ? 6 : 9)));

            if (cateData.adultOnlyYn != "Y") {
                addRecentlyVod(cateId, contsId, cateData.imgUrl, cateData.prInfo, isKidsCate);
            }
            // TV포인트 최신화
            DetailUtil.updateTvPoint(function (point) {
                pointData = point;
                setData(seriesData, pointData, cbCreate);
            });
            // 함많본 데이터 생성
            setBottomDataTimer(true);
        }

        /** 연관메뉴 생성 */
        function createContextMenu() {
            contextMenu = new VODDetailContext();
            _this.div.append(contextMenu.getView());
        }

        /** 연관 메뉴 변경 */
        function updateContextMenu(idx) {
            if (idx == null) idx = episodeFocusedIdx;
            contextMenu.clearContextMenu();
            contextMenu.setVodInfo({
                itemName: seriesData[idx].contsName || seriesData[idx].itemName,
                imgUrl: seriesData[idx].imgUrl,
                catId: cateId,
                contsId: seriesData[idx].contsId || seriesData[idx].itemId,
                itemType: 2,
                cmbYn: cateData.cmbYn,
                isDvdYn: seriesData[idx].isDvdYn,
                resolCd: seriesData[idx].resolCd,
                assetId: seriesData[idx].assetId,
                isKids: isKidsCate,
                prInfo: seriesData[idx].prInfo,
                callBackFunc: function (data) {
                    wishObjectList[idx].playCheckFlag = data.wishFlag;
                }
            });

            var optionData = optionList[idx];
            var previewBtnList = [];
            var _previewBtnList = [];
            for (var i in optionData) {
                if (!(optionData[i].buyYn || optionData[i].connerYn || optionData[i].pptBuyYn || optionData[i].lessPrice == 0)) {
                    for (var j in optionData[i].optionList) {
                        if (optionData[i].optionList[j].previewYn === "Y") {
                            if (!_previewBtnList[optionData[i].title]) _previewBtnList[optionData[i].title] = {
                                index: i,
                                previewStartTime: optionData[i].optionList[j].previewStartTime,
                                previewRunTime: optionData[i].optionList[j].previewRunTime
                            };
                        }
                    }
                }
            }
            var isFirstForPreviewBtn = true;
            for (var i in _previewBtnList) {
                if (isFirstForPreviewBtn) {
                    contextMenu.addSeparateLine();
                    var previewTime = (parseInt(Number(_previewBtnList[i].previewRunTime) / 60) || "N") + "분 ";
                    var previewText = previewTime + (_previewBtnList[i].previewStartTime == "00:00:00" ? "미리보기" : "하이라이트 보기");
                    contextMenu.addTitle(previewText, "highLightTitle");
                    isFirstForPreviewBtn = false;
                }
                contextMenu.addButton(i);
                previewBtnList.push(_previewBtnList[i]);
                // 미리보기 버튼 개수 2개로 제한
                if (previewBtnList.length >= 2) break;
            }

            var addedButton = false;

            var optionIdx = getSingleBtnObject(optionList[idx]);
            optionData = optionList[idx][optionIdx];
            contextMenuBtnIdxList = [];

            if (optionData && ((optionData.buyYn && !optionData.connerYn && !optionData.pptBuyYn) || optionData.lessPrice == 0) && !optionData.vodPlus) {
                var btnObj = {};

                for (var j = 0; j < optionData.optionList.length; j++) {
                    if (!optionData.optionList[j].buyYn && optionData.optionList[j].vatPrice != "0") {
                        btnObj.title = optionData.title;
                    }
                }
                if (btnObj.title) {
                    contextMenu.addSeparateLine();
                    contextMenu.addTitle("다른 화질 구매");
                    contextMenu.addButton(btnObj.title);
                    contextMenuBtnIdxList.push(optionIdx);
                    addedButton = true;
                }
            }
            optionIdx = getSeriesBtnObject(optionList[idx]);
            optionData = optionList[idx][optionIdx];

            if (optionData && ((optionData.buyYn && !optionData.connerYn && !optionData.pptBuyYn) || optionData.lessPrice == 0) && !optionData.vodPlus) {
                var btnObj = {};
                for (var j in optionData.seriesData.seriesList) {
                    if (!optionData.seriesData.seriesList[j].buyYn && optionData.seriesData.seriesList[j].vatPrice != "0") {
                        btnObj.title = optionData.title;
                    }
                }
                if (btnObj.title) {
                    if (!addedButton) {
                        contextMenu.addSeparateLine();
                        contextMenu.addTitle("다른 화질 구매");
                    }
                    contextMenu.addButton(btnObj.title);
                    contextMenuBtnIdxList.push(optionIdx);
                }
            }

            contextMenu.setEventListener(function (index) {
                if (previewBtnList.length > 0 && index < previewBtnList.length) {
                    // 미리보기 or 하이라이트 보기 프로세스 시작
                    if (!isAuthorizedContent && !isAuthComplete && UTIL.isLimitAge(optionList[episodeFocusedIdx][previewBtnList[index].index].prInfo)) {
                        openAdultAuthPopup(function (res) {
                            if (res) prePlayProcessForPreview(cateId, optionList[episodeFocusedIdx][previewBtnList[index].index].optionList, optionList[episodeFocusedIdx][previewBtnList[index].index], {
                                isSeries: true,
                                seriesData: seriesData,
                                index: episodeFocusedIdx,
                                sortGb: sortGb,
                                indexListener: indexListener
                            });
                        }, true);
                    }
                    else prePlayProcessForPreview(cateId, optionList[episodeFocusedIdx][previewBtnList[index].index].optionList, optionList[episodeFocusedIdx][previewBtnList[index].index], {
                        isSeries: true,
                        seriesData: seriesData,
                        index: episodeFocusedIdx,
                        sortGb: sortGb,
                        indexListener: indexListener
                    });
                }
                else openPaymentPopup(optionList[episodeFocusedIdx][contextMenuBtnIdxList[index - previewBtnList.length]], cateId, reqPathCd, function (res, data) {
                    if (res) {
                        VODManager.requestPlay(data, reqPathCd);
                    }
                }, false, buttonUpdateCallback);
            });

            // 연관메뉴에서 포커스가 바뀔 때마다 미리보기의 시간 정보를 갱신함
            contextMenu.onFocusChange(function (index, callback) {
                if (previewBtnList.length > 0 && index < previewBtnList.length) {
                    var previewTime = ((parseInt(previewBtnList[index].previewRunTime) / 60) || "N") + "분 ";
                    var previewText = previewTime + (previewBtnList[index].previewStartTime == "00:00:00" ? "미리보기" : "하이라이트 보기");
                    callback(previewText);
                }
            });
        }

        function getSingleBtnObject(objList) {
            for (var i in objList) {
                if (objList[i].type == ButtonObject.TYPE.NORMAL || objList[i].type == ButtonObject.TYPE.DVD) {
                    return i;
                }
            }
        }

        function getSeriesBtnObject(objList) {
            for (var i in objList) {
                if (objList[i].type == ButtonObject.TYPE.SERIES) {
                    return i;
                }
            }
        }

        this.show = function (options) {
            Layer.prototype.show.call(this);
            setMenuFocusChange(isEpisodeListFocused, episodeFocusedIdx);

            if (rowIdx != 2) {
                rowIdx = 2;
                btnFocusIdx = 0;
            }
            if (options && options.resume) {
                if (bottomMenu && bottomMenu.isShow) {
                    bottomMenu.resumeTextAnimation();
                }

                // [2018.03.13 TaeSang] updateOptionList 로직 진행 시점에 episodeFocusedIdx가 targetIndex로 바뀌면서 curOption이 undefined로 셋팅되는 오류가 있음
                // (ex : optionList에 1화까지의 정보밖에 없을때 targetIndex가 2화일 경우 optionList에 2화의 정보가 없어서 curOption이 undefined 됨)
                // 따라서 updateOptionList 진행 후 나머지 로직이 진행되도록 동기식으로 수정함
                // 그 외에도 updateOptionList와 refreshContentsData가 동시에 이루어지다보니
                // 데이터가 꼬이거나 미리보기 토스트 팝업이 잘못 노출되는 경우가 있어서 동기식으로 수정함
                function updateOptionList(_callback) {
                    var curIdx = episodeFocusedIdx;
                    var curOption = optionList[curIdx];
                    optionList = [];
                    optionList[curIdx] = curOption;
                    OptionListFactory.updateOptionList(seriesData[curIdx], cateData, reqPathCd, function (res, optionData) {
                        optionList[curIdx] = optionData;
                        updateButtonArea();
                        _callback();
                    }, wishObjectList[curIdx], curOption);
                }

                updateOptionList(function () {
                    log.printDbg("currentEpisodeIndex is " + episodeFocusedIdx + ", targetIndex is " + targetEpisode);
                    if (targetEpisode != null && targetEpisode != episodeFocusedIdx) {
                        episodeFocusedIdx = targetEpisode;
                        targetEpisode = null;
                        scrollFocusRefresh(isPageFocused = false);
                        setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                        refreshContentsData(function () {
                            DetailUtil.updateTvPoint(function (point) {
                                pointData = point;
                                log.printDbg(JSON.stringify(optionList));
                                _this.div.find("#btn_txt_area").empty().append(DetailUtil.getPointText(pointData, optionList[episodeFocusedIdx][0].vodPlus, optionList[episodeFocusedIdx][0].pptBuyYn, isConnerChk));
                            });
                        });
                        setBtnFocus(0);
                    }
                    else {
                        btnObjectRefresh(function () {
                            // 현재 포커스 중인 버튼이 PPT 버튼이고, 구매완료 되었다면 첫번째 포커스로 강제 셋팅
                            var curFocusBtn = btnList[btnFocusIdx];
                            if (curFocusBtn.type == ButtonObject.TYPE.VODPPT && curFocusBtn.buyData && curFocusBtn.buyData.pptBuyYn) btnFocusIdx = 0;
                            setBtnFocus(btnFocusIdx);
                            DetailUtil.updateTvPoint(function (point) {
                                pointData = point;
                                _this.div.find("#btn_txt_area").empty().append(DetailUtil.getPointText(pointData, optionList[episodeFocusedIdx][0].vodPlus, optionList[episodeFocusedIdx][0].pptBuyYn, isConnerChk));
                            });
                        });
                    }
                });
            }
            else {
                DetailUtil.originSubTitleLengthCheck(_this.div, "SERIES");

                for (var idx = 0; idx < btnList.length; idx++) {
                    _this.div.find("#btn_item_area .vod_btn_area .vod_btn_line").eq(idx).css("width", _this.div.find("#btn_item_area .vod_btn_area .vod_btn_price").eq(idx).width() + "px");
                }

                // 20171110_LeeBaeng :: [고도화-시리즈 상세] 타이틀 1줄 및 2줄에 따른 줄거리 표시 줄수 변경
                var lines = 9;

                if (_this.div.find("#poster_title").height() > 50) {
                    lines--;
                }

                if (cateData.originTitle) {
                    lines--;
                }
                _this.div.find("#poster_seriesInfo").css({"-webkit-line-clamp": "" + lines});
            }

            if (isEpisodeBtnFocus) {
                selectEpisode();
            }
            // 썸네일뷰 상태일 때 포커스가 버튼에 가있는 경우 투명도 적용
            if (isThumbnailShow) {
                _this.div.find("#thumbnail_itemList_div").toggleClass("dimmed", !isEpisodeListFocused && !isPageFocused);
            }

            KTW.managers.service.VoiceableManager.addNodeMatchListener(onNodeMatchListener);

            log.printDbg("show Detail~");
        };

        function onNodeMatchListener (element) {
            log.printDbg("onNodeMatchListener");

            var layer = LayerManager.getLastLayerInStack();
            if (layer.id !== _this.id) {
                return;
            }

            var el = _$(element);

            if (el.attr("id") === "synopsis_btn_area") {
                if (isEpisodeListFocused) {
                    selectEpisode();
                }

                rowIdx = 0;

                setBtnFocus(btnFocusIdx, true);

                setTimeout(function () {
                    _this.controlKey(KEY_CODE.OK);
                }, 0);
            }
            else if (el.hasClass("vod_btn_area")) {
                if (isEpisodeListFocused) {
                    selectEpisode();
                }

                rowIdx = 2;

                var index = Number(el.attr("index"));
                btnFocusIdx = index;

                setBtnFocus(btnFocusIdx, true);

                setTimeout(function () {
                    _this.controlKey(KEY_CODE.OK);
                }, 0);
            }
            else if (el.hasClass("thumbnail_item_div") || el.hasClass("noThumbnail_item_div")) {
                if (!isEpisodeListFocused) {
                    setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                    rowIdx = 2;
                    btnFocusIdx = 0;
                }

                isMovedPages = false;
                episodeFocusedIdx = Number(el.attr("index"));
                refreshContentsData();
            }
        }

        /** 버튼정보 갱신 */
        function refreshBtnBuyInfo(btnIndex) {
            _this.div.find("#btn_arw_left").toggle(Math.floor(btnIndex / 4) > 0);
            _this.div.find("#btn_arw_right").toggle(Math.floor(btnIndex / 4) != Math.floor((btnList.length - 1) / 4));
            _this.div.find("#btnArea #btn_item_area div.vod_btn_area").css("display", "none");

            for (var idx = Math.floor(btnIndex / 4) * 4; idx < (Math.floor(btnIndex / 4) * 4) + 4; idx++) {
                _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + idx + ")").css("display", "block");
            }
        }

        this.hide = function () {
            Layer.prototype.hide.call(this);
            contextMenu.close();

            KTW.managers.service.VoiceableManager.removeNodeMatchListener(onNodeMatchListener);

            log.printDbg("hide Detail~");
        };

        this.remove = function () {
            VODManager.removeIndexListener();
            Layer.prototype.remove.call(this);
        };

        /** 하단 추천 화면 타이머 */
        function setBottomDataTimer(_noTimer) {
            clearTimeout(timeoutBtnData);
            timeoutBtnData = setTimeout(function () {
                seriesData[episodeFocusedIdx].catId = cateId;
                seriesData[episodeFocusedIdx].previewData = previewData;
                bottomMenu.setData(seriesData[episodeFocusedIdx], isAuthorizedContent || seriesData[episodeFocusedIdx].adultOnlyYn === "Y");
                bottomMenu.updateData(seriesData[episodeFocusedIdx], DetailUtil.getAdditionalInfo(cateData, btnList));
            }, _noTimer ? 0 : 300);
        }

        this.controlKey = function (key_code) {
            if (contextMenu.isOpen()) {
                return contextMenu.onKeyAction(key_code);
            }

            if (bottomMenu.controlKey(key_code)) {
                return true;
            }

            switch (key_code) {
                case KEY_CODE.UP:
                    if (isEpisodeListFocused) {
                        isMovedPages = false;
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, NLC.DETAIL_SERIES_LIST, "");
                        } catch (e) {
                        }
                        episodeFocusedIdx = HTool.getIndex(episodeFocusedIdx, -1, totalContents);
                        refreshContentsData();
                    }
                    else if (isPageFocused) {
                        isMovedPages = true;
                        changePage(HTool.getIndex(curPage, -1, totalPage + 1));
                    }
                    else {
                        if (rowIdx < 2) {
                            try {
                                NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                            } catch (e) {
                            }
                        }
                        else {
                            try {
                                NavLogMgr.collectVODDetailPerchase(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, btnList[btnFocusIdx]);
                            } catch (e) {
                            }
                        }

                        if (rowIdx > 0) {
                            rowIdx--;
                        }
                        if (!isNotice && isDetailSyn) {
                            rowIdx = 0;
                        }
                        else if (isNotice && !isDetailSyn) {
                            rowIdx = isNoticeUnFocus ? 2 : 1;
                        }
                        else if (!isNotice && !isDetailSyn) {
                            rowIdx = 2;
                        }
                        else if (isNoticeUnFocus) {
                            rowIdx = 0;
                        }
                        setBtnFocus(btnFocusIdx);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (isEpisodeListFocused) {
                        isMovedPages = false;
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, NLC.DETAIL_SERIES_LIST, "");
                        } catch (e) {
                        }
                        episodeFocusedIdx = HTool.getIndex(episodeFocusedIdx, 1, totalContents);
                        refreshContentsData();
                    }
                    else if (isPageFocused) {
                        isMovedPages = true;
                        changePage(HTool.getIndex(curPage, 1, totalPage + 1));
                    }
                    else {
                        if (rowIdx < 2) {
                            try {
                                NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                            } catch (e) {
                            }
                            rowIdx++;
                        }
                        else {
                            try {
                                NavLogMgr.collectVODDetailPerchase(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, btnList[btnFocusIdx]);
                            } catch (e) {
                            }
                            bottomMenu.show();
                        }

                        if (!isNotice || isNoticeUnFocus) {
                            rowIdx = 2;
                        }
                        setBtnFocus(btnFocusIdx);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    // 인디케이터에 포커스 상태
                    if (isPageFocused) {
                        LayerManager.historyBack();
                    }
                    else if (isEpisodeListFocused) {
                        // 회차 리스트에 포커스 상태
                        // if (totalPage != 0) {
                        //     scrollFocusRefresh(isPageFocused = true);
                        //     setMenuFocusChange(isEpisodeListFocused = false, episodeFocusedIdx);
                        //
                        //     try {
                        //         NavLogMgr.collectVODDetail(key_code, cateId,
                        //             seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId,
                        //             seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName,
                        //             NLC.DETAIL_SERIES_LIST, "");
                        //     } catch (e) {
                        //     }
                        // } else {
                        LayerManager.historyBack();
                        // }
                    }
                    else {
                        if (rowIdx < 2) {
                            // 줄거리더보기 또는 공지사항 포커스 상태
                            try {
                                NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                            } catch (e) {
                            }

                            setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                            rowIdx = 2;
                            btnFocusIdx = 0;
                        }
                        else {
                            // 회차 상세에 포커스 상태
                            if (btnFocusIdx > 0) {
                                if (btnList[btnFocusIdx - 1].buyData.connerYn && btnList[btnFocusIdx - 1].type == ButtonObject.TYPE.CORNER) {
                                    btnFocusIdx = btnFocusIdx <= 1 ? btnFocusIdx : btnFocusIdx - 2;
                                }
                                else if (btnList[btnFocusIdx - 1].buyData.pptBuyYn && btnList[btnFocusIdx - 1].type == ButtonObject.TYPE.VODPPT) {
                                    btnFocusIdx = btnFocusIdx <= 1 ? btnFocusIdx : btnFocusIdx - 2;
                                }
                                else {
                                    try {
                                        NavLogMgr.collectVODDetailPerchase(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, btnList[btnFocusIdx]);
                                    } catch (e) {
                                    }

                                    btnFocusIdx--;
                                }
                            }
                            else {
                                setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                                rowIdx = 2;
                                btnFocusIdx = 0;

                                try {
                                    NavLogMgr.collectVODDetailPerchase(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, btnList[btnFocusIdx]);
                                } catch (e) {
                                }
                            }
                        }
                    }
                    setBtnFocus(btnFocusIdx);
                    return true;
                case KEY_CODE.RIGHT:
                    if (isEpisodeListFocused) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, NLC.DETAIL_SERIES_LIST, "");
                        } catch (e) {
                        }
                        selectEpisode();
                    }
                    else if (isPageFocused) {
                        var prevEpisodeIndex = episodeFocusedIdx;

                        if (isMovedPages) {
                            episodeFocusedIdx = curPage * (isThumbnailShow ? 6 : 9);
                        }

                        isMovedPages = false;
                        setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                        scrollFocusRefresh(isPageFocused = false);
                        // [2018.02.27 TaeSang] 회차 포커스가 변경되지 않아도 인디케이터에서 페이지 리스트로 포커스가 이동되면
                        // 무조건 화면이 갱신되면서 미리보기 토스트 팝업이 노출되는 등 불필요한 작업을 수행함
                        // 실제로 회차가 바뀌었을 경우에만 화면 및 연관메뉴 갱신하도록 수정
                        if (prevEpisodeIndex !== episodeFocusedIdx) refreshContentsData();
                    }
                    else {
                        if (btnFocusIdx < btnLen - 1 && rowIdx >= 2) {
                            if (btnList[btnFocusIdx + 1].buyData.connerYn && btnList[btnFocusIdx + 1].type === ButtonObject.TYPE.CORNER) {
                                btnFocusIdx = btnFocusIdx + 1 >= btnLen - 1 ? btnFocusIdx : btnFocusIdx + 2;
                            }
                            else if (btnList[btnFocusIdx + 1].buyData.pptBuyYn && btnList[btnFocusIdx + 1].type === ButtonObject.TYPE.VODPPT) {
                                btnFocusIdx = btnFocusIdx + 1 >= btnLen - 1 ? btnFocusIdx : btnFocusIdx + 2;
                            }
                            else {
                                try {
                                    NavLogMgr.collectVODDetailPerchase(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, btnList[btnFocusIdx]);
                                } catch (e) {
                                }
                                btnFocusIdx++;
                            }
                        }
                        else {
                            return true;
                        }
                    }
                    setBtnFocus(btnFocusIdx);
                    return true;
                case KEY_CODE.ENTER:
                    if (isEpisodeListFocused) {
                        selectEpisode();
                    }
                    else if (isPageFocused) {
                        this.controlKey(KEY_CODE.RIGHT);
                    }
                    else {
                        if (rowIdx < 2) {
                            var tmpData = {
                                title: seriesData[episodeFocusedIdx].contsName,
                                notice: seriesData[episodeFocusedIdx].bulletin || "",
                                synopsis: seriesData[episodeFocusedIdx].synopsis
                            };
                            LayerManager.activateLayer({
                                obj: {
                                    id: rowIdx == 0 ? "VodSynopsisDetailPopup" : "VodNoticePopup",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        data: tmpData, isKids: isKidsCate
                                    }
                                }, moduleId: "module.vod", new: true, visible: true
                            });
                            try {
                                NavLogMgr.collectVODDetail(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                            } catch (e) {
                            }
                        }
                        else {
                            if (btnList[btnFocusIdx]) {
                                try {
                                    NavLogMgr.collectVODDetailPerchase(key_code, cateId, seriesData[episodeFocusedIdx].contsId || seriesData[episodeFocusedIdx].itemId, seriesData[episodeFocusedIdx].contsName || seriesData[episodeFocusedIdx].itemName, btnList[btnFocusIdx]);
                                } catch (e) {
                                }
                                btnList[btnFocusIdx].enterKeyAction();
                            }
                        }
                    }
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    contextMenu.open({wishFlag: wishObjectList[episodeFocusedIdx].playCheckFlag});
                    return true;
                case KEY_CODE.RED :
                    if (isEpisodeListFocused || isPageFocused) {
                        isMovedPages = false;
                        var listGap = (isThumbnailShow ? 6 : 9);
                        if (isEpisodeListFocused) {
                            if (episodeFocusedIdx == 0) {
                                episodeFocusedIdx = totalContents - (totalContents % listGap);
                            }
                            else if (episodeFocusedIdx % listGap == 0) {
                                episodeFocusedIdx -= listGap;
                            }
                            else {
                                episodeFocusedIdx -= (episodeFocusedIdx % listGap);
                            }
                        }
                        else {
                            episodeFocusedIdx = curPage * listGap;
                            setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                            scrollFocusRefresh(isPageFocused = false);
                        }
                        refreshContentsData();
                    }
                    break;
                case KEY_CODE.BLUE :
                    if (isEpisodeListFocused || isPageFocused) {
                        isMovedPages = false;
                        var listGap = (isThumbnailShow ? 6 : 9);

                        if (isEpisodeListFocused) {
                            if (episodeFocusedIdx == totalContents - 1) {
                                episodeFocusedIdx = Math.min(listGap - 1, totalContents - 1);
                            }
                            else if (episodeFocusedIdx % listGap < (listGap - 1)) {
                                episodeFocusedIdx = Math.min(episodeFocusedIdx + ((listGap - 1) - episodeFocusedIdx % listGap), totalContents - 1);
                            }
                            else {
                                episodeFocusedIdx = episodeFocusedIdx = Math.min(episodeFocusedIdx + listGap, totalContents - 1);
                            }
                        }
                        else {
                            episodeFocusedIdx = Math.min((curPage * listGap) + (listGap - 1), totalContents - 1);
                            setMenuFocusChange(isEpisodeListFocused = true, episodeFocusedIdx);
                            scrollFocusRefresh(isPageFocused = false);
                        }
                        refreshContentsData();
                    }
                    break;
                default:
                    return false;
            }
        };

        /**
         * 현재 포커스된 회차를 선택. 버튼포커스 영역으로 이동한다.
         */
        function selectEpisode() {
            isEpisodeListFocused = false;
            btnFocusIdx = 0;
            setMenuFocusChange(isEpisodeListFocused, episodeFocusedIdx);
            setBtnFocus(btnFocusIdx);
            // 썸네일뷰 상태일 때 버튼영역으로 이동하면 썸네일에 투명도 적용 (선택된 회차 제외하고)
            if (isThumbnailShow) {
                _this.div.find("#thumbnail_itemList_div").addClass("dimmed");
            }
            // 구매 버튼으로 이동했을 때 미리보기&하이라이트 보기 피드백 제공
            if (previewData != null && previewData.isPreview) showToast(previewData.previewText, "ICON");

        }

        /** 인디케이터 포커스 */
        function scrollFocusRefresh(isScroll) {
            isScroll ? indicator.focused() : indicator.blurred();
        }

        /** 포커스 이동 */
        function setBtnFocus(btnIndex) {
            _this.div.find("#btnArea #btn_item_area div.vod_btn_area").removeClass("btn_focus");
            _this.div.find("#notice_infoArea").removeClass("focus");
            _this.div.find("#synopsis_btn_area").removeClass("focus");

            if (btnList[btnIndex].buyYn) {
                _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + btnIndex + ") .vod_btn_buyImg_f").removeClass("btn_focus");
            }

            if (!isEpisodeListFocused && !isPageFocused) {
                switch (rowIdx) {
                    case 0:
                        _this.div.find("#synopsis_btn_area").addClass("focus");
                        DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), btnList[0].buyData);
                        break;
                    case 1:
                        _this.div.find("#notice_infoArea").addClass("focus");
                        DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), btnList[0].buyData);
                        break;
                    case 2:
                        _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + btnIndex + ")").addClass("btn_focus");
                        if (btnList[btnIndex].buyYn) _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + btnIndex + ") .vod_btn_buyImg_f").addClass("btn_focus");

                        _this.div.find("#btn_arw_left").toggle(Math.floor(btnIndex / 4) > 0);
                        _this.div.find("#btn_arw_right").toggle(Math.floor(btnIndex / 4) != Math.floor((btnList.length - 1) / 4));

                        _this.div.find("#btnArea #btn_item_area div.vod_btn_area").css("display", "none");

                        for (var idx = Math.floor(btnIndex / 4) * 4; idx < (Math.floor(btnIndex / 4) * 4) + 4; idx++) {
                            _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + idx + ")").css("display", "block");
                        }

                        DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), btnList[btnIndex].buyData);
                        break;
                }
            }
            else {
                DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), btnList[0].buyData);
            }
        }

        function setMenuFocusChange(isChange, listIndex) {
            _this.div.find("#thumbnail_itemList_div").removeClass("dimmed");
            _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div")).removeClass("noBar");
            _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div") + ":eq(" + (episodeFocusedIdx + 1) + ")").addClass("noBar");
            if (isChange) {
                _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div")).removeClass("select");
                _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div") + ":eq(" + listIndex + ")").addClass("focus");
            }
            else {
                _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div") + ":eq(" + episodeFocusedIdx + ")").addClass("select");
                _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div")).removeClass("focus");
            }
            _this.div.find("#thumbnail_itemList_div").css("-webkit-transform", "translateY(" + (-curPage * (isThumbnailShow ? 894 : 972)) + "px) translateZ(0px)");
            bottomMenu.div.find(".series_bottom #pageChangerArea").toggle(!!isEpisodeListFocused || !!isPageFocused);
        }

        function changePage(page) {
            curPage = page;
            _this.div.find(".thumbDim").removeClass("show");
            _this.div.find("#thumbnail_itemList_div").css("-webkit-transform", "translateY(" + (-page * (isThumbnailShow ? 894 : 972)) + "px)");
            indicator.setPos(page);

            if (totalPage > 0) {
                if (curPage < totalPage) {
                    _this.div.find(".thumbDim.bottom").addClass("show");
                }

                if (curPage > 0) {
                    _this.div.find(".thumbDim.top").addClass("show");
                }
            }
        }

        /** 최초 돔tree 생성 */
        function constructDom() {
            //Background
            var backgroundArea = _$("<div/>", {id: "background_area"});
            backgroundArea.append(_$("<div/>", {class: "bgBlurWrapper"}).append(_$("<img>", {class: "bgBlur posterImg_main"})), _$("<div/>", {class: "bgBlurDim"}));

            //포스터 & 별점
            var customPosterArea = _$("<div/>", {id: "custom_poster_area"});
            var rectPosterArea = _$("<div/>", {id: "rect_poster_area"});
            var seriesSynopArea = _$("<div/>", {id: "seriesPoster_synopsis_area"});

            if (cateData.pImgUrl) {
                customPosterArea.append(_$("<img/>", {
                    class: "series_customVod_img", src: cateData.pImgUrl
                }), _$("<img/>", {class: "customVod_img_locked"}));

                rectPosterArea.css("visibility", "hidden");
                customPosterArea.css("visibility", "inherit");
            }
            else {
                rectPosterArea.css("visibility", "inherit");
                customPosterArea.css("visibility", "hidden");
            }

            rectPosterArea.append(_$("<img>", {id: "main_poster", class: "posterImg_main"}), _$("<img/>", {
                id: "poster_contentCoupon_img", src: window.modulePath + "resource/tag_poster_content_1.png"
            }), _$("<div/>", {id: "poster_contentInfo_area"}), _$("<img/>", {id: "poster_vodLock_img"}));

            var posterArea = _$("<div/>", {id: "posterArea"});
            posterArea.append(_$("<span>", {id: "poster_title"}), _$("<span>", {id: "poster_subTitle"}), _$("<div/>", {id: "main_poster_area"}).append(rectPosterArea, customPosterArea), seriesSynopArea.append(_$("<span>", {class: "basic_text"}), _$("<div/>", {id: "poster_starScore_area"}).append(_$("<div/>", {id: "score_area"})), _$("<span>", {id: "poster_seriesInfo"})));

            if (totalPage != 0) {
                var indicatorArea = _$("<div/>", {id: "indicatorArea"});
                indicatorArea.append(indicator.getView());
                _this.div.append(indicatorArea);
            }

            if (cateData.pImgUrl) {
                seriesSynopArea.addClass("onc");
            }

            // Info
            var infoArea = _$("<div/>", {id: "infoArea"});
            infoArea.append(_$("<div/>", {id: "episodeTitle_area"}).append(_$("<span>", {id: "episodeTitle_text"}), _$("<img/>", {id: "episodeTitle_age_icon"})), _$("<div/>", {id: "subtitle_area"}), _$("<div/>", {id: "subtitle_area_0"}), _$("<div/>", {id: "actor_area"}), _$("<div/>", {id: "synopsis_area"}), _$("<div/>", {id: "synopsis_btn_area", "data-voiceable-action": ""}).append(_$("<img/>", {
                class: "synopsis_btn_img", src: window.modulePath + "resource/btn_detail_vod.png"
            }), _$("<span/>", {class: "synopsis_btn_text"}).text("+ 더보기")));

            // 올레 모바일
            var ollehArea = _$("<div/>", {class: "ollehMobile_icon_area"});
            ollehArea.append(_$("<img/>", {
                class: "mobile_icon_img", src: window.modulePath + "resource/icon_detail_mobile.png"
            }), _$("<span/>", {class: "mobile_icon_text"}).text("올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다"));
            if (KTW.managers.service.ProductInfoManager.isBiz()) {
                ollehArea.css("visibility", "hidden");
            }

            var btnArea = _$("<div/>", {id: "btnArea"});
            btnArea.append(_$("<div/>", {id: "btn_item_area"}), _$("<div/>", {id: "btn_txt_area"}));

            var thumbnailArea = _$("<div>", {id: "thumbnailArea"});
            if (!isThumbnailShow) {
                thumbnailArea.append(_$("<div/>", {id: "noThumbnail_itemList_BG"}));
            }

            thumbnailArea.append(_$("<div/>", {id: "thumbnail_itemList_div"}));

            if (isThumbnailShow && totalPage > 0) {
                thumbnailArea.append(_$("<div/>", {class: "thumbDim top"}), _$("<div/>", {class: "thumbDim bottom"}));
            }

            // Tag를 미리 생성후 마지막 한번에 Append하여 속도 개선한다.
            // Append도 매번 호출이 아닌 Argument형태로 전달하면 그 개수에 맞게 Append를 수행한다
            _this.div.append(backgroundArea, posterArea, infoArea, _$("<div/>", {id: "notice_infoArea"}), ollehArea, btnArea, _$("<div/>", {id: "btn_arw_left"}), _$("<div/>", {id: "btn_arw_right"}), thumbnailArea, // 썸네일
                bottomMenu.createView(cateData.dispContsClass, true, reqPathCd, isKidsCate) //Bottom 메뉴 구성
            );

            if (totalPage != 0) {
                bottomMenu.div.find(".series_bottom").append(_$("<div/>", {id: "pageChangerArea"}).append(_$("<img/>", {class: "leftArrow"}), _$("<img/>", {class: "rightArrow"}), _$("<span>", {id: "pageKeyGuideTxt"}).text("페이지")));
            }
        }

        /** 최초진입 시 View에 데이터 주입*/
        function setData(data, pointData, callback) {
            setSeriesTitle(cateData.catName || cateData.itemName);

            // _this.div.find("#poster_title").html((cateData.catName || cateData.itemName) + "<img id='series_age_icon' src='" + window.modulePath + "resource/icon_age_series_" + window.UTIL.transPrInfo(cateData.prInfo) + ".png'>");
            // 2017.11.03 Y 시리즈 최초 진입 시 시청제한 아이콘 안그려지는 현상으로 인하여 수정
            if (!isAuthorizedContent && data[episodeFocusedIdx].adultOnlyYn != "Y" && UTIL.isLimitAge(data[episodeFocusedIdx].prInfo)) {
                _this.div.find(".customVod_img_locked").css("display", "inherit");
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "inherit");
            }
            else {
                _this.div.find(".customVod_img_locked").css("display", "none");
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "none");
            }

            _this.div.find("#poster_subTitle").text(cateData.originTitle);

            if (!cateData.originTitle) {
                _this.div.find("#poster_subTitle").css("display", "none");
            }
            var imgURLPath = "";
            // 2017.10.18 Y
            // 포스터 유형별 원본 이미지를 요청 후 css로 사이즈 지정해서 그리도록 수정
            switch (parseInt(cateData.posterUseType)) {
                case 0:
                    imgURLPath = cateData.imgUrl;
                    _this.div.find("#main_poster.posterImg_main").css({width: "340px", height: "486px"});
                    break;
                case 1:
                    imgURLPath = cateData.wideImgUrl;
                    break;
                case 2:
                    imgURLPath = cateData.squareImgUrl;
                    _this.div.find("#main_poster.posterImg_main").css({width: "340px", height: "340px"});
                    break;
                default:
                    imgURLPath = cateData.imgUrl;
                    _this.div.find("#main_poster.posterImg_main").css({width: "340px", height: "486px"});
                    break;
            }

            data[episodeFocusedIdx].bulletin = data[episodeFocusedIdx].bulletin.trim();
            isNotice = data[episodeFocusedIdx].bulletin.length > 0;
            _this.div.find("img.posterImg_main").attr("src", imgURLPath).load(function () {
            }).on('error', function () {
                // 포스터 이미지가 없을 경우 예외 처리 (default 포스터 일경우 blur 처리 hide)
                _this.div.find("#background_area .bgBlur").css("display", "none");
                _$(this).attr("src", window.modulePath + "resource/defaultPoster/" + (data.dispContsClass == "01" ? (parseInt(data.posterUseType) == 2 ? "default_music2.png" : "default_music.png") : "default_poster.png"));
            });
            _this.div.find("#poster_seriesInfo").text(cateData.synopsis);
            _this.div.find("#posterArea .basic_text").text("olleh tv 별점");

            // 우측 타이틀 : 초기 로딩시
            setEpisodeTitle(data[episodeFocusedIdx]);

            /** 2017_11_01 : 우측상단 타이틀 2줄 관련 코드 삭제 (고도화에서 1줄 고정으로 변경 됨) @author Leebaeng */
            _this.div.find("#episodeTitle_area").css("width", "1042px");
            _this.div.find("#notice_infoArea").css("top", "408px");
            _this.div.find(".ollehMobile_icon_area").css("top", "486px");
            _this.div.find("#btnArea").css("top", "538px");

            _this.div.find("#subtitle_area").append(_$("<span/>", {class: "subtitle_text"}));
            _this.div.find("#subtitle_area_0").append(_$("<span/>", {class: "subtitle_text"}), _$("<div/>", {class: "subtitle_icon_img"}));

            _this.div.find("#actor_area").append(DetailUtil.getActorArea(_this.div, data[episodeFocusedIdx]));
            _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                type: "SERIES",
                data: data[episodeFocusedIdx].synopsis,
                isNotice: isNotice,
                isKids: isKidsCate,
                callback: function (detailSync) {
                    // 더보기 버튼 활성화
                    isDetailSyn = detailSync;
                    _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                }
            }));

            var tmpMark = cateData.mark;
            if (tmpMark && tmpMark > 0) {
                DetailUtil.setStarNumber(_this.div, tmpMark);
            }

            _this.div.find("#posterArea #poster_starScore_area").append(stars.getView(stars.TYPE.RED_26));
            stars.setRate(_this.div.find("#posterArea #poster_starScore_area"), tmpMark);

            for (var idx = 0; idx < (Math.ceil(totalContents / (isThumbnailShow ? 6 : 9)) * (isThumbnailShow ? 6 : 9)); idx++) {
                if (isThumbnailShow) {
                    _this.div.find("#thumbnail_itemList_div").append(getThumbnailObject(idx));
                    // 2017.08.17 Lazuli
                    // 회차가 없는 경우는 이미 위의 getThumbnailObject 함수에서 디폴트 이미지를 세팅했으므로
                    // drawThumnail을 호출할 필요가 없음
                    // 회차가 있지만 URL이 없는 경우는 drawThumbnail 함수에서 체크하여 디폴트 이미지 세팅함
                    if (data[idx]) {
                        drawThumbnail(idx, data[idx].mainThumbnailUrl);
                    }
                }
                else {
                    _this.div.find("#thumbnail_itemList_div").append(getNoThumbnailObject(idx));
                }
            }

            _this.div.find("#thumbnail_itemList_div").css("height", (Math.ceil(totalContents / (isThumbnailShow ? 6 : 9)) * (isThumbnailShow ? 6 : 9)) * 180 + "px");

            _this.div.find("#notice_infoArea").append(_$("<div/>", {class: "notice_icon"}));
            _this.div.find("#notice_infoArea").append("<div id='notice_splitLine'></div>");
            _this.div.find("#notice_infoArea").append("<div id='notice_contents'>" + data[episodeFocusedIdx].bulletin + "</div>");

            if (window.UTIL.splitText(data[episodeFocusedIdx].bulletin, "RixHead L", 25, 784, -0.7).length == 1) {
                isNoticeUnFocus = true;
                _this.div.find("#notice_infoArea").addClass("noMore");
            }
            else {
                isNoticeUnFocus = false;
                _this.div.find("#notice_infoArea").removeClass("noMore");
            }

            _this.div.find("#notice_infoArea").css("visibility", (data[episodeFocusedIdx].bulletin ? "inherit" : "hidden"));

            var ollehObj = DetailUtil.makeSeamlessText(data[episodeFocusedIdx], savedSeamlessCode, 0);
            if (ollehObj.result !== false) {
                var ollehIcon = _this.div.find(".ollehMobile_icon_area");

                savedSeamlessCode = ollehObj.code;

                if (ollehObj.result === "") {
                    ollehIcon.css("visibility", "hidden");
                }
                else {
                    ollehIcon.css("visibility", "inherit");
                    ollehIcon.find(".mobile_icon_text").text(ollehObj.result);
                }
            }

            wishObjectList[episodeFocusedIdx] = wishObjectList[episodeFocusedIdx] || {contsId: data[episodeFocusedIdx].contsId};

            if (!optionList[episodeFocusedIdx]) {
                var curIdx = episodeFocusedIdx;

                OptionListFactory.getOptionListForSeries(seriesData[curIdx], cateData, reqPathCd, optionList, function (res, optionData) {
                    if (res) {
                        setOptionData(curIdx, optionData);
                    }
                    else {
                        callback(false);
                    }
                }, wishObjectList[curIdx]);
            }
            else {
                setOptionData(episodeFocusedIdx, optionList[episodeFocusedIdx]);
            }

            function setOptionData(idx, optionData) {
                if (optionData.length > 0) {
                    DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), optionData[0]);
                }
                else {
                    DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), null);
                }
                optionList[idx] = optionData;
                isConnerChk = false;
                isSeriesBuyChk = false;
                isConnerBuyChk = false;
                isVodPptBuyChk = false;
                isSingle = false;
                btnList = [];
                _this.div.find("#btn_item_area").empty();
                var singleOrSeries = -1;

                for (var i = 0; i < optionList[idx].length; i++) {
                    singleOrSeries = -1;

                    if (optionList[idx][i].type == ButtonObject.TYPE.CORNER) {
                        isConnerBuyChk = true;
                    }

                    if (optionList[idx][i].type == ButtonObject.TYPE.VODPPT) {
                        isVodPptBuyChk = true;
                    }

                    if (optionList[idx][i].type == ButtonObject.TYPE.SERIES) {
                        isSeriesBuyChk = true;
                        singleOrSeries = 1;
                    }

                    if (optionList[idx][i].type == ButtonObject.TYPE.NORMAL) {
                        isSingle = true;
                        singleOrSeries = 0;
                    }

                    if (optionList[idx][i].type == ButtonObject.TYPE.DVD) {
                        singleOrSeries = 0;
                    }

                    if (optionList[idx][i].connerYn) {
                        isConnerChk = true;
                    }

                    // [kh.kim] 단편, 시리즈 구매 유형이 함께 있을 때
                    // 단편 또는 시리즈에만 이용권이 발급된 경우
                    // 이용권 없는 구매버튼 선택 시 다른 구매유형의 이용권 존재 알림 팝업을 띄워야 한다.
                    // 이용권 체크를 button object 의 execute 를 통해 진입하는 prePlayProcessByButtonObj 에서 하기 때문에..
                    // 다른 구매유형의 이용권 체크하기 위해서는 버튼 데이터를 전달해줘야 한다.
                    // 미리 각 버튼에 다른 구매유형의 버튼을 저장해두고, execute 시 해당 버튼 데이터를 전달해서 처리하도록 했다.
                    // 이 로직은 상세화면에서 구매하는 시나리오에만 적용된다.
                    var otherBtn = null;
                    if (singleOrSeries == 0) {
                        for (var k = 0; k < optionList[idx].length; k++) {
                            if (optionList[idx][k].type == ButtonObject.TYPE.SERIES) {
                                otherBtn = optionList[idx][k];
                                break;
                            }
                        }
                    }
                    else if (singleOrSeries == 1) {
                        for (var k = 0; k < optionList[idx].length; k++) {
                            if (optionList[idx][k].type == ButtonObject.TYPE.NORMAL || optionList[idx][k].type == ButtonObject.TYPE.DVD) {
                                otherBtn = optionList[idx][k];
                                break;
                            }
                        }
                    }

                    DetailUtil.btnObject.prototype.enterKeyAction = btnKeyAction;
                    var btnObj = new DetailUtil.btnObject(optionList[idx][i], otherBtn, _this.div, i);
                    btnList.push(btnObj);

                    _this.div.find("#btn_item_area").append(btnList[i].getView());
                }

                if (seriesData[idx].btnName && seriesData[idx].btnName.length > 0) {
                    var nameList = seriesData[idx].btnName.split("|");
                    var typeList = seriesData[idx].btnType.split("|");
                    var trgtList = seriesData[idx].btnTrgt.split("|");

                    for (var i = 0; i < nameList.length; i++) {
                        var btn = new DetailUtil.addedBtnObject(nameList[i], typeList[i], trgtList[i], optionList[idx].length + i);
                        btnList.push(btn);
                        _this.div.find("#btn_item_area").append(btn.getView());
                    }
                }
                _this.div.find("#btn_txt_area").empty().append(DetailUtil.getPointText(pointData, optionList[idx][0].vodPlus, optionList[idx][0].pptBuyYn, isConnerChk));

                btnLen = btnList.length;
                isEpisodeListFocused = true;

                refreshBtnBuyInfo(0);
                setMenuFocusChange(isEpisodeListFocused, idx);

                for (var i = 0; i < btnList.length; i++) {
                    _this.div.find("#btn_item_area .vod_btn_area:eq(" + i + ") .vod_btn_line").css("width", _this.div.find("#btn_item_area .vod_btn_area:eq(" + i + ") .vod_btn_price").width() + "px");
                }

                _this.div.find("#subtitle_area .subtitle_text").empty().html(DetailUtil.getSubTitle(data[idx]));
                _this.div.find("#subtitle_area_0 .subtitle_text").empty().html(DetailUtil.getSubTitle_0(data[idx]));
                _this.div.find("#subtitle_area_0 .subtitle_icon_img").empty().html(DetailUtil.getSubTitleIconImg(data[idx], {
                    type: "SERIES",
                    parentDiv: _this.div,
                    isLongIcon: isSeriesLongChk,
                    isConnerBuyChk: isConnerBuyChk,
                    isVodPptBuyChk: isVodPptBuyChk,
                    isSingle: isSingle
                }));

                /** 미리보기, 하이라이트보기 체크 */
                previewData = null;
                DetailUtil.checkPreview(optionData, function (result) {
                    previewData = result;
                    bottomMenu.updateContextText(previewData);
                });

                updateContextMenu(idx);

                if (callback) {
                    callback(true);
                }
            }
        }

        // setMetaData
        function refreshContentsData(_optionListCallback) {
            setBottomDataTimer();
            // 2017.06.17 Lazuli
            // 회차 변경 시 btnObjectRefresh 함수가 checkMyContent 종료 후에 호출되면서
            // 단편장기 같은 아이콘 적용 여부가 늦게 갱신되는 문제가 있음. 함수 밖으로 뺌
            DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), null);
            btnObjectRefresh(_optionListCallback);
            if (!isAuthorizedContent && seriesData[episodeFocusedIdx].adultOnlyYn != "Y" && UTIL.isLimitAge(seriesData[episodeFocusedIdx].prInfo)) {
                _this.div.find(".customVod_img_locked").css("display", "inherit");
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "inherit");
            }
            else {
                _this.div.find(".customVod_img_locked").css("display", "none");
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "none");
            }
            seriesData[episodeFocusedIdx].bulletin = seriesData[episodeFocusedIdx].bulletin.trim();
            isNotice = seriesData[episodeFocusedIdx].bulletin.length > 0;
            // _this.div.find("#maintitle_area").html("<span>" + seriesData[episodeFocusedIdx].contsName + "<img id='episodeTitle_age_icon' src='" + window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(seriesData[episodeFocusedIdx].prInfo) + ".png'></span>");

            // 우측 타이틀 : 회차 변경 시
            setEpisodeTitle(seriesData[episodeFocusedIdx]);

            /** 2017_11_01 : 우측상단 타이틀 2줄 관련 코드 삭제 (고도화에서 1줄 고정으로 변경 됨) @author Leebaeng */
            _this.div.find("#episodeTitle_area").css("width", "1042px");
            _this.div.find("#notice_infoArea").css("top", "408px");
            _this.div.find(".ollehMobile_icon_area").css("top", "486px");
            _this.div.find("#btnArea").css("top", "538px");

            _this.div.find("#notice_infoArea #notice_contents").text(seriesData[episodeFocusedIdx].bulletin);
            if (window.UTIL.splitText(seriesData[episodeFocusedIdx].bulletin, "RixHead L", 25, 784, -0.7).length == 1) {
                isNoticeUnFocus = true;
                _this.div.find("#notice_infoArea").addClass("noMore");
            }
            else {
                isNoticeUnFocus = false;
                _this.div.find("#notice_infoArea").removeClass("noMore");
            }

            _this.div.find("#notice_infoArea").css("visibility", (seriesData[episodeFocusedIdx].bulletin ? "inherit" : "hidden"));

            _this.div.find("#actor_area").html(DetailUtil.getActorArea(_this.div, seriesData[episodeFocusedIdx]));
            _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                data: seriesData[episodeFocusedIdx].synopsis,
                type: "SERIES",
                isNotice: isNotice,
                isKids: isKidsCate,
                callback: function (detailSync) {
                    // 더보기 버튼 활성화
                    isDetailSyn = detailSync;
                    _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                }
            }));

            _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div")).removeClass("focus");
            _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div")).removeClass("noBar");
            _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div") + ":eq(" + episodeFocusedIdx + ")").addClass("focus");
            _this.div.find("#thumbnailArea #thumbnail_itemList_div " + (isThumbnailShow ? ".thumbnail_item_div" : ".noThumbnail_item_div") + ":eq(" + (episodeFocusedIdx + 1) + ")").addClass("noBar");

            var ollehObj = DetailUtil.makeSeamlessText(seriesData[episodeFocusedIdx], savedSeamlessCode, 0);
            if (ollehObj.result !== false) {
                var ollehIcon = _this.div.find(".ollehMobile_icon_area");

                savedSeamlessCode = ollehObj.code;

                if (ollehObj.result === "") {
                    ollehIcon.css("visibility", "hidden");
                }
                else {
                    ollehIcon.css("visibility", "inherit");
                    ollehIcon.find(".mobile_icon_text").text(ollehObj.result);
                }
            }

            if (Math.floor(episodeFocusedIdx / (isThumbnailShow ? 6 : 9)) != curPage) {
                changePage(Math.floor(episodeFocusedIdx / (isThumbnailShow ? 6 : 9)));
            }
        }

        /**
         * 각 회차별 타이틀 설정(우측 상단)
         * @param data 해당 회차 정보를 가진 series data
         */
        function setEpisodeTitle(data) {
            var title = data.contsName;

            var fontName = "RixHead B";
            var fontSize = 66;
            var letterSpace = -4.0;
            var maxLineWidthOnEllipsis = 1000;
            var maxLineWidth = 960;

            if (window.UTIL.getTextLength(title, fontName, fontSize, letterSpace) > maxLineWidth) {
                title = HTool.ellipsis(title, fontName, fontSize, maxLineWidthOnEllipsis, "...", letterSpace);
            }
            _this.div.find("#episodeTitle_area span").text(title);
            _this.div.find("#episodeTitle_age_icon").attr("src", window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(data.prInfo) + ".png");
        }

        /**
         * 화면 왼쪽 상단의 시리즈 제목을 설정 한다.
         * (2줄 초과시 ellipsis 처리)
         * @param seriesTitleText
         */
        function setSeriesTitle(seriesTitleText) {
            // RixHead B 폰트, 크기 38 일때 자간 -1.05 or -1.07 (여유값, 최대근사치)

            var lineWidth = 350;
            var lastLineWidth = 310;

            var seriesTitleData = ellipsisMultilineTxt(seriesTitleText, 2, lineWidth, lastLineWidth, "RixHead B", 38, -1.07);
            _this.div.find("#poster_title").html((seriesTitleData.txt) + "<img id='series_age_icon' src='" + window.modulePath + "resource/icon_age_series_" + window.UTIL.transPrInfo(cateData.prInfo) + ".png'>");

            if (seriesTitleData.totalLineCnt == 1 && cateData.originTitle) {
                _this.div.find("#custom_poster_area").css("top", "-49px");
            }
            else {
                _this.div.find("#custom_poster_area").css("top", "-63px");
            }

            if (seriesTitleData.totalLineCnt == 2 && !seriesTitleData.isEllipsis) {
                _this.div.find("#poster_title").css("width", "415px");
            }
            else if (seriesTitleData.isEllipsis) {
                _this.div.find("#poster_title").css("width", lineWidth + "px");
            }
            else {
                if (window.UTIL.getTextLength(seriesTitleData.txt, "RixHead B", 38, -1.05) < lineWidth) {
                    _this.div.find("#poster_title").css("width", "415px");
                }
                else {
                    _this.div.find("#poster_title").css("width", lineWidth + "px");
                }
            }
        }

        /**
         * 긴 문장을 lineCnt의 멀티라인 텍스트로 만들고 필요한 경우 마지막 라인의 텍스트를 ellipsis 처리한다.
         * @param text 가공하고자 하는 text의 원본
         * @param maxLineCnt 표시할 최대 라인 수
         * @param lineWidth 각 라인별 넓이
         * @param lastLineWidth 마지막 라인의 넓이 ( ellipsis 표기 후 뒤에 GUI 등을 붙일 경우, 필요 없을 경우 lineWidth와 동일한 값 대입 )
         * @param fontName for Check Text's Width
         * @param fontSize for Check Text's Width
         * @param letterSpace for Check Text's Width
         * @return 가공된 Text
         */
        function ellipsisMultilineTxt(text, maxLineCnt, lineWidth, lastLineWidth, fontName, fontSize, letterSpace) {
            var tmpStr = "";
            var lastLineTxt = "";

            var lineTxt = [];
            var isEllipsis = false;

            var tempLintTxt = window.UTIL.stringToMultiLine(text, fontName, fontSize, lineWidth, letterSpace);

            for (var j in tempLintTxt) {
                lineTxt.push(tempLintTxt[j]);
            }

            var lastLineIdx = (lineTxt.length > maxLineCnt) ? maxLineCnt - 1 : lineTxt.length - 1;

            if (lineTxt.length > maxLineCnt) {
                isEllipsis = true;
            }
            else if (lineTxt.length == maxLineCnt) {
                isEllipsis = (window.UTIL.getTextLength(lineTxt[lastLineIdx], fontName, fontSize, letterSpace) > lineWidth);
            }
            else {
                isEllipsis = false;
            }

            if (isEllipsis) {
                if (window.UTIL.getTextLength(lineTxt[lastLineIdx], fontName, fontSize, letterSpace) < lastLineWidth) {
                    lastLineTxt = lineTxt[lastLineIdx] + "...";
                }
                else {
                    lastLineTxt = HTool.ellipsis(lineTxt[lastLineIdx], fontName, fontSize, lastLineWidth, "...", letterSpace);
                }
            }
            else {
                lastLineTxt = lineTxt[lastLineIdx];
            }

            for (var i = 0; i < lastLineIdx; i++) tmpStr += lineTxt[i] + "<br>";

            tmpStr += lastLineTxt;

            return {
                txt: tmpStr, lineCnt: lastLineIdx + 1, totalLineCnt: lineTxt.length, isEllipsis: isEllipsis
            };
        }

        function btnObjectRefresh(callback) {
            var curIdx = episodeFocusedIdx;
            wishObjectList[curIdx] = wishObjectList[curIdx] || {contsId: seriesData[curIdx].contsId};
            if (!optionList[curIdx]) OptionListFactory.getOptionListForSeries(seriesData[curIdx], cateData, reqPathCd, optionList, function (res, optionData) {
                optionList[curIdx] = optionData;
                setOptionData(curIdx);
                if (callback) callback();
            }, wishObjectList[curIdx]);
            else {
                setOptionData(curIdx);
                if (callback) callback();
            }

            function setOptionData(index) {
                _this.div.find("#btn_item_area").empty();
                isConnerChk = false;
                isSeriesBuyChk = false;
                isConnerBuyChk = false;
                isVodPptBuyChk = false;
                isSingle = false;
                btnList = [];
                _this.div.find("#btn_item_area").empty();

                var singleOrSeries = -1;
                for (var i = 0; i < optionList[index].length; i++) {
                    singleOrSeries = -1;

                    if (optionList[index][i].type == ButtonObject.TYPE.CORNER) {
                        isConnerBuyChk = true;
                    }

                    if (optionList[index][i].type == ButtonObject.TYPE.VODPPT) {
                        isVodPptBuyChk = true;
                    }

                    if (optionList[index][i].type == ButtonObject.TYPE.SERIES) {
                        isSeriesBuyChk = true;
                        singleOrSeries = 1;
                    }

                    if (optionList[index][i].type == ButtonObject.TYPE.NORMAL) {
                        isSingle = true;
                        singleOrSeries = 0;
                    }

                    if (optionList[index][i].type == ButtonObject.TYPE.DVD) {
                        singleOrSeries = 0;
                    }

                    if (optionList[index][i].connerYn) {
                        isConnerChk = true;
                    }

                    // [kh.kim] 단편, 시리즈 구매 유형이 함께 있을 때
                    // 단편 또는 시리즈에만 이용권이 발급된 경우
                    // 이용권 없는 구매버튼 선택 시 다른 구매유형의 이용권 존재 알림 팝업을 띄워야 한다.
                    // 이용권 체크를 button object 의 execute 를 통해 진입하는 prePlayProcessByButtonObj 에서 하기 때문에..
                    // 다른 구매유형의 이용권 체크하기 위해서는 버튼 데이터를 전달해줘야 한다.
                    // 미리 각 버튼에 다른 구매유형의 버튼을 저장해두고, execute 시 해당 버튼 데이터를 전달해서 처리하도록 했다.
                    // 이 로직은 상세화면에서 구매하는 시나리오에만 적용된다.
                    var otherBtn = null;
                    if (singleOrSeries == 0) {
                        for (var k = 0; k < optionList[index].length; k++) {
                            if (optionList[index][k].type == ButtonObject.TYPE.SERIES) {
                                otherBtn = optionList[index][k];
                                break;
                            }
                        }
                    }
                    else if (singleOrSeries == 1) {
                        for (var k = 0; k < optionList[index].length; k++) {
                            if (optionList[index][k].type == ButtonObject.TYPE.NORMAL || optionList[index][k].type == ButtonObject.TYPE.DVD) {
                                otherBtn = optionList[index][k];
                                break;
                            }
                        }
                    }

                    DetailUtil.btnObject.prototype.enterKeyAction = btnKeyAction;
                    var btnObj = new DetailUtil.btnObject(optionList[index][i], otherBtn, _this.div);
                    btnList.push(btnObj);

                    _this.div.find("#btn_item_area").append(btnList[i].getView());
                }
                if (seriesData[index].btnName && seriesData[index].btnName.length > 0) {
                    var nameList = seriesData[index].btnName.split("|");
                    var typeList = seriesData[index].btnType.split("|");
                    var trgtList = seriesData[index].btnTrgt.split("|");

                    for (var i = 0; i < nameList.length; i++) {
                        var btn = new DetailUtil.addedBtnObject(nameList[i], typeList[i], trgtList[i]);
                        btnList.push(btn);
                        _this.div.find("#btn_item_area").append(btn.getView());
                    }
                }
                _this.div.find("#btn_txt_area").empty().append(DetailUtil.getPointText(pointData, optionList[index][0].vodPlus, optionList[index][0].pptBuyYn, isConnerChk));

                btnLen = btnList.length;

                for (var idx = 0; idx < btnList.length; idx++) {
                    _this.div.find("#btn_item_area .vod_btn_area:eq(" + idx + ") .vod_btn_line").css("width", _this.div.find("#btn_item_area .vod_btn_area:eq(" + idx + ") .vod_btn_price").width() + "px");
                }
                _this.div.find("#btnArea #btn_item_area div.vod_btn_area").css("display", "none");

                for (var idx = 0; idx < 4; idx++) {
                    _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + idx + ")").css("display", "block");
                }

                _this.div.find("#subtitle_area .subtitle_text").empty().html(DetailUtil.getSubTitle(seriesData[index]));
                _this.div.find("#subtitle_area_0 .subtitle_text").empty().html(DetailUtil.getSubTitle_0(seriesData[index]));
                _this.div.find("#subtitle_area_0 .subtitle_icon_img").empty().html(DetailUtil.getSubTitleIconImg(seriesData[index], {
                    type: "SERIES",
                    parentDiv: _this.div,
                    isLongIcon: isSeriesLongChk,
                    isConnerBuyChk: isConnerBuyChk,
                    isVodPptBuyChk: isVodPptBuyChk,
                    isSingle: isSingle
                }));

                DetailUtil.originSubTitleLengthCheck(_this.div, "SERIES");

                if (!isPageFocused && !isEpisodeListFocused) {
                    setBtnFocus(btnFocusIdx);
                }
                else {
                    DetailUtil.couponIconUpdate(_this.div.find("#rect_poster_area #poster_contentCoupon_img"), btnList[0].buyData);
                }

                /** 미리보기, 하이라이트보기 체크 */
                previewData = null;
                DetailUtil.checkPreview(optionList[curIdx], function (result) {
                    previewData = result;
                    bottomMenu.updateContextText(previewData);
                });

                updateContextMenu(index);
            }
        }

        function updateButtonArea() {
            _this.div.find("#btn_item_area").empty();
            isConnerChk = false;
            isSeriesBuyChk = false;
            isConnerBuyChk = false;
            isVodPptBuyChk = false;
            isSingle = false;
            btnList = [];
            _this.div.find("#btn_item_area").empty();

            log.printDbg(JSON.stringify(optionList));
            log.printDbg(JSON.stringify(episodeFocusedIdx));
            log.printDbg(JSON.stringify(optionList[episodeFocusedIdx]));

            var singleOrSeries = -1;
            for (var i = 0; i < optionList[episodeFocusedIdx].length; i++) {
                singleOrSeries = -1;

                if (optionList[episodeFocusedIdx][i].type == ButtonObject.TYPE.CORNER) {
                    isConnerBuyChk = true;
                }

                if (optionList[episodeFocusedIdx][i].type == ButtonObject.TYPE.VODPPT) {
                    isVodPptBuyChk = true;
                }

                if (optionList[episodeFocusedIdx][i].type == ButtonObject.TYPE.SERIES) {
                    isSeriesBuyChk = true;
                    singleOrSeries = 1;
                }

                if (optionList[episodeFocusedIdx][i].type == ButtonObject.TYPE.NORMAL) {
                    isSingle = true;
                    singleOrSeries = 0;
                }

                if (optionList[episodeFocusedIdx][i].type == ButtonObject.TYPE.DVD) {
                    singleOrSeries = 0;
                }

                if (optionList[episodeFocusedIdx][i].connerYn) {
                    isConnerChk = true;
                }

                // [kh.kim] 단편, 시리즈 구매 유형이 함께 있을 때
                // 단편 또는 시리즈에만 이용권이 발급된 경우
                // 이용권 없는 구매버튼 선택 시 다른 구매유형의 이용권 존재 알림 팝업을 띄워야 한다.
                // 이용권 체크를 button object 의 execute 를 통해 진입하는 prePlayProcessByButtonObj 에서 하기 때문에..
                // 다른 구매유형의 이용권 체크하기 위해서는 버튼 데이터를 전달해줘야 한다.
                // 미리 각 버튼에 다른 구매유형의 버튼을 저장해두고, execute 시 해당 버튼 데이터를 전달해서 처리하도록 했다.
                // 이 로직은 상세화면에서 구매하는 시나리오에만 적용된다.
                var otherBtn = null;
                if (singleOrSeries == 0) {
                    for (var k = 0; k < optionList[episodeFocusedIdx].length; k++) {
                        if (optionList[episodeFocusedIdx][k].type == ButtonObject.TYPE.SERIES) {
                            otherBtn = optionList[episodeFocusedIdx][k];
                            break;
                        }
                    }
                }
                else if (singleOrSeries == 1) {
                    for (var k = 0; k < optionList[episodeFocusedIdx].length; k++) {
                        if (optionList[episodeFocusedIdx][k].type == ButtonObject.TYPE.NORMAL || optionList[episodeFocusedIdx][k].type == ButtonObject.TYPE.DVD) {
                            otherBtn = optionList[episodeFocusedIdx][k];
                            break;
                        }
                    }
                }

                DetailUtil.btnObject.prototype.enterKeyAction = btnKeyAction;
                var btnObj = new DetailUtil.btnObject(optionList[episodeFocusedIdx][i], otherBtn, _this.div);
                btnList.push(btnObj);

                _this.div.find("#btn_item_area").append(btnList[i].getView());
            }

            var curSeriesData = seriesData[episodeFocusedIdx];

            if (curSeriesData.btnName && curSeriesData.btnName.length > 0) {
                var nameList = curSeriesData.btnName.split("|");
                var typeList = curSeriesData.btnType.split("|");
                var trgtList = curSeriesData.btnTrgt.split("|");

                for (var i = 0; i < nameList.length; i++) {
                    var btn = new DetailUtil.addedBtnObject(nameList[i], typeList[i], trgtList[i]);
                    btnList.push(btn);
                    _this.div.find("#btn_item_area").append(btn.getView());
                }
            }
            _this.div.find("#btn_txt_area").empty().append(DetailUtil.getPointText(pointData, optionList[episodeFocusedIdx][0].vodPlus, optionList[episodeFocusedIdx][0].pptBuyYn, isConnerChk));

            btnLen = btnList.length;

            for (var idx = 0; idx < btnList.length; idx++) {
                _this.div.find("#btn_item_area .vod_btn_area:eq(" + idx + ") .vod_btn_line").css("width", _this.div.find("#btn_item_area .vod_btn_area:eq(" + idx + ") .vod_btn_price").width() + "px");
            }
            _this.div.find("#btnArea #btn_item_area div.vod_btn_area").css("display", "none");

            for (var idx = 0; idx < 4; idx++) {
                _this.div.find("#btnArea #btn_item_area div.vod_btn_area:eq(" + idx + ")").css("display", "block");
            }

            _this.div.find("#subtitle_area .subtitle_text").empty().html(DetailUtil.getSubTitle(curSeriesData));
            _this.div.find("#subtitle_area_0 .subtitle_text").empty().html(DetailUtil.getSubTitle_0(curSeriesData));
            _this.div.find("#subtitle_area_0 .subtitle_icon_img").empty().html(DetailUtil.getSubTitleIconImg(curSeriesData, {
                type: "SERIES",
                parentDiv: _this.div,
                isLongIcon: isSeriesLongChk,
                isConnerBuyChk: isConnerBuyChk,
                isVodPptBuyChk: isVodPptBuyChk,
                isSingle: isSingle
            }));

            DetailUtil.originSubTitleLengthCheck(_this.div, "SERIES");

            if (!isPageFocused && !isEpisodeListFocused) {
                setBtnFocus(btnFocusIdx);
            }

            /** 미리보기, 하이라이트보기 체크 */
            previewData = null;
            DetailUtil.checkPreview(optionList[episodeFocusedIdx], function (result) {
                previewData = result;
                bottomMenu.updateContextText(previewData);
            });

            updateContextMenu(episodeFocusedIdx);
        }

        /** 버튼 액션 정의 */
        var btnKeyAction = function () {
            var that = this;

            if (!isAuthorizedContent && !isAuthComplete && UTIL.isLimitAge(that.buyData.prInfo)) {
                openAdultAuthPopup(function (res) {
                    if (res) {
                        isAuthComplete = true;
                        enterKeyActionImpl();
                    }
                }, true);
            }
            else {
                enterKeyActionImpl();
            }

            function enterKeyActionImpl() {
                that.buyData.execute(cateId, reqPathCd, function (res, resData) {
                    if (res) {
                        resData.isAuthorizedContent = isAdultCate || resData.originData.data.adultOnlyYn === "Y";
                        VODManager.requestPlaySeries(resData, (sortGb ? seriesData : [resData]), reqPathCd, episodeFocusedIdx, sortGb, indexListener);
                    }
                }, null, null, isAuthorizedContent || isAuthComplete, true, buttonUpdateCallback, false, {otherCouponBtn: that.otherCouponBtn});
            }
        };

        function indexListener(_selectedCatId, _selectedContsId) {
            log.printDbg("getSeriesIndex ::: " + cateId + " : " + _selectedCatId);
            log.printDbg("getSeriesIndex_selectedContsId ::: " + _selectedContsId);

            if (cateId == _selectedCatId) {
                targetEpisode = null;
                for (var i = 0; i < seriesData.length; i++) {
                    if (seriesData[i].contsId == _selectedContsId) {
                        targetEpisode = i;
                        break;
                    }
                }
            }
        }

        /** 미구매 상품 구매완료 시 구매정보 갱신을 위한 콜백 */
        function buttonUpdateCallback(buyData) {
            var tmpOption = optionList[episodeFocusedIdx];
            optionList = [];
            optionList[episodeFocusedIdx] = tmpOption;
            OptionListFactory.updateBuyInfo(optionList[episodeFocusedIdx], buyData, cateData);
            btnObjectRefresh(function () {
                if (buyData && buyData.buyType == "C") {
                    btnFocusIdx = 0;
                }

                setBtnFocus(btnFocusIdx);
            });
            for (var idx = 0; idx < btnList.length; idx++) {
                _this.div.find("#btn_item_area .vod_btn_area .vod_btn_line").eq(idx).css("width", _this.div.find("#btn_item_area .vod_btn_area .vod_btn_price").eq(idx).width() + "px");
            }
        }

        /** 썸네일 */
        function getNoThumbnailObject(index) {
            var div = _$("<div/>", {class: "noThumbnail_item_div", "data-voiceable-action": "", index: index});
            if (index < totalContents) {
                div.append(_$("<div/>", {class: "thumbnail_bar_area"}));
                div.find(".thumbnail_bar_area").append("<div class='noThumbnail_str'>" + seriesData[index].seqNo + "</div>");
            }
            else {
                div.append(_$("<div/>", {class: "thumbnail_bar_area"}));
            }
            return div;
        }

        function drawThumbnail(index, thumbnailImg) {
            var defaultThumUrl = modulePath + (isKidsCate ? "resource/kids/thum_noimg_kids_w224.png" : "resource/default_thumb_sm.jpg");
            _this.div.find("#thumbnail_itemList_div .thumbnail_item_div:eq(" + index + ") .thumbnail_item_img").attr({
                src: thumbnailImg + "?w=224&h=126&quality=90"
            }).load(function () {
            }).on('error', function () {
                _$(this).attr({src: defaultThumUrl});
            });
        }

        // Thumbnail 이미지 로드시 타임아웃 처리와 퍼포먼스 개선 필요
        // <img> 가 아닌 <div> background-image로 표시하도록 수정
        function getThumbnailObject(index) {
            var div = _$("<div/>", {class: "thumbnail_item_div", "data-voiceable-action": "", index: index});
            if (index < totalContents) {

                div.append("<img class='thumbnail_item_img'>", // 회차별 썸네일
                    "<img class='thumbnail_dim_img' src=" + ROOT_URL + "resource/dim_detail_episode_thum.png>", // 회차 번호 밑에 깔리는 그림자
                    "<div class='thumbnail_str'>" + seriesData[index].seqNo + "</div>", // 회차 번호
                    "<div class='thumbnail_unFocus_div'></div>", // 포커스 되지 않은 썸네일 살짝 어둡게
                    "<img class='thumbnail_select_img'>" // 선택된 썸네일 테두리 표시
                );
            }
            else {
                // 기본 이미지 (회차가 더이상 없을 경우)
                div.append("<img class='thumbnail_item_img default'>");
            }
            return div;
        }
    };

    vod_detail_series_layer.prototype = new Layer();
    vod_detail_series_layer.prototype.constructor = vod_detail_series_layer;

    vod_detail_series_layer.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_detail_series_layer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodDetailSeriesLayer"] = vod_detail_series_layer;
})();