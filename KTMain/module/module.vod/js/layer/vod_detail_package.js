/**
 * Created by ksk91_000 on 2016-11-02.
 */
(function() {
    var vod_detail_package_layer = function vodDetailPackageLayer(options) {
        Layer.call(this, options);
        var _this = this;
        var btnList = [];
        var currentIdx = 0;
        var focusRow = 0;
        var paramData;
        var pkgData;
        var pkgVodListData;
        var pointData;
        var listTotalPage = 0;
        var isDetailSyn = false;
        var isWishPkg = false;
        var contextMenu = null;
        var isAdultCate = null;
        var isAuthorizedContent = null;
        var ageLimit = null;

        var buyCheckData;
        var listCurPage = 0;
        var isKids = false;

        var FOCUS_ROW_BUTTON = 0;
        var FOCUS_ROW_LIST = 1;
        var FOCUS_ROW_DETAILBTN = 2;

        this.init = function(cbCreate) {
            paramData = this.getParams();
            this.div.attr({class: "arrange_frame vod vod_detail_package_layer", style: "background:black"});
            isKids = paramData.isKids;
            if (isKids) this.div.addClass("kids");
            isAdultCate = paramData.isAdultCate;
            isAuthorizedContent = isAdultCate||paramData.isAuthorizedContent;
            ageLimit = AuthManager.getPR();
            btnList = [];
            focusRow = FOCUS_ROW_BUTTON;
            currentIdx = 0;
            pkgData = paramData.cateInfo;
            console.debug(pkgData);
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";
            VODAmocManager.checkMyPkgVodW3(function(res, data) {
                if (res) {
                    buyCheckData = data;
                    isWishPkg = data.playCheckYn === "Y";
                    VODAmocManager.getItemDetlListW3(function(res, data) {
                        if (res) {
                            pkgVodListData = data.itemDetailList;
                            if (!Array.isArray(pkgVodListData)) pkgVodListData = [pkgVodListData];
                            constructDom(pkgVodListData);
                            DetailUtil.updateTvPoint(function(pointData) {
                                setData(pkgData, pointData);
                                cbCreate(true);
                            });
                        } else cbCreate(false);
                    }, pkgData.itemId || pkgData.catId, 1, "A");
                } else cbCreate(false);
            }, pkgData.pkgVodPrdCd, "H", paramData.req_cd, buyTypeYn, null);
        };

        this.show = function(options) {
            Layer.prototype.show.call(this);
            _this.div.find(".vod_btn_line").css("width", _this.div.find(".vod_btn_price").width() + "px");
            if (options && options.resume) {
                _this.refreshBuyInfo();
                DetailUtil.updateTvPoint(function(_point) {
                    _this.div.find("#btn_txt_area").empty();
                    _this.div.find("#btn_txt_area").append(DetailUtil.getPointText(_point, false, false, false));
                });
            }
        };

        this.refreshBuyInfo = function() {
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";
            VODAmocManager.checkMyPkgVodW3(function(res, data) {
                if (res) {
                    buyCheckData = data;
                    isWishPkg = data.playCheckYn === "Y";

                    var div = _this.div;
                    // 2017.08.04 Lazuli
                    // 구매 완료 후 구매버튼 갱신 시 ltFlag를 체크하지 않고 price 값만 참조하고 있어,
                    // 장기 전용 패키지 구매 시 구매버튼 갱신이 안되는 오류 발생
                    // ltFlag를 체크하여 ltPrice도 함께 체크하도록 수정
                    var prices = paramData.cateInfo.ltFlag == 2 ? paramData.cateInfo.vatLtPrice : paramData.cateInfo.vatPrice;

                    // [WEBIIIHOME-3809] 패키지 구매된 상태이면 무료 표시 div의 visibility를 변경
                    if ((buyCheckData.buyCheckYn == "Y" || buyCheckData.connerYn == "Y" || buyCheckData.pptBuyYn == "Y") && prices - 0 > 0) {
                        div.find(".vod_btn_line").css("visibility", "inherit");
                        div.find(".vod_btn_buyImg").css("visibility", "inherit");
                        div.find(".vod_btn_area").addClass("free");
                    } else {
                        div.find(".vod_btn_line").css("visibility", "none");
                        div.find(".vod_btn_buyImg").css("visibility", "none");
                        div.find(".vod_btn_area").removeClass("free");
                    }
                }
            }, pkgData.pkgVodPrdCd, "H", paramData.req_cd, buyTypeYn, null);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
            contextMenu.close();
        };

        // 연관메뉴 변경
        function setContextTitle() {
            _this.div.find(".contextTextArea").removeClass("btn_focus");
            if (focusRow == FOCUS_ROW_BUTTON) {
                _this.div.find(".contextTextArea").addClass("btn_focus");
                _this.div.find(".contextTextArea .wishText").text(isWishPkg ? "찜해제" : "찜하기");
            } else _this.div.find(".contextTextArea .wishText").text("찜");
        }

        function createContextMenu() {
            contextMenu = new VODDetailContext();
            _this.div.append(contextMenu.getView());
        }

        // 패키지 버튼으로 이동
        function setFocusRowToButton() {
            focusRow = FOCUS_ROW_BUTTON;
            currentIdx = 0;
            movieListFocus();
            buttonFocusRefresh();
            packageInfoRefresh();
        }

        // 패키지 줄거리 더보기로 이동
        function setFocusRowToDetail(key_code) {
            focusRow = FOCUS_ROW_DETAILBTN;
            detailBtnFocus();
        }

        // 패키지 리스트로 이동
        function setFocusRowToList(key_code) {
            focusRow = FOCUS_ROW_LIST;
            movieListFocus();
        }

        this.controlKey = function(key_code) {
            if (contextMenu.isOpen()) return contextMenu.onKeyAction(key_code);
            switch (key_code) {
                case KEY_CODE.UP:
                    if (focusRow == FOCUS_ROW_BUTTON) {
                        try {
                            sendVODDetailPurchase(key_code);
                        } catch (e) {
                        }
                        setFocusRowToList(key_code);
                    } else if (focusRow == FOCUS_ROW_LIST && isDetailSyn) {
                        setFocusRowToDetail(key_code)
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (focusRow == FOCUS_ROW_LIST) {
                        try {
                            sendVODDetail(key_code, NLC.DETAIL_PACKAGE_LIST);
                        } catch (e) {
                        }
                        setFocusRowToButton();
                    } else if (focusRow == FOCUS_ROW_DETAILBTN) {
                        setFocusRowToList(key_code);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (focusRow == FOCUS_ROW_BUTTON || focusRow == FOCUS_ROW_DETAILBTN) LayerManager.historyBack();
                    else if (focusRow == FOCUS_ROW_LIST) {
                        try {
                            sendVODDetail(key_code, NLC.DETAIL_PACKAGE_LIST);
                        } catch (e) {
                        }
                        currentIdx = HTool.getIndex(currentIdx, -1, pkgVodListData.length);
                        movieListFocus();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow == FOCUS_ROW_LIST) {
                        try {
                            sendVODDetail(key_code, NLC.DETAIL_PACKAGE_LIST);
                        } catch (e) {
                        }
                        currentIdx = HTool.getIndex(currentIdx, 1, pkgVodListData.length);
                        movieListFocus();
                    }
                    return true;
                case KEY_CODE.ENTER:
                    switch (focusRow) {
                        case FOCUS_ROW_BUTTON :
                            packagePlayProcess(pkgData, pkgVodListData, buyCheckData, paramData.req_cd,
                                function(res, data) {  // 구매후 재생 혹은 이미 구매된 패키지 재생시 호출
                                    if (res) {
                                        VODManager.requestPlay(data, paramData.req_cd);
                                    }
                                },
                                function updateCallback(buyData) {  // 미구매 상품 신규 구매후 구매정보 갱신을 위해 호출
                                    if (buyData.buyType === "K") {
                                        _this.refreshBuyInfo();
                                    }
                                }
                            );
                            try {
                                sendVODDetailPurchase(key_code);
                            } catch (e) {
                            }
                            return true;
                        case FOCUS_ROW_LIST :
                            // VOD 상세 이동
                            var cateInfo, catId;
                            // 2017.08.07 상위 catId를 넘겨주고 있어서 최근 시청목록에 추가 시 편성삭제 컨텐츠로 확인되는 이슈사항으로 해당 패키지 catId를 넘겨주도록 수정
                            if (pkgVodListData[currentIdx].itemType == 2) cateInfo = pkgData, catId = pkgData.catId || pkgData.itemId;
                            else cateInfo = pkgVodListData[currentIdx], catId = pkgVodListData[currentIdx].itemId;
                            VODImpl.showDetailWithData({
                                callback: function(success) {log.printDbg(success + " ShowDetail Complete");},
                                cat_id: catId,
                                const_id: pkgVodListData[currentIdx].itemId,
                                cateInfo: cateInfo,
                                contsInfo: pkgVodListData[currentIdx],
                                req_cd: paramData.req_cd,
                                isKids: isKids
                            });
                            try {
                                sendJumpVOD(NLC.DETAIL_PACKAGE_LIST);
                            } catch (e) {
                            }
                            return true;
                        case FOCUS_ROW_DETAILBTN :
                            // 더보기 상세 팝업 실행
                            var tmpData = {
                                title: pkgVodListData[currentIdx].contsName || pkgVodListData[currentIdx].itemName,
                                notice: pkgVodListData[currentIdx].bulletin || "",
                                synopsis: pkgVodListData[currentIdx].synopsis
                            };
                            LayerManager.activateLayer({
                                obj: {
                                    id: "VodSynopsisDetailPopup",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        data: tmpData,
                                        isKids: isKids
                                    }
                                },
                                moduleId: "module.vod",
                                new: true,
                                visible: true
                            });
                            try {
                                sendJumpVOD(NLC.DETAIL_PACKAGE_LIST);
                            } catch (e) {
                            }
                            return true;
                    }
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    if (focusRow == FOCUS_ROW_LIST) {
                        log.printDbg("openPkgContext ::: " + JSON.stringify(pkgVodListData[currentIdx]));
                        contextMenu.setVodInfo({
                            itemName: pkgVodListData[currentIdx].contsName || pkgVodListData[currentIdx].itemName,
                            imgUrl: pkgVodListData[currentIdx].imgUrl,
                            catId: pkgData.catId || pkgData.itemId,
                            contsId: pkgVodListData[currentIdx].contsId || pkgVodListData[currentIdx].itemId,
                            itemType: pkgVodListData[currentIdx].itemType,
                            cmbYn: pkgVodListData[currentIdx].cmbYn,
                            isDvdYn: pkgVodListData[currentIdx].isDvdYn,
                            resolCd: pkgVodListData[currentIdx].resolCd,
                            isKids: isKids,
                            prInfo: pkgVodListData[currentIdx].prInfo
                        });
                        contextMenu.open({wishFlag: false, openDeselectPopup: true});
                    } else if (focusRow == FOCUS_ROW_BUTTON) {
                        addWishList(0, pkgData.catId || pkgData.itemId, isWishPkg ? 2 : 1);
                    }
                    return true;
                default:
                    return false;
            }
        };

        function setWishListFlag() {
            var familyHomeModule = ModuleManager.getModule("module.family_home");
            if (familyHomeModule) familyHomeModule.execute({method: "setWishListFlag"});
        }

        function addWishList(itemType, catId, flag) {
            try {
                VODAmocManager.setPlayListNxt(1, itemType, catId, catId, flag, "", function(result, response) {
                    if (result) {
                        if (response.reqCode == 0) {
                            // 정상등록
                            if (flag == 1) {
                                showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다");
                                setWishListFlag();
                                _this.div.find(".contextTextArea .wishText").text("찜해제");
                                isWishPkg = true;
                            } else {
                                showToast("VOD 찜을 해제하였습니다");
                                setWishListFlag();
                                _this.div.find(".contextTextArea .wishText").text("찜하기");
                                isWishPkg = false;
                            }
                        } else if (response.reqCode == 1) {
                            addWishList(itemType, catId, flag)
                        } else if (response.reqCode == 3) throw "networkError(2)";
                    } else throw "networkError(1)";
                });
            } catch (e) {
                log.printErr(e);
            }
        }

        // 줄거리 더보기에 포커스
        function detailBtnFocus() {
            _this.div.find("#btnArea").removeClass("btn_focus");
            _this.div.find("#package_movieList_area div.movieList_item_div").removeClass("focus");
            _this.div.find("#synopsis_btn_area").removeClass("focus");
            if (focusRow == FOCUS_ROW_DETAILBTN) _this.div.find("#synopsis_btn_area").addClass("focus");
        }

        // 리스트에 포커스, 리스트 이동
        function movieListFocus() {
            _this.div.find("#synopsis_btn_area").removeClass("focus");
            _this.div.find("#btnArea").removeClass("btn_focus");
            _this.div.find("#package_movieList_area div.movieList_item_div").removeClass("focus");
            _this.div.find("#package_movieList_area div.movieList_item_div").eq(currentIdx - (Math.floor(currentIdx / 6) * 6)).addClass("focus");
            if (listCurPage != Math.floor(currentIdx / 6)) movieListPageRefresh(listCurPage = Math.floor(currentIdx / 6));
            if (focusRow == FOCUS_ROW_LIST) packageInfoRefresh();
            setContextTitle();
        }

        // 페이지에 따른 데이터 셋팅
        function movieListPageRefresh(page) {
            listCurPage = page;
            _this.div.find("#etcArea .package_listArrow_img").css("display", "none");
            if (listTotalPage > 0) {
                if (listCurPage != listTotalPage) _this.div.find("#etcArea .package_listArrow_img.right").css("display", "inherit");
                if (listCurPage > 0) _this.div.find("#etcArea .package_listArrow_img.left").css("display", "inherit");
            }
            for (var i = 0; i < 6; i++) {
                var dataIndex = listCurPage * 6 + i;
                changeVerticalPosterData(i, pkgVodListData[dataIndex]);
                dataIndex = null;
            }
        }

        // 패키지 버튼 포커스
        function buttonFocusRefresh() {
            _this.div.find("#btnArea").removeClass("btn_focus");
            if (focusRow == FOCUS_ROW_BUTTON) {
                _this.div.find("#synopsis_btn_area").removeClass("focus");
                _this.div.find("#package_movieList_area div.movieList_item_div").removeClass("focus");
                _this.div.find("#btnArea").addClass("btn_focus")
            }
            setContextTitle();
        }

        // 상단 패키지 상세정보 셋팅
        function packageInfoRefresh() {
            switch (focusRow) {
                case FOCUS_ROW_BUTTON :
                    _this.div.find("#infoArea #poster_starScore_area").css("display", "none");
                    _this.div.find("#infoArea").css("-webkit-transform", "translateY(0px)");
                    _this.div.find("#synopsis_area").removeClass("focus");
                    _this.div.find("#maintitle_area").text("");
                    // _this.div.find("#infoArea #poster_starScore_area").empty();
                    _this.div.find("#subtitle_area").text("");
                    _this.div.find("#subtitle_area_0").css("display", "none");
                    _this.div.find("#actor_area").empty();
                    _this.div.find("#actor_area").append();
                    _this.div.find("#synopsis_area").text("");
                    _this.div.find("#packageSynop_area").text(pkgData.pkgAdText);
                    _this.div.find("#packageSynop_area").css("visibility", "inherit");
                    _this.div.find("#etcArea .ollehMobile_icon_area").css("visibility", "hidden");
                    break;
                case FOCUS_ROW_DETAILBTN :
                case FOCUS_ROW_LIST :
                    _this.div.find("#infoArea #poster_starScore_area").css("display", "inherit");
                    _this.div.find("#infoArea").css("-webkit-transform", "translateY(-135px)");
                    _this.div.find("#synopsis_area").addClass("focus");
                    _this.div.find("#packageSynop_area").css("visibility", "hidden");
                    _this.div.find("#maintitle_area").html((pkgVodListData[currentIdx].contsName || pkgVodListData[currentIdx].itemName) + "<img id='package_age_icon' src='" + window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(pkgVodListData[currentIdx].prInfo) + ".png'>");
                    _this.div.find("#infoArea #poster_starScore_area .poster_starImg").removeClass("filled");
                    _this.div.find("#infoArea #poster_starScore_area .poster_starImg").removeClass("half");
                    _this.div.find("#infoArea #poster_starScore_area .poster_starImg").removeClass("empty");
                    _this.div.find("#infoArea #poster_starScore_area #score_area").empty();
                    var tmpMark = pkgVodListData[currentIdx].mark - 0;
                    setStarNumber(tmpMark);
                    stars.setRate(_this.div.find("#infoArea #poster_starScore_area .star_icon_area"), tmpMark);
                    _this.div.find("#subtitle_area").html(DetailUtil.getSubTitle(pkgVodListData[currentIdx]));
                    _this.div.find("#subtitle_area_0").css("display", "inherit");
                    _this.div.find("#subtitle_area_0 .subtitle_text").html(DetailUtil.getSubTitle_0(pkgVodListData[currentIdx]));
                    _this.div.find("#subtitle_area_0 .subtitle_icon_img").empty().append(DetailUtil.getSubTitleIconImg(pkgVodListData[currentIdx], {
                        parentDiv: _this.div,
                        type: "PACKAGE"
                    }));
                    _this.div.find("#actor_area").empty();
                    _this.div.find("#actor_area").append(DetailUtil.getActorArea(_this.div, pkgVodListData[currentIdx]));
                    //_this.div.find("#synopsis_area").html(synopsisConverter(pkgVodListData[currentIdx].synopsis));

                    _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                        data: pkgVodListData[currentIdx].synopsis,
                        type: "PACKAGE",
                        isKids: isKids,
                        callback: function (detailSync) {
                            // 더보기 버튼 활성화
                            isDetailSyn = detailSync;
                            _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                        }
                    }));
                    if (!KTW.managers.service.ProductInfoManager.isBiz()) {
                        _this.div.find("#etcArea .ollehMobile_icon_area").css("visibility", (pkgVodListData[currentIdx].contsLinkProperty && pkgVodListData[currentIdx].contsLinkProperty.indexOf("C") >= 0 ? "inherit" : "hidden"));
                    }
                    break;
            }
        }

        function setData(data, pointData) {
            // 패키지 운영 배경
            if (data.bImgUrl) _this.div.find("#background_area").append("<img id='packageBack_img' src='" + data.bImgUrl + "'>");
            // 패키지 포스터
            if (data.pImgUrl) _this.div.find("#packageInfo_area").append("<img id='packageBigPoster_img' src='" + data.pImgUrl + "'>");
            else _this.div.find("#packagePoster_Area").append("<img id='packagePoster_img' src='" + getPosterImageURL(data) + "'>");
            // 좌상단 패키지 타이틀 영역
            _this.div.find("#packageInfo_area").append(_$("<div/>", {id: "packagePoster_Area"}));
            _this.div.find("#packagePoster_Area").append(_$("<div/>", {class: "packagePoster_title"}).text(data.itemName || data.catName));
            _this.div.find("#packagePoster_Area").append(_$("<div/>", {class: "packagePoster_subTitle"}).text(data.originTitle));
            _this.div.find("#packagePoster_Area").append(_$("<div/>", {class: "packagePoster_info"}));
            // 좌하단 패키지 정보 영역
            _this.div.find("#packagePoster_Area").append(_$("<div/>", {id: "packagePoster_infoArea"}));
            _this.div.find("#packagePoster_infoArea").append(_$("<div/>", {class: "packagePoster_synopsis"}));
            _this.div.find("#packagePoster_infoArea .packagePoster_synopsis").text(data.synopsis);
            // 자물쇠 이미지
            _this.div.find("#packageInfo_area").append("<img id='customVod_img_locked'>");
            if (!isAuthorizedContent && UTIL.isLimitAge(pkgData.prInfo)) _this.div.find("#customVod_img_locked").css("display", "inherit");
            else _this.div.find("#customVod_img_locked").css("display", "none");
            // 우상단 패키지 정보 영역
            _this.div.find("#maintitle_area").text();
            _this.div.find("#subtitle_area").append();
            _this.div.find("#actor_area").append();
            _this.div.find("#packageSynop_area").text(data.pkgAdText);
            // 하단 패키지 버튼 구성
            // [WEBIIIHOME-3809] 패키지 구매여부 buyCheckYn 전달
            var btnObj = new DetailUtil.btnObject({
                title: "패키지",
                type: ButtonObject.TYPE.PACKAGE,
                lessPrice : (data.ltFlag == 2 ? data.vatLtPrice : data.vatPrice),
                buyCheckYn: buyCheckData.buyCheckYn == "Y"
            }, null, _this.div);

            btnList.push(btnObj);
            _this.div.find("#btn_item_area").append(btnList[0].getView());
            _this.div.find("#btn_txt_area").empty();
            _this.div.find("#btn_txt_area").append(DetailUtil.getPointText(pointData, false, false, false));
            _this.div.find(".vod_btn_line").css("width", _this.div.find(".vod_btn_price").width() + "px");
            buttonFocusRefresh();
            createContextMenu();
        }

        function constructDom(data) {
            _this.div.append(_$("<div>", {id: "background_area"}));
            _this.div.find("#background_area").append(_$("<div/>", {class: "bgBlurWrapper"}));
            _this.div.find(".bgBlurWrapper").append(_$("<img>", {class: "bgBlur posterImg", src: pkgData.imgUrl}));
            _this.div.find("#background_area").append(_$("<div/>", {class: "dim"}));
            _this.div.append(_$("<div>", {id: "packageInfo_area"}));
            _this.div.append(_$("<div>", {id: "infoArea"}));
            _this.div.find("#infoArea").append(_$("<div/>", {id: "maintitle_area"}));
            _this.div.find("#infoArea").append(_$("<div/>", {id: "subtitle_area"}));
            _this.div.find("#subtitle_area").append(_$("<span/>", {class: "subtitle_text"}));
            _this.div.find("#infoArea").append(_$("<div/>", {id: "subtitle_area_0"}));
            _this.div.find("#subtitle_area_0").append(_$("<span/>", {class: "subtitle_text"}));
            _this.div.find("#subtitle_area_0").append(_$("<div/>", {class: "subtitle_icon_img"}));
            // infoArea
            _this.div.find("#infoArea").append(_$("<div/>", {id: "poster_starScore_area"}));
            _this.div.find("#infoArea #poster_starScore_area").append("<div class='starStr basic_text'>olleh tv 별점</div><div class='star_icon_area'/>");
            _this.div.find("#infoArea #poster_starScore_area").append(_$("<div/>", {id: "score_area"}));
            var tmpMark = pkgVodListData[0].mark;
            setStarNumber(tmpMark);
            _this.div.find("#infoArea #poster_starScore_area .star_icon_area").append(stars.getView(stars.TYPE.RED_26));
            stars.setRate(_this.div.find("#infoArea #poster_starScore_area .star_icon_area"), tmpMark);
            _this.div.find("#infoArea").append(_$("<div/>", {id: "actor_area"}));
            _this.div.append(_$("<div/>", {id: "packageSynop_area"}));
            _this.div.find("#infoArea").append(_$("<div/>", {id: "synopsis_area"}));
            _this.div.find("#infoArea").append(_$("<div/>", {id: "synopsis_btn_area"}));
            _this.div.find("#infoArea #synopsis_btn_area").append(_$("<img/>", {
                class: "synopsis_btn_img",
                src: window.modulePath + "resource/btn_detail_vod.png"
            }));
            _this.div.find("#infoArea #synopsis_btn_area").append(_$("<span/>", {class: "synopsis_btn_text"}).text("+ 더보기"));
            _this.div.find("#infoArea #poster_starScore_area").css("display", "none");
            // package_movieList_area
            _this.div.append(_$("<div>", {id: "package_movieList_area"}));
            for (var idx = 0; idx < Math.min(data.length, 6); idx++) _this.div.find("#package_movieList_area").append(getVerticalPoster(data[idx]));
            // etcArea
            _this.div.append(_$("<div>", {id: "etcArea"}));
            _this.div.find("#etcArea").append(_$("<div/>", {id: "movieList_line"}));
            _this.div.find("#etcArea").append(_$("<img/>", {class: "package_listArrow_img right"}));
            _this.div.find("#etcArea").append(_$("<img/>", {class: "package_listArrow_img left"}));
            _this.div.find("#etcArea .package_listArrow_img").css("display", "none");
            if (data.length > 6) _this.div.find("#etcArea .package_listArrow_img.right").css("display", "inherit");
            _this.div.find("#etcArea").append(_$("<div/>", {class: "ollehMobile_icon_area"}));
            _this.div.find("#etcArea .ollehMobile_icon_area").append(_$("<img/>", {
                class: "mobile_icon_img",
                src: window.modulePath + "resource/icon_detail_mobile.png"
            }));
            _this.div.find("#etcArea .ollehMobile_icon_area").append(_$("<span/>", {class: "mobile_icon_text"}).text("올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다"));
            _this.div.find("#etcArea .ollehMobile_icon_area").css("visibility", "hidden");
            // btnArea
            _this.div.append(_$("<div>", {id: "btnArea"}));
            _this.div.find("#btnArea").append(_$("<div/>", {id: "btn_item_area"}));
            _this.div.find("#btnArea").append(_$("<div/>", {id: "btn_txt_area"}));

            _this.div.append(_$("<div>", {class: "contextTextArea btn_focus"}));
            _this.div.find(".contextTextArea").append(_$("<div/>", {class: "ctxIcon"}));
            _this.div.find(".contextTextArea").append(_$("<div/>", {class: "wishText"}));
            _this.div.find(".contextTextArea").append(_$("<div/>", {class: "verticalLine"}));
            _this.div.find(".contextTextArea").append(_$("<div/>", {class: "playListText"}).text("플레이리스트 추가"));

            listTotalPage = Math.floor(data.length / 6);
            listCurPage = 0;
        }

        function getPosterImageURL(data) {
            switch (data.posterUseType) {
                case 1:
                    return data.wideImgUrl;
                case 2:
                    return data.squareImgUrl;
                default:
                    return data.imgUrl;
            }
        }

        function setStarNumber(starData) {
            var tmpStarNum = starData;
            if (tmpStarNum - 0 > 0) {
                _this.div.find("#score_area").append(_$("<img>", {
                    class: "poster_scoreImg",
                    src: ROOT_URL + "resource/rating_vod_number_" + Math.floor(tmpStarNum) + ".png"
                }));

                var chgNum_0 = tmpStarNum * 10;
                var chgNum_1 = Math.floor(tmpStarNum) * 10;
                if (chgNum_0 - chgNum_1 > 0) {
                    var tmpStarNum2 = chgNum_0 - chgNum_1;
                    _this.div.find("#score_area").append(_$("<img>", {
                        class: "poster_scoreImg",
                        src: ROOT_URL + "resource/rating_vod_dot.png"
                    }));
                    _this.div.find("#score_area").append(_$("<img>", {
                        class: "poster_scoreImg",
                        src: ROOT_URL + "resource/rating_vod_number_" + tmpStarNum2 + ".png"
                    }));
                } else {
                    _this.div.find("#score_area").append(_$("<img>", {
                        class: "poster_scoreImg",
                        src: ROOT_URL + "resource/rating_vod_dot.png"
                    }));
                    _this.div.find("#score_area").append(_$("<img>", {
                        class: "poster_scoreImg",
                        src: ROOT_URL + "resource/rating_vod_number_0.png"
                    }));
                }
            } else {
                _this.div.find("#score_area").empty();
            }
        }

        function getVerticalPoster(data) {
            var element = _$("<div class='movieList_item_div'>" +
                "<div class='poster_area'>" +
                "<img src='" + ROOT_URL + "resource/poster_sdw_w154.png' class='moviePosterSdw'>" +
                "<img src='" + data.imgUrl + "' class='moviePosterImg' onerror='this.src=\"" + modulePath + "resource/defaultPoster/default_poster.png\"'>" +
                "<div class='icon_area'>" +
                "<div class='right_icon_area'/>" +
                "<div class='bottom_tag_area'/>" +
                "<div class='lock_icon'/>" +
                "</div>" +
                "</div>" +
                "</div>");

            if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
                element.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/tag_poster_hdr.png"}));
            }

            if (data.resolCd && data.resolCd.indexOf("UHD") >= 0) {
                element.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/tag_poster_uhd.png"}));
            }

            if (data.isDvdYn == "Y") {
                element.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/tag_poster_mine.png"}));
            }

            if (data.wEvtImageUrlW3) {
                element.find(".bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
            }

            if (!isAuthorizedContent && UTIL.isLimitAge(data.prInfo)) {
                element.find(".lock_icon").append(_$("<img>", {src: modulePath + "resource/img_vod_locked.png"}));
            }
            return element;
        }

        function changeVerticalPosterData(index, data) {
            var targetDiv = _this.div.find("#package_movieList_area .movieList_item_div").eq(index);
            if (data) {
                targetDiv.find(".moviePosterImg").attr({
                    src: data.imgUrl + "?w=259&h=370&quality=90",
                    onerror: "this.src='" + modulePath + "resource/image/default_poster.png'"
                });
                targetDiv.find("right_icon_area").empty();
                targetDiv.find("bottom_tag_area").empty();
                targetDiv.find("lock_icon").empty();

                if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
                    targetDiv.find(".right_icon_area").append(_$("<img>", {src: modulePath + "resource/tag_poster_hdr.png"}));
                }

                if (data.resolCd && data.resolCd.indexOf("UHD") >= 0) {
                    targetDiv.find("right_icon_area").append(_$("<img>", {src: modulePath + "resource/tag_poster_uhd.png"}));
                }

                if (data.isDvdYn == "Y") {
                    targetDiv.find("right_icon_area").append(_$("<img>", {src: modulePath + "resource/tag_poster_mine.png"}));
                }

                if (data.wEvtImageUrlW3) {
                    targetDiv.find("bottom_tag_area").append(_$("<img>", {src: data.wEvtImageUrlW3}));
                }

                if (!isAuthorizedContent && UTIL.isLimitAge(data.prInfo)) {
                    targetDiv.find("lock_icon").append(_$("<img>", {src: modulePath + "resource/img_vod_locked.png"}));
                }

                targetDiv.css("opacity", "0.99");
            } else {
                targetDiv.css("opacity", "0");
            }
        }

        /** 로그 관련 */
        function sendVODDetail(key_code, btnCode) {
            try {
                NavLogMgr.collectVODDetail(key_code, pkgData.catId || pkgData.itemId,
                    pkgVodListData[currentIdx].contsId || pkgVodListData[currentIdx].itemId,
                    pkgVodListData[currentIdx].contsName || pkgVodListData[currentIdx].itemName, btnCode,
                    pkgVodListData[currentIdx].contsId || pkgVodListData[currentIdx].itemId
                );
            } catch (e) {
            }
        }

        /** 로그 관련 */
        function sendVODDetailPurchase(key_code) {
            try {
                NavLogMgr.collectVODDetailPerchase(key_code, pkgData.catId || pkgData.itemId,
                    pkgVodListData[currentIdx].contsId || pkgVodListData[currentIdx].itemId,
                    pkgVodListData[currentIdx].contsName || pkgVodListData[currentIdx].itemName,
                    btnList[0]);
            } catch (e) {
            }
        }

        /** 로그 관련 */
        function sendJumpVOD(actionCode) {
            try {
                NavLogMgr.collectJumpVOD(actionCode,
                    pkgData.catId || pkgData.itemId,
                    pkgVodListData[currentIdx].contsId || pkgVodListData[currentIdx].itemId
                );
            } catch (e) {
            }
        }
    };

    vod_detail_package_layer.prototype = new Layer();
    vod_detail_package_layer.prototype.constructor = vod_detail_package_layer;

    //Override create function
    vod_detail_package_layer.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_detail_package_layer.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodDetailPackageLayer"] = vod_detail_package_layer;
})();
