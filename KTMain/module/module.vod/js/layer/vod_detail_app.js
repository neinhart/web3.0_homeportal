/**
 * Created by ksk91_000 on 2016-09-06.
 */
(function () {
    var vod_detail_app_layer = function vodDetailAppLayer(options) {
        Layer.call(this, options);

        var _this = this;

        var catId; // 카테고리 ID
        var appData; // 앱게임 정보
        var btnList = []; // 버튼 리스트
        var btnFocusIdx = 0; // 버튼 포커스
        var rowIdx = 2;
        var bottomMenu = null; // 하단 추천 메뉴

        var reqPathCd; // 진입코드
        var connerYn; // 월정액 유무
        var isDetailSyn = false; // 더보기 버튼 유무
        var isNotice = false; // 공지 유무
        var isNoticeUnFocus = false; // 공지 버튼 포커스 유무
        var isKidsCate = false; // 키즈카테고리 진입 유무

        this.init = function (cbCreate) {
            this.div.attr({
                class: "arrange_frame vod_detail_single_layer vod app",
                style: "background:black"
            });

            // App 전용
            appData = this.getParams().appData;
            catId = this.getParams().cat_id;
            reqPathCd = this.getParams().req_cd;

            btnFocusIdx = 0;
            rowIdx = 2;
            btnList = [];

            isKidsCate = this.getParams().isKids;

            if (isKidsCate) {
                this.div.addClass("kids");
            }

            bottomMenu = new DetailBottom();

            constructDom();
            setContentsData(cbCreate);
        };

        /** 데이터 주입 */
        function setContentsData(cbCreate) {
            btnList = [];
            btnList.push(new btnObject(appData));

            setButton();
            setData(appData);

            if (cbCreate) {
                cbCreate(true);
            }
        }

        this.show = function () {
            Layer.prototype.show.call(this);
            rowIdx = 2;

            if (options && options.resume) {
                setBtnFocus(btnFocusIdx);
            }
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.remove = function () {
            Layer.prototype.remove.call(this);
        };

        /** 키처리 */
        this.controlKey = function (key_code) {
            if (bottomMenu.controlKey(key_code)) return true;

            switch (key_code) {
                case KEY_CODE.UP:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, catId, appData.contsId || appData.itemId,
                                appData.contsName || appData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                    } else {
                        try {
                            NavLogMgr.collectVODDetailPerchase(key_code, catId, appData.contsId || appData.itemId,
                                appData.contsName || appData.itemName,
                                btnList[btnFocusIdx]);
                        } catch (e) {
                        }
                    }

                    if (rowIdx > 0) {
                        rowIdx--;
                    }
                    if (!isNotice && isDetailSyn) {
                        rowIdx = 0;
                    } else if (isNotice && !isDetailSyn) {
                        rowIdx = isNoticeUnFocus ? 2 : 1;
                    } else if (!isNotice && !isDetailSyn) {
                        rowIdx = 2;
                    } else if (isNoticeUnFocus) {
                        rowIdx = 0;
                    }
                    setBtnFocus(btnFocusIdx);
                    return true;
                case KEY_CODE.DOWN:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, catId, appData.contsId || appData.itemId,
                                appData.contsName || appData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                        rowIdx++;
                    } else {
                        try {
                            NavLogMgr.collectVODDetail(key_code, catId, appData.contsId || appData.itemId,
                                appData.contsName || appData.itemName,
                                NLC.DETAIL_INTERACTIVE, appData.locator);
                        } catch (e) {
                        }
                        bottomMenu.show();
                    }

                    if (!isNotice || isNoticeUnFocus) {
                        rowIdx = 2;
                    }
                    setBtnFocus(0);
                    return true;
                case KEY_CODE.LEFT:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, catId, appData.contsId || appData.itemId,
                                appData.contsName || appData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                        LayerManager.historyBack();
                    }
                    else {
                        var btnIdx = getBtnIndex(-1);
                        if (btnIdx < 0) {
                            LayerManager.historyBack();
                        } else {
                            setBtnFocus(btnIdx);
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    setBtnFocus(getBtnIndex(1));
                    return true;
                case KEY_CODE.ENTER:
                    if (rowIdx < 2) {
                        var tmpData = {
                            title: appData.itemName,
                            notice: appData.notice || "",
                            synopsis: appData.synopsis || ""
                        };

                        LayerManager.activateLayer({
                            obj: {
                                id: rowIdx == 0 ? "VodSynopsisDetailPopup" : "VodNoticePopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    data: tmpData,
                                    isKids: isKidsCate
                                }
                            },
                            moduleId: "module.vod",
                            new: true,
                            visible: true
                        });
                        try {
                            NavLogMgr.collectVODDetail(key_code, catId, appData.contsId || appData.itemId,
                                appData.contsName || appData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                    } else {
                        btnList[btnFocusIdx].enterKeyAction();
                    }
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    return true;
                default:
                    return false;
            }
        };

        /** 버튼 Index 정의 */
        function getBtnIndex(amount) {
            var newIdx = btnFocusIdx + amount;

            if (newIdx < 0) {
                return -1;
            }

            if (!btnList[newIdx]) {
                return btnFocusIdx;
            }

            if (connerYn && btnList[newIdx].type == 3) {
                return getBtnIndex(amount + (amount > 0 ? 1 : -1));
            } else {
                return newIdx;
            }
        }

        /** 포커스 이동 */
        function setBtnFocus(btnIndex) {
            btnFocusIdx = btnIndex;
            _this.div.find("#btnArea #btn_item_area div.vod_btn_area.btn_focus").removeClass("btn_focus");
            _this.div.find("#notice_infoArea").removeClass("focus");
            _this.div.find("#synopsis_btn_area").removeClass("focus");

            switch (rowIdx) {
                case 0:
                    _this.div.find("#synopsis_btn_area").addClass("focus");
                    break;
                case 1:
                    _this.div.find("#notice_infoArea").addClass("focus");
                    break;
                case 2:
                    _this.div.find("#btnArea #btn_item_area div.vod_btn_area").eq(btnIndex).addClass("btn_focus");
                    break;
            }
        }

        /** 최초 돔tree 생성 */
        function constructDom() {
            //Background
            var backgroundArea = _$("<div/>", {id: "background_area"});
            backgroundArea.append(
                _$("<div/>", {class: "bgBlurWrapper"}).append(_$("<img>", {class: "bgBlur posterImg_main"})),
                _$("<div/>", {class: "dim"})
            );

            //포스터
            var posterArea = _$("<div/>", {id: "posterArea"});
            posterArea.append(
                _$("<img>", {id: "main_poster", class: "posterImg_main"}),
                _$("<img/>", {id: "poster_vodLock_img"})
            );


            // Info
            var infoArea = _$("<div/>", {id: "infoArea"});
            infoArea.append(
                _$("<div/>", {id: "maintitle_area"}),
                _$("<div/>", {id: "synopsis_area"}),
                _$("<div/>", {id: "synopsis_btn_area"}).append(
                    _$("<img/>", {
                        class: "synopsis_btn_img",
                        src: window.modulePath + "resource/btn_detail_vod.png"
                    }),
                    _$("<span/>", {class: "synopsis_btn_text"}).text("+ 더보기")
                )
            );

            var btnArea = _$("<div/>", {id: "btnArea"});
            btnArea.append(
                _$("<div/>", {id: "btn_item_area"}),
                _$("<div/>", {id: "btn_txt_area"})
            );

            // Tag를 미리 생성후 마지막 한번에 Append하여 속도 개선한다.
            // Append도 매번 호출이 아닌 Argument형태로 전달하면 그 개수에 맞게 Append를 수행한다
            _this.div.append(
                backgroundArea,
                posterArea,
                infoArea,
                _$("<div/>", {id: "notice_infoArea"}),
                btnArea
            );
        }

        /** 최초진입 시 View에 데이터 주입*/
        function setData(data) {
            console.debug(JSON.stringify(data));
            // 성인컨텐츠 Lock 이미지 표시 여부
            if (!AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(data.prInfo)) {
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "inherit");
            }
            else _this.div.find("#posterArea #poster_vodLock_img").css("display", "none");


            //Bottom 메뉴 구성
            _this.div.append(bottomMenu.createView("APPGAME", false, reqPathCd, isKidsCate));

            // 공지사항 여부?
            isNotice = data.notice.length > 0;

            var mainTitle = (data.contsName || data.itemName);
            if (window.UTIL.getTextLength(mainTitle, "RixHead B", 60, -0.5) >= 1067) {
                _this.div.find("#maintitle_area").css("width", "1140px");
                _this.div.find("#maintitle_area span").css("width", "1140px");
                mainTitle = HTool.ellipsis(mainTitle, "RixHead B", 60, 1067, "...", -0.5);
            }
            //연령표시 (2017.06.04 LeeBaeng)
            _this.div.find("#maintitle_area").html("<span>" + mainTitle + "<img id='detailTitle_age_icon_single' src='" + window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(data.prInfo) + ".png'></span>");

            _this.div.find("img.posterImg").attr("src", data.imgUrl);

            // 시놉시스
            _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                data: data.synopsis,
                type: "APP",
                isNotice : isNotice,
                isKids: isKidsCate,
                callback: function (detailSync) {
                    // 더보기 버튼 활성화
                    isDetailSyn = detailSync;
                    _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                }
            }));

            // 공지사항
            _this.div.find("#notice_infoArea").append(_$("<div/>", {class: "notice_icon"}));
            _this.div.find("#notice_infoArea").append("<div id='notice_splitLine'></div>");
            _this.div.find("#notice_infoArea").append("<div id='notice_contents'>" + data.notice + "</div>");
            if (UTIL.splitText(data.notice, "RixHead L", 25, 960, -0.7).length == 1) {
                isNoticeUnFocus = true;
                _this.div.find("#notice_infoArea").addClass("noMore");
            } else {
                isNoticeUnFocus = false;
                _this.div.find("#notice_infoArea").removeClass("noMore");
            }
            _this.div.find("#notice_infoArea").css("visibility", (data.notice ? "inherit" : "hidden"));

            data.catId = catId;
            bottomMenu.setData(data);
            setBtnFocus(btnFocusIdx);
        }

        /** 버튼 갱신 */
        function setButton() {
            _this.div.find("#btn_item_area").empty();

            for (var i = 0; i < btnList.length; i++) {
                _this.div.find("#btn_item_area").append(btnList[i].getView());
            }
        }

        /** 버튼 객체 생성 */
        var btnObject = function (data) {
            this.buyData = data;

            var div = _$("<div/>", {class: "vod_btn_area added"});
            div.append(
                _$("<div/>", {class: 'vod_btn_tmpBackground'}),
                "<div class='vod_btn_title'>바로가기</div>"
            );

            this.getView = function () {
                return div;
            };
            this.enterKeyAction = function () {
                enterKeyActionImpl(data);
            };

            function enterKeyActionImpl(data) {
                try {
                    NavLogMgr.collectJump(NLC.DETAIL_INTERACTIVE, NLC.JUMP_DEST_INTERACTIVE,
                        catId, appData.itemId, "", appData.locator);
                } catch (e) {
                }

                switch (data.itemType - 0) {
                    //멀티캐스트 양방향 서비스
                    case 3:
                        var nextState;

                        if (data.locator == CONSTANT.APP_ID.MASHUP) {
                            nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                        } else {
                            nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
                        }

                        AppServiceManager.changeService({
                            nextServiceState: nextState,
                            obj: {
                                type: CONSTANT.APP_TYPE.MULTICAST,
                                param: data.locator,
                                ex_param: data.parameter
                            },
                            visible: true
                        });
                        break;
                    //유니캐스트 양방향 서비스
                    case 7:
                        var nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;

                        AppServiceManager.changeService({
                            nextServiceState: nextState,
                            obj: {
                                type: CONSTANT.APP_TYPE.UNICAST,
                                param: data.itemId
                            },
                            visible: true
                        });
                        break;
                    //웹뷰
                    case 8:
                        AppServiceManager.startChildApp(data.locator);
                        break;
                }
            }

        };
    };

    vod_detail_app_layer.prototype = new Layer();
    vod_detail_app_layer.prototype.constructor = vod_detail_app_layer;

    //Override create function
    vod_detail_app_layer.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_detail_app_layer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodDetailAppLayer"] = vod_detail_app_layer;
})();
