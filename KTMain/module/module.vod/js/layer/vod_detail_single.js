/**
 * Created by ksk91_000 on 2016-09-06.
 */
(function () {
    var vod_detail_single_layer = function vodDetailSingleLayer(options) {
        Layer.call(this, options);

        var _this = this;
        var sortGb; // MCID는 이어보기 경우가 발생하지않는다

        var cateId; // 카테고리 ID
        var cateData; // 카테고리 정보
        var btnList = [];  // 버튼 리스트
        var wishObject = {};
        var btnFocusIdx = 0; // 버튼 포커스
        var rowIdx = 2;
        var contextMenu = null; // 연관 메뉴
        var bottomMenu = null; // 하단 추천 메뉴
        var reqPathCd; // 진입코드

        var isDetailSyn = false; // 더보기 버튼 유무
        var isNotice = false; // 공지 유무
        var isNoticeUnFocus = false; // 공지 버튼 포커스 유무
        var isConnerChk = false; // 월정액 체크 여부
        var isConnerBuyChk = false; // 월정액 구매 여부
        var isVodPptBuyChk = false;
        var isAdultCate = false; // 성인카테고리
        var isAuthorizedContent = false; // 성인 인증된 컨텐츠인지 확인
        var isAuthComplete = false;  // 인증 완료 여부
        var isSingle = false; // 버튼타입 단편
        var couponCount = 0;
        var isKidsCate = false; // 키즈카테고리 진입 유무
        var isSubtitle = false; // 자막 유무
        var isShortLong = false; // 단편장기 아이콘
        var savedSeamlessCode = "1"; // 심리스 코드 저장
        var optionData;
        var pointData; // 포인트 정보
        var previewData;

        var vodData;
        var playListData;
        var contentsId;
        var isHideContextMenu = false;
        var isShopDetail = false;
        var originParams = null;
        var newParams = null;

        this.originParams = function (params) {
            if (params) {
                originParams = params;
            }
            return originParams;
        };

        this.reInit = function (cbCreate) {
            if (!!newParams) {
                _this.setParams(newParams);

                if (this.getParams().changeContentListener) {
                    this.getParams().changeContentListener(this.getParams().const_id);
                }

                this.div.empty();
                this.init(cbCreate);
                newParams = 0;
            } else if (cbCreate) {
                cbCreate();
            }
        };

        this.init = function (cbCreate) {
            this.div.addClass("arrange_frame vod_detail_single_layer vod");
            this.div.css("background", "black");

            this.originParams(this.getParams());

            cateId = this.getParams().cat_id;
            contentsId = this.getParams().const_id;
            reqPathCd = this.getParams().req_cd;

            cateData = _$.extend({}, this.getParams().cateInfo);
            isAdultCate = this.getParams().isAdultCate;
            isAuthorizedContent = isAdultCate || this.getParams().isAuthorizedContent;
            isKidsCate = this.getParams().isKids || KidsModeManager.isKidsMode();
            isShortLong = false;
            isDetailSyn = false;

            couponCount = 0;
            btnFocusIdx = 0;
            rowIdx = 2;
            btnList = [];

            wishObject = {
                contsId: contentsId
            };

            var tmpFlag = cateData.ltFlag.split("|");
            for (var idx = 0; idx < tmpFlag.length; idx++) {
                if (tmpFlag[idx] == 2 || tmpFlag[idx] == 1) {
                    isShortLong = true;
                }
            }

            playListData = this.getParams().playListData;
            sortGb = this.getParams().sortGb;

            if (isKidsCate) {
                this.div.addClass("kids");
            }

            bottomMenu = new DetailBottom();
            constructDom();

            var isOpenAdultAuthPopup = this.getParams().openAdultAuthPopup;

            if (this.getParams().contsInfo) {
                var contsInfo = this.getParams().contsInfo;

                if (isOpenAdultAuthPopup && contsInfo.adultOnlyYn === "Y") {
                    openAdultAuthPopup(function (res) {
                        if (res) {
                            setDataProcess(contsInfo);
                        } else if (cbCreate) {
                            cbCreate(false);
                        }
                    }, true);
                } else {
                    setDataProcess(contsInfo);
                }
            } else {
                // 컨텐츠 상세정보 호출
                VODAmocManager.getContentW3(function (result, data) {
                    if (!result) {
                        // 호출 에러
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({
                            message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                            reboot: true
                        });
                        cbCreate(false);
                    } else if (data.contsId) {
                        // 컨텐츠 정보 존재
                        if (isOpenAdultAuthPopup && data.adultOnlyYn === "Y") {
                            openAdultAuthPopup(function (res) {
                                if (res) {
                                    setDataProcess(data);
                                } else if (cbCreate) {
                                    cbCreate(false);
                                }
                            }, true);
                        } else {
                            setDataProcess(data);
                        }
                    } else {
                        // 컨텐츠 정보 없음
                        HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                        cbCreate(false);
                    }
                }, cateId, contentsId, DEF.SAID);
            }

            function setDataProcess(data) {
                // 상세화면 데이터 주입
                setContentsData(data, function (result) {
                    if (result) {
                        setButton();
                        createContextMenu();

                        if (vodData.adultOnlyYn != "Y") {
                            addRecentlyVod(cateId, contentsId, vodData.imgUrl, vodData.prInfo, isKidsCate);
                        }

                        DetailUtil.updateTvPoint(function (_pointData) {
                            setData(vodData, _pointData);

                            if (cbCreate) {
                                cbCreate(result);
                            }
                        });

                    } else if (cbCreate) {
                        cbCreate(result);
                    }
                });
            }

        };

        /** 데이터 주입 */
        function setContentsData(data, callback) {
            // getContentW3 -> 상세정보 주입
            vodData = data;

            if (DetailUtil.isCaption(data.capUrl)) {
                isSubtitle = true;
            } else {
                isSubtitle = false;
            }

            // 05 == 쇼핑일때 별점 표기 숨김
            if (vodData && vodData.dispContsClass && vodData.dispContsClass == "05") {
                isShopDetail = true;
                _this.div.find("#posterArea #poster_starScore_area").css("display", "none");
                _this.div.find("#posterArea .basic_text").css("display", "none");
                _this.div.find("#subtitle_area").css("display", "none");
            }
            // 2017.08.21 Yun 메인타이틀이 한줄로만 표시되도록 수정되면서 키즈 상세에 따른 버튼 높이 결정만 하도록 수정
            if (isKidsCate) {
                _this.div.find(".ollehMobile_icon_area").css("top", "408px");
                _this.div.find("#btnArea").css("top", "460px");
            } else {
                _this.div.find(".ollehMobile_icon_area").css("top", "517px");
                _this.div.find("#btnArea").css("top", "569px");
            }

            OptionListFactory.getOptionList(vodData, cateData, reqPathCd, function (res, optionList) {
                if (res) {
                    if (optionList.length > 0) {
                        DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), optionList[0]);
                    }
                    else {
                        DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), null);
                    }

                    optionData = optionList;
                    makeButtonList();
                    setButton();

                    /** 미리보기, 하이라이트보기 체크 */
                    previewData = null;
                    DetailUtil.checkPreview(optionData, function (result) {
                        previewData = result;
                    });
                }

                if (callback) {
                    callback(res);
                }
            }, wishObject, optionData);
        }

        /** 버튼 생성 */
        function makeButtonList() {
            btnList = [];
            isConnerChk = false;
            isConnerBuyChk = false;
            isVodPptBuyChk = false;
            isSingle = false;

            for (var i = 0; i < optionData.length; i++) {
                if (optionData[i].connerYn || optionData[i].pptBuyYn) {
                    isConnerChk = true;
                }

                if (optionData[i].type == ButtonObject.TYPE.CORNER) {
                    isConnerBuyChk = true;
                }

                if (optionData[i].type == ButtonObject.TYPE.VODPPT) {
                    isVodPptBuyChk = true;
                }

                if (optionData[i].type == ButtonObject.TYPE.NORMAL) {
                    isSingle = true;
                }

                DetailUtil.btnObject.prototype.enterKeyAction = btnKeyAction;
                var btnObj = new DetailUtil.btnObject(optionData[i], null, _this.div, i);
                btnList.push(btnObj);
            }

            if (vodData.btnName && vodData.btnName.length > 0) {
                var nameList = vodData.btnName.split("|");
                var typeList = vodData.btnType.split("|");
                var trgtList = vodData.btnTrgt.split("|");

                for (var i = 0; i < nameList.length; i++) {
                    btnList.push(new DetailUtil.addedBtnObject(nameList[i], typeList[i], trgtList[i], optionData.length + i));
                }
            }
        }

        /** 연관메뉴 생성 */
        function createContextMenu() {
            /**
             * itemType = 0 : Package
             * itemType = 1 && seriesType = 02 : MCID
             * itemType = 1 && else : Series
             * itemType = 2 : Single
             * itemType = 3 , 7 , 8 : App
             * */

            contextMenu = new VODDetailContext();
            contextMenu.setVodInfo({
                itemName: vodData.contsName || vodData.itemName,
                imgUrl: vodData.imgUrl,
                catId: cateId,
                contsId: vodData.contsId || vodData.itemId,
                itemType: 2,
                cmbYn: vodData.cmbYn,
                isDvdYn: vodData.isDvdYn,
                resolCd: vodData.resolCd,
                assetId: vodData.assetId,
                isKids: isKidsCate,
                prInfo: vodData.prInfo,
                callBackFunc: function (data) {
                    wishObject.playCheckFlag = data.wishFlag;
                }
            });

            var option = optionData[0];

            // TaeSang. 2018.02.21
            // 미리보기 버튼 체크 로직 추가
            var previewBtnList = [];
            var _previewBtnList = [];
            for (var i in optionData) {
                if (!(optionData[i].buyYn || optionData[i].connerYn || optionData[i].pptBuyYn || optionData[i].lessPrice == 0)) {
                    for (var j in optionData[i].optionList) {
                        if (optionData[i].optionList[j].previewYn === "Y") {
                            if (!_previewBtnList[optionData[i].title]) _previewBtnList[optionData[i].title] = {
                                index: i,
                                previewStartTime : optionData[i].optionList[j].previewStartTime,
                                previewRunTime : optionData[i].optionList[j].previewRunTime
                            };
                        }
                    }
                }
            }

            // TaeSang. 2018.02.21
            // 연관메뉴에 미리보기 버튼 추가
            var isFirstForPreviewBtn = true;
            for (var i in _previewBtnList) {
                if (isFirstForPreviewBtn) {
                    contextMenu.addSeparateLine();
                    var previewTime = (parseInt(Number(_previewBtnList[i].previewRunTime) / 60) || "N") + "분 ";
                    var previewText = previewTime + (_previewBtnList[i].previewStartTime == "00:00:00" ? "미리보기" : "하이라이트 보기");
                    contextMenu.addTitle(previewText, "highLightTitle");
                    isFirstForPreviewBtn = false;
                }

                contextMenu.addButton(i);
                previewBtnList.push(_previewBtnList[i]);
                // 미리보기 버튼 개수 2개로 제한
                if (previewBtnList.length >= 2) break;
            }

            if (((option.buyYn && !option.connerYn && !optionData.pptBuyYn) || option.lessPrice == 0) && !option.vodPlus) {
                var btnObj = {};

                for (var j = 0; j < option.optionList.length; j++) {
                    if (option.optionList[j].vatPrice > 0 && !option.optionList[j].buyYn) {
                        btnObj.title = option.title;
                    }
                }

                if (btnObj.title) {
                    contextMenu.addSeparateLine();
                    contextMenu.addTitle("다른 화질 구매");

                    if (btnObj.isSale) {
                        contextMenu.addButton(btnObj.title + " " + btnObj.saleRate + "%", "sale");
                    } else {
                        contextMenu.addButton(btnObj.title);
                    }
                }
            }

            contextMenu.setEventListener(function (index) {
                if (previewBtnList.length > 0 && index < previewBtnList.length) {
                    // 미리보기 or 하이라이트 보기 프로세스 시작
                    // console.debug(optionData[previewBtnList[index].index]);
                    if (!isAuthorizedContent && !isAuthComplete && UTIL.isLimitAge(optionData[previewBtnList[index].index].prInfo)) {
                        openAdultAuthPopup(function (res) {
                            if (res) prePlayProcessForPreview(cateId, optionData[previewBtnList[index].index].optionList, optionData[previewBtnList[index].index]);
                        }, true);
                    } else prePlayProcessForPreview(cateId, optionData[previewBtnList[index].index].optionList, optionData[previewBtnList[index].index]);
                }
                else
                    openPaymentPopup(optionData[index - previewBtnList.length], cateId, reqPathCd, function (res, data) {
                        if (res) {
                            VODManager.requestPlay(data, reqPathCd);
                        }
                    }, false, buttonUpdateCallback)
            });

            // 연관메뉴에서 포커스가 바뀔 때마다 미리보기의 시간 정보를 갱신함
            contextMenu.onFocusChange(function (index, callback) {
                if (previewBtnList.length > 0 && index < previewBtnList.length) {
                    var previewTime = ((parseInt(previewBtnList[index].previewRunTime) / 60) || "N") + "분 ";
                    var previewText = previewTime + (previewBtnList[index].previewStartTime == "00:00:00" ? "미리보기" : "하이라이트 보기");
                    callback(previewText);
                }
            });

            _this.div.append(contextMenu.getView());
        }

        function hideContextMenu(_isOtherApp) {
            if (_isOtherApp) {
                log.printDbg("OtherApp Btn Focus !!");
                isHideContextMenu = true;
                _this.div.find(".detail_bottom .bottom_relation_area").css("opacity", "0");
            } else {
                isHideContextMenu = false;
                _this.div.find(".detail_bottom .bottom_relation_area").css("opacity", ".99");
            }
        }

        this.show = function (options) {
            Layer.prototype.show.call(this);
            rowIdx = 2;
            if (options && options.resume && newParams !== 0) {
                if (bottomMenu && bottomMenu.isShow) {
                    bottomMenu.resumeTextAnimation();
                }
                setContentsData(vodData, function () {
                    setBtnFocus(getBtnIndex(0));
                    refreshBtnBuyInfo();

                    if (contextMenu) {
                        contextMenu.getView().remove();
                        createContextMenu();
                    }
                    // TV포인트 최신화
                    // 2017.08.16 Lazuli
                    // VOD 플러스 문구 업데이트 때문에 setContentsData 로직이 끝난 후에 실행되어야 함
                    DetailUtil.updateTvPoint(function (_pointData) {
                        _this.div.find("#btn_txt_area").empty();
                        _this.div.find("#btn_txt_area").append(DetailUtil.getPointText(
                            _pointData,
                            btnList[btnFocusIdx].getOptionList().vodPlus,
                            btnList[btnFocusIdx].getOptionList().pptBuyYn,
                            isConnerChk));
                    });
                    if (previewData != null && previewData.isPreview) showToast(previewData.previewText, "ICON");
                });
            } else {
                refreshBtnBuyInfo();
                // 2017.8.22 원제목이 길이 확인(길경우 한줄로 표시하는 기능)
                DetailUtil.originSubTitleLengthCheck(_this.div, "SINGLE");
                if (previewData != null && previewData.isPreview) showToast(previewData.previewText, "ICON");
                if (newParams === 0) {
                    newParams = null;
                }
            }

            KTW.managers.service.VoiceableManager.addNodeMatchListener(onNodeMatchListener);
        };

        function onNodeMatchListener (element) {
            log.printDbg("onNodeMatchListener");

            var layer = LayerManager.getLastLayerInStack();
            if (layer.id !== _this.id) {
                return;
            }

            var el = _$(element);

            if (el.attr("id") === "synopsis_btn_area") {
                rowIdx = 0;

                setBtnFocus(btnFocusIdx, true);

                setTimeout(function () {
                    _this.controlKey(KEY_CODE.OK);
                }, 0);
            }
            else if (el.hasClass("vod_btn_area")) {
                rowIdx = 2;

                var index = Number(el.attr("index"));
                btnFocusIdx = index;

                setBtnFocus(btnFocusIdx, true);

                setTimeout(function () {
                    _this.controlKey(KEY_CODE.OK);
                }, 0);
            }
        }

        /** 버튼정보 갱신 */
        function refreshBtnBuyInfo() {
            for (var i = 0; i < btnList.length; i++) {
                _this.div.find("#btn_item_area .vod_btn_area:eq(" + i + ") .vod_btn_line").css("width", _this.div.find("#btn_item_area .vod_btn_area:eq(" + i + ") .vod_btn_price").width() + "px");
            }
        }

        this.hide = function () {
            Layer.prototype.hide.call(this);
            contextMenu.close();

            KTW.managers.service.VoiceableManager.removeNodeMatchListener(onNodeMatchListener);
        };

        this.remove = function () {
            Layer.prototype.remove.call(this);
            VODManager.removeIndexListener();
        };

        this.controlKey = function (key_code) {
            if (contextMenu.isOpen()) {
                return contextMenu.onKeyAction(key_code);
            }
            if (bottomMenu.controlKey(key_code)) {
                return true;
            }

            switch (key_code) {
                case KEY_CODE.UP:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                    } else {
                        try {
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName,
                                btnList[btnFocusIdx]);
                        } catch (e) {
                        }
                    }

                    if (rowIdx > 0) {
                        rowIdx--;
                    }
                    if (!isNotice && isDetailSyn) {
                        rowIdx = 0;
                    } else if (isNotice && !isDetailSyn) {
                        rowIdx = isNoticeUnFocus ? 2 : 1;
                    } else if (!isNotice && !isDetailSyn) {
                        rowIdx = 2;
                    } else if (isNoticeUnFocus) {
                        rowIdx = 0;
                    }

                    setBtnFocus(btnFocusIdx);

                    return true;
                case KEY_CODE.DOWN:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                        rowIdx++;
                    } else {
                        try {
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName, btnList[btnFocusIdx]);
                        } catch (e) {
                            log.printDbg(e);
                        }
                        bottomMenu.show();
                    }
                    if (!isNotice || isNoticeUnFocus) {
                        rowIdx = 2;
                    }
                    setBtnFocus(0);
                    return true;
                case KEY_CODE.LEFT:
                    try {
                        NavLogMgr.collectVODDetailPerchase(key_code, cateId, vodData.contsId || vodData.itemId,
                            vodData.contsName || vodData.itemName, btnList[btnFocusIdx]);
                    } catch (e) {
                        log.printWarn("[collect] " + e);
                    }

                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                        LayerManager.historyBack();
                    } else {
                        var btnIdx = getBtnIndex(-1);
                        if (btnIdx < 0) {
                            LayerManager.historyBack();
                        } else {
                            setBtnFocus(btnIdx);
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (rowIdx < 2) {
                        return true;
                    }

                    var preFocusIdx = btnFocusIdx;
                    setBtnFocus(getBtnIndex(1));

                    if (preFocusIdx != btnFocusIdx) {
                        try {
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName, btnList[btnFocusIdx]);
                        } catch (e) {
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (rowIdx < 2) {
                        var that = this;
                        var tmpData = {
                            title: vodData.contsName || vodData.itemName,
                            notice: vodData.bulletin || "",
                            synopsis: vodData.synopsis
                        };
                        LayerManager.activateLayer({
                            obj: {
                                id: rowIdx == 0 ? "VodSynopsisDetailPopup" : "VodNoticePopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    data: tmpData,
                                    isKids: that.div.hasClass("kids")
                                }
                            },
                            moduleId: "module.vod",
                            new: true,
                            visible: true
                        });
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                    } else {
                        try {
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId,
                                vodData.contsId || vodData.itemId,
                                vodData.contsName || vodData.itemName,
                                btnList[btnFocusIdx]);
                        } catch (e) {
                        }
                        btnList[btnFocusIdx].enterKeyAction();
                    }
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    contextMenu.open({wishFlag: wishObject.playCheckFlag});
                    return true;
                default:
                    return false;
            }
        };

        function getBtnIndex(amount) {
            var newIdx = btnFocusIdx + amount;

            if (newIdx < 0) {
                return -1;
            }
            if (!btnList[newIdx]) {
                return btnFocusIdx;
            }

            var connerYn = false;
            for (var i in btnList) {
                if (btnList[i].isCornerChk) {
                    connerYn = true;
                    break;
                }
            }

            if (connerYn && btnList[newIdx].type == ButtonObject.TYPE.CORNER) {
                return getBtnIndex(amount + (amount > 0 ? 1 : -1));
            } else if (connerYn && btnList[newIdx].type == ButtonObject.TYPE.VODPPT) {
                return getBtnIndex(amount + (amount > 0 ? 1 : -1));
            } else {
                return newIdx;
            }
        }

        function setBtnFocus(btnIndex) {
            btnFocusIdx = btnIndex;
            _this.div.find("#btnArea #btn_item_area div.vod_btn_area.btn_focus").removeClass("btn_focus");
            _this.div.find("#notice_infoArea").removeClass("focus");
            _this.div.find("#synopsis_btn_area").removeClass("focus");

            switch (rowIdx) {
                case 0:
                    _this.div.find("#synopsis_btn_area").addClass("focus");
                    DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), btnList[0].buyData);
                    break;
                case 1:
                    _this.div.find("#notice_infoArea").addClass("focus");
                    DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), btnList[0].buyData);
                    break;
                case 2:
                    _this.div.find("#btnArea #btn_item_area div.vod_btn_area").eq(btnIndex).addClass("btn_focus");
                    _this.div.find("#btnArea #btn_item_area div.vod_btn_area").css("-webkit-transform", "translateX(" + (-1256 * Math.floor(btnIndex / 4)) + "px)");
                    _this.div.find("#btn_arw_left").toggle(Math.floor(btnIndex / 4) > 0);
                    _this.div.find("#btn_arw_right").toggle(Math.floor(btnIndex / 4) != Math.floor((btnList.length - 1) / 4));
                    DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), btnList[btnIndex].buyData);
                    break;
            }
        }

        /** 최초 돔tree 생성 */
        function constructDom() {
            //Background
            var backgroundArea = _$("<div/>", {id: "background_area"});
            backgroundArea.append(
                _$("<div/>", {class: "bgBlurWrapper"}).append(_$("<img>", {class: "bgBlur posterImg_main"})),
                _$("<div/>", {class: "dim"}),
                _$("<img>", {class: "bImg"})
            );

            //포스터 & 별점
            var posterArea = _$("<div/>", {id: "posterArea"});
            posterArea.append(
                _$("<img>", {id: "main_poster", class: "posterImg_main"}),
                _$("<span>", {class: "basic_text"}).text("olleh tv 별점"),
                _$("<div/>", {id: "poster_starScore_area"}),
                _$("<img/>", {
                    id: "poster_contentCoupon_img",
                    src: window.modulePath + "resource/tag_poster_content_1.png"
                }),
                _$("<div/>", {id: "poster_contentInfo_area"}),
                _$("<img/>", {id: "poster_contentInfo_evtImg"}),
                _$("<img/>", {id: "poster_vodLock_img"})
            );

            // Info
            var infoArea = _$("<div/>", {id: "infoArea"});
            infoArea.append(
                _$("<div/>", {id: "maintitle_area"}),
                _$("<div/>", {id: "subtitle_area"}),
                _$("<div/>", {id: "subtitle_area_0"}),
                _$("<div/>", {id: "actor_area"}),
                _$("<div/>", {id: "synopsis_area"}),
                _$("<div/>", {id: "synopsis_btn_area"}).append(
                    _$("<img/>", {
                        class: "synopsis_btn_img",
                        src: window.modulePath + "resource/btn_detail_vod.png"
                    }),
                    _$("<span/>", {class: "synopsis_btn_text"}).text("+ 더보기")
                )
            );

            var ollehArea = _$("<div/>", {class: "ollehMobile_icon_area"});
            ollehArea.append(
                _$("<img/>", {class: "mobile_icon_img", src: window.modulePath + "resource/icon_detail_mobile.png"}),
                _$("<span/>", {class: "mobile_icon_text"}).text("올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다")
            );
            if (KTW.managers.service.ProductInfoManager.isBiz()) {
                ollehArea.css("visibility", "hidden");
            }

            var btnArea = _$("<div/>", {id: "btnArea"});
            btnArea.append(
                _$("<div/>", {id: "btn_item_area"}),
                _$("<div/>", {id: "btn_txt_area"})
            );

            // Tag를 미리 생성후 마지막 한번에 Append하여 속도 개선한다.
            // Append도 매번 호출이 아닌 Argument형태로 전달하면 그 개수에 맞게 Append를 수행한다
            _this.div.append(
                backgroundArea,
                posterArea,
                infoArea,
                _$("<div/>", {id: "notice_infoArea"}),
                ollehArea,
                btnArea,
                _$("<div/>", {id: "btn_arw_left"}),
                _$("<div/>", {id: "btn_arw_right"})
            );
        }

        /** 최초진입 시 View에 데이터 주입*/
        function setData(data, _pointData) {
            // 성인컨텐츠 Lock 이미지 표시 여부
            if (isAuthorizedContent || data.adultOnlyYn === "Y" || !UTIL.isLimitAge(data.prInfo)) {
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "none");
            } else {
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "inherit");

                if (parseInt(data.posterUseType) == 2) {
                    _this.div.find("#posterArea #poster_vodLock_img").addClass("square");
                } else {
                    _this.div.find("#posterArea #poster_vodLock_img").removeClass("square");
                }
            }

            // EventImage
            if (data.wEvtImageUrlW3) {
                _this.div.find("#poster_contentInfo_evtImg").attr("src", data.wEvtImageUrlW3);
            }

            //Bottom 메뉴 구성
            _this.div.append(bottomMenu.createView(data.dispContsClass, false, reqPathCd, isKidsCate));

            var imgURLPath = "";
            // 2017.10.18 Y
            // 포스터 유형별 원본 이미지를 요청 후 css로 사이즈 지정해서 그리도록 수정
            switch (parseInt(data.posterUseType)) {
                case 0:
                    imgURLPath = data.imgUrl;
                    _this.div.find("#main_poster.posterImg_main").css({width: "329px", height: "470px"});
                    break;
                case 1:
                    imgURLPath = data.wideImgUrl;
                    break;
                case 2:
                    imgURLPath = data.squareImgUrl;
                    _this.div.find("#main_poster.posterImg_main").css({width: "329px", height: "329px"});
                    break;
                default:
                    imgURLPath = data.imgUrl;
                    _this.div.find("#main_poster.posterImg_main").css({width: "329px", height: "470px"});
                    break;
            }

            // 공지사항 여부?
            data.bulletin = data.bulletin.trim();
            isNotice = data.bulletin.length > 0;

            // 포스터 영역
            _this.div.find("img.posterImg_main").attr("src", imgURLPath).load(function () {
            }).on('error', function () {
                // 포스터 이미지가 없을 경우 예외 처리 (default 포스터 일경우 blur 처리 hide)
                _this.div.find("#background_area .bgBlur").css("display", "none");
                _$(this).attr("src", window.modulePath + "resource/defaultPoster/" + (data.dispContsClass == "01" ? (parseInt(data.posterUseType) == 2 ? "default_music2.png" : "default_music.png") : "default_poster.png"));
            });

            // 타이틀
            var mainTitle = (data.contsName || data.itemName);
            if (window.UTIL.getTextLength(mainTitle, "RixHead B", 60, -0.5) >= 1067) {
                _this.div.find("#maintitle_area").css("width", "1140px");
                _this.div.find("#maintitle_area span").css("width", "1140px");
                mainTitle = HTool.ellipsis(mainTitle, "RixHead B", 60, 1067, "...", -0.5);
            }
            _this.div.find("#maintitle_area").html("<span>" + mainTitle + "<img id='detailTitle_age_icon_single' src='" + window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(data.prInfo) + ".png'></span>");

            // 서브타이틀, 출연정보, 부가정보 등
            _this.div.find("#subtitle_area").empty().append(
                _$("<span/>", {class: "subtitle_text"}).append(DetailUtil.getSubTitle(data))
            );

            _this.div.find("#subtitle_area_0").empty().append(
                _$("<span/>", {class: "subtitle_text"}).append(DetailUtil.getSubTitle_0(data)),
                _$("<div/>", {class: "subtitle_icon_img"}).append(DetailUtil.getSubTitleIconImg(data, {
                    parentDiv: _this.div,
                    isLongIcon: isShortLong,
                    isConnerBuyChk: isConnerBuyChk,
                    isVodPptBuyChk: isVodPptBuyChk,
                    isSingle: isSingle
                }))
            );

            _this.div.find("#actor_area").empty().append(DetailUtil.getActorArea(_this.div, data));

            // 시놉시스
            _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                data: data.synopsis,
                type: "SINGLE",
                isNotice: isNotice,
                isKids: isKidsCate,
                isShop: isShopDetail,
                callback: function (detailSync) {
                    // 더보기 버튼 활성화
                    isDetailSyn = detailSync;
                    _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                }
            }));

            // 배경 이벤트 이미지
            _this.div.find(".bImg").toggle(data.bImgUrl).attr("src", data.bImgUrl);

            _this.div.find("#notice_infoArea").empty().append(
                _$("<div/>", {class: "notice_icon"}),
                "<div id='notice_splitLine'></div>",
                "<div id='notice_contents'>" + data.bulletin + "</div>"
            );

            if (window.UTIL.splitText(data.bulletin, "RixHead L", 25, 960, -0.7).length == 1) {
                isNoticeUnFocus = true;
                _this.div.find("#notice_infoArea").addClass("noMore");
            } else {
                isNoticeUnFocus = false;
                _this.div.find("#notice_infoArea").removeClass("noMore");
            }

            _this.div.find("#posterArea #poster_starScore_area").append(_$("<div/>", {id: "score_area"}));

            var tmpMark = data.mark;
            if (tmpMark && tmpMark > 0) {
                DetailUtil.setStarNumber(_this.div, tmpMark);
            }

            _this.div.find("#posterArea #poster_starScore_area").append(stars.getView(stars.TYPE.RED_26));
            stars.setRate(_this.div.find("#posterArea #poster_starScore_area"), tmpMark);

            _this.div.find("#notice_infoArea").css("visibility", (data.bulletin ? "inherit" : "hidden"));

            var ollehObj = DetailUtil.makeSeamlessText(data, savedSeamlessCode, 0);
            if (ollehObj.result !== false) {
                var ollehIcon = _this.div.find(".ollehMobile_icon_area");

                savedSeamlessCode = ollehObj.code;

                if (ollehObj.result === "") {
                    ollehIcon.css("visibility", "hidden");
                } else {
                    ollehIcon.css("visibility", "inherit");
                    ollehIcon.find(".mobile_icon_text").text(ollehObj.result);
                }
            }

            data.catId = cateId;
            data.previewData = previewData;
            bottomMenu.setData(data, isAuthorizedContent || data.adultOnlyYn === "Y");
            bottomMenu.updateData(data, DetailUtil.getAdditionalInfo(cateData, btnList));
            setBtnFocus(btnFocusIdx);

            _this.div.find("#btn_txt_area").empty();
            _this.div.find("#btn_txt_area").append(DetailUtil.getPointText(
                _pointData,
                btnList[btnFocusIdx].getOptionList().vodPlus,
                btnList[btnFocusIdx].getOptionList().pptBuyYn,
                isConnerChk));
        }

        /** 버튼 갱신 */
        function setButton() {
            _this.div.find("#btn_item_area").empty();
            for (var i = 0; i < btnList.length; i++) {
                _this.div.find("#btn_item_area").append(btnList[i].getView());
            }
        }

        /** 버튼 액션 정의 */
        function btnKeyAction() {
            var that = this;

            that.buyData.execute(cateId, reqPathCd, function (res, preplayData) {
                if (res) {
                    preplayData.isAuthorizedContent = isAdultCate || preplayData.originData.data.adultOnlyYn === "Y";
                    if (sortGb) {
                        console.debug(playListData);
                        LayerManager.startLoading();

                        function playSortGbContents(playList) {
                            LayerManager.stopLoading();
                            var tmpIdx = 0;
                            if (!(playList && playList.length > 0)) {
                                return;
                            }
                            for (var idx = 0; idx < playList.length; idx++) {
                                if (!playList[idx]) {
                                    continue;
                                }
                                if ((preplayData.assetId || preplayData.contsId || preplayData.itemId) == playList[idx].contsId || (preplayData.assetId || preplayData.contsId || preplayData.itemId) == playList[idx].itemId) {
                                    tmpIdx = idx;
                                    break;
                                }
                            }
                            VODManager.requestPlaySeries(preplayData, playList, reqPathCd, tmpIdx, sortGb, function (contsId, catId, contsData) {
                                newParams = _$.extend({}, originParams);
                                newParams.const_id = contsId;
                                newParams.contsInfo = contsData;
                            });
                        }

                        if (playListData.amount - 0 > 0) {
                            VODAmocManager.getItemDetlListW3(function (res, listData) {
                                if (res) {
                                    listData.itemDetailList = _$.isArray(listData.itemDetailList) ? listData.itemDetailList : [listData.itemDetailList];
                                    for (var i = 0; i < playListData.amount - 0; i++) {
                                        playListData.list[playListData.startIdx - 0 + i] = listData.itemDetailList[i];
                                    }
                                    playSortGbContents(playListData.list)
                                } else {
                                    // 몰아보기 데이터 로드 실패 시 현재 곡만 재생
                                    LayerManager.stopLoading();
                                    VODManager.requestPlay(preplayData, reqPathCd);
                                }
                            }, cateId, playListData.startIdx + 1, playListData.amount);
                        } else {
                            playSortGbContents(playListData.list);
                        }
                    } else {
                        VODManager.requestPlay(preplayData, reqPathCd);
                    }
                }
            }, null, null, isAuthorizedContent || isAuthComplete, true, buttonUpdateCallback, false);
        }

        /** 미구매 상품 구매완료 시 구매정보 갱신을 위한 콜백 */
        function buttonUpdateCallback(buyData) {
            OptionListFactory.updateBuyInfo(optionData, buyData, cateData);
            makeButtonList();
            setButton();
            setBtnFocus(getBtnIndex(0));
            refreshBtnBuyInfo();
        }
    };

    vod_detail_single_layer.prototype = new Layer();
    vod_detail_single_layer.prototype.constructor = vod_detail_single_layer;

    //Override create function
    vod_detail_single_layer.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_detail_single_layer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodDetailSingleLayer"] = vod_detail_single_layer;
})();
