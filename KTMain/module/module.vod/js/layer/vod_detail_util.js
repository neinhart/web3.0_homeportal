/**
 * Vod Detail에서 사용되는 공용 유틸
 * */

window.DetailUtil = new function () {
    /** 자막 체크 */
    var _isCaption = function isCaption(_capUrl) {
        if (_capUrl) {
            if (_capUrl.indexOf("|") > -1) {
                _capUrl = _capUrl.replace("|", "");
                return isCaption(_capUrl);
            } else {
                return (_capUrl.length > 0);
            }
        } else {
            return false;
        }
    };

    /** 원제목 표시 한줄로 변경 */
    var _originSubTitleLengthCheck = function originSubTitleLengthCheck(dom, type) {
        // 2017.08.22 원제목 표시가 두 줄로 넘어갈 경우 한 줄로 표시하도록 수정
        var txtWidht = 1067;

        if (type === "SERIES") {
            txtWidht = 912;
        }

        dom.find("#subtitle_area .subtitle_text span:eq(0)").removeClass("limitArea");

        var tmpTxt = dom.find("#subtitle_area .subtitle_text").text();
        var tmpTxtWidth = UTIL.getTextLength(tmpTxt, "RixHead M", 24, -0.7) + 56;

        if (tmpTxtWidth > txtWidht && dom.find("#subtitle_area").height() > 35) {
            dom.find("#subtitle_area .subtitle_text span:eq(0)").addClass("limitArea");

            var tmpSubTxtWidth = dom.find("#subtitle_area .subtitle_text span:eq(0)").width();
            tmpSubTxtWidth = tmpSubTxtWidth - (tmpTxtWidth - txtWidht);

            dom.find("#subtitle_area .subtitle_text span:eq(0)").css("width", tmpSubTxtWidth + "px");
        }
    };

    /** 일자에 점붙이기*/
    var _dateFormatter = function dateFormatter(d) {
        return d.substring(0, 4) + "." + d.substring(4, 6) + "." + d.substring(6, 8);
    };

    /** 숫자 콤마 붙이기 */
    var _numberWithCommas = function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    /** 페어링 문구*/
    var _makeSeamlessText = function makeSeamlessText(data, code, index) {
        if (KTW.managers.service.ProductInfoManager.isBiz()) {
            return {
                result: false
            };
        }

        var seamlessText,
            savedSeamlessCode = code || "0";

        var seamlessCode = HTool.checkSeamlessCode(data, index);

        if (savedSeamlessCode !== seamlessCode) {
            savedSeamlessCode = seamlessCode;
        }

        if (savedSeamlessCode === "1") {
            // 1:1 심리스
            seamlessText = "올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다";
        } else if (savedSeamlessCode === "2") {
            // 1:N 심리스
            seamlessText = "올레 tv 모바일앱(3대까지 동시시청 가능)에서도 함께 시청 가능합니다";
        } else {
            // 0 문구 미표시
            seamlessText = "";
        }

        log.printDbg("makeSeamlessText seamlessCode = " + savedSeamlessCode);

        return {
            result: seamlessText,
            code: savedSeamlessCode
        };
    };

    /** 포인트 */
    var _getPointText = function getPointText(data, isVodPlus, isVodPpt, isConnerChk) {
        var res = "";
        var tmpRes = "";

        if (isConnerChk) {
            if (isVodPlus) {
                res = "고객님이 가입하신 상품에서는 무료시청이 가능합니다";
            } else if (isVodPpt) {
                res = "고객님은 기간정액 가입 중으로 무료시청이 가능합니다";
            } else {
                res = "고객님은 월정액 가입 중으로 무료시청이 가능합니다";
            }
        } else {
            if (!KTW.managers.service.ProductInfoManager.isBiz() && data) {
                res = "<span style='margin-right: 10px;'></span>";
                if (data.StarPoint) {
                    tmpRes += "<span>KT 멤버십 " + _numberWithCommas(data.StarPoint) + "원</span>";
                }
                if (data.TvMoney || data.KtCoupon) {
                    tmpRes += (tmpRes.length > 0 ? "<div class='point_Bar'></div><span>TV쿠폰 " : "<span>TV쿠폰 ") + _numberWithCommas(parseInt(data.TvMoney) + parseInt(data.KtCoupon)) + "원</span>";
                }
                if (data.TvPoint) {
                    tmpRes += (tmpRes.length > 0 ? "<div class='point_Bar'></div><span>TV포인트 " : "<span>TV포인트 ") + _numberWithCommas(data.TvPoint) + "P</span>";
                }
                if (data.NumOfCoupon && data.NumOfCoupon != 0) {
                    tmpRes += (tmpRes.length > 0 ? "<div class='point_Bar'></div><span>콘텐츠이용권 " : "<span>콘텐츠이용권 ") + data.NumOfCoupon + "개</span>";
                }
                if (tmpRes.length == 0) {
                    res = "";

                } else {
                    res += tmpRes;
                }
            } else {
                res = "";
            }
        }
        return res;
    };

    /** 별점 정보 */
    var _setStarNumber = function setStarNumber(dom, starData) {
        var tmpStarNum = starData;
        dom.find("#score_area").append(_$("<img>", {
            class: "poster_scoreImg",
            src: ROOT_URL + "resource/rating_vod_number_" + Math.floor(tmpStarNum) + ".png"
        }));

        var chgNum_0 = tmpStarNum * 10;
        var chgNum_1 = Math.floor(tmpStarNum) * 10;
        if (chgNum_0 - chgNum_1 > 0) {
            var tmpStarNum2 = chgNum_0 - chgNum_1;
            dom.find("#score_area").append(_$("<img>", {
                class: "poster_scoreImg",
                src: ROOT_URL + "resource/rating_vod_dot.png"
            }));
            dom.find("#score_area").append(_$("<img>", {
                class: "poster_scoreImg",
                src: ROOT_URL + "resource/rating_vod_number_" + tmpStarNum2 + ".png"
            }));
        } else {
            dom.find("#score_area").append(_$("<img>", {
                class: "poster_scoreImg",
                src: ROOT_URL + "resource/rating_vod_dot.png"
            }));
            dom.find("#score_area").append(_$("<img>", {
                class: "poster_scoreImg",
                src: ROOT_URL + "resource/rating_vod_number_0.png"
            }));
        }
    };

    /** 시놉시스 라인 정의 */
    var _getSynopsisLineCnt = function getSynopsisLineCnt(type, isNotice, isKids, isShop) {
        var lineCnt;
        if (type === "MCID") {
            lineCnt = isKids ? 2 : 4;
        } else if (type === "SINGLE") {
            lineCnt = isShop ? 6 : 4;

            if (isKids) {
                lineCnt = isShop ? 5 : 2;
            }
        } else if (type === "SERIES") {
            lineCnt = 3;
        } else if (type === "PACKAGE") {
            lineCnt = 2;
        } else {
            // APPGAME
            lineCnt = 5;
        }

        if (isNotice) {
            lineCnt--;
        }

        log.printDbg("getSynopsisLineCnt type = " + type + " ,lineCnt = " + lineCnt);

        return lineCnt;
    };

    /** 티비포인트 갱신 */
    var _updateTvPoint = function updateTvPoint(cbComplete) {
        if (KTW.managers.service.ProductInfoManager.isBiz()) {
            // [kh.kim] 비즈 상품에서는 상세화면 진입 시 포인트 조회하지 않음
            cbComplete(null);
            return null;
        }

        var pointData = window.StorageManager.ps.load(window.StorageManager.KEY.STB_POINT_CACHE);

        if (!pointData || JSON.parse(pointData).ReqFlag === true) {
            VODPointManager.getPoint('1', function (res, _pointData) {
                if (res) {
                    var optionData = _pointData.settlWayList;
                    // 캐시 데이터 형태로 저장
                    var dataForCache = {};
                    var _data;
                    dataForCache.PointAmount = _pointData.totBlncAmt;
                    for (var i = 0; i < optionData.length; i++) {
                        _data = optionData[i];
                        if (_data.settlWayCd === "ST") {
                            //KT 맴버십
                            dataForCache.StarPoint = _data.blncAmt;
                            dataForCache.UsrDiv = _data.mbrTypeCd;
                            dataForCache.MembershipRate = _data.settlRate;
                        } else if (_data.settlWayCd === "CP") {
                            // TV 쿠폰 (KT제공)
                            dataForCache.KtCoupon = _data.blncAmt;
                        } else if (_data.settlWayCd === "TP") {
                            // TV 포인트
                            dataForCache.TvPoint = _data.blncAmt;
                            dataForCache.TvPointYn = _data.tvPointYn;
                        } else if (_data.settlWayCd === "CS") {
                            // TV 쿠폰
                            dataForCache.TvMoney = _data.blncAmt;
                        } else if (_data.settlWayCd === "CU") {
                            // 이용권 개수
                            dataForCache.NumOfCoupon = _data.blncAmt;
                        }
                    }

                    pointData = dataForCache;
                    dataForCache.ReqFlag = false;
                    dataForCache.UpdateTime = new Date().getTime();
                    window.StorageManager.ps.save(StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(dataForCache));
                } else {
                    pointData = {};
                }
                cbComplete(pointData);
            });
        } else {
            pointData = JSON.parse(pointData);
            cbComplete(pointData);
        }
        return pointData;
    };

    /** 상세화면 상세정보 첫번째 라인*/
    var _getSubTitle = function getSubTitle(data) {
        var res = "";

        if (data.originTitle) {
            res += "<span>" + data.originTitle + "</span>";

            if (data.delDate) {
                res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>서비스 종료일 " + DetailUtil.dateFormatter(data.delDate) + "</span>";
            } else if (data.releaseDate) {
                res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>개봉일 " + DetailUtil.dateFormatter(data.releaseDate) + "</span>";
            } else if (data.broadDate) {
                res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>방송일 " + DetailUtil.dateFormatter(data.broadDate) + "</span>";
            }
            return res;
        } else {
            var resCnt = 0;

            if (data.releaseDate && resCnt < 2) {
                res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>개봉일 " + DetailUtil.dateFormatter(data.releaseDate) + "</span>";
                resCnt++;

                if (data.delDate && resCnt < 2) {
                    res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>서비스 종료일 " + DetailUtil.dateFormatter(data.delDate) + "</span>";
                    resCnt++;
                }
            }

            if (data.broadDate && resCnt < 2) {
                res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>방송일 " + DetailUtil.dateFormatter(data.broadDate) + "</span>";
                resCnt++;
            }

            if (data.delDate && resCnt < 2) {
                res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>서비스 종료일 " + DetailUtil.dateFormatter(data.delDate) + "</span>";
            }
        }

        if (data.genreDetail) {
            res += (res.length > 0 ? "<div class='splitBar'></div>" : "") + "<span>" + data.genreDetail + "</span>";
        }
        return res;
    };

    /** 상세화면 상세정보 두번째 라인*/
    var _getSubTitle_0 = function getSubTitle_0(data) {
        var res = "";

        if (data.runtime) {
            res += "<span>" + data.runtime + "분</span><div class='splitBar'></div>";
        }
        return res;
    };

    /** 상세화면 아이콘 */
    var _getSubTitleIconImg = function getSubTitleIconImg(data, params) {
        var res = "", imageUrl = "";
        var tmpRes = data.resolCd.split("|");
        var pIconCnt = 0;
        var imageType = params.type === "SERIES" ? "1" : "2";
        var seriesIcon = false;

        params.parentDiv.find("#poster_contentInfo_area").empty();

        if (data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_hdr.png'>";

            imageUrl = window.modulePath + "resource/tag_poster_hdr_" + imageType + ".png";
            params.parentDiv.find("#poster_contentInfo_area").append(_$("<img>", {src: imageUrl}));
            pIconCnt++;
        }

        for (var i = tmpRes.length - 1; i >= 0; i--) {
            if (tmpRes[i] == "UHD") {
                res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_uhd.png'>";

                imageUrl = window.modulePath + "resource/tag_poster_uhd_" + imageType + ".png";
                params.parentDiv.find("#poster_contentInfo_area").append(_$("<img>", {src: imageUrl}));
                pIconCnt++;
            }

            if (tmpRes[i] == "HD") {
                res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_hd.png'>";
            }
        }
        if (tmpRes.length > 0) {
            var tmpSnd = data.audioType.split("|");

            for (var i = tmpSnd.length - 1; i >= 0; i--) {
                if (tmpSnd[i] == "E-AC3") {
                    res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_71.png'>";
                }

                if (tmpSnd[i] == "AC3") {
                    res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_51.png'>";
                }
            }
        } else {
            if (data.audioType == "E-AC3") {
                res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_71.png'>";
            }

            if (data.audioType == "AC3") {
                res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_51.png'>";
            }
        }
        if (_isCaption(data.capUrl)) {
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_subtitles.png'>";
        }

        if (data.multiAudYn == "Y" || (data.isMultiAud && data.isMultiAud == "Y")) {
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_tag_multisound.png'>";
        }

        if (data.buyType == BUY_TYPE.SERIES || data.buyType == BUY_TYPE.SERIES_SINGLE || data.buyType == BUY_TYPE.CORNER_SERIES || data.buyType == BUY_TYPE.CORNER_SERIES_SINGLE
            || data.buyType == BUY_TYPE.VODPPT_SERIES || data.buyType == BUY_TYPE.VODPPT_SERIES_SINGLE) {
            if (params.isLongIcon) {
                seriesIcon = true;
            } else {
                res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_detail_series_purchase.png'>";
            }
        }

        if (params.isConnerBuyChk) {
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_detail_monthly.png'>";
        }

        if (params.isVodPptBuyChk) {
            // 기간정액 아이콘 표시
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_detail_period.png'>";
        }

        if (params.isSingle && data.ltFlag == 2) {
            // 단편 장기
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_detail_short_long.png'>";
        }

        if (data.isDvdYn == "Y") {
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_detail_possess.png'>";

            if (pIconCnt != 2) {
                imageUrl = window.modulePath + "resource/tag_poster_mine_" + imageType + ".png";
                params.parentDiv.find("#poster_contentInfo_area").append(_$("<img>", {src: imageUrl}));
            }
        }

        /**
         * [WEBIIIHOME-3643] "시리즈구매- 월정액(기간정액) - 단편장기(소장용)- 시리즈장기" 순으로 icon 표시하도록 수정
         */
        if (seriesIcon) {
            // 시리즈 장기
            res += "<img class='vodRes_icon_img' src='" + ROOT_URL + "resource/icon_detail_series_long.png'>";
        }

        if (res.length > 0) {
            params.parentDiv.find("#subtitle_area_0 .subtitle_text .splitBar").css("display", "block");
        } else {
            params.parentDiv.find("#subtitle_area_0 .subtitle_text .splitBar").css("display", "none");
        }
        return res;
    };

    /** KT 버튼 추가 */
    var _addedBtnObject = function addedBtnObject(name, type, trgt, index) {
        log.printDbg("addedBtnObject name = " + name + " , type = " + type);

        var div = _$("<div/>", {class: "vod_btn_area added", "data-voiceable-action": "", index: index});

        this.buyData = trgt;
        this.type = 99;
        this.isOtherAppBtn = true;
        this.target = trgt; // MCID 상세화면에서 버튼 추가 할때, 동일 버튼 여부 체크 시 사용

        div.append(
            _$("<div/>", {class: 'vod_btn_tmpBackground'}),
            "<div class='vod_btn_title'>" + name + "</div>"
        );

        this.getView = function () {
            return div;
        };

        this.enterKeyAction = function () {
            goToTarget(type, trgt);
        };

        this.getOptionList = function () {
            return {};
        }
    };

    /** 버튼 Object에 추가 정보 주입 */
    var getButtonListByType = function (btnList) {
        var tmp = [];

        for (var i in btnList) {
            tmp[btnList[i].type] = btnList[i];
        }
        return tmp;
    };

    var _getAdditionalInfo = function getAdditionalInfo(data, btnList) {
        var btns = getButtonListByType(btnList),
            tmp;

        if (data.buyType == BUY_TYPE.SERIES) {
            if ((tmp = (btns[ButtonObject.TYPE.SERIES] || btns[ButtonObject.TYPE.PACKAGE]))) {
                return {
                    checkLinkTime: true,
                    connerYn: tmp.getOptionList().connerYn,
                    buyYn: tmp.getOptionList().buyYn
                }
            } else {
                return {
                    checkLinkTime: false
                }
            }
        } else {
            return {
                checkLinkTime: false
            }
        }
    };

    /** 출연자 정보 */
    var _getActorArea = function getActorArea(dom, data) {
        var res = "";

        if (data.overseerName) {
            res += "<span><obj>감독</obj>" + data.overseerName + "</span>";
        }

        if (data.play) {
            res += "<span><obj>각본</obj>" + data.play + "</span>";
        }

        if (data.actor) {
            res += "<span><obj>출연</obj>" + data.actor + "</span>";
        }
        dom.find("#actor_area").css("display", (res.length == 0 ? "none" : "block"));

        return res;
    };

    /**
     * 시놉시스 생성
     * 줄거리를 공지 유무에 따라 2,3줄로 설정하고 표시가능 글자수를 초과할 경우 ellipsis 처리를 한다.
     * @param synopsisData 수신된 줄거리 text
     * @returns {string} 가공된 줄거리 텍스트
     * */
    var _synopsisConverter = function synopsisConverter(param) {
        // 기본속성 정의
        var lineWidth = 1076;
        var finalLineWidth = 940;
        var synopsisTextFontName = "RixHead L";
        var synopsisTextFontSize = 28;
        var letterSpace = -0.71;
        var data = param.data;
        var type = param.type;
        var isNotice = param.isNotice || false;
        var isKids = param.isKids || false;
        var isShop = param.isShop || false;

        if (type === "SERIES") {
            lineWidth = 960;
            finalLineWidth = 829;
        } else if (type === "PACKAGE") {
            lineWidth = 1130;
            finalLineWidth = 1000;
            synopsisTextFontSize = 27;
        }

        // 공통처리 모듈화(HTool.synopsisConvertor())
        var convertedData = HTool.synopsisConvertor(
            data,
            _getSynopsisLineCnt(type, isNotice, isKids, isShop),
            lineWidth,
            finalLineWidth,
            synopsisTextFontName,
            synopsisTextFontSize,
            letterSpace
        );

        if (param.callback) {
            // 더보기 버튼 활성화
            param.callback(convertedData.isDetailSyn)
        }
        return convertedData.convertTxt;
    };

    /** 미리보기, 하이라이트보기 체크 */
    var _checkPreview = function checkPreview(optionData, callback) {
        var isPreview = false,
            contextText = "";

        for (var i in optionData) {
            if (!(optionData[i].buyYn || optionData[i].connerYn || optionData[i].pptBuyYn || optionData[i].lessPrice == 0)) {
                for (var j in optionData[i].optionList) {
                    if (optionData[i].optionList[j].previewYn === "Y") {
                        var previewText,
                            previewTime;

                        isPreview = true;

                        if (optionData[i].optionList[j].previewStartTime === "00:00:00") {
                            previewText = ["무료로 {N}분 동안 미리보세요!", "{N}분 미리보기"];
                            // [2018-02-28 TaeSang] 연관메뉴 가이드 문구에는 N분을 표기하지 않도록 함 (GUI 리뷰 요청 사항)
                            contextText = "미리보기";
                        } else {
                            previewText = ["가장 재밌는 하이라이트를 {N}분동안 미리보세요!", "{N}분 하이라이트 보기"];
                            contextText = "하이라이트 보기";
                        }

                        previewTime = parseInt(Number(optionData[i].optionList[j].previewRunTime) / 60) || "N";
                        for (i in previewText) {
                            previewText[i] = previewText[i].replace(/\{N}/g, previewTime);
                        }

                        if (callback) {
                            log.printDbg("_checkPreview >>> " + isPreview);
                            callback({
                                isPreview: isPreview,
                                previewText: previewText,
                                contextText: contextText
                            });
                        }

                        break;
                    }
                }
            }
        }

        // 옵션키 가이드 문구 변경을 위해, 미리보기가 아닐 때에도 callback 함수를 호출함
        if (isPreview === false && callback) {
            log.printDbg("_checkPreview >>> " + isPreview);
            callback({isPreview: isPreview});
        }
    };

    /** 버튼 객체 생성 */
        // otherCouponBtn - 상세화면에서 HD
    var _btnObject = function btnObject(data, otherCouponBtn, dom, index) {
            log.printDbg("btnObject");

            this.buyData = data;
            this.otherCouponBtn = otherCouponBtn;

            var resolution = data.resolCd,
                price = data.lessPrice,
                isList = data.isList;

            var div = _$("<div/>", {class: "vod_btn_area " + getDivClass(data.type), "data-voiceable-action": "", index: index});
            div.append(_$("<div/>", {class: 'vod_btn_tmpBackground'}));

            var isFree = (data.connerYn || data.pptBuyYn || data.buyCheckYn) ? true : false;
            var noCorner = false;
            var priceType = "";

            if (data.vodPlus) {
                // 프라임 상품
                priceType = resolution || "";
                div.append("<div class='vod_btn_price'><span class='price_type'>" + priceType + "</span><span class='price_value' style='font-size:31px;opacity: 0.8;'>" + (isFree ? " 무료시청" : "") + "</span><span class='price_won'></span></div>");
            } else if (data.onlyCorner) {
                if (!data.noCorner) {
                    priceType = (isList ? "최저 월" : "월") || "";
                    div.append(
                        "<div class='vod_btn_price'><span class='price_type'>" + priceType + "</span><span class='price_value' style='font-size:31px;opacity: 1.0;'> " + DetailUtil.numberWithCommas(price) + "</span><span class='price_won'> 원</span></div>",
                        "<div class='vod_btn_buyImg_bg'/><div class='vod_btn_buyImg'/>",
                        "<div class='vod_btn_line'></div>"
                    );
                } else {
                    noCorner = true;
                    div.addClass("noCorner");
                    div.append("<div class='vod_btn_buyImg_bg'/><div class='vod_btn_buyImg'/>");
                }

                if (isFree) {
                    // 무료 (부가세 미포함)
                    div.addClass("free");
                }
            } else if (data.onlyVodPpt) {
                if (!data.noCorner) {
                    priceType = (isList ? "최저 월" : "월") || "";
                    div.append(
                        "<div class='vod_btn_price'><span class='price_type'>" + priceType + "</span><span class='price_value' style='font-size:31px;opacity: 1.0;'> " + DetailUtil.numberWithCommas(price) + "</span><span class='price_won'> 원</span></div>",
                        "<div class='vod_btn_buyImg_bg'/><div class='vod_btn_buyImg'/>",
                        "<div class='vod_btn_line'></div>"
                    );
                } else {
                    noCorner = true;
                    div.addClass("noCorner");
                    div.append("<div class='vod_btn_buyImg_bg'/><div class='vod_btn_buyImg'/>");
                }

                if (isFree) {
                    // 무료 (부가세 미포함)
                    div.addClass("free");
                }
            } else {
                if (data.isOnlyView) {
                    noCorner = true;
                    div.addClass("noCorner free");
                    div.append("<div class='vod_btn_buyImg_bg'/><div class='vod_btn_buyImg'/>");
                } else if (price == 0) {
                    isFree = true;
                    priceType = resolution || "";
                    div.append("<div class='vod_btn_price'><span class='price_type'>" + priceType + "</span><span class='price_value' style='font-size:31px;opacity: 0.8;'> 무료시청 </span><span class='price_won'> </span></div>");
                } else {
                    priceType = (isList && !(data.buyYn) ? "최저" : resolution) || "";
                    div.append("<div class='vod_btn_price'><span class='price_type'>" + priceType + "</span><span class='price_value' style='font-size:31px;opacity: 1.0;'> " + DetailUtil.numberWithCommas(price) + "</span><span class='price_won'> 원</span></div>");
                }

                if (!div.hasClass("corner") && !data.isOnlyView) {
                    div.append(
                        "<div class='vod_btn_buyImg_bg'></div><div class='vod_btn_buyImg'></div>",
                        "<div class='vod_btn_line'></div>"
                    );
                }

                // [WEBIIIHOME-3809] 패키지 구매여부 buyCheckYn 처리
                if ((data.buyCheckYn || data.buyYn) && price > 0) {
                    div.addClass("free");
                }

                // 패키지의 경우 가입중이라는 문구 미표기
                if ((data.connerYn || data.pptBuyYn) && data.type !== ButtonObject.TYPE.PACKAGE) {
                    this.isCornerChk = true;
                    // 가입중 문구 표기
                    dom.addClass("hasConner");

                    if (price > 0) {
                        div.addClass("free");
                    }
                }
            }

            // 버튼 금액표기 (free)의 경우에는 부가세 문구를 표기하지 않는다
            // noCorner 버튼도 부가세 문구 표기하지 않는다
            // VOD Plus 버튼도 부가세 문구 표기하지 않는다...
            var tax = (!isFree && !noCorner && !data.vodPlus) ? " tax" : "";
            div.append("<div class='vod_btn_title " + (isFree ? "free" : "") + tax + "'>" + data.title + "</div>");

            this.getView = function () {
                return div;
            };
            this.getOptionList = function () {
                return data;
            };
            this.type = data.type;

            function getDivClass(type) {
                switch (type) {
                    case ButtonObject.TYPE.NORMAL:
                        return "single";
                    case ButtonObject.TYPE.DVD:
                        return "dvd";
                    case ButtonObject.TYPE.SERIES:
                        return "series";
                    case ButtonObject.TYPE.CORNER:
                        return "corner";
                    case ButtonObject.TYPE.PACKAGE:
                        return "package";
                    case ButtonObject.TYPE.VODPPT:
                        return "corner";
                }
            }
        };

    /** 버튼 생성 */
    function makeButtonList(optionData, curContData, callback) {
        log.printDbg("makeButtonList length = " + optionData.length);

        var btnList = [];

        var isSingle = false;
        var isConnerChk = false;
        var isConnerBuyChk = false;
        var isVodPptBuyChk = false;
        var isSeriesBuyChk = false;

        for (var i = 0; i < optionData.length; i++) {
            if (optionData[i].connerYn || optionData[i].pptBuyYn) {
                isConnerChk = true;
            }

            if (optionData[i].type == ButtonObject.TYPE.CORNER) {
                isConnerBuyChk = true;
            }

            if (optionData[i].type == ButtonObject.TYPE.VODPPT) {
                isVodPptBuyChk = true;
            }

            if (optionData[i].type == ButtonObject.TYPE.SERIES) {
                isSeriesBuyChk = true;
            }

            if (optionData[i].type == ButtonObject.TYPE.NORMAL) {
                isSingle = true;
            }
            btnList.push(new btnObject(optionData[i]));
        }

        // KT 버튼 생성
        if (curContData.btnName && curContData.btnName.length > 0) {
            var nameList = curContData.btnName.split("|");
            var typeList = curContData.btnType.split("|");
            var trgtList = curContData.btnTrgt.split("|");

            for (var i = 0; i < nameList.length; i++) {
                btnList.push(new DetailUtil.addedBtnObject(nameList[i], typeList[i], trgtList[i]));
            }
        }

        if (callback) {
            callback({
                btnList: btnList,
                isSingle: isSingle,
                isConnerChk: isConnerChk,
                isConnerBuyChk: isConnerBuyChk,
                isVodPptBuyChk: isVodPptBuyChk,
                isSeriesBuyChk: isSeriesBuyChk
            })
        }
    }

    /** 버튼리스트 추가 */
    function addAddedButton(btnObj) {
        for (var i = 0; i < btnList.length; i++) {
            if (btnList[i].isOtherAppBtn && btnList[i].target == btnObj.target) {
                return;
            }
        }
        btnList.push(btnObj)
    }

    /** 이용권 아이콘 업데이트 */
    // 단편, MCID 는 아이콘 div ID를 #posterArea #poster_contentCoupon_img 로,
    // 시리즈는 #rect_poster_area #poster_contentCoupon_img 로 쓰고 있어서
    // 각 화면에서 div를 넘겨받아서 아이콘 이미지, display 속성을 변경하도록 처리
    // btn을 null로 전달하면 display none 으로 변경
    function _couponIconUpdate(div, btn) {
        // 그리는 우선 순위는 다음과 같다. ACAP과 동일하게 맞춤.
        // 콘텐츠 이용권 > 시리즈 이용권 > 콘텐츠 할인권 > 시리즈 할인권
        if (div && (btn && btn.couponCount && btn.couponCount > 0)) {
            if (btn.hasCouponCC) { // 콘텐츠 이용권
                div.attr({src: window.modulePath + "resource/tag_poster_content_1.png"});
            }
            else if (btn.hasCouponSC) { // 시리즈 이용권
                div.attr({src: window.modulePath + "resource/tag_poster_series_1.png"});
            }
            else if (btn.hasCouponCD) { // 콘텐츠 할인권
                div.attr({src: window.modulePath + "resource/tag_poster_content_2.png"});
            }
            else if (btn.hasCouponSD) { // 시리즈 할인권
                div.attr({src: window.modulePath + "resource/tag_poster_series_2.png"});
            }
            div.css("display", "block");
        } else {
            div.css("display", "none");
        }
    }

    function _couponIconUpdateSeriesDetail(dom, btn) {
        // 그리는 우선 순위는 다음과 같다.
        // 콘텐츠 이용권 > 시리즈 이용권 > 콘텐츠 할인권 > 시리즈 할인권
        if (btn.couponCount && btn.couponCount > 0) {
            if (btn.hasCouponCC) {
                dom.div.find("#rect_poster_area #poster_contentCoupon_img").attr({src: window.modulePath + "resource/tag_poster_content_1.png"});
            }
            else if (btn.hasCouponSC) {
                dom.div.find("#rect_poster_area #poster_contentCoupon_img").attr({src: window.modulePath + "resource/tag_poster_series_1.png"});
            }
            else if (btn.hasCouponCD) {
                dom.div.find("#rect_poster_area #poster_contentCoupon_img").attr({src: window.modulePath + "resource/tag_poster_content_2.png"});
            }
            else if (btn.hasCouponSD) {
                dom.div.find("#rect_poster_area #poster_contentCoupon_img").attr({src: window.modulePath + "resource/tag_poster_series_2.png"});
            }
            dom.div.find("#rect_poster_area #poster_contentCoupon_img").css("display", "block");
        } else {
            dom.div.find("#rect_poster_area #poster_contentCoupon_img").css("display", "none");
        }
    }

    return {
        isCaption: _isCaption,
        btnObject: _btnObject,
        checkPreview: _checkPreview,
        getPointText: _getPointText,
        getSubTitle: _getSubTitle,
        getSubTitle_0: _getSubTitle_0,
        getActorArea: _getActorArea,
        dateFormatter: _dateFormatter,
        setStarNumber: _setStarNumber,
        updateTvPoint: _updateTvPoint,
        addedBtnObject: _addedBtnObject,
        synopsisConverter: _synopsisConverter,
        getAdditionalInfo: _getAdditionalInfo,
        numberWithCommas: _numberWithCommas,
        makeSeamlessText: _makeSeamlessText,
        getSynopsisLineCnt: _getSynopsisLineCnt,
        getSubTitleIconImg: _getSubTitleIconImg,
        originSubTitleLengthCheck: _originSubTitleLengthCheck,
        couponIconUpdate: _couponIconUpdate,
        couponIconUpdateSeriesDetail: _couponIconUpdateSeriesDetail
    }
};