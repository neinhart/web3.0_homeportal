/**
 * Created by Taesang on 2016-11-02.
 */

(function() {
    var vod_end_popup = function vodEndLayer(options) {
        var _this = this;
        Layer.call(this, options);
        var div, data, btnCnt, recommendDiv;
        var endMode = ["시청 중인 VOD를 종료하시겠습니까?", "다음 회를 이어서 보시겠습니까?"];
        var ratingText = ["안봤어요", "최악이에요", "괜히 봤어요", "재미없어요", "별로예요", "아쉬워요", "보통이에요", "괜찮아요", "재미있어요", "정말좋아요", "최고예요"];
        var buttonText = ["확인", "취소"];
        var curIdx = 0, curRow = 1, rating, orgRating, contsIdx = 0, curPage = 0, maxPage = 0;
        var recommendData;
        var autoContinue = false;
        var countDownTimeout = null, countDownNumber = 11;
        var closeFromEnterkey = false;
        var isAuthorizedContent = false;

        function countDown() {
            clearTimeout(countDownTimeout);
            countDownTimeout = void 0;
            if (autoContinue) {
                countDownTimeout = setTimeout(function() {
                    if (countDownNumber <= 0 && autoContinue) {
                        closeFromEnterkey = true;
                        LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                        data.okAction();
                    } else {
                        countDownNumber--;
                        countDown();
                    }
                    div.find(".description_area").text(countDownNumber + " 초 후 자동으로 시작됩니다");
                }, 1000);
            }
        }

        function setButtons() {
            btnCnt = 2;
            var buttonAreaWidth = (12 + 328) * btnCnt;
            var buttonAreaLeft = (1920 - buttonAreaWidth) / 2;
            div.find(".button_area").css({
                "left": buttonAreaLeft + "px",
                "width": buttonAreaWidth + "px"
            });
        }

        function setButtonFocus() {
            div.find(".button_area .button").removeClass("focus");
            div.find(".button_area .button").eq(curIdx).addClass("focus");
        }

        function setRowFocus() {
            // 별점 포커스
            if (curRow == 0) {
                if (rating == null) rating = 3;
                div.find(".rating_area").addClass("focus");
                div.find(".rating_text_area").addClass("focus").text(getRatingText(rating));
                div.find(".button_area .button.focus").removeClass("focus");
                setRating(rating);
            }
            // 버튼 포커스
            else if (curRow == 1) {
                curIdx = 0;
                div.find(".rating_area").removeClass("focus");
                div.find(".recommend_area").removeClass("focus");
                div.find(".bottom_dim_area").removeClass("focus");
                div.find(".button_area .button").eq(curIdx).addClass("focus");
            }
            // 함많본 포커스
            else if (curRow == 2) {
                div.find(".button_area .button.focus").removeClass("focus");
                div.find(".recommend_area").addClass("focus");
                div.find(".bottom_dim_area").addClass("focus");
            }
            changeFocus();
        }

        function setRating(star) {
            stars.setRate(div.find(".rating_area .star_area"), star);
            div.find(".rating_text_area").text(getRatingText(rating));
        }

        function getRatingText(star) {
            return ratingText[star * 2];
        }

        function getMyRating() {
            curationManager.getMyRating(StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT) != 1 ? "Y" : "N", data.contsId, function(result, response) {
                if (result && response.RESULT_CODE == 200) {
                    orgRating = parseFloat(response.RATING);
                    rating = parseFloat(response.RATING);
                    div.find(".rating_text_area").addClass("focus");
                    setRating(rating);
                } else {
                    div.find(".rating_text_area").removeClass("focus").text("별점 평가해보세요");
                    setRating(0);
                }
                log.printDbg(JSON.stringify(response));
            });
        }

        function saveRating() {
            curationManager.saveRating(StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT) != 1 ? "Y" : "N", data.contsId, rating, "P", function(result, response) {
                log.printDbg(JSON.stringify(response));
                // 내가평가한 콘텐츠 화면 갱신을 위해 별점 평가여부를 우리집맞춤TV 모듈에 알려줌
                var myhomeModule = ModuleManager.getModule("module.family_home");
                if (myhomeModule) myhomeModule.execute({method: "setRatingFlag"});
            });
        }

        function setRollingPage(leftPage, rightPage) {
            if (!recommendData || recommendData.length == 0) {
                noRecommendData();
                return;
            }
            var list = recommendDiv.find(".posterList");
            var renderPage = leftPage, tmpIdx;
            for (var j = 0; j < 16; j++) {
                var li = list.find(".content.poster_vertical").eq(j);
                if (j < 8) {
                    if (j == 0 && leftPage == 0) {
                        li.addClass("hide");
                        li.attr("id", "");
                        continue;
                    }
                    renderPage = leftPage;
                    tmpIdx = (renderPage * 7) + j - 1;
                }
                else {
                    renderPage = rightPage;
                    tmpIdx = (renderPage * 7) + j - 8;
                }
                if (!recommendData[tmpIdx]) {
                    li.addClass("hide");
                    li.attr("id", "");
                }
                else {
                    setRollingPageData(li, tmpIdx);
                }
            }
        }

        function moveRollingPage(goNext, currentPage, nextPage) {
            if (!recommendData || recommendData.length == 0) {
                noRecommendData();
                return;
            }
            var movingList = recommendDiv.find(".posterList"), showingPage = 0;
            // GoNextPage
            if (goNext) {
                if (movingList.hasClass("moved")) {
                    movingList.css("transition", "-webkit-transform 0");
                    movingList.removeClass("moved");
                    movingList[0].offsetWidth;
                    // DataChange
                    setRollingPage(currentPage, nextPage);
                    movingList.css("transition", "-webkit-transform .5s");
                }
                movingList.addClass("moved");
                showingPage = 1;
            }
            // GoPrevPage
            else {
                if (!movingList.hasClass("moved")) {
                    movingList.css("transition", "-webkit-transform 0");
                    movingList.addClass("moved");
                    movingList[0].offsetWidth;
                    setRollingPage(nextPage, currentPage);
                    movingList.css("transition", "-webkit-transform .5s");
                }
                movingList.removeClass("moved");
                showingPage = 0;
            }
            for (var i = 0; i < 16; i++) {
                if (movingList.find(".content.poster_vertical")[i].id != "") movingList.find(".content.poster_vertical").eq(i).removeClass("hide");
            }
            recommendDiv.find(".recommendRightDim").removeClass("hide");
            recommendDiv.find(".recommendLeftDim").removeClass("hide");
            if (nextPage == maxPage) {
                movingList.find(".content.poster_vertical").eq(showingPage == 0 ? 8 : 15).addClass("hide");
                recommendDiv.find(".recommendRightDim").addClass("hide");
            } else if (nextPage == 0) {
                movingList.find(".content.poster_vertical").eq(showingPage == 0 ? 0 : 7).addClass("hide");
                recommendDiv.find(".recommendLeftDim").addClass("hide");
            }
        }

        function setPosterIcon(elem, data) {
            var posterIconCnt = 0;
            var posterIconArea = elem.find(".posterIconArea");
            posterIconArea.empty();
            if (data.HDR_YN == "Y" && CONSTANT.IS_HDR) {
                posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_hdr.png"}));
                posterIconCnt++;
            }
            if (data.IS_HD == "UHD") {
                posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_uhd.png"}));
                posterIconCnt++;
            }
            if (data.SMART_DVD_YN == "Y" && posterIconCnt < 2) {
                posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_mine.png"}));
            }

            var newHotIconArea = elem.find(".newHotIconArea");
            newHotIconArea.empty();
            if (data.NEW_HOT == "NEW") newHotIconArea.append(_$("<img>", {src: modulePath + "resource/icon_flag_new.png"}));
        }

        function getVerticalPoster(data, index) {
            var element = _$("<li class='content poster_vertical' id='" + (data.ITEM_ID + "_" + index) + "'>" +
                "<div class='content'>" +
                "<img src='" + data.IMG_URL + HTool.addPosterOption(210, 300, 90) + "' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/defaultPoster/default_poster.png\"'>" +
                "<div class='posterIconArea'/>" +
                "<div class='newHotIconArea'/>" +
                "<img class='posterLockImg'>" +
                "<div class='contentsData'>" +
                "<div class='posterTitle'>" +
                "<span>" + data.ITEM_NAME + "</span>" +
                "</div>" +
                "<span class='posterDetail'>" +
                "<span class='stars'/>" +
                "<span class='isfreeArea'>" +
                "<img class='isfreeIcon'>" +
                "<span class='isfree'>" + "무료" + "</span>" +
                "</span>" +
                "<img src='" + ROOT_URL + "resource/detail/icon_age_list_" + ((data.RATING - 0) > 0 ? (data.RATING - 0) : "all" ) + ".png' class='age'>" +
                "</span>" +
                "</div>" +
                "</div>" +
                "</li>");
            element.find(".stars").append(stars.getView(stars.TYPE.RED_20));
            stars.setRate(element.find(".stars"), data.OLLEH_RATING);
            setPosterIcon(element, data);
            if (!isAuthorizedContent && UTIL.isLimitAge(data.RATING)) element.find("img.posterLockImg").css("display", "inherit");
            else element.find("img.posterLockImg").css("display", "none");
            if (HTool.isTrue(data.WON_YN)) {
                element.find("img.isfreeIcon").css("display", "inherit");
                element.find("span.isfree").css("display", "none");
            } else if (HTool.isFalse(data.WON_YN)) {
                element.find("span.isfree").css("display", "inherit");
                element.find("span.isfree").text("무료");
                element.find("img.isfreeIcon").css("display", "none");
            } else {
                element.find("img.isfreeIcon").css("display", "none");
                element.find("span.isfree").css("display", "none");
            }
            return element;
        }

        function getEmptyPoster() {
            return _$("<li class='content poster_vertical hide' id=''><div class='content'><img src='' class='posterImg poster'><div class='posterIconArea'/><div class='newHotIconArea'/><img class='posterLockImg'><div class='contentsData'><div class='posterTitle'><span></span></div><span class='posterDetail'><span class='stars'/><span class='isfreeArea'><img class='isfreeIcon'><span class='isfree'/>><img src='' class='age'></span></div></div></li>");
        }

        function setRollingPageData(list, idx) {
            if (!recommendData || recommendData.length == 0) {
                noRecommendData();
                return;
            }
            list.removeClass("hide");
            list.attr("id", recommendData[idx].ITEM_ID + "_" + idx);
            list.find(".posterImg.poster").attr("src", recommendData[idx].IMG_URL + HTool.addPosterOption(210, 300, 90));
            list.find(".posterTitle span").text(recommendData[idx].ITEM_NAME);
            if (HTool.isTrue(recommendData[idx].WON_YN)) {
                list.find("img.isfreeIcon").css("display", "inherit");
                list.find("span.isfree").css("display", "none");
            } else if (HTool.isFalse(recommendData[idx].WON_YN)) {
                list.find("span.isfree").css("display", "inherit");
                list.find("span.isfree").text("무료");
                list.find("img.isfreeIcon").css("display", "none");
            } else {
                list.find("img.isfreeIcon").css("display", "none");
                list.find("span.isfree").css("display", "none");
            }
            list.find(".age").attr("src", ROOT_URL + "resource/detail/icon_age_list_" + ((recommendData[idx].RATING - 0) > 0 ? (recommendData[idx].RATING - 0) : "all" ) + ".png");
            list.find(".stars").empty();
            setPosterIcon(list, recommendData[idx]);
            if (!isAuthorizedContent && UTIL.isLimitAge(recommendData[idx].RATING)) list.find("img.posterLockImg").css("display", "inherit");
            else list.find("img.posterLockImg").css("display", "none");
            list.find(".stars").append(stars.getView(stars.TYPE.RED_20));
            stars.setRate(list.find(".stars"), recommendData[idx].OLLEH_RATING);
        }

        function makeRecommendList() {
            div.removeClass("noData");
            if (!recommendData || recommendData.length == 0) {
                noRecommendData();
                return;
            }
            var j, list;
            if (!recommendDiv.find(".posterList")[0]) {
                recommendDiv.append(_$(
                    "<div class='recommendPage'>" +
                    "<ul class='posterList'/>" +
                    (recommendData.length > 7 ? "<img class='recommendRightDim'/>" : "<img class='recommendRightDim hide'/>") +
                    "<img class='recommendLeftDim hide'/>" +
                    "</div>"
                ));
                list = recommendDiv.find(".posterList");
                list.append(getEmptyPoster());
                for (j = 0; j < 15; j++) {
                    if (recommendData[j]) list.append(getVerticalPoster(recommendData[j], j));
                    else list.append(getEmptyPoster());
                }
            } else {
                list = recommendDiv.find(".posterList");
                if (list.hasClass("moved")) {
                    list.css("transition", "-webkit-transform 0");
                    list.removeClass("moved");
                    list[0].offsetWidth;
                    list.css("transition", "-webkit-transform .5s");
                }
                setRollingPage(0, 1);
                recommendDiv.find(".recommendLeftDim").addClass("hide");
                if (recommendData.length > 7) recommendDiv.find(".recommendRightDim").removeClass("hide");
                else recommendDiv.find(".recommendRightDim").addClass("hide");
            }
        }

        function noRecommendData() {
            div.addClass("noData");
        }

        // 최초 생성될 때 1번
        this.init = function(cbCreate) {
            var i = 0;
            div = this.div;
            div.addClass("arrange_frame vod end_popup");
            // 타이틀 영역 구성
            div.append(_$("<span/>", {class: "title_area"}));
            div.append(_$("<span/>", {class: "description_area"}));
            div.append(_$("<span/>", {class: "name_area"}));
            // 버튼 영역 구성
            div.append(_$("<div/>", {class: "button_area"}));
            for (; i < 2; i++) {
                div.find(".button_area").append(_$("<div/>", {class: "button"}));
                div.find(".button_area .button").eq(i).append(_$("<span/>", {class: "buttonText"}).text(buttonText[i]));
            }
            // 별점 평가 영역 구성
            div.append(_$("<div/>", {class: "rating_area whiteStarBox"}));
            div.find(".rating_area").append(_$("<div/>", {class: "rating_box"}));
            div.find(".rating_area").append(_$("<div/>", {class: "star_area"}));
            div.find(".rating_area").append(_$("<div/>", {class: "arrow left"}));
            div.find(".rating_area").append(_$("<div/>", {class: "arrow right"}));
            div.find(".rating_area .star_area").append(stars.getView(stars.TYPE.YELLOW_36));
            stars.setRate(div.find(".rating_area .star_area"), 0);
            div.append(_$("<span/>", {class: "rating_text_area"}));
            // 함께 많이 본 영상 영역 구성
            div.append(_$("<div/>", {class: "recommend_area"}));
            div.find(".recommend_area").append(_$("<div/>", {class: "dim_bg"}));
            div.find(".recommend_area").append(_$("<div/>", {class: "text_area"}).text("함께 많이 본 영상"));
            div.find(".recommend_area").append(_$("<div/>", {class: "line_area left"}));
            div.find(".recommend_area").append(_$("<div/>", {class: "line_area right"}));

            div.find(".recommend_area").append(_$("<div/>", {class: "recommendArea"}));
            recommendDiv = div.find(".recommend_area .recommendArea");
            div.append(_$("<div/>", {class: "bottom_dim_area"}));
            cbCreate(true);
        };

        // 팝업이 호출 될 때마다 (화면에 보일 때)
        this.show = function() {
            closeFromEnterkey = false;
            curIdx = 0;
            curRow = 1;
            data = this.getParams();
            recommendData = data.recommendData;
            isAuthorizedContent = data.isAuthorizedContent;
            maxPage = Math.floor((recommendData.length - 1) / 7);
            makeRecommendList();
            setButtons();
            setButtonFocus();
            div.find(".title_area").text(endMode[data.mode]);
            div.find(".name_area").text("\'" + data.contsName + "\'");
            div.removeClass("continue");
            if (data.mode == 0) {
                getMyRating();
                div.find(".description_area").text(data.channelTune ? "종료된 후에는 입력하신 채널로 이동합니다" : "종료된 후에는 채널 olleh tv로 이동합니다");
            }
            else if (data.mode == 1) {
                countDownNumber = 11;
                div.find(".description_area").text("10 초 후 자동으로 시작됩니다");
                div.addClass("continue");
                autoContinue = true;
                countDown();
            }
            contsIdx = 0;
            Layer.prototype.show.call(this);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            // 2017.08.18 Lazuli
            // 이어보기 팝업이 노출된 상태에서 외부원인으로 인해 팝업이 닫히면 로딩 아이프레임에서 멈쳐있는 이슈가 있음
            // 버튼으로 닫힌것이 아니며, 이어보기 팝업이고, 취소 이벤트 리스너가 있을 경우 호출 해줌
            if (closeFromEnterkey != true && data.mode == 1 && data.cancelAction) data.cancelAction();
            Layer.prototype.hide.call(this);
        };

        function saveAndExit(idx) {
            closeFromEnterkey = true;
            LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
            if (rating != null && rating != orgRating && data.mode == 0 && idx == 0) saveRating();
            if (idx == 0 && data.okAction) data.okAction();
            else if (idx == 1 && data.cancelAction) data.cancelAction();
        }

        function changeFocus() {
            recommendDiv.find(".posterList li").removeClass("focus");
            if (curRow == 2) {
                recommendDiv.find(".posterList li#" + recommendData[contsIdx].ITEM_ID + "_" + contsIdx).addClass("focus");
                setTextAnimation(recommendDiv.find(".posterList"), contsIdx);
            } else {
                var list = recommendDiv.find(".posterList");
                if (list.hasClass("moved")) {
                    list.css("transition", "-webkit-transform 0");
                    list.removeClass("moved");
                    list[0].offsetWidth;
                    list.css("transition", "-webkit-transform .5s");
                }
                contsIdx = 0;
                setRollingPage(0, 1);
                recommendDiv.find(".recommendLeftDim").addClass("hide");
            }
        }

        function setTextAnimation(_div, focus) {
            UTIL.clearAnimation(_div.find(".textAnimating span"));
            _div.find(".textAnimating").removeClass("textAnimating");
            void _div[0].offsetWidth;
            if (UTIL.getTextLength(recommendData[focus].ITEM_NAME, "RixHead L", 30) > 215) {
                var posterDiv = _div.find("#" + recommendData[focus].ITEM_ID + "_" + focus).eq(0).find(".posterTitle");
                posterDiv.addClass("textAnimating");
                UTIL.startTextAnimation({
                    targetBox: posterDiv
                });
            }
        }

        this.controlKey = function(keyCode) {
            autoContinue = false;
            if (data.mode == 1) div.find(".description_area").hide();
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (curRow == 0) {
                        // 별점 감소
                        rating -= 0.5;
                        if (rating < 0) rating = 0;
                        setRating(rating);
                    } else if (curRow == 1) {
                        // 버튼 포커스 이동
                        curIdx = HTool.indexMinus(curIdx, btnCnt);
                        setButtonFocus();
                    } else if (curRow == 2) {
                        // 함많본 포커스 이동
                        curPage = Math.floor(contsIdx / 7);
                        if (contsIdx % 7 == 0 && recommendData.length > 7) {
                            if (curPage == 0) moveRollingPage(false, curPage, maxPage);
                            else moveRollingPage(false, curPage, curPage - 1);
                        }
                        contsIdx = HTool.indexMinus(contsIdx, recommendData.length);
                        changeFocus();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (curRow == 0) {
                        // 별점 증가
                        rating += 0.5;
                        if (rating > 5) rating = 5;
                        setRating(rating);
                    } else if (curRow == 1) {
                        // 버튼 포커스 이동
                        curIdx = HTool.indexPlus(curIdx, btnCnt);
                        setButtonFocus();
                    } else if (curRow == 2) {
                        // 함많본 포커스 이동
                        curPage = Math.floor(contsIdx / 7);
                        if (((contsIdx + 1) % 7 == 0 || contsIdx == recommendData.length - 1) && recommendData.length > 7) {
                            if (curPage == maxPage) moveRollingPage(true, curPage, 0);
                            else moveRollingPage(true, curPage, curPage + 1);
                        }
                        contsIdx = HTool.indexPlus(contsIdx, recommendData.length);
                        changeFocus();
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (curRow == 0) {
                        curRow = 1;
                        curIdx = 0;
                        setRowFocus();
                    } else if (curRow == 1) {
                        saveAndExit(curIdx);
                    } else if (curRow == 2) {
                        closeFromEnterkey = true;
                        // 이어보기 팝업일 경우 함많본 선택하면 VOD 종료하고 상세화면으로 이동함
                        // 이 때 TV_VIEWING 으로 상태가 변하지 않도록 요청해야 함
                        if (data.mode == 1) VODManager.exitVOD(null, null, true);
                        VODImpl.showDetail({
                            callback: function(success) {log.printDbg(success + " ShowDetail Complete");},
                            cat_id: recommendData[contsIdx].PARENT_CAT_ID || recommendData[contsIdx].ITEM_ID,
                            const_id: recommendData[contsIdx].ITEM_ID,
                            req_cd: "22",
                            isKids: false,
                            isAuthorizedContent: isAuthorizedContent
                        });
                        try {
                            NavLogMgr.collectJumpVOD(NLC.JUMP_START_RELATED_CONTENTS,
                                recommendData[contsIdx].PARENT_CAT_ID || recommendData[contsIdx].ITEM_ID,
                                recommendData[contsIdx].ITEM_ID
                            );
                        } catch (e) {
                        }
                    }
                    return true;
                case KEY_CODE.UP:
                    if (curRow == 1 && data.mode == 1) return true;
                    curRow--;
                    if (curRow < 0) curRow = 0;
                    setRowFocus();
                    return true;
                case KEY_CODE.DOWN:
                    if (curRow == 1 && (!recommendData || recommendData.length == 0)) return true;
                    curRow++;
                    if (curRow > 2) curRow = 2;
                    setRowFocus();
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    closeFromEnterkey = true;
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    if (data.cancelAction) data.cancelAction();
                    return true;
                case KEY_CODE.CH_UP:
                case KEY_CODE.CH_DOWN:
                case KEY_CODE.BLUE:
                case KEY_CODE.RED:
                    return true;
                default:
                    return (data.mode == 1);
            }
        };
    };

    vod_end_popup.prototype = new Layer();
    vod_end_popup.prototype.constructor = vod_end_popup;

    vod_end_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_end_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodEndPopup"] = vod_end_popup;
})();