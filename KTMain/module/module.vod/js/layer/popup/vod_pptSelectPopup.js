/**
 * 기간정액 상품 선택 팝업
 * Created by kh.kim2 on 2018-02-13.
 */


(function () {
    var vod_ppt_select_popup = function VodPptSelectPopup(options) {
        Layer.call(this, options);
        var data;
        var focus, page;
        var btnFocus = false;
        var div;
        var callback;
        var callbackConsumed = false;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod month_flatRate_popup");
            div.append(_$("<span/>", {class:"title_area"}).text("기간정액 선택"));
            div.append(_$("<span/>", {class:"contents_title"}));
            div.append(_$("<span/>", {class:"description_area"}).text("구매하시려는 기간정액을 선택하세요"));
            div.append(_$("<div/>", {class:"left_arrow"}));
            div.append(_$("<div/>", {class:"right_arrow"}));
            div.append(_$("<div/>", {class:"item_area"}));
            div.append(_$("<div/>", {class:"btn_area"}).append("<div class='btn cancel'>취소</div>"));
            setItemArea(div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            for(var i=0; i<3; i++) {
                var a = _$("<div/>", {class:"item"});
                a.append(_$("<span/>", {class:"productName2"}));
                a.append(_$("<div/>", {class:"priceLabel2"}));
                div.append(a);
            }
        }

        this.show = function() {
            data = this.getParams();
            callback = data.callback;
            data.list = filterPptData(data.list);
            div.find(".contents_title").text(data.list[0].pptTitle);
            setPage(0);
            setFocus(0);

            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(false);
        };

        function filterPptData(data) {
            var firstPpt = null;
            var unifiedPpt = null;
            var returnList = [];
            for(var i=0; i<data.length; i++) {
                if (data[i].unifiedPptYn != "Y") {
                    if (!firstPpt) {
                        firstPpt = data[i];
                        break;
                    }
                }
            }for(var i=0; i<data.length; i++) {
                if (data[i].unifiedPptYn == "Y") {
                    if (!unifiedPpt) {
                        unifiedPpt = data[i];
                        break;
                    }
                }
            }

            if(firstPpt) returnList.push(firstPpt);
            if(unifiedPpt) returnList.push(unifiedPpt);
            return returnList;
        }

        function setPage(_page) {
            page = _page;
            setItemData(data, div.find(".item_area"));
        }

        function setFocus(_focus) {
            focus = _focus;
            div.find(".item").removeClass("focus").eq(_focus).addClass("focus");
            div.find(".contents_title").text(data.list[focus].pptTitle);
        }

        function setBtnFocus(isFocus) {
            btnFocus = isFocus;
            div.find(".item").eq(focus).toggleClass("focus", !btnFocus);
            div.find(".btn_area .btn").toggleClass("focus", btnFocus);
        }

        function setItemData(data, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");
            if(page==0) itemArea.find(".item").hide();
            else itemArea.find(".item").css("visibility", "hidden");
            var list = data.list;
            for(var i=0, j=page*4; i<4 && j<list.length; i++,j++) {
                var item = itemArea.find(".item").eq(i);
                item.find(".productName2").text(list[j].pptName);
                item.css("visibility", "inherit");
                if (list[j].vatPptPrice.split("|").length > 1) {
                    item.find(".priceLabel2").text(numberWithCommas("최저 " + list[j].vatPptPrice.split("|")[0]) + "원");
                } else {
                    item.find(".priceLabel2").text(numberWithCommas(getPeriodText(list[j].pptPeriod) + list[j].vatPptPrice.split("|")[0]) + "원");
                }
                item.show();
            }
            div.find(".left_arrow").toggle(page>0);
            div.find(".right_arrow").toggle((page+1)*4<list.length);
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function callCallback(res, data) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                callback(res, data);
            }
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(btnFocus) return true;
                    if(page*4+focus<=0) return true;
                    if(focus-1<0) { setPage(page-1); setFocus(3); }
                    else setFocus(focus-1);
                    return true;
                case KEY_CODE.RIGHT:
                    if(btnFocus) return true;
                    if(page*4+focus>=data.list.length-1) return true;
                    if(focus+1>4) { setPage(page+1); setFocus(0); }
                    else setFocus(focus+1);
                    return true;
                case KEY_CODE.UP:
                    if(btnFocus) setBtnFocus(false);
                    return true;
                case KEY_CODE.DOWN:
                    if(!btnFocus) setBtnFocus(true);
                    return true;
                case KEY_CODE.ENTER:
                    var selectedData = data.list[page*4+focus];
                    selectedData["reqPathCd"] = data.reqPathCd;
                    selectedData["catId"] = data.catId;
                    selectedData["contsId"] = data.contsId;
                    if(!btnFocus) callCallback(true, selectedData);
                    else callCallback(false);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    callCallback(false);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };
    };

    function getPeriodText(period) {
        // ppt 기간은 시간 단위로 내려온다
        // 일 단위로 변환하여 표시
        var period_day = Math.floor(period / 24);
        switch(period_day) {
            case 0:
                return period + "시간 ";
            default:
                return period_day + "일 ";
        }
    }

    vod_ppt_select_popup.prototype = new Layer();
    vod_ppt_select_popup.prototype.constructor = vod_ppt_select_popup;

    vod_ppt_select_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_ppt_select_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodPptSelectPopup"] = vod_ppt_select_popup;
})();