/**
 * Created by ksk91_000 on 2017-06-01.
 */
(function() {
    var vod_select_package_detail_popup = function VODSelectPackageDetailPopupLayer(options) {
        Layer.call(this, options);
        var seriesData, pkgList;
        var list;
        var focus = 0, page = 0;
        var div;
        var currentIdx;
        var callback;
        var btnFocus = false;
        var maxPage = 0;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod select_package_popup detail_select_package_popup");
            div.append(_$("<span/>", {class: "title_area"}));
            div.append(_$("<span/>", {class: "description_area"}));
            div.append(_$("<div/>", {class: "left_arrow"}));
            div.append(_$("<div/>", {class: "right_arrow"}));
            div.append(_$("<div/>", {class: "item_area"}));
            div.append(_$("<span/>", {class: "content_title_area"}));
            div.append(_$("<span/>", {class: "content_price_area"}));
            div.append(_$("<span/>", {class: "btn_area"}).append("<div class='btn'>취소</div>"));
            setItemArea(div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            for (var i = 0; i < 5; i++) {
                var a = _$("<div/>", {class: "item"});
                a.append(_$("<img>"));
                div.append(a);
            }
        }

        this.show = function() {
            var params = this.getParams();
            if(params) {
                seriesData = params.seriesData;
                pkgList = params.pkgList;
                callback = params.callback;
                seriesData.buyYn = !!params.seriesBuyYn;
                list = makeItemList(seriesData, pkgList);

                div.find(".title_area").text(seriesData ? "시리즈/패키지 선택" : "패키지 선택");
                div.find(".description_area").text(seriesData ? "시리즈/패키지를 선택하세요" : "패키지를 선택하세요");
                maxPage = Math.floor((list.length - 1) / 5);
                setItemData(list, div.find(".item_area"));

                Layer.prototype.show.call(this);
            } else {
                Layer.prototype.show.call(this);
                LayerManager.deactivateLayer({id: this.id});
            }
        };

        function makeItemList(seriesData, pkgList) {
            var itemList = [];
            if(seriesData) {
                itemList.push({
                    name: seriesData.name,
                    imgUrl: seriesData.imgUrl,
                    price: seriesData.vatPrice,
                    data: seriesData,
                    buyYn: seriesData.buyYn,
                    isSeries: true
                });
            } for(var i=0; i<pkgList.length; i++) {
                itemList.push({
                    name: pkgList[i].name,
                    imgUrl: pkgList[i].imgUrl,
                    price: pkgList[i].vatPrice,
                    data: pkgList[i],
                    buyYn: pkgList[i].buyYn,
                    isSeries: false
                });
            } return itemList;
        }

        function setItemData(list, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");
            itemArea.find(".item").css("visibility", "hidden");
            for (var i = 0; i < 5; i++) {
                if (page*5 + i < 0) continue;
                if (page*5 + i >= list.length){
                    if(page == 0){
                        itemArea.find(".item").eq(i).hide();
                        continue;
                    }else break;
                }

                var item = itemArea.find(".item").eq(i);
                item.find("img").attr("src", list[page*5 + i].imgUrl);
                if (i == focus) {
                    item.addClass("focus");
                    div.find(".content_title_area").text(list[page*5 + i].name);
                    div.find(".content_price_area").html("<span>" + HTool.addComma(list[page*5 + i].vatPrice) + "원</span>");
                    div.find(".content_price_area").toggleClass("bought", !!list[page*5 + i].buyYn);
                }
                item.css("visibility", "inherit");
                item.toggleClass("current", page*5 + i == currentIdx);
            }
            div.find(".left_arrow").toggle(page*5 > 0);
            div.find(".right_arrow").toggle(page*5 + 5 < list.length);
        }


        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(!btnFocus){
                        if (focus > 0) {
                            focus--;
                        }else if(page == 0 && focus == 0){
                            page = maxPage;
                            focus = (list.length-1) % 5;
                        }else if(focus == 0){
                            page--;
                            focus = 4;
                        }
                        setItemData(list, this.div.find(".item_area"));
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(!btnFocus){
                        if (focus < 4 && list.length-1 > focus + (page * 5)) {
                            focus++;
                        }else if(page == maxPage && focus == (list.length-1) % 5){
                            page = 0;
                            focus = 0;
                        }else if(focus == 4){
                            page++;
                            focus = 0;
                        }
                        setItemData(list, this.div.find(".item_area"));
                    }
                    return true;
                case KEY_CODE.UP:
                case KEY_CODE.DOWN:
                    if(btnFocus) {
                        div.find(".btn_area .focus").removeClass("focus");
                        setItemData(list, div.find(".item_area"));
                        btnFocus = false;
                    } else {
                        div.find(".item_area .focus").removeClass("focus");
                        div.find(".btn_area .btn").addClass("focus");
                        btnFocus = true;
                    } return true;
                case KEY_CODE.ENTER:
                    if(btnFocus) LayerManager.historyBack();
                    else callback(list[page*5 + focus]);
                    return true;
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    vod_select_package_detail_popup.prototype = new Layer();
    vod_select_package_detail_popup.prototype.constructor = vod_select_package_detail_popup;

    //Override create function
    vod_select_package_detail_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_select_package_detail_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VODSelectPackageDetailPopupLayer"] = vod_select_package_detail_popup;
})();