/**
 * 월정액 선택 팝업
 * Created by Yun on 2017-04-05.
 */

(function () {
    var vod_month_flatRate_popup = function VodMonthFlatRatePopup(options) {
        Layer.call(this, options);
        var data;
        var focus, page;
        var btnFocus = false;
        var div;
        var callback;
        var firstCorner, unifiedCorner;
        var callbackConsumed = false;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod month_flatRate_popup");
            div.append(_$("<span/>", {class:"title_area"}).text("월정액 선택"));
            div.append(_$("<span/>", {class:"contents_title"}));
            div.append(_$("<span/>", {class:"description_area"}).text("구매하시려는 월정액을 선택하세요"));
            div.append(_$("<div/>", {class:"left_arrow"}));
            div.append(_$("<div/>", {class:"right_arrow"}));
            div.append(_$("<div/>", {class:"item_area"}));
            div.append(_$("<div/>", {class:"btn_area"}).append("<div class='btn cancel'>취소</div>"));
            setItemArea(div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            for(var i=0; i<3; i++) {
                var a = _$("<div/>", {class:"item"});
                a.append(_$("<span/>", {class:"productName"}));
                a.append(_$("<div/>", {class:"priceLabel"}));
                div.append(a);
            }
        }

        this.show = function() {
            data = this.getParams();
            callback = data.callback;
            div.find(".contents_title").text(data.title);
            data.list = filterCornerData(data.list);
            setPage(0);
            setFocus(0);

            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(false);
        };

        function filterCornerData(data) {
            var firstCorner = null;
            var unifiedCorner = null;
            var returnList = [];
            for(var i=0; i<data.length; i++) {
                if (data[i].unifiedCornerYn != "Y") {
                    if (!firstCorner) {
                        firstCorner = data[i];
                        if (data[i].firstCornerYn == "Y") break;
                    } else if (data[i].firstCornerYn == "Y") {
                        firstCorner = data[i];
                        break;
                    }
                }
            }for(var i=0; i<data.length; i++) {
                if (data[i].unifiedCornerYn == "Y") {
                    if (!unifiedCorner) {
                        unifiedCorner = data[i];
                        if (data[i].firstCornerYn == "Y") break;
                    } else if (data[i].firstCornerYn == "Y") {
                        unifiedCorner = data[i];
                        break;
                    }
                }
            } if(firstCorner) returnList.push(firstCorner);
            if(unifiedCorner) returnList.push(unifiedCorner);
            return returnList;
        }

        function setPage(_page) {
            page = _page;
            setItemData(data, div.find(".item_area"));
        }

        function setFocus(_focus) {
            focus = _focus;
            div.find(".item").removeClass("focus").eq(_focus).addClass("focus");
        }

        function setBtnFocus(isFocus) {
            btnFocus = isFocus;
            div.find(".item").eq(focus).toggleClass("focus", !btnFocus);
            div.find(".btn_area .btn").toggleClass("focus", btnFocus);
        }

        function setItemData(data, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");
            if(page==0) itemArea.find(".item").hide();
            else itemArea.find(".item").css("visibility", "hidden");
            var list = data.list;
            for(var i=0, j=page*4; i<4 && j<list.length; i++,j++) {
                var item = itemArea.find(".item").eq(i);
                item.find(".productName").text(list[j].productName);
                item.css("visibility", "inherit");
                item.find(".priceLabel").text("월 " + numberWithCommas(list[j].vatProductPrice) + "원");
                item.show();
            }
            div.find(".left_arrow").toggle(page>0);
            div.find(".right_arrow").toggle((page+1)*4<list.length);
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function callCallback(res, data) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                callback(res, data);
            }
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(btnFocus) return true;
                    if(page*4+focus<=0) return true;
                    if(focus-1<0) { setPage(page-1); setFocus(3); }
                    else setFocus(focus-1);
                    return true;
                case KEY_CODE.RIGHT:
                    if(btnFocus) return true;
                    if(page*4+focus>=data.list.length-1) return true;
                    if(focus+1>4) { setPage(page+1); setFocus(0); }
                    else setFocus(focus+1);
                    return true;
                case KEY_CODE.UP:
                    if(btnFocus) setBtnFocus(false);
                    return true;
                case KEY_CODE.DOWN:
                    if(!btnFocus) setBtnFocus(true);
                    return true;
                case KEY_CODE.ENTER:
                    if(!btnFocus) callCallback(true, data.list[page*4+focus]);
                    else callCallback(false);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    callCallback(false);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };
    };

    vod_month_flatRate_popup.prototype = new Layer();
    vod_month_flatRate_popup.prototype.constructor = vod_month_flatRate_popup;

    vod_month_flatRate_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_month_flatRate_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodMonthFlatRatePopup"] = vod_month_flatRate_popup;
})();