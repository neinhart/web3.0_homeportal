/**
 * Created by ksk91_000 on 2016-10-24.
 */
(function() {
    var vod_select_corner_popup = function VODSelectCornerPopupLayer(options) {
        Layer.call(this, options);
        var data;
        var focus, page;
        var type;
        var div;
        var currentIdx;
        var focusedThumbnail;
        var fromContextMenu = false;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod select_corner_popup");
            div.append(_$("<span/>", {class: "pop_title"}));
            div.append(_$("<span/>", {class: "title_area"}));
            div.append(_$("<span/>", {class: "description_area"}));
            div.append(_$("<div/>", {class: "left_arrow"}));
            div.append(_$("<div/>", {class: "right_arrow"}));
            div.append(_$("<div/>", {class: "item_area"}));

            data = this.getParams().cornerGrpList;
            focusedThumbnail = this.getParams().focusedThumbnail;
            fromContextMenu = this.getParams().fromContextMenu;
            type = data.corner_grp_list[0].cornergrptype;
            if (type == "002") {
                div.find(".title_area").text("\'" + data.corner_grp_list[0].cornergrpnm + "\'");
                div.find(".description_area").text("선택한 회차부터 최신 회차까지 자동 재생됩니다");
                div.find(".pop_title").text("회차 선택");
            } else if (type == "001") {
                div.find(".title_area").text("\'" + data.corner_grp_list[0].cornergrpnm + "\' 골라보기");
                div.find(".description_area").text("선택한 구간부터 연속 재생됩니다");
                div.find(".pop_title").text("");
            }
            currentIdx = getCurVodIdx();
            page = Math.floor(currentIdx / 5);
            focus = currentIdx % 5;
            setItemArea(div.find(".item_area"));
            setItemData(data, div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            for (var i = 0; i < 5; i++) {
                var a = _$("<div/>", {class: "item"});
                a.append(_$("<img>"));
                a.append(_$("<span/>"));
                a.append(_$("<div/>", {class: "currentIcon"}));
                div.append(a);
            }
        }

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        function getCurVodIdx() {
            for (var i = 0; i < data.items.length; i++)
                if (data.items[i].content_id == focusedThumbnail.content_id && data.items[i].file_id == focusedThumbnail.file_id) return i
        }

        function setItemData(data, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");
            itemArea.find(".item").css("visibility", "hidden");
            var list = data.items;
            for (var i = 0; i < 5; i++) {
                if (page * 5 + i < 0) continue;
                if (page * 5 + i >= list.length) {
                    if (page == 0) {
                        itemArea.find(".item").eq(i).hide();
                        continue;
                    } else break;
                }

                var item = itemArea.find(".item").eq(i).show();
                item.find("img").attr({"src": list[page * 5 + i].file_url, "onerror": "this.src='" + ROOT_URL + "resource/default_thumb_sm.jpg'"});
                item.find("span").text(type === "002" ? list[page * 5 + i].cornername : list[page * 5 + i].cornerstart.split(".")[0]);
                if (i == focus) item.addClass("focus");
                item.css("visibility", "inherit");
                item.toggleClass("current", page * 5 + i == currentIdx);
            }
            div.find(".left_arrow").toggle(page * 5 > 0);
            div.find(".right_arrow").toggle(page * 5 + 5 < list.length);
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (page * 5 + focus <= 0) return true;
                    focus--;
                    if (focus < 0) {
                        page--;
                        focus = 4;
                    }
                    setItemData(data, this.div.find(".item_area"));
                    return true;
                case KEY_CODE.RIGHT:
                    if (page * 5 + focus >= data.items.length - 1) return true;
                    focus++;
                    if (focus > 4) {
                        page++;
                        focus = 0;
                    }
                    setItemData(data, this.div.find(".item_area"));
                    return true;
                case KEY_CODE.ENTER:
                    if (fromContextMenu) VODInfoManager.setCurrentCornerForContext();
                    VODManager.requestPlayCorner(page * 5 + focus, function() {
                        LayerManager.historyBack();
                        if (VODInfoManager.getCurrentCornerVod().content_id == VODManager.getCurrentVODInfo().assetId) {
                            MiniGuideManager.getMiniguide("Corner").setCornerData(data);
                            MiniGuideManager.changeMode("Corner");
                            MiniGuideManager.show();
                        }
                    });
                    return true;
                case KEY_CODE.DOWN:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    if (!fromContextMenu) MiniGuideManager.show(false);
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    vod_select_corner_popup.prototype = new Layer();
    vod_select_corner_popup.prototype.constructor = vod_select_corner_popup;

    //Override create function
    vod_select_corner_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_select_corner_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodSelectCornerPopup"] = vod_select_corner_popup;
})();