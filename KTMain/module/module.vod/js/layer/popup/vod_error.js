/**
 * Created by Taesang on 2017-01-20.
 */
(function() {
    var vod_error_popup = function vodErrorPopup(options) {
        Layer.call(this, options);
        var _this = this, div, errorData, buttonLength = 0, curIdx = 0;
        var callbackConsumed = false;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame vod vod_error");
            this.div.append(_$("<div/>", {class: "errorPopup"}));
            div = this.div.find(".errorPopup");
            div.append(_$("<span/>", {class: "errorTitle"}));
            div.append(_$("<span/>", {class: "errorText"}));
            div.append(_$("<div/>", {class: "button_area"}));
            if (!this.getParams().noShowSAID) this.div.append(_$("<span/>", {class: "errorSAID"}).text("SAID : ****" + DEF.SAID.substring(4)));
            cbCreate(true);
        };

        function makeButton() {
            buttonLength = 0;
            div.find(".button_area").empty();
            if (errorData.button) {
                buttonLength++;
                div.find(".button_area").append(_$("<div class='button'><div class='buttonText'>" + errorData.button + "</div></div>"));
            }
            if (errorData.reboot) {
                buttonLength++;
                div.find(".button_area").append(_$("<div class='button'><div class='buttonText'>재부팅</div></div>"));
            }
        }

        function makePopup() {
            var errMsg = "";
            var errorLength = errorData.message.length;
            for (var i = 0; i < errorLength; i++) {
                if (i == 0) errMsg += errorData.message[i];
                else errMsg += "<br>" + errorData.message[i];
            }
            div.find(".errorTitle").text(errorData.title);
            div.find(".errorText").html(errMsg);
        }

        function setFocus(_idx) {
            if (buttonLength > 0) {
                div.find(".button").removeClass("focus");
                div.find(".button").eq(_idx != null ? _idx : curIdx).addClass("focus");
            }
        }

        this.show = function() {
            Layer.prototype.show.call(this);
            errorData = _this.getParams();
            makePopup();
            makeButton();
            setFocus(0);
        };

        function callCallback() {
            if(!callbackConsumed) {
                callbackConsumed = true;
                if (errorData.callback) errorData.callback();
            }
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    setFocus(curIdx = HTool.indexMinus(curIdx, buttonLength));
                    return true;
                case KEY_CODE.RIGHT:
                    setFocus(curIdx = HTool.indexPlus(curIdx, buttonLength));
                    return true;
                case KEY_CODE.ENTER:
                    if (errorData.reboot && curIdx == 1) {
                        callbackConsumed = true;
                        stbBasic.reboot();
                    } else {
                        LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    }
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.apply(this, arguments);
            callCallback();
        };
    };

    vod_error_popup.prototype = new Layer();
    vod_error_popup.prototype.constructor = vod_error_popup;

    vod_error_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_error_popup.prototype.handleKeyEvent = function(key_code) {
        this.controlKey(key_code);
        return true;
    };

    arrLayer["VODError"] = vod_error_popup;
})();