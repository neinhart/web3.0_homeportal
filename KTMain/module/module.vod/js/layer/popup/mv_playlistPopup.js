/**
 * Created by Lazuli on 2017-06-28.
 */
/**
 * Created by Taesang on 2017-01-20.
 */
(function() {
    var mv_playList_popup = function mvPlayListPopup(options) {
        Layer.call(this, options);
        var _this = this, div, mvData, curIdx, curRow, totalPage, curPage, playList;

        function getPlayList(cbCreate) {
            LayerManager.startLoading();
            VODAmocManager.getMyPlayItemList(function(res, resData) {
                LayerManager.stopLoading();
                if (res === true && resData.playItemList != null) {
                    playList = resData.playItemList;
                    if (playList == null) playList = [];
                    if (!Array.isArray(playList)) playList = [playList];
                    totalPage = Math.floor((playList.length - 1) / 3) + 1;
                    if (totalPage > 1) div.find(".playList_area .pageText").text("1/" + totalPage);
                    else div.find(".playList_area .pageText").hide();
                    setPage(0);
                    cbCreate(true)
                } else cbCreate(false);
            });
        }

        function setPage(page) {
            curPage = page;
            for (var i = 0; i < 3; i++) {
                var listItem = playList[(page * 3) + i];
                var listBox = div.find(".playList_area .listBox").eq(i);
                if (listItem) {
                    listBox.show();
                    listBox.find(".listName").text(listItem.playListNm);
                    listBox.find(".listItemCount").text("(" + listItem.itemCnt + ")");
                    listBox.toggleClass("noItem", (listItem.itemCnt == null || listItem.itemCnt == 0));
                } else listBox.hide();
                listItem = null;
                listBox = null;
            }
            // 페이지에 따른 UI 셋팅
            if (totalPage > 1) {
                div.find(".arrow.left").toggle(curPage != 0);
                div.find(".arrow.right").toggle(curPage < totalPage - 1);
                div.find(".playList_area .pageText").text((curPage + 1) + "/" + totalPage);
            } else {
                div.find(".arrow").hide();
                div.find(".playList_area .pageText").hide();
            }
        }

        function setFocus(idx) {
            if (idx != null) curIdx = idx;
            div.find(".playList_area").removeClass("focus");
            div.find(".playList_area .listBox").removeClass("focus");
            div.find(".button_area .button").removeClass("focus");
            if (curRow == 0) {
                div.find(".playList_area .listBox").eq(curIdx % 3).addClass("focus");
                div.find(".playList_area").addClass("focus");
            }
            else div.find(".button_area .button").addClass("focus");
            if (Math.floor(curIdx / 3) != curPage) setPage(Math.floor(curIdx / 3));
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod mv_playList");
            div.append(_$("<span/>", {class: "popupTitle"}).text("마이 플레이리스트 추가"));
            div.append(_$("<span/>", {class: "mvTitle"}));
            div.append(_$("<div/>", {class: "playList_area"}));
            div.find(".playList_area").append(_$("<span/>", {class: "guideText"}).text("추가하려는 플레이리스트를 선택해 주세요"));
            div.find(".playList_area").append(_$("<span/>", {class: "pageText"}));
            div.find(".playList_area").append(_$("<div/>", {class: "playList"}));
            for (var i = 0; i < 3; i++) {
                div.find(".playList_area .playList").append(_$("<div class='listBox'><span class='listName'></span><span class='listItemCount'></span></div>"));
            }
            div.append(_$("<img/>", {class: "arrow left"}));
            div.append(_$("<img/>", {class: "arrow right"}));
            div.append(_$("<div/>", {class: "button_area"}));
            div.find(".button_area").append(_$("<div class='button'><div class='buttonText'>취소</div></div>"));
            getPlayList(cbCreate);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            mvData = _this.getParams();
            div.find(".mvTitle").text("\'" + (mvData.contsName || mvData.itemName) + "\'");
            setFocus(curRow = curIdx = 0);

            // 2017.08.11 Lazuli
            // 우리집 맞춤 TV 모듈이 로드되어 있을 때 플레이리스트 캐시 초기화
            var familyModule = ModuleManager.getModule("module.family_home");
            if (familyModule) familyModule.execute({method: "clearPlayListCache"});
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    curRow = 0;
                    setFocus();
                    return true;
                case KEY_CODE.DOWN:
                    curRow = 1;
                    setFocus();
                    return true;
                case KEY_CODE.LEFT:
                    if (curRow == 0) setFocus(curIdx = HTool.indexMinus(curIdx, playList.length));
                    return true;
                case KEY_CODE.RIGHT:
                    if (curRow == 0) setFocus(curIdx = HTool.indexPlus(curIdx, playList.length));
                    return true;
                case KEY_CODE.ENTER:
                    if (curRow == 0) {
                        VODAmocManager.setMyPlayItemDetList(function(res, data) {
                            if (res) {
                                if (data.resCode == 0) showToast("'" + playList[curIdx].playListNm + "'에 추가 하였습니다");
                                else if (data.resCode == 1) showToast("개수 초과! 마이 플레이리스트 콘텐츠 등록은 최대 20개까지 가능합니다");
                            }
                            LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                        }, 0, playList[curIdx].playListId, 1, mvData.contsId, "", mvData.itemType, null);
                    } else LayerManager.deactivateLayer({id: _this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;

            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };
    };

    mv_playList_popup.prototype = new Layer();
    mv_playList_popup.prototype.constructor = mv_playList_popup;

    mv_playList_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    mv_playList_popup.prototype.handleKeyEvent = function(key_code) {
        this.controlKey(key_code);
        return true;
    };

    arrLayer["MvPlayList"] = mv_playList_popup;
})();