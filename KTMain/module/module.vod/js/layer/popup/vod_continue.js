/**
 * Created by Taesang on 2016-11-02.
 */
(function() {
    var vod_continue_popup = function vodContinueLayer(options) {
        Layer.call(this, options);
        var div;
        var data;
        var progressWidth = 0;
        var curIdx = 0, curRow = 0;
        var ageIcon = [
            "module/module.vod/resource/icon_age_txtlist_all.png",
            "module/module.vod/resource/icon_age_txtlist_7.png",
            "module/module.vod/resource/icon_age_txtlist_12.png",
            "module/module.vod/resource/icon_age_txtlist_15.png",
            "module/module.vod/resource/icon_age_txtlist_19.png",
            "module/module.vod/resource/icon_age_txtlist_19.png"
        ];
        var buttonText = ["이어보기", "처음부터", "취소"];
        var hasOTMLinkTime = false;
        var onlyOTMLinkTime = false;
        var bookmarkLinkTime = "0";
        var callbackConsumed = false;

        function makeTimeStr(t) {
            var a = HTool.msecToTime(t);
            return a.hour + ":" + a.min + ":" + a.sec;
        }

        function setFocus() {
            div.find(".button_area .button").removeClass("focus");
            div.find(".OTM_Button").removeClass("focus");
            if (onlyOTMLinkTime) {
                if (curRow == 0) div.find(".OTM_Button").addClass("focus");
                else div.find(".button_area .button").eq(curIdx).addClass("focus");
            } else {
                if (curRow == 0) div.find(".button_area .button").eq(curIdx).addClass("focus");
                else div.find(".OTM_Button").addClass("focus");
            }
            setProgress();
        }

        function setProgress() {
            var currentTime;
            if (onlyOTMLinkTime) {
                if (curRow == 0) currentTime = data.otmLinkTime;
                else currentTime = bookmarkLinkTime;
            } else {
                if (curRow == 0) currentTime = bookmarkLinkTime;
                else currentTime = data.otmLinkTime;
            }
            progressWidth = Math.min((currentTime / ((data.runtime * 60) + 59)) * 495, 495);
            div.find(".currentTime_area").text(makeTimeStr(currentTime * 1E3));
            div.find(".progress_bar").css("width", progressWidth + "px");
        }

        function setResolCd() {
            if (data.resolCd == "SD") div.find(".resol_icon").attr("display", "none");
            else {
                div.find(".resol_icon").attr("display", "inherit");
                if (data.resolCd == "HD") div.find(".resol_icon").attr("src", "module/module.vod/resource/icon_tag_hd.png");
                if (data.resolCd == "FHD") div.find(".resol_icon").attr("src", "module/module.vod/resource/icon_tag_fullhd.png");
                if (data.resolCd == "UHD") div.find(".resol_icon").attr("src", "module/module.vod/resource/icon_tag_uhd.png");
            }
        }

        function setInfoArea() {
            div.find(".name_area").text("'" + data.contsName + "'");
            div.find(".playtime_area").text("상영시간 " + data.runtime + "분");
            // 실제상영시간이 runtime 보다 긴 경우가 있어서 의도적으로 59초 추가
            div.find(".totalTime_area").text(makeTimeStr(data.runtime * 60 * 1E3 + 59000));
            div.find(".age_icon").attr("src", ageIcon[parseInt(data.prInfo) - 1]);
            div.find(".description_area").text(!!data.bookmarkTime ? "북마크 장면부터 이어서 보시겠습니까?" : "이전 장면부터 이어서 보시겠습니까?");
        }

        // 최초 생성될 때 1번
        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod continue_popup");
            div.append(_$("<div/>", {class: "continueInfoArea"}));
            div.find(".continueInfoArea").append(_$("<span/>", {class: "title_area"}).text("이어보기"));
            div.find(".continueInfoArea").append(_$("<span/>", {class: "description_area"}).text("이전 장면부터 이어서 보시겠습니까?"));
            div.find(".continueInfoArea").append(_$("<span/>", {class: "name_area"}));
            div.find(".continueInfoArea").append(_$("<span/>", {class: "playtime_area"}));
            div.find(".continueInfoArea").append(_$("<span/>", {class: "currentTime_area"}));
            div.find(".continueInfoArea").append(_$("<span/>", {class: "totalTime_area"}));
            div.find(".continueInfoArea").append(_$("<img>", {class: "age_icon"}));
            div.find(".continueInfoArea").append(_$("<img>", {class: "resol_icon"}));
            div.find(".continueInfoArea").append(_$("<div/>", {class: "progress_background"}));
            div.find(".continueInfoArea").append(_$("<div/>", {class: "progress_bar"}));
            div.append(_$("<div/>", {class: "button_area"}));
            for (var i = 0; i < 3; i++) {
                div.find(".button_area").append(_$("<div/>", {class: "button"}));
                div.find(".button_area .button").eq(i).append(_$("<span/>", {class: "buttonText"}).text(buttonText[i]));
            }
            div.append(_$("<div/>", {class: "OTM_Button"}));
            div.find(".OTM_Button").append(_$("<span/>", {class: "OTM_buttonText"}).text("올레 tv 모바일 시청지점부터"));
            cbCreate(true)
        };

        function setButtons() {
            div.removeClass("hasOtmLinkTime");
            div.removeClass("onlyOtm");
            hasOTMLinkTime = onlyOTMLinkTime = false;
            if (data.otmLinkTime - 0 > 0) {
                hasOTMLinkTime = true;
                div.addClass("hasOtmLinkTime");
                if (bookmarkLinkTime > 0 || !!data.bookmarkTime) div.find(".button_area .button").eq(0).show();
                else {
                    onlyOTMLinkTime = true;
                    div.addClass("onlyOtm");
                    div.find(".button_area .button").eq(0).hide();
                }
            }
        }

        // 팝업이 호출 될 때마다 (화면에 보일 때)
        this.show = function() {
            data = this.getParams();
            if (!!data.bookmarkTime) bookmarkLinkTime = data.bookmarkTime;
            else bookmarkLinkTime = data.linkTime - 0;
            setInfoArea();
            setProgress();
            setResolCd();
            div.find(".button_area .buttonText").eq(0).text(!!data.bookmarkTime ? "북마크부터" : "이어보기");
            setButtons();
            curRow = curIdx = 0;
            setFocus();
            Layer.prototype.show.call(this);
        };

        function callCallback(res) {
            if (!callbackConsumed) {
                callbackConsumed = true;
                if (data.callback) data.callback(res);
            }
        }

        // 팝업이 닫힐 때
        this.hide = function() {
            Layer.prototype.hide.call(this);
            callCallback(-1);
        };

        function enterKeyAction(_logAct) {
            var logAct = _logAct;
            if (logAct == null) {
                if (curIdx == 0) {
                    logAct = !!data.bookmarkTime ? NLC.DETAIL_VOD_PLAY_BOOKMARK_CONTINUE : NLC.DETAIL_VOD_PLAY_CONTINUE;
                    callCallback(bookmarkLinkTime)
                } else if (curIdx == 1) {
                    logAct = NLC.DETAIL_VOD_PLAY_FROMBEGINNING;
                    callCallback(0)
                } else {
                    logAct = NLC.DETAIL_VOD_PLAY_CANCEL;
                    callCallback(-1);
                }
            }
            try {
                var logText = "";
                if (logAct == NLC.DETAIL_VOD_PLAY_CONTINUE) logText = "이어보기";
                else if (logAct == NLC.DETAIL_VOD_PLAY_FROMBEGINNING) logText = "처음부터";
                else if (logAct == NLC.DETAIL_VOD_PLAY_CANCEL) logText = "취소";
                else if (logAct == NLC.DETAIL_VOD_PLAY_BOOKMARK_CONTINUE) logText = "북마크부터";
                else if (logAct == NLC.DETAIL_VOD_PLAY_OTM_CONTINUE) logText = "올레 tv 모바일 시청지점부터";
                NavLogMgr.collectVODContinueDetail(KEY_CODE.ENTER, data.assetId, data.contsName, logAct, logText);
            } catch (e) {
            }
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if (hasOTMLinkTime && curRow == 1) {
                        curIdx = curRow = 0;
                        setFocus();
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (hasOTMLinkTime && curRow == 0) {
                        curIdx = onlyOTMLinkTime ? 1 : 0;
                        curRow = 1;
                        setFocus();
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (curRow == 0) {
                        if (!onlyOTMLinkTime) curIdx = HTool.indexMinus(curIdx, 3);
                    } else if (hasOTMLinkTime && onlyOTMLinkTime) {
                        curIdx = (HTool.indexMinus(curIdx - 1, 2) + 1);
                    }
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    if (curRow == 0) {
                        if (!onlyOTMLinkTime) curIdx = HTool.indexPlus(curIdx, 3);
                    } else if (hasOTMLinkTime && onlyOTMLinkTime) {
                        curIdx = (HTool.indexPlus(curIdx - 1, 2) + 1);
                    }
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if (data.callback) {
                        if (hasOTMLinkTime) {
                            if (onlyOTMLinkTime) {
                                if (curRow == 0) {
                                    callCallback(data.otmLinkTime, true);
                                    enterKeyAction(NLC.DETAIL_VOD_PLAY_OTM_CONTINUE);
                                }
                                else enterKeyAction();
                            } else {
                                if (curRow == 0) enterKeyAction();
                                else {
                                    callCallback(data.otmLinkTime, true);
                                    enterKeyAction(NLC.DETAIL_VOD_PLAY_OTM_CONTINUE);
                                }
                            }
                        } else enterKeyAction();
                    }
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

    };

    vod_continue_popup.prototype = new Layer();
    vod_continue_popup.prototype.constructor = vod_continue_popup;

    vod_continue_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_continue_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodContinuePopup"] = vod_continue_popup;
})();