/**
 * Created by Yun on 2017-05-12.
 */

(function() {
    var vod_couponCheck_popup = function vodCouponCheckPopup(options) {
        Layer.call(this, options);
        var div;
        var btnText = ["확인", "취소"];
        var focusIdx = 0;
        var vodData;
        var isSeries = false;
        var otherCoupon = false;
        var couponData = [];
        var optionsHasCoupon = [];
        var hasCouponData = null;
        var callback;
        var callbackConsumed = false;

        this.init = function (cbCreate) {
            div = this.div;
            vodData = this.getParams().vodData;
            isSeries = this.getParams().isSeries;
            otherCoupon = this.getParams().otherCoupon ? this.getParams().otherCoupon : false;
            callback = this.getParams().callback;
            couponData = [];
            hasCouponData = null;

            console.log("vod_couponCheck_popup() vodData = ", JSON.stringify(vodData, null, 4));
            console.log("vod_couponCheck_popup() isSeries = ", isSeries);

            var optionList, optionCouponData;

            if (isSeries) {
                optionList = vodData.seriesData.seriesList;
                optionCouponData = "seriesCouponData";
            } else {
                optionList = vodData.optionList;
                optionCouponData = "couponData";
            }

            for (var idx = 0; idx < optionList.length; idx++) {
                optionsHasCoupon.push(optionList[idx]);
                couponData.push(optionList[idx][optionCouponData]);
            }

            console.log("vod_couponCheck_popup() couponData = ", JSON.stringify(couponData, null, 4));
            var selectedCont = 0;
            for (var idx = 0 ; idx < couponData.length ; idx ++) {
                // 최고화질을 표기
                /** 
                 ** [WEBIIIHOME-3658] 이미 구매한 화질은 비교대상에서 제외하도록 수정 
                 **/
                if (couponData[idx] && (!optionList[idx].buyYn || optionList[idx].buyYn === "N")) {
                    if (hasCouponData == null) {
                        selectedCont = idx;
                        hasCouponData = couponData[idx];
                    }
                    else if (compareCoupon(hasCouponData, couponData[idx]) == -1) {
                        selectedCont = idx;
                        hasCouponData = couponData[idx];
                    }
                }
            }

            if (!hasCouponData) {
                callCallback(1);
            }
            else {
                if (otherCoupon) {
                    // 다른 구매수단 이용권 알림 팝업
                    if (hasCouponData.couponCase == "CC" || hasCouponData.couponCase == "SC") {
                        // 이용권
                        div.addClass("arrange_frame vod couponCheck_popup");
                        div.append(_$("<div/>", {class: "title_area"}).text(hasCouponData.couponCase == "CC" ? "콘텐츠 이용권 알림" : "시리즈 이용권 알림"));
                        div.append(_$("<div/>", {class: "contentsTitle_area"}));
                        div.append(_$("<div/>", {class: "popupInfo_area"}));

                        div.append(_$("<div/>", {class: "twoBtnArea_280"}));
                    }
                    else if (hasCouponData.couponCase == "CD" || hasCouponData.couponCase == "SD") {
                        // 할인권
                        div.addClass("arrange_frame vod couponCheck_popup");
                        div.append(_$("<div/>", {class: "title_area"}).text(hasCouponData.couponCase == "CD" ? "콘텐츠 할인권 알림" : "시리즈 할인권 알림"));
                        div.append(_$("<div/>", {class: "contentsTitle_area"}));
                        div.append(_$("<div/>", {class: "popupInfo_area"}));

                        div.append(_$("<div/>", {class: "twoBtnArea_280"}));
                    }
                }
                else {
                    optionsHasCoupon[selectedCont].isCouponSelected = true;

                    if (hasCouponData.couponCase == "CC" || hasCouponData.couponCase == "SC") {
                        // 이용권
                        div.addClass("arrange_frame vod couponCheck_popup");
                        div.append(_$("<div/>", {class: "title_area"}).text(hasCouponData.couponCase == "CC" ? "콘텐츠 이용권 알림" : "시리즈 이용권 알림"));
                        div.append(_$("<div/>", {class: "contentsTitle_area"}));
                        div.append(_$("<div/>", {class: "popupInfo_area"}));

                        div.append(_$("<div/>", {class: "twoBtnArea_280"}));
                    }
                    else if (hasCouponData.couponCase == "CD" || hasCouponData.couponCase == "SD") {
                        // 할인권
                        div.addClass("arrange_frame vod couponCheck_popup");
                        div.append(_$("<div/>", {class: "title_area"}).text(hasCouponData.couponCase == "CD" ? "콘텐츠 할인권 알림" : "시리즈 할인권 알림"));
                        div.find(".title_area").addClass("discount");
                        div.append(_$("<div/>", {class: "contentsTitle_area"}));
                        div.find(".contentsTitle_area").addClass("discount");
                        div.append(_$("<div/>", {class: "popupInfo_area"}));
                        div.find(".popupInfo_area").addClass("discount");
                        div.append(_$("<div/>", {class: "popupSubText_area"}).text("(부가세 제외한 금액에서 할인 적용)"));
                        div.append(_$("<div/>", {class: "popupCouponImg_area"}).html("<span class='popupCouponText'>COUPON</span><span class='popupCouponPrice'>" + addComma(hasCouponData.applicableCouponPriceP) + "</span><span class='popupCouponWon'>원</span>"));

                        div.append(_$("<div/>", {class: "twoBtnArea_280"}).css("top", "741px"));
                        div.find(".twoBtnArea_280").addClass("discount");
                    }
                }

                for (var i = 0; i < 2; i++) {
                    div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                    div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                    div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
                }

                setComponentData(hasCouponData);
                cbCreate(true);
            }
        };

        function setComponentData(coupon) {
            div.find(".contentsTitle_area").text("'" + vodData.optionList[0].contsName + "'");
            var resolTxt = getResolutionTypeText(coupon.resolution);
            if (otherCoupon) {
                switch (coupon.couponCase) {
                    case "CC" : // 콘텐츠 이용권
                        div.find(".popupInfo_area").html("무료 시청 가능한 콘텐츠 이용권" + resolTxt + "이 있습니다<br>시리즈로 결제하시겠습니까?");
                        break;
                    case "CD" : // 콘텐츠 할인권
                        div.find(".popupInfo_area").html("사용 가능한 콘텐츠 할인권" + resolTxt + "이 있습니다<br>시리즈로 결제하시겠습니까?");
                        break;
                    case "SC" : // 시리즈 이용권
                        div.find(".popupInfo_area").html("무료 시청 가능한 시리즈 이용권" + resolTxt + "이 있습니다<br>단편으로 결제하시겠습니까?");
                        break;
                    case "SD" : // 시리즈 할인권
                        div.find(".popupInfo_area").html("사용 가능한 시리즈 할인권" + resolTxt + "이 있습니다<br>단편으로 결제하시겠습니까?");
                        break;
                }
            }
            else {
                switch (coupon.couponCase) {
                    case "CC" : // 콘텐츠 이용권
                        div.find(".popupInfo_area").html("무료 시청 가능한 콘텐츠 이용권" + resolTxt + "이 있습니다<br>사용하시겠습니까?");
                        break;
                    case "CD" : // 콘텐츠 할인권
                        div.find(".popupInfo_area").html("사용 가능한 콘텐츠 할인권" + resolTxt + "이 있습니다<br>사용하시겠습니까?");
                        break;
                    case "SC" : // 시리즈 이용권
                        div.find(".popupInfo_area").html("무료 시청 가능한 시리즈 이용권" + resolTxt + "이 있습니다<br>사용하시겠습니까?");
                        break;
                    case "SD" : // 시리즈 할인권
                        div.find(".popupInfo_area").html("사용 가능한 시리즈 할인권" + resolTxt + "이 있습니다<br>사용하시겠습니까?");
                        break;
                }
            }
        }

        function getResolutionTypeText(value) {
            switch(value) {
                case 0:
                    return "(일반)";
                case 1:
                    return "(HD)";
                case 2:
                    return "(FHD)";
                case 3:
                    return "(UHD)";
                default:
                    return "";
            }
        }

        // couponA가 높으면 return 1
        // couponB가 높으면 return -1 (hasCouponData 교체)
        function compareCoupon(couponA, couponB) {
            if (couponA.couponCase == "CC" || couponA.couponCase == "SC") {
                if (couponB.couponCase == "CC" || couponB.couponCase == "SC") {
                    // 같은 이용권이면 화질 비교
                    return (couponA.resolution < couponB.resolution) ? -1 : 1;
                }
                else {
                    // 이용권 우선이므로 return couponA
                    return 1;
                }
            }
            else { // couponA는 할인권
                if (couponB.couponCase == "CC" || couponB.couponCase == "SC") {
                    // couponB는 이용권이므로 return -1
                    return -1;
                }
                else {
                    // 같은 할인권이면 화질 비교
                    return (couponA.resolution < couponB.resolution) ? -1 : 1;
                }
            }
            return 0;
        }

        this.show = function () {
            Layer.prototype.show.call(this);
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            callCallback(-1);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    callCallback(focusIdx);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

        function callCallback(res) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                callback(res);
            }
        }

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }

        var addComma = function(num) {
            return (num+"").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };
    };

    vod_couponCheck_popup.prototype = new Layer();
    vod_couponCheck_popup.prototype.constructor = vod_couponCheck_popup;

    vod_couponCheck_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_couponCheck_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodCouponCheckPopup"] = vod_couponCheck_popup;
})();