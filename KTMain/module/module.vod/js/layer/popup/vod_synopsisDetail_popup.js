/**
 * 줄거리 상세 팝업 (VOD 상세 > +더보기)
 * Created by Yun on 2017-04-24.
 */

(function() {
    var vod_synopsisDetail_popup = function VodSynopsisDetailPopup(options) {
        Layer.call(this, options);
        var div;
        var data;
        var totalPage;
        var curPage;
        var pageHeight = 414;

        this.init = function (cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod synopsisDetail_popup");
            div.append(_$("<div/>", {class: "title_area"}));
            div.append(_$("<div/>", {class: "synopsis_background_div"}));
            div.append(_$("<div/>", {class: "synopsis_area"}));
            div.find(".synopsis_area").append(_$("<div/>", {class: "synopsis_div"}));
            div.append(_$("<div/>", {class: "synopsis_scrollBar_base"}));
            div.append(_$("<div/>", {class: "synopsis_scrollBar_focus"}));
            div.append(_$("<div/>", {id: "button_area"}));
            div.find("#button_area").append(_$("<span/>", {class:"button_title"}).text("닫기"));

            cbCreate(true);
        };

        this.show = function () {
            data = this.getParams().data;
            div.find(".title_area").text("'" + data.title + "'");
            // div.find(".synopsis_div").html(data.synopsis.replace(/\n/g, '<br>'));
            div.find(".synopsis_div").html(HTool.convertTextToHtml(data.synopsis));

            Layer.prototype.show.call(this);
            var divHeight = div.find(".synopsis_div").height();
            divHeight = Math.ceil(divHeight / pageHeight);
            totalPage = divHeight;
            curPage = 0;
            div.find(".synopsis_scrollBar_focus").css("height", (pageHeight/totalPage) + "px");
            if (totalPage > 1) {
                div.find(".synopsis_scrollBar_base").css("display", "block");
                div.find(".synopsis_scrollBar_focus").css("display", "block");
            } else {
                div.find(".synopsis_scrollBar_base").css("display", "none");
                div.find(".synopsis_scrollBar_focus").css("display", "none");
            }
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if (totalPage > 1) changeNoticePage(HTool.getIndex(curPage, -1, totalPage));
                    return true;
                case KEY_CODE.DOWN:
                    if (totalPage > 1) changeNoticePage(HTool.getIndex(curPage, 1, totalPage));
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function changeNoticePage(page) {
            curPage = page;
            div.find(".synopsis_div").css("-webkit-transform", "translateY(" + (-page * pageHeight) + "px)");
            div.find(".synopsis_scrollBar_focus").css("-webkit-transform", "translateY(" + (page * div.find(".synopsis_scrollBar_focus").height()) + "px)");
        }
    };

    vod_synopsisDetail_popup.prototype = new Layer();
    vod_synopsisDetail_popup.prototype.constructor = vod_synopsisDetail_popup;

    vod_synopsisDetail_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_synopsisDetail_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodSynopsisDetailPopup"] = vod_synopsisDetail_popup;
})();