/**
 * 음악(M/V) Play bar > 음악 상세보기(곡 설명) 팝업
 * Created by Yun on 2017-02-13.
 */

(function() {
    var mv_synopsis_popup = function mvSynopsisPopup(options) {
        Layer.call(this, options);
        var div;
        var data;
        var totalPage;
        var curPage;
        var pageHeight = 246;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame mv synopsis_popup");
            div.append(_$("<div/>", {class: "title_area"}).text("곡 설명"));
            div.append(_$("<div/>", {class: "mvTitle_area"}).text(""));
            div.append(_$("<img/>", {id: "mvBackground_top_img", src: "module/module.vod/resource/bg_mv_desc_top.png"}));
            div.append(_$("<img/>", {id: "mvBackground_middle_img", src: "module/module.vod/resource/bg_mv_desc_m.png"}));
            div.append(_$("<img/>", {id: "mvBackground_bottom_img", src: "module/module.vod/resource/bg_mv_desc_btm.png"}));
            div.append(_$("<div/>", {id: "mvSynopsis_area"}));
            div.find("#mvSynopsis_area").append(_$("<div/>", {id: "mvSynopsis_div"}));
            div.append(_$("<div/>", {id: "button_area"}));
            div.find("#button_area").append(_$("<span/>", {class: "button_title"}).text("확인"));

            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            div.find(".mvTitle_area").text("'" + (data.vodData.contsName || data.vodData.itemName) + "'");
            div.find("#mvSynopsis_div").html(HTool.convertTextToHtml(data.vodData.synopsis));
            // div.find("#mvSynopsis_div").html(data.vodData.synopsis.replace(/\n/g, '<br>'));

            Layer.prototype.show.call(this);

            var divHeight = div.find("#mvSynopsis_div").height();
            divHeight = Math.ceil(divHeight / pageHeight);
            totalPage = divHeight;
            curPage = 0;

            log.printDbg(totalPage + ", " + curPage);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    changeSynopsisPage(HTool.getIndex(curPage, -1, totalPage));
                    return true;
                case KEY_CODE.DOWN:
                    changeSynopsisPage(HTool.getIndex(curPage, 1, totalPage));
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function changeSynopsisPage(page) {
            curPage = page;
            div.find("#mvSynopsis_div").css("-webkit-transform", "translateY(" + (-page * pageHeight) + "px)");
        }
    };

    mv_synopsis_popup.prototype = new Layer();
    mv_synopsis_popup.prototype.constructor = mv_synopsis_popup;

    mv_synopsis_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    mv_synopsis_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["MVSynopsisPopup"] = mv_synopsis_popup;
})();