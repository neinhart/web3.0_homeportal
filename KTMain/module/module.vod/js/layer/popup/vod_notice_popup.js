/**
 * 공지사항 팝업
 * Created by Yun on 2017-04-24.
 */

(function() {
    var vod_notice_popup = function vodNoticePopup(options) {
        Layer.call(this, options);
        var div;
        var data;
        var totalPage;
        var curPage;
        var pageHeight = 409;

        this.init = function (cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod notice_popup");
            div.append(_$("<div/>", {class: "title_area"}).text("공지사항"));
            div.append(_$("<div/>", {class: "notice_background_div"}));
            div.append(_$("<div/>", {class: "notice_area"}));
            div.find(".notice_area").append(_$("<div/>", {class: "notice_div"}));
            div.append(_$("<div/>", {class: "notice_scrollBar_base"}));
            div.append(_$("<div/>", {class: "notice_scrollBar_focus"}));
            div.append(_$("<div/>", {id: "button_area"}));
            div.find("#button_area").append(_$("<span/>", {class:"button_title"}).text("닫기"));

            cbCreate(true);
        };

        this.show = function () {
            data = this.getParams().data;
            div.find(".notice_div").html(HTool.convertTextToHtml(data.notice));
            // div.find(".notice_div").html(data.notice.replace(/\n/g, '<br>'));
            Layer.prototype.show.call(this);
            var divHeight = div.find(".notice_div").height();
            totalPage = Math.ceil(divHeight / pageHeight);
            curPage = 0;
            div.find(".notice_scrollBar_focus").css("height", (pageHeight/totalPage) + "px");
            if (totalPage > 1) {
                div.find(".notice_scrollBar_focus").css("display", "block");
                div.find(".notice_scrollBar_base").css("display", "block");
            } else {
                div.find(".notice_scrollBar_focus").css("display", "none");
                div.find(".notice_scrollBar_base").css("display", "none");
            }
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if (totalPage > 1) changeNoticePage(HTool.getIndex(curPage, -1, totalPage));
                    return true;
                case KEY_CODE.DOWN:
                    if (totalPage > 1) changeNoticePage(HTool.getIndex(curPage, 1, totalPage));
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function changeNoticePage(page) {
            curPage = page;
            div.find(".notice_div").css("-webkit-transform", "translateY(" + (-page * 409) + "px)");
            div.find(".notice_scrollBar_focus").css("-webkit-transform", "translateY(" + (page * div.find(".notice_scrollBar_focus").height()) + "px)");
        }
    };

    vod_notice_popup.prototype = new Layer();
    vod_notice_popup.prototype.constructor = vod_notice_popup;

    vod_notice_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_notice_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodNoticePopup"] = vod_notice_popup;
})();