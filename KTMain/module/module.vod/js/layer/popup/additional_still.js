/**
 * Created by Taesang on 2016-11-02.
 */
(function() {
    var additional_still = function additionalStillPopup(options) {
        Layer.call(this, options);
        var div, imgList, curIdx, curRow, page, thumbList, list = [], showList;

        // 최초 생성될 때 1번
        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod additional_still");
            div.append(_$("<img/>", {class: "bigThumb"}));
            div.append(_$("<div/>", {class: "thumbListArea"}));
            div.append(_$("<div/>", {class: "viewArrow left"}));
            div.append(_$("<div/>", {class: "viewArrow right"}));
            div.append(_$("<div/>", {class: "button"}));
            div.find(".button").append(_$("<span/>", {class: "text"}).text("닫기"));
            div.find(".thumbListArea").append(_$("<div/>", {class: "thumbList left"}));
            div.find(".thumbListArea").append(_$("<div/>", {class: "thumbList right"}));
            thumbList = div.find(".thumbListArea .thumbList");
            showList = list[0] = div.find(".thumbListArea .thumbList.left");
            list[1] = div.find(".thumbListArea .thumbList.right");
            for (var i = 0; i < 5; i++) {
                thumbList.append(_$(
                    "<div class='thumb'><img class='thumbImage'></div>"
                ));
            }
            cbCreate(true)
        };

        this.show = function() {
            imgList = this.getParams().imgList;
            curIdx = this.getParams().index;
            page = Math.floor(curIdx / 5);
            curRow = 0;
            setListData(list[0], page);
            setFocus();
            if (imgList.length > 5) div.find(".viewArrow").show();
            else div.find(".viewArrow").hide();
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        function setFocus() {
            div.find(".thumbListArea .thumbList .thumb.focus").removeClass("focus");
            div.find(".button.focus").removeClass("focus");
            if (curRow === 0) {
                showList.find(".thumb").eq(curIdx % 5).addClass("focus");
                div.find(".bigThumb")[0].src = imgList[curIdx];
                div.find(".bigThumb")[0].onerror = function(e) { e.target.src = modulePath + "resource/default_thumb.jpg";};
                page = Math.floor(curIdx / 5);
            } else div.find(".button").addClass("focus");
        }

        function setListData(_list, page) {
            for (var i = 0; i < 5; i++) {
                if (imgList[(page * 5) + i]) {
                    _list.find(".thumbImage")[i].src = imgList[(page * 5) + i];
                    _list.find(".thumbImage")[i].onerror = function(e) { e.target.src = modulePath + "resource/default_thumb_sm.jpg";};
                    _list.find(".thumbImage").eq(i).show();
                } else _list.find(".thumbImage").eq(i).hide();
            }
        }

        function goPrevPage() {
            if (showList === list[0]) {
                thumbList.css("transition", "-webkit-transform 0.0s");
                thumbList.addClass("moveLeft");
                setListData(list[0], page === 0 ? (Math.ceil(imgList.length / 5) - 1) : page - 1);
                setListData(list[1], page);
                void thumbList.offsetWidth;
                thumbList.css("transition", "-webkit-transform 0.5s");
                thumbList.removeClass("moveLeft");
            } else {
                thumbList.css("transition", "-webkit-transform 0.5s");
                setListData(list[0], page === 0 ? (Math.ceil(imgList.length / 5) - 1) : page - 1);
                thumbList.removeClass("moveLeft");
            }
            showList = list[0];
        }

        function goNextPage() {
            if (showList === list[0]) {
                thumbList.css("transition", "-webkit-transform 0.5s");
                setListData(list[1], page === (Math.ceil(imgList.length / 5) - 1) ? 0 : page + 1);
                thumbList.addClass("moveLeft");
            } else {
                thumbList.css("transition", "-webkit-transform 0.0s");
                thumbList.removeClass("moveLeft");
                setListData(list[1], page === (Math.ceil(imgList.length / 5) - 1) ? 0 : page + 1);
                setListData(list[0], page);
                void thumbList.offsetWidth;
                thumbList.css("transition", "-webkit-transform 0.5s");
                thumbList.addClass("moveLeft");
            }
            showList = list[1];
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (curRow === 0) {
                        curIdx = HTool.indexMinus(curIdx, imgList.length);
                        if (imgList.length > 5 && (curIdx === imgList.length - 1 || curIdx % 5 === 4)) goPrevPage();
                    }
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    if (curRow === 0) {
                        curIdx = HTool.indexPlus(curIdx, imgList.length);
                        if (imgList.length > 5 && (curIdx === 0 || curIdx % 5 === 0)) goNextPage();
                    }
                    setFocus();
                    return true;
                case KEY_CODE.UP:
                    curRow = 0;
                    setFocus();
                    return true;
                case KEY_CODE.DOWN:
                    curRow = 1;
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if (curRow === 1) LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    additional_still.prototype = new Layer();
    additional_still.prototype.constructor = additional_still;

    additional_still.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    additional_still.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["AdditionalStill"] = additional_still;
})();