/**
 * HDR 해상도 설정 안내 팝업
 * Created by lyllyl on 2017-06-01.
 */
(function() {
    var vod_hdr_resolution_set_popup = function vodHdrResolutionSetPopup(options) {
        Layer.call(this, options);
        var _this = this, curIdx = 0;
        var settingMenuManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.SETTING_MENU_MANAGER);
        var callbackConsumed = false;
        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame vod hdr_resolution_set_popup");
            this.div.append(_$("<span/>", {class: "title"}).text("HDR 재생 안내"));
            this.div.append(_$("<span/>", {class: "description"}).html("셋톱 해상도 설정 변경이 필요합니다<br>HDR VOD를 시청하시려면 두 가지 필수 설정을 확인해 주세요"));
            this.div.append(_$("<span/>", {class: "description2"}).html("1. 셋톱 해상도를 2160p(60fps)로 설정해 주세요<br>2. 보유하신 TV의 화면/영상 설정 > HDMI color 설정을 \'켜기\'로 설정해 주세요"));
            this.div.append(_$("<span/>", {class: "description3"}).html("※ TV 제조사에 따라 메뉴명과 위치가 상이할 수 있습니다"));
            this.div.append(_$("<div/>", {class: "buttonArea"}));
            var menuArr = ["해상도 설정", "VOD 구매"];
            for (var i = 0; i < 2; i++) {
                _this.div.find(".buttonArea").append(_$(
                    "<div class='button'>" +
                    "<span class='text'>" + menuArr[i] + "</span>" +
                    "</div>"
                ));
            }
            cbCreate(true);
        };

        this.show = function() {
            setFocus(0);
            this.div.find(".button:eq(1) .text").text(this.getParams().btnTxt);
            Layer.prototype.show.call(this);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            Layer.prototype.hide.call(this);
            callCallback(false);
        };

        function setFocus(idx) {
            _this.div.find(".buttonArea .button.focus").removeClass("focus");
            _this.div.find(".buttonArea .button").eq(parseInt(idx) || curIdx).addClass("focus");
        }

        function callCallback(res) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                _this.getParams().callback(res);
            }
        }

        this.handleKeyEvent = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    curIdx = HTool.indexMinus(curIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    curIdx = HTool.indexPlus(curIdx, 2);
                    setFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if (curIdx == 0) {
                        // 해상도 설정
                        callCallback(false);
                        settingMenuManager.activateSettingMenuByMenuID({
                            menuId: "MENU01002",
                            updateSubTitle: false
                        });
                    } else if (curIdx == 1){
                        callCallback(true);
                        LayerManager.deactivateLayer({id: _this.id, targetOnly: true});
                    } return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    callCallback(false);
                    LayerManager.deactivateLayer({id: _this.id, targetOnly: true});
                    return true;
            }
            return false;
        }
    };

    vod_hdr_resolution_set_popup.prototype = new Layer();
    vod_hdr_resolution_set_popup.prototype.constructor = vod_hdr_resolution_set_popup;

    vod_hdr_resolution_set_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    arrLayer["VODHdrResolutionSetPopup"] = vod_hdr_resolution_set_popup;
})();