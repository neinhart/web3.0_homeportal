/**
 * Created by ksk91_000 on 2016-10-24.
 */
(function () {
    var vod_pick_resolution_popup = function VodPickResolutionPopup(options) {
        Layer.call(this, options);
        var data;
        var focus, page;
        var btnFocus = false;
        var div;
        var callback;
        var callbackConsumed = false;

        this.init = function (cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod pick_resolution_popup");
            div.append(_$("<span/>", {class: "title_area"}).text("옵션 선택"), _$("<span/>", {class: "contents_title"}), _$("<span/>", {class: "description_area"}).html("다음 옵션을 구매하셨습니다<br>시청할 옵션을 선택하세요"), _$("<div/>", {class: "left_arrow"}), _$("<div/>", {class: "right_arrow"}), _$("<div/>", {class: "item_area"}), _$("<div/>", {class: "btn_area"}).append("<div class='btn cancel'>취소</div>"));
            setItemArea(div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            // 버튼 생성
            var buttonItem;

            for (var i = 0; i < 4; i++) {
                buttonItem = _$("<div/>", {class: "item"});
                buttonItem.append(_$("<span/>", {class: "resolution"}), _$("<div/>", {class: "label"}).text("시청하기"));

                div.append(buttonItem);
            }
        }

        this.show = function (options) {
            if (!options || !options.resume) {
                data = this.getParams();
                callback = data.callback;
                sortDataList(data.list);
                if (data.isHighLight != null) div.find(".description_area").html("시청하실 화질을 선택하세요");
                div.find(".contents_title").text("\'" + data.title + "\'");
                setPage(0);
                setFocus(0);
            }

            Layer.prototype.show.call(this);
        };

        function sortDataList(list) {
            var resolOrder = ["일반", "SD", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];

            list.sort(function (a, b) {
                var resol = resolOrder.indexOf(b.resolName) - resolOrder.indexOf(a.resolName);

                if (resol !== 0) {
                    return resol;
                }
                var subtitle = (a.data.subtitleDubbed ? a.data.subtitleDubbed - 0 : -1) - (b.data.subtitleDubbed ? b.data.subtitleDubbed - 0 : -1);

                if (subtitle !== 0) {
                    return subtitle;
                }
            });
        }

        function setPage(_page) {
            page = _page;
            setItemData(data, div.find(".item_area"));
        }

        function setFocus(_focus) {
            focus = _focus;
            div.find(".item").removeClass("focus").eq(_focus).addClass("focus");
            div.find(".contents_title").text("\'" + data.list[focus + page * 4].contsName + "\'");
        }

        function setBtnFocus(isFocus) {
            btnFocus = isFocus;
            div.find(".item").eq(focus).toggleClass("focus", !btnFocus);
            div.find(".btn_area .btn").toggleClass("focus", btnFocus);
        }

        function setItemData(data, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");

            if (page == 0) {
                itemArea.find(".item").hide();
            }
            else {
                itemArea.find(".item").css("visibility", "hidden");
            }

            var list = data.list;
            for (var i = 0, j = page * 4; i < 4 && j < list.length; i++, j++) {
                var item = itemArea.find(".item").eq(i);
                var itemName = list[j].resolName;

                if (list[j].data.subtitleDubbed == "0") {
                    itemName += " 자막";
                }
                else if (list[j].data.subtitleDubbed == "1") {
                    itemName += " 더빙";
                }

                item.find(".resolution").text(itemName);
                item.css("visibility", "inherit");
                item.show();
            }
            div.find(".left_arrow").toggle(page > 0);
            div.find(".right_arrow").toggle((page + 1) * 4 < list.length);
        }

        function callCallback(data) {
            if (!callbackConsumed) {
                callbackConsumed = true;
                callback(data);
            }
        }

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (btnFocus) {
                        return true;
                    }

                    if (page * 4 + focus <= 0) {
                        return true;
                    }
                    if (focus - 1 < 0) {
                        setPage(page - 1);
                        setFocus(3);
                    }
                    else {
                        setFocus(focus - 1);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (btnFocus) {
                        return true;
                    }
                    if (page * 4 + focus >= data.list.length - 1) {
                        return true;
                    }
                    if (focus + 1 >= 4) {
                        setPage(page + 1);
                        setFocus(0);
                    }
                    else {
                        setFocus(focus + 1);
                    }
                    return true;
                case KEY_CODE.UP:
                    if (btnFocus) {
                        setBtnFocus(false);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (!btnFocus) {
                        setBtnFocus(true);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (!btnFocus) {
                        callCallback(data.list[page * 4 + focus]);
                    }
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(-1);
        }
    };

    vod_pick_resolution_popup.prototype = new Layer();
    vod_pick_resolution_popup.prototype.constructor = vod_pick_resolution_popup;

    //Override create function
    vod_pick_resolution_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_pick_resolution_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodPickResolutionPopup"] = vod_pick_resolution_popup;
})();