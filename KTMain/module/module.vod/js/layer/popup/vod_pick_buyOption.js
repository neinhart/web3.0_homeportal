/**
 * Created by ksk91_000 on 2016-10-24.
 */
(function() {
    var vod_pick_buyOption_popup = function VodPickBuyOptionPopup(options) {
        Layer.call(this, options);
        var data;
        var focus, page;
        var btnFocus = false;
        var div;
        var callback;
        var callbackConsumed = false;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod pick_resolution_popup");
            div.append(
                _$("<span/>", {class: "title_area"}).text("옵션 선택"),
                _$("<span/>", {class: "description_area"}).html("구매 유형을 선택하세요"),
                _$("<div/>", {class: "left_arrow"}),
                _$("<div/>", {class: "right_arrow"}),
                _$("<div/>", {class: "item_area"}),
                _$("<div/>", {class: "btn_area"}).append("<div class='btn cancel'>취소</div>")
            );
            setItemArea(div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            // 버튼 생성
            var buttonItem,
                priceItem;

            for (var i = 0; i < 4; i++) {
                priceItem = _$("<div/>", {class: "priceInfo"});
                priceItem.append(
                    _$("<div/>", {class: "prefix"}),
                    _$("<div/>", {class: "price"})
                );

                buttonItem = _$("<div/>", {class: "item"});
                buttonItem.append(
                    _$("<span/>", {class: "resolution"}),
                    priceItem,
                    _$("<div/>", {class: "label"}).text("선택하기")
                );

                div.append(buttonItem);
            }
        }

        this.show = function(options) {
            if (!options || !options.resume) {
                data = this.getParams().dataList;
                callback = this.getParams().callback;
                div.find(".contents_title").text("'" + data.title + "'");
            }

            setPage(0);
            setFocus(0);

            Layer.prototype.show.call(this);
        };

        function setPage(_page) {
            page = _page;
            setItemData(data, div.find(".item_area"));
        }

        function setFocus(_focus) {
            focus = _focus;
            div.find(".item").removeClass("focus").eq(_focus).addClass("focus");
        }

        function setBtnFocus(isFocus) {
            btnFocus = isFocus;
            div.find(".item").eq(focus).toggleClass("focus", !btnFocus);
            div.find(".btn_area .btn").toggleClass("focus", btnFocus);
        }

        function setItemData(dataList, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");

            if (page == 0) {
                itemArea.find(".item").hide();
            } else {
                itemArea.find(".item").css("visibility", "hidden");
            }

            var list = dataList;

            for (var i = 0, j = page * 4; i < 4 && j < list.length; i++ , j++) {
                var item = itemArea.find(".item").eq(i);
                item.find(".resolution").text(list[j].title);
                item.find(".priceInfo .prefix").text(getPrefixText(list[j]));
                item.find(".priceInfo .price").text(getPriceText(list[j]));
                item.css("visibility", "inherit");
                item.show();
            }
            div.find(".left_arrow").toggle( page > 0 );
            div.find(".right_arrow").toggle( (page+1) * 4 < list.length );
        }

        function getPriceText(data) {
            return HTool.addComma(data.lessPrice) + "원";
        }

        function getPrefixText (data) {
            var text;

            if (data.isList) {
                text = "최저 ";
            } else {
                text = data.resolCd + " ";
            }
            return text;
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (btnFocus) {
                        return true;
                    }
                    if (page * 4 + focus <= 0) {
                        return true;
                    }
                    if (focus - 1 < 0) {
                        setPage(page - 1);
                        setFocus(3);
                    } else {
                        setFocus(focus - 1);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (btnFocus) {
                        return true;
                    }
                    if (page * 4 + focus >= data.length - 1) {
                        return true;
                    }
                    if (focus + 1 >= 4) {
                        setPage(page + 1);
                        setFocus(0);
                    } else {
                        setFocus(focus + 1);
                    }
                    return true;
                case KEY_CODE.UP:
                    if (btnFocus) {
                        setBtnFocus(false);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (!btnFocus) {
                        setBtnFocus(true);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (!btnFocus) {
                        callCallback(page * 4 + focus);
                    } else {
                        callCallback(-1);
                    }
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id:this.id, onlyTarget: true});
                    return true;
            }
        };

        function callCallback(res, data) {
            if (!callbackConsumed) {
                callbackConsumed = true;
                callback(res, data);
            }
        }

        this.hide = function () {
            Layer.prototype.hide.apply(this, arguments);
            callCallback(-1);
        }
    };

    vod_pick_buyOption_popup.prototype = new Layer();
    vod_pick_buyOption_popup.prototype.constructor = vod_pick_buyOption_popup;

    //Override create function
    vod_pick_buyOption_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_pick_buyOption_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodPickBuyOptionPopup"] = vod_pick_buyOption_popup;
})();