/**
 * 상품안내 팝업
 * Created by Taesang on 2017-01-20.
 */
(function() {
    var vod_no_oss_popup = function vodNoOssPopup(options) {
        Layer.call(this, options);
        var div, cornerData;
        var callback;

        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame vod vod_no_oss");
            div = _$("<div/>", {class: "popup_contents"});
            div.append(_$("<span/>", {class: "title"}).text("상품안내"));
            div.append(_$("<span/>", {class: "contents_name"}));
            div.append(_$("<span/>", {class: "price_area"}));
            div.append(_$("<span/>", {class: "description_area"}));
            div.append(_$("<span/>", {class: "phoneNum_area"}).text("상품 문의 : 국번 없이 100번"));
            div.append(_$("<div/>", {class: "button_area"}).append(_$("<div class='button focus'><div class='buttonText'>확인</div></div>")));

            this.div.append(div);
            cbCreate(true);
        };

        this.show = function() {
            cornerData = this.getParams().data;
            callback = this.getParams().callback;
            
            div.find(".contents_name").text("'" + cornerData.productName + "'");
            div.find(".price_area").text("월 " + HTool.addComma(cornerData.vatProductPrice) + "원");
            div.find(".description_area").html(HTool.convertTextToHtml(cornerData.productDesc));
            // div.find(".description_area").html((cornerData.productDesc).replace(/\r\n/g, '<br>'));

            Layer.prototype.show.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.EXIT:
                case KEY_CODE.BACK:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
            if(callback) callback(false);
        };
    };

    vod_no_oss_popup.prototype = new Layer();
    vod_no_oss_popup.prototype.constructor = vod_no_oss_popup;

    vod_no_oss_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_no_oss_popup.prototype.handleKeyEvent = function(key_code) {
        this.controlKey(key_code);
        return true;
    };

    arrLayer["VodNoOssPopup"] = vod_no_oss_popup;
})();