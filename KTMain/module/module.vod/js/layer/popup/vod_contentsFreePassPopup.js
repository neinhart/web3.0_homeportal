/**
 * Created by Yun on 2017-04-05.
 */

(function() {
    var vod_contentsFreePass_popup = function vodContentsFreePassPopup(options) {
        Layer.call(this, options);
        var div;
        var data;

        var focusIdx = 0;
        var btnText = ["확인", "취소"];

        this.init = function (cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod contents_freePass_popup");
            div.append(_$("<div/>", {class: "title_area"}).text("콘텐츠 이용권 알림"));
            div.append(_$("<div/>", {class: "vod_title_text"}));
            div.append(_$("<div/>", {class: "popup_info_text"}));
            div.append(_$("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }

            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            popupSetData();
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            focusIdx = index;
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }

        function popupSetData() {
            div.find(".vod_title_text").text(data.title);
            div.find(".popup_info_text").html("무료 시청 가능한 콘텐츠 이용권(" + data.resolution + ")이 있습니다<br>사용하시겠습니까?");
        }
    };

    vod_contentsFreePass_popup.prototype = new Layer();
    vod_contentsFreePass_popup.prototype.constructor = vod_contentsFreePass_popup;

    vod_contentsFreePass_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_contentsFreePass_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodContentsFreePassPopup"] = vod_contentsFreePass_popup;
})();