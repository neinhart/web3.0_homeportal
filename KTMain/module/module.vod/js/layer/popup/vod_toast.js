/**
 * Created by Taesang on 2017-01-20.
 */
(function () {
    var vod_toast_popup = function vodToastPopup(options) {
        Layer.call(this, options);
        var _this = this, data, closeTimeout;

        this.init = function (cbCreate) {
            this.div.addClass("arrange_frame vod vod_toast");
            this.div.append(_$("<div/>", {class: "toastArea"}));
            this.div.find(".toastArea").append(_$("<span/>", {class: "toastText"}));
            this.div.find(".toastArea").append(_$("<span/>", {class: "contextArea hide"}));
            this.div.find(".contextArea").append(_$("<div/>", {class: "contextIcon"}));
            this.div.find(".contextArea").append(_$("<div/>", {class: "contextText"}));
            cbCreate(true);
        };

        this.show = function () {
            Layer.prototype.show.call(this);
            data = _this.getParams();

            // 자막 설정 팝업의 경우 아이콘이 고정으로 되어있어서 별도의 클래스 추가함
            // 아이콘 타입으로 열 경우 옵션키 아이콘이 포함되어서 열림
            if (data.type === "SMI") {
                this.div.addClass("smi");
                this.div.find(".contextArea").removeClass("hide");
            } else if (data.type === "ICON") this.div.find(".contextArea").removeClass("hide");

            // text가 배열로 넘어올 경우 0번째는 토스트팝업 내용, 1번째는 아이콘 텍스트로 설정
            // text[0] + 연관메뉴아이콘 + text[1] 순서대로 정렬됨
            if (Array.isArray(data.text) && data.text.length === 2) {
                _this.div.find(".toastText").text(data.text[0]);
                _this.div.find(".contextText").text(data.text[1]);
            } else _this.div.find(".toastText").text(data.text);

            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            closeTimeout = setTimeout(function () {
                LayerManager.deactivateLayer({
                    id: _this.id,
                    remove: true
                });
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }, 3000);
        };

        // 팝업이 닫힐 때
        this.hide = function () {
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            Layer.prototype.hide.call(this);
        };
    };

    vod_toast_popup.prototype = new Layer();
    vod_toast_popup.prototype.constructor = vod_toast_popup;

    vod_toast_popup.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_toast_popup.prototype.handleKeyEvent = function () {
        return false;
    };

    arrLayer["VODToast"] = vod_toast_popup;
})();