/**
 * Created by ksk91_000 on 2016-10-24.
 */
(function() {
    var vod_select_episode_popup = function VODSelectEpisodePopupLayer(options) {
        Layer.call(this, options);
        var data;
        var focus, page;
        var div;
        var currentIdx;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame vod select_corner_popup");
            div.append(_$("<span/>", {class: "pop_title"}));
            div.append(_$("<span/>", {class: "title_area"}));
            div.append(_$("<span/>", {class: "description_area"}));
            div.append(_$("<div/>", {class: "left_arrow"}));
            div.append(_$("<div/>", {class: "right_arrow"}));
            div.append(_$("<div/>", {class: "item_area"}));
            setItemArea(div.find(".item_area"));

            cbCreate(true);
        };

        function setItemArea(div) {
            for (var i = 0; i < 5; i++) {
                var a = _$("<div/>", {class: "item"});
                a.append(_$("<img>"));
                a.append(_$("<span/>"));
                a.append(_$("<div/>", {class: "currentIcon"}));
                div.append(a);
            }
        }

        this.show = function() {
            data = VODManager.getCurrentVODInfo().thumbnailList;
            div.find(".title_area").text("'" + VODManager.getCurrentVODInfo().contsName + "'");
            currentIdx = getCurVodIdx();
            if (data[currentIdx].corner_grp_list[0].cornergrptype == "001") {
                div.find(".description_area").text("시청할 구간을 선택해 주세요");
                div.find(".pop_title").text("구간 선택");
            }
            else {
                div.find(".description_area").text("시청할 코너를 선택해 주세요");
                div.find(".pop_title").text("코너 선택");
            }
            page = Math.floor(currentIdx / 5);
            focus = currentIdx % 5;
            setItemData(data, div.find(".item_area"));
            Layer.prototype.show.call(this);
        };

        function getCurVodIdx() {
            var curCorner = VODInfoManager.getCurrentCornerVod();
            for (var i = 0; i < data.length - 1; i++) {
                if (curCorner.cornergrpid == data[i].corner_grp_list[0].cornergrpid && curCorner.file_id == data[i].file_id) return i;
            }
            return data.length - 1;
        }

        function setItemData(data, itemArea) {
            itemArea.find(".item.focus").removeClass("focus");
            itemArea.find(".item").css("visibility", "hidden");
            for (var i = 0; i < 5; i++) {
                if (page * 5 + i < 0) continue;
                if (page * 5 + i >= data.length) break;
                var item = itemArea.find(".item").eq(i);
                item.find("img").attr({"src": data[page * 5 + i].file_url, "onerror": "this.src='" + ROOT_URL + "resource/default_thumb_sm.jpg'"});
                item.find("span").text(data[page * 5 + i].corner_grp_list[0].cornergrpnm);
                if (i == focus) item.addClass("focus");
                item.css("visibility", "inherit");
                item.toggleClass("current", page * 5 + i == currentIdx);
            }
            div.find(".left_arrow").toggle(page * 5 > 0);
            div.find(".right_arrow").toggle(page * 5 + 5 < data.length);
        }

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (page * 5 + focus <= 0) return true;
                    focus--;
                    if (focus < 0) {
                        page--;
                        focus = 4;
                    }
                    setItemData(data, this.div.find(".item_area"));
                    return true;
                case KEY_CODE.RIGHT:
                    if (page * 5 + focus >= data.length - 1) return true;
                    focus++;
                    if (focus > 4) {
                        page++;
                        focus = 0;
                    }
                    setItemData(data, this.div.find(".item_area"));
                    return true;
                case KEY_CODE.ENTER:
                    LayerManager.historyBack();
                    VODManager.seekJumpTo(data[page * 5 + focus].time_stamp);
                    MiniGuideManager.changeMode("VOD");
                    MiniGuideManager.setThumbnailData(data);
                    MiniGuideManager.getMiniguide("VOD").changeState(VODMiniGuide.STATE.THUMBNAIL_VIEW);
                    // 실제 Seek 되는 위치가 요청한 위치보다 앞으로 잡히게 되면서 썸네일 포커스가 1칸 앞으로 잡히는 이슈가 있음
                    // 팝업에서 선택한 썸네일을 file_id 기준으로 찾아서 포커스 하도록 이슈 해결
                    // 일치하는 file_id가 없으면 기존의 로직대로 동작함 (CurrentPosition 기반)
                    MiniGuideManager.getMiniguide("VOD").setThumbnailFocus(data[page * 5 + focus]);
                    return true;
                case KEY_CODE.UP:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    MiniGuideManager.show(false);
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    vod_select_episode_popup.prototype = new Layer();
    vod_select_episode_popup.prototype.constructor = vod_select_episode_popup;

    //Override create function
    vod_select_episode_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    vod_select_episode_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodSelectEpisodePopup"] = vod_select_episode_popup;
})();