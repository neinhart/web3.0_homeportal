/**
 * Created by Taesang on 2016-11-02.
 */

(function() {
    var vod_point_popup = function vodPointPopup(options) {
        Layer.call(this, options);
        var div, data, ollehArea, userStarBox, focusRow = 0, focusIdx = 0, myPoint = 0;
        var ratingText = ["안봤어요", "최악이에요", "괜히 봤어요", "재미없어요", "별로예요", "아쉬워요", "보통이에요", "괜찮아요", "재미있어요", "정말좋아요", "최고예요"];

        function makeStar(_starArea, _point, ollehStarCount) {
            stars.setRate(_starArea, _point);
            if (ollehStarCount != null) {
                ollehArea.find(".ratingText").text(ratingText[Math.ceil(parseFloat(_point) * 2)]);
                ollehArea.find(".ratingCount").text("(" + ollehStarCount + "명 참여)");
            } else div.find(".userArea .pointArea .starText").text(ratingText[Math.ceil(parseFloat(_point) * 2)]);
        }

        function setOllehArea() {
            div.append(_$("<div/>", {class: "ollehArea"}));
            ollehArea = div.find(".ollehArea");
            ollehArea.append(_$("<div/>", {class: "crown left"}));
            ollehArea.append(_$("<div/>", {class: "crown right"}));
            ollehArea.append(_$("<span/>", {class: "title"}).text(data.ollehTitle));
            ollehArea.append(_$("<div/>", {class: "starArea"}));
            ollehArea.append(_$("<span/>", {class: "ratingText"}));
            ollehArea.append(_$("<span/>", {class: "ratingCount"}));
            ollehArea.find(".starArea").append(stars.getView(stars.TYPE.RED_29));
        }

        function setUserArea() {
            div.append(_$("<div/>", {class: "userArea"}).css("top", data.ollehPoint ? "507px" : "354px"));
            if (data.myPoint != null) {
                div.find(".userArea").append(_$("<div/>", {class: "textArea"}).css("height", "124px"));
                div.find(".userArea .textArea").append(_$("<span/>", {class: "center45M"}).text("이미 평가한 콘텐츠 입니다"));
                div.find(".userArea .textArea").append(_$("<span/>", {class: "center33L"}).text("평가를 변경할 수 있습니다"));
                myPoint = parseFloat(data.myPoint);
            } else {
                div.find(".userArea").append(_$("<div/>", {class: "textArea"}).css("height", "80px"));
                div.find(".userArea .textArea").append(_$("<span/>", {class: "center45M"}).text("얼마나 만족했는지 별점으로 평가해주세요"));
                myPoint = 3;
            }
            div.find(".userArea").append(_$("<div/>", {class: "pointArea"}));
            div.find(".userArea .pointArea").append(_$("<div/>", {class: "starArea whiteStarBox"}));
            div.find(".userArea .pointArea").append(_$("<span/>", {class: "starText"}));
            div.find(".userArea .pointArea .starArea").append(_$("<div/>", {class: "arrow left"}));
            div.find(".userArea .pointArea .starArea").append(_$("<div/>", {class: "arrow right"}));
            div.find(".userArea .pointArea .starArea").append(_$("<div/>", {class: "starBox"}));
            div.find(".userArea .pointArea .starArea .starBox").append(_$("<div/>", {class: "starDiv"}));
            userStarBox = div.find(".userArea .pointArea .starArea .starBox .starDiv");
            userStarBox.append(stars.getView(stars.TYPE.YELLOW_36));
            makeStar(userStarBox, data.myPoint || 3);
            div.find(".userArea .pointArea .starText").text(ratingText[Math.ceil(parseFloat(data.myPoint || 3) * 2)]);

            div.find(".userArea").append(_$("<div/>", {class: "buttonArea"}));
            div.find(".userArea .buttonArea").append(_$(
                "<div class='button'>" +
                "<span class='text'>확인</span>" +
                "</div>"
            ));
            div.find(".userArea .buttonArea").append(_$(
                "<div class='button'>" +
                "<span class='text'>닫기</span>" +
                "</div>"
            ));
        }

        function setFocus() {
            div.find(".userArea .buttonArea .button").removeClass("focus");
            if (focusRow == 0) div.find(".userArea .pointArea .starArea").addClass("focus");
            else {
                div.find(".userArea .pointArea .starArea").removeClass("focus");
                div.find(".userArea .buttonArea .button").eq(focusIdx).addClass("focus");
            }
        }

        this.init = function(cbCreate) {
            data = this.getParams();
            div = this.div;
            div.addClass("arrange_frame vod point_popup");
            setOllehArea();
            setUserArea();
            cbCreate(true);
        };

        this.show = function() {
            if (data.ollehPoint) {
                makeStar(ollehArea.find(".starArea"), data.ollehPoint[1], data.ollehPoint[0]);
                ollehArea.addClass("show");
            }
            focusRow = focusIdx = 0;
            setFocus();
            Layer.prototype.show.call(this);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (focusRow == 0 && myPoint > 0) {
                        myPoint -= 0.5;
                        makeStar(userStarBox, myPoint);
                    } else {
                        focusIdx = HTool.indexMinus(focusIdx, 2);
                        setFocus();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow == 0 && myPoint < 5) {
                        myPoint += 0.5;
                        makeStar(userStarBox, myPoint);
                    } else {
                        focusIdx = HTool.indexPlus(focusIdx, 2);
                        setFocus();
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 0) {
                        focusRow = 1;
                        focusIdx = 0;
                        setFocus();
                    } else {
                        if (focusIdx == 0 && parseFloat(data.myPoint) != myPoint) {
                            curationManager.saveRating(StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT) != 1 ? "Y" : "N", data.contsId, myPoint, "V", function(result, response) {
                                log.printDbg(JSON.stringify(response));
                                var myHomeModule = ModuleManager.getModule("module.family_home");
                                if (myHomeModule) myHomeModule.execute({method: "setRatingFlag"});
                            });
                            if (parseFloat(myPoint) == 0) myPoint = null;
                            data.callback(myPoint);
                        }
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.UP:
                    focusRow = 0;
                    setFocus();
                    return true;
                case KEY_CODE.DOWN:
                    if (focusRow == 0) {
                        focusRow = 1;
                        focusIdx = 0;
                        setFocus();
                    }
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    vod_point_popup.prototype = new Layer();
    vod_point_popup.prototype.constructor = vod_point_popup;

    vod_point_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_point_popup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodPointPopup"] = vod_point_popup;
})();