/**
 * TV 미지원 HDR 알림 팝업
 * Created by lyllyl on 2017-06-01.
 */
(function() {
    var vod_hdr_no_support_popup = function vodHdrNoSupportPopup(options) {
        Layer.call(this, options);
        var _this = this;
        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame vod hdr_no_support_popup");
            this.div.append(_$("<span/>", {class: "title"}).text("알림"));
            this.div.append(_$("<span/>", {class: "description"}).html("보유하신 TV에서 HDR을 지원하지 않아 시청할 수 없습니다<br>HDR 지원여부를 확인해 주세요"));
            this.div.append(_$("<div/>", {class: "buttonArea"}));
            this.div.find(".buttonArea").append(_$(
                "<div class='button focus'>" +
                "<span class='text'>확인</span>" +
                "</div>"
            ));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            this.getParams().callback(false);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.handleKeyEvent = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    LayerManager.deactivateLayer({id: _this.id, targetOnly: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id: _this.id, targetOnly: true});
                    return true;
            }
            return false;
        }
    };

    vod_hdr_no_support_popup.prototype = new Layer();
    vod_hdr_no_support_popup.prototype.constructor = vod_hdr_no_support_popup;

    vod_hdr_no_support_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    arrLayer["VODHdrNoSupportPopup"] = vod_hdr_no_support_popup;
})();