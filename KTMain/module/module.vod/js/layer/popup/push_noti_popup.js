(function() {
    var push_noti_popup = function pushNotiPopup(options) {
        Layer.call(this, options);
        var div;
        var btnText = ["시청", "취소"];
        var focusIdx = 0;
        var pushData = [];
        var callback;
        var callbackConsumed = false;

        this.init = function (cbCreate) {
            div = this.div;
            log.printDbg("push_noti_popup() getParams = " + JSON.stringify(this.getParams()));
            var _pushData = this.getParams().data.split("|");
            pushData = {
                contentId: _pushData[0],
                catId: _pushData[1],
                requesterCode: _pushData[2],
                contentName: _pushData[3],
                purchaseType: _pushData[4],
                ltFlag: _pushData[5],
                price: _pushData[6],
                viewTime: _pushData[7],
                durationTime: _pushData[8],
                resolCd: _pushData[9],
                targetContentId: _pushData[10],
                targetCatId: _pushData[11],
                saleYn: _pushData[12],
            };
            log.printDbg("push_noti_popup() pushData = " + JSON.stringify(pushData));

            callback = this.getParams().callback;

            div.addClass("arrange_frame vod push_noti_popup");
            div.append(_$("<div/>", {class: "title_area"}).text("결제 완료"));
            div.append(_$("<div/>", {class: "contentsTitle_area"}));
            div.append(_$("<div/>", {class: "opt_area"}));
            div.append(_$("<div/>", {class: "popupInfo_area"}).text("감사합니다. 결제가 완료되었습니다"));

            div.append(_$("<div/>", {class: "twoBtnArea_280"}));

            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append(_$("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append(_$("<span/>", {class: "btnText"}).text(btnText[i]));
            }

            setComponentData(pushData);
            cbCreate(true);
        };

        function setComponentData(option) {
            div.find(".contentsTitle_area").text("'" + option.contentName + "'");

            var optName = _$("<span/>", {class: "opt_name"}).html(getMiniTitle(option));
            div.find(".opt_area").append(optName);
        }

        var addComma = function(num) {
            return (num+"").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };

        function getMiniTitle(option) {
            var result;

            result = "구매옵션 : <white>";
            if (option.purchaseType == "3") {
                // VOD PPT는 화질이 없으므로  가격만 표시
                result += addComma(option.price) + "원</white>";
            }
            else {
                if (option.purchaseType == "2") {
                    result += "시리즈 ";
                }
                else if (option.purchaseType == "5") {
                    result += "패키지 ";
                }
                else {
                    result += "단편 ";
                }

                if (option.ltFlag == "2") {
                    result += "소장 ";
                }

                result += getResolutionTypeText(option.resolCd) + "<smallbar></smallbar>";
                result += addComma(option.price) + "원</white>";
            }

            result += "<bar></bar>";
            if (option.viewTime == 999999) {
                result += "기간 : <white>해지 시까지<white>";
            } else {
                result += "기간 : <white>" + Math.floor((option.viewTime) / 24) + "일 간 시청 가능<white>";
            }

            return result;
        }

        function getResolutionTypeText(value) {
            switch(value) {
                case 1:
                    return "HD";
                case 2:
                    return "일반";
                case 3:
                    return "FHD";
                case 4:
                    return "UHD";
                default:
                    return "";
            }
        }

        this.show = function () {
            Layer.prototype.show.call(this);
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            callCallback(-1);
        };

        this.controlKey = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = HTool.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    callCallback(focusIdx);
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.deactivateLayer({id: this.id, onlyTarget: true});
                    return true;
            }
        };

        function callCallback(res) {
            if(!callbackConsumed) {
                callbackConsumed = true;
                callback(res, pushData);
            }
        }

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }
    };

    push_noti_popup.prototype = new Layer();
    push_noti_popup.prototype.constructor = push_noti_popup;

    push_noti_popup.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    push_noti_popup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["PushNotiPopup"] = push_noti_popup;
})();