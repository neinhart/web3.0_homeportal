/**
 * Created by Lazuli on 2017-04-13.
 */
(function() {
    var vod_playList_fail = function vodPlayListFail(options) {
        Layer.call(this, options);
        var _this = this, curIdx = 0;
        this.init = function(cbCreate) {
            this.div.addClass("arrange_frame vod playList_fail");
            this.div.append(_$("<span/>", {class: "title"}).text("알림"));
            this.div.append(_$("<span/>", {class: "seriesName"}));
            this.div.append(_$("<span/>", {class: "description"}));
            this.div.append(_$(
                "<div class='button'>" +
                "<span class='text'>확인</span>" +
                "</div>"
            ));
            cbCreate(true);
        };

        this.show = function() {
            var seriesName = "";
            var failList = this.getParams().failList;
            if (failList.length > 1) {
                seriesName = "'" + failList[0] + "' 외 " + (failList.length - 1) + "개";
            } else seriesName = "'" + failList[0] + "'";
            _this.div.find(".seriesName").text(seriesName);
            _this.div.find(".description").html("해당 콘텐츠의 정보가 없습니다<br>편성에서 변경/삭제된 콘텐츠 입니다");
            Layer.prototype.show.call(this);
        };

        // 팝업이 닫힐 때
        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.handleKeyEvent = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    _this.getParams().callback();
                    LayerManager.deactivateLayer({id: _this.id, remove: true});
                    return true;
            }
            return false;
        }
    };

    vod_playList_fail.prototype = new Layer();
    vod_playList_fail.prototype.constructor = vod_playList_fail;

    vod_playList_fail.prototype.create = function(cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    arrLayer["VODPlayListFail"] = vod_playList_fail;
})();