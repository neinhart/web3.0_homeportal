/**
 * Created by Taesang on 2017-01-20.
 */
(function () {
    var centerFeedback = function centerFeedback_popup(options) {
        Layer.call(this, options);
        var _this = this, state, speed, closeTimeout, closeTimer = 1500, isShowing;
        this.iconList = [];

        this.init = function (cbCreate) {
            this.div.addClass("arrange_frame vod FFIndicator");
            this.div.append(_$("<div/>", {id: "main_indicator"}));
            this.div.find("#main_indicator").css("background", "url(" + ROOT_URL + "resource/center_control_bg.png" + ")");
            this.div.find("#main_indicator").append(_$("<div/>", {id: "main_indicator_wrapper"}));
            this.div.find("#main_indicator_wrapper").append(_$("<div/>", {id: "icon"}));
            this.div.find("#main_indicator_wrapper").append(_$("<span/>", {id: "speed_area"}));
            this.div.find("#speed_area").append(_$("<img>", {src: ROOT_URL + "resource/x_big.png"}));
            this.div.find("#speed_area").append(_$("<span/>", {id: "speed_txt"}));
            this.div.find("#main_indicator_wrapper #icon").append(_$("<img>", {
                id: "icon_0", src: ROOT_URL + "resource/icon_control_play.png"
            }));
            this.div.find("#main_indicator_wrapper #icon").append(_$("<img>", {
                id: "icon_1", src: ROOT_URL + "resource/icon_control_pause.png"
            }));
            this.div.find("#main_indicator_wrapper #icon").append(_$("<img>", {
                id: "icon_2", src: ROOT_URL + "resource/icon_control_ff.png", style: "-webkit-transform:scale(-1, 1);"
            }));
            this.div.find("#main_indicator_wrapper #icon").append(_$("<img>", {
                id: "icon_3", src: ROOT_URL + "resource/icon_control_ff.png"
            }));
            this.div.find("#main_indicator_wrapper #icon").append(_$("<img>", {
                id: "icon_4", src: ROOT_URL + "resource/icon_control_jump_back.png"
            }));
            this.div.find("#main_indicator_wrapper #icon").append(_$("<img>", {
                id: "icon_5", src: ROOT_URL + "resource/icon_control_jump_forward.png"
            }));
            this.iconList[VODManager.PLAY_STATE.STOP] = "";
            this.iconList[VODManager.PLAY_STATE.PLAY] = "icon_0";
            this.iconList[VODManager.PLAY_STATE.PAUSE] = "icon_1";
            this.iconList[VODManager.PLAY_STATE.REWIND] = "icon_2";
            this.iconList[VODManager.PLAY_STATE.FASTFOWARD] = "icon_3";
            this.iconList[VODManager.PLAY_STATE.FASTPLAY] = "icon_3";
            this.iconList[VODManager.PLAY_STATE.SLOWPLAY] = "icon_3";
            this.iconList[VODManager.PLAY_STATE.SEEKBACK] = "icon_4";
            this.iconList[VODManager.PLAY_STATE.SEEKFWD] = "icon_5";
            cbCreate(true);
        };

        function showIndicator() {
            _this.div.find("#icon .show").removeClass("show");
            _this.div.find("#icon #" + _this.iconList[state]).addClass("show");

            closeTimer = 1500;
            if (state == VODManager.PLAY_STATE.SEEKBACK || state == VODManager.PLAY_STATE.SEEKFWD) {
                if (speed - 0 > 0) {
                    if ((speed / 1E3) % 60 == 0) _this.div.find("#speed_txt").text((speed / 1E3) / 60 + "분");
                    else _this.div.find("#speed_txt").text((speed / 1E3) + "초");
                }
                else _this.div.find("#speed_txt").text("5분");
                _this.div.find("#speed_area").css("display", "block");
                _this.div.find("#speed_area>img").css("display", "none");
            }
            else if (speed) {
                _this.div.find("#speed_txt").text(Math.abs(speed - 0));
                _this.div.find("#speed_area").css("display", "block");
                _this.div.find("#speed_area>img").css("display", "inline-block");
            }
            else {
                _this.div.find("#speed_area").css("display", "none");
            }

            if (state == VODManager.PLAY_STATE.REWIND || state == VODManager.PLAY_STATE.FASTFOWARD || state == VODManager.PLAY_STATE.FASTPLAY || state == VODManager.PLAY_STATE.SLOWPLAY) closeTimer = 500;
        }

        this.show = function () {
            isShowing = true;
            Layer.prototype.show.call(this);
            state = _this.getParams().state;
            speed = _this.getParams().speed;
            if (speed == 1) speed = null;
            showIndicator();
            if (closeTimeout) {
                clearTimeout(closeTimeout);
            }
            closeTimeout = setTimeout(function () {
                if (_this.getParams().hideFunc) _this.getParams().hideFunc();
                LayerManager.deactivateLayer({id: _this.id});
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }, closeTimer);
        };

        // 팝업이 닫힐 때
        this.hide = function () {
            isShowing = false;
            if (closeTimeout) {
                clearTimeout(closeTimeout);
                closeTimeout = void 0;
            }
            Layer.prototype.hide.call(this);
        };

        this.isShowing = function () {
            return isShowing
        };
    };

    centerFeedback.prototype = new Layer();
    centerFeedback.prototype.constructor = centerFeedback;

    centerFeedback.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    centerFeedback.prototype.handleKeyEvent = function () {
        return false;
    };

    arrLayer["CenterFeedback"] = centerFeedback;
})();