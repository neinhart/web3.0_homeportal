/**
 * Created by ksk91_000 on 2016-07-27.
 */
(function () {
    var vod_player_layer = function vodPlayerLayer(options) {
        Layer.call(this, options);

        var contextMenu;
        var title;
        var getParams = {};
        var redKeyCallback;

        function hideContext() {
            contextMenu.hide();
        }

        this.getContextMenu = function () {
            return contextMenu
        };

        this.init = function () {
            this.div.attr({class: "arrange_frame vod"});

            var parentVBO = _$(parent.document.getElementById('mainVBOArea'));
            if (!parentVBO.find(".smiArea") || parentVBO.find(".smiArea").length == 0) {
                parentVBO.append(_$("<div/>", {class: "smiArea"}));
            }

            this.div.append(MiniGuideManager.getView());
            this.div.append(_$("<div/>", {class: "redKeyPromo"}));
            this.div.find(".redKeyPromo").append(_$("<div/>", {class: "redKeyPromoIcon"}));
            this.div.find(".redKeyPromo").append(_$("<span/>", {class: "redKeyPromoText"}).text("바로구매"));
            contextMenu = new VODPlayContext();
            contextMenu.setContext(hideContext);
            this.div.append(contextMenu.createView());
            this.div.append(MiniGuideManager.getMiniIndicatorView());
        };

        this.showRedKeyPromo = function (callback) {
            this.div.find(".redKeyPromo").addClass("show");
            redKeyCallback = callback;
        };

        this.hideRedKeyPromo = function () {
            this.div.find(".redKeyPromo").removeClass("show");
            redKeyCallback = null;
        };

        this.show = function (options) {
            Layer.prototype.show.call(this);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            MiniGuideManager.hide();

            if (contextMenu && contextMenu.isOpen()) {
                hideContext();
            }

            log.printDbg("hide VodPlayer");
        };

        this.load = function () {
            getParams = this.getParams();

            if (!getParams) {
                return;
            }

            title = getParams.title ? getParams.title : "";
            VODManager.play(this);

            if (contextMenu && contextMenu.isOpen()) {
                hideContext();
            }

            MiniGuideManager.hideMiniIndicator();
            contextMenu.setContextInfo();
        };

        this.onLoadComplete = function () {
            contextMenu.setCornerData(null);
            MiniGuideManager.setThumbnailData(null);
            MiniGuideManager.setTitle();
            MiniGuideManager.setRelationVod();
            if (VODManager.isCornerMode()) {
                MiniGuideManager.getMiniguide("Corner").setCornerData(VODInfoManager.getCurrentCornerData());
                MiniGuideManager.changeMode("Corner");
            }
            MiniGuideManager.hideMiniIndicator();
            MiniGuideManager.hide();
        };

        // 썸네일 로드 완료 시점
        this.onThumbLoadComplete = function (thumData) {
            contextMenu.setCornerData(thumData);
            MiniGuideManager.setThumbnailData(thumData);
        };

        this.onVODEnd = function () {
            if (contextMenu && contextMenu.isOpen()) {
                hideContext();
            }
            this.hideRedKeyPromo();
            MiniGuideManager.hideMiniIndicator();
            MiniGuideManager.hide();
        };

        this.controlKey = function (key_code) {
            if (VODManager.isLoading()) {
                return true;
            }

            if (contextMenu.isOpen()) {
                return contextMenu.controlKey(key_code);
            }

            if (key_code == KEY_CODE.RED && redKeyCallback != null) {
                redKeyCallback();
                return true;
            }

            if (MiniGuideManager.onKeyAction(key_code)) {
                return true;
            }

            switch (key_code) {
                case KEY_CODE.ENTER:
                    if (MiniGuideManager.isShowing()) {
                        MiniGuideManager.hide();
                    } else {
                        MiniGuideManager.show();
                    }
                    return true;
                case KEY_CODE.PLAY:
                    VODManager.pause();
                    return true;
                case KEY_CODE.RED:
                case KEY_CODE.REW :
                    VODManager.rew();
                    if (!VODManager.isADMode()) {
                        MiniGuideManager.show();
                    }
                    return true;
                case KEY_CODE.BLUE:
                case KEY_CODE.FF:
                    VODManager.fwd();

                    if (!VODManager.isADMode()) {
                        MiniGuideManager.show();
                    }
                    return true;
                case KEY_CODE.UP:
                    VODManager.playFast();
                    return true;
                case KEY_CODE.DOWN:
                    VODManager.playSlow();
                    return true;
                case KEY_CODE.LEFT:
                    VODManager.seekLeft();
                    return true;
                case KEY_CODE.RIGHT:
                    VODManager.seekRight();
                    return true;
                case KEY_CODE.BACK:
                    if (MiniGuideManager.isShowing()) {
                        MiniGuideManager.hide();
                    } else {
                        if (LayerManager.getLastLayerInStack({key: "type", value: Layer.TYPE.NORMAL}) != null) {
                            VODManager.showLastDetailLayer();
                        }
                        else LayerManager.activateHome();
                    }
                    return true;
                case 413:
                case KEY_CODE.EXIT:
                    if (StateManager.isOtherAppState()) return false;
                    if (MiniGuideManager.isShowing()) {
                        MiniGuideManager.hide();
                    } else {
                        var lastLayer = LayerManager.getLastLayerInStack({key: "type", value: Layer.TYPE.NORMAL});

                        if (lastLayer != null && lastLayer.isShowing()) {
                            return false;
                        }
                        VODManager.finish();
                    }
                    return true;
                case KEY_CODE.CONTEXT:
                    if (!VODManager.isADMode()) {
                        contextMenu.show();
                    }
                    return true;
            }
            return false;
        };
    };

    vod_player_layer.prototype = new Layer();
    vod_player_layer.prototype.constructor = vod_player_layer;

    //Override create function
    vod_player_layer.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);

        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    vod_player_layer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodPlayerLayer"] = vod_player_layer;
})();