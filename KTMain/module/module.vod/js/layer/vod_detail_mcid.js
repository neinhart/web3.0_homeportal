/**
 * Created by ksk91_000 on 2016-09-06.
 */
(function () {
    var vod_detail_mcid_layer = function vodDetailMCIDLayer(options) {
        Layer.call(this, options);

        var _this = this;

        var cateId; // 카테고리 ID
        var cateData; // 카테고리 정보
        var btnList = []; // 버튼 리스트
        var wishObject = {};
        var couponCount = 0;
        var btnFocusIdx = 0; // 버튼 포커스
        var rowIdx = 2;
        var contextMenu = null; // 연관 메뉴
        var bottomMenu = null; // 하단 추천 메뉴
        var reqPathCd; // 진입코드

        var isDetailSyn = false; // 더보기 버튼 유무
        var isNotice = false; // 공지 유무
        var isNoticeUnFocus = false; // 공지 버튼 포커스 유무
        var isConnerChk = false; // 월정액 체크 여부
        var isConnerBuyChk = false; // 월정액 구매 여부
        var isVodPptBuyChk = false; // 기간정액 구매 가능 여부
        var isAdultCate = false; // 성인카테고리
        var isAuthorizedContent = false; // 성인 인증된 컨텐츠인지 확인
        var isAuthComplete = false; // 인증 완료 여부
        var isSingle = false; // 버튼타입 단편
        var isKidsCate = false; // 키즈카테고리 진입 유무
        var isSubtitle = false; // 자막 유무
        var isLongCon = false; // 시리즈 장기 아이콘
        var savedSeamlessCode = "1"; // 심리스 코드 저장
        var optionData;
        var pointData; // 포인트 정보

        var contentsData;
        var repVodData;
        var otherResolBtnList;
        var previewBtnList;
        var previewData;

        var isHideContextMenu = false;
        var is2LineTitle = false;

        this.init = function (cbCreate) {
            log.printDbg("init");

            this.div.addClass("arrange_frame vod_detail_single_layer vod");
            this.div.css("background", "black");

            cateData = _$.extend({}, this.getParams().cateInfo);
            cateId = this.getParams().cat_id;
            reqPathCd = this.getParams().req_cd;

            isAdultCate = this.getParams().isAdultCate;
            isAuthorizedContent = isAdultCate || this.getParams().isAuthorizedContent;
            isKidsCate = this.getParams().isKids || KidsModeManager.isKidsMode();
            isLongCon = false;
            isDetailSyn = false;

            couponCount = 0;
            btnFocusIdx = 0;
            rowIdx = 2;
            btnList = [];

            wishObject = {
                contsId: cateId
            };

            var tmpFlag = cateData.ltFlag.split("|");
            for (var idx = 0; idx < tmpFlag.length; idx++) {
                if (tmpFlag[idx] == 2 || tmpFlag[idx] == 1) {
                    isLongCon = true;
                }
            }

            if (isKidsCate) {
                this.div.addClass("kids");
            }

            bottomMenu = new DetailBottom();
            constructDom();
            initContentsData(cbCreate);
        };

        function initContentsData(cbCreate) {
            log.printDbg("initContentsData");

            var getCateCont = cateData.cmbYn == "N" ? VODAmocManager.getCateContExtW3 : VODAmocManager.getCateContNxtW3;
            getCateCont.call(null, function (result, data) {
                if (result) {
                    setContentsData(cateData.cmbYn == "N" ? data.seriesContsList : data.cmbSeriesContsList, function (res) {
                        if (res) {
                            createContextMenu();

                            if (cateData.adultOnlyYn != "Y") {
                                addRecentlyVod(cateId, "", cateData.imgUrl, cateData.prInfo, isKidsCate);
                            }

                            DetailUtil.updateTvPoint(function (point) {
                                setData(cateData, point);

                                if (cbCreate) {
                                    cbCreate(res);
                                }
                            });
                        } else {
                            onError();
                        }
                    });
                    refreshBtnBuyInfo();
                } else {
                    onError();
                }
            }, cateId, DEF.SAID);

            function onError() {
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});

                if (cbCreate) {
                    cbCreate(false);
                }
            }
        }

        /** 데이터 주입 */
        function setContentsData(data, callback) {
            log.printDbg("setContentsData");

            if (!Array.isArray(data)) {
                data = [data];
            }

            contentsData = data;

            for (var idx = 0; idx < data.length; idx++) {
                if (DetailUtil.isCaption(data[idx].capUrl)) {
                    isSubtitle = true;
                    break;
                } else {
                    isSubtitle = false;
                }
            }
            repVodData = getRepVodData(data, cateData.repContsId);

            OptionListFactory.getOptionList(data, cateData, reqPathCd, function (res, optionList) {
                if (res) {
                    if (optionList.length > 0) {
                        DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), optionList[0]);
                    } else {
                        DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), null);
                    }

                    optionData = optionList;
                    makeButtonList(optionData);
                    setButton();

                    /** 미리보기, 하이라이트보기 체크 */
                    previewData = null;
                    DetailUtil.checkPreview(optionData, function (result) {
                        previewData = result;
                    });
                }
                if (callback) {
                    callback(res);
                }
            }, wishObject, optionData);
        }

        /** 버튼 생성 */
        function makeButtonList() {
            log.printDbg("makeButtonList length = " + optionData.length);

            btnList = [];
            isConnerChk = false;
            isConnerBuyChk = false;
            isVodPptBuyChk = false;
            isSingle = false;

            var btnObj;

            for (var i = 0; i < optionData.length; i++) {
                var singleOrSeries = -1;

                if (optionData[i].connerYn || optionData[i].pptBuyYn) {
                    isConnerChk = true;
                }

                if (optionData[i].type == ButtonObject.TYPE.CORNER) {
                    isConnerBuyChk = true;
                }

                if (optionData[i].type == ButtonObject.TYPE.VODPPT) {
                    isVodPptBuyChk = true;
                }

                if (optionData[i].type == ButtonObject.TYPE.NORMAL) {
                    isSingle = true;
                    singleOrSeries = 0;
                }

                if (optionData[i].type == ButtonObject.TYPE.DVD) {
                    singleOrSeries = 0;
                }

                if (optionData[i].type == ButtonObject.TYPE.SERIES) {
                    singleOrSeries = 1;
                }

                // [kh.kim] 단편, 시리즈 구매 유형이 함께 있을 때
                // 단편 또는 시리즈에만 이용권이 발급된 경우
                // 이용권 없는 구매버튼 선택 시 다른 구매유형의 이용권 존재 알림 팝업을 띄워야 한다.
                // 이용권 체크를 button object 의 execute 를 통해 진입하는 prePlayProcessByButtonObj 에서 하기 때문에..
                // 다른 구매유형의 이용권 체크하기 위해서는 버튼 데이터를 전달해줘야 한다.
                // 미리 각 버튼에 다른 구매유형의 버튼을 저장해두고, execute 시 해당 버튼 데이터를 전달해서 처리하도록 했다.
                // 이 로직은 상세화면에서 구매하는 시나리오에만 적용된다.
                var otherBtn = null;
                if (singleOrSeries == 0) {
                    for (var k = 0; k < optionData.length; k++) {
                        if (optionData[k].type == ButtonObject.TYPE.SERIES) {
                            otherBtn = optionData[k];
                            break;
                        }
                    }
                }
                else if (singleOrSeries == 1) {
                    for (var k = 0; k < optionData.length; k++) {
                        if ((optionData[k].type == ButtonObject.TYPE.NORMAL || optionData[k].type == ButtonObject.TYPE.DVD)
                        && (optionData[k].couponCount && optionData[k].couponCount > 0)) {
                            otherBtn = optionData[k];
                            break;
                        }
                    }
                }

                DetailUtil.btnObject.prototype.enterKeyAction = btnKeyAction;
                btnObj = new DetailUtil.btnObject(optionData[i], otherBtn, _this.div, i);
                btnList.push(btnObj);
            }

            // KT버튼 생성
            for (var j = 0; j < contentsData.length; j++) {
                if (contentsData[j].btnName && contentsData[j].btnName.length > 0) {
                    var nameList = contentsData[j].btnName.split("|");
                    var typeList = contentsData[j].btnType.split("|");
                    var trgtList = contentsData[j].btnTrgt.split("|");

                    for (var i = 0; i < nameList.length; i++) {
                        addAddedButton(new DetailUtil.addedBtnObject(nameList[i], typeList[i], trgtList[i], optionData.length + (j + i)));
                    }
                }
            }
        }

        /** 버튼리스트 추가 */
        function addAddedButton(btnObj) {
            for (var i = 0; i < btnList.length; i++) {
                if (btnList[i].isOtherAppBtn && btnList[i].target == btnObj.target) {
                    return;
                }
            }
            btnList.push(btnObj)
        }

        function getRepVodData(data, repContsId) {
            log.printDbg("getRepVodData");

            for (var i = 0; i < data.length; i++) {
                if ((data[i].contsId || data[i].itemId) == repContsId) {
                    return data[i];
                }
            }
            return data[0];
        }

        /** 연관메뉴 생성 */
        function createContextMenu() {
            log.printDbg("createContextMenu");

            /**
             * itemType = 0 : Package
             * itemType = 1 && seriesType = 02 : MCID
             * itemType = 1 && else : Series
             * itemType = 2 : Single
             * itemType = 3 , 7 , 8 : App
             * */

            contextMenu = new VODDetailContext();
            contextMenu.setVodInfo({
                itemName: cateData.catName || cateData.itemName,
                imgUrl: cateData.imgUrl,
                catId: cateData.catId || cateData.itemId,
                contsId: cateData.catId || cateData.itemId,
                itemType: 1,
                seriesType: "02",
                cmbYn: cateData.cmbYn,
                optionSet: optionData,
                isKids: isKidsCate,
                prInfo: cateData.prInfo,
                callBackFunc: function (data) {
                    wishObject.playCheckFlag = data.wishFlag;
                }
            });

            otherResolBtnList = [];
            previewBtnList = [];
            var _otherResolBtnList = [];
            var _previewBtnList = [];

            // TaeSang. 2018.02.21
            // 미리보기 버튼 체크 로직 추가
            for (var i in optionData) {
                if (!(optionData[i].buyYn || optionData[i].connerYn || optionData[i].pptBuyYn || optionData[i].lessPrice == 0)) {
                    for (var j in optionData[i].optionList) {
                        if (optionData[i].optionList[j].previewYn === "Y") {
                            if (!_previewBtnList[optionData[i].title]) _previewBtnList[optionData[i].title] = {
                                index: i,
                                previewStartTime : optionData[i].optionList[j].previewStartTime,
                                previewRunTime : optionData[i].optionList[j].previewRunTime
                            };
                        }
                    }
                }
            }

            for (var i in optionData) {
                if ((optionData[i].buyYn || optionData[i].connerYn || optionData[i].pptBuyYn || optionData[i].lessPrice == 0) && !optionData[i].vodPlus) {
                    for (var j in optionData[i].optionList) {
                        if (!optionData[i].optionList[j].buyYn && !optionData[i].optionList[j].connerYn && !optionData[i].optionList[j].pptBuyYn && optionData[i].optionList[j].vatPrice != "0") {
                            if (!_otherResolBtnList[optionData[i].title]) {
                                _otherResolBtnList[optionData[i].title] = {
                                    saleRate: 0,
                                    index: i
                                };
                            }
                        }
                    }
                }
            }

            var isFirst = true;
            var isFirstForPreviewBtn = true;

            // TaeSang. 2018.02.21
            // 연관메뉴에 미리보기 버튼 추가
            for (var i in _previewBtnList) {
                if (isFirstForPreviewBtn) {
                    contextMenu.addSeparateLine();
                    var previewTime = (parseInt(Number(_previewBtnList[i].previewRunTime) / 60) || "N") + "분 ";
                    var previewText = previewTime + (_previewBtnList[i].previewStartTime == "00:00:00" ? "미리보기" : "하이라이트 보기");
                    contextMenu.addTitle(previewText, "highLightTitle");
                    isFirstForPreviewBtn = false;
                }

                contextMenu.addButton(i);
                previewBtnList.push(_previewBtnList[i]);
                // 미리보기 버튼 개수 2개로 제한
                if (previewBtnList.length >= 2) break;
            }

            for (var i in _otherResolBtnList) {
                if (isFirst) {
                    contextMenu.addSeparateLine();
                    contextMenu.addTitle("다른 화질 구매");
                    isFirst = false;
                }

                contextMenu.addButton(i);
                otherResolBtnList.push(_otherResolBtnList[i]);
            }

            contextMenu.setEventListener(function (index) {
                if (previewBtnList.length > 0 && index < previewBtnList.length) {
                    // 미리보기 or 하이라이트 보기 프로세스 시작
                    // console.debug(optionData[previewBtnList[index].index]);
                    if (!isAuthorizedContent && !isAuthComplete && UTIL.isLimitAge(optionData[previewBtnList[index].index].prInfo)) {
                        openAdultAuthPopup(function (res) {
                            if (res) prePlayProcessForPreview(cateId, optionData[previewBtnList[index].index].optionList, optionData[previewBtnList[index].index]);
                        }, true);
                    } else prePlayProcessForPreview(cateId, optionData[previewBtnList[index].index].optionList, optionData[previewBtnList[index].index]);
                }
                else
                    openPaymentPopup(optionData[parseInt(otherResolBtnList[index - previewBtnList.length].index)], cateId, reqPathCd, function (res, data) {
                        if (res) {
                            VODManager.requestPlay(data, reqPathCd);
                        }
                    }, false, buttonUpdateCallback);
            });

            // 연관메뉴에서 포커스가 바뀔 때마다 미리보기의 시간 정보를 갱신함
            contextMenu.onFocusChange(function (index, callback) {
                if (previewBtnList.length > 0 && index < previewBtnList.length) {
                    var previewTime = ((parseInt(previewBtnList[index].previewRunTime) / 60) || "N") + "분 ";
                    var previewText = previewTime + (previewBtnList[index].previewStartTime == "00:00:00" ? "미리보기" : "하이라이트 보기");
                    callback(previewText);
                }
            });

            _this.div.append(contextMenu.getView());
        }

        /** 연관 메뉴 변경 */
        function updateContextMenu(idx) {

        }

        function hideContextMenu(_isOtherApp) {
            if (_isOtherApp) {
                log.printDbg("OtherApp Btn Focus !!");
                isHideContextMenu = true;
                _this.div.find(".detail_bottom .bottom_relation_area").css("opacity", "0");
            } else {
                isHideContextMenu = false;
                _this.div.find(".detail_bottom .bottom_relation_area").css("opacity", ".99");
            }
        }

        this.show = function (options) {
            log.printDbg("show");

            Layer.prototype.show.call(this);
            rowIdx = 2;
            if (options && options.resume) {
                if (bottomMenu && bottomMenu.isShow) {
                    bottomMenu.resumeTextAnimation();
                }
                setContentsData(contentsData, function () {
                    setBtnFocus(getBtnIndex(0));
                    refreshBtnBuyInfo();

                    if (contextMenu) {
                        contextMenu.getView().remove();
                        createContextMenu();
                    }
                    // 2017.08.16 Lazuli
                    // VOD 플러스 문구 업데이트 때문에 setContentsData 로직이 끝난 후에 실행되어야 함
                    DetailUtil.updateTvPoint(function (point) {
                        _this.div.find("#btn_txt_area").empty().append(
                            DetailUtil.getPointText(
                                point,
                                btnList[btnFocusIdx].getOptionList().vodPlus,
                                btnList[btnFocusIdx].getOptionList().pptBuyYn,
                                isConnerChk
                            ));
                    });
                    if (previewData != null && previewData.isPreview) showToast(previewData.previewText, "ICON");
                });
            } else {
                refreshBtnBuyInfo();
                // 2017.8.22 원제목이 길이 확인(길경우 한줄로 표시하는 기능)
                DetailUtil.originSubTitleLengthCheck(_this.div, "SINGLE");
                if (previewData != null && previewData.isPreview) showToast(previewData.previewText, "ICON");
            }

            KTW.managers.service.VoiceableManager.addNodeMatchListener(onNodeMatchListener);
        };

        function onNodeMatchListener (element) {
            log.printDbg("onNodeMatchListener");

            var layer = LayerManager.getLastLayerInStack();
            if (layer.id !== _this.id) {
                return;
            }

            var el = _$(element);

            if (el.attr("id") === "synopsis_btn_area") {
                rowIdx = 0;

                setBtnFocus(btnFocusIdx, true);

                setTimeout(function () {
                    _this.controlKey(KEY_CODE.OK);
                }, 0);
            }
            else if (el.hasClass("vod_btn_area")) {
                rowIdx = 2;

                var index = Number(el.attr("index"));
                btnFocusIdx = index;

                setBtnFocus(btnFocusIdx, true);

                setTimeout(function () {
                    _this.controlKey(KEY_CODE.OK);
                }, 0);
            }
        }

        /** 버튼정보 갱신 */
        function refreshBtnBuyInfo() {
            for (var i = 0; i < btnList.length; i++) {
                _this.div.find("#btn_item_area .vod_btn_area:eq(" + i + ") .vod_btn_line").css("width", _this.div.find("#btn_item_area .vod_btn_area:eq(" + i + ") .vod_btn_price").width() + "px");
            }
        }

        this.hide = function () {
            Layer.prototype.hide.call(this);
            contextMenu.close();

            KTW.managers.service.VoiceableManager.removeNodeMatchListener(onNodeMatchListener);
        };

        this.remove = function () {
            Layer.prototype.remove.call(this);
        };

        /** 키처리 */
        this.controlKey = function (key_code) {
            if (contextMenu.isOpen()) {
                return contextMenu.onKeyAction(key_code);
            }
            if (bottomMenu.controlKey(key_code)) {
                return true;
            }

            switch (key_code) {
                case KEY_CODE.UP:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, optData.contsId, optData.contsName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                    } else {
                        try {
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId, optData.contsId,
                                optData.contsName, btnList[btnFocusIdx]);
                        } catch (e) {
                        }
                    }

                    if (rowIdx > 0) {
                        rowIdx--;
                    }

                    if (!isNotice && isDetailSyn) {
                        rowIdx = 0;
                    } else if (isNotice && !isDetailSyn) {
                        rowIdx = isNoticeUnFocus ? 2 : 1;
                    } else if (!isNotice && !isDetailSyn) {
                        rowIdx = 2;
                    } else if (isNoticeUnFocus) {
                        rowIdx = 0;
                    }

                    setBtnFocus(btnFocusIdx, true);
                    return true;
                case KEY_CODE.DOWN:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, optData.contsId, optData.contsName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                        rowIdx++;
                    } else {
                        try {
                            var optData = btnList[btnFocusIdx].getOptionList().optionList[0];
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId, optData.contsId,
                                optData.contsName, btnList[btnFocusIdx]);
                        } catch (e) {
                        }
                        bottomMenu.show();
                    }
                    if (!isNotice || isNoticeUnFocus) {
                        rowIdx = 2;
                    }
                    setBtnFocus(btnFocusIdx, true);
                    return true;
                case KEY_CODE.LEFT:
                    if (rowIdx < 2) {
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, optData.contsId,
                                optData.contsName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                        LayerManager.historyBack();
                    } else {
                        var preFocusIdx = btnFocusIdx;
                        var btnIdx = getBtnIndex(-1);
                        if (btnIdx < 0) {
                            LayerManager.historyBack();
                        } else {
                            setBtnFocus(btnIdx);

                            if (preFocusIdx != btnFocusIdx) {
                                try {
                                    var optData = btnList[preFocusIdx].getOptionList().optionList[0];
                                    NavLogMgr.collectVODDetailPerchase(key_code, cateId, optData.contsId,
                                        optData.contsName, btnList[preFocusIdx]);
                                } catch (e) {
                                }
                            }
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (rowIdx < 2) {
                        return true;
                    }

                    var preFocusIdx = btnFocusIdx;
                    setBtnFocus(getBtnIndex(1));

                    if (preFocusIdx != btnFocusIdx) {
                        try {
                            var optData = btnList[preFocusIdx].getOptionList().optionList[0];
                            NavLogMgr.collectVODDetailPerchase(key_code, cateId, optData.contsId,
                                optData.contsName, btnList[preFocusIdx]);
                        } catch (e) {
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (rowIdx < 2) {
                        var selectedData;

                        if (btnList[btnFocusIdx].type === 99) {
                            // KT 버튼의 경우 카테고리 데이터를 이용하여 시놉시스팝업 편성
                            selectedData = cateData;
                        } else {
                            // 일반 구매 버튼의 경우
                            selectedData = btnList[btnFocusIdx].getOptionList().optionList[0].data;
                        }

                        var tmpData = {
                            title: selectedData.contsName || selectedData.itemName,
                            notice: selectedData.bulletin || "",
                            synopsis: selectedData.synopsis
                        };

                        LayerManager.activateLayer({
                            obj: {
                                id: rowIdx == 0 ? "VodSynopsisDetailPopup" : "VodNoticePopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    data: tmpData,
                                    isKids: isKidsCate
                                }
                            },
                            moduleId: "module.vod",
                            new: true,
                            visible: true
                        });
                        try {
                            NavLogMgr.collectVODDetail(key_code, cateId, optData.contsId, optData.contsName,
                                (rowIdx == 0 ? NLC.DETAIL_SYNOP : NLC.DETAIL_NOTICE), "");
                        } catch (e) {
                        }
                    } else {
                        btnList[btnFocusIdx].enterKeyAction();
                    }
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.CONTEXT:
                    contextMenu.open({wishFlag: wishObject.playCheckFlag});
                    return true;
                default:
                    return false;
            }
        };

        /** 버튼 Index 정의 */
        function getBtnIndex(amount) {
            log.printDbg("getBtnIndex btnFocusIdx = " + btnFocusIdx + ", amount = " + amount);

            var newIdx = btnFocusIdx + amount;

            if (newIdx < 0) {
                return -1;
            }

            if (!btnList[newIdx]) {
                return btnFocusIdx;
            }

            var connerYn = false;
            for (var i in btnList) {
                if (btnList[i].isCornerChk) {
                    connerYn = true;
                    break;
                }
            }

            if (connerYn && (btnList[newIdx].type == ButtonObject.TYPE.CORNER || btnList[newIdx].type == ButtonObject.TYPE.VODPPT)) {
                return getBtnIndex(amount + (amount > 0 ? 1 : -1));
            } else {
                return newIdx;
            }
        }

        /** 포커스 이동 */
        function setBtnFocus(btnIdx, noChangeBtnIdx) {
            log.printDbg("setBtnFocus btnIdx = " + btnIdx + " ,nochange =" + noChangeBtnIdx);

            btnFocusIdx = btnIdx;
            _this.div.find("#btnArea #btn_item_area div.vod_btn_area.btn_focus").removeClass("btn_focus");
            _this.div.find("#notice_infoArea").removeClass("focus");
            _this.div.find("#synopsis_btn_area").removeClass("focus");

            switch (rowIdx) {
                case 0:
                    _this.div.find("#synopsis_btn_area").addClass("focus");
                    DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), btnList[0].buyData);
                    break;
                case 1:
                    _this.div.find("#notice_infoArea").addClass("focus");
                    DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), btnList[0].buyData);
                    break;
                case 2:
                    _this.div.find("#btnArea #btn_item_area div.vod_btn_area").eq(btnIdx).addClass("btn_focus");
                    _this.div.find("#btnArea #btn_item_area div.vod_btn_area").css("-webkit-transform", "translateX(" + (-1256 * Math.floor(btnIdx / 4)) + "px)");
                    _this.div.find("#btn_arw_left").toggle(Math.floor(btnIdx / 4) > 0);
                    _this.div.find("#btn_arw_right").toggle(Math.floor(btnIdx / 4) != Math.floor((btnList.length - 1) / 4));
                    DetailUtil.couponIconUpdate(_this.div.find("#posterArea #poster_contentCoupon_img"), btnList[btnIdx].buyData);
                    break;
            }

            if (!noChangeBtnIdx) {
                setMetaData(btnIdx);
            }
        }

        /* 이건 뭐지? 쓰는 곳이 없어서 일단 주석처리 한다.
        function playVod(optionData) {
            console.debug(optionData);
            VODPointManager.getStbContentCouponInfo(function (res, couponData) {
                if (res) {
                    optionData.couponData = couponData;
                    prePlayProcessByButtonObj(optionData, cateId, reqPathCd, function (res, data) {
                        if (res) VODManager.requestPlay(data, reqPathCd);
                    }, null, null, null, true);
                }
            }, cateId, optionData.optionList[0].contsId, optionData.optionList[0].contsName, optionData.lessPrice);
        }
        */

        /** 최초 돔tree 생성 */
        function constructDom() {
            //Background
            var backgroundArea = _$("<div/>", {id: "background_area"});
            backgroundArea.append(
                _$("<div/>", {class: "bgBlurWrapper"}).append(_$("<img>", {class: "bgBlur posterImg_main"})),
                _$("<div/>", {class: "dim"}),
                _$("<img>", {class: "bImg"})
            );

            //포스터 & 별점
            var posterArea = _$("<div/>", {id: "posterArea"});
            posterArea.append(
                _$("<img>", {id: "main_poster", class: "posterImg_main"}),
                _$("<span>", {class: "basic_text"}).text("olleh tv 별점"),
                _$("<div/>", {id: "poster_starScore_area"}),
                _$("<img/>", {
                    id: "poster_contentCoupon_img",
                    src: window.modulePath + "resource/tag_poster_content_1.png"
                }),
                _$("<div/>", {id: "poster_contentInfo_area"}),
                _$("<img/>", {id: "poster_contentInfo_evtImg"}),
                _$("<img/>", {id: "poster_vodLock_img"})
            );

            // Info
            var infoArea = _$("<div/>", {id: "infoArea"});
            infoArea.append(
                _$("<div/>", {id: "maintitle_area"}),
                _$("<div/>", {id: "subtitle_area"}),
                _$("<div/>", {id: "subtitle_area_0"}),
                _$("<div/>", {id: "actor_area"}),
                _$("<div/>", {id: "synopsis_area"}),
                _$("<div/>", {id: "synopsis_btn_area", "data-voiceable-action": ""}).append(
                    _$("<img/>", {
                        class: "synopsis_btn_img",
                        src: window.modulePath + "resource/btn_detail_vod.png"
                    }),
                    _$("<span/>", {class: "synopsis_btn_text"}).text("+ 더보기")
                )
            );

            // 올레 모바일
            var ollehArea = _$("<div/>", {class: "ollehMobile_icon_area"});
            ollehArea.append(
                _$("<img/>", {class: "mobile_icon_img", src: window.modulePath + "resource/icon_detail_mobile.png"}),
                _$("<span/>", {class: "mobile_icon_text"}).text("올레 tv 모바일앱에서도 시청기간 동안 무료로 시청할 수 있습니다")
            );
            if (KTW.managers.service.ProductInfoManager.isBiz()) {
                ollehArea.css("visibility", "hidden");
            }

            var btnArea = _$("<div/>", {id: "btnArea"});
            btnArea.append(
                _$("<div/>", {id: "btn_item_area"}),
                _$("<div/>", {id: "btn_txt_area"})
            );

            // Tag를 미리 생성후 마지막 한번에 Append하여 속도 개선한다.
            // Append도 매번 호출이 아닌 Argument형태로 전달하면 그 개수에 맞게 Append를 수행한다
            _this.div.append(
                backgroundArea,
                posterArea,
                infoArea,
                _$("<div/>", {id: "notice_infoArea"}),
                ollehArea,
                btnArea,
                _$("<div/>", {id: "btn_arw_left"}),
                _$("<div/>", {id: "btn_arw_right"})
            );
        }

        /** 최초진입 시 View에 데이터 주입*/
        function setData(data, point) {
            log.printDbg("setData");

            pointData = point;

            // 성인컨텐츠 Lock 이미지 표시 여부
            if (isAuthorizedContent || data.adultOnlyYn === "Y" || !UTIL.isLimitAge(data.prInfo)) {
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "none");
            } else {
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "inherit");

                if (parseInt(data.posterUseType) == 2) {
                    _this.div.find("#posterArea #poster_vodLock_img").addClass("square");
                } else {
                    _this.div.find("#posterArea #poster_vodLock_img").removeClass("square");
                }
            }

            // EventImage
            if (data.wEvtImageUrlW3) {
                _this.div.find("#poster_contentInfo_evtImg").attr("src", data.wEvtImageUrlW3);
            }

            //Bottom 메뉴 구성
            _this.div.append(bottomMenu.createView(data.dispContsClass, false, reqPathCd, isKidsCate));

            var imgURLPath = "";
            // 2017.10.18 Y
            // 포스터 유형별 원본 이미지를 요청 후 css로 사이즈 지정해서 그리도록 수정
            switch (parseInt(data.posterUseType)) {
                case 0:
                    imgURLPath = data.imgUrl;
                    _this.div.find("img.posterImg_main").css({width: "329px", height: "470px"});
                    break;
                case 1:
                    imgURLPath = data.wideImgUrl;
                    break;
                case 2:
                    imgURLPath = data.squareImgUrl;
                    _this.div.find("img.posterImg_main").css({width: "329px", height: "329px"});
                    break;
                default:
                    imgURLPath = data.imgUrl;
                    _this.div.find("img.posterImg_main").css({width: "329px", height: "470px"});
                    break;
            }

            // 공지사항 여부?
            data.bulletin = data.bulletin.trim();
            isNotice = data.bulletin.length > 0;

            // 포스터 영역
            _this.div.find("img.posterImg_main").attr("src", imgURLPath).load(function () {
            }).on('error', function () {
                // 포스터 이미지가 없을 경우 예외 처리 (default 포스터 일경우 blur 처리 hide)
                _this.div.find("#background_area .bgBlur").css("display", "none");
                _$(this).attr("src", window.modulePath + "resource/defaultPoster/" + (data.dispContsClass == "01" ? (parseInt(data.posterUseType) == 2 ? "default_music2.png" : "default_music.png") : "default_poster.png"));
            });

            // 타이틀
            var mainTitle = (data.catName || data.itemName);
            if (window.UTIL.getTextLength(mainTitle, "RixHead B", 60, -0.7) >= 1067) {
                _this.div.find("#maintitle_area").css("width", "1140px");
                _this.div.find("#maintitle_area span").css("width", "1140px");
                mainTitle = HTool.ellipsis(mainTitle, "RixHead B", 60, 1067, "...", -0.5);
            }
            _this.div.find("#maintitle_area").empty().append("<span>" + mainTitle + "<img id='detailTitle_age_icon_single' src='" + window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(data.prInfo) + ".png'></span>");

            // 서브타이틀, 출연정보, 부가정보 등
            _this.div.find("#subtitle_area").empty().append(
                _$("<span/>", {class: "subtitle_text"}).append(DetailUtil.getSubTitle(data))
            );

            _this.div.find("#subtitle_area_0").empty().append(
                _$("<span/>", {class: "subtitle_text"}).append(DetailUtil.getSubTitle_0(data)),
                _$("<div/>", {class: "subtitle_icon_img"}).append(DetailUtil.getSubTitleIconImg(data, {
                    parentDiv: _this.div,
                    isLongIcon: isLongCon,
                    isConnerBuyChk: isConnerBuyChk,
                    isVodPptBuyChk: isVodPptBuyChk,
                    isSingle: isSingle
                }))
            );

            _this.div.find("#actor_area").empty().append(DetailUtil.getActorArea(_this.div, data));

            // 시놉시스
            _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                data: data.synopsis,
                type: "MCID",
                isNotice: isNotice,
                isKids: isKidsCate,
                callback: function (detailSync) {
                    // 더보기 버튼 활성화
                    isDetailSyn = detailSync;
                    _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                }
            }));

            if (is2LineTitle) {
                _this.div.find("#synopsis_area").css("margin-top", "15px");
            }

            // 배경 이벤트 이미지
            _this.div.find(".bImg").toggle(data.bImgUrl).attr("src", data.bImgUrl);
            _this.div.find("#notice_infoArea").empty().append(
                _$("<div/>", {class: "notice_icon"}),
                "<div id='notice_splitLine'></div>",
                "<div id='notice_contents'>" + data.bulletin + "</div>"
            );

            if (window.UTIL.splitText(data.bulletin, "RixHead L", 25, 960, -0.7).length == 1) {
                isNoticeUnFocus = true;
                _this.div.find("#notice_infoArea").addClass("noMore");
            } else {
                isNoticeUnFocus = false;
                _this.div.find("#notice_infoArea").removeClass("noMore");
            }

            _this.div.find("#posterArea #poster_starScore_area").empty().append(_$("<div/>", {id: "score_area"}));

            var tmpMark = data.mark;
            if (tmpMark && tmpMark > 0) {
                DetailUtil.setStarNumber(_this.div, tmpMark);
            }

            _this.div.find("#posterArea #poster_starScore_area").append(stars.getView(stars.TYPE.RED_26));
            stars.setRate(_this.div.find("#posterArea #poster_starScore_area"), tmpMark);

            _this.div.find("#notice_infoArea").css("visibility", (data.bulletin ? "inherit" : "hidden"));

            var ollehObj = DetailUtil.makeSeamlessText(data, savedSeamlessCode, 0);
            if (ollehObj.result !== false) {
                var ollehIcon = _this.div.find(".ollehMobile_icon_area");

                savedSeamlessCode = ollehObj.code;

                if (ollehObj.result === "") {
                    ollehIcon.css("visibility", "hidden");
                } else {
                    ollehIcon.css("visibility", "inherit");
                    ollehIcon.find(".mobile_icon_text").text(ollehObj.result);
                }
            }

            data.reqPathCd = reqPathCd;
            bottomMenu.setData({
                overseerName: cateData.overseerName,
                play: cateData.play,
                actor: cateData.actor,
                contsId: repVodData.contsId || repVodData.itemId,
                catId: cateId,
                recommendYn: repVodData.recommendYn,
                adultOnlyYn: repVodData.adultOnlyYn
            }, isAuthorizedContent || repVodData.adultOnlyYn === "Y");
            setBtnFocus(btnFocusIdx);
        }

        /** 상세화면 메타정보만 변경 (버튼 이동)*/
        function setMetaData(btnIdx) {
            log.printDbg("setMetaData btnIdx = " + btnIdx);

            var data, iconData, isCate, mainTitle, optionData;

            if (btnList[btnIdx].type == 0 || btnList[btnIdx].type == 1) {
                // 일반 버튼
                optionData = btnList[btnIdx].getOptionList().optionList;
                var repContent = getRepContData(optionData);
                data = repContent.data;
                iconData = makeIconData(optionData);
                mainTitle = btnList[btnIdx].type == 0 ? (cateData.catName || cateData.itemName) : (data.itemName || data.contsName || data.catName);
                isCate = false;
            } else {
                // KT버튼의 경우
                data = cateData;
                iconData = cateData;
                mainTitle = (cateData.catName || cateData.itemName);
                isCate = true;
            }

            isNotice = data.bulletin.length > 0;
            // 2017.10.18 Y
            // 포스터 유형별 원본 이미지를 요청 후 css로 사이즈 지정해서 그리도록 수정
            var imgURLPath = "";
            switch (parseInt(data.posterUseType)) {
                case 0:
                    imgURLPath = data.imgUrl;
                    _this.div.find("img.posterImg_main").css({width: "329px", height: "470px"});
                    break;
                case 1:
                    imgURLPath = data.wideImgUrl;
                    break;
                case 2:
                    imgURLPath = data.squareImgUrl;
                    _this.div.find("img.posterImg_main").css({width: "329px", height: "329px"});
                    break;
                default:
                    imgURLPath = data.imgUrl;
                    _this.div.find("img.posterImg_main").css({width: "329px", height: "470px"});
                    break;
            }
            _this.div.find("img.posterImg_main").attr("src", imgURLPath);

            if (!isAuthorizedContent && data.adultOnlyYn != "Y" && UTIL.isLimitAge(data.prInfo)) {
                _this.div.find("#posterArea #poster_vodLock_img").css("display", "inherit");

                if (parseInt(data.posterUseType) == 2) {
                    _this.div.find("#posterArea #poster_vodLock_img").addClass("square");
                } else {
                    _this.div.find("#posterArea #poster_vodLock_img").removeClass("square");
                }
            }
            if (data.wEvtImageUrlW3) {
                _this.div.find("#poster_contentInfo_evtImg").attr("src", data.wEvtImageUrlW3);
            }

            // 메인타이틀 Ellipsis
            if (window.UTIL.getTextLength(mainTitle, "RixHead B", 60, -0.7) >= 1067) {
                _this.div.find("#maintitle_area").css("width", "1140px");
                _this.div.find("#maintitle_area span").css("width", "1140px");
                mainTitle = HTool.ellipsis(mainTitle, "RixHead B", 60, 1067, "...", -0.5);
            }
            _this.div.find("#maintitle_area").empty().append("<span>" + mainTitle + "<img id='detailTitle_age_icon_single' src='" + window.modulePath + "resource/icon_age_detail_" + window.UTIL.transPrInfo(data.prInfo) + ".png'></span>");
            _this.div.find("#poster_contentInfo_area").empty();
            _this.div.find("#subtitle_area").empty().append(
                _$("<span/>", {class: "subtitle_text"}).append(DetailUtil.getSubTitle(data))
            );

            _this.div.find("#subtitle_area_0").empty().append(
                _$("<span/>", {class: "subtitle_text"}).append(DetailUtil.getSubTitle_0(data)),
                _$("<div/>", {class: "subtitle_icon_img"}).append(DetailUtil.getSubTitleIconImg(iconData, {
                    parentDiv: _this.div,
                    isLongIcon: isLongCon,
                    isConnerBuyChk: isConnerBuyChk,
                    isVodPptBuyChk: isVodPptBuyChk,
                    isSingle: isSingle
                }))
            );

            DetailUtil.originSubTitleLengthCheck(_this.div, "MCID");

            _this.div.find("#actor_area").empty().append(DetailUtil.getActorArea(_this.div, data));
            _this.div.find("#synopsis_area").html(DetailUtil.synopsisConverter({
                type: "MCID",
                data: data.synopsis,
                isNotice: isNotice,
                isKids: isKidsCate,
                callback: function (detailSync) {
                    // 더보기 버튼 활성화
                    isDetailSyn = detailSync;
                    _this.div.find("#infoArea #synopsis_btn_area").css("display", (isDetailSyn ? "block" : "none"));
                }
            }));
            _this.div.find(".bImg").toggle(!!data.bImgUrl).attr("src", data.bImgUrl);

            var infoArea = _this.div.find("#notice_infoArea");
            infoArea.empty().append(
                _$("<div/>", {class: "notice_icon"}),
                "<div id='notice_splitLine'></div>",
                "<div id='notice_contents'>" + data.bulletin + "</div>"
            );

            if (window.UTIL.splitText(data.bulletin, "RixHead L", 25, 960, -0.7).length == 1) {
                isNoticeUnFocus = true;
                infoArea.addClass("noMore");
            } else {
                isNoticeUnFocus = false;
                infoArea.removeClass("noMore");
            }

            _this.div.find("#posterArea #poster_starScore_area").empty().append(_$("<div/>", {id: "score_area"}));

            var tmpMark = data.mark;
            if (tmpMark && tmpMark > 0) {
                DetailUtil.setStarNumber(_this.div, tmpMark);
            }

            _this.div.find("#posterArea #poster_starScore_area").append(stars.getView(stars.TYPE.RED_26));
            stars.setRate(_this.div.find("#posterArea #poster_starScore_area"), tmpMark);

            _this.div.find("#btn_txt_area").empty();
            _this.div.find("#btn_txt_area").append(DetailUtil.getPointText(
                pointData,
                btnList[btnIdx].getOptionList().vodPlus,
                btnList[btnIdx].getOptionList().pptBuyYn,
                isConnerChk
            ));

            _this.div.find("#notice_infoArea").css("visibility", (data.bulletin ? "inherit" : "hidden"));

            var ollehObj = DetailUtil.makeSeamlessText(data, savedSeamlessCode, btnIdx);
            if (ollehObj.result !== false) {
                var ollehIcon = _this.div.find(".ollehMobile_icon_area");

                savedSeamlessCode = ollehObj.code;

                if (ollehObj.result === "") {
                    ollehIcon.css("visibility", "hidden");
                } else {
                    ollehIcon.css("visibility", "inherit");
                    ollehIcon.find(".mobile_icon_text").text(ollehObj.result);
                }
            }

            bottomMenu.updateData({
                overseerName: data.overseerName,
                play: data.play,
                actor: data.actor,
                contsId: isCate ? (repVodData.contsId || repVodData.itemId) : (data.contsId || data.itemId),
                catId: cateId,
                prInfo: data.prInfo,
                adultOnlyYn: data.adultOnlyYn,
                previewData: previewData
            }, DetailUtil.getAdditionalInfo(cateData, btnList));
        }

        function getRepContData(list) {
            log.printDbg("getRepContData");

            for (var i in list) {
                if (list[i].contsId === repVodData.contsId) {
                    list[i].isRepVod = true;
                    return list[i];
                }
            }

            list[0].isRepVod = true;
            return list[0];
        }

        /** 버튼 갱신 */
        function setButton() {
            _this.div.find("#btn_item_area").empty();

            for (var i = 0; i < btnList.length; i++) {
                _this.div.find("#btn_item_area").append(btnList[i].getView());
            }
        }

        /** 상세화면 아이콘정보 생성*/
        function makeIconData(optionList) {
            var result = {
                audioType: "",
                resolCd: ""
            };

            for (var i in optionList) {
                if (optionList[i].data.subtitleYn == "Y") {
                    result.subtitleYn = "Y";
                }

                if (optionList[i].data.isMultiAud == "Y") {
                    result.multiAudYn = "Y";
                }

                if (optionList[i].data.isDvdYn == "Y") {
                    result.isDvdYn = "Y";
                }

                if (optionList[i].data.isHdrYn == "Y" && CONSTANT.IS_HDR) {
                    result.isHdrYn = "Y";
                }

                if (optionList[i].data.audioType.indexOf("E-AC3") > -1 && result.audioType.indexOf("E-AC3") < 0) {
                    result.audioType += "E-AC3|";
                }

                if (optionList[i].data.audioType.indexOf("AC3") > -1 && result.audioType.indexOf("AC3") < 0) {
                    result.audioType += "AC3|";
                }

                if (optionList[i].data.resolCd.indexOf("UHD") > -1 && result.resolCd.indexOf("UHD") < 0) {
                    result.resolCd += "UHD|";
                }

                if (optionList[i].data.resolCd.indexOf("HD") > -1 && result.resolCd.indexOf("HD") < 0) {
                    result.resolCd += "HD|";
                }
            }

            if (result.audioType.length > 0) {
                result.audioType = result.audioType.substr(0, result.audioType.length - 1);
            }

            if (result.resolCd.length > 0) {
                result.resolCd = result.resolCd.substr(0, result.resolCd.length - 1);
            }
            return result;
        }


        /** 버튼 액션 정의 */
        function btnKeyAction() {
            log.printDbg("enterKeyAction");

            try {
                var optData = this.getOptionList().optionList[0];
                NavLogMgr.collectVODDetailPerchase(KEY_CODE.ENTER, cateId,
                    optData.contsId, optData.contsName, btnList[btnFocusIdx]);
            } catch (e) {
            }

            var that = this;
            if (!isAuthorizedContent && !isAuthComplete && UTIL.isLimitAge(this.buyData.prInfo)) {
                openAdultAuthPopup(function (res) {
                    if (res) {
                        isAuthComplete = true;
                        enterKeyActionImpl();
                    }
                }, true);
            } else {
                enterKeyActionImpl();
            }

            function enterKeyActionImpl() {
                that.getOptionList().execute(cateId, reqPathCd, function (res, resData) {
                    // 구매후 재생 혹은 이미 구매된 패키지 재생시 호출
                    if (res) {
                        resData.isAuthorizedContent = isAdultCate || resData.originData.data.adultOnlyYn === "Y";
                        VODManager.requestPlay(resData, reqPathCd);
                    }
                }, null, null, isAuthorizedContent || isAuthComplete, true, buttonUpdateCallback, false);
            }
        }

        /** 미구매 상품 구매완료 시 구매정보 갱신을 위한 콜백 */
        function buttonUpdateCallback(buyData) {
            log.printDbg("buttonUpdateCallback");

            OptionListFactory.updateBuyInfo(optionData, buyData, cateData);
            makeButtonList();
            setButton();
            setBtnFocus(getBtnIndex(0));
            refreshBtnBuyInfo();
        }
    };

    vod_detail_mcid_layer.prototype = new Layer();
    vod_detail_mcid_layer.prototype.constructor = vod_detail_mcid_layer;

    //Override create function
    vod_detail_mcid_layer.prototype.create = function (cbCreate) {
        Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    vod_detail_mcid_layer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    arrLayer["VodDetailMCIDLayer"] = vod_detail_mcid_layer;
})();
