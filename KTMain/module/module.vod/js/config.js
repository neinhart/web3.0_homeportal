/**
 * Created by Lazuli on 2017-02-22.
 */

VOD = {
    HTTP: {
        CURATION: {
            LIVE_URL: "http://profile.ktipmedia.co.kr:7003/wasProfile",
            TEST_URL: "http://125.140.114.151:7002/wasProfile"
        },
        AMOC: {
            HTTP_URL: "http://webui.ktipmedia.co.kr:8080",
            HTTPS_URL: "https://webui.ktipmedia.co.kr",
            WAPI_URL:"http://wapi.ktipmedia.co.kr"
        },
        RECOMMEND: {
            LIVE_URL: "http://recommend.ktipmedia.co.kr:7002/",
            TEST_URL: "http://125.140.114.151:7002/"
        },
        KTPG: {
            LIVE_URL: "http://ktpay.kt.com:10088/webapi/json/stb/",
            TEST_URL: "http://221.148.188.212:10088/webapi/json/stb/"
        },
        LUPIN: {
            LIVE_URL: "https://ktpay.kt.com/adaptor/ktpg/dcb/",
            TEST_URL: "http://etbips.olleh.com:10088/adaptor/ktpg/dcb/"
        },
        SMLS: {
            LIVE_URL: "http://222.122.121.80:8080",
            TEST_URL: "http://203.255.241.154:8080"
        }
    }
};

BUY_TYPE = {
    CORNER : 0,
    SERIES : 1,
    SERIES_SINGLE : 2,
    SINGLE : 3,
    CORNER_SINGLE : 4,
    CORNER_SERIES : 5,
    CORNER_SERIES_SINGLE : 6,
    VODPPT : 7,
    VODPPT_SINGLE : 8,
    VODPPT_SERIES : 9,
    VODPPT_SERIES_SINGLE : 10,
    VODPLUS : 11
};