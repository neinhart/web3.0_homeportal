/**
 * Created by ksk91_000 on 2016-09-26.
 */
window.CornerMiniGuide = function() {
    this.data = [];
    this.vodPlaybar = new VODPlayBar();
    this.thumbnailList;

    var _this = this;
    var viewArea;
    var currentViewMode = 0;
    var changeViewTimeout = null;
    var NORMAL = 0, PREVIEW = 1, THUMBNAIL = 2;

    var curIdx = -1, pageLength;
    var page = -1, focus;
    var cornerType;

    this.setTitle = function() {

    };

    this.setThumbnailData = function(data) {
        this.thumbnailList = data;
    };

    this.setTime = function() {
        if (cornerType == "002") { //몰아보기
            this.vodPlaybar.setTime(VODManager.getPlayTimeForCorner(), VODManager.getCurrentPositionForCorner());
        } else {
            this.vodPlaybar.setTime(VODManager.getPlayTime(), VODManager.getCurrentPosition());
        }
    };

    // 프로그레스바 위치 변경
    this.setProgress = function(currentTime) {
        if (cornerType == "002") {
            this.vodPlaybar.setProgress(VODManager.getPlayTimeForCorner());
        } else {
            this.vodPlaybar.setProgress(currentTime);
        }
    };

    this.setPlayState = function(state) {

    };

    this.setCornerData = function(data) {
        this.data = data;
        cornerType = data.corner_grp_list[0].cornergrptype;
        curIdx = -1;
        page = -1;
        this.setCurIdx();
    };

    this.setCurIdx = function() {
        var data = this.data;
        var tmp = getCurVodIdx(data);
        this.setFocus(tmp);
        if (tmp != curIdx) {
            curIdx = tmp;
            pageLength = Math.ceil(data.items.length / 6);

            this.div.find(".title_area .corner_title").text("'" + data.corner_grp_list[0].cornergrpnm + (cornerType == "002" ? "' 몰아보기" : "' 골라보기 "));
            this.div.find(".title_area .conts_title").text(getText(data.items[curIdx]));

            this.div.find(".contents_area .left_arrow").toggleClass("hide", !(page > 0));
            this.div.find(".contents_area .right_arrow").toggleClass("hide", !(page < pageLength - 1));
            this.div.find(".down_arw").text(VODManager.getCurrentVODInfo().contsName);

            this.div.find(".sep_line").toggle(cornerType == "002");
            this.div.find(".conts_title").toggle(cornerType == "002");
        }
    };

    this.setFocus = function(_focus) {
        focus = _focus;
        this.setPage(Math.floor(focus / 6));
        this.div.find(".item_area.focus").removeClass("focus");
        this.div.find(".item_area").eq(focus % 6).addClass("focus");
    };

    this.setPage = function(_page) {
        if (page == _page) return;
        page = _page;
        for (var i = 0; i < 6; i++) setThumbnail(this.div.find(".contents_area .item_area").eq(i), this.data.items[page * 6 + i]);
        this.div.find(".contents_area .left_arrow").toggleClass("hide", !(page > 0));
        this.div.find(".contents_area .right_arrow").toggleClass("hide", !(page < pageLength - 1));
    };

    function setThumbnail(div, data) {
        if (data) {
            div.find(".poster_image").attr("src", data.file_url);
            div.find(".title").text(getText(data));
            div.show();
        } else {
            div.hide();
        }
    }

    function getText(data) {
        return cornerType == "002" ? data.cornername.substring(0, 10) : data.cornerstart.split(".")[0];
    }

    this.getType = function() {
        return "Corner";
    };

    function getCurVodIdx(data) {
        for (var i = 0; i < data.items.length; i++)
            if (data.items[i].content_id == VODManager.getCurrentVODInfo().currentCorner.content_id && data.items[i].file_id == VODManager.getCurrentVODInfo().currentCorner.file_id) return i
    }

    this.onKey = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (currentViewMode != THUMBNAIL) {
                    this.setShowTimeout();
                    this.changeView(THUMBNAIL);
                }
                return true;
            case KEY_CODE.DOWN:
                if (currentViewMode == THUMBNAIL) {
                    this.setShowTimeout();
                    this.changeView(PREVIEW);
                }
                else {
                    var that = this;
                    var prevFocusIdx = parseInt(focus);
                    MiniGuideManager.show(true);
                    this.setFocus(prevFocusIdx);
                    LayerManager.activateLayer({
                        obj: {
                            id: "VodSelectEpisodePopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: that.thumbnailList
                        },
                        moduleId: "module.vod",
                        visible: true
                    });
                }
                return true;
            case KEY_CODE.LEFT:
                if (currentViewMode == THUMBNAIL) {
                    if (focus > 0) this.setFocus(focus - 1);
                    this.setShowTimeout();
                    return true;
                } else return false;
            case KEY_CODE.RIGHT:
                if (currentViewMode == THUMBNAIL) {
                    if (focus < this.data.items.length - 1) this.setFocus(focus + 1);
                    this.setShowTimeout();
                    return true;
                } else return false;
                return true;
            case KEY_CODE.ENTER:
                if (currentViewMode == THUMBNAIL) {
                    VODManager.requestPlayCorner(focus, function() {
                        if (VODInfoManager.getCurrentCornerVod().content_id == VODManager.getCurrentVODInfo().assetId) {
                            _this.setShowTimeout();
                            _this.changeView(PREVIEW);
                        }
                        else MiniGuideManager.hide();
                    });
                } else this.hide();
                return true;
            case KEY_CODE.BACK:
                this.hide();
                return true;
            default:
                return false;
        }
    };

    this.changeView = function(_viewMode, _noTimeout) {
        viewArea = this.div;
        currentViewMode = _viewMode;
        this.div.find(".down_arw").css("display", "inherit");
        this.clearViewTimeout();
        this.vodPlaybar.invertProgress(false);
        switch (_viewMode) {
            case NORMAL:
                viewArea.addClass("normalMode");
                viewArea.removeClass("previewMode");
                break;
            case PREVIEW:
                viewArea.removeClass("normalMode");
                viewArea.addClass("previewMode");
                if (!_noTimeout) changeViewTimeout = setTimeout(function() {
                    _this.changeView(NORMAL);
                }, 2E3);
                break;
            case THUMBNAIL:
                this.div.find(".down_arw").css("display", "none");
                viewArea.removeClass("normalMode");
                viewArea.removeClass("previewMode");
                this.setCurIdx();
                this.vodPlaybar.invertProgress(true);
                break;
        }
    };

    this.clearViewTimeout = function() {
        clearTimeout(changeViewTimeout);
        changeViewTimeout = null;
    };

    this.create();
};

CornerMiniGuide.prototype = new MiniGuide();
CornerMiniGuide.prototype.constructor = CornerMiniGuide;

CornerMiniGuide.prototype.create = function() {
    MiniGuide.prototype.create.call(this);
    this.div.addClass("cornerMiniGuide");
    this.div.append(_$("<div/>", {class: "contents_area"}).append("<div class='left_arrow'/><div class='thumb_list'/><div class='right_arrow'/>"));
    this.div.append(_$("<div/>", {class: "title_area"}).append("<div class='corner_title'/><div class='sep_line'/><div class='conts_title'/>"));
    var itemArea = "<div class='item_area'><div class='poster_area'><img class='poster_image'/><div class='sdw_image'/></div><span class='title'/></div>";
    this.div.find(".contents_area .thumb_list").append(itemArea + itemArea + itemArea + itemArea + itemArea + itemArea);
    this.div.append(this.vodPlaybar.getView());
    this.div.append(_$("<div/>", {class: 'down_arw'}));
    this.div.append(_$(
        "<div id='context_area'>" +
        "<div class='contextIcon'></div>" +
        "<span class='contextText'>자막</span>" +
        "<div class='vertical_bar'></div>" +
        "<span class='contextText2'>음성 변경</span>" +
        "</div>"
    ));
};

CornerMiniGuide.prototype.init = function() {
    // this.changeState(VODMiniGuide.STATE.NORMAL_VIEW);
};

CornerMiniGuide.prototype.show = function(alwaysShow) {
    MiniGuide.prototype.show.apply(this, arguments);
    this.setCurIdx();
    this.vodPlaybar.onFocused();
    this.clearViewTimeout();
    this.changeView(1, alwaysShow);
    this.setTime();
};

CornerMiniGuide.prototype.hide = function() {
    MiniGuide.prototype.hide.apply(this, arguments);
    this.changeView(1);
    this.clearViewTimeout();
};

CornerMiniGuide.prototype.getView = function() {
    return this.div;
};

CornerMiniGuide.prototype.onKeyAction = function(keyCode) {
    if (!this.isShowing() || VODManager.isADMode()) return;
    this.clearViewTimeout();
    return this.onKey(keyCode);
};