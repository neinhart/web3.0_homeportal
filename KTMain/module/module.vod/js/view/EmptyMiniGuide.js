window.EmptyMiniGuide = function () {};

EmptyMiniGuide.prototype.create = function () {
};

EmptyMiniGuide.prototype.show = function(alwaysShow) {
};

EmptyMiniGuide.prototype.hide = function() {
};

EmptyMiniGuide.prototype.getView = function() {
};

EmptyMiniGuide.prototype.isShowing = function() {
};

EmptyMiniGuide.prototype.setTitle = function() {
};

EmptyMiniGuide.prototype.setTime = function() {
};

EmptyMiniGuide.prototype.setProgress = function() {
};

EmptyMiniGuide.prototype.setThumbnailData = function() {
};

