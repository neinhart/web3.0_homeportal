/**
 * Created by ksk91_000 on 2016-09-26.
 */
window.MiniGuide = function () {
    this.div;
    this.showing;
};

MiniGuide.prototype.create = function () {
    this.div = _$("<div/>", {id:"miniguide_area"});
};

MiniGuide.prototype.init = function () {};

MiniGuide.prototype.show = function(alwaysShow) {
    this.div.css("display", "inherit");
    this.showing = true;
    this.clearShowTimeout();
    if(!alwaysShow) this.setShowTimeout();
    MiniGuideManager.hideMiniIndicatorBg();
};

MiniGuide.prototype.clearShowTimeout = function() {
    if(this.hTimeout){
        clearTimeout(this.hTimeout);
        this.hTimeout = null;
    }
};

MiniGuide.prototype.setShowTimeout = function () {
    var that = this;
    this.clearShowTimeout();
    this.hTimeout = setTimeout(function() { that.hide.call(that); }, 5000);
};

MiniGuide.prototype.hide = function() {
    this.div.css("display", "none");
    this.showing = false;
    this.clearShowTimeout();
    MiniGuideManager.showMiniIndicatorBg();
};

MiniGuide.prototype.getView = function() {
    return this.div;
};

MiniGuide.prototype.isShowing = function() {
    return this.showing;
};