/**
 * Created by Yun on 2017-01-11.
 */

window.MVMiniGuide = function() {
    var _this = this;
    this.mvPlayerBar = new MVPlayBar();
    this.mvRelationMenu = new MVPlayBar_RelationMenu();
    this.mvRelationVOD = new RelationVOD();

    this.state = MVMiniGuide.STATE.NORMAL_VIEW;
    this.compList = [];
    this.currentComp = null;

    this.playList = [];
    this.curIdx = 0;

    this.focusIdx = 0;

    this.setTitle = function() {
        this.mvPlayerBar.setTitle();
    };

    this.setTime = function() {
        if (this.mvPlayerBar.getCurruntIndex() == this.mvPlayerBar.getFocusIndex()) this.mvPlayerBar.setTime(VODManager.getPlayTime(), VODManager.getCurrentPosition());
        // this.mvPlayerBar.setTime(VODManager.getPlayTime(), VODManager.getCurrentPosition());
    };

    this.setProgress = function(currentTime) {
        this.mvPlayerBar.setProgress(currentTime);
    };

    this.setPlayState = function(state) {
        this.mvPlayerBar.changeMiniIndicator(state);
    };

    this.setRelationVod = function(catId, hasAdult) {
        this.mvRelationVOD.setRelationVod(catId, hasAdult, true);
    };

    this.getType = function() {
        return "MV";
    };

    this.onKeyForMVPlayerView = function(keyCode) {
        this.setShowTimeout();
        switch (keyCode) {
            case KEY_CODE.UP:
                return true;
            case KEY_CODE.DOWN:
                if (this.mvPlayerBar.getCurruntIndex() == this.mvPlayerBar.getFocusIndex()) this.changeState(MVMiniGuide.STATE.RELATED_VIEW);
                return true;
            case KEY_CODE.ENTER:
                this.mvPlayerBar.setProgress(VODManager.getCurrentPosition());
                if (this.mvPlayerBar.getCurruntIndex() == this.mvPlayerBar.getFocusIndex()) {
                    VODManager.pause();
                } else {
                    MiniGuideManager.hide();
                    VODManager.requestPlayListWithIdx(this.mvPlayerBar.getFocusIndex(), true);
                }
                return true;
            default:
                return this.mvPlayerBar.onKeyAction(keyCode);
        }
    };

    this.onKeyForMVRelatedMenu = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.DOWN:
                return true;
            case KEY_CODE.UP:
                this.changeState(MVMiniGuide.STATE.NORMAL_VIEW);
                return true;
            case KEY_CODE.LEFT:
                this.mvRelationMenu.buttonFocusRefresh(this.focusIdx = HTool.getIndex(this.focusIdx, -1, this.mvRelationVOD.hasRecommendData() ? 3 : 2));
                return true;
            case KEY_CODE.RIGHT:
                this.mvRelationMenu.buttonFocusRefresh(this.focusIdx = HTool.getIndex(this.focusIdx, 1, this.mvRelationVOD.hasRecommendData() ? 3 : 2));
                return true;
            case KEY_CODE.ENTER:
                switch (this.focusIdx) {
                    case 0:
                        // 곡 설명
                        LayerManager.activateLayer({
                            obj: {
                                id: "MVSynopsisPopup",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    vodData: VODManager.getCurrentOriginVODInfo()
                                }
                            },
                            moduleId: "module.vod",
                            new: true,
                            visible: true
                        });
                        break;
                    case 1:
                        // 담기
                        this.playList[this.curIdx].itemType = 2;
                        LayerManager.activateLayer({
                            obj: {
                                id: "MvPlayList",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: this.playList[this.curIdx]
                            },
                            moduleId: "module.vod",
                            visible: true,
                            new: true
                        });
                        break;
                    case 2:
                        if (this.mvRelationVOD.hasRecommendData()) this.changeState(MVMiniGuide.STATE.RELATED_VOD);
                        break;
                }
                return true;
            default:
                return false;
        }
    };

    this.onKeyForMVRelatedVOD = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
            case KEY_CODE.BACK:
                this.changeState(MVMiniGuide.STATE.RELATED_VIEW);
                return true;
            default:
                return this.mvRelationVOD.onKeyAction(keyCode);
        }
    };

    this.changeState = function(state) {
        var prevState = this.state.toString();
        this.state = state;
        switch (state) {
            case MVMiniGuide.STATE.NORMAL_VIEW:
                this.mvPlayerBar.mvPlayBarShow();
                this.mvPlayerBar.onFocused();
                this.mvRelationMenu.hide();
                this.mvRelationVOD.onBlurred();
                this.mvRelationVOD.hide();
                this.setShowTimeout();
                break;
            case MVMiniGuide.STATE.RELATED_VIEW:
                this.playList = VODInfoManager.getPlayList();
                this.curIdx = VODInfoManager.getCurrentPlayListIdx();
                this.mvPlayerBar.mvPlayBarShow();
                this.mvPlayerBar.onBlurred();
                this.mvRelationMenu.show();
                if (parseInt(prevState) == MVMiniGuide.STATE.RELATED_VOD) this.mvRelationMenu.buttonFocusRefresh(this.focusIdx);
                else this.mvRelationMenu.buttonFocusRefresh(this.focusIdx = 0);
                this.mvRelationVOD.onBlurred();
                this.mvRelationVOD.hide();
                this.clearShowTimeout();
                break;
            case MVMiniGuide.STATE.RELATED_VOD:
                this.mvPlayerBar.mvPlayBarHide();
                this.mvRelationMenu.hide();
                this.mvRelationVOD.show();
                this.mvRelationVOD.onFocused();
                this.clearShowTimeout();
                break;
        }
    };

    this.create();
};

MVMiniGuide.prototype = new MiniGuide();
MVMiniGuide.prototype.constructor = MVMiniGuide;

MVMiniGuide.prototype.create = function() {
    MiniGuide.prototype.create.call(this);
    var _this = this;
    this.compList.push(this.mvPlayerBar);
    this.compList.push(this.mvRelationMenu);
    this.compList.push(this.mvRelationVOD);

    this.compList.forEach(function(comp) {
        comp.onBlurred();
        _this.div.append(comp.getView());
    });

    this.currentComp = 0;
    this.compList[this.currentComp].onFocused();
};

MVMiniGuide.prototype.init = function() {
    this.changeState(MVMiniGuide.STATE.NORMAL_VIEW);
    this.show();
};

MVMiniGuide.prototype.show = function(alwaysShow, noRefresh) {
    MiniGuide.prototype.show.apply(this, arguments);
    this.setTime();
    this.mvPlayerBar.setProgress(VODManager.getCurrentPosition());
    if (!noRefresh) this.mvPlayerBar.refreshListData(this.mvPlayerBar.getCurruntIndex());
    if (this.mvRelationVOD.hasRecommendData()) {
        this.mvPlayerBar.setHasRecommendData(true);
        this.mvRelationMenu.setHasRecommendData(true);
    }
    else {
        this.mvPlayerBar.setHasRecommendData(false);
        this.mvRelationMenu.setHasRecommendData(false);
    }
};

MVMiniGuide.prototype.hide = function() {
    this.state = MVMiniGuide.STATE.NORMAL_VIEW;
    this.changeState(MVMiniGuide.STATE.NORMAL_VIEW);
    MiniGuide.prototype.hide.apply(this, arguments);
};

MVMiniGuide.prototype.getView = function() {
    return this.div;
};

MVMiniGuide.prototype.onKeyAction = function(keyCode) {
    if (!this.isShowing()) {
        var targetMV = 0;
        var mvPlayerBar = this.mvPlayerBar;
        if (keyCode == KEY_CODE.LEFT) {
            if (mvPlayerBar.getCurruntIndex() > 0 || (VODInfoManager.isPlayListLoop() && mvPlayerBar.playList.length > 2)) {
                targetMV = mvPlayerBar.getCurruntIndex() - 1;
                this.show();
                if (VODInfoManager.isPlayListLoop()) {
                    if (targetMV < 0) {
                        if (mvPlayerBar.playList.length > 2) targetMV = mvPlayerBar.playList.length - 1;
                        else targetMV = 0;
                    }
                } else {
                    if (targetMV < 0) targetMV = 0;
                }
                this.mvPlayerBar.refreshListData(targetMV);
            }
            return true;
        } else if (keyCode == KEY_CODE.RIGHT) {
            /**
             * [dj.son] [WEBIIIHOME-3638] playbar 가 hide 상태일때, 현재 재생중인 컨텐츠 index 를 비교하도록 수정
             */
            if ((mvPlayerBar.playList.length > 1 && mvPlayerBar.curIdx < mvPlayerBar.playList.length - 1) || (VODInfoManager.isPlayListLoop() && mvPlayerBar.playList.length > 2)) {
                targetMV = this.mvPlayerBar.getCurruntIndex() + 1;
                this.show();
                if (VODInfoManager.isPlayListLoop()) {
                    if (targetMV > mvPlayerBar.playList.length - 1) {
                        if (mvPlayerBar.playList.length > 2) targetMV = 0;
                        else targetMV = mvPlayerBar.playList.length - 1;
                    }
                } else {
                    if (targetMV > mvPlayerBar.playList.length - 1) targetMV = mvPlayerBar.playList.length - 1;
                }
                this.mvPlayerBar.refreshListData(targetMV);
            }
            return true;
        }
        return false;
    } else if (VODManager.isADMode()) return;
    switch (this.state) {
        case MVMiniGuide.STATE.NORMAL_VIEW:
            return this.onKeyForMVPlayerView(keyCode);
        case MVMiniGuide.STATE.RELATED_VIEW:
            return this.onKeyForMVRelatedMenu(keyCode);
        case MVMiniGuide.STATE.RELATED_VOD:
            return this.onKeyForMVRelatedVOD(keyCode);
    }
};

Object.defineProperty(MVMiniGuide, "STATE", {
    value: {NORMAL_VIEW: 0, RELATED_VIEW: 1, RELATED_VOD: 2},
    writable: false,
    configurable: false
});