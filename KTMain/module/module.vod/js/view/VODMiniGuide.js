/**
 * Created by ksk91_000 on 2016-09-26.
 */
window.VODMiniGuide = function () {
    this.vodPlaybar = new VODPlayBar();
    this.thumbnailList = new ThumbnailList();
    this.relationVod = new RelationVOD();

    this.state = VODMiniGuide.STATE.NORMAL_VIEW;
    this.compList = [];
    this.currentComp = null;
    var hasThumbnail;

    this.setTitle = function () {
        this.vodPlaybar.setTitle();
    };

    this.setTime = function () {
        this.vodPlaybar.setTime(VODManager.getPlayTime(), VODManager.getCurrentPosition());
    };

    // 프로그레스바 위치 변경
    this.setProgress = function (currentTime) {
        this.vodPlaybar.setProgress(currentTime);
    };

    this.setThumbnailPosition = function (time) {
        this.vodPlaybar.setIconPosition(time);
        this.setShowTimeout();
    };

    this.setThumbnailData = function (data) {
        this.thumbnailList.setData(data);
        this.thumbnailList.setIndicatorArea(this.div.find("#thumb_indicator_area"));
        hasThumbnail = !!(data && data.length > 0);
        this.div.find("#thumb_indicator_area").css("visibility", hasThumbnail ? "inherit" : "hidden");
    };

    this.setThumbnailFocus = function (thumbData) {
        this.thumbnailList.setThumbnailFocus(thumbData);
    };

    this.setRelationVod = function (catId, hasAdult) {
        this.relationVod.setRelationVod(catId, hasAdult);
    };

    this.getType = function () {
        return "VOD";
    };

    this.onKeyForThumbnailView = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (!this.thumbnailList.hasCorner()) this.changeState(VODMiniGuide.STATE.NORMAL_VIEW);
                else this.thumbnailList.onKeyAction(keyCode);
                return true;
            case KEY_CODE.DOWN:
                this.changeState(VODMiniGuide.STATE.NORMAL_VIEW);
                return true;
            case KEY_CODE.BACK:
                this.hide();
                return true;
            case KEY_CODE.ENTER:
                this.changeState(VODMiniGuide.STATE.NORMAL_VIEW);
            default:
                return this.thumbnailList.onKeyAction(keyCode);
        }
    };

    this.onKeyForNormalView = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (hasThumbnail) {
                    this.changeState(VODMiniGuide.STATE.THUMBNAIL_VIEW);
                    return true;
                } else return false;
            case KEY_CODE.DOWN:
                if (this.relationVod.hasRecommendData()) {
                    this.changeState(VODMiniGuide.STATE.RELATION_VIEW);
                    return true;
                } else return false;
            case KEY_CODE.ENTER:
            case KEY_CODE.BACK:
                this.hide();
                return true;
            default:
                return this.vodPlaybar.onKeyAction(keyCode);
        }
    };

    this.onKeyForRelationView = function (keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
            case KEY_CODE.BACK:
                this.changeState(VODMiniGuide.STATE.NORMAL_VIEW);
                return true;
            case KEY_CODE.DOWN:
                return true;
            case KEY_CODE.CONTEXT :
                return true;
            default:
                return this.relationVod.onKeyAction(keyCode);
        }
    };

    this.changeState = function (state) {
        this.state = state;
        this.vodPlaybar.invertProgress(false);
        switch (state) {
            case VODMiniGuide.STATE.THUMBNAIL_VIEW:
                this.thumbnailList.show();
                this.vodPlaybar.show(true, true);
                this.thumbnailList.onFocused();
                this.vodPlaybar.onBlurred();
                this.vodPlaybar.hideTitle();
                this.relationVod.onBlurred();
                this.thumbnailList.show(true);
                this.relationVod.hide();
                this.div.find("#context_area").show();
                this.setShowTimeout();
                this.div.css("z-index", "");
                this.vodPlaybar.invertProgress(true);
                break;
            case VODMiniGuide.STATE.NORMAL_VIEW:
                this.thumbnailList.show();
                this.vodPlaybar.show(true, true);
                this.thumbnailList.onBlurred();
                this.vodPlaybar.onFocused();
                this.relationVod.onBlurred();
                this.thumbnailList.setCloseTimer();
                if (this.relationVod.hasRecommendData()) this.relationVod.show();
                this.div.find("#context_area").show();
                this.setShowTimeout();
                this.div.css("z-index", "");
                break;
            case VODMiniGuide.STATE.RELATION_VIEW:
                this.thumbnailList.hide();
                this.vodPlaybar.hide();
                if (this.relationVod.hasRecommendData()) {
                    this.relationVod.show();
                    this.relationVod.onFocused();
                }
                this.div.find("#thumb_indicator_area").hide();
                this.div.find("#context_area").hide();
                this.clearShowTimeout();
                this.div.css("z-index", "5");
                break;
        }
    };

    this.create();
};

VODMiniGuide.prototype = new MiniGuide();
VODMiniGuide.prototype.constructor = VODMiniGuide;

VODMiniGuide.prototype.create = function () {
    MiniGuide.prototype.create.call(this);
    var _this = this;

    this.compList.push(this.thumbnailList);
    this.compList.push(this.vodPlaybar);
    this.compList.push(this.relationVod);

    this.compList.forEach(function (comp) {
        comp.onBlurred();
        _this.div.append(comp.getView());
    });
    this.div.append(_$("<div/>", {id: "thumb_indicator_area"}).text("구간점프"));
    this.div.append(_$(
        "<div id='context_area'>" +
        "<span class='contextText2'>음성 변경</span>" +
        "<div class='vertical_bar'></div>" +
        "<span class='contextText'>자막</span>" +
        "<div class='contextIcon'></div>" +
        "</div>"
    ));
    this.currentComp = 1;
    this.compList[this.currentComp].onFocused();
};

VODMiniGuide.prototype.init = function () {
    this.changeState(VODMiniGuide.STATE.NORMAL_VIEW);
};

VODMiniGuide.prototype.show = function (alwaysShow) {
    if (this.state == VODMiniGuide.STATE.RELATION_VIEW) return;
    this.div.find("#context_area").show();
    MiniGuide.prototype.show.apply(this, arguments);
    if (this.state == VODMiniGuide.STATE.NORMAL_VIEW) {
        if (this.state != 2) this.thumbnailList.show(alwaysShow);
        if (this.state != 0 && this.relationVod.hasRecommendData()) this.relationVod.show();
        else this.relationVod.hide();
    }
    this.setTime();
};

VODMiniGuide.prototype.hide = function () {
    this.vodPlaybar.invertProgress(false);
    this.state = VODMiniGuide.STATE.NORMAL_VIEW;
    this.thumbnailList.show();
    this.vodPlaybar.show(true, true);
    this.relationVod.onBlurred();
    this.thumbnailList.onBlurred();
    this.vodPlaybar.onFocused();
    this.thumbnailList.clearCloseTimer();
    this.div.css("z-index", "");
    MiniGuide.prototype.hide.apply(this, arguments);
};

VODMiniGuide.prototype.getView = function () {
    return this.div;
};

VODMiniGuide.prototype.onKeyAction = function (keyCode) {
    if (!this.isShowing() || VODManager.isADMode()) return;

    switch (this.state) {
        case VODMiniGuide.STATE.THUMBNAIL_VIEW:
            return this.onKeyForThumbnailView(keyCode);
        case VODMiniGuide.STATE.NORMAL_VIEW:
            return this.onKeyForNormalView(keyCode);
        case VODMiniGuide.STATE.RELATION_VIEW:
            return this.onKeyForRelationView(keyCode);
    }
};

Object.defineProperty(VODMiniGuide, "STATE", {
    value: {THUMBNAIL_VIEW: 0, NORMAL_VIEW: 1, RELATION_VIEW: 2},
    writable: false,
    configurable: false
});