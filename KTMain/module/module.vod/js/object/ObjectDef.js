/**
 * Created by ksk91_000 on 2017-05-27.
 */
window.OptionObject = function (data, catData, resolIdx, resolCd, contsName, contsId, ltFlag, price, vatPrice, period,
                                mobile, isContCoupon, voucherYn, previewYn, previewRunTime, previewStartTime, seamlessCode) {

    this.data = data;
    this.catData = catData;
    this.resolIdx = resolIdx;
    this.resolCd = resolCd;
    this.contsName = contsName;
    this.contsId = contsId;
    this.ltFlag = ltFlag;
    this.price = price - 0;
    this.vatPrice = vatPrice - 0;
    this.period = period;
    this.mobile = mobile;
    this.prInfo = data ? data.prInfo : 0;
    this.resolName = getResolName(data, resolCd);
    this.ltName = ltFlag === true || ltFlag === "Y" ? "장기" : "";
    this.cCSersId = catData ? catData.cCSersId : null;
    this.isContCoupon = isContCoupon;
    this.voucherYn = voucherYn;
    this.previewYn = previewYn; // 미리보기
    this.previewRunTime = previewRunTime; // 미리보기 재생 시간
    this.previewStartTime = previewStartTime; // 미리보기 시작 시간
    this.seamlessCode = seamlessCode; // 1:n 페어링 코드

    function getResolName(data, resolCd) {
        if (!data) {
            return "";
        }
        if (data.isHdrYn == "Y") {
            return "HDR";
        } else if (data.dimension == "3D") {
            return "3D";
        } else if (data.screenFormat == 2) {
            return "와이드";
        } else if (resolCd == "SD") {
            return "일반";
        } else {
            return resolCd;
        }
    }
};

window.ContentOptionObject = function (data, catData, resolIdx, resolCd, contsName, contsId, ltFlag, price, vatPrice,
                                       period, mobile, assetId, ltName, capUrl, isMultiAud, basicCaptViewYn, isImaginary,
                                       isContCoupon, voucherYn,  previewYn, previewRunTime, previewStartTime, seamlessCode) {

    OptionObject.call(this, data, catData, resolIdx, resolCd, contsName, contsId, ltFlag, price, vatPrice, period,
        mobile, isContCoupon, voucherYn, previewYn, previewRunTime, previewStartTime, seamlessCode);

    this.assetId = assetId;
    this.ltName = ltName;
    this.capUrl = capUrl;
    this.isMultiAud = isMultiAud;
    this.basicCaptViewYn = basicCaptViewYn;
    this.isImaginary = isImaginary;
    this.isContent = true;
};
ContentOptionObject.prototype = new OptionObject();

window.SeriesOptionObject = function (data, catData, resolIdx, resolCd, contsName, contsId, ltFlag, price, vatPrice, period, mobile, voucherYn) {

    OptionObject.call(this, data, catData, resolIdx, resolCd, contsName, contsId, ltFlag, price, vatPrice, period, mobile, "N", voucherYn);
    this.ltName = ltFlag === true || ltFlag === "Y" ? "장기" : "";
    this.isSeries = true;
};
SeriesOptionObject.prototype = new OptionObject();

window.PackageObject = function (prdtCd, name, price, vatPrice, buyYn, imgUrl) {
    this.prdtCd = prdtCd;
    this.name = name;
    this.price = price - 0;
    this.vatPrice = vatPrice - 0;
    this.buyYn = buyYn;
    this.imgUrl = imgUrl;
};

window.SeriesObject = function (catData, vodData) {
    this.catData = catData;
    this.vodData = vodData;
    this.imgUrl = catData.imgUrl;
    this.name = catData.catName || catData.itemName;
    this.price = -1;
    this.vatPrice = -1;
    this.resolCd = "";
    this.seriesList = [];

    if (catData.cmbYn == "Y") {
        var resolList = catData.resolCd.split('|');
        var ltFlagList = catData.ltFlag.split('|');
        var periodList = catData.period ? catData.period.split('|') : catData.viewTime ? catData.viewTime.split('|') : "";
        var ltPeriodList = catData.ltPeriod.split('|');
        // 부가세 금액추가
        var priceList = catData.price.split('|');
        var vatPriceList = catData.vatPrice.split('|');
        var ltPriceList = catData.ltPrice.split('|');
        var vatLtPriceList = catData.vatLtPrice.split('|');
        // 시리즈 이용권
        var voucherList = catData.voucherYn.split('|');

        for (var i = 0; i < resolList.length; i++) {
            if (!!resolList[i] && resolList[i] != "") {
                var ltFlag = ltFlagList[i] == 2;
                var price = (ltFlag ? ltPriceList[i] : priceList[i]) - 0;
                var vatPrice = (ltFlag ? vatLtPriceList[i] : vatPriceList[i]) - 0;
                var period = (ltFlag ? ltPeriodList[i] : periodList[i]) - 0;
                var resolName = resolList[i] == "SD" ? "일반" : resolList[i];

                this.seriesList.push(new SeriesOptionObject(vodData, catData, i, resolList[i], this.name,
                    catData.itemId || catData.catId, ltFlag, price, vatPrice, period, false, voucherList[i]));

                if (this.price == -1 || price < this.price) {
                    this.price = price;
                    this.vatPrice = vatPrice;
                    this.resolCd = resolName;
                }
            }
        }
    } else {
        var ltFlag = catData.ltFlag == 2;
        var price = (ltFlag ? catData.ltPrice : catData.price) - 0;
        var vatPrice = (ltFlag ? catData.vatLtPrice : catData.vatPrice) - 0;
        var period = ltFlag ? catData.ltPeriod : (catData.period || catData.viewTime);
        var resolName = catData.resolCd == "SD" ? "일반" : catData.resolCd;
        var voucherYn = catData.voucherYn;

        this.seriesList.push(new SeriesOptionObject(vodData, catData, 0, catData.resolCd, this.name,
            catData.itemId || catData.catId, ltFlag, price, vatPrice, period, false, voucherYn));

        if (this.price == -1 || price < this.price) {
            this.price = price;
            this.vatPrice = vatPrice;
            this.resolCd = resolName;
        }
    }
    sortSeriesOption(this.seriesList);


    function sortSeriesOption(list) {
        var resolOrder = ["일반", "SD", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];

        list.sort(function (a, b) {
            var resol = resolOrder.indexOf(a.resolName) - resolOrder.indexOf(b.resolName);
            if (resol != 0) return resol;
            return a.ltFlag ? 1 : -1;
        });
    }
};

window.ButtonObject = function (type, title, optionList, lessPrice, resolCd) {
    this.title = title;
    this.type = type;
    this.optionList = optionList;
    this.lessPrice = lessPrice;
    this.resolCd = resolCd;
    this.buyYn;
    this.connerYn;
    this.pptBuyYn;
    this.prInfo = 0;
    this.isList = false;

    function getTopPrInfo(a, b) {
        a = UTIL.transPrInfo(a);
        b = UTIL.transPrInfo(b);

        if (a == "all") return b;
        else if (b == "all") return b;
        return a > b ? a : b;
    }

    this.setCornerYn = function (cornerYn) {
        this.connerYn = cornerYn;
        for (var i = 0; i < this.optionList.length; i++) this.optionList[i].connerYn = cornerYn;
    };

    this.setBuyYn = function (buyYn) {
        this.buyYn = buyYn;
    };

    this.setPptBuyYn = function (pptBuyYn) {
        this.pptBuyYn = pptBuyYn;
        for (var i = 0; i < this.optionList.length; i++) this.optionList[i].pptBuyYn = pptBuyYn;
    };

    this.updatePrInfo = function (optionList) {
        this.prInfo = 0;
        if (optionList) for (var i = 0; i < optionList.length; i++)
            this.prInfo = this.prInfo ? getTopPrInfo(this.prInfo, optionList[i].prInfo) : optionList[i].prInfo;
    };

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
    }

    this.updatePrInfo(optionList);
};

window.SingleButtonObject = function (type, optionList) {
    this.couponCount;

    var resolOrder = ["일반", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];
    var lessPrice = -1;
    var resolCd = "";
    for (var i = 0; i < optionList.length; i++) {
        if (lessPrice == 0) {
            if (optionList[i].vatPrice == 0 && resolOrder.indexOf(resolCd) < resolOrder.indexOf(optionList[i].resolName)) {
                lessPrice = optionList[i].vatPrice - 0;
                resolCd = optionList[i].resolName;
            }
        } else if (lessPrice == -1 || optionList[i].vatPrice - 0 < lessPrice - 0) {
            lessPrice = optionList[i].vatPrice - 0;
            resolCd = optionList[i].resolName;
        }
    }

    ButtonObject.call(this, type, type == ButtonObject.TYPE.NORMAL ? "단편" : "소장", optionList, lessPrice, resolCd);
    this.isList = optionList.length > 1;

    this.setBuyYn = function (buyYn, resolCd, assetId) {
        if (assetId) {
            return this.setBuyYnForId(buyYn, false, assetId);
        }

        if (buyYn) {
            for (var i = 0; i < this.optionList.length; i++) {
                if (!resolCd || this.optionList[i].resolCd == resolCd) {
                    this.optionList[i].buyYn = true;
                    if (this.lessPrice > 0 && (!this.buyYn || resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(optionList[i].resolName))) {
                        this.lessPrice = optionList[i].vatPrice - 0;
                        this.resolCd = optionList[i].resolName;
                    }
                    this.buyYn = true;
                }
            }
        } else {
            this.buyYn = false;
        }
    };

    this.setOtnYn = function (otnYn, assetId) {
        this.setBuyYnForId(otnYn, otnYn, assetId);
    };

    this.setBuyYnForId = function (buyYn, otnYn, assetId) {
        if (buyYn) {
            for (var i = 0; i < this.optionList.length; i++) {
                if (assetId == this.optionList[i].assetId) {
                    this.optionList[i].buyYn = true;
                    if (this.lessPrice > 0 && (!this.buyYn || resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(optionList[i].resolName))) {
                        this.lessPrice = optionList[i].vatPrice - 0;
                        this.resolCd = optionList[i].resolName;
                    }
                    if (otnYn) {
                        this.optionList[i].otnYn = true;
                        this.otnYn = true;
                    }
                    this.buyYn = true;
                }
            }
        }
    };

    this.setCornerYn = function (cornerYn, assetId) {
        this.connerYn = cornerYn;

        for (var i = 0; i < this.optionList.length; i++) {
            if (!assetId || this.optionList[i].assetId === assetId) {
                this.optionList[i].connerYn = cornerYn;
                if (resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(optionList[i].resolName)) {
                    this.lessPrice = optionList[i].vatPrice - 0;
                    this.resolCd = optionList[i].resolName;
                }
            }
        }
    };

    this.setPptBuyYn = function (pptBuyYn, assetId) {
        this.pptBuyYn = pptBuyYn;

        for (var i = 0; i < this.optionList.length; i++) {
            if (!assetId || this.optionList[i].assetId === assetId) {
                this.optionList[i].pptBuyYn = pptBuyYn;
                if (resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(optionList[i].resolName)) {
                    this.lessPrice = optionList[i].vatPrice - 0;
                    this.resolCd = optionList[i].resolName;
                }
            }
        }
    };

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay, extraParams) {
        prePlayProcessByButtonObj(this, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, false, updateCallback, autoPlay, extraParams);
    }
};
SingleButtonObject.prototype = new ButtonObject();

window.SeriesButtonObject = function (catData, vodData, optionList) {
    this.seriesData = new SeriesObject(catData, vodData);
    var resolOrder = ["일반", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];
    var resolName = this.seriesData.resolCd;

    if (resolName == "SD") {
        resolName = "일반";
    }

    ButtonObject.call(this, ButtonObject.TYPE.SERIES, "시리즈", optionList, this.seriesData.vatPrice - 0, resolName);
    this.isList = this.seriesData.seriesList.length > 1;

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay, extraParams) {
        prePlayProcessByButtonObj(this, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, false, updateCallback, autoPlay, extraParams);
    };

    this.setBuyYn = function (buyYn, resolCd) {
        if (buyYn) {
            for (var i = 0; i < this.optionList.length; i++) {
                if (!resolCd || this.optionList[i].resolCd == resolCd) {
                    this.optionList[i].buyYn = true;
                }
            }
            for (var i = 0; i < this.seriesData.seriesList.length; i++) {
                if (!resolCd || this.seriesData.seriesList[i].resolCd == resolCd) {
                    this.seriesData.seriesList[i].buyYn = true;

                    if (!this.buyYn || resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(this.seriesData.seriesList[i].resolName)) {
                        this.lessPrice = this.seriesData.seriesList[i].vatPrice - 0;
                        this.resolCd = this.seriesData.seriesList[i].resolName;
                    }
                    this.buyYn = true;
                }
            }
        } else {
            this.buyYn = false;
        }
    };

    this.setCornerYn = function (cornerYn) {
        this.connerYn = cornerYn;
        for (var i = 0; i < this.optionList.length; i++) {
            this.optionList[i].connerYn = true;
        }

        for (var i = 0; i < this.seriesData.seriesList.length; i++) {
            if (resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(this.seriesData.seriesList[i].resolName)) {
                this.lessPrice = this.seriesData.seriesList[i].vatPrice - 0;
                this.resolCd = this.seriesData.seriesList[i].resolName;
            }
        }
    };

    this.setPptBuyYn = function (pptBuyYn) {
        this.pptBuyYn = pptBuyYn;
        for (var i = 0; i < this.optionList.length; i++) {
            this.optionList[i].pptBuyYn = pptBuyYn;
        }

        for (var i = 0; i < this.seriesData.seriesList.length; i++) {
            if (resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(this.seriesData.seriesList[i].resolName)) {
                this.lessPrice = this.seriesData.seriesList[i].vatPrice - 0;
                this.resolCd = this.seriesData.seriesList[i].resolName;
            }
        }
    };

    this.setOtnYn = function (otnYn, resolCd) {
        for (var i = 0; i < this.optionList.length; i++) {
            if (!resolCd || this.optionList[i].resolCd == resolCd) {
                this.optionList[i].buyYn = true;
                this.optionList[i].otnYn = true;
            }
        }
        for (var i = 0; i < this.seriesData.seriesList.length; i++) {
            if (!resolCd || this.seriesData.seriesList[i].resolCd == resolCd) {
                this.seriesData.seriesList[i].buyYn = true;

                if (!this.buyYn || resolOrder.indexOf(this.resolCd) < resolOrder.indexOf(this.seriesData.seriesList[i].resolName)) {
                    this.lessPrice = this.seriesData.seriesList[i].vatPrice - 0;
                    this.resolCd = this.seriesData.seriesList[i].resolName;
                }
                this.buyYn = true;
                this.otnYn = true;
                this.seriesData.seriesList[i].otnYn = true;
            }
        }
    };
};
SeriesButtonObject.prototype = new ButtonObject();

window.PackageButtonObject = function (packageData, optionList) {
    this.packageList = [];
    this.seriesData;
    this.seriesBuyYn;
    this.packageBuyYn;

    var pkgVodPrdCdList = packageData.pkgVodPrdCdList.split('|');
    var pkgVodNameList = packageData.pkgVodNameList.split('|');
    var pkgVodPrdPriceList = packageData.pkgVodPrdPriceList.split('|');
    var vatPkgVodPrdPriceList = packageData.vatPkgVodPrdPriceList.split('|');
    var pkgVodBuyList = packageData.pkgVodBuyList.split('|');
    var imgUrlList = packageData.imgUrlList.split('|');
    var lessPrice = -1;

    for (var i = 0; i < pkgVodPrdCdList.length; i++) {
        this.packageList.push(new PackageObject(pkgVodPrdCdList[i], pkgVodNameList[i], pkgVodPrdPriceList[i], vatPkgVodPrdPriceList[i], pkgVodBuyList[i] === "Y", imgUrlList[i]));

        if (lessPrice == -1 || vatPkgVodPrdPriceList[i] - 0 < lessPrice) {
            lessPrice = vatPkgVodPrdPriceList[i] - 0;
        }

        if (pkgVodBuyList[i] == "Y") {
            this.packageBuyYn = true;
            this.buyYn = true;
        }
    }

    ButtonObject.call(this, ButtonObject.TYPE.PACKAGE, "패키지", optionList, lessPrice, "");
    this.isList = this.packageList.length > 1;

    this.setSeriesData = function (_seriesData) {
        this.seriesData = _seriesData;
        this.title = "시리즈/패키지";

        if (_seriesData.vatPrice - 0 < this.lessPrice - 0) {
            this.lessPrice = _seriesData.vatPrice - 0;
            this.resolCd = "";
        }
    };

    this.setBuyYn = function (buyYn, resolCd) {
        this.buyYn = this.buyYn || buyYn;
        this.seriesBuyYn = buyYn;

        if (buyYn) {
            for (var i = 0; i < this.optionList.length; i++) {
                if (!resolCd || this.optionList[i].resolCd === resolCd) {
                    this.optionList[i].buyYn = true;
                }
            }
            if (this.seriesData) for (var i = 0; i < this.seriesData.seriesList.length; i++) {
                if (!resolCd || this.seriesData.seriesList[i].resolCd === resolCd) {
                    this.seriesData.seriesList[i].buyYn = true;
                }
            }
        }
    };

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
        var that = this;
        if (this.seriesData || this.packageList.length > 1) {
            LayerManager.activateLayer({
                obj: {
                    id: "VODSelectPackageDetailPopupLayer",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        pkgList: that.packageList,
                        seriesData: that.seriesData,
                        seriesBuyYn: that.seriesBuyYn,
                        callback: function (pickedData) {
                            if (pickedData.isSeries) {
                                var seriesBtn = new SeriesButtonObject(pickedData.data.catData, pickedData.data.vodData, that.optionList);
                                seriesBtn.buyYn = that.seriesBuyYn;
                                prePlayProcessByButtonObj(seriesBtn, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, false, updateCallback, autoPlay);
                            } else {
                                VODImpl.showDetailForPkgProdCd({prdtCd: pickedData.data.prdtCd, req_cd: reqPathCd});
                            }
                        }
                    }
                },
                moduleId: "module.vod",
                visible: true
            });
        } else {
            VODImpl.showDetailForPkgProdCd({
                prdtCd: this.packageList[0].prdtCd,
                req_cd: "01"
            });
        }
    }
};
PackageButtonObject.prototype = new ButtonObject();

window.CornerButtonObject = function (cornerList, optionList, onlyCorner) {
    cornerList = filterCornerData(cornerList);

    this.cornerList = cornerList;
    this.onlyCorner = onlyCorner;

    for (var i in cornerList) {
        cornerList[i].isOnlyCorner = onlyCorner
    }

    var lessPrice = -1, resolCd = "";

    for (var i in cornerList) {
        if (lessPrice == -1 || cornerList[i].vatProductPrice - 0 < lessPrice - 0) {
            lessPrice = cornerList[i].vatProductPrice - 0;
        }
    }

    ButtonObject.call(this, ButtonObject.TYPE.CORNER, "월정액", optionList, lessPrice - 0, "월");
    this.isList = this.cornerList.length > 1;

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
        var that = this;

        if (KTW.managers.service.ProductInfoManager.isMultiroomPackage()) {
            HTool.openErrorPopup({
                title: "알림",
                message: ["가입하신 상품에서는", "월정액을 구매할 수 없습니다", "", ERROR_TEXT.ET_CALL],
                button: "닫기",
                callback: function () {
                    callback(false)
                }
            });
        }
        else {
            prePlayProcessByButtonObj(this, catId, reqPathCd, function (res) {
                if (res) {
                    prePlayProcessByButtonObj(new SingleButtonObject(ButtonObject.TYPE.NORMAL, that.optionList), catId,
                        reqPathCd, callback, noContinuePopup, bookmarkTime, true, false, updateCallback, autoPlay);
                } else {
                    callback(false);
                }
            }, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay);
        }
    };

    this.setBuyYn = function (buyYn, resolCd) {
    };

    function filterCornerData(data) {
        var firstCorner = null;
        var unifiedCorner = null;
        var returnList = [];

        for (var i = 0; i < data.length; i++) {
            if (data[i].unifiedCornerYn != "Y") {
                if (!firstCorner) {
                    firstCorner = data[i];

                    if (data[i].firstCornerYn == "Y") {
                        break;
                    }
                } else if (data[i].firstCornerYn == "Y") {
                    firstCorner = data[i];
                    break;
                }
            }
        }
        for (var i = 0; i < data.length; i++) {
            if (data[i].unifiedCornerYn == "Y") {
                if (!unifiedCorner) {
                    unifiedCorner = data[i];

                    if (data[i].firstCornerYn == "Y") {
                        break;
                    }
                } else if (data[i].firstCornerYn == "Y") {
                    unifiedCorner = data[i];
                    break;
                }
            }
        }

        if (firstCorner) {
            returnList.push(firstCorner);
        }

        if (unifiedCorner) {
            returnList.push(unifiedCorner);
        }
        return returnList;
    }
};
CornerButtonObject.prototype = new ButtonObject();

window.NoCornerButtonObject = function (optionList) {
    this.onlyCorner = true;
    this.isList = false;
    this.noCorner = true;

    ButtonObject.call(this, ButtonObject.TYPE.CORNER, "월정액", optionList, 0, "");

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
        var that = this;
        if (this.connerYn) {
            prePlayProcessByButtonObj(new SingleButtonObject(ButtonObject.TYPE.NORMAL, that.optionList), catId,
                reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, false, updateCallback, autoPlay);
        } else {
            // 월정액 가입 불가 팝업 activate!!!
            HTool.openErrorPopup({
                title: "알림",
                message: ["가입하신 상품에서는", "월정액을 구매할 수 없습니다", "", ERROR_TEXT.ET_CALL],
                button: "닫기",
                callback: function () {
                    callback(false)
                }
            })
        }
    };

    this.setBuyYn = function (buyYn) {
    };
};
NoCornerButtonObject.prototype = new ButtonObject();

window.VodPlusButtonObject = function (catData, optionList) {
    this.catData = catData;
    this.vodPlus = true;
    var resolName = "";
    var resolList = ["일반", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];
    for (var i = 0; i < optionList.length; i++)
        if (resolList.indexOf(optionList[i].resolName) > resolList.indexOf(resolName)) resolName = optionList[i].resolName;

    ButtonObject.call(this, ButtonObject.TYPE.VODPLUS, "단편", optionList, "", resolName);

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
        var that = this;
        if (this.connerYn) {
            prePlayProcessByButtonObj(new SingleButtonObject(ButtonObject.TYPE.NORMAL, that.optionList), catId, reqPathCd,
                callback, noContinuePopup, bookmarkTime, authComplete, false, updateCallback, autoPlay);
        } else {
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            if (custEnv.buyTypeYn == "N") {
                HTool.openErrorPopup({
                    title: "알림",
                    message: ERROR_TEXT.ET008.concat([""].concat(ERROR_TEXT.ET_CALL)),
                    callback: function () {
                        if (callback) callback(false);
                    }
                });
            }
            else {
                // [kh.kim] fullPackage 가 아니면 에러 팝업
                if (KTW.CONSTANT.IS_BIZ) {
                    HTool.openErrorPopup({
                        title: "알림",
                        message: ERROR_TEXT.ET008.concat([""].concat(ERROR_TEXT.ET_CALL)),
                        callback: function () {
                            if (callback) callback(false);
                        }
                    });
                }
                else {
                    ModuleManager.getModuleForced("module.vod_payment", function (vodPaymentModule) {
                        vodPaymentModule.execute({
                            method: "getVodUpSaleInfo",
                            params: {
                                catId: catId,
                                callback: function (result) {
                                    if (result) {
                                        that.setCornerYn(true);
                                        prePlayProcessByButtonObj(that, catId, reqPathCd, callback, null, null, null, false, updateCallback, autoPlay);
                                    } else if (callback) callback(false);
                                }
                            }
                        });
                    });
                }
            }
        }
    };
};
VodPlusButtonObject.prototype = new ButtonObject();

// VOD PPT 버튼
window.VodPptButtonObject = function (productList, optionList, onlyVodPpt) {
    productList = filterVodPptData(productList);

    this.productList = productList;
    this.onlyVodPpt = onlyVodPpt;
    this.vodPpt = true;

    for (var i in productList) {
        productList[i].onlyVodPpt = onlyVodPpt
    }

    var lessPrice = -1, resolCd = "";

    for (var i in productList) {
        var price = productList[i].ltFlag == "Y" ? productList[i].vatLtPptPrice.split("|")[0] : productList[i].vatPptPrice.split("|")[0];
        if (lessPrice == -1 || price - 0 < lessPrice - 0) {
            lessPrice = price - 0;
        }
    }

    ButtonObject.call(this, ButtonObject.TYPE.VODPPT, "기간정액", optionList, lessPrice - 0, "");
    this.isList = this.productList.length > 1;

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
        var that = this;

        prePlayProcessByButtonObj(this, catId, reqPathCd, function (res) {
            if (res) {
                prePlayProcessByButtonObj(new SingleButtonObject(ButtonObject.TYPE.NORMAL, that.optionList), catId,
                    reqPathCd, callback, noContinuePopup, bookmarkTime, true, false, updateCallback, autoPlay);
            } else {
                callback(false);
            }
        }, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay);
    };

    this.setBuyYn = function (buyYn, resolCd) {
    };

    this.setCornerYn = function (cornerYn) {
    };

    function filterVodPptData(data) {
        var firstVodPpt = null;
        var unifiedVodPpt = null;
        var returnList = [];

        for (var i = 0; i < data.length; i++) {
            if (data[i].unifiedPptYn != "Y") {
                if (!firstVodPpt) {
                    firstVodPpt = data[i];
                    break;
                }
            }
        }
        for (var i = 0; i < data.length; i++) {
            if (data[i].unifiedPptYn == "Y") {
                if (!unifiedVodPpt) {
                    unifiedVodPpt = data[i];
                    break;
                }
            }
        }

        if (firstVodPpt) {
            returnList.push(firstVodPpt);
        }

        if (unifiedVodPpt) {
            returnList.push(unifiedVodPpt);
        }
        return returnList;
    }
};
VodPptButtonObject.prototype = new ButtonObject();

window.NoVodPptButtonObject = function (optionList) {
    this.onlyVodPpt = true;
    this.isList = false;
    this.noCorner = true;
    this.vodPpt = true;

    ButtonObject.call(this, ButtonObject.TYPE.VODPPT, "기간정액", optionList, 0, "");

    this.execute = function (catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isCallbyDetailScreen, updateCallback, autoPlay) {
        var that = this;
        if (this.pptBuyYn) {
            prePlayProcessByButtonObj(new SingleButtonObject(ButtonObject.TYPE.NORMAL, that.optionList), catId,
                reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, false, updateCallback, autoPlay);
        } else {
            // 월정액 가입 불가 팝업 activate!!!
            HTool.openErrorPopup({
                title: "알림",
                message: ["가입하신 상품에서는", "월정액을 구매할 수 없습니다", "", ERROR_TEXT.ET_CALL],
                button: "닫기",
                callback: function () {
                    callback(false)
                }
            })
        }
    };

    this.setBuyYn = function (buyYn) {
    };

    this.setCornerYn = function (cornerYn) {
    };
};
NoVodPptButtonObject.prototype = new ButtonObject();

Object.defineProperty(ButtonObject, "TYPE", {
    writable: false, value: {
        NORMAL: 0,
        DVD: 1,
        SERIES: 2,
        PACKAGE: 3,
        CORNER: 4,
        VODPLUS: 5,
        VODPPT: 6
    }
});