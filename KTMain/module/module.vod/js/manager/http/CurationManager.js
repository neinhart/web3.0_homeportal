"use strict";
/**
 * 추천 고도화 API
 */
window.curationManager = (function() {
    function getUrl() {
        if (DEF.RELEASE_MODE === CONSTANT.ENV.BMT || DEF.RELEASE_MODE === CONSTANT.ENV.LIVE) {
            return VOD.HTTP.CURATION.LIVE_URL;
        } else {
            return VOD.HTTP.CURATION.TEST_URL
        }
    }

    function getPkgCode() {
        var baseCd = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
        var pkgList = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);

        return  baseCd + (pkgList?("," + pkgList):"");
    }

    function ajax(url, post_data, /**Function*/callback, params) {
        return ajaxFactory.createAjax("curationManager", true, "post", "json", url, post_data, true, -1, callback, params);
    }

    function _ajaxStop(request) {
        ajaxFactory.stopAjax(request);
    }

    var REQ_CODE = CONSTANT.STB_TYPE.ANDROID ? "G" : "E",
        SVC_CODE = CONSTANT.IS_OTS ? "OTS" : "OTV",
        STB_VER = CONSTANT.STB_TYPE.ANDROID ? "1.2" : (CONSTANT.IS_HDR ? "3.2" : "3.1");

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     * content_id -> 컨텐츠 ID.
     * rating -> 별점 점수 (0~5)
     * req_type -> 'V' : 상세 / 'P' : VOD 시청 종료 / 'M' : 마이메뉴
     */
    function saveRating(no_adult, content_id, rating, req_type, callback, params) {
        var api = "/saveRating";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': no_adult,
            'STB_VER': STB_VER,
            'CONS_ID': content_id,
            'REQ_TYPE': req_type,
            'RATING': rating
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    /**
     * no_adult -> true -> 성인 미노출
     * callback -> callback.
     * params -> callback 은 (isSuccess, result, params) callback 에 필요한 객체를 params 를 통해 전달 가능함.
     * content_id - 컨텐츠 ID.
     */
    function getMyRating(no_adult, content_id, callback, params) {
        var api = "/getMyRating";

        var post_data = {
            'CONTENT': "VOD",
            'USER_ID': DEF.SAID,
            'PRODUCT_LIST': getPkgCode(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_RATE': no_adult,
            'STB_VER': STB_VER,
            'CONS_ID': content_id
        };

        return ajax(getUrl() + api, post_data, callback, params);
    }

    return {
        'saveRating': saveRating,
        'getMyRating': getMyRating,
        'ajaxStop': _ajaxStop
    };
}());