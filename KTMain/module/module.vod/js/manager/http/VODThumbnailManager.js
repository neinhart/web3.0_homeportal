/**
 * Created by user on 2015-12-22.
 */
"use strict";
/**
 * @class 추천서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 *
 */
window.VodThumbnailManager = (function() {
    function getUrl() {
        return VOD.HTTP.AMOC.WAPI_URL;
    }

    function ajax(async, timeout, url, postData, /**Function*/callback) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return ajaxFactory.createAjax("VOD Thumbnail Manager", async, "get", "json", url, postData, true, -1, callback, null);
    }

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AJAX request stop!!");
            request.abort();
            request = null;
        }
    }

    /**
     * thumbnail list
     *
     * response
     *
     * @param callback
     * @param contId
     * @returns {*}
     * @private
     */
    function _getThumbnailList(callback, contId) {
        return ajax(true, 0, getUrl() + "/api/thumbnail/merge.json", "content_id=" + contId + "&said=" + DEF.SAID, callback);
    }

    function _getCorner(callback, corner_grp_id, content_id) {
        return ajax(true, 0, getUrl() + "/api/corner/list.json", "content_id=" + content_id + "&cornergrpid=" + corner_grp_id + "&said=" + DEF.SAID, callback);
    }

    /**
     * content ID 에 해당되는 북마크 정보 조회.
     *
     * response
     * time_stamp / title / file_url / cc_content_id / content_id / nid
     *
     * @param callback
     * @param content_id
     * @returns {*}
     * @private
     */
    function _getBookmark(callback, content_id) {
        return ajax(true, 0, getUrl() + "/api/t_vod_bookmark/list.json", "content_id=" + content_id + "&said=" + DEF.SAID, callback);
    }

    return {
        getThumbnailList: _getThumbnailList,
        getCorner: _getCorner,
        getBookmark: _getBookmark
    }
}());