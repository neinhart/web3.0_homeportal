"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId);
 * posterDm.ajaxMenu(createPoster, failAjax);
 *
 */
window.VODAmocManager = (function() {
    var usageId;
    var api;
    var data;

    var AMOC_SERVER_HTTP_URL = VOD.HTTP.AMOC.HTTP_URL;
    var AMOC_SERVER_HTTPS_URL = VOD.HTTP.AMOC.HTTPS_URL;

    var IS_RELEASE = (DEF.UI_VERSION != "0.01.000");

    /**
     * ajax를 이용하여 json 데이터 수집<br>
     * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
     * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    var ajax = function(async, timeout, url, postData, /**Function*/callback, reqType, param) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAX request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }

        if (postData instanceof Object) {
            if (!postData) {
                postData = {};
            }
            postData.WMOCKey = DEF.AMOC.AUTH_KEY;
        } else {
            postData = postData ? postData + "&WMOCKey=" + DEF.AMOC.AUTH_KEY : "WMOCKey=" + DEF.AMOC.AUTH_KEY;
        }

        return ajaxFactory.createAjax("AMOC Manager", async, (reqType ? reqType : "post"), "json", url, postData, false, timeout, callback, param);
    };

    var ajaxData = function(async, timeout, url, postData, /**Function*/callback) {
        if (!IS_RELEASE) {
            log.printForced("AMOC AJAXDATA request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }
        postData = postData ? postData : new Object();
        postData.WMOCKey = DEF.AMOC.AUTH_KEY;
        //	    postData = postData ? postData+"&WMOCKey="+DEFAULT_AMOC_AUTH_KEY : "WMOCKey="+DEFAULT_AMOC_AUTH_KEY;

        return ajaxFactory.createAjax("AMOC Manager [DATA]", async, "post", "json", url, postData, false, timeout, callback, null);
    };

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AMOC AJAX request stop!!");
            try {
                request.abort();
            } catch (e) {
                log.printExec(e.message);
            }

            request = null;
        }
    }
/*
    function _useMyStarPointVodNxt(callback, saId, pkgYn, contsId, contsName, buyingDate, buyingPrice, buyingType, catId, appCd,
                                   reqPathCd, ltFlag, hdYn, stbIp, suppInfo, pointType, saleYn, percent) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-pgcpn-api/vod/buy/with-star-point-nxt";

        data = {
            "saId": saId,
            "pkgYn": pkgYn, //묶음 구매 (Y/N)
            "contsId": contsId, //(건별시는 컨텐츠 ID, 묶음 구매시는 CAT ID)
            "contsName": contsName, //역시 컨텐츠명 / 카테고리명
            "buyingDate": buyingDate,
            "buyingPrice": buyingPrice,
            "buyingType": buyingType, //C 입력
            "catId": catId,
            "appCd": appCd, //appcd
            "reqPathCd": reqPathCd,
            "ltFlag": ltFlag,
            "hdYn": hdYn,
            "stbIp": stbIp,
            "suppInfo": suppInfo,
            "pointType": pointType,
            "saleYn": (saleYn ? saleYn : "N"),
            "percent": percent
        };
        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _useTvMoneyVodNxt(callback, saId, pkgYn, contsId, contsName, buyingDate, buyingPrice, buyingType, catId,
                               catName, appCd, reqPathCd, ltFlag, hdYn, stbIp, tvPoint, tvMoney, detPoint1, detPoint2,
                               suppInfo, pointType, saleYn) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-pgcpn-api/vod/buy/with-tvmoney-nxt";

        data = {
            "saId": saId, //
            "pkgYn": pkgYn, //
            "itemId": contsId, //
            "itemName": contsName, //
            "buyingDate": buyingDate, //
            "amount": buyingPrice, //
            "buyingType": buyingType, //
            "catId": catId, //
            "catName": catName, //
            "appCd": appCd, //
            "reqPathCd": reqPathCd, //
            "ltFlag": ltFlag, //
            "hdYn": hdYn, //
            "stbIp": stbIp, //
            "tvPoint": tvPoint, //
            "tvMoney": tvMoney, //
            "detPoint1": detPoint1, //
            "detPoint2": detPoint2, //
            "suppInfo": suppInfo, //
            "pointType": pointType, //
            "saleYn": (saleYn ? saleYn : "N")
        };

        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }
*/
    function _useComplexPayNew(callback, data) {
        api = "/amoc-pgcpn-api/vod/buy/with-complex-pay-new";
        var createdPostData = {
            saId       : (data.saId || KTW.SAID),
            itemId     : data.itemId,
            itemName   : data.itemName,
            pkgYn      : data.pkgYn,
            hdYn       : data.hdYn,
            ltFlag     : data.ltFlag,
            price      : data.price,
            vatPrice   : data.vatPrice,
            amount     : data.amount,
            buyingDate : data.buyingDate,
            buyingType : (data.buyingType || "H"),
            catId      : data.catId,
            catName    : data.catName,
            appCd      : data.appCd,
            reqPathCd  : (data.reqPathCd || "01")
        };

        createdPostData["pVoucherAmount"] = data.pVoucherAmount || 0;
        createdPostData["dcVoucherAmount"] = data.dcVoucherAmount || 0;
        createdPostData["starPoint"] = data.starPoint || 0;
        createdPostData["pTvMoney"] = data.pTvMoney || 0;
        createdPostData["tvPoint"] = data.tvPoint || 0;
        createdPostData["sTvMoney"] = data.sTvMoney || 0;
        createdPostData["cash"] = data.cash || 0;

        return ajaxData(true, 0, AMOC_SERVER_HTTPS_URL + api, createdPostData, callback);
    }

    function _requestOPC(callback, saId, pkgYn, saleYn) { //미사용
        api = "/amoc-api/vod/buy/with-pgmobile";
        data = "saId=" + saId + "&pkgYn=" + pkgYn + "&saleYn=" + (saleYn ? saleYn : "N") + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _requestOAC(callback, saId, buyingPrice, mobile, telcoCode, subscriberSSN, orderDate, contentType, contsName, stbIp, merchantId) { //미사용
        api = "/amoc-pgcpn-api/vod/buy/pgmobile-auth";
        data = "saId=" + saId + "&buyingPrice=" + buyingPrice + "&mobile=" + mobile + "&telcoCode=" + telcoCode + "&subscriberSSN=" + subscriberSSN
            + "&orderDate=" + orderDate + "&contentType=" + contentType + "&contsName=" + contsName + "&stbIp=" + stbIp + "&merchantId=" + merchantId + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _registPrepaidCpn(callback, domainId, saId, cpnNo) { //미사용
        api = "/amoc-pgcpn-api/vod/coupon-register";
        data = "domainId=" + domainId + "&saId=" + saId + "&cpnNo=" + cpnNo + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _getMyExpireList(callback, domainId, saId, currPageNo, cntPerPage) { //미사용
        api = "/amoc-pgcpn-api/vod/coupon-expire-due-list";
        data = "domainId=" + domainId + "&saId=" + saId + "&currPageNo=" + currPageNo + "&cntPerPage=" + cntPerPage + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    /**
     *
     * @param pptPkgCd: 기간 정액제 상품코드 (getLinkTimeNxt 에서 받은 값 사용. 웹은 없음)
     * @private
     */
    function _authorizePVOD(callback, saId, contsId, buyingDate, catId, appCd, reqPathCd, startPoint, isAsync, pptPkgCd) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/pvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&buyingDate=" + buyingDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint + "&pptPkgCd=" + pptPkgCd;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _authorizeOTNPVOD(callback, saId, contsId, buyingDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/otn-pvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&buyingDate=" + buyingDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _authorizeSVOD(callback, saId, contsId, systemDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/svod";
        data = "saId=" + saId + "&contsId=" + contsId + "&systemDate=" + systemDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _authorizeFVOD(callback, saId, contsId, systemDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/fvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&systemDate=" + systemDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd + "&startPoint=" + startPoint;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _authorizeZVOD(callback, saId, contsId, systemDate, catId, appCd, reqPathCd, startPoint, isAsync) {
        if (!reqPathCd)
            reqPathCd = "01";
        api = "/amoc-api/vod/start/zvod";
        data = "saId=" + saId + "&contsId=" + contsId + "&systemDate=" + systemDate + "&catId=" + catId + "&appCd=" + appCd + "&reqPathCd=" + reqPathCd;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTPS_URL + api, data, callback);
    }

    function _setVodEnd(callback, saId, contsId, second) {
        api = "/amoc-api/vod/end";
        data = "saId=" + saId + "&contsId=" + contsId + "&second=" + second;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    /**
     * request-
     * said: SAID
     * contsId: assetId
     * uhdYn: uhd stb 여부 ('Y'/'N')
     * buyTypeYn: Biz Type 인지 아닌지 여부 'N' 이면 아님 web 은 BizType 없음
     */
    function _getLinkTimeNxt(callback, saId, contsId, buyTypeYn, isAsync, param) {
        api = "/amoc-api/vod/prepare-nxt";
        data = "saId=" + saId + "&contsId=" + contsId + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&buyTypeYn=" + buyTypeYn;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _getViewHistEP4(postData, callback) {
        api = "/amoc-api/vod/watch-history";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, postData, callback);
    }

    function _setDelWatch(callback, saId, contsId, contYn) {
        api = "/amoc-api/vod/watch-history/delete";
        data = "saId=" + saId + "&contsId=" + contsId + "&contYn=" + contYn + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    /**
     *
     * @param callback
     * @param saId
     * @param catId
     * @param cmbYn : 통합편성여부 (Y/N)
     * @param param
     * @private
     */
    function _getContStatusNxt(callback, saId, catId, cmbYn, buyTypeYn, param) {
        api = "/amoc-api/vod/category/auth-info-nxt";
        data = "saId=" + saId + "&catId=" + catId + "&cmbYn=" + (cmbYn ? cmbYn : "N") + "&buyTypeYn=" + buyTypeYn + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     *
     * @param callback
     * @param saId
     * @param catId
     * @param cmbYn : 통합편성여부 (Y/N)
     * @param param
     * @private
     */
    function _getItemPerInfo(callback, catId, param) {
        api = "/amoc-api/vod/category/item-per-info";
        data = "saId=" + DEF.SAID + "&catId=" + catId;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _getCateContExtW3(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/series-conts-ext-w3";
        data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        // data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=Y&smartYn=N&hdrYn=Y";
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _getCateContNxtW3(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/series-conts-nxt-w3";
        data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        // data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=Y&smartYn=N&hdrYn=Y";
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _getCateInfoW3(callback, catId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/info-w3";
        data = "catId=" + catId + "&saId=" + saId + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTP_URL + api, data, callback, null, param);
    }

    function _getContentW3(callback, catId, contsId, saId, isAsync, param) {
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/content/info-w3";
        data = "catId=" + catId + "&contsId=" + contsId + "&saId=" + saId + "&platType=W&smartYn=N&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&hdrYn=" + hdr_yn;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTP_URL + api, data, callback, null, param);
    }

    /**
     * 회차/메뉴/양방향/시리즈/단편 모두 얻을 수 있음.
     * @param callback
     * @param catId
     * @param startIndex
     * @param itemCnt
     * @param saId
     * @param platType W 가 web
     * @param smartYn Y 면 smart app tv N 이면 stb.
     * @param param
     *
     */
    function _getItemDetlListW3(callback, catId, startIndex, itemCnt, sortOpt, filterCdList, filterOptList, param) {
        // [jh.lee] 화면에 그려야할 내용이 맞는지 비교하기 위한 param(next) 값을 추가
        var hdr_yn;

        hdr_yn = CONSTANT.IS_HDR === true ? "Y" : "N";

        api = "/amoc-api/vod/category/item-detail-list-w3";
        data = "catId=" + catId + "&startIndex=" + startIndex + "&itemCnt=" + itemCnt + "&saId=" + DEF.SAID + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;

        if(sortOpt) data += "&sortOpt=" + sortOpt;
        if(filterCdList) data += "&filterCdList=" + filterCdList;
        if(filterOptList) data += "&filterOptList=" + filterOptList;

        return ajax(callback?true:false, 0, AMOC_SERVER_HTTP_URL + api, data, callback, null, param);
    }

    function _getCustEnv(callback, saId) {
        api = "/amoc-api/vod/subscriber/info";
        data = "saId=" + saId + "&platformGubun=W&smartYn=N";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getCustPkgInfo(callback, saId) {
        api = "/amoc-api/vod/subscriber/pkg-cat-list";
        data = "saId=" + saId + "&platformGubun=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getCustCugInfo(callback, saId) { // [jh.lee] 미사용 노노. 사용함(CUG).
        api = "/amoc-api/vod/subscriber/cug-list";
        data = "saId=" + saId + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getProductInfo(callback, pkgCode, buyTypeYn, param) {
        api = "/amoc-api/product/info";
        data = "saId=" + DEF.SAID + "&pkgCode=" + pkgCode + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, null, param);
    }

    /**
     *
     * @param adultYn : 성인물 포함 여부 (A/Y/N)
     * @param callback
     * @private
     */
    function _getPlayListNxt(adultYn, callback) {
        api = "/amoc-api/vod/subscriber/play-list-nxt";
        data = "saId=" + DEF.SAID + "&adultYn=" + adultYn + "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        data += "&hdrYn=" + (CONSTANT.IS_HDR === true ? "Y" : "N");
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _addPlayListCmb(itemType, itemId, catId, hdYn, callback, param) {
        api = "/amoc-api/vod/subscriber/set-play-list";

        data = "saId=" + DEF.SAID + "&itemCount=1&itemType=" + itemType + "&itemId=" + itemId + "&categoryId=" + catId + "&flag=1" + (hdYn ? ("&hdYn=" + hdYn) : "") + "&platformGubun=W";

        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _removePlayListCmb(itemIds, delCount, callback, param) {
        api = "/amoc-api/vod/subscriber/set-play-list";

        data = "saId=" + DEF.SAID + "&itemCount=" + delCount + "&itemId=" + itemIds + "&flag=2" + "&platformGubun=W";

        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    /**
     *
     * @param itemCount 추가 삭제 개수 (추가는 1개만 가능)
     * @param itemType 1 시리즈 2 일반 (삭제는 생략)
     * @param itemId "|" 로 구분, 최대 50개 (삭제시)
     * @param catId 삭제할때는 Null
     * @param flag 1: 추가, 2: 삭제
     * @param hdYn 통합편성된 시리즈 카테고리/컨텐츠 찝할때 (Y/N/F/U) 값 을 넣음.
     * @private
     */
    function _setPlayListNxt(itemCount, itemType, itemId, catId, flag, hdYn, callback, param) {
        api = "/amoc-api/vod/subscriber/set-play-list-nxt";
        data = {
            'saId': DEF.SAID,
            'itemCount': itemCount,
            'itemType': itemType,
            'itemId': itemId,
            'categoryId': catId,
            'flag': flag,
            'hdYn': hdYn,
        };
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _getViewHistoryNxt(postData, callback) {
        api = "/amoc-api/vod/subscriber/view-history-nxt";
        postData += "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        postData += "&hdrYn=" + (CONSTANT.IS_HDR === true ? "Y" : "N");
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, postData, callback);
    }

    /**
     * 가입상품 목록 조회
     */
    function _getCustPkgList(postData, buyTypeYn, callback, sync) {
        api = "/amoc-api/vod/subscriber/pkg-list";

        postData += "&platformGubun=W&buyTypeYn=" + buyTypeYn;

        var reqAsync = true;
        if (sync)
            reqAsync = false;

        return ajax(reqAsync, 0, AMOC_SERVER_HTTP_URL + api, postData, callback);
    }

    function _getBuyHistoryNxt(postData, callback) {
        api = "/amoc-api/vod/subscriber/buy-history-nxt";

        postData += "&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N");
        postData += "&hdrYn=" + (CONSTANT.IS_HDR === true ? "Y" : "N");

        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, postData, callback);
    }

    function _setDelBuy(postData, callback) {
        api = "/amoc-api/vod/subscriber/buy-history/delete";

        postData += "&platformGubun=W";

        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, postData, callback);
    }

    /**
     * amocManager.getCornerList(function(arg1,arg2){console.log(arg1);console.log(arg2);},"4100","TT120418048");
     */
    function _getCornerList(callback, catId, saId) {
        api = "/amoc-api/vod/category/corner-list";
        data = "catId=" + catId + "&saId=" + saId + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    /**
     * getCategoryId 는 호출하는 쪽에서 platformGubun넘김
     */
    function _getCategoryId(postData, callback) {
        api = "/amoc-api/vod/category/category-id";

        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, postData, callback);
    }

    /**
     * exchangeItemId는 모바일 공유목록에서 전사시리즈 id를 시리즈 아이디로 바꾸는데 사용된다.
     */

    function _exchangeItemId(callback, exchgId, exchgType, seamYn, isHd, saId) {
        api = "/amoc-api/vod/category/exchange-item-id";
        data = "exchgId=" + exchgId + "&exchgType=" + exchgType + "&seamYn=" + seamYn + "&isHd=" + isHd + "&saId=" + saId + "&platformGubun=W";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    /**
     * 컨텐츠/시리즈 부가 상세 정보
     *  1/null: 긴 크기의 시놉시스
     *  2: 별점정보
     *
     *  1.  long synopsisL.
     *  2.  별점구분|참여자수|별점
     *  3,4 별점구분|평균별점|상세별점명|상세 참여자수|상세별점
     *  5,6 별점구분|평균별점|전문가명|상세별점|코멘트
     *  //평균별점은 어느데이타에 근거한 데이터지 ?.
     */
    function _getDetailContentInfo(content_id, info_type, callback, param) {
        api = "/amoc-api/vod/content/detail-content-info";
        data = "saId=" + DEF.SAID + "&contsId=" + content_id + "&infoType=" + info_type;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    function _checkMyContentW3(content_id, category_id, app_cd, req_path_cd, check_type, buyTypeYn, callback, param) {
        api = "/amoc-api/vod/subscriber/check-my-content-w3";
        data = "saId=" + DEF.SAID + "&contsIdList=" + content_id + "&catId=" + category_id + "&appCd=" + app_cd + "&reqPathCd=" + req_path_cd + "&checkType=" + check_type + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    function _checkMyPkgVodW3(callback, pkgVodPrdCd, app_cd, req_path_cd, buyTypeYn, param) {
        api = "/amoc-api/vod/subscriber/check-my-pkg-vod-w3";
        data = "saId=" + DEF.SAID + "&pkgVodPrdCd=" + pkgVodPrdCd + "&appCd=" + app_cd + "&reqPathCd=" + req_path_cd + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    function _getChannelInfo(callback, serviceId, buyTypeYn, param) {
        api = "/amoc-api/channel/channel-info";
        data = "saId=" + DEF.SAID + "&serviceId=" + serviceId + "&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined/*req_type*/, param);
    }

    /**
     * 가입상품 목록 조회
     */
    function _getMyPkgList(callback, buyTypeYn) {
        api = "/amoc-api/vod/subscriber/my-pkg-list";
        data = "saId=" + DEF.SAID + "&platformGubun=W&buyTypeYn=" + buyTypeYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getMyPlayItemList(callback) {
        api = "/amoc-api/vod/subscriber/my-play-item-list";
        data = "saId=" + DEF.SAID;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getMyPlayItemDetList(callback, playListId) {
        api = "/amoc-api/vod/subscriber/my-play-item-det-list";
        data = "saId=" + DEF.SAID + "&playListId=" + playListId;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _setMyPlayItemDetList(callback, cmdFlag, playListId, itemCount, contsId, contsKey, itemType, hdYn) {
        api = "/amoc-api/vod/subscriber/set-my-play-item-det-list";
        data = "saId=" + DEF.SAID + "&cmdFlag=" + cmdFlag + "&playListId=" + playListId + "&itemCount=" + itemCount + "&contsId=" + contsId + "&contsKey=" + contsKey + "&itemType=" + itemType + "&hdYn=" + hdYn;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getAdditionalInfoW3(callback, contsId) {
        api = "/amoc-api/vod/content/additional-info-w3";
        data = "saId=" + DEF.SAID + "&contsId=" + contsId + "&itemType=2";
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback);
    }

    function _getPkgVodW3(callback, pkgCd, isAsync, param) {

        api = "/amoc-api/vod/category/pkg-vod-w3";
        // data = "catId=" + catId + "&saId=" + saId + "&platType=W&uhdYn=" + (CONSTANT.IS_UHD === true ? "Y" : "N") + "&smartYn=N&hdrYn=" + hdr_yn;
        data = "pkgVodPrdCd=" + pkgCd + "&saId=" + DEF.SAID;
        return ajax(isAsync == undefined ? true : isAsync, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }

    function _getMyPkgVodListW3(callback, seriesYn, contsIdList, catId, reqPathCd, param) {
        api = "/amoc-api/vod/category/my-pkg-vod-list-w3";
        data = "saId=" + DEF.SAID + "&seriesYn=" + seriesYn + "&contsIdList=" + contsIdList + "&catId=" + catId + "&appCd=H&reqPathCd=" + reqPathCd;
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, param);
    }


    /**
    reqData = {
        said : 사용자 SAID
        contsId : 콘텐츠ID 리스트 또는 시리즈 가상카테고리ID 하나
        multiDevYn : 복수단말 여부
        vodPrimePrdcYn : VOD Prime 사용자 여부
        paringYn : OTM 페어링 여부
        itemType : 1(시리즈), 2(콘텐츠 default)
        cmbYn : itemType 1 일때만 입력
        param : option
    }
     */
    function _checkShrContent(reqData, callback) {
        api = "/amoc-api/vod/subscriber/check-share-content";
        data = "saId=" + reqData.saId + "&contsId=" + reqData.contsId + "&multiDevYn=" + reqData.multiDevYn + "&vodPrimePrdcYn="
            + reqData.vodPrimePrdcYn + "&paringYn=" + reqData.paringYn + "&itemType=" + reqData.itemType;
        if (reqData.cmbYn) {
            data += ("&cmbYn=" + reqData.cmbYn);
        }
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, reqData.param);
    }

    /**
     * 카테고리에 편성된 기간정액 상품 정보 조회
     * @param reqData = {
        said : 사용자 SAID
        catId : 카테고리 ID
        param : option
        }
     * @param callback (res, data)
     * response data 는 cornerList 로 구성
     * cornerList 는 아래의 데이터를 포함하고 있음
     *   {
     *       "pptCd": "PC2P87",
     *       "pptTitle": "한솔 활짝eE",
     *       "pptName": "한솔 활짝e 한글나라",
     *       "unifiedPptYn": "Y",
     *       "pptPrice": "2200|3400|4400",
     *       "pptPeriod": "24|48|26",
     *       "ltFlag": "1",
     *       "pptLtPrice": "2000|5400|5500",
     *       "pptLtPeriod": "744|800|900",
     *       "pptCatId": "10000000000000112936"
     *   }
     * cornerList 는 unifiedPptYn=N인 상품 중 첫번째와 Y인 상품중 첫번째 것만 사용한다.
     * 일반 월정액과 동일하다고 보면 됨
     * @private
     */
    function _getCatPptList(reqData, callback) {
        api = "/amoc-api/vod/category/ppt-list";
        data = "saId=" + reqData.saId + "&catId=" + reqData.catId;
        if (reqData.cmbYn) {
            data += ("&cmbYn=" + reqData.cmbYn);
        }
        return ajax(true, 0, AMOC_SERVER_HTTP_URL + api, data, callback, undefined, reqData.param);
    }

    return {
        ajaxStop: _ajaxStop,
//        useMyStarPointVodNxt: _useMyStarPointVodNxt,
//        useTvMoneyVodNxt: _useTvMoneyVodNxt,
        useComplexPayNew: _useComplexPayNew,
        requestOPC: _requestOPC,
        requestOAC: _requestOAC,
        registPrepaidCpn: _registPrepaidCpn,
        getMyExpireList: _getMyExpireList,
        authorizePVOD: _authorizePVOD,
        authorizeOTNPVOD: _authorizeOTNPVOD,
        authorizeSVOD: _authorizeSVOD,
        authorizeFVOD: _authorizeFVOD,
        authorizeZVOD: _authorizeZVOD,
        setVodEnd: _setVodEnd,
        getLinkTimeNxt: _getLinkTimeNxt,
        getViewHistEP4: _getViewHistEP4,
        setDelWatch: _setDelWatch,
        getContStatusNxt: _getContStatusNxt,
        getCateContNxtW3: _getCateContNxtW3,
        getCateContExtW3: _getCateContExtW3,
        getCateInfoW3: _getCateInfoW3,
        getContentW3: _getContentW3,
        getItemDetlListW3: _getItemDetlListW3,
        getCustEnv: _getCustEnv,
        getCustPkgInfo: _getCustPkgInfo,
        getCustCugInfo: _getCustCugInfo,
        getProductInfo: _getProductInfo,
        getPlayListNxt: _getPlayListNxt,
        setPlayListNxt: _setPlayListNxt,
        getViewHistoryNxt: _getViewHistoryNxt,
        getCustPkgList: _getCustPkgList,
        getBuyHistoryNxt: _getBuyHistoryNxt,
        setDelBuy: _setDelBuy,
        getCornerList: _getCornerList,
        getCategoryId: _getCategoryId,
        exchangeItemId: _exchangeItemId,
        getDetailContentInfo: _getDetailContentInfo,
        checkMyContentW3: _checkMyContentW3,
        checkMyPkgVodW3: _checkMyPkgVodW3,
        getItemPerInfo: _getItemPerInfo,
        getChannelInfo: _getChannelInfo,
        getMyPkgList: _getMyPkgList,
        getMyPlayItemList: _getMyPlayItemList,
        getMyPlayItemDetList: _getMyPlayItemDetList,
        setMyPlayItemDetList: _setMyPlayItemDetList,
        getAdditionalInfoW3: _getAdditionalInfoW3,
        getPkgVodW3: _getPkgVodW3,
        getMyPkgVodListW3: _getMyPkgVodListW3,
        checkShrContent: _checkShrContent,
        getCatPptList: _getCatPptList
    }
}());
