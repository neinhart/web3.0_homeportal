"use strict";
/**
 * @class 추천서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 *
 */
window.VodRecmManager = (function() {
    function getUrl() {
        if (DEF.RELEASE_MODE === CONSTANT.ENV.TEST) {
            return VOD.HTTP.RECOMMEND.TEST_URL;
        }
        else {
            return VOD.HTTP.RECOMMEND.LIVE_URL;
        }
    }

    function ajax(async, timeout, url, postData, /**Function*/callback) {
        return ajaxFactory.createAjax("VOD Recommand Manager", async, "post", "json", url, postData, true, -1, callback, null);
    }

    function _ajaxStop(request) {
        if (request) {
            log.printDbg("AJAX request stop!!");
            request.abort();
            request = null;
        }
    }

    var REQ_CODE = CONSTANT.STB_TYPE.ANDROID ? "G" : "E",
        SVC_CODE = CONSTANT.IS_OTS ? "OTS" : "OTV",
        STB_VER = CONSTANT.STB_TYPE.ANDROID ? "1.2" : (CONSTANT.IS_HDR ? "3.2" : "3.1");

    /**
     * 상세보기, 종료 시 추천 VOD
     * "ITEM_VOD2" : "ITEM_VOD", 라이브 VOD 추천
     */
    function _getRelatedContents(recType, recGbCode, said, product_list, consid, catid, hasAdult, itemCnt, callback) {
        log.printDbg("_getRelatedContents !!");
        var api = getUrl() + "wasRCS/recommend";
        var postData = {
            'CONTENT': "VOD",
            'REC_TYPE': recType,
            'REC_GB_CODE': recGbCode,
            'USER_ID': said,
            'PRODUCT_LIST': product_list,
            'REQ_DATE': HTool.getDateStr(),
            'REQ_CODE': REQ_CODE,
            'SVC_CODE': SVC_CODE,
            'CONS_ID': consid,
            'CAT_ID': catid,
            'CAT_TYPE': hasAdult == 'Y' ? "Adult" : "null",
            'ITEM_CNT': itemCnt,
            'CONS_RATE': "N", //필터링 옵션임. Y가 19세이상 필터링 (무조건 "N"으로 호출)
            'STB_VER': STB_VER,
            'STB_mode': KidsModeManager.isKidsMode() ? "Kids" : "Normal"
        };
        return ajax(true, 0, api, postData, callback);
    }

    return {
        ajaxStop: _ajaxStop,
        getRelatedContents: _getRelatedContents
    }
}());
