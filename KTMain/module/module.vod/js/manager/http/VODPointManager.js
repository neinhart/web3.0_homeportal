/**
 * Created by Yun on 2017-02-22.
 */

window.VODPointManager = (function () {

    var KTPG_SERVER_URL = DEF.RELEASE_MODE === CONSTANT.ENV.TEST ? VOD.HTTP.KTPG.TEST_URL : VOD.HTTP.KTPG.LIVE_URL;
    var LUPIN_SERVER_URL = DEF.RELEASE_MODE === CONSTANT.ENV.TEST ? VOD.HTTP.LUPIN.TEST_URL : VOD.HTTP.LUPIN.LIVE_URL;

    /**
     * ajax를 이용하여 데이터 수집<br>
     * 응답은 json 데이터로 오지만 요청헤더 contentType은 text로 보내야한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    function ajax(async, timeout, url, postData, /**Function*/callback, params) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return ajaxFactory.createAjax("PointManager", async, "post", "json", url, postData, true, -1, callback, params);
    }

    function ajax2(async, timeout, url, postData, /**Function*/callback, params) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return ajaxFactory.createAjaxServerTimeout("PointManager", async, "post", "json", url, postData, true, 6000, callback, params);
    }

    function _ajaxStop(request){
        if(request){
            request.abort();
        }
    }

    /**
     *
     * @param point_type : "1": myMenu 에서, 이용권 포함 호출
     *                     "9": 결제 진행에서 호출, 포인트, 쿠폰 만 호출
     * @param callback
     * @returns {*}
     * @private
     */
    // Lupin 서버 신규 API
    function _getPoint(point_type, callback) {
        var postData = {
            settlIdTypeCd: "7", // 결제 아이디 타입
            settlId      : KTW.SAID, // 결제 아이디
            storId       : "KTPGMTV002", // 가맹점 ID (현재는 사용하지 않음)
            retvTypeCd   : point_type? point_type : "9", // 조회 유형 코드
            ipadr        : getIP(), // IP address
            adtnInfo     : "" // 부가 정보
        };

        var createdData = {
            async   : true,
            url     : LUPIN_SERVER_URL + "retvPoint.json",
            postData: postData
        };

        KTW.utils.Log.printDbg("getTvPoint");
        return ajax2(true, 0, LUPIN_SERVER_URL + "retvPoint.json", postData, callback);
    }

    // 이용권, 할인권 조회
    // Lupin 서버 신규 API
    function _getStbCouponInfo(data, callback) {
        var postData = {
            settlIdTypeCd: "7", // 결제 아이디 타입
            settlId      : KTW.SAID, // 결제 아이디
            storId       : "KTPGMTV002", // 가맹점 아이디 (부가세 포함 끝자리 2, 미포함 1)
            retvTypeCd   : data.retvTypeCd, // 포인트 종류 (콘텐츠 이용권 3)
            ipadr        : getIP(),
            ctgryId      : data.ctgryId, // 카테고리 아이디
            contsId      : data.contsId // 콘텐츠 아이디
        };

        if (data.settlNm) {
            postData["settlNm"] = data.settlNm; // 콘텐츠 명
        }

        KTW.utils.Log.printDbg("getStbCouponInfo");
        return ajax(true, 0, LUPIN_SERVER_URL + "retvContsCoupnDtl.json",postData, callback, data.params);
    };

    function getIP(){
        return oipfAdapter.hwAdapter.networkInterface.getIpAddress();
    }

    return {
        ajaxStop : _ajaxStop,
        getPoint: _getPoint,
        getStbCouponInfo : _getStbCouponInfo
    }
}());