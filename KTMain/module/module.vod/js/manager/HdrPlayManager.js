/**
 * Created by ksk91_000 on 2017-06-01.
 */
"use strict";
window.HdrPlayManager = (function () {
    var HDR_POPUP_TYPE = {
        IMPOSSIBILITY : 0,
        RESOLUTION : 1
    };

    var SDR_SUPPORT = false;

    var data_obj;
    var button_check_obj;

    /**
     * HDR 콘텐츠 재생 시 HDR 관련 내용 체크하여 후 처리 수행
     * 1. 데이터
     * 2. VOD 구매 다음 수행 콜백
     * 3. 취소 시 콜백
     */
    function _checkHDRSupportForTv(obj) {
        log.printDbg("checkHDRSupportForTv() IS_HDR : " + CONSTANT.IS_HDR);

        // 만약 HDR을 지원하지 않는 STB이라면 무조건 바로 구매 process 처리하도록 리턴
        if (CONSTANT.IS_HDR === false) {
            return false;
        }

        var is_hdr = false;
        var is_hdr_tv = false;
        var resolution;
        var data;

        data_obj = obj;
        button_check_obj = data_obj.buyingCheck;
        data = data_obj.data;

        log.printDbg("data_obj : " + JSON.stringify(data_obj));
        log.printDbg("data : " + JSON.stringify(data));

        if (data && data.isHdrYn) {
            // isHdrYn 값이 있는지 확인
            // 없으면 "N"
            is_hdr = data.isHdrYn === "Y" ? true : false;
        }

        is_hdr_tv = oipfAdapter.basicAdapter.getConfigText(DEF.CONFIG.KEY.EDID) === "true";

        log.printDbg("checkHDRSupportForTv is_hdr : " + is_hdr);
        log.printDbg("checkHDRSupportForTv is_hdr_tv : " + is_hdr_tv);


    }

    /**
     * [jh.lee] HDR 재생 전 팝업
     */
    function showHDRErrorPopup(type) {
        log.printDbg("showHDRErrorPopup");
        var popup_data;
        var popup_view;

        // 코너 몰아보기를 선택해서 재생하는 경우 로딩을 그리기 때문에 로딩 스탑
        layerManager.stopLoading();
        hideHDRErrorPopup();

        if (type === HDR_POPUP_TYPE.IMPOSSIBILITY) {
            // 재생 불가
            // 단, SDR converting 지원하는 경우 별도 처리
            if (SDR_SUPPORT === true) {
                popup_data = {
                    title: "알림",
                    desc: [
                        {"size":"24", "color":"rgb(0,0,0)", "text":OTW.ERROR.EX_CODE.VOD.EX806[0]},
                        {"size":"24", "color":"rgb(0,0,0)", "text":OTW.ERROR.EX_CODE.VOD.EX806[1]},
                        {"size":"24", "color":"rgb(0,0,0)", "text":OTW.ERROR.EX_CODE.VOD.EX806[2]}
                    ],
                    type: OTW.ui.popup.BaseDialog.TYPE.DIALOG,
                    callback : onSDRPlay,
                };
            }
            else {
                popup_data = {
                    title: "알림",
                    desc: [
                        {"size":"24", "color":"rgb(0,0,0)", "text":OTW.ERROR.EX_CODE.VOD.EX802[0]},
                        {"size":"24", "color":"rgb(0,0,0)", "text":OTW.ERROR.EX_CODE.VOD.EX802[1]},
                        {"size":"24", "color":"rgb(0,0,0)", "text":OTW.ERROR.EX_CODE.VOD.EX802[2]}
                    ],
                    type: OTW.ui.popup.BaseDialog.TYPE.CONFIRM,
                    callback : onImpossibility
                };
            }
            popup_view = OTW.ui.popup.BaseDialog;
        } else {
            // 해상도 관련 팝업. 재생은 가능
            popup_data = {
                callback : onResolution,
                button_check_obj : button_check_obj
            };
            popup_view = OTW.ui.popup.HdrGuideDialog;
        }

        layerManager.activateLayer(
            {
                id: OTW.vod.Def.VIEW_ID.HDR_CHECK_POPUP_ID,
                type: OTW.ui.Layer.TYPE.POPUP,
                priority: OTW.ui.Layer.PRIORITY.POPUP,
                view : popup_view,
                params: {
                    data : popup_data,
                    enableDCA : false,
                    enableHotKey: !OTW.vod.VodInterface.onNextPlayBlock,
                    enableMenuKey: !OTW.vod.VodInterface.onNextPlayBlock,
                    enableChannelKey: !OTW.vod.VodInterface.onNextPlayBlock
                }
            });

        popup_data = null;
    }

    function hideHDRErrorPopup() {
        layerManager.deactivateLayer(OTW.vod.Def.VIEW_ID.HDR_CHECK_POPUP_ID, true);
    }

    function onImpossibility(result) {
        log.printDbg("onImpossibility");
        hideHDRErrorPopup();
        if (data_obj.onCancel) {
            // 취소 콜백 존재하는 경우 호출
            data_obj.onCancel();
        }
    }

    function onResolution(result) {
        log.printDbg("onResolution");
        if (result !== -1) {
            hideHDRErrorPopup();
            if (result === 0) {
                // 해상도 설정
                layerManager.activateLayer({id: OTW.ui.Layer.ID.SETTING, params: {menuId:menuDataManager.MENU_ID.SETTING_RESOLUTION}});
                if (data_obj.onCancel) {
                    // 취소 콜백 존재하는 경우 호출
                    data_obj.onCancel();
                }
            } else if (result === 1) {
                // VOD 구매
                data_obj.onBuyProcess();
            }
        } else {
            // 뒤로가기, 종료 키
            if (data_obj.onCancel) {
                // 취소 콜백 존재하는 경우 호출
                data_obj.onCancel();
            }
        }
    }

    function onSDRPlay(result) {
        log.printDbg("onSDRPlay() result = " + result);

        hideHDRErrorPopup();
        if (result === 0) {
            // 확인
            data_obj.onBuyProcess();
        }
        else {
            // 1 : 취소 버튼
            // -1(이외의 경우) : 뒤로가기, 종료 키
            if (data_obj.onCancel) {
                // 취소 콜백 존재하는 경우 호출
                data_obj.onCancel();
            }
        }
    }

    return {
        checkHDRSupportForTv : _checkHDRSupportForTv
    };

}());