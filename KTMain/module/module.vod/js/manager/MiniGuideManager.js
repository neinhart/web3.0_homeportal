/**
 * Created by ksk91_000 on 2017-02-24.
 */
window.MiniGuideManager = new function () {
    var div = _$("<div/>", {class: "miniguide_wrapper"});
    var vodMG = new VODMiniGuide();
    var mvMG = new MVMiniGuide();
    var cornerMG = new CornerMiniGuide();
    var playMiniIndicator = new PlayMiniIndicator();
    var currentMiniGuide = vodMG;
    var currentVODState = 0;

    this.setThumbnailData = function (data) {
        if (currentMiniGuide.hasOwnProperty("setThumbnailData")) currentMiniGuide.setThumbnailData(data)
    };

    this.setPlayState = function (state) {
        currentVODState = state;
        if (currentMiniGuide.hasOwnProperty("setPlayState")) currentMiniGuide.setPlayState(state);
    };

    this.setTitle = function () {
        currentMiniGuide.setTitle();
        // 코너 미니가이드 상태라면, VOD 미니가이드의 타이틀도 업데이트 해줘야 함
        if (currentMiniGuide.getType() == "Corner") vodMG.setTitle();
    };

    this.setRelationVod = function () {
        if (currentMiniGuide.hasOwnProperty("setRelationVod"))
            currentMiniGuide.setRelationVod(VODManager.getCurrentVODInfo().catId, VODManager.getCurrentVODInfo().adultOnlyYn);
    };

    this.setTime = function () {
        currentMiniGuide.setTime();
    };

    this.setThumbnailPosition = function (time) {
        if (currentMiniGuide.hasOwnProperty("setThumbnailPosition"))
            currentMiniGuide.setThumbnailPosition(time);
    };

    this.changeMode = function (mode) {
        currentMiniGuide = this.getMiniguide(mode);
        if (mode == "MV") playMiniIndicator.setMVMode();
        else playMiniIndicator.setNormalMode();
        div.empty().append(currentMiniGuide.getView());
        this.setTime();
    };

    this.show = function (alwaysShow, noRefresh) {
        // noRefresh 는 MVMiniGuide 에서만 사용함.
        // 미니가이드가 열려있을 때 VOD 컨트롤하면 보고있던 플레이리스트가 초기화 되는 현상 방지하기 위함
        currentMiniGuide.show(alwaysShow, (noRefresh && currentMiniGuide.isShowing()));
        playMiniIndicator.hideBg();
    };

    this.hide = function () {
        currentMiniGuide.hide();
        if (LayerManager.getLayer("CenterFeedback") && LayerManager.getLayer("CenterFeedback").isShowing() && currentVODState == VODManager.PLAY_STATE.PAUSE) playMiniIndicator.hideIndicator();
        else playMiniIndicator.showBg();
    };

    this.showMiniIndicatorBg = function () {
        playMiniIndicator.showBg();
    };
    this.hideMiniIndicatorBg = function () {
        playMiniIndicator.hideBg();
    };

    this.hideMiniIndicator = function () {
        if (playMiniIndicator) playMiniIndicator.hideIndicator();
    };

    // 미니컨트롤러 포함한 모든 View Hide
    this.hideAll = function () {
        vodMG.hide();
        mvMG.hide();
        cornerMG.hide();
        this.hideMiniIndicatorBg();
    };

    this.isShowing = function () {
        return currentMiniGuide.isShowing();
    };

    this.onKeyAction = function (keyCode) {
        if (!currentMiniGuide) return false;
        if (currentMiniGuide.getType() != "MV") this.setTime(VODManager.getPlayTime(), VODManager.getCurrentPosition());
        return currentMiniGuide.onKeyAction(keyCode)
    };

    this.getMiniguide = function (mode) {
        switch (mode) {
            case "VOD" :
                return vodMG;
            case "MV" :
                return mvMG;
            case "Corner" :
                return cornerMG;
        }
    };

    this.getState = function () {
        return currentMiniGuide.getType();
    };

    this.getView = function () {
        currentMiniGuide.hide();
        return div;
    };

    this.getMiniIndicatorView = function () {
        return playMiniIndicator.getView();
    };

    this.onChangedPlayState = function (state, speed, noIndicator) {
        if (!VODManager.getCurrentPlayLayer()) return;
        switch (state) {
            default:
                playMiniIndicator.showIndicator(state, speed);
                if (!noIndicator) {
                    if (state != VODManager.PLAY_STATE.PAUSE) this.show(null, true);
                }
                this.setPlayState(state);
        }
    };

    // 뮤직비디오 미니가이드가 아닐 때 연관메뉴 가이드 문구를 옵션에 따라 변경함
    this.setContextText = function (option) {
        if (currentMiniGuide.getType() !== "MV" && option != null) {
            var contextDiv = currentMiniGuide.div.find("#context_area");

            contextDiv.find(".contextText").text("");
            contextDiv.find(".contextText2").hide();
            contextDiv.find(".vertical_bar").hide();

            if (option.haveSMI && option.haveSound) {
                contextDiv.find(".contextText").text("자막");
                contextDiv.find(".contextText2").text("음성 변경").show();
                contextDiv.find(".vertical_bar").show();
            } else if (option.haveSMI) contextDiv.find(".contextText").text("자막 변경");
            else if (option.haveSound) contextDiv.find(".contextText").text("음성 변경");
            else {
                if (option.haveMultiView) contextDiv.find(".contextText").text("VOD-채널 동시시청");
                else contextDiv.find(".contextText").text("상세정보");
            }
        }
    };
};