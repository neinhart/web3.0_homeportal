/**
 * Created by Lazuli on 2017-01-09.
 */

window.VODManager = (function () {
    var PLAY_SPEED = [-32, -8, -2, 0, 0.8, 1, 1.2, 2, 8, 32];
    var passErrorCode = ["300", "40040", "40042", "40043", "40048", "40054", "40055", "40056", "40055", "40100", "40101", "40102", "40401", "40402", "42000"];
    var cemsReportErrorCode = ["40012", "40013", "30020", "30304", "31050", "40053"];
    var currentVOD = null, currentPlayLayer, currentState, currentSpeedIndex = 5, currentReqPathCd, prevVOD;
    var smiInstance;
    var isCorner, isAD = false, isLoading = true, retryCnt = 0;
    var smi, isKidsLastVod = false, kidsModule = null;
    var pauseTimeout = null;
    // 성급 VOD 재생 모드
    var isQuickContents = false;
    // 시리즈/플레이리스트 등 다음회차 이어볼수 있는 재생 모드
    var isContinuePlay = false;
    // 시리즈 재생 모드 (이어보기 또는 몰아보기)
    var isSeriesPlay = false;
    // 몰아보기 모드 (다음회차 재생 여부 팝업 노출 여부, true 이면 노출 안함)
    var isAutoContinue = false;
    // 미리보기 or 하이라이트 모드
    var isHighLight = false;
    // POST 플레이 여부
    var isForcedKidsIframe = false;
    var isPostPlay = false;
    var isKidsEndSound = null;
    var positionInterval = null, cacheCurrentPosition = null;
    var changeServiceByVODModule = false;
    var endPopupInfo = {contsId: null, catId: null};
    var screenPosition = null;
    var prepareTimeoutTimer;
    var indexListener = null;
    var isOtherAppState = false;
    var multiAudioSetComplete = false;
    var navInterfaceState = 0;

    function cachePosition() {
        if (positionInterval) {
            var positionTimeout = setTimeout(function () {
                if (positionInterval) {
                    cacheCurrentPosition = vodAdapter.getPlayPosition();

                    if (cacheCurrentPosition) {
                        cacheCurrentPosition = cacheCurrentPosition / 1E3;
                    }
                    else {
                        cacheCurrentPosition = 0;
                    }
                }
                clearTimeout(positionTimeout);
                positionTimeout = void 0;
                cachePosition();
            }, 10000);
        }
    }

    function init() {
        isLoading = true;
        isPostPlay = false;
        cacheCurrentPosition = 0;
        retryCnt = 0;

        MiniGuideManager.hideAll();

        if (watermarkAdapter) {
            watermarkAdapter.stop();
        }

        if (smiInstance) {
            smiInstance.stop();
            smiInstance.destroy();
        }

        if (vodAdapter) {
            vodAdapter.stop();
        }

        if (VODTimer) {
            VODTimer.clearTimer();
        }

        positionInterval = null;
        changeServiceByVODModule = false;
        multiAudioSetComplete = false;
        clearPrepareTimer();
    }

    function setLoadingIframe(_vodInfo, _isDefaultIframe) {
        try {
            if (IframeManager.getType() != IframeManager.DEF.TYPE.VOD_LOADING && IframeManager.getType() != IframeManager.DEF.TYPE.KIDS_VOD_LOADING) {
                isLoading = true;
                var vodInfo = null;

                if (_vodInfo) {
                    vodInfo = _vodInfo;
                }
                else {
                    vodInfo = currentVOD;
                }

                if (screenPosition) {
                    oipfAdapter.basicAdapter.resizeScreen(screenPosition.x, screenPosition.y, screenPosition.width, screenPosition.height);
                }
                else {
                    oipfAdapter.basicAdapter.resizeScreen(0, 0, CONSTANT.RESOLUTION.WIDTH, CONSTANT.RESOLUTION.HEIGHT);
                }
                IframeManager.changeIframe();

                log.printDbg("setLoadingIframe " + vodInfo.dispContsClass);

                if (_isDefaultIframe) {
                    IframeManager.setType(IframeManager.DEF.TYPE.DEFAULT_VOD_LOADING);
                }
                else {
                    if (vodInfo.dispContsClass == "06" || KidsModeManager.isKidsMode() || isForcedKidsIframe) {
                        IframeManager.setType(IframeManager.DEF.TYPE.KIDS_VOD_LOADING);
                    }
                    else {
                        IframeManager.setType(IframeManager.DEF.TYPE.VOD_LOADING);
                    }
                }
            }
        } catch (e) {
            log.printErr(e);
            IframeManager.setType(IframeManager.DEF.TYPE.VOD_LOADING);
        }

    }

    function hideLoadingIframe() {
        if (screenPosition) {
            oipfAdapter.basicAdapter.resizeScreen(screenPosition.x, screenPosition.y, screenPosition.width, screenPosition.height);
        }
        else {
            oipfAdapter.basicAdapter.resizeScreen(0, 0, CONSTANT.RESOLUTION.WIDTH, CONSTANT.RESOLUTION.HEIGHT);
        }

        IframeManager.setType(IframeManager.DEF.TYPE.NONE);
    }

    function showIndicator(state, speed, hideFunc) {
        if (LayerManager.getLayer("CenterFeedback")) LayerManager.deactivateLayer({
            id: "CenterFeedback", remove: false
        });
        if (state != null && state > -1) {
            LayerManager.activateLayer({
                obj: {
                    id: "CenterFeedback",
                    type: Layer.TYPE.BACKGROUND,
                    priority: Layer.PRIORITY.REMIND_POPUP,
                    linkage: true,
                    params: {state: state, speed: speed, hideFunc: hideFunc}
                }, moduleId: "module.vod", visible: true
            });
        }
    }

    /*
     ######################## 에러로직 관련
     */

    function _error() {
        log.printErr("VODManager ERROR");
        _exitVOD();
    }

    function _openErrorPopup(msg, isReboot, noAction) {
        LayerManager.activateLayer({
            obj: {
                id: "VODError", type: Layer.TYPE.POPUP, priority: Layer.PRIORITY.POPUP, linkage: true, params: {
                    title: "오류", message: msg, button: "닫기", reboot: isReboot, callback: noAction ? null : _error
                }
            }, moduleId: "module.vod", visible: true
        });
    }

    function _isPassErrorCode(_code) {
        for (var i = 0; i < passErrorCode.length; i++) {
            if (_code == passErrorCode[i]) return true;
        }
        return false;
    }

    /**
     * CAS STOP EVENT 수신 시 CEMS 리포트 체크 함수
     */
    function checkCemsError(reason) {
        var tmp_reason = "";
        if (reason) {
            // reason 이 존재하는 경우
            tmp_reason += reason;
            if (reason == 5 || reason == 8) {
                // ACAP 기준 reason = 5 VOD_PAS_INTERNAL_ERROR
                // ACAP 기준 reason = 8 VOD_PAS_METADATA_GET_FAIL
                extensionAdapter.notifySNMPError("VODE-" + reason);
            }
            else {
                // 그외에는 ACAP에서 사용하는 CEMS 에러코드에 존재하는 것만 리포트
                for (var i = 0; i < cemsReportErrorCode.length; i++) {
                    if (cemsReportErrorCode[i] == tmp_reason) extensionAdapter.notifySNMPError("VODE-" + tmp_reason);
                }
            }
        }
    }

    var errorMessageToCodeTable = {
        "1002": {message: "", code: 16, name: "VODCAStopEvent"},
        "1002.0": {message: "00006", code: 16, name: "VODCAStopEvent(00006)"},
        "1002.9": {message: "(01000)", code: 16, name: "VODCAStopEvent(01000)"},
        "1003": {message: "00004", code: 1, name: "InternalErrorEvent"},
        "1004": {message: "00005", code: 2, name: "ControllerErrorEvent"},
        "1005": {message: "", code: -1, name: "ControllerClosedEvent"},
        "1006": {message: "00007", code: 5, name: "ServiceRemovedEvent"},
        "1007": {message: "00008", code: 6, name: "StopByResourceLossEvent"},
        "1008": {message: "", code: -1, name: "ResourceWithdrawnEvent"},
        "1009": {message: "", code: -1, name: "ResourceReturnedEvent"},
        "1010": {message: "00009", code: 10, name: "DataStarvedEvent"},
        "1011": {message: "00010", code: 11, name: "ResourceUnavailableEvent"}
    };

    function retryVODPlayOnError(errorPosition, retryFailFunc) {
        extensionAdapter.notifySNMPError("VOD-chgE-001");

        if (retryCnt == 0) {
            retryCnt = 1;
            // 2017.08.22 Lazuli
            // prevVOD가 없는 상태에서 절체로 인해 VOD가 다시 재생 될 경우 자막 설정이 초기화 되는 이슈가 있음
            // 자막 설정 등을 유지하기 위해 이전 VOD 값을 현재의 VOD 값으로 바꿔줌
            prevVOD = JSON.parse(JSON.stringify(currentVOD));
            var url = currentVOD.resetupUrl;

            if (errorPosition > 0 && errorPosition != currentVOD.getLinkTime.linkTime) {
                vodAdapter.setPlayUrl(_getLinkTimeUrl(url, errorPosition));
            }
            else {
                vodAdapter.setPlayUrl(_getLinkTimeUrl(url, currentVOD.getLinkTime.linkTime > 0 ? currentVOD.getLinkTime.linkTime : false));
            }

            _setMiniGuide();
            _setSMI();
            _changeSMILanguage(_getDefaultSMILanguageIdx(), true);

            LayerManager.getLayer("VodPlayerLayer").load();
        }
        else {
            retryFailFunc();
        }
    }

    function processErrorCode(param) {
        var error = {
            code: 0, message: param.message, originalCode: param.error
        };

        switch (Number(param.error)) {
            case -9001://prepare timeout.
            case -9002:
                error.code = -3;
                error.message = param.message;
                break;
            case 1001: // ConnectionErrorEvent
                if (!param.message) {
                    error.code = 0;
                }
                else if (param.message == "42000") {
                    error.code = 15;
                }
                else {
                    if (_isPassErrorCode(param.message)) {
                        //unknown error 이고
                        //-1 이면 아무것도 안함 state 도 play 로 변경해 줌.
                        error.code = -1;
                        //아무것도 안하고 state
                    }
                    else {
                        error.code = 0;
                        // ConnectionErrorEvent 의 경우에 30020 및 40012 전달됨
                        // 그러므로 CEMS 전달여부를 확인하고 처리될 수 있도록 보완함
                        checkCemsError(param.message);
                    }
                }
                break;
            case 1002: //VODCAStopEvent
                if (param.message == 0) {
                    error = errorMessageToCodeTable["1002.0"];
                }
                else if (param.message == 9) {
                    error = errorMessageToCodeTable["1002.9"];
                }
                else {
                    error = errorMessageToCodeTable["1002"];
                    error.message = param.message;
                }
                // R6 CEMS
                checkCemsError(param.message);
                break;
            case 1003: //InternalErrorEvent
            case 1004: //ControllerErrorEvent
            case 1005: //ControllerClosedEvent
            case 1006: //ServiceRemovedEvent
            case 1007: //StopByResourceLossEvent
            case 1008: //ResourceWithdrawnEvent
            case 1009: //ResourceReturnedEvent
            case 1010: //DataStarvedEvent
            case 1011: //ResourceUnavailableEvent
                error = errorMessageToCodeTable[param.error];
                // error code 1003 ~ 1011 까지 전달되는 message 중
                // CEMS 연동이 필요한지 체크하여 처리될 수 있도록 보완 처리함
                if (error.code !== -1) {
                    checkCemsError(param.message);
                }
                break;
            default:
                //popup 만 보여준다 ?.
                error.code = -2;
                error.message = param.message;
                break;
        }

        return error;
    }

    function processError(error) {
        log.printDbg("[VOD/P] processError:" + JSON.stringify(error));
        var errMsg = null;
        clearPrepareTimer();

        if (error.code >= 0 && error.code < 12) {
            errMsg = ERROR_TEXT.ET000;

            if (error.message) {
                errMsg = errMsg.concat(["", "(VODE-" + error.message + ")"]);
            }

            if (!KTW.managers.device.NetworkManager.isNetworkConnected()) {
                _setVodEndTime(true);
                _openErrorPopup(errMsg);
            }
            else {
                log.printWarn("RetryPlayVOD... retry count is " + retryCnt);
                retryVODPlayOnError(_setVodEndTime(), function () {
                    log.printWarn("RetryPlayVOD Fail");
                    _setVodEndTime(true);
                    _openErrorPopup(errMsg);
                });
            }
        }
        else if (error.code == 15) {
            // "1001", "42000" -> 이거 skipped 에 있는데 ?
            // 이게 뭔질 모르니, 처리를 할 수 없다. error 조차..
            // 하지만 아무것도 안함. 근데 왜 따로 parsing 한걸까 ?
            log.printWarn("[VOD/P] we are not process this error currently, error.code:" + error.code);
        }
        else if (error.code == 16) { //"1002", Cas 오류.
            errMsg = ERROR_TEXT.ET_REBOOT.concat(["", ERROR_TEXT.ET_CALL[0]]);

            if (error.message) {
                errMsg = errMsg.concat(["(VODE-" + error.message + ")"]);
            }

            _openErrorPopup(errMsg, true);
        }
        else if (error.code == -2) {
            // WEB2.0에서도 Popup만 노출하도록 되어 있음
            errMsg = ERROR_TEXT.ET000;

            if (error.message) {
                errMsg = errMsg.concat(["(VODE-" + error.message + ")"]);
            }

            _openErrorPopup(errMsg, false, true);
        }
        else {
            log.printWarn("[VOD/P] we are not process this error currently, error.code:" + error.code);
            //이 경우는 아무것도 안하는게 맞다.
        }
    }

    /*
     ### EventListener
     */

    // 2017.08.09 Lazuli
    // 홈포탈 상태를 판단하여 워터마크를 보여주거나 숨김
    // 광고 중일 경우, AV 사이즈가 변경됐을 경우, OTHER_APP 실행중인 경우 숨김
    function _watermarkVisibleCheck() {
        if (watermarkAdapter) {
            var watermarkShow = false;

            if (isAD || isOtherAppState) {
                watermarkShow = false;
            }
            else {
                if (screenPosition) {
                    watermarkShow = (screenPosition.x == 0 && screenPosition.width == 1920);
                }
                else {
                    watermarkShow = true;
                }
            }
            watermarkAdapter.changeVisible(watermarkShow);
            log.printDbg("Watermark Visible Change === isShow ? " + watermarkShow);
        }
    }

    function _onPlaySpeedChanged(_speed) {
        log.printDbg("onPlaySpeedChanged ::: " + _speed);
        _speed = _speed.toFixed(1);

        if (!isAD) {
            VodNavInterface.changePlaySpeed(_speed);
        }

        if (_speed == 0) {
            currentSpeedIndex = 3;

            if (currentState != VODManager.PLAY_STATE.STOP) {
                currentState = VODManager.PLAY_STATE.PAUSE;
            }

            if (isCorner || isHighLight) {
                VODTimer.clearTimer();
            }
        }
        else if (_speed == 1) {
            currentSpeedIndex = 5;
            currentState = VODManager.PLAY_STATE.PLAY;
            MiniGuideManager.onChangedPlayState(currentState, 1, true);

            if (!isAD) {
                if (isCorner) VODTimer.createTimeout(HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend) - vodAdapter.getPlayPosition());
                if (isHighLight) VODTimer.createTimeout(currentVOD.previewEndTime - vodAdapter.getPlayPosition(), true);
            }
        }
        else {
            if (!isAD) {
                if (_speed < 0) {
                    if (isCorner) VODTimer.createInterval("rew", HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerstart));
                    if (isHighLight) VODTimer.createInterval("rew", currentVOD.previewStartTime, true);
                }
                else {
                    if (isCorner) VODTimer.createInterval("fwd", HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend));
                    if (isHighLight) VODTimer.createInterval("fwd", currentVOD.previewEndTime, true);
                }
            }

            MiniGuideManager.onChangedPlayState(currentState, _speed);
            showIndicator(currentState, _speed);
        }

        if (currentState != VODManager.PLAY_STATE.PAUSE) {
            clearPauseTimeout();
        }
    }

    function _onPlayStateChangeListener(_stateObj) {
        log.printDbg("onPlayStateChanged ::: " + _stateObj.state);

        switch (_stateObj.state) {
            case 1:
                clearPrepareTimer();
                hideLoadingIframe();
                watermarkAdapter.start(_watermarkVisibleCheck);

                if (smiInstance && !isAD) {
                    smiInstance.play(vodAdapter.getRealMpegObject());
                }
                isLoading = false;
                break;
            case 2:
            case 3:
            case 4:
                break;
            case 5:
                log.printDbg("Play Finished !!!");
                if (isKidsEndSound) {
                    // 키즈 사운드가 종료된 경우 STB 종료 함수 호출
                    isKidsEndSound();
                    _exitVOD(null, true);
                }
                else _finish(true);
                break;
            case 6:
                log.printErr("Play ERROR ::: " + _stateObj.error + " ::: " + _stateObj.message);
                //error code 처리.
                processError(processErrorCode(_stateObj));
                if (isKidsEndSound) isKidsEndSound();
        }
    }

    function _onPlayPositionChanged(_position) {
        log.printDbg("onPlayPositionChanged ::: " + _position);
        cacheCurrentPosition = _position || vodAdapter.getPlayPosition();

        if (cacheCurrentPosition) {
            cacheCurrentPosition = cacheCurrentPosition / 1E3;
        }
        else {
            cacheCurrentPosition = 0;
        }

        MiniGuideManager.setTime();
    }

    function _onReadyToPlay() {
        log.printDbg("onReadyToPlay !!! ");

        if (isKidsEndSound) {
            return;
        }

        positionInterval = true;

        cachePosition();
        clearPrepareTimer();

        MiniGuideManager.setTime(_getPlayTime(), currentVOD.getLinkTime.linkTime * 1E3);

        hideLoadingIframe();
        currentPlayLayer.onLoadComplete();

        log.printDbg("WaterMark Info === " + vodAdapter.getPlayPosition() + ", " + currentVOD.prInfo + ", " + currentVOD.getLinkTime.waterMark);
        watermarkAdapter.prepare(vodAdapter.getPlayPosition(), currentVOD.prInfo, currentVOD.getLinkTime.waterMark);

        VodNavInterface.notifyStartVOD(vodAdapter.getADSInfo(), {
            const_id: currentVOD.assetId,
            cat_id: currentVOD.catId,
            series_id: (isSeriesPlay ? currentVOD.catId : null),
            viewpoint: currentVOD.getLinkTime.linkTime > 0 ? currentVOD.getLinkTime.linkTime : 0,
            duration: Math.floor(vodAdapter.getPlayTime() / 1000),
            rating: currentVOD.prInfo
        });

        if (VODInfoManager.checkKidsVODCnt()) {
            isKidsLastVod = true;
            kidsModeCall("kidsLastVODCount");
            log.printDbg("KidsMode - LastVOD Start");
        }
        else {
            isKidsLastVod = false;
        }

        isLoading = false;

        if (currentVOD.capUrl && smi.language.length > 0) {
            var lang;
            if (currentVOD.smiLanguage && currentVOD.smiLanguage != "hideSmi") {
                for (var i = 0; i < smi.language.length; i++) if (smi.language[i] == currentVOD.smiLanguage) {
                    lang = _transLanguage(currentVOD.smiLanguage);
                    break;
                }
            }
            else {
                lang = smi.language[0] || smi.language[1];
                lang = _transLanguage(lang);
            }
            showToast([lang + " 자막 서비스 제공 중입니다.", "자막 설정"], "SMI");
        }
        if (isQuickContents) {
            showToast("현재 방송중인 프로그램의 VOD 입니다");
        }
        // 썸네일 셋팅 (음악플레이바는 썸네일 제공하지 않으므로 조회 안함)
        // 2018.02.22 TaeSang : 미리보기 시에도 썸네일 제공하지 않음
        if (currentVOD.dispContsClass == "01" || isHighLight) {
            currentPlayLayer.onThumbLoadComplete([]);
        }
        else {
            VODInfoManager.getThumbnail(function (_thumbData) {
                if (_thumbData && parseInt(_thumbData.result) == 200) {
                    currentVOD.thumbnailList = _thumbData.items;
                    currentPlayLayer.onThumbLoadComplete(currentVOD.thumbnailList);
                }
            }, currentVOD.assetId);
        }
        // 시리즈 상세화면이 등록한 리스너가 존재하는 경우 재생 시작한 컨텐츠 정보를 넘겨줌
        if (indexListener) {
            indexListener(currentVOD.catId, currentVOD.originData.data.contsId, currentVOD.originData.data);
        }

        _setMultiAudio();

        VodNavInterface.playVOD(currentVOD.contsName, currentVOD.assetId, currentVOD.dispContsClass);
        navInterfaceState = 0;
    }

    function _onMediaContentChanged(_mideaState) {
        log.printDbg("onMediaContentChanged ::: " + _mideaState);
        log.printDbg("getCurrentAdvertType ::: " + vodAdapter.getCurrentAdvertType());

        if (isKidsEndSound) {
            return;
        }

        var adType = vodAdapter.getCurrentAdvertType();

        if (_mideaState == 2 || _mideaState == 8) {
            isAD = true;
            clearPrepareTimer();
            if (adType !== null && adType >= -2 && adType <= 1) {
                // [WEB2.0 Comment]
                // 실제 광고일때만 아래 로직을 타도록 정의한다. CAS Error Event의 경우 adType 이 null,
                // 터무니 없는 값이 오게 되는데, 이때 setPlaySpeed 호출로 인해 Play를 요청하며, 이 때문에
                // Play -> Cas Error Event -> getCurrentAdvertType -> setPlaySpeed -> Play … (반복) 되는 이슈가
                // 발생 한다. 따라서 adType 이 null 이 아닌 경우에만 동작하도록 수정.

                // 정상 배속으로 변경
                _changeNormalSpeed();
                _changeSMIVisible(false);
                _watermarkVisibleCheck();
            }
            // 종료 광고일 경우 이어보기 시간 0으로 설정
            if (adType == -2) {
                cacheCurrentPosition = 0;
                positionInterval = null;
                isPostPlay = true;
                // 종료 광고일 경우 워터마크 종료
                if (watermarkAdapter) {
                    watermarkAdapter.stop();
                }
            }
            else {
                isPostPlay = false;
            }
        }
        else if (_mideaState == 3 || _mideaState == 9) {
            isAD = false;
            // 2017.08.22 Lazuli
            // VOD가 시작했음에도 아이프레임이 사라지지 않는 이슈가 있었음
            // 광고 종료 시 아이프레임 숨기도록 단말팀 가이드 받음
            // 이렇게 수정 후 로딩 아이프레임이 아주 잠깐 보이는 이슈가 있으며, VOD 시작 시 블랙 화면이 잠시 보이는 이슈가 있음
            hideLoadingIframe();
            clearPrepareTimer();
            _changeSMIVisible(true);

            if (!isPostPlay) {
                if (isCorner) VODTimer.createTimeout(HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend) - vodAdapter.getPlayPosition());
                if (isHighLight) {
                    currentPlayLayer.showRedKeyPromo(_prePlayProcessImplForPreview);
                    VODTimer.createTimeout(currentVOD.previewRunTime * 1000, true);
                }
            }

            MiniGuideManager.setTime();

            if (smiInstance) {
                smiInstance.play(vodAdapter.getRealMpegObject());
            }

            if (watermarkAdapter) {
                // 종료 광고가 아닐 경우 워터마크 다시 시작
                if (!isPostPlay) {
                    watermarkAdapter.start(_watermarkVisibleCheck);
                }
                // 2017.08.09 Lazuli
                // 중간광고가 종료됐을 경우엔 워터마크 Visible 체크하여 워터마크를 보이거나 숨김
                if (adType == 1) {
                    _watermarkVisibleCheck();
                }
            }
            _setMultiAudio(true);
        }
    }

    function _onPlaybackEndInProgressListener() {
        if (isQuickContents && currentState != VODManager.PLAY_STATE.PLAY) {
            showToast("현재 방송중인 프로그램이므로 더 이상 배속할 수 없습니다");
        }

        _changeNormalSpeed();
    }

    function kidsModeCall(method) {
        kidsModule = ModuleManager.getModule("module.kids");

        if (!kidsModule) {
            ModuleManager.loadModule({
                moduleId: "module.kids", cbLoadModule: function (success) {
                    if (success) {
                        kidsModeCall(method);
                    }
                }
            });
        }
        else {
            kidsModule.execute({
                method: method, params: null
            });
        }
    }

    /*
     ### Authorize
     */

    function _getLinkTimeUrl(_url, _linkTime) {
        if (_linkTime) {
            _url += "@" + 1E3 * parseInt(_linkTime);
        }

        return _url;
    }

    function _authorizeComplete(_result, _authData, _isFromHighLight) {
        if (!_result || !_authData) {
            extensionAdapter.notifySNMPError("VODE-00013");
            _openErrorPopup(ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), true);
        }
        else if (_authData.flag == 1) {
            extensionAdapter.notifySNMPError("VODE-00011");
            _openErrorPopup(ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00011)"]), true);
        }
        else if (_authData.flag == 2) {
            _openErrorPopup(ERROR_TEXT.ET003, false);
        }
        else if (_authData.flag == 3) {
            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인", callback: _error});
        }
        else if (_authData.flag == 0) {
            currentVOD.authorize = _authData;
            currentVOD.assetUrl = _authData.vodServer1 + "?p=" + _authData.advParam + ":S3;;" + _authData.ticketId + "X:";
            currentVOD.assetUrl2 = _authData.vodServer2 + "?p=" + _authData.advParam + ":S3;;" + _authData.ticketId + "X:";
            currentVOD.resetupUrl = _authData.vodServer2 + "?p=" + _authData.advParam + ":S3&resetup=y;;" + _authData.ticketId + "X:";

            // 미리보기 or 하이라이트 보기 시 이미 실행했으므로 아래 과정 생략함
            if (_isFromHighLight == null) {
                vodAdapter.setPlayUrl(_getLinkTimeUrl(currentVOD.assetUrl, currentVOD.getLinkTime.linkTime > 0 ? currentVOD.getLinkTime.linkTime : false));

                _setMiniGuide();
                _setSMI();

                // 2017.08.07 Lazuli
                // VOD가 Play되기 전에 자막이 먼저 Play되는 이슈가 있음
                // 이곳에서는 자막 언어 설정만 하고 자막을 재생하지는 않도록 수정 (자막 재생은 onPlayStateChange : 1)
                _changeSMILanguage(_getDefaultSMILanguageIdx(), true);
                _resizeScreenListener(basicAdapter.getScreenSize());
                LayerManager.getLayer("VodPlayerLayer").load();
            }
            else _isFromHighLight();
            // 최근 시청 캐시
            var familyModule = ModuleManager.getModule("module.family_home");
            var kidsModule = ModuleManager.getModule("module.kids");

            // 2017.08.07 Lazuli
            // 부가영상/예고편은 최근시청목록 캐시하지 않음
            // 2018.02.22 TaeSang
            // 미리보기or하이라이트 보기 영상도 최근시청목록에 캐시하지 않도록 추가
            if (familyModule && !currentVOD.isAdditional && !isHighLight) {
                familyModule.execute({
                    method: "addToRecentlyList",
                    data: currentVOD.recentlyData,
                    linkTime: currentVOD.getLinkTime.linkTime
                });
            }

            if (kidsModule && !currentVOD.isAdditional && !isHighLight) {
                kidsModule.execute({method: "kidsRecentlyListCheck"});
            }
        }
        else {
            _openErrorPopup(ERROR_TEXT.ET006);
        }
    }

    function _setMiniGuide() {
        if (currentVOD.dispContsClass == "01") {
            MiniGuideManager.changeMode("MV");
        }
        else if (isCorner) {
            MiniGuideManager.changeMode("Corner");
        }
        else {
            MiniGuideManager.changeMode("VOD");
        }
    }

    function _requestAuthorize(_isFromHighLight) {
        if (currentVOD.getLinkTime != null) currentVOD.getLinkTime.linkTime = parseInt(currentVOD.getLinkTime.linkTime) > 0 ? parseInt(currentVOD.getLinkTime.linkTime) : 0;
        if (currentVOD.authType == "P") {
            VODAmocManager.authorizePVOD(function (_result, _data) {
                _authorizeComplete(_result, _data, _isFromHighLight);
            }, DEF.SAID, currentVOD.assetId, currentVOD.getLinkTime.buyingDate, currentVOD.catId, "H", isCorner ? "44" : currentReqPathCd, currentVOD.getLinkTime.linkTime, null, currentVOD.getLinkTime.pptBuyYn == "Y" ? currentVOD.getLinkTime.pptPkgCd : null);
        }
        else if (currentVOD.authType == "S") {
            VODAmocManager.authorizeSVOD(function (_result, _data) {
                _authorizeComplete(_result, _data, _isFromHighLight);
            }, DEF.SAID, currentVOD.assetId, HTool.getDateStr(), currentVOD.catId, "H", isCorner ? "44" : currentReqPathCd, currentVOD.getLinkTime.linkTime, null);
        }
        else if (currentVOD.authType == "F") {
            VODAmocManager.authorizeFVOD(function (_result, _data) {
                _authorizeComplete(_result, _data, _isFromHighLight);
            }, DEF.SAID, currentVOD.assetId, HTool.getDateStr(), currentVOD.catId, "H", isCorner ? "44" : currentReqPathCd, currentVOD.getLinkTime.linkTime, null);
        }
        else if (currentVOD.authType == "O") {
            /**
             * [WEBIIIHOME-3664] authorizeOTNPVOD 호출시 실제 서버에서 주는 buyingData 값으로 하도록 수정
             * - 필히 테스트 요망
             */
            var buyingDate = HTool.getDateStr(); // buyingDate 없으면 현재 시간으로 전달
            if (currentVOD.originData.otmBuyingDate) {
                buyingDate = currentVOD.originData.otmBuyingDate;
            }

            VODAmocManager.authorizeOTNPVOD(function (_result, _data) {
                _authorizeComplete(_result, _data, _isFromHighLight);
            }, DEF.SAID, currentVOD.assetId, buyingDate, currentVOD.catId, "H", isCorner ? "44" : currentReqPathCd, currentVOD.getLinkTime.linkTime, null);
        }
        else if (currentVOD.authType == "Z") {
            VODAmocManager.authorizeZVOD(function (_result, _data) {
                _authorizeComplete(_result, _data, _isFromHighLight);
            }, DEF.SAID, currentVOD.assetId, HTool.getDateStr(), currentVOD.catId, "H", currentReqPathCd);
        }
    }

    /*
     ### RequestPlay
     */

    function _resizeScreenListener(_screenPosition) {
        screenPosition = _screenPosition;
        if (_screenPosition.x == 0 && _screenPosition.width == 1920) _changeSMIPosition();
        else _changeSMIPosition(_screenPosition, true);
        _watermarkVisibleCheck();
    }

    // 자막 설정
    function _setSMI(langIdx) {
        if (currentVOD.capUrl) {
            langIdx = langIdx || 0;
            smi = smiAdapter.getSMIInfo(currentVOD.capUrl, currentVOD.assetId);
            if (smi.language.indexOf("hideSmi") < 0) smi.language.push("hideSmi");
            if (!smi.language[langIdx] || smi.language[langIdx] == "hideSmi") {
                currentVOD.smiLanguage = "hideSmi";
                return false;
            }
            if (!smiInstance) smiInstance = smiAdapter.getInstance();
            smiInstance.prepare(smi.url + "&Language=" + smi.language[langIdx], _$(parent.document.getElementById('mainVBOArea')).find(".smiArea")[0]);
            currentVOD.smiLanguage = smi.language[langIdx];
            _changeSMIPosition();
            return true;
        }
        else {
            smi = null;
            return false;
        }
    }

    // 자막 위치 변경
    function _changeSMIPosition(_screenPosition, _isResize) {
        if (smiInstance && _$(parent.document.getElementById('mainVBOArea')).find(".smiArea #subtitlediv") && _$(parent.document.getElementById('mainVBOArea')).find(".smiArea #subtitlediv").length > 0) {
            var smiEl = _$(parent.document.getElementById('mainVBOArea')).find(".smiArea #subtitlediv");
            if (_isResize) {
                smiEl.css({
                    "left": _screenPosition.x + "px",
                    "width": _screenPosition.width + "px",
                    "bottom": (1080 - _screenPosition.y - _screenPosition.height) + "px"
                });
                smiInstance.resize();
                smiEl.css({"font-size": "22px", "line-height": "30px", "font-family": "RixHead M"});
            }
            else {
                smiEl.css({"left": "0px", "width": "1920px", "bottom": "0px"});
                smiInstance.resize();
                smiEl.css({"font-size": "55px", "line-height": "75px", "font-family": "RixHead M"});
            }
        }
    }

    // 자막 노출 여부 변경
    function _changeSMIVisible(isVisible) {
        if (smiInstance && _$(parent.document.getElementById('mainVBOArea')).find(".smiArea #subtitlediv") && _$(parent.document.getElementById('mainVBOArea')).find(".smiArea #subtitlediv").length > 0) {
            var smiEl = _$(parent.document.getElementById('mainVBOArea')).find(".smiArea #subtitlediv");
            if (isVisible) smiEl.css("display", "inherit");
            else smiEl.css("display", "none");

        }
    }

    // 자막 변경
    function _changeSMILanguage(langIdx, noPlay) {
        if (smiInstance) {
            smiInstance.stop();
            smiInstance.destroy();
            // 2017.08.07 Lazuli
            // VOD가 Play되기 전에 자막이 먼저 Play되는 이슈가 있음
            // 실질적인 자막 재생은 VOD가 정상적으로 Play 된 이후에 하도록 수정
            if (_setSMI(langIdx) && !noPlay) smiInstance.play(vodAdapter.getRealMpegObject());
        }
    }

    function _getDefaultSMILanguageIdx() {
        // Default 값은 재생할 VOD 설정의 기본 값
        var idx = 0, i = 0;
        if (_getSMILanguage().length > 0) {
            if (prevVOD && isContinuePlay) {
                // 이전 VOD가 존재하고, 몰아보기 중일 경우
                if (prevVOD.basicCaptViewYn) {
                    // 이전 VOD 자막 노출 여부 : Y
                    if (currentVOD.basicCaptViewYn && prevVOD.smiLanguage) {
                        // 현재 VOD 자막 노출 여부 : Y (종료된 VOD 설정 상태 유지)
                        log.printDbg("PrevbasicCaptViewYn ::: " + prevVOD.basicCaptViewYn + ", PrevSMILanguage ::: " + prevVOD.smiLanguage);
                        for (i = 0; i < _getSMILanguage().length; i++) if (_getSMILanguage()[i].toLowerCase() == prevVOD.smiLanguage.toLowerCase()) {
                            idx = i;
                            break;
                        }
                        // 현재 VOD 자막 노출 여부 : N (자막의 기본 값을 "없음"으로 셋팅)
                    }
                    else idx = _getSMILanguage().length - 1;
                }
                else if (!currentVOD.basicCaptViewYn) {
                    // 이전 VOD 자막 노출 여부 : N, 현재 VOD 자막 노출 여부 : N (종료된 VOD 설정 상태 유지)
                    log.printDbg("PrevbasicCaptViewYn ::: " + prevVOD.basicCaptViewYn + ", PrevSMILanguage ::: " + prevVOD.smiLanguage);
                    for (i = 0; i < _getSMILanguage().length; i++) if (_getSMILanguage()[i].toLowerCase() == prevVOD.smiLanguage.toLowerCase()) {
                        idx = i;
                        break;
                    }
                }
                // 이전 VOD가 없을 경우, 현재 VOD 자막 노출 여부 :N (자막의 기본 값을 "없음"으로 셋팅)
            }
            else if (!currentVOD.basicCaptViewYn) idx = _getSMILanguage().length - 1;
        }
        return idx;
    }

    function _getSMILanguage() {
        return smi && smi.language ? smi.language : [];
    }

    function _setMultiAudio(_noTimer) {
        if (!isAD && !multiAudioSetComplete) {
            // 오디오 셋팅 (getComponent가 제대로 오지 않는 경우가 있어 의도적으로 Timer 셋팅해줌)
            var multiAudioSetting = setTimeout(function () {
                currentPlayLayer.getContextMenu().setAudioComponent(function (result) {
                    multiAudioSetComplete = result
                });
                clearTimeout(multiAudioSetting);
                multiAudioSetting = null;
            }, _noTimer ? 0 : 500);
        }
    }

    function _getDefaultSoundIdx(_audioComponent) {
        var idx = -1, i = 0;
        if (isContinuePlay && prevVOD && prevVOD.audioLanguage) {
            for (i = 0; i < _audioComponent.length; i++) if (_audioComponent.item(i).language.toLowerCase() == prevVOD.audioLanguage.toLowerCase()) {
                idx = i;
                currentVOD.audioLanguage = prevVOD.audioLanguage;
                return idx;
            }
        }
        if (currentVOD.defAudio) {
            for (i = 0; i < _audioComponent.length; i++) if (_audioComponent.item(i).language.toLowerCase() == currentVOD.defAudio.toLowerCase()) {
                idx = i;
                currentVOD.audioLanguage = _audioComponent.item(i).language;
                return idx;
            }
        }
        if (!currentVOD.audioLanguage) currentVOD.audioLanguage = vodAdapter.getCurrentActiveComponents(1).item(0).language;
        return idx;
    }

    function _setAudioLanguage(lang) {
        currentVOD.audioLanguage = lang
    }

    function _activatePlayer(_notDestroy) {
        if (!_notDestroy) LayerManager.hideNormalLayer();
        LayerManager.activateLayer({
            obj: {
                id: "VodPlayerLayer",
                type: Layer.TYPE.TRIGGER,
                priority: Layer.PRIORITY.TRIGGER,
                params: {title: currentVOD.contsName}
            }, moduleId: "module.vod", visible: true, cbActivate: function () {

            }
        });
    }

    function clearPrepareTimer() {
        if (prepareTimeoutTimer) {
            clearTimeout(prepareTimeoutTimer);
            prepareTimeoutTimer = null;
        }
    }

    function _play(_playLayer) {
        currentPlayLayer = _playLayer;
        currentState = VODManager.PLAY_STATE.PLAY;
        vodAdapter.play(1);
        clearPrepareTimer();
        prepareTimeoutTimer = setTimeout(function () {
            log.printDbg("[VOD/P] prepare timeout");
            _onPlayStateChangeListener({state: 6, error: -9001, message: "FF0001"});
        }, 40 * 1000);
        VodNavInterface.loadContent();

        // Web2.0 로직 그대로 가져옴
        // 2분룰 혹은 연령제한 중 VOD 시청 직후는 무조건 parental rating 이 다시 체크되어야 하는 이유
        try {
            KTW.oipf.AdapterHandler.casAdapter.initializeLimited();
        } catch (e) {
            log.printErr(e);
        }
    }

    // 키즈 사운드 재생 요청
    function _requestPlayKidsSound(params) {
        // 사운드 재생 후 호출할 Callback 함수 등록
        isKidsEndSound = params.onSoundEnded;
        if (params.setVodEndTime && !isHighLight) _setVodEndTime();
        // 사운드 재생
        if (params.soundUrl != null) {
            // 사운드 재생 종료 후 exitVOD 호출함
            initVODManager();
            vodAdapter.setPlayUrl(params.soundUrl);
            vodAdapter.play(1);
        }
        else if (isKidsEndSound) {
            // 캐릭터가 설정되어 있지 않아, 사운드 재생 없이 바로 종료하는 경우
            // callback 함수 호출 후 바로 exitVOD 호출
            isKidsEndSound();
            _exitVOD(null, true);
        }
        isKidsLastVod = false;
    }

    function _requestPlay(_recvContent, _reqPathCd, _notDestroy, hasListener) {
        init();
        setLoadingIframe(_recvContent);
        initVODManager();
        log.printDbg("requestPlay - " + _recvContent.contsName);
        log.printDbg(JSON.stringify(_recvContent));
        log.printDbg(JSON.stringify(_recvContent.getLinkTime));
        if (!hasListener) {
            indexListener = null;
            log.printDbg("noSeriesIndexListener..");
        }
        // 현재 상태가 VOD 상태가 아닐 경우 ServiceState 먼저 변경 후 VOD 요청 시작
        if (StateManager.serviceState != CONSTANT.SERVICE_STATE.VOD && StateManager.serviceState != CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
            AppServiceManager.changeService({nextServiceState: CONSTANT.SERVICE_STATE.VOD});
        }
        StateManager.addServiceStateListener(_serviceStateEventListener);
        currentVOD = _recvContent;
        // 성급 VOD 판단
        isQuickContents = (currentVOD.quickVodYn == "Y" && currentVOD.quickSvcCd == 2);
        _activatePlayer(_notDestroy);
        currentSpeedIndex = 5;
        currentReqPathCd = _reqPathCd ? _reqPathCd : "01";
        _requestAuthorize();
    }

    function _requestSMLSBuyInfo() {
        if (StorageManager.ps.load(StorageManager.KEY.OTN_PARING) == "Y") {
            //OTN Paring중일 때 구매공유 여부 전달
            if (currentVOD.pkgYn == "Y") {
                payment.SMLSManager.recvBuyItemInfo(function (result) {
                }, DEF.SAID, currentVOD.cCSeridsId || "", currentVOD.buyingDate, data.expireDate, 1, buyObj.price, getSMLSBuyType(), buyObj.ltFlag ? "Y" : "N", "N", "Y", "", buyObj.resolCd);
            }
            else if (currentVOD.pkgYn == "N") {
                payment.SMLSManager.recvBuyItemInfo(function (result) {
                }, DEF.SAID, buyObj.assetId, buyObj.buyingDate, data.expireDate, 2, buyObj.price, getSMLSBuyType(), buyObj.ltFlag ? "Y" : "N", "N", "Y", buyObj.cCSeridsId || "", buyObj.resolCd);
            }
        }
    }

    function _requestPlayList(playListData) {
        log.printDbg("requestPlayList");
        // [kh.kim] 국방상품에서는 연속/몰아보기 기능 미제공
        if (KTW.managers.service.ProductInfoManager.isMilitary()) {
            playListData.sortGb = null;
        }
        // 2017.11.30 Lazuli
        // 키즈플레이리스트에서 재생 요청한 경우 컨텐츠의 장르값과 관계 없이 무조건 키즈 로딩아이프레임을 노출함
        isForcedKidsIframe = playListData.isKids;
        if (!playListData.alreadySetPlayList) VODInfoManager.setPlayList(function (result) {
            if (result) {
                setLoadingIframe(playListData.currentContent);
                isContinuePlay = true;
                _requestPlay(playListData.currentContent, playListData.reqPathCd, false, playListData.indexListener);
            }
        }, playListData.playList, playListData.currentIndex, playListData.loop, playListData.sortGb);
        else {
            // VODImpl에서 이미 setPlayList를 수행한 경우 바로 requestPlay 호출 함
            setLoadingIframe(playListData.currentContent);
            isContinuePlay = true;
            _requestPlay(playListData.currentContent, playListData.reqPathCd, false, playListData.indexListener);
        }
    }

    function _nextPlayListPlay() {
        log.printDbg("nextPlayListPlay");
        isCorner = false;
        isContinuePlay = true;
        prevVOD = JSON.parse(JSON.stringify(currentVOD));
        VODTimer.clearTimer();
        positionInterval = null;
        try {
            VODInfoManager.nextPlayList(function (_content) {
                // 2017.11.30 Lazuli
                // 장르 값도 갖고 있지 않으며, 강제 키즈 아이프레임 모드가 아니라면 디폴트 로딩 아이프레임을 노출 한다.
                var setDefaultIframe = (!_content.dispContsClass && !isForcedKidsIframe && !KidsModeManager.isKidsMode());
                setLoadingIframe(_content, setDefaultIframe);
                var catData;
                if (_content.catData) catData = _content.catData;
                else if (_content.catId === currentVOD.catId && currentVOD.originData) catData = currentVOD.originData.catData;
                closeAllVODPopup();
                prePlayProcess(_content, _content.catId, currentReqPathCd, function (res, data) {
                    if (res) _requestPlay(data, currentReqPathCd, true);
                    else _exitVOD();
                    // 2017.10.12 Lazuli
                    // 플레이리스트 재생 중에는 이어보기 팝업을 노출하지 않음
                    // isAssetPlay는 특정 assetId(화질)를 재생하기 위해 전달하는 값
                    // playResolCd는 특정 assetId를 모르는 경우, 특정 화질을 재생하기 위해 전달 하는 값
                }, {
                    noContinuePopup: true,
                    catData: catData,
                    playResolCd: _content.playResolCd ? _content.playResolCd : null,
                    forced: true,
                    isAssetPlay: true
                });
            });
        } catch (e) {
            log.printErr(e);
            _exitVOD();
        }
    }

    function _requestPlayListWithIdx(_idx, isFromUserSelected, notUpdateVodEndTime) {
        if (!VODInfoManager.getPlayList()) return;
        isCorner = false;
        isContinuePlay = true;
        prevVOD = JSON.parse(JSON.stringify(currentVOD));
        VODTimer.clearTimer();
        positionInterval = null;
        try {
            if (currentVOD) {
                VODInfoManager.getPlayListWithIdx(_idx, function (_content) {
                    // 2017.10.12 Lazuli
                    // 유저가 선택한 상황이 아닌 다음 리스트 재생 등으로 재생 시에는 로딩 아이프레임 표시
                    if (!isFromUserSelected) {
                        // 2017.11.30 Lazuli
                        // 장르 값도 갖고 있지 않으며, 강제 키즈 아이프레임 모드가 아니라면 디폴트 로딩 아이프레임을 노출 한다.
                        var setDefaultIframe = (!_content.dispContsClass && !isForcedKidsIframe && !KidsModeManager.isKidsMode());
                        setLoadingIframe(_content, setDefaultIframe);
                    }
                    if (_content) {
                        closeAllVODPopup();
                        prePlayProcess(_content, _content.catId, currentReqPathCd, function (res, data) {
                            if (res) {
                                setLoadingIframe(data);
                                if (!notUpdateVodEndTime) _setVodEndTime();
                                // 2017.10.12 Lazuli
                                // 재생이 가능한 상황이면 현재 Index를 요청들어온 Index 값으로 변경함
                                VODInfoManager.setCurrentPlayListIdx(_idx);
                                _requestPlay(data, currentReqPathCd, true, indexListener);
                            }
                            // 2017.10.12 Lazuli
                            // 유저가 선택한 상황이 아닌 다음 리스트 재생 등으로 재생 시, 재생 실패하면 VOD 종료 함
                            // 그렇지 않은 경우는 아무런 행동을 취하지 않음
                            else if (!isFromUserSelected) {
                                if (!notUpdateVodEndTime) _setVodEndTime();
                                _exitVOD();
                            }
                        }, {
                            playResolCd: _content.playResolCd ? _content.playResolCd : null,
                            forced: true,
                            isAssetPlay: true
                        });
                    }
                    else {
                        log.printErr("no have content");
                        if (!notUpdateVodEndTime) _setVodEndTime();
                        _exitVOD();
                    }
                });
            }
        } catch (e) {
            log.printErr(e);
            _setVodEndTime();
            _exitVOD();
        }
    }

    function _requestPlaySeries(_currentSeries, _recvSeries, _reqPathCd, idx, sortGb, _indexListener) {
        isCorner = false;
        isContinuePlay = true;
        isSeriesPlay = true;
        isAutoContinue = false;
        VODTimer.clearTimer();
        // [kh.kim] 국방상품인 경우 연속/몰아보기 기능 미제공
        if (KTW.managers.service.ProductInfoManager.isMilitary()) {
            sortGb = null;
        }

        if (!sortGb) _recvSeries = [_currentSeries];
        else isAutoContinue = (sortGb == "a" || sortGb == "d");
        indexListener = _indexListener;
        _requestPlayList({
            currentContent: _currentSeries,
            reqPathCd: _reqPathCd,
            playList: _recvSeries,
            currentIndex: idx,
            indexListener: true,
            sortGb: sortGb
        });
    }

    // 재생 요청 받은 코너를 재생함 (재생 실패 시 현 재생상태 유지)
    function _requestPlayCorner(index, callback) {
        var curCorner = VODInfoManager.getCornerVod(index);
        log.printDbg(JSON.stringify(curCorner || {}));
        if (curCorner) {
            isCorner = true;
            currentReqPathCd = "44";
            if (currentVOD.assetId == curCorner.content_id) {
                currentVOD.currentCorner = curCorner;
                VODInfoManager.setCurrentCornerIndex(index);
                if (callback) callback();
                _seek(HTool.parseVODTime(curCorner.cornerstart));
                VODTimer.createTimeout(_getPlayTimeForCorner());
            }
            else {
                var contsInfo = VODInfoManager.getPlayListWithContsId(curCorner.content_id);
                closeAllVODPopup();
                prePlayProcess(contsInfo ? contsInfo : {contsId: curCorner.content_id}, contsInfo ? currentVOD.catId : "N", currentReqPathCd, function (res, data) {
                    if (res) {
                        navInterfaceStopVOD();
                        positionInterval = null;
                        data.currentCorner = curCorner;
                        VODInfoManager.setCurrentCornerIndex(index);
                        if (callback) callback();
                        data.getLinkTime.linkTime = parseInt(HTool.parseVODTime(curCorner.cornerstart) / 1000);
                        log.printDbg("PlayCorner ::: " + VODInfoManager.getCurrentCornerVod().cornername);
                        _requestPlay(data, currentReqPathCd, true, true);
                    }
                }, {noContinuePopup: true, autoPlay: true, catData: contsInfo ? currentVOD.originData.catData : null});
            }
        }
        else log.printErr("playCorner Error. wrong CornerIndex");
    }

    // 코너 재생 중 다음 코너를 재생함 (재생 실패 시 VOD 종료)
    function _nextCornerPlay() {
        var nextCornerIndex = VODInfoManager.getNextCornerIndex();
        var curCorner = VODInfoManager.getCornerVod(nextCornerIndex);
        log.printDbg(JSON.stringify(curCorner || {}));
        if (curCorner) {
            isCorner = true;
            currentReqPathCd = "44";
            if (currentVOD.assetId == curCorner.content_id) {
                currentVOD.currentCorner = curCorner;
                VODInfoManager.setCurrentCornerIndex(nextCornerIndex);
                _seek(HTool.parseVODTime(curCorner.cornerstart));
                VODTimer.createTimeout(_getPlayTimeForCorner());
            }
            else {
                init();
                navInterfaceStopVOD();
                setLoadingIframe(currentVOD);
                var contsInfo = VODInfoManager.getPlayListWithContsId(curCorner.content_id);
                closeAllVODPopup();
                prePlayProcess(contsInfo ? contsInfo : {contsId: curCorner.content_id}, contsInfo ? currentVOD.catId : "N", currentReqPathCd, function (res, data) {
                    if (res) {
                        data.currentCorner = curCorner;
                        VODInfoManager.setCurrentCornerIndex(nextCornerIndex);
                        data.getLinkTime.linkTime = parseInt(HTool.parseVODTime(curCorner.cornerstart) / 1000);
                        log.printDbg("PlayCorner ::: " + VODInfoManager.getCurrentCornerVod().cornername);
                        _requestPlay(data, currentReqPathCd, true, true);
                    }
                    else {
                        _setVodEndTime();
                        _exitVOD();
                    }
                }, {noContinuePopup: true, autoPlay: true, catData: contsInfo ? currentVOD.originData.catData : null});
            }
        }
        else {
            isCorner = false;
            VODTimer.clearTimer();
            _setVodEndTime();
            _exitVOD();
        }
    }

    function _nextSeriesPlay(tmpVod) {
        isCorner = false;
        isContinuePlay = true;
        prevVOD = JSON.parse(JSON.stringify(currentVOD));
        VODTimer.clearTimer();
        positionInterval = null;
        vodAdapter.stop();
        try {
            closeAllVODPopup();
            prePlayProcess(tmpVod, currentVOD.catId, currentReqPathCd, function (res, data) {
                if (res) _requestPlay(data, currentReqPathCd, true, true);
                else _exitVOD();
            }, {
                // 2017.10.12 Lazuli
                // 자동몰아보기가 설정되어있거나, 몰아보기 컨텐츠일 경우 이어보기 팝업을 노출하지 않음
                noContinuePopup: ((StorageManager.ps.load(StorageManager.KEY.SERIES_AUTO_PLAY) && StorageManager.ps.load(StorageManager.KEY.SERIES_AUTO_PLAY) == "1") || isAutoContinue), // autoPlay가 true일 경우 옵션선택 팝업을 노출하지 않고 무조건 최고화질로 재생한다. (동일 화질 일때 자막/더빙 유무는 구분하지 않고 앞에 편성된 컨텐츠로 재생함)
                autoPlay: true, catData: (currentVOD.originData) ? currentVOD.originData.catData : null
            });
        } catch (e) {
            log.printErr(e);
            _exitVOD();
        }
        tmpVod = null;
    }

    function _nextCorner() {
        VODTimer.clearTimer();
        if (LayerManager.getLayer("VodSelectEpisodePopup")) LayerManager.deactivateLayer({
            id: "VodSelectEpisodePopup", remove: true
        });
        if (LayerManager.getLayer("VodSelectCornerPopup")) LayerManager.deactivateLayer({
            id: "VodSelectCornerPopup", remove: true
        });
        MiniGuideManager.hideAll();
        log.printDbg("현재 코너 종료 ::: " + VODInfoManager.getCurrentCornerVod().cornername);
        log.printDbg("다음 코너 재생 !!! ");
        _nextCornerPlay();
    }

    function _requestAdditionalContents(_catId, _contsId, _contsName, _reqCd, isBuga, _additionalInfo) {
        var tmpVodInfo = {
            catId: _catId,
            assetId: _contsId,
            contsId: _contsId,
            contsName: _contsName,
            getLinkTime: {linkTime: 0},
            isAdditional: true
        };

        if (isBuga && _additionalInfo.checkLinkTime) {
            if (_additionalInfo.buyYn === true) {
                var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                VODAmocManager.getLinkTimeNxt(function (res, data) {
                    if (res) {
                        tmpVodInfo.authType = "P";
                        tmpVodInfo.getLinkTime = data;
                        tmpVodInfo.getLinkTime.linkTime = 0;
                        _requestPlay(tmpVodInfo, _reqCd);
                    }
                    else {
                        _openErrorPopup(ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), true);
                    }
                }, DEF.SAID, tmpVodInfo.assetId, custEnv ? custEnv.buyTypeYn : "Y", true);
            }
            else {
                showToast("부가영상은 본 콘텐츠 구매 시 시청 가능합니다");
            }
        }
        else {
            tmpVodInfo.authType = "F";
            _requestPlay(tmpVodInfo, _reqCd);
        }
    }

    function _requestPlayHighLight(_highLightInfo) {
        isHighLight = true;
        _requestPlay(_highLightInfo, 56);
    }

    // 미리보기 or 하이라이트 보기 종료 시 호출되는 함수
    function _onHighLightEnd() {
        _prePlayProcessImplForPreview(true);
    }

    // 미리보기 or 하이라이트 모드를 종료 시킴 (결제완료)
    function _finishHighLight(paymentData) {
        currentPlayLayer.hideRedKeyPromo();
        VODTimer.clearTimer();
        // 결제한 VOD와 재생중이던 미리보기가 같은 경우 그대로 재생함
        log.printDbg(JSON.stringify(paymentData));
        currentReqPathCd = "57";
        // if (currentVOD.assetId === paymentData.assetId) {
        // 현재 위치를 linkTime 값에 저장함
        var endTime;
        var adapterTime = vodAdapter ? vodAdapter.getPlayPosition() : 0;
        if (adapterTime > 0) endTime = parseInt(Math.floor(adapterTime / 1E3));
        else endTime = parseInt(cacheCurrentPosition || 0);

        // paymentData가 없다면 월정액으로 간주함 : 테스트 필요
        currentVOD.getLinkTime = paymentData != null ? paymentData.getLinkTime : {};
        currentVOD.getLinkTime.linkTime = endTime;
        currentVOD.authType = paymentData != null ? paymentData.authType : "S";

        _requestAuthorize(function () {
            isHighLight = false;

            function playContinue(fromBegin) {
                isLoading = false;
                if (fromBegin) _seek(0);
                _forceResume();
                // 썸네일 정보 재요청
                VODInfoManager.getThumbnail(function (_thumbData) {
                    if (_thumbData && parseInt(_thumbData.result) == 200) {
                        currentVOD.thumbnailList = _thumbData.items;
                        currentPlayLayer.onThumbLoadComplete(currentVOD.thumbnailList);
                    }
                }, currentVOD.assetId);

                if (currentVOD.extraParam && currentVOD.extraParam.isSeries) {
                    isCorner = false;
                    isContinuePlay = true;
                    isSeriesPlay = true;
                    isAutoContinue = false;
                    indexListener = currentVOD.extraParam.indexListener;
                    if (indexListener != null) indexListener(currentVOD.catId, currentVOD.originData.data.contsId, currentVOD.originData.data);
                    VODInfoManager.setPlayList(function (result) {
                    }, currentVOD.extraParam.seriesData, currentVOD.extraParam.index, false, currentVOD.extraParam.sortGb)
                }
            }

            // 하이라이트 보기인 경우 이어보기 팝업을 노출하도록 함
            if (currentVOD.previewStartTime > 0) {
                LayerManager.activateLayer({
                    obj: {
                        id: "VodContinuePopup", type: Layer.TYPE.POPUP, priority: Layer.PRIORITY.POPUP, params: {
                            assetId: currentVOD.assetId,
                            contsName: currentVOD.contsName,
                            runtime: currentVOD.originData.data.runtime,
                            prInfo: currentVOD.originData.data.prInfo,
                            resolCd: currentVOD.resolCd,
                            bookmarkTime: null,
                            linkTime: endTime,
                            otmLinkTime: null,
                            callback: function (res) {
                                if (res == 0) playContinue(true);
                                else if (res > 0) playContinue();
                                else if (res > -1) _exitVOD();
                            }
                        }
                    }, new: true, moduleId: "module.vod", visible: true
                });
            }
            else playContinue();
        });
        // } else {
        //     isHighLight = false;
        //     _requestPlay(paymentData, currentReqPathCd, true)
        // }
    }

    // 미리보기 중 레드키 눌렀을 때 호출되는 함수
    function _prePlayProcessImplForPreview(_isOnEnd) {
        if (isHighLight && !isLoading) {
            // 결제프로세스 진행되는 동안 키 입력을 막기 위해 임의로 isLoading 값을 true로 변경함
            isLoading = true;
            log.printDbg("requestShow === VODManager(HighLigh Play Mode)");
            requestShow();
            // 결제팝업과 일시정지 아이콘이 겹치지 않기 위해, 일시정지 아이콘을 노출하지 않고 일시정지함
            _forcePause(true);
            var tmpData;
            for (var i = 0; i < currentVOD.optionData.optionList.length; i++) {
                // 미리보기 시 선택한 화질로 구매 프로세스 진행 가능하도록 셋팅해줌
                if (currentVOD.optionData.optionList[i].assetId === currentVOD.assetId) tmpData = currentVOD.optionData.optionList[i];
            }
            currentVOD.optionData.optionList = [tmpData];
            // 메뉴나 팝업이 호출된 상태에서 미리보기 재생 완료 시 모든 화면 닫고 구매 프로세스 진행
            if (_isOnEnd) LayerManager.hideNormalLayer();
            prePlayProcessByButtonObj(currentVOD.optionData, currentVOD.catId, 57, function (result, data) {
                if (result) {
                    // 결제가 성공적으로 종료된 경우 미리보기 모드를 종료하고 이어 재생함
                    _finishHighLight(data);
                }
                else {
                    // 결제 취소했고 미리보기 시간이 남지 않았을 경우 VOD 종료
                    if (_isOnEnd) _exitVOD();
                    else {
                        // 결제 취소 했으나 미리보기 시간이 남았을 경우 계속 재생

                        _forceResume();
                    }
                    isLoading = false;
                }
                log.printDbg("notifyHide === VODManager(HighLigh Play Mode)");
                notifyHide();
                // 이미 미리보기 선택 전에 성인인증을 진행 했으므로 authComplete 값을 true로 셋팅하여 preplay 호출함
            }, false, false, true);
        }
    }

    /*
     ### VOD END
     */

    function _setVodEndTime(positionEnd) {
        // 2017.08.07 Lazuli
        // 부가영상/예고편은 이어보기 시간 저장을 하지 않음
        if (!currentVOD || currentVOD.isAdditional || isHighLight) return 0;
        if (currentVOD.alreadySetVodEndTime) return 0;
        currentVOD.alreadySetVodEndTime = true;

        var tmpCurVod = currentVOD;
        var endTime;
        if (positionEnd) endTime = 0;
        else if (isPostPlay) endTime = 0;
        else {
            var adapterTime = vodAdapter ? vodAdapter.getPlayPosition() : 0;
            if (adapterTime > 0) endTime = parseInt(Math.floor(adapterTime / 1E3));
            else endTime = parseInt(cacheCurrentPosition || 0);
        }

        // 2017.08.08 Lazuli
        // VOD 종료 시 setPassedTime API가 Timeout이 잦게 발생하는 현상이 있어
        // API Request 시점을 뒤로 밀어서 해결함
        setTimeout(function () {
            try {
                VODAmocManager.setVodEnd(function (result, data) {
                }, DEF.SAID, tmpCurVod.assetId, endTime);
                SMLSManager.setOTVPassedTime(function () {
                }, DEF.SAID, tmpCurVod.assetId, endTime);
                // 최근 시청 캐시
                if (ModuleManager.getModule("module.family_home")) {
                    ModuleManager.getModule("module.family_home").execute({
                        method: "addToRecentlyList", data: tmpCurVod.recentlyData, linkTime: endTime, onlyUpdate: true
                    });
                }
                log.printDbg("setVodEndTime ::: " + endTime);
            } catch (e) {
                log.printErr(e);
            } finally {
                tmpCurVod = null;
                endTime = null;
            }
        }, 100);
        return endTime;
    }

    function _finish(positionEnd) {
        if (vodAdapter.getPlayState() < 0) return;
        clearPauseTimeout();
        if (positionEnd) {
            navInterfaceStopVOD();
            log.printDbg("VOD END");
            positionInterval = null;
            cacheCurrentPosition = 0;
            _setVodEndTime(positionEnd);
            if (LayerManager.getLayer("VodEndPopup")) LayerManager.deactivateLayer({
                id: "VodEndPopup", remove: false
            });
            if (currentPlayLayer) currentPlayLayer.onVODEnd();
            if (isKidsLastVod) {
                kidsModeCall("kidsSTBOff");
                log.printDbg("KidsMode - LastVOD End");
            }
            else {
                if (isContinuePlay) {
                    if (isSeriesPlay) {
                        if (VODInfoManager.nextSeries()) {
                            // 시리즈 이어보기 상태
                            log.printDbg("VOD NEXT SERIES");
                            var nextVod = VODInfoManager.getCurrentSeries();
                            setLoadingIframe(nextVod);
                            // 시리즈이지만 몰아보기 상태
                            if ((StorageManager.ps.load(StorageManager.KEY.SERIES_AUTO_PLAY) && StorageManager.ps.load(StorageManager.KEY.SERIES_AUTO_PLAY) == "1") || isAutoContinue) _nextSeriesPlay(nextVod);
                            else {
                                // 2017.09.14 Lazuli
                                // 시리즈 이어보기 상태일 때에는 동시채널 팝업 유지되지 않음
                                // 자동 몰아보기 상태 또는 플레이리스트 몰아보기 상태일 경우에만 유지
                                if (pipManager.isPipLayerShow()) pipManager.deactivateMultiView();
                                LayerManager.hideNormalLayer();
                                _openEndPopup(1, function () {
                                    _nextSeriesPlay(nextVod)
                                }, _exitVOD, null, nextVod);
                            }
                        }
                        else _exitVOD();
                    }
                    else if (VODInfoManager.getPlayList() && VODInfoManager.getPlayList().length > 0) {
                        // 플레이리스트로 이어보기 상태
                        if (VODInfoManager.getPlayList().length - 1 <= VODInfoManager.getCurrentPlayListIdx()) {
                            log.printDbg("VOD NEXT PLAYLIST ::: isLoop === " + VODInfoManager.isPlayListLoop());
                            // 2017.10.12 Lazuli
                            // 이미 위에서 setVodEndTime을 호출했으므로, _requestPlayListWithIdx에서는 setVodEndTime을 호출하지 않도록 notUpdateVodEndTime 값을 true로 설정함
                            // VOD가 종료되고 다음 리스트를 재생하기 위한 호출이므로 isFromUserSelected 값을 false로 설정함
                            if (VODInfoManager.isPlayListLoop()) _requestPlayListWithIdx(0, false, true);
                            else _exitVOD();
                        }
                        else _nextPlayListPlay();
                    }
                    else _exitVOD();
                }
                else {
                    // 맞춤프로모 적용
                    var chObj = null;
                    if (!KidsModeManager.isKidsMode() && currentVOD.getLinkTime && currentVOD.getLinkTime.promoLocator) {
                        chObj = oipfAdapter.navAdapter.getHiddenPromoChannelByTriplet(currentVOD.getLinkTime.promoLocator);
                        _exitVOD(chObj, null, null, (chObj != null));
                    }
                    else _exitVOD(chObj);
                }
            }
        }
        else {
            _openEndPopup(0, function () {
                _setVodEndTime();
                _exitVOD();
            });
        }
    }

    function _openEndPopup(mode, okAction, cancelAction, channelTune, _nextVOD, noRecommend) {
        if (LayerManager.getLayer("VodEndPopup")) LayerManager.deactivateLayer({
            id: "VodEndPopup", remove: false
        });
        if (noRecommend) {
            okAction = isKidsLastVod ? function () {
                _setVodEndTime();
                kidsModeCall("kidsSTBOff");
                log.printDbg("KidsMode - LastVOD End");
            } : okAction;
            LayerManager.activateLayer({
                obj: {
                    id: "VodEndPopup", type: Layer.TYPE.POPUP, priority: Layer.PRIORITY.POPUP, linkage: true, params: {
                        recommendData: [],
                        contsName: mode == 0 ? currentVOD.contsName : _nextVOD.contsName,
                        contsId: currentVOD.assetId,
                        mode: mode,
                        okAction: okAction,
                        cancelAction: cancelAction,
                        channelTune: channelTune,
                        isAuthorizedContent: currentVOD.isAuthorizedContent
                    }
                }, moduleId: "module.vod", visible: true
            });
            return;
        }
        log.printDbg("recommendYn is " + (currentVOD.originData ? currentVOD.originData.data.recommendYn : null));
        if (!currentVOD.originData || currentVOD.originData.data.recommendYn != "P") {
            endPopupInfo.recommendData = [];
            endPopupInfo.contsId = currentVOD.assetId;
            endPopupInfo.catId = currentVOD.catId;
        }
        if (endPopupInfo.recommendData != null) {
            if (endPopupInfo.contsId != currentVOD.assetId || endPopupInfo.catId != currentVOD.catId) {
                endPopupInfo.recommendData = null;
                _openEndPopup(mode, okAction, cancelAction, channelTune, _nextVOD);
            }
            else {
                okAction = isKidsLastVod ? function () {
                    _setVodEndTime();
                    kidsModeCall("kidsSTBOff");
                    log.printDbg("KidsMode - LastVOD End");
                } : okAction;
                LayerManager.activateLayer({
                    obj: {
                        id: "VodEndPopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            recommendData: endPopupInfo.recommendData,
                            contsName: mode == 0 ? currentVOD.contsName : _nextVOD.contsName,
                            contsId: currentVOD.assetId,
                            mode: mode,
                            okAction: okAction,
                            cancelAction: cancelAction,
                            channelTune: channelTune,
                            isAuthorizedContent: currentVOD.isAuthorizedContent
                        }
                    }, moduleId: "module.vod", visible: true
                });
            }
        }
        else {
            HTool.updateCustEnv(function () {
                var loadPackageList = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                var productList = (loadPackageList && loadPackageList != null && loadPackageList.length > 0) ? StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST) + "," + loadPackageList : StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
                VodRecmManager.getRelatedContents("ITEM_VOD2", "P", DEF.SAID, productList, currentVOD.assetId, currentVOD.catId, currentVOD.adultOnlyYn, 30, function (result, response) {
                    if (response.RESULT_CODE == 200) {
                        endPopupInfo.recommendData = response.RC_LIST;
                    }
                    else endPopupInfo.recommendData = [];
                    endPopupInfo.contsId = currentVOD.assetId;
                    endPopupInfo.catId = currentVOD.catId;
                    _openEndPopup(mode, okAction, cancelAction, channelTune, _nextVOD);
                });
            });
        }
    }

    function navInterfaceStopVOD() {
        if (navInterfaceState == 0) {
            VodNavInterface.stopVOD();
            navInterfaceState = 1;
        }
    }

    function _exitVOD(channelObj, _notChangeService, _blockRestoreLayer, isPromoCh, requestFromOther, notDestroyPlayList) {
        /**
         * [dj.son] [WEBIIIHOME-3675] 키즈 종료 사운드 재생시, exit 로직을 타도록 수정
         */
        if (!isKidsEndSound && currentVOD == null && currentPlayLayer == null) {
            log.printDbg("Already EXIT PlayLayer !!!");
            return;
        }

        log.printDbg("EXIT PlayLayer !!!");
        if (isHighLight) currentPlayLayer.hideRedKeyPromo();
        if (currentPlayLayer) currentPlayLayer.onVODEnd();
        // 2017.08.08 Lazuli
        // requestFromOther 호출되는 경우
        // requestPlay에서 어차피 재실행 해주는 부분이기 때문에 아래 로직을 실행하지 않음
        if (!requestFromOther) {
            init();
            if (LayerManager.getLayer("VodPlayerLayer")) LayerManager.deactivateLayer({
                id: "VodPlayerLayer", remove: false
            });
            hideLoadingIframe();
        }
        clearPauseTimeout();
        clearPrepareTimer();
        if (smiInstance) {
            smiInstance.stop();
            smiInstance.destroy();
        }
        // 2017.09.14 Lazuli
        // VOD가 완전히 종료될 때에는 채널 동시시청 팝업 닫음
        // 이어보기/몰아보기/플레이리스트 상태에서는 _exitVOD 함수 실행되지 않음
        if (pipManager.isPipLayerShow()) pipManager.deactivateMultiView();
        VODInfoManager.destroy(notDestroyPlayList);
        navInterfaceStopVOD();
        // 종료 될때 서비스 상태 체크
        var serviceState = StateManager.getServiceStateString(StateManager.serviceState);
        // 시청시간제한이나 대기모드 상태가 아닐 경우 TV_VIEWING 상태로 변경
        // 키즈모드의 마지막 편일 경우 STB이 종료되야 하기 때문에 TV_VIEWING 상태로 변경하지 않음
        if (!(serviceState == "TIME_RESTRICTED" || serviceState == "STANDBY" || isKidsLastVod) && !_notChangeService) {
            // 맞춤프로모 적용
            if (!channelObj && !KidsModeManager.isKidsMode() && currentVOD.getLinkTime && currentVOD.getLinkTime.promoLocator) {
                channelObj = oipfAdapter.navAdapter.getHiddenPromoChannelByTriplet(currentVOD.getLinkTime.promoLocator);
                isPromoCh = (channelObj != null);
            }
            changeServiceByVODModule = true;
            _showLastDetailLayer(true, function () {
                AppServiceManager.changeService({
                    nextServiceState: CONSTANT.SERVICE_STATE.TV_VIEWING, restoreLayer: !_blockRestoreLayer, obj: {
                        channel: channelObj ? channelObj : null, tune: "true", isSelect: !!isPromoCh
                    }
                });
            });
        }

        if (!requestFromOther) {
            log.printDbg("[VODManager] removeEventListener and VODManager destroy");
            StateManager.removeServiceStateListener(_serviceStateEventListener);
            vodAdapter.removePlaySpeedChangeListener(_onPlaySpeedChanged);
            vodAdapter.removePlayStateChangeListener(_onPlayStateChangeListener);
            vodAdapter.removePlayPositionChangeListener(_onPlayPositionChanged);
            vodAdapter.removeReadyToPlayListener(_onReadyToPlay);
            vodAdapter.removeMediaContentChangeListener(_onMediaContentChanged);
            vodAdapter.removePlaybackEndInProgressListener(_onPlaybackEndInProgressListener);
            basicAdapter.removeResizeScreenListener(_resizeScreenListener);
        }

        currentVOD = null;
        currentPlayLayer = null;
        currentState = null;
        currentSpeedIndex = null;
        smiInstance = null;
        isCorner = null;
        isHighLight = null;
        isAD = null;
        isQuickContents = null;
        isKidsEndSound = null;
        isPromoCh = null;
        positionInterval = null;
        cacheCurrentPosition = null;
        isContinuePlay = null;
        isSeriesPlay = null;
    }

    /*
     ### Controller
     */
    function _getPlayTime() {
        if (!isLoading) return vodAdapter.getPlayTime();
        return 0;
    }

    function _getCurrentPosition() {
        if (!isLoading) return vodAdapter.getPlayPosition();
        return 0;
    }

    function _getPlayTimeForCorner() {
        if (isCorner) return HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend) - HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerstart);
        return 0;
    }

    function _getCurrentPositionForCorner() {
        if (isCorner) return parseInt(vodAdapter.getPlayPosition() - HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerstart));
        return 0;
    }

    function _pause(code, noIndicator) {
        if (!vodAdapter || vodAdapter.getPlayState() > 2 || isLoading) return;
        if (code && code == "pause") return _forcePause();
        if (code && code == "resume") return _forceResume();
        if (currentState == VODManager.PLAY_STATE.PLAY && currentSpeedIndex == 5) {
            currentState = VODManager.PLAY_STATE.PAUSE;
            showIndicator(currentState, null, function () {
                MiniGuideManager.onChangedPlayState(currentState, null, noIndicator);
            });
            if (MiniGuideManager.isShowing()) MiniGuideManager.onChangedPlayState(currentState, null, noIndicator);
            vodAdapter.pause();
            setPauseTimeout();
        }
        else if ((currentState == VODManager.PLAY_STATE.PLAY && currentSpeedIndex != 5) || currentState != VODManager.PLAY_STATE.PLAY) {
            if (isHighLight && currentVOD.previewEndTime - vodAdapter.getPlayPosition() < 1) return;
            clearPauseTimeout();
            currentState = VODManager.PLAY_STATE.PLAY;
            showIndicator(currentState, null);
            MiniGuideManager.onChangedPlayState(currentState, null, noIndicator);
            vodAdapter.resume();
        }
    }

    function _forcePause(_noCenterFeedback) {
        currentState = VODManager.PLAY_STATE.PAUSE;
        MiniGuideManager.hideMiniIndicator();
        showIndicator(_noCenterFeedback ? -1 : currentState, null, function () {
            MiniGuideManager.onChangedPlayState(currentState, null);
        });
        if (MiniGuideManager.isShowing()) MiniGuideManager.onChangedPlayState(currentState, null);
        vodAdapter.pause();
        setPauseTimeout();
        return true;
    }

    function _forceResume() {
        if (isHighLight && currentVOD.previewEndTime - vodAdapter.getPlayPosition() < 1) return;
        clearPauseTimeout();
        currentState = VODManager.PLAY_STATE.PLAY;
        MiniGuideManager.hideMiniIndicator();
        showIndicator(currentState, null);
        vodAdapter.resume();
        return true;
    }

    function _playFast() {
        if (isLoading || isAD || vodAdapter.getPlayState() > 2) return;
        var currentPosition = vodAdapter.getPlayPosition(), playTime;
        if (isCorner) playTime = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend);
        else if (isHighLight) playTime = currentVOD.previewEndTime;
        else playTime = vodAdapter.getPlayTime();
        if (playTime - currentPosition < 5E3) {
            if (isQuickContents) showToast("현재 방송중인 프로그램이므로 더 이상 배속할 수 없습니다");
            return;
        }
        if (currentState == VODManager.PLAY_STATE.FASTPLAY || currentState == VODManager.PLAY_STATE.SLOWPLAY || currentSpeedIndex == 6) {
            currentState = VODManager.PLAY_STATE.PLAY;
            currentSpeedIndex = 5;
            MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.PLAY, null);
            showIndicator(currentState, PLAY_SPEED[currentSpeedIndex]);
        }
        else {
            currentState = VODManager.PLAY_STATE.FASTPLAY;
            currentSpeedIndex = 6;
        }
        _changePlaySpeed(PLAY_SPEED[currentSpeedIndex]);
    }

    function _playSlow() {
        if (isLoading || isAD || vodAdapter.getPlayState() > 2) return;
        var currentPosition = vodAdapter.getPlayPosition(), playTime;
        if (isCorner) playTime = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend);
        else if (isHighLight) playTime = currentVOD.previewEndTime;
        else playTime = vodAdapter.getPlayTime();
        if (playTime - currentPosition < 5E3) {
            if (isQuickContents) showToast("현재 방송중인 프로그램이므로 더 이상 배속할 수 없습니다");
            return;
        }
        if (currentState == VODManager.PLAY_STATE.FASTPLAY || currentState == VODManager.PLAY_STATE.SLOWPLAY || currentSpeedIndex == 4) {
            currentState = VODManager.PLAY_STATE.PLAY;
            currentSpeedIndex = 5;
            MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.PLAY, null);
            showIndicator(currentState, PLAY_SPEED[currentSpeedIndex]);
        }
        else {
            currentState = VODManager.PLAY_STATE.SLOWPLAY;
            currentSpeedIndex = 4;
        }
        _changePlaySpeed(PLAY_SPEED[currentSpeedIndex]);
    }

    function _seekLeft() {
        if (isLoading || isAD || (currentSpeedIndex != 5 && currentSpeedIndex != 3)) return;
        var currentPosition = vodAdapter.getPlayPosition(), startPoint;
        log.printDbg("[VODManager] currentPosition ::: " + currentPosition);

        if (isCorner) startPoint = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerstart);
        else if (isHighLight) startPoint = currentVOD.previewStartTime;
        else startPoint = 0;

        if (currentPosition - 3E5 < startPoint) _seek(startPoint);
        else _seek(currentPosition - 3E5);
        currentPosition = null;
        showIndicator(VODManager.PLAY_STATE.SEEKBACK, null);
        MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.SEEKBACK, null);
    }

    function _seekRight() {
        if (isLoading || isAD || (currentSpeedIndex != 5 && currentSpeedIndex != 3)) return;
        var currentPosition = vodAdapter.getPlayPosition(), playTime;
        log.printDbg("[VODManager] currentPosition ::: " + currentPosition);

        if (isCorner) playTime = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend);
        else if (isHighLight) playTime = currentVOD.previewEndTime;
        else playTime = vodAdapter.getPlayTime();

        if (playTime - currentPosition < 5E3) {
            if (isQuickContents) showToast("현재 방송중인 프로그램이므로 더 이상 배속할 수 없습니다");
            return;
        }
        else if (playTime - currentPosition < 3E5) _seek(playTime - 5E3);
        else _seek(currentPosition + 3E5);
        currentPosition = null;
        playTime = null;
        showIndicator(VODManager.PLAY_STATE.SEEKFWD, null);
        MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.SEEKFWD, null);
    }

    function _seekLeftWithPosition(_position) {
        var currentPosition = vodAdapter.getPlayPosition(), startPoint;
        log.printDbg("[VODManager] currentPosition ::: " + currentPosition);

        if (isCorner) startPoint = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerstart);
        else if (isHighLight) startPoint = currentVOD.previewStartTime;
        else startPoint = 0;

        if (currentPosition + _position < startPoint) _seek(startPoint);
        else _seek(currentPosition + _position);
        currentPosition = null;
        showIndicator(VODManager.PLAY_STATE.SEEKBACK, _position * -1);
        MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.SEEKBACK, _position * -1);
    }

    function _seekRightWithPosition(_position) {
        var currentPosition = vodAdapter.getPlayPosition(), playTime;
        log.printDbg("[VODManager] currentPosition ::: " + currentPosition);

        if (isCorner) playTime = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend);
        else if (isHighLight) playTime = currentVOD.previewEndTime;
        else playTime = vodAdapter.getPlayTime();

        if (playTime - currentPosition < 5E3) {
            if (isQuickContents) showToast("현재 방송중인 프로그램이므로 더 이상 배속할 수 없습니다");
            return;
        }
        else if (playTime - currentPosition < _position) _seek(playTime - 5E3);
        else _seek(currentPosition + _position);
        currentPosition = null;
        playTime = null;
        showIndicator(VODManager.PLAY_STATE.SEEKFWD, _position);
        MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.SEEKFWD, _position);
    }

    function _seekJumpTo(time) {
        if (isLoading || isAD) return;
        isCorner = false;
        VODTimer.clearTimer();
        _seek(HTool.parseVODTime(time));
    }

    function _seek(position) {
        if (isLoading || isAD) return;

        var playTime, startPoint;
        if (isCorner) {
            playTime = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend);
            startPoint = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerstart);
        }
        else if (isHighLight) {
            playTime = currentVOD.previewEndTime;
            startPoint = currentVOD.previewStartTime;
        }
        else {
            playTime = vodAdapter.getPlayTime();
            startPoint = 0;
        }

        if (position < startPoint) position = startPoint;
        else if (position > playTime) position = playTime - 5E3;
        vodAdapter.seek(position);
        if (vodAdapter.getPlaySpeed().toFixed(1) != 1) vodAdapter.play(1);
        log.printDbg("[VODManager] currentPosition ::: " + vodAdapter.getPlayPosition());
        if (isCorner) VODTimer.createTimeout(HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend) - vodAdapter.getPlayPosition());
        if (isHighLight) VODTimer.createTimeout(currentVOD.previewEndTime - vodAdapter.getPlayPosition(), true);
        currentState = VODManager.PLAY_STATE.PLAY;
        MiniGuideManager.setTime();
        MiniGuideManager.onChangedPlayState(currentState, null, true);
    }

    function _rew() {
        if (isLoading || isAD) return;
        if (currentState != VODManager.PLAY_STATE.REWIND) currentSpeedIndex = 3;
        currentState = VODManager.PLAY_STATE.REWIND;
        if (currentSpeedIndex == 5) currentSpeedIndex = 2;
        else currentSpeedIndex--;
        if (currentSpeedIndex < 0) {
            currentSpeedIndex = 5;
            currentState = VODManager.PLAY_STATE.PLAY;
            MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.PLAY, null);
            showIndicator(currentState, PLAY_SPEED[currentSpeedIndex]);
        }
        _changePlaySpeed(PLAY_SPEED[currentSpeedIndex]);
    }

    function _fwd() {
        if (isLoading || isAD) return;
        var currentPosition = vodAdapter.getPlayPosition(), playTime;
        if (isCorner) playTime = HTool.parseVODTime(VODInfoManager.getCurrentCornerVod().cornerend);
        else if (isHighLight) playTime = currentVOD.previewEndTime;
        else playTime = vodAdapter.getPlayTime();
        if (playTime - currentPosition < 5E3) {
            if (isQuickContents) showToast("현재 방송중인 프로그램이므로 더 이상 배속할 수 없습니다");
            return;
        }
        if (currentState != VODManager.PLAY_STATE.FASTFOWARD && currentState != VODManager.PLAY_STATE.FASTPLAY) currentSpeedIndex = 5;
        currentState = VODManager.PLAY_STATE.FASTFOWARD;
        currentSpeedIndex++;
        if (currentSpeedIndex > 9) {
            currentSpeedIndex = 5;
            currentState = VODManager.PLAY_STATE.PLAY;
            MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.PLAY, null);
            showIndicator(currentState, PLAY_SPEED[currentSpeedIndex]);
        }
        _changePlaySpeed(PLAY_SPEED[currentSpeedIndex]);
    }

    function _changePlaySpeed(speed) {
        if (isLoading || isAD) return;
        log.printDbg("[VODManager] changeSpeed ::: " + speed);
        vodAdapter.play(speed);
    }

    function _changeNormalSpeed() {
        // 정상 배속으로 변경
        if (currentState != VODManager.PLAY_STATE.STOP && currentState != VODManager.PLAY_STATE.PAUSE && currentState != VODManager.PLAY_STATE.PLAY) {
            currentState = VODManager.PLAY_STATE.PLAY;
            currentSpeedIndex = 5;
            showIndicator(VODManager.PLAY_STATE.PLAY, 1);
            log.printDbg("[VODManager] changeSpeed ::: " + PLAY_SPEED[currentSpeedIndex]);
            vodAdapter.play(PLAY_SPEED[currentSpeedIndex]);
            MiniGuideManager.onChangedPlayState(VODManager.PLAY_STATE.PLAY, null);
        }
    }

    function setPauseTimeout() {
        log.printDbg("[VODManager] PAUSE TIMER ON");
        if (pauseTimeout) clearPauseTimeout();
        pauseTimeout = setTimeout(function () {
            log.printDbg("[VODManager] PAUSE TIMER END, VOD STOP !!");
            closeAllVODPopup();
            _setVodEndTime();
            _exitVOD();
        }, 18 * 1E5);
    }

    function clearPauseTimeout() {
        log.printDbg("[VODManager] PAUSE TIMER OFF");
        clearTimeout(pauseTimeout);
        pauseTimeout = void 0;
    }

    function _serviceStateEventListener(_state) {
        log.printDbg(changeServiceByVODModule + ", " + (currentPlayLayer != null) + " :: NextState === " + StateManager.getServiceStateString(_state.nextServiceState) + ", key === " + _state.key);
        isOtherAppState = false;
        if (currentPlayLayer != null) {
            if (_state.nextServiceState == CONSTANT.SERVICE_STATE.STANDBY) {
                // 대기모드 진입 시 VOD 종료 처리
                _setVodEndTime();
                _exitVOD(null, true);
            }
            else if (_state.nextServiceState == CONSTANT.SERVICE_STATE.TV_VIEWING || _state.nextServiceState == CONSTANT.SERVICE_STATE.OTHER_APP) {
                // TV_VIEWING 또는 OTHER_APP 실행 시
                if (!changeServiceByVODModule) {
                    // VOD가 직접 Service를 변경하지 않은 경우에는 VOD 종료 처리
                    _setVodEndTime();
                    _exitVOD(null, true);
                }
                changeServiceByVODModule = false;
            }
            else if (_state.nextServiceState == CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                isOtherAppState = true;
                // if (currentPlayLayer && currentPlayLayer.isShowing()) currentPlayLayer.hide();
                if (MiniGuideManager.isShowing()) MiniGuideManager.hide();
                // VOD 실행 중 OTHER_APP 실행 시 워터마크와 자막 Hide 처리
                _watermarkVisibleCheck();
                _changeSMIVisible(false);
            }
            else if (_state.nextServiceState == CONSTANT.SERVICE_STATE.VOD) {
                // 2017.08.10 Lazuli
                // 핫키를 통해 VOD로 전환된 경우에는 플레이어 Show 호출 하지 않음
                // (핫키로 인해 홈메뉴 UI가 노출중일 수도 있기때문)
                var isForHotKey = _state.key && LayerManager.isHotKey(_state.key);
                // VOD 상태일 때 플레이어가 Hide 된 상태라면 Show 호출
                if (currentPlayLayer && !currentPlayLayer.isShowing() && !isForHotKey) {
                    currentPlayLayer.show();
                    // 2017.07.27 Lazuli
                    // Pause 광고인 경우 Resume 되면서 미니가이드가 노출되어야 하나,
                    // ChangeSpeed 이벤트가 ServiceState 이벤트보다 빨리 호출되면서 미니가이드가 깜빡이는 현상이 있음
                    // 미니가이드가 노출 중이라면 다시 호출해서 Timeout 초기화 하기 위함
                    log.printDbg("MiniGuideManager.isShowing(), " + MiniGuideManager.isShowing());
                    if (MiniGuideManager.isShowing()) MiniGuideManager.show();
                }
                if (isLoading && isAD) return;
                // VOD 로 상태 변경 시 워터마크와 자막 Show 처리
                _watermarkVisibleCheck();
                _changeSMIVisible(true);
            }
        }
    }

    function _transLanguage(language) {
        if (!language) return "";
        switch (language.toLowerCase()) {
            case "hidesmi" :
                language = "표시 안함";
                break;
            case "chi" :
                language = "중국어";
                break;
            case "en":
            case "eng" :
                language = "영어";
                break;
            case "fre" :
                language = "불어";
                break;
            case "ger" :
                language = "독일어";
                break;
            case "jpn" :
                language = "일본어";
                break;
            case "unknown":
            case "kr":
            case "kor":
            case "xko":
                language = "한국어";
                break;
            case "rus" :
                language = "러시아어";
                break;
            case "spa" :
                language = "스페인어";
                break;
            case "fil" :
                language = "필리핀어";
                break;
            case "vie" :
                language = "베트남어";
                break;
            case "mon" :
                language = "몽골어";
                break;
            case "tha" :
                language = "태국어";
                break;
        }
        return language;
    }

    function requestFromOther(notDestroyPlayList) {
        isCorner = null;
        isHighLight = null;
        isContinuePlay = null;
        isSeriesPlay = null;
        isAutoContinue = null;
        isForcedKidsIframe = null;
        prevVOD = null;

        clearPauseTimeout();

        if (currentVOD) {
            log.printDbg("stopVOD - " + currentVOD.contsName);

            if (currentPlayLayer) {
                currentPlayLayer.onVODEnd();
            }

            _setVodEndTime();

            if (isKidsLastVod) { // 2017.08.12 Yun 예고편에 따른 마지막편 제한 종료 시나리오로 인하여 재추가
                kidsModeCall("kidsSTBOff");
                log.printDbg("KidsMode - LastVOD End");
                return false;
            }
            else {
                _exitVOD(null, true, null, null, true, notDestroyPlayList);
            }
        }
        else {
            VODInfoManager.destroy(notDestroyPlayList);
        }
        return true;
    }

    function _showLastDetailLayer(isOnlyDataChange, initComplete) {
        var lastLayer = LayerManager.getLastLayerInStack();

        log.printDbg("_showLastDetailLayer(" + isOnlyDataChange + ")");
        log.printDbg("_showLastDetailLayer ::: LastLayer is " + (lastLayer ? lastLayer.id : null));

        // 시리즈 이어보기 상태이고, 홈포탈이 노출되고 있지 않으며, 마지막 레이어가 단편 상세일 경우
        if (isContinuePlay && isSeriesPlay && lastLayer && !lastLayer.isShowing() && lastLayer.id.indexOf("VodDetailSingleLayer") > -1) {
            var prevParams = lastLayer.originParams();
            log.printDbg("_showLastDetailLayer ::: const_id = " + (prevParams ? prevParams.const_id : null));
            log.printDbg("_showLastDetailLayer ::: current const_id = " + currentVOD.originData.data.contsId);

            if (!prevParams || prevParams.const_id == currentVOD.originData.data.contsId) {
                if (!isOnlyDataChange) {
                    LayerManager.resumeNormalLayer();
                }

                if (initComplete) {
                    initComplete();
                }
            }
            else {
                lastLayer.reInit(function () {
                    if (initComplete) {
                        initComplete();
                    }

                    if (!isOnlyDataChange) {
                        LayerManager.resumeNormalLayer();
                    }
                });
            }
        }
        else if (!isOnlyDataChange) {
            if (initComplete) {
                initComplete();
            }

            LayerManager.resumeNormalLayer();
        }
        else if (initComplete) {
            initComplete();
        }
    }

    /*
     ### OnCreate
     */
    function initVODManager() {
        log.printDbg("[VODManager] addEventListener and VODManager init");
        vodAdapter.addPlaySpeedChangeListener(_onPlaySpeedChanged);
        vodAdapter.addPlayStateChangeListener(_onPlayStateChangeListener);
        vodAdapter.addPlayPositionChangeListener(_onPlayPositionChanged);
        vodAdapter.addReadyToPlayListener(_onReadyToPlay);
        vodAdapter.addMediaContentChangeListener(_onMediaContentChanged);
        vodAdapter.addPlaybackEndInProgressListener(_onPlaybackEndInProgressListener);
        basicAdapter.addResizeScreenListener(_resizeScreenListener);
    }

    vodAdapter.init();

    return {
        requestPlay: function () {
            if (requestFromOther()) _requestPlay.apply(this, arguments);
        },
        requestPlaySeries: function () {
            if (requestFromOther()) _requestPlaySeries.apply(this, arguments);
        },
        requestPlayList: function () {
            if (requestFromOther(true)) _requestPlayList.apply(this, arguments);
        },
        requestAdditionalContents: function () {
            if (requestFromOther()) _requestAdditionalContents.apply(this, arguments);
        },
        requestPlayHighLight: function () {
            if (requestFromOther()) _requestPlayHighLight.apply(this, arguments);
        },
        requestPlayListWithIdx: _requestPlayListWithIdx,
        requestPlayCorner: _requestPlayCorner,
        requestPlayKidsSound: _requestPlayKidsSound,
        nextCorner: _nextCorner,
        removeIndexListener: function () {
            indexListener = null
        },
        play: _play,
        exitVOD: _exitVOD,
        setVodEndTime: _setVodEndTime,
        finish: _finish,
        openEndPopup: _openEndPopup,
        pause: _pause,
        playFast: _playFast,
        playSlow: _playSlow,
        seekLeft: _seekLeft,
        seekLeftWithPosition: _seekLeftWithPosition,
        seek: _seek,
        seekRight: _seekRight,
        seekRightWithPosition: _seekRightWithPosition,
        seekJumpTo: _seekJumpTo,
        rew: _rew,
        fwd: _fwd,
        getSMILanguage: _getSMILanguage,
        getPlayTime: _getPlayTime,
        getCurrentPosition: _getCurrentPosition,
        getPlayTimeForCorner: _getPlayTimeForCorner,
        getCurrentPositionForCorner: _getCurrentPositionForCorner,
        getCurrentVODInfo: function () {
            return currentVOD
        },
        getCurrentOriginVODInfo: function () {
            return (currentVOD && currentVOD.originData) ? currentVOD.originData.data : null
        },
        getCurrentPlayLayer: function () {
            return currentPlayLayer
        },
        isCornerMode: function () {
            return isCorner
        },
        isADMode: function () {
            return isAD
        },
        isLoading: function () {
            return isLoading
        },
        changeSMILanguage: _changeSMILanguage,
        changeSMIPosition: _changeSMIPosition,
        getDefaultSMILanguageIdx: _getDefaultSMILanguageIdx,
        getDefaultSoundIdx: _getDefaultSoundIdx,
        setAudioLanguage: _setAudioLanguage,
        transLanguage: _transLanguage,
        showLastDetailLayer: _showLastDetailLayer,
        getIsKidsLastVOD: function () {
            return isKidsLastVod;
        },
        setIsKidsLastVOD: function (isLast) {
            isKidsLastVod = isLast;
        },
        onHighLightEnd: _onHighLightEnd
    }
}());

Object.defineProperty(VODManager, "PLAY_STATE", {
    value: {
        STOP: 0, PLAY: 1, PAUSE: 2, REWIND: 3, FASTFOWARD: 4, FASTPLAY: 5, SLOWPLAY: 6, SEEKBACK: 7, SEEKFWD: 8
    }, writable: !1, configurable: !1
});