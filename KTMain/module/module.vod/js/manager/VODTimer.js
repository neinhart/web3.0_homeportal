/**
 * Created by Taesang on 2016-10-05.
 */

window.VODTimer = new function () {
    var timeout, interval;

    function clearTimer(timer) {
        if (timer == "T") {
            clearTimeout(timeout);
            timeout = undefined;
        }
        else if (timer == "I") {
            clearInterval(interval);
            interval = undefined;
        } else {
            clearTimer("T");
            clearTimer("I");
        }
    }

    function createTimeout(time, _isHighLight) {
        clearTimer("I");
        if (timeout) clearTimer("T");
        timeout = setTimeout(function () {
            if (_isHighLight) VODManager.onHighLightEnd();
            else VODManager.nextCorner();
        }, time);
    }

    function createInterval(dir, targetTime, _isHighLight) {
        clearTimer("T");

        function funcSetInterval() {
            try {
                if (dir == "fwd") {
                    log.printDbg("FWD PositionInterval Start");
                    interval = setInterval(function () {
                        if (vodAdapter.getPlayPosition() > targetTime) {
                            if (_isHighLight) VODManager.onHighLightEnd();
                            else VODManager.nextCorner();
                        }
                    }, 5E3);
                } else {
                    log.printDbg("REW PositionInterval Start");
                    interval = setInterval(function () {
                        if (vodAdapter.getPlayPosition() < targetTime) VODManager.seek(targetTime);
                    }, 5E3);
                }
            } catch (e) {
                log.printDbg(e);
            }
        }

        if (!interval) funcSetInterval();
    }

    return {
        clearTimer: clearTimer,
        createTimeout: createTimeout,
        createInterval: createInterval
    };
};
