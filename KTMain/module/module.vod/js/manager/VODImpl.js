/**
 * Created by Lazuli on 2016-12-16.
 */

window.VODImpl = (function () {
    var result = "", const_id = "";

    function _isHDR() {
        if (VODManager.getCurrentVODInfo() != null && _isVODPlaying()) {
            log.printDbg("isHDRYn === " + VODManager.getCurrentVODInfo().isHdrYn);
            return VODManager.getCurrentVODInfo().isHdrYn == "Y";
        }
    }

    function _isKidsVOD() {
        if (VODManager.getCurrentVODInfo() != null && _isVODPlaying()) {
            log.printDbg("isKidsVOD === " + VODManager.getCurrentVODInfo().dispContsClass);
            return VODManager.getCurrentVODInfo().dispContsClass == "06";
        }
    }

    function _isVODPlaying() {
        return (VODManager.getCurrentPlayLayer() != null) ? "1" : "0";
    }

    function _getCurrentVODInfo() {
        var vodData = VODManager.getCurrentVODInfo();
        var returnObj = {};
        if (StateManager.isVODPlayingState() && vodData) returnObj = {
            const_id: vodData.assetId,
            title: vodData.contsName,
            duration: Math.floor(vodAdapter.getPlayTime()),
            viewpoint: Math.floor(VODManager.getCurrentPosition())
        };
        else returnObj = {const_id: "error", title: "error", duration: "error", viewpoint: "error"};
        return returnObj;
    }

    function _stopVODPlaying() {
        result = "0";
        const_id = "";
        if (StateManager.isVODPlayingState() == true || _isVODPlaying() == "1") {
            try {
                const_id = VODManager.getCurrentVODInfo().assetId;
                VODManager.setVodEndTime();
                VODManager.exitVOD();
                result = "0";
            } catch (e) {
                log.printErr(e);
                result = "2";
            }
        } else result = "1";
        return {result: result, const_id: result == "0" ? const_id : ""};
    }

    function _pauseVODPlaying() {
        result = "0";
        if (StateManager.isVODPlayingState() == true || _isVODPlaying() == "1") {
            try {
                VODManager.pause("pause");
                result = "0";
            } catch (e) {
                log.printErr(e);
                result = "2";
            }
        } else result = "1";
        return result;
    }

    function _seekVODPlaying(params) {
        try {
            log.printDbg("_seekVODPlaying :: " + JSON.stringify(params ? params : {}));
            if (StateManager.isVODPlayingState() == true || _isVODPlaying() == "1") {
                if (VODManager.isADMode() || VODManager.isLoading()) result = "0";
                else {
                    if (parseInt(params.seek) < 0) VODManager.seekLeftWithPosition(parseInt(params.seek) * 1E3);
                    else VODManager.seekRightWithPosition(parseInt(params.seek) * 1E3);
                    result = "1";
                }
            } else result = "0";
        } catch (e) {
            result = "0";
        }
        return result;
    }

    function _speedVODPlaying(params) {
        try {
            var keyCode = params.speed;
            if (StateManager.isVODPlayingState() == true || _isVODPlaying() == "1") {
                if (VODManager.isADMode() || VODManager.isLoading()) return "0";
                if (keyCode == KEY_CODE.FF || keyCode == KEY_CODE.BLUE) VODManager.fwd();
                else if (keyCode == KEY_CODE.REW || keyCode == KEY_CODE.RED) VODManager.rew();
                else return "0";
                result = "1";
            } else result = "0";
        } catch (e) {
            result = "0";
        }
        return result;
    }

    function _playVODPaused() {
        result = "0";
        if (StateManager.isVODPlayingState() == true || _isVODPlaying() == "1") {
            try {
                VODManager.pause("resume");
                result = "0";
            } catch (e) {
                log.printErr(e);
                result = "2";
            }
        } else result = "1";
        return result;
    }

    /**
     * VOD 리스트가 아닌 곳에서 상세화면으로 진입 시 호출 (카테고리 정보를 갖고 있지 않은 경우 호출)
     * 디폴트 포커스를 첫번째 구매버튼으로 둔다
     * @public
     * @param param (object) {callback, cat_id, const_id, req_cd, isKids, forced}
     */
    function _showDetailPurchase(param) {
        param.isBtnFocus = true;
        _showDetail(param);
    }

    /**
     * VOD 리스트가 아닌 곳에서 상세화면으로 진입 시 호출 (카테고리 정보를 갖고 있지 않은 경우 호출)
     * @public
     * @param param (object) {callback, cat_id, const_id, req_cd, isKids, isBtnFocus, forced}
     */
    function _showDetail(param) {
        LayerManager.startLoading({preventKey: true});
        var oldCb = param.callback;
        param.callback = function () {
            if (oldCb) oldCb();
            LayerManager.stopLoading();
        };

        if (param.cateInfo) {
            moveLayerImpl(param);
        } else if (!param.cat_id || param.cat_id === "N") {
            _showDetailForNoCatId(param);
        } else if (param.cat_id === "X") {
            _showDetailForXCatId(param);
        } else {
            try {
                var layerId = "";
                VODAmocManager.getCateInfoW3(function (result, response) {
                    if (!result) {
                        log.printErr(result);
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({
                            message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                            reboot: true
                        });

                        if (param.callback) {
                            param.callback("0");
                        }
                        return;
                    } else if (response.catId === "" || (param.useUseYn && response.useYn === "N")) { // 카테고리 정보의 useYn이 N이면 제한 컨텐츠로 봄. (otn에서만)
                        if (param.noCateForced) {
                            _showDetailForXCatId(param);
                        } else {
                            log.printErr(result);
                            HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                            if (param.callback) param.callback("0");
                        }
                        return;
                    }
                    param.cateInfo = response;
                    moveLayerImpl(param);
                }, param.cat_id, DEF.SAID);
            } catch (e) {
                log.printErr(e);

                if (param.callback) {
                    param.callback("0");
                }
            }
        }

        function moveLayerImpl(param) {
            if (param.cat_id == param.const_id && param.cateInfo.catType == "PkgVod") {
                layerId = "VodDetailPackageLayer";
            } else if (param.cateInfo.seriesYn == 'Y') {
                if (param.cateInfo.seriesType == "02") {
                    layerId = "VodDetailMCIDLayer";
                } else {
                    layerId = "VodDetailSeriesLayer";
                }
            } else {
                layerId = "VodDetailSingleLayer";
            }

            log.printDbg(JSON.stringify(param));

            moveLayer(layerId, param);
        }
    }

    function _showDetailForNoCatId(param) {
        LayerManager.startLoading({preventKey: true});
        var oldCb = param.callback;
        param.callback = function () {
            if (oldCb) oldCb();
            LayerManager.stopLoading();
        };

        try {
            VODAmocManager.getContentW3(function (result, response) {
                if (!result) {
                    // 통신 실패
                    log.printErr(result);
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    if (param.callback) param.callback("0");
                } else if (response.contsId) {
                    // 정상
                    param.cat_id = response.catId;
                    param.contsInfo = response;
                    _showDetail(param);
                } else if (param.forced) {
                    // 강제 재호출
                    _showDetailForXCatId(param);
                } else {
                    // 컨텐츠 존재하지 않음
                    HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                    if (param.callback) param.callback("0");
                }
            }, "N", param.const_id, DEF.SAID);
        } catch (e) {
            log.printErr(e);
            if (param.callback) param.callback("0");
        }
    }

    function _showDetailForXCatId(param) {
        try {
            if (param.contsInfo) {
                showDetailForXCatIdImpl(param.contsInfo);
            } else VODAmocManager.getContentW3(function (result, response) {
                if (!result) {
                    log.printErr(result);
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});

                    if (param.callback) {
                        param.callback("0");
                    }
                } else if (response.contsId) {
                    showDetailForXCatIdImpl(response);
                } else {
                    HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});

                    if (param.callback) {
                        param.callback("0");
                    }
                }
            }, "X", param.const_id, DEF.SAID);
        } catch (e) {
            log.printErr(e);

            if (param.callback) {
                param.callback("0");
            }
        }

        function showDetailForXCatIdImpl(contsInfo) {
            param.cat_id = "X";
            param.contsInfo = contsInfo;
            param.cateInfo = {
                buyType: 3,
                catId: "X",
                ltFlag: ""
            };
            moveLayer("VodDetailSingleLayer", param);
        }
    }

    /**
     * VOD 리스트에서 상세화면으로 진입 시 호출 (카테고리 정보를 이미 갖고 있는 경우 호출)
     * @public
     * @param param (object) {callback, cat_id, const_id, req_cd, cateInfo, contsInfo isKids}
     */
    function _showDetailWithData(param) {
        var layerId = "";
        try {
            if (param.contsInfo.itemType == 0) {
                if (param.contsInfo.catType == "PkgVod") {
                    layerId = "VodDetailPackageLayer";
                } else {
                    return;
                }
            } else if (param.contsInfo.itemType == 1) {
                if (param.contsInfo.seriesType == "02") {
                    layerId = "VodDetailMCIDLayer";
                } else {
                    layerId = "VodDetailSeriesLayer";
                }
            } else if (param.contsInfo.itemType == 2) {
                layerId = "VodDetailSingleLayer";
            } else if (param.contsInfo.itemType == 3 || param.contsInfo.itemType == 7 || param.contsInfo.itemType == 8) {
                layerId = "VodDetailAppLayer";
                param.appData = param.contsInfo;
            }
            moveLayer(layerId, param);
        } catch (e) {
            log.printErr(e);
        } finally {
            layerId = null;
            param = null;
        }
    }

    /**
     * 패키지 상품코드로 패키지 상세화면으로 진입 시 호출 (카테고리 정보를 갖고 있지 않은 경우 호출)
     * @public
     * @param param (object) {callback, prdtCd, req_cd}
     */
    function _showDetailForPkgProdCd(param) {
        LayerManager.startLoading({preventKey: true});
        var oldCb = param.callback;
        param.callback = function () {
            if (oldCb) oldCb();
            LayerManager.stopLoading();
        };

        try {
            VODAmocManager.getPkgVodW3(function (result, response) {
                if (!result || response.catId === "") {
                    log.printErr(result);
                    HTool.openErrorPopup({message: ERROR_TEXT.ET004, title: "알림", button: "확인"});
                    if (param.callback) param.callback("0");
                    return;
                }

                param.cateInfo = response;
                log.printDbg(JSON.stringify(param));
                moveLayer("VodDetailPackageLayer", param);
            }, param.prdtCd, DEF.SAID);
        } catch (e) {
            log.printErr(e);
            if (param.callback) param.callback("0");
        }
    }

    /**
     * VOD 상세화면 레이어 Activate
     * @private
     * @param layerId (string) 상세화면 Layer ID 지정
     * @param param (object) {cat_id, const_id, req_cd, cateInfo, isKids}
     */
    function moveLayer(layerId, param) {
        if (!param.isAuthorizedContent && (param.cateInfo.adultOnlyYn == "Y" || (param.contsInfo && param.contsInfo.adultOnlyYn == "Y"))) {
            openAdultAuthPopup(function () {
                param.isAuthorizedContent = true;
                moveLayer(layerId, param);
            }, true, "성인인증 비밀번호");
        } else if (LayerManager.getLayers("VodDetail").length >= 10) {
            showToast("최대 탐색 횟수를 초과하였습니다");
            if (param.callback) param.callback("0");
        } else try {
            LayerManager.activateLayer({
                obj: {
                    id: layerId,
                    type: Layer.TYPE.NORMAL,
                    priority: Layer.PRIORITY.NORMAL,
                    linkage: false,
                    params: param
                },
                moduleId: "module.vod",
                new: true,
                visible: true,
                cbActivate: function () {
                    if (param.callback) param.callback("1");
                }
            });
        } catch (e) {
            log.printErr(e);
            if (param.callback) param.callback("0");
        }
    }

    /**
     * 컨텐츠 시청 및 구매 요청
     * @public
     * @param param (object) {callback, cat_id, const_id, req_cd}
     */
    function _watchContent(param) {
        try {
            if (param.req_cd == null) param.req_cd = "01";
            closeAllVODPopup();
            prePlayProcess({contsId: param.const_id}, param.cat_id, param.req_cd, function (res, data) {
                if (res) {
                    VODManager.requestPlay(data, param.req_cd);
                    if (param.callback) param.callback("1", data);
                } else if (param.callback) param.callback("0");
            }, {forced: param.forced, isWatchContent: true, isAssetPlay: param.isAssetPlay});
        } catch (e) {
            log.printErr(e);
            if (param.callback) param.callback("0");
        }
    }

    /**
     * 컨텐츠 시청 및 구매 요청
     * @public
     * @param param (object) {callback, cat_id, const_id, req_cd}
     */
    function _watchContentForced(param) {
        param.forced = true;
        if (param.req_cd == null) param.req_cd = "24";
        _watchContent(param);
    }

    /**
     * 비즈 결제 후 구매 완료 및 재생 (reqPath = 39)
     * @param param (object) {callback, data}
     * @private
     */
    function _playPurchasedVOD(param) {
        log.printDbg("_playPurchasedVOD() param = " + param.data);
        LayerManager.activateLayer({
            obj: {
                id: "PushNotiPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    "data": param.data,
                    "callback": function (res, pushData) {
                        log.printDbg("_playPurchasedVOD() callback : res = " + res + ", pushData = " + JSON.stringify(pushData));
                        LayerManager.deactivateLayer({
                            id: "PushNotiPopup",
                            remove: true
                        });

                        if (res == 0) { // 시청하기
                            if (pushData.purchaseType == "5") { // 패키지 상세로 점프
                                _showDetailForPkgProdCd({
                                    prdtCd: pushData.contentId,
                                    req_cd: "39"
                                });
                            }
                            else { // 재생
                                // var isAssetPlay = false;
                                // if (pushData.purchaseType == "1" || pushData.purchaseType == "2") {
                                //     isAssetPlay = true;
                                // }
                                _watchContentForced({
                                    const_id: pushData.targetContentId,
                                    cat_id: pushData.targetCatId,
                                    req_cd: "39",
                                    callback: function (res, vodData) {
                                        if (res == "1" && StorageManager.ps.load(StorageManager.KEY.OTN_PARING) == "Y") {
                                            //OTN Paring중일 때 구매공유 여부 전달
                                            if (pushData.purchaseType == "2") { // 시리즈구매
                                                window.SMLSManager.recvBuyItemInfo(function (result) {
                                                }, DEF.SAID, vodData.cCSeridsId || "", vodData.linkTime.buyingDate, pushData.durationTime, 1, pushData.price, "4", pushData.ltFlag == "2" ? "Y" : "N", "N", "Y", "", vodData.resolCd);
                                            } else if (pushData.purchaseType == "1") { // 단편 구매
                                                window.SMLSManager.recvBuyItemInfo(function (result) {
                                                }, DEF.SAID, vodData.assetId, vodData.linkTime.buyingDate, pushData.durationTime, 2, pushData.price, "4", pushData.ltFlag == "2" ? "Y" : "N", "N", "Y", vodData.cCSeridsId || "", vodData.resolCd);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        if (param.callback) param.callback(res);
                    }
                }
            },
            new: true,
            moduleId: "module.vod",
            visible: true
        });
    }

    /**
     * VOD 시청 종료 및 채널 튜닝
     * @public
     * @param param (object) {ChannelObj}
     */
    function _stopVodWithChannelSelection(param) {
        if (!param.channelObj) {
            _stopVodWithEndPopup({withPopup: true, notChangeService: false, channelObj: null});
        } else {
            VODManager.openEndPopup(0, function () {
                VODManager.setVodEndTime();
                VODManager.exitVOD(param.channelObj, false, true);
            }, null, true);
        }
    }

    /**
     * VOD 시청 종료 (팝업 노출 설정 가능)
     * @public
     * @param param (object) {withPopup, notChangeService, channelObj, noRecommend}
     */
    function _stopVodWithEndPopup(param) {
        if (param.withPopup) {
            VODManager.openEndPopup(0, function () {
                VODManager.setVodEndTime();
                VODManager.exitVOD(param.channelObj, param.notChangeService, true);
            }, null, (!param.notChangeService && param.channelObj), null, param.noRecommend);
        } else {
            VODManager.setVodEndTime();
            VODManager.exitVOD(param.channelObj, param.notChangeService, true);
        }
    }

    /**
     * 북마킹 컨텐츠 시청 및 구매 요청
     * @public
     * @param param (object) {callback, cat_id, const_id, time, req_cd}
     */
    function _watchBookmarkContent(param) {
        try {
            if (param.req_cd == null) param.req_cd = "43";
            closeAllVODPopup();
            prePlayProcess({contsId: param.const_id}, param.cat_id, param.req_cd, function (res, data) {
                if (res) {
                    VODManager.requestPlay(data, param.req_cd);
                    if (param.callback) param.callback("1");
                } else if (param.callback) param.callback("0");
            }, {bookmarkTime: param.time});
        } catch (e) {
            log.printErr(e);
            if (param.callback) param.callback("0");
        }
    }

    /**
     * 북마크용 시청 중인 VOD정보 요청
     * @public
     */
    function _getCurrentVODInfoForBookmark() {
        var vodData = VODManager.getCurrentVODInfo();

        var returnObj = {};
        if (StateManager.isVODPlayingState() && vodData) {
            returnObj = {
                const_id: vodData.assetId,
                cat_id: vodData.catId,
                title: vodData.contsName,
                duration: Math.floor(vodAdapter.getPlayTime()),
                viewpoint: Math.floor(VODManager.getCurrentPosition())
            };
        } else returnObj = {const_id: "error", cat_id: "error", title: "error", duration: "error", viewpoint: "error"};
        log.printDbg(JSON.stringify(returnObj));
        return returnObj;
    }

    /**
     * 플레이리스트 재생 요청
     * @public
     * @param param (object) {playList, currentIdx, isLoop, req_cd}
     */
    function _requestVODPlayList(param) {
        log.printDbg(JSON.stringify(param));
        if (param.req_cd == null) param.req_cd = "46";
        VODInfoManager.setPlayList(function (result) {
            if (result) {
                if (param.playList.length > 0) {
                    closeAllVODPopup();
                    prePlayProcess(param.playList[0], param.playList[0].catId, param.req_cd, function (res, data) {
                        if (res) {
                            VODManager.requestPlayList({
                                currentContent: data,
                                reqPathCd: param.req_cd,
                                playList: param.playList,
                                currentIndex: 0,
                                loop: param.isLoop,
                                alreadySetPlayList: true,
                                isKids: param.isKids
                            });
                        }
                    }, {playResolCd: param.playList[0].playResolCd || null, forced: true, isAssetPlay: true});
                }
            }
        }, param.playList, 0, param.isLoop, null);
    }

    /**
     * VOD 모듈에게 KeyEvent 전달
     * @public
     * @param param (object) {keyCode}
     */
    function _onKeyActionForVOD(param) {
        log.printDbg("_onKeyActionForVOD : " + param.keyCode);
        if (param.keyCode == KEY_CODE.EXIT) {
            var lastLayer = LayerManager.getLastLayerInStack({key: "type", value: Layer.TYPE.NORMAL});
            if (lastLayer != null && lastLayer.isShowing()) {
                LayerManager.hideNormalLayer();
                notifyHide();
                if (VODManager.getCurrentPlayLayer() && !VODManager.getCurrentPlayLayer().isShowing()) VODManager.getCurrentPlayLayer().show();
                return true;
            }
        }
        if (VODManager.isLoading() && _isBlockKey(param.keyCode)) return true;
        else {
            switch (param.keyCode) {
                case KEY_CODE.PLAY:
                case KTW.KEY_CODE.PAUSE:
                    if (!VODManager.isLoading()) VODManager.pause();
                    return true;
                case KEY_CODE.CH_UP:
                case KEY_CODE.CH_DOWN:
                case KTW.KEY_CODE.STOP:
                    if (!VODManager.isLoading()) {
                        VODManager.openEndPopup(0, function () {
                            VODManager.setVodEndTime();
                            VODManager.exitVOD();
                        });
                    }
                    return true;
            }
        }
        return false;
    }

    function _isBlockKey(keyCode) {
        // 숫자키
        if (UTIL.keyCodeToNumber(keyCode) > -1) return true;
        switch (keyCode) {
            // 핫키
            case KEY_CODE.FAVORITE :
            case KEY_CODE.FULLEPG :
            case KEY_CODE.FULLEPG_OTS :
            case KEY_CODE.MENU :
            case KEY_CODE.MONTHLY:
            case KEY_CODE.MOVIE :
            case KEY_CODE.MYMENU :
            case KEY_CODE.PURCHASE_LIST :
            case KEY_CODE.REWATCH :
            case KEY_CODE.RECOMMEND :
            case KEY_CODE.SEARCH :
            case KEY_CODE.SETTING :
            case KEY_CODE.SKY_CHOICE :
            case KEY_CODE.WATCH_LIST :
            case KEY_CODE.WISH_LIST :
            // 핫키는 아니지만 추가로 막아야 될 키
            case KEY_CODE.APP_STORE:
            case KEY_CODE.SHOPPING:
            /**
             * [dj.son] 키즈 리모컨 관련 핫키 추가
             */
            case KTW.KEY_CODE.KIDS_MENU :
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_1:
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_2:
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_3:
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_4:
                return true;
        }
        return false;
    }

    return {
        isHDR: _isHDR,
        isKidsVOD: _isKidsVOD,
        isVODPlaying: _isVODPlaying,
        getCurrentVODInfo: _getCurrentVODInfo,
        getCurrentVODInfoForBookmark: _getCurrentVODInfoForBookmark,
        stopVODPlaying: _stopVODPlaying,
        stopVodWithChannelSelection: _stopVodWithChannelSelection,
        stopVodWithEndPopup: _stopVodWithEndPopup,
        pauseVODPlaying: _pauseVODPlaying,
        playVODPaused: _playVODPaused,
        seekVODPlaying: _seekVODPlaying,
        speedVODPlaying: _speedVODPlaying,
        showDetail: _showDetail,
        showDetailWithData: _showDetailWithData,
        showDetailForPkgProdCd: _showDetailForPkgProdCd,
        showDetailPurchase: _showDetailPurchase,
        watchContent: _watchContent,
        watchContentForced: _watchContentForced,
        hp_playPurchasedVOD: _playPurchasedVOD,
        watchBookmarkContent: _watchBookmarkContent,
        requestVODPlayList: _requestVODPlayList,
        onKeyActionForVOD: _onKeyActionForVOD
    }
}());