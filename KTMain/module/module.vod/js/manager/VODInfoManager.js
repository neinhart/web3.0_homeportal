/**
 * Created by Taesang on 2016-09-20.
 */
window.VODInfoManager = new function() {
    var currentCorner = null;
    var cornerGroup = {};
    var sortGb = "";
    var isKidsMode = false, kidsCurrentCnt = 0, kidsLimitCnt = 5;

    var playList = [], playListLoop = false;
    var currentPlayListIdx = 0;
    var currentCornerIdx = 0;
    var currentCornerForContext = null;

    function checkKidsVODCnt() {
        if (isKidsMode) {
            kidsCurrentCnt++;
            log.printDbg("KIDS_CURRENT_VOD_CNT >>> " + kidsCurrentCnt + "/" + kidsLimitCnt);
            return (kidsCurrentCnt == kidsLimitCnt);
        }
        return false;
    }

    function setKidsCnt(cnt) {
        isKidsMode = (cnt > 0);
        log.printDbg("KIDS_VOD_CNT >>> " + cnt);
        kidsLimitCnt = cnt;
        kidsCurrentCnt = 0;
        // 2017.08.10 Lazuli
        // 카운트 리셋 됐을 경우 VOD 매니져에게도 상태를 알려야 함
        if (cnt - 0 == 0) VODManager.setIsKidsLastVOD(false);
    }

    function getThumbnail(callback, assetId) {
        cornerGroup = {};
        VodThumbnailManager.getThumbnailList(function(result, data) {
            if (result) callback(data);
            else callback(false);
        }, assetId);
    }

    function getCorner(callback, assetId, grpId, isContext) {
        log.printDbg(JSON.stringify(cornerGroup));
        if (grpId in cornerGroup) {
            if (isContext) currentCornerForContext = cornerGroup[grpId];
            else currentCorner = cornerGroup[grpId];
            callback(cornerGroup[grpId]);
        }
        else {
            VodThumbnailManager.getCorner(function(result, data) {
                if (result && data && data.items && data.items.length > 0) {
                    for (var i = 0; i < data.items.length; i++) {
                        if (data.items[i].content_id == assetId) {
                            data.curIdx = i;
                            break;
                        }
                    }
                    cornerGroup[grpId] = data;
                    if (isContext) currentCornerForContext = cornerGroup[grpId];
                    else currentCorner = data;
                    callback(data);
                }
                else callback(false);
            }, grpId, assetId);
        }
    }

    function setCurrentCornerForContext() {
        currentCorner = currentCornerForContext;
    }

    function getCornerVod(index) {
        if (currentCorner.items.length <= index || index < 0) return false;
        return currentCorner.items[index];
    }

    function getNextCornerIndex() {
        return (sortGb == "d" || sortGb == "D") ? currentCornerIdx - 1 : currentCornerIdx + 1;
    }

    function setCurrentCornerIndex(index) {
        currentCornerIdx = index;
        for (var j = 0; j < playList.length; j++) {
            if ((playList[j].contsId || playList[j].itemId) == currentCorner.items[currentCornerIdx].content_id) {
                currentPlayListIdx = j;
                break;
            }
        }
    }

    function getCurrentCornerVod() {
        return currentCorner.items[currentCornerIdx];
    }

    function nextSeries() {
        (sortGb == "d" || sortGb == "D") ? currentPlayListIdx-- : currentPlayListIdx++;
        if (playList.length <= currentPlayListIdx || currentPlayListIdx < 0) return false;
        return (playList[currentPlayListIdx] != null);
    }

    function getCurrentSeries() {
        if (currentPlayListIdx != null && playList[currentPlayListIdx]) return playList[currentPlayListIdx];
        else return null;
    }

    function destroy(notDestroyPlayList) {
        currentCorner = null;
        sortGb = null;
        cornerGroup = {};
        currentCornerIdx = 0;
        // 2017.09.11 Lazuli
        // 플레이리스트 재생 시 리스트 셋팅 후 이부분 로직을 타면서 플레이리스트 정보가 초기화 됨
        // 플레이리스트 재생 시에는 플레이리스트 정보 초기화 하지 않도록 수정
        if (!notDestroyPlayList) {
            playList = [];
            playListLoop = null;
            currentPlayListIdx = 0;
        }
    }

    function setPlayList(_callback, _playList, idx, loop, _sortGb) {
        destroy();
        var completeCnt = 0;
        var listLength = _playList.length;
        var isNetworkFail = false;
        var failList = [];
        LayerManager.startLoading();
        log.printDbg("setPlayList === Total " + _playList.length);

        function pushComplete(fail) {
            if (fail) {
                // 2017.09.12 Lazuli
                // 시리즈물 API 통신 중 res 값이 false 일 경우 (네트워크 통신 오류) 관련 에러 팝업 노출 후 VOD 재생 하지 않음
                LayerManager.stopLoading();
                isNetworkFail = true;
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                _callback(false);
            } else if (completeCnt == listLength) {
                LayerManager.stopLoading();
                playList = _playList;
                playListLoop = loop;
                currentPlayListIdx = idx;
                if (KTW.managers.service.ProductInfoManager.isMilitary()) {
                    sortGb = null;
                } else {
                    sortGb = _sortGb;
                }
                log.printDbg("setPlayListComplete === Total " + playList.length);
                // 2017.09.12 Lazuli
                // 편성 삭제된 시리즈물이 있을 경우 편성 삭제 팝업 노출 후 VOD 재생
                if (failList.length > 0) {
                    log.printDbg("failSeriesList === Total " + failList.length);
                    LayerManager.activateLayer({
                        obj: {
                            id: "VODPlayListFail",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                failList: failList,
                                callback: function() {_callback(true);}
                            }
                        },
                        moduleId: "module.vod",
                        visible: true
                    });
                } else _callback(true);
            }
        }

        for (var i = 0; i < _playList.length; i++) {
            if (isNetworkFail) return;
            if (!_playList[i].contsName && _playList[i].itemName) _playList[i].contsName = _playList[i].itemName;
            if (!_playList[i].contsId && _playList[i].itemId) _playList[i].contsId = _playList[i].itemId;
            if (_playList[i].seriesYn == "Y") {
                separateSeriesDataForPlayList(_playList[i], function(res, data, originData) {
                    if (res) {
                        _playList.splice.apply(_playList, [_playList.indexOf(originData), 1].concat(data));
                        completeCnt++;
                        pushComplete();
                    }
                    else if (data != null) {
                        // res 값이 false 일 때는 data 값을 originData로 사용한다
                        failList.push(data.contsName || data.itemName);
                        _playList.splice.apply(_playList, [_playList.indexOf(data), 1]);
                        completeCnt++;
                        pushComplete();
                    } else pushComplete("Fail");
                });
            } else {
                completeCnt++;
                pushComplete();
            }
        }
    }

    function separateSeriesDataForPlayList(listCont, callback) {
        VODAmocManager.getCateInfoW3(function(res, catData, _tmpPlayList) {
            if (res) {
                if (!catData || !catData.catId) {
                    callback(false, listCont);
                    return;
                }

                if (catData.cmbYn === "Y") {
                    // 통합편성
                    VODAmocManager.getCateContNxtW3(function(res, data, _tmpPlayList) {
                        if (res && !!data.cmbSeriesContsList) {
                            if (Object.prototype.toString.call(data.cmbSeriesContsList) == "[object Object]") data.cmbSeriesContsList = [data.cmbSeriesContsList];
                            for (var i in data.cmbSeriesContsList) {
                                data.cmbSeriesContsList[i].catId = catData.catId;
                                data.cmbSeriesContsList[i].playResolCd = _tmpPlayList.isHd;
                                data.cmbSeriesContsList[i].catData = catData;
                            }
                            callback(true, data.cmbSeriesContsList, listCont);
                        } else {
                            callback(false);
                        }
                    }, catData.catId, DEF.SAID, true, _tmpPlayList);
                } else {
                    // 일반편성
                    VODAmocManager.getCateContExtW3(function(res, data) {
                        if (res && !!data.seriesContsList) {
                            if (Object.prototype.toString.call(data.seriesContsList) == "[object Object]") data.seriesContsList = [data.seriesContsList];
                            for (var i in data.seriesContsList) {
                                data.seriesContsList[i].catId = catData.catId;
                                data.seriesContsList[i].catData = catData;
                            }
                            callback(true, data.seriesContsList, listCont);
                        } else {
                            callback(false);
                        }
                    }, catData.catId, DEF.SAID, true, _tmpPlayList);
                }
            } else {
                callback(false);
            }
        }, listCont.contsId, DEF.SAID, true, listCont);
    }

    function isPlayListLoop() {
        return playListLoop;
    }

    function getPlayList() {
        if (playList == null || playList.length == 0) playList = [VODManager.getCurrentOriginVODInfo()];
        return playList;
    }

    function getPlayListWithIdx(idx, callback) {
        // 2017.10.12 Lazuli
        // 요청들어온 Index 값에 해당하는 컨텐츠만 돌려주고 currentPlayListIdx 값은 변경하지 않도록 수정
        // currentPlayListIdx 값은 재생 가능여부에 따라 setCurrentPlayListIdx 함수로 수정
        if (playList[idx]) callback(playList[idx]);
        else callback(false);
    }

    function getPlayListWithContsId(_contsId) {
        var callbackData = null;
        for (var i = 0; i < playList.length; i++) {
            if ((playList[i].contsId || playList[i].itemId) == _contsId) callbackData = playList[i];
        }
        return callbackData;
    }

    function getCurrentPlayListIdx() {
        return currentPlayListIdx;
    }

    function setCurrentPlayListIdx(_idx) {
        currentPlayListIdx = _idx;
    }

    function nextPlayList(callback) {
        (sortGb == "d" || sortGb == "D") ? currentPlayListIdx-- : currentPlayListIdx++;
        if (playList.length <= currentPlayListIdx || currentPlayListIdx < 0) {
            VODManager.exitVOD();
            callback(null);
        }
        else {
            callback(playList[currentPlayListIdx]);
        }
    }

    return {
        checkKidsVODCnt: checkKidsVODCnt,
        setKidsCnt: setKidsCnt,
        getThumbnail: getThumbnail,
        getCorner: getCorner,
        getCornerVod: getCornerVod,
        getNextCornerIndex: getNextCornerIndex,
        setCurrentCornerForContext: setCurrentCornerForContext,
        getCurrentCornerVod: getCurrentCornerVod,
        setCurrentCornerIndex: setCurrentCornerIndex,
        getCurrentCornerData: function() {return currentCorner},
        nextSeries: nextSeries,
        getCurrentSeries: getCurrentSeries,
        setPlayList: setPlayList,
        getPlayList: getPlayList,
        getPlayListWithIdx: getPlayListWithIdx,
        getPlayListWithContsId: getPlayListWithContsId,
        isPlayListLoop: isPlayListLoop,
        getCurrentPlayListIdx: getCurrentPlayListIdx,
        setCurrentPlayListIdx: setCurrentPlayListIdx,
        nextPlayList: nextPlayList,
        destroy: destroy
    }
};