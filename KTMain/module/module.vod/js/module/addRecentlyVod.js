/**
 * Created by ksk91_000 on 2017-03-29.
 */
window.addRecentlyVod = function (catId, contsId, imgUrl, prInfo, isKids) {
    var a = JSON.parse(StorageManager.ms.load(StorageManager.KEY.RECENTLY_VOD));
    if(!a) a = [];
    if(a.length>=10) a.pop();
    for(var i=0; i<a.length; i++) if(a[i].catId===catId&&a[i].contsId==contsId){a.splice(i,1);break;}
    a.unshift({catId: catId, contsId: contsId, imgUrl: imgUrl, prInfo: prInfo, isKids: isKids});

    StorageManager.ms.save(StorageManager.KEY.RECENTLY_VOD, JSON.stringify(a));
};