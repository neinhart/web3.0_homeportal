/**
 * Created by ksk91_000 on 2017-04-05.
 */
window.goToTarget = function (type, target, reqPathCd) {
    log.printDbg("GoToTarget type = " + type + " , reqPathCd = " + reqPathCd);

    switch (type - 0) {
        case 0:
            var menuData = MenuDataManager.searchMenu({
                menuData: MenuDataManager.getMenuData(),
                allSearch: false,
                cbCondition: function (menu) {
                    if (menu.id == target) {
                        return true;
                    }
                }
            })[0];

            if (menuData != undefined) {
                MenuServiceManager.jumpMenu({menu: menuData});
            } else {
                showToast("잘못된 접근입니다");
            }
            break;
        case 1:    //시리즈
            VODImpl.showDetail({cat_id: target, const_id: target, req_cd: reqPathCd});
            break;
        case 2:    //컨텐츠
            VODImpl.showDetail({cat_id: "N", const_id: target, req_cd: reqPathCd});
            break;
        case 3:    //멀티캐스트 양방향 서비스
            var loc = target.split(",");
            var nextState;

            if (loc[0] == CONSTANT.APP_ID.MASHUP) {
                nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            } else {
                nextState = CONSTANT.SERVICE_STATE.OTHER_APP;
            }

            AppServiceManager.changeService({
                nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {
                    type: CONSTANT.APP_TYPE.MULTICAST,
                    param: loc[0],
                    ex_param: loc[3]
                }
            });
            break;
        case 7:    //유니캐스트 양방향 서비스
            var nextState = StateManager.isVODPlayingState() ? CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
            AppServiceManager.changeService({
                nextServiceState: nextState,
                obj: {
                    type: CONSTANT.APP_TYPE.UNICAST,
                    param: target
                }
            });
            break;
        case 8:    //웹뷰
            AppServiceManager.startChildApp(target);
            break;
    }
}