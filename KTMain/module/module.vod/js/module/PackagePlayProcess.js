/**
 * Created by lyllyl on 2017-01-09.
 *
 * 재생 요청시 구매여부 확인, 결제, 화질선택까지 진행한 뒤,
 * VODManager에 필요한 정보를 리턴한다.
 *
 * vodInfo: VOD 정보
 * callback: 재생여부, VODManager에 필요한 정보.
 */
window.packagePlayProcess = function(packageInfo, contentsList, linkTimeInfo, reqPathCd, callback, updateCallback, authComplete) {
    if (!authComplete && UTIL.isLimitAge(packageInfo.prInfo)) {
        LayerManager.stopLoading();
        openAdultAuthPopup(function(res) {
            if (res) {
                packagePlayProcess(packageInfo, contentsList, linkTimeInfo, reqPathCd, callback, updateCallback, true);
            } else callback(false);
        }, true);
        return;
    }

    if(linkTimeInfo.buyCheckYn=="Y"||linkTimeInfo.connerYn=="Y" || linkTimeInfo.pptBuyYn=="Y") {
        onPaymentComplete(true);
    } else {
        paymentProcess();
    }

    function paymentProcess() {
        if(JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV)).ppvUseYn==="N") {
            HTool.openErrorPopup({message: ["고객님께서 가입하신 상품은", "유료 VOD를 이용하실 수 없습니다", "", "신청문의: 국번없이 100번"], button:"확인", "title": "결제", callback: function () { callback(false); }});
        } else LayerManager.activateLayer({
            obj: {
                id: "PaymentPackagePopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: false,
                params: {
                    catId: packageInfo.catId||packageInfo.itemId,
                    optionList: makePackageOption(),
                    linkTimeMap: linkTimeInfo,
                    reqPathCd: reqPathCd,
                    callback: function onPaymentCompletePrev(res) {
                        if(res && updateCallback) updateCallback({buyType:"K"});
                        LayerManager.deactivateLayer({id:"PaymentPackagePopup", onlyTarget: true});
                        onPaymentComplete(res);
                    }
                }
            },
            visible: true,
            moduleId: "module.vod_payment"
        });
    }

    function makePackageOption() {
        var option = {};

        option.data = packageInfo;
        option.isPackage = true;
        option.ltFlag = packageInfo.ltFlag==2;
        option.price = option.ltFlag?packageInfo.ltPrice:packageInfo.price;
        option.vatPrice = option.ltFlag?packageInfo.vatLtPrice:packageInfo.vatPrice;
        option.itemName = packageInfo.itemName||packageInfo.catName;
        option.pkgVodPrdCd = packageInfo.pkgVodPrdCd;
        option.viewTime = option.ltFlag?packageInfo.ltPeriod:packageInfo.viewTime;
        option.resolCd = packageInfo.resolCd;
        return [option];
    }

    function onPaymentComplete(result) {
        if(result) {
            if(contentsList.length==1) prePlayProcessImpl(contentsList[0]);
            else {
                LayerManager.activateLayer({
                    obj: {
                        id: "VODSelectPackagePlayPopupLayer",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: false,
                        params: {
                            list: contentsList,
                            isBuyNow : true,
                            callback: function(res, data) {
                                if(res) prePlayProcessImpl(data);
                            }
                        }
                    },
                    visible: true,
                    moduleId: "module.vod"
                });
            }
        }else callback(false);
    }

    function prePlayProcessImpl(data) {
        if(data.itemType==2) { // 단편인 경우 바로 재생
            packageInfo.contStatusData = { buyYn: "Y", connerYn: "N", pptBuyYn: "N" };
            prePlayProcess(data, packageInfo.itemId || packageInfo.catId, reqPathCd, callback, { catData: packageInfo, authComplete: authComplete });
        } else if(data.itemType==1) { // 시리즈인 경우 회차 조회 후 1회차 재생
            LayerManager.startLoading({preventKey: true});
            var cateContFunc = data.cmbYn=="Y"?VODAmocManager.getCateContNxtW3:VODAmocManager.getCateContExtW3;
            cateContFunc(function (res, data2) {
                var contsList;
                if(res && data2 && (contsList = data.cmbYn=="Y"?data2.cmbSeriesContsList:data2.seriesContsList)) {
                    if(!Array.isArray(contsList)) contsList = [contsList];
                    data.contStatusData = { buyYn: "Y", connerYn: "N", pptBuyYn: "N" };
                    prePlayProcess(contsList[0], data.catId || data.itemId, reqPathCd, callback, { catData: data, authComplete: authComplete });
                } else {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    LayerManager.stopLoading();
                }
            }, data.catId || data.itemId, DEF.SAID);
        }
    }
};