/**
 * Created by ksk91_000 on 2016-10-31.
 */
window.openAdultAuthPopup = function(callback, autoClose) {
    if(AdultAuthorizedCheck.isAdultAuthorized()) {
        callback(true);
    }else LayerManager.activateLayer({
        obj: {
            id: "VodAdultAuthPopup",
            type: Layer.TYPE.POPUP,
            priority: Layer.PRIORITY.POPUP,
            linkage: true,
            params: {
                callback: function (res) {
                    if (autoClose){
                        LayerManager.deactivateLayer({
                            id: "VodAdultAuthPopup",
                            remove: true
                        });
                    } if(callback) callback(res);
                }
            }
        },
        new: true,
        moduleId: "module.vod",
        visible: true
    });
};