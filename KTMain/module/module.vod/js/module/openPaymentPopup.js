/**
 * Created by lyllyl on 2017-01-09.
 *
 * 재생 요청시 구매여부 확인, 결제, 화질선택까지 진행한 뒤,
 * VODManager에 필요한 정보를 리턴한다.
 * 현재 "다른화질 구매"에만 사용됨.
 *
 * vodInfo: VOD 정보
 * callback: 재생여부, VODManager에 필요한 정보.
 */
window.openPaymentPopup = function(buttonList, catId, reqPathCd, callback, authComplete, updateCallback) {
    //// 키즈모드ON 편수제한에 따른 종료 이벤트
    if (KidsModeManager.isKidsMode() && VODManager.getIsKidsLastVOD()) {
        kidsModeCall("kidsSTBOff");
        log.printDbg("KidsMode - LastVOD End");
        return;
    }

    if(!authComplete && UTIL.isLimitAge(buttonList.prInfo)) {
        openAdultAuthPopup(function(res) { if(res) openPaymentPopup(buttonList, catId, reqPathCd, callback, true, updateCallback); }, true);
        return;
    }

    var linkTimeMap = {};
    var optionList = buttonList.optionList;
    var isSeries = false;
    if (buttonList.type == ButtonObject.TYPE.SERIES) {
        optionList = buttonList.seriesData.seriesList;
        for (var i = 0; i < optionList.length; i++) {
            optionList[i].seriesCouponData = buttonList.optionList[i].seriesCouponData;
        }
        isSeries = true;
    }

    if (buttonList.couponCount && buttonList.couponCount > 0) {
        openCouponCheckPopup(buttonList, isSeries, function(isCheckCoupon) {
            if(isCheckCoupon==-1) {
                callback(false);
            } else {
                paymentProcess({isCheckCoupon: isCheckCoupon == 0});
            }
        });
    } else {
        paymentProcess();
    }

    function paymentProcess(option) {
        LayerManager.activateLayer({
            obj: {
                id: "PaymentVODPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    catId: catId,
                    optionList: optionList,
                    reqPathCd: reqPathCd,
                    isCheckCoupon: option ? !!option.isCheckCoupon : false,
                    callback: onPaymentComplete
                }
            },
            new: true,
            visible: true,
            moduleId: "module.vod_payment"
        });

        function onPaymentComplete(result, data) {
            if(result) {
                if (data.option.isSeries) {
                    if(updateCallback) updateCallback({buyType:"S", resolCd:data.option.resolCd});
                    var btn = new SingleButtonObject(ButtonObject.TYPE.NORMAL, buttonList.optionList);
                    btn.setBuyYn(true, data.option.data[0].resolCd.indexOf("|")>=0?data.option.resolCd:null);
                    prePlayProcessByButtonObj(btn, catId, reqPathCd, callback, false, null, authComplete, undefined, updateCallback, false);
                } else {
                    if(updateCallback) updateCallback({buyType:"N", resolCd:data.option.resolCd, assetId: data.option.assetId});

                    LayerManager.startLoading({preventKey: true});
                    getLinkTimeData([data.option], function(res) {
                        LayerManager.stopLoading();
                        if (res) {
                            if (linkTimeMap[data.option.assetId].linkTime - 0 > 0 || data.option.otmLinkTime - 0 > 0) {
                                openContinuePopup(data.option, linkTimeMap[data.option.assetId], function(result) {
                                    if(result==-1) callback(false);
                                    else {
                                        linkTimeMap[data.option.assetId].linkTime = result;
                                        returnData(data.option);
                                    }
                                });
                            } else returnData(data.option);
                        } else {
                            callback(false, data);
                        }
                    });
                }
            }else callback(false, data);
        }
    }

    function openCouponCheckPopup(vodInfo, isSeries, callback) {
        LayerManager.activateLayer({
            obj: {
                id: "VodCouponCheckPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    vodData: vodInfo,
                    isSeries: isSeries,
                    callback: callback
                }
            },
            moduleId: "module.vod",
            new: true,
            visible: true
        });
    }

    function openContinuePopup(vodInfo, linkTimeInfo, callback) {
        LayerManager.stopLoading();
        log.printInfo("Activate ContinuePopup!!!");
        log.printInfo("bookmarkTime: " + bookmarkTime + ", linkTime:" + linkTimeInfo.linkTime + ", otmLinkTime:" + linkTimeInfo.otmLinkTime);
        LayerManager.activateLayer({
            obj: {
                id: "VodContinuePopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    contsName: vodInfo.contsName,
                    runtime: vodInfo.data.runtime,
                    prInfo: vodInfo.data.prInfo,
                    resolCd: vodInfo.resolCd,
                    linkTime: linkTimeInfo.linkTime,
                    otmLinkTime: vodInfo.otmLinkTime,
                    callback: function (res, isOtmPlay) {
                        log.printInfo("ContinuePopup ResTime : " + res);
                        callback(res, isOtmPlay);
                    }
                }
            },
            new: true,
            moduleId: "module.vod",
            visible: true
        });
    }

    function getAuthType(option) {
        var linkTimeInfo = linkTimeMap[option.assetId];
        if(linkTimeInfo.connerYn=="Y") {
            return "S";
        }else if(linkTimeInfo.pptBuyYn =="Y") {
            return "P";
        }else if(linkTimeInfo.buyYn == "Y") {
            return "P";
        }else if(option.otnYn) {
            return "O";
        }else if(option.vatPrice==0) {
            return "F";
        }
    };

    function getLinkTimeData(optionList, callback) {
        var linkTimeCnt = 0;
        for (var i = 0; i < optionList.length; i++) {
            if (!linkTimeMap.hasOwnProperty(optionList[i].assetId)) {
                linkTimeMap[optionList[i].assetId] = {};
                var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                VODAmocManager.getLinkTimeNxt(function (result, data, assetId) {
                    if (result) {
                        linkTimeCnt++;
                        linkTimeMap[assetId] = data;
                        if (linkTimeCnt == optionList.length) callback(true);
                    } else callback(false, "네트워크 에러");
                }, DEF.SAID, optionList[i].assetId, custEnv ? custEnv.buyTypeYn : "Y", true, optionList[i].assetId);
            } else linkTimeCnt++;
        }
    }

    function getRecentlyData(option) {
        if(option.catData && (option.catData.itemType==1 || option.catData.seriesYn=="Y")) {
            return {
                imgUrl: option.catData.imgUrl,
                contsName: option.catData.catName||option.catData.itemName,
                prInfo: option.catData.prInfo,
                catId: catId,
                contsId: catId,
                contYn: "N",    //시리즈:"N", 컨텐츠:"Y"
                runTime: option.data.runtime, //최종 재생시간
                wonYn: option.catData.wonYn,
                adultOnlyYn: option.data.adultOnlyYn,
                cCSersId: option.catData.cCSersId
            }
        } else {
            return {
                imgUrl: option.data.imgUrl,
                contsName: option.contsName,
                prInfo: option.data.prInfo,
                catId: catId,
                contsId: option.contsId,
                contYn: "Y",
                runTime: option.data.runtime, //최종 재생시간
                wonYn: option.data.wonYn,
                adultOnlyYn: option.data.adultOnlyYn
            }
        }
    }

    var kidsModule;
    function kidsModeCall(method) { // 키즈모듈 API호출
        kidsModule = ModuleManager.getModule("module.kids");
        if (!kidsModule) {
            ModuleManager.loadModule({
                moduleId: "module.kids",
                cbLoadModule: function(success) {
                    if (success) kidsModeCall(method);
                }
            });
        }
        else {
            kidsModule.execute({
                method: method,
                params: null
            });
        }
    }

    function returnData(option) {
        callback(true, {
            assetId: option.assetId,
            catId: catId,
            resolCd: option.resolCd,
            contsName: option.contsName||option.itemName,
            prInfo: option.data.prInfo,
            dispContsClass: option.data.dispContsClass,
            isHdrYn : option.data.isHdrYn,
            isMultiAud: option.isMultiAud,
            capUrl : option.capUrl,
            basicCaptViewYn: !!option.basicCaptViewYn,
            defAudio: option.data.defAudio,
            quickVodYn: option.data.quickVodYn,
            quickSvcCd: option.data.quickSvcCd,
            adultOnlyYn: option.data.adultOnlyYn,
            originData: option,
            getLinkTime: linkTimeMap[option.assetId],
            authType: getAuthType(option),
            recentlyData: getRecentlyData(option)
        });
    }
};