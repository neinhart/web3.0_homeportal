/**
 * Created by lyllyl on 2017-01-09.
 *
 * 재생 요청시 구매여부 확인, 결제, 화질선택까지 진행한 뒤,
 * VODManager에 필요한 정보를 리턴한다.
 *
 * vodInfo: VOD 정보
 * callback: 재생여부, VODManager에 필요한 정보.
 */
window.cornerSelectProcess = function(buttonList, catId, reqPathCd, callback, authComplete, updateCallback) {
    (function() {
        if(buttonList.noCorner) {
            buttonList.execute(catId, reqPathCd, callback, false, null, authComplete);
        } else if(buttonList.connerYn) {
            callback(true, authComplete);
        } else {
            if(JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV)).ppvUseYn==="N") {
                HTool.openErrorPopup({message: ["고객님께서 가입하신 상품은", "유료 VOD를 이용하실 수 없습니다", "", "신청문의: 국번없이 100번"], button:"확인", "title": "결제", callback: function () { callback(false); }});
            } else if (reqPathCd == "39") {
                // Push 재생인데 시청 권한이 없는 경우, 다시 구매 로직을 타면 안되니 오류 처리한다.
                HTool.openErrorPopup({
                    message: ["선택하신 VOD를 재생할 수 없습니다", "이용에 불편을 드려 죄송합니다"],
                    button: "닫기",
                    "title": "오류",
                    callback: function () {
                        callback(false);
                    }
                });
            } else if (buttonList.cornerList.length == 1) {
                openCornerRegPopup(buttonList.cornerList[0]);
            } else {
                LayerManager.activateLayer({
                    obj: {
                        id: "VodMonthFlatRatePopup",
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: false,
                        params: {
                            list: buttonList.cornerList,
                            callback: openPaymentCornerPopup
                        }
                    },
                    new: true,
                    visible: true,
                    moduleId: "module.vod"
                });
            }
        }
    })();

    function openCornerRegPopup(data) {
        if(data.ossYn=="Y") {
            LayerManager.activateLayer({
                obj: {
                    id: "PaymentAskBuyPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        data: data,
                        callback: onAgreeBuyPopup
                    }
                },
                new: true,
                moduleId:"module.vod_payment",
                visible: true
            });
        } else {
            LayerManager.activateLayer({
                obj: {
                    id: "VodNoOssPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        data: data,
                        callback: callback
                    }
                },
                new: true,
                moduleId:"module.vod",
                visible: true
            });
        }
    }

    function onAgreeBuyPopup(result, data){
        if(result) {
            openPaymentCornerPopup(result, data);
        } else callback(false);
    }

    function openPaymentCornerPopup(res, data) {
        if(res) {
            LayerManager.activateLayer({
                obj: {
                    id: "PaymentCornerPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        data: data,
                        callback: onPaymentComplete
                    }
                },
                new: true,
                moduleId: "module.vod_payment",
                visible: true
            });
        } else callback(false);
    }

    function onPaymentComplete(result, data, id) {
        if(result) {
            LayerManager.deactivateLayer({id:id||"PaymentCornerPopup", onlyTarget: true});
            buttonList.setCornerYn(true);
            callback(true, authComplete);

            if(updateCallback) updateCallback({buyType:"C"});

            AppServiceManager.updatePkgInfo(function() {
                log.printDbg(StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST));
            });
        }else {
            LayerManager.deactivateLayer({id:id||"PaymentCornerPopup", onlyTarget: true});
            if(data) onError(data);
            callback(false);
        }
    }
    
    function onError(data) {
        LayerManager.stopLoading();
        switch (data) {
            case "VODE-00013": HTool.openErrorPopup({message:ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot:true}); break;
            default: HTool.openErrorPopup({message: data}); break;
        }

        callback(false);
    }
};