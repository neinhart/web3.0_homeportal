/**
 * Created by lyllyl on 2017-01-09.
 *
 * 재생 요청시 구매여부 확인, 결제, 화질선택까지 진행한 뒤,
 * VODManager에 필요한 정보를 리턴한다.
 *
 * vodInfo: VOD 정보
 * callback: 재생여부, VODManager에 필요한 정보를 리턴함.
 * extraParam :
 *       - noContinuePopup : 이어보기 팝업 노출 여부 ('false'면 무조건 0초부터 재생)
 *       - bookmarkTime : 북마크 시간
 *       - authComplete : 성인인증이 완료되었는지 여부 ('true'면 성인인증팝업 띄우지 않음)
 *       - forced : 카테고리 정보가 없을 때 "X"로 호출할지 여부 (구매목록 등 에서 사용)
 *       - isWatchContent : 외부에 의해 재생되는지 여부 (watchContent)
 *       - updateCallback : 구매완료시 받을 콜백 함수 (화면 갱신을 위함)
 *       - autoPlay : 자동 몰아보기 여부 ('true'면 팝업 없이 최고화질 재생)
 */
window.prePlayProcess = function (vodInfo, catId, reqPathCd, callback, extraParam) {
    if (!extraParam) extraParam = {};
    log.printInfo("extraParam : " + JSON.stringify(extraParam));

    if (KidsModeManager.isKidsMode() && VODManager.getIsKidsLastVOD()) {
        // 키즈모드ON 편수제한에 따른 종료 이벤트
        kidsModeCall("kidsSTBOff");
        log.printDbg("KidsMode - LastVOD End");
        return;
    }

    if (!extraParam.authComplete && (UTIL.isLimitAge(vodInfo.prInfo) || vodInfo.adultOnlyYn === "Y")) {
        // 성인인증이 필요하면 우선 성인인증 팝업을 띄움
        openAdultAuthPopup(function (res) {
            extraParam.authComplete = true;
            if (res) prePlayProcess(vodInfo, catId, reqPathCd, callback, extraParam);
            else callback(false);
        }, true);
        return;
    }

    var oldCb = callback;

    callback = function () {
        // 콜백 호출전에 로딩 종료
        LayerManager.stopLoading();
        if (oldCb) oldCb.apply(null, arguments);
    };

    LayerManager.startLoading({preventKey: true});

    if (!vodInfo.contsId) {
        if (vodInfo.itemId) vodInfo.contsId = vodInfo.itemId;
        else {
            callback(false);
            return;
        }
    }

    if (catId === "X") {
        VODAmocManager.getContentW3(function (res, vodData) {
            if (res) OptionListFactory.getSingleOption(vodData, {catId: "X"}, reqPathCd, function (res, data) {
                if (res) {
                    prePlayProcessImpl(data);
                } else {
                    onError(data);
                }
            });
        }, "X", vodInfo.contsId, KTW.SAID);
    } else if (!catId || catId === "N" || !vodInfo.hasOwnProperty("resolCd") || !vodInfo.hasOwnProperty("price")) {
        //getContents의 데이터가 아닐경우 (빈약한 데이터)
        VODAmocManager.getContentW3(function (res, data) {
            if (res) {
                if (data.contsId && data.catId) {
                    prePlayProcess(data, data.catId, reqPathCd, callback, extraParam);
                }
                else if (extraParam.forced) {
                    prePlayProcess(vodInfo, "X", reqPathCd, callback, extraParam);
                }
                else {
                    onError("VODE-00017");
                }
            } else {
                onError("VODE-00013");
            }
        }, catId ? catId : "N", vodInfo.contsId, KTW.SAID);
    } else {
        // 정상적인 데이터라면
        if (extraParam.catData) {
            OptionListFactory.getOptionListForPlay(vodInfo, extraParam.catData, reqPathCd, function (res, btnList) {
                if (res) {
                    prePlayProcessImpl(btnList);
                } else {
                    onError(btnList);
                }
            });
        } else {
            VODAmocManager.getCateInfoW3(function (res, catData) {
                if (res) {
                    OptionListFactory.getOptionListForPlay(vodInfo, catData, reqPathCd, function (res, btnList) {
                        if (res) {
                            prePlayProcessImpl(btnList);
                        } else {
                            onError(btnList);
                        }
                    });
                } else {
                    onError("VODE-00013");
                }
            }, catId, DEF.SAID);
        }
    }

    function getAvailableData(data) {
        if (extraParam.playResolCd) {
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].optionList.length; j++) {
                    if (extraParam.playResolCd === data[i].optionList[j].resolCd) {
                        data[i].optionList = [data[i].optionList[j]];
                    }
                }
            }
        } else if (extraParam.isAssetPlay) {
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].optionList.length; j++) {
                    if (vodInfo.contsId === data[i].optionList[j].assetId) {
                        data[i].optionList = [data[i].optionList[j]];
                    }
                }
            }
        }

        var boughtData = [];
        var noBoughtData = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].buyYn || data[i].connerYn || data[i].pptBuyYn || data[i].lessPrice == 0) {
                boughtData.push(data[i]);

            } else {
                noBoughtData.push(data[i]);
            }
        }

        if (boughtData.length > 0) {
            return [boughtData[0]];
        } else {
            return noBoughtData;
        }
    }

    function prePlayProcessImpl(btnList) {
        var availableData = getAvailableData(btnList);
        if (availableData.length > 1) {
            LayerManager.stopLoading();
            LayerManager.activateLayer({
                obj: {
                    id: "VodPickBuyOptionPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        dataList: btnList,
                        callback: function (index) {
                            if (index == -1) callback(false);
                            else availableData[index].execute(catId, reqPathCd, callback, extraParam.noContinuePopup, extraParam.bookmarkTime, extraParam.authComplete, false, extraParam.updateCallback, extraParam.autoPlay)
                        }
                    }
                },
                new: true,
                moduleId: "module.vod",
                visible: true
            });
        } else {
            prePlayProcessByButtonObj(availableData[0], catId, reqPathCd, callback, extraParam.noContinuePopup, extraParam.bookmarkTime, extraParam.authComplete, extraParam.isWatchContent, extraParam.updateCallback, extraParam.autoPlay);
        }
    }

    function onError(errCode) {
        switch (errCode) {
            case "VODE-00017":
                HTool.openErrorPopup({
                    message: ERROR_TEXT.ET002.concat([""].concat(ERROR_TEXT.ET_CALL).concat(["(VODE-00017)"])),
                    callback: function () {
                        callback(false);
                    }
                });
                break;
            case "VODE-00013":
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                break;
            default:
                callback(false);
                break;
        }
    }

    var kidsModule;

    function kidsModeCall(method) { // 키즈모듈 API호출
        kidsModule = ModuleManager.getModule("module.kids");
        if (!kidsModule) {
            ModuleManager.loadModule({
                moduleId: "module.kids",
                cbLoadModule: function (success) {
                    if (success) kidsModeCall(method);
                }
            });
        }
        else {
            kidsModule.execute({
                method: method,
                params: null
            });
        }
    }
};

window.prePlayProcessByButtonObj = function (buttonObj, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, isWatchContent, updateCallback, autoPlay, extraParams) {
    if (!reqPathCd) {
        reqPathCd = "01";
    }
    var oldCb = callback;
    callback = function () {
        LayerManager.stopLoading();

        if (oldCb) {
            oldCb.apply(null, arguments);
        }
    };

    //// 키즈모드ON 편수제한에 따른 종료 이벤트
    if (KidsModeManager.isKidsMode() && VODManager.getIsKidsLastVOD()) {
        kidsModeCall("kidsSTBOff");
        log.printDbg("KidsMode - LastVOD End");
        return;
    }

    LayerManager.startLoading({preventKey: true});

    if (!authComplete && UTIL.isLimitAge(buttonObj.prInfo)) {
        LayerManager.stopLoading();
        openAdultAuthPopup(function (res) {
            if (res) {
                prePlayProcessByButtonObj(buttonObj, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, true, isWatchContent, updateCallback, autoPlay);
            } else {
                callback(false);
            }
        }, true);
        return;
    }

    var linkTimeMap = {};
    var optionList = buttonObj.optionList;

    if (buttonObj.type == ButtonObject.TYPE.CORNER) {
        LayerManager.stopLoading();
        cornerSelectProcess(buttonObj, catId, reqPathCd, callback, authComplete, updateCallback);
    } else if (buttonObj.type == ButtonObject.TYPE.VODPPT) {
        // VOD 기간정액
        LayerManager.stopLoading();
        vodPptSelectProcess(buttonObj, catId, optionList[0].contsId, reqPathCd, callback, authComplete, updateCallback);
    } else if (buttonObj.type == ButtonObject.TYPE.SERIES) {
        if (!buttonObj.buyYn && !buttonObj.connerYn && !buttonObj.pptBuyYn && !buttonObj.otnYn && buttonObj.lessPrice - 0 > 0) {
            if (reqPathCd == "39") {
                // Push 재생인데 시청 권한이 없는 경우, 다시 구매 로직을 타면 안되니 오류 처리한다.
                HTool.openErrorPopup({
                    message: ["선택하신 VOD를 재생할 수 없습니다", "이용에 불편을 드려 죄송합니다"],
                    button: "닫기",
                    "title": "오류",
                    callback: function () {
                        callback(false);
                    }
                });
            } else {
                optionList = buttonObj.seriesData.seriesList;
                // 일단 쿠폰 데이터가 buttonObj 에 있으니까
                // 이걸 구매 시에 사용하는 optionList 로 전달..
                for (var i = 0; i < optionList.length; i++) {
//                optionList[i].couponData = buttonObj.optionList[i].couponData;
                    optionList[i].seriesCouponData = buttonObj.optionList[i].seriesCouponData;
                }

                if (buttonObj.couponCount && buttonObj.couponCount > 0) {
                    openCouponCheckPopup(buttonObj, true, false, function (isCheckCoupon) {
                        if (isCheckCoupon == -1) {
                            callback(false);
                        } else {
                            LayerManager.startLoading({preventKey: true});
                            paymentProcess({isCheckCoupon: isCheckCoupon == 0});
                        }
                    });
                } else if (extraParams && extraParams.otherCouponBtn && extraParams.otherCouponBtn.couponCount && extraParams.otherCouponBtn.couponCount > 0) {
                    // 타 구매수단 이용권 알림 팝업 (시리즈 선택했는데 단편 이용권이 있는 경우)
                    openCouponCheckPopup(extraParams.otherCouponBtn, false, true, function (isCheckCoupon) {
                        if (isCheckCoupon == -1) {
                            callback(false);
                        } else if (isCheckCoupon == 0) { // 타 구매수단 이용권 알림 팝업에서 확인 입력 시 그냥 결제 진행
                            LayerManager.startLoading({preventKey: true});
                            paymentProcess();
                        }
                        else { // 취소 입력 시 닫음.
                            callback(false);
                        }
                    });
                } else {
                    LayerManager.startLoading({preventKey: true});
                    paymentProcess();
                }
            }
        } else {
            var btn = new SingleButtonObject(ButtonObject.TYPE.NORMAL, buttonObj.optionList);
            prePlayProcessByButtonObj(btn, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, undefined, updateCallback, autoPlay);
        }
    } else {
        LayerManager.stopLoading();

        pickResolProcess(function (optionData) {
            if (optionData === -1) {
                callback(false);
            } else if (optionData) {
                // 시청 가능한 옵션이 있으면 HDR 여부 확인 후 재생.
                checkPlayableResolProcess(optionData.resolName, function (res) {
                    if (res) {
                        playProcess(optionData);
                    } else {
                        callback(false);
                    }
                });
            } else {
                if (reqPathCd == "39") {
                    // Push 재생인데 시청 권한이 없는 경우, 다시 구매 로직을 타면 안되니 오류 처리한다.
                    HTool.openErrorPopup({
                        message: ["선택하신 VOD를 재생할 수 없습니다", "이용에 불편을 드려 죄송합니다"],
                        button: "닫기",
                        "title": "오류",
                        callback: function () {
                            callback(false);
                        }
                    });
                } else {
                    // 시청 가능한 옵션이 없으면 이용권 확인 후 결제 프로세스.
                    if (buttonObj.couponCount && buttonObj.couponCount > 0) {
                        openCouponCheckPopup(buttonObj, false, false, function (isCheckCoupon) {
                            if (isCheckCoupon == -1) {
                                callback(false);
                            } else {
                                LayerManager.startLoading({preventKey: true});
                                paymentProcess({isCheckCoupon: isCheckCoupon == 0});
                            }
                        });
                    } else if (extraParams && extraParams.otherCouponBtn && extraParams.otherCouponBtn.couponCount && extraParams.otherCouponBtn.couponCount > 0) {
                        // 타 구매수단 이용권 알림 팝업 (단편 선택했는데 시리즈 이용권이 있는 경우)
                        var tmp_optionList = extraParams.otherCouponBtn.seriesData.seriesList;
                        for (var i = 0; i < tmp_optionList.length; i++) {
                            tmp_optionList[i].seriesCouponData = extraParams.otherCouponBtn.optionList[i].seriesCouponData;
                        }

                        openCouponCheckPopup(extraParams.otherCouponBtn, true, true, function (isCheckCoupon) {
                            if (isCheckCoupon == -1) {
                                callback(false);
                            } else if (isCheckCoupon == 0) { // 타 구매수단 이용권 알림 팝업에서 확인 입력 시 그냥 결제 진행
                                LayerManager.startLoading({preventKey: true});
                                paymentProcess();
                            }
                            else { // 취소 입력 시 닫음.
                                callback(false);
                            }
                        });
                    } else {
                        LayerManager.startLoading({preventKey: true});
                        paymentProcess();
                    }
                }
            }
        });
    }

    function playProcess(optionData) {
        LayerManager.startLoading({preventKey: true});
        getLinkTimeData([optionData], function (res) {
            LayerManager.stopLoading();
            if (res) {
                if (!!bookmarkTime || linkTimeMap[optionData.assetId].linkTime - 0 > 0 || optionData.otmLinkTime - 0 > 0) {
                    openContinuePopup(optionData, linkTimeMap[optionData.assetId], function (result, isOtmPlay) {
                        if (result == -1) callback(false);
                        else {
                            linkTimeMap[optionData.assetId].linkTime = result;
                            returnData(optionData);
                        }
                    });
                } else returnData(optionData);
            } else onError("VODE-00013");
        });
    }

    function openCouponCheckPopup(vodInfo, isSeries, isOtherCoupon, callback) {
        LayerManager.stopLoading();
        LayerManager.activateLayer({
            obj: {
                id: "VodCouponCheckPopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    vodData: vodInfo,
                    isSeries: isSeries,
                    otherCoupon: isOtherCoupon,
                    callback: callback
                }
            },
            moduleId: "module.vod",
            new: true,
            visible: true
        });
    }

    function openContinuePopup(vodInfo, linkTimeInfo, callback) {
        LayerManager.stopLoading();
        if (!!noContinuePopup) {
            log.printInfo("No ContinuePopup!!!");
            callback(0);
            return;
        }

        log.printInfo("Activate ContinuePopup!!!");
        log.printInfo("bookmarkTime: " + bookmarkTime + ", linkTime:" + linkTimeInfo.linkTime + ", otmLinkTime:" + linkTimeInfo.otmLinkTime);
        LayerManager.activateLayer({
            obj: {
                id: "VodContinuePopup",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                params: {
                    assetId: vodInfo.assetId,
                    contsName: vodInfo.contsName,
                    runtime: vodInfo.data.runtime,
                    prInfo: vodInfo.data.prInfo,
                    resolCd: vodInfo.resolCd,
                    bookmarkTime: bookmarkTime,
                    linkTime: linkTimeInfo.linkTime,
                    otmLinkTime: vodInfo.otmLinkTime,
                    callback: function (res, isOtmPlay) {
                        log.printInfo("ContinuePopup ResTime : " + res);
                        callback(res, isOtmPlay);
                    }
                }
            },
            new: true,
            moduleId: "module.vod",
            visible: true
        });
    }

    function paymentProcess(option) {
        LayerManager.stopLoading();

        if (JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV)).ppvUseYn === "N") {
            HTool.openErrorPopup({
                message: ["고객님께서 가입하신 상품은", "유료 VOD를 이용하실 수 없습니다", "", "신청문의: 국번없이 100번"],
                button: "확인",
                "title": "결제",
                callback: function () {
                    callback(false);
                }
            });
        } else if (!catId || catId === "X") {
            // 카테고리 ID가 없거나 X로 들어오면 결제를 진행시키지 않는다.
            // X로 들어온 경우 시청을 위해 강제로 진입한 경우밖에 없으므로 구매할 수 없다.
            HTool.openErrorPopup({
                message: ERROR_TEXT.ET004, title: "알림", button: "확인", callback: function () {
                    callback(false);
                }
            });
        } else {
            LayerManager.activateLayer({
                obj: {
                    id: "PaymentVODPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        catId: catId,
                        optionList: optionList,
                        reqPathCd: reqPathCd,
                        isCheckCoupon: option ? !!option.isCheckCoupon : false,
                        callback: onPaymentComplete
                    }
                },
                new: true,
                visible: true,
                moduleId: "module.vod_payment"
            });
        }

        function onPaymentComplete(result, data) {
            if (result) {
                // 구매완료 시 구매정보 갱신을 위한 콜백 호출
                if (data.option.isSeries) {
                    if (updateCallback) updateCallback({buyType: "S", resolCd: data.option.resolCd});
                    var btn = new SingleButtonObject(ButtonObject.TYPE.NORMAL, buttonObj.optionList);
                    btn.setBuyYn(true, data.option.data[0].resolCd.indexOf("|") >= 0 ? data.option.resolCd : null);
                    prePlayProcessByButtonObj(btn, catId, reqPathCd, callback, noContinuePopup, bookmarkTime, authComplete, undefined, updateCallback, autoPlay);
                } else {
                    if (updateCallback) updateCallback({
                        buyType: "N",
                        resolCd: data.option.resolCd,
                        assetId: data.option.assetId
                    });

                    LayerManager.startLoading({preventKey: true});
                    getLinkTimeData([data.option], function (res) {
                        LayerManager.stopLoading();
                        if (res) {
                            if (!!bookmarkTime || linkTimeMap[data.option.assetId].linkTime - 0 > 0 || data.option.otmLinkTime - 0 > 0) {
                                openContinuePopup(data.option, linkTimeMap[data.option.assetId], function (result) {
                                    if (result == -1) {
                                        callback(false);
                                    } else {
                                        linkTimeMap[data.option.assetId].linkTime = result;
                                        returnData(data.option);
                                    }
                                });
                            } else returnData(data.option);
                        } else {
                            onError("VODE-00013");
                        }
                    });
                }
            } else onError();
        }
    }

    function pickResolProcess(callback) {
        var availabeList = [];
        for (var i in optionList) {
            if (!optionList[i].isImaginary && (optionList[i].vatPrice == 0 || optionList[i].buyYn || optionList[i].connerYn || optionList[i].pptBuyYn || optionList[i].otnYn)) availabeList.push(optionList[i]);
        }

        if (availabeList.length <= 1) {
            callback(availabeList[0]);
        } else if (autoPlay) {
            var defOption;
            var subDubOrder = ["", "0", "1"];
            var resolOrder = ["일반", "SD", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];
            for (var i in availabeList) {
                if (!defOption) defOption = availabeList[i];
                else if (defOption.resolName === availabeList[i].resolName) {
                    if (subDubOrder.indexOf(defOption.subtitleDubbed) < subDubOrder.indexOf(availabeList[i].subtitleDubbed)) defOption = availabeList[i];
                } else if (resolOrder.indexOf(defOption.resolName) < resolOrder.indexOf(availabeList[i].resolName)) defOption = availabeList[i];
            }
            callback(defOption);
        } else {
            var title;
            if (optionList[0].catData) title = optionList[0].catData.itemName || optionList[0].catData.catName;
            else title = optionList[0].contsName;
            LayerManager.activateLayer({
                obj: {
                    id: "VodPickResolutionPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        title: title,
                        list: availabeList,
                        callback: callback
                    }
                },
                new: true,
                visible: true,
                moduleId: "module.vod"
            });
        }
        return availabeList.length > 0;
    }

    function getAuthType(option) {
        var linkTimeInfo = linkTimeMap[option.assetId];
        if (linkTimeInfo.connerYn === "Y") {
            return "S";
        } else if (linkTimeInfo.pptBuyYn === "Y") {
            return "P";
        } else if (linkTimeInfo.buyYn === "Y") {
            return "P";
        } else if (option.otnYn) {
            return "O";
        } else /*if (option.price == 0)*/ {
            return "F";
        } // else return "F";
    }

    function getLinkTimeData(optionList, callback) {
        var linkTimeCnt = 0;
        for (var i = 0; i < optionList.length; i++) {
            if (!linkTimeMap.hasOwnProperty(optionList[i].assetId)) {
                linkTimeMap[optionList[i].assetId] = {};
                var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
                VODAmocManager.getLinkTimeNxt(function (result, data, assetId) {
                    if (result) {
                        linkTimeCnt++;
                        linkTimeMap[assetId] = data;
                        if (linkTimeCnt == optionList.length) callback(true);
                    } else {
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({
                            message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                            reboot: true,
                            callback: function () {
                                callback(false);
                            }
                        });
                        callback(false);
                    }
                }, DEF.SAID, optionList[i].assetId, custEnv ? custEnv.buyTypeYn : "Y", true, optionList[i].assetId);
            } else {
                linkTimeCnt++;
                if (linkTimeCnt == optionList.length) callback(true);
            }
        }
    }

    function getRecentlyData(option) {
        if (option.catData && (option.catData.itemType == 1 || option.catData.seriesYn == "Y")) {
            return {
                imgUrl: option.catData.imgUrl,
                contsName: option.catData.catName || option.catData.itemName,
                prInfo: option.catData.prInfo,
                catId: catId,
                contsId: catId,
                contYn: "N",    //시리즈:"N", 컨텐츠:"Y"
                runTime: option.data.runtime, //최종 재생시간
                wonYn: option.catData.wonYn,
                adultOnlyYn: option.data.adultOnlyYn,
                cCSersId: option.catData.cCSersId
            }
        } else {
            return {
                imgUrl: option.data.imgUrl,
                contsName: option.contsName,
                prInfo: option.data.prInfo,
                catId: catId,
                contsId: option.contsId,
                contYn: "Y",
                runTime: option.data.runtime, //최종 재생시간
                wonYn: option.data.wonYn,
                adultOnlyYn: option.data.adultOnlyYn
            }
        }
    }

    function onError(errorType) {
        LayerManager.stopLoading();
        switch (errorType) {
            case "VODE-00013":
                HTool.openErrorPopup({
                    message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                    reboot: true,
                    callback: function () {
                        callback(false);
                    }
                });
                break;
        }

        callback(false);
    }

    var kidsModule;

    function kidsModeCall(method) { // 키즈모듈 API호출
        kidsModule = ModuleManager.getModule("module.kids");
        if (!kidsModule) {
            ModuleManager.loadModule({
                moduleId: "module.kids",
                cbLoadModule: function (success) {
                    if (success) kidsModeCall(method);
                }
            });
        }
        else {
            kidsModule.execute({
                method: method,
                params: null
            });
        }
    }

    function returnData(option) {
        callback(true, {
            assetId: option.assetId,
            catId: catId,
            resolCd: option.resolCd,
            contsName: option.contsName || option.itemName,
            prInfo: option.data.prInfo,
            dispContsClass: option.data.dispContsClass,
            isHdrYn: option.data.isHdrYn,
            isMultiAud: option.isMultiAud,
            capUrl: option.capUrl,
            basicCaptViewYn: !!option.basicCaptViewYn,
            defAudio: option.data.defAudio,
            quickVodYn: option.data.quickVodYn,
            quickSvcCd: option.data.quickSvcCd,
            adultOnlyYn: option.data.adultOnlyYn,
            originData: option,
            getLinkTime: linkTimeMap[option.assetId],
            authType: getAuthType(option),
            recentlyData: getRecentlyData(option)
        });
    }
};

window.prePlayProcessForPreview = function (catId, optionList, optionData, extraParam) {
    log.printDbg(JSON.stringify(optionList));
    if (optionList.length > 1) {
        var availableCnt = 0;
        var availableOption;
        for (var i = 0; i < optionList.length; i++) {
            if (optionList[i].previewYn == "Y") {
                availableCnt++;
                availableOption = optionList[i];
            }
        }
        // 미리보기 가능한 화질이 여러개라면 화질선택 팝업을 노출하고, 그렇지 않다면 미리보기 가능한 화질로 재생함
        if (availableCnt > 1) {
            var title;
            if (optionList[0].catData) title = optionList[0].catData.itemName || optionList[0].catData.catName;
            else title = optionList[0].contsName;
            LayerManager.activateLayer({
                obj: {
                    id: "VodPickResolutionPopup",
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        title: title,
                        list: optionList,
                        isHighLight: optionList[0].previewStartTime,
                        callback: function (selectedOption) {
                            if (selectedOption != -1) requestPlay(selectedOption, extraParam);
                        }
                    }
                },
                new: true,
                visible: true,
                moduleId: "module.vod"
            });
        } else requestPlay(availableOption, extraParam);
    } else {
        requestPlay(optionList[0], extraParam);
    }

    function getRecentlyData(option) {
        if (option.catData && (option.catData.itemType == 1 || option.catData.seriesYn == "Y")) {
            return {
                imgUrl: option.catData.imgUrl,
                contsName: option.catData.catName || option.catData.itemName,
                prInfo: option.catData.prInfo,
                catId: catId,
                contsId: catId,
                contYn: "N",    //시리즈:"N", 컨텐츠:"Y"
                runTime: option.data.runtime, //최종 재생시간
                wonYn: option.catData.wonYn,
                adultOnlyYn: option.data.adultOnlyYn,
                cCSersId: option.catData.cCSersId
            }
        } else {
            return {
                imgUrl: option.data.imgUrl,
                contsName: option.contsName,
                prInfo: option.data.prInfo,
                catId: catId,
                contsId: option.contsId,
                contYn: "Y",
                runTime: option.data.runtime, //최종 재생시간
                wonYn: option.data.wonYn,
                adultOnlyYn: option.data.adultOnlyYn
            }
        }
    }

    function requestPlay(selectedOption, extraParam) {
        log.printDbg(JSON.stringify(selectedOption));
        VODManager.requestPlayHighLight({
            assetId: selectedOption.assetId,
            catId: catId,
            resolCd: selectedOption.resolCd,
            contsName: selectedOption.contsName || selectedOption.itemName,
            prInfo: selectedOption.prInfo,
            dispContsClass: selectedOption.data.dispContsClass,
            isHdrYn: selectedOption.data.isHdrYn,
            isMultiAud: selectedOption.isMultiAud,
            capUrl: selectedOption.capUrl,
            basicCaptViewYn: !!selectedOption.basicCaptViewYn,
            defAudio: selectedOption.data.defAudio,
            quickVodYn: selectedOption.data.quickVodYn,
            quickSvcCd: selectedOption.data.quickSvcCd,
            adultOnlyYn: selectedOption.data.adultOnlyYn,
            originData: selectedOption,
            getLinkTime: {linkTime: HTool.parseVODTime(selectedOption.previewStartTime) / 1000},
            previewRunTime: selectedOption.previewRunTime - 0,
            previewStartTime: HTool.parseVODTime(selectedOption.previewStartTime),
            previewEndTime: HTool.parseVODTime(selectedOption.previewStartTime) + (selectedOption.previewRunTime * 1000),
            authType: "Z",
            recentlyData: getRecentlyData(selectedOption),
            optionData: _$.extend({}, optionData),
            extraParam: extraParam
        });
    }
};