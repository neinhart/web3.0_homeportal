/**
 * Created by ksk91_000 on 2017-03-09.
 */
window.OptionListFactory = new function() {
    var _getOptionList = function(vodInfo, cateInfo, reqPathCd, callback, wishObject, oldButtonList) {
        log.printDbg("[OptListFact] getOptionList");

        if (!vodInfo) {
            return callback(false, "VODE-00017");
        }
        if (oldButtonList) {
            return _updateOptionList(vodInfo, cateInfo, reqPathCd, callback, wishObject, oldButtonList);
        }
        if (!Array.isArray(vodInfo)) {
            vodInfo = [vodInfo];
        }
        if (!reqPathCd) {
            reqPathCd = "01";
        }

        var buttonList = [];
        var dataList = _separateVodInfo(vodInfo, cateInfo);
        var buyTypeData = _getBuyTypeData(cateInfo);

        _getContStatusData(buyTypeData, cateInfo, function (res, contStatusData) {
            log.printDbg("[OptListFact] getContStatusData");

            if (res) {
                // 이용권 조회
                _getContCouponData(buyTypeData, dataList.assetList, function(res, couponData) {
                    if (res) {
                        // 이용권 조회 완료
                        if (couponData) {
                            console.debug("couponData = ", JSON.stringify(couponData, null, 4));
                        }
                        // 버튼 생성
                        _makeButtonProcess(buttonList, vodInfo, cateInfo, dataList, buyTypeData, contStatusData, couponData, reqPathCd, function (res) {
                            console.debug("buttonList = ", JSON.stringify(buttonList));
                            if (res) {
                                _checkMyContProcess(buyTypeData, buttonList, dataList, cateInfo, vodInfo, contStatusData, reqPathCd, wishObject, function (res) {
                                    var resList = [];
                                    for (var i in buttonList) {
                                        resList.push(buttonList[i]);
                                    }
                                    callback(res, resList);
                                });
                            } else {
                                callback(false);
                            }
                        });
                    }
                    else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    };

    /** 단품에 대한 OptionList 데이터 생성 */
    var _getSingleOption = function(vodInfo, cateInfo, reqPathCd, callback) {
        log.printDbg("[OptListFact] getSingleOption");

        if (!vodInfo) {
            return callback(false, "VODE-00017");
        }

        if (!Array.isArray(vodInfo)) {
            vodInfo = [vodInfo];
        }

        if (!reqPathCd) {
            reqPathCd = "01";
        }

        var buttonList = [];
        var dataList = _separateVodInfo(vodInfo, cateInfo);
        var buyTypeData = {single: true, series: false, corner: false, vodPlus: false};

        _getContCouponData(buyTypeData, dataList.assetList, function(res, couponData) {
            if (res) {
                _makeContentButton(buttonList, dataList, cateInfo, couponData, function (res) {
                    if (res) {
                        _checkMyContProcess(buyTypeData, buttonList, dataList, cateInfo, vodInfo, {
                            buyYn: "N",
                            connerYn: "N",
                            pptBuyYn: "N"
                        }, reqPathCd, null, function (res) {
                            var resList = [];
                            for (var i in buttonList) {
                                resList.push(buttonList[i]);
                            }
                            callback(res, resList);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    };

    var _getOptionListForPlay = function(vodInfo, cateInfo, reqPathCd, callback) {
        log.printDbg("[OptListFact] getOptionListForPlay");

        if (!vodInfo) {
            return callback(false, "VODE-00017");
        }
        if (!Array.isArray(vodInfo)) {
            vodInfo = [vodInfo];
        }
        if (!reqPathCd) {
            reqPathCd = "01";
        }

        var buttonList = [];
        var dataList = _separateVodInfo(vodInfo, cateInfo);
        var buyTypeData = _getBuyTypeData(cateInfo);

        var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
        var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";

        _getContStatusData(buyTypeData, cateInfo, function (res, contStatusData) {
            if (res) {
                _getContCouponData(buyTypeData, dataList.assetList, function (res, couponData) {
                    if (res) {
                        if (contStatusData.buyYn.split('|').indexOf("Y") > -1 || contStatusData.connerYn === "Y" || contStatusData.pptBuyYn == "Y" || hasFreeContents(dataList.optionList)) {
                            _makeContentButton(buttonList, dataList, cateInfo, couponData, function (res) {
                                var resList = [];
                                for (var i in buttonList) {
                                    // [kh.kim] buyTypeYn = Y인 경우 connerYn을 보고, N인경우 pptBuyYn을 본다
                                    if (buyTypeYn == "Y" && contStatusData.connerYn === "Y") {
                                        buttonList[i].setCornerYn(true);
                                    } else if (buyTypeYn == "N" && contStatusData.pptBuyYn === "Y") {
                                        buttonList[i].setPptBuyYn(true);
                                    } else {
                                        var buyList = contStatusData.buyYn.split('|');

                                        if (buyList.length === 1) {
                                            buttonList[i].setBuyYn(buyList[0]);
                                        } else {
                                            for (var j = 0; j < buyList.length; j++) {
                                                buttonList[i].setBuyYn(buyList[j], ["HD", "SD", "FHD", "UHD"][j])
                                            }
                                        }
                                    }
                                    resList.push(buttonList[i]);
                                }
                                callback(res, resList);
                            }); // end _makeContentButton
                        } else {
                            if (buyTypeData.vodPlus) {
                                buttonList.push(new VodPlusButtonObject(cateInfo, dataList.optionList));
                                callback(true, buttonList);
                            } else {
                                var tryCnt = 4, isFail = false;

                                if (buyTypeData.single) {
                                    _makeContentButton(buttonList, dataList, cateInfo, couponData, checkCallback);
                                } else {
                                    tryCnt--;
                                }

                                if (buyTypeData.series) {
                                    _makeSeriesButton(buttonList, dataList.optionList, cateInfo, vodInfo, couponData, null, checkCallback);
                                } else {
                                    tryCnt--;
                                }

                                if (buyTypeData.corner) {
                                    _makeConnerButton(buttonList, dataList.optionList, cateInfo, vodInfo, contStatusData, checkCallback);
                                } else {
                                    tryCnt--;
                                }

                                if (buyTypeData.vodPpt) {
                                    _makeVodPptButton(buttonList, dataList.optionList, cateInfo, vodInfo, contStatusData, checkCallback);
                                } else {
                                    tryCnt--;
                                }

                                if (tryCnt == 0) {
                                    _checkMyContProcess(buyTypeData, buttonList, dataList, cateInfo, vodInfo, contStatusData, reqPathCd, null, returnData);
                                }

                                function checkCallback(result) {
                                    if (!result) {
                                        isFail = true;
                                    }
                                    if (--tryCnt == 0) {
                                        if (!isFail && result) {
                                            _checkMyContProcess(buyTypeData, buttonList, dataList, cateInfo, vodInfo, contStatusData, reqPathCd, null, returnData)
                                        } else {
                                            callback(false);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        callback(false);
                    }
                }); // end _getContCouponData
            } else {
                callback(false, "VODE-00013");
            }
        });

        function returnData(res) {
            var resList = [];
            for (var i in buttonList) {
                resList.push(buttonList[i]);
            }
            callback(res, resList);
        }

        function hasFreeContents(optionList) {
            for (var i in optionList) {
                if (optionList[i].vatPrice == 0) {
                    return true;
                }
            }
            return false;
        }
    };

    /** 시리즈 회차에 대한 OptionList 데이터 생성 */
    var _getOptionListForSeries = function(vodInfo, cateInfo, reqPathCd, oldButtonList, callback, wishObject) {
        log.printDbg("[OptListFact] getOptionListForSeries");

        if (Array.isArray(oldButtonList)) {
            oldButtonList = getFirstObject(oldButtonList);

            function getFirstObject(list) {
                for (var i in list) {
                    return list[i];
                }
            }
        }

        //최초 요청인 경우
        if (!oldButtonList) {
            _getOptionList(vodInfo, cateInfo, reqPathCd, callback, wishObject);
            return;
        }

        if (!vodInfo) {
            return callback(false, "VODE-00017");
        }
        if (!Array.isArray(vodInfo)) {
            vodInfo = [vodInfo];
        }

        var buttonList = [];
        var dataList = _separateVodInfo(vodInfo, cateInfo);
        var buyTypeData = _getBuyTypeData(cateInfo);

        for (var i in oldButtonList) {
            if (oldButtonList[i].type !== ButtonObject.TYPE.NORMAL && oldButtonList[i].type !== ButtonObject.TYPE.DVD) {
                buttonList[oldButtonList[i].type] = _$.extend({}, oldButtonList[i]);
                buttonList[oldButtonList[i].type].optionList = dataList.optionList;
                buttonList[oldButtonList[i].type].updatePrInfo(dataList.optionList);
            }
        }

        // 시리즈 쿠폰 정보
        _getContCouponData(buyTypeData, dataList.assetList, function (res, couponData) {
            if (res) {
                // 시리즈 버튼에 쿠폰 정보 업데이트
                if (buyTypeData.series) {
                    buttonList[ButtonObject.TYPE.SERIES].couponCount = 0;
                    var seriesCoupon = couponData != null ? couponData.seriesCouponList : null;
                    var _optionList = buttonList[ButtonObject.TYPE.SERIES].optionList;
                    for (var idx = 0; seriesCoupon != null && idx < _optionList.length; idx++) {
                        if (seriesCoupon[_optionList[idx].resolCd]) {
                            var newResolCd = _convertResolCd(_optionList[idx].resolCd);
                            _optionList[idx].seriesCouponData = seriesCoupon[newResolCd];
                            buttonList[ButtonObject.TYPE.SERIES].couponCount = buttonList[ButtonObject.TYPE.SERIES].couponCount ?
                                buttonList[ButtonObject.TYPE.SERIES].couponCount + 1 : 1;

                            if (seriesCoupon[_optionList[idx].resolCd].couponCase == "SC") {
                                buttonList[ButtonObject.TYPE.SERIES].hasCouponSC = true;
                            }
                            else if (seriesCoupon[_optionList[idx].resolCd].couponCase == "SD") {
                                buttonList[ButtonObject.TYPE.SERIES].hasCouponSD = true;
                            }

                        }
                    }
                }

                // 시리즈 콘텐츠에 단편 구매 타입도 있는 경우 버튼 정보 해당 회차로 업데이트
                if (buyTypeData.single) {
                    _makeContentButton(buttonList, dataList, cateInfo, couponData, function (res) {
                        if (res) {
                            setCheckMyContent();
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    setCheckMyContent();
                }
            }
            else {
                callback(false);
            }
        });

        function setCheckMyContent() {
            log.printDbg("[OptListFact] setCheckMyContent");

            _checkOtmParing(buyTypeData, dataList, buttonList, cateInfo, function (res) {
                log.printDbg("[OptListFact] checkOtmParing res = " + res);

                if (res) {
                    if (buyTypeData.series || buyTypeData.corner || buyTypeData.vodPlus) {
                        if (buttonList[ButtonObject.TYPE.VODPLUS] && buttonList[ButtonObject.TYPE.VODPLUS].connerYn) {
                            for (var i in buttonList) {
                                buttonList[i].setCornerYn(true);
                            }
                            callCallbackFunc(true, buttonList);
                        } else if (buttonList[ButtonObject.TYPE.CORNER] && buttonList[ButtonObject.TYPE.CORNER].connerYn) {
                            for (var i in buttonList) {
                                buttonList[i].setCornerYn(true);
                            }
                            callCallbackFunc(true, buttonList);
                        } else if (buttonList[ButtonObject.TYPE.VODPPT] && buttonList[ButtonObject.TYPE.VODPPT].pptBuyYn) {
                            for (var i in buttonList) {
                                buttonList[i].setPptBuyYn(true);
                            }
                            callCallbackFunc(true, buttonList);
                        } else if (buttonList[ButtonObject.TYPE.SERIES] && buttonList[ButtonObject.TYPE.SERIES].buyYn) {
                            var isBuyAll = true;

                            if (buttonList[ButtonObject.TYPE.SERIES].isList) {
                                for (var j in buttonList[ButtonObject.TYPE.SERIES].seriesData.seriesList) {
                                    if (buttonList[ButtonObject.TYPE.SERIES].seriesData.seriesList[j].buyYn) {
                                        for (var i in buttonList) {
                                            buttonList[i].setBuyYn(true, buttonList[ButtonObject.TYPE.SERIES].seriesData.seriesList[j].resolCd);
                                        }
                                    } else {
                                        isBuyAll = false;
                                    }
                                }
                            } else {
                                for (var i in buttonList) {
                                    buttonList[i].setBuyYn(true);
                                }
                            }

                            /**
                             * [WEBIIIHOME-3656] 현재 단편이 포함된 통합 편성이고, 모든 시리즈 화질이 구매 안된 상태이면 checkMyContent 호출시 구매 여부까지 체크
                             */
                            if (cateInfo && cateInfo.cmbYn === "Y" && buyTypeData.single && !isBuyAll) {
                                callCheckMyContentImpl(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd,  wishObject);
                            }
                            else {
                                callCallbackFunc(true, buttonList);
                            }
                        } else if (buttonList[ButtonObject.TYPE.PACKAGE] && buttonList[ButtonObject.TYPE.PACKAGE].seriesBuyYn) {
                            if (buttonList[ButtonObject.TYPE.PACKAGE].seriesData.seriesList.length > 1) {
                                for (var j in buttonList[ButtonObject.TYPE.PACKAGE].seriesData.seriesList) {
                                    if (buttonList[ButtonObject.TYPE.PACKAGE].seriesData.seriesList[j].buyYn) {
                                        for (var i in buttonList) {
                                            buttonList[i].setBuyYn(true, buttonList[ButtonObject.TYPE.PACKAGE].seriesData.seriesList[j].resolCd);
                                        }
                                    }
                                }
                            } else {
                                for (var i in buttonList) {
                                    buttonList[i].setBuyYn(true);
                                }
                            }
                            callCallbackFunc(true, buttonList);
                        } else {
                            callCheckMyContentImpl(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd, wishObject);
                        }
                    } else {
                        callCheckMyContentImpl(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd, wishObject);
                    }
                } else {
                    callCallbackFunc(false);
                }
            });
        }

        function callCheckMyContentImpl(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd, wishObject) {
            _callCheckMyContent(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd, wishObject, function (res) {
                returnButtonList(res, buttonList);
            })
        }

        function callCallbackFunc(res, buttonList) {
            if (res) {
                _callWishCheckFlagFunc(wishObject, cateInfo, reqPathCd, function (res) {
                    returnButtonList(res, buttonList);
                });
            } else callback(false);
        }

        function returnButtonList(res, buttonList) {
            var resList = [];
            for (var i in buttonList) {
                resList.push(buttonList[i]);
            }
            callback(res, resList);
        }
    };

    var _updateBuyInfo = function (btnList, buyData, cateInfo) {
        log.printDbg("[OptListFact] updateBuyInfo");

        // 구매정보 변겅
        for (var i in btnList) {
            switch (buyData.buyType) {
                case "N":  // 단편결제
                    if (btnList[i].type === ButtonObject.TYPE.NORMAL || btnList[i].type === ButtonObject.TYPE.DVD) {
                        btnList[i].setBuyYn(true, buyData.resolCd, buyData.assetId);
                    }
                    break;
                case "S" :  // 시리즈 결제
                    if (cateInfo && cateInfo.contStatusData) {
                        setContStatusData(cateInfo.contStatusData, buyData.resolCd);
                    }
                    btnList[i].setBuyYn(true, buyData.resolCd);
                    break;
                case "C" :  // 코너 가입
                    if (cateInfo && cateInfo.contStatusData) {
                        cateInfo.contStatusData.connerYn = "Y";
                    }
                    btnList[i].setCornerYn(true);
                    break;
            }
        }

        function setContStatusData(contStatusData, resolCd) {
            log.printDbg("[OptListFact] setContStatusData");

            var buyList = contStatusData.buyYn.split('|');
            var res = "";
            buyList[["HD", "SD", "FHD", "UHD"].indexOf(resolCd)] = "Y";
            for (var j = 0; j < buyList.length; j++) {
                res += (res.length > 0 ? "|" : "") + buyList[j];
            }
            contStatusData.buyYn = res;
        }
    };

    var _updateOptionList = function (vodInfo, cateInfo, reqPathCd, callback, wishObject, oldButtonList) {
        log.printDbg("[OptListFact] updateOptionList");

        if (!vodInfo) {
            return callback(false, "VODE-00017");
        }
        if (!Array.isArray(vodInfo)) {
            vodInfo = [vodInfo];
        }
        if (!reqPathCd) {
            reqPathCd = "01";
        }

        var buttonList = [];
        var dataList = _separateVodInfo(vodInfo, cateInfo);
        var buyTypeData = _getBuyTypeData(cateInfo);

        for (var i in oldButtonList) {
            buttonList[oldButtonList[i].type] = oldButtonList[i];
        }

        delete cateInfo.contStatusData;
        delete cateInfo.seriesCheckShrContent;

        _getContStatusData(buyTypeData, cateInfo, function (res, contStatusData) {
            if (res) {
                if (buttonList[ButtonObject.TYPE.PACKAGE]) {
                    _makePackageButton(cateInfo, vodInfo, dataList.optionList, reqPathCd, function (res, btn) {
                        if (res) {
                            if (btn) {
                                if (buttonList[ButtonObject.TYPE.PACKAGE].seriesData) {
                                    btn.setSeriesData(buttonList[ButtonObject.TYPE.PACKAGE].seriesData);
                                }
                                buttonList[ButtonObject.TYPE.PACKAGE] = btn;
                            }
                            checkMyContProcessImpl(contStatusData);
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    checkMyContProcessImpl(contStatusData);
                }
            } else {
                callback(false);
            }
        });

        function checkMyContProcessImpl(contStatusData) {
            _checkMyContProcess(buyTypeData, buttonList, dataList, cateInfo, vodInfo, contStatusData, reqPathCd, wishObject, function (res) {
                var resList = [];
                for (var i in buttonList) {
                    resList.push(buttonList[i]);
                }
                callback(res, resList);
            });
        }
    };

    /** 버튼정보에 맞게 VOD 데이터 가공*/
    function _separateVodInfo(vodInfo, cateInfo) {
        log.printDbg("[OptListFact] separateVodInfo");

        var optionList = [];
        var assetList = [];
        var isSaleOptionApply = false;

        for (var i = 0; i < vodInfo.length; i++) {
            if (vodInfo[i].saleOptionYn == "Y") {
                isSaleOptionApply = true;
                break;
            }
        }

        for (var i = 0; i < Math.min(vodInfo.length, 12); i++) {
            //FIXME saleOption에 따라 노출/미노출 결정.
            if (!isSaleOptionApply || vodInfo[i].saleOptionYn != "N") {
                addToOptionList(optionList, assetList, vodInfo[i], cateInfo);
            }
        }

        function addToOptionList(optionList, assetList, vodInfo, cateInfo) {
            var resols = vodInfo.resolCd.split("|");
            var ltFlags = vodInfo.ltFlag.split("|");

            for (var i = 0; i < resols.length; i++) {
                if (resols[i].length > 0) {
                    var tmpList = [];

                    if (ltFlags[i] != 2) {
                        tmpList.push(getOptionData(i, resols[i], false));
                    }

                    if (ltFlags[i] != 0) {
                        tmpList.push(getOptionData(i, resols[i], true, ltFlags[i] == 1));
                    }

                    for (var j in tmpList) {
                        optionList.push(tmpList[j]);
                    }

                    assetList.push(new function () {
                        this.optionList = tmpList;
                        this.setData = function (key, value) {
                            for (var i in this.optionList) {
                                this.optionList[i][key] = value;
                            }
                        };
                        this.getData = function (key) {
                            return this.optionList[0][key];
                        };
                        this.getRepOption = function () {
                            return this.optionList[0];
                        };
                    });
                }
            }

            function getOptionData(index, resolCd, ltFlag, isImaginary) {
                var captViewYn, seamlessCode;
                if (vodInfo.basicCaptViewYn) {
                    if (vodInfo.basicCaptViewYn.indexOf("|") < 0) {
                        captViewYn = (vodInfo.basicCaptViewYn == "Y");
                    } else {
                        captViewYn = vodInfo.basicCaptViewYn.split("|")[index];
                    }
                } else {
                    captViewYn = false;
                }
                if (!vodInfo.cmbYn) {
                    vodInfo.cmbYn = cateInfo.cmbYn;
                }


                seamlessCode = vodInfo.seamlessCode;

                if (seamlessCode) {
                    /**
                     * 0: 심리스 불가능
                     * 1: 1:1 심리스
                     * 2: 1:N 심리스
                     */
                    try {
                        seamlessCode = seamlessCode.split("|")[(index || 0)];
                    } catch (e) {
                        seamlessCode = "0";
                    }
                } else if ((vodInfo.contsLinkProperty && vodInfo.contsLinkProperty.indexOf("C") >= 0) || vodInfo.mobileShareYn === "Y") {
                    // 기존 문구 표시
                    seamlessCode = "1";
                } else {
                    // 문구 미표시
                    seamlessCode = "0";
                }

                var optionData = {
                    data: vodInfo,
                    catData: cateInfo,
                    resolIdx: index,
                    resolCd: resolCd,
                    contsName: vodInfo.contsName || vodInfo.itemName,
                    contsId: vodInfo.contsId || vodInfo.itemId,
                    ltFlag: ltFlag,
                    price: ltFlag ? vodInfo.ltPrice.split("|")[index] : vodInfo.price.split("|")[index],
                    vatPrice: ltFlag ? vodInfo.vatLtPrice.split("|")[index] : vodInfo.vatPrice.split("|")[index],
                    assetId: (vodInfo.assetId || vodInfo.contsId || vodInfo.itemId).split("|")[index],
                    period: ltFlag ? (vodInfo.ltPeriod ? vodInfo.ltPeriod.split("|")[index] : "") : (vodInfo.period ? vodInfo.period.split("|")[index] : vodInfo.viewTime ? vodInfo.viewTime.split("|")[index] : ""),
                    mobile: vodInfo.contsLinkProperty.indexOf("C") > -1,
                    ltName: ltFlag ? "장기" : "",
                    capUrl: vodInfo.capUrl.split("|")[index],
                    isMultiAud: vodInfo.isMultiAud,
                    basicCaptViewYn: captViewYn,
                    isImaginary: !!isImaginary,
                    isContCoupon: vodInfo.contsLinkProperty.indexOf("A") > -1, // 콘텐츠 이용권
                    voucherYn: cateInfo.voucherYn ? cateInfo.voucherYn.split("|")[index] : "N",  // 시리즈 이용권
                    previewYn: vodInfo.previewYn.split("|")[index] || "N", // 미리보기
                    previewRunTime: vodInfo.previewRunTime.split("|")[index] || "0", // 미리보기 재생 시간
                    previewStartTime: vodInfo.previewStartTime.split("|")[index] || "00:00:00", // 미리보기 시작 시간
                    seamlessCode: seamlessCode
                };

                return new ContentOptionObject(
                    optionData.data,
                    optionData.catData,
                    optionData.resolIdx,
                    optionData.resolCd,
                    optionData.contsName,
                    optionData.contsId,
                    optionData.ltFlag,
                    optionData.price,
                    optionData.vatPrice,
                    optionData.period,
                    optionData.mobile,
                    optionData.assetId,
                    optionData.ltName,
                    optionData.capUrl,
                    optionData.isMultiAud,
                    optionData.basicCaptViewYn,
                    optionData.isImaginary,
                    optionData.isContCoupon, // 콘텐츠 이용권
                    optionData.voucherYn,  // 시리즈 이용권
                    optionData.previewYn, // 미리보기
                    optionData.previewRunTime, // 미리보기 재생 시간
                    optionData.previewStartTime, // 미리보기 시작 시간
                    optionData.seamlessCode
                );
            }
        }

        _sortOptionList(optionList);
        return {optionList: optionList, assetList: assetList};
    }

    /** 버튼 타입에 맞게 종류 결정 */
    function _getBuyTypeData(cateInfo) {
        log.printDbg("[OptListFact] getBuyTypeData buyType = " + cateInfo.buyType + ", bizBuyType = " + cateInfo.bizBuyType);

        // buyTypeYn 에 따라 buyType을 사용할 지, bizBuyType을 사용할 지 결정
        // Y : buyType 사용
        // N : bizBuyType 사용
        var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
        var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";
        var buyType = buyTypeYn == "Y" ? cateInfo.buyType : cateInfo.bizBuyType;

        var buyTypeData = {
            single: false, series: false, corner: false, vodPlus: false, vodPpt: false
        };

        if (buyType == null || buyType == BUY_TYPE.SERIES_SINGLE || buyType == BUY_TYPE.SINGLE || buyType == BUY_TYPE.CORNER_SINGLE || buyType == BUY_TYPE.CORNER_SERIES_SINGLE
            || buyType == BUY_TYPE.VODPPT_SINGLE || buyType == BUY_TYPE.VODPPT_SERIES_SINGLE) { //건별
            buyTypeData.single = true;
        }
        if (buyType == BUY_TYPE.SERIES || buyType == BUY_TYPE.SERIES_SINGLE || buyType == BUY_TYPE.CORNER_SERIES || buyType == BUY_TYPE.CORNER_SERIES_SINGLE
            || buyType == BUY_TYPE.VODPPT_SERIES || buyType == BUY_TYPE.VODPPT_SERIES_SINGLE) { //묶음
            buyTypeData.series = true;
        }
        if (buyType == BUY_TYPE.CORNER || buyType == BUY_TYPE.CORNER_SINGLE || buyType == BUY_TYPE.CORNER_SERIES || buyType == BUY_TYPE.CORNER_SERIES_SINGLE) { //코너
            buyTypeData.corner = true;
        }
        if (buyType == BUY_TYPE.VODPPT || buyType == BUY_TYPE.VODPPT_SINGLE || buyType == BUY_TYPE.VODPPT_SERIES || buyType == BUY_TYPE.VODPPT_SERIES_SINGLE) { //VOD 기간정액
            buyTypeData.vodPpt = true;
        }
        if (buyType == BUY_TYPE.VODPLUS) {
            buyTypeData.vodPlus = true;
        }
        return buyTypeData;
    }

    /**
     * 종류에 맞는 버튼 생성
     * make XXX button 호출
     * */
    function _makeButtonProcess(buttonList, vodInfo, cateInfo, dataList, buyTypeData, contStatusData, couponData, reqPathCd, callback) {
        log.printDbg("[OptListFact] makeButtonProcess");

        // 어떤 버튼을 생성할지 체크
        if (buyTypeData.vodPlus) {
            buttonList.push(new VodPlusButtonObject(cateInfo, dataList.optionList));
            callback(true, buttonList);
        } else {
            // 버튼 패키지 -> 단편 -> 시리즈 -> 월정액 순으로 체크
            _makePackageButton(cateInfo, vodInfo, dataList.optionList, reqPathCd, function(res, btn) {
                if(res) {
                    if (btn) {
                        buttonList[ButtonObject.TYPE.PACKAGE] = btn;
                    }

                    var tryCnt = 4, isFail = false, isApply = false;

                    if (buyTypeData.single) {
                        _makeContentButton(buttonList, dataList, cateInfo, couponData, checkCallback);
                    } else {
                        tryCnt--;
                    }

                    if (buyTypeData.series) {
                        _makeSeriesButton(buttonList, dataList.optionList, cateInfo, vodInfo, couponData, btn, checkCallback);
                    } else {
                        tryCnt--;
                    }

                    if (buyTypeData.corner) {
                        _makeConnerButton(buttonList, dataList.optionList, cateInfo, vodInfo, contStatusData, checkCallback);
                    } else {
                        tryCnt--;
                    }

                    if (buyTypeData.vodPpt) {
                        _makeVodPptButton(buttonList, dataList.optionList, cateInfo, vodInfo, contStatusData, checkCallback);
                    } else {
                        tryCnt--;
                    }

                    if (tryCnt == 0 && !isApply) {
                        isApply = true;
                        callback(true, buttonList);
                    }

                    function checkCallback(result) {
                        if (!result) {
                            isFail = true;
                        }

                        if (--tryCnt == 0 && !isApply) {
                            isApply = true;
                            callback(!isFail && result, buttonList);
                        }
                    }
                } else {
                    callback(false);
                }
            });
        }
    }

    /** 패키지 버튼 생성 */
    function _makePackageButton(cateInfo, vodInfo, optionList, reqPathCd, callback) {
        log.printDbg("[OptListFact] makePackageButton type = " + cateInfo.itemType);

        var seriesYn, contsId = "";
        if ((cateInfo.itemType == 1 || cateInfo.seriesYn === "Y") && cateInfo.seriesType !== "02") {
            seriesYn = "Y";
            contsId = "";
        } else {
            seriesYn = "N";
            for (var i = 0; i < vodInfo.length; i++) {
                contsId += "|" + (vodInfo[i].contsId || vodInfo[i].itemId);
            }
            contsId = contsId.substr(1, contsId.length - 1);
        }
        VODAmocManager.getMyPkgVodListW3(function (res, data) {
            if (res) {
                if (data && !!data.pkgVodPrdCdList) var packageButton = new PackageButtonObject(data, optionList);
                callback(true, packageButton);
            } else {
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                callback(false);
            }
        }, seriesYn, contsId, cateInfo.catId || cateInfo.itemId, reqPathCd);
    }

    /** 시리즈 버튼 생성 */
    function _makeSeriesButton(buttonList, optionList, cateInfo, vodInfo, couponData, btn, callback) {
        log.printDbg("[OptListFact] makeSeriesButton btn = " + btn);

        if (btn) {
            btn.setSeriesData(new SeriesObject(cateInfo, vodInfo));
        } else {
            buttonList[ButtonObject.TYPE.SERIES] = new SeriesButtonObject(cateInfo, vodInfo, optionList);
        }

        // 시리즈 이용권 정보 셋팅
        var seriesCoupon = couponData ? couponData.seriesCouponList : null;
        var _optionList = buttonList[ButtonObject.TYPE.SERIES].optionList;
        for(var idx = 0; seriesCoupon && idx < _optionList.length; idx++) {
            if (seriesCoupon[_optionList[idx].resolCd]) {
                var newResolCd = _convertResolCd(_optionList[idx].resolCd);
                _optionList[idx].seriesCouponData = seriesCoupon[newResolCd];
                buttonList[ButtonObject.TYPE.SERIES].couponCount = buttonList[ButtonObject.TYPE.SERIES].couponCount ?
                    buttonList[ButtonObject.TYPE.SERIES].couponCount + 1 : 1;

                if (seriesCoupon[_optionList[idx].resolCd].couponCase == "SC") {
                    buttonList[ButtonObject.TYPE.SERIES].hasCouponSC = true;
                }
                else if (seriesCoupon[_optionList[idx].resolCd].couponCase == "SD") {
                    buttonList[ButtonObject.TYPE.SERIES].hasCouponSD = true;
                }
            }
        }

        callback(true);
    }

    /** 월정액 버튼 생성 */
    function _makeConnerButton(buttonList, optionList, cateInfo, vodInfo, contStatusData, callback) {
        log.printDbg("[OptListFact] makeConnerButton type = " + cateInfo.buyType);

        if(cateInfo.buyType != BUY_TYPE.CORNER && contStatusData.connerYn==="Y") { // 가입중 버튼
            buttonList[ButtonObject.TYPE.CORNER] = new CornerButtonObject([], optionList, cateInfo.buyType==BUY_TYPE.CORNER);
            callback(true);
        } else {
            VODAmocManager.getCornerList(function(result, data) {
                if (result) {
                    var cornerList = data.cornerList;

                    if (cornerList == null) {
                        cornerList = [];
                    } else if (!Array.isArray(cornerList)) {
                        cornerList = [cornerList];
                    }

                    if (cornerList.length > 0) {
                        buttonList[ButtonObject.TYPE.CORNER] = new CornerButtonObject(cornerList, optionList, cateInfo.buyType==BUY_TYPE.CORNER);
                    } else {
                        buttonList[ButtonObject.TYPE.CORNER] = new NoCornerButtonObject(optionList);
                    }

                    callback(true);
                } else {
                    callback(false);
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                }
            }, cateInfo.catId || cateInfo.itemId, DEF.SAID);
        }
    }

    /** 기간정액 버튼 생성 */
    function _makeVodPptButton(buttonList, optionList, cateInfo, vodInfo, contStatusData, callback) {
        log.printDbg("[OptListFact] _makeVodPptButton bizBuyType = " + cateInfo.bizBuyType);

        if(cateInfo.buyType != BUY_TYPE.VODPPT && contStatusData.pptBuyYn == "Y") { // 가입중 버튼
            buttonList[ButtonObject.TYPE.VODPPT] = new VodPptButtonObject([], optionList, cateInfo.buyType==BUY_TYPE.VODPPT);
            buttonList[ButtonObject.TYPE.VODPPT].setPptBuyYn(true);
            callback(true);
        } else {
            var reqData = {
                catId : cateInfo.catId || cateInfo.itemId,
                saId : DEF.SAID
            };
            // 기간정액 상품 리스트 조회
            VODAmocManager.getCatPptList(reqData, function(result, data) {
                if (result) {
                    var cornerList = data.cornerList;
//                    if (test) {
//                        cornerList = [
//                            {
//                                "pptCd": "PCTT04",
//                                "pptTitle": "PCTT04",
//                                "pptName": "지상파 무제한 즐기기(TEST) 1",
//                                "unifiedPptYn": "Y",
//                                "pptPrice": "1000|2000|3000|4000",
//                                "vatPptPrice": "1100|2200|3300|4400",
//                                "pptPeriod": "24|72|168|720",
//                                "ltFlag": "0",
//                                "pptCatId": "10000000000000172131"
//                            },
//                            {
//                                "pptCd": "PCTT03",
//                                "pptTitle": "PCTT03",
//                                "pptName": "KBS 무제한 즐기기 (TEST) 1",
//                                "unifiedPptYn": "N",
//                                "pptPrice": "1000",
//                                "vatPptPrice": "1100",
//                                "pptPeriod": "24",
//                                "ltFlag": "0",
//                                "pptCatId": "10000000000000172132"
//                            }
//                        ];
//                    }

                    if (cornerList == null) {
                        cornerList = [];
                    } else if (!Array.isArray(cornerList)) {
                        cornerList = [cornerList];
                    }

                    for (var i in cornerList) {
                        cornerList[i]["catId"] = cateInfo.catId;
                        cornerList[i]["contsId"] = optionList.contsId;
                    }

                    if (cornerList.length > 0) {
                        buttonList[ButtonObject.TYPE.VODPPT] = new VodPptButtonObject(cornerList, optionList, cateInfo.buyType==BUY_TYPE.VODPPT);
                    } else {
                        buttonList[ButtonObject.TYPE.VODPPT] = new NoVodPptButtonObject(optionList);
                    }

                    buttonList[ButtonObject.TYPE.VODPPT].setPptBuyYn(contStatusData.pptBuyYn == "Y");
                    callback(true);
                } else {
                    callback(false);
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                }
            });
        }
    }

    /** 단편 버튼 생성 */
    function _makeContentButton(buttonList, dataList, cateInfo, couponData, callback) {
        log.printDbg("[OptListFact] makeContentButton");

        var normalOptionList = [], dvdOptionList = [];
        var optionList = dataList.optionList;
        var assetList = dataList.assetList;

        for (var i = 0; i < optionList.length; i++) {
            if (optionList[i].data.isDvdYn == "Y") {
                dvdOptionList.push(optionList[i]);
            } else {
                normalOptionList.push(optionList[i]);
            }
        }

        _sortOptionList(normalOptionList);
        _sortOptionList(dvdOptionList);

        var contCoupon = couponData ? couponData.contCouponList : null;

        if (normalOptionList.length > 0) {
            buttonList[ButtonObject.TYPE.NORMAL] = new SingleButtonObject(ButtonObject.TYPE.NORMAL, normalOptionList);
        }

        if (dvdOptionList.length > 0) {
            buttonList[ButtonObject.TYPE.DVD] = new SingleButtonObject(ButtonObject.TYPE.DVD, dvdOptionList);
        }

        // 이전 프로세스에서 조회한 콘텐츠 이용권 정보를 버튼에 저장
        for (var idx = 0; contCoupon && idx < assetList.length; idx++) {
            var _assetId = assetList[idx].getRepOption().assetId;
            console.log("contCoupon[" + _assetId + "] ", JSON.stringify(contCoupon[_assetId], null, 4));

            if (contCoupon[_assetId]) {
                assetList[idx].setData("couponData", contCoupon[_assetId]);

                if (assetList[idx].getData("data").isDvdYn == "Y") { // 소장용 버튼
                    buttonList[ButtonObject.TYPE.DVD].couponCount = buttonList[ButtonObject.TYPE.DVD].couponCount ?
                        buttonList[ButtonObject.TYPE.DVD].couponCount + 1 : 1;

                    if (contCoupon[_assetId].couponCase == "CC") { //이용권
                        buttonList[ButtonObject.TYPE.DVD].hasCouponCC = true;
                    }
                    else if (contCoupon[_assetId].couponCase == "CD") { //할인권
                        buttonList[ButtonObject.TYPE.DVD].hasCouponCD = true;
                    }
                }
                else { // 일반 단편 버튼
                    buttonList[ButtonObject.TYPE.NORMAL].couponCount = buttonList[ButtonObject.TYPE.NORMAL].couponCount ?
                        buttonList[ButtonObject.TYPE.NORMAL].couponCount + 1 : 1;

                    if (contCoupon[_assetId].couponCase == "CC") { //이용권
                        buttonList[ButtonObject.TYPE.NORMAL].hasCouponCC = true;
                    }
                    else if (contCoupon[_assetId].couponCase == "CD") { //할인권
                        buttonList[ButtonObject.TYPE.NORMAL].hasCouponCD = true;
                    }
                }
            }
        }

        callback(true);
    }

    /** 버튼 정렬 */
    function _sortOptionList(list) {
        log.printDbg("[OptListFact] sortOptionList");

        var resolOrder = ["일반", "SD", "HD", "FHD", "UHD", "HDR", "3D", "와이드"];

        list.sort(function (a, b) {
            var resol = resolOrder.indexOf(a.resolName) - resolOrder.indexOf(b.resolName);

            if (resol != 0) {
                return resol;
            }
            var subtitle = (a.data.subtitleDubbed ? a.data.subtitleDubbed - 0 : -1) - (b.data.subtitleDubbed ? b.data.subtitleDubbed - 0 : -1);

            if (subtitle != 0) {
                return subtitle;
            }
            return a.ltFlag ? 1 : -1;
        });
    }

    /** [구매체크] 시리즈 카테고리 구매정보 확인 */
    function _getContStatusData(buyTypeData, cateInfo, callback) {
        log.printDbg("[OptListFact] getContStatusData");

        if (cateInfo.contStatusData) {
            callback(true, cateInfo.contStatusData);
        } else if (buyTypeData.series || buyTypeData.corner || buyTypeData.vodPlus || buyTypeData.vodPpt) {
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            VODAmocManager.getContStatusNxt(function (res, data) {
                if (res) {
                    cateInfo.contStatusData = data;
                    callback(res, data);
                } else {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                    callback(false);
                }
            }, DEF.SAID, cateInfo.catId || cateInfo.itemId, cateInfo.cmbYn, custEnv ? custEnv.buyTypeYn : "Y");
        } else {
            cateInfo.contStatusData = {buyYn: "N", connerYn: "N", pptBuyYn: "N"};
            callback(true, cateInfo.contStatusData);
        }
    }

    /** [구매체크] 구매, 월정액, 찜목록 체크하기 위한 메인 로직 */
    function _checkMyContProcess(buyTypeData, buttonList, dataList, cateInfo, vodInfo, contStatusData, reqPathCd, wishObject, callback) {
        log.printDbg("[OptListFact] checkMyContProcess");

        var ignoreCheckMyCont = false;

        if (buyTypeData.series || buyTypeData.corner || buyTypeData.vodPlus || buyTypeData.vodPpt) {
            // [kh.kim] buyTypeYn = Y인 경우 connerYn을 보고, N인경우 pptBuyYn을 본다
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";
            var resolCd = cateInfo.resolCd.split("|");
            var buyYn = contStatusData.buyYn.split("|");
            var connerYn = buyTypeYn == "Y" ? contStatusData.connerYn : "N";
            var pptBuyYn = buyTypeYn == "N" ? contStatusData.pptBuyYn : "N";
            // var multiDevBuyYn = data.multiDevBuyYn.split("|");
            // var multiDevConnerYn = data.multiDevConnerYn.split("|");

            if (connerYn === "Y" || pptBuyYn === "Y" || buyYn.indexOf("Y") >= 0) {
                for (var i in buttonList) {
                    if (connerYn === "Y") {
                        buttonList[i].setCornerYn(true);
                        ignoreCheckMyCont = true;
                    }
                    if (pptBuyYn === "Y") {
                        buttonList[i].setPptBuyYn(true);
                        ignoreCheckMyCont = true;
                    }
                    if (cateInfo.cmbYn === "Y") {
                        var tmpIgnoreFlag = true;
                        for (var j = 0; j < resolCd.length; j++) {
                            if (buyYn[["HD", "SD", "FHD", "UHD"].indexOf(resolCd[j])] === "Y") {
                                buttonList[i].setBuyYn(true, (resolCd[j]));
                            } else if (buyYn[["HD", "SD", "FHD", "UHD"].indexOf(resolCd[j])] === "N") {
                                tmpIgnoreFlag = false;
                            }
                        }
                        ignoreCheckMyCont = ignoreCheckMyCont || tmpIgnoreFlag;
                    } else if (buyYn[0] === "Y") {
                        buttonList[i].setBuyYn(true);
                        ignoreCheckMyCont = true;
                    }
                }
            // [17.10.23] 단편이 없는 경우에도 월정액/시리즈 구매가 되지 않은 경우 checkMyContentW3 호출하도록 수정
            // } else if (!buyTypeData.vodPlus && !(buyTypeData.single || cateInfo.buyType==1)) { // 시리즈 Only인 경우에도 CheckMyCont 호출
            //     ignoreCheckMyCont = true;
            }
        } else if(!buyTypeData.single) {
            ignoreCheckMyCont = true;
        }

        /** 심리스 OTM 페어링 체크 */
        _checkOtmParing(buyTypeData, dataList, buttonList, cateInfo, function (res) {
            log.printDbg("[OptListFact] checkOtmParing");

            if (res) {
                if (ignoreCheckMyCont) {
                    callCallbackFunc(true);
                } else {
                    _callCheckMyContent(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd, wishObject, callback);
                }
            } else {
                callback(false);
            }
        });

        /** 찜여부를 조회한 뒤 Process 종료 */
        function callCallbackFunc(res) {
            if (res) {
                _callWishCheckFlagFunc(wishObject, cateInfo, reqPathCd, function () {
                    callback(res);
                });
            } else {
                callback(res);
            }
        }
    }

    /** 찜하기 여부리스트 체크 콜백 */
    function _callWishCheckFlagFunc(wishObject, cateInfo, reqPathCd, callback) {
        log.printDbg("[OptListFact] callWishCheckFlagFunc");

        if(wishObject) {
            var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
            var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";
            VODAmocManager.checkMyContentW3(wishObject.contsId, cateInfo.catId || cateInfo.itemId, "H", reqPathCd, "A", buyTypeYn, function (result, data) {
                if (result) {
                    wishObject.playCheckFlag = data.playCheckList === "Y";
                }
                callback(result);
            });
        } else {
            callback(true);
        }
    }

    /** [구매체크] checkMyCont 호출 함수 */
    function _callCheckMyContent(buyTypeData, dataList, buttonList, cateInfo, vodInfo, reqPathCd, wishObject, callback) {
        log.printDbg("[OptListFact] callCheckMyContent");

        var assetList = dataList.assetList;
        var constIds = "";
        var mainContIdx = -1;

        for (var i = 0; i < assetList.length && i < 12; i++) {
            constIds += (constIds.length > 0 ? "|" : "") + assetList[i].getData("assetId");

            if (!!wishObject && wishObject.contsId === assetList[i].getData("assetId")) {
                mainContIdx = i;
            }
        }

        if (wishObject && mainContIdx === -1) {
            if (assetList.length >= 12) {
                _callWishCheckFlagFunc(wishObject, cateInfo, reqPathCd, function () {
                });
            } else {
                constIds += (constIds.length > 0 ? "|" : "") + wishObject.contsId;
                mainContIdx = assetList.length;
            }
        }
        var custEnv = JSON.parse(StorageManager.ps.load(StorageManager.KEY.CUST_ENV));
        var buyTypeYn = custEnv ? custEnv.buyTypeYn : "Y";
        VODAmocManager.checkMyContentW3(constIds, cateInfo.catId || cateInfo.itemId, "H", reqPathCd, "B", buyTypeYn, function(result, data) {
            if (result) {
                var buyCheckList = data.buyCheckList.split("|");
                var buyingDateList = data.buyingDateList.split("|");
                var connerYn = data.connerYn.split("|");
                var pptBuyYn = data.pptBuyYn.split("|");
                var playCheckList = data.playCheckList.split("|");
                var buyFlagList = data.buyFlagList.split("|");
                // var previewYnList = data.previewYnList.split("|");
                // var previewStartTimeList = data.previewStartTimeList.split("|");
                // var previewRunTimeList = data.previewRunTimeList.split("|");
                // var multiDevBuyCheckList = data.multiDevBuyCheckList.split("|");
                // var multiDevConnerYn = data.multiDevConnerYn.split("|");
                var contStatusData = assetList[0].optionList[0].catData.contStatusData;
                var seriesBuyYn = contStatusData ? contStatusData.buyYn.split("|") : "";
                for (var i = 0; i < assetList.length; i++) {
                    if (!assetList[i].optionList[0].buyYn) { // 해당 화질의 시리즈 구매가 안 된 경우에만...
                        if (buyCheckList[i] === "Y") {
                            var type = assetList[i].getData("data").isDvdYn === "Y" ? ButtonObject.TYPE.DVD : ButtonObject.TYPE.NORMAL;

                            // seriesOnly인데 단편 구매가 된 경우 단편버튼 추가 (재생용)
                            if (!buyTypeData.single) {
                                if (buttonList[type]) {
                                    if (!hasAsset(buttonList[type], assetList[i])) buttonList[type].optionList.push(assetList[i].getRepOption());
                                } else {
                                    buttonList[type] = new SingleButtonObject(type, [assetList[i].getRepOption()]);
                                    buttonList[type].isOnlyView = true;
                                }
                            }

                            if (buttonList[type]) buttonList[type].setBuyYn(true, assetList[i].getData("resolCd"), assetList[i].getData("assetId"));
                            assetList[i].setData("buyYn", true);
                        }
                        // [kh.kim] buyTypeYn = Y인 경우 connerYn을 보고, N인경우 pptBuyYn을 본다
                        if (buyTypeYn == "Y" && connerYn[i] === "Y") {
                            var type = assetList[i].getData("data").isDvdYn === "Y" ? ButtonObject.TYPE.DVD : ButtonObject.TYPE.NORMAL;

                            if (!buyTypeData.single) {
                                if (buttonList[type]) {
                                    if (!hasAsset(buttonList[type], assetList[i])) buttonList[type].optionList.push(assetList[i].getRepOption());
                                } else {
                                    buttonList[type] = new SingleButtonObject(type, [assetList[i].getRepOption()]);
                                    buttonList[type].isOnlyView = true;
                                }
                            }

                            if (buttonList[type]) {
                                buttonList[type].setCornerYn(true, assetList[i].getData("assetId"));
                            }
                            assetList[i].setData("connerYn", true);
                        }

                        if (buyTypeYn == "N" && pptBuyYn[i] === "Y") {
                            var type = assetList[i].getData("data").isDvdYn === "Y" ? ButtonObject.TYPE.DVD : ButtonObject.TYPE.NORMAL;

                            if (!buyTypeData.single) {
                                if (buttonList[type]) {
                                    if (!hasAsset(buttonList[type], assetList[i])) buttonList[type].optionList.push(assetList[i].getRepOption());
                                } else {
                                    buttonList[type] = new SingleButtonObject(type, [assetList[i].getRepOption()]);
                                    buttonList[type].isOnlyView = true;
                                }
                            }

                            if (buttonList[type]) {
                                buttonList[type].setPptBuyYn(true, assetList[i].getData("assetId"));
                            }
                            assetList[i].setData("pptBuyYn", true);
                        }
                    }

                    // var ltFlag = assetList[i].getData("ltFlag");
                    assetList[i].setData("checkContentData", {
                        buyYn: buyCheckList[i],
                        buyingDate: buyingDateList[i],
                        connerYn: buyTypeYn == "Y" ? connerYn[i] : "N",
                        pptBuyYn: buyTypeYn == "N" ? pptBuyYn[i] : "N",
                        playCheck: playCheckList[i],
                        buyFlag: false//ltFlag ? (buyFlagList[i] == 1 || buyFlagList[i] == 2) : (buyFlagList[i] == 0 || buyFlagList[i] == 1)
                        // previewYnList: previewYnList[i],
                        // previewStartTimeList: previewStartTimeList[i],
                        // previewRunTimeList: previewRunTimeList[i],
                        // multiDevBuyCheckList: multiDevBuyCheckList[i],
                        // multiDevConnerYn: multiDevConnerYn[i]
                    })
                }

                if (!!wishObject && mainContIdx >= 0) {
                    wishObject.playCheckFlag = playCheckList[mainContIdx] === "Y";
                }

                callback(result);

            } else {
                extensionAdapter.notifySNMPError("VODE-00013");
                HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
                callback(false);
            }
        });

        function hasAsset(button, asset) {
            var assetId = asset.getData("assetId");
            for (var i in button.optionList) {
                if (button.optionList[i].assetId == assetId) {
                    return true;
                }
            }
            return false;
        }
    }

    /** [구매체크] OTM 페어링 체크 */
    function _checkOtmParing(buyTypeData, dataList, buttonList, cateInfo, callback) {
        log.printDbg("[OptListFact] checkOtmParing");

        if (StorageManager.ps.load(StorageManager.KEY.OTN_PARING) == "Y") {
            var assetList = dataList.assetList;
            var contentList = [], contentIdList = "";

            for (var idx = 0; idx < assetList.length; idx++) {
                contentList[assetList[idx].getData("assetId")] = assetList[idx];
            }

            for(var i in contentList) {
                contentIdList += i + "|";
            }

            if (contentIdList.length == 0) {
                return callback(true);
            }

            contentIdList = contentIdList.substr(0, contentIdList.length - 1);

            /** [구매체크] 복수 단말, OTV 프라임, OTM 구매 체크*/

            if (buyTypeData.series) {
                _getSeriesCheckShrContentData(cateInfo, function(result, data) {
                    // OTM 시리즈 구매여부 처리
                    if (result) {
                        cateInfo.seriesCheckShrContent = data;
                        var omtBuyList = data.otmBuyYn.split("|");
                        var otmLinkTimeList = data.otmLinkTime.split("|");
                        var otmBuyingDateList = data.buyingDate.split("|");
                        var resolCd = cateInfo.resolCd.split("|");
                        var ignoreCheckShrContent = false;

                        // 시리즈 버튼에 otn 구매 처리
                        if (data.otmBuyYn.indexOf("Y") >= 0) {
                            if (cateInfo.cmbYn === "Y") {
                                var tmpIgnoreFlag = true;
                                var resolIdx = ["HD", "SD", "FHD", "UHD"].indexOf(resolCd[j]);
                                for (var j = 0; j < resolCd.length; j++) {
                                    if (omtBuyList[resolIdx] === "Y") {
                                        buttonList[ButtonObject.TYPE.SERIES].setOtnYn(true, (resolCd[j]));
                                    } else if (omtBuyList[resolIdx] === "N") {
                                        tmpIgnoreFlag = false;
                                    }
                                }
                                ignoreCheckShrContent = ignoreCheckShrContent || tmpIgnoreFlag;
                            } else if (omtBuyList[0] === "Y") {
                                buttonList[ButtonObject.TYPE.SERIES].setOtnYn(true);
                                ignoreCheckShrContent = true;
                            }

                            var buyingDate;
                            var type;

                            // 단편 버튼에도 구매된 화질에 대해서 구매 처리
                            for (var idx = 0; idx < assetList.length; idx++) {
                                if (cateInfo.cmbYn === "Y") {
                                    for (var j = 0; j < resolCd.length; j++) {
                                        var resolIdx = ["HD", "SD", "FHD", "UHD"].indexOf(assetList[idx].resolCd);
                                        if (omtBuyList[resolIdx] === "Y") {
                                            // 단편 버튼에 구매여부 설정
                                            type = assetList[idx].getData("data").isDvdYn === "Y" ? ButtonObject.TYPE.DVD : ButtonObject.TYPE.NORMAL;
                                            if (buyTypeData.single && buttonList[type]) {
                                                buttonList[type].setOtnYn(true, assetList[idx].getData("assetId"));
                                                buttonList[type].otnYn = true;
                                            }

                                            // assetId 별 데이터에 otmLinkTime, otmBuyingDate 설정
                                            assetList[idx].setData("otmLinkTime", otmLinkTimeList[resolIdx]);
                                            buyingDate = otmBuyingDateList[resolIdx];
                                            if (buyingDate && buyingDate !== "null" && buyingDate !== "NULL" && buyingDate !== "") {
                                                assetList[idx].setData("otmBuyingDate", buyingDate);
                                            }
                                        }
                                    }
                                } else if (omtBuyList[0] === "Y") {
                                    // 단편 버튼에 구매여부 설정
                                    type = assetList[idx].getData("data").isDvdYn === "Y" ? ButtonObject.TYPE.DVD : ButtonObject.TYPE.NORMAL;
                                    if (buyTypeData.single && buttonList[type]) {
                                        buttonList[type].setOtnYn(true, assetList[idx].getData("assetId"));
                                        buttonList[type].otnYn = true;
                                    }

                                    // assetId 별 데이터에 otmLinkTime, otmBuyingDate 설정
                                    assetList[idx].setData("otmLinkTime", otmLinkTimeList[0]);
                                    if (otmBuyingDateList[0] && otmBuyingDateList[0] !== "null" && otmBuyingDateList[0] !== "NULL" && otmBuyingDateList[0] !== "") {
                                        assetList[idx].setData("otmBuyingDate", otmBuyingDateList[0]);
                                    }
                                }
                            }
                        }

                        // 단편에 대해 호출
                        if (!ignoreCheckShrContent) {
                            // 시리즈 구매된 게 없을 때만 호출
                            reqData = {
                                saId: DEF.SAID,
                                contsId: contentIdList,
                                paringYn: "Y",
                                vodPrimePrdcYn: "N",
                                itemType: "2"
                            };
                            VODAmocManager.checkShrContent(reqData, responseCheckShrContent);
                        } else {
                            callback(true);
                        }
                    } else {
                        extensionAdapter.notifySNMPError("VODE-00013");
                        HTool.openErrorPopup({
                            message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                            reboot: true
                        });
                        callback(false);
                    }
                });
            }
            else {
                // 시리즈 구매 타입이 없는 경우 바로 단편 구매여부 조회
                var reqData = {
                    saId: DEF.SAID,
                    contsId: contentIdList,
                    paringYn: "Y",
                    vodPrimePrdcYn: "N",
                    itemType: "2"
                };
                VODAmocManager.checkShrContent(reqData, responseCheckShrContent);
            }
        } else {
            callback(true);
        }

        function responseCheckShrContent(result, data) {
            // 단편에 대한 checkShrContent 호출 후 callBack
            try{
                if(result) {
                    var idList = contentIdList.split("|");
                    var omtBuyList = data.otmBuyYn.split("|");
                    // var otmResolCdList = data.otmResolCd.split("|");
                    var otmLinkTimeList = data.otmLinkTime.split("|");
                    var otmBuyingDateList = data.buyingDate.split("|");

                    for (var i = 0; i < idList.length; i++) {
                        var assetData = contentList[idList[i]];

                        /**
                         * [dj.son] otm 구매 일시 저장
                         */
                        if (otmBuyingDateList[i] && otmBuyingDateList[i] !== "null" && otmBuyingDateList[i] !== "NULL" && otmBuyingDateList[i] !== "") {
                            assetData.setData("otmBuyingDate", otmBuyingDateList[i]);
                        }
                        else {
                            assetData.setData("otmBuyingDate", null);
                        }

                        if (otmLinkTimeList[i]) {
                            assetData.setData("otmLinkTime", otmLinkTimeList[i]);
                        }
                        if (omtBuyList[i] === "Y") {
                            var type = assetData.getData("data").isDvdYn === "Y" ? ButtonObject.TYPE.DVD : ButtonObject.TYPE.NORMAL;

                            // seriesOnly인데 단편 구매가 된 경우 단편버튼 추가 (재생용)
                            if (!buyTypeData.single) {
                                if (buttonList[type]) {
                                    if (!hasAsset(buttonList[type], assetData)) {
                                        buttonList[type].optionList.push(assetData.getRepOption());
                                    }
                                } else {
                                    buttonList[type] = new SingleButtonObject(type, [assetData.getRepOption()]);
                                    buttonList[type].isOnlyView = true;
                                }
                            }
                            if (buttonList[type]) {
                                buttonList[type].setOtnYn(true, assetData.getData("assetId"));
                                buttonList[type].otnYn = true;
                            }
                            // } if(otmResolCdList[i]) {
                            //     if(option.cmbYn=="Y") {
                            //         var type = assetData.getData("data").isDvdYn === "Y"?ButtonObject.TYPE.DVD:ButtonObject.TYPE.NORMAL;
                            //
                            //         // seriesOnly인데 단편 구매가 된 경우 단편버튼 추가 (재생용)
                            //         if (!buyTypeData.single) {
                            //             if (buttonList[type]) {
                            //                 if(!hasAsset(buttonList[type], assetData)) buttonList[type].optionList.push(assetData.getRepOption());
                            //             } else {
                            //                 buttonList[type] = new SingleButtonObject(type, [assetData.getRepOption()]);
                            //                 buttonList[type].isOnlyView = true;
                            //             }
                            //         }
                            //         var assetId = option.assetId ? (option.assetId.split("|")[option.resolCd.split("|").indexOf(otmResolCdList[i])]) : ""; //일반편성 시리즈는 assetId가 없음. 따라서 다른화질 구매로 오면 무시함.
                            //         if(assetId && buttonList[type]) {
                            //             buttonList[type].setOtnYn(true, assetId);
                            //             buttonList[type].otnYn = true;
                            //         }
                            //     }
                        }
                    } callback(true);
                } else {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    HTool.openErrorPopup({
                        message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]),
                        reboot: true
                    });
                    callback(false);
                }
            } catch (e) {
                log.printErr(e);
                callback(false);
            }
        };

        function hasAsset(button, asset) {
            var assetId = asset.getData("assetId");

            for (var i in button.optionList) {
                if (button.optionList[i].assetId == assetId) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * 시리즈 OTM 구매여부 조회
     * 시리즈 화면 내에서는 재조회를 하지 않기 위해, 기존에 조회한 데이터가 있으면
     * API를 호출하지 않고 기존 데이터 사용
     * @param cateInfo
     * @param callback
     * @private
     */
    function _getSeriesCheckShrContentData(cateInfo, callback) {
        if (cateInfo.seriesCheckShrContent) {
            callback(true, cateInfo.seriesCheckShrContent);
        } else {
            var reqData = {
                saId: DEF.SAID,
                contsId: cateInfo.catId || cateInfo.itemId,
                paringYn: "Y",
                vodPrimePrdcYn: "N",
                itemType: "1",
                cmbYn: cateInfo.cmbYn
            };
            VODAmocManager.checkShrContent(reqData, function (result, data) {
                // OTM 시리즈 구매여부 처리
                callback(result, data);
            });
        }
    }

    /** 콘텐츠/시리즈 이용권 조회 및 데이터 구성 */
    /*
    return [ "contCoupon" : 콘텐츠 이용권 list,
             "seriesCoupon" : 시리즈 이용권 list ]
    */
    function _getContCouponData(buyTypeData, assetList, callback) {
        var contMap = {}; // 콘텐츠 쿠폰은 key = assetId
        var seriesMap = {}; // 시리즈 쿠폰은 key = resolCd

        if (buyTypeData.single || buyTypeData.series) {
            // assetList 가 assetId 별로 나눠진게 맞다면...
            // option 데이터 에서 voucherYn과 contCoupon 여부를 보고 ID별로 이용권 조회 한다

            // ID별 조회가 모두 끝난 후 다음 프로세스로 가야 하기 때문에 count 해서 확인한다.
            var checkCount = assetList.length;

            for (var i = 0; i < assetList.length; i++) {
                var optionData = assetList[i].getRepOption();
                // 쿠폰 사용 가능한 콘텐츠인 경우에만 조회한다.
                if (optionData.voucherYn == "Y" || optionData.isContCoupon) {
                    var reqData = {
                        retvTypeCd: 3,
                        ctgryId: optionData.catData.catId || optionData.catData.itemId,
                        contsId: optionData.assetId,
                        settlNm: optionData.contsName,
                        params: i + ""
                    };
                    console.log(reqData);

                    // Lupin 에서는 시리즈 이용권 조회 시 assetID의 해상도를 기준으로 찾아준다고 했음.
                    // 즉.. catID+assetID 로 조회하게 되면 assetID 에 해당하는 단편 이용권과 이 화질에 해당하는 시리즈 이용권 정보를 내려줌.
                    VODPointManager.getStbCouponInfo(reqData, function (res, couponData, _idx) {
                        console.log("[getStbCouponInfo] res = " + res);
                        var _optionData = assetList[_idx].getRepOption()

                        if (KTW.TEST_CODE) {
                        var resol = (_optionData.resolCd == "Y" || _optionData.resolCd == "HD") ? "01" : "00";
                            couponData.resCd = "0000";
                            couponData.resMsg = "사용 가능한 이용권이 존재합니다";
                            couponData.couponCount = 2;
                            couponData.item = [
                                {
                                    "couponCase": "CD",
                                    "resolution": resol,
                                    "couponCategory": "P",
                                    "discountType": "P",
                                    "discountAmountLabel": "1000",
                                    "applicableCouponPriceP": 1000,
                                    "applicableCouponPriceS": 0,
                                    "maxApplicableCouponPrice": 1000
                                },
                                {
                                    "couponCase": "SC",
                                    "resolution": resol,
                                    "couponCategory": "P",
                                    "discountType": "P",
                                    "discountAmountLabel": "2000",
                                    "applicableCouponPriceP": 2000 + (_idx * 200),
                                    "applicableCouponPriceS": 0,
                                    "maxApplicableCouponPrice": 2000
                                }
                            ];
                        }

                        if (couponData && couponData.resCd == "0000") {
                            var items = couponData.item;
                            for (var itemIdx = 0; itemIdx < couponData.item.length; itemIdx++) {
                                // 해상도 값을 좀 바꾸자. 해상도 대소 비교 시 숫자가 편함.
                                if (items[itemIdx].resolution == "00") items[itemIdx].resolution = 0;
                                else if (items[itemIdx].resolution == "01") items[itemIdx].resolution = 1;
                                else if (items[itemIdx].resolution == "02") items[itemIdx].resolution = 2;
                                else if (items[itemIdx].resolution == "03") items[itemIdx].resolution = 3;

                                // 콘텐츠 이용권 저장
                                if (buyTypeData.single && _optionData.isContCoupon
                                    && (items[itemIdx].couponCase == "CC" || items[itemIdx].couponCase == "CD")) {
                                    // 콘텐츠 쿠폰은 assetId 기준으로 매핑
                                    // 이용권, 할인권 두가지가 있지만 assetId 기준으로 하나만 발급 가능하기 때문에
                                    // map 에 ID별로 한 개만 저장하면 된다.
                                    contMap[_optionData.assetId] = items[itemIdx];
                                }
                                // 시리즈 이용권 저장
                                if (buyTypeData.series && _optionData.voucherYn == "Y"
                                    && (items[itemIdx].couponCase == "SC" || items[itemIdx].couponCase == "SD")) {
                                    // 홈포탈에 제공되는 시리즈 가상카테고리 ID는 해상도 구분이 없기 때문에
                                    // 시리즈 쿠폰은 해상도 기준으로 매핑해야 한다.
                                    var newResolCd = _convertResolCd(_optionData.resolCd);
                                    seriesMap[newResolCd] = items[itemIdx];
                                }
                            }
                        }
                        checkCallback(res);
                    });
                } // end if (voucherYn or contCoupon)
                else {
                    // 콘텐츠 사용 불가인 경우
                    checkCount--;
                }
            } // end for (assetList)
        }
        else { // single, series 구매가 불가능하면 이용권 사용할 수 없음
            callback(true);
        }

        if (checkCount == 0) {
            callback(true);
        }

        var res = true;

        function checkCallback(result) {
            if (res) {
                if (!result) {
                    res = false;
                    callback(true, null);
                } else if (--checkCount == 0) {
                    callback(true, {"contCouponList" : contMap, "seriesCouponList" : seriesMap} );
                }
            }
        }
    }

    function _convertResolCd(resolCd) {
        if (resolCd == "N" || resolCd == "SD") {
            return "SD";
        }
        if (resolCd == "N" || resolCd == "HD") {
            return "HD";
        }
        if (resolCd == "F" || resolCd == "FHD") {
            return "FHD";
        }
        if (resolCd == "U" || resolCd == "UHD") {
            return "UHD";
        }
    }

    return {
        getOptionList: _getOptionList,
        getSingleOption: _getSingleOption,
        getOptionListForPlay: _getOptionListForPlay,
        getOptionListForSeries: _getOptionListForSeries,
        updateBuyInfo: _updateBuyInfo,
        updateOptionList: _updateOptionList
    }
};