/**
 * Created by Yun on 2017-01-02.
 */

window.MVPlayBar_RelationMenu = function() {
    this.playList = [];
    this.curIdx = 0;

    this.menuData = [{title: "곡 설명", classType: "info"}, {title: "담기", classType: "add"}, {title: "인기/연관", classType: "related"}];

    this.onFocused = function() {
        this.buttonFocusRefresh(0);
    };

    this.onBlurred = function() {

    };

    this.buttonFocusRefresh = function(index) {
        this.div.find("#mvPlayRelatedMenu_div .mvPlayRelatedMenu_btn").removeClass("focus");
        this.div.find("#mvPlayRelatedMenu_div .mvPlayRelatedMenu_btn").eq(index).addClass("focus");
    };

    this.setHasRecommendData = function(has) {
        this.div.find("#mvPlayRelatedMenu_div .mvPlayRelatedMenu_btn.related").toggle(has);
    };

    this.create();
};

MVPlayBar_RelationMenu.prototype.create = function() {
    var menuData = this.menuData;
    this.div = _$("<div/>", {id: "mvPlayBar_relationMenu_area"});
    this.div.append(_$("<img/>", {id: "mvPlayRelatedMenu_bg"}));
    this.div.append(_$("<div/>", {id: "mvPlayRelatedMenu_div"}));
    for (var idx = 0; idx < this.menuData.length; idx++) {
        this.div.find("#mvPlayRelatedMenu_div").append(_$(
            "<div class='mvPlayRelatedMenu_btn " + menuData[idx].classType + "'>" +
            "<img class='mvPlayBottomMenu_icon'/>" +
            "<span class='mvPlayBottomMenu_icon_txt'>" + menuData[idx].title + "</span>" +
            "</div>"
        ));
    }
};

MVPlayBar_RelationMenu.prototype.getView = function() {
    return this.div;
};

MVPlayBar_RelationMenu.prototype.show = function() {
    this.playList = [];
    this.playList = VODInfoManager.getPlayList();
    this.curIdx = VODInfoManager.getCurrentPlayListIdx();
    log.printDbg(this.playList[this.curIdx]);
    this.div.addClass("show");
};

MVPlayBar_RelationMenu.prototype.hide = function() {
    this.div.removeClass("show");
};

MVPlayBar_RelationMenu.prototype.onKeyAction = function(keyCode) {
};