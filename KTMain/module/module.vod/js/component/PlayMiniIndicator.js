/**
 * Created by ksk91_000 on 2016-09-07.
 */
window.PlayMiniIndicator = function() {
    this.div;
    this.iconList = [];
    this.mainTimeout = null;
    this.subTimeout = null;
    var _this = this;
    var speedArray = [];
    var isShowBg = false;
    speedArray[VODManager.PLAY_STATE.REWIND] = [2, 8, 32];
    speedArray[VODManager.PLAY_STATE.FASTFOWARD] = [1.2, 2, 8, 32];

    this.setMVMode = function() {
        this.div.addClass("mvMiniIndicator");
    };

    this.setNormalMode = function() {
        this.div.removeClass("mvMiniIndicator");
    };

    this.showIndicator = function(state, speed) {
        clearHideTimeout();
        this.div.find("#main_area .icon").attr("class", "icon " + this.iconList[state]);
        this.div.find("#main_area .icon").show();
        this.div.find("#sub_area").css("visibility", "inherit");
        this.hideBg();
        // 배속 텍스트 영역 설정
        this.div.find("#main_area .speed_area").show();
        this.div.find("#main_area .speed_area .speed_text").removeClass("pause");
        this.div.find("#main_area .speed_area .speed_text").removeClass("seek");
        this.div.find("#main_area .speed_area .speedX").addClass("hide");
        // 배속에 따른 배경 사이즈 초기화
        this.div.find("#bg_area").removeClass("bg2");
        this.div.find("#bg_area").removeClass("bg3");
        this.div.find("#sub_area").removeClass("style2");
        this.div.find("#sub_area").removeClass("style3");
        isShowBg = false;
        switch (state) {
            case VODManager.PLAY_STATE.STOP :
            case VODManager.PLAY_STATE.PLAY :
                if (state == VODManager.PLAY_STATE.PLAY) this.div.find("#main_area .icon").hide();
                this.div.find("#main_area .speed_area").hide();
                hideSubIndicator();
                setHideIndicator();
                break;
            case VODManager.PLAY_STATE.PAUSE :
                isShowBg = true;
                if (!MiniGuideManager.isShowing()) this.showBg();
                this.div.find("#main_area .speed_area .speed_text").text("일시정지").addClass("pause");
                hideSubIndicator();
                break;
            case VODManager.PLAY_STATE.REWIND :
            case VODManager.PLAY_STATE.FASTFOWARD :
                if (Math.abs(speed - 0) >= 10) {
                    this.div.find("#bg_area").addClass("bg3");
                    this.div.find("#sub_area").addClass("bg3");
                }
                else {
                    if (Math.abs(speed - 0) >= 2) {
                        this.div.find("#sub_area").addClass("style3");
                        this.div.find("#bg_area").addClass("bg2");
                    }
                    else this.div.find("#sub_area").addClass("style2");
                }
                isShowBg = true;
                if (!MiniGuideManager.isShowing()) this.showBg();
                this.div.find("#main_area .speed_area .speedX").removeClass("hide");
                this.div.find("#main_area .speed_area .speed_text").text(Math.abs(speed - 0));
                showSubIndicator();
                setSubArea(this.div.find("#sub_area .speed_text"), state, speed);
                break;
            case VODManager.PLAY_STATE.FASTPLAY :
            case VODManager.PLAY_STATE.SLOWPLAY :
                this.div.find("#main_area .speed_area .speedX").removeClass("hide");
                this.div.find("#main_area .speed_area .speed_text").text(Math.abs(speed - 0));
                this.div.find("#sub_area").addClass("style2");
                isShowBg = true;
                if (!MiniGuideManager.isShowing()) this.showBg();
                hideSubIndicator();
                break;
            case VODManager.PLAY_STATE.SEEKBACK :
            case VODManager.PLAY_STATE.SEEKFWD :
                this.div.find("#main_area .speed_area .speed_text").addClass("seek");
                if (speed - 0 > 0) {
                    if ((speed / 1E3) % 60 == 0) this.div.find("#main_area .speed_area .speed_text").text((speed / 1E3) / 60 + "분");
                    else this.div.find("#main_area .speed_area .speed_text").text((speed / 1E3) + "초");
                }
                else this.div.find("#main_area .speed_area .speed_text").text("5분");
                hideSubIndicator();
                setHideIndicator();
                break;
        }
        _this.div.css("visibility", "inherit");
    };

    function clearHideTimeout() {
        if (_this.mainTimeout) {
            clearTimeout(_this.mainTimeout);
            _this.mainTimeout = null;
        }
        if (_this.subTimeout) {
            clearTimeout(_this.subTimeout);
            _this.subTimeout = null;
        }
    }

    function setHideIndicator() {
        _this.mainTimeout = setTimeout(function() {_this.hideIndicator.call(_this)}, 1500);
    }

    this.hideIndicator = function() {
        _this.div.css("visibility", "hidden");
        clearHideTimeout();
    };

    /*
     ####### 다른 배속 표시
     */
    function showSubIndicator() {
        _this.div.find("#sub_area").css("visibility", "inherit");
        _this.subTimeout = setTimeout(function() {_this.div.find("#sub_area").css("visibility", "hidden");}, 1500);
    }

    function hideSubIndicator() {
        _this.div.find("#sub_area").css("visibility", "hidden");
    }

    function setSubArea(div, state, speed) {
        for (var i = 0; i < 3; i++) {
            speed = Math.abs(speed - 0);
            var str = speedArray[state][HTool.getIndex(speedArray[state].indexOf(speed), i + 1, 4)];
            div.eq(i).toggle(!!str).text(str);
        }
    }

    /*
     ####### 배경 표시
     */
    this.showBg = function() {
        if (isShowBg) {
            this.div.addClass("showingBg");
            this.div.find("#bg_area").css("visibility", "inherit");
        } else this.hideBg();
    };
    this.hideBg = function() {
        this.div.removeClass("showingBg");
        this.div.find("#bg_area").css("visibility", "hidden");
    };

    this.create();
    this.hideIndicator();
};

PlayMiniIndicator.prototype.create = function() {
    this.div = _$("<div/>", {id: "PlayMiniIndicator"});
    this.div.append(_$("<div/>", {id: "bg_area"}));
    this.div.append(_$("<div/>", {id: "main_area"}));
    this.div.append(_$("<div/>", {id: "sub_area"}));
    this.div.find("#main_area").append(_$("<div/>", {class: "icon"}));
    this.div.find("#main_area").append(_$("<div/>", {class: "speed_area"}));
    this.div.find("#main_area .speed_area").append(_$("<div/>", {class: "speedX hide"}));
    this.div.find("#main_area .speed_area").append(_$("<span>", {class: "speed_text"}));
    this.div.find("#sub_area").append(_$("<div/>", {class: "speed_text"}));
    this.div.find("#sub_area").append(_$("<div/>", {class: "speed_text"}));
    this.div.find("#sub_area").append(_$("<div/>", {class: "speed_text"}));

    this.iconList[VODManager.PLAY_STATE.STOP] = "";
    this.iconList[VODManager.PLAY_STATE.PLAY] = "icon_play";
    this.iconList[VODManager.PLAY_STATE.PAUSE] = "icon_pause";
    this.iconList[VODManager.PLAY_STATE.REWIND] = "icon_rew";
    this.iconList[VODManager.PLAY_STATE.FASTFOWARD] = "icon_ff";
    this.iconList[VODManager.PLAY_STATE.FASTPLAY] = "icon_ff";
    this.iconList[VODManager.PLAY_STATE.SLOWPLAY] = "icon_ff";
    this.iconList[VODManager.PLAY_STATE.SEEKBACK] = "icon_sb";
    this.iconList[VODManager.PLAY_STATE.SEEKFWD] = "icon_sf";
};

PlayMiniIndicator.prototype.getView = function() {
    return this.div;
};