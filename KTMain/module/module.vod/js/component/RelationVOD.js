/**
 * Created by Yun on 2017-02-15.
 */

window.RelationVOD = function() {
    var _this = this;
    var focusIdx = 0;
    var curPage = 0, maxPage;
    var recommendDiv, recommendData = null;
    var currentCatId = null;
    var hasAdult = false, isMV = false;
    var isAuthorizedContent = false;

    this.onFocused = function() {
        if (!_this.div.hasClass("focused")) {
            LayerManager.startLoading();
            setData(currentCatId, function() {
                LayerManager.stopLoading();
                _this.div.addClass("focused");
                focusIdx = 0;
                changeFocus();
                log.printDbg("requestShow === RelationVOD");
                requestShow();
            });
        }
    };

    this.onBlurred = function() {
        // 2017.08.08 Lazuli
        // onBlurred 함수가 불필요하게 많이 호출됨으로 인해, Timeout 등 속도 저하 발생
        // notifyHide 함수가 꽤 무거움
        // focused 상태일 때에만 로직 실행하도록 변경
        if (_this.div.hasClass("focused")) {
            recommendDiv = this.div.find(".poster_area .recommendArea");
            this.div.removeClass("focused");
            var list = recommendDiv.find(".posterList");
            if (list.hasClass("moved")) {
                list.css("transition", "-webkit-transform 0");
                list.removeClass("moved");
                list[0].offsetWidth;
                list.css("transition", "-webkit-transform .5s");
            }
            focusIdx = 0;
            setRollingPage(0, 1);
            recommendDiv.find(".recommendLeftDim").addClass("hide");
            log.printDbg("notifyHide === RelationVOD");
            notifyHide();
        }
    };

    function changeFocus() {
        if (recommendData == null || recommendData.length == 0) return;
        recommendDiv.find(".posterList li").removeClass("focus");
        recommendDiv.find(".posterList li#" + recommendData[focusIdx].ITEM_ID + "_" + focusIdx).addClass("focus");
        setTextAnimation(recommendDiv.find(".posterList"), focusIdx);
    }

    function setTextAnimation(_div, focus) {
        UTIL.clearAnimation(_div.find(".textAnimating span"));
        _div.find(".textAnimating").removeClass("textAnimating");
        void _div[0].offsetWidth;
        if (UTIL.getTextLength(recommendData[focus].ITEM_NAME, "RixHead L", 30) > 215) {
            var posterDiv = _div.find("#" + recommendData[focus].ITEM_ID + "_" + focus).eq(0).find(".posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    function moveRollingPage(goNext, currentPage, nextPage) {
        if (!recommendData || recommendData.length == 0) {
            return;
        }
        var movingList = recommendDiv.find(".posterList"), showingPage = 0;
        // GoNextPage
        if (goNext) {
            if (movingList.hasClass("moved")) {
                movingList.css("transition", "-webkit-transform 0");
                movingList.removeClass("moved");
                movingList[0].offsetWidth;
                // DataChange
                setRollingPage(currentPage, nextPage);
                movingList.css("transition", "-webkit-transform .5s");
            }
            movingList.addClass("moved");
            showingPage = 1;
        }
        // GoPrevPage
        else {
            if (!movingList.hasClass("moved")) {
                movingList.css("transition", "-webkit-transform 0");
                movingList.addClass("moved");
                movingList[0].offsetWidth;
                setRollingPage(nextPage, currentPage);
                movingList.css("transition", "-webkit-transform .5s");
            }
            movingList.removeClass("moved");
            showingPage = 0;
        }
        for (var i = 0; i < 16; i++) {
            if (movingList.find(".content.poster_vertical")[i].id != "") movingList.find(".content.poster_vertical").eq(i).removeClass("hide");
        }
        recommendDiv.find(".recommendRightDim").removeClass("hide");
        recommendDiv.find(".recommendLeftDim").removeClass("hide");
        if (nextPage == maxPage) {
            movingList.find(".content.poster_vertical").eq(showingPage == 0 ? 8 : 15).addClass("hide");
            recommendDiv.find(".recommendRightDim").addClass("hide");
        } else if (nextPage == 0) {
            movingList.find(".content.poster_vertical").eq(showingPage == 0 ? 0 : 7).addClass("hide");
            recommendDiv.find(".recommendLeftDim").addClass("hide");
        }
    }

    function setRollingPage(leftPage, rightPage) {
        if (!recommendData || recommendData.length == 0) {
            return;
        }
        var list = recommendDiv.find(".posterList");
        var renderPage = leftPage, tmpIdx;
        for (var j = 0; j < 16; j++) {
            var li = list.find(".content.poster_vertical").eq(j);
            if (j < 8) {
                if (j == 0 && leftPage == 0) {
                    li.addClass("hide");
                    li.attr("id", "");
                    continue;
                }
                renderPage = leftPage;
                tmpIdx = (renderPage * 7) + j - 1;
            }
            else {
                renderPage = rightPage;
                tmpIdx = (renderPage * 7) + j - 8;
            }
            if (!recommendData[tmpIdx]) {
                li.addClass("hide");
                li.attr("id", "");
            }
            else {
                setRollingPageData(li, tmpIdx);
            }
        }
    }

    function setRollingPageData(list, idx) {
        if (!recommendData || recommendData.length == 0) {
            return;
        }
        list.removeClass("hide");
        list.attr("id", recommendData[idx].ITEM_ID + "_" + idx);
        list.find(".posterImg.poster").attr("src", recommendData[idx].IMG_URL + HTool.addPosterOption(210, 300, 90));
        list.find(".posterTitle span").text(recommendData[idx].ITEM_NAME);
        if (!recommendData[idx].WON_YN) {
            list.find("img.isfreeIcon").css("display", "none");
            list.find("span.isfree").css("display", "none");
        } else if (HTool.getBoolean(recommendData[idx].WON_YN)) {
            list.find("img.isfreeIcon").css("display", "inherit");
            list.find("span.isfree").css("display", "none");
        } else {
            list.find("span.isfree").css("display", "inherit");
            list.find("span.isfree").text("무료");
            list.find("img.isfreeIcon").css("display", "none");
        }
        list.find(".age").attr("src", ROOT_URL + "resource/detail/icon_age_list_" + ((recommendData[idx].RATING - 0) > 0 ? (recommendData[idx].RATING - 0) : "all" ) + ".png");
        list.find(".stars").empty();
        setPosterIcon(list, recommendData[idx]);
        if (!isAuthorizedContent && UTIL.isLimitAge(recommendData[idx].RATING)) list.find("img.posterLockImg").css("display", "inherit");
        else list.find("img.posterLockImg").css("display", "none");
        list.find(".stars").append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(list.find(".stars"), recommendData[idx].OLLEH_RATING);
    }

    function setPosterIcon(elem, data) {
        var posterIconCnt = 0;
        var posterIconArea = elem.find(".posterIconArea");
        posterIconArea.empty();
        if (data.HDR_YN == "Y" && CONSTANT.IS_HDR) {
            posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_hdr.png"}));
            posterIconCnt++;
        }
        if (data.IS_HD == "UHD") {
            posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_uhd.png"}));
            posterIconCnt++;
        }
        if (data.SMART_DVD_YN == "Y" && posterIconCnt < 2) {
            posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_mine.png"}));
        }

        var newHotIconArea = elem.find(".newHotIconArea");
        newHotIconArea.empty();
        if (data.NEW_HOT == "NEW") newHotIconArea.append(_$("<img>", {src: modulePath + "resource/icon_flag_new.png"}));
    }

    function getVerticalPoster(data, index) {
        var element = _$("<li class='content poster_vertical' id='" + (data.ITEM_ID + "_" + index) + "'>" +
            "<div class='content'>" +
            "<img src='" + data.IMG_URL + HTool.addPosterOption(210, 300, 90) + "' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/defaultPoster/default_poster.png\"'>" +
            "<div class='posterIconArea'/>" +
            "<div class='newHotIconArea'/>" +
            "<img class='posterLockImg'>" +
            "<div class='contentsData'>" +
            "<div class='posterTitle'>" +
            "<span>" + data.ITEM_NAME + "</span>" +
            "</div>" +
            "<span class='posterDetail'>" +
            "<span class='stars'/>" +
            "<span class='isfreeArea'>" +
            "<img class='isfreeIcon'>" +
            "<span class='isfree'>" + "무료" + "</span>" +
            "</span>" +
            "<img src='" + ROOT_URL + "resource/detail/icon_age_list_" + ((data.RATING - 0) > 0 ? (data.RATING - 0) : "all" ) + ".png' class='age'>" +
            "</span>" +
            "</div>" +
            "</div>" +
            "</li>");
        element.find(".stars").append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(element.find(".stars"), data.OLLEH_RATING);
        setPosterIcon(element, data);
        if (!isAuthorizedContent && UTIL.isLimitAge(data.RATING)) element.find("img.posterLockImg").css("display", "inherit");
        else element.find("img.posterLockImg").css("display", "none");
        if (HTool.isTrue(data.WON_YN)) {
            element.find("img.isfreeIcon").css("display", "inherit");
            element.find("span.isfree").css("display", "none");
        } else if (HTool.isFalse(data.WON_YN)) {
            element.find("span.isfree").css("display", "inherit");
            element.find("span.isfree").text("무료");
            element.find("img.isfreeIcon").css("display", "none");
        } else {
            element.find("img.isfreeIcon").css("display", "none");
            element.find("span.isfree").css("display", "none");
        }
        return element;
    }

    function getEmptyPoster() {
        return _$("<li class='content poster_vertical hide' id=''><div class='content'><img src='' class='posterImg poster'><div class='posterIconArea'/><div class='newHotIconArea'/><img class='posterLockImg'><div class='contentsData'><div class='posterTitle'><span></span></div><span class='posterDetail'><span class='stars'/><span class='isfreeArea'><img class='isfreeIcon'><span class='isfree'/>><img src='' class='age'></span></div></div></li>");
    }

    function makeRecommendList() {
        var j, list;
        if (!recommendDiv.find(".posterList")[0]) {
            recommendDiv.append(_$(
                "<div class='recommendPage'>" +
                "<ul class='posterList'/>" +
                (recommendData.length > 7 ? "<img class='recommendRightDim'/>" : "<img class='recommendRightDim hide'/>") +
                "<img class='recommendLeftDim hide'/>" +
                "</div>"
            ));
            list = recommendDiv.find(".posterList");
            list.append(getEmptyPoster());
            for (j = 0; j < 15; j++) {
                if (recommendData[j]) list.append(getVerticalPoster(recommendData[j], j));
                else list.append(getEmptyPoster());
            }
        } else {
            list = recommendDiv.find(".posterList");
            if (list.hasClass("moved")) {
                list.css("transition", "-webkit-transform 0");
                list.removeClass("moved");
                list[0].offsetWidth;
                list.css("transition", "-webkit-transform .5s");
            }
            setRollingPage(0, 1);
            recommendDiv.find(".recommendLeftDim").addClass("hide");
            if (recommendData.length > 7) recommendDiv.find(".recommendRightDim").removeClass("hide");
            else recommendDiv.find(".recommendRightDim").addClass("hide");
        }
    }

    this.setRelationVod = function(_catId, _hasAdult, _isMV) {
        currentCatId = _catId;
        hasAdult = _hasAdult;
        isMV = _isMV;
        recommendData = null;
    };

    this.showTopIconArea = function() {
        _this.div.addClass("show");
        if (isMV) this.div.find(".top_icon_area").css("opacity", "0");
        else this.div.find(".top_icon_area").css("opacity", ".99");
    };

    function setData(catId, callback) {
        if (recommendData != null) {
            callback();
            return;
        }
        isAuthorizedContent = VODManager.getCurrentVODInfo().isAuthorizedContent;
        recommendDiv = _this.div.find(".poster_area .recommendArea");
        _this.div.find(".no_contents_area").removeClass("hide");
        if (VODManager.getCurrentOriginVODInfo()) log.printDbg("recommendYn is " + VODManager.getCurrentOriginVODInfo().recommendYn);
        if (VODManager.getCurrentOriginVODInfo() && VODManager.getCurrentOriginVODInfo().recommendYn == "P") {
            HTool.updateCustEnv(function() {
                var loadPackageList = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                var productList = (loadPackageList && loadPackageList != null && loadPackageList.length > 0) ? StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST) + "," + loadPackageList : StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
                VodRecmManager.getRelatedContents("ITEM_VOD2", "P", DEF.SAID, productList, VODManager.getCurrentOriginVODInfo().contsId, catId, hasAdult, 30, function(res, response) {
                    if (res && response.RESULT_CODE == 200 && response.RC_LIST.length > 0) {
                        recommendData = response.RC_LIST;
                        maxPage = Math.floor((recommendData.length - 1) / 7);
                        makeRecommendList();
                        _this.div.find(".recommendArea").show();
                        _this.div.find(".no_contents_area").addClass("hide");
                    } else {
                        _this.div.find(".recommendArea").hide();
                    }
                    callback();
                });
            });
        } else {
            callback();
            recommendData = [];
            maxPage = 0;
            _this.div.find(".recommendArea").hide();
            _this.onBlurred();
            _this.hide();
        }
    }

    this.hasRecommendData = function() {return (VODManager.getCurrentOriginVODInfo() && VODManager.getCurrentOriginVODInfo().recommendYn == "P")};

    this.controlKey = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT:
                curPage = Math.floor(focusIdx / 7);
                if (focusIdx % 7 == 0 && recommendData.length > 7) {
                    if (curPage == 0) moveRollingPage(false, curPage, maxPage);
                    else moveRollingPage(false, curPage, curPage - 1);
                }
                focusIdx = HTool.indexMinus(focusIdx, recommendData.length);
                changeFocus();
                return true;
            case KEY_CODE.RIGHT:
                curPage = Math.floor(focusIdx / 7);
                if (((focusIdx + 1) % 7 == 0 || focusIdx == recommendData.length - 1) && recommendData.length > 7) {
                    if (curPage == maxPage) moveRollingPage(true, curPage, 0);
                    else moveRollingPage(true, curPage, curPage + 1);
                }
                focusIdx = HTool.indexPlus(focusIdx, recommendData.length);
                changeFocus();
                return true;
            case KEY_CODE.ENTER:
                var catId;
                switch (recommendData[focusIdx].ITEM_TYPE - 0) {
                    case 1:
                        catId = recommendData[focusIdx].ITEM_ID;
                        break;
                    default:
                        catId = recommendData[focusIdx].PARENT_CAT_ID;
                        break;
                }
                execute({
                    method: "showDetail",
                    params: {
                        cat_id: catId,
                        const_id: recommendData[focusIdx].ITEM_ID,
                        req_cd: "68",
                        isAuthorizedContent: isAuthorizedContent
                    }
                });
                try {
                    NavLogMgr.collectJumpVOD(NLC.JUMP_START_RELATED_CONTENTS, catId, recommendData[focusIdx].ITEM_ID);
                } catch (e) {
                }
                return true;
            default:
                return false;
        }
    };

    this.create();
};

RelationVOD.prototype.create = function() {
    this.div = _$("<div/>", {class: "relationVodArea"});
    this.div.append(_$("<div/>", {class: "top_icon_area"}));
    this.div.append(_$("<div/>", {class: "poster_area"}));
    this.div.find(".poster_area").append(_$("<div/>", {class: "title_area"}).text("함께 많이 본 영상"));
    this.div.find(".poster_area").append(_$("<div/>", {class: "recommendArea"}));
    this.div.find(".poster_area").append(_$("<div/>", {class: "no_contents_area"}).append("<img src='" + modulePath + "/resource/icon_noresult.png'><p>연관 VOD가 없습니다</p>"));
};

RelationVOD.prototype.getView = function() {
    return this.div;
};

RelationVOD.prototype.show = function() {
    this.showTopIconArea();
};

RelationVOD.prototype.hide = function() {
    this.div.removeClass("show");
};

RelationVOD.prototype.onKeyAction = function(keyCode) {
    return this.controlKey(keyCode);
};