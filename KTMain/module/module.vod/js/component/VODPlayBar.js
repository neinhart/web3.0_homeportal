/**
 * Created by ksk91_000 on 2016-09-05.
 */
window.VODPlayBar = function() {
    this.div;
    this.endTime;
    this.currentTime;
    this.hTimeout;
    this.showing = false;

    this.setTitle = function() {
        var vodTitle = "";
        VODManager.getCurrentVODInfo().contsName != null ? vodTitle = VODManager.getCurrentVODInfo().contsName :
            VODManager.getCurrentVODInfo().itemName != null ? vodTitle = VODManager.getCurrentVODInfo().itemName : "";
        this.div.find('#title_area').text(vodTitle);
    };
    this.setTime = function(endTime, currentTime) {
        this.div.find('#end_time_area').text(makeTimeStr(endTime));
        this.endTime = endTime;
        this.setProgress(currentTime < 0 ? 0 : currentTime);
    };

    // 프로그레스바 위치 변경
    this.setProgress = function(currentTime) {
        this.currentTime = currentTime;
        var pos = Math.min((currentTime / this.endTime) * 1740, 1740);
        this.div.find('#red_bar').css("width", pos + "px");
        this.div.find('#bar_icon.focus').css("-webkit-transform", "translateX(" + pos + "px)");
        this.div.find('#current_time_area').text(makeTimeStr(currentTime));
    };

    this.invertProgress = function(_invert) {
        this.div.find('#red_bar').toggleClass("invert", !!_invert);
    };

    this.setIconPosition = function(position) {
        var pos = (position / this.endTime) * 1740;
        this.div.find('#bar_icon.unfocus').css("-webkit-transform", "translateX(" + pos + "px)");
    };

    function makeTimeStr(t) {
        var a = HTool.msecToTime(t);
        return a.hour + ":" + a.min + ":" + a.sec;
    }

    this.onFocused = function() {
        this.div.find('#bar_icon.focus').show();
        this.div.find('#bar_icon.unfocus').hide();
        this.showTitle();
    };

    this.onBlurred = function() {
        this.div.find('#bar_icon.focus').hide();
        this.div.find('#bar_icon.unfocus').show();
    };

    this.hideTitle = function() { this.div.find('#title_area').hide() };
    this.showTitle = function() { this.div.find('#title_area').show() };

    this.create();
};

VODPlayBar.prototype.create = function() {
    this.div = _$('<div/>', {id: "vodPlayBar"});
    this.div.append(_$('<span/>', {id: 'title_area'}));
    this.div.append(_$("<div/>", {id: 'bar_area'}));
    this.div.find('#bar_area').append(_$("<div/>", {id: "base_bar"}));
    this.div.find('#bar_area').append(_$("<div/>", {id: "red_bar"}));
    this.div.find('#bar_area').append(_$("<div/>", {id: "bar_icon", class: "unfocus"}));
    this.div.find('#bar_area').append(_$("<div/>", {id: "bar_icon", class: "focus"}));
    this.div.find('#bar_icon.focus').append(_$("<img/>", {id: "icon_play_point", src: "module/module.vod/resource/icon_play_point_f.png"}));
    this.div.find('#bar_icon.unfocus').append(_$("<img/>", {id: "icon_play_point", src: "module/module.vod/resource/icon_play_point.png"}));
    this.div.append(_$("<span/>", {id: 'current_time_area', class: 'time_area'}));
    this.div.append(_$("<span/>", {id: 'end_time_area', class: 'time_area'}));
};

VODPlayBar.prototype.show = function(alwaysShow, ignoreProgress) {
    var _this = this;
    this.div.css("visibility", "inherit");
    this.showing = true;

    if (!ignoreProgress) this.setProgress(VODManager.getCurrentPosition());
    if (this.hTimeout) {
        clearTimeout(this.hTimeout);
        this.hTimeout = null;
    }
    if (!alwaysShow) this.hTimeout = setTimeout(function() { _this.hide.call(_this); }, 5000);
};

VODPlayBar.prototype.hide = function() {
    this.div.css("visibility", "hidden");
    this.showing = false;

    if (this.hTimeout) {
        clearTimeout(this.hTimeout);
        this.hTimeout = null;
    }
};

VODPlayBar.prototype.getView = function() {
    return this.div;
};

VODPlayBar.prototype.isShowing = function() {
    return this.showing;
};

VODPlayBar.prototype.onKeyAction = function(keyCode) {
    switch (keyCode) {
        default:
            return false;
    }
};