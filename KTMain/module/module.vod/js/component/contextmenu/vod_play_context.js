/**
 * Created by Taesang on 2016-11-02.
 */

(function () {
    window.VODPlayContext = function () {
        var _this = this;
        var focus, page;
        var itemList = [];

        var btDevice = DeviceManager.bluetoothDevice;
        var cornerList = [];
        var isFailGetAudioComponent = false;

        this.addItem = function (item, index) {
            itemList[index] = item;
            this.div.append(item.div);
        };

        this.closeAllDropBox = function () {
            if (!itemList || itemList.length == 0) return;
            for (var i = 0; i < itemList.length; i++) {
                if (itemList[i] && itemList[i].hasOwnProperty("close")) itemList[i].close();
            }
        };

        this.optionApply = function (id, data) {
            _this.hide();
            if (id == null) return;
            switch (id) {
                case "corner":
                    VODInfoManager.getCorner(function (res) {
                        if (res) {
                            log.printDbg(res);
                            res.corner_grp_list = cornerList[data].corner_grp_list;
                            LayerManager.activateLayer({
                                obj: {
                                    id: "VodSelectCornerPopup",
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        cornerGrpList: res,
                                        focusedThumbnail: cornerList[data],
                                        fromContextMenu: true
                                    }
                                },
                                moduleId: "module.vod",
                                visible: true
                            })
                        }
                    }, cornerList[data].content_id, cornerList[data].corner_grp_list[0].cornergrpid, true);
                    break;
                case "smi":
                    // 자막 변경
                    VODManager.changeSMILanguage(data);
                    break;
                case "voice_change" :
                    // 음성 변경
                    var audioComp = vodAdapter.getComponents(1).item(data);
                    vodAdapter.selectComponent(audioComp);
                    VODManager.setAudioLanguage(audioComp.language);
                    break;
                case "bluetooth_change":
                    // 음성 변경 (블루투스)
                    if (btDevice.isAudioDeviceConnected())
                        vodAdapter.selectComponent(vodAdapter.getComponents(1, btDevice.getBtAudioDevice()).item(data), btDevice.getBtAudioDevice());
                    break;
                case "bluetooth_listen":
                    // 음성 듣기 (블루투스)
                    if (btDevice.isAudioDeviceConnected()) {
                        btDevice.setListenAloneMode(data != 0);
                        updateView();
                    }
                    break;
                case "sound_change":
                    // 사운드 변경
                    var audioValue = CONSTANT.IS_OTS ? ["uncompressed", "ac3"] : ["off", "uncompressed", "ac3"];
                    oipfAdapter.hwAdapter.setAVOutputDigitalAudioMode(audioValue[data]);
                    log.printDbg("setAVOutputDigitalAudioMode >>> " + audioValue[data]);
                    audioValue = null;
                    break;
                case "show_detail":
                    LayerManager.clearNormalLayer();
                    VODImpl.showDetail({
                        // 2017.10.13 Lazuli
                        // 부가영상의 경우 상세화면 진입이 안되는 케이스가 있음. 부가영상에 한해서 카테고리 ID를 "N" 호출하면 정상 진입됨
                        // cat_id: VODManager.getCurrentVODInfo().isAdditional ? "N" : VODManager.getCurrentVODInfo().catId,
                        cat_id: VODManager.getCurrentVODInfo().catId,
                        const_id: VODManager.getCurrentVODInfo().assetId,
                        isAuthorizedContent: VODManager.getCurrentVODInfo().isAuthorizedContent
                    });
                    break;
                case "vod_channel":
                    // 멀티 뷰
                    if (pipManager.isPipLayerShow()) pipManager.deactivateMultiView();
                    else {
                        pipManager.activateMultiView(function () {
                            if (VODManager.getCurrentPlayLayer()) VODManager.getCurrentPlayLayer().show();
                        }, function () {
                            VODManager.finish();
                            return false;
                        });
                    }
                    break;
                case "bookmark":
                    AppServiceManager.changeService({
                        nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD,
                        obj: {
                            type: CONSTANT.APP_TYPE.MULTICAST,
                            param: CONSTANT.APP_ID.MASHUP,
                            ex_param: CONSTANT.APP_CHILD_ID.BOOKMARK
                        }
                    });
                    break;
                case "home_widget":
                    AppServiceManager.changeService({
                        nextServiceState: CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD,
                        obj: {
                            type: CONSTANT.APP_TYPE.MULTICAST,
                            param: CONSTANT.APP_ID.MASHUP,
                            ex_param: CONSTANT.APP_CHILD_ID.HOME_WIDGET
                        }
                    });
                    break;
                case "kids_mode":
                    _this.eventListner();
                    LayerManager.activateLayer({
                        obj: {
                            id: (KidsModeManager.isKidsMode() ? "KidsModeOffPopup" : "KidsModeSettingPopup"),
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: {
                                complete: function () {
                                }
                            }
                        },
                        moduleId: "module.kids",
                        new: true,
                        visible: true
                    });
                    break;
            }
        };

        function checkBluetooth() {
            if (btDevice.isAudioDeviceConnected()) {
                var title = (DeviceManager.bluetoothDevice.checkKTBlueToothA2DPDevice() === true) ? "이어폰" : "블루투스";
                itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_LISTEN].setTitle(title + " 음성 듣기");
                itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_CHANGE].setTitle(title + " 음성 변경");
                itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_LISTEN].enable = true;
                itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_LISTEN].setOption(["같이 듣기", "혼자 듣기"], btDevice.isListenAloneMode() ? 1 : 0);
            } else {
                itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_LISTEN].enable = false;
            }

            // 음성 다중 컨텐츠 일 때 블루투스 음성 변경 노출 및 혼자듣기 여부에 따라 기존 음성 변경 숨김
            if (VODManager.getCurrentVODInfo().isMultiAud && VODManager.getCurrentVODInfo().isMultiAud === "Y") {
                if (btDevice.isAudioDeviceConnected()) {
                    itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = !btDevice.isListenAloneMode();
                    itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_CHANGE].enable = true;

                    var audioComponent = vodAdapter.getComponents(1, btDevice.getBtAudioDevice());

                    if (audioComponent && audioComponent.length > 1) {
                        var dropList = [], i;

                        for (i = 0; i < audioComponent.length; i++) {
                            dropList.push(VODManager.transLanguage(audioComponent.item(i).language));
                        }

                        itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_CHANGE].setOption(
                            dropList,
                            dropList.indexOf(VODManager.transLanguage(vodAdapter.getCurrentActiveComponents(1, btDevice.getBtAudioDevice()).item(0).language))
                        );
                    } else {
                        _this.div.find(".topArea .optionGroup").eq(3).removeClass("show");
                    }
                } else {
                    itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = true;
                    itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_CHANGE].enable = false;
                }

                if (isFailGetAudioComponent) {
                    itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = false;
                }
            } else {
                itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = false;
                itemList[VODPlayContext.ITEM_TYPE.BLUETOOTH_CHANGE].enable = false;
            }
        }

        var updateView = function () {
            _this.div.toggleClass("kids", KidsModeManager.isKidsMode());

            itemList[VODPlayContext.ITEM_TYPE.KIDS_MODE].enable = KidsModeManager.isKidsMode();
            checkBluetooth();
            if (itemList[VODPlayContext.ITEM_TYPE.CORNER].enable) {
                var curCorner = VODManager.getCurrentVODInfo().currentCorner;
                var curCornerIdx = 0, i = 0;
                log.printDbg(JSON.stringify(cornerList || {}));
                if (VODManager.isCornerMode()) {
                    for (i = 0; i < cornerList.length; i++) {
                        if (cornerList[i].content_id == curCorner.content_id && cornerList[i].file_id == curCorner.file_id) {
                            curCornerIdx = i
                        }
                    }
                } else {
                    var curTime = VODManager.getCurrentPosition();
                    curCornerIdx = cornerList.length - 1;

                    for (i = 0; i < cornerList.length; i++) {
                        if (curTime < HTool.parseVODTime(cornerList[i].time_stamp)) {
                            curCornerIdx = i - 1;
                            if (curCornerIdx < 0) curCornerIdx = 0;
                            break;
                        }
                    }
                }
                _this.setCornerData(cornerList, curCornerIdx);
            }
            setFocus(null, true);
        };
        this.updateView = updateView;

        // 자막 옵션 추가
        function setSMIOption() {
            if (VODManager.getSMILanguage() && VODManager.getSMILanguage().length > 0) {
                var dropList = [], i;

                for (i = 0; i < VODManager.getSMILanguage().length; i++) {
                    dropList.push(VODManager.transLanguage(VODManager.getSMILanguage()[i]));
                }

                itemList[VODPlayContext.ITEM_TYPE.SMI].setOption(dropList, VODManager.getDefaultSMILanguageIdx());
                itemList[VODPlayContext.ITEM_TYPE.SMI].enable = true;
            } else {
                itemList[VODPlayContext.ITEM_TYPE.SMI].enable = false;
            }
        }

        // 사운드 출력 옵션 추가
        function setSoundOption() {
            var dropList = [], select;
            dropList.push("Stereo(PCM)");          //uncompressed
            dropList.push("Dolby AC3(5.1 채널)");  //ac3

            if (!CONSTANT.IS_OTS) {
                dropList.unshift("광출력 OFF");
            }

            if (oipfAdapter.hwAdapter.getAVOutputDigitalAudioMode() == "off") {
                select = 0;
            } else if (oipfAdapter.hwAdapter.getAVOutputDigitalAudioMode() == "ac3") {
                select = dropList.length - 1;
            } else if (oipfAdapter.hwAdapter.getAVOutputDigitalAudioMode() == "uncompressed") {
                select = dropList.length - 2;
            }

            itemList[VODPlayContext.ITEM_TYPE.SOUND_CHANGE].enable = true;
            itemList[VODPlayContext.ITEM_TYPE.SOUND_CHANGE].setOption(dropList, select);
        }

        function setOptionOnKidsMode() {
            if (KidsModeManager.isKidsMode()) {
                itemList[VODPlayContext.ITEM_TYPE.BOOKMARK].enable = false;
                itemList[VODPlayContext.ITEM_TYPE.HOME_WIDGET].enable = false;
            } else {
                itemList[VODPlayContext.ITEM_TYPE.BOOKMARK].enable = true;
                itemList[VODPlayContext.ITEM_TYPE.HOME_WIDGET].enable = true;
            }
        }

        function setMultiViewOption() {
            if (CONSTANT.IS_OTS == true && (VODManager.getCurrentVODInfo().resolCd == "UHD" || CONSTANT.IS_DCS)) {
                itemList[VODPlayContext.ITEM_TYPE.VOD_CHANNEL].enable = false;
            } else {
                itemList[VODPlayContext.ITEM_TYPE.VOD_CHANNEL].enable = !KidsModeManager.isKidsMode();
            }
        }

        // 음성다중 옵션 추가
        this.setAudioComponent = function (_callback) {
            if (VODManager.getCurrentVODInfo().isMultiAud && VODManager.getCurrentVODInfo().isMultiAud === "Y") {
                isFailGetAudioComponent = false;
                var audioComponent = vodAdapter.getComponents(1);

                if (audioComponent && audioComponent.length > 1) {
                    var dropList = [], i;
                    for (i = 0; i < audioComponent.length; i++) dropList.push(VODManager.transLanguage(audioComponent.item(i).language));
                    var defSelectedSound = VODManager.getDefaultSoundIdx(audioComponent);

                    if (defSelectedSound > -1) {
                        vodAdapter.selectComponent(audioComponent.item(defSelectedSound));
                        itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].setOption(
                            dropList,
                            dropList.indexOf(VODManager.transLanguage(audioComponent.item(defSelectedSound).language)));
                    } else {
                        itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].setOption(
                            dropList,
                            dropList.indexOf(VODManager.transLanguage(vodAdapter.getCurrentActiveComponents(1).item(0).language)));
                    }
                    itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = true;
                    _callback(true);
                } else {
                    itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = false;
                    isFailGetAudioComponent = true;
                    _callback(false);
                }
            } else {
                itemList[VODPlayContext.ITEM_TYPE.VOICE_CHANGE].enable = false;
                _callback(false);
            }
        };

        this.setCornerData = function (cornerData, idx) {
            cornerList = [];
            var optionList = [];
            for (var i in cornerData) {
                var data = cornerData[i];
                if (data.corner_grp_list && data.corner_grp_list.length > 0 && data.corner_grp_list[0].cornergrptype == "002") {
                    cornerList.push(data);
                    optionList.push(data.corner_grp_list[0].cornergrpnm);
                }
            }

            itemList[VODPlayContext.ITEM_TYPE.CORNER].setOption(optionList, idx || 0);
            itemList[VODPlayContext.ITEM_TYPE.CORNER].enable = cornerList.length > 0;
        };

        this.setContextInfo = function () {
            setSMIOption();
            setSoundOption();
            setOptionOnKidsMode();
            setMultiViewOption();
            MiniGuideManager.setContextText({
                haveSMI: (itemList[VODPlayContext.ITEM_TYPE.SMI] != null && itemList[VODPlayContext.ITEM_TYPE.SMI].enable === true),
                haveSound: (VODManager.getCurrentVODInfo().isMultiAud && VODManager.getCurrentVODInfo().isMultiAud === "Y"),
                haveMultiView: (itemList[VODPlayContext.ITEM_TYPE.VOD_CHANNEL] != null && itemList[VODPlayContext.ITEM_TYPE.VOD_CHANNEL].enable === true)
            });
        };

        function setPage(_page, forced) {
            if (!forced && page == _page) {
                return;
            }

            page = _page;

            var y = 0;

            for (var i = 0; i < itemList.length; i++) {
                if (itemList[i].enable) {
                    y += itemList[i].getHeight();
                    itemList[i].div.toggle(Math.floor(y / 974) == page);
                } else {
                    itemList[i].div.hide();
                }
            }
            _this.div.find(".dwArw").toggle(Math.floor(y / 974) > page);
            _this.div.find(".upArw").toggle(page > 0);
        }

        function changeFocus(amount) {
            var newFocus = HTool.getIndex(focus, amount, itemList.length);
            // if (itemList[focus + amount]) {
            if (!itemList[newFocus].enable || itemList[newFocus].noFocus) {
                changeFocus(amount + (amount / Math.abs(amount)));
            } else {
                setFocus(newFocus);
            }
            // }
        }

        function setFocus(_focus, forced) {
            if (itemList[focus]) {
                itemList[focus].removeClass("focus");
            }

            if (_focus == null) {
                _focus = 0;

                while (_focus < itemList.length && !itemList[_focus].enable) {
                    _focus = _focus + 1;
                }
            }
            focus = _focus;
            itemList[focus].addClass("focus");

            var y = 0;

            for (var i = 0; i <= focus; i++) {
                if (itemList[i].enable) {
                    y += itemList[i].getHeight();
                }
            }
            setPage(Math.floor(y / 974), forced);
        }

        this.controlKey = function (keyCode) {
            if (itemList[focus].onKeyAction(keyCode)) return true;
            switch (keyCode) {
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                    return true;
                case KEY_CODE.UP:
                    changeFocus(-1);
                    return true;
                case KEY_CODE.DOWN:
                    changeFocus(1);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.LEFT:
                case KEY_CODE.CONTEXT:
                    this.hide();
                    return true;
                case KEY_CODE.EXIT:
                    this.hide();
                    return true;
            }
            return false;
        };

        this.isOpen = function () {
            return this.div.hasClass("activate");
        }
    };

    VODPlayContext.prototype.createView = function () {
        var div = _$("<div/>", {class: 'context_menu_area play_context_menu'});
        div.append(_$("<div/>", {class: 'background_dim'}));
        div.append(_$("<div/>", {class: 'left_arrow'}));

        this.div = _$("<div/>", {class: "context_area play_context_menu"});

        this.div.append(_$("<img>", {src: ROOT_URL + "resource/arw_related_up.png", class: "upArw"}));
        this.addItem(new DropBox("corner", "코너 몰아보기", [], this.optionApply), VODPlayContext.ITEM_TYPE.CORNER);
        this.addItem(new DropBox("smi", "자막 변경", [], this.optionApply), VODPlayContext.ITEM_TYPE.SMI);
        this.addItem(new DropBox("voice_change", "음성 변경", [], this.optionApply), VODPlayContext.ITEM_TYPE.VOICE_CHANGE);
        this.addItem(new DropBox("bluetooth_change", "블루투스 음성 변경", [], this.optionApply), VODPlayContext.ITEM_TYPE.BLUETOOTH_CHANGE);
        this.addItem(new DropBox("bluetooth_listen", "블루투스 음성 듣기", [], this.optionApply), VODPlayContext.ITEM_TYPE.BLUETOOTH_LISTEN);
        this.addItem(new DropBox("sound_change", "사운드 변경", [], this.optionApply), VODPlayContext.ITEM_TYPE.SOUND_CHANGE);
        this.addItem(new SepLine(), VODPlayContext.ITEM_TYPE.SEPLINE);
        this.addItem(new Button("show_detail", "상세정보", this.optionApply), VODPlayContext.ITEM_TYPE.SHOW_DETAIL);
        this.addItem(new Button("vod_channel", "VOD-채널 동시시청", this.optionApply), VODPlayContext.ITEM_TYPE.VOD_CHANNEL);
        this.addItem(new Button("bookmark", "북마크로 이동", this.optionApply), VODPlayContext.ITEM_TYPE.BOOKMARK);
        this.addItem(new Button("home_widget", "홈 위젯", this.optionApply), VODPlayContext.ITEM_TYPE.HOME_WIDGET);
        if (KidsModeManager.isKidsMode()) this.addItem(new SepLine(), VODPlayContext.ITEM_TYPE.SEPLINE);
        this.addItem(new KidsButton("kids_mode", "키즈모드 OFF", this.optionApply), VODPlayContext.ITEM_TYPE.KIDS_MODE);
        this.div.append(_$("<img>", {src: ROOT_URL + "resource/arw_related_dw.png", class: "dwArw"}));

        div.append(this.div);
        return div;
    };

    VODPlayContext.prototype.setContext = function (_eventListner) {
        this.eventListner = _eventListner;
    };

    VODPlayContext.prototype.show = function () {
        _$(_$.find(".context_area.play_context_menu")[0].parentElement).addClass("activate");
        this.div.addClass("activate");
        this.updateView();

        var div = _$(".textAnimating").removeClass("textAnimating").addClass("pauseAnimationByCtxMenu");
        UTIL.clearAnimation(div.find("span"));
        log.printDbg("requestShow === VODPlayContext");
        requestShow();
    };

    VODPlayContext.prototype.hide = function () {
        _$(_$.find(".context_area.play_context_menu")[0].parentElement).removeClass("activate");
        this.div.removeClass("activate");
        this.closeAllDropBox();
        var div = _$(".pauseAnimationByCtxMenu").removeClass("pauseAnimationByCtxMenu").addClass("textAnimating");
        UTIL.startTextAnimation({
            targetBox: div
        });
        log.printDbg("notifyHide === VODPlayContext");
        notifyHide();
    };

    var ContextItem = function (id, _onSelectListener) {
        this.enable = true;
        this.onSelectListener = _onSelectListener;
        this.div = _$("<div/>", {id: "context_item_" + id, class: "context_item"});
        this.onKeyAction = function (keyCode) {
            return false;
        };
        this.getHeight = function () {
            return 0;
        };
        this.addClass = function () {
            this.div.addClass.apply(this, arguments)
        };
        this.removeClass = function () {
            this.div.removeClass.apply(this, arguments)
        };
    };

    var SepLine = function () {
        ContextItem.call(this, "sepLine");
        this.enable = true;
        this.noFocus = true;
        this.div.append(_$("<div/>", {class: "sep_line"}));
        this.getHeight = function () {
            return 18;
        }
    };
    SepLine.prototype = new ContextItem();

    var Button = function (id, text, onSelectListener) {
        ContextItem.call(this, id, onSelectListener);
        this.div.append(_$("<div/>", {class: "btn"}).text(text));
        this.onKeyAction = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    onSelectListener(id);
            }
        };

        this.addClass = function (classNm) {
            this.div.find(".btn").addClass(classNm)
        };
        this.removeClass = function (classNm) {
            this.div.find(".btn").removeClass(classNm)
        };

        this.getHeight = function () {
            return 78;
        }
    };
    Button.prototype = new ContextItem();

    var KidsButton = function (id, text, onSelectListener) {
        ContextItem.call(this, id, onSelectListener);
        this.div.append(_$("<div/>", {class: "title"}).text("키즈모드 설정"));
        this.div.append(_$("<div/>", {class: "btn"}).text(text));
        this.onKeyAction = function (keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    onSelectListener(id);
            }
        };

        this.addClass = function (classNm) {
            this.div.find(".btn").addClass(classNm)
        };
        this.removeClass = function (classNm) {
            this.div.find(".btn").removeClass(classNm)
        };

        this.getHeight = function () {
            return 134;
        }
    };
    KidsButton.prototype = new ContextItem();

    var DropBox = function (id, title, optionList, onSelectListener) {
        ContextItem.call(this, id, onSelectListener);
        var _thisDrop = this;
        var options;
        var indexList = [];
        var page = 0;
        var titleArea = _$("<div/>", {class: "title"}).text(title);

        this.div.append(titleArea);
        var div = _$("<div/>", {class: "drop_box"}).html("<span class='text'></span><div class='option_dw'/><div class='options'></div>");

        this.div.append(div);
        this.selectedIdx = 0;
        this.focusIdx = 0;

        this.setTitle = function (title) {
            titleArea.text(title);
        };

        this.setOption = function (optionList, _index) {
            if (optionList == null || optionList.length == 0) {
                return;
            }

            options = optionList;
            var optionArea = div.find(".options").empty();

            var optionBtn;
            var isLong = false;

            for (var i = 0; i < options.length; i++) {
                if (i < 5) {
                    isLong = UTIL.getTextLength(options[i]) > 210;
                    optionBtn = _$("<div/>", {class: 'option'});
                    optionBtn.append(_$("<div/>", {class: 'option_text'}).text(options[i]));
                    optionArea.append(optionBtn).toggleClass("long", isLong);
                }
                indexList[i] = i;
            }
            optionArea.append("<div class='focus_line'/>");

            if (options.length > 5) {
                optionArea.append("<div class='scroll_bg'><div class='scroll_bar'/></div>");
            }

            div.find(".scroll_bar").css("height", 317 / Math.ceil(options.length / 5) + "px");

            if (_index != null && _index >= 0) {
                this.setSelectedIdx(_index);
            }
        };

        function sortOption() {
            var isSetOption = false;
            if (_thisDrop.selectedIdx == null) return;
            for (var i = 0; i < options.length; i++) {
                if (i == _thisDrop.selectedIdx) {
                    indexList[0] = i;
                    isSetOption = true;
                } else {
                    indexList[isSetOption ? i : (i + 1)] = i;
                }
            }
            setPage(0);
        }

        function setPage(_page) {
            page = _page;
            div.find(".checked").removeClass("checked");
            var optionArea = div.find(".options .option");
            var i = 0;

            for (; i < (options.length - page * 5) && i < 5; i++) {
                // if ((page * 5) + i == 0) optionArea.eq(i).addClass("checked");
                if ((page * 5) + i == _thisDrop.selectedIdx) {
                    optionArea.eq(i).addClass("checked");
                }
                optionArea.eq(i).children(0).text(options[indexList[(page * 5) + i]]).toggleClass("long", UTIL.getTextLength(options[indexList[(page * 5) + i]]) > 220);
            }

            for (; i < 5; i++) {
                optionArea.eq(i).children(0).text("");
            }

            div.find(".scroll_bar").css("top", (317 / Math.ceil(options.length / 5) * page) + "px");
        }

        this.setSelectedIdx = function (index) {
            this.selectedIdx = index;
            div.find(".text").text(options[_thisDrop.selectedIdx]).toggleClass("long", UTIL.getTextLength(options[_thisDrop.selectedIdx]) > 220);
            // sortOption.call(this);
            // setPage(0);
            setPage(Math.floor(_thisDrop.selectedIdx / 5))
        };

        this.setFocus = function (focus) {
            div.find(".focus").removeClass("focus");
            _thisDrop.focusIdx = focus;
            if (Math.floor(_thisDrop.focusIdx / 5) != page) setPage(Math.floor(_thisDrop.focusIdx / 5));
            div.find(".option").eq(focus % 5).addClass("focus");
            div.find(".focus_line").css("-webkit-transform", "translateY(" + (63 * (_thisDrop.focusIdx % 5)) + "px)");
        };

        this.open = function () {
            div.addClass("open");
            this.setFocus(_thisDrop.selectedIdx);
        };

        this.close = function () {
            div.removeClass("open");
        };

        this.onKeyAction = function (keyCode) {
            if (div.hasClass("open")) {
                switch (keyCode) {
                    case KEY_CODE.UP:
                        this.setFocus(HTool.getIndex(this.focusIdx, -1, options.length));
                        return true;
                    case KEY_CODE.DOWN:
                        this.setFocus(HTool.getIndex(this.focusIdx, 1, options.length));
                        return true;
                    case KEY_CODE.ENTER:
                        if ((indexList[this.focusIdx] != this.selectedIdx) || id == "corner") {
                            this.setSelectedIdx(indexList[this.focusIdx]);
                            onSelectListener(id, this.selectedIdx);
                        } else if (indexList[this.focusIdx] == this.selectedIdx) onSelectListener(null);
                        this.close();
                        return true;
                    case KEY_CODE.BACK:
                    case KEY_CODE.CONTEXT:
                        this.close();
                        return true;
                    case KEY_CODE.EXIT:
                        this.close();
                        return true;
                    case KEY_CODE.LEFT:
                        return true;
                }
            } else if (keyCode == KEY_CODE.ENTER) {
                this.open();
                return true;
            }
        };

        this.addClass = function (classNm) {
            div.addClass(classNm)
        };
        this.removeClass = function (classNm) {
            div.removeClass(classNm)
        };

        this.getHeight = function () {
            return 145;
        };
        this.setOption(optionList);
        this.getOption = function () {
            return optionList;
        };
    };
    DropBox.prototype = new ContextItem();

    Object.defineProperty(VODPlayContext, "ITEM_TYPE", {
        value: {
            CORNER: 0,
            SMI: 1,
            VOICE_CHANGE: 2,
            BLUETOOTH_CHANGE: 3,
            BLUETOOTH_LISTEN: 4,
            SOUND_CHANGE: 5,
            SEPLINE: 6,
            SHOW_DETAIL: 7,
            VOD_CHANNEL: 8,
            BOOKMARK: 9,
            HOME_WIDGET: 10,
            KIDS_MODE: 11
        },
        writable: false
    });
})();