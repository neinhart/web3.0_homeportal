/**
 * Created by ksk91_000 on 2017-04-05.
 */
window.VODDetailContext = function () {
    var _this = this;
    var div = _$("<div/>", {class: 'context_menu_area vod_context_menu'});
    var contextMenu = _$("<div/>", {class: 'context_area vod_context_menu'});
    var vodArea = _$("<div/>", {class: "vod_info_area"});
    var contextArea = _$("<div/>", {class: "context_menu_area"});

    var vodInfo;
    var listener;
    var onFocusChangeListener;
    var menuList = [];
    var vodFocus = -1;
    var focus = 0;
    var recentlyVod = [];
    var callBackFunc = null;
    var openDeselectPopup = false;
    var wishFlag = false;

    var openedDropBox;

    var isKidsMode;

    div.append(_$("<div/>", {class: 'background_dim'}));
    div.append(_$("<div/>", {class: 'left_arrow'}));
    div.append(contextMenu);

    (function () {
        isKidsMode = window.KidsModeManager.isKidsMode();
        contextMenu.append(vodArea).append(contextArea);

        vodArea.html("<div class='poster_area'><img class='content_poster'><div class='icon_area'><img src='" + modulePath + "resource/detail/img_vod_locked_big.png'></div></div>" +
            "<div class='btn mini'>찜하기</div>" +
            "<div class='btn mini'>플레이리스트 추가</div>");
        if (isKidsMode) vodArea.find(".btn.mini:eq(0)").css("display", "none");
        else vodArea.find(".btn.mini:eq(0)").css("display", "block");
    })();

    this.open = function (_infoObj) {
        wishFlag = _infoObj.wishFlag;
        vodArea.find(".btn.mini").eq(0).text(wishFlag ? "찜해제" : "찜하기");
        openDeselectPopup = _infoObj.openDeselectPopup;
        setVodFocus(isKidsMode ? 1 : 0);
        div.addClass("activate");
        recentlyVod = JSON.parse(StorageManager.ms.load(StorageManager.KEY.RECENTLY_VOD)) || [];
        setRecentlyContents();
        // setFocus(focus);
        pauseTextAnimation();
    };

    this.close = function () {
        div.removeClass("activate");
        resumeTextAnimating();
    };

    this.isOpen = function () {
        return div.hasClass("activate");
    };

    this.clearContextMenu = function () {
        menuList = [];
        contextArea.empty();
    };

    this.addButton = function (title, classNm) {
        var item = _$("<div/>", {class: "btn " + (classNm ? classNm : "")}).text(title);
        menuList.push({item: item});
        contextArea.append(item);
    };

    this.addSeparateLine = function (classNm) {
        var item = _$("<div/>", {class: "sep_line " + (classNm ? classNm : "")});
        contextArea.append(item);
    };

    this.addTitle = function (title, classNm) {
        var item = _$("<div/>", {class: "title " + (classNm ? classNm : "")}).text(title);
        contextArea.append(item);
    };

    this.onFocusChange = function (eventListener) {
        onFocusChangeListener = eventListener;
    };

    this.setEventListener = function (eventListener) {
        listener = eventListener;
    };

    this.setVodInfo = function (options) {
        vodInfo = options;
        callBackFunc = vodInfo.callBackFunc;
        contextMenu.find(".content_poster").attr("src", vodInfo.imgUrl + "?w=198&h=240&quality=90");
        contextMenu.find(".icon_area").toggle(!AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(vodInfo.prInfo));
    };

    function setFocus(_focus) {
        if (menuList.length <= 0) {
            return;
        }

        vodFocus = -1;
        vodArea.find(".focus").removeClass("focus");

        if (menuList[focus]) {
            menuList[focus].item.removeClass("focus");
        }

        focus = _focus;
        menuList[focus].item.addClass("focus");

        // 다른화질구매 또는 미리보기 버튼에 포커스가 위치했을 때
        if (vodFocus < 0) {
            onFocusChangeListener(focus, function (title) {
                contextArea.find(".highLightTitle").eq(focus).text(title);
            });
        }
    }

    function setVodFocus(_focus) {
        vodFocus = _focus;

        if (menuList[focus]) {
            menuList[focus].item.removeClass("focus");
        }

        vodArea.find(".focus").removeClass("focus");

        switch (_focus) {
            case 0:
            case 1:
                vodArea.find(".btn").eq(vodFocus).addClass("focus");
                break;
            case 2:
            case 3:
                vodArea.find(".recently_content").eq(vodFocus - 2).addClass("focus");
                break;
        }
    }

    function setRecentlyContents() {
        var i = 0, contsArea = contextMenu.find(".recently_content img");

        for (i = 0; i < recentlyVod.length; i++) {
            contsArea.eq(i).show().attr("src", recentlyVod[i].imgUrl);
        }
        for (; i < 2; i++) {
            contsArea.eq(i).hide();
        }
    }

    this.onKeyAction = function (keyCode) {
        if (openedDropBox) return openedDropBox.onKeyAction(keyCode);
        switch (keyCode) {
            case KEY_CODE.CONTEXT:
            case KEY_CODE.BACK:
            case KEY_CODE.LEFT:
                this.close();
                return true;
            case KEY_CODE.ENTER:
                enterKeyAction();
                return true;
            case KEY_CODE.RIGHT:
                return true;
            default:
                if (vodFocus >= 0) {
                    return onKeyForVodMenu.call(this, keyCode);
                } else {
                    return onKeyForContextMenu.call(this, keyCode);
                }
        }
    };

    function onKeyForVodMenu(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                // 키즈모드ON에서 찜하기 버튼 비활성화된 상태에 포커스 안가도록 처리
                if (isKidsMode && vodFocus == 1) {
                    return false;
                }

                if (vodFocus > 0) {
                    setVodFocus(vodFocus - 1);
                }
                return true;
            case KEY_CODE.DOWN:
                if (vodFocus < 1) {
                    setVodFocus(1);
                } else {
                    setFocus(0);
                }
                return true;
        }
    }

    function onKeyForContextMenu(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (focus == 0) {
                    setVodFocus(1);
                } else {
                    setFocus(focus - 1);
                }
                return true;
            case KEY_CODE.DOWN:
                setFocus(Math.min(focus + 1, menuList.length - 1));
                return true;
        }
    }

    function enterKeyAction() {
        if (vodFocus >= 0) {
            switch (vodFocus) {
                case 0:
                    addWishList(vodInfo.itemType, vodInfo.catId, vodInfo.contsId, vodInfo.cmbYn);
                    _this.close();
                    return true;
                case 1:
                    LayerManager.activateLayer({
                        obj: {
                            id: "AddToMyPlayListPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            params: vodInfo
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                    _this.close();
                    return true;
            }
        } else {
            listener(focus);
            _this.close();
        }

        function addWishFail() {
            extensionAdapter.notifySNMPError("VODE-00013");
            HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
        }

        function setWishListFlag() {
            var familyHomeModule = ModuleManager.getModule("module.family_home");
            if (familyHomeModule) familyHomeModule.execute({method: "setWishListFlag"});
        }

        function addWishList(itemType, catId, contsId, cmbYn) {
            if (itemType == 1 || itemType == 0) catId = contsId;
            try {
                if (wishFlag) {
                    VODAmocManager.setPlayListNxt(1, itemType, contsId, "", 2, "", function (result, response) {
                        if (result) {
                            if (response.reqCode == 0) {
                                //정상삭제
                                var familyHomeModule = ModuleManager.getModule("module.family_home");

                                if (familyHomeModule) {
                                    familyHomeModule.execute({method: "setWishListFlag"});
                                }

                                showToast("VOD 찜을 해제하였습니다");
                                setWishListFlag();
                                vodArea.find(".btn.mini").eq(0).text("찜하기");

                                if (callBackFunc) {
                                    callBackFunc({wishFlag: false});
                                }
                            } else {
                                addWishFail();
                            }
                        } else {
                            addWishFail();
                        }
                    });
                } else {
                    VODAmocManager.setPlayListNxt(1, itemType, contsId, catId, 1, (cmbYn === "Y" || cmbYn === true) ? "Y" : "", function (result, response) {
                        if (result === true) {
                            if (response.reqCode == 0) {
                                // 정상등록
                                var familyHomeModule = ModuleManager.getModule("module.family_home");

                                if (familyHomeModule) {
                                    familyHomeModule.execute({method: "setWishListFlag"});
                                }

                                showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다");
                                setWishListFlag();
                                vodArea.find(".btn.mini").eq(0).text("찜해제");

                                if (callBackFunc) {
                                    callBackFunc({wishFlag: true});
                                }
                            } else if (response.reqCode == 1) {
                                if (openDeselectPopup) {
                                    LayerManager.activateLayer({
                                        obj: {
                                            id: "VODWishConfirm",
                                            type: Layer.TYPE.POPUP,
                                            priority: Layer.PRIORITY.POPUP,
                                            linkage: true,
                                            params: {
                                                callback: function () {
                                                    VODAmocManager.setPlayListNxt(1, itemType, contsId, "", 2, "", function (result, response) {
                                                        if (result) {
                                                            if (response.reqCode == 0) {
                                                                //정상삭제
                                                                var familyHomeModule = ModuleManager.getModule("module.family_home");
                                                                if (familyHomeModule) {
                                                                    familyHomeModule.execute({method: "setWishListFlag"});
                                                                }

                                                                showToast("VOD 찜을 해제하였습니다");
                                                                setWishListFlag();
                                                                vodArea.find(".btn.mini").eq(0).text("찜하기");

                                                                if (callBackFunc) {
                                                                    callBackFunc({wishFlag: false});
                                                                }
                                                            } else {
                                                                addWishFail();
                                                            }
                                                        } else {
                                                            addWishFail();
                                                        }
                                                    });
                                                }
                                            }
                                        },
                                        moduleId: "module.vod",
                                        visible: true
                                    });
                                } else {
                                    VODAmocManager.setPlayListNxt(1, itemType, contsId, catId, 2, (cmbYn === "Y" || cmbYn === true) ? "Y" : "", function (result, response) {
                                        if (result === true) {
                                            if (response.reqCode == 0) {
                                                //정상삭제
                                                var familyHomeModule = ModuleManager.getModule("module.family_home");

                                                if (familyHomeModule) {
                                                    familyHomeModule.execute({method: "setWishListFlag"});
                                                }

                                                showToast("VOD 찜을 해제하였습니다");
                                                setWishListFlag();

                                                vodArea.find(".btn.mini").eq(0).text("찜하기");

                                                if (callBackFunc) {
                                                    callBackFunc({wishFlag: false});
                                                }
                                            } else {
                                                addWishFail();
                                            }
                                        } else {
                                            addWishFail();
                                        }
                                    });
                                }
                            } else {
                                addWishFail();
                            }
                        } else {
                            addWishFail();
                        }
                    });
                }
            } catch (e) {
                log.printErr(e);
                addWishFail();
            }
        }
    }

    var showToast = function (string) {
        LayerManager.activateLayer({
            obj: {
                id: "VODToast",
                type: Layer.TYPE.BACKGROUND,
                priority: Layer.PRIORITY.REMIND_POPUP,
                linkage: true,
                params: {text: string}
            },
            moduleId: "module.vod",
            visible: true
        })
    };

    this.removeFocus = function () {
        menuList[focus].item.removeClass("focus");
    };

    this.addFocus = function () {
        menuList[focus].item.addClass("focus");
    };

    this.getView = function () {
        return div;
    };

    function pauseTextAnimation() {
        var div = _$(".textAnimating").removeClass("textAnimating").addClass("pauseAnimationByCtxMenu");
        UTIL.clearAnimation(div.find("span"));
    }

    function resumeTextAnimating() {
        var div = _$(".pauseAnimationByCtxMenu").removeClass("pauseAnimationByCtxMenu").addClass("textAnimating");
        UTIL.startTextAnimation({
            targetBox: div
        })
    }
};