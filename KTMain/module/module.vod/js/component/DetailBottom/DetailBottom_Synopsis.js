/**
 * Created by Lazuli on 2017-02-15.
 */

window.DetailBottom_Synopsis = function(_main, _contsId, _contentInfo) {
    var _this = this;
    var main = _main;
    var synopsis = null, currentPage = 0, totalPage = 0, indicator = null;
    var contentInfo;

    this.isNoData = false;
    this.getTotalPage = function() {return totalPage;};
    this.focusIn = function() {
        movePageSynopsis();
        if (indicator) indicator.focused();
    };

    this.focusOut = function() {
        currentPage = 0;
        movePageSynopsis();
        main.changeMenuFocus(0, null);
        if (indicator) indicator.blurred();
    };

    this.keyEvent = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                if (currentPage == 0) {
                    try {
                        NavLogMgr.collectVODDetail(keyCode, contentInfo.catId, contentInfo.contsId,
                            contentInfo.contsName, NLC.DETAIL_SYNOPL, "");
                    } catch (e) {}
                    _this.focusOut();
                }
                else {
                    currentPage--;
                    movePageSynopsis();
                }
                return true;
            case KEY_CODE.DOWN:
                if (currentPage < totalPage - 1) currentPage++;
                else currentPage = 0;
                movePageSynopsis();
                return true;
            case KEY_CODE.BACK:
                _this.focusOut();
                return true;
            case KEY_CODE.RIGHT :
            case KEY_CODE.LEFT :
                return true;
            case KEY_CODE.ENTER:
                return true;
        }
        return false;
    };

    function movePageSynopsis() {
        main.div.find(".tabDetailArea.SYNOPSIS .synopsisArea .synopsis").css("-webkit-transform", "translateY(" + (currentPage * -314) + "px)");
        if (indicator) indicator.setPos(currentPage);
    }

    function setNoData() {
        _this.isNoData = true;
        main.div.find(".tabDetailArea.SYNOPSIS").append(_$(
            "<div class='noData'>" +
            // [5월 형상] No Data 아이콘 삭제
            // "<div class='icon'></div>" +
            "<span class='text'>줄거리 더보기 정보가 없습니다</span>" +
            "</div>"
        ));
    }

    this.getTabID = function() { return NLC.DETAIL_SYNOPL_TAB; }
    this.getTabStr = function() { return "상세줄거리더보기 탭";}

    this.destroy = function() {
        _this = main = synopsis = currentPage = totalPage = null;
    };

    function onCreate() {
        try {
            contentInfo = _contentInfo;
            VODAmocManager.getDetailContentInfo(_contsId, 1, function(result, data) {
                log.printDbg(JSON.stringify(data));
                if (!result) {
                    extensionAdapter.notifySNMPError("VODE-00013");
                    setNoData();
                } else if (!data.synopsisL || data.synopsisL.length == 0) setNoData();
                else {
                    synopsis = data.synopsisL;
                    currentPage = 0;
                    main.div.find(".tabDetailArea.SYNOPSIS").append(_$(
                        "<span class='synopsisAreaHidden'>" + HTool.convertTextToHtml(synopsis) + "</span>" +
                        "<div class='synopsisArea'>" +
                        "<span class='synopsis'>" + HTool.convertTextToHtml(synopsis) + "</span>" +
                        // "<span class='synopsisAreaHidden'>" + synopsis.replace(/\n/g, '<br>') + "</span>" +
                        // "<div class='synopsisArea'>" +
                        // "<span class='synopsis'>" + synopsis.replace(/\n/g, '<br>') + "</span>" +
                        "</div>"
                    ));
                    totalPage = Math.ceil(main.div.find(".tabDetailArea.SYNOPSIS .synopsisAreaHidden")[0].offsetHeight / 314);
                    log.printDbg("synopsisTotalPage = " + totalPage);
                    main.div.find(".tabDetailArea.SYNOPSIS .synopsisArea .synopsis").css("-webkit-transform", "translateY(" + (currentPage * -314) + "px)");
                    if (totalPage > 1) {
                        indicator = new Indicator(3);
                        indicator.setSize(totalPage, 0);
                        main.div.find(".tabDetailArea.SYNOPSIS").append(_$("<div/>", {class: "indicator"}));
                        main.div.find(".tabDetailArea.SYNOPSIS .indicator").append(indicator.getView());
                    }
                }
            });
        } catch (e) {
            log.printErr(e);
            setNoData();
            extensionAdapter.notifySNMPError("VODE-00013");
        }
    }

    onCreate();
};