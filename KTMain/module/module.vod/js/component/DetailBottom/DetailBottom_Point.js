/**
 * Created by Lazuli on 2017-02-15.
 */

window.DetailBottom_Point = function (_main, _contsId, _isKids, _contentInfo) {
    var _this = this;
    var main = _main;
    var isKidsType = _isKids;
    var point = null;
    var ollehBox = null;
    var focusIdx = 0, totalIdx = 1, currentPage = 0;
    var ratingText = ["안봤어요", "최악이에요", "괜히 봤어요", "재미없어요", "별로예요", "아쉬워요", "보통이에요", "괜찮아요", "재미있어요", "정말좋아요", "최고예요"];
    var contentInfo;

    this.focusIn = function () {
        main.div.find(".tabDetailArea.POINT .starArea").css("transition", "-webkit-transform .5s");
        moveFocus();
    };

    this.focusOut = function () {
        focusIdx = 0;
        currentPage = 0;
        moveFocus();
        main.div.find(".tabDetailArea.POINT .starArea .starDiv").removeClass("focus");
    };

    this.keyEvent = function (keyCode) {

        try { // 통계처리
            if (focusIdx == 0) {
                NavLogMgr.collectVODDetail(keyCode, contentInfo.catId, contentInfo.contsId, contentInfo.contsName,
                    NLC.DETAIL_RATING, (point.ollehPoint.split("|")[2]) + "|" + (point.myPoint * 1.0));
            } else {
                NavLogMgr.collectVODDetail(keyCode, contentInfo.catId, contentInfo.contsId, contentInfo.contsName,
                    NLC.DETAIL_RATING, (point.expert[focusIdx - 1].split("|")[3]));
            }
        } catch (e) {
        }

        switch (keyCode) {
            case KEY_CODE.DOWN:
                return true;
            case KEY_CODE.UP:
            case KEY_CODE.BACK:
                main.div.find(".tabDetailArea.POINT .starArea").css("transition", "-webkit-transform 0s");
                main.changeMenuFocus(0, null);
                _this.focusOut();
                return true;
            case KEY_CODE.LEFT:
                focusIdx = HTool.indexMinus(focusIdx, totalIdx);
                moveFocus();
                return true;
            case KEY_CODE.RIGHT:
                focusIdx = HTool.indexPlus(focusIdx, totalIdx);
                moveFocus();
                return true;
            case KEY_CODE.ENTER:
                if (focusIdx == 0) {
                    LayerManager.activateLayer({
                        obj: {
                            id: "VodPointPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                isKids: isKidsType,
                                ollehTitle: point.ollehPoint ? point.ollehPoint.split("|")[0] : "",
                                ollehPoint: point.ollehPoint ? [point.ollehPoint.split("|")[1], point.ollehPoint.split("|")[2]] : null,
                                myPoint: point.myPoint,
                                contsId: _contsId,
                                callback: function (_point) {
                                    ollehBox.empty();
                                    point.myPoint = _point;
                                    setOllehBox();
                                }
                            }
                        },
                        moduleId: "module.vod",
                        new: true,
                        visible: true
                    });
                }
                return true;
        }
        return false;
    };

    function moveFocus() {
        main.div.find(".tabDetailArea.POINT .starArea .starDiv.focus").removeClass("focus");
        main.div.find(".tabDetailArea.POINT .starArea .starDiv").eq(focusIdx).addClass("focus");
        currentPage = Math.floor(focusIdx / 3);
        main.div.find(".tabDetailArea.POINT .starArea .starDiv.expert").css("opacity", "1");
        main.div.find(".tabDetailArea.POINT .rightDim").hide();
        if (currentPage == 2) {
            main.div.find(".tabDetailArea.POINT .starArea .starDiv.expert").eq(4).css("opacity", "0");
            main.div.find(".tabDetailArea.POINT .starArea").css("-webkit-transform", "translateX(-3095px)");
        }
        else {
            if (currentPage == 0) {
                main.div.find(".tabDetailArea.POINT .starArea").css("-webkit-transform", "translateX(0px)");
                if (point.expert.length > 2) main.div.find(".tabDetailArea.POINT .rightDim").show();
            }
            else if (currentPage == 1) {
                main.div.find(".tabDetailArea.POINT .starArea .starDiv.expert").eq(1).css("opacity", "0");
                main.div.find(".tabDetailArea.POINT .starArea").css("-webkit-transform", "translateX(-1496px)");
                if (point.expert.length > 5) main.div.find(".tabDetailArea.POINT .rightDim").show();
            }
        }
    }

    function setNoData() {
        ollehBox.append(_$("<div/>", {class: "myArea"}).css("top", "-76px"));
        ollehBox.find(".myArea").append(_$("<span/>", {class: "myPointText"}).text("얼마나 만족했는지"));
        ollehBox.find(".myArea").append(_$("<span/>", {class: "myPointText second"}).text("별점으로 평가해주세요"));
        ollehBox.find(".myArea").append(_$("<div/>", {class: "myPointButton"}));
        ollehBox.find(".myArea .myPointButton").append(_$("<span/>", {class: "btnText sdw"}).text("별점 평가하기"));
        ollehBox.find(".myArea .myPointButton").append(_$("<span/>", {class: "btnText"}).text("별점 평가하기"));
    }

    function makeOllehArea() {
        var src = [];
        src.push(ROOT_URL + "resource/detail/rating/rating_more_number_" + point.ollehPoint.split("|")[2].split(".")[0] + ".png");
        src.push(ROOT_URL + "resource/detail/rating/rating_more_number_" + (point.ollehPoint.split("|")[2].split(".")[1] || 0) + ".png");
        src.push(ROOT_URL + "resource/detail/rating/rating_more_number_f_" + point.ollehPoint.split("|")[2].split(".")[0] + ".png");
        src.push(ROOT_URL + "resource/detail/rating/rating_more_number_f_" + (point.ollehPoint.split("|")[2].split(".")[1] || 0) + ".png");
        ollehBox.append(_$("<div/>", {class: "mLine olleh"}));
        ollehBox.append(_$("<div/>", {class: "ollehTextArea"}));
        ollehBox.find(".ollehTextArea").append(_$("<span/>", {class: "ollehText"}).text(point.ollehPoint.split("|")[0]));
        ollehBox.find(".ollehTextArea").append(_$("<span/>", {class: "ollehRatingCnt"}).text("(" + point.ollehPoint.split("|")[1] + "명 참여)"));
        ollehBox.append(_$("<span/>", {class: "ratingText"}).text(ratingText[Math.ceil(parseFloat(point.ollehPoint.split("|")[2]) * 2)]));
        ollehBox.append(_$("<div/>", {class: "ratingNumber"}).css({
            'left': '219px',
            "background-image": "url(" + src[0] + ")"
        }));
        ollehBox.append(_$("<div/>", {class: "ratingNumber"}).css({
            'left': '278px',
            "background-image": "url(" + src[1] + ")"
        }));
        ollehBox.append(_$("<div/>", {class: "ratingNumber focus"}).css({
            'left': '219px',
            "background-image": "url(" + src[2] + ")"
        }));
        ollehBox.append(_$("<div/>", {class: "ratingNumber focus"}).css({
            'left': '278px',
            "background-image": "url(" + src[3] + ")"
        }));
        ollehBox.append(_$("<div/>", {class: "ratingNumber dot"}));
        ollehBox.append(_$("<div/>", {class: "ratingStarArea"}));
        ollehBox.find(".ratingStarArea").append(stars.getView(stars.TYPE.RED_26));
        stars.setRate(ollehBox.find(".ratingStarArea"), point.ollehPoint.split("|")[2]);
    }

    function setOllehBox() {
        var src = [];
        if (point.myPoint != null) {
            if (!point.ollehPoint) ollehBox.append(_$("<div/>", {class: "myArea"}).css("top", "-107px"));
            else {
                makeOllehArea();
                ollehBox.append(_$("<div/>", {class: "myArea"}));
            }
            point.myPoint = point.myPoint.toString();
            src = [];
            src.push(ROOT_URL + "resource/detail/rating/rating_more_my_number_" + point.myPoint.split(".")[0] + ".png");
            src.push(ROOT_URL + "resource/detail/rating/rating_more_my_number_" + (point.myPoint.split(".")[1] || 0) + ".png");
            src.push(ROOT_URL + "resource/detail/rating/rating_more_my_number_f_" + point.myPoint.split(".")[0] + ".png");
            src.push(ROOT_URL + "resource/detail/rating/rating_more_my_number_f_" + (point.myPoint.split(".")[1] || 0) + ".png");
            ollehBox.find(".myArea").append(_$("<span/>", {class: "ollehText my"}).text("나의 별점"));
            ollehBox.find(".myArea").append(_$("<span/>", {class: "ratingText my"}).text(ratingText[Math.ceil(parseFloat(point.myPoint) * 2)]));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "ratingNumber my"}).css({
                'left': '219px',
                "background-image": "url(" + src[0] + ")"
            }));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "ratingNumber my"}).css({
                'left': '278px',
                "background-image": "url(" + src[1] + ")"
            }));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "ratingNumber my focus"}).css({
                'left': '219px',
                "background-image": "url(" + src[2] + ")"
            }));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "ratingNumber my focus"}).css({
                'left': '278px',
                "background-image": "url(" + src[3] + ")"
            }));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "ratingNumber my dot"}));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "ratingStarArea my"}));
            ollehBox.find(".ratingStarArea.my").append(stars.getView(stars.TYPE.YELLOW_26));
            stars.setRate(ollehBox.find(".ratingStarArea.my"), point.myPoint);
            ollehBox.find(".mLine").addClass("hasMyPoint");
        } else {
            if (!point.ollehPoint) ollehBox.append(_$("<div/>", {class: "myArea"}).css("top", "-76px"));
            else {
                makeOllehArea();
                ollehBox.append(_$("<div/>", {class: "myArea"}));
            }
            ollehBox.find(".myArea").append(_$("<span/>", {class: "myPointText"}).text("얼마나 만족했는지"));
            ollehBox.find(".myArea").append(_$("<span/>", {class: "myPointText second"}).text("별점으로 평가해주세요"));
            ollehBox.find(".myArea").append(_$("<div/>", {class: "myPointButton"}));
            ollehBox.find(".myArea .myPointButton").append(_$("<span/>", {class: "btnText sdw"}).text("별점 평가하기"));
            ollehBox.find(".myArea .myPointButton").append(_$("<span/>", {class: "btnText"}).text("별점 평가하기"));
            ollehBox.find(".mLine").removeClass("hasMyPoint");
        }
    }

    function setData() {
        totalIdx = 1;
        if (point.expert) {
            for (var i = 0; i < point.expert.length; i++) {
                main.div.find(".tabDetailArea.POINT .starArea").append(_$(
                    "<div class='starDiv expert whiteStarBox'>" +
                    "<div class='starBox expert'>" +
                    "<img class='expertIcon'/>" +
                    "<span class='expert'>" + point.expert[i].split("|")[0] + "</span>" +
                    "<span class='expertName'>" + point.expert[i].split("|")[2] + "</span>" +
                    "<div class='mLine'/>" +
                    "<div class='ratingStarArea'/>" +
                    "<span class='expertRatingNumber'>" + Number(point.expert[i].split("|")[3]).toFixed(1) + "</span>" +
                    "<span class='detailText'>" + point.expert[i].split("|")[4] + "</span>" +
                    "</div></div>"
                ));
                main.div.find(".tabDetailArea.POINT .starArea .starBox.expert .ratingStarArea").eq(i).append(stars.getView(stars.TYPE.MINT_20));
                stars.setRate(main.div.find(".tabDetailArea.POINT .starArea .starBox.expert .ratingStarArea").eq(i), point.expert[i].split("|")[3]);
            }
            totalIdx += point.expert.length;
        }
        if (totalIdx > 3) main.div.find(".tabDetailArea.POINT").append(_$("<div/>", {class: "rightDim"}));
        setOllehBox();
    }

    this.getTabID = function () {
        return NLC.DETAIL_RATING_TAB;
    };
    this.getTabStr = function () {
        return "평점더보기 탭";
    };

    this.destroy = function () {

    };

    function onCreate() {
        try {
            var finishCnt = 0;

            function onFinished() {
                if (finishCnt == 2) setData();
            }

            main.div.find(".tabDetailArea.POINT").append(_$("<div/>", {class: "starArea"}));
            main.div.find(".tabDetailArea.POINT .starArea").append(_$("<div class='starDiv olleh whiteStarBox'><div class='starBox olleh'/></div>"));
            ollehBox = main.div.find(".tabDetailArea.POINT .starArea .starBox.olleh");
            point = {};
            point.expert = [];
            contentInfo = _contentInfo;
            VODAmocManager.getDetailContentInfo(_contsId, 2, function (result, data) {
                log.printDbg(JSON.stringify(data));
                if (result) {
                    if (data.ollehPoint && data.ollehPoint.length > 0) point.ollehPoint = data.ollehPoint;
                    if (data.expert1Point && data.expert1Point.length > 0) point.expert.push(data.expert1Point);
                    if (data.expert2Point && data.expert2Point.length > 0) point.expert.push(data.expert2Point);
                    if (data.expert3Point && data.expert3Point.length > 0) point.expert.push(data.expert3Point);
                    if (data.expert4Point && data.expert4Point.length > 0) point.expert.push(data.expert4Point);
                    if (data.expert5Point && data.expert5Point.length > 0) point.expert.push(data.expert5Point);
                    if (data.expert6Point && data.expert6Point.length > 0) point.expert.push(data.expert6Point);
                    log.printDbg(JSON.stringify(point));
                    data = null;
                }
                finishCnt++;
                onFinished();
            });
            curationManager.getMyRating(StorageManager.ps.load(StorageManager.KEY.RECOMMEND_ADULT_LIMIT) != 1 ? "Y" : "N", _contsId, function (result, response) {
                log.printDbg(JSON.stringify(response));
                if (result && response.RESULT_CODE == 200) {
                    point.myPoint = response.RATING.toString();
                } else {
                    point.myPoint = null;
                }
                finishCnt++;
                onFinished();
            });
        } catch (e) {
            log.printErr(e);
            setNoData();
        }
    }

    onCreate();
};