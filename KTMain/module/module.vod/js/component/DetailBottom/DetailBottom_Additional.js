/**
 * Created by Lazuli on 2017-02-15.
 */

window.DetailBottom_Additional = function(_main, _contsId, _catId, _contentInfo, _additionalInfo) {
    var _this = this;
    var main = _main;
    var additionalData = null;
    var focusIdx = 0, page = 0, imgArray = [];
    var contentInfo;

    this.isNoData = false;
    this.focusIn = function() {
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea").css("transition", "-webkit-transform .5s");
        focusIdx = page = 0;
        setFocus();
    };

    this.focusOut = function() {
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .additional.focus").removeClass("focus");
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea").css("-webkit-transform", "translateX(0px) translateZ(0)");
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea").css("transition", "-webkit-transform 0");
        if (Math.ceil(additionalData.length / 3) > 1) main.div.find(".tabDetailArea.ADDITIONAL .rightDim").show();
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .additional").css("opacity", "1");
        main.changeMenuFocus(0, null);
    };

    this.keyEvent = function(keyCode) {

        try {
            // 통계처리
            NavLogMgr.collectVODDetail(keyCode, contentInfo.catId, contentInfo.contsId || contentInfo.itemId,
                contentInfo.contsName || contentInfo.itemName, NLC.DETAIL_PREVIEW,
                (additionalData[focusIdx].contsType == "3" ? additionalData[focusIdx].imgUrl : additionalData[focusIdx].contsId));
        } catch (e) {
        }

        switch (keyCode) {
            case KEY_CODE.DOWN:
                return true;
            case KEY_CODE.UP:
                _this.focusOut();
                return true;
            case KEY_CODE.LEFT:
                focusIdx = HTool.indexMinus(focusIdx, additionalData.length);
                setFocus();
                return true;
            case KEY_CODE.RIGHT:
                focusIdx = HTool.indexPlus(focusIdx, additionalData.length);
                setFocus();
                return true;
            case KEY_CODE.ENTER:
                if (imgArray.length > 0 && additionalData[focusIdx].contsType == "3") openStillPopup();
                else {
                    if (UTIL.isLimitAge(contentInfo.prInfo)) {
                        openAdultAuthPopup(function(res) {
                            if (res) VODManager.requestAdditionalContents(_catId, additionalData[focusIdx].contsId, additionalData[focusIdx].displayText, "62", additionalData[focusIdx].contsType == 2, _additionalInfo);
                        }, true)
                    } else VODManager.requestAdditionalContents(_catId, additionalData[focusIdx].contsId, additionalData[focusIdx].displayText, "62", additionalData[focusIdx].contsType == 2, _additionalInfo);
                }
                return true;
        }
        return false;
    };

    function openStillPopup() {
        LayerManager.activateLayer({
            obj: {
                id: "AdditionalStill",
                type: Layer.TYPE.POPUP,
                priority: Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    imgList: imgArray,
                    index: focusIdx - (additionalData.length - imgArray.length)
                }
            },
            moduleId: "module.vod",
            new: true,
            visible: true
        });
    }

    function setFocus() {
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .additional.focus").removeClass("focus");
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .additional").eq(focusIdx).addClass("focus");
        page = Math.floor(focusIdx / 3);
        main.div.find(".tabDetailArea.ADDITIONAL .rightDim").hide();
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .additional").css("opacity", "1");
        main.div.find(".tabDetailArea.ADDITIONAL .additionalArea").css("-webkit-transform", "translateX(" + (page * -1563) + "px) translateZ(0)");
        if (page + 1 < Math.ceil(additionalData.length / 3)) main.div.find(".tabDetailArea.ADDITIONAL .rightDim").show();
        if (page > 0) main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .additional").eq(page * 3 - 2).css("opacity", "0");
    }

    function setData() {
        main.div.find(".tabDetailArea.ADDITIONAL").append(_$("<div/>", {class: "additionalArea"}));
        for (var i = 0; i < additionalData.length; i++) {
            main.div.find(".tabDetailArea.ADDITIONAL .additionalArea").append(_$(
                "<div class='additional'>" +
                "<div class='addThumb'>" +
                "<img src='" + additionalData[i].imgUrl + "' class='thumbImage' onerror='this.src=\"" + modulePath + "resource/default_thumb.jpg\"'>" +
                "<div class='thumbDim'/>" +
                (parseInt(additionalData[i].contsType) < 3 ? "<img class='playIcon'><img class='lockIcon'>" : "") +
                "<span class='addName'>" + additionalData[i].displayText + "</span>" +
                "</div>" +
                "</div>"
            ));
            if (additionalData[i].contsType == "3") imgArray.push(additionalData[i].imgUrl);
        }
        if (!main.isAuthorizedContent() && !AdultAuthorizedCheck.isAdultAuthorized() && UTIL.isLimitAge(_contentInfo.prInfo)) main.div.find(".tabDetailArea.ADDITIONAL .additionalArea .lockIcon").show();
        if (Math.ceil(additionalData.length / 3) > 1) main.div.find(".tabDetailArea.ADDITIONAL").append(_$("<div/>", {class: "rightDim"}));
    }

    function setNoData() {
        _this.isNoData = true;
        main.div.find(".tabDetailArea.ADDITIONAL").append(_$(
            "<div class='noData'>" +
            // [5월 형상] No Data 아이콘 삭제
            // "<div class='icon'></div>" +
            "<span class='text'>예고편/부가영상 정보가 없습니다</span>" +
            "</div>"
        ));
    }

    this.getTabID = function() { return NLC.DETAIL_PREVIEW_TAB; }
    this.getTabStr = function() { return "예고편/부가영상 탭";}

    this.destroy = function() {

    };

    function onCreate() {
        try {
            if (!_contsId) setNoData();
            else {
                contentInfo = _contentInfo;
                VODAmocManager.getAdditionalInfoW3(function(result, data) {
                    log.printDbg(JSON.stringify(data));
                    additionalData = data.seriesContsList;
                    if (additionalData && Object.prototype.toString.call(additionalData) == "[object Object]") additionalData = [additionalData];
                    if (!result) {
                        extensionAdapter.notifySNMPError("VODE-00013");
                        setNoData();
                    } else if (!additionalData || additionalData.length == 0) setNoData();
                    else {
                        additionalData = additionalData.sort(function(a, b) {return (a.contsType > b.contsType) ? 1 : ((b.contsType > a.contsType) ? -1 : 0);});
                        setData();
                    }
                }, _contsId);
            }
        } catch (e) {
            log.printErr(e);
            extensionAdapter.notifySNMPError("VODE-00013");
            setNoData();
        }
    }

    onCreate();
};