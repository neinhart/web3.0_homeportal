/**
 * Created by Lazuli on 2017-02-15.
 */

window.DetailBottom_Search = function(_main, _contentInfo) {
    var _this = this;
    var main = _main, data = _contentInfo;
    var personLength = 0, focusIdx = 0, focusPage = 0;

    this.isNoData = false;
    this.focusIn = function() {
        main.div.find(".tabDetailArea.SEARCH .personArea").css("transition", "-webkit-transform .5s");
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox.focus").removeClass("focus");
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox").eq(focusIdx).addClass("focus");
        main.div.find(".tabDetailArea.SEARCH .personArea").css("-webkit-transform", "translateX(" + (focusPage * -1620) + "px) translateZ(0)");
    };

    this.focusOut = function() {
        main.div.find(".tabDetailArea.SEARCH .personArea").css("transition", "-webkit-transform 0s");
        main.changeMenuFocus(0);
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox.focus").removeClass("focus");
        focusIdx = focusPage = 0;
        main.div.find(".tabDetailArea.SEARCH .personArea").css("-webkit-transform", "translateX(0px) translateZ(0)");
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox").css("opacity", "1");
        if (focusPage < Math.floor(personLength / 7)) main.div.find(".tabDetailArea.SEARCH .rightDim").show();
    };

    function setFocus() {
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox.focus").removeClass("focus");
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox").eq(focusIdx).addClass("focus");
        focusPage = Math.floor((focusIdx + 1) / 7);
        main.div.find(".tabDetailArea.SEARCH .rightDim").hide();
        main.div.find(".tabDetailArea.SEARCH .personArea .personBox").css("opacity", "1");
        main.div.find(".tabDetailArea.SEARCH .personArea").css("-webkit-transform", "translateX(" + (focusPage * -1620) + "px) translateZ(0)");
        if (focusPage > 0) main.div.find(".tabDetailArea.SEARCH .personArea .personBox").eq(focusPage * 7 - 2).css("opacity", "0");
        if (focusPage < Math.floor(personLength / 7)) main.div.find(".tabDetailArea.SEARCH .rightDim").show();
    }

    this.keyEvent = function(keyCode) {

        try { // 통계처리
            NavLogMgr.collectVODDetail(keyCode, data.catId, data.contsId, data.contsName,
                NLC.DETAIL_CHARA_SEARCH, main.div.find(".tabDetailArea.SEARCH .personArea .personBox.focus .person .personName").text());
        } catch(e) {}

        switch (keyCode) {
            case KEY_CODE.DOWN:
                return true;
            case KEY_CODE.BACK:
            case KEY_CODE.UP:
                _this.focusOut();
                return true;
            case KEY_CODE.LEFT:
                focusIdx = HTool.indexMinus(focusIdx, personLength);
                setFocus();
                return true;
            case KEY_CODE.RIGHT:
                focusIdx = HTool.indexPlus(focusIdx, personLength);
                setFocus();
                return true;
            case KEY_CODE.ENTER:
                moveSearch(main.div.find(".tabDetailArea.SEARCH .personArea .personBox.focus .person .personName").text());
                return true;
        }
        return false;
    };

    function moveSearch(_word) {
        _word = HTool.wordTrim(_word);
        var a = ModuleManager.getModule("module.search");
        if (!a) {
            ModuleManager.loadModule({
                moduleId: "module.search",
                cbLoadModule: function(success) {
                    log.printDbg(success);
                    if (success) moveSearch(_word);
                }
            });
            return;
        }
        a.execute({
            method: "searchVOD",
            params: {
                srchword: _word,
                append: false,
                srchopt: 10,
                req_cd: main.reqPathCd
            }
        })
    }

    function pushPersonElement(_name, _type) {
        main.div.find(".tabDetailArea.SEARCH .personArea").append(_$(
            "<div class='personBox'>" +
            (_name.replace(/\s/g, '').length > 6 ? "<div class='person overline'>" : "<div class='person'>") +
            "<span class='personName'>" + _name + "</span>" +
            "<span class='personType'>" + _type + "</span>" +
            "<div class='line'></div>" +
            "<div class='icon'></div>" +
            "</div>" +
            "</div>"
        ));
    }

    function setNoData() {
        _this.isNoData = true;
        main.div.find(".tabDetailArea.SEARCH").append(_$(
            "<div class='noData'>" +
            // [5월 형상] No Data 아이콘 삭제
            // "<div class='icon'></div>" +
            "<span class='text'>인물 정보가 없습니다</span>" +
            "</div>"
        ));
    }

    this.getTabID = function() { return NLC.DETAIL_CHARA_SEARCH_TAB; }
    this.getTabStr = function() { return "인물검색 탭";}

    this.destroy = function() {
        _this = main = data = personLength = focusIdx = focusPage = null;
    };

    function onCreate() {
        if (!data.overseerName && !data.play && !data.actor) setNoData();
        else {
            var overseerName = data.overseerName ? data.overseerName.split(",") : [];
            var play = data.play ? data.play.split(",") : [];
            var actor = data.actor ? data.actor.split(",") : [];
            var i = 0;
            personLength = overseerName.length + play.length + actor.length;
            if (personLength == 0) {
                setNoData();
                return;
            }
            main.div.find(".tabDetailArea.SEARCH").append(_$("<div/>", {class: "personArea"}));
            for (; i < overseerName.length; i++) {
                if (overseerName[i].replace(/\s/g, '')) pushPersonElement(overseerName[i], "감독");
                else personLength--;
            }
            for (i = 0; i < play.length; i++) {
                if (play[i].replace(/\s/g, '')) pushPersonElement(play[i], "각본");
                else personLength--;
            }
            for (i = 0; i < actor.length; i++) {
                if (actor[i].replace(/\s/g, '')) pushPersonElement(actor[i], "출연");
                else personLength--;
            }
            if (personLength > 6) main.div.find(".tabDetailArea.SEARCH").append(_$("<div/>", {class: "rightDim"}));
        }
    }

    onCreate();
};