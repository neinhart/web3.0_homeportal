/**
 * Created by Lazuli on 2017-02-15.
 */

window.DetailBottom_Recommend = function(_main, _contentInfo, _isAppGame) {
    var _this = this;
    var main = _main, recommendDiv = null;
    var recommendData = null;
    var focusIdx = 0, focusRow = 0, recPage = 0, maxPage = [];
    var contentInfo;

    this.isNoData = false;
    this.focusIn = function() {
        console.debug(main);
        focusIdx = recPage = 0;
        recommendDiv.find(".posterList").addClass("focus");
        recommendDiv.find(".posterList li").removeClass("focus");
        recommendDiv.find(".posterList.focus li#" + recommendData[focusRow].itemList[focusIdx].ITEM_ID + "_" + focusIdx).addClass("focus");
        main.div.find(".tabDetailArea.RECOMMEND .etcInfo").addClass("focus");
        changeFocus();
    };

    this.focusOut = function() {
        if (focusRow != 0) {
            focusRow = 0;
            changePage();
        } else {
            var list = recommendDiv.find(".posterList");
            if (list.hasClass("moved")) {
                list.css("transition", "-webkit-transform 0");
                list.removeClass("moved");
                list[0].offsetWidth;
                list.css("transition", "-webkit-transform .5s");
            }
            setRollingPage(0, 1);
        }
        recommendDiv.find(".posterList").removeClass("focus");
        recommendDiv.find(".posterList li").removeClass("focus");
        main.div.find(".tabDetailArea.RECOMMEND .etcInfo").removeClass("focus");
        main.changeMenuFocus(0);
        clearTextAnimation();
    };

    function changePage() {
        constructDom();
        recommendDiv.find(".posterList").addClass("focus");
        focusIdx = recPage = 0;
        changeFocus();
    }

    function setRollingPageData(list, idx) {
        list.removeClass("hide");
        list.attr("id", recommendData[focusRow].itemList[idx].ITEM_ID + "_" + idx);
        list.find(".posterImg.poster").attr("src", recommendData[focusRow].itemList[idx].IMG_URL + HTool.addPosterOption(182, 260, 90));
        list.find(".posterTitle span").text(recommendData[focusRow].itemList[idx].ITEM_NAME);
        if (HTool.isTrue(recommendData[focusRow].itemList[idx].WON_YN)) {
            list.find("img.isfreeIcon").css("display", "inherit");
            list.find("span.isfree").css("display", "none");
        } else if (HTool.isFalse(recommendData[focusRow].itemList[idx].WON_YN)) {
            list.find("span.isfree").css("display", "inherit");
            list.find("span.isfree").text("무료");
            list.find("img.isfreeIcon").css("display", "none");
        } else {
            list.find("img.isfreeIcon").css("display", "none");
            list.find("span.isfree").css("display", "none");
        }

        list.find(".age").attr("src", ROOT_URL + "resource/detail/icon_age_list_" + ((recommendData[focusRow].itemList[idx].RATING - 0) > 0 ? (recommendData[focusRow].itemList[idx].RATING - 0) : "all" ) + ".png");
        list.find(".stars").empty();
        setPosterIcon(list, recommendData[focusRow].itemList[idx]);

        if (main.isAuthorizedContent() || !UTIL.isLimitAge(recommendData[focusRow].itemList[idx].RATING)) list.find("img.posterLockImg").css("display", "none");
        else list.find("img.posterLockImg").css("display", "inherit");

        list.find(".stars").append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(list.find(".stars"), recommendData[focusRow].itemList[idx].OLLEH_RATING);
    }

    function setRollingPage(leftPage, rightPage) {
        var list = recommendDiv.find(".posterList");
        var renderPage = leftPage, tmpIdx;
        for (var j = 0; j < 16; j++) {
            var li = list.find(".content.poster_vertical").eq(j);
            if (j < 8) {
                if (j == 0 && leftPage == 0) {
                    li.addClass("hide");
                    li.attr("id", "");
                    continue;
                }
                renderPage = leftPage;
                tmpIdx = (renderPage * 7) + j - 1;
            }
            else {
                renderPage = rightPage;
                tmpIdx = (renderPage * 7) + j - 8;
            }
            if (!recommendData[focusRow].itemList[tmpIdx]) {
                li.addClass("hide");
                li.attr("id", "");
            }
            else {
                setRollingPageData(li, tmpIdx);
            }
        }
    }

    function moveRollingPage(goNext, currentPage, nextPage) {
        var movingList = recommendDiv.find(".posterList.focus"), showingPage = 0;
        // GoNextPage
        if (goNext) {
            if (movingList.hasClass("moved")) {
                movingList.css("transition", "-webkit-transform 0");
                movingList.removeClass("moved");
                movingList[0].offsetWidth;
                // DataChange
                setRollingPage(currentPage, nextPage);
                movingList.css("transition", "-webkit-transform .5s");
            }
            movingList.addClass("moved");
            showingPage = 1;
        }
        // GoPrevPage
        else {
            if (!movingList.hasClass("moved")) {
                movingList.css("transition", "-webkit-transform 0");
                movingList.addClass("moved");
                movingList[0].offsetWidth;
                setRollingPage(nextPage, currentPage);
                movingList.css("transition", "-webkit-transform .5s");
            }
            movingList.removeClass("moved");
            showingPage = 0;
        }
        for (var i = 0; i < 16; i++) {
            if (movingList.find(".content.poster_vertical")[i].id != "") movingList.find(".content.poster_vertical").eq(i).removeClass("hide");
        }
        recommendDiv.find(".recommendRightDim").removeClass("hide");
        if (nextPage == maxPage[focusRow]) {
            movingList.find(".content.poster_vertical").eq(showingPage == 0 ? 8 : 15).addClass("hide");
            recommendDiv.find(".recommendRightDim").addClass("hide");
        }
        movingList.find(".content.poster_vertical").eq(showingPage == 0 ? 0 : 7).addClass("hide");
    }

    function changeFocus() {
        recommendDiv.find(".posterList li").removeClass("focus");
        recommendDiv.find(".posterList.focus li#" + recommendData[focusRow].itemList[focusIdx].ITEM_ID + "_" + focusIdx).addClass("focus");
        if (isAppType(recommendData[focusRow].itemList[focusIdx].ITEM_TYPE)) main.div.find(".tabDetailArea.RECOMMEND .etcInfo .contextArea").hide();
        else main.div.find(".tabDetailArea.RECOMMEND .etcInfo .contextArea").show();

        setTextAnimation(recommendDiv.find(".posterList.focus"), focusIdx);
    }

    function setTextAnimation(_div, focus) {
        clearTextAnimation();
        if (UTIL.getTextLength(recommendData[focusRow].itemList[focus].ITEM_NAME, "RixHead L", 27) > 182) {
            var posterDiv = _div.find("#" + recommendData[focusRow].itemList[focus].ITEM_ID + "_" + focus).eq(0).find(".posterTitle");
            posterDiv.addClass("textAnimating");
            UTIL.startTextAnimation({
                targetBox: posterDiv
            });
        }
    }

    this.resumeTextAnimation = function() {
        setTextAnimation(recommendDiv.find(".posterList.focus"), focusIdx);
    };

    function clearTextAnimation() {
        UTIL.clearAnimation(recommendDiv.find(".posterList.focus").find(".textAnimating span"));
        recommendDiv.find(".posterList.focus").find(".textAnimating").removeClass("textAnimating");
        // void recommendDiv.find(".posterList.focus")[0].offsetWidth;
    }

    function isAppType(_itemType) {
        return (_itemType == 3 || _itemType == 7 || _itemType == 8)
    }

    function getHDYN(_ishd) {
        if (_ishd == "HD") return "Y";
        else if (_ishd == "SD") return "N";
        else if (_ishd == "UHD") return "U";
        else if (_ishd == "FHD") return "F";
        return "";
    }

    function showToast(txt) {
        LayerManager.activateLayer({
            obj: {
                id: "VODToast",
                type: Layer.TYPE.BACKGROUND,
                priority: Layer.PRIORITY.REMIND_POPUP,
                linkage: true,
                params: {
                    text: txt
                }
            },
            moduleId: "module.vod",
            visible: true
        })
    }

    function setPlayListFail() {
        extensionAdapter.notifySNMPError("VODE-00013");
        HTool.openErrorPopup({message: ERROR_TEXT.ET_REBOOT.concat(["", "(VODE-00013)"]), reboot: true});
    }

    function setWishListFlag() {
        var familyHomeModule = ModuleManager.getModule("module.family_home");
        if (familyHomeModule) familyHomeModule.execute({method: "setWishListFlag"});
    }

    function setPlayList(item) {
        try {
            VODAmocManager.setPlayListNxt(1, (parseInt(item.ITEM_TYPE) == 4 ? "2" : item.ITEM_TYPE), item.ITEM_ID, item.PARENT_CAT_ID || item.ITEM_ID, 1, (item.CMB_YN == "Y") ? getHDYN(item.IS_HD) : "", function(result, response) {
                if (result === true) {
                    if (response.reqCode == 1) {
                        LayerManager.activateLayer({
                            obj: {
                                id: "VODWishConfirm",
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    isKids: main.isKids,
                                    callback: function() {
                                        VODAmocManager.setPlayListNxt(1, null, item.ITEM_ID, null, 2, (item.CMB_YN == "Y") ? getHDYN(item.IS_HD) : "", function(result, response) {
                                            if (result === true) {
                                                if (response.reqCode == 0) {
                                                    showToast("VOD 찜을 해제하였습니다");
                                                    setWishListFlag();
                                                }
                                                else setPlayListFail();
                                            } else setPlayListFail();
                                        });
                                    }
                                }
                            },
                            moduleId: "module.vod",
                            visible: true
                        });
                    } else if (response.reqCode == 0) {
                        showToast("VOD를 찜 하였습니다. " + HTool.getMyHomeMenuName() + ">찜한 목록에서 확인할 수 있습니다");
                        setWishListFlag();
                    } else throw setPlayListFail();
                } else setPlayListFail();
            });
        } catch (e) {
            log.printErr(e);
            setPlayListFail();
        }
    }

    this.keyEvent = function(keyCode) {
        switch (keyCode) {
            case KEY_CODE.UP:
                sendVODDetail(keyCode);
                if (focusRow == 0) _this.focusOut();
                else {
                    focusRow--;
                    changePage();
                }
                return true;
            case KEY_CODE.DOWN:
                if (focusRow < recommendData.length - 1) {
                    sendVODDetail(keyCode);
                    focusRow++;
                    changePage();
                }
                return true;
            case KEY_CODE.LEFT:
                sendVODDetail(keyCode);
                recPage = Math.floor(focusIdx / 7);
                if (focusIdx % 7 == 0 && recommendData[focusRow].itemList.length > 7) {
                    if (recPage == 0) moveRollingPage(false, recPage, maxPage[focusRow]);
                    else moveRollingPage(false, recPage, recPage - 1);
                }
                focusIdx = HTool.indexMinus(focusIdx, recommendData[focusRow].itemList.length);
                changeFocus();
                return true;
            case KEY_CODE.RIGHT:
                sendVODDetail(keyCode);
                recPage = Math.floor(focusIdx / 7);
                if (((focusIdx + 1) % 7 == 0 || focusIdx == recommendData[focusRow].itemList.length - 1) && recommendData[focusRow].itemList.length > 7) {
                    if (recPage == maxPage[focusRow]) moveRollingPage(true, recPage, 0);
                    else moveRollingPage(true, recPage, recPage + 1);
                }
                focusIdx = HTool.indexPlus(focusIdx, recommendData[focusRow].itemList.length);
                changeFocus();
                return true;
            case KEY_CODE.ENTER:
                clearTextAnimation();
                VODImpl.showDetail({
                    callback: function(success) {log.printDbg(success + " ShowDetail Complete");},
                    cat_id: recommendData[focusRow].itemList[focusIdx].PARENT_CAT_ID || recommendData[focusRow].itemList[focusIdx].ITEM_ID,
                    const_id: recommendData[focusRow].itemList[focusIdx].ITEM_ID,
                    req_cd: "19",
                    isKids: main.isKids,
                    isAuthorizedContent: main.isAuthorizedContent()
                });
                sendJumpVOD();
                return true;
            case KEY_CODE.CONTEXT:
                if (!isAppType(recommendData[focusRow].itemList[focusIdx].ITEM_TYPE) && !KidsModeManager.isKidsMode()) setPlayList(recommendData[focusRow].itemList[focusIdx]);
                return true;
        }

        return false;
    };

    function setNoData() {
        _this.isNoData = true;
        var noDataText = "";
        if (main.dispContsClass == "05") noDataText = "함께 많이 본 상품이 없습니다";
        else if (main.dispContsClass == "APPGAME") noDataText = "함께 많이 본 영상 & 서비스가 없습니다";
        else noDataText = "함께 많이 본 영상이 없습니다";
        main.div.find(".tabDetailArea.RECOMMEND").append(_$(
            "<div class='noData'>" +
            // [5월 형상] No Data 아이콘 삭제
            // "<div class='icon'></div>" +
            // (main.isKids ? "<div class='icon'></div>" : "") +
            "<span class='text'>" + noDataText + "</span>" +
            "</div>"
        ));
        // 키즈 상세화면에서 함많본이 없을 경우 하단 딤 처리 하지 않음
        if (main.isKids) main.div.find(".bottomDim").addClass("hide");
        // 시리즈 상세화면일 때 처리
        if (main.isSeries) {
            var seriesDiv = main.div.find(".series_bottom .series_recommendArea");
            seriesDiv.empty();
            seriesDiv.append(_$("<div/>", {class: "noDataText"}).text("함께 많이 본 영상이 없습니다"));
        } else {
            // 시리즈 상세가 아닐 때에는 하단 Dim이 문구를 덮지 않도록 z-index 값을 변경함
            main.div.find(".detail_bottom").css("z-index", "5");
        }
    }

    function setPosterIcon(elem, data) {
        var posterIconCnt = 0;
        var posterIconArea = elem.find(".posterIconArea");
        posterIconArea.empty();
        if (data.HDR_YN == "Y") {
            posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_hdr.png"}));
            posterIconCnt++;
        }
        if (data.IS_HD == "UHD") {
            posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_uhd.png"}));
            posterIconCnt++;
        }
        if (data.SMART_DVD_YN == "Y" && posterIconCnt < 2) {
            posterIconArea.append(_$("<img>", {src: modulePath + "resource/tag_poster_mine.png"}));
        }

        var newHotIconArea = elem.find(".newHotIconArea");
        newHotIconArea.empty();
        if (data.NEW_HOT == "NEW") newHotIconArea.append(_$("<img>", {src: modulePath + "resource/icon_flag_new.png"}));
    }

    function getVerticalPoster(data, index) {
        var element = _$("<li class='content poster_vertical' id='" + (data.ITEM_ID + "_" + index) + "'>" +
            "<div class='content'>" +
            "<img src='" + data.IMG_URL + HTool.addPosterOption(182, 260, 90) + "' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/defaultPoster/default_poster.png\"'>" +
            "<div class='posterIconArea'/>" +
            "<div class='newHotIconArea'/>" +
            "<img class='posterLockImg'>" +
            "<div class='contentsData'>" +
            "<div class='posterTitle'>" +
            "<span>" + data.ITEM_NAME + "</span>" +
            "</div>" +
            "<span class='posterDetail'>" +
            "<span class='stars'/>" +
            "<span class='isfreeArea'>" +
            "<img class='isfreeIcon'>" +
            "<span class='isfree'>" + "무료" + "</span>" +
            "</span>" +
            "<img src='" + ROOT_URL + "resource/detail/icon_age_list_" + ((data.RATING - 0) > 0 ? (data.RATING - 0) : "all" ) + ".png' class='age'>" +
            "</span>" +
            "</div>" +
            "</div>" +
            "</li>");
        element.find(".stars").append(stars.getView(stars.TYPE.RED_20));
        stars.setRate(element.find(".stars"), data.OLLEH_RATING);
        setPosterIcon(element, data);

        if (main.isAuthorizedContent() || !UTIL.isLimitAge(data.RATING)) element.find("img.posterLockImg").css("display", "none");
        else element.find("img.posterLockImg").css("display", "inherit");

        if (HTool.isTrue(data.WON_YN)) {
            element.find("img.isfreeIcon").css("display", "inherit");
            element.find("span.isfree").css("display", "none");
        } else if (HTool.isFalse(data.WON_YN)) {
            element.find("span.isfree").css("display", "inherit");
            element.find("span.isfree").text("무료");
            element.find("img.isfreeIcon").css("display", "none");
        } else {
            element.find("img.isfreeIcon").css("display", "none");
            element.find("span.isfree").css("display", "none");
        }
        return element;
    }

    function getEmptyPoster() {
        return _$("<li class='content poster_vertical hide' id=''><div class='content'><img src='' class='posterImg poster'><div class='posterIconArea'/><div class='newHotIconArea'/><img class='posterLockImg'><div class='contentsData'><div class='posterTitle'><span></span></div><span class='posterDetail'><span class='stars'/><span class='isfreeArea'><img class='isfreeIcon'><span class='isfree'/>><img src='' class='age'></span></div></div></li>");
    }

    function getVerticalPosterForSeries(data) {
        var element = _$("<li class='content poster_vertical' id='" + data.ITEM_ID + "'>" +
            "<div class='content'>" +
            "<img src='" + data.IMG_URL + HTool.addPosterOption(182, 260, 90) + "' class='posterImg poster' onerror='this.src=\"" + modulePath + "resource/defaultPoster/default_poster.png\"'>" +
            "<div class='posterIconArea'/>" +
            "<div class='newHotIconArea'/>" +
            "<img class='posterLockImg'>" +
            "</div>" +
            "</li>");
        setPosterIcon(element, data);
        if (main.isAuthorizedContent() || !UTIL.isLimitAge(data.RATING)) element.find("img.posterLockImg").css("display", "none");
        else element.find("img.posterLockImg").css("display", "inherit");
        return element;
    }

    function constructDom() {
        if (recommendData == null || recommendData.length == 0 || recommendData[0].itemList.length == 0) setNoData();
        else {
            var j, list;
            if (!recommendDiv.find(".posterList")[0]) {
                recommendDiv.append(_$(
                    "<div class='recommendPage'>" +
                    "<ul class='posterList'/>" +
                    (recommendData[focusRow].itemList.length > 7 ? "<img class='recommendRightDim'/>" : "<img class='recommendRightDim hide'/>") +
                    "</div>"
                ));
                list = recommendDiv.find(".posterList");
                list.append(getEmptyPoster());
                for (j = 0; j < 15; j++) {
                    if (recommendData[focusRow].itemList[j]) list.append(getVerticalPoster(recommendData[focusRow].itemList[j], j));
                    else list.append(getEmptyPoster());
                }
            } else {
                list = recommendDiv.find(".posterList");
                if (list.hasClass("moved")) {
                    list.css("transition", "-webkit-transform 0");
                    list.removeClass("moved");
                    list[0].offsetWidth;
                    list.css("transition", "-webkit-transform .5s");
                }
                setRollingPage(0, 1);
                if (recommendData[focusRow].itemList.length > 7) recommendDiv.find(".recommendRightDim").removeClass("hide");
                else recommendDiv.find(".recommendRightDim").addClass("hide");
            }
            main.div.find(".tabDetailArea.RECOMMEND").find(".recommendTitle").text(recommendData[focusRow].itemName || "");
            if (focusRow == 0) {
                main.div.find(".tabDetailArea.RECOMMEND").find(".recommendArrow.up").hide();
                if (recommendData.length > 1) main.div.find(".tabDetailArea.RECOMMEND").find(".recommendArrow.dw").show();
                else main.div.find(".tabDetailArea.RECOMMEND").find(".recommendArrow.dw").hide();
            } else {
                main.div.find(".tabDetailArea.RECOMMEND").find(".recommendArrow.up").show();
                if (recommendData.length - 1 == focusRow) main.div.find(".tabDetailArea.RECOMMEND").find(".recommendArrow.dw").hide();
                else main.div.find(".tabDetailArea.RECOMMEND").find(".recommendArrow.dw").show();
            }
        }
    }

    function constructDomSeries() {
        // 시리즈 상세화면일 때 처리
        if (main.isSeries) {
            var seriesDiv = main.div.find(".series_bottom .series_recommendArea");
            seriesDiv.empty();
            for (var i = 0; i < Math.min(4, recommendData[0].itemList.length); i++) {
                seriesDiv.append(getVerticalPosterForSeries(recommendData[0].itemList[i]));
            }
        }
    }

    function parsingData(_response) {
        var tmpRecommendData = [];
        var i = 0;
        if (_response.RC_LIST.length > 0) tmpRecommendData.push({itemList: _response.RC_LIST});
        if (_response.MENU_LIST.length > 0) {
            for (i = 0; i < _response.MENU_LIST.length; i++) {
                var tmpObj = {};
                tmpObj.itemName = _response.MENU_LIST[i].MENU_NAME;
                tmpObj.itemList = [];
                for (var j = 0; j < _response.ITEM_LIST.length; j++) {
                    if (_response.MENU_LIST[i].MENU_ID == _response.ITEM_LIST[j].MENU_ID) tmpObj.itemList.push(_response.ITEM_LIST[j]);
                }
                if (tmpObj.itemList.length > 0) tmpRecommendData.push(tmpObj);
                tmpObj = null;
            }
        }
        for (i = 0; i < tmpRecommendData.length; i++) {
            maxPage[i] = Math.floor((tmpRecommendData[i].itemList.length - 1) / 7);
        }
        return tmpRecommendData;
    }

    this.getTabID = function() { return NLC.DETAIL_RECOMMEND_TAB; };
    this.getTabStr = function() { return "함께많이본영상 탭";};

    this.destroy = function() {

    };

    function getOneDepthCategoryId(id) {
        var MenuDataManager = homeImpl.get(homeImpl.DEF_FRAMEWORK.MENU_DATA_MANAGER);
        var a = MenuDataManager.searchMenu({
            menuData: MenuDataManager.getMenuData(),
            allSearch: false,
            cbCondition: function(menu) { return menu.id == id; }
        })[0];
        if (!a) return null;
        while (a.parent != null) a = a.parent;
        return a.id;
    }

    function sendVODDetail(key_code) {
        try { // 통계처리
            NavLogMgr.collectVODDetail(key_code,
                contentInfo.catId || recommendData[focusRow].itemList[focusIdx].PARENT_CAT_ID,
                contentInfo.contsId || contentInfo.itemId,
                contentInfo.contsName || contentInfo.itemName,
                NLC.DETAIL_RECOMMEND,
                recommendData[focusRow].itemList[focusIdx].ITEM_ID);
        } catch (e) {
        }
    }

    function sendJumpVOD() {
        try {
            NavLogMgr.collectJumpVOD(NLC.JUMP_START_RELATED_CONTENTS,
                contentInfo.catId || recommendData[focusRow].itemList[focusIdx].PARENT_CAT_ID,
                recommendData[focusRow].itemList[focusIdx].ITEM_ID
            );
        } catch (e) {
        }
    }

    function onCreate() {
        try {
            main.div.find(".tabDetailArea.RECOMMEND").append(_$("<div/>", {class: "recommendArea"}));
            main.div.find(".tabDetailArea.RECOMMEND").append(_$("<div/>", {class: "etcInfo"}));
            main.div.find(".tabDetailArea.RECOMMEND .etcInfo").append(_$("<span/>", {class: "recommendTitle"}));
            main.div.find(".tabDetailArea.RECOMMEND .etcInfo").append(_$("<div/>", {class: "recommendArrow up"}));
            main.div.find(".tabDetailArea.RECOMMEND .etcInfo").append(_$("<div/>", {class: "recommendArrow dw"}));
            if (!KidsModeManager.isKidsMode()) {
                main.div.find(".tabDetailArea.RECOMMEND .etcInfo").append(_$("<div/>", {class: "contextArea"}));
                main.div.find(".tabDetailArea.RECOMMEND .etcInfo .contextArea").append(_$("<div/>", {class: "icon"}));
                main.div.find(".tabDetailArea.RECOMMEND .etcInfo .contextArea").append(_$("<span/>", {class: "text"}).text("찜하기"));
            }
            recommendDiv = main.div.find(".tabDetailArea.RECOMMEND .recommendArea");
            contentInfo = _contentInfo;
            log.printDbg("recommendYn is " + _contentInfo.recommendYn);
            // 2017.08.16 Lazuli
            // 양방향 상세화면일 경우 recommendYn 값이 없으므로 recommendYn 값과 무관하게 API 호출함
            if (_isAppGame || _contentInfo.recommendYn == "Y" || _contentInfo.recommendYn == "P") {
                var categoryId = _isAppGame ? getOneDepthCategoryId(_contentInfo.catId) : _contentInfo.catId;
                var recommendCache = main.getRecommendDataForSeries(_contentInfo.contsId + categoryId);
                if (recommendCache == null) {
                    HTool.updateCustEnv(function() {
                        var loadPackageList = StorageManager.ms.load(StorageManager.MEM_KEY.PKG_LIST);
                        var productList = (loadPackageList && loadPackageList != null && loadPackageList.length > 0) ? StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST) + "," + loadPackageList : StorageManager.ms.load(StorageManager.MEM_KEY.PKG_BASE_LIST);
                        VodRecmManager.getRelatedContents(_isAppGame ? "ITEM_APP" : "ITEM_VOD", "M", DEF.SAID, productList, _contentInfo.contsId, categoryId, _contentInfo.adultOnlyYn, 21, function(result, response) {
                            if (response.RESULT_CODE == 200) {
                                // Lazuli 2017.10.31
                                // 시리즈 상세화면에서 잦은 API 호출을 방지하기 위해 DetailBottom에 데이터를 캐시함
                                recommendData = parsingData(response);
                                main.addRecommendDataForSeries(_contentInfo.contsId + categoryId, recommendData);
                                constructDom();
                                constructDomSeries();
                            } else setNoData();
                        });
                    });
                } else {
                    recommendData = recommendCache;
                    for (var i = 0; i < recommendData.length; i++) {
                        maxPage[i] = Math.floor((recommendData[i].itemList.length - 1) / 7);
                    }
                    constructDom();
                    constructDomSeries();
                }
            } else setNoData();
        } catch (e) {
            log.printErr(e);
            setNoData();
        }
    }

    onCreate();
};