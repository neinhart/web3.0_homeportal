/**
 * Created by Taesang on 2016-11-02.
 */
window.DetailBottom = function () {
    var _this = this;
    var currentTab = null;
    var tabRecommend = null;
    var tabAdditional = null;
    var tabPoint = null;
    var tabSearch = null;
    var tabSynopsis = null;
    var focusRow = 0, selectedMenuIdx = 0, tabLength = 5;
    var contentInfo = null, masterContentInfo = null;
    var focusedTab = null;
    var additionalInfo = null;
    var recommendDataMap = {};
    var recommendDataMapIndex = [];

    this.isShow = false;
    this.dispContsClass = "";
    var isAuthorizedContent = false;

    function keyDown() {
        if (!currentTab.isNoData) {
            if (currentTab == tabSynopsis && tabSynopsis.getTotalPage() < 2) {
            }
            else {
                currentTab.focusIn();
                focusedTab.removeClass("focus");
                _this.div.find(".tabDiv .arrow").removeClass("show");
                _this.div.find(".tabArea").removeClass("focus");
                focusRow = 1;
            }
        }
    }

    // 시리즈 상세화면에서 함께많이본영상 캐시를 위해 사용함
    // _id는 contsId + catId 값을 사용함
    this.addRecommendDataForSeries = function (_id, _data) {
        recommendDataMap[_id] = _data;
        recommendDataMapIndex.push(_id);
        if (recommendDataMapIndex.length > 20) {
            delete recommendDataMap[recommendDataMapIndex[0]];
            recommendDataMapIndex.shift();
        }
    };

    this.getRecommendDataForSeries = function (_id) {
        return recommendDataMap[_id];
    };

    this.resumeTextAnimation = function () {
        if (currentTab == tabRecommend) currentTab.resumeTextAnimation();
    };

    this.setTabLength = function (_length) {
        tabLength = _length
    };
    this.controlKey = function (keyCode) {
        if (!_this.isShow || !currentTab) return false;
        if (focusRow != 0 && keyCode != KEY_CODE.BACK) {
            if (currentTab != tabRecommend && keyCode == KEY_CODE.CONTEXT) return true;
            return currentTab.keyEvent(keyCode);
        }
        if (keyCode == KEY_CODE.CONTEXT) return true;

        try {
            NavLogMgr.collectVODDetail(keyCode, masterContentInfo.catId, masterContentInfo.contsId, masterContentInfo.contsName || masterContentInfo.itemName || masterContentInfo.catName, currentTab.getTabID(), focusedTab.find(".tabName").text());
        } catch (e) {
        }

        switch (keyCode) {
            case KEY_CODE.BACK:
                if (focusRow != 0 && currentTab.hasOwnProperty("focusOut")) currentTab.focusOut();
                _this.changeMenuFocus(null, 0);
                _this.hide();
                return true;
            case KEY_CODE.UP:
                _this.changeMenuFocus(null, 0);
                _this.hide();
                return true;
            case KEY_CODE.DOWN:
                keyDown();
                return true;
            case KEY_CODE.LEFT:
                _this.changeMenuFocus(null, HTool.indexMinus(selectedMenuIdx, tabLength));
                return true;
            case KEY_CODE.RIGHT:
                _this.changeMenuFocus(null, HTool.indexPlus(selectedMenuIdx, tabLength));
                return true;
            case KEY_CODE.ENTER:
                keyDown();
                return true;
        }
        return false;
    };

    this.changeMenuFocus = function (_focusRow, _selectedMenuIdx) {
        if (_focusRow != null) focusRow = _focusRow;
        if (_selectedMenuIdx != null) selectedMenuIdx = _selectedMenuIdx;
        _this.removeFocus();

        focusedTab = _this.div.find(".tabArea .tab").eq(selectedMenuIdx);
        focusedTab.addClass("focus");
        _this.div.find(".tabArea").addClass("focus");
        _this.div.find(".tabDiv .arrow").addClass("show");

        switch (focusedTab.find(".tabName").text()) {
            case DetailBottom.TABTYPE.ADDITIONAL:
                if (!tabAdditional) {
                    tabAdditional = new DetailBottom_Additional(_this, contentInfo.contsId, contentInfo.catId, contentInfo, additionalInfo);
                }
                currentTab = tabAdditional;
                break;
            case DetailBottom.TABTYPE.SYNOPSIS:
                if (!tabSynopsis) tabSynopsis = new DetailBottom_Synopsis(_this, contentInfo.contsId, contentInfo);
                currentTab = tabSynopsis;
                break;
            case DetailBottom.TABTYPE.POINT:
                if (!tabPoint) tabPoint = new DetailBottom_Point(_this, contentInfo.contsId, this.isKids, contentInfo);
                currentTab = tabPoint;
                break;
            case DetailBottom.TABTYPE.SEARCH:
                if (!tabSearch) tabSearch = new DetailBottom_Search(_this, {
                    overseerName: contentInfo.overseerName,
                    play: contentInfo.play,
                    actor: contentInfo.actor,
                    catId: contentInfo.catId,
                    contsId: contentInfo.contsId,
                    contsName: contentInfo.contsName
                });
                currentTab = tabSearch;
                break;
            case DetailBottom.TABTYPE.RECOMMEND:
                currentTab = tabRecommend;
                break;
        }

        focusedTab.find(".tabName").addClass("focus");
        _this.div.find(".tabDetailArea.focus").removeClass("focus");
        _this.div.find(".tabDetailArea").eq(selectedMenuIdx).addClass("focus");
    };

    this.removeFocus = function () {
        _this.div.find(".tabArea .tab.focus").removeClass("focus");
        _this.div.find(".tabArea .tabName.focus").removeClass("focus");
    };

    this.isAuthorizedContent = function () {
        return isAuthorizedContent;
    };

    this.setData = function (_masterContentInfo, _isAuthorizedContent) {
        log.printDbg(JSON.stringify(_masterContentInfo));

        isAuthorizedContent = _isAuthorizedContent;
        // 메뉴 초기화
        _masterContentInfo.contsId = _masterContentInfo.contsId || _masterContentInfo.itemId;

        if (!masterContentInfo || masterContentInfo.contsId != _masterContentInfo.contsId || masterContentInfo.catId != _masterContentInfo.catId) {
            masterContentInfo = _masterContentInfo;
            destroyTab();
            _this.div.find(".tabDetailArea").empty();
            tabRecommend = new DetailBottom_Recommend(_this, masterContentInfo, (_this.dispContsClass === "APPGAME"));
        }
        else {
            destroyTab(true);
        }

        currentTab = tabRecommend;
        focusRow = selectedMenuIdx = 0;
        _this.removeFocus();

        _this.div.find(".tabDetailArea").eq(selectedMenuIdx).addClass("focus");
    };

    this.updateData = function (_contentInfo, _additionalInfo) {
        log.printDbg("updateData " + JSON.stringify(_contentInfo));
        // 2017.08.07 Lazuli
        // 하단탭이 보여지고 있는 상태이면 데이터 갱신하지 않도록 수정
        // VOD 재생 하고 돌아왔을 때 구매버튼이 갱신되면서 updateDate 함수가 호출 되는데
        // 예고편 재생 후 돌아왔을 경우 위 로직으로 인해 예고편 리스트가 날아가는 현상이 있음
        if (this.isShow) {
            return;
        }

        _contentInfo.contsId = _contentInfo.contsId || _contentInfo.itemId;
        contentInfo = _contentInfo;

        // 메뉴 초기화
        destroyTab(true);

        _this.updateContextText(contentInfo.previewData);

        for (var i = 1; i < _this.div.find(".tabDetailArea").length; i++) {
            _this.div.find(".tabDetailArea").eq(i).empty();
        }

        additionalInfo = _additionalInfo;
    };

    // 미리보기 관련 연관메뉴 텍스트를 업데이트함
    this.updateContextText = function (_previewData) {
        _this.div.find(".bottom_relation_area .bottom_relation_bar").show();
        _this.div.find(".bottom_relation_area .relText").show();
        if (_previewData && _previewData.isPreview) {
            _this.div.find(".bottom_relation_area").addClass("preview");
            _this.div.find(".relText").text(_previewData.contextText);
        }
        else {
            _this.div.find(".bottom_relation_area").removeClass("preview");
            if (KidsModeManager.isKidsMode()) {
                _this.div.find(".bottom_relation_area .bottom_relation_bar").hide();
                _this.div.find(".bottom_relation_area .relText").hide();
            }
            else _this.div.find(".relText").text("찜");
        }
    };

    function destroyTab(isFocusChange) {
        if (tabRecommend && !isFocusChange) {
            tabRecommend.destroy();
            tabRecommend = null;
        }
        if (tabAdditional) {
            tabAdditional.destroy();
            tabAdditional = null;
        }
        if (tabPoint) {
            tabPoint.destroy();
            tabPoint = null;
        }
        if (tabSearch) {
            tabSearch.destroy();
            tabSearch = null;
        }
        if (tabSynopsis) {
            tabSynopsis.destroy();
            tabSynopsis = null;
        }
    }
};

DetailBottom.prototype.createView = function (_dispContsClass, isSeries, reqCd, _isKids) {
    var _this = this;
    log.printDbg("CreateBottom : dispContsClass is " + _dispContsClass + ", isSeries ? " + isSeries + ", reqCd is " + reqCd + ", isKids ? " + _isKids);
    this.isSeries = isSeries;
    this.reqCd = reqCd;
    this.isKids = _isKids;
    this.dispContsClass = _dispContsClass;
    this.div = _$("<div/>", {class: "bottomArea"});
    this.div.append(_$("<div/>", {class: "detail_bottom"}));
    this.div.append(_$("<div/>", {class: "bottomDim"}));

    if (isSeries) {
        this.div.append(_$("<div class='series_bottom'>" + "<span class='tabName' style='left: 90px;'>함께 많이 본 영상</span>" + "<span class='tabName' style='left: 397px;'>예고편/부가영상</span>" + "<span class='tabName' style='left: 715px;'>줄거리 더보기</span>" + "<div class='series_recommendArea'/>" + "</div>"));
        this.div.find(".series_bottom").append(_$("<div/>", {class: "series_bottom_dim"}), _$("<div/>", {class: "bottom_relation_area"}).append(_$("<img/>", {class: "bottom_relation_icon"}), _$("<div/>", {class: "relationText"}).append(_$("<span />", {class: "relText"}).text("찜"), _$("<div />", {class: "bottom_relation_bar"}), _$("<span />", {class: "relText2"}).text("플레이리스트 추가"))));
        this.div.find(".series_bottom").addClass("show");
    }
    else {
        this.div.find(".bottomDim").addClass("show");
    }

    this.div.find(".detail_bottom").append(_$("<div/>", {class: "dim"}), _$("<div/>", {class: "tabDiv"}));

    if (!isSeries && _dispContsClass != "APPGAME") {
        this.div.find(".tabDiv").append(_$("<div/>", {class: "bottom_relation_area"}).append(_$("<img/>", {class: "bottom_relation_icon"}), _$("<span />", {class: "relText"}).text("찜"), _$("<div />", {class: "bottom_relation_bar"}), _$("<span />", {class: "relText2"}).text("플레이리스트 추가")));
    }
    this.div.find(".tabDiv").append(_$("<div/>", {class: "arrow"}), _$("<div/>", {class: "tabArea"}).append("<div class='tabLine' style='top: 0px'/>", "<div class='tabLine' style='top: 81px'/>"));

    function addTab(_tabName) {
        if (_tabName == DetailBottom.TABTYPE.APPGAME) {
            return _$("<div class='tab'>" + "<span class='tabName long'>" + _tabName + "</span>" + "</div>");
        }
        else {
            return _$("<div class='tab'>" + "<span class='tabName'>" + _tabName + "</span>" + "</div>");
        }
    }

    switch (_dispContsClass) {
        // 전시용 콘텐츠 분류
        case "01" : // 뮤직 비디오
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.RECOMMEND));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea RECOMMEND"}));
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.POINT));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea POINT"}));
            _this.setTabLength(2);
            break;
        case "02" : // 지상파
        case "03" : // 영화
        case "06" : // 키즈
        case "99" : // 기타
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.RECOMMEND));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea RECOMMEND"}));
            if (!_isKids) {
                _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.SEARCH));
                _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea SEARCH"}));
                _this.setTabLength(5);
            }
            else _this.setTabLength(4);
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.ADDITIONAL));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea ADDITIONAL"}));
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.POINT));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea POINT"}));
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.SYNOPSIS));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea SYNOPSIS"}));
            break;
        case "04" : // (뮤직비디오 외) 음악
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.RECOMMEND));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea RECOMMEND"}));
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.SEARCH));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea SEARCH"}));
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.POINT));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea POINT"}));
            _this.setTabLength(3);
            break;
        case "05" : // 쇼핑
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.PRODUCT));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea RECOMMEND"}));
            _this.setTabLength(1);
            break;
        case "APPGAME" : // 앱/게임
            _this.div.find(".tabArea").append(addTab(DetailBottom.TABTYPE.APPGAME));
            _this.div.find(".tabDiv").append(_$("<div/>", {class: "tabDetailArea RECOMMEND"}));
            _this.setTabLength(1);
            break;
    }

    return this.div;
};

DetailBottom.prototype.show = function () {
    this.isShow = true;
    this.changeMenuFocus(0, 0);
    this.div.find(".tabDiv .bottom_relation_area").css("display", "none");
    this.div.find(".tabArea").addClass("focus");
    this.div.find(".detail_bottom").addClass("show");
    this.div.find(".tabDiv .arrow").addClass("show");

    if (this.div.find(".series_bottom")[0]) {
        this.div.find(".series_bottom").css("transition-delay", "0s");
        this.div.find(".series_bottom").removeClass("show");
    }
    else {
        this.div.find(".bottomDim").removeClass("show");
    }
};

DetailBottom.prototype.hide = function () {
    this.isShow = false;
    this.removeFocus();
    this.div.find(".tabDiv .bottom_relation_area").css("display", "block");
    this.div.find(".tabArea").addClass("focus");
    this.div.find(".detail_bottom").removeClass("show");
    this.div.find(".tabDiv .arrow").removeClass("show");

    if (this.div.find(".series_bottom")[0]) {
        this.div.find(".series_bottom").css("transition-delay", ".3s");
        this.div.find(".series_bottom").addClass("show");
    }
    else {
        this.div.find(".bottomDim").addClass("show");
    }
};

Object.defineProperty(DetailBottom, "TABTYPE", {
    value: {
        RECOMMEND: "함께 많이 본 영상",
        ADDITIONAL: "예고편/부가영상",
        SYNOPSIS: "줄거리 더보기",
        POINT: "평점 더보기",
        SEARCH: "인물 검색",
        PRODUCT: "함께 많이 본 상품",
        APPGAME: "함께 많이 본 영상 & 서비스"
    }, writable: !1, configurable: !1
});