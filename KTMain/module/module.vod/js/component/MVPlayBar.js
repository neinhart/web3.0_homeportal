/**
 * Created by Yun on 2016-12-29.
 */

window.MVPlayBar = function() {
    var _this = this;
    this.div;
    this.showing = false;
    this.playList = [];
    this.endTime;
    this.curIdx;
    this.focusIdx;
    this.curTime;
    var curState = 0;

    function getImgUrl(mvObj) {
        var url, imgType;
        if (mvObj.squareImgUrl) {
            url = mvObj.squareImgUrl + "?w=135&h=135&quality=90";
            imgType = "square";
        } else if (mvObj.imgUrl) {
            url = mvObj.imgUrl + "?w=106&h=151&quality=90";
            imgType = "vertical";
        } else {
            url = ROOT_URL + "resource/defaultPoster/default_music2.png";
            imgType = "square";
        }
        return {imgType: imgType, url: url};
    }

    this.setTitle = function() {
        var vodTitle = "";
        VODManager.getCurrentVODInfo().contsName != null ? vodTitle = VODManager.getCurrentVODInfo().contsName :
            VODManager.getCurrentVODInfo().itemName != null ? vodTitle = VODManager.getCurrentVODInfo().itemName : "";
        this.div.find("#mvTitle_area").text(vodTitle);
        this.settingPlayListwithData();
    };

    this.settingPlayListwithData = function() {
        this.playList = VODInfoManager.getPlayList();
        this.curIdx = VODInfoManager.getCurrentPlayListIdx();
        this.focusIdx = this.curIdx;
        this.refreshListData(_this.curIdx);
    };

    this.changeMiniIndicator = function(state) {
        if (this.curIdx == this.focusIdx) {
            curState = state;
            this.div.find("#mv_playIcon_img").removeClass("show");
            this.div.find("#mv_pauseIcon_img").removeClass("show");
            if (state != VODManager.PLAY_STATE.PLAY) {
                this.div.find("#mv_playIcon_img").addClass("show");
            } else {
                this.div.find("#mv_pauseIcon_img").addClass("show");
            }
            this.setProgress(VODManager.getCurrentPosition());
        } else {
            this.div.find("#mv_playIcon_img").addClass("show");
            this.setProgress(0);
        }
    };

    this.setHasRecommendData = function(has) {
        this.div.find("#mvBottomMenu_div .mvBottomMenu_icon.related").toggle(has);
    };

    this.setNextMusicVideoData = function(title) {
        this.div.find("#nextMV_title").toggleClass(UTIL.getTextLength(title, "RixHead M", 27, -0.75) < 208);
        this.div.find("#nextMV_title").text(title);
    };

    this.setPrevMusicVideoData = function(title) {
        this.div.find("#prevMV_title").toggleClass(UTIL.getTextLength(title, "RixHead M", 27, -0.75) < 208);
        this.div.find("#prevMV_title").text(title);
    };

    this.setTime = function(endTime, currentTime) {
        this.endTime = endTime;
        this.div.find("#mvEnd_time_area").text(makeTimeStr(endTime));
        this.setProgress(currentTime);
    };

    this.setProgress = function(currentTime) {
        this.curTime = currentTime;
        var pos = Math.min((currentTime / this.endTime) * 990, 990);
        this.div.find("#mvRed_bar").css("width", pos + "px");
        this.div.find("#mvCurrent_time_area").text(makeTimeStr(currentTime));
    };

    function makeTimeStr(t) {
        var a = HTool.msecToTime(t);
        return a.min + ":" + a.sec;
    }

    this.getFocusIndex = function() {
        return this.focusIdx;
    };

    this.getCurruntIndex = function() {
        return this.curIdx;
    };

    this.onFocused = function() {
        this.div.find("#mvBottomMenu_div").css("display", "block");
    };

    this.mvPlayBarShow = function() {
        this.div.removeClass("hide");
        MiniGuideManager.hideMiniIndicatorBg();
    };

    this.mvPlayBarHide = function() {
        this.div.addClass("hide");
        MiniGuideManager.showMiniIndicatorBg();
    };

    this.onBlurred = function() {
        this.div.find("#mvBottomMenu_div").css("display", "none");
    };

    this.hideTitle = function() { this.div.find('#mvTitle_area').hide(); };
    this.showTitle = function() { this.div.find('#mvTitle_area').show(); };

    function setPrevMv(mvInfo) {
        _this.div.find("#prev_mv_div").removeClass("square");
        _this.div.find("#prev_mv_div").removeClass("vertical");
        _this.div.find("#prev_mv_div").removeClass("posterLock");
        if (mvInfo != null) {
            _this.div.find("#prev_mv_div").css("display", "block");
            _this.div.find("#prev_mv_div").addClass(getImgUrl(mvInfo).imgType);
            _this.setPrevMusicVideoData(mvInfo.contsName);
            if (UTIL.isLimitAge(mvInfo.prInfo)) _this.div.find("#prev_mv_div").addClass("posterLock");
            _this.div.find("#prevMVAlbum_img")[0].src = getImgUrl(mvInfo).url;
            _this.div.find("#prevMVAlbum_img")[0].onerror = function(e) {
                e.target.src = ROOT_URL + "resource/defaultPoster/default_music2.png";
                _this.div.find("#prev_mv_div").removeClass(getImgUrl(mvInfo).imgType);
            };
        } else {
            _this.div.find("#prev_mv_div").css("display", "none");
            _this.setPrevMusicVideoData("");
            _this.div.find("#prevMVAlbum_img").attr("src", "");
        }
    }

    function setNextMv(mvInfo) {
        _this.div.find("#next_mv_div").removeClass("square");
        _this.div.find("#next_mv_div").removeClass("vertical");
        _this.div.find("#next_mv_div").removeClass("posterLock");
        if (mvInfo != null) {
            _this.div.find("#next_mv_div").css("display", "block");
            _this.div.find("#next_mv_div").addClass(getImgUrl(mvInfo).imgType);
            _this.setNextMusicVideoData(mvInfo.contsName);
            if (UTIL.isLimitAge(mvInfo.prInfo)) _this.div.find("#next_mv_div").addClass("posterLock");
            _this.div.find("#nextMVAlbum_img")[0].src = getImgUrl(mvInfo).url;
            _this.div.find("#nextMVAlbum_img")[0].onerror = function(e) {
                e.target.src = ROOT_URL + "resource/defaultPoster/default_music2.png";
                _this.div.find("#next_mv_div").removeClass(getImgUrl(mvInfo).imgType);
            };
        } else {
            _this.div.find("#next_mv_div").css("display", "none");
            _this.setNextMusicVideoData("");
            _this.div.find("#nextMVAlbum_img").attr("src", "");
        }
    }

    this.refreshListData = function(index) {
        this.focusIdx = index;

        this.div.find("#mv_playIcon_img").removeClass("show");
        this.div.find("#mv_pauseIcon_img").removeClass("show");

        this.div.find("#mvTitle_area").text(this.playList[index].contsName);
        this.div.find("#mv_album_img").attr("src", getImgUrl(this.playList[index]).url);
        _this.div.find("#mv_album_img")[0].src = getImgUrl(this.playList[index]).url;
        _this.div.find("#mv_album_img")[0].onerror = function(e) { e.target.src = ROOT_URL + "resource/defaultPoster/default_cd.png"; };
        _this.div.find("#mvBar_area").toggleClass("posterLock", UTIL.isLimitAge(_this.playList[index].prInfo));

        // 이전곡 셋팅
        if (this.playList[index - 1]) setPrevMv(this.playList[index - 1]);
        else {
            if (VODInfoManager.isPlayListLoop() && this.playList.length > 2) setPrevMv(this.playList[this.playList.length - 1]);
            else setPrevMv(null);
        }

        // 다음 곡 셋팅
        if (this.playList[index + 1]) setNextMv(this.playList[index + 1]);
        else {
            if (VODInfoManager.isPlayListLoop() && this.playList.length > 2) setNextMv(this.playList[0]);
            else setNextMv(null);
        }

        /**
         * [dj.son] [WEBIIIHOME-3657] 현재 play 중인 컨텐츠가 아니면 정확한 playtime 을 알 수 있는 방법이 없음
         * 따라서 현재 재생중인 컨텐츠인 경우에만 시작시간 / 종료시간 표시하고 나머지는 hidden 처리
         */
        if (this.curIdx == this.focusIdx) {
            this.onFocused();

            this.div.find("#mvCurrent_time_area").css({visibility: ""});
            this.div.find("#mvEnd_time_area").css({visibility: ""});
        }
        else {
            this.onBlurred();

            this.div.find("#mvCurrent_time_area").css({visibility: "hidden"});
            this.div.find("#mvEnd_time_area").css({visibility: "hidden"});
        }

        this.changeMiniIndicator(curState);
    };

    this.create();
};

MVPlayBar.prototype.create = function() {
    this.div = _$("<div/>", {id: "mvPlayBar"});
    this.div.append(_$("<div/>", {id: "mvBar_area"}));
    this.div.find("#mvBar_area").append(_$("<div/>", {id: "mvTitle_area"}));
    this.div.find("#mvBar_area").append(_$("<div/>", {id: "mvBase_bar"}));
    this.div.find("#mvBar_area").append(_$("<div/>", {id: "mvRed_bar"}));
    this.div.find("#mvBar_area").append(_$("<span/>", {id: 'mvCurrent_time_area', class: 'time_area'}));
    this.div.find("#mvBar_area").append(_$("<span/>", {id: 'mvEnd_time_area', class: 'time_area'}));
    this.div.find("#mvBar_area").append(_$("<img/>", {id: 'mv_album_img', src: ""}));
    this.div.find("#mvBar_area").append(_$("<img/>", {id: "mv_album_lock"}));
    this.div.find("#mvBar_area").append(_$("<img/>", {id: 'mv_albumFrame_img', src: "module/module.vod/resource/mv_album_line.png"}));
    this.div.find("#mvBar_area").append(_$("<img/>", {id: "mv_playIcon_img", src: "module/module.vod/resource/icon_mv_play.png"}));
    this.div.find("#mvBar_area").append(_$("<img/>", {id: "mv_pauseIcon_img", src: "module/module.vod/resource/icon_mv_pause.png"}));

    this.div.append(_$("<div/>", {id: "next_mv_div"}));
    this.div.find("#next_mv_div").append(_$("<img/>", {id: "nextMVArrow_img", src: "module/module.vod/resource/arw_view_r.png"}));
    this.div.find("#next_mv_div").append(_$("<img/>", {id: "nextMVAlbum_img", src: ""}));
    this.div.find("#next_mv_div").append(_$("<img/>", {id: "nextMVAlbum_lock"}));
    this.div.find("#next_mv_div").append(_$("<div/>", {id: "nextMVAlbum_shadow"}));
    this.div.find("#next_mv_div").append(_$("<div/>", {id: "nextMV_title"}));
    this.div.find("#next_mv_div").append(_$("<div/>", {id: "nextMV_artist"}));
    this.div.append(_$("<div/>", {id: "prev_mv_div"}));
    this.div.find("#prev_mv_div").append(_$("<img/>", {id: "prevMVAlbum_img", src: ""}));
    this.div.find("#prev_mv_div").append(_$("<img/>", {id: "prevMVAlbum_lock"}));
    this.div.find("#prev_mv_div").append(_$("<div/>", {id: "prevMVAlbum_shadow"}));
    this.div.find("#prev_mv_div").append(_$("<div/>", {id: "prevMV_title"}));
    this.div.find("#prev_mv_div").append(_$("<div/>", {id: "prevMV_artist"}));
    this.div.find("#prev_mv_div").append(_$("<img/>", {id: "prevMVArrow_img", src: "module/module.vod/resource/arw_view_l.png"}));

    this.div.append(_$("<div/>", {id: "mvBottomMenu_div"}));
    this.div.find("#mvBottomMenu_div").append(_$("<img/>", {class: "mvBottomMenu_icon info"}));
    this.div.find("#mvBottomMenu_div").append(_$("<img/>", {class: "mvBottomMenu_icon add"}));
    this.div.find("#mvBottomMenu_div").append(_$("<img/>", {class: "mvBottomMenu_icon related"}));
};

MVPlayBar.prototype.show = function() {
    this.showing = true;
};

MVPlayBar.prototype.hide = function() {
    this.showing = false;
};

MVPlayBar.prototype.getView = function() {
    return this.div;
};

MVPlayBar.prototype.isShowing = function() {
    return this.showing;
};

MVPlayBar.prototype.onKeyAction = function(keyCode) {
    switch (keyCode) {
        case KEY_CODE.LEFT:
            if (this.focusIdx > 0) this.refreshListData(this.focusIdx - 1);
            else {
                if (VODInfoManager.isPlayListLoop() && this.playList.length > 2) this.refreshListData(this.playList.length - 1);
                else this.refreshListData(0);
            }
            return true;
        case KEY_CODE.RIGHT:
            if (this.playList.length > 1 && this.focusIdx < this.playList.length - 1) this.refreshListData(this.focusIdx + 1);
            else {
                if (VODInfoManager.isPlayListLoop() && this.playList.length > 2) this.refreshListData(0);
                else this.refreshListData(this.playList.length - 1);
            }
            return true;
        default:
            return false;
    }
};