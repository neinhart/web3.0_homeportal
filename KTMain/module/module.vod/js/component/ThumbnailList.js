/**
 * Created by ksk91_000 on 2016-09-12.
 */
window.ThumbnailList = function() {
    this.div;
    this.data;
    this.focus = 0;
    this.page = 0;
    this.indicatorArea;
    var closeTimer = null;

    this.setData = function(data) {
        this.data = data || [];
        log.printDbg("************************ThumbnailData!!***********************");
        log.printDbg(this.data);
        log.printDbg("**************************************************************");
        this.div.find("#thumbnail_item_area .itemArea").hide();
        this.data.forEach(function(info, index) { setItem.call(this, index, info); }, this);
        this.setFocus(this.focus = 0);
    };

    this.setIndicatorArea = function(indicatorArea) {
        this.indicatorArea = indicatorArea;
    };

    function setItem(idx, data) {
        var li = this.div.find("#thumbnail_item_area #item_" + idx);
        if (li[0]) {
            li.find(".itemImg").attr({"src": data.file_url, "onerror": "this.src='" + ROOT_URL + "resource/default_thumb_sm.jpg'"});
            if (data.corner_grp_list && data.corner_grp_list.length > 0) {
                // li.find(".arwText").text(data.corner_grp_list[0].cornergrptype == "001" ? ("'" + data.corner_grp_list[0].cornergrpnm + "' 골라보기") : "코너 몰아보기");
                li.find(".arwText").text(data.corner_grp_list[0].cornergrptype == "001" ? "골라보기" : "코너 몰아보기");
                li.find(".itemTitle").text(data.corner_grp_list[0].cornergrpnm);
                li.find(".arrowArea").css("display", "");
            } else {
                li.find(".itemTitle").text(data.time_stamp.split('.')[0]);
                li.find(".arrowArea").hide();
            }
            li.show();
        } else this.div.find("#thumbnail_item_area").append(createItem(idx, data));
    }

    function createItem(idx, data) {
        var item = _$("<div/>", {id: "item_" + idx, class: "itemArea"});
        item.append(_$("<div/>", {class: "arrowArea"}));
        item.find(".arrowArea").append(_$("<span>", {class: "arwText"}));
        item.find(".arrowArea").append(_$("<img>", {class: "arwIcon", src: ROOT_URL + "resource/arw_view_mini_up.png"}));
        item.append(_$("<div/>", {class: "imgArea"}));
        item.find(".imgArea").append(_$("<img>", {class: "itemSdw", src: ROOT_URL + "resource/thum_sdw_w252.png"}));
        item.find(".imgArea").append(_$("<img>", {class: "itemImg", src: data.file_url, onerror: "this.src='" + ROOT_URL + "resource/default_thumb_sm.jpg'"}));
        item.find(".imgArea").append(_$("<div/>", {class: "itemDim"}));
        item.append(_$("<div/>", {class: "itemTitle"}));
        if (data.corner_grp_list && data.corner_grp_list.length > 0) {
            item.find(".itemTitle").text(data.corner_grp_list[0].cornergrpnm);
            // item.find(".arwText").text(data.corner_grp_list[0].cornergrptype == "001" ? ("'" + data.corner_grp_list[0].cornergrpnm + "' 골라보기") : "코너 몰아보기");
            item.find(".arwText").text(data.corner_grp_list[0].cornergrptype == "001" ? "골라보기" : "코너 몰아보기");
            item.find(".arrowArea").show();
        } else {
            item.find(".itemTitle").text(data.time_stamp.split('.')[0]);
            item.find(".arrowArea").hide();
        }

        return item;
    }

    this.setCloseTimer = function() {
        var _this = this;
        if (closeTimer) clearTimeout(closeTimer);
        closeTimer = setTimeout(function() {
            _this.hide.call(_this);
        }, 2000)
    };

    this.clearCloseTimer = function() {
        if (closeTimer) clearTimeout(closeTimer);
        closeTimer = null;
    };

    this.setFocus = function(focus) {
        this.focus = focus;
        this.div.find(".itemArea.focus").removeClass("focus");
        this.div.find("#item_" + focus).addClass("focus");
        // VODManager.getMiniGuide().setIconPosition()
        this.changePage(Math.floor(focus / 6));

        if (this.data[focus]) {
            MiniGuideManager.setThumbnailPosition(HTool.parseVODTime(this.data[focus].time_stamp));
        }
    };

    this.setThumbnailFocus = function(thumbData) {
        var idx = -1;
        var data = this.data;
        for (var i = 0; i < data.length; i++) {
            if (thumbData.file_id == data[i].file_id) {
                idx = i;
                break;
            }
        }
        if (idx > -1) this.setFocus(idx);
    };

    this.changePage = function(page) {
        this.page = page;
        var thumbnailPageWidth = -1 * page * (1690 - 8);
        this.div.find("#thumbnail_item_area").css("-webkit-transform", "translateX(" + thumbnailPageWidth + "px)")
        this.div.find("#left_arrow_area").toggle(page != 0);
        this.div.find("#right_arrow_area").toggle(page != Math.floor((this.data.length - 1) / 6));
    };

    this.hasCorner = function() {
        var data = this.data[this.focus];
        return (data.corner_grp_list && data.corner_grp_list.length > 0);
    };

    this.create();
};

ThumbnailList.prototype.create = function() {
    this.div = _$("<div/>", {id: "thumbnailList"});
    this.div.append(_$("<div/>", {id: "left_arrow_area", class: "arrow_area"}));
    this.div.append(_$("<div/>", {id: "thumbnail_area"}));
    this.div.append(_$("<div/>", {id: "right_arrow_area", class: "arrow_area"}));
    this.div.find("#left_arrow_area").append(_$("<img>", {src: ROOT_URL + "resource/arw_view_mini_l.png"}));
    this.div.find("#right_arrow_area").append(_$("<img>", {src: ROOT_URL + "resource/arw_view_mini_r.png"}));
    this.div.find("#thumbnail_area").append(_$("<div/>", {id: "thumbnail_item_area"}));
};

ThumbnailList.prototype.getView = function() {
    return this.div;
};

ThumbnailList.prototype.show = function(alwaysShow) {
    this.div.css("visibility", "inherit");
    if (!alwaysShow) this.setCloseTimer();
    if (this.indicatorArea) this.indicatorArea.hide();
    else this.clearCloseTimer();
};

ThumbnailList.prototype.hide = function() {
    this.div.css("visibility", "hidden");
    if (this.indicatorArea) this.indicatorArea.show();
    this.clearCloseTimer();
};

ThumbnailList.prototype.onBlurred = function() {
    this.div.addClass("blurred");
};

ThumbnailList.prototype.onFocused = function() {
    this.div.removeClass("blurred");
    this.clearCloseTimer();

    var data = this.data;
    this.setFocus(getFocusIdx());
    function getFocusIdx() {
        var curTime = VODManager.getCurrentPosition();
        var nearIdx = data.length - 1;
        for (var i = 0; i < data.length; i++) {
            if (curTime < HTool.parseVODTime(data[i].time_stamp)) {
                nearIdx = i - 1;
                return nearIdx > 0 ? nearIdx : 0;
            }
        }
        return nearIdx;
    }
};

ThumbnailList.prototype.onKeyAction = function(keyCode) {
    switch (keyCode) {
        case KEY_CODE.ENTER :
            VODManager.seekJumpTo(this.data[this.focus].time_stamp);
            return true;
        case KEY_CODE.LEFT:
            if (this.focus > 0) {
                this.setFocus(this.focus - 1);
            }
            return true;
        case KEY_CODE.RIGHT:
            if (this.focus < this.data.length - 1) {
                this.setFocus(this.focus + 1);
            }
            return true;
        case KEY_CODE.UP:
            var data = this.data[this.focus];
            if (!data.corner_grp_list || data.corner_grp_list.length < 1) return false;
            MiniGuideManager.show(true);
            VODInfoManager.getCorner(function(res) {
                if (res) {
                    log.printDbg(res);
                    res.corner_grp_list = data.corner_grp_list;
                    LayerManager.activateLayer({
                        obj: {
                            id: "VodSelectCornerPopup",
                            type: Layer.TYPE.POPUP,
                            priority: Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {cornerGrpList: res, focusedThumbnail: data}
                        },
                        moduleId: "module.vod",
                        visible: true
                    })
                }
            }, data.content_id, data.corner_grp_list[0].cornergrpid);
            return true;
        default:
            return false;
    }
};