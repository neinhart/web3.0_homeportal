"use strict";

KTW.utils.util = (function () {

    var TEXT_ANIMATION_STATE = {
        START: "text_animation_start",
        MOVE_BOX_START: "text_animation_move_box_start",
        MOVE_BOX_END: "text_animation_move_box_end",
        END: "text_animation_end"
    };
    var TEXT_SLIDE_SPEED = 60;
    var TEXT_SLIDE_DELAY = 1;

    function getCanvas() {
        var tag = document.getElementsByTagName("canvas");
        var element = null;
        if (tag.length > 0) {
            element = tag[0];
        } else {
            //$("body").append("<canvas id='tmpCanvas' style='display:none;'></canvas>");
            // ie 에서 HierarchyRequestError 발생하기 때문에 pure js code로 처리함
            //document.body.innerHTML += "<canvas id='tmpCanvas' style='display:none;'></canvas>";
            //jm.kang 위 코드처럼 추가하면 tstation-broadcastobj 의 shadow 영역의 size 가 변경되지 않음.
            element = document.createElement('canvas');
            element.id='tmpCanvas';
            element.style.display='none';
            document.getElementsByTagName("body")[0].appendChild(element);
        }
		
		return element;
    }
    
    /**
     * get current time
     * 
     * @returns
     */
    function _getCurrentDateTime() {
        var cDate = new Date();
        return  (cDate.getYear()+1900)+
                    _numToStr(cDate.getMonth()+1, 2)+
                    _numToStr(cDate.getDate(), 2)+
                    _numToStr(cDate.getHours(),2)+
                    _numToStr(cDate.getMinutes(),2)+
                    _numToStr(cDate.getSeconds(),2);
    }
    
    /**
     * 시간을 00:00 포멧으로 변경 
     */
    var _getTimeString = function(cTime){
        var tt = new Date(cTime);
        var hh = tt.getHours();
        var hhh = hh < 10 ? "0" + hh : "" + hh;
        var mm = tt.getMinutes();
        var mmm = mm < 10 ? "0" + mm : "" + mm;
        
        return hhh + ":" + mmm;
    };
    
    /**
     * 숫자를 문자로 반환, length 값으로 앞에 '0'을 채운다.
     * 
     * @param no
     * @param length
     * @param max
     * @returns
     */
    function _numToStr(no, length, max) {
        if (max != null && no > max) {
            return max;
        }
        var result = "" + no;
        for (var i = length-1; i > 0; i--) {
            var tmp = Math.pow(10, i);
            if (no < tmp) {
                result = "0" + result;
            }
        }
        return result;
    }
    
    function _homeMenuArrayCopy(arr) {
        var newArr = arr.slice();
        for (var i=0; i<newArr.length; i++) {
            if (newArr[i]) {
                newArr[i] = $.extend(true,{}, arr[i]);
                newArr[i].__proto__ = arr[i].__proto__;
                if (newArr[i].children) {
                    newArr[i].children = _homeMenuArrayCopy(newArr[i].children);
                }
            } else
                newArr[i] = undefined;
        }
        return newArr;
    }
    
    /**
     * make html element width jquery
     * 
     * @param  {Object} options tag     - tag name          - "<div />", "<img />", ....
     *                          attrs   - element attribute - {id: "test", class: "testClass", css: {left: 0, top: 0}, ...}
     *                          text    - element text
     *                          parent  - parent element
     *                          
     * 
     * @return {jQuery Element}
     */
    function _makeElement(options) {
        var element = $(options.tag, options.attrs);
        
        if (options.text !== undefined && options.text !== null) {
            element.html(options.text);
        }
        
        if (options.parent !== undefined && options.parent !== null) {
            options.parent.append(element);
        }
        
        return element;
    }
    
    /*
     * arry를 카피한다. array내에 undefined가 들어간 경우 Object로 카피되는것을 방지한다. 
     */
    function _arrayCopy(arr) {
        var newArr = arr.slice();
        for (var i=0; i<newArr.length; i++) {
            if (newArr[i])
                newArr[i] = $.extend(true,{}, arr[i]);
            else
                newArr[i] = undefined;
        }
        return newArr;
    }    
    function _objectCopy(obj) {
    	var newObj = $.extend(true,{}, obj);
    	newObj.__proto__ = obj.__proto__;
    	return newObj;
    }
    /**
     * Object 를 recursive 하게 모든 properties 삭제.
     */
    function _initObject(object, deleteObj, maxDepth, startdepth){
    	if(startdepth == undefined || startdepth == null) startdepth = 0;
    	if(maxDepth == undefined || maxDepth == null) maxDepth = 10; //10단계 object 까지 clear. 
    	
    	for(var key in object){
    		var obj = object[key];
    		
    		//console.log("removeObj key:"+key + " startdepth:"+ startdepth+ " isarray:"+ (obj instanceof Array)+ " isobj:" + (obj instanceof Object));
    		if(obj instanceof Array){
    			for(var arrKey in obj){
    				if(obj[arrKey] instanceof Object){
    					//상호 참조를 만날경우에만 이경우를 탄다고 보고. 이 경우는 강제로 나간다.
    					if(startdepth < maxDepth){
    						_initObject(obj[arrKey], deleteObj, maxDepth, (startdepth+1));
    					}
    				}else{    					
    					//console.log((deleteObj ? "delete" : "initial") + " array key:" + key + "[" + arrKey + "]");
    					if(deleteObj) delete obj[arrKey];
    					else obj[arrKey] = null;
    				}
    			}
    		}else if(obj instanceof Function){
    			//do nothing. 
    		}else if(obj instanceof Object){
    			if(startdepth < maxDepth){
					_initObject(obj, deleteObj, maxDepth, (startdepth+1));
				}
    		}
    		//console.log((deleteObj ? "delete" : "initial") + " key:" + key);
    		if(deleteObj) delete object[key];
    		else object[key] = null;
    	}
    }

    /**
     * 문자열의 실제 element width 리턴
     * - ex) getTextLength("아야어여오요", "RixHead L", 30, -1.5);
     *
     * @param text : target 문자열
     * @param fontName : 현재 쓰이는 폰트
     * @param fontSize : 현재 설정된 사이즈
     * @param letterSpacing : 자간
     */
    function _getTextLength(text, fontName , fontSize, letterSpacing) {
        var textWidth = 0;
        letterSpacing = letterSpacing ? letterSpacing : 0;
        if (text && text.length > 0) {
            var element = getCanvas();
            var context = element.getContext('2d');
            var ff = "";

            if(fontName !== null && fontName !== "") {
                ff = "" + fontSize + "px '" + fontName + "'";
            }else {
                ff = "" + fontSize + "px 'RixHeadM'";
            }
            context.font = ff;
            /**
             * jjh1117 2016.10.28
             * 텍스트 길이 정보가 소수점까지 전달 되므로 반올림해서 return 되어야함.
             */
            textWidth = Math.ceil(context.measureText(text).width) + (letterSpacing * (text.length - 1));
        }
        
        return textWidth;
    }
    
    /**
     * [jh.lee] 최대 길이를 넘어가면 넘어가는 만큼을 잘라내고 ... 을 붙여주는 함수
     * [jh.lee] 글자크기 및 폰트사이즈에 따라 "..."의 넓이가 달라짐
     * [jh.lee] 그래서 글자크기 및 폰트사이즈에 따른 버퍼를 계산하는 함수로 변경하여 새로 만듬.
     * [dj.son] letterSpacing - 자간 파라미터 추가
     */
    function _elipsis (text, font, size, max_size, postfix, letterSpacing){
    	var element;
    	var buffer_size = 0;
    	var context;
    	var text_style;
    	var text_width;

        letterSpacing ? letterSpacing : 0;
    	
    	element = getCanvas();
    	context = element.getContext("2d");
    	text_style = "" + size + "px '" + font + "'";
    	context.font = text_style;
    	// [jh.lee] 원본 text의 사이즈를 계산
    	text_width = context.measureText(text).width + (letterSpacing * (text.length - 1));
    	// [jh.lee] postfix 값이 존재하지 않으면 디폴트로 "..." 을 사용함.
        postfix = postfix ? postfix : "...";
    	// [jh.lee] 버퍼사이즈 계산
    	buffer_size = _getTextLength(postfix, font, size, letterSpacing);
    	
    	if(max_size >= text_width) {
    		// [jh.lee] 원본 text의 길이가 최대 길이보다 짧으면 그대로 원본 text를 사용
    		return text;
    	}
    	else if (max_size < buffer_size) {
    		// 최대 길이가 postfix 길이보다 작으면 그냥 ""를 return
    		return "";
    	}
    	else {
    		// [jh.lee] 원본 text의 길이가 최대 길이보다 길면 줄인 후 "..."을 붙인다.
    		// [jh.lee] 단, 줄인 text + "..." 의 전체길이가 최대 길이보다는 짧아야하므로 비교할 때 최대 길이 - buffer_size
    		// [jh.lee] 줄이다 줄이다 원본 text까지 전부 줄여버리게 되면 결국 postfix 로 지정한 문자열만 리턴
    		for (var i = 0; i < text.length; i++) {
                var temp_text = text.substring(0, text.length - i) + postfix;
                text_width = context.measureText(temp_text).width + (letterSpacing * (temp_text.length - 1));
                if (max_size - buffer_size >= text_width) {
                    return temp_text;
                }
    		}
    		return postfix;
    	}
    }
    
    /**
     * [jh.lee] 에러메세지가 존재하는 경우 팝업 width 만큼의 길이로 분할
     */
    function _stringToArray(error_desc, width) {
        var text_length;
        var message = [];
        var tmp_text_length;
        var max_width = width ? width : 703;
        
        if (error_desc) {
            // [jh.lee] error_desc 가 존재하는 경우 파싱
            text_length = _getTextLength(error_desc,"RixHead M" , 42,-2.1);
            // [jh.lee] 에러메세지는 baseDialog 를 사용하기 때문에 해당 팝업 메세지 width 기준으로 -20 한 값으로 설정(370px)
            if (text_length <= max_width) {
                // [jh.lee] 팝업 문구가 한줄안에 포함되는 경우
                message[0] = error_desc; 
            } else {
                // [jh.lee] 팝업 문구가 한줄안에 포함되지 않는 경우 파싱
                for(var i = 0, j = 0; i < error_desc.length; i++) {
                    tmp_text_length = _getTextLength(error_desc.substr(0, i+1), "RixHead M" , 42,-2.1);
                    
                    if (tmp_text_length < max_width) {
                        // [jh.lee] 파싱한 문자열 길이가 max_width 보다 작은 겨우
                        if (i+1 === error_desc.length) {
                            // [jh.lee] 에러 메세지의 끝인 경우
                            message[j] = error_desc;
                            break;
                        }
                    } else if (tmp_text_length === max_width) {
                        // [jh.lee] 길이가 max_width 인 경우
                        message[j++] = error_desc.substr(0, i+1);
                        error_desc = error_desc.substr(i+1);
                        i = 0;
                    } else {
                        // [jh.lee] 길이가 넘어가는 경우
                        message[j++] = error_desc.substr(0, i);
                        error_desc = error_desc.substr(i);
                        i = 0;
                    }
                }
            }
            return message;
        } else {
            message[0] = error_desc;
            return message;
        }
    }

    /**
     * jjh1117 2016.10.28
     * 폰트명을 입력하지 않은 Method 추가함
     */
    function _stringToMultiLine(orgText, fontname , fontSize , lineWidth , letterSpacing) {
        var text_length;
        var message = [];
        var tmp_text_length;
        var max_width = lineWidth;

        if (orgText) {
            text_length = _getTextLength(orgText, fontname , fontSize , letterSpacing);
            if (text_length <= max_width) {
                message[0] = orgText;
            } else {
                // [jh.lee] 팝업 문구가 한줄안에 포함되지 않는 경우 파싱
                for(var i = 0, j = 0; i < orgText.length; i++) {
                    tmp_text_length = _getTextLength(orgText.substr(0, i+1), fontname, fontSize , letterSpacing);

                    if (tmp_text_length < max_width) {
                        if (i+1 === orgText.length) {
                            message[j] = orgText;
                            break;
                        }
                    } else if (tmp_text_length === max_width) {
                        // [jh.lee] 길이가 max_width 인 경우
                        message[j++] = orgText.substr(0, i+1);
                        orgText = orgText.substr(i+1);
                        i = 0;
                    } else {
                        // [jh.lee] 길이가 넘어가는 경우
                        message[j++] = orgText.substr(0, i);
                        orgText = orgText.substr(i);
                        i = 0;

                        tmp_text_length = _getTextLength(orgText, fontname, fontSize , letterSpacing);
                        if (tmp_text_length < max_width) {
                            message[j] = orgText;
                            break;
                        }
                    }
                }
            }
            return message;
        } else {
            return orgText;
        }
    }

    
    /**
     * VCategory Object를 array로 묶어주어 리턴한다
     * @param type - ITEM_DETAIL
     * @param list - json data
     * @returns array
     */
    function _getVCategoryList(type, list, parent) {
        var categoryList = null;
        if (list && list.length > 0) {
            categoryList = [];
            for (var i = 0; i < list.length; i++) {
                if (type == "ITEM_DETAIL") {
                    var tempItem = null;

                    if (list[i].itemType === KTW.DATA.MENU.ITEM.INT_M || 
                            list[i].itemType === KTW.DATA.MENU.ITEM.INT_U || 
                            list[i].itemType == KTW.DATA.MENU.ITEM.INT_W) { // 양방향인 경우, 웹뷰
                        tempItem = new VInteractive(list[i].itemType,
                            list[i].itemId, list[i].itemName, parent,
                            list[i].newHot=="H"?"Y":"N", list[i].prInfo, list[i].wonYn,
                            list[i].notice, list[i].imgUrl, list[i].locator, list[i].delDate,
                            list[i].locator2, list[i].menuImgUrl, list[i].parameter,
                            list[i].templateNo, list[i].templateType, list[i].templateLoc,
                            list[i].templateImg, list[i].templateName, list[i].ktCasLocator,
                            list[i].platformGubun, list[i].appImgUrl, list[i].webMenuImgUrl,
                            list[i].w3ImgUrl, list[i].itemSubText, list[i].englishItemName);
                    }
                    else {
                        tempItem = new VCategory(list[i].itemType, list[i].itemId,
                                list[i].itemName, parent, list[i].newHot=="N"?"Y":"N",
                                list[i].prInfo, list[i].wonYn, list[i].templateNo, list[i].templateType,
                                list[i].templateLoc, list[i].templateImg, list[i].templateName,
                                list[i].menuImgUrl, list[i].platformGubun, list[i].imgUrl,
                                list[i].webMenuImgUrl, list[i].notice, list[i].catType, list[i].isHD,
                                list[i].isDolby, list[i].is51Ch, list[i].buyType, list[i].connerId,
                                list[i].sortGb, list[i].itemCnt, list[i].listType, list[i].hybridYn,
                                list[i].hsImgUrl, list[i].hsTargetType, list[i].hsTargetId,
                                list[i].hsLocator, list[i].hsLocator2, list[i].hsKTCasLocator,
                                list[i].englishItemName, list[i].webItemCnt, list[i].webHsImgUrl,

                                list[i].w3HsImgUrl, list[i].itemSubText, list[i].englishItemSubText,
                                list[i].previewImgUrl1, list[i].previewImgUrl2, list[i].previewImgUrl3,
                                list[i].w3ListImgUrl, list[i].posterListType);
                    }

                    var tempData = list[i];
                    for (var key in tempData) {
                        tempItem[key] = tempData[key];
                    }

                    categoryList.push(tempItem);
                }
            }
        }
        return categoryList;
    }

    /* 2017.05.29 dhlee 3.0 에서는 사용하지 않는 함수
    function _findMenuData(menus, menuId){
        var result = null;
        for(var i=0;i<menus.length;i++){
            if(menus[i] && menus[i].id == menuId){
                result = menus[i];
                break;
            }
        }
        
        if(result){
            return result;
        }else{
            for(var i=0;i<menus.length;i++){
                if(menus[i] && menus[i].children && menus[i].children.length){
                    result = _findMenuData(menus[i].children, menuId);
                    if(result){
                        break;
                    }
                }
            }
            return result;
        }
    }
    */

    /**
     * 문자열을 조건에 맞게 잘라준다.
     *
     * @param text 전체 문자열 text
     * @param font 사용하는 font name string
     * @param size font size
     * @param maxSize 영역의 width 정보
     * @returns {Array}
     * @private
     *
     * [dj.son] letterSpacing - 자간 파라미터 추가
     */
    function _splitText (text, font, size, maxSize, letterSpacing){
    	var textArray = [];
    	var element = getCanvas();
    	var context = element.getContext('2d');
    	var ff = "" + size + "px '" + font + "'";
        letterSpacing = letterSpacing ? letterSpacing : 0;
    	context.font = ff;
    	var textWidth = context.measureText(text).width + (letterSpacing * (text.length - 1));
    	if(maxSize > textWidth){
    		textArray[textArray.length] = text;
    	}else{
    		for(var i=0; i < text.length; i++){
    			var bb = text.substring(0, i+1);
    			textWidth = context.measureText(bb).width + (letterSpacing * (bb.length - 1));
    			if(maxSize < textWidth){
    				textArray[textArray.length] = text.substring(0, i);
    				text = text.substring(i);
    				i=0;
    			}
    		}
    		if(text.length > 0){
    			textArray[textArray.length] = text;
    		}
    	}
    	return textArray;
    }
    
    /**
     * [jh.lee] UPDATE 아이콘 추가
     * Event>소장용>NEW>HOT>UPDATE>HD 
     * @param {Object} iconCd
     * @param {Object} isHd
     */
    /*
     * 2017.05.29 dhlee
     * 3.0 에서는 사용하지 않는다.
    function _getIconImg(iconCd, resolCd, isFocus) {
        if (iconCd) {
            if (iconCd == "E") {
                return isFocus == true ? "icon/icon_event_f.png" : "icon/icon_event.png";
            } else if (iconCd == "D") {
                return isFocus == true ? "icon/icon_mine_c_f.png" : "icon/icon_mine_c_d.png";
            } else if (iconCd == "L") {
                return isFocus == true ? "icon/icon_mine_c_f.png" : "icon/icon_mine_c_d.png";
            } else if (iconCd == "N") {
                return isFocus == true ? "icon/icon_new_f.png" : "icon/icon_new.png";
            } else if (iconCd == "H") {
                return isFocus == true ? "icon/icon_hot_f.png" : "icon/icon_hot.png";
            } else if (iconCd == "U") {//Update
                return isFocus == true ? "icon/icon_update_f.png" : "icon/icon_update.png";
            } else if (iconCd == "X") {//독점
                return isFocus == true ? "icon/icon_monopoly_f.png" : "icon/icon_monopoly.png";
//            }else if(iconCd == "O"){//오픈 
//                return isFocus == true ? "icon/icon_hot_f.png":"icon/icon_hot.png";
//            }else if(iconCd == "C"){//시리즈 최종회 
//                return isFocus == true ? "icon/icon_hot_f.png":"icon/icon_hot.png";
            } else if (iconCd == "B") {//할인
                return isFocus == true ? "icon/icon_dc_f.png" : "icon/icon_dc.png";
            } else if (iconCd == "R") {//추천
                return isFocus == true ? "icon/icon_recommend_f.png" : "icon/icon_recommend.png";
            } else if (iconCd == "P") {//인기
                return isFocus == true ? "icon/icon_best_f.png" : "icon/icon_best.png";
            } else if (iconCd == "S") {//가격인하
                return isFocus == true ? "icon/icon_dc_2_f.png" : "icon/icon_dc_2.png";
            } else if (iconCd == "T") {//극장동시
                return isFocus == true ? "icon/icon_theater_f.png" : "icon/icon_theater.png";
            } else {
                if(KTW.vod.ContentUtil.hasUHD(resolCd)) {
                    return isFocus == true ? "icon/icon_vodlist_uhd_f.png" : "icon/icon_vodlist_uhd.png";
                } else if(KTW.vod.ContentUtil.hasHD(resolCd)) {
                    return isFocus == true ? "icon/icon_hd_f.png" : "icon/icon_hd.png";
                } else {
                    return null;
                }
            }
        } else {
            if(KTW.vod.ContentUtil.hasUHD(resolCd)) {
                return isFocus == true ? "icon/icon_vodlist_uhd_f.png" : "icon/icon_vodlist_uhd.png";
            } else if(KTW.vod.ContentUtil.hasHD(resolCd)) {
                return isFocus == true ? "icon/icon_hd_f.png" : "icon/icon_hd.png";
            } else {
                return null;
            }
        }
    }
    */

    function _transPrInfo(prInfo){
    	// [jh.lee] 당신을 위한 VOD 추천에서는 prInfo 값이 실제 연령제한 값으로 넘어온다.
        if(prInfo == '02' || prInfo === "7"){
            return "7";
        }else if(prInfo == '03' || prInfo === "12"){
            return "12";
        }else if(prInfo == '04' || prInfo === "15"){
            return "15";
        }else if(prInfo == '05' || prInfo === "19"){
            return "19";
        }else{
            return "all";
        }
    }

    /**
     * 2017.05.29 dhlee
     * 전달 받은 prInfo 와 STB의 PR을 비교하여 연령제한 여부를 return 한다.
     *
     * @param prInfo
     * @returns {boolean}
     */
    function _isLimitAge(prInfo){
        var isLimit = false;
        var prRating;
        
        if (KTW.PLATFORM_EXE === "PC") {
            // [jh.lee] PC 테스트 용
            prRating = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.PARENTAL_RATING);  
        } else {
            // [jh.lee] 웹홈에서는 casAdapter를 이용해서 받아오도록 수정.
            //prRating = KTW.oipf.AdapterHandler.casAdapter.getParentalRating();
            // 2017.05.26 AuthManager에서 가져오도록 한다.
            prRating = KTW.managers.auth.AuthManager.getPR();
        }
        
        if(_transPrInfo(prInfo) != "all" && prRating != 0 /* mook_t ParentalHandler.PR_NONE*/){ //비밀번호 인증이 필요할때
            if((Number(_transPrInfo(prInfo)) >= Number(prRating))){ //컨텐츠가 지정한 연령제한 이상이라면
                isLimit = true;
            }
        }
        
        return isLimit;
    }


    function _getResolCd(resolCd) {
        if (resolCd) {
            var resol_cd = resolCd.split("|");
            if (resol_cd.length === 1) {
                switch (resol_cd[0]) {
                    case "H" :
                    case "HD":
                    case "Y":
                        return ["Y", "", "", ""];
                    case "FHD":
                    case "F":
                        return ["", "", "F", ""];
                    case "UHD":
                    case "U":
                        return ["", "", "", "U"];
                    case "S" :
                    case "SD":
                    case "N":
                        return ["", "N", "", ""];
                }
            } else {
                return resol_cd;
            }
        } else {
            return ["", "", "", ""];
        }
    }

    function _checkPosterResolutionIcon(obj) {
        var arr = [];

        var count = 0;
        if (obj.hdr === "Y" && KTW.CONSTANT.IS_HDR === true) {
            // 해당 컨텐츠가 hdr 컨텐츠이고 단말이 hdr을 지원하는 경우
            arr.push("HDR");
            count++;
        }
        if(obj.resolCd) {
            var resolCd = _getResolCd(obj.resolCd);
            if(resolCd && resolCd.length>=4 && resolCd[3] !== null && resolCd[3] !== "") {
                arr.push("UHD");
                count++;
            }
        }

        if(count < 2) {
            if (obj.isDvdYn === "Y") {
                // 소장용 인 경우
                arr.push("DVD");
            }
        }

        return arr;

    }
    
    function _transPageNumber(number){
        var pageNumber = String(number);
        if(pageNumber.length == 1){
            pageNumber = "00" + pageNumber;
        }else if(pageNumber.length == 2){
            pageNumber = "0" + pageNumber;
        }
        return pageNumber;
    }
    
    /**
	 * prog_list 에서 현재 재생 중인 프로그램의 index 값을 리턴한다.
	 * @param prog_list 확인하려는 프로그램 목록
	 * @returns {Integer} Index prog_list 에서 현재 재생 중인 프로그램의 index 값.
	 */
    function _getPlayingProg(prog_list) {
    	if (prog_list == null)
    		return null;
    	
    	var prog = null;
    	for (var i=0; i<prog_list.length; i++) {
    		prog = prog_list[i];
    		if (_isPlayingProg(prog))
    			return i;
    	}
    	return null;
    }
    /**
     * 해당 프로그램이 현재 재생 중인지를 알려준다.
     * @param prog 확인하려는 프로그램
     * @returns {Boolean} true: 현재 재생 중, false: 지나간 프로그램 혹은 미래 프로그램.
     */
    function _isPlayingProg(prog) {
    	if (prog == null || prog.startTime == null || prog.duration == null)
    		return false;
    	
    	var now = new Date().getTime();
    	var IS_TIME_UNIT_SEC = KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC;
    	if (IS_TIME_UNIT_SEC === true) {
        	now = Math.floor(now/1000);
    	}
    	if (prog.startTime <= now && now < prog.startTime + prog.duration ) {
    		return true;
    	}
    	return false;
    }
    
    function _renameDupId(parantId, tag, id, isVodDetail){
        var _tempLength = $(parantId+" "+tag+"[id^='"+id+"']").length;
        var _tempId = id;
        if(_tempLength > 0){
            _tempId = id + "_"+_tempLength;
        }else if(isVodDetail && _tempLength == 0){
        	_tempId = id + "_0";
        }
        return _tempId;
    }
    
    function _getLayerCount(parentId, tag, id) {
    	return $(parentId+" "+tag+"[id^='"+id+"']").length;
    }
    
    function _transformX(dom, px){
    	dom.css("webkit-transform","translateX("+px+"px)");
		dom.css("ms-transform","translateX("+px+"px)");
    }
    
    function _transformY(dom, px){
    	dom.css("webkit-transform","translateY("+px+"px)");
		dom.css("ms-transform","translateY("+px+"px)");
    }
    function _scaleX(dom, val){
        dom.css("webkit-transform","scaleX("+val+")");
        dom.css("ms-transform","scaleX("+val+")");
    }
    function _scaleY(dom, val){
        dom.css("webkit-transform","scaleY("+val+")");
        dom.css("ms-transform","scaleY("+val+")");
    }
    
    /**
     * [jh.lee] X, Y를 모두 지정하는 함수
     */
    function _transformXY(dom, x_px, y_px) {
    	dom.css("webkit-transform", "translateX(" + x_px + "px)translateY(" + y_px + "px)");
		dom.css("ms-transform", "translateX(" + x_px + "px)translateY(" + y_px + "px)");
    }
    
    function _getFormatDate(/**Date*/dt, f){
        if (!dt.valueOf()) 
            return " ";
        var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];    
        return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, 
                        function($1) {
                            var h;
                            switch ($1) {
                                case "yyyy": return dt.getFullYear();
                                case "yy": return _numToStr((dt.getFullYear() % 1000),2);
                                case "MM": return _numToStr(dt.getMonth() + 1,2);
                                case "dd": return _numToStr(dt.getDate(),2);
                                case "E": return weekName[dt.getDay()];
                                case "HH": return _numToStr(dt.getHours(),2);
                                case "hh": return _numToStr(((h = dt.getHours() % 12) ? h : 12),2);
                                case "mm": return _numToStr(dt.getMinutes(),2);
                                case "ss": return _numToStr(dt.getSeconds(),2);
                                case "a/p": return dt.getHours() < 12 ? "오전" : "오후";
                                default: return $1;
                             }
                        });
    }
    
    function _setComma(number){
        var reg = /(^[+-]?\d+)(\d{3})/; 
        number += "";
        while(reg.test(number)){
            number = number.replace(reg,'$1'+","+"$2");
        }
        return number;
    }
    
    function stripComma(str) {
        var re = /,/g;
        return str.replace(re, "");
    }
    
    function _formatComma(num, pos) {
        if(!num){
            return num;
        }else{
            if (!pos) 
                pos = 0;  //소숫점 이하 자리수
            var re = /(-?\d+)(\d{3}[,.])/;
       
            var strNum = stripComma(num.toString());
            var arrNum = strNum.split(".");
       
            arrNum[0] += ".";
       
            while (re.test(arrNum[0])) {
                arrNum[0] = arrNum[0].replace(re, "$1,$2");
            }
       
            if (arrNum.length > 1) {
                if (arrNum[1].length > pos) {
                    arrNum[1] = arrNum[1].substr(0, pos);
                }
                return arrNum.join("");
            } else {
                return arrNum[0].split(".")[0];
            }    
        }
    }

    function _isFloat (num) {
        var tempNum = "" + num;

        if (tempNum[0] === ".") {
            return false;
        }

        for (var i = 1; i < tempNum.length; i++) {
            if (tempNum[i] === ".") {
                return true;
            }

            if (isNaN(tempNum[i])) {
                return false;
            }
        }

        return false;
    }
    
    /**
     * [jh.lee] 아이콘종류별 이미지와 너비를 리턴해주는 함수
     * 2017.05.29 dhlee 3.0 에서는 사용하지 않는 함수
     */
    /*
    function _getFocusIconWithWidth(icon_cd, resolCd){
    	if(icon_cd){ 
        	if(icon_cd == "E"){
                return ["icon_event_f.png", 66];
        	}else if(icon_cd == "D"){
                return ["icon_mine_c_f.png", 66];
        	}else if(icon_cd == "L"){
                return ["icon_mine_c_f.png", 66];
            }else if(icon_cd == "N"){
                return ["icon_new_f.png", 50];
            }else if(icon_cd == "H"){
                return ["icon_hot_f.png", 50];
            }else if(icon_cd == "U"){//Update
                return ["icon_update_f.png", 66];
            }else if(icon_cd == "X"){//독점 
                return ["icon_monopoly_f.png", 50];
//            }else if(icon_cd == "O"){//오픈 
//                return ["icon_hot_f.png"];
//            }else if(icon_cd == "C"){//시리즈 최종회 
//                return ["icon_hot_f.png"];
            }else if(icon_cd == "B"){//할인 
                return ["icon_dc_f.png", 50];
            }else if(icon_cd == "R"){//추천 
                return ["icon_recommend_f.png", 50];
            }else if(icon_cd == "P"){//인기 
                return ["icon_best_f.png", 50];
            }else if(icon_cd == "S"){//가격인하 
                return ["icon_dc_2_f.png", 68];
            }else if(icon_cd == "T"){//극장동시 
                return ["icon_theater_f.png", 70];
            }else{
                if (KTW.vod.ContentUtil.hasUHD(resolCd)) {
                    return ["icon_vodlist_uhd_f.png", 47];
                } else if (KTW.vod.ContentUtil.hasHD(resolCd)) {
                    return ["icon_hd_f.png", 36];
                } else{
                    return null;
                }
            }
        }else{
            if (KTW.vod.ContentUtil.hasUHD(resolCd)) {
                return ["icon_vodlist_uhd_f.png", 47];
            } else if (KTW.vod.ContentUtil.hasHD(resolCd)) {
                return ["icon_hd_f.png", 36];
            } else{
                return null;
            }
        }
    }
    */
    
    /**
     * 텍스트를 원하는 날짜 포맷으로 변경한다.
     * 20120101 --> 2012.01.01 / 2012-01-01 / 2012/01/01
     * @param text - 대상 텍스트
     * @param sprt - 원하는 구분자 
     */
    function _transDateStr(text, sprt) {
    	var returnVal = "";
    	returnVal = text.substr(0, 4) + sprt;
    	
    	returnVal += text.substr(4, 2) + sprt;
    	
    	returnVal += text.substr(6, 2);
    	
    	return returnVal;
    } 
    
    /**
     * 요일 변환 함수
     */
    function _transDay(day, lang){
    	if(day == 1){
    		if(lang == "kor"){
    			return "월";
    		}else{
    			return "MON";
    		}
    	}else if(day == 2){
    		if(lang == "kor"){
    			return "화";
    		}else{
    			return "TUE";
    		}
    	}else if(day == 3){
    		if(lang == "kor"){
    			return "수";
    		}else{
    			return "WED";
    		}
    	}else if(day == 4){
    		if(lang == "kor"){
    			return "목";
    		}else{
    			return "THU";
    		}
    	}else if(day == 5){
    		if(lang == "kor"){
    			return "금";
    		}else{
    			return "FRI";
    		}
    	}else if(day == 6){
    		if(lang == "kor"){
    			return "토";
    		}else{
    			return "SAT";
    		}
    	}else{
    		if(lang == "kor"){
    			return "일";
    		}else{
    			return "SUN";
    		}
    	}
    }
    
 	/**
 	 * [jh.lee] 넘겨 받은 value 값을 * 로 변경 (setting.js 에서 이동)
 	 */
 	function _tranAstrix(value) {
		var return_value = "";
		
		if (!value) {
			return "";
		}
		for (var i = 0; i < value.length; i++) {
			return_value += "*";
		}
		return return_value;
	}
 	
 	function _getCpUseYnType(cp_use_yn) {
 		var str = "" + cp_use_yn;
 		var result = "";
 		
        if (str ===  "0" ) {
        	// 일반결제
            result = "일반결제";
        } else if (str === "1" ) {
        	// 선불권
            result = "선불권";
        } else if (str === "2" ) {
        	// 쿠폰
            result = "tv쿠폰";
        } else if (str === "3" ) { 
        	// tv포인트
            result = "TV포인트";
        } else if (str === "4" ) { 
        	// 휴대폰결제
            result = "휴대폰결제";
        } else if (str === "C" ) {
        	// 올레클럽 별
            result = "올레클럽 별";
        } else if (str === "9" ) {
        	// 이용권
            result = "콘텐츠이용권";
        } else if (str === "N") {
        	result = "일반결제";
        } else {
        	result = "정보없음";
        }
 	    return result;
 	}
 	
 	/**
 	 * 250D16H44M과 같은 문자열을 변환
 	 * 365D이상인 경우 해지 전까지  
 	 * 2D이상인 경우 일단위 표시
 	 * 2D이하는 시간 단위 표시 
 	 * 1H이하는 분 단위표시  
 	 */
 	function _getExpireDate(str) {
 		var minus = str.indexOf("-");
 		if (minus >= 0 ) {
 			return "종료";
 		}
 		
 		if (str.indexOf("PERM") >= 0) {
 			return "해지 전까지";
 		}
 		
 		var day_index = str.indexOf("D");
 		
 		if (day_index < 0) {
 			var hour_index = str.indexOf("H");
 			if (hour_index < 0) {
 				var min_index = str.indexOf("M");
 				if (min_index < 0) {
 					return "종료";
 				} else {
 					return str.substr(0, min_index)+"분";
 				}
 			} else {
 				return str.substr(0, hour_index) + "시간";
 			} 
 		} else {
 			var days = parseInt(str.substr(0, day_index));
 			if (days >= 2 && days < 365) {
 				return days + "일";
 			} else if (days >= 365) {
 				return "해지 전까지";
 			} else if (days === 1) {
 				var hour_index = str.indexOf("H");
 				return (parseInt(str.substr(day_index+1, hour_index-(day_index+1))) + 24) +"시간";
 			} else { //무조건 0 
 				var hour_index = str.indexOf("H");
 				var hour = parseInt(str.substr(day_index+1, hour_index-(day_index+1)));
 				
 				if (hour === 0) {
 					var min_index = str.indexOf("M");
 					return (parseInt(str.substr(hour_index+1, min_index-(hour_index+1)))) +"분";
 				} else {
 					return (parseInt(str.substr(day_index+1, hour_index-(day_index+1)))) +"시간";
 				}
 			}
 		}
 	}
    
 	/**
 	 * [jh.lee] 마지막 파라미터에 type 추가
 	 * [jh.lee] util.showErrorPopup 사용하는 곳 중 아직 재부팅 팝업으로 노출해야하는 경우는 E013 인 경우 하나임.
 	 * [jh.lee] 향후 재부팅 팝업으로 노출 필요시 type 값 추가하여 생성
 	 */

    function _showCasErrorPopup(popupType , layerId , messages, title, callback , type , enableDCA , enableChannelKey , enableMenuKey , enableExitKey , enableBackKey , enableHotKey , hideAnyKey , bypassKey){

        var params = {};

        if(enableDCA !== undefined && enableDCA !== null) {
            params.enableDCA = enableDCA;
        }

        if(enableChannelKey !== undefined && enableChannelKey !== null) {
            params.enableChannelKey = enableChannelKey;

        }

        if(enableMenuKey !== undefined && enableMenuKey !== null) {
            params.enableMenuKey = enableMenuKey;

        }
        if(enableExitKey !== undefined && enableExitKey !== null) {
            params.enableExitKey = enableExitKey;

        }
        if(enableExitKey !== undefined && enableExitKey !== null) {
            params.enableExitKey = enableExitKey;

        }
        if(enableBackKey !== undefined && enableBackKey !== null) {
            params.enableBackKey = enableBackKey;

        }
        if(enableHotKey !== undefined && enableHotKey !== null) {
            params.enableHotKey = enableHotKey;

        }
        if(hideAnyKey !== undefined && hideAnyKey !== null) {
            params.hideAnyKey = hideAnyKey;

        }
        if(bypassKey !== undefined && bypassKey !== null) {
            params.bypassKey = bypassKey;

        }

        _showErrorPopup(popupType , layerId , messages, title, callback, undefined,  type ,  params);
    }

    function _showCommonErrorPopup(){
        _showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined ,["서비스가 일시적으로 원활하지 않습니다" , "셋톱 재부팅 시 해결 될 수 있습니다" , "재부팅 버튼을 선택하면 자동 재부팅 됩니다"], null, null, true,"reboot");
    }


    function _showErrorPopup(popupType , layerId , messages, title, callback, isShowSaid, type , params){
        KTW.utils.Log.printDbg("_showErrorPopup(), title = " + title + " , popupType : " + popupType + " , type : " + type);
        var desc = [];

        if (messages) {
            if(messages instanceof Array){
                // [jh.lee] error_message 가 존재하는 경우
                for(var i = 0; i < messages.length; i++) {
                    desc[i] = messages[i];
                }
            }else{
                desc[0] = messages;
            }

        }



        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: [(title !== undefined && title !== null) ? title : "시스템 오류"],
                cssObj: {}
            }],

            arrButton: [],
            isShowSaid : (isShowSaid !== undefined && isShowSaid !== null) ? isShowSaid : false ,
            cbAction: function (buttonId) {
                var consumed = false;
                if (!isCalled) {
                    isCalled = true;

                    if(buttonId === "ok") {
                        KTW.ui.LayerManager.deactivateLayer({id:(layerId !== undefined ? layerId : "popup_common_error")});
                        if(callback) {
                            callback(buttonId);
                        }
                    }else if(buttonId === "reboot") {
                        KTW.oipf.AdapterHandler.extensionAdapter.reboot();
                        KTW.ui.LayerManager.deactivateLayer({id:(layerId !== undefined ? layerId : "popup_common_error")});
                        if(callback) {
                            callback(buttonId);
                        }
                    }else {
                        if(callback !== undefined && callback !== null) {
                            if(buttonId !== KTW.KEY_CODE.OK) {
                                consumed = callback(buttonId);
                                if(consumed === true) {
                                    KTW.ui.LayerManager.deactivateLayer({id:(layerId !== undefined ? layerId : "popup_common_error")});
                                }else {
                                    isCalled = false;
                                }
                            }else {
                                isCalled = false;
                                consumed = true;
                            }
                        }
                    }

                    return consumed;
                }
            }
        };

        /**
         * [dj.son] [WEBIIIHOME-3209]
         * - cas error popup 띄우기 위해 showErrorPopup 호출시, type 이 confirm 인 경우 확인 버튼 띄우도록 수정
         */
        if (!type) {
            popupData.arrButton.push({id: "ok", name: "확인"});
        }
        else {
            if (type === "reboot") {
                popupData.arrButton.push({id: "ok", name: "확인"});
                popupData.arrButton.push({id: "reboot", name: "재부팅"});
            }
            else if (type === "confirm") {
                popupData.arrButton.push({id: "ok", name: "확인"});
            }
        }

        for (var i = 0; messages && messages instanceof Array && i < messages.length; i++) {
            popupData.arrMessage.push({
                type: "MSG_42",
                message: [desc[i]],
                cssObj: {}
            });
        }

        var localParams = {};
        if(params !== undefined) {
            params.data = popupData;
        }else {
            localParams.data = popupData;
        }
        
        
        var layerOptions = {
            obj: {
                id: layerId !== undefined ? layerId : "popup_common_error",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: popupType !== undefined ?  popupType :  KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: params !== undefined ? params : localParams
            },
            visible: true,
            cbActivate: function () {}
        };
        KTW.ui.LayerManager.activateLayer(layerOptions);
    }
    
    function _nbsp(n){
        var ret = "<pre style='display:inline'>";
        
        if(!n)
            n = 1;
        
        for(var i = 0 ; i < n ; i++){
            ret += " ";
        }
        
        return ret + "</pre>";
    }
    
    function makeKey(){
        var key = KTW.SAID + ' ' + KTW.SAID.split("").reverse().join("");
        
        //24bit 키가 되도록.
        while(key.length === 24){
            key += ' ';
        }
        
        return key;
    }
    
    /**
     * asscii 값 안에 수만 가능.
     */
    function _encrypt(message){
        return des(makeKey(), message, 1, 0);
    }
    function _decrypt(encryptMessage){
        return des(makeKey(), encryptMessage, 0, 0);
    }
    
    function _keyCodeToNumber(key_code) {
        var number = -1;
        
        switch(key_code) {
            case KTW.KEY_CODE.NUM_0 :
                number = 0;
                break;
            case KTW.KEY_CODE.NUM_1 :
                number = 1;
                break;
            case KTW.KEY_CODE.NUM_2 :
                number = 2;
                break;
            case KTW.KEY_CODE.NUM_3 :
                number = 3;
                break;
            case KTW.KEY_CODE.NUM_4 :
                number = 4;
                break;
            case KTW.KEY_CODE.NUM_5 :
                number = 5;
                break;
            case KTW.KEY_CODE.NUM_6 :
                number = 6;
                break;
            case KTW.KEY_CODE.NUM_7 :
                number = 7;
                break;
            case KTW.KEY_CODE.NUM_8 :
                number = 8;
                break;
            case KTW.KEY_CODE.NUM_9 :
                number = 9;
                break;
        }
        
        return number;
    }
    
    function _getStringToMilliseconds(time_str) {
        
        var date = new Date();
        if (time_str) {
            var year = time_str.substring(0,4);
            var month = parseInt(time_str.substring(4,6)) - 1;
            var day = time_str.substring(6,8);
            var hour = time_str.substring(8,10);
            var min = time_str.substring(10,12);
            
            date = new Date(year, month, day, hour, min);
        }
        
        return date.getTime();
    }
    
    function _startsWith(str, comp) {
        if (typeof String.prototype.startsWith != 'function') {
            String.prototype.startsWith = function( str ) {
                return this.substring( 0, str.length ) === str;
            }
        }
        
        return str.startsWith(comp);
    }
    
    function _isValidVariable(obj, keys) {
        
        var is_valid = true;
        if (obj === null || obj === undefined || obj === "") {
            is_valid = false;
        }
        
        if (is_valid === true && keys !== null && keys !== undefined) {
            for (var key in keys) {
                if (obj.hasOwnProperty(keys[key]) === false) {
                    is_valid = false;
                    break;
                }
            }
        }
        
        return is_valid;
    }
    
    function _isNumber(s) {
    	s += ''; // 문자열로 변환
	    s = s.replace(/^\s*|\s*$/g, ''); // 좌우 공백 제거
	    
	    if (s == '' || isNaN(s)) 
	    	return false;
	    
	    return true;
    }
    
    function _readUTF16String(bytes, stringData) {
    	var string = "";
    	var bigEndian = true;
        //KTW.utils.Log.printDbg("_readUTF16String() bytes.length = [" + bytes.length +"]");
        //KTW.utils.Log.printDbg("_readUTF16String() stringData.length = [" + stringData.length +"]");
    	try{
    		var ix = 0;
    		var offset1 = 1, offset2 = 0;
    		if( stringData.slice(0,2) == "\xFE\xFF") {
    			bigEndian = true;
    			ix = 2;
    		} else if( stringData.slice(0,2) == "\xFF\xFE") {
    			bigEndian = false;
    			ix = 2;
    		}

    		if( bigEndian ) {
    			offset1 = 0;
    			offset2 = 1;
    			
    		}else{
    			//printlog("22222222222222222222222222222");
    		}

    		for( ; ix < bytes.length; ix+=2 ) {
    			var byte1 = bytes[ix+offset1];
    			var byte2 = bytes[ix+offset2];

                if(byte2 !== undefined && byte2 !== null) {
                    var word1 = (byte1<<8)+byte2;
                    if(byte1 < 127){
                        string += String.fromCharCode(byte2);
                    }else{
                        string += String.fromCharCode(word1);
                    }
                }else {
                    break;
                }
    		}

        }catch(e){
    		console.log(e);
    	}
        //KTW.utils.Log.printDbg("_readUTF16String() string = [" + string +"]");
        //KTW.utils.Log.printDbg("_readUTF16String() string.length = [" + string.length+"]");
    	return string;
    }
    
    /**
     * a: 데이타 array. 
     * b: object 중 비교대상 key string
     * l: 생략 가능 quicksort 의 시작점.
     * r: 생략 가능 quicksort 의 끝점
     * 
     * recusive call 없음.
     */
    /*
    function _quickSort(a, k, l, r) {
    	// a: array to sort, k: key to sort by,
        // l, r: optional array index array range
     
        // i: stack index, s: stack,
        // p: pivot index, v: pivot value,
        // t: temporary array item,
        // x, y: partion low/high
     
        var i, s, p, v, t, x, y;
     
        l = l || 0;
        r = r || a.length - 1;
     
        i = 2;
        s = [l, r];
     
        while (i > 0) {
            r = s[--i];
            l = s[--i];
     
            if (l < r) {
                // partition
     
                x = l;
                y = r - 1;
     
                p = l;
                v = a[p];
                a[p] = a[r];
     
                while (true) {
                    while (
                        x <= y &&
                        a[x] != undefined &&
                        a[x][k] < v[k])
                        x++;
                    while (
                        x <= y &&
                        a[y] != undefined &&
                        a[y][k] >= v[k])
                        y--;
                    if (x > y)
                        break;
                    t = a[x];
                    a[x] = a[y];
                    a[y] = t;
                }
     
                a[r] = a[x];
                a[x] = v;
     
                // end
     
                s[i++] = l;
                s[i++] = x - 1;
                s[i++] = x + 1;
                s[i++] = r;
            }
        }
    }
    */
    
    /**
     * [jh.lee] 프로그램 정보에서 타입을 추출
     */
    function _getExtendedEventDescriptor(arr){
    	if (arr == null)  {
    		return null;
    	}
    	
        var eed = new Object();
        var idx = 0;
        
        try{
            for(var i=0; i < arr.length; i++){
                idx = 7;
                var data = arr[i];
                var itemDescLength = data.charCodeAt(idx++);
                var itemDesc = "";
                for(var j=0; j < itemDescLength; j++){
                    var tmp = data.charCodeAt(idx++);
                    if(tmp != 0 && tmp != 17){
                        itemDesc += String.fromCharCode(tmp);
                    }
                }
                var itemLength = data.charCodeAt(idx++);
                var item = "";
                for(var j=0; j < itemLength; j++){
                    var tmp = data.charCodeAt(idx++);
                    if(tmp != 0 && tmp != 17){
                        item += String.fromCharCode(tmp);
                    }
                }
                //printlog("itemDesc ===================== " + itemDesc);
                if(itemDesc == "VCID"){
                    eed.vcId = item;
                }else if(itemDesc == "VAID"){
                    eed.vaId = item;
                }else if(itemDesc == "CN"){
                    eed.cn = item;
                }else if(itemDesc == "LinkType"){
                    eed.linkType = item;
                }else if(itemDesc == "Loc"){
                    eed.locator = item;
                }else if(itemDesc == "rsl"){
                    eed.hdType = item;
                }
                //printlog("item ===================== " + item);
            }
        }catch(e){
            console.log(e);
            return null;
        }

        if(eed.vcId){
            return eed;
        }else{
            return null;
        }
    }
    
    function _isFavID(ch, ids){
        if (ch) {
            var list = null;
            if(ch.cacheFavIDs){
                list = ch.cacheFavIDs;
            }else{
                list = ch.favIDs;
                ch.cacheFavIDs = list;
            }
            if(list){
                for(var i=0; i < list.length; i++){
                    for(var j=0; j < ids.length; j++){
                        if(list[i] == "favourite:" + ids[j]){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    function _isSkyChoiceChannel(ch) {
    	if (ch != null && ch.favIDs != null) {
    		if (ch.favIDs.indexOf(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_PPV) > -1) {
    			return true;
    		}
    	}
    	return false;
    }
    
    function _getPkgCode() {
    	var result = KTW.managers.http.amocManager.getCustPkgList("saId=" + KTW.SAID, undefined, true);
		var base_pkg_code = "";
		var pkg_code = "";
		
		if (result) {
			var package_list = JSON.parse(result.responseText).pkgList;
			if (package_list.length) {
				for (var i=0; i<package_list.length; i++) {
					if (package_list[i].pkgClass == "1")
						base_pkg_code = package_list[i].pkgCode;
					else
						pkg_code += package_list[i].pkgCode + ",";
				}
			} else {
				base_pkg_code = package_list.pkgCode;
				pkg_code = "";
			}
			
			if (pkg_code)
				pkg_code = pkg_code.substring(0, pkg_code.length -1);
		} else {
			// 2016.09.05 dhlee R6 형상 CEMS 적용
			// sync 호출이기 때문에 result 가 없는 경우 리포트하도록 구현
			KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("VODE-00013");
	}
		
		return {
			'base' :  base_pkg_code,
			'additional' : pkg_code
		}
    }
    
    function _makeImageUrl(url, defaul_image_path) {
    	var suffix = "?w=183&h=263&quality=90";
    	return url ? url + suffix  : (defaul_image_path ? defaul_image_path : /*KTW.CONSTANT.IMAGE_PATH + "default_vod.png"*/"");
    }
    
    function _getPointString(point_value) {
    	if(point_value) {
    		point_value = point_value * 1; //string to integer.
        	if(point_value != 0) {
        		point_value = point_value.toFixed(1);
        	}
        	
        	return point_value;
    	}
    	
    	return 0;
    }

    // app.properties 관련 내용 삭제
    //function _getProperty(key) {
    //	var value = jQuery.i18n.prop(key);
    //
    //	if("[" + key + "]" == value) {
    //		value = "";
    //	}
    //
    //	return value;
    //}
    
    function _convertToInt(/*Byte[]*/ value) {
        var u_byte_max_value = 256;
        
        if (value == undefined || value.length != 4) {
            return undefined;
        }
        var converted_value = 0;

        for (var i = 0; i < 4; i++) {
            converted_value += (value[i] < 0 ? value[i] + u_byte_max_value : value[i]);
            if (i < 3) {
                converted_value <<= 8;
            }
        }
        return converted_value;
    }

    /**
     * [KTWHPTZOF-1782, KTWEB-4] 이슈 수정 사항
     * TV 인터넷 상태에서 시스템 팝업 노출 시 마우스 OFF 해야 팝업으로 키전달이 가능
     * DATA 상태 중 full browser 상태일 경우 mouse disable 처리함
     * @param enable
     */
    function _changeMouseControlStatus (enable) {
        // [KTWHPTZOF-1782, KTWEB-4] 이슈 수정 사항
        // TV 인터넷 상태에서 시스템 팝업 노출 시 마우스 OFF 해야 팝업으로 키전달이 가능
        // DATA 상태 중 full browser 상태일 경우 mouse disable 처리함
        if (KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
            if (KTW.oipf.AdapterHandler.extensionAdapter.getOSDState() === 1) {
                if (enable === true) {
                    KTW.oipf.AdapterHandler.hwAdapter.mouseControl.restoreMouseControl();
                    KTW.oipf.AdapterHandler.hwAdapter.mouseControl.status = "on";
                }
                else {
                    KTW.oipf.AdapterHandler.hwAdapter.mouseControl.disableMouseControl();
                    KTW.oipf.AdapterHandler.hwAdapter.mouseControl.status = "off";
                }
            }
        }
    }

    /**
     * return App's absolute path
     *
     * ex) "file:///mnt/usb1/web3.0/"
     */
    function _getAppPath () {
        var appPath = window.location.href;

        appPath = appPath.replace(window.location.search, "");

        if (appPath.indexOf("index.html") > -1) {
            appPath = appPath.substring(0, appPath.indexOf("index.html") - 1);
        }

        if (appPath[appPath.length - 1] !== "/") {
            appPath += "/";
        }

        return appPath;
    }

    /*
    * [sw.nam] 2017-02-21
    * APIs that are used in HTool.js(it's on settingModule.) moved to utils.js
    * */
    function _indexPlus(curIdx, maxIndex) {
        return (curIdx + 1) % maxIndex;
    }

    function _indexMinus(curIdx, maxIndex) {
        return (curIdx - 1 + maxIndex) % maxIndex;
    }

    /**
     * 연산된 인덱스값 반환. 방향키나 OK키로 depth 만큼 이동되는 메뉴, 기타 인덱스 연산등에서 사용
     * @param curIdx 현재 인덱스
     * @param depth 이동(키) 값
     * @param len 전체 메뉴길이
     * @return 연산된 인덱스 값
     */
     function _getIndex (curIdx,depth,len) {
        curIdx += depth;
        if (curIdx < 0)
            curIdx += len;
        else if (curIdx >= len)
            curIdx -= len;
        return curIdx;
    }


    /**
     * title sliding animation 을 지원하는 함수
     * - element 구조는 상황마다 다르기 때문에 굉장히 제한적인 조건에서만 호출한다고 가정
     * - 애니메이션은 css 의 transition, transform 사용
     * - 사용 조건
     *   - 타겟의 엘레먼트 구조는 아래와 같다
     *     : <div id="targetBox"> <span id="targetSpan">target title target title target title target title</span> </div>
     *     : 타겟박스는 size 가 정해져 있고 overflow 가 hidden 이다.
     *     : span 은 width 를 임의로 설정하지 않는다. (browser 가 자동으로 계산할 수 있게끔)
     *   - 기본적으로 애니메이션 동작이 event listener 기반이므로, 애니메이션 clear 는 사용자가 책임을 갖는다.
     * - 사용 예제
     *     : HomeRecommendView 의 setTitleAnimation 참조
     *
     *
     * @param options targetBox - 타겟 span 태그를 자식으로 가지고 있는 타겟 div (jquery div)
     *                 speed - 애니메이션 속도 (default 125)
     *                 delayTime - 애니메이션 시작시, delay 시간
     *                 isMoveBoxEnd - true 일 경우 target 이 -100% 까지 갔을때 애니메이션을 box 끝부분에서 시작, false 일 경우 box 시작부분부터 시작
     */
    function _startTextAnimation (options) {
        if (!options || !options.targetBox) {
            return;
        }

        var targetBox = options.targetBox;
        var boxWidth = options.targetBox.width();
        var titleElement = options.targetBox.find("span");
        var titleWidth = titleElement.width();
        var delayTime = options.delayTime ? options.delayTime : TEXT_SLIDE_DELAY;
        var speed = options.speed ? options.speed : TEXT_SLIDE_SPEED;
        var isMoveBoxEnd = options.isMoveBoxEnd ? options.isMoveBoxEnd : false;

        _clearAnimation(titleElement);

        if (titleWidth > boxWidth) {
            var animatinState = TEXT_ANIMATION_STATE.START;
            titleElement.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {
                if (animatinState === TEXT_ANIMATION_STATE.START) {
                    if (isMoveBoxEnd) {
                        animatinState = TEXT_ANIMATION_STATE.MOVE_BOX_END;
                    }
                    else {
                        animatinState = TEXT_ANIMATION_STATE.MOVE_BOX_START;
                    }
                }
                else if (animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_START) {
                    animatinState = TEXT_ANIMATION_STATE.START;
                }
                else if (animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_END) {
                    animatinState = TEXT_ANIMATION_STATE.END;
                }
                else {
                    animatinState = TEXT_ANIMATION_STATE.START;
                }

                setTextAnimation({
                    targetBox: targetBox,
                    animatinState: animatinState,
                    speed: speed,
                    delay: delayTime,
                    isMoveBoxEnd: isMoveBoxEnd
                });
            });
            setTextAnimation({
                targetBox: targetBox,
                animatinState: animatinState,
                speed: speed,
                delay: delayTime,
                isMoveBoxEnd: isMoveBoxEnd
            });
        }
    }

    function setTextAnimation (options) {
        var cssOptions = null;
        var animationTime = "";
        var delayTime = "";

        var boxWidth = options.targetBox.width();
        var titleElement = options.targetBox.find("span");
        var titleWidth = titleElement.width();

        if (options.animatinState === TEXT_ANIMATION_STATE.START) {
            animationTime = (titleWidth / options.speed) + "s";
            if (options.delay) {
                delayTime = options.delay + "s";
            }

            cssOptions = {
                opacity: 1,
                "-webkit-transition": "-webkit-transform " + animationTime + " linear " + delayTime,
                "-webkit-transform": "translate3d(-100%, 0, 0)"
            };
        }
        else if (options.animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_START) {
            cssOptions = {
                opacity: 0.01,
                "-webkit-transition": "-webkit-transform 10ms",
                "-webkit-transform": "translate3d(0, 0, 0)"
            };
        }
        else if (options.animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_END) {
            cssOptions = {
                opacity: 0.01,
                "-webkit-transition": "-webkit-transform 10ms",
                "-webkit-transform": "translate3d(" + boxWidth + "px, 0, 0)"
            };
        }
        else {
            animationTime = Math.ceil(boxWidth / options.speed) + "s";

            cssOptions = {
                opacity: 1,
                "-webkit-transition": "-webkit-transform " + animationTime + " linear ",
                "-webkit-transform": "translate3d(0, 0, 0)"
            };
        }

        titleElement.css(cssOptions);
    }

    /**
     * div sliding animation 을 지원하는 함수
     * - element 구조는 상황마다 다르기 때문에 굉장히 제한적인 조건에서만 호출한다고 가정
     * - 애니메이션은 css 의 transition, transform 사용
     * - 사용 조건
     *   - 타겟의 엘레먼트 구조는 아래와 같으며, parameter 로는 targetBox 를 넘긴다
     *     : <div id="box"> <div id="targetBox"> ..... </div> </div>
     *   - box 는 사이즈가 정해져 있으며, overflow hidden 이어야 한다.
     *   - 기본적으로 애니메이션 동작이 event listener 기반이므로, 애니메이션 clear 는 사용자가 책임을 갖는다.
     * - 사용 예제
     *     : FullEpgView 의 drawFocusProgramme 참조
     *
     *
     * @param options targetBox - 타겟 div (jquery div)
     *                 targetWidth - targetBox 의 실제 width
     *                 boxWidth - targetBox 를 감싸는 박스의 width
     *                 speed - 애니메이션 속도 (default 125)
     *                 delayTime - 애니메이션 시작시, delay 시간
     *                 isMoveBoxEnd - true 일 경우 target 이 -100% 까지 갔을때 애니메이션을 box 끝부분에서 시작, false 일 경우 box 시작부분부터 시작
     */
    function _startBoxSlidingAnimation (options) {
        if (!options || !options.targetBox) {
            return;
        }

        var targetBox = options.targetBox;
        var targetWidth = options.targetWidth;
        var boxWidth = options.boxWidth;

        var delayTime = options.delayTime ? options.delayTime : TEXT_SLIDE_DELAY;
        var speed = options.speed ? options.speed : TEXT_SLIDE_SPEED;
        var isMoveBoxEnd = options.isMoveBoxEnd ? options.isMoveBoxEnd : false;

        _clearAnimation(targetBox);

        var animatinState = TEXT_ANIMATION_STATE.START;
        targetBox.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {
            if (animatinState === TEXT_ANIMATION_STATE.START) {
                if (isMoveBoxEnd) {
                    animatinState = TEXT_ANIMATION_STATE.MOVE_BOX_END;
                }
                else {
                    animatinState = TEXT_ANIMATION_STATE.MOVE_BOX_START;
                }
            }
            else if (animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_START) {
                animatinState = TEXT_ANIMATION_STATE.START;
            }
            else if (animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_END) {
                animatinState = TEXT_ANIMATION_STATE.END;
            }
            else {
                animatinState = TEXT_ANIMATION_STATE.START;
            }

            setBoxSlidingAnimation({
                targetBox: targetBox,
                animatinState: animatinState,
                targetWidth: targetWidth,
                boxWidth: boxWidth,
                speed: speed,
                delay: delayTime,
                isMoveBoxEnd: isMoveBoxEnd
            });
        });
        setBoxSlidingAnimation({
            targetBox: targetBox,
            animatinState: animatinState,
            targetWidth: targetWidth,
            boxWidth: boxWidth,
            speed: speed,
            delay: delayTime,
            isMoveBoxEnd: isMoveBoxEnd
        });
    }

    function setBoxSlidingAnimation (options) {
        var cssOptions = null;
        var animationTime = "";
        var delayTime = "";

        var targetBox = options.targetBox;
        var boxWidth = options.boxWidth;
        var targetWidth = options.targetWidth;

        if (options.animatinState === TEXT_ANIMATION_STATE.START) {
            animationTime = Math.ceil(targetWidth / options.speed) + "s";
            if (options.delay) {
                delayTime = options.delay + "s";
            }

            cssOptions = {
                opacity: 1,
                "-webkit-transition": "-webkit-transform " + animationTime + " linear " + delayTime,
                "-webkit-transform": "translate3d(-100%, 0, 0)"
            };
        }
        else if (options.animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_START) {
            cssOptions = {
                opacity: 1,
                "-webkit-transition": "-webkit-transform 10ms",
                "-webkit-transform": "translate3d(0, 0, 0)"
            };
        }
        else if (options.animatinState === TEXT_ANIMATION_STATE.MOVE_BOX_END) {
            cssOptions = {
                opacity: 0.01,
                "-webkit-transition": "-webkit-transform 10ms",
                "-webkit-transform": "translate3d(" + boxWidth + "px, 0, 0)"
            };
        }
        else {
            animationTime = Math.ceil(boxWidth / options.speed) + "s";

            cssOptions = {
                opacity: 1,
                "-webkit-transition": "-webkit-transform " + animationTime + " linear ",
                "-webkit-transform": "translate3d(0, 0, 0)"
            };
        }

        targetBox.css(cssOptions);
    }

    /**
     * startTextAnimation, startBoxSlidingAnimation 으로 시작한 애니메이션 clear
     *
     * @param target - 애니메이션중인 타겟 div, span 태그 (jquery div)
     *
     * - [dj.son] [WEBIIIHOME-3271] opacity 를 0.01 로 주고 10ms 동안 text 위치를 원래 위치로 변경도중 clear 될 경우 opacity 값이 0.01 로 남아있는 이슈 수정
     */
    function _clearAnimation (target) {
        target.off("webkitAnimationEnd");
        target.css({
            opacity: "",
            "-webkit-transition": "",
            "-webkit-transform": ""
        });
    }

    /**
     * 서버에 요청하여 받아오는 이미지의 paramter를 생성하여 전달한다.
     * width와 height 중 하나라도 지정되어 있지 않으면 type에 따른 default 값을 사용하여 전달한다.
     *
     * @param url
     * @param type
     * @param width
     * @param height
     * @returns {string}
     */
    function _getImageUrl(url, type, width, height) {
        if (width === 0 && height === 0) {
            if (type === KTW.CONSTANT.POSTER_TYPE.VERTICAL) {
                return url + "?w=" + KTW.CONSTANT.VERTICAL_POSTER_SIZE.WIDTH + "&h=" + KTW.CONSTANT.VERTICAL_POSTER_SIZE.HEIGHT + "&quality=90";
            }
            else if (type === KTW.CONSTANT.POSTER_TYPE.HORIZONTAL) {
                return url + "?w=" + KTW.CONSTANT.HORIZONTAL_POSTER_SIZE.WIDTH + "&h=" + KTW.CONSTANT.HORIZONTAL_POSTER_SIZE.HEIGHT + "&quality=90";
            }
        } else {
            return url + "?w=" + width + "&h=" + height + "&quality=90";
        }
    }


    /**
     * categoryView 에서 사용 할 focusIndex, pageIndex 를 계산
     *
     * @param curPages   ( Array )
     * @param curPageIdx
     * @param curFocusIdx
     * @param callback   (function 을 수행한 뒤, 실행 할 callback)
     */
    function _getNewCategoryIndex(curPages, curPageIdx, curFocusIdx, callback) {
        if (!curPages || !curPages.length || curPages.length < 0) {
            return ;
        }

        var pages = curPages;
        var focusIndex = curFocusIdx;
        var pageIndex = curPageIdx;
        var curPageLen = pages[pageIndex].length;

        // [si.mun] 아래 18.05월 UI와 관련된 주석 이전에 curPageLen 과 focusIndex 를 먼저 비교하지 않으면
        // 음성 검색이 아닌 정상적으로 해당 메뉴를 접근하였을 때 key navigation이 이상하게 동작한다.
        // (for loop 를 무조건 수행하면 2page -> 1page 로 이동 시 if(focusIndex > curPageLen)를 수행하면서
        // focusIndex, pageIndex 값들이 꼬이게 되므로 현재 pageIndex 와 focusIndex 를 비교함.
        if (curPageLen < focusIndex) {
            var pagesLen = pages.length;

            // [si.mun] 기존에는 pageIndex, focusIndex 가 아닌 focus 란 변수 하나로 category data에 접근하였음.
            // 하지만, 18.05월 UI 변경이 되면서 categoryView의 구조도 바꾸었다.
            // 때문에 음성 검색으로 내가 평가한 콘텐츠 메뉴로 진입 시에
            // home_foryou.js 에선 pages 배열의 index를 초과하는 newFocus를 전달하게 되면서 오류가 발생했다.
            // 때문에 위와 같은 상황을 방지하기 위해 아래 for loop를 수행
            pageIndex = 0;
            for (var idx = 0; idx < pagesLen ; ++idx) {
                curPageLen = pages[idx].length;
                if (focusIndex > curPageLen) {
                    focusIndex -= curPageLen;
                    pageIndex++;
                }
            }

            if (callback) {
                callback(focusIndex, pageIndex);
            }
        }
    }


    return {
        nbsp:_nbsp,
        getCurrentDateTime: _getCurrentDateTime,
        getTimeString: _getTimeString,
        numToStr: _numToStr,
        homeMenuArrayCopy: _homeMenuArrayCopy,
        makeElement: _makeElement,
        arrayCopy: _arrayCopy,
        objectCopy:_objectCopy,
        initObject:_initObject,
        getTextLength: _getTextLength,
        elipsis: _elipsis,
        getVCategoryList: _getVCategoryList,
        //findMenuData:_findMenuData,
        splitText: _splitText,
        //getIconImg: _getIconImg,
        isLimitAge:_isLimitAge,
        checkPosterResolutionIcon : _checkPosterResolutionIcon,
        transPrInfo : _transPrInfo,
        transPageNumber: _transPageNumber,
		getPlayingProg: _getPlayingProg,
		isPlayingProg: _isPlayingProg,
        renameDupId: _renameDupId,
        getLayerCount:_getLayerCount,
		transformX:_transformX,
		transformY:_transformY,
		transformXY:_transformXY,
		scaleX:_scaleX,
		scaleY:_scaleY,
		getFormatDate:_getFormatDate,
		setComma:_setComma,
		formatComma:_formatComma,
        isFloat: _isFloat,
		//getFocusIconWithWidth: _getFocusIconWithWidth,
        showCasErrorPopup: _showCasErrorPopup,
		showErrorPopup:_showErrorPopup,
        showCommonErrorPopup: _showCommonErrorPopup,
		transDateStr: _transDateStr,
		transDay: _transDay,
		tranAstrix: _tranAstrix,
		getCpUseYnType: _getCpUseYnType,
		getExpireDate: _getExpireDate,
		encrypt:_encrypt,
		decrypt:_decrypt,
		stringToArray:_stringToArray,
        stringToMultiLine:_stringToMultiLine,
		keyCodeToNumber: _keyCodeToNumber,
		getStringToMilliseconds: _getStringToMilliseconds,
		startsWith: _startsWith,
		isValidVariable: _isValidVariable,
		isNumber:_isNumber,
		readUTF16String: _readUTF16String,
		getExtendedEventDescriptor: _getExtendedEventDescriptor,
		isFavID: _isFavID,
		isSkyChoiceChannel: _isSkyChoiceChannel,
		getPkgCode: _getPkgCode,
		makeImageUrl: _makeImageUrl,
		getPointString:_getPointString,
		//getProperty:_getProperty,
		convertToInt:_convertToInt,
        changeMouseControlStatus: _changeMouseControlStatus,

        getAppPath: _getAppPath,

        getIndex: _getIndex,
        indexMinus: _indexMinus,
        indexPlus: _indexPlus,

        startTextAnimation: _startTextAnimation,
        startBoxSlidingAnimation: _startBoxSlidingAnimation,
        clearAnimation: _clearAnimation,

        getImageUrl: _getImageUrl,

        getNewCategoryIndex: _getNewCategoryIndex,
    };

}());

/**
 * 문자열의 a를 b로 바꾼다. 
 * Ex) str = "a테스트bcd테스트efg".replaceAll("테스트", ""); => str = "abcdefg";
 * @returns String
 */
String.prototype.replaceAll = function(a, b) {
	var s = this;
	var n1, n2, s1, s2;

	while (true) {
		if ( s=="" || a=="" ) 
			break;
		n1 = s.indexOf(a);
		if ( n1 < 0 ) 
			break;
		n2 = n1 + a.length;
		
		if ( n1==0 ) {
			s1 = b;
		} else {
			s1 = s.substring(0, n1) + b;
		}
		if ( n2 >= s.length ) {
			s2 = "";
		} else {
			s2 = s.substring(n2, s.length);
		}
		s = s1 + s2;
	}
	return s;
};


