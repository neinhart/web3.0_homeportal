"use strict";

KTW.utils.Retryer = function(func, condition_func, interval, retry_count) {
	var run_function = func;
	var condition_func = condition_func;
	var interval = interval ? interval : 1000;
	
	var timer;
	
	this.run = run;
	function run() {
		if(timer) clearTimeout(timer);
		
		if(condition_func() === true) {
			run_function();
		} else{
			timer = setTimeout(run,interval);
		}
	}
	
	this.stop = function() {
		if(timer) clearTimeout(timer);
	}
}