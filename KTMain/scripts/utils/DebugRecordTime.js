KTW.utils.util.DebugRecordTimer = function() {
	var log = KTW.utils.Log;
	
	var start_time = -1;
	
	this.start = function() {
		log.printErr("[TEST] start ============");
		start_time = Date.now();
	}
	
	this.check = function(msg) {
		log.printErr("[TEST] "+ msg +" LAP:" + Math.ceil(Date.now() - start_time));
	}
}