"use strict";

KTW.utils.TimedKeyBlock = function KeyBlocker(_key_code, _time) {
    var filtering_key_code = _key_code;
    var block_time = _time ? _time : 500;
    var pre_key_time = 0;

    this.IsBlocked = function(key_code) {
        if(filtering_key_code == key_code) {
            var current_time = (new Date()).getTime();

            if(current_time - pre_key_time < block_time) {
                return true; //block.
            } else {
                pre_key_time = current_time;
            }
        }

        return false;
    }
};