"use strict";

KTW.utils.HashMap = function() {
    this.type = "hash";
    this.length = 0;
    this.key = [];
    this.obj = [];
}

KTW.utils.HashMap.prototype.put = function( _key, _obj ) {
    try {
    		var ret = this.get(_key);
		if (ret) {
			for (var i = 0 ; i < this.length; i++ ) {
	        		if ( _key == this.key[i])
	        			this.obj[i] = _obj;
			}
			return;
		}
        this.key.push( _key);
        this.obj.push( _obj );
        this.length++;   
    } catch( e ) {
        printlog( e );
    }
}

KTW.utils.HashMap.prototype.elementAt = function( i ) {
    var ret = null;
    try {
        i = parseInt( i, 10 );
        if ( i >= 0 && i < this.length ) {
            ret = this.obj[i];
        }
    } catch( e ) {
    		printlog( e );
    }
    return ret;
}

KTW.utils.HashMap.prototype.get = function( _key ) {
    var ret = null;
    try {
        for (var i = 0 ; i < this.length; i++ ) {
        		if (_key == this.key[i])
        			ret = this.obj[i];
        }
    } catch( e ) {
    		printlog( e );
    }
    return ret;
}

KTW.utils.HashMap.prototype.insertAt = function( i , _key, _obj) {
    try {
        i = parseInt( i, 10 );
        if( i == this.length ) {
            this.put( _key , _obj);
            return;
        }
        this.key.splice( i, 0, _key );
        this.obj.splice( i, 0, _obj );
        this.length++;
    } catch( e ) {
        printlog( e );
    }
}

KTW.utils.HashMap.prototype.remove = function( _key ) {
    var ret = null;
    try {
        for (var i = 0 ; i < this.length; i++ ) {
        		if ( _key == this.key[i]) {
	            ret = this.obj[i];
	            this.key.splice( i, 1 );
	            this.obj.splice( i, 1 );
	            this.length--;
        		}
        }
    } catch( e ) {
        printlog( e );
    }
    return ret;
}

KTW.utils.HashMap.prototype.clear = function() {
	try {
        this.key = [];
        this.obj = [];
        this.length = 0;
    } catch( e ) {
        printlog( e );
    }
}

KTW.utils.HashMap.prototype.size = function() {
    return this.length;
}


KTW.utils.HashMap.prototype.set = function( i, _key, _obj ) {
    var ret = null;
    try {
        i = parseInt( i, 10 );
        if ( i >= 0 && i < this.length ) {
            this.key[i] = _key;
            this.obj[i] = _obj;
        }
    } catch( e ) {
        printlog( e );
    }
    return ret;
}