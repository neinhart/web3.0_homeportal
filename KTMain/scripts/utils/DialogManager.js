"use strict";
KTW.utils.DialogManager = function(){
	this.dialog_id = [];
}

KTW.utils.DialogManager.prototype.register = function(id, layer, bg_hide, /*type, */callback){
	if(!this.check(id)){
		KTW.utils.Log.printDbg("[DialogManager] register ID:" + id);
		this.dialog_id.push({id:id, /*type:type,*/ callback:callback, parent_layer:layer, bg_hide:bg_hide});
		
		if(bg_hide == true && layer && layer.parent.div){
			layer.parent.div.css("visibility","hidden");
		}
	}
};

KTW.utils.DialogManager.prototype.unregister = function(id, type){
	for (var i = 0; i < this.dialog_id.length; i++) {
		if (this.dialog_id[i].id === id || ((id === undefined || id === null) && this.dialog_id[i].type === type)) {
			KTW.utils.Log.printDbg("[DialogManager] unregister ID:" + id);
			
			if(this.dialog_id[i].bg_hide == true && this.dialog_id[i].parent_layer && this.dialog_id[i].parent_layer.parent.div){
				this.dialog_id[i].parent_layer.parent.div.css("visibility","inherit");
				this.dialog_id[i].parent_layer = null;
			}
			
			return this.dialog_id.splice(i, 1);
		}
	}
	return null;
};

KTW.utils.DialogManager.prototype.check = function(id){
	for ( var key in this.dialog_id) {
		if (this.dialog_id[key].id == id) {
			return true;
		}
	}
	return false;
};

/**
 * id 와 type 은 배타적 id 가 있으면 type 은 무시
 * id 가 undefined 이면 type 만 체크.
 */
KTW.utils.DialogManager.prototype.close = function(id, type, param){
	if(this && this.dialog_id && this.dialog_id.length) {
		for (var i = (this.dialog_id.length - 1); i >= 0; i--) {
			/**
			 * id 가 없으면 close all.
			 */
			if ((id == undefined && type === undefined) || ((id === undefined || id === null) && this.dialog_id[i].type === type) || (this.dialog_id[i].id === id)) {
				if(this && this.dialog_id && this.dialog_id[i]) {
					var dialog_id = this.dialog_id[i].id;

					KTW.utils.Log.printDbg("[DialogManager] close dialog ID:" + dialog_id);

					if(this.dialog_id[i].callback){
						this.dialog_id[i].callback(param);
						this.dialog_id[i].callback = null;
					}

					//QAT deactivate 되면서 해당 리스트가 변경된다.
					//해서 list 순서를 거꾸로 하도록 변경하고 null check 를 강화 한다.
					KTW.ui.LayerManager.deactivateLayer(dialog_id, true);
					this.unregister(dialog_id);
				}
			}
		}
	}
};

Object.defineProperty(KTW.utils.DialogManager, "TYPE", {
    value: {
        NORMAL: 1,
        BUY:    2,
        ADULT:  3,
    },
    writable: false,
    configurable: false
});
