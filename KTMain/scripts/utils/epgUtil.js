"use strict";

/**
 * <code>EPG Util</code>
 * EPG 와 관련된 util method 모음.
 * @author ggobugi
 * @since 2015. 03. 13.
 */
KTW.utils.epgUtil = (function () {
	var log = KTW.utils.Log;
	var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
	var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
	var layerManager = KTW.ui.LayerManager;
	var storageManager = KTW.managers.StorageManager;
	var util = KTW.utils.util;
	
	/**
	 * 현재 재생중인 프로그램의 자막, 오디오 관련 정보를 관리.
	 * 채널, 프로그램 변경 시 init()을 호출 한 이후에 사용한다.
	 */
	var _progInfo = (function(){
		var component = {
				video: [],
				audio: [],
				subtitle: []
			};
		
		var is_multi = false;
		var has_cc = false;
		var is_desc = false;
		var desc_comp = [];
		
		function _init() {
			log.printDbg('[progInfo] _init()');
			is_multi = false;
			has_cc = false;
			is_desc = false;
			desc_comp = [];
		}
		
		function _updateInfo(channel_control) {
			log.printDbg('[progInfo] _updateInfo()');
			//var channelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            var channelControl = navAdapter.getChannelControl(channel_control, false);
			
//			try {
//				component.video = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_VIDEO)
//				log.printDbg('[progInfo] component.video: '+JSON.stringify(component.video));
//			}
//			catch (e) {
//				log.printErr(e);
//			}
			try {
				component.audio = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO);
				log.printDbg('[progInfo] component.audio: '+JSON.stringify(component.audio));
			}
			catch (e) {
				log.printErr(e);
			}
			try {
				component.subtitle = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_SUBTITLE);
				log.printDbg('[progInfo] component.subtitle: '+JSON.stringify(component.subtitle));
			}
			catch (e) {
				log.printErr(e);
			}
			
			_init();
			if (component.subtitle != null && component.subtitle.length > 0) {
				has_cc = true;
			}
			if (component.audio != null && component.audio.length > 0) {
				var lang = null;
                var audioLength = component.audio.length;
				for (var i=0; i < audioLength; i++) {
					if (component.audio[i].audioDescription === true) {
						if (is_desc === false) {
							is_desc = true;
						}
						desc_comp[desc_comp.length] = component.audio[i];
					}
					else if (is_multi === false) {
						if (lang === null) {
							lang = component.audio[i].language;
						}
						else if (lang != component.audio[i].language) {
							is_multi = true;
						}
					}
				}
			}
			
			if (is_desc === true) {
				if (Number(basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VISUAL_IMPAIRED)) === 1) {
					_selectDescriptionAudio();
				}
			}
			log.printDbg('[progInfo] is_multi = ' + is_multi);
			log.printDbg('[progInfo] has_cc = ' + has_cc);
			log.printDbg('[progInfo] is_desc = ' + is_desc);
		}
		
		function _getVideoComponent() {
			return component.video;
		}
		function _getAudioComponent() {
			return component.audio;
		}
		function _getSubtitleComponent() {
			return component.subtitle;
		}
		function _isMultiAudio() {
			return is_multi;
		}
		function _isAudioDescription() {
			return is_desc;
		}
		function _hasSubtitle() {
			return has_cc;
		}
		function _selectDescriptionAudio(language) {
			var channelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
			
			if (language === false) {
				channelControl.selectComponent(component.audio[0]);
				return;
			}
			
			// lang - 0: 한국어, 1: 영어, 2:기타
            // 2017.08.31 dhlee
            // 원래 코드의 경우 지역 변수 lang을 선언하면 항상 null 이므로 configuration 값을 항상 이용하게 된다.
            // 현재 _selectDescriptionAudio() 함수를 사용하는 곳이 _updateInfo() 밖에 없으므로 문제가 없지만
            // 원래 의도에 따라 param으로 전달된 값을 기준으로 비교하도록 수정한다.
			//var lang = lang == null ? basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.AUDIO_LANGUAGE) : language;
            var lang = (language === null ? basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.AUDIO_LANGUAGE) : language);
			log.printDbg('[progInfo] _selectDescriptionAudio ('+lang+')');
			for (var i=0; i<desc_comp.length; i++) {
				if (desc_comp[i].language === lang) {
					channelControl.selectComponent(desc_comp[i]);
					log.printDbg('[progInfo] select ' + desc_comp[i].language);
					return;
				}
			}
			log.printDbg('selectDescriptionAudio() - There is no audio description component supporting selected language!');
			// 화면해설이 아닌 주음성 중에서 동일한 언어를 선택
			for (var i=0; i<component.audio.length; i++) {
				if (component.audio[i].language === lang) {
					channelControl.selectComponent(component.audio[i]);
					log.printDbg('[progInfo] select ' + component.audio[i].language);
					return;
				}
			}
			log.printErr('selectDescriptionAudio() - There is no audio component supporting selected language!');
		}
		
		function _getAudioStr(lang) {
			var result = null;
			if (lang === KTW.utils.epgUtil.DATA.AUDIO.LANG_CODE.KOR) {
				result = KTW.utils.epgUtil.DATA.AUDIO.LANG_STR.KOR;
			}
			else if (lang === KTW.utils.epgUtil.DATA.AUDIO.LANG_CODE.ENG) {
				result = KTW.utils.epgUtil.DATA.AUDIO.LANG_STR.ENG;
			}
			else {
				result = KTW.utils.epgUtil.DATA.AUDIO.LANG_STR.ETC;
			}
			return result;
		}

		return {
			init: _init,
			updateInfo: _updateInfo,
			getVideoComponent: _getVideoComponent,
			getAudioComponent: _getAudioComponent,
			getSubtitleComponent: _getSubtitleComponent,
			isMultiAudio: _isMultiAudio,
			isAudioDescription: _isAudioDescription,
			hasSubtitle: _hasSubtitle,
			selectDescriptionAudio: _selectDescriptionAudio,
			getAudioStr: _getAudioStr
		};
		
	}());

    /**
     * 채널목록을 재구성한다.
     * channel object에 "desc" property를 추가하여 getSIDescriptor 값을 할당한다.
     * ch_list에서 숨김채널을 제거한다.
     * 단, cur_ch이 있으면 해당 채널은 제거하지 않는다.
     *
     * ### desc ###
     * - 0 : fullEPG 프로그램정보 표시, miniEPG 프로그램정보 표시(일반채널)
     * - 1 : fullEPG 채널 숨김, miniEPG에서 채널명으로 표시
     * - 2 : fullEPG 채널명으로 표시, miniEPG 프로그램정보 표시
     * - 3 : fullEPG 채널명으로 표시, miniEPG 채널명으로 표시
     */
    /*
    function _reconstructionChList(ch_list, from_miniepg) {
        var CH_TYPE = KTW.utils.epgUtil.DATA.CHANNEL_TYPE;
        var ch_array = [];
        var ch_list_length = 0;
        if (util.isValidVariable(ch_list) === true) {
            ch_list_length = ch_list.length;
        }
        var chIndex = 0;

        for(var i=0; i < ch_list_length; i++){
            var ch = ch_list[i];
            // IPTV 채널 처리
            if (ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS) {
                // getSIDescriptor 는 Channel 객체의 prototype 으로 선언되어 있다.
                // 0x91 은 extItem 에서  HideService 를 확인하는 tag.
                var chDesc0x91 = ch.getSIDescriptor(0x91);
                if(chDesc0x91 && chDesc0x91.length > 0){
                    if(chDesc0x91[0] == 1){
                        log.printDbg("remove channel ==== " + ch);
                        if (from_miniepg === true) {
                            ch_array[chIndex] = ch;
                            ch_array[chIndex].desc = 1;
                        }
                    }
                    else{
                        // HideService 채널은 PromoChannel 을 포함하여 일반적으로 "03" 값을 return.
                        // 성인채널의 경우 "02" 값을 return.
                        ch_array[chIndex] = ch;
                        ch_array[chIndex].desc = chDesc0x91[0];
                    }
                }else{
                    ch_array[chIndex] = ch;
                    ch_array[chIndex].desc = 0;
                }
                chIndex++;
            }
            // Sky 채널 처리
            else {
                var chDesc0xCC = ch.getSIDescriptor(0xCC);
                if(chDesc0xCC && chDesc0xCC.length > 0){
                    var desc = 0;
                    var isHiddenChannel = false;
                    for(var j=0; j < chDesc0xCC.length; j++){
                        desc = chDesc0xCC[j] & 0xFF;
                        if (desc === CH_TYPE.GRID_HIDDEN_PROGRAM_SKYLIFE_WITH_SETTING) {
                            log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' GRID_HIDDEN_PROGRAM_SKYLIFE_WITH_SETTING');
                            // 성인메뉴숨김 ON
                            // 전체편성표: 채널, 미니가이드: 프로그램
                            // 성인메뉴숨김 OFF
                            // 전체편성표: 프로그램, 미니가이드: 프로그램

                            // objectCopy를 하는 이유는 미니가이드와 편성표에서 채널링을 각각 가지고 있는데
                            // ch 객체를 link만 걸어서 그대로 넣어버리면 desc 값을 어느 한 쪽에서 변경하면 다른 쪽에서도 변경된 값을 참조하게 된다
                            // 따라서 desc 설정값을 다르게 가져가야 하는 채널에 대해서는 objectCopy를 하여 각각의 desc 값을 참조하도록 한다
                            // desc 값이 다른 애들만 objectCopy를 하는 이유는 모든 채널을 다 objectCopy 하기에는 시간이 너무 오래 걸려서
                            //ch_array[ch_array.length] = util.objectCopy(ch);
                            //ch_array[ch_array.length] = ch;
                            ch_array[chIndex] = ch;
							// TODO 2016.09.05 dhlee
							// 2.0 R6 형상에서는 AdultMenuLockManager를 이용하고 있는데 추후 확인 필요함.
                            if (storageManager.ps.load(storageManager.KEY.MENU_LOCK_ADULT) === 'true') {
                                ch_array[chIndex].desc = 2;
                            }
                            else {
                                ch_array[chIndex].desc = 0;
                            }
                            chIndex++;
                            isHiddenChannel = true;
                            break;
                        }
                        else if (desc === CH_TYPE.GRID_HIDDEN_CHANNEL_SKYLIFE) {
                            log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' GRID_HIDDEN_CHANNEL_SKYLIFE from_miniepg='+from_miniepg);
                            // 전체편성표: 채널삭제, 미니가이드: 채널
                            log.printDbg("remove channel ==== " + ch.majorChannel);
                            if (from_miniepg === true) {
                                ch_array[chIndex] = ch;
                                ch_array[chIndex].desc = 1;
                                chIndex++;
                            }
                            isHiddenChannel = true;
                            break;
                        }
                        else if (desc === CH_TYPE.GRID_HIDDEN_PROGRAM_SKYLIFE) {
                            log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' GRID_HIDDEN_PROGRAM_SKYLIFE from_miniepg='+from_miniepg);
                            // 전체편성표: 채널, 미니가이드: 프로그램
//    							ch_array[ch_array.length] = util.objectCopy(ch);
//    							if (from_miniepg === true) {
//    								ch_array[ch_array.length-1].desc = 0;
//    							}
//    							else {
//    								ch_array[ch_array.length-1].desc = 3;
//    							}
                            ch_array[chIndex] = ch;
                            ch_array[chIndex].desc = 2;
                            chIndex++;
                            isHiddenChannel = true;
                            break;
                        }
                        else if (desc === CH_TYPE.HIDDEN_PROGRAM_SKYLIFE) {
                            log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' HIDDEN_PROGRAM_SKYLIFE');
                            // 전체편성표: 채널, 미니가이드: 채널
                            ch_array[chIndex] = ch;
                            ch_array[chIndex].desc = 3;
                            chIndex++;
                            isHiddenChannel = true;
                            break;
                        }
                    }
                    // 위에 해당하는 채널이 아닌 경우에는 일반채널로 처리
                    if (isHiddenChannel === false) {
                        ch_array[chIndex] = ch;
                        ch_array[chIndex].desc = 0;
                        chIndex++;

                        isHiddenChannel = false;
                    }
                }
                else {
                    ch_array[chIndex] = ch;
                    ch_array[chIndex].desc = 0;
                    chIndex++;
                }
            }
        }
//    	ch_list = ch_array;
//    	ch_array = null;
        return ch_array;
    }
    */
    function _reconstructionCh(ch) {
    	var chlist = [];
    	chlist[chlist.length] = ch;
    	return _reconstructionChList(chlist);
    }

    function _reconstructionChList(ch_list, from_miniepg) {
    	var CH_TYPE = KTW.utils.epgUtil.DATA.CHANNEL_TYPE;
    	var ch_array = [];
    	var ch_list_length = 0;
		if (util.isValidVariable(ch_list) === true) {
			ch_list_length = ch_list.length;
		}

        // 2016.10.19 dhlee
        // 성인 메뉴 display 여부는 oipf configuration 을 통해 관리된다.
        // 2017.08.31 dhlee
        // MenuDataManager 에서 관리하는 값을 이용한다.
        //var isAdultMenuDisplay = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY) === "true";
        var isAdultMenuDisplay = KTW.managers.data.MenuDataManager.isAdultMenuDisplay();
        log.printDbg("_reconstructionChList(), isAdultMenuDisplay = " + isAdultMenuDisplay);
    	for(var i=0; i < ch_list_length; i++){
    		var ch = ch_list[i];
			// IPTV 채널 처리
			if (ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS) {
				// getSIDescriptor 는 Channel 객체의 prototype 으로 선언되어 있다.
        		// 0x91 은 extItem 에서  HideService 를 확인하는 tag. 
        		var chDesc0x91 = ch.getSIDescriptor(0x91);
        		if(chDesc0x91 && chDesc0x91.length > 0){
            		if(chDesc0x91[0] == 1){
            			log.printDbg("remove channel ==== " + ch);
            			if (from_miniepg === true) {
            				ch_array[ch_array.length] = ch;
                    		ch_array[ch_array.length-1].desc = 1;
            			}
            		}
            		else{
            			// HideService 채널은 PromoChannel 을 포함하여 일반적으로 "03" 값을 return.
            			// 성인채널의 경우 "02" 값을 return.
            			ch_array[ch_array.length] = ch;
            			ch_array[ch_array.length-1].desc = chDesc0x91[0];
            		}
            	}else{
            		ch_array[ch_array.length] = ch;
            		ch_array[ch_array.length-1].desc = 0;
            	}
			}
			// Sky 채널 처리
			else {
				var chDesc0xCC = ch.getSIDescriptor(0xCC);
				if(chDesc0xCC && chDesc0xCC.length > 0){
					var desc = 0;
					var isHiddenChannel = false;
					for(var j=0; j < chDesc0xCC.length; j++){
						desc = chDesc0xCC[j] & 0xFF;
						if (desc === CH_TYPE.GRID_HIDDEN_PROGRAM_SKYLIFE_WITH_SETTING) {
							log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' GRID_HIDDEN_PROGRAM_SKYLIFE_WITH_SETTING');
							// 성인메뉴숨김 ON
							// 전체편성표: 채널, 미니가이드: 프로그램
							// 성인메뉴숨김 OFF
							// 전체편성표: 프로그램, 미니가이드: 프로그램
							
							// objectCopy를 하는 이유는 미니가이드와 편성표에서 채널링을 각각 가지고 있는데
							// ch 객체를 link만 걸어서 그대로 넣어버리면 desc 값을 어느 한 쪽에서 변경하면 다른 쪽에서도 변경된 값을 참조하게 된다
							// 따라서 desc 설정값을 다르게 가져가야 하는 채널에 대해서는 objectCopy를 하여 각각의 desc 값을 참조하도록 한다
							// desc 값이 다른 애들만 objectCopy를 하는 이유는 모든 채널을 다 objectCopy 하기에는 시간이 너무 오래 걸려서
//    							ch_array[ch_array.length] = util.objectCopy(ch);
							ch_array[ch_array.length] = ch;
                            // 2016.10.19 dhlee
                            // AdultMenuLockManager는 더 이상 사용하지 않게 되었으므로
                            // Local Storage 값을 사용하지 않고 OIPF의 Configuration object를 통해 정보를 획득한다.
                            // 그 이유는 공용 제어를 통해 해당 설정 값이 다른 app 에서도 사용해야 하기 때문이다.
                            // 즉, 2.0 R5 버전까지는 홈포털에서만 사용했으나 R6 부터 공용제어를 통해 다른 app 에서도 사용 가능하도록
                            // 하기 위해 Local Storage -> oipf configuration 으로 저장 위치가 변경되었다.
                            // AdultMenuLockManager는 이런 요구사항을 만족시키기 위한 로직이었으며 3.0 에서는 사용하지 않도록 한다.
                            // 그리고 반복문 내에서 access 하지 않고 반복문 진입 전에 해당 key 값을 확인하도록 한다.
                            //if (storageManager.ps.load(storageManager.KEY.MENU_LOCK_ADULT) === 'true') {
                            if (isAdultMenuDisplay === false) {
								ch_array[ch_array.length-1].desc = 2;
							}
							else {
								ch_array[ch_array.length-1].desc = 0;
							}
							isHiddenChannel = true;
							break;
						}
						else if (desc === CH_TYPE.GRID_HIDDEN_CHANNEL_SKYLIFE) {
							log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' GRID_HIDDEN_CHANNEL_SKYLIFE from_miniepg='+from_miniepg);
							// 전체편성표: 채널삭제, 미니가이드: 채널
							log.printDbg("remove channel ==== " + ch.majorChannel);
							if (from_miniepg === true) {
								ch_array[ch_array.length] = ch;
                        		ch_array[ch_array.length-1].desc = 1;
							}
							isHiddenChannel = true;
							break;
						}
						else if (desc === CH_TYPE.GRID_HIDDEN_PROGRAM_SKYLIFE) {
							log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' GRID_HIDDEN_PROGRAM_SKYLIFE from_miniepg='+from_miniepg);
							// 전체편성표: 채널, 미니가이드: 프로그램
//    							ch_array[ch_array.length] = util.objectCopy(ch);
//    							if (from_miniepg === true) {
//    								ch_array[ch_array.length-1].desc = 0;
//    							}
//    							else {
//    								ch_array[ch_array.length-1].desc = 3;
//    							}
							ch_array[ch_array.length] = ch;
							ch_array[ch_array.length-1].desc = 2;
							isHiddenChannel = true;
							break;
						}
						else if (desc === CH_TYPE.HIDDEN_PROGRAM_SKYLIFE) {
							log.printDbg('_reconstructionChList ['+ch.majorChannel+'] '+ch.name+' HIDDEN_PROGRAM_SKYLIFE');
							// 전체편성표: 채널, 미니가이드: 채널
							ch_array[ch_array.length] = ch;
                    		ch_array[ch_array.length-1].desc = 3;
                    		isHiddenChannel = true;
							break;
						}
					}
					// 위에 해당하는 채널이 아닌 경우에는 일반채널로 처리
					if (isHiddenChannel === false) {
						ch_array[ch_array.length] = ch;
    					ch_array[ch_array.length-1].desc = 0;
    					isHiddenChannel = false;
					}
				}
				else {
					ch_array[ch_array.length] = ch;
					ch_array[ch_array.length-1].desc = 0;
				}
			}
    	}
//    	ch_list = ch_array;
//    	ch_array = null;
    	return ch_array;
    }
    
    /**
     * 프로그램 정보를 보정한다.
     * 비어있는 프로그램, 겹치는 프로그램, 프로그램 정보를 숨기는 채널의 프로그램 등을 처리한다.
     */
    function _revisePrograms(ch, prog_list, from_miniepg) {
    	var IS_TIME_UNIT_SEC = KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC;
    	var TIME_UNIT = IS_TIME_UNIT_SEC === true ? 1 : 1000;
    	var prog_array = [];
    	var NULL_NAME = KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL;
        var UPDATE_NAME = KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE;  // 2017.03.31 dhlee 2.0 형상 반영
    	var now = new Date().getTime();
    	if (IS_TIME_UNIT_SEC === true) {
    		now = Math.floor(now/1000);
    	}
    	
    	// 프로그램 정보 숨기고 채널명으로 표시하는 경우
    	if (ch.desc == 1 || (ch.desc == 2 && from_miniepg !== true) || ch.desc == 3 ) {
    			// OTS audio portal channel도 편성표에 채널 이름으로 표시
//    			KTW.CONSTANT.IS_OTS === true && navAdapter.isAudioChannel(ch) === false && navAdapter.isAudioChannel(ch, true) === true) {
    		var prog = {};
            prog.name = ch.name;
            prog.startTime = now - 24*60*60*TIME_UNIT;
            prog.duration = 4*24*60*60*TIME_UNIT;
            prog.channel = ch;
            prog.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME;
            prog_array[prog_array.length] = prog;
    	}
    	// 프로그램 정보를 표시하는 일반적인 경우
    	else {
    		// 프로그램 정보가 없는 경우
    		if (prog_list == null || prog_list.length === 0) {
    			var prog = {};
                prog.name = UPDATE_NAME;    // 2017.03.31 dhlee 2.0 형상 반영
                prog.startTime = now - 24*60*60*TIME_UNIT;
                prog.duration = 4*24*60*60*TIME_UNIT;
                prog.channel = ch;
                prog.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE;
                prog_array[prog_array.length] = prog;
    		}
    		// 프로그램 정보가 편성되어 있는 경우
    		else {
    			// 처음에 비어있는 시간 보정
    			if (now < prog_list[0].startTime) {
    				var prog = {};
                    prog.name = NULL_NAME;
                    prog.startTime = now - 60*60*TIME_UNIT;
                    prog.duration = prog_list[0].startTime - prog.startTime;
                    prog.channel = ch;
                    prog.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL;
                    prog_array[prog_array.length] = prog;
    			}
                else {
                    prog_list[0].reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NORMAL;
                }

    			prog_array[prog_array.length] = prog_list[0];
        		for (var i=0; i<prog_list.length; i++) {
        			var e_time = prog_list[i].startTime + prog_list[i].duration;
        			// 다음 프로그램이 비어있으면 껍데기 프로그램 추가
        			// 보통 마지막 프로그램인 경우 뒤에 껍데기 프로그램 추가하기 위해 사용
	    			if (prog_list[i+1] == null) {
	    				var prog = {};
	                    prog.name = NULL_NAME;
	                    prog.startTime = e_time;
	                    prog.duration = 4*24*60*60*TIME_UNIT;
	                    prog.channel = ch;
                        prog.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL;
	                    prog_array[prog_array.length] = prog;
	    			}
	    			// 다음 프로그램 시작 전까지 시간이 비어있는 경우 껍데기 프로그램 추가
	    			else if (e_time < prog_list[i+1].startTime) {
	    				var prog = {};
	                    prog.name = NULL_NAME;
	                    prog.startTime = e_time;
	                    prog.duration = prog_list[i+1].startTime - prog.startTime;
	                    prog.channel = ch;
                        prog.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL;
	                    prog_array[prog_array.length] = prog;
	                    prog_array[prog_array.length] = prog_list[i+1];
	    			}
	    			// 현재 프로그램과 다음 프로그램의 시간이 겹치는 경우 보정
	    			// XXX KTW에서는 프로그램 객체의 startTime 과 같은 property는 read-only property라서 값 할당이 안됨
	    			// 따라서 프로그램 객체를 복사하여 사용한다
	    			else if (e_time > prog_list[i+1].startTime) {
	    				// 다다음 프로그램이 없으면 현재 프로그램이 끝나는 시점을 다음 프로그램의 시작 시점으로 변경
	    				if (prog_list[i+2] == null) {
	    					var obj = _programCopy(prog_list[i+1]);
	    					obj.startTime = e_time;
                            obj.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NORMAL;
    						prog_array[prog_array.length] = obj;
    					}
	    				// 다다음 프로그램이 존재하는 경우
	    				else {
	    					var j = 0;
	    					// 이후 프로그램의 시작시점이 현재 프로그램의 종료시점보다 늦거나 동일할 때까지 돌린다
		    				for (j=i+2; j<prog_list.length; j++) {
		    					// 다다음 프로그램의 시작시점이 현재 프로그램의 종료 시점과 동일하면
		    					// 다음 프로그램은 목록에서 제외하고 stop
	    						if (prog_list[j].startTime === e_time) {
                                    prog_list[j].reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NORMAL;
	    							prog_array[prog_array.length] = prog_list[j];
	    							i = j - 1;
	    							break;
	    						}
	    						// 다다음 프로그램의 시작시간이 현재 프로그램의 종료시점보다 늦는 경우
	    						// 다음 프로그램의 시작시점을 현재 프로그램이 끝나는 시점으로 변경하고 stop
	    						else if (prog_list[j].startTime > e_time) {
	    							var obj = _programCopy(prog_list[j-1]);
	    							obj.startTime = e_time;
                                    obj.reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NORMAL;
	    							prog_array[prog_array.length] = obj;
	    							i = j - 2;
	    							break;
	    						}
		    				}
		    				// 끝까지 다 돌았으면 다시 index 값 원복
		    				if (j === prog_list.length) {
		    					i = j - 2;
		    				}
	    				}
	    			}
	    			// 그 외의 일반적인 경우 처리
	    			else {
                        prog_list[i+1].reviseType = KTW.utils.epgUtil.DATA.PROGRAM_REVISE_TYPE.NORMAL;
	    				prog_array[prog_array.length] = prog_list[i+1];
	    			}
	    		}
    		}
    	}

    	return prog_array;
    }
    
    /**
     * 프로그램 객체를 복사한다
     * 일반적으로 프로그램 객체는 read-only property라서
     * 프로그램 값 변경이 필요한 경우에 사용한다
     */
    function _programCopy(prog) {
    	var obj = {};
    	if (prog.name) obj.name = prog.name;
    	if (prog.startTime) obj.startTime = prog.startTime;
    	if (prog.duration) obj.duration = prog.duration;
    	if (prog.Fa) obj.Fa = prog.Fa;
    	if (prog.getSIDescriptors) obj.getSIDescriptors = prog.getSIDescriptors;
    	if (prog.channel) obj.channel = prog.channel;
    	if (prog.channelID) obj.channelID = prog.channelID;
    	if (prog.programmeID) obj.programmeID = prog.programmeID;
    	if (prog.programmeIDType) obj.programmeIDType = prog.programmeIDType;
    	if (prog.isHD) obj.isHD = prog.isHD;
    	// DVB channel
    	if (prog.description) obj.description = prog.description;
    	
    	return obj;
    }
    
    /**
     * 채널의 장르 추출 (OTV)
     */
    function _getChGenre(ch) {
    	if (ch == null || ch.getSIDescriptor == null) {
    		return null;
    	}
    	
    	var siDesc = ch.getSIDescriptor(0x54);
    	if (siDesc != null) {
    		return siDesc[0];
    	}
    	return null;
    }

    /**
	 * 프로그램의 연령 정보 추출
	 */
	function _getAge(program){
    	if (program == null || program.getSIDescriptors == null) {
    		return null;
    	}
    	
    	var age = 0;
    	try{
    		var siDesc = program.getSIDescriptors(0x55);
    		var tmpAge = siDesc === null ? 0 : siDesc[0].charCodeAt(5);
    		if((program.channel != null && program.channel.idType == KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS) ||
    				KTW.CONSTANT.IS_OTS === false){
    			if(tmpAge > 0 && tmpAge <= 16){
    				age = tmpAge + 3;
    			}
    		}else{
    			if(tmpAge < 4){
    				age = 0;
    			}else if(tmpAge < 7){
    				age = 7;
    			}else if(tmpAge < 10){
    				age = 12;
    			}else if(tmpAge < 13){
    				age = 15;
    			}else if(tmpAge < 15){
    				age = 19;
    			}
    		}
    	}catch(e){
    		log.printErr(e);
    	}
    	return age;
    }

    /**
     * program 의 연령과 P.R 값을 비교하여 연령제한 프로그램인지를 판단한다.
     *
     * @param program
     * @returns {boolean}
     * @private
     */
    function _isAgeLimitProgram(program) {
        log.printDbg("_isAgeLimitProgram()");

        var programAge = _getAge(program);
        if (programAge != null) {
            //var pr = KTW.oipf.AdapterHandler.casAdapter.getParentalRating();
            var pr = KTW.managers.auth.AuthManager.getPR();
            if (pr !== 0 && programAge >= pr) {
                return true;
            }
        }

        return false;
    }
	
	/**
	 * 오디오 프로그램의 가수 정보 추출 (OTV)
	 */
	function _getSinger(prog){
		if (prog == null || prog.getSIDescriptors == null) {
			return null;
		}
		var sidesc = prog.getSIDescriptors(0x4E);
		if (sidesc == null) {
			return null;
		}
		var idx = 0;
        var descLength = sidesc.length;
		try{
			//for(var i=0; i < sidesc.length; i++){
            for (var i=0; i < descLength; i++) {
				idx = 7;
				var data = sidesc[i];
				var itemDescLength = data.charCodeAt(idx++);

                var sidescdata = [];
                for(var x=0;x<data.length;x++) {
					sidescdata[x] = data.charCodeAt(x);
                }

                var logdata = "";
				for(var y=0; y < sidescdata.length ; y++) {
					logdata += "[" + sidescdata[y] + "]";
				}
				
				var itemDesc = "";
				for(var j=0; j < itemDescLength; j++){
					var tmp = data.charCodeAt(idx++);
					//log.printDbg("tmp[" + j + "] : " +  tmp);
					if(tmp != 0 && tmp != 17){
						itemDesc += String.fromCharCode(tmp);
					}
				}
				if(itemDesc == "Singer"){
					var itemLength = data.charCodeAt(idx++);
					var item = "";
					//var tmpArr = new Array();
                    var tmpArr = [];
					var prevChar = null;
					for(var j=0; j < itemLength; j++){
						var tmp = data.charCodeAt(idx++);
						if(tmp != 17){
							tmpArr[tmpArr.length] = tmp;
							item += String.fromCharCode(tmp);
							prevChar = tmp;
						}else {
							if(prevChar !== null && prevChar>=128) {
								tmpArr[tmpArr.length] = tmp;
								item += String.fromCharCode(tmp);
								prevChar = null;
							}
						}
					}
                    return util.readUTF16String(tmpArr, item);
				}
			}
		}catch(e){
			console.log(e);
			return null;
		}
		return null;
	}
	
	/**
	 * skyChoice PPV 구매 관련
	 */
	function _saveSkyChoiceData(skyChoiceData) {
		if (skyChoiceData == null) {
			return;
		}
		
		var arr = storageManager.ps.load(storageManager.KEY.PPV_PURCHASE_INFO);
		if (arr == null || arr.length == 0) {
			arr = [];
		}
		else {
			arr = JSON.parse(arr);
		}
		arr.push(skyChoiceData);
		storageManager.ps.save(storageManager.KEY.PPV_PURCHASE_INFO, JSON.stringify(arr));
	}
	
	/**
	 * @return obj = {
	 * 				result: true - 구매한 PPV, false - 미구매 PPV
	 * 				endDate: null - 미구매 PPV, "2015072913290600" - 구매한 PPV의 유효기간 종료시점 ("yyyyMMddHHmmss")
	 * 			}
	 */
	function _getSkyChoiceData(chNo) {
		var obj = {
				result : false,
				endDate : null
			};
		var arr = storageManager.ps.load(storageManager.KEY.PPV_PURCHASE_INFO);
		if (arr != null && arr.length > 0) {
			arr = JSON.parse(arr);
			var now = util.getFormatDate(new Date(), "yyyyMMddHHmmss");
			var tmpArr = [];
			for (var i in arr) {
				if(arr[i].ppdSvcCloseDh > now){
					tmpArr.push(arr[i]);
					if(arr[i].chlNo == ("" + chNo)){
						obj.result = true;
						obj.endDate = arr[i].ppdSvcCloseDh;
					}
				}
			}
			storageManager.ps.save(storageManager.KEY.PPV_PURCHASE_INFO, JSON.stringify(tmpArr));
		}
		
		return obj;
	}

	// /**
	//  * 선호채널 등록/해제 등 관련 동작을 처리하는 모듈
	//  */
	// var FavChannelControl = (function(){
	//     // 선호채널 설정가능 최대 갯수
	//     // OTV의 경우 70개, OTS의 경우 각각 40개
	//     var FAVORITE_MAX = null;
	//     // 선호채널이 없는 경우 선호채널 편성표 호출 시 보여줄 popup id 및 callback
	//     var favEmptyPopupId = "favEmptyPopup";
	//     var callback_favEmpty = null;
	//
	//     function _checkMax() {
	//     	if (FAVORITE_MAX == null) {
	//     		FAVORITE_MAX = KTW.CONSTANT.IS_OTS === true ? 40 : 70;
	//     	}
	//     }
	//
	//     /**
	// 	 * 특정 채널을 선호채널로 등록
	// 	 * @param channel
	// 	 * @param callback 등록 성공 시 실행할 callback 함수
	// 	 */
	// 	function addFavChannel(channel, callback) {
	// 		if (channel == null) {
	// 			return false;
	// 		}
	// 		var ch_type = channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ?
	// 					KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE :
	// 					KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE;
	// 		_checkMax();
     //    	if (navAdapter.getChannelList(ch_type).length < FAVORITE_MAX) {
     //    		navAdapter.changeChannelList(ch_type, undefined, {"ccid":channel.ccid, "majorChannel":channel.majorChannel, "type":0, "update":true });
     //        	if (callback != null) {
     //        		callback();
     //        	}
     //    	}
     //    	else {
     //    		_showFavLimitedPopup();
     //    	}
	// 	}
	// 	/**
	// 	 * 특정 채널을 선호채널에서 제외
	// 	 * @param channel
	// 	 * @param callback 등록 성공 시 실행할 callback 함수
	// 	 */
	// 	function removeFavChannel(channel, callback) {
	// 		if (channel == null) {
	// 			return false;
	// 		}
	// 		var ch_type = channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ?
	// 				KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE :
	// 				KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE;
    	// 	navAdapter.changeChannelList(ch_type, undefined, {"ccid":channel.ccid, "majorChannel":channel.majorChannel, "type":1, "update":true });
     //    	if (callback != null) {
     //    		callback();
     //    	}
	// 	}
	// 	/**
	// 	 * 선호채널 갯수 초과 시 보여줄 안내 팝업
	// 	 */
	// 	function _showFavLimitedPopup() {
	// 		var popup_data = {};
	// 		_checkMax();
	//         popup_data = {
	//                 title: "선호 채널",
	//                 desc: [
	//                        {"size":"20", "color":"rgb(0,0,0)", "text":"선호 채널은"},
	//                        {"size":"20", "color":"rgb(0,0,0)", "text":FAVORITE_MAX + "개까지 설정이 가능합니다"}
	//                        ],
	//                 type: KTW.ui.popup.BaseDialog.TYPE.CONFIRM
	//         };
	//         layerManager.activateLayer(
	//         		{
	//         			id: "fav_limited", type: KTW.ui.Layer.TYPE.POPUP,
	//         			priority: KTW.ui.Layer.PRIORITY.POPUP,
	//         			view : KTW.ui.popup.BaseDialog,
	//         			params: {
	//         				data : popup_data
	//         			}
	//         		});
	//         popup_data = null;
	// 	}
	//
	// 	/**
	//      * 선호채널 없는 경우 선호채널등록 팝업 호출
	//      */
	//     function showFavEmptyPopup(callback) {
	// 		var params = {};
	//     	if (callback != null) {
	//     		callback_favEmpty = callback;
	// 			params.callback = callback_favEmpty;
	// 		}
    //
	// 		KTW.ui.LayerManager.activateLayer({
	// 			obj: {
	// 				id: "FavoriteChannelEmptyPopup",
	// 				type: KTW.ui.Layer.TYPE.POPUP,
	// 				priority: KTW.ui.Layer.PRIORITY.POPUP ,
	// 				view : KTW.ui.view.popup.FavoriteChannelEmptyPopup,
	// 				linkage: false,
	// 				params: {
	// 					data : params
	// 				}
	// 			},
	// 			visible: true
	// 		});
    //
    //
	// 	}
	// 	function _callbackFavEmptyPopup(result, param) {
	// 		layerManager.deactivateLayer(favEmptyPopupId, true);
	// 		if (result === 0) {
	// 			if (callback_favEmpty != null) {
	// 				callback_favEmpty();
	// 				callback_favEmpty = null;
	// 			}
	// 		    // [jh.lee] 선호채널 메뉴 ID 참조 변경
	//         	layerManager.activateLayer({
	//         		id: KTW.ui.Layer.ID.SETTING,
	//         		params: {menuId:KTW.managers.data.MenuDataManager.MENU_ID.SETTING_FAVORITE_CHANNEL}
	//         		});
	// 		}
	// 	}
	//
	// 	return {
	// 		addFavChannel: addFavChannel,
	// 		removeFavChannel: removeFavChannel,
	// 		showFavEmptyPopup: showFavEmptyPopup
	// 	};
	// }());

    /**
     * [dj.son] Date 의 minute 을 30 분 단위로 floor 하는 함수, sec 와 millisec 는 0 으로 설정
     */
    function _reviseDate (date) {
        if (date.getMinutes() < 30) {
            date.setMinutes(0);
        }
        else {
            date.setMinutes(30);
        }
        date.setSeconds(0);
        date.setMilliseconds(0);

        return date;
    }

    function _isValidProgram(program) {
    	if(program === null) {
    		return false;
		}
        if (program.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL ||
			program.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE ||
			program.startTime == null || program.duration == null) {
            return false;
        }

        return true;
	}

    return {
    	get progInfo() {
    		return _progInfo;
    	},
		reconstructionChList: _reconstructionChList,
        reconstructionCh: _reconstructionCh,
		revisePrograms: _revisePrograms,
		getChGenre: _getChGenre,
		getAge: _getAge,
		getSinger: _getSinger,
		saveSkyChoiceData: _saveSkyChoiceData,
		getSkyChoiceData: _getSkyChoiceData,
		isValidProgram:_isValidProgram,
		// addFavChannel: FavChannelControl.addFavChannel,
		// removeFavChannel: FavChannelControl.removeFavChannel,
		// showFavEmptyPopup: FavChannelControl.showFavEmptyPopup,

        reviseDate: _reviseDate,
        isAgeLimitProgram: _isAgeLimitProgram
        //isCustomPromoChannel: _isCustomPromoChannel
    };

}());

Object.defineProperty(KTW.utils.epgUtil, "DATA", {
	value: {
		AUDIO : {
			LANG_CODE: {
				KOR : "kor",
		    	ENG : "eng"
			},
			LANG_STR: {
				KOR : "한국어",
		    	ENG : "영어",
		    	ETC : "기타"
			}
		},
		/**
	     * 프로그램 시간 단위가 초 단위인지 여부
	     * true: second 단위, false: millisecond 단위
	     * SSTV, LGS는 ms, KTW는 s 단위
	     */
	    IS_TIME_UNIT_SEC : true,
	    PROGRAM_TITLE : {
	    	NULL : "프로그램 정보가 없습니다",
            UPDATE: "업데이트 중입니다",
	    	BLOCKED : "시청 제한 채널입니다",
	    	AGE_LIMIT : "시청연령 제한 프로그램입니다",
	    	NOT_SUBSCRIBED : "가입 즉시 시청이 가능합니다!"
	    },
        PROGRAM_REVISE_TYPE: {
            NULL: 0,
            UPDATE: 1,
            CHANNEL_NAME: 2,
            NORMAL: 3
        },
	    CHANNEL_TYPE : {
	    	//성인메뉴 숨김 ON:전체편성표 채널명으로표기 OFF: 프로그램명으로 노출
	    	GRID_HIDDEN_PROGRAM_SKYLIFE_WITH_SETTING : 176,
	    	//전체편성표에서는 채널 제거, 미니가이드에서는 채널명 노출
	    	GRID_HIDDEN_CHANNEL_SKYLIFE : 177,
	    	//전체 편성표에서 채널명으로 표기
	    	GRID_HIDDEN_PROGRAM_SKYLIFE : 178,
	    	//전체편성표와 미니가이드에서 채널명 노출
	    	HIDDEN_PROGRAM_SKYLIFE : 179
	    }
	},
	writable: false,
	configurable: false
});
