"use strict";
KTW.utils.Stack = function Stack() {
	var path_stack = [];
	
	this.push = function push(path) {
        if (path_stack === null) {
        	path_stack = [];
        }
        path_stack.push(path);
    }
    
	this.pop = function pop() {
    	if(path_stack.length > 0) {
    		var path = path_stack[path_stack.length-1];
        	path_stack.splice(path_stack.length-1, 1);
        	return path;
    	}
    	return null;
    }
    
    this.peek = function peek() {
    	if(path_stack.length > 0) {
    		var path = path_stack[path_stack.length-1];
        	return path;
    	}
    	return null;
    }
}