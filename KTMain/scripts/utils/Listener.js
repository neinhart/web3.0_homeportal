"use strict";
KTW.utils.Listener = function() {
	this.listener = [];
}

KTW.utils.Listener.prototype.checkDup = function(l) {
	for ( var key in this.listener) {
		if (this.listener[key] == l) {
			return true;
		}
	}
	return false;
}

KTW.utils.Listener.prototype.addListener = function(l) {
	if (!this.checkDup(l)) {
		this.listener.push(l);
	}
};

KTW.utils.Listener.prototype.removeListener = function(l) {
	for (var i = 0; i < this.listener.length; i++) {
		if (this.listener[i] == l) {
			return this.listener.splice(i, 1);
		}
	}
	return null;
};

KTW.utils.Listener.prototype.notify = function() {
	if(this.listener.length == 0)
		return false;
	
	for ( var key in this.listener) {
		this.listener[key].apply(this, arguments);
	}
	return true;
};

KTW.utils.Listener.prototype.removeAllListeners = function() {
	for ( var key in this.listener) {
		this.listener[key] = null;
	}
	this.listener = [];
};

KTW.utils.Listener.prototype.isAvailable = function() {
	if(this.listener.length == 0)
		return false;
	
	return true;
}