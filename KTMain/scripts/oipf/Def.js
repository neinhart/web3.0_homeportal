"use strict";

/**
 * The definitions for navigator
 */
KTW.oipf.Def = {
    BROADCAST : { // VideoBroadcast Object
        EVENT : {
            CHANNEL_CHANGE_SUCCEEDED : "ChannelChangeSucceeded",
            CHANNEL_CHANGE_ERROR : "ChannelChangeError",
            REMINDER : "Reminder",
            PMT_CHANGED : "PMTChanged",
            SELECTED_COMPONENT_CHANGED : "SelectedComponentChanged"
        },
        CHANNEL_CONFIG : { // ChannelConfig Object
            CHANNEL_LIST_UPDATE : "ChannelListUpdate",
            FAVOURITE_LIST_UPDATE : "FavouriteListUpdate",
            
            FAVOURITE_SURFABLE : "favourite:SURFABLE",
            FAVOURITE_VIDEO : "favourite:VIDEO_SERVICE",
            FAVOURITE_FAVORITE : "favourite:FAVORITE", //선호채널 
            FAVOURITE_BLOCKED : "favourite:BLOCKED", //제한채널 
            FAVOURITE_SKIPPED : "favourite:SKIPPED", //건너뛰기채널 
            //FAVOURITE_PPV : "favourite:PPV", //PPV
            //FAVOURITE_GENRE_TERRESTRIAL : "favourite:TERRESTRIAL", //지상파/홈쇼핑
            FAVOURITE_AUDIO : "favourite:AUDIO", //오디오채널
            FAVOURITE_DATA : "favourite:DATA_SERVICE", //데이터 서비스채널
            //FAVOURITE_VOD : "favourite:VOD_SERVICE", //VOD  서비스채널
            FAVOURITE_KIDS_CARE : "favourite:KT_CHANNELS_KIDS_CARE", //KIDS CARE 채널
            FAVOURITE_TV_APP : "favourite:KT_CHANNELS_TV_APP", //TV 앱 채널
            FAVOURITE_CUG : "favourite:KT_CHANNELS_CUG", //CUG 채널

            // skipped channel을 제외한 내부용 channel list
            FAVOURITE_FULL_EPG : "favourite:KT_VIDEO_FULL_EPG", //OTV 전체 편성표 용 채널
            FAVOURITE_UHD : "favourite:KT_CHANNELS_UHD",
            FAVOURITE_SURFABLE_AUDIO: "favourite:KT_SURFABLE_AUDIO",
            
            FAVOURITE_SORT_LIST : "favourite:SORT_LIST", //SkippedChannel - getList

            SKYLIFE_CHANNELS_SURFABLE : "favourite:SKYLIFE_CHANNELS_SURFABLE",
            SKYLIFE_CHANNELS_VIDEO : "favourite:SKYLIFE_CHANNELS_VIDEO",
            SKYLIFE_CHANNELS_SKYHD : "favourite:SKYLIFE_CHANNELS_SKYHD",
            SKYLIFE_CHANNELS_PPV : "favourite:SKYLIFE_CHANNELS_PPV",
            SKYLIFE_CHANNELS_AUDIO : "favourite:SKYLIFE_CHANNELS_AUDIO",
            SKYLIFE_CHANNELS_AUDIO_PORTAL : "favourite:SKYLIFE_CHANNELS_AUDIO_PORTAL",
            SKYLIFE_AUDIO_KISS : "favourite:SKYLIFE_CHANNELS_AUDIO_KISS",
            SKYLIFE_AUDIO_SATIO : "favourite:SKYLIFE_CHANNELS_AUDIO_SATIO",
            SKYLIFE_CHANNELS_FAVORITE : "favourite:SKYLIFE_CHANNELS_FAVORITE",
            SKYLIFE_CHANNELS_BLOCKED : "favourite:SKYLIFE_CHANNELS_LIMITED",
            SKYLIFE_CHANNELS_SKIPPED : "favourite:SKYLIFE_CHANNELS_SKIP",
            SKYLIFE_CHANNELS_KIDS_CARE : "favourite:SKYLIFE_CHANNELS_KIDS_CARE",

            // [dj.son] Web 3.0 키즈모드 & 키즈 채널 편성표용
            FAVOURITE_KIDS_MODE: "favourite:KIDS_MODE", // OTV 키즈 채널
            SKYLIFE_CHANNELS_KIDS_MODE: "favourite:SKYLIFE_CHANNELS_KIDS_MODE", // OTS 키즈 채널

            /** 2017.04.18 dhlee 맞춤 프로모 채널 */
            FAVOURITE_HIDDEN_PROMO: "favourite:HIDDEN_PROMO",

            ALL_SKY_KT : "favourite:ALL_SKY_KT",  // OTS 전체 채널
            ALL : "favourite:ALL" // OTV 전체 채널
        }
    },
    CONFIG : { // Configuration Object
        KEY : {
            SKYLIFE : "skylife_support",
            MBS : "mbs.ots",
            UHD : "support.uhd",
            HDR : "hdr_support",
            EDID : "hdr_support_by_edid",
            SAID : "SAID",
            SMARTCARD_ID : "id.smartcard",
            CHIP_ID : "id.chip",
            /** STB 구분 */
            STB_TYPE: "stb.type",
            /**
             * [jh.lee] 자막설정 ON / OFF
             */
            SUBTITLE_ONOFF : "CC_ONOFF",
            /**
             * [jh.lee] 글자 크기
             */
            SUBTITLE_SIZE : "CC_FONT_SIZE",
            /**
             * [jh.lee] 자막 언어 선택
             */
            SUBTITLE_LANGUAGE : "CC_LANGUAGE",
            /**
             * [jh.lee] 화면 해설 방송 ON/OFF
             */
            VISUAL_IMPAIRED : "Visual Impaired",
            /**
             * [jh.lee] 화면 해설 방송 언어 설정
             */
            AUDIO_LANGUAGE : "Audio Language",
            /**
             * [jh.lee] 기본 언어
             */
            LANGUAGE : "User Language",
            /**
             * [jh.lee] 시청시간제한 설정 값
             */
            RESTRICTION_TIME : "restrictionTime",
            /**
             * [jh.lee] UHD 인 경우 hdmi 연결 확인 유무
             */
            HDMI_IS_CONNECTED : "hdmi.isConnected",
            /**
             * [jh.lee] HDMI 전원 동기화 ON / OFF (OTS)
             */
            HDMI_CEC : "hdmiCEC",
            /**
             * [jh.lee] HDMI 전원 동기화 ON / OFF
             */
            HDMI_CEC_FN : "hdmiCECFunctionMasc",
            /**
             * [jh.lee] *시스템 정보* 업데이트 날짜
             */
            SOFTWARE_UPDATE_DATE : "info.softwareUpdateDate",
            /**
             * [jh.lee] *시스템 정보* 펌웨어 버전
             */
            VERSION_FIRMWARE : "version.firmware",
            /**
             * [jh.lee] *시스템 정보* Main 소프트웨어 버전
             */
            MAIN_SOFTWARE : "version.mainSoftware",
            /**
             * [jh.lee] *시스템 정보* 미들웨어 버전
             */
            MIDDLEWARE : "version.middleware",
            /**
             * [jh.lee] *시스템 정보* 네비게이터 버전
             */
            NAVIGATOR : "version.navigator",
            /**
             * [jh.lee] *시스템 정보* CA Adaptor 버전
             */
            VERSION_CAADAPTOR : "version.caAdaptor",
            /**
             * [jh.lee] *시스템 정보* KT-CAS Library 버전
             */
            VERSION_VERIFIRE : "version.verifier",
            /**
             * [jh.lee] *시스템 정보* Glue 버전
             */
            VERSION_SOFTWARE : "version.software",
            /**
             * [jh.lee] *시스템 정보* 로더 버전
             */
            VERSION_LOADER : "version.loader",
            /**
             * [jh.lee] *시스템 정보* 서비스 관리자 버전
             */
            OBSERVER : "version.observer",
            /**
             * [jh.lee] *시스템 정보* MAC 주소
             */
            MAC_ADDRESS : "network.macAddress",
            /**
             * [jh.lee] *시스템 정보* ATTS 주소
             */
            INFO_ATTSADDRESS : "info.attsAddress",
            /**
             * [jh.lee] *시스템 정보* 스마트카드 일련번호
             */
            ID_CHIP : "id.chip",
            /**
             * [jh.lee] *시스템 정보* 상품 코드
             */
            INFO_PRODUCTCODE : "info.productCode",
            /**
             * [jh.lee] *시스템 정보* Driver 버전
             */
            VERSION_DRIVER : "version.driver",
            /**
             * [jh.lee] *시스템 정보* 채널정보 업데이트 날짜
             */
            DI_UPDATEPERIOD : "di.updatePeriod",
            /**
             * [jh.lee] *시스템 정보* 채널정보 버전(BDI) 
             */
            BDI_VERSION : "bdi.version",
            /**
             * [jh.lee] *시스템 정보* 채널정보 버전(PDI)
             */
            PDI_VERSION : "pdi.version",
            /**
             * [jh.lee] *시스템 정보* 채널정보 버전(ADI)
             */
            ADI_VERSION : "adi.version",
            /**
             * [jh.lee] *시스템 정보* 네트워크 연결 상태
             */
            NETWORKSTATUE : "networkStatus",
            /**
             * [dkstjdals777] *시스템 정보* 긴급메시지 알림 설정
             */
            BMAIL : "bmailNotify.ots",
            /**
             * [dkstjdals777] *시스템 정보* 채널정보 출력 설정
             */
            CHANNEL_INFO_OUTPUT : "channelInfoOutput",
            /**
             * [jh.lee] 시스템 정보 S/W Download Status 정보 (OTS)
             */
            DOWNLOAD_STATUS : "downloadStatus",
            /**
             * [jh.lee] 시스템 정보 시리얼 번호 정보 (OTS)
             */
            SERAILNUMBER : "serialNumber",
            /**
             * [jh.lee] 시스템 정보 Tune Status 정보 (OTS)
             */
            TUNE_STATE : "tuneStatus",
            /**
             * [jh.lee] 시스템 정보 Manufacturer ID 정보 (OTS)
             */
            ID_MANUFACTURER : "id.manufacturer",
            /**
             * [jh.lee] 시스템 정보 하드웨어 버전 정보 (OTS)
             */
            VERSION_HARDWARE : "version.hardware", 
            /**
             * [jh.lee] 시스템 정보 CAK 버전 정보 (OTS)
             */
            VERSION_CAAPI : "version.caAPI",
            /**
             * [jh.lee] 시스템 정보 모델 넘버 정보 (OTS) ex) UHD-S1 ...
             */
            MODEL_NUMBER : "modelNumber",
            /**
             * [jh.lee] 수신품질 측정에 필요한 모델 넘버 정보 (OTS) ex) 0x25 0x28 ...
             */
            SIGNAL_MODEL_NUMBER : "model_number",
            /**
             * [jh.lee] 시스템 정보 스마트카드 버전 정보 (OTS)
             */
            VERSION_CARD : "version.card",
            /**
             * [jh.lee] CAS 정보 ECM Metadata (OTS 히든 메뉴)
             */
            CAS_ECMMETA : "caECMMetadata",
            /**
             * [jh.lee] CAS 정보 Zip Code (OTS 히든 메뉴)
             */
            CAS_ZIP_CODE : "caZipCode",
            /**
             * [jh.lee] CAS 정보 Segments (OTS 히든 메뉴)
             */
            CAS_SEGMENT : "caSegment",
            /**
             * [jh.lee] CAS 정보 BAT ID (OTS 히든 메뉴)
             */
            ID_BOUQUET : "id.bouquet",
            /**
             * [jh.lee] CAS 정보 STB chipset NUID (OTS 히든 메뉴)
             */
            CAS_NUID : "caNUID",
            /**
             * [jh.lee] CAS 정보 STB CA S/N (OTS 히든 메뉴)
             */
            CAS_SERIAL_NUM : "caSerialNumber",
            /**
             * [jh.lee] CAS 정보 Project Information (OTS 히든 메뉴)
             */
            CAS_PROJECT_INFO : "caProjectInfo",
            /**
             * [jh.lee] CAS 정보 Chipset Version (OTS 히든 메뉴)
             */
            VERSION_CHIPSET : "version.chipset",
            /**
             * [jh.lee] CAS 정보 Chipset Revision (OTS 히든 메뉴)
             */
            INFO_CHIPSET_REV : "info.chipsetRevision",
            /**
             * [jh.lee] 저전력 설정값 (UHD)
             */
            POWER_STATUS : "stb.standbyLevel",
            /**
             * [jh.lee] 앱스토어 버전
             */
            APP_STORE_VERSION : "version.appstore",
            /**
             * [jh.lee] OAM 버전
             */
            OAM_VERSION : "version.oam",
            /**
             * [jh.lee] 개인방송 버전
             */
            P_BROADCAST_VERSION : "version.pbroadcast",
            /**
             * [jh.lee] 매시업 매니져 버전
             */
            MASHUP_VERSION : "version.mashup",
            /**
             * [jh.lee] Smart push 버전
             */
            SMART_PUSH_VERSION : "version.smartpush",
            /**
             * [jh.lee] 자녀안심 설정 모드
             * 2016.08.23 dhlee OTS 자녀안심채널 설정
             */
            KIDSCARE_MODE : "kidsCareMode",
            /**
             * [jh.lee] 자동 자녀안심 설정 모드
             * 2016.08.23 dhlee OTS 자녀안심 모드
             */
            KIDSCARE_MODE_AUTO : "kidsCareModeAuto",
            /**
             * jm.kang 성질급한 VOD Player 지원 여부 (lastTime 이 가변으로 변해야 함. 
             * SUPPORTED
             * SUPPRORT_QUICK_LIVE_VOD 를 SUPPORT_XXX 로 변경
             */
            SUPPORT_QUICK_LIVE_VOD : "live_vod_support",
            /**
             * [jh.lee] CUG 데이터
             */
            DEFAULT_CHANNEL : "default_channel",
            /**
             * [jh.lee] UHD2 STB 전용 BLE RCU 관련 값. "true" or "false"
             */
            BLE_RCU_SUPPORT : "ble_rcu_support",
            /**
             * [jh.lee] 화면해설 방송 롱프레스 키 관련 값. "true" or "false"
             */
            LONGPRESS_SHARP : "longpress_sharp",

            /**
             * 2016.10.19 dhlee 공영제어 버전
             * TODO 관련 로직 확인해서 3.0 에도 추가해야 할 것으로 판단됨.
             */
            REMOTE_AGENT: "version.remoteagent",
            /**
             * 2016.10.19 dhlee LCW 부팅 지원 여부
             * TODO 관련 로직 확인해서 3.0 에도 추가해야 할 것으로 판단됨.
             */
            LCW_BOOTING_SUPPORT: "lcw_booting_support",
            /**
             * 2016.10.19 dhlee 성인 메뉴 노출 여부 "true" or "false" (or "undefined")
             */
            ADULT_MENU_DISPLAY: "Adult Menu Display",

            /** 2017.01.23 dhlee VOD SDK 버전 */
            CASTIS_SDK_VERSION: "castis_sdk_version",

            /**
             *  2016.11.21 dhlee UHD3 세부 프러퍼티 정의
             */
            UHD3 : {
                BEACON  : "audio_tts_support",
                TTS     : "ble_beacon_support",
                SPEAKER : "emspeaker_vol_control",
                VOICE   : "em_voice_support"
            },
            /**
             * 2016.11.21 dhlee beacon
             *
             * 송신 주기 beacon_interval
             * 송신 세기 beacon_tx_power_level
             */
            BEACON : {
                INTERVAL    : "beacon_interval",
                POWER_LEVEL : "beacon_tx_power_level"
            },
            /**
             * 2016.11.21 dhlee
             * 시리즈 예약 기능 지원 여부
             * TODO
             * 1. OTS 시리즈 예약 기능 지원 여부인지 단순히 appdata support 여부인지 확인
             * 2. hasReminder 동작여부와 관련되는지 확인
             */
            SERIES_RESERVATION : "reminder_appdata_support",

            /**
             * 2017.02.10 dj.son
             *
             * 키즈모드 사용 여부
             *
             * value === "ON" or "OFF"
             */
            USE_KIDS_MODE: "use_kids_mode",

            /**
             * 2017.04.10 dhlee 자동 대기 모드 (네비게이터에서 기능 제공함)
             * value === "ON"(사용) or "OFF"(사용하지 않음)
             */
            AUTO_STANDBY: "auto_standby",

            /** 2017.04.19 dhlee ATTS SI Locator 주소 */
            INFO_ATTS_LOCATOR: "info.attsLocator",

            /** 2017.04.19 dhlee 보안 키 */
            HOME_CIPHER_KEY: "home_cipher_key",
            /**
             * 2017.05.16 sw.nam *시스템 정보* 음성클라이언트 버전 정보 */
            VOICE_VERSION : "voice_version",

            /** 2017.05.29 dhlee 홈포털 버전 */
            CEMS_VERSION_HOEMPORTAL : "version.homeportal",

            /** 2017.07.11 dhlee 유튜브 지원 여부 */
            YOUTUBE_SUPPORT : "youtube_support",

            /**
             * 2017.07.18 dj.son
             * 부팅 혹은 대기모드에서 동작모드 전환시, 네비게이터에서 채널 튠 여부
             * https://issues.alticast.com/jira/browse/WEBIIIHOME-2777 이슈 수정을 위해 추가
             * value === "true" or "false"
             */
            KIDS_RESTRICTION_STATE: "home_blocked",
            /**
             * 2017.02.10 dj.son
             *
             * STB 부팅시 키즈모드로 시작 여부
             * "use_kids_mode" 가 ON 이고 해당 값이 ON 이면 키즈모드로 부팅
             * "use_kids_mode" 가 ON 이고 해당 값이 OFF 이면 키즈모드 선택 팝업 노출 후 부팅
             *
             * value === "ON" or "OFF"
             *
             * 2017.04.04 dhlee
             * 사용하지 않음(해당 시나리오 삭제됨)
             */
            //START_TV_BY_KIDS_MODE: "start_tv_by_kids_mode"

            /**
             * 2017.11.20 sw.nam
             * 음성가이드 configuration key 정의
             */
             VOICE_GUIDE : "long_distance_voice", // 음성가이드 ON / OFF / OFF 수동설정 여부
             VOICE_GUIDE_AVAILABLE_MANUAL_OFF : "long_distance_voice_new_value", // OFF 수동설정 지원 펌웨어 여부

            /**
             * 2018.02.02 sw.nam
             * 음성가이드 호출어 설정 가능 여부
             * 값이 존재하면 호출어 설정 가능. 없으면 불가능 (음성가이드 설정 메뉴 on )
             */
            VOICE_GUIDE_TRIGGER_VOICE_TYPE: "trigger_voice_type",

            /**
             * 2018.02.20 kh.kim
             * TV페이 모듈 지원 여부
             * true 이면 개인 결제 수단에 TV페이 결제수단 표시
             */
            TV_PAY_SUPPORT: "tv_pay_support",

            /**
             * [dj.son] 성인인증 PIN 넘버, 
             * 
             * - 반드시 read 용으로만 사용
             * - 부팅시 키즈 핫키에 의해 키즈 모드 변경시 사용
             */
            STB_PIN_NUMBER: "stb.pinnumber"

        },
        EVENT : {
            POWER_STATE_CHANGE : "PowerStateChange",
            NETWORK : "NetworkStateChange", // custom
            DEVICE : "DeviceStateChange", // custom
            DEVICE_CONNECT : "DeviceConnect",
            DEVICE_DISCONNECT : "DeviceDisconnect",
            DEVICE_ERROR : "DeviceError",
            BLUETOOTH_DISCOVERY : "Discovery" ,
            BLUETOOTH_ADD_DEVICE : "AddDevice",
            TEXT_CHANGE: "TextChange"   // 2016.12.20 add for oipf configuration change event
        },
        DEVICE_TYPE : {
            STORAGE : 1,
            BLUE_TOOTH : 2,
            INPUT : 3
        },
        POWER_MODE : {
            // OFF(1) 및 PASSIVE_STANDBY_HIBERNATE(4) 등도 있지만
            // 실제 사용하지 않아서 배제함
            /**
             * running mode
             */
            ON : 1,
            /**
             * standby mode (저전력 기능에 의해 standby mode 진입 시)
             * 실질적으로 
             */
            PASSIVE_STANDBY : 2,
            /**
             * standby mode (전원 key 및 버튼으로 standby mode 진입 시)
             */
            ACTIVE_STANDBY : 3
        }
    },
    // TODO 2016.10.19 dhlee 해당 정보는 VOD 쪽에서 사용하는 정보이므로 VOD 관련 Def 파일을 따로 구성하는 방안 고민
    // 만약 여기가 변경된다면 현재 reference 하고 있는 VodAdapter 도 변경되어야 함.
    MPEG : { // VideoMpeg Object
        PLAY_STATE_TYPE: {
            STOP : 0,
            PLAY : 1,
            PAUSE : 2,
            CONNECTING : 3,
            BUFFERING : 4,
            FINISHED : 5,
            ERROR : 6
        }
    },
    CSEM : { // ChannelSelectionEventManager
        CHANNEL_SELECTION_EVENT : "ChannelSelectionEvent",
        CHANNEL_SELECTING : "onChannelSelecting"
    },
    CLOSED_CAPTION : {
        ON_OFF : {
            ON : "ON",
            OFF : "OFF"
        },
        LANGUAGE : {
            BASIC : "Basic", // default
            KOREAN : "kor",
            ENGLISH : "eng"
        },
        FONT_SIZE : {
            BASIC : "BASIC",  // default
            SMALL : "SMALL",
            MEDIUM : "STANDARD",
            LARGE : "LARGE"
        }
    },
    // TODO 2017.01.23 dhlee 아래 내용은 신규로 추가됨. 관련 코드 작성 필요
    AUDIO_TTS: {
        STATE: {
            /** 음성 변환 혹은 재생 동작이 시작됨*/
            STATE_START: 0,
            /** 음성 변환 혹은 재생 동작이 종료됨*/
            STATE_END: 1,
            /** 음성 변환 혹은 재생 동작 중 에러가 발생함*/
            STATE_ERROR: 2
        },
        FORMAT: {
            /** OGG 오디오 포맷*/
            FORMAT_OGG: 0,
            /** OWAV 오디오 포맷*/
            FORMAT_WAV: 1
        },
        VOICE: {
            /** 여성(유진) 화자*/
            VOICE_FEMALE: 0,
            /** 어린이(마루) 화자*/
            VOICE_CHILD: 7
        }
    },
    AV_OUTPUT : {
        DEVICE : {
            /** STB에 내장된 모든 출력 장치*/
            ALL : 0,
            /** 메인 출력 장치*/
            MAIN : 1,
            /** 내장 스피커*/
            INTERNAL_SPEAKER : 2
        }
    },
    KIDS_MODE: {
        ON: "ON",
        OFF: "OFF"
    },
    /**
     * [dj.son] SpeechRecognizerObject 모드 추가
     *
     * TODO 실제 펌웨어에 적용되면 확인 (11/3 일 이후부터 적용 예정)
     */
    SR_MODE: {
        NONE: 0,
        KEYWORD: 1,
        PIN: 2
    }
};
