"use strict";

/**
 * control OIPF Adapter
 */
KTW.oipf.AdapterHandler = (function () {
    
    var log = KTW.utils.Log;
    
    var oipfFactory = KTW.oipf.oipfObjectFactory;
    
    var vbo_obj = null;
    var conf_obj = null;
    var app_manager = null;
    
    var configuration = null;
    
    function getVideoBroadcastObject() {
        if (!vbo_obj) {
        	try {
        		vbo_obj = oipfFactory.createVideoBroadcastObject(KTW.nav.Def.CONTROL.MAIN);
        		log.printDbg("created VideoBroadcastObject");
        	} catch(e) {
        	    log.printErr("failed createVideoBroadcastObject");
        		log.printExec(e);
        	}
        }
        return vbo_obj;
    }
    
    function getConfigurationObject() {
        if (!conf_obj) {
        	try {
        		conf_obj = oipfFactory.createConfigurationObject();
        		log.printDbg("created ConfigurationObject");
        	} catch(e) {
        	    log.printErr("failed createConfigurationObject");
        		log.printExec(e);
        	}
        }
        return conf_obj;
    }
    
    function getAppManager() {
        if (!app_manager) {
            try {
                app_manager = oipfFactory.createApplicationManagerObject();
                log.printDbg("created ApplicationManagerObject");
            } catch(e) {
                log.printErr("failed createApplicationManagerObject");
                log.printExec(e);
            }
        }
        
        return app_manager;
    }
    
    function getConfiguration() {
        if (!configuration) {
            try {
                configuration = getConfigurationObject().configuration;
                log.printDbg("got configuration");
            } catch(e) {
                log.printErr("failed get ConfigurationObject#configuration");
                log.printExec(e);
            }
        }
        
        return configuration;
    }
    
    function _init() {
    	//init 이 필요한 adapters. 

        //R5. Chrome 실행을 위해.
        try {
            //KTW.oipf.adapters.vodAdapter.init();
            KTW.oipf.adapters.watermarkAdapter.init();
        } catch (e) {
            log.printExec(e);
        }
    }
    
    /**
     * basic adapter
     * 
     * set key event listener
     * storage handling
     */
    var _basicAdapter = (function () {

        //var CONFIG_DATA_KEYS = [
        //    "User Language"
        //];
        
        var prev_screen_size = {};
        var resizeScreenListeners = [];

        var backup_activate_input = {};

        function blockKeyListener (evt) {
            try {
                switch (evt.keyCode) {
                    case KTW.KEY_CODE.LEFT:
                    case KTW.KEY_CODE.RIGHT:
                    case KTW.KEY_CODE.UP:
                    case KTW.KEY_CODE.DOWN:
                        evt.preventDefault();
                        evt.stopPropagation();
                        break;
                }
            }
            catch (e) {
                KTW.utils.Log.printErr(e.message);
            }
        }
        
        function _addKeyListener(listener) {
            //document.addEventListener("keydown", listener);
            $("body").on("keydown", listener);
            _serviceAdapter.self.onKeyDown = listener;	// TODO 2016.09.05 dhlee R6 형상에는 없어짐 확인할 것.

            $("body").on("keypress", blockKeyListener);
            $("body").on("keyup", blockKeyListener);
        }
        
        function _removeKeyListener(listener) {
            //document.removeEventListener("listener");
            $("body").off("keydown", listener);
            _serviceAdapter.self.onKeyDown = null;	// TODO 2016.09.05 dhlee R6 형상에는 없어짐 확인할 것.

            $("body").off("keypress", blockKeyListener);
            $("body").off("keyup", blockKeyListener);
        }

		// TODO 2016.09.05 dhlee R6 형상에는 없어짐 확인할 것.
        function _setKeyset(set) {
            var keyset = _serviceAdapter.self.privateData.keyset;
            keyset.setValue(keyset.OTHER, set);
        }

        function _setKeyPriority(focus, priority, window_priority) {
        	if (window_priority == undefined) {
        	    window_priority = priority;
        	}

        	_serviceAdapter.self.activateInput(focus, priority, window_priority);

            backup_activate_input.focus = focus;
            backup_activate_input.priority = priority;
            backup_activate_input.window_priority = window_priority;
        }

        function _getKeyPriority() {
            return backup_activate_input;
        }
        
        /**
         * get configuration value
         * 
         * @param key - config key 값 @see { KTW.oipf.Def.CONFIG }
         */
        function _getConfigText(key, blockLog) {
            return getConfiguration().getText(key, blockLog);
        }
        
        /**
         * set configuration value
         * 
         * @param key - config key 값 @see { KTW.oipf.Def.CONFIG }
         * @parm value - config value 값
         */
        function _setConfigText(key, value) {
            getConfiguration().setText(key, value);
            // 2017.04.25 Configuration change는 navigator 에서 제공하는 event callback 사용
            //notifyConfigDataChange(key);
        }

        /**
         * add Configuration key change event listener
         *
         * @param listener
         */
        function _addConfigChangeEventListener(listener) {
            log.printDbg("_addConfigChangeEventListener() called");
            getConfiguration().addEventListener(KTW.oipf.Def.CONFIG.EVENT.TEXT_CHANGE , listener);
        }

        /**
         * remove Configuration key change event listener
         *
         * @param listener
         */
        function _removeConfigChangeEventListener(listener) {
            log.printDbg("_removeConfigChangeEventListener() called");
            getConfiguration().removeEventListener(KTW.oipf.Def.CONFIG.EVENT.TEXT_CHANGE, listener);
        }

        /**
         * resize screen
         * 
         * TV_VIEWING 상태와 VOD 상태의 video object가 다르기 때문에
         * 별도로 처리해야 함
         * 우선 basicAdaptor에 두고 추후 navAdapter와 vodAdapter 개별적으로
         * 처리하는 방안을 고민해도 될 듯...
         * 
         * 15.6.25. vod 재생중 full epg -> vod 강제 종료 -> vmo 는 작은 size vbo 는 fullsize 인 상태로 종료됨  
         * 이 상태에선 채널은 작게 나와야 하는데 전체로 나오고. vmo 는 나중 재생때 pip size 로 나오게 됨. 
         * 상태는 항상 같이 움직이도록 수정.
         */

		// TODO 2016.09.05 dhlee R6 형상과 비교해봐야 함.
        function _resizeScreen(x, y, width, height, force) {
            //var state = KTW.managers.service.StateManager.state;
            //var time_restrict_in_state = KTW.managers.service.StateManager.time_restrict_in_state;
            //
            //log.printDbg("resizeScreen(x:" + x + " y:" + y + " w:" + width + " h:" + height + ") state:" + state + "type:"+type);
            //
            //var is_full_screen = false;
            //
            //prev_screen_size.x = x;
            //prev_screen_size.y = y;
            //prev_screen_size.width = width;
            //prev_screen_size.height = height;
            //
            //var video_object = [];
            //video_object.push(getVideoBroadcastObject());
            ////VOD 도 같이 하나 VOD 안할땐 type 에 ch 를 넣음 된다.
            //if(type !== "ch") {
            //    video_object.push(KTW.oipf.adapters.vodAdapter.getMpegObject());
            //    //시청제한인데 VOD 에서 시청제한으로 갔을땐 자막 resize 해야 해서 조건 추가.
            //    if (state === KTW.CONSTANT.SERVICE_STATE.VOD || (state === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED && time_restrict_in_state === KTW.CONSTANT.SERVICE_STATE.VOD)) {
            //        // VOD 재생 중 smi 자막이 존재하는 영상의 경우
            //        // AV resize 될 때 의도적으로 호출해 준다.
            //        try {
            //            KTW.vod.VodControl.resizeCaption(x, y, width, height);
            //        }
            //        catch (e) {
            //            log.printExec(e);
            //        }
            //    }
            //}
            //
            //if (width === KTW.CONSTANT.RESOLUTION.WIDTH) {
            //    is_full_screen = true;
            //}
            //
            //for(var i = 0 ; i < video_object.length ; i++) {
            //	video_object[i].x = x;
            //    video_object[i].y = y;
            //    video_object[i].width = width;
            //    video_object[i].height = height;
            //    video_object[i].setFullScreen(is_full_screen);
            //}


            if (prev_screen_size.x === x && prev_screen_size.y === y && prev_screen_size.width === width && prev_screen_size.height === height) {
                if (!force) {
                    log.printDbg("setVideSize(), same size. so return");
                    return;
                }
            }
            prev_screen_size.x = x;
            prev_screen_size.y = y;
            prev_screen_size.width = width;
            prev_screen_size.height = height;

            var mainChannelControl = KTW.oipf.adapters.NavAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            mainChannelControl.setVideoSize(x, y, width, height);

            mainChannelControl.vbo;

            KTW.oipf.adapters.vodAdapter.resizeVODScreen(x, y, width, height);

            notifyResizeScreen();
        }
        
        function _getScreenSize() {
            var mainChannelControl = KTW.oipf.adapters.NavAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            return mainChannelControl.getVideoSize();
        }

        function _addResizeScreenListener (listener) {
            resizeScreenListeners.push(listener);
        }

        function _removeResizeScreenListener (listener) {
            var index = resizeScreenListeners.indexOf(listener);
            if (index !== -1) {
                resizeScreenListeners.splice(index, 1);
            }
        }

        function notifyResizeScreen () {
            for (var i = 0; i < resizeScreenListeners.length; i++) {
                resizeScreenListeners[i](prev_screen_size);
            }
        }
        
        function _addObjectChangeEventListener(url, listener) {
            getVideoBroadcastObject().addObjectChangeEventListener(url, listener);
        }
        
        //function notifyConfigDataChange(key) {
        //    var index;
        //
        //    index = CONFIG_DATA_KEYS.indexOf(key);
        //
        //    if (index !== -1) {
        //        var length = listeners.length;
        //        for (var i = 0; i < length; i++) {
        //            listeners[i](key);
        //        }
        //    }
        //}
        
        /**
         * [jh.lee] 미들웨어에서 관리하는 데이터가 변경되었을시 실행할 Listener 등록
         * @deprecated
        function _addConfigDataChangeListener(listener) {
            listeners.push(listener);
        }
        */
        /**
         * [jh.lee] Listener 등록 해제
         * @deprecated
        function _removeConfigDataChangeListener(listener) {
            var index = listeners.indexOf(listener);
            if (index !== -1) {
                listeners.splice(index, 1);
            }
        }
        */
        
        return {
            addKeyListener: _addKeyListener,
            removeKeyListener: _removeKeyListener,
            setKeyset: _setKeyset, // TODO 2016.09.05 dhlee R6 형상에는 없어짐 확인할 것.
            setKeyPriority: _setKeyPriority,
            getKeyPriority: _getKeyPriority,
            
            getConfigText: _getConfigText,
            setConfigText: _setConfigText,
            addConfigChangeEventListener: _addConfigChangeEventListener,
            removeConfigChangeEventListener: _removeConfigChangeEventListener,
            
            resizeScreen: _resizeScreen,
            getScreenSize:_getScreenSize,
            addResizeScreenListener: _addResizeScreenListener,
            removeResizeScreenListener: _removeResizeScreenListener,
            
            get localStorage() {
                return localStorage;
            },
            
            get sessionStorage() {
                return sessionStorage; 
            },
            
            addObjectChangeEventListener: _addObjectChangeEventListener
            // 2017.04.25 dhlee 아래 listener 는 사용해서는 안된다.
            //addConfigDataChangeListener: _addConfigDataChangeListener,
            //removeConfigDataChangeListener: _removeConfigDataChangeListener
        }
    }());
    
    var _serviceAdapter = (function () {
        
        var self;
        var app_list = [];
        
        function _getOwnApp() {
            if (!self) {
                self = getAppManager().getOwnerApplication();
            }
            
            return self;
        }
        
        function _findApplications(key, value) {
            return getAppManager().findApplications(key, value);
        }
        
        function _setAppLoadListener(listener) {
            
            if (listener === null) {
                getAppManager().onApplicationLoaded = null;
                getAppManager().onApplicationUnloaded = null;
                getAppManager().onApplicationLoadError = null;
            }
            else {
                getAppManager().onApplicationLoaded = function(app) {
                    listener("loaded", app);
                };
                getAppManager().onApplicationUnloaded = function(app) {
                    listener("unloaded", app);
                };
                getAppManager().onApplicationLoadError = function(app) {
                    listener("error", app);
                };
            }
        }
        
        function _startAppByLocator(locator) {
            
            try {
                var channel = KTW.oipf.adapters.NavAdapter.getChannelByTriplet(locator);
                getVideoBroadcastObject().setChannel(channel);
            }
            catch(e) {
                log.printErr("_startMulticastApp() failed tune by locator")
                log.printExec(e);
            }
        }
        
        return {
            get self() {
                return _getOwnApp();
            },
            
            findApplications: _findApplications,
            
            setAppLoadListener: _setAppLoadListener,
            
            startAppByLocator: _startAppByLocator
        }
    }());
    
    var _casAdapter = (function () {
        
        var CA_EVENT_LISTENER_NAME = "caEventArrived";
        var MMI_EVENT_LISTENER_NAME = "receiveMMIEvent";
        
        var cas_agent = null;
        var mmi_agent = null;
        var parental_control = null;
        
        function getCasAgent() {
            if (!cas_agent) {
                try {
                    cas_agent = oipfFactory.createCasAgentObject();
                    log.printDbg("created CasAgentObject");
                } catch(e) {
                    log.printErr("failed createCasAgentObject");
                    log.printExec(e);
                }
            }
            
            return cas_agent;
        }
        
        function getMmiAgent() {
            if (!mmi_agent) {
                try {
                    mmi_agent = oipfFactory.createMMIAgentObject();
                    log.printDbg("created MMIAgentObject");
                } catch(e) {
                    log.printErr("failed createMMIAgentObject");
                    log.printExec(e);
                }
            }
            
            return mmi_agent;
        }
        
        function getParentalControl() {
            if (!parental_control) {
                try {
                    parental_control = oipfFactory.createParentalControl();
                    log.printDbg("created ParentalControl");
                } catch(e) {
                    log.printErr("failed createParentalControl");
                    log.printExec(e);
                }
            }
            
            return parental_control;
        }
        
        function _sendCaMsg(app_id, trans_id, tag, message) {            
            log.printDbg("casAdapter#sendCaMsg()");
            
            try {
                getCasAgent().request(app_id, trans_id, tag, message);
            } catch(e) {
                log.printErr("failed send ca message");
                log.printExec(e);
            }
        }
        
        function _setCaEventListener(listener) {
            var result;
            
            try {
                result = getCasAgent().addCAEventListener(CA_EVENT_LISTENER_NAME, listener);
            } catch(e) {
                log.printErr("failed addCAEventListener");
                log.printExec(e);
            }
            
            return result;
        }
        
        function _setMmiEventListener(listener) {
            try {
                getMmiAgent().addMMIEventListener(MMI_EVENT_LISTENER_NAME, listener);
            } catch(e) {
                log.printErr("failed addMMIEventListener");
                log.printExec(e);
            }
        }
        
        /**
         * 성인 PIN 인증
         * 
         * @param pin
         */
        function _verifyPIN(pin) {
            return getParentalControl().verifyPIN(pin);
        }
        
        /**
         * 성인 PIN 변경
         * 
         * @param old_pin - 기존 pin
         * @param new_pin - 변경할 pin
         */
        function _changePIN(old_pin, new_pin) {
            return getParentalControl().changePIN(old_pin, new_pin);
        }
        
        /**
         * 성인 PIN 초기화
         * 
         * @param old_pin - 기존 pin
         * @param new_pin - 변경할 pin
         */
        function _resetPIN() {
            getParentalControl().resetPIN();
        }
        
        /**
         * get parental rating value
         * 
         * @returns rating value (실제 rating value 값이 리턴됨)
         *          0 - 연령제한 없음
         *          7 - 7세 이상 제한
         *          12 - 12세 이상 제한
         *          15 - 15세 이상 제한
         *          19 - 19세 이상 제한
         */
        function _getParentalRating() {
            return getParentalControl().getParentalRating();
        }
        
        /**
         * set parental rating value
         * 
         * @param rate - parental rating value to change
         * @param pin - 성인 인증 pin 
         */
        function _setParentalRating(rate, pin) {
            getParentalControl().setParentalRating(rate, pin);
            
            //변경 여부 notify.
            // 2017.05.29 set 이후 바로 하지 않고 Rating을 set 하는 쪽에서 listener 를 호출하도록 한다.
            //onParentalRatingChangeListener.notify();
        }
        
        /**
         * release parental rating block 
         * 
         * @param pin - 성인 인증 pin 
         */
        function _releaseParentalRating(pin) {
            return getParentalControl().releaseParentalRating(pin);
        }
        
        function _initializeLimited() {
            getParentalControl().initializeLimited();
        }
        
        var onParentalRatingChangeListener = new KTW.utils.Listener();
        
        function _addPRChangeListener(l) {
        	onParentalRatingChangeListener.addListener(l);
        }
        function _removePRChangeListener(l) {
        	onParentalRatingChangeListener.removeListener(l);
        }

        /**
         * 2017.05.29 dhlee
         * PR 변경 정보를 notify
         */
        function notifyPRChangeListener() {
            onParentalRatingChangeListener.notify();
        }
        
        return {
            // cas agent
            sendCaMsg: _sendCaMsg,
            setCaEventListener: _setCaEventListener,
            
            // mmi agent
            setMmiEventListener: _setMmiEventListener,
            
            // parental control
            verifyPIN: _verifyPIN,
            changePIN: _changePIN,
            resetPIN: _resetPIN,
            getParentalRating: _getParentalRating,
            setParentalRating: _setParentalRating,
            releaseParentalRating: _releaseParentalRating,
            initializeLimited: _initializeLimited,
            
            //SW Interface 를 위해 추가.
            addPRChangeListener: _addPRChangeListener,
            removePRChangeListener: _removePRChangeListener,
            notifyPRChangeListener: notifyPRChangeListener
        }
    }());
    
    /**
     * handling native module dealing with acba
     */
    var _extensionAdapter = (function () {
        
        var nav_stb_ctrl = null;
        var nav_external = null;
        var nav_nvod = null;
        var nav_legacy = null;
        var nav_emb_spk_ctrl = null;
        
        var tv_service_ext_obj = null;
        // dhlee 2016.08.05 R6 SNMP(CEMS) 관련 코드 추가
        var snmp_manager_obj = null;
        
        function _getNavStbCtrl() {
            if (!nav_stb_ctrl) {
                nav_stb_ctrl = acba("js:nav/STBCtrl");
            }
            return nav_stb_ctrl;
        }
        
        function _getNavExternal() {
            if (!nav_external) {
                nav_external = acba("js:nav/External");
            }
            return nav_external;
        }
        
        function _getNavNvod() {
            if (!nav_nvod) {
                nav_nvod = acba("js:nav/NVOD");
            }
            return nav_nvod;
        }

        function _getNav() {
            if(!nav_legacy) {
                nav_legacy = acba("js:nav/NavLegacy");
            }

            return nav_legacy;
        }

        function _getTVServiceExtObj() {
            if (!tv_service_ext_obj) {
                try {
                    tv_service_ext_obj = oipfFactory.createTVServiceExtensionObject();
                    log.printDbg("created TVServiceExtensionObject");
                } catch(e) {
                    log.printErr("failed createTVServiceExtensionObject(");
                    log.printExec(e);
                }
            }
            return tv_service_ext_obj;
        }

        // dhlee 2016.08.05 R6 SNMP(CEMS) 관련 코드 추가
        function _getSNMPManagerObj() {
            if (!snmp_manager_obj) {
                try {
                    snmp_manager_obj = oipfFactory.createSNMPManagerObject();
                    log.printDbg("created SNMPManagerObject");
                } catch(e) {
                    log.printErr("failed SNMPManagerObject(");
                    log.printExec(e);
                }
            }
            return snmp_manager_obj;
        }

        function _getEmbeddedSpeakerVolumeController (){
            if(!nav_emb_spk_ctrl) {
                nav_emb_spk_ctrl = oipfFactory.createEmbeddedSpeakerVolumeControllerObject();
            }
            return nav_emb_spk_ctrl.getEmbeddedSpeakerVolumeController();
        }
        
        return {
            /**
             * system 초기화
             */
            doFactoryReset: function() {
                _getNavStbCtrl().doFactoryReset(2);
            },
            /**
             * STB rebooting
             * - System 초기화 이후 rebooting 할 경우 호출
             * - 자동대기모드 전환 후 60초 뒤에 자동리부팅 시 호출  
             */
            reboot: function() {
                _getNavStbCtrl().reboot();
            },
            /**
             * Full Browser 상태를 종료하고 특정 channel로 tune
             * - USB app 실행 시
             * - TV_VIEWING 가 아닌 상태에서 예약알림에 의해 채널이 tune 되는 경우  
             */
            requestDestroyFullBrowser: function(channel) {
                if (channel === undefined) {
                    channel = KTW.oipf.adapters.NavAdapter.getCurrentChannel();
                } 
                _getNavExternal().requestDestroyFullBrowser(channel.ccid);
            },
            /**
             * HOME 상태가 아닌 경우를 체크하기 위한 API
             */
            getOSDState: function() {
                // OSD 상태. getOSDState() return 값은
                // 1. ext.STATE_DEFAULT (상수 0)
                // 2. ext.FULL_BROWS (상수 1)
                // 3. ext.USB_APP (상수 2)
                // 4. ext.WEB_APP(상수 4)
                return _getNavExternal().getOSDState();
            },
            /**
             * 키즈케어등 연동이슈로 nav에 vod의 상태를 바로 알려줘야할 필요성이있어서
             * 확장 api를 만들었다.
             * 
                    //VOD 가 loading 될 때 호출
                    navNVOD.setVODLoading();
                    
                    /VOD 가 시작되면 호출, vod id 와 name 을 넣어준다
                    navNVOD.setVODStarted(voidID, vodName)
                    
                    //VOD 가 끝나면 호출
                    navNVOD.setVODEnd();
                    
                    VOD 의 play rate 가 변경될 때 마다 호출
                    navNVOD.setVODPlayRate(rate);
             */
            /**
             * kidscare 서비스 상태에서 VOD 실행 상태를 전달하기 위한 API
             * VOD loading 될 때 호출
             */
            setVODLoading: function() {
                _getNavNvod().setVODLoading();
            },
            /**
             * kidscare 서비스 상태에서 VOD 실행 상태를 전달하기 위한 API
             * VOD 의 play rate 가 변경될 때 마다 호출
             */
            setVODPlayRate: function(rate) {
                _getNavNvod().setVODPlayRate(rate);
            },
            /**
             * kidscare 서비스 상태에서 VOD 실행 상태를 전달하기 위한 API
             * VOD 가 시작되면 호출, vod content id 와 title 전달
             */
            setVODStarted: function(id, title) {
                _getNavNvod().setVODStarted(id, title);
            },
            /**
             * kidscare 서비스 상태에서 VOD 실행 상태를 전달하기 위한 API
             * VOD 종료 시 호출
             */
            setVODEnd: function() {
                _getNavNvod().setVODEnd();
            },
            /**
             * 시청시간 제한 해제 시 이를 알려주는 API
             * 홈에서 해제를 하기 때문에 이를 전달하도록 함
             */
            releaseTimeRestricted: function() {
                try {
                    _getTVServiceExtObj().releaseWatchingTimeBlock();
                } catch(e) {
                    log.printErr("failed to release watching time block");
                    log.printExec(e);
                }
            },

            /*releaseTimeRestricted 을 할 수 있는지 여부 .*/
            hasTVServiceExtension: function() {
                try {
                    return _getTVServiceExtObj().hasTVServiceExtension();
                } catch(e) {
                    log.printErr("failed to release watching time block");
                    log.printExec(e);
                }
            },

            getRealCurrentChannel: function() {
                try {
                    return _getNav().getRealCurrentChannel();
                } catch(e) {
                    log.printExec(e);
                }

                return null;
            },

            // dhlee 2016.08.05 R6 SNMP(CEMS) 관련 코드 추가
            notifySNMPError: function(errorCode) {
                try {
                    return _getSNMPManagerObj().notifySNMPError(errorCode);
                } catch (e) {
                    log.printErr("failed to notifySNMPError");
                    log.printExec(e);
                }
            },

            getEmbeddedSpeakerVolumeController : function(){
                try {
                    return _getEmbeddedSpeakerVolumeController();
                } catch(e) {
                    log.printErr("failed to getEmbeddedSpeakerVolumeController");
                    log.printExec(e);
                }
            },

            getNavStbCtrl : function(){
                try {
                    return _getNavStbCtrl();
                } catch(e) {
                    log.printErr("failed to getNavStbCtrl");
                    log.printExec(e);
                }
            }
        }
    }());
    
    var _speechRecognizerAdapter = (function() {

        var is_started = false;
    	var voiceRecongizerObject = null;
    	var speechRecongnizerListener = new KTW.utils.Listener();
    	
    	function getSpeechRecognizerObject () {
    		if (!voiceRecongizerObject) {
    			voiceRecongizerObject = oipfFactory.createSpeechRecognizerObject();
            }

    		return voiceRecongizerObject;
    	}

        /**
         * [dj.son] isPinMode 가 true 일 경우 PIN 모드로 동작, 그 외에는 KEYWORD 모드로 동작
         */
    	function start (isPinMode) {
            var sco = getSpeechRecognizerObject();

            //시작 중인데 start 가 또 오면 ..
            if (is_started === true) {
                sco.stop();
            }

            /**
             * [dj.son] 음성핀 지원을 안하는 펌웨어에서는 start 함수를 호출안하도록 예외처리 추가
             */
            if (isPinMode && (sco.MODE_PIN === undefined || sco.MODE_PIN === null)) {
                return;
            }

            var param = null;
            if (isPinMode) {
                param = KTW.oipf.Def.SR_MODE.PIN;
            }

            sco.start(param);
            is_started = true;
    	}
    	
    	function stop () {
            getSpeechRecognizerObject().stop();
            is_started = false;
    	}
    	
    	function addSpeechRecognizerListener (l) {
    		speechRecongnizerListener.addListener(l);
    	}
    	function removeSpeechRecognizerListener (l) {
    		speechRecongnizerListener.removeListener(l);
    	}
    	
    	getSpeechRecognizerObject().onended = function() {
    		log.printDbg("[SpeechRecognizer] onended ");
    		speechRecongnizerListener.notify('end');
    	};
    	getSpeechRecognizerObject().onrecognized = function (input, mode) {
			log.printDbg("[SpeechRecognizer] onrecognized input:" + input + ", mode : " + mode);

            /**
             * [dj.son] 비밀번호 음성 인식 모드 적용, 해당 모드일 경우 숫자가 아니거나 4자리가 아니면 "" 으로 전달하도록 사전 체크
             */
            if (input === undefined || input === null) {
                input = "";
            }

            if (mode === KTW.oipf.Def.SR_MODE.PIN) {
                var tempInput = parseInt(input, 10);
                if (isNaN(tempInput)) {
                    input = "";
                }
                else {
                    if (input.length !== 4) {
                        input = "";
                    }
                    // else {
                    //     input = "" + tempInput;
                    // }
                }
            }

			speechRecongnizerListener.notify('recognized', input, mode);
		};

        /**
         * [dj.son] [WEBIIIHOME-3592] Pin 모드 지원 여부 체크한 후 지원하면 stop 을 호출하는 함수 추가
         * 2018.01.04 sw.nam [WEBIIIHOME-3592] 원복 코드 재 반영
         */
        function _checkPinModeStop () {
            var sco = getSpeechRecognizerObject();

            if (sco.MODE_PIN === undefined || sco.MODE_PIN === null) {
                return;
            }
            stop();
        }

        return {
			'start': start,
			'stop': stop,
			'addSpeechRecognizerListener': addSpeechRecognizerListener,
			'removeSpeechRecognizerListener': removeSpeechRecognizerListener,

             checkPinModeStop: _checkPinModeStop
		}
    } ());
    
    var _zipAdapter = (function(){
    	var zipObject = null;
    	
    	function getZipExtractorObject() {
    		if(!zipObject) {
    			zipObject = oipfFactory.createZipExtractorObject();
    		}	
    		return zipObject;
    	}
    	
    	return {
    		hasZipExtractor: function() {
    			return getZipExtractorObject().hasZipExtractor();
    		},
    		/**
    		 * path 와 dest 가 같은 폴더면 안풀림.
    		 */
    		requestExtraction: function(path, dest_dir, callback){
    			return getZipExtractorObject().requestExtraction(path, dest_dir, callback);
    		},
    		cancelRequest: function(handle){
    			return getZipExtractorObject().cancelRequest(handle);
    		}
    	};
    }());
    
    return {
    	init: _init,
        get basicAdapter() {
            return _basicAdapter;
        },
        get navAdapter() {
        	return KTW.oipf.adapters.NavAdapter;
        },
        get serviceAdapter() {
            return _serviceAdapter;
        },
        get casAdapter() {
            return _casAdapter;
        },
        get hwAdapter() {
            return KTW.oipf.adapters.HWAdapter;
        },
        get programSearchAdapter() {
        	return KTW.oipf.adapters.ProgramSearchAdapter;
        },
        get vodAdapter() {
        	return KTW.oipf.adapters.vodAdapter;
        },
        get smiAdapter() {
        	return KTW.oipf.adapters.smiAdapter;
        },
        get wmAdapter() {
        	return KTW.oipf.adapters.watermarkAdapter;
        },
        get extensionAdapter() {
            return _extensionAdapter;
        },
        get speechRecognizerAdapter() {
        	return _speechRecognizerAdapter;
        },
        get zipAdapter() {
        	return _zipAdapter;
        },
        // 2016.12.05 dhlee R7 형상 추가
        get androidAdapter() {
            return KTW.oipf.adapters.AndroidAdapter;
        },
        // 2016.12.05 dhlee R7 형상 추가
        get audioTtsAdapter() {
            return KTW.oipf.adapters.AudioTtsAdapter;
        }
    }
}());
