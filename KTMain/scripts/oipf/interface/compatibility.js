//KTW.
if(KTW.PLATFORM_EXE === "PC") {
	function FakeOipfObjectFactory() {
	    
	    var log = KTW.utils.Log;
	    var onMediaContentChangedListener;
	    
		return {
			createConfigurationObject: function() {
				return{
					get localSystem() {
						return {
							get networkInterfaces() {
								log.printInfo("[compatibility:ConfigurationObject#localSystem] get networkInterfaces");
								
								var arr = [{ipAddress:"172.30.1.131", macAddress:"08:00:27:34:21:B5", connected:true}];
								arr.item = function(index) {
									return arr[index];
								}
								return arr;
							},
							
							addEventListener: function(e, h) {
								log.printInfo("[compatibility:ConfigurationObject#localSystem] get addEventListener");
							},
							
							removeEventListener: function(e, h) {
								log.printInfo("[compatibility:ConfigurationObject#localSystem] get removeEventListener");
							},
							
							get outputs() {
							    log.printInfo("[compatibility:ConfigurationObject#localSystem] get outputs");
							    return [ {} ];
							},
							
							setPowerState: function(state) {
							    log.printInfo("[compatibility:ConfigurationObject#localSystem] set PowerState");
							}
						}
					},
					
					get configuration() {
						return {
							get preferredAudioLanguage() {
								log.printInfo("[compatibility:ConfigurationObject#configuration] get preferredAudioLanguage");
							},
							
							set preferredAudioLanguage(v) {
								log.printInfo("[compatibility:ConfigurationObject#configuration] set preferredAudioLanguage");
							},
							
							get preferredSubtitleLanguage() {
								log.printInfo("[compatibility:ConfigurationObject#configuration] get preferredSubtitleLanguage");
							},
							
							set preferredSubtitleLanguage(v) {
								log.printInfo("[compatibility:ConfigurationObject#configuration] set preferredAudioLanguage");
							},
							
							getText: function(k) {
								if(k === 'SAID') {
									return KTW.SAID;
								}
								
								log.printInfo("[compatibility:ConfigurationObject#configuration] getText");
								return localStorage.getItem(k);
							},
							
							setText: function(k,v) {
								log.printInfo("[compatibility:ConfigurationObject#configuration] setText");
								return localStorage.setItem(k,v);
							}
						}//return
					}//confi
				}//return
			},//createconfi
			
			createVideoBroadcastObject: function() {
				var object = document.createElement("object");
				
				object.bindToCurrentChannel = function() {
					log.printInfo("[compatibility:VideoBrocastObject] bindToCurrentChannel");
				};			
				object.addEventListener = function() {
					log.printInfo("[compatibility:VideoBrocastObject] addEventListener");
				};			
				object.removeEventListener = function() {
					log.printInfo("[compatibility:VideoBrocastObject] removeEventListener");
				};			
				object.addObjectChangeEventListener = function() {
					log.printInfo("[compatibility:VideoBrocastObject] addObjectChangeEventListener");
				};			
				object.removeReminder = function() {
					log.printInfo("[compatibility:VideoBrocastObject] sremoveReminderetText");
				};			
				object.setReminder = function() {
					log.printInfo("[compatibility:VideoBrocastObject] setReminder");
				};			
				object.setFullScreen = function() {
					log.printInfo("[compatibility:VideoBrocastObject] setFullScreen");
				};
				object.enableAudio = function() {
					log.printInfo("[compatibility:VideoBrocastObject] enableAudio");
				};
				object.disableAudio = function() {
					log.printInfo("[compatibility:VideoBrocastObject] disableAudio");
				};
				object.setChannel = function() {
					log.printInfo("[compatibility:VideoBrocastObject] setChannel");
				};
				object.stop = function() {
					log.printInfo("[compatibility:VideoBrocastObject] stop");
				};
				object.getComponents = function() {
					log.printInfo("[compatibility:VideoBrocastObject] getComponents");
	            };
	            object.selectComponent = function() {
	                log.printInfo("[compatibility:VideoBrocastObject] selectComponent");
	            };
	            object.getCurrentActiveComponents = function() {
	                log.printInfo("[compatibility:VideoBrocastObject] getCurrentActiveComponents");
	            };
				
				object.reminders = {};
				object.currentChannel = {};
				object.programmes = [];
				
				object.getChannelConfig = function() {
				    return {
				        addEventListener : function() {
				            log.printInfo("[compatibility:VideoBrocastObject#ChannelConfig] addEventListener");
				        },
				        get favouriteLists() {
				            log.printInfo("[compatibility:VideoBrocastObject#ChannelConfig] favouriteLists");
				            return [];
				        }
				    }
				};
				
				return object;
			},//createVBO,
			
			createApplicationManagerObject: function(){			
				return{
					findApplications: function (key, id) {
						log.printInfo("[compatibility:ApplicationManagerObject] findApplications");
						
						return [{
						        	show:function(){
						        		log.printInfo("[compatibility:ApplicationManagerObject] Application. show");
						        	}
							    },];
				    },
				    
				    getOwnerApplication: function () {
				    	log.printInfo("[compatibility:ApplicationManagerObject] getOwnerApplication");
				    	
				    	return {
				    	    get visible() {
	                            log.printInfo("[compatibility:ApplicationManagerObject] getOwnerApplication#visible");
	                        },
	                        set onKeyDown(v) {
	                            log.printInfo("[compatibility:ApplicationManagerObject] getOwnerApplication#onKeyDown");
	                        },
				    		show:function(){
				    			log.printInfo("[compatibility:ApplicationManagerObject] getOwnerApplication#show");
				    		},
				    		activateInput:function(){
				    			log.printInfo("[compatibility:ApplicationManagerObject] getOwnerApplication#activateInput");
				    		},
		                    privateData: {
		                        keyset: {
		                            setValue: function () {
		                                log.printInfo("[compatibility:ApplicationManagerObject] getOwnerApplication#privateData.keyset.setValue");
		                            }
		                        }
		                    }
					    };
				    },
				    
				    getChildApplications: function (val) {
				    	log.printInfo("[compatibility:ApplicationManagerObject] getChildApplications");
				    },
				    
				    get privateData() {
				    	log.printInfo("[compatibility:ApplicationManagerObject] privateData");
				    	return {
				    	    keyset: {
	                            setValue: function () {
	                                log.printInfo("[compatibility:ApplicationManagerObject] privateData#keyset setValue()");
	                            }
	                        }
				    	};
				    }
				}
			},
			
			createChannelSelectionEventManager: function() {
			    return {
			        addChannelSelectionEventListener: function(cb) {
			            log.printInfo("[compatibility:ChannelSelectionEventManager] addChannelSelectionEventListener");
			        },
	                addDirectChannelEventListener: function(cb) {
	                    log.printInfo("[compatibility:ChannelSelectionEventManager] addDirectChannelEventListener");
	                }
			    }
			},
			
			createExtendedChannelDatabase: function() {
				return {
					get skyPlusChannel(){
						console.log("[compatibility:exChannelDb] skyPlusChannel getter ");
					},
					
					get promoChannel(){
						console.log("[compatibility:exChannelDb] promoChannel getter ");
					},
					
					selectPromoChannel: function(isBlockSelection, isBlockOSD){
						console.log("[compatibility:exChannelDb] selectPromoChannel isBlockSelection:" + isBlockSelection + " isBlockOSD:" + isBlockOSD);
					},
					
					updateDIDatabase: function(){
						console.log("[compatibility:exChannelDb] updateDIDatabase ");
					},
					
					getSameKTChannel: function(channel){
						console.log("[compatibility:exChannelDb] getSameKTChannel channel:" + channel);
					},
					
					getSameSkyChannel: function(channel){
						console.log("[compatibility:exChannelDb] getSameSkyChannel channel:" + channel);
					},
					
					isKidsCareChannel: function(channel){
						console.log("[compatibility:exChannelDb] isKidsCareChannel channel:" + channel);
					},
					
					requestFirmwareDownload: function(){
						console.log("[compatibility:exChannelDb] requestFirmwareDownload");
					}
				}
			},
			
			createSearchManagerObject : function() {
				return {
				    createSearch: function(search_target) {
				        log.printInfo("[compatibility:SearchManagerObject] createSearch");
				    }
				};
			},
			
			createSSOClient : function() {
				return {
					terminalLogon: function(cb) {
			            log.printInfo("[compatibility:SSOClient] terminalLogon");
			        },
			        getSTBToken: function(cb) {
			            log.printInfo("[compatibility:SSOClient] getSTBToken");
			        },
			        getHDSToken: function(cb) {
			            log.printInfo("[compatibility:SSOClient] getHDSToken");
			        },
			        logout: function(cb) {
			            log.printInfo("[compatibility:SSOClient] logout");
			        },
			        addUserLogonEventListener: function(cb) {
			            log.printInfo("[compatibility:SSOClient] addUserLogonEventListener");
			        }
			    }
			},
			
			createVideoMpegObject: function() {
				var object = document.createElement("object");

				var onReadyToPlay;
				var onPlayStateChange;
				var onPlaySpeedChange;
				var onPlayPositionChanged;
                var onPlaybackEndInProgress;
				var playTimeTimer;
				var adTimer;
				var playTime = 0;
				var path;
				var playState;
				var speed;
				var totalTime;
				
				var isStopped = true;
				var changeTime = 0;
				var has_ad = 0;
				
				function is_ad() {
					if(has_ad % 2 == 1) 
						return true;
					return false;
				}
				
				function playing_time_calc(){
					var multiplier = speed && speed > 0 ? speed : 1; 
	            	playTime += multiplier * (1 * 1000);
	            	
	            	changeTime++;
	            	
	            	//bg_player.src = "http://moonish1123.iptime.org/image/player_emul_"+(changeTime%4)+".jpg";
	            	if(playTime > totalTime) {
	            		if(!isStopped) {
	            			clearInterval(playTimeTimer);
	            			clearTimeout(adTimer);
	    					playTimeTimer = null;
	    					isStopped = true;
	    					
	            			bg_player.style.visibility = 'hidden';
	            			onPlayStateChange(KTW.vod.Def.PLAY_STATE_TYPE.FINISHED);
	            		}
	            	}
	            }
				
				var bg_ad = document.createElement("img");
				var bg_player = document.createElement("img");
				
				bg_ad.style.position = "absolute";
				bg_ad.style.visibility = "hidden";
				bg_ad.style.left = "0px";
				bg_ad.style.top = "0px";
				bg_ad.style.width = "1280px";
				bg_ad.style.height = "720px";
				bg_ad.src = "http://cfile5.uf.tistory.com/image/22735D34556D7F9B09186A";
				bg_ad.style.zIndex = 2;
				
				bg_player.style.position = "absolute";
				bg_player.style.visibility = "hidden";
				bg_player.style.left = "0px";
				bg_player.style.top = "0px";
				bg_player.style.width = "1280px";
				bg_player.style.height = "720px";
				bg_player.src = "";
				bg_player.style.zIndex = 2;
					
				document.getElementById('videoBroadcastArea').appendChild(bg_ad);
				document.getElementById('videoBroadcastArea').appendChild(bg_player);
				document.getElementById('videoBroadcastArea').style.zIndex = 0;

				var tmp = {
					get x () {
						log.printDbg("[compatibility:video] get x");
					},
					set x (v) {
						log.printDbg("[compatibility:video] set x");
					},
					get y () {
						log.printDbg("[compatibility:video] get y");
					},
					set y (v) {
						log.printDbg("[compatibility:video] set y");
					},
					get width () {
						log.printDbg("[compatibility:video] get w");
					},
					set width (v) {
						log.printDbg("[compatibility:video] set w");
					},
					get height () {
						log.printDbg("[compatibility:video] get h");
					},
					set height (v) {
						log.printDbg("[compatibility:video] set h");
					},
					setFullScreen: function(v) {
						log.printDbg("[compatibility:video] setFullscreen");
					},
					get data () {
						log.printDbg("[compatibility:video] get data");
						return path;
					},
					set data (v) {
						log.printDbg("[compatibility:video] set data");
						path = v;
						totalTime = parseInt(KTW.vod.VodControl.getContent().runtime) * 60 * 1000;
						has_ad++;
						bg_player.src = KTW.vod.VodControl.getContent().imgUrl;
					},
					get playTime() {
						log.printDbg("[compatibility:video] get playTime");
						return totalTime;
					},
					get playPosition() {
						log.printDbg("[compatibility:video] get playPosition");
						return playTime;
					},
					get speed() {
						log.printDbg("[compatibility:video] get speed");
						return speed;
					},
					get playState() {
						return playState;
						log.printDbg("[compatibility:video] get playState");
					},
					set onPlaySpeedChanged(l) {
						onPlaySpeedChange = l
						log.printDbg("[compatibility:video] set onPlaySpeedChanged");
					},
					set onPlayPositionChanged(l) {
						onPlayPositionChanged = l
						log.printDbg("[compatibility:video] set onPlayPositionChanged");
					},
					set onReadyToPlay(l) {
						onReadyToPlay  = l;
						log.printDbg("[compatibility:video] set onReadyToPlay");
					},
					set onPlayStateChange(l) {
						onPlayStateChange = l;
						log.printDbg("[compatibility:video] set onPlayStateChange");
					},
					set onFullScreenChange(l) {
						log.printDbg("[compatibility:video] set onFullScreenChange");
					},
                    set onPlaybackEndInProgress(l) {
                        onPlaybackEndInProgress = l;
                        log.printDbg("[compatibility:video] set onPlaybackEndInProgress");
                    },
					stop: function() {
						log.printDbg("[compatibility:video] stop");
						
						clearInterval(playTimeTimer);
						clearTimeout(adTimer);
						playTimeTimer = null;
						isStopped = true;
						
						setTimeout(function() {
							bg_player.style.visibility = 'hidden';
							onPlayStateChange(KTW.vod.Def.PLAY_STATE_TYPE.STOP);
						}, 500);
					},
					play: function(playSpeed) {
						log.printDbg("[compatibility:video] play playSpeed:" + playSpeed);
						
						speed = playSpeed;
						
						if(isStopped) {
							//정지 후 재생이니 stop 이나 finish
							isStopped = false;
							
							setTimeout(function(){
								//준비!.
								if(onReadyToPlay) {
									onReadyToPlay(true);
								}
								
								if(onMediaContentChangedListener) {
									//0.5초뒤 광고 시작.
									setTimeout(function() {
										onMediaContentChangedListener(KTW.vod.Def.AD_STATE.AD_START);
										bg_ad.style.visibility = 'visible';
										
										//15초뒤 광고 긑.
										adTimer = setTimeout(function() {
											onMediaContentChangedListener(KTW.vod.Def.AD_STATE.AD_END);
											bg_ad.style.visibility = 'hidden';
											
											//500ms 뒤 본편 시작.
											setTimeout(function() {
												onPlayStateChange(KTW.vod.Def.PLAY_STATE_TYPE.PLAY);
												bg_player.style.visibility = 'visible';
												
												playTime = 0;
												clearInterval(playTimeTimer);
												playTimeTimer = setInterval(playing_time_calc, 1000);
												
												onPlaySpeedChange(speed);
											}, 500);
										}, 1 * 1000);
									}, 500);
								}
							}, 3000);
						} else {
							if(speed == 0) {
								onPlayStateChange(KTW.vod.Def.PLAY_STATE_TYPE.PAUSE);
								clearInterval(playTimeTimer);
							} else if(speed == 1) {
								onPlayStateChange(KTW.vod.Def.PLAY_STATE_TYPE.PLAY);
								clearInterval(playTimeTimer);
								playTimeTimer = setInterval(playing_time_calc, 1000);
							} else {
								clearInterval(playTimeTimer);
								playTimeTimer = setInterval(playing_time_calc, 1000);
							}
							onPlaySpeedChange(speed);
						}
					},
					seek: function(position) {
						log.printDbg("[compatibility:video] seek");
						playTime = position;
						
						clearInterval(playTimeTimer);
						playTimeTimer = setInterval(playing_time_calc, 1000);
											
						onPlayPositionChanged(playTime);
					},
					getComponents: function(type) {
						return JSON.parse('[{"type": 1,"encrypted": "true","encoding": "AC3","language": "KR","audioChannels": "2","strPid": "3166"}, {"type": 1,"encrypted": "true","encoding": "AC3","language": "EN","audioChannels": "2","strPid": "3167"}, {"type": 1,"encrypted": "true","encoding": "AC3","language": "rus","audioChannels": "2","strPid": "3168"}]');
					},
					selectComponent: function(type, component) {
						log.printDbg("[compatibility:video] selectComponent");
					},
					unselectComponent: function(type, component) {
						log.printDbg("[compatibility:video] unselectComponent");
					},
					getCurrentActiveComponents: function(type, component) {
						log.printDbg("[compatibility:video] getCurrentActiveComponents");
					}
				};

				tmp.object = object;

				return tmp;
			},
			
			createMediaTimeLineControl: function() {
				return {
					trickPlayPossible: function() {
						log.printDbg("[compatibility:mediaTimeLineControl] trickPlayPossible");
						return true;
					},
					getAdsInfo: function() {
						log.printDbg("[compatibility:mediaTimeLineControl] getAdsInfo");
						
						return "M02F60J6SGL1500001_K20150626022315.mpg,M64F60BXSGL1500001.mpg[-1*1]";
					},
					set onMediaContentChanged(cb){
						log.printDbg("[compatibility:mediaTimeLineControl] set onMediaContentChanged");
						onMediaContentChangedListener = cb;
					}
				};
			},
			
			createCasAgentObject: function() {
	            return{
	                request: function (aid, tid, tag, msg) {
	                    log.printDbg("[compatibility:CasAgentObject] request");
	                },
	                
	                addCAEventListener: function (evt, cb) {
	                    log.printDbg("[compatibility:CasAgentObject] addCAEventListener");
	                },
	                
	                removeCAEventListener: function (evt, cb) {
	                    log.printDbg("[compatibility:CasAgentObject] removeCAEventListener");
	                },
	                
	                getDeviceID : function() {
	                    log.printDbg("[compatibility:CasAgentObject] getDeviceID");
	                }
	            };
	        },
	        
	        createParentalControl: function() {
	            return{
	                verifyPIN: function (pin) {
	                    log.printDbg("[compatibility:ParentalControl] verifyPIN");
	                },
	                
	                changePIN: function (old_pin, new_pin) {
	                    log.printDbg("[compatibility:ParentalControl] changePIN");
	                },
	                
	                resetPIN: function () {
	                    log.printDbg("[compatibility:ParentalControl] resetPIN");
	                },
	                
	                getParentalRating: function () {
	                    log.printDbg("[compatibility:ParentalControl] getParentalRating");
	                },
	                
	                setParentalRating: function (rate, pin) {
	                    log.printDbg("[compatibility:ParentalControl] setParentalRating");
	                },
	                
	                releaseParentalRating: function (pin) {
	                    log.printDbg("[compatibility:ParentalControl] releaseParentalRating");
	                },
	                initializeLimited: function() {
	                	log.printDbg("[compatibility:ParentalControl] initializeLimited");
	                }
	            };
	        },
	            
	        createSMIManagerObject: function() {
	        	return {
	        		prepare: function(){
	        			log.printDbg("[compatibility:SMI] prepare");
	        		},
	        		play: function(){
	        			log.printDbg("[compatibility:SMI] play");
	        		},
	        		stop: function(){
	        			log.printDbg("[compatibility:SMI] stop");
	        		},
	        		destroy: function(){
	        			log.printDbg("[compatibility:SMI] destroy");
	        		},
	        		setLanguage: function(){
	        			log.printDbg("[compatibility:SMI] setLanguage");
	        		},
	        		getAvailableLanguages: function(){
	        			log.printDbg("[compatibility:SMI] getAvailableLanguages");
	        			return ['kor'];
	        		},
	        		getState: function(){
	        			log.printDbg("[compatibility:SMI] getState");
	        			return 1;
	        		},
	        		resize:function() {
	        			log.printDbg("[compatibility:SMI] resize");
	        		}
	        	}
	        },
			
	        createTVServiceExtensionObject: function() {
	            return {
	                releaseWatchingTimeBlock: function(){
	                    log.printDbg("[compatibility:TVServiceExtension] releaseWatchingTimeBlock");
	                }
	            }
	        },

	        createSpeechRecognizerObject: function() {
	        	return {
	        		start: function() {
	        			log.printDbg("[compatibility:createSpeechRecognizerObject] start");
	        		},
	        		
	        		stop: function() {
	        			log.printDbg("[compatibility:createSpeechRecognizerObject] stop");
	        		}
	        	}
	        },
	        
	        createZipExtractorObject: function() {
	        	return {
	        		requestExtraction: function() {
	        			log.printDbg("[compatibility:createZipExtractorObject] requestExtraction");
	        		},
	        		
	        		cancelRequest: function() {
	        			log.printDbg("[compatibility:createZipExtractorObject] cancelRequest");
	        		}
	        	}
	        },
	        
	        createStorageManagerObject: function() {
                return {
                    get storages() {
                        log.printDbg("[compatibility:createStorageManagerObject] storages");
                        return [];
                    }
                }
            },
            
            createMouseControlObject: function() {
                return {
                    restoreMouseControl: function() {
                        log.printDbg("[compatibility:createMouseControlObject] restoreMouseControl");
                    },
                    disableMouseControl: function() {
                        log.printDbg("[compatibility:createMouseControlObject] disableMouseControl");
                    }
                }
            }
		}//return
	}

	if (!window.oipfObjectFactory) {
		window.oipfObjectFactory = FakeOipfObjectFactory();
		KTW.utils.Log.printInfo("[compatibility] Fake OIPF Loading");
	}

	function FakeACBA(name) {
	    
	    var log = KTW.utils.Log;
	    
	    return {
	        doFactoryReset: function() {
	            log.printDbg("[compatibility:" + name + "] doFactoryReset");
	        },
	        reboot: function() {
	            log.printDbg("[compatibility:" + name + "] reboot");
	        },
	        requestDestroyFullBrowser: function() {
	            log.printDbg("[compatibility:" + name + "] requestDestroyFullBrowser");
	        },
	        getOSDState: function() {
	            log.printDbg("[compatibility:" + name + "] getOSDState");
	        },
	        setVODLoading: function() {
	            log.printDbg("[compatibility:" + name + "] setVODLoading");
	        },
	        setVODPlayRate: function(rate) {
	            log.printDbg("[compatibility:" + name + "] setVODPlayRate");
	        },
	        setVODStarted: function(contsId, title) {
	            log.printDbg("[compatibility:" + name + "] setVODStarted");
	        },
	        setVODEnd: function() {
	            log.printDbg("[compatibility:" + name + "] setVODEnd");
	        }
	    }
	}

	//for VOD Emulating.
	if (!window.MediaExtension) {
	    window.MediaExtension = {
	            COMPONENT_TYPE_VIDEO:0,
	            COMPONENT_TYPE_AUDIO:1
	    };
	    KTW.utils.Log.printWarn("[compatibility: fake MediaExtension] ");
	}

	if (!window.BluetoothDevice) {
	    window.BluetoothDevice = {
	            TYPE_MOUSE: 0,
	            TYPE_HEADPHONE: 1,
	            TYPE_AD2P_SNK_UNKNOWN : 2,
	            TYPE_KEYBOARD: 3
	            
	    };
	    KTW.utils.Log.printWarn("[compatibility: fake BluetoothDevice] ");
	}

	if (!window.BluetoothInterface) {
	    window.BluetoothInterface = {
	            DISCOVERY_STARTED: 0,
	            DISCOVERY_DEVICE_FOUND: 1,
	            DISCOVERY_CANCELED: 2,
	            DISCOVERY_DONE: 3,
	            STATE_STARTED: 4,
	            STATE_COMPLETE: 5,
	            STATE_PIN_FAILED: 6,
	            STATE_FAILED: 7,
	            STATE_PASSCODE_REQUIRED: 8
	    };
	    KTW.utils.Log.printWarn("[compatibility: fake BluetoothInterface] ");
	}

	if (!window.acba) {
	    window.acba = function(name) {
	        return FakeACBA(name);
	    }
	    KTW.utils.Log.printInfo("[compatibility] Fake ACBA Loading");
	}

	if (!window.Channel) {
		window.Channel = {};
		KTW.utils.Log.printInfo("[compatibility] Fake CHANNEL OBJECT");
	}
}


