"use strict";

(function (global) {
    
    var LOG_PREFIX = "oipf Interface";
    
    var oipfFactory = {};
    
    Object.defineProperties(oipfFactory, {    
    	ready:{value:function(){
    		//객체를 미리 생성할 수 있도록.
    		//혹은 초기화 관련 코드를 여기에 넣을 수 있도록 함.
    		oipfFactory.createVideoBroadcastObject();
    		oipfFactory.createConfigurationObject();
    		oipfFactory.createApplicationManagerObject();
        	//oipfFactory.createVideoMpegObject();
    	}},
    	createVideoBroadcastObject:{value:makeSingletonForVbo()},
        createApplicationManagerObject:{value:makeSingleton(KTW.oipf.ApplicationManagerImp)},
        createExtendedChannelDatabase:{value:makeSingleton(KTW.oipf.ExtendedChannelDatabaseImp)},
        createMediaTimeLineControl:{value:function(vmpeg){
        	//이건 singletone 이 아닌것 같은데 음..
        	return Class.validate(new KTW.oipf.MediaTimeLineControlImp(vmpeg));
        }},
        createMouseControlObject:{value:makeSingleton(KTW.oipf.MouseControlImp)},
        createConfigurationObject:{value:makeSingleton(KTW.oipf.ConfigurationImp)},
        createMMIAgentObject:{value:makeSingleton(KTW.oipf.MMIAgentImp)},
        createCasAgentObject:{value:makeSingleton(KTW.oipf.CasAgentImp)},
        createSearchManagerObject:{value:makeSingleton(KTW.oipf.SearchManagerImp)},
        createSMIManagerObject:{value:function(){
        	return Class.validate(new KTW.oipf.SMIManagerImp());
        }},
        createVideoMpegObject:{value:makeSingleton(KTW.oipf.VideoMpegImp, global)},
        createSSOClient:{value:makeSingleton(KTW.oipf.SSOClientImp)},
        createStorageManagerObject:{value:makeSingleton(KTW.oipf.StorageManagerImp)},
        createClosedCaptionPlayer:{value:makeSingleton(KTW.oipf.ClosedCaptionPlayerImp)},
        createClosedCaptionControl:{value:makeSingleton(KTW.oipf.ClosedCaptionControlImp)},
        createTunerAccess:{value:makeSingleton(KTW.oipf.TunerAccessImp)},
        createTransponderControl:{value:makeSingleton(KTW.oipf.TransPonderControlImp)},
        createChannelSelectionEventManager:{value:makeSingleton(KTW.oipf.ChannelSelectionEventManagerImp)},
        createParentalControl:{value:makeSingleton(KTW.oipf.ParentalControlImp)},
        createTVServiceExtensionObject:{value:makeSingleton(KTW.oipf.TVServiceExtensionImp)},
        createSpeechRecognizerObject:{value:makeSingleton(KTW.oipf.SpeechRecognizerImp)},
        createZipExtractorObject:{value:makeSingleton(KTW.oipf.ZipExtractorImp)},
        createSNMPManagerObject:{value:makeSingleton(KTW.oipf.SNMPManagerImp)},  // 2016.08.05 dhlee SNMP 추가
        createAndroidBridgeObject:{value:makeSingleton(KTW.oipf.AndroidBridgeImp)}, // 2016.12.05 dhlee 추가(추후 확인후 필요 없을 수 있음)
        createVODMonitorObject : {value:makeSingleton(KTW.oipf.VODMonitorImp)}, // 2016.12.05 dhlee 추가(R7에 추가됨)
        createEmbeddedSpeakerVolumeControllerObject:{value:makeSingleton(KTW.oipf.EmbeddedSpeakerVolumeControllerImp)}, // 2016.12.05 dhlee UDH3를 위해 추가
        createMediaControlObject : {value:makeSingleton(KTW.oipf.MediaControlImp)}, // 2016.12.05 dhlee 추가 (R7에 추가됨)
        createAudioTtsObject : {value:makeSingleton(KTW.oipf.AudioTtsImp)}  // 2016.12.05 dhlee 추가 (R7에 추가됨)
    });
    
    KTW.oipf.oipfObjectFactory = (function () {
        return oipfFactory;
    }());
    
    var vbo_cache = {};
    function makeSingletonForVbo() {
    	return function(type, pip_mode) {
    		if(!type) {
    			//singleton caching 을 위함.
    			type = KTW.nav.Def.CONTROL.MAIN;
    		}
    		
    		/**
    		 * 혹시나 layer 에서 ID 가 삭제되어 버렸다면 
    		 * div 를 새로 리턴해야 하기 때문에 null 로 초기화.
    		 */
    		if(type == KTW.nav.Def.CONTROL.PIP && vbo_cache[type] && $("#pip_vbo_object").length == 0) {
    			KTW.utils.Log.printWarn("[interface] PIP VBO removed from DOM");
    			vbo_cache[type] = null;
    		}
    		
    		if(type == KTW.nav.Def.CONTROL.STITCHING && vbo_cache[type] && $("#stitching_vbo_object").length == 0) {
    			KTW.utils.Log.printWarn("[interface] STITCHING VBO removed from DOM");
    			vbo_cache[type] = null;
    		}
    		
    		if(!vbo_cache[type]) {
    			vbo_cache[type] = Class.validate(new KTW.oipf.VideoBroadcastImp(type, pip_mode));
    		}
    		return vbo_cache[type].vbo;
    	}
    }
  
    function makeSingleton(Clazz, param1, param2, param3, param4){
		var _instance = null;
		var clazzName = "no named class";
		
		//console.log("makeSingleton:" + clazzName);
		
		return function(){
			if(!_instance){
				//console.log("makeSingleton: first allocation " + clazzName);
				
				_instance = Class.validate(new Clazz(param1, param2, param3, param4));
				if(_instance)
					clazzName = _instance.clazzName || "no named class";
			}
			
			//console.log("makeSingleton: return " + clazzName);
			return _instance;
		}
	}
    
    KTW.oipf.oipfTest = function(){
    	window.configurationObject = KTW.oipf.oipfObjectFactory.createConfigurationObject();
    	window.casAgentObject = KTW.oipf.oipfObjectFactory.createCasAgentObject();
    	window.searchManagerObject = KTW.oipf.oipfObjectFactory.createSearchManagerObject();
    	window.smiManagerObject = KTW.oipf.oipfObjectFactory.createSMIManagerObject();
    	window.mmiAgentObject = KTW.oipf.oipfObjectFactory.createMMIAgentObject();
    	window.mouseControl = KTW.oipf.oipfObjectFactory.createMouseControlObject();
    	window.ssoManagerObject = KTW.oipf.oipfObjectFactory.createSSOClient();
    	window.videoMpegObject = KTW.oipf.oipfObjectFactory.createVideoMpegObject();
    	window.mediaTimeLineControlObject = KTW.oipf.oipfObjectFactory.createMediaTimeLineControl(window.videoMpegObject);
    	window.channelSelectionEventManagerObject = KTW.oipf.oipfObjectFactory.createChannelSelectionEventManager();
    	window.transponder = KTW.oipf.oipfObjectFactory.createTransponderControl();
    	window.tunner = KTW.oipf.oipfObjectFactory.createTunerAccess();
    	window.closedCaptionPlayer = KTW.oipf.oipfObjectFactory.createClosedCaptionPlayer();
    	window.closedCaptionControl = KTW.oipf.oipfObjectFactory.createClosedCaptionControl();
    	window.storageManagerObject = KTW.oipf.oipfObjectFactory.createStorageManagerObject();
    	window.extendedChannelDB = KTW.oipf.oipfObjectFactory.createExtendedChannelDatabase();
    	window.appManagerObj = KTW.oipf.oipfObjectFactory.createApplicationManagerObject();
    	window.videoBroadcastObj = KTW.oipf.oipfObjectFactory.createVideoBroadcastObject();
    	window.parentalControl = KTW.oipf.oipfObjectFactory.createParentalControl();
    	window.tvServiceExtension = KTW.oipf.oipfObjectFactory.createTVServiceExtensionObject();
    	
    	//ConfigurationObject Test.
    	configurationObject.configuration.setText("test", "val");
    	configurationObject.configuration.getText("test");
    	console.log("preferredAudioLanguage:"+configurationObject.configuration.preferredAudioLanguage);

    	console.log("volumn:"+configurationObject.localSystem.volumn);
    	console.log("mute:"+configurationObject.localSystem.mute);
    	console.log("outputs:"+configurationObject.localSystem.outputs);
    	console.log("networkInterfaces:"+configurationObject.localSystem.networkInterfaces);
    	
    	console.log("reminderTime:"+configurationObject.localSystem.reminderTime);
    	configurationObject.localSystem.reminderTime = 100;
    	console.log("reminderTime:"+configurationObject.localSystem.reminderTime);
    	
    	console.log("powerState:"+configurationObject.localSystem.powerState);
    	
    	configurationObject.localSystem.setScreenSize(100,100);
    	configurationObject.localSystem.setPowerState(5);
    	configurationObject.localSystem.setFrontPanetText("setFrontPanetText Test");
    	configurationObject.localSystem.addEventListener("event_test", function(){ return "callback done" });
    	
    	//CAS Agent test
    	casAgentObject.request(100,200,"tag","hello");
    	casAgentObject.addCAEventListener("success_event", function(){return "success cb";});
    	casAgentObject.removeCAEventListener("success_event", null);
    	
    	//Search Manager
    	searchManagerObject.onMetadataSearch = new function(){return "metaDataSearch callback"};
    	var metadataSearch = searchManagerObject.createSearch(1);
    	metadataSearch.setQuery(new KTW.oipf.Query());
    	metadataSearch.orderBy();
    	metadataSearch.createQuery(1,2,3);
    	metadataSearch.addChannelConstraint("constraint channel is what ?");
    	console.dir(metadataSearch.result);
    	console.dir(searchManagerObject);
    	
    	//SMI Manager Call. ?�인.
    	
    	//MMI  call ?�인
    	mmiAgentObject.addMMIEventListener("evt", new function(){return "callback";});
    	mmiAgentObject.removeMMIEventListener("evt", null);
    	//mounse control call ?�인
    	mouseControl.disableMouseControl();
    	mouseControl.restoreMouseControl();
    	//sso manager call ?�인
    	ssoManagerObject.terminalLogon("11");
    	ssoManagerObject.getPINNumber();
    	ssoManagerObject.getSTBToken();
    	ssoManagerObject.getHDSToken();
    	ssoManagerObject.logout();
    	ssoManagerObject.addUserLogonEventListener(new function(){});
    	
    	//videoObject call test. 
    	videoMpegObject.x = 1;
    	videoMpegObject.y = 1;
    	videoMpegObject.width = KTW.CONSTANT.RESOLUTION.WIDTH;
    	videoMpegObject.height = KTW.CONSTANT.RESOLUTION.HEIGHT;
    	videoMpegObject.playState = "none";
    	videoMpegObject.data = "cirtsp://10.12.0.51/MG4F8N5FSGL0800001_K20130822145032.mpg;;@200";
    	
    	console.log("videoMpegObject[" + videoMpegObject.x + "," + videoMpegObject.y + "," + videoMpegObject.width + "," + videoMpegObject.height + "," + videoMpegObject.playState + "," + videoMpegObject.data);
    	
    	videoMpegObject.setPlaySpeed(2);
    	console.log("speed:"+videoMpegObject.speed);
    	console.log("playTime:"+videoMpegObject.playTime);
    	console.log("playPosition:"+videoMpegObject.playPosition);
    	
    	videoMpegObject.setFullScreen(false);
    	
    	videoMpegObject.play("ss");
    	videoMpegObject.stop();
    	videoMpegObject.pause();
    	videoMpegObject.resume();
    	videoMpegObject.seek(100);
    	videoMpegObject.selectComponent([100,200]);
    	
    	//channelSelectionEventManagerObject
    	channelSelectionEventManagerObject.addChannelSelectionEventListener(function(){});
    	
    	//transponder
    	transponder.getCollectibleList();
    	transponder.collectOneTransponder("what",11);
    	transponder.tune("11");
    	
    	//tunner access
    	tunner.setTSInfo(1, {}, true);
    	tunner.getTSInfos();
    	tunner.getAdditionalTunerInfos();
    	tunner.getLNB(10);
    	tunner.setLNB(10, [1,2,3]);
    	
    	//closed caption
    	closedCaptionPlayer.getClosedCaptionInfoList();
    	closedCaptionControl.getClosedCaptionInfo();
    	
    	//storage manager
    	console.log("storages:"+storageManagerObject.storages);
    	
    	//..extendedChannelDB
    	extendedChannelDB.updateDIDatabase();
    	extendedChannelDB.requestFirmwareDownload();
    	extendedChannelDB.getSameKTChannel("channel");
    	extendedChannelDB.isKidsCareChannel("channel");
    	console.log("promoChannel:"+extendedChannelDB.promoChannel);
    	console.log("skyPlusChannel:"+extendedChannelDB.skyPlusChannel);
    	
    	//App manager. 
    	console.log("findApplications:"+appManagerObj.findApplications("dvb.appId", '100293'));
    	var self = appManagerObj.getOwnerApplication( window.document );
    	console.dir(self);
    	self.activateInput(true, 0, 0);
    	var keyset = self.privateData.keyset;
    	keyset.setValue(keyset.maximumValue, [38,40,412,417,406,403]);
    	
    	//VideoBroadcastObject
    	videoBroadcastObj.height = KTW.CONSTANT.RESOLUTION.HEIGHT;
    	videoBroadcastObj.width = KTW.CONSTANT.RESOLUTION.WIDTH;
    	videoBroadcastObj.x = 1;
    	videoBroadcastObj.y = 1;
    	console.dir(videoBroadcastObj.x + "," + videoBroadcastObj.y + "," + videoBroadcastObj.width + "," + videoBroadcastObj.height);
    	videoBroadcastObj.setFullScreen("?");
    	videoBroadcastObj.enableAudio();
    	videoBroadcastObj.disableAudio();
    	videoBroadcastObj.addEventListener("d","");
    	videoBroadcastObj.removeEventListener("d","");
    	videoBroadcastObj.bindToCurrentChannel("channel");
    	//videoBroadcastObj.setChannel(1,2,3);
    	videoBroadcastObj.stop();
    	videoBroadcastObj.addObjectChangeEventListener();
    	var channelConfig = videoBroadcastObj.getChannelConfig();
    	
    	// Parental Control
    	console.log("pin check = " + parentalControl.verifyPIN("0000"));
    	
    	syncGet();
    	
    };    
}(window));
