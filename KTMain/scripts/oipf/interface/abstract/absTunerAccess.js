"use strict";

/**
 * SS SmartTV 에선 필요 없어서 abstract method 로 지정하지 않음
 * 다만 WEBHomeportal 에선 사용하게 됨으로 Imp 부는 구현해야함.
 */
KTW.oipf.abs.AbsTunerAccess = Class.extend({
	construct : function construct() {
		this.clazzName = "AbsTunerAccess";
	},
	
	setTSInfo: Class.absFunction(function(index, info, flag){
	}),
	
	getTSInfos : Class.absFunction(function(){
	}),
	
	getAdditionalTunerInfos : Class.absFunction(function(){
	}),
	
	getLNB: Class.absFunction(function(port){
	}),
	
    setLNB: Class.absFunction(function(port, value/*array*/){
	}),
});