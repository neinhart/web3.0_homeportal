"use strict";

/**
 * Abstract Class 정의
 * 
 * Abstract method 는 Class.absFunction 을 통해서 실행해야 한다. 
 * 
 */
KTW.oipf.abs.AbsExtendedChannelDatabase = Class.extend({
	construct : function() {
		this.clazzName = "AbsExtendedChannelDatabase";
	},
	
	abstractGetter: ['skyPlusChannel', 'promoChannel'],
	
	updateDIDatabase: Class.absFunction(function(){
		
	}),
	
	getSameKTChannel: Class.absFunction(function(currentChannel){
		
	}),
	
	isKidsCareChannel: Class.absFunction(function(skyVideoChannel){
		
	}),
	
	requestFirmwareDownload: Class.absFunction(function(){
		
	}),
});
