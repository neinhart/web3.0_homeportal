"use strict";

/**
 * SS SmartTV 에선 필요 없어서 abstract method 로 지정하지 않음
 * 다만 WEBHomeportal 에선 사용하게 됨으로 Imp 부는 구현해야함.
 */
KTW.oipf.abs.AbsVideoMpeg = Class.extend({
	construct : function construct(global) {
		this.clazzName = "AbsVideoMpeg";
		
	},
	
	abstractSetter:['x','y','width','height', 'data', 'onPlaySpeedChanged' , 'onPlayPositionChanged', 'onReadyToPlay', 'onPlayStateChange', 'onFullScreenChange'],     
	abstractGetter:['x','y','width','height','playState','data','playTime','speed', 'playPosition', 'playState'],
	
    setFullScreen: Class.absFunction(function (b) {}),
    play: Class.absFunction(function (s) {}),
    stop: Class.absFunction(function () {}),
    seek: Class.absFunction(function (pos) {}),
    getComponents: Class.absFunction(function (a,b) {}),
    selectComponent: Class.absFunction(function (a,b) {}),
    unselectComponent: Class.absFunction(function (a,b) {}),
    getCurrentActiveComponents: Class.absFunction(function (a,b) {}),
});