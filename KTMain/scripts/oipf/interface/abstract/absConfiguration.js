"use strict";

/**
 * Abstract Class 정의
 * 
 * Abstract method 는 Class.absFunction 을 통해서 실행해야 한다. 
 * 
 */

KTW.oipf.abs.AbsConfiguration = Class.extend({
	construct : function() {
		this.clazzName = "AbsConfiguration";
	},
	
	abstractGetter: ['configuration', 'localSystem'],
});

KTW.oipf.abs.AbsInnerConfiguration = Class.extend({
	construct : function() {
		this.clazzName = "AbsInnerConfiguration";
	},
	
	abstractGetter: ['preferredAudioLanguage', 'preferredSubtitleLanguage'],
	abstractSetter: ['preferredAudioLanguage', 'preferredSubtitleLanguage'],
	
	getText: Class.absFunction(function(k){
		
	}),
	
	setText: Class.absFunction(function(k, v){
		
	}),
});

KTW.oipf.abs.AbsLocalSystem = Class.extend({
	construct : function() {
		this.clazzName = "AbsLocalSystem";
	},
	
	abstractGetter: ['volumn', 'mute', 'outputs', 'networkInterfaces', 'reminderTime', 'tuners', 'modelName'],
	abstractSetter: ['mute', 'reminderTime'],
	
    setScreenSize: Class.absFunction(function (w, h) {}),
    setPowerState: Class.absFunction(function (type) {}),
    setFrontPanetText: Class.absFunction(function (txt) {}),
    addEventListener: Class.absFunction(function (evt, hdlr) {}),
    removeEventListener: Class.absFunction(function (evt) {}),
});
