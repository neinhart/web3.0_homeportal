"use strict";

KTW.oipf.abs.AbsTVServiceExtension = Class.extend({
    construct : function construct() {
        this.clazzName = "AbsTVServiceExtension";
    },
	
    hasTVServiceExtension: Class.absFunction(function () {}),
    releaseWatchingTimeBlock: Class.absFunction(function () {}),
});