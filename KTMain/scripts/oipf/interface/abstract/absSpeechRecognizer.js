"use strict";

/**
 * SS SmartTV 에선 필요 없어서 abstract method 로 지정하지 않음
 * 다만 WEBHomeportal 에선 사용하게 됨으로 Imp 부는 구현해야함.
 */
KTW.oipf.abs.AbsSpeechRecognizer = Class.extend({
	construct : function construct(global) {
		this.clazzName = "AbsSpeechRecognizer";
	},
	
	abstractSetter:['onended','onrecognized'],     
	//abstractGetter:['onended','onrecognized'],
	
    start: Class.absFunction(function (select_list) {}),
    stop: Class.absFunction(function () {}),
});