"use strict";

KTW.oipf.abs.AbsSMIManager = Class.extend({
	construct : function construct(search_target){
		this.clazzName = "AbsSMIManager";
	},
		
	abstractGetter : ['STATE'],
	
	prepare: Class.absFunction(function (url, element) {}),    
    play: Class.absFunction(function (vmo) {}),
    stop: Class.absFunction(function () {}),    
    destroy: Class.absFunction(function () {}),    
    setLanguage: Class.absFunction(function(index) {}),    
    getAvailableLanguages: Class.absFunction(function() {}),
    getState: Class.absFunction(function() {}),
});