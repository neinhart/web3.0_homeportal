"use strict";

KTW.oipf.abs.AbsApplicationInfo = Class.extend({
	construct : function construct(doc){
		this.clazzName = "AbsApplicationInfo";
	},
	
	abstractGetter: ['privateData'],
	
	findApplications: function (key, id) {},
    
    getOwnerApplication: function () {},
    
    getChildApplications: function (val) {},
});