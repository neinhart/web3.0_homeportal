"use strict";

KTW.oipf.abs.AbsVideoBroadcast = Class.extend({
	construct: function construct(global){
		this.clazzName = "AbsVideoBroadcast";
	},
	
	abstractGetter: ['x','y','width','height'],
	abstractSetter: ['x','y','width','height'],
	
	setFullScreen: Class.absFunction(function(b){}),
	enableAudio: Class.absFunction(function(){}),
	
	addEventListener: Class.absFunction(function(evt, cb){
		
	}),
	
	removeEventListener: Class.absFunction(function(evt, cb){ 
		
	}),
	
	bindToCurrentChannel: Class.absFunction(function(){
		
	}),
	
	setChannel: Class.absFunction(function(ch, isblockSelection, isblockOSD){
		
	}),
	
	stop: Class.absFunction(function(){
		
	}),
	
	disableAudio: Class.absFunction(function(){
		
	}),
	
	addObjectChangeEventListener: Class.absFunction(function(evt, cb){
		
	}),
	
	getChannelConfig: Class.absFunction(function(){
		
	}),
	selectComponent: Class.absFunction(function (selectComponent, device) {}),
    getComponents: Class.absFunction(function (s) {}),
});


