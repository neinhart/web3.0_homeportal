/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>absAndroidBridge.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.abs.AbsAndroidBridge = Class.extend({
    construct: function construct() {
        this.clazzName = "AbsAndroidBridge";
    },
    getAndroidBridge: Class.absFunction(function () {
    }),
    launchApplication: Class.absFunction(function (packageName) {
    }),
    getState: Class.absFunction(function () {
    }),
    getAudioOutputDevice: Class.absFunction(function () {
    }),
    setAudioOutputDevice: Class.absFunction(function (device) {
    }),
    setStateChangedListener: Class.absFunction(function (listener) {
    })
});