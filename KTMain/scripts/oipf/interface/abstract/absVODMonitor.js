/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>absVODMonitor.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.abs.AbsVODMonitor = Class.extend({
    construct : function construct() {
        this.clazzName = "AbsVODMonitor";
    },
    abstractSetter: ['onVODStateChanged']
});