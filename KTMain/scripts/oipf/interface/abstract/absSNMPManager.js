"use strict";

KTW.oipf.abs.AbsSNMPManager = Class.extend({
    construct : function construct() {
        this.clazzName = "AbsSNMPManager";
    },
    
    notifySNMPError: Class.absFunction(function (errorCode) {}),
});