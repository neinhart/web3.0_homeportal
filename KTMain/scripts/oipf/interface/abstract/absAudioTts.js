/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>absAudioTts.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.abs.AbsAudioTts = Class.extend({
    construct: function construct() {
        this.clazzName = "AbsAudioTts";
    },
    getAudioTts: Class.absFunction(function () {
    }),

    startSpeech: Class.absFunction(function (text, voice) {
    }),
    stopSpeech: Class.absFunction(function (id) {
    }),
    startSynthesize: Class.absFunction(function (text, format, voice) {
    }),
    stopSynthesize: Class.absFunction(function (id) {
    }),
    setVolume: Class.absFunction(function (volume) {
    }),
    getVolume: Class.absFunction(function () {
    }),
    addSpeechStateChangeListener: Class.absFunction(function (listener) {
    }),
    addSynthesizeStateChangeListener: Class.absFunction(function (listener) {
    })
});