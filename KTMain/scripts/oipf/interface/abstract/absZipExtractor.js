"use strict";

/**

 */
KTW.oipf.abs.AbsZipExtractor = Class.extend({
	construct : function construct(global) {
		this.clazzName = "AbsZipExtractor";
		
	},
	
	//abstractSetter:[],     
	//abstractGetter:[],
	
	requestExtraction: Class.absFunction(function (path, dest_dir, callback) {}),
	cancelRequest: Class.absFunction(function (handle) {}),
});