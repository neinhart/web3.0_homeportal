"use strict";

/**
 * Abstract Class 정의
 * 
 * Abstract method 는 Class.absFunction 을 통해서 실행해야 한다. 
 * 
 */

KTW.oipf.abs.AbsCasAgent = Class.extend({
	construct : function() {
		this.clazzName = "AbsCasAgent";

	},
	
	request:Class.absFunction(function(aid, tid, tag, msg){
		
	}),
	
	addCAEventListener:Class.absFunction(function(evt, cb){
		
	}),
	
	removeCAEventListener:Class.absFunction(function(evt, cb){
		
	}),	
});
