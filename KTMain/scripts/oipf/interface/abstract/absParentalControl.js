"use strict";

/**
 * Abstract Class 정의
 * 
 * Abstract method 는 Class.absFunction 을 통해서 실행해야 한다. 
 * 
 */

KTW.oipf.abs.AbsParentalControl = Class.extend({
	construct : function() {
		this.clazzName = "AbsParentalControl";

	},
	
	verifyPIN:Class.absFunction(function(pin) {
        
    }),
    
    changePIN:Class.absFunction(function(old_pin, new_pin) {
        
    }),
    
    resetPIN:Class.absFunction(function() {
        
    }),
    
    getParentalRating:Class.absFunction(function() {
        
    }),
    
    setParentalRating:Class.absFunction(function(rate, pin) {
        
    }),
    
    releaseParentalRating:Class.absFunction(function(pin) {
        
    }),
    
    initializeLimited:Class.absFunction(function() {
        
    }),
});
