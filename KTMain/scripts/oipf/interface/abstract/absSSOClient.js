"use strict";

/**
 * SS SmartTV 에선 필요 없어서 abstract method 로 지정하지 않음
 * 다만 WEBHomeportal 에선 사용하게 됨으로 Imp 부는 구현해야함.
 */
KTW.oipf.abs.AbsSSOClient = Class.extend({
	construct : function construct() {
		this.clazzName = "AbsSSOClient";
		
	},	
	terminalLogon: Class.absFunction(function(service_id){}),	
	logout: Class.absFunction(function(){}),	
	getPINNumber : Class.absFunction(function(){}),	
	getSTBToken : Class.absFunction(function(){}),	
	getHDSToken : Class.absFunction(function(){}),	
	addUserLogonEventListener : Class.absFunction(function(callback){}),
});
