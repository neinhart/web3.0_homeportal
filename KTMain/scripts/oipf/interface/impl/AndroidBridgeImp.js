/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AndroidBridgeImp.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.AndroidBridgeImp = KTW.oipf.abs.AbsAndroidBridge.extend({

    construct: function construct() {
        construct.$.construct.call(this);
        this.clazzName = "AndroidBridgeImp";

        Class.definePrivate(this, (function () {
            //private.
            var androidBridge = oipfObjectFactory.createAndroidBridgeObject();

            return {
                get androidBridge() {
                    return androidBridge;
                }
            };
        }()));
    },
    getAndroidBridge : function(){
        return this._p.androidBridge;
    },
    launchApplication: function (packageName) {
        console.log("[oipf AndroidBridgeImpl] launchApplication packageName:" + packageName);
        return this._p.androidBridge.launchApplication(packageName);
    },
    getState: function () {
        console.log("[oipf AndroidBridgeImpl] getState");
        return this._p.androidBridge.getState();
    },
    getAudioOutputDevice: function () {
        console.log("[oipf AndroidBridgeImpl] getAudioOutputDevice");
        return this._p.androidBridge.getAudioOutputDevice();
    },
    setAudioOutputDevice: function (device) {
        console.log("[oipf AndroidBridgeImpl] setAudioOutputDevice device:" + device);
        this._p.androidBridge.setAudioOutputDevice(device);
    },
    setStateChangedListener : function( listener ){
        console.log("[oipf AndroidBridgeImpl] setStateChangedListener");
        this._p.androidBridge.onstatechanged = listener;
    }
});