"use strict";
KTW.oipf.SpeechRecognizerImp = KTW.oipf.abs.AbsSpeechRecognizer.extend({	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "SpeechRecognizerImp";
		
        Class.definePrivate(this, (function(){
            //private. 
        	var speechRecognizer = null;
        	try {
        		speechRecognizer = oipfObjectFactory.createSpeechRecognizerObject();
        	} catch (e) {
        		KTW.utils.Log.printExec(e);
        	}
                        
            function start(mulit_result) {
            	if(speechRecognizer)
            		speechRecognizer.start(mulit_result);
            }
            
            function stop() {
            	if(speechRecognizer)
            		speechRecognizer.stop();
            }
            
            return{
            	start:start,
            	stop:stop,
            	
            	set onended(v) {
            		console.log("[oipf speechRecognizer] set onended");
            		
            		if(speechRecognizer)
            			speechRecognizer.onended = v;
            	},
            	set onrecognized(v) {
            		console.log("[oipf speechRecognizer] set onrecognized");
            		
            		if(speechRecognizer)
            			speechRecognizer.onrecognized = v;
            	},

                get MODE_PIN () {
                    return speechRecognizer.MODE_PIN;
                },
                get MODE_KEYWORD () {
                    return speechRecognizer.MODE_KEYWORD;
                }
            };
        }()));
	},
	
    start: function(mulit_result){
		console.log("[oipf speechRecognizer] start mulit_result:" + mulit_result);
		this._p.start(mulit_result);
	},
	stop: function(){
		console.log("[oipf speechRecognizer] start");
		this._p.stop();
	},
	set onended(v) {
		this._p.onended = v;
	},
	set onrecognized(v) {
		this._p.onrecognized = v;
	},

    get MODE_PIN () {
        return this._p.MODE_PIN;
    },
    get MODE_KEYWORD () {
        return this._p.MODE_KEYWORD;
    }
});