"use strict";

KTW.oipf.SNMPManagerImp = KTW.oipf.abs.AbsSNMPManager.extend({
    
    construct : function construct() {
        construct.$.construct.call(this);
        this.clazzName = "SNMPManagerImp";
        
        Class.definePrivate(this, (function(){
            //private.
            var SNMPManager = null;
            try {
                SNMPManager = oipfObjectFactory.createSNMPManagerObject();
            } catch (e) {
                KTW.utils.Log.printExec(e);
            }

            function notifySNMPError(errorCode) {
                if(SNMPManager) {
                    SNMPManager.notifySNMPError(errorCode);
                }   
            }
            
            return{
                notifySNMPError:notifySNMPError
            };
        }()));
    },

    notifySNMPError: function(errorCode){
        console.log("[oipf SNMPManagerImp] notifySNMPError errorCode:" + errorCode);
        return this._p.notifySNMPError(errorCode);
    }
});