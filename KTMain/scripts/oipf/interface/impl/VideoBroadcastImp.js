"use strict";
KTW.oipf.VideoBroadcastImp = KTW.oipf.abs.AbsVideoBroadcast.extend({
	construct: function construct(type, pip_mode) {
		construct.$.construct.call(this);
		this.clazzName = "AbsVideoBroadcast";
		
		Class.definePrivate(this, (function(type, pip_mode) {
			var _vbo = null;
			type = type ? type : KTW.nav.Def.CONTROL.MAIN;
			
			function makeParam(obj, name, value) {
				var param = document.createElement("param");
				param.setAttribute("name", name);
				param.setAttribute("value", value); 
				obj.appendChild(param);
			};
			
			function setParentElement(id) {
				if (type !== KTW.nav.Def.CONTROL.MAIN) {
					try {
					    document.getElementById(id).appendChild(_vbo);
					} catch (e) {
						KTW.utils.Log.printErr("PIP Video Layer is not defined, Add to Main VBO Layer.\n Error:" + e);
					}
				}
			};
			
			function removeParams() {
				var param_elements = _vbo.getElementsByTagName("param");
				param_elements = param_elements.length ? param_elements : [param_elements];
				
				//뒤에서 부터.
				for(var i = param_elements.length-1; i >= 0; i--) {
					if (param_elements[i].name === 'channelControl') {
						param_elements[i].remove();
					}
				}
			};
			
			function changeMode(mode) {
				if (type === KTW.nav.Def.CONTROL.PIP) {
					removeParams();
					
					switch(mode) {
						case KTW.nav.Def.PIP_MODE.MULTI_VIEW:
							makeParam(_vbo, "channelControl", "true");
							break;
						case KTW.nav.Def.PIP_MODE.MINI_EPG:
						case KTW.nav.Def.PIP_MODE.FULL_EPG:
						case KTW.nav.Def.PIP_MODE.FAV_EPG:
						case KTW.nav.Def.PIP_MODE.RESERVATION:
							makeParam(_vbo, "channelControl", "false");
							break;
					}
				}
			}
			
			function getVideoBroadcastObject() {
				if (!_vbo) {
					_vbo = oipfObjectFactory.createVideoBroadcastObject();
					_vbo.style.visibility = "hidden";
					//interface 추가.
					_vbo.setParentElement = setParentElement;
					_vbo.changeMode = changeMode;
					
					if (type === KTW.nav.Def.CONTROL.PIP) {
						KTW.utils.Log.printWarn("CREATE PIP");

                        //_vbo.style.visibility = "hidden";
						_vbo.id = 'pip_vbo_object';
						makeParam(_vbo, "zindex", "1");
						makeParam(_vbo, "pip", "true");
						makeParam(_vbo, "changeMainService", "false");
						makeParam(_vbo, "videoPlaneId", 1);
						changeMode(pip_mode);
					} else if (type === KTW.nav.Def.CONTROL.STITCHING) {
						KTW.utils.Log.printWarn("CREATE STITCHING");

                        //_vbo.style.visibility = "hidden";
						_vbo.id = 'stitching_vbo_object';
						_vbo.style.position = "absolute";
						_vbo.style.left = "0px";
						_vbo.style.top = "0px";
						_vbo.style.width = "1920px";
						_vbo.style.height = "1080px";

						makeParam(_vbo, "zindex", "1");
						makeParam(_vbo, "pip", "true");
						makeParam(_vbo, "audio", "true");
						makeParam(_vbo, "changeMainService", "false");
						makeParam(_vbo, "videoPlaneId", 0);
						
						_vbo.x = 0;
						_vbo.y = 0;
						_vbo.width = KTW.CONSTANT.RESOLUTION.WIDTH;
						_vbo.height = KTW.CONSTANT.RESOLUTION.HEIGHT;
						
						//API 추가.
						_vbo.setVideoSize = function(x,y,w,h) {
							KTW.utils.Log.printDbg("_vbo.setVideoSize x:" + x + " y:" + y + " w:" + w + " h:" + h);
							_vbo.style.left = x+"px";
							_vbo.style.top = y+"px";
							_vbo.style.width = w+"px";
							_vbo.style.height = h+"px";
							
							_vbo.x = x;
							_vbo.y = y;
							_vbo.width = w;
							_vbo.height = h;
						};
					} else if (type === KTW.nav.Def.CONTROL.MAIN) {
						KTW.utils.Log.printWarn("CREATE MAIN");
						_vbo.id = 'videoBroadcastObj';
						_vbo.style.position = "absolute";
						_vbo.style.left = "0px";
						_vbo.style.top = "0px";
						_vbo.style.width = "1920px";
						_vbo.style.height = "1080px";
						
						makeParam(_vbo, "zindex", "0");
						makeParam(_vbo, "audio", "true");
						
						_vbo.x = 0;
						_vbo.y = 0;
						_vbo.width = KTW.CONSTANT.RESOLUTION.WIDTH;
						_vbo.height = KTW.CONSTANT.RESOLUTION.HEIGHT;
						
						document.getElementById('mainVBOArea').appendChild(_vbo);
					}
				}
				
				return _vbo;
			};
			return getVideoBroadcastObject();
		}(type, pip_mode)));
	},
	get x() { 
	}, 
	set x(v) { 
	},
    get y() { 
	}, 
    set y(v) { 
    },
    get width() { 
    }, 
    set width(v) { 
    },
    get height() {
    }, 
    set height(v) {  
    },	    
    get currentChannel () {
    },    
    get programmes () {
    },    
    getComponents: function(type) { //check
    },
    getCurrentActiveComponents : function (type, device) {  //check
    },
    selectComponent : function(component, device) {
    },
    unselectComponent : function(component, device) {
    },        
    setFullScreen: function (b) {
    },
    enableAudio: function () {
    },  
    disableAudio: function () {
    },    
    setChannel: function (ch, isBlockSelection, isBlockOSD) {
    },
    bindToCurrentChannel: function () {
    },
    stop: function () {
    },    
    getPMTDescriptors: function(val) {
    },
    getChannelConfig: function () {
    },    
    addEventListener: function (evt, cb) {
    },
    removeEventListener: function (evt, cb) {
    },
    addObjectChangeEventListener: function (evt, cb) {
    },    
    removeReminder: function(program) {
    },
    setReminder: function(program) {
    },
    get reminders () {
    },
    get vbo () {
    	return this._p;
    }
});
