"use strict";

KTW.oipf.ParentalControlImp = KTW.oipf.abs.AbsParentalControl.extend({
    
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "ParentalControlImp";
		
		
		Class.definePrivate(this, (function(){
            //private. 
		    var parental_control = oipfObjectFactory.createParentalControl();
            
            return{
                get parentalControl(){
                    return parental_control;
                },
            };
        }()));
	},
	
	verifyPIN: function (pin) {
        KTW.utils.Log.printDbg("[oipf ParentalControl] verifyPIN()");
        return this._p.parentalControl.verifyPIN(pin);
    },
    
    changePIN: function (old_pin, new_pin) {
        KTW.utils.Log.printDbg("[oipf ParentalControl] changePIN()");
        return this._p.parentalControl.changePIN(old_pin, new_pin);
    },
    
    resetPIN: function () {
        KTW.utils.Log.printDbg("[oipf ParentalControl] resetPIN()");
        return this._p.parentalControl.resetPIN();
    },
    
    getParentalRating: function () {
        KTW.utils.Log.printDbg("[oipf ParentalControl] getParentalRating()");
        return this._p.parentalControl.getParentalRating();
    },
    
    setParentalRating: function (rate, pin) {
        KTW.utils.Log.printDbg("[oipf ParentalControl] setParentalRating() " + rate);
        this._p.parentalControl.setParentalRating(rate, pin);
    },
    
    releaseParentalRating: function (pin) {
        KTW.utils.Log.printDbg("[oipf ParentalControl] releaseParentalRating()");
        return this._p.parentalControl.releaseParentalRating(pin);
    },
    
    initializeLimited: function() {
    	KTW.utils.Log.printDbg("[oipf ParentalControl] initializeLimited()");
        return this._p.parentalControl.initializeLimited();
    }
});