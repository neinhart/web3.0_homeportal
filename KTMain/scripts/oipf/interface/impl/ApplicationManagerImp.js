"use strict";

KTW.oipf.ApplicationManagerImp = KTW.oipf.abs.AbsApplicationInfo.extend({
	construct : function construct(doc){
		construct.$.construct.call(this, doc);
		this.clazzName = "ApplicationManagerImp";

		Class.definePrivate(this, (function(_configuration){
			var applicationManager = oipfObjectFactory.createApplicationManagerObject();
			
			return {
				get appMgr() {
					return applicationManager;
				}
			}
		}()));
	},
	
	findApplications: function (key, id) {
		console.log("[oipf appMgr] findApplications() key:" + key + " id:" + id);
		return this._p.appMgr.findApplications(key, id);
    },
    
    getOwnerApplication: function () {
    	console.log("[oipf appMgr] getOwnerApplication()");
    	return this._p.appMgr.getOwnerApplication();
    },
    
    getChildApplications: function (val) {
    	console.log("[oipf appMgr] getChildApplications() doc:" + val);
    	return this._p.appMgr.getChildApplications(val);
    },
    
    get privateData() {
    	return this._p.appMgr.privateData;
    },
    
    get onApplicationUnloaded() {
    	return this._p.appMgr.onApplicationUnloaded;
    },
    
    set onApplicationUnloaded(v) {
    	this._p.appMgr.onApplicationUnloaded = v;
    },
    
    get onApplicationLoadError() {
    	return this._p.appMgr.onApplicationLoadError;
    },
    
    set onApplicationLoadError(v) {
    	this._p.appMgr.onApplicationLoadError = v;
    },
    
    get onApplicationLoaded() {
    	return this._p.appMgr.onApplicationLoaded;
    },
    
    set onApplicationLoaded(v) {
    	this._p.appMgr.onApplicationLoaded = v;
    },
});