"use strict";

KTW.oipf.TVServiceExtensionImp = KTW.oipf.abs.AbsTVServiceExtension.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "TVServiceExtensionImp";
		
		Class.definePrivate(this, (function(){
            //private.
			var tvServiceExtension = null;
			try {
				tvServiceExtension = oipfObjectFactory.createTVServiceExtensionObject();
			} catch (e) {
				KTW.utils.Log.printExec(e);
			}

            return{
                get tvServiceExtension(){
                    return tvServiceExtension;
                },
            };
        }()));
	},

	hasTVServiceExtension: function() {
		return (this._p.tvServiceExtension !== null);
	},
	
	releaseWatchingTimeBlock: function () {
	    console.log("[oipf TVServiceExtension] releaseWatchingTimeBlock()");

		if(this._p.tvServiceExtension) {
			this._p.tvServiceExtension.releaseWatchingTimeBlock();
		}
	},
});