"use strict";

KTW.oipf.MediaTimeLineControlImp = KTW.oipf.abs.AbsMediaTimeLineControl.extend({
	construct : function construct(vmpeg){
		construct.$.construct.call(this);
		this.clazzName = "MediaTimeLineControlImp";
		
		Class.definePrivate(this, (function(vmpeg){
			var mediaTimelineControl = oipfObjectFactory.createMediaTimeLineControl(vmpeg);
			
			return{
				getAdsInfo: function() {
					return mediaTimelineControl.getAdsInfo();
				},
				trickPlayPossible: function() {
					return mediaTimelineControl.trickPlayPossible();
				},
				set onMediaContentChanged(cb){
					mediaTimelineControl.onMediaContentChanged = cb;
				},
				clear: function() {
					mediaTimelineControl = null;
				},
                getCurrentAdvertType : function() {
                    if( mediaTimelineControl.getCurrentAdvertType ){
                        return mediaTimelineControl.getCurrentAdvertType() ;
                    }else{
                        return null;
                    }
                }
			}
		}(vmpeg)));
	},
	
	set onMediaContentChanged(cb){
		KTW.utils.Log.printDbg("[oipf|MediaTimeLineControl] set onMediaContentChanged");
		this._p.onMediaContentChanged = cb;
	},
	
	getAdsInfo: function() {
		KTW.utils.Log.printDbg("[oipf|MediaTimeLineControl] getAdsInfo");
		return this._p.getAdsInfo();
	},
	
	trickPlayPossible: function() {
		KTW.utils.Log.printDbg("[oipf|MediaTimeLineControl] trickPlayPossible");
		return this._p.trickPlayPossible();
	},
	
	clear: function() {
		this._p.clear();
	},

    getCurrentAdvertType : function() {
        KTW.utils.Log.printDbg("[oipf|MediaTimeLineControl] getCurrentAdvertType");
        var _return = this._p.getCurrentAdvertType();

        // 리턴값이 0이 올수도 있기 때문에 숫자임을 체크하고 난 뒤에 숫자의 경우
        // 해당 값을 리턴 하도록 한다
        if( KTW.utils.util.isNumber(_return) ) {
            return _return;
        } else {
            return null;
        }
    }
});