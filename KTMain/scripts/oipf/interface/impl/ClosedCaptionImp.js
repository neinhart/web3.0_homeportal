"use strict";

KTW.oipf.ClosedCaptionControlImp = KTW.oipf.abs.AbsClosedCaptionControl.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "ClosedCaptionControlImp";
		
	},
	
	getClosedCaptionInfo: function(){
		console.log("fake getClosedCaptionInfo");
	}
});


KTW.oipf.ClosedCaptionPlayerImp = KTW.oipf.abs.AbsClosedCaptionPlayer.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "ClosedCaptionPlayerImp";
		
	},
	
	getClosedCaptionInfoList: function(){
		console.log("fake getClosedCaptionInfoList");
	}
});