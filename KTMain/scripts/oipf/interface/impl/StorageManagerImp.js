"use strict";

KTW.oipf.StorageManagerImp = KTW.oipf.abs.AbsStorageManager.extend({
	
	construct : function construct() {
	    construct.$.construct.call(this);
        this.clazzName = "StorageManagerImp";
        
        
        Class.definePrivate(this, (function(){
            //private. 
            var storageManager = oipfObjectFactory.createStorageManagerObject();
            
            return{
                get storageManager(){
                    return storageManager;
                },
            };
        }()));
	},
	
	get storages(){
		console.log("fake getter storages ");
		return this._p.storageManager.storages;
	}
});