"use strict";

KTW.oipf.LocalSystemImp = KTW.oipf.abs.AbsLocalSystem.extend({
	construct : function construct(_configuration) {
		construct.$.construct.call(this);		
		this.clazzName = "LocalSystemImp";
		
		Class.definePrivate(this, (function(_configuration){
			//private 영역.
									
			//interface 영역.
			return _configuration.localSystem;
		}(_configuration)));
	},
	
	get volumn() {
		var v = this._p.volume;
        KTW.utils.Log.printDbg("[oipf localsyatem] get volumn:" + v);
        return v;
    },
    set volume(v) {
    	KTW.utils.Log.printDbg("[oipf localSystem] set volumn:" + v);
    	this._p.volume = v;
    },
    get mute() {
    	var v = this._p.mute;
    	KTW.utils.Log.printDbg("[oipf localSystem] set mute:" + v);
        return v;
    },
    set mute(v) {
    	KTW.utils.Log.printDbg("[oipf localSystem] set mute:" + v);
    	this._p.mute = v;
    },    
    get outputs() {
        if (this._p.outputs) {
            KTW.utils.Log.printDbg("[oipf localSystem] get outputs length:" + this._p.outputs.length);
        }
        return this._p.outputs;
    },
    get networkInterfaces() {
        KTW.utils.Log.printDbg("[oipf localSystem] get networkInterfaces");
        
        return this._p.networkInterfaces;
    },
    get reminderTime() {
    	var v = this._p.reminderTime;
    	KTW.utils.Log.printDbg("[oipf localSystem] get reminderTime:" + v);
        return v;
    },
    set reminderTime(t) {
    	KTW.utils.Log.printDbg("[oipf localSystem] set reminderTime " + t);
        this._p.reminderTime = t;
    },
    get powerState() {
    	var v = this._p.powerState;
        KTW.utils.Log.printDbg("[oipf localSystem] get powerState " + v);
        return v;
    },
    set powerState(v){
    	KTW.utils.Log.printDbg("[oipf localSystem] set powerState " + v);
    	this._p.powerState = v;
    },  
    get modelName () {
    	var v = this._p.modelName;
        KTW.utils.Log.printDbg("[oipf localSystem] get modelName " + v);
        return v;
    },
    setPowerState: function (v) {
    	KTW.utils.Log.printDbg("[oipf localSystem] setPowerState " + v);
        return this._p.setPowerState(v);
    },
    setScreenSize: function (w, h) {
        KTW.utils.Log.printDbg("[oipf localSystem] setScreenSize("+ w +","+ h +")");
        this._p.setScreenSize(w,h);
    },
    setFrontPanetText: function (txt) {
        KTW.utils.Log.printDbg("[oipf localSystem] setFrontPanetText("+ txt +")");
        this._p.setFrontPanetText(txt);
    },    
    addEventListener: function (evt, hdlr) {
    	KTW.utils.Log.printDbg("[oipf localSystem] addSystemEventListener("+ evt +")");
    	this._p.addEventListener(evt, hdlr);
    },
    removeEventListener: function (evt) {
    	KTW.utils.Log.printDbg("[oipf localSystem] removeSystemEventListener("+ evt +")");
    	this._p.removeEventListener(evt);
    },    
    get tuners(){
        if (this._p.tuners) {
            KTW.utils.Log.printDbg("[oipf localSystem] get outputs length:" + this._p.tuners.length);
        }
    	return this._p.tuners;
    },
    get supportedPowerStates() {
        return this._p.supportedPowerStates;
    }
});

KTW.oipf.InnerConfigurationImp = KTW.oipf.abs.AbsInnerConfiguration.extend({
	construct : function construct(_configuration) {
		construct.$.construct.call(this);		
		this.clazzName = "InnerConfigurationImp";
		
		Class.definePrivate(this, (function(_configuration){
			//private 영역.
												
			//interface 영역.
			return _configuration.configuration;
		}(_configuration)));
	},
	
	get preferredAudioLanguage(){
		var v = this._p.preferredAudioLanguage;
		KTW.utils.Log.printDbg("[oipf:configuration] get preferredAudioLanguage:" + v);
		return v;
	},
	
	set preferredAudioLanguage(v){
		KTW.utils.Log.printDbg("[oipf:configuration] set preferredAudioLanguage:" + v);
		this._p.preferredAudioLanguage = v;
	},
	
	get preferredSubtitleLanguage(){
		var v =  this._p.preferredSubtitleLanguage;
		KTW.utils.Log.printDbg("[oipf:configuration] get preferredSubtitleLanguage:" + v);
		return v;
	},
	
	set preferredSubtitleLanguage(v){
		KTW.utils.Log.printDbg("[oipf:configuration] set preferredSubtitleLanguage v:" + v);
		this._p.preferredSubtitleLanguage = v;
	},
	
	getText: function(k, blockLog){
		var v = this._p.getText(k);

        if (!blockLog) {
            KTW.utils.Log.printDbg("[oipf:configuration]  getText() k = " + k + " value:" + v);
        }
        
        return v;
	},
	
	setText: function(k, v){
		KTW.utils.Log.printDbg("[oipf:configuration] setText() k = " + k + ", value: " + v);
		
		this._p.setText(k, v);
	},
	
	// TODO 2016.09.05 dhlee 2.0 R6 형상 적용. 관련 시나리오등 추후 확인해야 함.
	addEventListener: function(k, callback){
        KTW.utils.Log.printDbg("[oipf:configuration] addEventListener() k = " + k + ", callback: " + callback);        
        this._p.addEventListener(k, callback);
    },
    
// TODO 2016.09.05 dhlee 2.0 R6 형상 적용. 관련 시나리오등 추후 확인해야 함.
    removeEventListener: function(k){
        KTW.utils.Log.printDbg("[oipf:configuration] removeEventListener() k = " + k);        
        this._p.removeEventListener(k);
    }
});

KTW.oipf.ConfigurationImp = KTW.oipf.abs.AbsConfiguration.extend({
	construct : function construct() {
		construct.$.construct.call(this);		
		this.clazzName = "ConfigurationImp";
		
		Class.definePrivate(this, (function(){
			//private 영역.
			var _configuration = oipfObjectFactory.createConfigurationObject();			
			//미리 생성해 놓아야함.
			var configuration = new KTW.oipf.InnerConfigurationImp(_configuration);
			var localSystem = new KTW.oipf.LocalSystemImp(_configuration);
			
			return {				
				_getConfiguration : function(){
					if(configuration === null){
						configuration = new KTW.oipf.InnerConfigurationImp(_configuration);
					}
					
					return configuration;
				},
				
				_getLocalSystem : function(){
					if(localSystem === null){
						localSystem = new KTW.oipf.LocalSystemImp(_configuration);
					}
					
					return localSystem;
				}
			}
		}()));
	},
	
	//singleton 객체 return. 
	get configuration(){
		return this._p._getConfiguration();
	},
	
	//singleton 객체 return. 
	get localSystem(){
		return this._p._getLocalSystem();
	}
});