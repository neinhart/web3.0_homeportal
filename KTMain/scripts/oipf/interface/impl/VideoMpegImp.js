"use strict";

KTW.oipf.VideoMpegImp = KTW.oipf.abs.AbsVideoMpeg.extend({
	construct : function construct() {
		this.construct.$.construct.call(this);
		this.clazzName = "VideoMpegImp";
		
		Class.definePrivate(this, (function(){
			var _mpeg = oipfObjectFactory.createVideoMpegObject();
			_mpeg.id = 'videoMpegObj';

			try {
				_mpeg.style.position = "absolute";
				_mpeg.style.visibility = "hidden";
				_mpeg.style.left = "0px";
				_mpeg.style.top = "0px";
				_mpeg.style.width = "1920x";
				_mpeg.style.height = "1080px";

				_mpeg.x = 0;
				_mpeg.y = 0;
				_mpeg.width = KTW.CONSTANT.RESOLUTION.WIDTH;
				_mpeg.height = KTW.CONSTANT.RESOLUTION.HEIGHT;

				document.getElementById('mainVBOArea').appendChild(_mpeg);
			} catch (e) {
				console.error("error make vmo");
			}

			return {
				get vmo() {
					return _mpeg;
				},
				get x () {
					return _mpeg.x;
				},
				set x (v) {
					_mpeg.x = v;
				},
				get y () {
					return _mpeg.y;
				},
				set y (v) {
					_mpeg.y = v;
				},
				get width () {
					return _mpeg.width;
				},
				set width (v) {
					_mpeg.width = v;
				},
				get height () {
					return _mpeg.height;
				},
				set height (v) {
					_mpeg.height = v;
				},
				setFullScreen: function(v) {
					_mpeg.setFullScreen(v);
				},
				get data () {
					return _mpeg.data;
				},
				set data (v) {
					_mpeg.data = v;
				},
				get playTime() {
					return _mpeg.playTime;
				},
				get playPosition() {
					return _mpeg.playPosition;
				},
				get speed() {
					return _mpeg.speed;
				},
				get playState() {
					return _mpeg.playState;
				},
				set onPlaySpeedChanged(l) {
					_mpeg.onPlaySpeedChanged = l;
				},
				set onPlayPositionChanged(l) {
					_mpeg.onPlayPositionChanged = l;
				},
				set onReadyToPlay(l) {
					_mpeg.onReadyToPlay = l;
				},
				set onPlayStateChange(l) {
					_mpeg.onPlayStateChange = l;
				},
				set onFullScreenChange(l) {
					_mpeg.onFullScreenChange = l;
				},
                set onPlaybackEndInProgress(l) {
                    _mpeg.onPlaybackEndInProgress = l;
                },
				stop: function() {
					return _mpeg.stop();
				},
				play: function(playSpeed) {
					return _mpeg.play(playSpeed);
				},
				seek: function(position) {
					return _mpeg.seek(position);
				},
				getComponents: function(type, device) {
					return _mpeg.getComponents(type, device);
				},
				selectComponent: function(type, device) {
					return _mpeg.selectComponent(type, device);
				},
				unselectComponent: function(type, device) {
					return _mpeg.unselectComponent(type, device);
				},
				getCurrentActiveComponents: function(type, device) {
					return _mpeg.getCurrentActiveComponents(type, device);
				}
			};
		}()));
	},
	
	get x() { var v = this._p.x; KTW.utils.Log.printDbg("[oipf:mpeg] get_x " + v); return v; },
	set x(v) { KTW.utils.Log.printDbg("[oipf:mpeg] set_x " + v); this._p.x = v; },
    get y() { var v = this._p.y; KTW.utils.Log.printDbg("[oipf:mpeg] get_y " + v); return v; },
    set y(v) { KTW.utils.Log.printDbg("[oipf:mpeg] set_y " + v); this._p.y = v; },
    get width() { var v = this._p.width; KTW.utils.Log.printDbg("[oipf:mpeg] get_w " + v); return v; },
    set width(v) { KTW.utils.Log.printDbg("[oipf:mpeg] set_w " + v); this._p.width = v; },
    get height() { var v = this._p.height; KTW.utils.Log.printDbg("[oipf:mpeg] get_h " + v); return v; },
    set height(v) { KTW.utils.Log.printDbg("[oipf:mpeg] set_h " + v); this._p.height = v; },
    setFullScreen: function (b) {KTW.utils.Log.printDbg("[oipf:mpeg] setFullScreen " + b); this._p.setFullScreen(b); },
    get data() { var v = this._p.data; KTW.utils.Log.printDbg("[oipf:mpeg] get data:" + v); return v; },
    set data(v) { KTW.utils.Log.printDbg("[oipf:mpeg] set data:" + v); this._p.data = v; },
    get playTime() {var v = this._p.playTime; KTW.utils.Log.printDbg("[oipf:mpeg] playTime:" + v); return v; },
    get playPosition() { var v = this._p.playPosition; KTW.utils.Log.printDbg("[oipf:mpeg] playPosition " + v); return v; },
    get speed() {var v = this._p.speed; KTW.utils.Log.printDbg("[oipf:mpeg] get speed:" + v); return v; },
    get playState() {var v = this._p.playState;  KTW.utils.Log.printDbg("[oipf:mpeg] get playState:" + v); return v; },
	get vmo() {
		return this._p.vmo;
	},	
    play: function (s) {
        KTW.utils.Log.printDbg("[oipf:mpeg] play speed:" + s);
        return this._p.play(s);
    },
    stop: function () {
    	KTW.utils.Log.printDbg("[oipf:mpeg] stop");
    	return this._p.stop();
    },
    seek: function (pos) {
        KTW.utils.Log.printDbg("[oipf:mpeg] seek pos:" + pos);
        return this._p.seek(pos);
    },
    
    getComponents: function(type, device){
    	var components = this._p.getComponents(type, device);
    	KTW.utils.Log.printDbg("[oipf:mpeg] getComponents type:"+type + " comp count:" + (components ? components.length : 0));
    	if(components && components.length > 1) {
    		for( var i = 0 ; i < components.length ; i++) {
    			if(type == 1)
    				KTW.utils.Log.printDbg("[oipf:mpeg] components lan:" + components[i].language + " encoding:" + components[i].encoding);
    			else if(type == 0) 
    				KTW.utils.Log.printDbg("[oipf:mpeg] components encoding:" + components[i].encoding);
    		}
    	}
    	return components;
    },
    
    selectComponent : function(type, device){
    	KTW.utils.Log.printDbg("[oipf:mpeg] selectComponent component:" + type + " device:" + JSON.stringify(device));
    	return this._p.selectComponent(type, device);
    },
    unselectComponent : function(type, device){
    	KTW.utils.Log.printDbg("[oipf:mpeg] unselectComponent component:" + type + " device:" + JSON.stringify(device));
    	return this._p.unselectComponent(type, device);
    },
    getCurrentActiveComponents : function (type, device) {
    	var ret = this._p.getCurrentActiveComponents(type, device);
    	KTW.utils.Log.printDbg("[oipf:mpeg] getCurrentActiveComponents type: " + type +  " component:" + JSON.stringify(ret));
    	return ret;
    },    
    //callbacks. 
    set onPlaySpeedChanged(l) {
    	KTW.utils.Log.printDbg("[oipf:mpeg] setCallback onPlaySpeedChanged");
		this._p.onPlaySpeedChanged = l;
	},
	set onPlayPositionChanged(l) {
		KTW.utils.Log.printDbg("[oipf:mpeg] setCallback onPlayPositionChanged");
		this._p.onPlayPositionChanged = l;
	},
	set onReadyToPlay(l) {
		KTW.utils.Log.printDbg("[oipf:mpeg] setCallback onReadyToPlay");
		this._p.onReadyToPlay = l;
	},
	set onPlayStateChange(l) {
		KTW.utils.Log.printDbg("[oipf:mpeg] setCallback onPlayStateChange");
		this._p.onPlayStateChange = l;
	},
	set onFullScreenChange(l) {
		KTW.utils.Log.printDbg("[oipf:mpeg] setCallback onFullScreenChange");
		this._p.onFullScreenChange = l;
	},
    set onPlaybackEndInProgress(l){
        KTW.utils.Log.printDbg("[oipf:mpeg] setCallback onPlaybackEndInProgress");
        this._p.onPlaybackEndInProgress = l;
    }
});