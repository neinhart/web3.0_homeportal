"use strict";

KTW.oipf.TransPonderControlImp = KTW.oipf.abs.AbsTransPonderControl.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "TransPonderControlImp";
		
        Class.definePrivate(this, (function(){
            //private. 
            var transponderControl = oipfObjectFactory.createTransponderControl();
            
            return{
                get transponderControl(){
                    return transponderControl;
                },
            };
        }()));
		
	},
	
	getCollectibleList: function() {
		console.log("[oipf tunerAccess]getCollectibleList.");
		
	    return this._p.transponderControl.getCollectibleList();
	},
	
	collectOneTransponder : function(name, id) {
		console.log("[oipf tunerAccess]collectOneTransponder " + name + " id:" + id);
		
	    return this._p.transponderControl.collectOneTransponder(name, id);
	},
	
	tune : function(v){
		console.log("[oipf tunerAccess]tune " + v);
		
	    this._p.transponderControl.tune(v);
	}
});