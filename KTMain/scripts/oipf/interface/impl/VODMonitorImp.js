/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>VODMonitorImp.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.VODMonitorImp = KTW.oipf.abs.AbsVODMonitor.extend({
    construct: function construct() {
        construct.$.construct.call(this);
        this.clazzName = "VODMonitorImp";

        Class.definePrivate(this, (function () {
            //private.
            var vodMonitor = oipfObjectFactory.createVODMonitorObject();
            return {
                get vodMonitor() {
                    return vodMonitor;
                },
                set onVODStateChanged(cb){
                    vodMonitor.onVODStateChanged = cb;
                }
            };
        }()));
    },
    set onVODStateChanged(cb){
        OTW.utils.Log.printDbg("[oipf|vodMonitor] set onVODStateChanged");
        this._p.onVODStateChanged = cb;
    }
});