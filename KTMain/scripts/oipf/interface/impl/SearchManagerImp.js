"use strict";

KTW.oipf.SearchManagerImp = KTW.oipf.abs.AbsSearchManager.extend({
	construct : function construct(){
		construct.$.construct.call(this);
		this.clazzName = "searchManagerImp";
		
		Class.definePrivate(this, (function(){
			//private 영역.
			var searchManager = oipfObjectFactory.createSearchManagerObject();
			
			return {
				get searchManager () {
					return searchManager;
				} 
			}
		}()));
	},
	
	set onMetadataSearch(cb) { 
		this._p.searchManager.onMetadataSearch = cb; 
    },
    
    set onMetadataUpdate (cb) {
    	this._p.searchManager.onMetadataUpdate= cb;
    },
	
	createSearch: function createSearch(search_target) {
		return this._p.searchManager.createSearch(search_target);
    }
});