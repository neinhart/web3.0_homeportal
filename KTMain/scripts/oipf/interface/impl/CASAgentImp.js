"use strict";

KTW.oipf.CasAgentImp = KTW.oipf.abs.AbsCasAgent.extend({
    
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "CasAgentImp";
		
		
		Class.definePrivate(this, (function(){
            //private. 
		    var casAgent = oipfObjectFactory.createCasAgentObject();
            
            return{
                get casAgent(){
                    return casAgent;
                },
            };
        }()));
	},
	
	request: function (aid, tid, tag, msg) {
        console.log("[oipf CasAgent] request() " + aid + " "+ tid + " "+ tag + " "+ msg);
        return this._p.casAgent.request(aid,tid,tag,msg);
    },
    
    addCAEventListener: function (evt, cb) {
        console.log("[oipf CasAgent] addCAEventListener() " + evt);
        return this._p.casAgent.addCAEventListener(evt, cb);
    },
    
    removeCAEventListener: function (evt, cb) {
        console.log("[oipf CasAgent] removeCAEventListener() " + evt);
        this._p.casAgent.removeCAEventListener(evt, cb);
    },
    
    getDeviceID : function() {
    	var deviceID = this._p.casAgent.getDeviceID();
    	console.log("[oipf CasAgent] getDeviceID() " + deviceID);
    	return deviceID;
    },
    
});