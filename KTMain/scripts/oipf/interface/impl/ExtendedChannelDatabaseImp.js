"use strict";

KTW.oipf.ExtendedChannelDatabaseImp = KTW.oipf.abs.AbsExtendedChannelDatabase.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "ExtendedChannelDatabaseImp";
		
		Class.definePrivate(this, (function(){
			//private. 
			var extended_channel_db = oipfObjectFactory.createExtendedChannelDatabase();
			
			return{
				get extended_channel_db(){
					return extended_channel_db;
				}
			};
		}()));
	},
	
	get skyPlusChannel(){
		console.log("[oipf exChannelDb] skyPlusChannel getter ");
		
		return this._p.extended_channel_db.skyPlusChannel;
	},
	
	get promoChannel(){
		console.log("[oipf exChannelDb] promoChannel getter ");
		
		return this._p.extended_channel_db.promoChannel;
	},
	
	selectPromoChannel: function(isBlockSelection, isBlockOSD){
		console.log("[oipf exChannelDb] selectPromoChannel isBlockSelection:" + isBlockSelection + " isBlockOSD:" + isBlockOSD);
		
		this._p.extended_channel_db.selectPromoChannel(isBlockSelection, isBlockOSD);
	},
	
	updateDIDatabase: function(){
		console.log("[oipf exChannelDb] updateDIDatabase ");
		
		return this._p.extended_channel_db.updateDIDatabase();
	},
	
	getSameKTChannel: function(channel){
		console.log("[oipf exChannelDb] getSameKTChannel channel:" + channel);
		
		return this._p.extended_channel_db.getSameKTChannel(channel);
	},
	
	getSameSkyChannel: function(channel){
		console.log("[oipf exChannelDb] getSameSkyChannel channel:" + channel);
		
		return this._p.extended_channel_db.getSameSkyChannel(channel);
	},
	
	isKidsCareChannel: function(channel){
		console.log("[oipf exChannelDb] isKidsCareChannel channel:" + channel);
		
		return this._p.extended_channel_db.isKidsCareChannel(channel);
	},
	
	requestFirmwareDownload: function(){
		console.log("[oipf exChannelDb] requestFirmwareDownload");
		
		return this._p.extended_channel_db.requestFirmwareDownload();
	},
});