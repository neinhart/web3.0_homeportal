/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MediaControlImp.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.MediaControlImp = KTW.oipf.abs.AbsMediaControl.extend({

    construct : function construct() {
        construct.$.construct.call(this);
        this.clazzName = "MediaControlImp";


        Class.definePrivate(this, (function(){
            //private.
            var mediaControl = oipfObjectFactory.createMediaControlObject();

            return{
                get mediaControl(){
                    return mediaControl;
                }
            };
        }()));
    },

    startVideoAndAudio: function(){
        console.log("[oipf MediaControl] startVideoAndAudio");
        this._p.mediaControl.startVideoAndAudio();
    },

    stopVideoAndAudio : function(){
        console.log("[oipf MediaControl] stopVideoAndAudio");
        this._p.mediaControl.stopVideoAndAudio();
    }
});