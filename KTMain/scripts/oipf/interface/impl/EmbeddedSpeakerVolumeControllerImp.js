/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EmbeddedSpeakerVolumeControllerImp.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.EmbeddedSpeakerVolumeControllerImp = KTW.oipf.abs.AbsEmbeddedSpeakerVolumeController.extend({

    construct: function construct() {
        construct.$.construct.call(this);
        this.clazzName = "EmbeddedSpeakerVolumeControllerImp";

        Class.definePrivate(this, (function () {
            //private.
            var embeddedSpeakerVolumeController = null;
            try {
                embeddedSpeakerVolumeController = oipfObjectFactory.createEmbeddedSpeakerVolumeControllerObject();
            }catch (e) {
                KTW.utils.Log.printExec(e);
            }
            return {
                get embeddedSpeakerVolumeController() {
                    return embeddedSpeakerVolumeController;
                }
            };
        }()));
    },

    getEmbeddedSpeakerVolumeController : function () {
        console.log("[oipf EmbeddedSpeakerVolumeControllerImp] getEmbeddedSpeakerVolumeController");
        return this._p.embeddedSpeakerVolumeController;
    }
});