"use strict";

KTW.oipf.SSOClientImp = KTW.oipf.abs.AbsSSOClient.extend({
	construct : function construct() {
		this.construct.$.construct.call(this);
		this.clazzName = "SSOClientImp";
		
		Class.definePrivate(this, (function(){
			var ssoClient = oipfObjectFactory.createSSOClient();
			
			return {
				terminalLogon: function(service_id) {
					return ssoClient.terminalLogon(service_id);
		        },
		        getSTBToken: function() {
		        	return ssoClient.getSTBToken();
		        },
		        getHDSToken: function() {
		        	return ssoClient.getHDSToken();
		        },
		        logout: function() {
		        	return ssoClient.logout();
		        },
		        addUserLogonEventListener: function(cb) {
		        	return ssoClient.addUserLogonEventListener(cb);
		        },
		        getSAID: function() {
		        	return ssoClient.getSAID();
		        },
		        getPINNumber: function() {
		        	return ssoClient.getPINNumber();
		        }
			};
		}()));
	},
	
	terminalLogon: function (service_id) {
        KTW.utils.Log.printDbg("[oipf SSOClient] terminalLogon() service_id:" + service_id);
        
        return this._p.terminalLogon(service_id);
    },
    
    addUserLogonEventListener: function (callback) {
        KTW.utils.Log.printDbg("[oipf SSOClient] addUserLogonEventListener()");
        
        this._p.addUserLogonEventListener(callback);
    },
    
    logout: function () {
        KTW.utils.Log.printDbg("[oipf SSOClient] logout()");
        
        return this._p.logout();
    },
    
    getSTBToken: function () {
    	var stb_token = this._p.getSTBToken();
    	
        KTW.utils.Log.printDbg("[oipf SSOClient] getSTBToken() stb_token:" + stb_token);
        
        return stb_token;
    },
    
    getHDSToken: function () {
    	var hds_token = this._p.getHDSToken();
    	
        KTW.utils.Log.printDbg("[oipf SSOClient] getHDSToken() hds_token:" + hds_token);
        
        return hds_token; 
    },
    
    getSAID: function () {
    	KTW.utils.Log.printDbg("[oipf SSOClient] getSAID()");
    	
    	return this._p.getSAID();
    }, 
    
    getPINNumber: function () {
    	KTW.utils.Log.printDbg("[oipf SSOClient] getPINNumber()");
    	
    	return this._p.getPINNumber();
    },
});