"use strict";

KTW.oipf.MouseControlImp = KTW.oipf.abs.AbsMouseControl.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "MouseControlImp";
		
		
		Class.definePrivate(this, (function(){
            //private. 
		    var mouseControl = oipfObjectFactory.createMouseControlObject();
            
            return{
                get mouseControl(){
                    return mouseControl;
                },
            };
        }()));
	},
	
	restoreMouseControl: function(){
		console.log("[oipf MouseControl] restoreMouseControl");
		 this._p.mouseControl.restoreMouseControl();
	},
	
	disableMouseControl : function(){
		console.log("[oipf MouseControl] disableMouseControl");
		this._p.mouseControl.disableMouseControl();
	}
});