/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AudioTtsImp.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.AudioTtsImp = KTW.oipf.abs.AbsAudioTts.extend({
    construct: function construct() {
        construct.$.construct.call(this);
        this.clazzName = "AudioTtsImp";

        Class.definePrivate(this, (function () {
            //private.
            var audioTts = oipfObjectFactory.createAudioTtsObject();
            return {
                get audioTts() {
                    return audioTts;
                }
            };
        }()));
    },
    getAudioTts: function () {
        return this._p.audioTts;
    },
    startSpeech: function (text, voice) {
        console.log("[oipf AudioTtsImp] startSpeech() - text : " + text + ", voice : " + voice);
        return this._p.audioTts.startSpeech(text, voice);
    },
    stopSpeech: function (id) {
        console.log("[oipf AudioTtsImp] stopSpeech() - id :" + id);
        return this._p.audioTts.stopSpeech(id);
    },
    startSynthesize: function (text, format, voice) {
        console.log("[oipf AudioTtsImp] startSynthesize() - text :" + text + ", format : " + format + ", voice : " + voice);
        return this._p.audioTts.startSynthesize(text, format, voice);
    },
    stopSynthesize: function (id) {
        console.log("[oipf AudioTtsImp] stopSynthesize() - id :" + id);
        return this._p.audioTts.stopSynthesize(id);
    },
    setVolume: function (volume) {
        console.log("[oipf AudioTtsImp] setVolume() - volume :" + volume);
        return this._p.audioTts.setVolume(volume);
    },
    getVolume: function () {
        console.log("[oipf AudioTtsImp] getVolume()");
        return this._p.audioTts.getVolume();
    },
    addSpeechStateChangeListener: function (listener) {
        console.log("[oipf AudioTtsImp] addSpeechStateChangeListener()");
        return this._p.audioTts.onSpeechStateChange = listener;
    },
    addSynthesizeStateChangeListener: function (listener) {
        console.log("[oipf AudioTtsImp] addSynthesizeStateChangeListener()");
        return this._p.audioTts.onSynthesizeStateChange = listener;
    },

    setDefaultVoiceMode: function (mode) {
        console.log("[oipf AudioTtsImp] setDefaultVoiceMode()");
        return this._p.audioTts.defaultVoiceMode = mode;
    },
    get defaultVoiceMode () {
        return this._p.audioTts.defaultVoiceMode;
    }
});