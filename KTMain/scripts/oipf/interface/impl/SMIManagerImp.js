"use strict";

KTW.oipf.SMIManagerImp = KTW.oipf.abs.AbsSMIManager.extend({
	construct : function construct(search_target){
		construct.$.construct.call(this);
		this.clazzName = "SMIManagerImp";
		
		Class.definePrivate(this, (function(){
			
			var smi_object = oipfObjectFactory.createSMIManagerObject();
			
			function _prepare(url, element) {
				smi_object.prepare(url, element);
			};
			
			function _play(vmo) {
				smi_object.play(vmo);
			};
			
			function _stop() {
				smi_object.stop();
			};
			
			function _destroy() {
				smi_object.destroy();
			};
			
			function _setLanguage(index) {
				smi_object.setLanguage(index);
			};
			
			function _getAvailableLanguages() {
				return smi_object.getAvailableLanguages();
			};
			
			function _getState() {
				return smi_object.getState();
			};
			
			function _resize() {
				return smi_object.resize();
			}
			
			return {
				prepare: _prepare,
				play: _play,
				stop: _stop,
				destroy: _destroy,
				setLanguage: _setLanguage,
				getAvailableLanguages: _getAvailableLanguages,
				getState: _getState,
				resize:_resize,
				STATE: {
					PREPARED: smi_object.PREPARED,
					STARTED: smi_object.STARTED,
					STOPPED: smi_object.STOPPED,
					DESTROYED: smi_object.DESTROYED,
				},
			}			
		}()));
	},
	
	prepare: function (url, element) {
        KTW.utils.Log.printDbg("[oipf:smi] prepare. url:"+ url + ", parent node:"+ element);
        return this._p.prepare(url, element);
    },
    
    play: function (vmo) {
    	KTW.utils.Log.printDbg("[oipf:smi] play");
        return this._p.play(vmo);
    },

    stop: function () {
        KTW.utils.Log.printDbg("[oipf:smi] stop");
        this._p.stop();
    },
    
    destroy: function () {
        KTW.utils.Log.printDbg("[oipf:smi] destroy");
        this._p.destroy();
    },
    
    setLanguage: function(index) {
    	KTW.utils.Log.printDbg("[oipf:smi] setLanguage index:" + index);
        this._p.setLanguage();
    },
    
    getAvailableLanguages: function() {
    	var v = this._p.getAvailableLanguages();
    	KTW.utils.Log.printDbg("[oipf:smi] getAvailableLanguages v:" + JSON.stringify(v));
    	return v;
    },
    
    getState: function() {
    	var v = this._p.getState();
    	KTW.utils.Log.printDbg("[oipf:smi] getState v:" + v);
    	return v;
    },
    
    resize: function() {
    	this._p.resize();
    },
    
    get STATE() {
    	return this._p.STATE;
    }
});