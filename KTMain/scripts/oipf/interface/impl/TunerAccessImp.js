"use strict";

KTW.oipf.TunerAccessImp = KTW.oipf.abs.AbsTunerAccess.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "TunerAccessImp";
		
        Class.definePrivate(this, (function(){
            //private. 
            var tunerAccess = oipfObjectFactory.createTunerAccess();
            
            return{
                get tunerAccess(){
                    return tunerAccess;
                },
            };
        }()));
	},
	
    setTSInfo: function(index, info, flag){
		console.log("[oipf tunerAccess]setTSInfo. " + index + "," + info + "," + flag);
		
		this._p.tunerAccess.setTSInfo(index, info, flag);
	},
	
	getTSInfo : function(index){
		console.log("[oipf tunerAccess]getTSInfo index:" + index);
		
		return this._p.tunerAccess.getTSInfo(index);
	},
	
	getTSInfos : function(){
		console.log("[oipf tunerAccess]getTSInfos");
		
		return this._p.tunerAccess.getTSInfos();
	},
	
	getAdditionalTunerInfos : function(){
		console.log("[oipf tunerAccess]getAdditionalTunerInfos");
		
		return this._p.tunerAccess.getAdditionalTunerInfos();
	},
	
	getLNB: function(port){
		console.log("[oipf tunerAccess]getLNB port:" + port);
		
		return this._p.tunerAccess.getLNB(port);
	},
	
    setLNB: function(port, value/*array*/){
    	console.log("[oipf tunerAccess]setLNB. port:" + port + " value:" + value);
    	
    	this._p.tunerAccess.setLNB(port, value);
	},
});