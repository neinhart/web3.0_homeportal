"use strict";

KTW.oipf.ZipExtractorImp = KTW.oipf.abs.AbsZipExtractor.extend({
	construct : function construct() {
		construct.$.construct.call(this);
		this.clazzName = "ZipExtractorImp";
		
        Class.definePrivate(this, (function(){
            //private. 
        	var zip_extractor = null;
        	
        	try {
        		zip_extractor = oipfObjectFactory.createZipExtractorObject();
        	} catch(e) {
        		KTW.utils.Log.printExec(e);
        	}
            
            function requestExtraction(path, dest_dir, callback) {
            	if(zip_extractor) {
            		var cb = {
        				"notifyZipExtractorState": callback,
            		};
            		zip_extractor.requestExtraction(path, dest_dir, cb);
            	}
            }
            
            function cancelRequest(handle) {
            	if(zip_extractor) {
            		zip_extractor.cancelRequest(handle);
            	}	
            }
            
            function hasZipExtractor() {
            	if(window.FORCE_JS_ZIP === true) return false;
            	
            	return (zip_extractor) ? true : false;
            }
            
            return{
            	hasZipExtractor: hasZipExtractor,
            	requestExtraction:requestExtraction,
            	cancelRequest:cancelRequest,
            };
        }()));
	},
	
	hasZipExtractor: function() {
		return this._p.hasZipExtractor();
	},
	requestExtraction: function(path, dest_dir, callback){
		console.log("[oipf ZipExtractorImp] requestExtraction path:" + path + " dest_dir:" + dest_dir);
		return this._p.requestExtraction(path, dest_dir, callback);
	},
	cancelRequest: function(handle){
		console.log("[oipf ZipExtractorImp] cancelRequest handle:" + handle);
		return this._p.cancelRequest(handle);
	},
});