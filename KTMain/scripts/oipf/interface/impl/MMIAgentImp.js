"use strict";

KTW.oipf.MMIAgentImp = KTW.oipf.abs.AbsMMIAgent.extend({
    
    construct : function construct() {
        construct.$.construct.call(this);
        this.clazzName = "MMIAgentImp";
        
        
        Class.definePrivate(this, (function(){
            //private. 
            var mmiAgent = oipfObjectFactory.createMMIAgentObject();
            
            return{
                get mmiAgent(){
                    return mmiAgent;
                },
            };
        }()));
    },
	
	addMMIEventListener: function(event, callback){
	    console.log("[oipf MMIAgent] addMMIEventListener() " + event);
        return this._p.mmiAgent.addMMIEventListener(event, callback);
	},
	
	removeMMIEventListener : function(event, callback){
	    console.log("[oipf MMIAgent] removeMMIEventListener() " + event);
        this._p.mmiAgent.removeMMIEventListener(event, callback);
	}
});