"use strict";

KTW.oipf.ChannelSelectionEventManagerImp = KTW.oipf.abs.AbsChannelSelectionEventManager.extend({
	
	construct : function construct() {
		construct.$.construct.call(this);		
		this.clazzName = "ChannelSelectionEventManagerImp";
		
		Class.definePrivate(this, (function() {
            var channelSelectionEventManager = oipfObjectFactory.createChannelSelectionEventManager();
            
            return {
                get channelSelectionEventManager() {
                    return channelSelectionEventManager;
                }
            }
        }()));
	},
	
	addChannelSelectionEventListener: function(cb) {
		console.log("fake addChannelSelectionEventListener()");
		
		this._p.channelSelectionEventManager.addChannelSelectionEventListener(cb);
	},
	
	removeChannelSelectionEventListener: function(cb) {
		console.log("fake removeChannelSelectionEventListener()");
		
		this._p.channelSelectionEventManager.removeChannelSelectionEventListener(cb);
	}, 
	
	addDirectChannelEventListener: function(cb) {
		console.log("fake addDirectChannelEventListener()");
		
		this._p.channelSelectionEventManager.addDirectChannelEventListener(cb);
	},
});