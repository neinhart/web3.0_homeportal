"use strict";
/**
 * 설명:
 * http://kbase.alticast.com/confluence/display/IPTV2/JS+Object+Oriented+style
 * 
 * extend: 상속 구현.
 *   예약어 
 *      abstractGetter:Array
 *            설명: Array 에 있는 Getter 를 Overriding 해야 한다.
 *      abstractSetter:Array
 *            설명: Array 에 있는 Setter 를 Overriding 해야 한다.
 *      construct:Function
 *            설명: 생성자함수의 본체이다. 여기서 객체가 구성된다.
 *      [함수명]:Function
 *            설명: public method 가 된다. 이 객체는 construct 에서 생성되는 객체의 __proto__ 와 링크 되며 동시에 construct 함수의 prototype 값에 복사된다.
 *      
 *   API. 
 *      Class.absFunction(Function) 
 *            설명: 이 함수는 Abstract 이다.
 *      Class.ASSERT(condition == true) 
 *      Class.definePrivate(this binding, 즉시실행함수로 생성된 Object)
 *            설명: parent 든 child 든 쓸 수 있다. _priate 를 기준으로 항상 Function 을 통해 생성되는 객체를 머지한다. 
 *                 parent 가 생성한 _p 가 있다면 그걸 기준으로 병합한다. 중복은 overwrite 된다. closure 구조로 생성되기 때문에 
 *                 항상 메모리 사용을 생각하면서 짜자. 
 *      
 *   예약된 변수
 *   각 method 의 $ (callee.$ 이거나 기명함수(var fun = function func())를 사용해 함수명.$)
 *   _p <= private 영역을 구성하는 closure
 *   
 *   extend(). 
 *       desc: 객체의 상속을 구현, 미 구현된 abstrac method, abstract setter,getter 를 runtime 에 check 한다.
 *   
 *   copyObject(n, dest, src).
 *       desc: 객체 property n 을 복사.
 *   
 */
/**
 * 
 * @param n : 멤버 이름
 * @param dest : 복사할 대상
 * @param src : 복사할 원본 
 * @returns 객체인경우 객체 return, getter, setter 의 경우 undefined 리턴.
 * @desc 얕은 복사임.
 */

function Class() {
};
Class.isObfuscation = function(){
    var tmp = {obfuscation:true};
    if(tmp["obfuscation"] == undefined)
        return true;
    
    return false;
}
Class.prototype.construct = function() {};
Class.extend = function extend(def) {
  var classDef = function classDef() {
      if (arguments[0] !== Class) {
    	  //현재 객체의 super 즉 prototype link 공유.
    	  this.parentLink = classDef.prototype;
    	  
    	  //생성자에서 생성자 함수의 method 에 abstract 가 있는지 체크.
    	  if(Class.validate(this) == null){
    		  alert("can't create Object with abstract class");
    		  var error = Error("can't create Object with abstract class");
    		  KTW.utils.Log.printErr(error.stack);
    		  return;
    	  }
    	  this.construct.apply(this, arguments);
      }
  };
 
  var proto = new this(Class);
  var superClass = this.prototype;
  
  for (var n in def) {	  
	  //getter setter 를 포함 object 를 복사.
	  var item = Class.copyObject(n, proto, def);
	  	  
	  //KTW.utils.Log.printErr("copyMethod:"+ n + " getter:" + (getter !== undefined) + " setter:" +(setter !== undefined) + " item:" + (item !== undefined));
      
      if (item instanceof Function){
    	  item.$ = superClass;
      }
  }
  proto.parentLink = superClass; 
    
  var methodList = [];
  var setterGetterList = [];
  
  if(!Class.isObfuscation()){
    //abstract & setter & getter check. 
      for(var n in superClass){
          if(Class.getOwnPropertyDescriptor(superClass, n).get){//superClass.__lookupGetter__(n)){
              //getter 자체로는 abstract 가 없기 때문에, 여기서 체크할 필요가 없다. 
              //문제는 object[method name] 으로 접근하려는 순간 getter function 이 call 되기 때문에 객체가 만들어지기 전에 this 로 접근하게 되어 문제가 발생한다.
              //setter 는 사실 여기서 체크 안해도 된다.
              continue;
          }else if(superClass[n] instanceof Function && superClass[n].isAbstract === true){
              var found = false;
              for (var target in def) {
                  if(target == n){ 
                      if(superClass[n].length === def[target].length){
                          found = true;
                      }
                  }
              }
              if(found === false){
                  methodList.push("method " + n);
              }
          }else if(proto[n] instanceof Array && n === 'abstractGetter'){
              for(var index = (proto[n].length-1) ; index >= 0 ; index--){
                  var getter = proto[n][index];
                  if(!Class.getOwnPropertyDescriptor(def, getter).get){//!def.__lookupGetter__(getter)){
                      methodList.push("getter " + getter);
                  }
              }       
          }else if(proto[n] instanceof Array && n === 'abstractSetter'){
              for(var index = (proto[n].length-1) ; index >= 0 ; index--){
                  var setter = proto[n][index];
                  if(!Class.getOwnPropertyDescriptor(def, setter).set){//!def.__lookupSetter__(setter)){
                      methodList.push("setter " + setter);
                  }
              }
          }
      }
      
      if(methodList.length > 0){
          var message = "";
          
          for(var method in methodList){
              message += methodList[method] + ",";
          }
          
          if(def._abstracClass === true){
              console.log("parent abstract method:"+message);
          }else{
              var error = Error("Implements Abstract member: " + message);
              console.log(error); 
          }
      }  
     
      classDef.prototype = proto;
     
      //Give this new class the same static extend method    
      classDef.extend = this.extend;      
      return classDef;
  }
};

Class.getOwnPropertyDescriptor = function(target, name){
	var descriptor = Object.getOwnPropertyDescriptor(target, name);
	
	if(!descriptor){
		if(!target.parentLink){
			return {
				get:undefined,
				set:undefined,
			};
		}else{
			return Class.getOwnPropertyDescriptor(target.parentLink, name);
		}
	}else{
		return descriptor;
	}
};

Class.copyObject = function copyObject(n, dest, src){
	var object = null;
	var getter = Class.getOwnPropertyDescriptor(src, n).get;//src.__lookupGetter__(n);
	var setter = Class.getOwnPropertyDescriptor(src, n).set;//src.__lookupSetter__(n);
	  	  
	/* getter 값을 여기서 읽음 곤란함. */
	if(getter !== undefined || setter !== undefined){
		if(getter !== undefined && setter !== undefined){
			Object.defineProperty(dest, n, {get: getter, set: setter});//dest.__defineGetter__(n, getter);
		}else if(getter !== undefined){
			Object.defineProperty(dest, n, {get: getter});//dest.__defineGetter__(n, getter);
		}else if(setter !== undefined){
			Object.defineProperty(dest, n, {set: setter});//dest.__defineSetter__(n, setter);
		}
		return undefined;
	}else{
		dest[n] = src[n];		
		return src[n];
	} 
};

/**
 * Object 객체에 미구현된 abstract function 이 있는지 확인.
 * @param Object
 * @returns
 */
Class.validate = function validate(targetObj) {
	for (var method in targetObj) {
		if(Class.getOwnPropertyDescriptor(targetObj, method).get){//targetObj.__lookupGetter__(method)){
		    //getter 자체로는 abstract 가 없기 때문에, 여기서 체크할 필요가 없다. 
		    //문제는 targetObj[method name] 으로 접근하려는 순간 getter function 이 call 되기 때문에 객체가 만들어지기 전에 this 로 접근하게 되어 문제가 발생한다.
		    //setter 는 사실 여기서 체크 안해도 된다.
		    continue;
		}else if (targetObj[method] instanceof Function && targetObj[method].isAbstract === true) {
			return null;
		}else if (targetObj[method] instanceof Array && method === 'abstractGetter') {
			for (var index = (targetObj[method].length - 1); index >= 0; index--) {
				var getter = targetObj[method][index];
				if (!Class.getOwnPropertyDescriptor(targetObj, getter).get){//!targetObj.__lookupGetter__(getter)) {
					return null;
				}
			}
		} else if (targetObj[method] instanceof Array && method === 'abstractSetter') {
			for (var index = (targetObj[method].length - 1); index >= 0; index--) {
				var setter = targetObj[method][index];
				if (!Class.getOwnPropertyDescriptor(targetObj, setter).set){//!targetObj.__lookupSetter__(setter)) {
					return null;
				}
			}
		}
	}

	return targetObj;
};

/**
 * 지정된 함수를 abstract function 으로 flag setting
 * @param func
 * @returns
 */
Class.absFunction = function absFunction(func){
	Object.defineProperty(func, 'isAbstract', {get : function(){ return true; }});
	return func;
};

Class.ASSERT = function ASSERT(val, message){
	if(!val){
		alert(message || "ASSERT !! check");
		
		var error = Error(message || "ASSERT !! check");
		KTW.utils.Log.printErr(error.stack);
	}
};

/**
 * JQuery. Extend 복사후 defineSetter 부분만 추가. 객체의 병합. 중복 key 값은 overwrite.
 */
Class.merge = function merge() {
	var src, copyIsArray, copy, name, options, clone,
	target = arguments[0] || {},
	i = 1,
	length = arguments.length,
	deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;
	
		// skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}
	
	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}
	
	// extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}
	
	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];
	
				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}
	
				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];
	
					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}
	
					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );
	
				// Don't bring in undefined values
				} else/* if ( copy !== undefined )*/ {
					Class.copyObject(name, target, options);
				}
			}
		}
	}

	// Return the modified object.
	return target;
};

Class.definePrivate = function definePrivate(thiz, def){
	Class.ASSERT(thiz instanceof Object);	
	Class.ASSERT(!(def instanceof Function));
	Class.ASSERT(arguments.length === 2);
	thiz._p = (thiz._p ? Class.merge({}, thiz._p, def) : def);
};

/**
 * 객체의 멤버 변수 복사 (깊은 복사)
 * recursion 으로 object 의 경우 object 도 복사함 
 * 아마 prototype 도 결국 객체니 모두 복사해 버리겠지. 그렇다면 constructor 는 어떨지 ?. 
 * 이건 확인 필요.
 * 
 * @param parent
 * @param child
 * @returns {___anonymous3150_3151}
 */
Class.deepCopyMethod = function deepCopyMethod(parent, child) {
	var i, toStr = Object.prototype.toString;
	var astr = "[object Array]";
	child = child || {};
	for (i in parent) {
		if (parent.hasOwnProperty(i)) {
			if (typeof parent[i] === "object") {
				child[i] = (toStr.call(parent[i]) === astr) ? [] : {};
				extendDeep(parent[i], child[i]);
			} else {
				child[i] = parent[i];
			}
		}
	}
	return child;
};