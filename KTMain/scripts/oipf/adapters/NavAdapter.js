"use strict";

/**
 * handle to channel navgation
 * manage favourite channel list and control channel event 
 */
KTW.oipf.adapters.NavAdapter = (function () {
    
    var NAV_DEF_CONTROL = KTW.nav.Def.CONTROL;
    
    var log = KTW.utils.Log;
    
    var oipfFactory = KTW.oipf.oipfObjectFactory;
    var channelDatabaseManager = KTW.nav.si.ChannelDatabaseManager;
    
    var main_vbo = null;
    var pip_vbo = null;
    var stitching_vbo = null;
    var channel_config = null;
    var ex_ch_db = null;
    var csem = null;
    var media_control = null;
    
    var mainChannelControl = null;
    var pipChannelControl = null;
    var stitchingChannelControl = null;
    
    var focus_channels = null;
    
    function _init() {
        focus_channels = [];
        
        main_vbo = getVideoBroadcastObject(NAV_DEF_CONTROL.MAIN);
        channelDatabaseManager.init(getChannelConfig(main_vbo));
        
        // 초기에 main channel control만 생성
        _getChannelControl(NAV_DEF_CONTROL.MAIN, true);

        // create MediaControlObject
        try {
            media_control = oipfFactory.createMediaControlObject();
            log.printDbg("created MediaControlObject");
        } catch(e) {
            log.printErr("failed createMediaControlObject");
            log.printExec(e);
        }
    }
    
    function _start() {
        channelDatabaseManager.start();
    }
    
    function _pause() {
        if (mainChannelControl !== null) {
            mainChannelControl.pause();
        }
        if (pipChannelControl !== null) {
            pipChannelControl.pause();
        }
        if (stitchingChannelControl !== null) {
            stitchingChannelControl.pause();
        }
    }
    
    function _getPromoChannel() {
        return getExtendedChannelDB().promoChannel;
    }
    
    function _getSkyPlusChannel() {
        return getExtendedChannelDB().skyPlusChannel;
    }
    
    function _getSameKTChannel(channel) {
        return getExtendedChannelDB().getSameKTChannel(channel);
    }
    
    /**
     * [jh.lee] 자녀안심채널인지 체크하는 API
     */
    function _isKidsCareChannel(channel) {
        return getExtendedChannelDB().isKidsCareChannel(channel);
    }
    
    /**
     * [jh.lee] 메뉴데이터를 생성하는 시점에 포커스 채널을 저장.
     * [jh.lee] TODO 해당 함수의 위치가 변경될 수 있음. 확인 필요
     * [jh.lee]
     */
    function _setFocusChannels(locators) {
        
        var channel;
        var count = 0;
        var length = locators.length;
        for (var i = 0; i < length; i++) {
            log.printDbg("_setFocusChannels() locators[" + i + "] : " + locators[i]);
            channel = _getChannelByTriplet(locators[i]);
            if (KTW.utils.util.isValidVariable(channel) === true) {
                log.printDbg("_setFocusChannels() channel = " + channel.name);
                focus_channels[count] = channel;
                count++;
            }
        }
    }
    
    /**
     * [jh.lee] 포커스 채널 반환
     * [jh.lee] TODO 해당 함수의 위치가 변경될 수 있음. 확인 필요
     */
    function _isFocusChannel(channel) {
        
        if (KTW.utils.util.isValidVariable(channel) === false) {
            return false;
        }
        
        var is_focus_channel = false;
        
        var length = focus_channels.length;
        for (var i = 0; i < length; i++) {
            if (focus_channels[i].ccid === channel.ccid) {
                is_focus_channel = true;
                break;
            }
        }
        
        return is_focus_channel;
    }
    
    /**
     * get channel object by channel major number
     * 
     * @num1 - olleh channel number
     * @num2 - sky channel number
     */
    function _getChannelByNum(num1, num2) {
        return channelDatabaseManager.getChannelByNum(num1, num2);
    }

    /**
     * 우리집 맞춤 TV -> 알림박스 에서 채널 연동시 채널 번호 기준으로 channel obj 를 찾는데,
     * _getChannelByNum 로는 data channel 를 찾을 수가 없어서 전체 채널링에서 채널 번호를 기준으로 channel obj 를 찾는 함수 추가
     * 실시간 인기채널에서 채널 번호 기준으로 찾는 함수
     */
    function _getChannelByNumOnAll (num , isRealTimeChannel) {
        return channelDatabaseManager.getChannelByNumOnAll(num , isRealTimeChannel);
    }
    
    function _getChannelByCCID(ccid) {
        return channelDatabaseManager.getChannelByCCID(ccid);
    }
    
    /**
     * return channel object matching with sid and id_type
     * 
     * @param sid - sid of channel
     * @param id_type - idType of channel
     */
    function _getChannelBySID(sid, id_type) {
        return channelDatabaseManager.getChannelBySID(sid, id_type);
    }
    
    function _getChannelByTriplet(locator) {
        return channelDatabaseManager.getChannelByTriplet(locator);
    }

    function _getHiddenPromoChannelByTriplet(locator) {
        return channelDatabaseManager.getHiddenPromoChannelByTriplet(locator);
    }
    
    function _getChannelList(id) {
        return channelDatabaseManager.getChannels(id);
    }
    
    /**
     * change channel list
     * 
     * @param id
     * @param list - 전체 list를 변경하는 경우
     * @param obj - 특정 channel을 insert 하거나 remove 하는 경우
     *    ex) obj = { ccid : 111, type : 0, update : false }
     *    ie. type - 0 : insert / type - 1 : remove 
     */
    function _changeChannelList(id, list, obj) {
        channelDatabaseManager.changeChannelList(id, list, obj);
    }
    
    function _isAudioChannel(channel, include_audio_portal) {
        return channelDatabaseManager.isAudioChannel(channel, include_audio_portal);
    }
    
    function _isFavoriteChannel(channel) {
        return channelDatabaseManager.isFavoriteChannel(channel);
    }
    
    function _isBlockedChannel(channel) {
        return channelDatabaseManager.isBlockedChannel(channel);
    }
    
    function _isSkippedChannel(channel) {
        return channelDatabaseManager.isSkippedChannel(channel);
    }
    
    function _isDataServiceChannel(channel) {
        return channelDatabaseManager.isDataServiceChannel(channel);
    }
    
    function _isSkyChoiceChannel(channel) {
        return channelDatabaseManager.isSkyChoiceChannel(channel);
    }
    
    function _isSkyHDChannel(channel) {
        return channelDatabaseManager.isSkyHDChannel(channel);
    }

    function _isHiddenPromoChannel(channel) {
        return channelDatabaseManager.isHiddenPromoChannel(channel);
    }

    function _isKidsChannel (channel) {
        return channelDatabaseManager.isKidsChannel(channel);
    }

    /**
     * add channel list upate listener
     */
    function _addChannelListUpdateListener(listener) {
        channelDatabaseManager.addEventListener(listener);
    }
    
    /**
     * remove channel list update listener
     */
    function _removeChannelListUpdateListener(listener) {
        channelDatabaseManager.removeEventListener(listener);
    }
    
    /**
     * return current channel for main vbo
     * 
     * backward compatibility를 고려해서 current channel 획득 API를 그대로 유지함
     * 단, main vbo에 대한 current channel을 리턴하므로
     * pip의 경우 별도로 _getChannelControl(KTW.nav.Def.CONTROL.PIP).getCurrentChannel()을 이용해서 획득해야 한다.
     */
    function _getCurrentChannel() {
        return _getChannelControl(NAV_DEF_CONTROL.MAIN).getCurrentChannel();
    }
    
    /**
     * channel tune
     * 
     * backward compatibility를 고려해서 channel change API를 그대로 유지함
     * 단, main channel control 기준으로만 동작함.
     */
    function _changeChannel(channel, only_select, forced, block_event, block_osd) {
        _getChannelControl(NAV_DEF_CONTROL.MAIN).changeChannel(channel, only_select, forced, block_event, block_osd);
    }
    
    /**
     * stop channel
     * 
     * backward compatibility를 고려해서 stop channel API를 그대로 유지함
     * 단, main channel control 기준으로만 동작함.
     */
    function _stopChannel() {
        _getChannelControl(NAV_DEF_CONTROL.MAIN).stopChannel();
    }
    
    /**
     * release channel block (Parental Rating Block or Blocked channel)
     */
    function _releaseChannelBlock() {
        _getChannelControl(NAV_DEF_CONTROL.MAIN).releaseChannelBlock();
    }
    
    /**
     * channel update.
     * 2017.07.19 dhlee return value 추가
     */
    function _updateDIDatabase() {
    	return getExtendedChannelDB().updateDIDatabase();
    }

    function _requestFirmwareDownload() {
        return getExtendedChannelDB().requestFirmwareDownload();
    }

    /**
     * start audio and video component only
     * it must call after called stopVideoAndAudio function already
     */
    function _startAV() {
        if (media_control) {
            media_control.startVideoAndAudio();
        }
    }

    /**
     * stop audio and video component only
     * it's different vbo.stop()
     */
    function _stopAV() {
        if (media_control) {
            media_control.stopVideoAndAudio();
        }
    }
    
    /**
     * return main or pip, stitching channel control object
     * 생성되지 않았을 경우 한번만 object 생성 및 초기화 이후 재사용 한다. 
     * 단, PIP 또는 STITCHING type의 경우 무조건 object가 필요한 모듈에서는
     * 무조건 create 파라미터를 true로 해서 호출해야 한다. 
     * 
     * channel select/stop 및 components 및 closed caption 정보의 경우
     * 개별 channel control object 하위의 함수를 활용해서 획득한다.  
     * 
     * @param type - it representing channel control type for main, pip, stitching
     * @param create - if true, create object when object is null,
     *               otherwise return current channel control object 
     */
    function _getChannelControl(type, create) {
        
        if (KTW.utils.util.isValidVariable(type) === false) {
            return null;
        }
        
        switch(type) {
            case NAV_DEF_CONTROL.MAIN :
                if (mainChannelControl === null && create === true) {
                    mainChannelControl = createChannelControl(type);
                    mainChannelControl.init();
                    mainChannelControl.start();
                }
                return mainChannelControl;
            case NAV_DEF_CONTROL.PIP :
                if (pipChannelControl === null && create === true) {
                    pipChannelControl = createChannelControl(type);
                    pipChannelControl.init();
                }
                return pipChannelControl;
            case NAV_DEF_CONTROL.STITCHING :
                if (stitchingChannelControl === null && create === true) {
                    stitchingChannelControl = createChannelControl(type);
                    stitchingChannelControl.init();
                }
                return stitchingChannelControl;
        }
    }
    
    function _removeChannelControl(type) {
    	if (KTW.utils.util.isValidVariable(type) === false) {
            return null;
        }
        
        switch(type) {
            case NAV_DEF_CONTROL.MAIN :
            case NAV_DEF_CONTROL.PIP :
            	break;
            case NAV_DEF_CONTROL.STITCHING :
            	var vbo_div = document.getElementById("stitching_vbo_object");
            	if(vbo_div) vbo_div.remove();
            	stitchingChannelControl = null;
            	stitching_vbo = null;
            	break;
        }
    }
    
    /**
     * main/pip/stitching 별 channel control 생성
     * ChannelSelectionEventManager의 경우 main만 전달하도록 한다.
     * 
     * TODO pip vbo object 의 경우 특정 graphics 상위에 보여질 수 있기 때문에
     * STACK layer 보다 하위의 NORMAL layer를 생성하여
     * vbo 생성 시 전달될 수 있도록 한다.
     */
    function createChannelControl(type) {
        log.printDbg("createChannelControl(" + type + ")");
        
        var vbo = getVideoBroadcastObject(type);
        var ccc = oipfFactory.createClosedCaptionControl(vbo); 
        var csem_obj = (type === NAV_DEF_CONTROL.MAIN) ? getCSEM() : undefined;
        var ex_ch_db_obj = (type === NAV_DEF_CONTROL.MAIN) ? getExtendedChannelDB() : undefined;
        
        return new KTW.nav.ChannelControl(type, vbo, ccc, csem_obj, ex_ch_db_obj);
    }
    
    /******************************* OIPF object *******************************************/
    function getVideoBroadcastObject(type) {
        
        try {
            if (type === NAV_DEF_CONTROL.MAIN) {
                if (!main_vbo) {
                    main_vbo = oipfFactory.createVideoBroadcastObject(type);
                    log.printDbg("created VideoBroadcastObject(" + type + ")");
                }
            }
            else if (type === NAV_DEF_CONTROL.PIP) {
                if (!pip_vbo) {
                    pip_vbo = oipfFactory.createVideoBroadcastObject(type);
                    log.printDbg("created VideoBroadcastObject(" + type + ")");
                }
            }
            else if (type === NAV_DEF_CONTROL.STITCHING) {
                if (!stitching_vbo) {
                    stitching_vbo = oipfFactory.createVideoBroadcastObject(type);
                    log.printDbg("created VideoBroadcastObject(" + type + ")");
                }
            }
        } catch(e) {
            log.printErr("failed createVideoBroadcastObject(" + type + ")");
            log.printExec(e);
        }
        
        if (type === NAV_DEF_CONTROL.MAIN) {
            return main_vbo;
        }
        else if (type === NAV_DEF_CONTROL.PIP) {
            return pip_vbo;
        }
        else if (type === NAV_DEF_CONTROL.STITCHING) {
            return stitching_vbo;
        }
        
        return null;
    }
    
    function getChannelConfig(vbo) {
        if (!channel_config) {
            try {
                channel_config = vbo.getChannelConfig();
                log.printDbg("get ChannelConfigObject");
            } catch(e) {
                log.printErr("failed get VideoBroadcastObject#ChannelConfig");
                log.printExec(e);
            }
        }
        
        return channel_config;
    }
    
    function getExtendedChannelDB() {
        if (!ex_ch_db) {
            try {
                ex_ch_db = oipfFactory.createExtendedChannelDatabase();
                log.printDbg("created ExtendedChannelDatabase");
            } catch(e) {
                log.printErr("failed createExtendedChannelDatabase");
                log.printExec(e);
            }
        }
        
        return ex_ch_db;
    }
    
    function getCSEM() {
        if (!csem) {
            try {
                csem = oipfFactory.createChannelSelectionEventManager();
                log.printDbg("created ChannelSelectionEventManager");
            } catch(e) {
                log.printErr("failed createChannelSelectionEventManager");
                log.printExec(e);
            }
        }
        return csem;
    }
    /******************************* OIPF object *******************************************/
    
    return {
        init: _init,
        start: _start,
        pause: _pause,
        
        getPromoChannel: _getPromoChannel,
        getSkyPlusChannel: _getSkyPlusChannel,
        getSameKTChannel: _getSameKTChannel,
        isKidsCareChannel: _isKidsCareChannel,
        setFocusChannels: _setFocusChannels,
        isFocusChannel: _isFocusChannel,
        
        getChannelByNum: _getChannelByNum,
        getChannelByNumOnAll: _getChannelByNumOnAll,
        getChannelByCCID: _getChannelByCCID,
        getChannelBySID: _getChannelBySID,
        getChannelByTriplet: _getChannelByTriplet,
        getHiddenPromoChannelByTriplet: _getHiddenPromoChannelByTriplet,
        
        // channel list handling
        getChannelList: _getChannelList,
        changeChannelList: _changeChannelList,
        isAudioChannel: _isAudioChannel,
        isFavoriteChannel: _isFavoriteChannel,
        isBlockedChannel: _isBlockedChannel,
        isSkippedChannel: _isSkippedChannel,
        isDataServiceChannel: _isDataServiceChannel,
        isSkyChoiceChannel: _isSkyChoiceChannel,
        isSkyHDChannel: _isSkyHDChannel,
        isHiddenPromoChannel: _isHiddenPromoChannel,

        isKidsChannel: _isKidsChannel,
        
        addChannelListUpdateListener: _addChannelListUpdateListener,
        removeChannelListUpdateListener: _removeChannelListUpdateListener,
        
        getCurrentChannel: _getCurrentChannel,
        changeChannel: _changeChannel,
        stopChannel: _stopChannel,
        releaseChannelBlock: _releaseChannelBlock,
        
        updateDIDatabase: _updateDIDatabase,

        requestFirmwareDownload : _requestFirmwareDownload,

        getChannelControl: _getChannelControl,
        removeChannelControl: _removeChannelControl,

        /** MediaControlObject API */
        startAV: _startAV,
        stopAV: _stopAV
    }
}());