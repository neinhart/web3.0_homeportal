KTW.oipf.adapters.smiAdapter = (function() {
	var log = KTW.utils.Log;
	
	/**
	 * smiManager 는 multi instance.
	 */
	function _getInstance() {
		var smi_object;
		
	    function getSmiObject() {
	    	//내부가 singleton 형태로 구현됨.
	    	if(!smi_object){
	    		smi_object = KTW.oipf.oipfObjectFactory.createSMIManagerObject();
	    	}
	    	
	    	return smi_object;
	    }
	    
	    function _create() {
	    }
	    
	    function _prepare(url, element) {
            try {
                getSmiObject().prepare(url, element);
            } catch (e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#prepare");
                log.printExec(e);
            }

	    }
	    
	    function _play(vmo) {
            try {
                getSmiObject().play(vmo);
            } catch (e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#play");
                log.printExec(e);
            }

	    }
	    
	    function _stop() {
	    	try {
	    		getSmiObject().stop();
	    	} catch (e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#stop");
				log.printExec(e);
			}
	    }
	    
	    function _destroy() {
	    	try {
	    		getSmiObject().destroy();
	    	} catch (e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#destory");
	    		log.printExec(e);
	    	}
	    	
	    	smi_object = null;
	    }

        function _getState() {
            var ret = null;

            try {
                ret = getSmiObject().getState();
            } catch(e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#getState");
                log.printExec(e);
            }
            return ret;
        }

        function _getAvailableLanguages() {
            var ret = null;

            try {
                ret = getSmiObject().getAvailableLanguages();
            } catch(e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#getAvailableLanguages");
                log.printExec(e);
            }
            return ret;
        }

        function _setLanguage(index) {
            var ret = null;

            try {
                ret = getSmiObject().setLanguage(index);
            } catch(e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#setLanguage");
                log.printExec(e);
            }
            return ret;

        }

        function _resize() {

            try {
                getSmiObject().resize();
            } catch(e) {
                log.printWarn("[smi adapter] failed SMIManagerObejct#resize");
                log.printExec(e);
            }
        }

	    return {
	    	create:_create,
	    	prepare:_prepare,
	    	play:_play,
	    	stop:_stop,
	    	create:_create,
	    	destroy:_destroy,
	    	
	    	getState:_getState,
	    	getAvailableLanguages:_getAvailableLanguages,
	    	setLanguage:_setLanguage,
	    	
	    	resize:_resize
	    }
	};
	
	function _getSMIInfo(captionUrl, contsId) {
        var divide = captionUrl.indexOf("_");
        
        var ip = "";
        var language = [];
        if (divide === -1) {
            // 유효하지 않은 url인 경우 언어 정보를 공백으로 처리함
            // TODO 2017.02.16 dhlee 2.0 R7 적용
            // 유효하지 않은 경우 정보가 없는 채로 전달하여 자막 시나리오 동작하지 않도록 한다.
            //ip = captionUrl;
            //language[0] = "";
            return undefined;
        }

        ip = captionUrl.substring(0,divide);
        language = captionUrl.substring(divide+1).split("_");

        var url = ip + "/RequestWholeSubtitle.aspx?ContentID="+contsId
                  + "&STB=web&MODEL=" + KTW.oipf.adapters.HWAdapter.getModelName(); 
        
        return {"url":url,"language":language};
    }
	
	return {
		get STATE() {
    		return {
				PREPARED: 1,
				STARTED: 2,
				STOPPED: 3,
				DESTROYED: 0
			};
    	},
    	
    	getSMIInfo:_getSMIInfo,
		getInstance: _getInstance
	}
}());
