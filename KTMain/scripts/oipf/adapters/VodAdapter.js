"use strict";
KTW.oipf.adapters.vodAdapter = (function() {
	var log = KTW.utils.Log;
	var util = KTW.utils.util;
		
	var CALLBACK_PLAY_SPEED_CHANGE = "PlaySpeedChanged";
	var CALLBACK_READY_TO_PLAY = "ReadyToPlay";
	var CALLBACK_PLAY_POSITION_CHANGE = "PlayPositionChange";
	var CALLBACK_PLAY_STATE_CHANGE = "PlayStateChange";
	var CALLBACK_MEDIA_CONTENT_CHANGED = "MediaContentChanged";
    var CALLBACK_PLAYBACK_END_INPROGRESS = "PlaybackEndInProgress";
    var CALLBACK_VOD_STATE_CHANGE = "VodStateChanged";  // 2017.02/16 dhlee 2.0 r7
	
	var mpeg_object = null;
	var media_timeline_control_object = null;
    var vodmonitor_object = null;   // 2017.02.16 dhlee
	var listeners = {};
	var cached_play_time = 0;
	
	function _getMpegObject() {
    	//내부가 singleton 형태로 구현됨.
    	if(!mpeg_object) {
    		mpeg_object = KTW.oipf.oipfObjectFactory.createVideoMpegObject();
    	}        	
    	return mpeg_object;
    }

    /**
     * 2017.02.16 dhlee 2.0 R7 로직 추가
     *
     * @returns {*}
     */
    function _getVodMonitorObject(){
        if(!vodmonitor_object) {
            vodmonitor_object = KTW.oipf.oipfObjectFactory.createVODMonitorObject();
            log.printDbg("[vod adapter] _getVodMonitorObject :"+vodmonitor_object);
        }
        return vodmonitor_object;
    }
    
    function _getMediaTimelineObject() {    	
    	if(!media_timeline_control_object) {
    		log.printDbg("[vod adapter] _getMediaTimelineObject");
    		media_timeline_control_object = KTW.oipf.oipfObjectFactory.createMediaTimeLineControl(_getMpegObject());
    	}        	
    	return media_timeline_control_object;
    }
    
    function _clearMediaTimelineObject() {
    	log.printDbg("[vod adapter] _clearMediaTimelineObject");
    	media_timeline_control_object = null;
    }
    
    function _getADSInfo() {
    	/*var tmp = KTW.oipf.oipfObjectFactory.createMediaTimeLineControl(_getMpegObject());
    	var ret = tmp.getAdsInfo();
    	tmp.clear();
    	tmp = null;*/
    	var ret = null;
    	
    	try {
    		ret = _getMediaTimelineObject().getAdsInfo();
    		log.printDbg("[vod adapter] _getADSInfo ad_info:" + (ret ? log.stringify(ret) : ""));
    	} catch (e) {
    		log.printExec(e);
    	}
    	
    	return ret;
    }
    
    function _trickPlayPossible() {
    	return _getMediaTimelineObject().trickPlayPossible();
    }

    /**
     *
     * @param ad_info
     * @returns {boolean}
     */
    function _hasAdvertisement(ad_info) {
    	var existAd = false;
        if (ad_info && ad_info.indexOf(".mpg") > 0) {
			var temp = ad_info.split("]");
			for (var i=0; i<temp.length; i++) {
				log.printDbg("[vod adapter] has ad parsed:" + temp[i]);
				if (temp[i].length < 10)
					break;
				else
					existAd = true;
			}
        }
        
        return existAd;
    }

    /**
     * 현재 play 되고 있는 광고의 type 을 알려준다.
     *
     * @returns {*}
     */
    function _getCurrentAdvertType() {
        var ret = null;

        try {
            // TODO 2017.02.16 dhlee 2.0 R7 코드 추가
            // 성질급한 VOD Player 지원 여부 (lastTime 가변적임)
            // SUPPORTED
            var value = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.SUPPORT_QUICK_LIVE_VOD);

            if (value && value.toLowerCase() === "true") {
                ret = _getMediaTimelineObject().getCurrentAdvertType();
                log.printDbg("[vod adapter] getCurrentAdvertType :" + (ret ? log.stringify(ret) : ""));
            }
        } catch (e) {
            log.printWarn("[vod adapter] Cannot find 'getMediaTimelineObject().getCurrentAdvertType' function. "+
            "You should check getMediaTimelineObject at first");
        }

        return ret;
    }
    
    function _inner_onPlaySpeedChanged(speed) {
    	log.printDbg("[vod adapter _inner_onPlaySpeedChanged Callback] propagation callback. ");
    	
    	if(listeners[CALLBACK_PLAY_SPEED_CHANGE])
    		listeners[CALLBACK_PLAY_SPEED_CHANGE].notify(speed);
    }
    
    function _inner_onPlayPositionChanged(position) {
    	log.printDbg("[vod adapter _inner_onPlayPositionChanged Callback] propagation callback. ");
    	
    	if(listeners[CALLBACK_PLAY_POSITION_CHANGE])
    		listeners[CALLBACK_PLAY_POSITION_CHANGE].notify(position);
    }
    
    function _inner_onReadyToPlay() {
    	log.printDbg("[vod adapter _inner_onReadyToPlay Callback] propagation callback. ");
    	
    	if(listeners[CALLBACK_READY_TO_PLAY])
    		listeners[CALLBACK_READY_TO_PLAY].notify();
    }
    
    function _inner_onPlayStateChange(state,error,message) {
    	log.printDbg("[vod adapter _inner_onPlayStateChange Callback] propagation callback. ");
    	
    	if(listeners[CALLBACK_PLAY_STATE_CHANGE])
    		listeners[CALLBACK_PLAY_STATE_CHANGE].notify({'state':state,
    			                                          'error':error,
    			                                          'message':message});
    }
    
    //evt 8: 광고 시장 9: 광고 끝.
    function _inner_onMediaContentChanged(evt) {
    	log.printDbg("[vod adapter _inner_onMediaContentChanged Callback] propagation callback. ");
    	
    	if(listeners[CALLBACK_MEDIA_CONTENT_CHANGED])
    		listeners[CALLBACK_MEDIA_CONTENT_CHANGED].notify(evt);
    }

    function _inner_onPlaybackEndInProgress(evt){
        log.printDbg("[vod adapter _inner_onPlaybackEndInProgress Callback] propagation callback. ");

        if(listeners[CALLBACK_PLAYBACK_END_INPROGRESS])
            listeners[CALLBACK_PLAYBACK_END_INPROGRESS].notify(evt);
    }

    function _inner_onVODStateChanged(evt){
        log.printDbg("[vod adapter _inner_onVODStateChanged Callback] propagation callback. ");

        if(listeners[CALLBACK_VOD_STATE_CHANGE])
            listeners[CALLBACK_VOD_STATE_CHANGE].notify(evt);
    }
    
    function _init() {
    	//listener 정리.
    	_getMpegObject().onPlaySpeedChanged = _inner_onPlaySpeedChanged;
    	_getMpegObject().onPlayPositionChanged = _inner_onPlayPositionChanged;
    	_getMpegObject().onReadyToPlay = _inner_onReadyToPlay;
    	_getMpegObject().onPlayStateChange = _inner_onPlayStateChange;
    	//_getMpegObject().onFullScreenChange; 이건 안쓰임. 제거.
    	
    	//vmo 도 1개 임으로 여기서 mtl 도 1개만 만들자.
    	_getMediaTimelineObject().onMediaContentChanged = _inner_onMediaContentChanged;

        // 2016.12.06 dhlee 성질급한 VOD EndEvt
        // by kingsae1
        _getMpegObject().onPlaybackEndInProgress = _inner_onPlaybackEndInProgress;
    	
    	_resizeVODScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);
    }
    
    function _getVODScreenSize() {
    	return {
    		'x':_getMpegObject().x,
    		'y':_getMpegObject().y,
    		'width':_getMpegObject().width,
    		'height':_getMpegObject().height
    	}
    }
    
    function _resizeVODScreen(x, y, w, h) {
    	_getMpegObject().x = x ? x : 0;
    	_getMpegObject().y = y  ? y : 0;
    	_getMpegObject().width = w  ? w : 0;
    	_getMpegObject().height = h  ? h : 0;
    	
    	// VOD 재생 중 smi 자막이 존재하는 영상의 경우
        // AV resize 될 때 의도적으로 호출해 준다. 
        //try {
        //    KTW.vod.VodControl.resizeCaption(x, y, w, h);
        //}
        //catch(e) {
        //    log.printExec(e);
        //}
    	
    	if(w == KTW.CONSTANT.RESOLUTION.WIDTH && h == KTW.CONSTANT.RESOLUTION.HEIGHT) {
    		_getMpegObject().setFullScreen(true);
    	} else {
    		_getMpegObject().setFullScreen(false);
    	}
    }
    
    function _setFullScreen(f) {
    	_getMpegObject().setFullScreen(f);
    }
    
    function _setPlayUrl(url) {
    	_getMpegObject().data = url;
    }
    
    function _getPlayUrl() {
    	return _getMpegObject().data;
    }
    
    function _play(speed) {
    	var ret = _getMpegObject().play(speed);
    	
    	log.printDbg("[vod adapter]_play ret:" + ret);
    	
    	return (ret != undefined ? ret : true);
    }
    
    /**
     * comparibility for lgs. 
     */
    function _pause() {
    	return _play(0);
    }
    
    function _resume() {
    	return _play(1);
    }
    
    function _setPlaySpeed(speed) {
    	var ret = _getMpegObject().play(speed);
    	
    	log.printDbg("[vod adapter]_setPlayspeed ret:" + ret);
    	
    	return (ret != undefined ? ret : true);
    }
    
    function _getPlaySpeed() {
    	return _getMpegObject().speed;
    }
    
	function _seek(position) {
		var ret = _getMpegObject().seek(position);
		
		log.printDbg("[vod adapter]_seek ret:" + ret);
		
		return (ret != undefined ? ret : true);
	};

	function _stop() {
		var ret = _getMpegObject().stop();

		log.printDbg("[vod adapter]_stop ret:" + ret);
		
		return (ret != undefined ? ret : true);
	};

	function _getPlayTime() {
		var tmp_Time = _getMpegObject().playTime;
		
		if(tmp_Time != 0)
			cached_play_time = tmp_Time;
		
		log.printDbg("[vod adapter] playtime:" + (tmp_Time == 0) ? cached_play_time : tmp_Time);
		
		return (tmp_Time == 0) ? cached_play_time : tmp_Time;
	};
	
	function _getPlayPosition() {
		return _getMpegObject().playPosition;
	};
	
	function _getComponents(type) {
		return _getMpegObject().getComponents(type);
	};

	function _selectComponent(component, device) {
		return _getMpegObject().selectComponent(component, device);
	};
	
	function _unselectComponent(component, device) {
		return _getMpegObject().unselectComponent(component, device);
	};
	
	function _getCurrentActiveComponents(component, device) {
		return _getMpegObject().getCurrentActiveComponents(component, device);
	};
	
	function _getPlayState() {
		return _getMpegObject().playState;
	};
	
	function _isPlayerRunning() {
		/**
		 * 광고에 대한 고려 필요. 
		 */
		switch(_getMpegObject().playState) {
			case KTW.oipf.Def.MPEG.PLAY_STATE_TYPE.PAUSE:
			case KTW.oipf.Def.MPEG.PLAY_STATE_TYPE.PLAY:
			case KTW.oipf.Def.MPEG.PLAY_STATE_TYPE.BUFFERING:
			case KTW.oipf.Def.MPEG.PLAY_STATE_TYPE.CONNECTING:
					return true;
		}
		return false;
	};
	
	function _isPlayerPlaying() {
		switch(_getMpegObject().playState) {
			case KTW.oipf.Def.MPEG.PLAY_STATE_TYPE.PLAY:
				return true;
		}
		return false;
	};
		
	function _addListener(name, listener) {
		if(name && !listeners[name]) {
			listeners[name] = new KTW.utils.Listener();
		}
		
		if(name && listener && listeners[name]) {
			listeners[name].addListener(listener);
			return true;
		}
		return false;
	};
	
	function _removeListener(name, listener) {
		if(name && listeners[name]) {
			listeners[name].removeListener(listener);
			return true;
		}
		
		return false;
	};
	
	function _addPlaySpeedChangeListener(l) {
		return _addListener(CALLBACK_PLAY_SPEED_CHANGE, l);
	}
	function _removePlaySpeedChangeListener(l) {
		return _removeListener(CALLBACK_PLAY_SPEED_CHANGE, l);
	}
	
	function _addReadyToPlayListener(l) {
		return _addListener(CALLBACK_READY_TO_PLAY, l);
	}
	function _removeReadyToPlayListener(l) {
		return _removeListener(CALLBACK_READY_TO_PLAY, l);
	}
	
	function _addPlayPositionChangeListener(l) {
		return _addListener(CALLBACK_PLAY_POSITION_CHANGE, l);
	}
	function _removePlayPositionChangeListener(l) {
		return _removeListener(CALLBACK_PLAY_POSITION_CHANGE, l);
	}
	
	function _addPlayStateChangeListener(l) {
		return _addListener(CALLBACK_PLAY_STATE_CHANGE, l);
	}
	function _removePlayStateChangeListener(l) {
		return _removeListener(CALLBACK_PLAY_STATE_CHANGE, l);
	}
	
	function _addMediaContentChangeListener(l) {
		return _addListener(CALLBACK_MEDIA_CONTENT_CHANGED, l);
	}
	
	function _removeMediaContentChangeListener(l) {
		return _removeListener(CALLBACK_MEDIA_CONTENT_CHANGED, l);
	}

    /**
     * TODO 2017.02.16 dhlee 주석처리 되어 있는데 2.0 로직을 살펴보면 실제 사용을 하고 있음. R7에서..
     * 어떻게 해야 할지 추후 정리해야 함.(분석도 필요하고)
     *
     * @param l
     * @returns {*}
     * @private
     */
    //function _stopPlayer(mode, param) {
    //    switch(mode) {
    //        case OTW.vod.Def.EXIT_TYPE.EXIT_FORCE_RESPONSE:
    //            OTW.vod.VodInterface.terminate(/*param.noResponse*/false);
    //            break;
    //
    //        case OTW.vod.Def.EXIT_TYPE.EXIT_FORCE_DCA:
    //            var last_layer = OTW.ui.LayerManager.getLastStackLayer(true);
    //            if (util.isValidVariable(last_layer) === true) {
    //                OTW.ui.LayerManager.deactivateLayer(last_layer.id, true, true);
    //            }
    //
    //            OTW.vod.VodInterface.terminate(/*param.noResponse*/true, true);
    //            break;
    //
    //        case OTW.vod.Def.EXIT_TYPE.EXIT_FORCE:
    //            OTW.vod.VodInterface.terminate(/*param.noResponse*/true);
    //            break;
    //
    //    /**
    //     * channel : 돌아갈 채널 객체
    //     * uiAction : OTW.vod.Def.EXIT_UI_ACTION 참고 [none, clearstack, showwindow]
    //     * dialogType : OTW.vod.Def.EXIT_DIALOG_TYPE.[NONE, RELATION] (연관 VOD 를 보여줄지 말지 여부)
    //     * callback : 모든 동작이 끝난 후 callback 을 처리.
    //     */
    //        case OTW.vod.Def.EXIT_TYPE.EXIT_DIALOG:
    //            OTW.vod.VodInterface.confirmVod(param.channel, param.uiAction, param.dialogType, param.callback);
    //            break;
    //        case OTW.vod.Def.EXIT_TYPE.EXIT_NORMAL:
    //            OTW.vod.VodControl.stopVod(param);
    //            break;
    //    }
    //}

    function _addPlaybackEndInProgressListener(l){
        return _addListener(CALLBACK_PLAYBACK_END_INPROGRESS, l);
    }

    function _removePlaybackEndInProgressListener(l) {
        return _removeListener(CALLBACK_PLAYBACK_END_INPROGRESS, l);
    }

    function _addVODStateChangeListener(l){
        return _addListener(CALLBACK_VOD_STATE_CHANGE, l);
    }

    function _removeVODStateChangeListener(l){
        return _removeListener(CALLBACK_VOD_STATE_CHANGE, l);
    }

    /**
     * 성질급한 VOD 지원 여부 리턴
     *
     * @returns {boolean}
     */
	function _isSupportQuickLiveVod() {
        var val = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.SUPPORT_QUICK_LIVE_VOD);
        
        log.printDbg("live_vod_support:" + val);
        
        if (val && val.toLowerCase() === "true") {
        	return true;
        }
        
        if (KTW.QUICK_VOD_SUPPORT_TEST_CODE === true) {
        	return true;
        }
        
        return false;
	}

	function _updateOTMParing(cust_env) {
		//여기서 Paring 여부 upate 해 주어야 한다.
		log.printDbg("[Seamless/Paring] paringYn:" + (cust_env && cust_env.paringYn));

		if(cust_env && cust_env.paringYn) {
			var storageManager = KTW.managers.StorageManager;
			var paring_yn = cust_env.paringYn;
			//update paring YN.
			if(paring_yn === "Y" || paring_yn === "N") {
				storageManager.ps.save(storageManager.KEY.OTN_PARING, paring_yn);
			}
		}
	}

	function _isOTMParing(callback, obj){
		var storageManager = KTW.managers.StorageManager;
		var is_paring = storageManager.ps.load(storageManager.KEY.OTN_PARING);

		var ret = false;

		if (is_paring === 'Y') {
			ret = true;
		} else {
			ret = false;
		}

		log.printDbg("[Seamless/Paring] is_paring: " + is_paring);

		if(callback)
			callback(ret, obj);
		return ret;
	};
	
	//function _changeExtVodViewVisible(visible) {
	//	KTW.vod.VodControl.changeCaptionVisible(visible);
	//	KTW.oipf.adapters.watermarkAdapter.changeVisible(visible);
	//}
	
	return {
		init:_init,
		
		/**
		 * 실제 middleware 에서 넘겨주는 object 로 처리.
		 */
		getMpegObject: _getMpegObject,
		getRealMpegObject: function() {
			/**
			 * 이렇게 한 이유는 단 하나, 
			 * vmo object interface 의 로그를 보고 싶어서임. 
			 * 실제 미들웨어의 object 를 바로 받는곳은 smiManager 뿐임. 
			 */
			return _getMpegObject().vmo;
		},
        _getMediaTimelineObject : _getMediaTimelineObject,
		
		//oipf mpeg object interface
		getVODScreenSize:_getVODScreenSize,
		resizeVODScreen:_resizeVODScreen,
		setFullScreen:_setFullScreen,
		
		setPlayUrl:_setPlayUrl,
		getPlayUrl:_getPlayUrl,
		play:_play,
		pause:_pause,
		resume:_resume,
	    seek:_seek,
	    stop:_stop,
	    setPlaySpeed:_setPlaySpeed,
	    getPlaySpeed:_getPlaySpeed,
	    getPlayTime:_getPlayTime,
	    getPlayPosition:_getPlayPosition,
	    getPlayState:_getPlayState,
	    isPlayerRunning:_isPlayerRunning,
	    isPlayerPlaying:_isPlayerPlaying,
	    
	    getComponents:_getComponents,
	    selectComponent:_selectComponent,
	    unselectComponent:_unselectComponent,
	    getCurrentActiveComponents:_getCurrentActiveComponents,
	    
	    addPlaySpeedChangeListener: _addPlaySpeedChangeListener,
		removePlaySpeedChangeListener: _removePlaySpeedChangeListener, 
		
		addReadyToPlayListener: _addReadyToPlayListener, 
		removeReadyToPlayListener: _removeReadyToPlayListener, 
		
		addPlayPositionChangeListener: _addPlayPositionChangeListener, 
		removePlayPositionChangeListener: _removePlayPositionChangeListener, 
		
		addPlayStateChangeListener: _addPlayStateChangeListener, 
		removePlayStateChangeListener: _removePlayStateChangeListener, 
		
		addMediaContentChangeListener: _addMediaContentChangeListener, 
		removeMediaContentChangeListener: _removeMediaContentChangeListener, 
		
		//addOnPlayerEndListener: function(l){
		//	KTW.vod.VodControl.addOnPlayerEndListener(l);
		//},
		//
		//removeOnPlayerEndListener: function(l) {
		//	KTW.vod.VodControl.removeOnPlayerEndListener(l);
		//},
	
	    //MediaTimeControl 
	    getADSInfo:_getADSInfo,
	    hasAdvertisement:_hasAdvertisement,
	    trickPlayPossible:_trickPlayPossible,
        getCurrentAdvertType: _getCurrentAdvertType,    // 현재 광고 타입을 얻는다. (R7)
	    
	    //Seamless Paring. 
	    isOTMParing:_isOTMParing,
		updateOTMParing:_updateOTMParing,
	    
	    //vodControl Interface.
	    //stopPlayer: _stopPlayer,
	    
	    //_isSupportQuickLiveVod 성질급한 VOD R4 요구사항 처리를 위한 Player 적용 여부.
	    isSupportQuickLiveVod:_isSupportQuickLiveVod,
	    
	    //hp_hideOtherApp 대응 API. 
	    //vod 에서 미들웨어나 oipf 를 통해 그리는 view 의 visible 변경 (ex: 자막/워터마크) 
	    //changeExtVodViewVisible:_changeExtVodViewVisible

        // 2016.12.06. dhlee [R7] 성급한 VOD End event
        addPlaybackEndInProgressListener : _addPlaybackEndInProgressListener,
        removePlaybackEndInProgressListener : _removePlaybackEndInProgressListener,

        // [R7] 외부 Application이 현재 VOD의 status를 모니터링 할 수 있다.
        // by kingsae1
        addVODStateChangeListener 		: _addVODStateChangeListener,
        removeVODStateChangeListener 	: _removeVODStateChangeListener

    };
}());
