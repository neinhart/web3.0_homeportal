"use strict";
KTW.oipf.adapters.ProgramSearchAdapter = (function() {
	var log = KTW.utils.Log;
    var oipfFactory = KTW.oipf.oipfObjectFactory;
    
	var search_manager_obj;
	
	var DEFAULT_SEARCH_COUNT = 200;
	
	var PROGRAM_GENRE_CODE = {
			MOVIE_DRAMA: "1_,",
			NEW_AFFAIR: "2_,",
			SHOW_GAME: "3_,",
			SPORTS: "4_,",
			CHILDREN: "5_,",
			MUSIC: "6_,",
			ART_CULTURE: "7_,",
			SOCIAL_ECONOMICS: "8_,",
			EDUCATION_SCIENCE: "9_,",
			LEISURE_HOBBIES: "A_,",
			ETC: "B_,"
	};
	
	var SEARCH_TARGET = {
			SCHEDULED_CONTENT:1,
			ON_DEMAND_CONTENT:2
	};
	
	var QUERY_COMPARASION = {
			EQ: 0, // "="
			NE: 1, // "!="
			GT: 2, // ">"
			GE: 3, // ">="
			LT: 4, // "<"
			LE: 5, // "<="
			CONTAIN: 6, //case insensitive (ignore case), Terminator is equal with term. 
			EXIST: 7,
			getString:function(comparasion) {
				switch(comparasion) {
				case QUERY_COMPARASION.EQ:
					return "==";
				case QUERY_COMPARASION.NE:
					return "!=";
				case QUERY_COMPARASION.GT:
					return ">";
				case QUERY_COMPARASION.GE:
					return ">=";
				case QUERY_COMPARASION.LT:
					return "<";
				case QUERY_COMPARASION.LE:
					return "<=";
				case QUERY_COMPARASION.CONTAIN:
					return "contain";
				case QUERY_COMPARASION.EXIST:
					return "exist";
				}
			}
	};

	function getSearchManager() {
    	if (!search_manager_obj) {
    		try {
    			search_manager_obj = oipfFactory.createSearchManagerObject();
    			log.printDbg("created searchManagerObject");
    		} catch(e) {
    			log.printExec(e);
    		}
    	}
    	
    	return search_manager_obj;
    };
	
	function SearchObject() {
    	var videoSearchManagerObject = getSearchManager();
    	
    	var channel;
    	var genre;
		var search_count;
		var search_offset;
    	var callback;
    	var orderby;
    	
    	var query_array = [];
    		
    	function getSearch() {
    		if (videoSearchManagerObject) {
    		    return videoSearchManagerObject.createSearch(SEARCH_TARGET.SCHEDULED_CONTENT);
    		}
    		return null;
    	}
    	
    	function setSearchOffset(offset) {
    		search_offset = offset;
    	}
    	function getSearchOffset() {
    		return (search_offset ? search_offset : 0);
    	}
    	          	
    	function setSearchCount(count) {
    		search_count = count;
    	}    	
    	function getSearchCount() {
    		return search_count ? search_count : DEFAULT_SEARCH_COUNT;
    	}    
    	
    	function setCallback(cb) {
    		callback = cb;
    	}
    	
    	function setChannel(ch) {
    		channel = ch;
    	}
    	
    	function setOrdering(order) {
    		orderby = order;
    	}
    	    	
    	/**
    	 * @param comparasion
    	 *	QUERY_COMPARASION <- 여기 참조.
    	 */
    	function addQuery(conjunction, field, comparasion, value) {
    		var search_obj = getSearch();
    		
    		log.printDbg("[SearchManager] addQuery " + (query_array.length != 0 ? conjunction : "") + " [" + field + " " + QUERY_COMPARASION.getString(comparasion) + " " + value + "]");
    		
    		if(search_obj) {
    			var query_statement = search_obj.createQuery(field, comparasion, value);
    			
    			if(query_array.length == 0) {
        			query_array.push({conjunction:undefined, query:query_statement});
        			
        		}else{
        			query_array.push({conjunction:conjunction, query:query_statement});
        		}
    		}    		
    	}
    	
    	function getResult() {
    		var search_obj = getSearch();
    		
    		var query_current;
    		var result;
    		
    		if (search_obj) {
    		    for(var i = 0 ; i < query_array.length ; i++) {
                    if(i == 0) {
                        query_current = query_array[i].query;
                    } else {
                        var query = query_array[i].query;
                        switch (query_array[i].conjunction.toUpperCase()) {
                            case "AND":
                                query_current = query_current.and(query);
                                break;
                            case "OR":
                                query_current = query_current.or(query);
                                break;
                            default:
                                KTW.utils.Log.printErr("ERROR - Query No Conjunction !!");
                                break;
                        }
                    }   
                }
                
                //linked list 로 연결됨.
                search_obj.setQuery(query_current);
                
                /**
                 * orderBy( field, ascending );
                 */
                if(orderby && orderby == "N") {
                    search_obj.orderBy("programme.name", true);
                }
                else if(orderby && orderby == "T") {
                	// 시간이 동일한 경우 channel number 순으로 정렬
                    search_obj.orderBy("programme.startTime", true);
                    search_obj.orderBy("majorChannel", true);
                }
                else {
                    search_obj.orderBy("programme.startTime", true);
                    search_obj.orderBy("majorChannel", true);
                }
                
                //mook_t ?
                if(callback) {
                    videoSearchManagerObject.onMetadataSearch = callback;
                }
                
                /**
                 * addChannelConstraint( channelList );
                 * addChannelConstraint( channel );
                 * . 반복된 add 는 channel 이 계속 추가 됨. null 은 다 지움. 
                 * . 해당 channel 내에서 검색.
                 */
                if(channel) {
                    search_obj.addChannelConstraint(channel);
                }
                
                log.printDbg("[SearchManager] getResult orderby:" + orderby);
                log.printDbg("[SearchManager] getResult search count:" + getSearchCount());
                log.printDbg("[SearchManager] getResult search offset:" + getSearchOffset());
                
                if(channel && channel instanceof Array) {
                    for(var i = 0 ; i < channel.length ; i++) {
                        log.printDbg("[SearchManager] getResult channel:" + channel[i].name);
                    }
                } else if (channel != null) {
                    log.printDbg("[SearchManager] getResult channel:" + channel && channel.name);
                } else {
                	log.printDbg("[SearchManager] getResult channel is null!");
                }
                
                result = search_obj.result;
                try {
                    result.getResults(getSearchOffset(), getSearchCount());
                }
                catch (e) {
                    log.printErr(e.message);
                }
    		}
    		
	        return result;
    	}
    	
    	this.setSearchCount = setSearchCount;
    	this.setSearchOffset = setSearchOffset;
    	this.setChannel = setChannel;
    	this.setCallback = setCallback;
    	this.setOrdering = setOrdering;
    	this.addQuery = addQuery;
    	this.getResult = getResult;
    }
	
	function isSkyChannel(channel) {
		if(channel) {
			//mook_t channel 객체에 있다는데 확인해 보자. 
			
			if(channel.idType && channel.idType == KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S)
    			return true;
		}
		
		return false;
	};
	
	function getStartTime(time, channel) {
		var currentTimeMs = new Date();
		var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
		
		if(time) {
			currentTimeMs = new Date(time);
		} else {
			if(KTW.CONSTANT.IS_OTS || !navAdapter.isAudioChannel(channel)) {
				if(currentTimeMs.getMinutes() < 30) {
					currentTimeMs.setMinutes(0);
	            } else {
	            	currentTimeMs.setMinutes(30);
	            }
			}
		}
		
		return (currentTimeMs.getTime() / 1000);
	};
	
	function getTime(time) {
		var currentTimeMs = 0;
		
		if(time) {
			currentTimeMs = new Date(time);
			
			return (currentTimeMs.getTime() / 1000);
		} else {
			currentTimeMs = new Date();
			
			return (currentTimeMs.getTime() / 1000);
		}
		
		return 0;
	};
	
	function getDefaultEndTime(startTime, is_sky_channel) {
	    var day = 1;
		if (is_sky_channel) {
			day = 3;
		}
		return startTime + (60 * 60 * 24 * day);
	};
		
	/**
	 * @param start: ms, 시작 시간.
	 * @param end: ms, 종료 시간.
	 * @param genre: 장르 code. 
	 * @channels: constraint channel. 
	 * @count: 검색 결과 max count. 
	 * @ordering: T: 시간(default) N: name. 
	 * @callback: callback to run. 
	 */
	function _getPrograms(start, end, genreCode, channel, count, ordering, callback) {
		log.printDbg("[SearchManager] _getPrograms start:" + start + " end:" + end + " genreCode:" + genreCode + " count:" + count + " ordering:" + ordering);
		
		var search_obj = new SearchObject();
		var result;
        
        if (search_obj) {
            if (channel) search_obj.setChannel(channel);
            if (count) search_obj.setSearchCount(count);
            if (ordering) search_obj.setOrdering(ordering);
            if (callback) search_obj.setCallback(callback);
            
            var start_time = getStartTime(start);
            
            if (start_time) {
                search_obj.addQuery("and", "programme.startTime+programme.duration", QUERY_COMPARASION.GE, start_time);
            }
            
            if (end) {
                search_obj.addQuery("and", "programme.startTime", QUERY_COMPARASION.LT, getTime(end));
            } else {
                search_obj.addQuery("and", "programme.startTime", QUERY_COMPARASION.LT, getDefaultEndTime(start_time, isSkyChannel((channel instanceof Array) ? channel[0] : channel)));
            }
            
            if (genreCode) {
                search_obj.addQuery("and", "programme.nibbles", QUERY_COMPARASION.CONTAIN, genreCode);
            }
            
            result = search_obj.getResult();
        }
    	
    	return result;
	};
	
	/**
	 * @param start: ms, 시작 시간.
	 * @param duration : ms, 시작 시간 부터 + 시간 .
	 * @channels: constraint channel. 
	 * @ordering: T: 시간(default) N: name.
	 */
	function _getPrograms2(start, duration, channel, max_count, ordering) {
		return _getPrograms(start, start + duration, undefined, channel, max_count, ordering); 
	}
	
	function _getGenreProgramList(chList, currentTimeInSec, genreCode, callback, ordering) {
		log.printDbg("[SearchManager] _getGenreProgramList currentTimeInSec:" + currentTimeInSec + " genreCode:" + genreCode + " ordering:" + ordering);
		
		var search_obj = new SearchObject();
		var result;
		
		if (search_obj) {
		    if (chList) search_obj.setChannel(chList);
	        search_obj.setSearchCount(999);
	        if (ordering) search_obj.setOrdering(ordering);
	        if (callback) search_obj.setCallback(callback);
	        
	        if (!currentTimeInSec) {
	            currentTimeInSec =  getTime();
	        }
	        
	        if (genreCode) {
	            search_obj.addQuery("and", "programme.nibbles", QUERY_COMPARASION.CONTAIN, genreCode);
	        }
	        search_obj.addQuery("and", "programme.startTime+programme.duration", QUERY_COMPARASION.GE, currentTimeInSec);
	        search_obj.addQuery("and", "programme.startTime", QUERY_COMPARASION.LT, currentTimeInSec + (60*60*12));
	        
	        result = search_obj.getResult();
		}
		
    	
    	return result;
	};
	
	function _doSearch(channel, field, comparasion, value, offset, count, callback) {
		log.printDbg("[SearchManager] _doSearch field:" + field + " comparasion:" + comparasion + " value:" + value + " offset:" + offset + " count:" + count);
		
		var search_obj = new SearchObject();
		var result;
		
		if (search_obj) {
		    if (channel) search_obj.setChannel(channel);
	        if (count) search_obj.setSearchCount(count);
	        if (offset) search_obj.setSearchOffset(offset);
	        if (callback) search_obj.setCallback(callback);
	        
	        search_obj.addQuery("and", field, comparasion, value);
	        
	        result = search_obj.getResult();
		}
		
    	return result;
	};
	
	// inteface
	return {
		// ReadOnly. property.
		get PROGRAM_GENRE() {
			return PROGRAM_GENRE_CODE;
		},
		
		get COMPARASION() {
			return QUERY_COMPARASION;
		},
		
		getSearchManager: getSearchManager,
		
		/*getSearchResult:_getSearchResult,
		getSearchResult2:_getSearchResult2,*/
		
		getPrograms: _getPrograms, //endtime 으로.
		getPrograms2: _getPrograms2, //duration 으로.
		getGenreProgramList: _getGenreProgramList,
		
		doSearch: _doSearch
	};
}());
        			
/*
 * 이전 코드
function _getSearchResult(ch, isSkyRing, durationTime, callback, searchCount, startTime) {
	var search = null;

	if (!searchCount) {
		searchCount = 200;
	}
	if (durationTime == 0) {
		if (isSkyRing) {
			durationTime = 24 * 3;
		} else {
			durationTime = 24;
		}
	}
	
	var videoSearchManagerObject = getSearchManager();
	
	if (videoSearchManagerObject) {
		search = videoSearchManagerObject.createSearch(1);
		// log(LOG_LEVEL_DEBUG,"create search !!");
		// if(!fromTimeInSec || fromTimeInSec == 0){
		var currentTime = new Date();
		if (startTime) {
			currentTime = new Date(startTime);
		} else {
			if (!KTW.CONSTANT.IS_OTS && isOTVAudioChannel(ch)) {

			} else {
				if (currentTime.getMinutes() < 30) {
					currentTime.setMinutes(0);
				} else {
					currentTime.setMinutes(30);
				}
			}
		}

		var fromTimeInSec = currentTime.getTime() / 1000;

		// }
		// if(!toTimeInSec || toTimeInSec == 0){
		var toTimeInSec = fromTimeInSec + 60 * 60 * durationTime;
		// }

		KTW.utils.Log.pringDbg(ch.name + " : " + (new Date(fromTimeInSec * 1000)) + " : " + (new Date(toTimeInSec * 1000)));

		var qry2 = search.createQuery("programme.startTime+programme.duration", 3, fromTimeInSec);
		var qry = qry2.and(search.createQuery("programme.startTime", 4, toTimeInSec));

		search.setQuery(qry);
		if (callback) {
			videoSearchManagerObject.onMetadataSearch = callback();
		}

		search.addChannelConstraint(ch);

		var result = search.result;
		result.getResults(0, searchCount);

		return result;
	}

	return null;
}



function _getSearchResult2(channels, isSkyRing, durationTime, callback, searchCount, startTime) {
	var search = null;
	
	if(!searchCount){
		searchCount = 200;
	}
	
	if(durationTime == 0){
		if(isSkyRing){
			durationTime = 24*3;
        }else{
        	durationTime = 24;
        }
	}
	
	var videoSearchManagerObject = getSearchManager();
	
	if (videoSearchManagerObject) {
		search = videoSearchManagerObject.createSearch(1);
		// log(LOG_LEVEL_DEBUG,"create search !!");
		// if(!fromTimeInSec || fromTimeInSec == 0){
		var currentTime = new Date();
		if(startTime){
			currentTime = new Date(startTime);
		}else{
			if(!KTW.CONSTANT.IS_OTS && isOTVAudioChannel(channels[0])){
				
			}else{
				if(currentTime.getMinutes() < 30){
	            	currentTime.setMinutes(0);
	            }else{
	            	currentTime.setMinutes(30);
	            }
			}
		}
		
        
        var fromTimeInSec = currentTime.getTime() / 1000;            
        var toTimeInSec = fromTimeInSec + 60*60*durationTime;
        
		var qry2 = search.createQuery("programme.startTime+programme.duration", 3, fromTimeInSec);
		var qry = qry2.and(search.createQuery("programme.startTime", 4, toTimeInSec));

	    search.setQuery(qry);
	    
	    if(callback){
	    	videoSearchManagerObject.onMetadataSearch = callback();
	    }
	    
	    search.addChannelConstraint(channels);
	    var result = search.result;
        result.getResults(0, searchCount);

        return result;
	}
	
	return null;
};

function _doSearch(channel, field, comparasion, value, offset, count, callback) {
	var search = null;
	
	var videoSearchManagerObject = getSearchManager();
	
	if (videoSearchManagerObject)
		search = videoSearchManagerObject.createSearch(1);

	if (search) {
		var myQuery = search.createQuery(field, comparasion, value);
	    
        search.setQuery(myQuery);

        if(callback)
        	videoSearchManagerObject.onMetadataSearch = callback();
        
		search.addChannelConstraint(channel);
		
		// search.result.getResults(offset, count)
		// if (search.result.getResults(offset, count)) {
        // some results are available immediately, e.g. because they were cached do stuff with the results
		var result = search.result;
        result.getResults(offset, count);
        return result;
		// }
	}
	
	return undefined;
}*/