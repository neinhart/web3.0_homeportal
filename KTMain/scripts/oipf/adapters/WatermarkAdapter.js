/**
 * jm.kang. 
 * watermark 모듈을 모아둠. 
 * 
 * 시간나면 이 부분을 watermarkAdapter 와 watermarkControl 로 분리하자. 
 * 실제 API 부와 Control 처리부를 분리.할것.
 *
 * 2017.02.22 dhlee
 * TODO 3.0 에서는 VOD 모듈이 별도로 개발되므로 정리가 필요하다. VodControl reference 하는 부분은 일단 주석 처리함.
 */
"use strict";
KTW.oipf.adapters.watermarkAdapter = (function() {
	var log = KTW.utils.Log;
	var util = KTW.utils.util;
	
	//var vodControl;
    var vodAdapter;
    var stateManager;
    var hwAdapter;
    var ocManager = KTW.managers.data.OCManager;
	
	var STATE = {
			WM_IDLE: -1,
			WM_PREPARED: 0,
			WM_INITED: 1,
			WM_RUNNING: 2,
			WM_PAUSED: 3,
			WM_HIDE: 4
	};
	
	var wm_object;
	var wm_properties;
	var wm_pos = {x:0 , y:0};
	var wm_state = STATE.WM_IDLE;
	
	var media_time;
	var age;
	var watermark_type;
	var is_logo_only_mode = false;
	
	/**
	    ##WM default 운용모드 (로고형:O, 비가시적:O)
		WM_DEFAULT_OPERATING_MODE=1
		
		WM_DEFAULT_OPERATING_MODE (워터마크 출력 설정 인듯)
		0 : watermark disable (가시/비가시 모두 출력 안함)
		1 : 로고 only X , 로고with FM O  / 비가시 출력 O
		2 : 로고 only X , 로고with FM O  / 비가시 출력 X
		3 : 로고 only X , 로고with FM X  / 비가시 출력 O
		4 : 로고 only O , 로고with FM X  / 비가시 출력 O
		5 : 로고 only O , 로고with FM X  / 비가시 출력 X

		##로고형FM 속성
		#WM 기능 동작 여부 (0: 동작안함, 1: 동작함)
		#Enable=1
		LGFM_ENABLED=1
		#시작시간(초) : Ts
		#StartTime=300
		LGFM_START_TIME=300
		#WM logo 출력 주기 : Te
		#Repeat=2
		LGFM_REPEAT=2
		#노출 시간 간격(초) : Tp
		#GapTime=600
		LGFM_GAP_TIME=600
		#임의 노출 시간(초) : ?? 시작시간 임의로 조절
		#RandomTime=0
		LGFM_RANDOM_TIME=0
		#로고별 출력 시간 (milli sec) : Tu
		#DisplayTime=200
		LGFM_DISPLAY_TIME=200
		#ALL 시청등급 ON/OFF (0: OFF, 1: ON) : 신규추가 
		LGFM_ALL_ONOFF=1
		#출력위치 (0: 좌상, 1: 우상)
		#Position=1
		LGFM_POSITION=1
		#전체보기시 좌측 좌표
		#XPos1=84
		#YPos1=47
		LGFM_X_POS_1=84
		LGFM_Y_POS_1=47
		#원본보기시 좌측 좌표
		#XPos2=177
		#YPos2=98
		LGFM_X_POS_2=177
		LGFM_Y_POS_2=98
		#전체보기시 우측 좌표
		#XPos3=826
		#YPos3=47
		LGFM_X_POS_3=826
		LGFM_Y_POS_3=47
		#원본보기시 우측 좌표
		#XPos4=733
		#YPos4=98
		LGFM_X_POS_4=733
		LGFM_Y_POS_4=98
		
		
		##비가시적FM 속성
		#비가시적FM 동작여부
		IVFM_ENABLED=0
		#시작시간(초)
		IVFM_START_TIME=60
		#동기패턴 출력시간(초)
		IVFM_SYNC_PATTERN_TIME=40
		#메세지패턴 출력시간(초)
		IVFM_MSG_PATTERN_TIME=40
		#동기패턴->메세지패턴 Gap(초) : iTin
		IVFM_SYNC_MSG_GAP=5
		#메세지패턴->동기패턴 Gap(초) : iTp
		IVFM_MSG_SYNC_GAP=5
	 */
	
	function _init () {
		log.printDbg("[WM] init");
		
		hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
		stateManager = KTW.managers.service.StateManager;
		
		vodAdapter = KTW.oipf.AdapterHandler.vodAdapter;
		//vodControl = KTW.vod.VodControl;
		
		ocManager = KTW.managers.data.OCManager;
		
		//OCManager 에 listener 등록.
		ocManager.addOCUpdateListener(_updateWMProperties);
	}
		
	function _getWMObj () {
		if (!wm_object) {
			wm_object = document.createElement("object");
			
			wm_object.type = "application/watermark";
			wm_object.id = "watermarkObj";
			wm_object.style.position = "absolute";
			wm_object.style.visibility = "hidden";
			wm_object.style.left = "0px";
			wm_object.style.top = "0px";
			wm_object.style.width = KTW.CONSTANT.RESOLUTION.WIDTH + "px";
			wm_object.style.height = KTW.CONSTANT.RESOLUTION.HEIGHT + "px";
			
			document.getElementById('mainVBOArea').appendChild(wm_object);
		}
		
		return wm_object;
	}
		
	function draw (_media_time, age, logo_only) {
		log.printDbg("[WM]_draw() media_time:" + _media_time + " age:" + age + " logo_only:" + logo_only);

		if (wm_properties.LGFM_ENABLED != 0) {
			//가시적.
			media_time = parseInt(_media_time);
			var start_time = parseInt(wm_properties.LGFM_START_TIME);
			var repeat = parseInt(wm_properties.LGFM_REPEAT);
			var random = parseInt(wm_properties.LGFM_RANDOM_TIME);
			var gap = parseInt(wm_properties.LGFM_GAP_TIME);
			var display_time = parseInt(wm_properties.LGFM_DISPLAY_TIME);
			
			log.printDbg("[WM]_draw param media_time:" + media_time + " X:" + wm_pos.x + " Y:" + wm_pos.y + " start:" + start_time + " repeat:" + repeat + " random:" + random + " gap:" + gap + " dpTime:" + display_time);
			try {
				_getWMObj().wmp_visible_draw(media_time, wm_pos.x, wm_pos.y, start_time, repeat, random, gap, display_time, logo_only);
			}
            catch (e) {
				log.printErr(e);
				//20141112 가시적 API  가 변경되어 구 펌웨어(2014. 12월이전)에서는 에러가 발생할수 있다.
				_getWMObj().wmp_visible_draw(media_time, wm_pos.x, wm_pos.y, start_time, repeat, random, gap, display_time);
			}
		}
		
		if (wm_properties.IVFM_ENABLED != 0) {
			//비가시적.
			try {
				media_time = parseInt(media_time);
				var start_time = parseInt(wm_properties.IVFM_START_TIME);
				var pattern_time = parseInt(wm_properties.IVFM_SYNC_PATTERN_TIME);
				var msg_pattern_time = parseInt(wm_properties.IVFM_MSG_PATTERN_TIME);
				var gap = parseInt(wm_properties.IVFM_SYNC_MSG_GAP);
				var sync_gap = parseInt(wm_properties.IVFM_MSG_SYNC_GAP);
			
				_getWMObj().wmp_invisible_draw(media_time, 0, 0, start_time, pattern_time, msg_pattern_time, gap, sync_gap, 255 /*alpha*/);
			}
            catch (e) {
				log.printErr(e);
			}
		}
	}
	
	function getLog () {
		var val = "";
		
		if(wm_properties.LGFM_ENABLED != 0) val += "visible ";
		if(wm_properties.IVFM_ENABLED != 0) val += "invisible ";
			
		return val == "" ? "none" : val;
	}
	
	function pause () {
		log.printDbg("[WM]_pause type:" + getLog());
		
		try {
			if(wm_properties.LGFM_ENABLED != 0) _getWMObj().wmp_visible_pause();
			if(wm_properties.IVFM_ENABLED != 0) _getWMObj().wmp_invisible_pause();
		}
        catch (e) {
			log.printErr(e);
		}
	}
	
	function resume (media_time) {
		log.printDbg("[WM]_resume type:" + getLog());

		if (!media_time) {
			media_time = vodAdapter.getPlayPosition();
		}
		
		try {
			if(wm_properties.LGFM_ENABLED != 0) _getWMObj().wmp_visible_resume(media_time, wm_pos.x, wm_pos.y);
			if(wm_properties.IVFM_ENABLED != 0) _getWMObj().wmp_invisible_resume(media_time, 0, 0);
		}
        catch (e) {
			log.printErr(e);
		}
	}
	
	function stop () {
		log.printDbg("[WM]_stop:" + getLog());
		
		try{
			if(wm_properties.LGFM_ENABLED != 0) _getWMObj().wmp_visible_stop();
			if(wm_properties.IVFM_ENABLED != 0) _getWMObj().wmp_invisible_stop();
		}
        catch (e) {
			log.printErr(e);
		}
	}
	
	function hide () {
		log.printDbg("[WM]_hide:" + getLog());
		
		try {
			if(wm_properties.LGFM_ENABLED != 0) _getWMObj().wmp_visible_hide();
			if(wm_properties.IVFM_ENABLED != 0) _getWMObj().wmp_invisible_hide();
		}
        catch (e) {
			log.printErr(e);
		}
	}
	
	/**
	 * 배속변경. 
	 */
	function trick () {
		log.printDbg("[WM]_trick:" + getLog());
		
		try {
			if(wm_properties.LGFM_ENABLED != 0) _getWMObj().wmp_visible_trick();
			if(wm_properties.IVFM_ENABLED != 0) _getWMObj().wmp_invisible_trick();
		}
        catch (e) {
			log.printErr(e);
		}
	}
	
	function changePosition () {
		log.printDbg("[WM]_change_position:" + getLog());
		
		/**
		 * 이 값을 직접 부를때 기존 timer 를 제거함. 
		 */
		if (dup_check_timer){
			clearTimeout(dup_check_timer);
			dup_check_timer = null;
		}
		
		_calcXY();
		
		try {
			if(wm_properties.LGFM_ENABLED != 0) _getWMObj().wmp_visible_change_position(wm_pos.x, wm_pos.y);
			if(wm_properties.IVFM_ENABLED != 0) _getWMObj().wmp_invisible_change_position(0, 0);
		}
        catch (e) {
			log.printErr(e);
		}
	}
	
	function _updateWMProperties (source_url) {
		var OC = KTW.managers.data.OCManager;
		
		log.printDbg("is update wm properties " + (source_url == OC.DATA.WM.URL));
		
		// 여기서 직접 부르거나, 
		// OC Updated 되거나. 둘다의 경우를 체크 해야함.
		if (source_url === undefined || source_url == OC.DATA.WM.URL) {
			try {
				var wm_properties_txt = ocManager.getWmPropertiesData();
		    	
		    	if (!wm_properties_txt || wm_properties_txt.length == 0){
		    		//read default. 
		    		//mook_t path 변경.s
		    		$.get('./assets/oc/wm.properties', function(result) {
		    			wm_properties_txt = result;
		    		});
		    	}
		    	
		    	_parseWMProperties(wm_properties_txt);
			}
            catch (e) {
				log.printErr(e);
			}
		}
	}
	
	function _parseWMProperties (wm_properties_txt) {
		log.printDbg("parse wm properties");
		
		var data = wm_properties_txt.split("\n");
		var properties = [];	
		for (var i = 0, j = 0; i < data.length; i++) {
			if (data[i].indexOf("#") != 0 && data[i] != undefined && data[i].trim().length > 0)
				properties[j++] = data[i]; 
		}
		
		wm_properties = {};
		var temp;
		for (var i = 0; i < properties.length; i++) {
			temp = properties[i].split("=");
			
			wm_properties[temp[0]] = temp[1];
		}
	};
	
	function _getVideoRatio () {
		var video_component = vodAdapter.getMpegObject().getComponents(MediaExtension.COMPONENT_TYPE_VIDEO);
		
		try {
			if (video_component && video_component[0].aspectRatio) {
                // video_component[0].aspectRatio 가 없는 경우가 발생
				return video_component[0].aspectRatio;
			}
		}
        catch (e) {
			log.printWarn("no videoRatio:" + e);	
		}
		
		return 0;
	}
	
	function round (val, precision) {
	  val = val * Math.pow(10, precision);
	  val = Math.round(val); 
	  return val/Math.pow(10, precision);
	}

	function _calcXY () {
		var videoRatio = _getVideoRatio();
		var aspectRatio = hwAdapter.getAVOutputTvAspectRatio();
		var videoMode = hwAdapter.getAVOutputVideoMode();
		
		log.printDbg("_calcXY videoRatio:" + videoRatio + " aspectRatio:" + aspectRatio + " videoMode:" + videoMode);
		
		if (!wm_properties)
			_updateWMProperties();
		
		if (videoRatio != 0 && aspectRatio == "4:3" && round(videoRatio, 2) == round(4/3, 2)) {
			if (wm_properties.LGFM_POSITION == 0) {
				wm_pos.x = parseInt(wm_properties.LGFM_X_POS_1);
				wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_1);
			}
            else {
				wm_pos.x = parseInt(wm_properties.LGFM_X_POS_3);
				wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_3);
			}
		}
        else if (videoRatio  != 0 && aspectRatio == "16:9" && round(videoRatio, 2) == round(16/9, 2)) {
			if (wm_properties.LGFM_POSITION == 0) {
				wm_pos.x = parseInt(wm_properties.LGFM_X_POS_1);
				wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_1);
			}
            else {
				wm_pos.x = parseInt(wm_properties.LGFM_X_POS_3);
				wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_3);
			}
		}
        else {
			if (wm_properties.LGFM_POSITION == 0) {
				if (aspectRatio == "4:3") {
					if (videoMode == "stretch" || videoMode == "zoom") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_1);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_1);
					}
                    else if (videoMode == "normal") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_1);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_2);
					}
				}
                else {
					if (videoMode == "stretch" || videoMode == "zoom") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_1);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_1);
					}
                    else if (videoMode == "normal") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_2);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_1);
					}
				}
			}
            else {
				if (aspectRatio == "4:3") {
					if (videoMode == "stretch" || videoMode == "zoom") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_3);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_3);
					}
                    else if (videoMode == "normal") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_3);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_4);
					}
				}
                else {
					if (videoMode == "stretch" || videoMode == "zoom") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_3);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_3);
					}
                    else if (videoMode == "normal") {
						wm_pos.x = parseInt(wm_properties.LGFM_X_POS_4);
						wm_pos.y = parseInt(wm_properties.LGFM_Y_POS_3);
					}
				}
			}
		}
		
		log.printDbg("_calcXY X:" + wm_pos.x + " Y:" + wm_pos.y);
	}
	
	function _isEnable () {
		if (!wm_properties || (wm_properties.LGFM_ENABLED == 0 && wm_properties.IVFM_ENABLED == 0)) {
			
			log.printWarn("WM Disable cause by wm_properties" + JSON.stringify(wm_properties));
			return false;
		}
		
		return true;
	}

	function _prepare (_media_time, _age, _watermark_type) {
		log.printDbg("[WM] _prepare _watermark_type:" + _watermark_type);

		media_time = _media_time;
		age = _age;
		watermark_type = _watermark_type;
		
		if (!wm_properties) {
			_updateWMProperties();
			
			setTimeout(prepareInternal, 500);
		}
        else {
			prepareInternal();
		}
	}
	
	function prepareInternal () {
		log.printDbg("[WM] prepareInternal mediaTime:" + media_time + " age:" + age + " watermark_type:" + watermark_type);
		
		/* 초기화 */
		wm_properties.LGFM_ENABLED = 1;
		wm_properties.IVFM_ENABLED = 1;
		is_logo_only_mode = false;
		
		/* 값 설정 */
		if (watermark_type == "" || !watermark_type) {
			watermark_type = wm_properties.WM_DEFAULT_OPERATING_MODE;
			log.printDbg("[WM] from OC File : "+watermark_type);
		}
		
		if (watermark_type == "2" || watermark_type == "5" || watermark_type == "0")
			wm_properties.IVFM_ENABLED = 0;
		
		if (watermark_type == "0" || watermark_type == "3")
			wm_properties.LGFM_ENABLED = 0;
		
		if (watermark_type == "4" || watermark_type == "5")
			is_logo_only_mode = true;
		
		media_time = parseInt((media_time !== undefined && media_time >= 0) ? media_time : 0);
		age = parseInt((age !== undefined && age >= 0) ? age : 0);

        if (wm_state < STATE.WM_RUNNING) {
            wm_state = STATE.WM_PREPARED;
        }
	}

	var interval_wm = null;

	function _start (cbStart) {
        log.printDbg("[WM] _start : " + wm_state);

		if (_isEnable()) {
            // [R7] 워터마크 Start가 호출될 시기에 타이밍적으로 trickPlayPossible이 false가 올 수가 있다.
            // R6에서는 정상동작 하는데, R7에서 이슈가 되는 이유는 2가지로 가정된다.
            // 1. VodAdapter를 이용하는 API들이 (종료 광고, 성질급한 VOD) 많아져서 호출되는 시점과
            // 다르게 return값을 전달 받을수 있다
            // 2. onPlayingStart 가 호출되는 로직이 조금 개선되어 기존보다 빠르게 Start를 처리하게 되고
            // 이때 trickPlay를 확인 했을때 false가 올수가 있다.
            //
            // 기본적으로 Timeout을 2초걸어 정확치않은 시점에 TrickPlay를 확인하기 보다는 retry를 1초간 4번
            // 시도하여 워터마크 상태를 체크하도록 수정한다
            var _retry_count = 0;

            clearInterval( interval_wm );
            interval_wm = setInterval( function (){
                if (wm_state !== STATE.WM_PREPARED) {
                    log.printWarn("[WM] wm_state is not prepared... Retry to prepare checking watermark : " + _retry_count );
                    _retry_count++;
                }
                else {
                    if (vodAdapter.trickPlayPossible() === true) {
                        log.printDbg("[WM] trickPlayPossible() true");
                        interval_wm = clearInterval(interval_wm);
                        startInternal();

                        cbStart();
                        return;
                    }
                    else {
                        log.printWarn("[WM] trickPlayPossible() false... Retry to prepare checking watermark : " + _retry_count );
                        _retry_count++;
                    }
                }

                if (_retry_count > 4) {
                    interval_wm = clearInterval(interval_wm);
                    log.printWarn("[WM] retry count is over... so clearInterval...");

                    cbStart();
                }
            }, 1000);
		}
	}
	
	function startInternal () {
        log.printDbg("[WM] startInternal");

        _calcXY();

        if (stateManager.isVODPlayingState()) {
            try {
                _getWMObj().wmp_init(KTW.SAID, util.getFormatDate(new Date(), "yyyyMMddHHmm"), parseInt(age), KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);
            }
            catch (e) {
                log.printErr(e);
                _stop();
                return;
            }

            _getWMObj().style.width = KTW.CONSTANT.RESOLUTION.WIDTH + "px";
            _getWMObj().style.height = KTW.CONSTANT.RESOLUTION.HEIGHT + "px";
            _getWMObj().style.visibility = "visible";

            wm_state = STATE.WM_INITED;

            //seek 등과 같이 position 변경 동작을 하면 호출.
            vodAdapter.addPlayPositionChangeListener(_onPlayPositionChangedInSeek);
            //playspeed 변경.
            vodAdapter.addPlaySpeedChangeListener(_onPlaySpeedChanged);
            //VideoMode Setting 변경.
            hwAdapter.addVideoOutputChangeListener(_onVideoOutputChangeListener);

            //시작과 동시에 갱신.
            draw(media_time, age, is_logo_only_mode);
            wm_state = STATE.WM_RUNNING;
        }
        else {
            log.printWarn("[WM] not vod state");
            _stop();
        }
	}
	
	function _stop () {
		log.printDbg("[WM] _stop wm_state:" + wm_state);
		
		if (wm_state >= STATE.WM_RUNNING) {
			stop();
		}
		
		if (wm_state >= STATE.WM_INITED) {
			try {
				_getWMObj().wmp_term();
			}
            catch (e) {
				log.printErr(e);
			}
			
			vodAdapter.removePlayPositionChangeListener(_onPlayPositionChangedInSeek);
			vodAdapter.removePlaySpeedChangeListener(_onPlaySpeedChanged);
			hwAdapter.removeVideoOutputChangeListener(_onVideoOutputChangeListener);

            try {
                _getWMObj().style.visibility = "hidden";
            }
            catch (e) {
                log.printErr(e);
            }
		}
		
		//초기화 상태. 아무것도 안하는 상태. 
		wm_state = STATE.WM_IDLE;
	}
	
	/**
	 * 실제 OIPF 에서 올려주는 callback, seek 후 불릴거라 추측됨.
	 */
	function _onPlayPositionChangedInSeek (position) {
		if (_isEnable() && wm_state !== STATE.WM_HIDE) {
			log.printDbg("[WM] _onPlayPositionChangedInSeek mediaTime:" + position);
			media_time = position;
			resume(media_time);
            wm_state = STATE.WM_RUNNING;
		}
        else {
			log.printDbg("[WM] _onPlayPositionChangedInSeek fail state:" + wm_state);
		}
	}
	
	var was_speed = -1;
	function _onPlaySpeedChanged (speed) {
		if (_isEnable()) {
			log.printDbg("[WM] _onPlaySpeedChanged speed: " + speed + ", was_speed: " + was_speed + ", wm_state: " + wm_state);
			
			if (was_speed != speed && wm_state !== STATE.WM_HIDE) {
				was_speed = speed;
				switch(speed) {
					case 0:
						pause();
                        wm_state = STATE.WM_PAUSED;
						break;
					case 1:
						resume();
                        wm_state = STATE.WM_RUNNING;
						break;
					default:
						trick();
				}//switch
			}
            else { //if speed diffrent
				log.printDbg("[WM] _onPlaySpeedChanged same speed ignored");
			}
		}
        else {//if is enable
			log.printDbg("[WM] _onPlaySpeedChanged fail state:" + wm_state);
		}
	}
	
	function _changeVisible (is_visible) {
		log.printDbg("[WM] _changeVisible is_visible:" + is_visible + " wm_state:" + wm_state);
		
		if (is_visible) {
			if (wm_state === STATE.WM_HIDE) {
				resume();
                wm_state = STATE.WM_RUNNING;
			}
		}
        else {
			if (wm_state === STATE.WM_RUNNING || wm_state === STATE.WM_PAUSED) {
				hide();
                wm_state = STATE.WM_HIDE;
			}
		}
	}
	
	/**
	 * 이 값은 setting 에서 바뀌고, 동시에 2개의 값을 같이 바꾸기 때문에 
	 * 2개를 연속을 받았을때만 해당 값을 처리해야 한다.
	 * timer 로 중복을 제거함.  
	 */
	var dup_check_timer = null;
	function _onVideoOutputChangeListener (mode) {
		log.printDbg("[WM] _onVideoOutputChangeListener mode:" + mode);
		
		if (dup_check_timer){
			clearTimeout(dup_check_timer);
			dup_check_timer = null;
		}
		
		switch (mode) {
			case 'VideoMode' :
			case 'AspectRatio':
				dup_check_timer = setTimeout(changePosition, 500);
				break;
		}
	}
			
	return {
		STATE: STATE,
		
		init: _init,
		
		/* water mark interface. */
		prepare: _prepare,
		start: _start,
		stop: _stop,
		
		changeVisible: _changeVisible,
		
		getState: function() {
			return wm_state;
		}
	};
}());
