/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AndroidAdapter.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.adapters.AndroidAdapter = (function () {

    var AUDIO_DEVICE = {
        HDMI: "hdmi",
        SPEAKER: "speaker"
    };

    var oipfFactory = KTW.oipf.oipfObjectFactory;

    var log = KTW.utils.Log;

    var android_bridge_object;

    function getAndroidBridgeObject() {
        if (!android_bridge_object) {
            try {
                android_bridge_object = oipfFactory.createAndroidBridgeObject();
                log.printDbg("created AndroidBridgeObject");
            } catch(e) {
                log.printErr("failed createAndroidBridgeObject");
                log.printExec(e);
            }
        }

        return android_bridge_object;
    }

    /**
     * execute android intent by packageName
     */
    function _launchApplication(package_name) {
        try {
            getAndroidBridgeObject().launchApplication(package_name);

            log.printDbg("launchApplication() package_name = " + package_name);
        }
        catch(e) {
            log.printErr("launchApplication() failed launch android application");
        }
    }

    /**
     * get current android state (ACTIVE or INACTIVE)
     *
     * @returns {number}
     */
    function _getState() {
        var state = -1;

        try {
            state = getAndroidBridgeObject().getState();
            log.printDbg("getState(), state = " + state);
        } catch (e) {
            log.printErr("getState(), failed to get android state");
        }

        return state;
    }

    function _getAudioOutputDevice() {

        var device = undefined;

        try {
            device = getAndroidBridgeObject().getAudioOutputDevice();

            log.printDbg("getAudioOutputDevice() device = " + device);
        }
        catch(e) {
            log.printErr("getAudioOutputDevice() failed to get audio output device");
        }

        return device;
    }

    function _setAudioOutputDevice(device) {

        try {
            getAndroidBridgeObject().setAudioOutputDevice(device);
        }
        catch(e) {
            log.printErr("setAudioOutputDevice() failed to set audio output device");
        }
    }

    /**
     * set listener to check state android ACTIVE/INACITVE
     */
    function _setStateChangedListener(listener) {
        try {
            getAndroidBridgeObject().setStateChangedListener(listener);
        }
        catch(e) {
            log.printErr("setStateChangedListener() failed to set listener");
        }
    }

    return {
        AUDIO_DEVICE: AUDIO_DEVICE,

        getState: _getState,
        getAudioOutputDevice: _getAudioOutputDevice,
        setAudioOutputDevice: _setAudioOutputDevice,
        launchApplication: _launchApplication,
        setStateChangedListener: _setStateChangedListener
    };
}());
