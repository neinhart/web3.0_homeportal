/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AudioTtsAdapter.js</code>
 *
 * @author dhlee
 * @since 2016-12-05
 */

"use strict";

KTW.oipf.adapters.AudioTtsAdapter = (function () {
    /** oipfFactory 오브젝트. */
    var oipfFactory = KTW.oipf.oipfObjectFactory;
    /** Log 오브젝트. */
    var log = KTW.utils.Log;
    /** audio/tts 임베디드 오브젝트. */
    var audio_tts_obj = null;
    /** 음성 재생 동작 상태 event listener. */
    var speech_listeners = null;
    /** 음성 합성 동작 상태 event listener.*/
    var synthesize_listeners = null;

    /**
     * 초기화.
     * TODO 초기화 함수를 어디서 불러줘야 하는지 확인 필요함.
     */
    function _init() {
        log.printDbg("called init()");
        speech_listeners = new KTW.utils.Listener();
        synthesize_listeners = new KTW.utils.Listener();
        getAudioTtSObject().addSpeechStateChangeListener(onSpeechStateChange);
        getAudioTtSObject().addSynthesizeStateChangeListener(onSynthesizeStateChange);
    }

    /**
     * 음성 재생 동작 상태 event listener 등록
     *
     * @param listener
     */
    function _addSpeechListener(listener) {
        log.printDbg("called addSpeechListener() - listener : " + listener);
        if (speech_listeners && listener) {
            speech_listeners.addListener(listener);
        }
    }

    /**
     * 음성 재생 동작 상태 event listener 제거.
     *
     * @param listener
     */
    function _removeSpeechListener(listener) {
        log.printDbg("called removeSpeechListener() - listener : " + listener);
        if (speech_listeners) {
            speech_listeners.removeListener(listener);
        }
    }

    /**
     * 음성 합성 동작 상태 event listener 등록.
     *
     * @param listener
     */
    function _addSynthesizeListener(listener) {
        log.printDbg("called addSynthesizeListener() - listener : " + listener);
        if (synthesize_listeners) {
            synthesize_listeners.addListener(listener);
        }
    }

    /**
     * 음성 합성 동작 상태 event listener 제거.
     *
     * @param listener
     */
    function _removeSynthesizeListener(listener) {
        log.printDbg("called removeSynthesizeListener() - listener : " + listener);
        if (synthesize_listeners) {
            synthesize_listeners.removeListener(listener);
        }
    }

    /**
     * AudioTtsObject를 반환한다.
     *
     * @returns {*} AudioTtsObject
     */
    function getAudioTtSObject() {
        log.printDbg("called getAudioTtSObject() - audio_tts_obj : " + audio_tts_obj);
        if (!audio_tts_obj) {
            try {
                audio_tts_obj = oipfFactory.createAudioTtsObject();
            } catch (e) {
                log.printErr("getAudioTtSObject() - failed create audio tts object");
                log.printExec(e);
            }
        }
        return audio_tts_obj;
    }

    /**
     * 아규먼트로 전달한 텍스트와 목소리를 음성으로 변환해 재생한다.
     *
     * @param text 음성으로 재생할 텍스트
     * @param voice 설정하고자 하는 화자 / KTW.oipf.Def,AUDIO_TTS.VOICE
     * @returns {number} 요청에 대한 유니크 아이디
     */
    function _startSpeech(text, voice) {
        log.printDbg("called startSpeech() - text : " + text + ", voice : " + voice);
        var id = 0;
        try {
            id = getAudioTtSObject().startSpeech(text, voice);
        } catch (e) {
            log.printErr("startSpeech() - failed start speech");
        }
        log.printDbg("startSpeech() - id : " + id);
        return id;
    }

    /**
     * 아규먼트로 전달한 유니크 아이디에 해당하는 음성 재생 동작을 중지시킨다.
     *
     * @param id 유니크 아이디
     * @returns {boolean} 음성 재생 동작의 중지가 실패한 경우 false를 그 외의 경우 true를 반환
     */
    function _stopSpeech(id) {
        log.printDbg("called stopSpeech() - id : " + id);
        var result = false;
        try {
            result = getAudioTtSObject().stopSpeech(id);
        } catch (e) {
            log.printErr("stopSpeech() - failed stop speech");
        }
        log.printDbg("stopSpeech() - result: " + result);
        return result;
    }

    /**
     * 아규먼트로 전달한 텍스트와 목소리를 음성으로 변환해 재생한다.
     *
     * @param text 음성으로 재생할 텍스트
     * @param format 음성 포맷 / OOTW.oipf.Def,AUDIO_TTS..FORMAT
     * @param voice 설정하고자 하는 화자 / OOTW.oipf.Def,AUDIO_TTS..VOICE
     * @returns {number} 유니크 아이디
     */
    function _startSynthesize(text, format, voice) {
        log.printDbg("called startSynthesize() - text : " + text + ", format : " + format + ", voice : " + voice);
        var id = 0;
        try {
            id = getAudioTtSObject().startSynthesize(text, format, voice);
        } catch (e) {
            log.printErr("startSynthesize() - failed start synthesize");
        }
        log.printDbg("startSynthesize() - id : " + id);
        return id;
    }

    /**
     * 아규먼트로 전달한 유니크 아이디에 해당하는 음성 변환 동작을 중지시킨다.
     *
     * @param id 유니크 아이디
     * @returns {boolean} 음성 재생 동작의 중지가 실패한 경우 false를 그 외의 경우 true를 반환
     */
    function _stopSynthesize(id) {
        log.printDbg("called stopSynthesize() - id : " + id);
        var result = false;
        try {
            result = getAudioTtSObject().stopSynthesize(id);
        } catch (e) {
            log.printErr("stopSynthesize() - failed stop synthesize");
        }
        log.printDbg("stopSynthesize() - result: " + result);
        return result;
    }

    /**
     * TTS 볼륨을 설정한다.
     *
     * @param volume 볼륨 값 (지정 가능한 값은 0에서 100까지)
     * @returns {boolean} 볼륨 변경 동작이 성공하면 본 메쏘드는 true를 반환하며, 실패한 경우 false를 반환
     */
    function _setVolume(volume) {
        log.printDbg("called setVolume() - volume : " + volume);
        var result = false;
        try {
            result = getAudioTtSObject().setVolume(volume);
        } catch (e) {
            log.printErr("setVolume() - failed set volume");
        }
        log.printDbg("setVolume() - result: " + result);
        return result;
    }

    /**
     * TTS 음성의 실제 출력 볼륨 값(마스터 볼륨 X TTS 볼륨(%))을 반환한다.
     *
     * @returns {number} 볼륨 값
     */
    function _getVolume() {
        log.printDbg("called getVolume()");
        var volume = 0;
        try {
            volume = getAudioTtSObject().getVolume();
        } catch (e) {
            log.printErr("getVolume() - failed get volume");
        }
        log.printDbg("getVolume() - volume: " + volume);
        return volume;
    }

    /**
     * 음성 재생 동작의 상태가 변경되는 경우 발생하는 이벤트 Listener
     *
     * @param id 음성 재생 동작을 나타내는 유니크 아이디
     * @param state 음성 재생 동작 상태 / KTW.oipf.Def,AUDIO_TTS.STATE
     */
    function onSpeechStateChange(id, state) {
        log.printDbg("called onSpeechStateChange() - id : " + id + ", state : " + state + ", speech_listeners : " + speech_listeners);
        if (speech_listeners) {
            speech_listeners.notify(id, state);
        }
    }

    /**
     * 음성 합성 동작의 상태가 변경되는 경우 발생하는 이벤트 Listener
     *
     * @param id 음성 합성 동작을 나타내는 유니크 아이디
     * @param state 음성 합성 동작 상태 / KTW.oipf.Def,AUDIO_TTS.STATE
     * @param speech 함성된 음성 데이터
     */
    function onSynthesizeStateChange(id, state, speech) {
        log.printDbg("called onSynthesizeStateChange() - id : " + id + ", state : " + state + ", speech : " + speech + ", synthesize_listeners : " + synthesize_listeners);
        if (synthesize_listeners) {
            synthesize_listeners.notify(id, state, speech);
        }
    }

    /**
     * UHD3 셋탑 전용 음성가이드 화자설정 property getter
     * @returns {*}
     * @private
     */
    function _getDefaultVoiceMode() {
        log.printDbg("_getDefaultVoiceMode()");
        var defaultVoiceMode;
        defaultVoiceMode = getAudioTtSObject().defaultVoiceMode;
        return defaultVoiceMode;
    }

    /**
     *
     * @param mode 설정될 화자에 맞는 value
     * @returns {*}
     * @private
     */
    function _setDefaultVoiceMode(mode) {
        log.printDbg("_setDefaultVoiceMode()");
        var result;
        try {
            result = getAudioTtSObject().setDefaultVoiceMode(mode);
        } catch (e) {
            log.printErr("setDefaultVoiceMode() - failed set setDefaultVoiceMode");
        }
        log.printDbg("setDefaultVoiceMode() - result: " + result);
        return result;
    }

    return {
        init: _init,
        addSpeechListener: _addSpeechListener,
        removeSpeechListener: _removeSpeechListener,
        addSynthesizeListener : _addSynthesizeListener,
        removeSynthesizeListener : _removeSynthesizeListener,
        startSpeech: _startSpeech,
        stopSpeech: _stopSpeech,
        startSynthesize: _startSynthesize,
        stopSynthesize: _stopSynthesize,
        setVolume: _setVolume,
        getVolume: _getVolume,
        getDefaultVoiceMode: _getDefaultVoiceMode,
        setDefaultVoiceMode: _setDefaultVoiceMode
    }
}());
