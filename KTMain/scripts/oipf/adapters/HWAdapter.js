"use strict";

/**
 * handling power state, network and device(usb, bluetooth)
 * and AV outputs, tuner and so on
 */
KTW.oipf.adapters.HWAdapter = (function () {
    
    var CONFIG_EVENT = KTW.oipf.Def.CONFIG.EVENT;
    var oipfFactory = KTW.oipf.oipfObjectFactory;
    var AV_OUTPUT_DEVICE = KTW.oipf.Def.AV_OUTPUT.DEVICE;
    
    var log = KTW.utils.Log;
    
    var conf_obj = null;
    var local_system = null;
    var tuner_access = null;
    var transponder_obj = null;
    var storage_manager_obj = null;
    var mouse_control_obj = null;
    
    var device_interfaces;
    var bluetooth_interface;
    var network_interface;
    var av_output;
    
    var power_state_listener;
    var network_listener;
    var listeners;
    var av_output_change_listener;
    
    function _init() {
        _reset();
        
        listeners = new KTW.utils.Listener();
        av_output_change_listener = new KTW.utils.Listener();
        
        getLocalSystem().addEventListener(CONFIG_EVENT.POWER_STATE_CHANGE, onPowerStateChange);
        getLocalSystem().addEventListener(CONFIG_EVENT.DEVICE_CONNECT, onDeviceConnect);
        getLocalSystem().addEventListener(CONFIG_EVENT.DEVICE_DISCONNECT, onDeviceDisconnect);
        getLocalSystem().addEventListener(CONFIG_EVENT.DEVICE_ERROR, onDeviceError);
    }
    
    /**
     * system 초기화 될 경우 interface를 초기화 하기 위한 function
     */
    function _reset() {
        device_interfaces = getLocalSystem().networkInterfaces;
        
        var length = device_interfaces ? device_interfaces.length : 0;
        for (var i = 0; i < length; i++) {
            try {
                if (device_interfaces[i] instanceof NetworkInterface) {
                    network_interface = device_interfaces[i];
                }
                else if (device_interfaces[i] instanceof BluetoothInterface) {
                    bluetooth_interface = device_interfaces[i];
                }
            } catch(e) {
                log.printErr("Bluetooth device is not supported");
                log.printExec(e);
            }
        }
    }
    
    function _addEventListener(event, listener) {
        if (event === CONFIG_EVENT.POWER_STATE_CHANGE) {
            power_state_listener = listener;
        }
        else if (event === CONFIG_EVENT.NETWORK) {
            network_listener = listener;
        }
        else {
            listeners.addListener(listener);
        }
    }
    
    function _removeEventListener(event, listener) {
        if (event && event === CONFIG_EVENT.POWER_STATE_CHANGE) {
            power_state_listener = undefined;
        }
        else if (event === CONFIG_EVENT.NETWORK) {
            network_listener = undefined;
        }
        else {
            listeners.removeListener(listener);
        }
    }
    
    function _getIpAddress() {
        return device_interfaces && device_interfaces[0] ? device_interfaces[0].ipAddress : "";
    }
    
    function _getMacAddress() {
        return device_interfaces && device_interfaces[0] ? device_interfaces[0].macAddress : "";
    }
    
    function _setPowerState(state) {
        getLocalSystem().setPowerState(state);
    }
    
    function _getPowerState() {
        return getLocalSystem().powerState;
    }
    
    /**
     * [jh.lee] MenuDataManager에서 저전력 지원 여부 확인
     */
    function _getSupportedPowerStates() {
        return getLocalSystem().supportedPowerStates;
    }
    
    function onPowerStateChange(event) {
        if (power_state_listener) {
            power_state_listener(event);
        }
    }


    /**
     *  [sw.nam] spdif / hdmi 출력단자 설정 지원 여부 확인
     * @returns {*}
     */
    function _getSupportedDigitalAudioModes() {
        log.printDbg("getSupportedDigitalAudioModes()");
        return getLocalSystem().outputs[0].supportedDigitalAudioModes;
    }
    
    /**
     * onDeviceConnect callback
     * network와 device를 구분해서 listener callback 처리함
     */
    function onDeviceConnect(event) {
        fireDeviceEvent(event);
    }
    
    /**
     * onDeviceDisconnect callback
     * network와 device를 구분해서 listener callback 처리함
     */
    function onDeviceDisconnect(event) {
        fireDeviceEvent(event);
    }
    
    /**
     * onDeviceError callback
     * network와 device를 구분해서 listener callback 처리함
     */
    function onDeviceError(event) {
        fireDeviceEvent(event);
    } 
    
    function fireDeviceEvent(event) {
        if ((bluetooth_interface !== undefined && event.device instanceof BluetoothDevice)) {
            listeners.notify(KTW.oipf.Def.CONFIG.DEVICE_TYPE.BLUE_TOOTH, event);
        }
        else if (event.device instanceof InputDeviceInfo) {
            listeners.notify(KTW.oipf.Def.CONFIG.DEVICE_TYPE.INPUT, event);
        }
        else if (event.device instanceof Storage) {
            listeners.notify(KTW.oipf.Def.CONFIG.DEVICE_TYPE.STORAGE, event);
        }
        else if (event.device instanceof Tuner) {
            if (network_listener) {
                network_listener(event);
            }
        }
    }
    
    function _getModelName() {
        return getLocalSystem().modelName;
    }

    function _getAVOutputTvAspectRatio() {
        return getAllAVOutput().tvAspectRatio;
    }

    function _setAVOutputTvAspectRatio(ratio) {
        getAllAVOutput().tvAspectRatio = ratio;

        av_output_change_listener.notify('AspectRatio', ratio);
    }

    function _getAVOutputVideoMode() {
        return getAllAVOutput().videoMode;
    }

    function _setAVOutputVideoMode(mode) {
        getAllAVOutput().videoMode = mode;

        av_output_change_listener.notify('VideoMode', mode);
    }

    /**
     * [jh.lee] 해상도 메뉴에서 사용 set (changeHDVideoFormat)
     */
    function _setHDVideoFormat(format) {
        getAllAVOutput().changeHDVideoFormat(format);
    }

    /**
     * [jh.lee] 해상도 메뉴에서 사용 get (changeHDVideoFormat)
     */
    function _getHDVideoFormat() {
        return getAllAVOutput().changeHDVideoFormat;
    }

    /**
     * [jh.lee] 해상도 설정값을 반환
     * @returns hdVideoFormat
     */
    function _getAVOutputHDVideoFormat() {
        return getAllAVOutput().hdVideoFormat;
    }

    /**
     * [jh.lee] 해상도 설정값 저장
     * @returns hdVideoFormat
     */
    function _setAVOutputHDVideoFormat(format) {
        getAllAVOutput().hdVideoFormat = format;
    }

    /**
     * [jh.lee] AVOutput 오디오출력 설정값을 반환
     */
    function _getAVOutputDigitalAudioMode() {
        return getAllAVOutput().digitalAudioMode;
    }

    /**
     * [jh.lee] AVOutput 오디오 출력 설정값을 설정
     */
    function _setAVOutputDigitalAudioMode(mode) {
        getAllAVOutput().digitalAudioMode = mode;
    }
    
    function _addVideoOutputChangeListener(l) {
        av_output_change_listener.addListener(l);
    }
    
    function _removeVideoOutputChangeListener(l) {
        av_output_change_listener.removeListener(l);
    }

    /**
     * [jh.lee] 해상도 * 입력후 저장완료
     */
    function _confirmHDVideoFormat() {
        getAllAVOutput().confirmHDVideoFormat();
    }

    /**
     * [jh.lee] UHD2가 새로 생기면서 현재 지원하는 해상도를 확인하기 위해 추가
     * [jh.lee] R3에는 추가는되어있는데 사용을 안함. R4에서는 사용이 필요하여 추가
     * [jh.lee] 예를들어 UHD 인 경우 총 7개의 해상도 값을 보내준다. 배열형태로...
     */
    function _isSupportedHdVideoFormats() {
        return getAllAVOutput().supportedHdVideoFormats;
    }

    /**
     * STB에 내장된 출력 장치들의 정보를 가진 AVOutput 반환
     *
     * @param index OTW.oipf.Def.AV_OUTPUT.DEVICE
     *               .ALL(0) : 모든 출력 장치
     *               .MAIN(1) : 메인 출력 장치
     *               .INTERNAL_SPEAKER(2) : 내장 스피커
     * @returns {*} AVOutput
     * @see KTW.oipf.Def.AV_OUTPUT.DEVICE
     */
    function getAVOutput(index) {
        log.printDbg("called getAVOutput() - index : " + index);
        var output = null;
        if (!av_output) {
            av_output = getLocalSystem().outputs;
        }
        if (av_output && av_output.length > index) {
            output = av_output[index];
        }
        log.printDbg("getAVOutput() - output : " + output);

        return output;
    }

    function getAllAVOutput() {
        return getAVOutput(AV_OUTPUT_DEVICE.ALL);
    }

    /**
     * STB에 내장된 출력 장치 ON/OFF
     *
     * @param index OTW.oipf.Def.AV_OUTPUT.DEVICE
     *               .ALL(0) : 모든 출력 장치
     *               .MAIN(1) : 메인 출력 장치
     *               .INTERNAL_SPEAKER(2) : 내장 스피커
     * @param enabled true : ON
     *                 false : OFF
     * @see KTW.oipf.Def.AV_OUTPUT.DEVICE
     */
    function _setEnableAVOutput(index, enabled) {
        log.printDbg("called _setEnableAVOutput() - index : " + index + ", enabled : " + enabled);
        var output = getAVOutput(index);
        if(output) {
            output.enabled = enabled;
        }
    }
    
    function getConfigurationObject() {
        if (!conf_obj) {
            try {
                conf_obj = oipfFactory.createConfigurationObject();
                log.printDbg("created ConfigurationObject");
            } catch(e) {
                log.printErr("failed createConfigurationObject");
                log.printExec(e);
            }
        }
        return conf_obj;
    }
    
    function getLocalSystem() {
        if (!local_system) {
            try {
                local_system = getConfigurationObject().localSystem;
                log.printDbg("get LocalSystemObject");
            } catch(e) {
                log.printErr("failed get ConfigurationObject#localSystem");
                log.printExec(e);
            }
        }
        
        return local_system;
    }
    
    /**
     * [jh.lee] OTS LNB 메뉴 관련
     */
    function getTunerAccess() {
        if (!tuner_access) {
            try {
                tuner_access = oipfFactory.createTunerAccess();
                log.printDbg("created TunerAccess");
            } catch(e) {
                log.printErr("failed createTunerAccess");
                log.printExec(e);
            }
        }
        
        return tuner_access;
    }
    
    /**
     * [jh.lee] LNB 데이터 get
     */
    function _getLNB(port) {
        return getTunerAccess().getLNB(port);
    }
    
    /**
     * [jh.lee] LNB 데이터 set
     */
    function _setLNB(port, value/*array*/) {
        getTunerAccess().setLNB(port, value);
    }
    
    /**
     * [jh.lee] 중계기 메뉴 관련 데이터 set
     */
    function _setTSInfo(index, info, flag) {
        getTunerAccess().setTSInfo(index, info, flag);
    }
    
    /**
     * [jh.lee] 중계기 메뉴 관련 데이터 get
     */
    function _getTSInfos() {
        return getTunerAccess().getTSInfos();
    }
    
    function _getAdditionalTunerInfos() {
        return getTunerAccess().getAdditionalTunerInfos();
    }
    
    /**
     * [jh.lee] 수신품질 측정 메뉴 관련
     */
    function createTransponderControl() {
        if (!transponder_obj) {
            try {
                transponder_obj = oipfFactory.createTransponderControl();
                log.printDbg("created TransponderControl");
            } catch(e) {
                log.printErr("failed createTransponderControl");
                log.printExec(e);
            }
        }
        
        return transponder_obj;
    }
    
    /**
     * [jh.lee] 수신품질 측정 데이터 수집
     */
    function _getCollectibleList() {
        return createTransponderControl().getCollectibleList();
    }
    
    function _tune(ts_info) {
        createTransponderControl().tune(ts_info);
    }
    
    function _collectOneTransponder(name, id) {
        return createTransponderControl().collectOneTransponder(name, id);
    }
    
    function _getStorages() {
        return getStorageManager().storages;
    }
    
    function getStorageManager() {
        if (!storage_manager_obj) {
            try {
                storage_manager_obj = oipfFactory.createStorageManagerObject();
                log.printDbg("created StorageManagerObject");
            } catch(e) {
                log.printErr("failed create StorageManagerObject");
                log.printExec(e);
            }
        }
        
        return storage_manager_obj;
    }
    
    function _getMouseControl() {
        if (!mouse_control_obj) {
            try {
            	mouse_control_obj = oipfFactory.createMouseControlObject();
                log.printDbg("created MouseControlObject");
            } catch(e) {
                log.printErr("failed create MouseControlObject");
                log.printExec(e);
            }
        }
        
        return mouse_control_obj;
    }
    
    return {
        init: _init,
        
        addEventListener: _addEventListener,
        removeEventListener: _removeEventListener,
        
        get networkInterface() {
            return {
                getIpAddress: _getIpAddress,
                getMacAddress: _getMacAddress
            }
        },
        
        get bluetoothInterface() {
            return bluetooth_interface;
        },
        
        setPowerState: _setPowerState,
        getPowerState: _getPowerState,
        getSupportedPowerStates: _getSupportedPowerStates,
        
        /**
         * OTS 위성 상태 체크를 위한 함수
         */
        get enableTuner() {
            var result = true;
            
            try {
                result = getLocalSystem().tuners[0].enableTuner;
            }
            catch(e) {
                log.printErr("failed get enable tuner");
                log.printExec(e);
            }
            
            return result;
        },
        
        getModelName: _getModelName,
        
        getAVOutputTvAspectRatio:_getAVOutputTvAspectRatio,
        setAVOutputTvAspectRatio:_setAVOutputTvAspectRatio,
        getAVOutputVideoMode:_getAVOutputVideoMode,
        setAVOutputVideoMode:_setAVOutputVideoMode,
        
        addVideoOutputChangeListener:_addVideoOutputChangeListener,
        removeVideoOutputChangeListener:_removeVideoOutputChangeListener,
        
        setHDVideoFormat:_setHDVideoFormat,
        getHDVideoFormat:_getHDVideoFormat,
        getAVOutputHDVideoFormat:_getAVOutputHDVideoFormat,
        setAVOutputHDVideoFormat:_setAVOutputHDVideoFormat,
        getAVOutputDigitalAudioMode:_getAVOutputDigitalAudioMode,
        setAVOutputDigitalAudioMode:_setAVOutputDigitalAudioMode,
        confirmHDVideoFormat:_confirmHDVideoFormat,
        isSupportedHdVideoFormats:_isSupportedHdVideoFormats,
        getSupportedDigitalAudioModes:_getSupportedDigitalAudioModes,
        
        getLNB:_getLNB,
        setLNB:_setLNB,
        setTSInfo:_setTSInfo,
        getTSInfos:_getTSInfos,
        getAdditionalTunerInfos:_getAdditionalTunerInfos,
        
        getCollectibleList:_getCollectibleList,
        collectOneTransponder:_collectOneTransponder,
        tune:_tune,
        
        getStorages: _getStorages,
        setEnableAVOutput : _setEnableAVOutput,

        get mouseControl() {
        	return _getMouseControl();
        }
    }
}());