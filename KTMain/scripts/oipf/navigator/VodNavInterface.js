"use strict";

/**
 * VOD 관련하여 네비게이터와 Interface 하기 위한 모듈. 
 * 
 * 1 . VOD 상태를 Navigator -> KidsCare App 에 알려주기 위한 interface.
 * 2 . OAM App 에 VOD 상태를 알려주는 Interface.  
 *  
 */
KTW.oipf.navigator.VodNavInterface = (function(){
	//var messageManager = KTW.managers.MessageManager;
	var log = KTW.utils.Log;
	var util = KTW.utils.util;
	
	var nav_vod_interface = null;
	try {
		nav_vod_interface = acba("js:nav/NVOD");
	} catch (e) {
		log.printExec(e);
		
		nav_vod_interface = {
				setVODLoading: function() {},
				setVODStarted: function() {},
				setVODPlayRate: function() {},
				setVODEnd: function() {}
		}
	}
	
	
	function _loadContent() {
		log.printDbg("navNVOD:setVODLoading");
		try {
			nav_vod_interface.setVODLoading();
		} catch (e) {
			log.printExec(e);
		}
	}

    /**
     * navigator 에 vod start 를 전달
     *
     * @param content_id
     * @param title
     * @param assetId
     */
	//function _playVOD(content_id, title, ad_info, assetId) {
    function _playVOD(title, assetId, genre) {
		log.printDbg("navNVOD:_playVOD title:" + title + " assetId:" + assetId, " genre:" + genre);

        /**
		 * [dj.son] 먼저 setVODStartedWithGenre 를 호출하고, 실패시 setVODStarted 호출하도록 수정
         */
        
		var bSuccess = false;
        try {
            nav_vod_interface.setVODStartedWithGenre(assetId, title, genre);
            bSuccess = true;
        }
        catch (e) {
            log.printExec(e);
		}

        if (!bSuccess) {
            try {
                nav_vod_interface.setVODStarted(assetId, title);
                bSuccess = true;
            }
            catch (e) {
                log.printExec(e);
            }
		}

		if (bSuccess) {
            notifyHide();
            // 2016.12.02 dhlee 현재 시점에서는 광고 정보를 알 수 없으므로 별도 함수를 통해 전달하도록 위치 변경
            //notifyVodPlayingToOAMnMashUp(ad_info, assetId);
            //notifyVodPlayingToSmartPush(assetId)
		}
	}

    /**
     * VOD 광고 획득 가능한 시점에 외부 app 에 vod play 메시지(with ad_info)를 전달하도록 별도 함수 추가
     *
     * @param ad_info
     * @param content_info
     * Key	Value
     * conts_id	Asset ID
     * cat_id	카테고리 ID
     * series_id	시리즈 컨텐츠 경우 카테고리 ID 전달, 그렇지 않을 경우 null 값 전달
     * viewpoint	AMOC getLinkTime 이어보기 시간(초 단위) 전달, 처음 보는 VOD일 경우 “0” 전달
     * duration	플레이 타임 (초 단위)
     * rating	PR Info (01, 02, 03, 04, 05, 06)
     */
    function _notifyStartVOD(ad_info, content_info) {
        log.printDbg("navNVOD:_notifyStartVOD ad_info = " + JSON.stringify(ad_info) + ", content_info = " + JSON.stringify(content_info));

        notifyVodPlayingToOAMnMashUp(ad_info, content_info);
        notifyVodPlayingToSmartPush(content_info);
    }
	
	function _changePlaySpeed(speed) {
		log.printDbg("navNVOD:_changePlaySpeed speed:" + speed);
		try {
			nav_vod_interface.setVODPlayRate(speed);
		} catch (e) {
			log.printExec(e);
		}
	}
	
	function _stopVOD() {
		log.printDbg("navNVOD:_stopVOD");
		try {
			nav_vod_interface.setVODEnd();
			
			/** mook_t 이 코드가 필요할까요 ?. 
			if(container.emptyHomeStack() && PopupManager.existPopup() == false) {
				notifyHomeState(false, HOME_STATE_HIDE);	
			}
			 */
			notifyVodStop();
		} catch (e) {
			log.printExec(e);
		}
	}
	
	function parseADInfo(ad_info) {
		var ad_info_list = [];
		
		/**
		 * AdsInfo 패턴"M42F5AJSSGL0800143_K20130516050845.mpg,M64F56VOSGL0800001.mpg[-1*1]" 반복
		 */
		//if (ad != "0" && ad != "") {
		if (ad_info && ad_info.indexOf(".mpg") > 0) {
			var temp = ad_info.split("]");
			for (var i=0; i<temp.length; i++) {
				if (temp[i].length < 10)
					break;
				var pos = temp[i].substring(temp[i].indexOf("[")+1);
				var asset = temp[i].substring(0, temp[i].indexOf("["));
				asset = asset.substring(temp[i].lastIndexOf(",")+1);
				//"Ad_ContentID":"M64E4123SGL0800001.mpg" ,  "Ad_Pos":-1  ,"Ad_Repeat":1} ,
				ad_info_list[i] = {
						"Ad_ContentID" : asset,
						"Ad_Pos" : parseInt(pos.substring(0, pos.indexOf("*"))),
						"Ad_Repeat" : parseInt(pos.substring(pos.indexOf("*")+1))
				};
				
				log.printDbg("ad_info at " + i + " = " + JSON.stringify(ad_info_list[i]));
			}
		}
		
		return ad_info_list;
	}

    /**
     * AMOC 에서 전달하는 PR 값을 3rd party app 전달 규격에 맞게 변환 한다.
     *
     * @param pr_info
     * @returns {string}
     */
	function parsePrInfo(pr_info) {
		if (!pr_info || pr_info == "0") { 
			return "00";
		} else if(pr_info == '02'){
			return "07";
	    }else if(pr_info == '03'){
	    	return "12";
	    }else if(pr_info == '04'){
	    	return "15";
	    }else if(pr_info == '05'){
	    	return "19";
	    }else{
	    	return "21";
	    }
	}
	
	 function notifyHide() {
	     // vod play 시점에 stack 및 popup UI 들이 없다면
         // obs_notifyHide 를 전달하도록 한다
         // 이렇게 해야 OAM 이나 smart push 등에서 띄우는 UI가
         // 문제 없이 노출된다.
         // 혹여 홈포탈에서 띄우는 UI가 있다면 어차피
         // obs_reqShow를 할 것이라서 문제 없다.
         var notify_hide = true;
         var last_layer = KTW.ui.LayerManager.getLastLayerInStack();
         log.printDbg("navNVOD:_playVOD last_layer = " + last_layer);
         if (util.isValidVariable(last_layer) === true) {
             if (last_layer.type === KTW.ui.Layer.TYPE.POPUP ||
                 (last_layer.type === KTW.ui.Layer.TYPE.NORMAL && last_layer.isShowing() === true)) {
                 notify_hide = false;
             }
         }
         
         if (notify_hide === true) {
             var message = {
                 "method": KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE,
                 "from": KTW.CONSTANT.APP_ID.HOME,
                 "to" : KTW.CONSTANT.APP_ID.OBSERVER };
             KTW.managers.MessageManager.sendMessage(message);
         }
    }

    /**
     * VOD가 시작되었음을 알린다.
     *
     * @param ad_info
     * @param content_info
     */
	function notifyVodPlayingToOAMnMashUp(ad_info, content_info) {
		var cust_env = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CUST_ENV));
		var ad_info_list = parseADInfo(ad_info);

		//hide App.
		var message = {
            "method": KTW.managers.MessageManager.METHOD.OAM.VOD_START,
            "from": KTW.CONSTANT.APP_ID.HOME,
            
			"contentID" : content_info.const_id,
			"rating" : content_info.rating,
			"categoryID" : content_info.cat_id,
			"DongCode" : cust_env ? cust_env.dongCd : "",
			"NodeCode" : cust_env ? cust_env.nodeCd : "",
			"AdInfoCount" : ad_info_list ? ad_info_list.length : 0,
			"AdInfoList" : ad_info_list
		};
		
		//기존엔 30ms delay 가 있었는데 일단 없앰.
		message.to = KTW.CONSTANT.APP_ID.OAM;
        KTW.managers.MessageManager.sendMessage(message);
        message.to = KTW.CONSTANT.APP_ID.MASHUP;
        KTW.managers.MessageManager.sendMessage(message);
	}
	
    /**
     * VOD Start 되었음을 알린다.
     *
     * @param content_info  vod content meta object
     */
	function notifyVodPlayingToSmartPush(content_info) {

		var message = {
	            "method": KTW.managers.MessageManager.METHOD.SMART_PUSH.VOD_START,
	            "from": KTW.CONSTANT.APP_ID.HOME,
	            "to" : KTW.CONSTANT.APP_ID.SMART_PUSH,
	            
	            'const_id' : content_info.const_id,
	            'cat_id' : content_info.cat_id,
	            'viewpoint' : content_info.viewpoint,
	            'duration' : content_info.duration ? content_info.duration : undefined,

	            'rating' : parsePrInfo(content_info.rating),
	            'series_id' : content_info.series_id != null ? content_info.series_id : undefined
		};
		
		//기존엔 30ms delay 가 있었는데 일단 없앰.
        KTW.managers.MessageManager.sendMessage(message);
	}
	
	function notifyVodStop() {
		var message = {
			"from": KTW.CONSTANT.APP_ID.HOME
		};
		
		message.method = KTW.managers.MessageManager.METHOD.OAM.VOD_STOP;
		message.to = KTW.CONSTANT.APP_ID.OAM;
        KTW.managers.MessageManager.sendMessage(message);
        
        message.method = KTW.managers.MessageManager.METHOD.OAM.VOD_STOP;
        message.to = KTW.CONSTANT.APP_ID.MASHUP;
        KTW.managers.MessageManager.sendMessage(message);
        
        message.to = KTW.CONSTANT.APP_ID.SMART_PUSH;
        message.method = KTW.managers.MessageManager.METHOD.SMART_PUSH.VOD_STOP;
        KTW.managers.MessageManager.sendMessage(message);
	}
	
	return {
		loadContent: _loadContent,
		playVOD: _playVOD,
		changePlaySpeed: _changePlaySpeed,
		stopVOD: _stopVOD,
        notifyStartVOD: _notifyStartVOD
	}
}());
