/**
 * Created by user on 2016-06-04.
 */


(function() {

    var framework_table = new KTW.utils.HashMap();
    var _DEF_FRAMEWORK = {
        KEY_CODE: "KEY_CODE",

        ENV_CONSTANT: "ENV_CONSTANT",
        MENU_CONSTANT: "MENU_CONSTANT",
        SUBHOME_CONSTANT: "SUBHOME_CONSTANT",
        FILTER_CONSTANT: "FILTER_CONSTANT",

        OIPF_DEF: "OIPF_DEF",
        OIPF_ADAPTER: "OIPF_ADAPTER",
        OIPF_OBJECTFACTORY : "OIPF_OBJECTFACTORY",

        AJAX_FACTORY: "AJAX_FACTORY",
        HDS_MANAGER: "HDS_MANAGER" ,

        LAYER_MANAGER: "LAYER_MANAGER",
        LAYER: "LAYER",
        VIEW: "VIEW",
        EPG_VIEW: "EPG_VIEW",

        MODULE_MANAGER: "MODULE_MANAGER",
        MODULE_MAPPING_MANANGER: "MODULE_MAPPING_MANAGER",
        MODULE_DEF : "MODULE_DEF",
        MODULE : "MODULE",

        MENU_DATA_MANAGER: "MENU_DATA_MANAGER",
        SUB_HOME_DATA_MANAGER: "SUB_HOME_DATA_MANAGER",

        ADULT_AUTHORIZED_CHECK: "ADULT_AUTHORIZED_CHECK",
        AUTH_MANAGER: "AUTH_MANAGER",
        APP_SERVICE_MANAGER: "APP_SERVICE_MANAGER",
        RESERVATION_MANAGER: "RESERVATION_MANAGER",
        STATE_MANAGER: "STATE_MANAGER",
        MESSAGE_MANAGER: "MESSAGE_MANAGER",
        PIP_MANAGER: "PIP_MANAGER",
        STORAGE_MANAGER: "STORAGE_MANAGER",
        FAVOURITE_CHANNEL_MANAGER: "FAVOURITE_CHANNEL_MANAGER",
        IFRAME_MANAGER: "IFRAME_MANAGER",
        MENU_SERVICE_MANAGER: "MENU_SERVICE_MANAGER",
        SETTING_MENU_MANAGER: "SETTING_MENU_MANAGER",
        POWER_MODE_MANAGER: "POWER_MODE_MANAGER",
        DEVICE_MANAGER: "DEVICE_MANAGER",
        NETWORK_MANAGER: "NETWORK_MANAGER",

        HOMESHOT_DATA_MANAGER: "HOMESHOT_DATA_MANAGER",

        TVPAY_MANAGER: "TVPAY_MANAGER",

        KIDS_MODE_MANAGER: "KIDS_MODE_MANAGER",
        EPG_REL_MANAGER: "EPG_REL_MANAGER",
        USER_LOG_MANAGER: "USER_LOG_MANAGER",

        OTMP_MANAGER: "OTMP_MANAGER",
        KOL_MANAGER: "KOL_MANAGER",

        VOD_NAV_INTERFACE: "VOD_NAV_INTERFACE",

        UTIL: "UTIL",
        EPG_UTIL: "EPG_UTIL",
        LOG: "LOG",
        KTW_DEF : "KTW_DEF",
        COMMON_IMAGE: "COMMON_IMAGE",

        COMPONENT_CLOCK: "COMPONENT_CLOCK",

        STB_BASIC: "STB_BASIC"
    };

    function _init() {
        set({key: _DEF_FRAMEWORK.KEY_CODE, value: KTW.KEY_CODE});

        set({key: _DEF_FRAMEWORK.ENV_CONSTANT, value: KTW.CONSTANT});
        set({key: _DEF_FRAMEWORK.MENU_CONSTANT, value: KTW.DATA.MENU});
        set({key: _DEF_FRAMEWORK.SUBHOME_CONSTANT, value: KTW.DATA.SUBHOME});
        set({key: _DEF_FRAMEWORK.FILTER_CONSTANT, value: KTW.DATA.FILTER});
        set({key: _DEF_FRAMEWORK.COMMON_IMAGE, value: KTW.COMMON_IMAGE});

        set({key: _DEF_FRAMEWORK.OIPF_DEF, value: KTW.oipf.Def});
        set({key: _DEF_FRAMEWORK.OIPF_ADAPTER, value: KTW.oipf.AdapterHandler});
        set({key: _DEF_FRAMEWORK.OIPF_OBJECTFACTORY, value: KTW.oipf.oipfObjectFactory});

        set({key: _DEF_FRAMEWORK.LAYER_MANAGER, value: KTW.ui.LayerManager});
        set({key: _DEF_FRAMEWORK.LAYER, value: KTW.ui.Layer});
        set({key: _DEF_FRAMEWORK.VIEW, value: KTW.ui.View});
        set({key: _DEF_FRAMEWORK.EPG_VIEW, value: KTW.ui.view.EpgView});

        set({key: _DEF_FRAMEWORK.MODULE_MANAGER, value: KTW.managers.module.ModuleManager});
        set({key: _DEF_FRAMEWORK.MODULE_DEF, value: KTW.managers.module.Def});
        set({key: _DEF_FRAMEWORK.MODULE, value: KTW.managers.module.Module});

        set({key: _DEF_FRAMEWORK.MENU_DATA_MANAGER, value: KTW.managers.data.MenuDataManager});
        set({key: _DEF_FRAMEWORK.SUB_HOME_DATA_MANAGER, value: KTW.managers.data.SubHomeDataManager});

        set({key: _DEF_FRAMEWORK.AJAX_FACTORY, value: KTW.managers.http.AjaxFactory});
        set({key: _DEF_FRAMEWORK.HDS_MANAGER, value: KTW.managers.http.HDSManager});
        set({key: _DEF_FRAMEWORK.ADULT_AUTHORIZED_CHECK, value: KTW.managers.auth.AdultAuthorizedCheck});
        set({key: _DEF_FRAMEWORK.AUTH_MANAGER, value: KTW.managers.auth.AuthManager});

        set({key: _DEF_FRAMEWORK.APP_SERVICE_MANAGER, value: KTW.managers.service.AppServiceManager});
        set({key: _DEF_FRAMEWORK.RESERVATION_MANAGER, value: KTW.managers.service.ReservationManager});
        set({key: _DEF_FRAMEWORK.STATE_MANAGER, value: KTW.managers.service.StateManager});
        set({key: _DEF_FRAMEWORK.MESSAGE_MANAGER, value: KTW.managers.MessageManager});
        set({key: _DEF_FRAMEWORK.PIP_MANAGER, value: KTW.managers.service.PipManager});
        set({key: _DEF_FRAMEWORK.STORAGE_MANAGER, value: KTW.managers.StorageManager});
        set({key: _DEF_FRAMEWORK.FAVOURITE_CHANNEL_MANAGER, value: KTW.managers.service.FavoriteChannelManager});
        set({key: _DEF_FRAMEWORK.IFRAME_MANAGER, value: KTW.managers.service.IframeManager});
        set({key: _DEF_FRAMEWORK.MENU_SERVICE_MANAGER, value: KTW.managers.service.MenuServiceManager});
        set({key: _DEF_FRAMEWORK.SETTING_MENU_MANAGER, value: KTW.managers.service.SettingMenuManager});
        set({key: _DEF_FRAMEWORK.POWER_MODE_MANAGER, value: KTW.managers.device.PowerModeManager});
        set({key: _DEF_FRAMEWORK.DEVICE_MANAGER, value: KTW.managers.device.DeviceManager});
        set({key: _DEF_FRAMEWORK.NETWORK_MANAGER, value: KTW.managers.device.NetworkManager});
        set({key: _DEF_FRAMEWORK.HOMESHOT_DATA_MANAGER, value: KTW.managers.service.HomeShotDataManager});
        set({key: _DEF_FRAMEWORK.TVPAY_MANAGER, value: KTW.managers.service.TVPayManager});


        set({key: _DEF_FRAMEWORK.KIDS_MODE_MANAGER, value: KTW.managers.service.KidsModeManager});
        set({key: _DEF_FRAMEWORK.EPG_REL_MANAGER, value: KTW.managers.service.EpgRelatedMenuManager});
        set({key: _DEF_FRAMEWORK.USER_LOG_MANAGER, value: KTW.managers.UserLogManager});

        set({key: _DEF_FRAMEWORK.OTMP_MANAGER,value: KTW.managers.otmPairing.OTMPManager});
        set({key: _DEF_FRAMEWORK.KOL_MANAGER,value: KTW.managers.otmPairing.KOLManager});

        set({key: _DEF_FRAMEWORK.VOD_NAV_INTERFACE, value: KTW.oipf.navigator.VodNavInterface});

        set({key: _DEF_FRAMEWORK.UTIL, value: KTW.utils.util});
        set({key: _DEF_FRAMEWORK.LOG, value: KTW.utils.Log});
        set({key: _DEF_FRAMEWORK.EPG_UTIL, value: KTW.utils.epgUtil});

        set({key: _DEF_FRAMEWORK.COMPONENT_CLOCK, value: KTW.ui.component.Clock});

        set({key: _DEF_FRAMEWORK.KTW_DEF, value: {
            SAID : KTW.SAID,
            SMARTCARD_ID : KTW.SMARTCARD_ID,
            STB_MODEL: KTW.STB_MODEL,
            RELEASE_MODE : KTW.RELEASE_MODE,
            UI_VERSION : KTW.UI_VERSION,
            FRAMEWORK_VERSION : KTW.FRAMEWORK_VERSION,
            AMOC : {
                AUTH_KEY : KTW.DATA.AMOC.AUTH_KEY,
                TIMEOUT : KTW.DATA.AMOC.TIMEOUT
            },
            IS_VAT_PRICE : true,
            IP : KTW.oipf.AdapterHandler.hwAdapter.networkInterface.getIpAddress()
        }});

        set({key: _DEF_FRAMEWORK.STB_BASIC, value: {reboot: function () {KTW.main.destroy(true);}}});

    }

    function _get(key) {
        return framework_table.get(key);
    }

    function set(options) {
        if (options && options.key && options.value) {
            framework_table.put(options.key, options.value);
        }
    }

    window.homeImpl = {
        DEF_FRAMEWORK: _DEF_FRAMEWORK,
        init: _init,
        get: _get,

        framework_table: framework_table
    };
})();