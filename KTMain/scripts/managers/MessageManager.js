"use strict";

/**
 * postMessage control
 * 
 * send message to observer and receive message form observer or other
 * application
 */
KTW.managers.MessageManager = (function() {

	var log = KTW.utils.Log;
	var util = KTW.utils.util;
	
	var layerManager;
	var serviceAdapter;
	var stateManager;
	var basicAdapter;
	//var vodControl;
	var vodAdapter;
	var hwAdapter;
	var storageManager;
	var navAdapter;
    var moduleManager;

	var receive_listener = [];

	var listeners = [];

	function _init() {
		util = KTW.utils.util;
		
		layerManager = KTW.ui.LayerManager;
		serviceAdapter = KTW.oipf.AdapterHandler.serviceAdapter;
		stateManager = KTW.managers.service.StateManager;
		navAdapter = KTW.oipf.AdapterHandler.navAdapter;
		vodAdapter = KTW.oipf.AdapterHandler.vodAdapter;
		hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
		basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
		storageManager = KTW.managers.StorageManager;
        moduleManager = KTW.managers.module.ModuleManager;

		serviceAdapter.self.show();

		window.addEventListener("message", onReceiveMessage, false);
	}

	/**
	 * 기존 API ==> sendMessage(method, message, to, msgType, src, callback)
	 */
	function _sendMessage(message, callback) {
		var IS_RELEASE = ((KTW.UI_VERSION == "0.01.000") ? false : true);
		var log_fn = IS_RELEASE ? log.printDbg : log.printForced;

        log_fn("_sendMessage(), message = " + JSON.stringify(message));
		try {
			if (callback) {
                receive_listener[message.method] = callback;
			}
			
			if (!message.src) {
				var app = serviceAdapter.findApplications("dvb.appId", message.to);
				log_fn("_sendMessage() method = " + message.method + " message = " + JSON.stringify(message));
				if (app[0]) {
					app[0].window.postMessage(message, serviceAdapter.self);

					if (message.hide_after_send === true) {
						notifyMessage({method:KTW.managers.MessageManager.METHOD.OBS.HIDE});
					}
				} else {
					log_fn("_sendMessage() " + message.to + " not found... so don't send message");
				}
			} else {
				log_fn("_sendMessage() postMessage by src");
				message.src.postMessage(message, serviceAdapter.self);
			}
		} catch (e) {
			log.printExec(e);
		}
	}

    /**
     * 외부 application 으로부터 받은 event message를 처리한다.
     *
     * @param event
     */
	function onReceiveMessage(event) {
		var message = event.data;

		log.printDbg("onReceiveMessage() message = " + JSON.stringify(message));

		if (receive_listener[message.method]) {
            receive_listener[message.method](message);
            receive_listener[message.method] = undefined;
            return;
		}

		message.src = event.source;

		var async = true;
		
		// 다음 API 들은 sync하게 호출
		switch (message.method) {
			case KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW:
			case KTW.managers.MessageManager.METHOD.OBS.NOTIFY_KEY_EVENT:
			case KTW.managers.MessageManager.METHOD.OBS.SHOW:
			case KTW.managers.MessageManager.METHOD.OBS.HIDE:
			case KTW.managers.MessageManager.METHOD.OBS.SET_WATCHING_TIME_BLOCK:
			case KTW.managers.MessageManager.METHOD.HP.NOTIFY_STOP_AV:
				async = false;
				notifyMessage(message);
				break;
		}
		
		if (async === true) {
			setTimeout(function() { processMessage(message); }, 0);
		}
	}

   /**
    * watchContent
    * showDetail
    * showSeriesDetail 이 공통으로 사용하는 response callback function
    */
	function onVodCommonProcessDone(obj) {
		var response_msg = {};
		response_msg.result = obj.result;
		response_msg.from = KTW.CONSTANT.APP_ID.HOME;
		response_msg.method = obj.method;
		response_msg.to = 'undefined';
		response_msg.messageType = 'JSON';
		response_msg.src = obj.src;

		setTimeout(function() {
			_sendMessage(response_msg);
		}, 25);

		/*//oam result 로 1 이 와야 하는데 뭐 이건 아무것도 안옴 -.-..
		if (obj.jsonParams.method == HomeStateManager.HP_WATCH_CONTENT && obj.result == 0) {
			//VOD재생에 실패했으므로 아무작업하지 않는다.
		} else {
			//홈이 뜬다고 알려준다. 그럼으로서 src app은 hide된다. 
			notifyHomeState(true, HOME_STATE_MENU);
		}*/
	}

    /**
     * 외부 appalication 등으로부터 받은 message 종류에 따라 필요한 잡을 수행한다.
     *
     * @param message
     */
	function processMessage(message) {
		log.printDbg("processMessage() " + log.stringify(message));

		// 응답을 줘야 하는 message method
		var response_method;
		var response_msg = {};

        var messageManager = KTW.managers.MessageManager;

        // [kh.kim] byPass 로 오는 경우 실제 method는 message.message.type 에 있음
        // sample
        // message={
        //     "message": {
        //         "data": "10000000000000070242|CV0000000000011|100|부라더|1|1|14000|48|20140331150000|1|||N",
        //         "type": "hp_playPurchasedVOD"
        //     },
        //     "method": "sp_bypass",
        //     "from": "4e30.3015"
        // }
        if (message.method == messageManager.METHOD.HP.BY_PASS) {
            var convertedMessage = {
                method: message.message.type,
                data: message.message.data,
                from: message.from ? message.from : ""
            };
            message = convertedMessage;
        }

		switch (message.method) {
            case messageManager.METHOD.OBS.RELEASE_WATCHING_TIME_BLOCK:

                // [dj.son] RELEASE_WATCHING_TIME_BLOCK 은 AppServiceManager 에서 처리
                notifyMessage(message);
				break;
			case messageManager.METHOD.HP.SHOW_SERIES_DETAIL:
            case messageManager.METHOD.HP.SHOW_CONTENT_DETAIL:
            case messageManager.METHOD.HP.SHOW_SERIES_DETAIL_PURCHASE:
            case messageManager.METHOD.HP.SHOW_CONTENT_DETAIL_PURCHASE:
				// container.setKeyPriority(0); mook_t 기존 코드 state 관련.
				// notifyHomeState(true, HOME_STATE_MENU);
                // 2017.01.13 dhlee
                // TODO 일단 아래와 같이 처리하지만 notifyFn 은 어떤 용도인지 내용을 파악해야 한다.
                var isSeries = false;
                if (message.method === KTW.managers.MessageManager.METHOD.HP.SHOW_SERIES_DETAIL
                    || message.method === KTW.managers.MessageManager.METHOD.HP.SHOW_SERIES_DETAIL_PURCHASE) {
                    isSeries = true;
                }
                var isFocusButton = false;
                if (message.method === KTW.managers.MessageManager.METHOD.HP.SHOW_SERIES_DETAIL_PURCHASE
                    || message.method === KTW.managers.MessageManager.METHOD.HP.SHOW_CONTENT_DETAIL_PURCHASE ) {
                    isFocusButton = true;
                }
                showVodContent(isSeries, isFocusButton, message, cbShowVodContent);
                //postMessage를 통한 Jump
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.POST_MSG,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.VOD_DETAIL,
                    reqPathCd: message.req_cd
                });
				//KTW.vod.VodUtil.showContent((message.method === KTW.managers.MessageManager.METHOD.HP.SHOW_SERIES_DETAIL), message, onVodCommonProcessDone, notifyMessage);
				return; // callback 으로 response 처리.
            case messageManager.METHOD.HP.WATCH_BOOKMARK_CONTENT:
				// fullbrowser상태에서 호출된 경우 self.visible이 false상태여서 홈이 안보이게 된다.
				if (serviceAdapter.self.visible == false) {
					serviceAdapter.self.show();
				}

                if (stateManager.isVODPlayingState()) {
                    // 재생 중에는 seek 만 해야 한다.
                    message.req_cd = message.req_cd ? message.req_cd : KTW.CONSTANT.ENTRANCE_PATH.BOOKMARK;
                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (vodModule) {
                        vodModule.execute({
                            method: "watchBookmarkContent",
                            params: {
                                callback: function(result) {
                                    message.result = result;
                                    onVodCommonProcessDone(message);
                                },
                                const_id: message.const_id,
                                cat_id: message.cat_id,
                                req_cd: message.req_cd,
                                time: message.time
                            }
                        });
                    }
                }
                else {
                    // TODO 2017.05.24 dhlee
                    // DATA 상태인 경우에 대한 처리가 필요하다.
                    message.req_cd = message.req_cd ? message.req_cd : KTW.CONSTANT.ENTRANCE_PATH.BOOKMARK;
                    moduleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                        if (vodModule) {
                            vodModule.execute({
                                method: "watchBookmarkContent",
                                params: {
                                    callback: function(result) {
                                        message.result = result;
                                        onVodCommonProcessDone(message);
                                    },
                                    const_id: message.const_id,
                                    cat_id: message.cat_id,
                                    req_cd: message.req_cd,
                                    time: message.time
                                }
                            });
                        }
                    });
                }
				break;

			//"MF0F600USGL150000100"<-콘텐츠 이용권
			//KTW.managers.MessageManager.processMessage({"method":"hp_watchContent","const_id":"MF0F600USGL150000100", "cat_id":"CV000000000016231018", "req_cd":KTW.vod.EntrancePath.POC});
			//KTW.managers.MessageManager.processMessage({"method":"hp_watchContent","const_id":"MZWF302ASGL150000100", "cat_id":"CV000000000016231018", "req_cd":KTW.vod.EntrancePath.POC});
			//KTW.managers.MessageManager.processMessage({"method":"hp_watchContent","const_id":"MZWF302ASGL150000100"});
			//KTW.managers.MessageManager.processMessage({"method":"hp_watchContentForced","const_id":"MZWF302ASGL150000100", "cat_id":"CV000000000016231018", "req_cd":KTW.vod.EntrancePath.POC});
			//KTW.managers.MessageManager.processMessage({"method":"hp_watchContentForced","const_id":"MZWF302ASGL150000100", "req_cd":KTW.vod.EntrancePath.POC});
			//KTW.managers.MessageManager.processMessage({"cat_id":"","price_yn":"N","seemlees_time":"","method":"hp_watchContentForced","from":"kidscare","otn_said":"","const_id":"MTKF901KSGL150000100","src":{}});
			//KTW.managers.MessageManager.processMessage({"cat_id":"","price_yn":"N","seemlees_time":"","method":"hp_watchContentForced","from":"kidscare","otn_said":"","const_id":"MTKF901LSGL150000100","src":{}});
			case messageManager.METHOD.HP.WATCH_CONTENT:
            case messageManager.METHOD.HP.WATCH_CONTENT_FORCED:
				// fullbrowser상태에서 호출된 경우 self.visible이 false상태여서 홈이 안보이게 된다.
				if (serviceAdapter.self.visible == false) {
					serviceAdapter.self.show();
				}

                // 2017.06.22 dhlee
                // 키즈모드일 때는 안내 팝업을 보여주고 result "0" 으로 response message를 보낸다.
                if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                    showJumpCategoryFailPopup(KTW.ERROR.EX_CODE.KIDS_MODE.EX002);
                    message.result = "0";
                } else {
                    /**
                     * [dj.son] hp_watchContent, hp_watchContentForced 가 온 경우, unbound app 정리 로직 추가 (2.0 로직 적용)
                     */
                    KTW.managers.MessageManager.sendMessage({
                        "method": 'obs_stopOBSManagedApplications',
                        "from": KTW.CONSTANT.APP_ID.HOME,
                        "to" : KTW.CONSTANT.APP_ID.OBSERVER
                    });

                    /**
                     * 모바일 에서 공유목록 재생 시 호출될 수 있음. 이경우 바운드채널에서 뒷배경이 black이 되는 현상이 발생하여
                     * workaround로 처리한다. 무조건 promo로 이동
                     *
                     * [dj.son] 풀브라우저가 실행된 상태인 경우도 채널 튠 해주기 위해 조건문 변경
                     */
                    var timeout = 30;
                    /*var current_channel = navAdapter.getCurrentChannel();
                    if (navAdapter.isDataServiceChannel(current_channel) === true) {
                        navAdapter.changeChannel(navAdapter.getPromoChannel());
                        timeout = 1500;
                    }*/
                    if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                        navAdapter.changeChannel(navAdapter.getPromoChannel());
                        timeout = 1500;
                    }
                    /* mook_t 필요 없을건 같긴 한데 나중에 문제 생기면 확인 필요
                     else if (runningOtherApp) { notifyHomeState(true, HOME_STATE_MENU); }*/

                    setTimeout(function() {
                        /*vodControl.setVodDetailObj(undefined);
                         container.setKeyPriority(0);*/

                        /**
                         * 알 수 없지만, seamless 의 경우는 무조건 forced 로 오고 이때는 otn share list 가 맞을듯.
                         */
                        var default_req_cd = (message.method === KTW.managers.MessageManager.METHOD.HP.WATCH_CONTENT_FORCED) ? KTW.CONSTANT.ENTRANCE_PATH.OTN_SHARE_LIST : KTW.CONSTANT.ENTRANCE_PATH.FOCUS_MOVIE;
                        message.req_cd = message.req_cd ? message.req_cd : default_req_cd;
                        moduleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                            if (vodModule) {
                                vodModule.execute({
                                    method: message.method === KTW.managers.MessageManager.METHOD.HP.WATCH_CONTENT_FORCED ? "watchContentForced" : "watchContent",
                                    params: {
                                        callback: function(result) {
                                            message.result = result;
                                            onVodCommonProcessDone(message);
                                        },
                                        const_id: message.const_id,
                                        cat_id: message.cat_id,
                                        req_cd: message.req_cd
                                    }
                                });
                            }
                        });
                        //KTW.vod.VodUtil.watchContent(message, onVodCommonProcessDone, (message.method === KTW.managers.MessageManager.METHOD.HP.WATCH_CONTENT_FORCED));
                    }, timeout);
                    return; // callback 으로 response 처리.
                }
                break;
			//KTW.managers.MessageManager.processMessage({"method":"hp_showCategory", "cat_id":"MENU_ID"});
            case messageManager.METHOD.HP.SHOW_CATEGORY:
				response_method = message.method;
                response_msg.result = "0";
                var menu = null;

                // 2017.05.23 dhlee
                // MenuDataManager.getMenuData() 에서 return 되는 메뉴는 키즈모드인 경우 키즈 메뉴만 전달 된다.
                // 사용자에게 안내해야 하는 메시지 등이 다른 경우 키즈 모드더라도 전체 메뉴를 return 받아야 할 수 있다.
                // 2017.05.26 dhlee
                // 자동대기모드 팝업이 뜬 경우 네비게이터는 postMessage를 통해 설정 변경을 위한 자동대기모드 설정 화면으로 이동을 요청하게 된다.
                // 이때는 키즈모드이더라도 이동이 되어야 하므로 아래와 같이 예외 처리 한다.
                if (message.cat_id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_STANDBY_MODE) {
                    menu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (tmpMenu) {
                            if (tmpMenu.id === message.cat_id) {
                                return true;
                            }
                        }
                    })[0];
                }
                else {
                    menu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                        cbCondition: function (tmpMenu) {
                            if (tmpMenu.id === message.cat_id) {
                                return true;
                            }
                        }
                    })[0];
                }

                if (menu) {
                    // var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenuId(message.cat_id);
                    var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

                    if (moduleId) {
                        KTW.managers.service.MenuServiceManager.jumpMenu({
                            moduleId: moduleId,
                            menu: menu,
                            jump: true,
                            updateSubTitle: false,  // 설정 메뉴인 경우를 위해 추가
                            cbJumpMenu: function () {
                                if (moduleId === KTW.managers.module.Module.ID.MODULE_FRAMEWORK) {
                                    response_msg.result = "1";
                                } else {
                                    var module = KTW.managers.module.ModuleManager.getModule(moduleId);
                                    if (module) {
                                        response_msg.result = "1";
                                    }
                                }

                                setTimeout(function() {
                                    _sendMessage({
                                        method: KTW.managers.MessageManager.METHOD.HP.SHOW_CATEGORY,
                                        from: KTW.CONSTANT.APP_ID.HOME,
                                        to: message.from,
                                        src: message.src,
                                        result: response_msg.result
                                    });
                                }, 0);
                            }
                        });
                        return;
                    }
                    //postMessage를 통한 Jump
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.JUMP_TO,
                        act: KTW.data.EntryLog.JUMP.CODE.POST_MSG,
                        jumpType: KTW.data.EntryLog.JUMP.TYPE.CATEGORY,
                        reqPathCd: message.req_cd
                    });
                }
                else {
                    showJumpCategoryFailPopup();

                    // TODO 췍 필요
                    //util.showErrorPopup(KTW.ERROR.CODE.E089.split("\n"), null, null, true);
                }

				break;

			//KTW.managers.MessageManager.processMessage({"method":"hp_isVODPlaying"});
			case messageManager.METHOD.HP.IS_VOD_PLAYING:
				response_method = message.method;
	
				if (stateManager.isVODPlayingState()) {
					response_msg.status = "1";
				} else {
					response_msg.status = "0";
				}
				break;
			// 2016.09.05 dhlee R6 형상 적용
			case messageManager.METHOD.HP.GET_CURRENT_VOD_INFO_FOR_BOOKMARK:
			    response_method = message.method;
                var bSuccess = true;

			    if (stateManager.isVODPlayingState()) {
                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (!vodModule) {
                        bSuccess = false;
                    } else {
                        var contentInfo = vodModule.execute({
                            method: "getCurrentVODInfoForBookmark",
                            params: {}
                        });
                        if (!contentInfo) {
                            bSuccess = false;
                        } else {
                            response_msg.const_id = contentInfo.const_id;
                            response_msg.cat_id = contentInfo.catId;
                            response_msg.title = contentInfo.title;
                            response_msg.duration = contentInfo.duration;
                            response_msg.viewpoint = contentInfo.viewpoint;
                        }
                    }
                } else {
                    bSuccess = false;
                }

                if (bSuccess === false) {
                    response_msg.const_id = "error";
                    response_msg.cat_id = "error";
                    response_msg.title = "error";
                    response_msg.duration = "error";
                    response_msg.viewpoint = "error";
                }
			    break;

			//KTW.managers.MessageManager.processMessage({"method":"hp_getCurrentVODInfo"});
			case messageManager.METHOD.HP.GET_CURRENT_VOD_INFO:
                response_method = message.method;
                var bSuccess = true;

                if (stateManager.isVODPlayingState()) {
                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (!vodModule) {
                        bSuccess = false;
                    } else {
                        //var contentInfo = {};
                        var contentInfo = vodModule.execute({
                            method: "getCurrentVODInfo",
                            params: {}
                        });
                        log.printDbg("processMessage(), method = " + message.method + ", contentInfo = " + JSON.stringify(contentInfo));
                        if (!contentInfo) {
                            bSuccess = false;
                        } else {
                            response_msg.const_id = contentInfo.const_id;
                            response_msg.title = contentInfo.title;
                            response_msg.duration = contentInfo.duration;
                            response_msg.viewpoint = contentInfo.viewpoint;
                        }
                    }
                } else {
                    bSuccess = false;
                }

                if (bSuccess === false) {
                    response_msg.const_id = "error";
                    response_msg.title = "error";
                    response_msg.duration = "error";
                    response_msg.viewpoint = "error";
                }
                break;

			//KTW.managers.MessageManager.processMessage({"method":"hp_stopVODPlaying"});
			case messageManager.METHOD.HP.STOP_VOD_PLAYING:
				response_method = message.method;
	
				if (stateManager.isVODPlayingState()) {
                    // TODO 어떻게 할지 지니프릭스와 논의 필요
                    // TODO vodModule 에서 stop process 처리시 AppServiceManager 가 호출되리라 기대됨 -> 확인 필요
                    // [dj.son] AppServiceManager 사용
                    //KTW.managers.service.StateManager.exitToService();
                    //KTW.managers.service.AppServiceManager.changeService({
                    //    nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING
                    //});
					response_msg.result = "0";
                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (!vodModule) {
                        response_msg.const_id = "";
                    } else {
                        var contentInfo = vodModule.execute({
                            method: "stopVODPlaying",
                            params: {}
                        });
                        response_msg.const_id = contentInfo.const_id;
                    }
				} else {
					response_msg.result = "1";
					response_msg.const_id = "";
				}
				break;
	
		    //KTW.managers.MessageManager.processMessage({"method":KTW.managers.MessageManager.METHOD.HP.PAUSE_VOD_PLAYING});
			case messageManager.METHOD.HP.PAUSE_VOD_PLAYING:
				response_method = message.method;
	
				if (stateManager.isVODPlayingState()) {
                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (vodModule) {
                        var result = vodModule.execute({
                            method: "pauseVODPlaying",
                            params: {}
                        });
                        response_msg.result = result;
                    } else {
                        // dhlee 2016.12.28
                        // vod 시청중인데 vod module 정보를 획득하지 못했다? 어떤 상황일까?
                        response_msg.result = "2"
                    }
				} else {
					response_msg.result = "1";
				}
				break;

			//KTW.managers.MessageManager.processMessage({"method":KTW.managers.MessageManager.METHOD.HP.PLAY_VOD_PAUSED});
			case messageManager.METHOD.HP.PLAY_VOD_PAUSED:
				response_method = message.method;
	
				if (stateManager.isVODPlayingState()) {
                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (vodModule) {
                        var result = vodModule.execute({
                            method: "playVODPaused",
                            params: {}
                        });
                        response_msg.result = result;
                    } else {
                        // dhlee 2016.12.28
                        // vod 시청중인데 vod module 정보를 획득하지 못했다? 어떤 상황일까?
                        response_msg.result = "2"
                    }
				} else {
					response_msg.result = "1";
				}
				break;
				
			//new version 이냐 아니냐 차이.
			//KTW.managers.MessageManager.processMessage({"method":KTW.managers.MessageManager.METHOD.HP.SEARCH_RESULT, "srchword":"아이유"});
			case messageManager.METHOD.HP.SEARCH_RESULT:
			case messageManager.METHOD.HP.SEARCH_VOD:
				response_method = message.method;
                var result = processSearchMessage(message);
                log.printDbg("processMessage(), Search Message, result = " + result);
                response_msg.result = result;

                //postMessage를 통한 Jump
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.POST_MSG,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.SEARCH,
                    reqPathCd: message.req_cd
                });
                //if (result != null) {
                //    response_msg.result = result;
                //}
                /*
				if (hwAdapter.getPowerState() == KTW.oipf.Def.CONFIG.POWER_MODE.ON) {
					if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
						response_msg.result = '0';
					} else {
						response_msg.result = '1';
						if (KTW.vod.VodInterface.onNextPlayBlock === true)
							KTW.vod.VodInterface.closeAllRelatedDialog();
						
						var layer_obj = {
							id : KTW.ui.Layer.ID.SEARCH,
							params : {
								data : {
									'req_cd' : message.req_cd,
									'srchword' : message.srchword,
									'srchopt' : message.srchopt,
									'srchQry' : message.srchQry,
									'wordType' : message.wordType,
									'contType' : message.contType,
									'result' : message.result,
									'append' : message.append,
									'jsonParam' : message,
									'isNewVersion': (message.method === KTW.managers.MessageManager.METHOD.HP.SEARCH_RESULT) ? true : false
								}
							}
						};
						
						// 양방향 서비스 중인 경우에는 obs_show 메세지를 먼저 전달하고
						// 이후 requestShow 를 처리하도록 한다.
						if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.DATA) {
							var msg_obj = {
                                "method": KTW.managers.MessageManager.METHOD.OBS.SHOW,
                                "key": "*",
                                "args": "PREV_STATE",
                                "layer": layer_obj
                            };
							notifyMessage(msg_obj);
							
							var obs_msg = {
						            "method": KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW,
						            "from": KTW.CONSTANT.APP_ID.HOME,
						            "to" : KTW.CONSTANT.APP_ID.OBSERVER };
						    _sendMessage(message);
						}
						else {
							// clear stack 으로 모두 지우고 검색 띄우기.
							layerManager.activateLayer(layer_obj, true);
						}
						
						// notifyHomeState(true, HOME_STATE_MENU);
					}
				}
				*/
				break;
	
			case messageManager.METHOD.HP.START_SEAMLESS_PAIRING:
				//KTW.managers.MessageManager.processMessage({"method":"startSeamlessPairing","from":"kidscare"});
				storageManager.ps.save(storageManager.KEY.OTN_PARING, "Y");
				break;

			//KTW.managers.MessageManager.processMessage({"method":"stopSeamlessPairing","from":"kidscare"});
			case messageManager.METHOD.HP.STOP_SEAMLESS_PAIRING:
				storageManager.ps.save(storageManager.KEY.OTN_PARING, "N");
				break;

			//KTW.managers.MessageManager.processMessage({"method":KTW.managers.MessageManager.METHOD.HP.CHECK_VERSION,"from":"kidscare"});
			case KTW.managers.MessageManager.METHOD.HP.CHECK_VERSION:
				response_method = message.method;
				response_msg.version = KTW.UI_VERSION;
				response_msg.api = "";
	
				for (var key in KTW.managers.MessageManager.METHOD.HP) {
					var item = KTW.managers.MessageManager.METHOD.HP[key];
					
					if(item == KTW.managers.MessageManager.METHOD.HP.SHOW_CHILD_APP)
						continue;
					response_msg.api += item;
					response_msg.api += ",";
				}
	
				response_msg.api = response_msg.api.substring(0, response_msg.api.length - 1);
				log.printDbg("support api:" + response_msg.api);
				break;

			//messageManager.processMessage({"method":messageManager.METHOD.HP.REQ_RESIZE_AV,"x":"0", "y":"0", "width":"1280", "height":"720", "type":"ch"});
			case messageManager.METHOD.HP.REQ_RESIZE_AV :
				//if(message.type == "vod") {
				//	KTW.oipf.adapters.vodAdapter.resizeVODScreen(message.x, message.y, message.width, message.height);
				//} else if(message.type ==  "ch") {
				//	KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(parseInt(message.x), parseInt(message.y), parseInt(message.width), parseInt(message.height), message.type);
				//}
                KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(parseInt(message.x), parseInt(message.y), parseInt(message.width), parseInt(message.height));
	            break;

			//messageManager.processMessage({"method":messageManager.METHOD.HP.GET_DONG_CODE});
			case messageManager.METHOD.HP.GET_DONG_CODE:
				response_method = message.method;
				
				var cust_env = JSON.parse(storageManager.ps.load(storageManager.KEY.CUST_ENV));
				
				if(cust_env && cust_env.dongCd) {
					response_msg.dongCd = cust_env.dongCd;
				} else {
                    // [dj.son] error 수정
					response_msg.dongCd = "";
				}
				break;

			//messageManager.processMessage({"method":messageManager.METHOD.HP.GET_PKG_CODE});
			case messageManager.METHOD.HP.GET_PKG_CODE:
				response_method = message.method;
				
				//.. 이게 중간에 상품이 가입되면 최신값이 리턴 안될 수 있어서. 
				//.. 항상 최신값을 주려면 API Call 을 해야 함. 
				/*storageManager.ps.save(storageManager.KEY.PACKAGE_LIST, pkg_code);
	            storageManager.ps.save(storageManager.KEY.PACKAGE_LIST_BASE, base_pkg_code);*/
				var pkg_code = util.getPkgCode();
							
				response_msg.pkgCode = pkg_code.base;
				response_msg.addPkgCode = pkg_code.additional;
				break;

			//messageManager.processMessage({"method":messageManager.METHOD.HP.GET_MINI_EPG_STATUS});
			case messageManager.METHOD.HP.GET_MINI_EPG_STATUS:
				response_method = message.method;

                var miniEpgLayer = layerManager.getLayer(KTW.ui.Layer.ID.MINI_EPG);

                if (miniEpgLayer && miniEpgLayer.isShowing()) {
                    response_msg.status = "show";
                }
                else {
                    response_msg.status = "hide";
                }
				break;
				
			case messageManager.METHOD.HP.REQUEST_CHANNEL_UPDATE:
				navAdapter.updateDIDatabase();
                setTimeout(function () {
                    KTW.managers.service.AppServiceManager.requestCustInfo(null);
                }, 0);
				break;

			//messageManager.processMessage({"method":messageManager.METHOD.HP.IS_PIP_ON});
			case messageManager.METHOD.HP.IS_PIP_ON :
			    response_method = message.method;
	            response_msg.result = isPipOn();
	            break;
	            
			case messageManager.METHOD.HP.STOP_VOD_WITH_CHANNEL_SELECTION:
				if (stateManager.isVODPlayingState() === true) {
					var channel = navAdapter.getChannelByCCID(message.ccid);

                    var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (vodModule) {
                        var result = vodModule.execute({
                            method: "stopVodWithChannelSelection",
                            params: {
                                channelObj: channel
                            }
                        });
                    } else {
                        // TODO 2017.01.17 dhlee something error.
                    }
				}
				break;
				
			/*case messageManager.METHOD.HP.NOTIFY_STOP_AV:
				notifyMessage(message);
				break;*/
	
			case messageManager.METHOD.HP.HIDE_OTHER_APP:
			    notifyMessage(message);
				break;
	
            case messageManager.METHOD.HP.SHOW_OTHER_APP:
                // TODO HIDE_OTHER_APP 처럼 AppServiceManager 에서 처리해도 될듯, 나중에 확인

			    // unbound app이 활성화 되었다는 것으로 노출되고 있던 UI를 정리한다.
			    if (stateManager.isTVViewingState() === true) {
			        // PIP channel control을 stop
		            var pip_cc = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
		            if (pip_cc !== null && pip_cc.getState() === KTW.nav.Def.CONTROL.STATE.STARTED) {
		                pip_cc.stop();
		            }
		            
		            // hide promo trigger
		            KTW.managers.service.PromoManager.stopTrigger();
			    }
			    else if (stateManager.isVODPlayingState() === true) {
			        // WaterMark .. hide
	                // vod caption hide.
	                // vod control view hide
                    //vodAdapter.changeExtVodViewVisible(false);
		            // vod 일때 key priority 를 container.setKeyPriority(-1, undefined, 1);
			        // 로 바꾸는데 왜 ?.
			    }
			    
			    // child App 이 있으면 child App destroy.
                // [dj.son] AppServiceManager 사용
                KTW.managers.service.AppServiceManager.destroyChildApp();
                //stateManager.destroyChildApp();
	
				// vbo 가 stopped state 이고, "4e30.3019"(개인방송앱) 때문에 불린게 아니고
				// 기존 state 가 channel 일 경우 (audio 포함) 채널 tune 을 해 준다. (current)
				notifyMessage(message);
				break;
				
			case messageManager.METHOD.HP.GET_AUTO_POWER:
				response_method = message.method;
				response_msg.result = "";
				
				var auto_power = storageManager.ps.load(storageManager.KEY.AUTO_POWER);
				if (util.isValidVariable(auto_power) === true) {
					// 설정된 값이 존재하면 [반복여부;켜짐시간;꺼짐시간]  까지만 전달
	                var temp = auto_power.split(";");
	                
	                var repeat = temp[0];
	                var on_time = temp[1] === "undefined" ? undefined : temp[1];
	                var off_time = temp[2] === "undefined" ? undefined : temp[2];
	                
	                // 자동전원 설정 값 유효성 여부 체크
	                if (util.isValidVariable(repeat) === true &&
	                		util.isValidVariable(on_time) === true &&
	                		util.isValidVariable(off_time) === true) {
	                	response_msg.result = response_msg.result.concat(repeat).concat(";");
	                	response_msg.result = response_msg.result.concat(on_time).concat(";");
	                	response_msg.result = response_msg.result.concat(off_time);
	                }
				}
				break;
				
			case messageManager.METHOD.HP.SET_AUTO_POWER:
				 response_method = message.method;
				 response_msg.result = 0;
				
				 // repeat	반복여부 (매일 - true/ 한번만 - false)
			     // on_time	켜짐시간 (24시간 기준, 오후 6시20분이면 1820)
			     // off_time	켜짐시간 (24시간 기준, 오후 11시 30분이면 2330)
				 if (util.isValidVariable(message, ["repeat", "on_time", "off_time"]) === true) {
					 
					 // parameter value 유효여부 체크
					 if (util.isValidVariable(message.repeat) === true &&
		                		util.isValidVariable(message.on_time) === true &&
		                		util.isValidVariable(message.off_time) === true) {
						 
						 // 시간 자리수 및 범위 유효여부 체크
						 if ((message.on_time.length === 4 && isValidTime(message.on_time) === true) &&
								 (message.off_time.length === 4 && isValidTime(message.off_time) === true)) {
							 
							 var auto_power = "";
							 auto_power = auto_power.concat(message.repeat).concat(";");
							 auto_power = auto_power.concat(message.on_time).concat(";");
							 auto_power = auto_power.concat(message.off_time).concat(";");
							 auto_power = auto_power.concat("false");
							 
							 storageManager.ps.save(storageManager.KEY.AUTO_POWER, auto_power);
							 
							 response_msg.result = 1;
						 }
						 else {
							 log.printErr("processMessage() occured error on " + 
									 response_method + " = not valid time value(length or range)");
						 }
					 }
					 else {
						 log.printErr("processMessage() occured error on " + 
								 response_method + " = not valid parameter");
					 }
				 }
				 else {
					 log.printErr("processMessage() occured error on " + 
							 response_method + " = not exist necessary parameter");
				 }
				break;
            case messageManager.METHOD.HP.SHOW_STITCHING:  // 2016.11.17 실시간 인기 채널로 tune 요청
                response_method = message.method;
                // 기본값 "0" : fail
                // VOD 상태 또는 이미 실시간 인기 채널인 상태에서는 fail 반환
                response_msg.result = "0";

                if (isPossibleStitching() === true) {
                    response_msg.result = "1";
                    if (KTW.CONSTANT.IS_OTS) {
                        if (navAdapter.isAudioChannel(navAdapter.getCurrentChannel()) === true) {
                            var lcw = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).getBackChannel();
                            // TODO 2016.11.17 dhlee BackIframe 과 관련된 일련의 작업을 해줘야 함(iframe 제거)
                            navAdapter.changeChannel(lcw, true);
                        }
                        
                        layerManager.activateLayer({
                            obj: {
                                id: KTW.ui.Layer.ID.STITCHING,
                                type: KTW.ui.Layer.TYPE.NORMAL,
                                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                params: {
                                    fromKey:true
                                }
                            },
                            visible: true
                        });
                    }else {
                        var menu = KTW.managers.data.MenuDataManager.searchMenu({
                            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                            cbCondition: function (tmpMenu) {
                                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL) {
                                    return true;
                                }
                            }
                        })[0];

                        if (menu) {
                            var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

                            if (moduleId) {
                                KTW.managers.service.MenuServiceManager.jumpMenu({
                                    moduleId: moduleId,
                                    menu: menu,
                                    jump: true,
                                    cbJumpMenu: function () {}
                                });

                                return;
                            }
                        }
                    }
                }
                break;
            case messageManager.METHOD.HP.SEEK_VODPLAYING: // 2016.11.17 VOD trick play 요청
            case messageManager.METHOD.HP.SPEED_VODPLAYING:
                response_method = message.method;
                response_msg.result = trickVodPlaying(message);
                break;
            case messageManager.METHOD.MASHUP.IS_SUPPORT_SERIES:   // 2016.11.29 시리즈 지원 여부 확인 응답
                KTW.managers.service.ReservationManager.processMashupSeries(message);
                break;
            case messageManager.METHOD.HP.GET_AUTO_STANDBY: // 2017.03.16 자동 대기 모드 설정 값 요청 (migration 전용)
                response_method = message.method;
                response_msg.result = "";
                var autoStandby = storageManager.ps.load(storageManager.KEY.AUTO_STANDBY);
                if (util.isValidVariable(autoStandby) === true) {
                    autoStandby === "true" ? response_msg.result = "ON" : response_msg.result = "OFF";
                }
                break;
            case messageManager.METHOD.HP.SHOW_HOMESHOT: // [dj.son] 2017.02.07 홈샷 노출
                response_method = message.method;
                response_msg.result = "";

                KTW.managers.service.HomeShotDataManager.addCategoryHomeShot(message);
                break;
            case messageManager.METHOD.HP.DELETE_HOMESHOT: // [dj.son] 2018-02-07 홈샷 삭제
                response_method = message.method;
                response_msg.result = "";

                KTW.managers.service.HomeShotDataManager.removeCategoryHomeShot(message);
                break;
            case messageManager.METHOD.HP.PLAY_PURCHASED_VOD: // [dj.son] 2018-02-07 biz 결제 및 vod play
                response_method = message.method;
                response_msg.result = "";
                // TODO Biz 결제 및 VOD Play 로직 타도록 수정해야 함

                // fullbrowser상태에서 호출된 경우 self.visible이 false상태여서 홈이 안보이게 된다.
                if (serviceAdapter.self.visible == false) {
                    serviceAdapter.self.show();
                }

                // 2017.06.22 dhlee
                // 키즈모드일 때는 안내 팝업을 보여주고 result "0" 으로 response message를 보낸다.
                // [kh.kim] 2018.03.07 키즈모드 상관 없이 PUSH 팝업 노출한다.

                // Normal Layer는 모두 닫는다
                KTW.ui.LayerManager.clearPopupLayer();
                // TV페이 Dummy Layer도 닫는다
                KTW.ui.LayerManager.deactivateLayer({id: KTW.ui.Layer.ID.TV_PAY_DUMMY_PUPUP});

                var timeout = 30;
                /*var current_channel = navAdapter.getCurrentChannel();
                if (navAdapter.isDataServiceChannel(current_channel) === true) {
                    navAdapter.changeChannel(navAdapter.getPromoChannel());
                    timeout = 1500;
                }*/
                if (stateManager.serviceState != KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                    // TODO 양방향 채널인 경우 동작하지 않음..

                    setTimeout(function () {
                        /*vodControl.setVodDetailObj(undefined);
                         container.setKeyPriority(0);*/
                        log.printDbg("processMessage(), method = " + message.method + ", message.data = " + message.data);
                        var pushData = message.data.split("|");
                        /**
                        구매유형
                        단편 : 1
                        시리즈 : 2
                        카테고리PPT : 3
                        채널 PPT : 4
                        패키지 VOD : 5
                         */
                        if (pushData[4] == 4) {
                            // 채널 PPT Push 팝업
                            log.printDbg("processMessage(), pushData.length : " + pushData.length);
                            if(pushData.length > 8 ) {
                                var name = pushData[3];
                                var price = pushData[6];
                                var period = pushData[7];

                                log.printDbg("processMessage() pushData name : " + name + " , price : " + price + " , period : " + period);

                                var isCalled = false;
                                var popupData = {
                                    productName: name,
                                    period : period,
                                    price : price
                                };

                                KTW.ui.LayerManager.activateLayer({
                                    obj: {
                                        id: "BizPurchaseSuccessPopup",
                                        type: KTW.ui.Layer.TYPE.POPUP,
                                        priority: KTW.ui.Layer.PRIORITY.POPUP,
                                        view : KTW.ui.view.popup.BizPurchaseSuccessPopup,
                                        params: {
                                            data : popupData
                                        }
                                    },
                                    visible: true,
                                    cbActivate: function () {}
                                });
                            }


                        }
                        else {
                            // VOD Push 팝업
                            // VOD 모듈의 hp_playPurchasedVod 실행
                            moduleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                                if (vodModule) {
                                    vodModule.execute({
                                        method: message.method,
                                        params: {
                                            data: message.data,
                                            callback: function (result) { // callback 은 필요 없을 듯?
                                                message.result = result;
                                                // onVodCommonProcessDone(message);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }, timeout);
                    return;
                }
                break;
            case messageManager.METHOD.HP.SET_KIDS_MODE_ON_OFF: // [dj.son] 2018-02-27 키즈 리모컨 관련 API 추가
                response_method = message.method;
                response_msg.result = "";

                var kidsMode = null;
                if (message.kids_mode === KTW.oipf.Def.KIDS_MODE.ON) {
                    kidsMode = KTW.oipf.Def.KIDS_MODE.ON;
                }
                else if (message.kids_mode === KTW.oipf.Def.KIDS_MODE.OFF) {
                    kidsMode = KTW.oipf.Def.KIDS_MODE.OFF;
                }

                if (kidsMode) {
                    var onlyChangeSetting = !message.is_show;

                    if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                        onlyChangeSetting = true;
                    }

                    KTW.managers.service.KidsModeManager.changeMode({
                        kidsMode: kidsMode,
                        onlyChangeSetting: onlyChangeSetting
                    });

                    /**
                     * [dj.son] [KTUHDIII-772] 시청 시간 제한 -> 대기 모드 상태에서 키즈 전원 키 눌렀을때, 예외처리 추가 
                     */
                    if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                        KTW.oipf.AdapterHandler.extensionAdapter.releaseTimeRestricted();
                    }
                }

                break;
		} // switch

		// response
		if (response_method) {
            response_msg.method = response_method;
			response_msg.from = KTW.CONSTANT.APP_ID.HOME;
			response_msg.to = message.from;
			response_msg.src = message.src;

			setTimeout(function() { _sendMessage(response_msg); }, 0);
		}
	}

    function isPipOn() {
    	var NAV_CONTROL = KTW.nav.Def.CONTROL;
    	
        var is_pip_on = false;
        var cc = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(NAV_CONTROL.PIP);
        if (cc !== null && cc.getState() === NAV_CONTROL.STATE.STARTED) {
            if (cc.vbo.style.visibility === "visible") {
                is_pip_on = true;
            }
        }
        
        if (is_pip_on === false) {
            cc = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(NAV_CONTROL.STITCHING);
            if (cc !== null && cc.getState() === NAV_CONTROL.STATE.STARTED) {
                if (cc.vbo.style.visibility === "visible") {
                    is_pip_on = true;
                }
            }
        }
        
        return is_pip_on;
    }

    /**
     * 실시간 인기 채널 이동 요청에 대하여 가능한지를 판단한다.
     * 만약 불가능한 상황이면 에러 팝업을 보여준다.
     *
     * @returns {boolean}
     */
    function isPossibleStitching() {
        log.printDbg("isPossibleStitching()");

        if (KTW.managers.service.KidsModeManager.isKidsMode()) {
            log.printDbg("isPossibleStitching() current Kids Mode. so return false");
            KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup();
            return false;
        }
        if (stateManager.isVODPlayingState() === true) {
            log.printDbg("isPossibleStitching() current is vod play state. so return false");
            showStitchingFailPopup();
            return false;
        }

        var stitchingChannelControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.STITCHING);

        if(stitchingChannelControl !== null && stitchingChannelControl.getState() === KTW.nav.Def.CONTROL.STATE.STARTED) {
            log.printDbg("isPossibleStitching() current state is stitching already. so return false");
            return false;
        }

        return true;
    }

    /**
     * 검색 관련 메시지(hp_searchVOD, hp_searchResult)를 처리한다.
     *
     * @param message
     * @returns {*}
     */
    function processSearchMessage(message) {
        log.printDbg("processSearchMessage()");

        var result = null;

        if (hwAdapter.getPowerState() == KTW.oipf.Def.CONFIG.POWER_MODE.ON) {
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED || stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                return "0";
            } else {
                moduleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_SEARCH, function (searchModule) {
                    if (searchModule) {
                        log.printDbg("processSearchMessage$getModuleForced(), find searchModule case");
                        // 양방향 서비스 중인 경우에는 obs_show 메세지를 먼저 전달하고
                        // 이후 requestShow 를 처리하도록 한다.

                        /**
                         * [dj.son] OTHER_APP 상태에서 RequestShow 로직을 타도록 수정
                         * - 음성 비서에서 오는 경우는, 이미 음성비서에서 채널 튠 한 후에 해당 메세지를 보내고 있음
                         */

                        //if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                        ////if (stateManager.isOtherAppState()) {
                        //    log.printDbg("processSearchMessage$getModuleForced(), DATA state");
                        //    var msg_obj = {
                        //        "method": KTW.managers.MessageManager.METHOD.OBS.SHOW,
                        //        "key"   : "*",
                        //        "args"  : "PREV_STATE"
                        //    };
                        //    notifyMessage(msg_obj);
                        //    // Search 관련 메시지를 다시 send 한다.
                        //    // DATA 상태에서 음성비서가 실행되었으므로 DATA 상태를 우선 벗어나고(notifyMessage 이용)
                        //    // 다시 한번 search 관련 메시지를 전송함으로써 else-statement 가 실행되도록 한다.
                        //    _sendMessage(message);
                        //
                        //    //return "1";
                        //    result = "1";
                        //}
                        //else {
                            log.printDbg("processSearchMessage$getModuleForced(), normal state");
                            // clear stack 으로 모두 지우고 검색 띄우기.
                            //layerManager.activateLayer(layer_obj, true);
                            if (message.method === KTW.managers.MessageManager.METHOD.HP.SEARCH_VOD) {
                                result = searchModule.execute({
                                    method: "searchVOD",
                                    params : {
                                        srchword: message.srchword,
                                        srchopt: message.srchopt,
                                        req_cd: message.req_cd
                                    }
                                });
                            } else {
                                result = searchModule.execute({
                                    method: "searchResult",
                                    params : {
                                        srchword: message.srchword,
                                        srchQry: message.srchQry,
                                        srchopt: message.srchopt,
                                        wordType: message.wordType,
                                        contType: message.contType,
                                        result: message.result,
                                        req_cd: message.req_cd
                                    }
                                });
                            }
                            //return result;
                        //}
                    } else {
                        //return "0";
                        result = "0";
                    }
                });
            }
        }

        return result;
    }

    /**
     * 2016.11.17 dhlee 실시간 인기 채널 tune이 불가능할 때 보여지는 오류 팝업
     */
    function showStitchingFailPopup() {
        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["알림"],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                message: KTW.ERROR.EX_CODE.HOME.EX001.message,
                cssObj: {}
            }],
            isShowSaid : true,
            arrButton: [{id: "ok", name: "확인"}],
            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;

                    if (buttonId) {
                        KTW.ui.LayerManager.deactivateLayer({
                            id: "RealTimeLayerBlockGuidePopup",
                            remove: true
                        });
                    }
                }
            }
        };

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "RealTimeLayerBlockGuidePopup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true,
            cbActivate: function () {}
        });
    }

    /**
     * hp_showCategory Message 에 대한 예외 처리
     */
    function showJumpCategoryFailPopup(msg) {
        if (KTW.managers.service.KidsModeManager.isKidsMode()) {
            // 키즈모드일 때 안내 팝업 실행
            KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup(msg);
        } else {
            // 키즈모드가 아닐때 안내 팝업 실행
            log.printDbg("showJumpCategoryFailPopup(), normal mode");
            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                    message: ["알림"],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                    message: KTW.ERROR.CODE.E089,
                    cssObj: {}
                }],
                isShowSaid : true,
                arrButton: [{id: "ok", name: "확인"}],
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        if (buttonId) {
                            KTW.ui.LayerManager.deactivateLayer({
                                id: "CannotJumpCategoryPopup",
                                remove: true
                            });
                        }
                    }
                }
            };

            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: "CannotJumpCategoryPopup",
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData
                    }
                },
                visible: true,
                cbActivate: function () {}
            });
            //util.showErrorPopup(KTW.ERROR.CODE.E089.split("\n"), null, null, true);
        }
    }

    /**
     * VOD Content 상세 화면을 제공하기 위하여 VODImpl 의 API를 호출한다.
     *
     * @param isSeries
     * @param isFocusButton
     * @param message
     * @param callback
     */
    function showVodContent(isSeries, isFocusButton, message, callback) {
       log.printDbg("showVodContent(), isSeries = " + isSeries);

        var param = {
            "jsonParam": message,
            "callback": callback,
            "isSeries": isSeries,
            "reqPathCd": message.req_cd
        };

        if (isSeries === true && !message.cat_id) {
            log.printWarn("showVodContent(), cat_id does not exist");
            if (callback) {
                param.result = 0;
                callback(param);
            }
        } else if (isSeries === false && !message.const_id) {
            log.printWarn("showVodContent(), const_id does not exist");
            if (callback) {
                param.result = 0;
                callback(param);
            }
        } else {
            moduleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                if (vodModule) {
                    // 2017.02.09 dhlee VODImpl 규격서 변경에 따라 callback parameter를 추가한다.
                    vodModule.execute({
                        method: isFocusButton ? "showDetailPurchase" : "showDetail",
                        params: {
                            cat_id: message.cat_id,
                            const_id: message.const_id,
                            req_cd: message.req_cd,
                            forced: true,   // 2017.09.04 dhlee 외부에서 요청이 온 경우는 forced 값을 true로 보낸다.
                            callback: function (result) {
                                param.result = result;
                                callback(param);
                            }
                        }
                    });
                }
            });
        }
    }

    /**
     * showDetail, showSeriesDetail 에 대한 공통 response callback
     *
     * @param param
     */
    function cbShowVodContent(param) {
        var response_msg = {};
        response_msg.result = param.result;
        response_msg.from = KTW.CONSTANT.APP_ID.HOME;
        response_msg.method = param.jsonParam.method;
        response_msg.to = 'undefined';
        response_msg.messageType = 'JSON';
        response_msg.src = param.jsonParam.src;

        setTimeout(function() {
            _sendMessage(response_msg);
        }, 25);
    }

    /**
     * moduleId 에 대응되는 Module 정보를 전달한다.
     *
     * @param moduleId
     * @returns {*}
     */
    /*
    function getModuleForced(moduleId) {
        log.printDbg("getModuleForced(), moduleId = " + moduleId);

        var moduleManager = KTW.managers.module.ModuleManager;
        var module = null;

        module = moduleManager.loadModule({
            moduleId: moduleId,
            cbLoadModule: function(bResult) {
                if (bResult) {
                    module = moduleManager.getModule(moduleId);
                } else {
                    module = null;
                }
            }
        });

        return module;
    }
    */

    /**
     * 2016.11.17 dhlee
     * 외부 요청에 따라 VOD trick play 를 요청한다.
     * VOD module 정보를 찾은 후 method를 호출한다.
     *
     * @param message
     * @return "0" for abnormal case, "1" for normal case
     */
    function trickVodPlaying(message) {
        log.printDbg("trickVodPlaying(), message = " + JSON.stringify(message));

        var result = "0";

        if (stateManager.isVODPlayingState() === true) {
            var vodModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
            if (!vodModule) {
                result = "1";
            } else {
                if (message.seek !== undefined) {
                    result = vodModule.execute({
                        method: "seekVODPlaying",
                        params: {
                            seek: message.seek
                        }
                    });
                } else if (message.speed != undefined) {
                    result = vodModule.execute({
                        method: "speedVODPlaying",
                        params: {
                            speed: message.speed
                        }
                    });
                }
            }
        }

        return result;
    }
    
    function isValidTime(time_str) {
        var hours = parseInt(time_str.substring(0, 2));
        var minutes = parseInt(time_str.substring(2, 4));
        var seconds = 0;
        if (time_str.length > 4) {
            seconds = parseInt(time_str.substring(4, 6));
        }
        
        return ((hours * 60 * 60) + (minutes * 60) + seconds) <= (24 * 60 * 60);
    }
    
	function _addMessageListener(listener) {
		listeners.push(listener);
	}

	function _removeMessageListener(listener) {
		var index = listeners.indexOf(listener);
		if (index !== -1) {
			listeners.splice(index, 1);
		}
	}

	function notifyMessage(message) {
		var length = listeners.length;
		for (var i = 0; i < length; i++) {
			listeners[i](message);
		}
	}

	function _sendInternalMessage(message) {
		if (message.method === KTW.managers.MessageManager.METHOD.CUSTOM.NOTIFY_PLAY_AV) {
			notifyMessage(message);
		}
	}
	
	return {
		init : _init,

		sendMessage : _sendMessage,

		addMessageListener : _addMessageListener,
		removeMessageListener : _removeMessageListener,

		// test
		processMessage : processMessage,
		
		sendInternalMessage: _sendInternalMessage
	}

})();

// define constant.
Object.defineProperty(KTW.managers.MessageManager, "METHOD", {
	value : {
		OBS : {
			/** ********* REQUEST ************** */
			APP_READY : "obs_appReady",
			REQUEST_SHOW : "obs_requestShow",
			// Unbound Application이 show여부 확인
			IS_LOADING_UNBOUND_APPPLICATION : "obs_isLoadingUnboundApplication",
			// 특정 언바운드 어플리케이션을 실행
			START_UNBOUND_APPLICATION : "obs_startUnboundApplication",
			// Full browser application을 실행
			START_BROWSER : "obs_startBrowser",

			START_UNICASTING_APP : "obs_startUnicastingApp",
	        STOP_UNICASTING_APP : "obs_stopUnicastingApp",
			
			/** ********* NOTIFY ************** */
			NOTIFY_HIDE : "obs_notifyHide",
			SHOW : "obs_show",
			HIDE : "obs_hide",
			// show 상태에서 hot key event 전달
			NOTIFY_KEY_EVENT : "obs_notifyKeyEvent",
			// 시청시간제한 시작
			SET_WATCHING_TIME_BLOCK : "obs_setWatchingTimeBlock",
			// 시청시간제한 해제
			RELEASE_WATCHING_TIME_BLOCK : "obs_releaseWatchingTimeBlock",
            /** 2017.06.26 dhlee YOUTUBE app 실행 요청 */
            START_YOUTUBE : "obs_startYoutube"
		},
		HP : {
			CHECK_VERSION : "hp_checkVersion",
			// 다른 app이 종료된 경우(appStore에서 나오는 경우 등)
			SHOW_CONTENT_DETAIL : "hp_showContentDetail", // 컨텐츠 상세화면 조회 요청
			SHOW_SERIES_DETAIL : "hp_showSeriesDetail", // 시리즈 상세화면 조회요청
			WATCH_CONTENT : "hp_watchContent", // 컨텐츠 시청/구매요청
			WATCH_CONTENT_FORCED : "hp_watchContentForced", // 강제로 본다.
			WATCH_BOOKMARK_CONTENT : "hp_watchBookmarkContent", // 강제로 본다.
			SHOW_CATEGORY : "hp_showCategory", // 홈메뉴 호출
			HIDE_OTHER_APP : "hp_hideOtherApp",
			IS_VOD_PLAYING : "hp_isVODPlaying", // 현재 VOD시청 여부 조회 요청
			STOP_VOD_PLAYING : "hp_stopVODPlaying", // VOD 시청 종료 요청
			PAUSE_VOD_PLAYING : "hp_pauseVODPlaying", // 재생중 VOD 일시정지 요청
			PLAY_VOD_PAUSED : "hp_playVODPaused", // 일시정지한 VOD재생 요청
			GET_CURRENT_VOD_INFO : "hp_getCurrentVODInfo", // 시청중인 VOD정보 조회 요청
			SEARCH_VOD : "hp_searchVOD", // 음성 검색 요청
			START_SEAMLESS_PAIRING : "startSeamlessPairing", // OTN페어링 시작
			STOP_SEAMLESS_PAIRING : "stopSeamlessPairing", // OTN 페어링 중지
			NOTIFY_STOP_AV : "hp_notifyStopAV", // iframe 제거 및 vod stop
			SHOW_OTHER_APP : "hp_showOtherApp", // 다른 앱이 show 가 됨.
			REQ_RESIZE_AV : "hp_reqResizeAV", // 채널 및 VOD 의 resize 를 요청.
			GET_DONG_CODE : "hp_getDongCode", // dongcode 전달 GET_PKG_CODE :
			GET_PKG_CODE : "hp_getPkgCode", // dongcode 전달 GET_PKG_CODE :
			GET_MINI_EPG_STATUS : "hp_getMiniEpgStatus", //미니가이드 상태 요청.
 			REQUEST_CHANNEL_UPDATE: "hp_reqChannelUpdate", //채널 update.
 			SEARCH_RESULT: "hp_searchResult", // 검색 요청
 			IS_PIP_ON : "hp_isPipOn", // PIP 사용유무 리턴 (true / false)
 			STOP_VOD_WITH_CHANNEL_SELECTION: "hp_stopVodWithChannelSelection",
 			
 			// 홈포탈이 보내는 Message
 			SHOW_CHILD_APP: "hp_showChildApp",
 			
 			// OTP 자동전원 message
 			GET_AUTO_POWER : "hp_getAutoPower", // 자동전원 설정 값 획득 요청
 			SET_AUTO_POWER : "hp_setAutoPower", // 자동전원 설정
			GET_CURRENT_VOD_INFO_FOR_BOOKMARK : "hp_getCurrentVODInfoForBookmark", // 시청중인 VOD정보 조회 요청(only bookmark)

            SHOW_STITCHING: "hp_showStitching", // dhlee 2016.11.17 R7 항목 추가 실시간 인기 채널 tune

            SEEK_VODPLAYING: "hp_seekVODPlaying",   // dhlee 2016.11.17 R7 항목 추가 VOD 앞 뒤로 seek
            SPEED_VODPLAYING: "hp_speedVODPlaying",  // dhlee 2016.11.17 R7 항목 추가 VOD 배속 변경
            GET_AUTO_STANDBY: "hp_getAutoStandby",   // dhlee 2017.03.16 Web 3.0 에서는 자동 대기 모드 설정 값을 네비에서 관리한다. Local Storage에 저장하고 있던 설정 값 migration을 위해 추가

            SHOW_CONTENT_DETAIL_PURCHASE: "hp_showContentDetailPurchase",   // [dj.son] 2017-06-21 컨텐츠 상세화면 버튼으로 포커스 가는 API 추가
            SHOW_SERIES_DETAIL_PURCHASE: "hp_showSeriesDetailPurchase",   // [dj.son] 2017-06-21 시리즈 상세화면 버튼으로 포커스 가는 API 추가

            SHOW_HOMESHOT: "hp_showHomeShot",   // [dj.son] 2018-02-07 홈샷 노출 요청 API 추가
            DELETE_HOMESHOT: "hp_deleteHomeShot",   // [dj.son] 2018-02-07 홈샷 삭제 요청 API 추가

            PLAY_PURCHASED_VOD: "hp_playPurchasedVOD",   // [dj.son] 2018-02-07 biz 결제 및 vod play API 추가

            SET_KIDS_MODE_ON_OFF: "hp_setKidsModeOnOff",   // [dj.son] 2018-02-27 키즈 모드 on / off 할 수 있는 API 추가

            BY_PASS: "sp_bypass" // [kh.kim] byPass 타입 추가
		},

		OAM : {
			VOD_START : "OAM_VOD_START",
			VOD_STOP : "OAM_VOD_END"
		},

		SMART_PUSH : {
			VOD_START : "spc_notifyVODStarted",
			VOD_STOP : "spc_notifyVODEnded"
		},

		EXT : {
			RUN_APP_STORE : "RunSASApp",
			AS_WIZGAME: "as_wizGameState"
		},
		
		CUSTOM : {
			NOTIFY_PLAY_AV : "cus_notifyPlayAV"
		},

        /**
         * 2016.11.17 dhlee 매쉬업 어플리케이션 연동 항목 추가
         */
        MASHUP: {
            IS_SUPPORT_SERIES: "mashup_isSeries",   // 시리즈 예약 지원 여부 확인
            REQ_SERIES_INFO: "mashup_reqSeriesInfo",    // 시리즈/지난 VOD 조회
            SET_SERIES: "mashup_setSeries",             // 시리즈 예약 설정
            GET_SERIES_RESERVATION_LIST: "mashup_getSeriesReservationList", // 시리즈 예약 목록 조회
            DEL_SERIES: "mashup_delSeries",  // 시리즈 예약 항목 삭제
            REQ_ASSOCIATE_INFO: "mashup_reqAssociateInfo",  // 연관 정보 요청
            REQ_ASSOCIATE_DATA: "mashup_reqAssociateData",  // 연관 정보 메타 데이터 요청
            REQ_ASSOCIATE_INFO_DATA: "mashup_reqAssociateInfoData",  // 연관 정보 + 메타 데이터 함께 요청
			REQ_START_APPLICATION : "mashup_startApplication" // mashup 차일 앱 실행
        }
	},
	writable : false,
	configurable : false
});
