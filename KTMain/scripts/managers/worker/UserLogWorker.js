/**
 * wait for the start 'user log' message
 * 
 * @param e - the event and e.data contains the JSON object or variable array
 */
self.onmessage = function(e) {
    if (e.data[0] === "COLLECT") {
        // log 수집하는 경우
        convertLogData(e.data);
    }
    else if (e.data[0] === "SEND") {
        // log 전송하는 경우
        sendLog(e.data);
    }
};

function convertLogData (data) {
    
    var type = data[2];
    var act = data[3];
    var item = data[5];
    var layer_id = data[6];
    
    var act_code = undefined;
    
    act_code = filterAndConvertActCode(type, act);

    // HOT_KEY 로그 수집 대상인 경우에만 log 수집
    if (type === "HOT_KEY" && act_code === undefined) {
        //throw new Error("not allow hot keycode... so don't collect log");
        printLog("error", "not allow hot keycode... so don't collect log");
        return;
    }
    
    if (type === "JUMP_TO") {
        if (act === "JUMP_POST_MSG" || act_code === "JUMP_POST_MSG") {
            // 만약 채널가이드, 마이메뉴, 설정 카테고리로 jump 하는 경우라면
            // jumpType 값을 "ETC_JUMP" 로 변경
            var etc_item = { "id" : item["contsId"] };
            if (filterEtcMenu(etc_item)) {
                item["jumpType"] = "ETC_JUMP";
            }
        }
        else {
            // postMessage 형태의 jump로그가 아니면 reqCd를 공백처리
            // 보통 해당 값을 전달하지 않는 경우가 있어서
            // 이 경우에 대해서도 자연스럽게 추가될 수 있도록 함
            item["reqCd"] = "";
        }
        
        //if (act_code === undefined) {
        //    // 홈메뉴, 마이메뉴, 양방향 등에서 양방향 서비스 선택 시
        //    // jump 로그 수집하지 않음
        //    // 보통 act만 정의되면 수집 가능해야 하지만
        //    // JUMP 진입 경로에 대한 정보가 없기 때문에 제외함
        //    if (layer_id && DEF.except_layer_ids[type].indexOf(layer_id) !== -1) {
        //        printLog("error", "don't collect jump log from " + layer_id);
        //        return;
        //    }
        //}
    }

    /**
     * [dj.son]
     * ETC_MENU 의 경우 홈메뉴에서 진입하는 경우밖에 없고,
     * 또한 우리집 맞춤 TV 설정 같은 경우 편성으로 올 수 있으므로 ID 검사에 안걸릴 가능성이 있음
     * 따라서 ETC_MENU 의 경우에는 ID check 를 하지 않도록 수정
     */
    //if (type === "ETC_MENU" && !filterEtcMenu(item)) {
    //    printLog("error", "invalid etc menu id... so don't collect log");
    //    return;
    //}

    /**
     * ETC_MENU 의 경우, 명확하게 해당 Layer (채널 가이드, 우리집 맞춤 TV 등) 내에서만 navigation 될때 로그를 남기므로
     * 아래 코드 주석 처리
     */
    // HOME_MENU 로그 중 채널가이드 및 하위 메뉴의 경우
    // ETC_MENU로 converting 처리
    // 단, 진입한 로그만 남겨야 하므로 OK 및 RIGHT 키에 의해
    // 진입한 경우에만 로그를 수집하고 이외의 경우에는 예외처리함
    //if (type === "HOME_MENU") {
    //    var etc_item = { "id" : item["contsId"], "cd" : "full_epg" };
    //    if (filterEtcMenu(etc_item)) {
    //        if (act_code === "OK" || act_code === "RIGHT") {
    //            type = "ETC_MENU";
    //            item["id"] = item["contsId"];
    //            item["name"] = item["contsName"];
    //
    //            delete item["catId"];
    //            delete item["catName"];
    //            delete item["contsId"];
    //            delete item["contsName"];
    //        }
    //        else {
    //            printLog("error", "don't collect navigation log(" + act_code + ") for channel guide menu");
    //            return;
    //        }
    //    }
    //}
    
    if (act_code !== undefined) {
        act = act_code;
    }
    
    var message = {
        "index" : data[1],
        "type" : type,
        "act" : act,
        "timestamp" : data[4],
        "item" : item
    };
    sendPostMessage("res", message, "create");
}

/**
 * ETC_MENU 타입의 MENU ID 유효 체크
 * @param item 통계 로그 추가 data
 * @returns {boolean} MENU ID 유효 여부
 */
function filterEtcMenu (item) {
    if(!item) {
        return false;
    }
    var is_valid_menu = false;
    var menu_id = item.id;//메뉴 id
    var menu_cd = item.cd;//진입 경로
    var index = -1;

    //메뉴 ID 초기화
    var etc_menu_ids = DEF.menu_ids.etc_menu_ids;
    var ch_menu_ids = DEF.menu_ids.ch_menu_ids;
    var my_menu_ids = DEF.menu_ids.my_menu_ids;
    var setting_menu_ids = DEF.menu_ids.setting_menu_ids;
    if(!etc_menu_ids || etc_menu_ids.length <= 0) {
        etc_menu_ids = [];
        etc_menu_ids = etc_menu_ids.concat(ch_menu_ids);
        etc_menu_ids = etc_menu_ids.concat(my_menu_ids);
        etc_menu_ids = etc_menu_ids.concat(setting_menu_ids);
    }

    //메뉴 ID 유효 체크
    if (menu_cd) {
        if (ch_menu_ids && menu_cd === "full_epg") {//채널 가이드
            index = ch_menu_ids.indexOf(menu_id);
        } else if (my_menu_ids && menu_cd === "my_menu") {//마이 메뉴
            index = my_menu_ids.indexOf(menu_id);
        } else if (setting_menu_ids && menu_cd === "setting") {//사용자 설정
            index = setting_menu_ids.indexOf(menu_id);
        } else if (etc_menu_ids) {
            index = etc_menu_ids.indexOf(menu_id);
        }
    } else if (etc_menu_ids) {
        index = etc_menu_ids.indexOf(menu_id);
    }
    if(index !== -1) {
        is_valid_menu = true;
    }
    return is_valid_menu;
}

function filterAndConvertActCode (type, act) {
    if (type === "HOT_KEY") { // 핫키 로그
        if (DEF.key_codes.hot_keys[act]) {
            return "HOTKEY_" + DEF.key_codes.hot_keys[act];
        }
    }
    else if (type === "JUMP_TO") { // jump 로그
        if (act) {
            // step1 : reqPathCd 값으로 확인
            //if (DEF.req_path_cds[act]) {
            //    return "JUMP_" + DEF.req_path_cds[act];
            //}
            //// step2 : last layer를 기준으로 확인
            //// 이때 vod 상세화면 layer id의 경우 "vod_detail_x" 형태이기 때문에
            //// 이를 다시 vod_detail로 변경하여 처리함
            //if (act.indexOf("vod_detail") !== -1) {
            //    act = "vod_detail";
            //}
            //if (DEF.layer_ids[act]) {
            //    return "JUMP_" + DEF.layer_ids[act];
            //}
        }
    }
    else {
        /*if (type === "HOME_MENU") {
            // 추가로 pageUP/pageDown에 해당하는 red/blue 키를 추가
            // 해당 키는 "UP"/"DOWN" 값으로 변환하여 처리함
            DEF.key_codes.key[403] = "UP";
            DEF.key_codes[406] = "DOWN";
        }*/
        return DEF.key_codes.nav_keys[act];
    }
}

function convertKeyCode (act) {
    return DEF.key_codes[act];
}

function convertReqPathCd (reqPath) {
    return DEF.req_path_cds[reqPath] ? DEF.req_path_cds[reqPath] : "";
}

/**
 * 기본적으로 type 별 꼭 필요한 data null 체크, keyCode 값에 대한 컨버팅을 수행
 */
function checkLogData (log) {
    if (log) {
        switch (log.LOG_TYPE) {
            case DEF.TYPE.HOT_KEY:
                var keyCode = convertKeyCode(log.START_ACT);
                if (keyCode) {
                    log.START_ACT = keyCode;
                }
                else {
                    log = null;
                }
                break;
            case DEF.TYPE.HOME_MENU:
                //2017 06.30 sw.nam 모듈별 카테고리 navigation 로그 수집을 위한 type 추가
            case DEF.TYPE.SH_MENU:
            case DEF.TYPE.FH_MENU:
            case DEF.TYPE.KIDS_MENU:
                // 우리집 맞춤 TV 설정 같은 경우 편성으로 올 수 있으므로 ID 검사에 안걸릴 가능성이 있음.
                // 따라서 수집된 로그 데이터는 상황별로 정확한 type 을 보낸다고 가정하고 여기서는 필수 data null 체크만 수행

                var keyCode = convertKeyCode(log.START_ACT);
                if(keyCode) {
                    log.START_ACT = keyCode;
                }
/*                var keyCode = convertKeyCode(log.START_ACT);
                if (keyCode && (log.CATEGORY_ID !== null && log.CATEGORY_ID !== undefined) && (log.CATEGORY_NAME !== null && log.CATEGORY_NAME !== undefined)) {
                    log.START_ACT = keyCode;
                }
                else {
                    log = null;
                }*/
                break;
            case DEF.TYPE.JUMP_TO:
                // START_ACT
                if (log.START_ACT) {
                    if (log.START_ACT === DEF.JUMP.CODE.POST_MSG) {
                        log.REQ_PATH_CD = convertReqPathCd(log.REQ_PATH_CD);
                    }

                    if (log.JUMP_TYPE) {
                        if (((log.JUMP_TYPE === DEF.JUMP.TYPE.CATEGORY || log.JUMP_TYPE === DEF.JUMP.TYPE.ETC) && !log.CATEGORY_ID)
                            || (log.JUMP_TYPE === DEF.JUMP.TYPE.VOD_DETAIL && !log.CONTENT_ID)
                            || (log.JUMP_TYPE === DEF.JUMP.TYPE.DATA && !log.LOCATOR)) {
                            log = null;
                        }
                    }

                }
                else {
                    log = null;
                }
                break;
            case DEF.TYPE.VOD_DETAIL:
                var keyCode = convertKeyCode(log.START_ACT);
                if(keyCode) {
                    log.START_ACT = keyCode;
                }
                break;
            case DEF.TYPE.ETC_MENU:
                // 우리집 맞춤 TV 설정 같은 경우 편성으로 올 수 있으므로 ID 검사에 안걸릴 가능성이 있음.
                // 따라서 수집된 로그 데이터는 상황별로 정확한 type 을 보낸다고 가정하고 여기서는 필수 data null 체크만 수행
                var keyCode = convertKeyCode(log.START_ACT);
                if (keyCode && log.MENU_ID && log.MENU_NAME) {
                    log.START_ACT = keyCode;
                }
                else {
                    log = null;
                }
                break;
            case DEF.TYPE.MENU_UPDATE:
                // 버전 데이터 유효성 체크만 수행
                if (!log.MENU_VERSION) {
                    log = null;
                }
                break;
        }
    }

    return log;
}

function reviseLogArray (arrLog) {
    if (!arrLog) {
        return;
    }

    var length = arrLog.length;
    var arrReviseLog = [];
    for (var i = 0; i < length; i++) {
        var log = arrLog[i];

        log = checkLogData(log);

        if (log) {
            arrReviseLog.push(log);
        }
    }

    return arrLog;
}


function sendLog (data) {
    var meta_data = data[1];
    var log_data = reviseLogArray(data[2]);
    var post_data = {
        "VERSION" : meta_data.version,
        "COUNT" : log_data.length,
        "SAID" : meta_data.said,
        "STATS_ARRAY" : log_data
    };
    var url = meta_data.url;
    printLog("debug", "sendLog() url = " + url);

    if (log_data && log_data.length > 0) {
        if(meta_data.testFlag) {
            printLog("debug", "_SAVE_ON_LOCAL_STORAGE "+JSON.stringify(post_data));
        }else {
            requestAjax(url, post_data, 0, onComplete);
        }
    }
    else {
        sendPostMessage("res", undefined, "send");
    }
}

//simple XHR request in pure JavaScript
function requestAjax (url, data, retry_count, callback) {
    var xhr;
    
    var params = [url, data, retry_count];
    
    try {
        printLog("debug", "post_data = " + JSON.stringify(data));
        
        xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.timeout = 5 * 1000;

        // Cross Domain 문제 해결
        // by kingsae1
        xhr.withCredentials = "true";
        xhr.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    // 성공한 경우
                    callback(true, xhr.responseText);
                }
                else {
                    // timeout 및 기타 error 상황이 발생하는 경우(retry 해야함)
                    callback(false, "Server Error(" + xhr.status + ")", params);
                }
            }
        };
        /*xhr.ontimeout = function (e) {
            // XMLHttpRequest timed out. Do something here.
            callback(false, "Timeout", params);
        };*/
        // 로그 전송 시 json object를 문자열 처리함
        xhr.send(JSON.stringify(data));
    } catch(e) {
        var msg = "Error occured in XMLHttpRequest ==> " +
            "ReadyState: " + xhr.readyState + ", Status: " + xhr.status + ", " + e;
        printLog("error", msg);
    }
}

function onComplete (success, result, params) {
    printLog("debug", "onComplete() success = " + success + ", result = " + result);
    
    if (success === true) {
        sendPostMessage("res", undefined, "send");
    }
    else {
        var retry_count = params[2] + 1;
        
        if (retry_count >= 3) {
            // 재시도 2회 이후에도 실패한 경우 전송 종료
            sendPostMessage("res", undefined, "send");
        }
        else {
            var url = params[0];
            var post_data = params[1];
            
            // 5초 뒤 재전송 시도
            setTimeout(function() {
                requestAjax(url, post_data, retry_count, onComplete);
            }, 5000);
        }
    }
}

function printLog (type, message) {
    sendPostMessage(type, message);
}

function sendPostMessage (type, message, method) {
    var msg = {
        type : type,
        message : message,
        method : method
    };
    self.postMessage(msg);
}

var DEF = {
    TYPE: {
        HOT_KEY : "HOT_KEY",
        HOME_MENU : "HOME_MENU",
        VOD_DETAIL : "VOD_DETAIL",
        JUMP_TO : "JUMP_TO",
        ETC_MENU : "ETC_MENU",
        MENU_UPDATE : "MENU_UPDATE",
        //2017.06.16 sw.nam - 모듈별 navigation type 구분값 추가 (규격서 기반)
        SH_MENU : "SH_MENU", // subHome
        FH_MENU : "FH_MENU", // 우리집 맞춤 TV
        KIDS_MENU: "KIDS_MENU" // KIDS 모듈
    },
    JUMP: {
        TYPE : {
            /* 카테고리 */
            CATEGORY : "CATEGORY_JUMP",
            /* VOD 상세화면 */
            VOD_DETAIL : "DETAIL_JUMP",
            /* 양방향 */
            DATA : "INTERACTIVE_JUMP",
            /* 검색 */
            SEARCH : "SEARCH_JUMP",
            /* 마이메뉴, 설정, 채널가이드 */
            ETC : "ETC_JUMP",
            /* 2채널 동시시청, VOD-채널 동시시청 */
            PIP : "PIP_JUMP",
            /* 편성표 */
            EPG : "EPG_JUMP"
        },
        CODE : {
            /* 홈샷 */
            HOMESHOT : "JUMP_CATE_HOMESHOT",
            /* promo trigger */
            PROMO_CH : "JUMP_PROMO_CHANNEL",
            /* 공지사항 */
            NOTICE : "JUMP_NOTICE",
            /* 검색 */
            SEARCH : "JUMP_SEARCH",
            /* postMessage에 의해 카테고리 및 상세화면 등으로 이동하는 경우
             * 1. hp_showCategory
             * 2. hp_showContentDetail
             * 3. hp_showSeriesDetail
             */
            POST_MSG : "JUMP_POST_MSG",
            /*
             * 연관메뉴
             * - 미니가이드 연관콘텐츠
             * - 북마크 보관함
             * - 홈위젯
             */
            CONTEXT : "JUMP_CONTEXT",
            /*
             * 연관콘텐츠
             */
            RELATED : "JUMP_RELATED_CONTENTS",
            /**
             * 홈메뉴 좌측 추천 컨텐츠
             */
            MAIN_RECOMMEND : "JUMP_MAIN_RECOMMEND",
            /**
             * subhome 화면 추천 컨텐츠
             */
            SUBHOME_RECOMMEND: "JUMP_SUBHOME_RECOMMEND",
            SUBHOME: "JUMP_SUBHOME"
        }
    },
    /** 진입 경로 (key : OTW.vod.EntrancePath / value : 규격서 Appendix D) */
    req_path_cds: {
        /* 홈샷 */
        "25": "JUMP_CATE_HOMESHOT",
        /* 검색 */
        "04": "JUMP_SEARCH", // 검색결과
        "48": "JUMP_SEARCH", // 검색창 하단 theme list
        /* 연관콘텐츠 */
        "20" : "JUMP_RELATED_CONTENTS", // channel 연관 콘텐츠
        "19" : "JUMP_RELATED_CONTENTS", // VOD 함께 많이본 영상
        "22" : "JUMP_RELATED_CONTENTS", // VOD 종료시 함께 많이 본 영상
        "33" : "JUMP_RELATED_CONTENTS", // 쇼윈도의 추천더보기 및 마이메뉴의 당신을 위한 VOD 추천
        /* 프로모 트리거 */
        "P0" : "JUMP_PROMO_CHANNEL",
        "P1" : "JUMP_PROMO_CHANNEL",
        "P2" : "JUMP_PROMO_CHANNEL",
        "P3" : "JUMP_PROMO_CHANNEL",
        "P4" : "JUMP_PROMO_CHANNEL",
        "12" : "JUMP_PROMO_CHANNEL",
        /* 북마크, 홈위젯 등의 postMessage */
        "26" : "JUMP_POST_MSG", // 홈위젯
        "43" : "JUMP_POST_MSG", // 북마크
        "54" : "JUMP_POST_MSG", // G-box 음성비서
        "55" : "JUMP_POST_MSG", // 음성인식 검색
        "PMD" : "JUMP_POST_MSG" // hp_showContentDetail 및 hp_showSeriesDetail 메세지상 req_cd 값이 없는 경우
    },
    /** 리모컨 키 코드 (key : OTW.KEY_CODE / value : 규격서 Appendix B) */
    key_codes: {
        36: "HOTKEY_HOME",//홈메뉴 핫키
        438: "HOTKEY_MYMENU",//마이메뉴 핫키
        435: "HOTKEY_SEARCH",//검색 핫키
        //114: "FAVCH",//선호채널 핫키
        //118: "SKYCHOICE",//스카이초이스 핫키
        458: "HOTKEY_EPG",//편성표 핫키
        613: "HOTKEY_EPG",//편성표 핫키
        //433: "MOVIE",//영화 핫키
        //434: "TV"//TV다시보기 핫키

        39: "RIGHT",//오른쪽 방향 키
        37: "LEFT",//왼쪽 방향 키
        38: "UP",//위 방향 키
        40: "DOWN",//아래 방향 키
        13: "OK",//확인 키
        461: "BACK", //이전 키
        403 : "UP", // RED
        406 : "DOWN" // BLUE
        //412 : "UP", // Rewind
        //417 : "DOWN" // FastForward
    },
    /** 메뉴 ID (value : OTW.managers.data.MenuDataManager.MENU_ID) */
    menu_ids: {
        /** 전체 기타 메뉴 ID 코드. */
        etc_menu_ids : [],
        /** 채널 가이드 메뉴 ID 코드. */
        ch_menu_ids: [
            "MENU00000" ,               // 채널 가이드
            "MENU00013",          // 오늘의 채널 가이드, ChSub catType 이 없을 경우, 생성되는 VMenu ID
            "MENU00001",          // 전체 채널
            "MENU00012",                  // OTV - UHD 채널
            "MENU00002",          // OTS - UHD 채널
            "MENU00003",                 // OTS - 무비 초이스
            "MENU00006",         // 선호 채널
            "MENU00004",                // 오디오 채널
            "MENU00005",            // 우리만의 채널
            "MENU00010",               // TV 앱 / 쇼핑 채널
            "MENU00009" ,            // OTS - olleh tv
            "MENU00007",   // 방송 예약 목록
            "T000000000003",           // 4채널 동시 시청
            "T000000000004",        // 실시간 인기 채널

            "MENU00008",               // OTS - 프로그램 검색
            "MENU09001",                // OTS 프로그램 - 영화/드라마
            "MENU09002",                 // OTS 프로그램 - 뉴스/시사
            "MENU09003",        // OTS 프로그램 - 쇼/오락
            "MENU09004",               // OTS 프로그램 - 스포츠
            "MENU09005",                 // OTS 프로그램 - 어린이/청소년
            "MENU09006",                // OTS 프로그램 - 음악/무용
            "MENU09007",                  // OTS 프로그램 - 예술/문화
            "MENU09008",              // OTS 프로그램 - 사회/정치/경제
            "MENU09009",                  // OTS 프로그램 - 교육/과학/정보
            "MENU09010",              // OTS 프로그램 - 여가/취미
            "MENU09011",                  // OTS 프로그램 - 기타

            "MENU00011",                // OTV - 장르별 채널
            "MENU09101",            // OTV 장르 - 지상파/종편/홈쇼핑
            "MENU09102",          // OTV 장르 - 드라마/오락/음악
            "MENU09103",                  // OTV 장르 - 영화/시리즈
            "MENU09104",                 // OTV 장르 - 스포츠/레져
            "MENU09105",                   // OTV 장르 - 애니/유아/교육
            "MENU09106",            // OTV 장르 - 다큐/교양
            "MENU09107",                   // OTV 장르 - 뉴스/경제
            "MENU09108",                 // OTV 장르 - 공공/공익/정보
            "MENU09109"                // OTV 장르 - 종교/오픈,
        ],
        /** 우리집 맞춤 TV ID 코드. */
        my_menu_ids: [
            "MENU00100", // Config catType 이 없을 경우, 생성되는 VMenu ID

            "T000000000006",    // 우리집 맞춤 TV - 마이 메뉴 1
            "MENU00601", // 우리집 맞춤 TV - 최근 시청 목록
            "MENU01200", // 우리집 맞춤 TV - 찜한 목록
            "T000000000013", // 우리집 맞춤 TV - 즐겨 찾는 메뉴
            "T000000000014",    // 우리집 맞춤 TV - 마이 메뉴 2
            "MENU07003", // 우리집 맞춤 TV - 당신을 위한 VOD 추천

            // 마이 메뉴 1 하위 메뉴들
            "T000000000007",   // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스
            "T000000000008",   // 우리집 맞춤 TV - 마이 메뉴 1 - TV 쿠폰
            "T000000000009",   // 우리집 맞춤 TV - 마이 메뉴 1 - KT 멤버십
            "MENU00605", // 우리집 맞춤 TV - 마이 메뉴 1 - TV 포인트
            "T000000000010", // 우리집 맞춤 TV - 마이 메뉴 1 - 컨텐츠 이용권

            // 알림 박스 하위 메뉴들
            "MENU00608", // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스 - 공지사항
            "T000000000011", // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스 - 알림
            "T000000000012", // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스 - 이벤트

            // 마이 메뉴 2 하위 메뉴들
            "MENU00610", // 우리집 맞춤 TV - 마이 메뉴 2 - 마이 소장용VOD (2017.02.08 메뉴명 변경)
            "MENU00603", // 우리집 맞춤 TV - 마이 메뉴 2 - 마이 플레이리스트
            "MENU00602", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매 내역 보기
            "MENU01400", // 우리집 맞춤 TV - 마이 메뉴 2 - 가입 정보

            // 구매 내역 보기 하위 메뉴들
            "MENU00611", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매내역 보기 - VOD 구매목록
            "MENU00609", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매내역 보기 - 모바일 공유목록
            "MENU00612", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매내역 보기 - 무비초이스 구매목록

            // 당신을 위한 VOD 추천 하위 메뉴들
            "MENU07004", // 우리집 맞춤 TV - 당신을 위한 VOD 추천 - 나의 취향 알아보기
            "MENU07005" // 우리집 맞춤 TV - 당신을 위한 VOD 추천 -  내가 평가한 콘텐츠
        ],
        /** 사용자 설정 메뉴 ID 코드.*/
        setting_menu_ids: [
            "MENU07002", // Config catType 이 없을 경우, 생성되는 VMenu ID

            "MENU00800", // 자녀안심 설정
            "MENU00801", // 비밀번호 변경/초기화
            "MENU00802", // 시청연령 제한
            "MENU00804", // 시청시간 제한
            "MENU00902", // 시청채널 제한

            "MENU00900", // 채널 설정
            "MENU00901", // 선호 채널
            "MENU00904", // OTV - 채널 숨김 : OTS - 건너뛰기 채널
            "MENU00903", // 채널정보 표시시간
            "MENU00906", // 화면 해설 방송
            "MENU00907", // 자막 방송
            "MENU00908",  // 시리즈 몰아보기

            "MENU01000", // 시스템 설정
            "MENU01001", // 화면 비율
            "MENU01002", // 해상도
            "MENU01003", // 자동 전원
            "MENU01022", // 저전력 전원
            "MENU01004", // 자동 대기 모드
            "MENU01005", // 사운드
            "MENU01027", // 기가지니 only - 오디오 출력 설정 2017.02.28 추가
            "MENU01024", // UHD3 only - 스피커 볼륨 설정 => 음성가이드 볼륨 (메뉴명 변경)
            "MENU01025", // UHD3 only  - 원거리 음성 인식 => 음성가이드 (메뉴명 변경)
            "MENU00606", // 이동식 디스크 연결
            "MENU01023", // 블루투스 장치 연결
            "MENU01028", // VOD 보기 옵션 (2017.03.29 명칭 변경)
            "MENU01006", // 알림 메시지
            "MENU01015", // HDMI 전원 동기화
            "MENU01018", // 기본 언어
            "MENU01007", // 전체 초기화
            "MENU01012", // 시작 채널
            "MENU01016", // OTS - 중계기
            "MENU01017", // OTS - 수신품질 측정
            "MENU01008" // 시스템 정보
        ]
    }
};
