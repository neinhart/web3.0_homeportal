/**
 * Created by user on 2016-06-10.
 */

"use strict";

/**
 * module data 형식 - {
 *                      // 배포서버로부터 받은 정보
 *                      moduleId: "",
 *                      moduleName: "",
 *                      version: "",    // 서버 버전
 *                      serverPath: "",
 *                      downloadUrl: "",
 *                      serverLoading: true / false
 *
 *                      // App 에서 추가하는 정보
 *                      localVersion: "",   // 로컬 버전
 *                      localPath: "",
 *                      state: "",
 *                    }
 *
 *
 * [dj.son] 부팅시 모듈 버전 비교
 * 1. framework 버전과 localStorage 에 저장된 framework 버전 비교
 * 1.1. 다르면 송출이 새로 되었다는 의미이므로 localStorage clear & defaultModuleData 사용
 * 1.2. 같으면 localStorage 데이터 사용
 *
 * 이후로는 기존과 같은 시나리오
 *
 * 2. oc 로 오는 모듈 버전 데이터 비교 (각각의 모듈 버전 비교, 다름으로 비교)
 * 2.1. daVersion 이 같으면 아무것도 안함
 * 2.2. 모듈 버전이 하나라도 다르면 set server request flag true
 * 2.3. 모듈 버전이 전부 같으면 아무것도 안함...
 *
 * 3. 첫키 시나리오시 flag 보고 모듈 버전 API 호출 & 모듈 데이터 비교 (각가의 모듈 버전 비교, 다름으로 비교)
 * 3.1. response list 가 없거나 [] 이면 기존 데이터 사용
 * 3.2. 각각의 모듈 버전 비교 후 다르면 response 로 온 데이터 사용, 기본적으로 response list 가 기준 데이터가 됨 (자세한 내용은 updateModuleData() 함수 내부에 주석 참조)
 */
KTW.managers.module.ModuleDataManager = (function() {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var Def = KTW.managers.module.Def;
    var storageManager = null;
    var ocManager = null;

    var model = null;
    var module_data = [];
    var daVersion = null;

    var check_module_data_from_server = false;
    var check_download_module = false;

    function _init() {
        log.printDbg("_init() ");

        model = KTW.oipf.adapters.HWAdapter.getModelName();

        storageManager = KTW.managers.StorageManager;
        ocManager = KTW.managers.data.OCManager;

        // 로컬 스토리지에서 모듈 데이터 로드
        var localData = getModuleDataOnStorage();
        var localList = localData.list;
        // ModuleSateTable.json 로드
        var defaultData = getDefaultModuleData();

        if (localList) {
            /**
             * [dj.son] framework version 비교
             * - 다르면 localStorage clear 한 후 defaultData 사용
             * - 같으면 localStorage 데이터 사용
             */
            if (KTW.FRAMEWORK_VERSION !== localData.frameworkVersion) {
                clearModuleDataOnStorage();
                module_data = defaultData.module;
            }
            else {
                module_data = localList;
                daVersion = localData.daVersion;
            }
        }
        else {
            module_data = defaultData.module;
        }

        // 첫 부팅시 oc 업데이트 listener 안불림;;;
        // 첫 부팅시 무조건 oc 데이터와 비교
        _checkNeedModuleDataUpdate();

        _checkNeedModuleDownload();

        // OC listener 등록
        ocManager.addOCUpdateListener(onOCUpdate);
    }

    function _checkNeedModuleDownload () {
        check_download_module = false;

        if (module_data && module_data.length > 0) {
            for (var i = 0; i < module_data.length; i++) {
                if (module_data[i].state == Def.STATE.NONE
                    || module_data[i].state == Def.STATE.DOWNLOADED
                    || module_data[i].state == Def.STATE.DOWNLOADING
                    || module_data[i].state == Def.STATE.NEED_DOWNLOAD) {
                    check_download_module = true;
                    break;
                }
            }
        }
    }

    function _isNeedModuleDownload () {
        return check_download_module;
    }

    function getDefaultModuleData () {
        log.printDbg("getDefaultModuleData() ");

        var defaultData = {
            module: null
        };

        try {
            $.ajax({
                url: "module/ModuleStateTable.json?" + ((new Date()).getTime()),
                async: false,
                dataType: "json",
                timeout: KTW.DATA.AMOC.TIMEOUT
            }).done(function (data) {
                log.printDbg("default Module Data : " + log.stringify(data));

                if (data) {
                    defaultData = data;
                }
            }).fail(function () {
                log.printDbg("fail get default Module Data !!!!!");
            });
        }
        catch (e) {
            log.printExec(e);
            defaultData = {
                module: null
            };
        }

        return defaultData
    }

    function clearModuleDataOnStorage () {
        log.printDbg("setModuleDataOnStorage() ");

        storageManager.ps.remove(storageManager.KEY.MODULE_VERSION_DATA);
    }

    function getModuleDataOnStorage () {
        log.printDbg("getModuleDataOnStorage() ");

        var data = null;

        try {
            data = JSON.parse(storageManager.ps.load(storageManager.KEY.MODULE_VERSION_DATA));
        }
        catch(e) {
            log.printExec(e);
        }

        if (!data) {
            data = {};
        }

        return data;
    }

    function setModuleDataOnStorage () {
        log.printDbg("setModuleDataOnStorage() ");

        try {
            storageManager.ps.save(storageManager.KEY.MODULE_VERSION_DATA, JSON.stringify({
                frameworkVersion: KTW.FRAMEWORK_VERSION,
                daVersion: daVersion,
                list: module_data
            }));
        }
        catch(e) {
            log.printExec(e);
        }
    }

    function onOCUpdate (source_url) {
        log.printDbg("onOCUpdate() " + source_url);

        if(source_url === undefined || source_url == ocManager.DATA.MODULE_VERSION.URL) {
            _checkNeedModuleDataUpdate();
        }
    }

    function getOCData () {
        log.printDbg("getOCData() ");

        var oc_data = {
            daVersion: null,
            list: null
        };
        var data = null;
        var modelData = null;

        try {
            data = JSON.parse(ocManager.getModuleVersionData());
        }
        catch (e) {
            log.printErr("get module version data from ocManager -> fail.... : " + e.message);
        }

        if (data) {
            oc_data.daVersion = data.daVersion;

            if (data.list && data.list.length > 0) {
                for (var i = 0; i < data.list.length; i++) {
                    if (data.list[i].model === model) {
                        modelData = data.list[i];
                        break;
                    }
                }

                if (modelData && modelData.moduleList && modelData.moduleList.length > 0) {
                    oc_data.list = modelData.moduleList.concat();
                }
            }
        }

        return oc_data;
    }

    function _checkNeedModuleDataUpdate () {
        log.printDbg("_checkNeedModuleDataUpdate() ");

        var oc_data = getOCData();
        var oc_module_data = null;
        var oc_list = null;

        /**
         * 로컬 데이터 미존재 -> check_module_data_from_server = true
         * 로컬 데이터 존재 -> version 체크 후 check_module_data_from_server 설정
         */
        if (module_data && module_data.length > 0) {
            if (oc_data.daVersion && oc_data.daVersion !== daVersion) {
                oc_list = oc_data.list;

                if (oc_list) {
                    for (var i = 0; i < module_data.length; i++) {
                        oc_module_data = findModuleData({
                            data: oc_list,
                            moduleId: module_data[i].moduleId
                        });

                        if (oc_module_data) {
                            if (oc_module_data.version !== module_data[i].version) {
                                check_module_data_from_server = true;
                                break;
                            }

                            if (oc_module_data.requestVersion) {
                                check_module_data_from_server = true;
                                break;
                            }

                            module_data[i].serverLoading = oc_module_data.serverLoading;

                            oc_list.splice(oc_list.indexOf(oc_module_data), 1);
                        }
                        else {
                            check_module_data_from_server = true;
                            break;
                        }
                    }

                    if (oc_list.length > 0) {
                        check_module_data_from_server = true;
                    }
                }
                else {
                    check_module_data_from_server = true;
                }
            }
        }
        else {
            check_module_data_from_server = true;
        }

        daVersion = oc_data.daVersion;
    }

    function _checkModuleData (cbCheckModuleData) {
        log.printDbg("_checkModuleData() " + check_module_data_from_server);

        if (check_module_data_from_server) {
            getModuleDataOnServer(function (success) {
                if (success) {
                    check_module_data_from_server = false;
                }

                if (cbCheckModuleData) {
                    cbCheckModuleData(success);
                }
            });
        }
        else {
            if (cbCheckModuleData) {
                cbCheckModuleData(true);
            }
        }
    }

    function getModuleDataOnServer (cbCheckModuleData) {
        log.printDbg("getModuleDataOnServer() ");

        var url = "";
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT) {
            url = KTW.DATA.HTTP.MODULE_SERVER.BMT_HTTPS_URL + "/deploy-api/update/version";
        }
        else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            url = KTW.DATA.HTTP.MODULE_SERVER.LIVE_HTTPS_URL + "/deploy-api/update/version";
        }
        else {
            url = KTW.DATA.HTTP.MODULE_SERVER.TEST_URL + "/deploy-api/update/version";
        }

        KTW.managers.module.ModuleManager.checkOfficeCode(function (success) {
            if (success) {
                var officeCd = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OFFICE_CODE);
                var postData = {
                    saId: KTW.SAID,
                    officeCd: officeCd,
                    model: model,
                    frameworkVersion: KTW.FRAMEWORK_VERSION
                };

                $.ajax({
                    url: url,
                    type: "post",
                    dataType: 'json',
                    async: true,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(postData)
                }).done(function (data) {
                    log.printDbg("getModuleDataOnServer done : " + log.stringify(data));

                    var success = false;

                    if (data) {
                        success = updateModuleData(data);
                    }

                    if (cbCheckModuleData) {
                        cbCheckModuleData(success);
                    }
                }).fail(function () {
                    log.printDbg("getModuleDataOnServer fail");

                    if (cbCheckModuleData) {
                        cbCheckModuleData();
                    }
                });
            } else {
                log.printErr("getModuleDataOnServer(), officeCd is null");
            }
        });
        /*
        var officeCd = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OFFICE_CODE);
        var postData = {
            saId: KTW.SAID,
            officeCd: officeCd,
            model: model,
            frameworkVersion: KTW.FRAMEWORK_VERSION
        };

        $.ajax({
            url: url,
            type: "post",
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData)
        }).done(function (data) {
            log.printDbg("getModuleDataOnServer done : " + log.stringify(data));

            var success = false;

            if (data) {
                success = updateModuleData(data);
            }

            if (cbCheckModuleData) {
                cbCheckModuleData(success);
            }
        }).fail(function () {
            log.printDbg("getModuleDataOnServer fail");

            if (cbCheckModuleData) {
                cbCheckModuleData();
            }
        });
        */
    }

    function updateModuleData (data) {
        log.printDbg("updateModuleData(" + log.stringify(data) + ")");

        var success = false;

        if (data && util.isValidVariable(data, ["resultCd", "list"])) {
            if (data.resultCd === "0" && data.list.length > 0) {
                /**
                 * 기존 데이터가 null 일 경우, 서버 데이터로 업데이트
                 *
                 * 기존 데이터와 비교
                 * 기존 데이터 미존재 & 서버 데이터 존재 -> 모듈 추가
                 * 기존 데이터 존재 & 서버 데이터 미존재 -> 모듈 삭제
                 * 기존 데이터 존재 & 서버 데이터 존재 -> version 비교 후 업데이트
                 */

                var server_data = data.list;
                var server_module_data = null;

                // 기존 데이터 없을때
                if (!module_data || module_data.length < 1) {
                    module_data = server_data;

                    // state 를 NONE 으로 설정
                    for (var i = 0; i < module_data.length; i++) {
                        module_data[i].state = Def.STATE.NONE;
                        module_data[i].localVersion = null;
                        module_data[i].localPath = null;
                    }

                    check_download_module = true;
                }
                else {
                    // 모듈 하나 하나 정보 비교하면서 state 값 변경
                    for (var i = 0; i < module_data.length; i++) {
                        server_module_data = findModuleData({
                            data: server_data,
                            moduleId: module_data[i].moduleId
                        });

                        /**
                         * [dj.son] 모듈 추가 / 삭제에 대한 고려는 하지 않는다. 그 정도 상황이면 다시 송출될 것이라 가정
                         */

                        // 서버 response 데이터중 기존 모듈 데이터가 존재하면, 데이터 비교 후 업데이트
                        // 없으면 기존 모듈 데이터 삭제, 실제 로컬에 남아있는 파일들은 부팅시 혹은 모듈 다운로드시 삭제
                        if (server_module_data) {
                            if (module_data[i].version !== server_module_data.version) {
                                module_data[i] = server_module_data;

                                module_data[i].state = Def.STATE.NEED_DOWNLOAD;
                                module_data[i].localVersion = null;
                                module_data[i].localPath = null;

                                check_download_module = true;
                            }

                            module_data[i].serverLoading = server_module_data.serverLoading;

                            server_data.splice(server_data.indexOf(server_module_data), 1);
                        }
                    }
                }

                setModuleDataOnStorage();

                check_module_data_from_server = false;

                success = true;

                log.printDbg("module version data update success...");
            }
            else {
                log.printErr("resultCd is " + data.resultCd + " and list length is " + data.list.length);
            }
        }
        else {
            log.printDbg("module version data is null or not valid parameter... so return");
        }

        return success;
    }

    function findModuleData (options) {
        for (var i = 0; i < options.data.length; i++) {
            if (options.data[i].moduleId === options.moduleId) {
                return options.data[i];
            }
        }
    }

    function _completeModuleUpdate (module) {
        var targetModule = findModuleData({
            data: module_data,
            moduleId: module.moduleId
        });

        targetModule.localVersion = module.localVersion;
        targetModule.localPath = module.localPath;
        targetModule.state = Def.STATE.STORED;

        setModuleDataOnStorage();
    }

    function _getModuleData(moduleId) {
        log.printDbg("_getModuleData() " + moduleId);

        var moduleData = null;

        if (moduleId && moduleId.length > 0) {
            for (var i = 0; i < module_data.length; i++) {
                if (module_data[i].moduleId === moduleId) {
                    moduleData = module_data[i];
                    break;
                }
            }
        }
        else {
            moduleData = module_data;
        }

        return moduleData
    }

    function _getUpdateModuleList() {
        log.printDbg("_getUpdateModuleList() ");

        var arrModuleId = [];

        for (var i = 0; i < module_data.length; i++) {
            if (module_data[i].state == Def.STATE.NONE
                || module_data[i].state == Def.STATE.DOWNLOADED
                || module_data[i].state == Def.STATE.DOWNLOADING
                || module_data[i].state == Def.STATE.NEED_DOWNLOAD) {

                arrModuleId.push(module_data[i]);
            }
        }

        return arrModuleId;
    }

    return {
        init: _init,

        checkNeedModuleDataUpdate: _checkNeedModuleDataUpdate,
        checkModuleData: _checkModuleData,

        checkNeedModuleDownload: _checkNeedModuleDownload,
        isNeedModuleDownload: _isNeedModuleDownload,

        getModuleData: _getModuleData,
        getUpdateModuleList: _getUpdateModuleList,

        completeModuleUpdate: _completeModuleUpdate,

        testFunc: function () { return module_data; }
    };
})();

