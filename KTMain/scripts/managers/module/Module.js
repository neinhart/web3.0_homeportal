/**
 * Created by user on 2016-06-10.
 */

"use strict";

(function() {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    /**
     * load iframe by innerHTML
     *
     * @param options iframe - iframe tag
     *                 url - iframe url
     *                 cbLoad - iframe load callback
     *
     * @returns jQuery ajax xhr
     */
    function loadIFrame(options) {
        return $.ajax({
            type: "get",
            async: true,
            url: options.url,
            dataType: "text",
            cache: false,
            timeout: KTW.managers.module.Def.MODULE_LOADING_TIMEOUT,
            success: function(data, status, xhr) {
                options.iframe.load(options.cbLoad);

                options.iframe[0].contentWindow.document.open();
                options.iframe[0].contentWindow.document.write(data);
                options.iframe[0].contentWindow.document.close();
            },
            error: function() {
                options.cbLoad();
            }
        });
    }

    KTW.managers.module.Module = function(options) {

        this.moduleId = options.moduleData.moduleId;
        this.moduleName = options.moduleData.moduleName;
        this.state = options.moduleData.state;

        this.version = options.moduleData.version;
        this.localVersion = options.moduleData.localVersion;
        this.serverPath = options.moduleData.serverPath;
        this.localPath = options.moduleData.localPath;

        this.loadLocation = null;

        this.downloadUrl = options.moduleData.downloadUrl;
        this.serverLoading = options.moduleData.serverLoading;

        this.iframe = util.makeElement({
            tag: "<iframe />",
            attrs: {
                id: "iframe_" + this.moduleId,
                name: "iframe_" + this.moduleId,
                scrolling: "no",
                frameBorder: "0",
                allowtransparency: true,
                overflow: "hidden",
                css: {
                    position: "absolute",
                    visibility: "hidden"
                    //width: KTW.CONSTANT.RESOLUTION.WIDTH,
                    //height: KTW.CONSTANT.RESOLUTION.HEIGHT
                }
            },
            parent: options.parentDiv
        });
    };

    KTW.managers.module.Module.prototype.load = function(cbLoad) {
        log.printDbg("load()");

        var _this = this;

        var modulePath = "";

        if (this.serverLoading === true || this.serverLoading === "true") {
            modulePath = this.serverPath;
            this.loadLocation = KTW.managers.module.Def.LOAD_LOCATION.SERVER;
        }
        else {
            switch (this.state) {
                case KTW.managers.module.Def.STATE.NONE:
                case KTW.managers.module.Def.STATE.DOWNLOADED:
                case KTW.managers.module.Def.STATE.NEED_DOWNLOAD:
                case KTW.managers.module.Def.STATE.DOWNLOADING:
                    // 서버 로드
                    modulePath = this.serverPath;
                    this.loadLocation = KTW.managers.module.Def.LOAD_LOCATION.SERVER;
                    break;
                case KTW.managers.module.Def.STATE.DEPLOYED:
                    // 로컬 로드 (앱 내부)
                    var appPath = util.getAppPath();
                    this.localPath = appPath + "module/" + this.moduleId + "/";

                    modulePath = this.localPath;
                    this.loadLocation = KTW.managers.module.Def.LOAD_LOCATION.LOCAL;
                    break;
                case KTW.managers.module.Def.STATE.STORED:
                    // 로컬 로드 (다운로드 저장 위치)
                    modulePath = this.localPath;
                    this.loadLocation = KTW.managers.module.Def.LOAD_LOCATION.LOCAL;
                    break;
            }
        }

        log.printDbg("module path : " + modulePath + "module.html");

        this.ajaxRequest = loadIFrame({
            iframe: this.iframe,
            url: modulePath + "module.html",
            cbLoad: function() {
                log.printDbg(modulePath + "module.html cbLoad");

                var success;
                try {
                    success = _this.iframe[0].contentWindow.isLoadComplete();

                    _this.ajaxRequest = null;
                    if (success) {
                        _this.state = KTW.managers.module.Def.STATE.LOADED;
                    }
                }
                catch (e) {
                    log.printExec(e);
                    success = false;
                }

                /**
                 * 로컬 로딩을 시도했는데 실패하였을 경우, 서버 로딩 시도
                 */
                if (!success && (_this.state === KTW.managers.module.Def.STATE.STORED || _this.state === KTW.managers.module.Def.STATE.DEPLOYED)) {
                    if (_this.state === KTW.managers.module.Def.STATE.DEPLOYED) {
                        var serverUrl = "";
                        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
                            serverUrl = KTW.DATA.HTTP.MODULE_LOCATION_DOMAIN.LIVE_URL;
                        }
                        else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT) {
                            serverUrl = KTW.DATA.HTTP.MODULE_LOCATION_DOMAIN.BMT_URL;
                        }
                        else {
                            serverUrl = KTW.DATA.HTTP.MODULE_LOCATION_DOMAIN.TEST_URL;
                        }

                        var platform = "otv";
                        if (KTW.CONSTANT.IS_OTS) {
                            platform = "ots";
                        }

                        modulePath = serverUrl + "/web3/" + platform + "/module/html/" + _this.moduleId + "/" + KTW.FRAMEWORK_VERSION + "/" + _this.version + "/";
                        _this.serverPath = modulePath;
                    }
                    else {
                        modulePath = _this.serverPath;
                    }

                    _this.loadLocation = KTW.managers.module.Def.LOAD_LOCATION.SERVER;

                    log.printDbg("module load fail & retry server path : " + modulePath + "module.html");

                    _this.ajaxRequest = loadIFrame({
                        iframe: _this.iframe,
                        url: modulePath + "module.html",
                        cbLoad: function() {
                            log.printDbg(modulePath + "module.html cbLoad");

                            var success;
                            try {
                                success = _this.iframe[0].contentWindow.isLoadComplete();

                                _this.ajaxRequest = null;
                                if (success) {
                                    _this.state = KTW.managers.module.Def.STATE.LOADED;
                                }
                            }
                            catch (e) {
                                log.printExec(e);
                                success = false;
                            }

                            if (cbLoad) {
                                cbLoad(success);
                            }
                        }
                    });
                }
                else {
                    if (cbLoad) {
                        cbLoad(success);
                    }
                }
            }
        });
    };

    KTW.managers.module.Module.prototype.remove = function() {
        if (this.ajaxRequest) {
            this.ajaxRequest.abort();
            this.ajaxRequest = null;
        }

        try {
            this.iframe[0].contentWindow.onRemove();
        }
        catch (e) {
            log.printExec(e);
        }

        this.iframe.remove();
        this.iframe = null;
    };

    /**
     * Module 내부에서 정의한 Layer 를 가져오는 API
     */
    KTW.managers.module.Module.prototype.getLayer = function(layerId) {
        try {
            return this.iframe[0].contentWindow.getLayer(layerId);
        }
        catch (e) {
            log.printExec(e);
        }
    };

    /**
     * Menu 를 기반으로 Layer 를 activate 해야 할때, 사용하는 API
     * - 모듈에 menu 를 넘겨주면 모듈이 판단 후 적절한 Layer activate 수행
     * - 반드시 options parameter 내에 menu 가 존재해야 하며, 필요에 따라 다른 property 도 추가 가능
     *
     * @param options - menu : menu object (VItem)
     *
     */
    KTW.managers.module.Module.prototype.activateLayerByMenu = function(options) {
        try {
            this.iframe[0].contentWindow.activateLayerByMenu(options);
        }
        catch (e) {
            log.printExec(e);
        }
    };

    /**
     * 각 모듈이 제공하는 기능을 수행하기 위한 인터페이스
     * - method 를 기반으로 수행해야할 기능들을 구분한 후, 해당 기능을 수행
     * - 반드시 options parameter 내에 method 가 존재해야 하며, 모듈에서 정의한 method 에 따라 다른 property 도 추가
     *
     * @param options - method : 각 모듈이 정의한 기능
     *
     */
    KTW.managers.module.Module.prototype.execute = function(options) {
        try {
            return this.iframe[0].contentWindow.execute(options);
        }
        catch (e) {
            log.printExec(e);
        }
    };

    Object.defineProperty(KTW.managers.module.Module, "ID", {
        value: {
            MODULE_KIDS: "module.kids",
            MODULE_FAMILY_HOME: "module.family_home",
            MODULE_SUBHOME: "module.subhome",
            MODULE_APP_GAME: "module.app_game",
            MODULE_SEARCH: "module.search",
            MODULE_CHANNEL_PAYMENT: "module.channel_payment",
            MODULE_VOD_PAYMENT: "module.vod_payment",
            MODULE_VOD: "module.vod",
            MODULE_NOTICE: "module.notice", // [dj.son] [2017-07-10] 신규 추가 모듈
            MODULE_FRAMEWORK: "framework"   // framework 에서 제공되는 기능
        }
    });
})();