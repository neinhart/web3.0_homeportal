/**
 * Created by user on 2016-06-10.
 */

"use strict";

KTW.managers.module.Def = {
    STATE: {
        NONE: 0,            // none 상태
        DEPLOYED: 1,        // ??
        DOWNLOADED: 2,      // zip 다운로드 완료 상태
        STORED: 3,          // 저장이 완료된 상태
        LOADED: 4,          // 로딩된 상태
        NEED_DOWNLOAD: 5,   // 다운로드가 필요한 상태
        DOWNLOADING: 6      // 다운로드중인 상태
    },

    LOAD_LOCATION: {
        LOCAL: 0,
        SERVER: 1
    },

    MODULE_DIR_NAME: "module",

    MODULE_LOADING_TIMEOUT: 3 * 1000,

    MODULE_CHECK_PERIOD: 2 * 60 * 60 * 1000,
    MODULE_CHECK_MINIMUM_START: 10 * 60 * 1000
};