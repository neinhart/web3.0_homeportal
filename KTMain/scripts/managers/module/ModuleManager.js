"use strict";

/**
 */
KTW.managers.module.ModuleManager = (function() {

	var log = KTW.utils.Log;
	var util = KTW.utils.util;

    var Def = KTW.managers.module.Def;
    var moduleDataManager = null;
    var moduleUpdateManager = null;
    var powerModeManager = null;
    var layerManager = null;

    var stanbyTimer = null;
    var standbyTime = -1;


    var module_map = new KTW.utils.HashMap();
    var moduleArea;


    function _init() {
        log.printDbg("_init() ");

        moduleArea = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "module_area"
            },
            parent: $("body")
        });

        moduleDataManager = KTW.managers.module.ModuleDataManager;
        moduleUpdateManager = KTW.managers.module.ModuleUpdateManager;
        powerModeManager = KTW.managers.device.PowerModeManager;
        layerManager = KTW.ui.LayerManager;

        moduleDataManager.init();
        moduleUpdateManager.init();

        powerModeManager.addPowerModeChangeListener(onPowerModeChange);
    }

    function onPowerModeChange (mode) {
        log.printDbg("onPowerModeChange() ");

        if (mode === KTW.oipf.Def.CONFIG.POWER_MODE.ACTIVE_STANDBY) {
            standbyTime = -1;

            startStanbyTimer();
        }
        else {
            clearStanbyTimer();
        }
    }

    function startStanbyTimer () {
        if (!stanbyTimer) {
            stanbyTimer = setTimeout(function() {
                checkModuleUpdate();
            }, getNextCheckTime());
        }
    }

    function clearStanbyTimer () {
        if (stanbyTimer) {
            clearTimeout(stanbyTimer);
            stanbyTimer = null;
        }
    }

    /**
     * [dj.son] STANDBY 상태에서 버전 업데이트 체크시 서버 부하를 막기 위한 보강 로직
     *
     * - 최초에는 10분 ~ 2시간 사이에 수행
     * - 그 이후 부터는 2시간 간격으로 수행
     */
    function getNextCheckTime () {
        var now = Date.now();
        var checkTime;
        /**
         * 10분 ~ 2시간
         */
        if (standbyTime < 0) {
            checkTime = Def.MODULE_CHECK_MINIMUM_START + getTryTimeBasedOnMAC(now, Def.MODULE_CHECK_PERIOD - Def.MODULE_CHECK_MINIMUM_START);
            checkTime = checkTime - now;
            standbyTime = checkTime;
        }
        else {
            standbyTime = Def.MODULE_CHECK_PERIOD;
        }

        log.printDbg("checkTime " + standbyTime);

        return standbyTime;
    }

    /**
     * 부하 분산처리를 위해서 MAC정보로 처리한다.
     */
    function getTryTimeBasedOnMAC (currentTime, timePeriod) {

        var mac = null;
        if (KTW.PLATFORM_EXE === "PC") {
            mac = "f3e249008a70"; // FOR TEST CODE
        }
        else {
            mac = KTW.oipf.adapters.HWAdapter.networkInterface.getMacAddress();
        }

        // 마지막 4byte 값을 이용하여 접근 시각값을 계산한다.
        var cnt = 0;
        var total = 0;
        for (var i = mac.length - 1; i >= 0 && cnt < 4; i--) {
            var ch = mac.charAt(i);
            try {
                var n = parseInt(ch, 16);
                total = total * 16 + n;
                log.printDbg("ch=" + ch + ",int=" + n + ",sum=" + total);
                cnt++;
            } catch (error) {
                log.printErr(error.message);
            }
        }

        // offset은 STB마다 0시간 ~ timePeriod 사이의 고정 값
        /**
         * [dj.son] 분 단위에서 초단위로 랜덤값 뽑도록 수정
         */
        var offset = (total * 1000) % timePeriod;
        // STB마다 특정 시간에 update를 요청하도록 하기 위해서 timePeriod의 배수에 offset을 더한다
        var tryTime = Math.floor(currentTime / timePeriod) * timePeriod + offset;
        if (tryTime < currentTime) {
            tryTime += timePeriod;
        }
        log.printDbg("tryTime= " + new Date(tryTime));

        return tryTime;
    }

    function checkModuleUpdate () {
        log.printDbg("checkModuleUpdate() ");

        clearStanbyTimer();
        startStanbyTimer();

        /**
         * [dj.son] 부팅 후 첫키 시나리오를 안타고 대기 모드 진입하면, office code 가 null 인 경우가 존재할 수 있음
         *          이를 해결하기 위해 모듈 버전 조회전에 첫키 시나로오 & office code 가져오는 시나리오를 수행하도록 처리
         */
        if (KTW.managers.service.AppServiceManager.checkFirstKey()) {
            _checkOfficeCode(function (success) {
                if (success) {
                    _checkModuleData(function () {
                        if (moduleDataManager.isNeedModuleDownload()) {
                            startModuleUpdate();
                        }
                    });
                }
            });
        }
        else {
            KTW.managers.service.AppServiceManager.processFirstKey(function () {
                _checkOfficeCode(function (success) {
                    if (success) {
                        _checkModuleData(function () {
                            if (moduleDataManager.isNeedModuleDownload()) {
                                startModuleUpdate();
                            }
                        });
                    }
                });
            });
        }
    }

    function _checkOfficeCode (cbCheckOfficeCode) {
        log.printDbg("_checkOfficeCode() ");

        KTW.managers.service.AppServiceManager.getOfficeCode(function () {
            var officeCd = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OFFICE_CODE);

            if (officeCd && officeCd.length > 0) {
                cbCheckOfficeCode(true);
            }
            else {
                cbCheckOfficeCode();
            }
        });
    }

    function startModuleUpdate () {
        log.printDbg("startModuleUpdate() ");

        // 1. 업데이트할 모듈 리스트 얻기
        var updateList = moduleDataManager.getUpdateModuleList();

        // 2. 모듈 & 모듈 레이어 제거
        for (var i = 0; i < updateList.length; i++) {
            _removeModule(module_map.get(updateList[i].moduleId));
        }

        // 3. 모듈 업데이트 시작
        moduleUpdateManager.updateModule({
            list: updateList,
            cbUpdateModule: function() {
                moduleDataManager.checkNeedModuleDownload();
            }
        });
    }

    function _checkModuleData(cbCheckModuleData) {
        log.printDbg("_checkModuleData() ");

        moduleDataManager.checkModuleData(cbCheckModuleData);
    }

    function _loadModule(options) {
        log.printDbg("_loadModule() ");

        if (!options || !options.moduleId) {
            log.printDbg("moduleId is not exist... so return ");
            return;
        }

        if (KTW.managers.service.AppServiceManager.checkFirstKey()) {
            log.printDbg("_onKeyDown() received first key event");

            createModule(options);
        }
        else {
            KTW.managers.service.AppServiceManager.saveCallbackProcessFirstKey(function () {
                createModule(options);
            });
            KTW.managers.service.AppServiceManager.processFirstKey(function () {
                createModule(options);
            });
        }

        //createModule(options);

        // [dj.son] 여러개의 모듈을 load 하는 로직 삭제, 추후 필요시 추가
        // 모듈의 defendancy 는 없어졌지만, 여러개의 모듈을 로딩하는 로직은 남김
        //if (!options.arrModuleId || options.arrModuleId.length === 0) {
        //    options.arrModuleId = [options.moduleId];
        //}
        //else {
        //    if (options.arrModuleId.indexOf(options.moduleId) < 0) {
        //        options.arrModuleId.unshift(options.moduleId);
        //    }
        //}
        //
        //if (options.arrModuleId && options.arrModuleId.length > 0) {
        //    createModule(options);
        //}
        //else {
        //    if (options.cbLoadModule) {
        //        options.cbLoadModule(false);
        //    }
        //}
    }

    function createModule(options) {
        log.printDbg("createModule() ");

        var moduleId = options.moduleId;
        var module = module_map.get(moduleId);
        var moduleData = moduleDataManager.getModuleData(moduleId);

        if (!module && moduleData) {
            module = new KTW.managers.module.Module({
                moduleData: moduleData,
                parentDiv: moduleArea
            });

            module_map.put(moduleId, module);

            module.load(function(success) {
                if (!success) {
                    module_map.remove(moduleId);
                    module.remove();

                    // TODO 안내 팝업 띄우는 시나리오가 추가될 것이라 예상됨
                    showModuleLoadingFailPopup(moduleId);
                }

                if (options.cbLoadModule) {
                    options.cbLoadModule(success);
                }
            });
        }
        else {
            if (options.cbLoadModule) {
                options.cbLoadModule(false);
            }
        }

        // [dj.son] 여러개의 모듈을 load 하는 로직 삭제, 추후 필요시 추가
        //if (options.arrModuleId.length > 0) {
        //    var moduleId = options.arrModuleId[0];
        //    var module = module_map.get(moduleId);
        //    var moduleData = moduleDataManager.getModuleData(moduleId);
        //
        //    if (!module && moduleData) {
        //        module = new KTW.managers.module.Module({
        //            moduleData: moduleData,
        //            parentDiv: moduleArea
        //        });
        //
        //        module_map.put(moduleId, module);
        //
        //        module.load(function(success) {
        //            options.arrModuleId.shift();
        //
        //            if (success) {
        //                createModule(options);
        //            }
        //            else {
        //                module.remove();
        //                module_map.remove(moduleId);
        //
        //                if (options.cbLoadModule) {
        //                    options.cbLoadModule(false);
        //                }
        //            }
        //        });
        //    }
        //    else {
        //        options.arrModuleId.shift();
        //        createModule(options);
        //    }
        //}
        //else {
        //    if (options.cbLoadModule) {
        //        options.cbLoadModule(true);
        //    }
        //}
    }


    function showModuleLoadingFailPopup (moduleId) {
        log.printDbg("showModuleLoadingFailPopup() ");

        /**
         * [dj.son] 각 모듈별로 error 코드 구분하도록 수정
         */
        var errorCode = "MODULE-00100";

        switch (moduleId) {
            case KTW.managers.module.Module.ID.MODULE_KIDS:
                errorCode = "MODULE-00101";
                break;
            case KTW.managers.module.Module.ID.MODULE_FAMILY_HOME:
                errorCode = "MODULE-00102";
                break;
            case KTW.managers.module.Module.ID.MODULE_SUBHOME:
                errorCode = "MODULE-00103";
                break;
            case KTW.managers.module.Module.ID.MODULE_APP_GAME:
                errorCode = "MODULE-00104";
                break;
            case KTW.managers.module.Module.ID.MODULE_SEARCH:
                errorCode = "MODULE-00105";
                break;
            case KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT:
                errorCode = "MODULE-00106";
                break;
            case KTW.managers.module.Module.ID.MODULE_VOD_PAYMENT:
                errorCode = "MODULE-00107";
                break;
            case KTW.managers.module.Module.ID.MODULE_VOD:
                errorCode = "MODULE-00108";
                break;
            case KTW.managers.module.Module.ID.MODULE_NOTICE:
                errorCode = "MODULE-00109";
                break;
        }

        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: [KTW.ERROR.EX_CODE.MODULE.EX001.title],
                cssObj: {}
            },{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                message: KTW.ERROR.EX_CODE.MODULE.EX001.message.concat("(" + errorCode + ")"),
                cssObj: {}
            }],

            arrButton: [{id: "ok", name: "확인"}],

            cbAction: function (id) {
                if (id === "ok") {
                    layerManager.deactivateLayer({
                        id: "module_loading_fail_popup"
                    });
                }
            }
        };

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "module_loading_fail_popup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true,
            cbActivate: function () {}
        });
    }

    function _removeModule (module) {
        log.printDbg("_removeModule() ");

        if (module) {
            var arrModuleLayer = layerManager.getModuleLayerArray(module.moduleId);

            // 모듈 레이어 전부 제거
            if (arrModuleLayer && arrModuleLayer.length > 0) {
                for (var i = 0; i < arrModuleLayer.length; i++) {
                    layerManager.deactivateLayer({
                        id: arrModuleLayer[i].id,
                        remove: true
                    });
                }
            }

            // 모듈 제거
            module_map.remove(module.moduleId);
            module.remove();
        }
    }

    function _getModule(moduleId) {
        log.printDbg("_getModule() ");

        return module_map.get(moduleId);
    }

    /**
     * module object 획득
     * 만약 loading 이 되어 있지 않은 경우 해당 모듈을 loading 한다.
     *
     * @param moduleId
     * @param callback
     */
    function _getModuleForced(moduleId, callback) {
        log.printDbg("_getModuleForced(), moduleId = " + moduleId);
        _loadModule({
            moduleId: moduleId,
            cbLoadModule: function() {
                callback(_getModule(moduleId));
            }
        });
    }

	return {
		init : _init,

        checkModuleData: _checkModuleData,
        loadModule: _loadModule,

        getModule: _getModule,
        getModuleForced: _getModuleForced,

        // _loadModule
        removeModule: _removeModule,

        checkOfficeCode: _checkOfficeCode,

        decrypt: function (source) {
            log.printDbg("decryptModuleConfig()");

            /**
             * 복호화키값을 로그로 노출하지 않도록 수정
             */
            var rsaKey = KTW.oipf.AdapterHandler.basicAdapter.getConfigText("home_cipher_key", true);
            var result = "";

            try {
                var arrSource = source.split(";");
                var crypt = new JSEncrypt();
                crypt.setPrivateKey(rsaKey);

                log.printDbg("Start RSA Decrypt");
                for (var i = 0; i < arrSource.length; i++) {
                    result += crypt.decrypt(arrSource[i]);
                }
                log.printDbg("End RSA Decrypt");
            }
            catch (e) {
                log.printErr(e.message);
            }

            log.printDbg("result : " + result);

            return result;
        }
	}

})();