/**
 * Created by user on 2016-06-10.
 */

"use strict";

KTW.managers.module.ModuleUpdateManager = (function() {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var Def = KTW.managers.module.Def;
    var moduleDataManager = null;
    var fileManager = null;

    var DEFAULT_MODULE_PATH = "live/" + Def.MODULE_DIR_NAME;

    function _init() {
        log.printDbg("_init() ");

        moduleDataManager = KTW.managers.module.ModuleDataManager;

        DEFAULT_MODULE_PATH = KTW.CONSTANT.DEFAULT_FILE_PATH + "/" + Def.MODULE_DIR_NAME;

        fileManager = KTW.managers.data.fileManager;

        fileManager.mkDir(DEFAULT_MODULE_PATH, function(is_success, msg, path_obj, dirs){
            log.printDbg('##### init() is_success: ' + is_success);
            log.printDbg('##### init() path_obj: ' + log.stringify(path_obj));
            log.printDbg('##### init() dirs: ' + dirs);
        });
    }

    /**
     * 모듈 Update
     *
     * - 불필요한 directory 및 파일 삭제
     * - startModuleDownload 호출
     */
    function _updateModule(options) {
        log.printDbg("_updateModule() ");

        _checkUnnecessaryDir(function() {
            startModuleDownload({
                list: options.list,
                cbComplete: function() {
                    if (options.cbUpdateModule) {
                        options.cbUpdateModule();
                    }
                }
            });
        });
    }

    /**
     * 불필요한 모듈 directory 삭제
     *
     * - module directory 하위 디렉토리 리스트 가져옴
     * - 하위 디렉토리 리스트 이름과 모든 모듈들의 id 와 비교해서 같은 값이 있는지 검사
     * - 만약 있다면, checkModuleDir() 호출해서 이전 버전의 디렉토리 삭제
     * - 만약 없다면, 해당 디렉토리 삭제
     */
    function _checkUnnecessaryDir(cbCheckUnnecessaryDir) {
        log.printDbg("_checkUnnecessaryDir() ");

        fileManager.listFiles(DEFAULT_MODULE_PATH, function(success, fileEntry) {
            log.printDbg("_checkUnnecessaryDir() - get modules dir child list - " + success);

            if (success) {
                // 모듈 데이터 정보 가져와 fileEntry 와 path 비교해서 모듈 데이터에 없는 path 면 삭제

                var moduleData = moduleDataManager.getModuleData();
                var moduleLength = moduleData.length;
                var module = null;

                if (fileEntry && fileEntry.length > 0) {
                    var fileEntryLength = fileEntry.length;

                    for (var i = 0; i < fileEntryLength; i++) {
                        // directory 가 아니면 삭제 (혹시 zip 파일이 남아있으면 삭제하도록)
                        if (!fileEntry[i].isDirectory) {
                            fileManager.removeFile(fileEntry[i].fullPath);
                            continue;
                        }

                        module = null;
                        for (var j = 0; j < moduleLength; j++) {
                            if (moduleData[j].moduleId === fileEntry[i].name) {
                                module = moduleData[j];
                                break;
                            }
                        }

                        /**
                         * [dj.son] 모듈을 filesystem 에 저장시 version 을 구분하지 않기로 함
                         * 따라서 version directory 검사 후 삭제하는 로직 제거
                         */
                        if (!module) {
                            fileManager.deleteDirectory(fileEntry[i].fullPath);
                        }
                    }
                }
            }

            if (cbCheckUnnecessaryDir) {
                cbCheckUnnecessaryDir();
            }
        });
    }

    /**
     * 모듈 다운로드 & 저장 로직
     *
     * - options.list 에서 module data 가져옴
     * - download & store
     * - 모듈 Update 결과에 대해 배포서버에 notify
     * - 다시 startModuleDownload 호출 & 반복
     */
    function startModuleDownload(options) {
        log.printDbg("startModuleDownload() ");

        if (options && options.list && options.list.length > 0) {
            var module = options.list.shift();

            downloadModule({
                module: module,
                cbDownload: function(xhr) {
                    if (xhr && xhr.responseText && xhr.responseText.length > 0) {
                        log.printDbg("startModuleDownload() xhr.responseText is exist!!!");

                        storeModule({
                            module: module,
                            xhr: xhr,
                            cbStoreModule: function(result) {
                                // 성공시
                                // 모듈 데이터 변경

                                if (result.success) {
                                    module.localVersion = module.version;
                                    module.localPath = result.localPath;
                                    module.state = Def.STATE.STORED;

                                    moduleDataManager.completeModuleUpdate(module);

                                    log.printDbg("startModuleDownload() " + module.moduleId + " update success");

                                    notifyUpdateResult({
                                        success: true,
                                        module: module
                                    });
                                }
                                else {
                                    log.printDbg("startModuleDownload() " + module.moduleId + " update fail");

                                    notifyUpdateResult({
                                        success: false,
                                        module: module,
                                        failMessage: result.errorMsg
                                    });
                                }

                                startModuleDownload(options);
                            }
                        })
                    }
                    else {
                        log.printDbg("startModuleDownload() xhr.responseText is not exist!!!");

                        startModuleDownload(options);

                        notifyUpdateResult({
                            success: false,
                            module: module,
                            failMessage: "Module Download fail"
                        });
                    }
                }
            });
        }
        else {
            log.printDbg("startModuleDownload() all module update logic finish");

            if (options.cbComplete) {
                options.cbComplete();
            }
        }
    }

    function downloadModule(options) {
        log.printDbg("downloadModule() " + options.module.downloadUrl);

        $.ajax({
            url: options.module.downloadUrl,
            type: "get",
            beforeSend: function(xhr) {
                xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
            }
        }).done(function(data, status, xhr) {
            log.printDbg("downloadModule() success");

            options.cbDownload(xhr);
        }).fail(function(xhr, status, error) {
            log.printDbg("downloadModule() fail");
            log.printDbg(error);

            options.cbDownload();
        });
    }

    function storeModule(options) {
        log.printDbg("storeModule() ");

        // dir path 설정 후 saveZipFile 호출
        // saveZipFile 에서는 간단히 zip 파일 저장, unzip, zip 파일 삭제 순으로 동작

        var dir = DEFAULT_MODULE_PATH + "/" + options.module.moduleId;

        fileManager.saveZipFile(dir, options.xhr, function(success, entry, paths) {
            log.printDbg("storeModule() end - " + success);

            var result = {
                success: success
            };

            if (success) {
                log.printDbg("storeModule() saved path " + dir + "/");

                /**
                 * [dj.son] download 받는 zip 의 directory 구조를 고려하여 path 설정
                 * - zip 의 directory 구조 : "moduleId/......"
                 */
                result.localPath = paths.path + options.module.moduleId + "/";
            }
            else {
                log.printDbg("storeModule() errorMsg " + entry);

                result.errorMsg = entry;
            }

            options.cbStoreModule(result);
        });
    }

    function notifyUpdateResult (options) {
        log.printDbg("notifyDownloadSuccess() ");

        var url = "";
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT) {
            url = KTW.DATA.HTTP.MODULE_SERVER.BMT_HTTPS_URL + "/deploy-api/update";
        }
        else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            url = KTW.DATA.HTTP.MODULE_SERVER.LIVE_HTTPS_URL + "/deploy-api/update";
        }
        else {
            url = KTW.DATA.HTTP.MODULE_SERVER.TEST_URL + "/deploy-api/update";
        }

        KTW.managers.module.ModuleManager.checkOfficeCode(function (success) {
            if (success) {
                var officeCd = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OFFICE_CODE);
                var postData = {
                    saId: KTW.SAID,
                    officeCd: officeCd,
                    model: KTW.oipf.adapters.HWAdapter.getModelName(),
                    moduleId: options.module.moduleId,
                    frameworkVersion: KTW.FRAMEWORK_VERSION,
                    version: options.module.version
                };

                if (options.success) {
                    url += "/success";
                }
                else {
                    url += "/fail";
                    postData.failMessage = options.failMessage;
                }

                $.ajax({
                    url: url,
                    type: "post",
                    dataType: 'json',
                    async: true,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(postData),
                    crossOrigin: false
                });
            } else {
                log.printErr("notifyUpdateResult(), officeCd is null");
            }
        });
        /*
        var officeCd = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OFFICE_CODE);
        var postData = {
            saId: KTW.SAID,
            officeCd: officeCd,
            model: KTW.oipf.adapters.HWAdapter.getModelName(),
            moduleId: options.module.moduleId,
            frameworkVersion: KTW.FRAMEWORK_VERSION,
            version: options.module.version
        };

        if (options.success) {
            url += "/success";
        }
        else {
            url += "/fail";
            postData.failMessage = options.failMessage;
        }

        $.ajax({
            url: url,
            type: "post",
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            crossOrigin: false
        });
        */
    }

    return {
        init: _init,

        updateModule: _updateModule,
        checkUnnecessaryDir: _checkUnnecessaryDir
    };
})();

