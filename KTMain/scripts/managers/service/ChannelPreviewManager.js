/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelPreviewManager.js</code>
 *
 * 미가입 채널 미리보기 기능을 관리한다.
 * 특정 미가입 채널의 경우 가입되지 않은 상태에서도 일정 시간(대부분 3분 이하) 동안 채널을 시청할 수 있는 기능을 제공한다.
 * 단, 이 기능은 CAS 로부터 전달 받은 미리보기 정보를 기반으로 동작되어야 한다.
 *
 * 3.0 에서는 main vbo 와 pip vbo 가 동시에 뜨는 경우가 있음 (ex: 채널 편성표)
 * 또한 PipLayer 에서 channel & cas 이벤트 수신해서 알아서 관련 UI 를 띄우고 있고, 여기에는 미리보기도 포함됨 (pip 관련 UI 는 pip 관련 로직에서 전부 처리)
 * 따라서 ChannelPreviewManager 에서는 main vbo 에 대한 미리보기 처리만 한다.
 *
 * 실제로 ChannelPreviewManager 에서는 Preview 에 대한 Object 생성 및 start / stop / changeIframeMode 만 호출하고,
 * 나머지 미리보기의 모든 로직은 Preview 내부에서 처리한다.
 *
 * @author jjh1117
 * @since 2016-09-08
 */

"use strict";

KTW.managers.service.ChannelPreviewManager = (function () {
    
    var log = KTW.utils.Log;
    
    var main_preview = null;
    var pip_preview = null;

    var is_init = false;

    /**
     * start channel preview
     * 
     * @param data - preview meta data
     */
    function _start (is_main , data , ch) {
        log.printDbg("_start() is_main : " + is_main + " , data : " + JSON.stringify(data) + " ch : " + JSON.stringify(ch));
        /**
         * 보완 코드 형태임
         * ChannelSelectEvent에서 먼저 Stop을 호출함.
         */
        if(is_main === true) {
            if(main_preview !== null) {
                var currentChannel = main_preview.getChannel();
                if(currentChannel.ccid !== ch.ccid) {
                    _stop(is_main, is_main === true ? true : false);
                }else {
                    return;
                }
            }
        }else {
            if(pip_preview !== null) {
                var currentChannel = pip_preview.getChannel();
                if(currentChannel.ccid !== ch.ccid) {
                    _stop(is_main, is_main === true ? true : false);
                }else {
                    return ;
                }
            }

        }

        if(is_main === true) {
            main_preview = new Preview(ch , is_main);
            main_preview.startPreview(data);
        }else {
            pip_preview = new Preview(ch , is_main);
            pip_preview.startPreview(data);
        }

        if(is_init === false) {
            KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);
            KTW.ui.LayerManager.addChangeLayerListener(_checkPreviewDisplayMode);
            is_init = true;
        }

    }
    
    /**
     * stop channel preview
     */
    function _stop (is_main , timeout) {
        log.printDbg("_stop() is_main " + is_main + " , timeout : " + timeout);

        if(is_main === true) {
            if (main_preview !== null) {
                main_preview.stopPreview(timeout);
                main_preview.clearPauseTimer();
                main_preview = null;
            }
        }else {
            if (pip_preview !== null) {
                pip_preview.stopPreview(timeout);
                pip_preview.clearPauseTimer();
                pip_preview = null;
            }
        }
    }
    
    function _changeIframeMode (mode) {
        log.printDbg("_changeIframeMode() mode : " + mode);

        if (main_preview !== null) {
            main_preview.changeIframeMode(mode);
        }

        if (pip_preview !== null) {
            pip_preview.changeIframeMode(mode);
        }
    }

    function _isstart (is_main) {
        if(is_main === true) {
            if(main_preview !== null) {
                return true;
            }else {
                return false;
            }
        }else {
            if(pip_preview !== null) {
                return true;
            }else {
                return false;
            }

        }
    }

    function _checkPreviewDisplayMode() {
        log.printDbg("_checkPreviewDisplayMode() ");
        var lastLayer = KTW.ui.LayerManager.getLastLayerInStack();

        if (lastLayer && lastLayer.type === KTW.ui.Layer.TYPE.NORMAL && lastLayer.isShowing()) {
            if (main_preview !== null) {

                var screen_size = KTW.oipf.AdapterHandler.basicAdapter.getScreenSize();
                if (screen_size !== undefined && screen_size !== null) {
                    var left =  screen_size.x;
                    var top = screen_size.y;
                    var screenWidth = screen_size.width;
                    var screenHeight = screen_size.height;

                    var iframeMode = -1;
                    if (screenWidth !== undefined && screenWidth === KTW.CONSTANT.RESOLUTION.WIDTH
                        && screenHeight !== undefined && screenHeight === KTW.CONSTANT.RESOLUTION.HEIGHT) {
                        iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE_HOME;
                    }
                    else {
                        if (left === KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT && top === KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP &&
                            screenWidth === KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH && screenHeight == KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT) {
                            iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE;
                        }
                        else if (left === KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT && top === KTW.CONSTANT.FULL_EPG_PIG_STYLE.TOP
                            && screenWidth === KTW.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH && screenHeight == KTW.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT) {
                            iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE;
                        }
                    }
                    if(iframeMode != -1) {
                        _changeIframeMode(iframeMode);
                    }
                }

            }
        }else {
            if (main_preview !== null) {
                var screen_size = KTW.oipf.AdapterHandler.basicAdapter.getScreenSize();
                if (screen_size !== undefined && screen_size !== null) {
                    var left =  screen_size.x;
                    var top = screen_size.y;
                    var screenWidth = screen_size.width;
                    var screenHeight = screen_size.height;

                    var iframeMode = -1;
                    if (screenWidth !== undefined && screenWidth === KTW.CONSTANT.RESOLUTION.WIDTH
                        && screenHeight !== undefined && screenHeight === KTW.CONSTANT.RESOLUTION.HEIGHT) {
                        iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;
                    }
                    else {
                        if (left === KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT && top === KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP &&
                            screenWidth === KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH && screenHeight == KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT) {
                            iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE;
                        }
                        else if (left === KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT && top === KTW.CONSTANT.FULL_EPG_PIG_STYLE.TOP
                            && screenWidth === KTW.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH && screenHeight == KTW.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT) {
                            iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE;
                        }
                    }
                    if(iframeMode != -1) {
                        _changeIframeMode(iframeMode);
                    }
                }
            }
        }
    }



    function _getChannel(is_main) {
        if(is_main === true) {
            if(main_preview !== null) {
                return main_preview.getChannel()
            }
        }else {
            if(pip_preview !== null) {
                return pip_preview.getChannel()
            }
        }

        return null;
    }

    function onServiceStateChange (options) {
        log.printDbg("PreviewData#onServiceStateChange() options.nextServiceState : " + options.nextServiceState);

        var is_main = true;
        /**
         * KTW.CONSTANT.SERVICE_STATE.OTHER_APP 체크 하지 않아도 되는 이유는
         * ChnanelSelectEvent에서 먼저 Stop 호출함.
         */
        if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED ||
        options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
            is_main = true;
            if (main_preview !== null) {
                _stop(is_main);
            }

            is_main = false;
            if (pip_preview !== null) {
                _stop(false);
            }
            KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
            KTW.ui.LayerManager.removeChangeLayerListener(_checkPreviewDisplayMode);
            
            is_init = false;
        }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.VOD || options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
            is_main = true;
            if (main_preview !== null) {
                _stop(is_main);
            }

            is_main = false;
            if (pip_preview !== null) {
                _stop(false);
            }
            KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
            KTW.ui.LayerManager.removeChangeLayerListener(_checkPreviewDisplayMode);
            is_init = false;
        }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV) {
            is_main = true;
            if(_isstart(is_main)) {
                var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.CHANNEL_PREVIEW);
                layer.hide({pause: true});
                if (main_preview !== null) {
                    main_preview.setPauseTimer(true);
                }
            }
            is_main = false;
            if(_isstart(is_main)) {
            }

        }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
            is_main = true;
            if(_isstart(is_main)) {
                var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.CHANNEL_PREVIEW);
                layer.show({resume: true});
            }

            is_main = false;
            if(_isstart(is_main)) {
            }
        }
    }

    
    var Preview = function(ch , is_main) {
        var current_channel = ch;
        var isMain = is_main;
        var iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;

    	var preview_time = 0; // 미리보기 시간
        var preview_duration = 0; // 미리보기 남은시간
        var preview_time_clock = 0; // 미리보기 시작시각

        var preview_type;
        var preview_service_number;

        var pauseTimer = null;

        function _getChannel() {
            return current_channel;
        }
        /**
         * set channel preview data
         *
         * preview_data syntax
         * {
         *     "ch_attr_type" : 1, // 채널 상품 구분
         *     "ch_preview_time" : 60, // 미리보기 시간 (초)
         *     "cool_time" : 1, // 미리보기 재진입 방지 시간(분)
         *     "preview_time" : 90, // 남은 미리보기 시간 (초)
         *     "service_number" : 656, // 채널 sid
         * }
         */
        function initData (data) {
            log.printDbg("PreviewData#initData() preview data = " + log.stringify(data));

            /*data = {
             "ch_attr_type" : "0", // 채널 상품 구분
             "ch_preview_time" : 60, // 미리보기 시간 (초)
             "cool_time" : 1, // 미리보기 재진입 방지 시간(분)
             "preview_time" : 90, // 남은 미리보기 시간 (초)
             "service_number" : 656, // 채널 sid
             }*/

            preview_time_clock = new Date().getTime();
            preview_duration = getPreviewDuration(data);
            preview_time = data.ch_preview_time;
            preview_service_number = data.service_number;
            // ch_attr_type
            // 0 - ISU (부가상품)
            // 1 - Upselling (상위 속성 상품)
            // 2 - ISU or Upselling
            preview_type = data.ch_attr_type;

            iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;

        }

        /**
         * show preview ui
         */
        function activate () {
            log.printDbg("activate()");

            var elapse_time = (new Date().getTime() -  preview_time_clock) / 1000;
            var remain_time = preview_duration - elapse_time;

            // 1초 미만인 경우 미리보기 UI를 노출하지 않도록 함
            if (remain_time < 1) {
                log.printDbg("PreviewData#activate() preview time over... so return");
                return;
            }

            if(isMain === true) {
                KTW.ui.LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.CHANNEL_PREVIEW,
                        type: KTW.ui.Layer.TYPE.BACKGROUND,
                        priority: KTW.ui.Layer.PRIORITY.BACKGROUND,
                        linkage: true,
                        params: {
                            data : {
                                "preview_time" : preview_time,
                                "duration" : preview_duration,
                                "remain_time" : Math.ceil(remain_time)
                            },
                            visible: true
                        }
                    },
                    //clear_normal: true,
                    visible: true,
                    cbActivate: function() {

                    }
                });

                _changeIframeMode();
                // var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.HOME_MENU);
                // if(layer !== undefined && layer !== null) {
                //     log.printDbg("PreviewData#_deactivate() layer.isShowing() : " + layer.isShowing());
                // }
                // if(layer !== undefined && layer !== null && layer.isShowing() === true) {
                //     _changeIframeMode(KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE_HOME);
                // }else {
                //     _changeIframeMode();
                // }

            }else {
                _clearPauseTimer();
                _setPauseTimer(isMain);
            }
        }

        /**
         * hide preview ui
         */
        function deactivate () {
            log.printDbg("PreviewData#_deactivate()");

            if(isMain === true) {
                var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.CHANNEL_PREVIEW);

                if (layer && layer.isShowing()) {
                    log.printDbg("PreviewData#deactivate() layer.isShowing()... so return");

                    layer.displayHide();
                    setTimeout(function(){
                        KTW.ui.LayerManager.deactivateLayer({
                            id: KTW.ui.Layer.ID.CHANNEL_PREVIEW
                        });
                    }, 10);
                }
            }
        }
        
        /**
         * set preview duration
         * ch_preview_time < preview_time : ch_preview_time
         * ch_preview_time > preview_time : preview_time
         */
        function getPreviewDuration (params) {
            var duration = 0;
            var ch_preview_time = params.ch_preview_time;
            var preview_time = params.preview_time;
            
            if (ch_preview_time <= preview_time) {
                duration = ch_preview_time;
            }
            else if (ch_preview_time > preview_time) {
                duration = preview_time;
            }
            
            return duration;
        }


        function _setIframeMode (mode) {
            log.printDbg("setIframeMode()");

            var screen_size = KTW.oipf.AdapterHandler.basicAdapter.getScreenSize();

            if(mode !== undefined && mode !== null) {
                iframeMode = mode;
                return;
            }else {
                iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;
            }

            // var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.HOME_MENU);
            // if(layer !== undefined && layer !== null) {
            //     log.printDbg("_setIframeMode layer.isShowing() : " + layer.isShowing());
            // }
            //
            // if(layer !== undefined && layer !== null && layer.isShowing() === true) {
            //     iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE_HOME;
            //     return;
            // }

            if (screen_size !== undefined && screen_size !== null) {
                var left =  screen_size.x;
                var top = screen_size.y;
                var screenWidth = screen_size.width;
                var screenHeight = screen_size.height;

                if (screenWidth !== undefined && screenWidth === KTW.CONSTANT.RESOLUTION.WIDTH && screenHeight !== undefined
                    && screenHeight === KTW.CONSTANT.RESOLUTION.HEIGHT) {
                    log.printDbg("setIframeMode() step#1");

                    iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;
                }
                else {
                    log.printDbg("setIframeMode() step#2");

                    if (left === KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT && top === KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP &&
                        screenWidth === KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH
                        && screenHeight == KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT) {
                        log.printDbg("setIframeMode() step#3");
                        iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE;
                    }
                    else if (left === KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT && top === KTW.CONSTANT.FULL_EPG_PIG_STYLE.TOP &&
                        screenWidth === KTW.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH
                        && screenHeight == KTW.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT) {
                        log.printDbg("setIframeMode() step#4");

                        iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE;
                    }
                }
            }
            else {
                log.printDbg("PreviewData#_changeIframeMode() screen_size == null screen_size : " + screen_size);
            }
        }



        function _startPreview (data) {
            log.printDbg("PreviewData#_startPreview()");

            initData(data);
            activate();


        }

        function _stopPreview (timeout) {
            log.printDbg("PreviewData#_stopPreview(" + timeout + ")");

            deactivate();

            preview_duration = 0;
            preview_time_clock = 0;

            // 시간이 만료되어 정상종료되는 경우에는
            // stopPreview caRequest 가 전달되도록 함
            if (timeout) {
                KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_STOP_PREVIEW, {"service_number" : preview_service_number});
            }


        }

        function _changeIframeMode (mode) {
            log.printDbg("PreviewData#_changeIframeMode() mode : " + mode);

            _setIframeMode(mode);

            var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.CHANNEL_PREVIEW);
            if (layer !== null) {
                log.printDbg("PreviewData#_changeIframeMode() iframeMode  : " + iframeMode);
                layer.changeIframeMode(iframeMode);
            }
            else {
                log.printDbg("PreviewData#_changeIframeMode() getLayer == null layer : " + layer);
            }
        }

        function _setPauseTimer(is_main) {
            log.printDbg("PreviewData#_setPauseTimer() is_main  : " + is_main);
            var elapse_time = (new Date().getTime() -  preview_time_clock) / 1000;
            elapse_time = Math.ceil(elapse_time);
            log.printDbg("PreviewData#_setPauseTimer() elapse_time  : " + elapse_time);

            pauseTimer = setTimeout(function () {
                log.printDbg("PreviewData#_setPauseTimer() Timeout");
                KTW.managers.service.ChannelPreviewManager.stop(is_main, true);
                _clearPauseTimer();
            }, (preview_duration - elapse_time) * 1000);
            log.printDbg("PreviewData#_setPauseTimer() pauseTimer  : " +  (preview_duration - elapse_time) * 1000);
        }

        function _clearPauseTimer() {
            if (pauseTimer !== null) {
                clearTimeout(pauseTimer);
                pauseTimer = null;
            }
        }


        return {
            startPreview: _startPreview,
            stopPreview: _stopPreview,
            getChannel: _getChannel,
        	
        	changeIframeMode: _changeIframeMode,
            setIframeMode : _setIframeMode,
            setPauseTimer: _setPauseTimer,
            clearPauseTimer: _clearPauseTimer
        }
    };
    
    return {
        start: _start,
        stop: _stop,
        isstart: _isstart,
        getChannel : _getChannel,
        changeIframeMode: _changeIframeMode,
        checkPreviewDisplayMode: _checkPreviewDisplayMode
    };
}());