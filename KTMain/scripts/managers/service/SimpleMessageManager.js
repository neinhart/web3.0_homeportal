/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SimpleMessageManager</code>
 *
 *  토스트 팝업 노출 생성 관리
 *  {
 *  simple_message_list : [
 *  {
 *    message_type : MESSAGE_TYPE_ICON
 *    data : images/xxx/xxx.png
 *  },
 *  {
 *    message_type : MESSAGE_TYPE_TEXT
 *    data : 문구
 *  }]
 *  };
 *
 *   아이콘 가로/세로 사이즈는 고정 됨.
 *
 * @author jjh1117
 * @since 2017-01-20
 */

"use strict";

KTW.managers.service.SimpleMessageManager = (function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;


    function _init() {
    }

    function _showMessageTextOnly(messageText) {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , messageText);

        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }


    function _showCaptionServiceNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "자막서비스 제공 중입니다 ");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON , "images/icon/icon_option_related.png");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "연관 메뉴에서 변경가능합니다 ");
        simpleMessageInfo[simpleMessageInfo.length] = info;


        _activatePopup(simpleMessageInfo);
    }

    function _showAddFavChannelNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "선호 채널로 등록되었습니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showRemoveFavChannelNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "선호 채널에서 해제되었습니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showAdultCheckPasswordErrorNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showAdultCheckPasswordErrorMaxCountNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "비밀번호를 잊어버리셨을 경우, 설정 > 비밀번호 변경/초기화를 이용해 주세요");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo , true);
    }

    function _showBuyPinCheckPasswordErrorNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "인증번호가 일치하지 않습니다. 다시 입력해 주세요");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showUHDChannelErrorNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "UHD채널에서는 채널 동시시청 기능을 실행 할 수 없습니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showPasswordCheckNetworkErrorNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "통신 서버 오류입니다 잠시 후 다시 입력해주세요");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showMovieChoicePPVStartNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "본편이 시작하면 해당 채널로 이동합니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showBlueToothConnectionNotiMessage(deviceName , isAudioDevice) {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON_BIG , "images/icon/icon_bt_audio2.png");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , deviceName + " 블루투스 장치가 연결되었습니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        //2017.05.15 [sw.nam] 팝업 내 음성 설정 버튼 제거
/*        if(isAudioDevice !== undefined && isAudioDevice !== null && isAudioDevice === true) {
            info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.CONTEXT_TEXT , "음성설정");
            simpleMessageInfo[simpleMessageInfo.length] = info;
        }*/


        _activatePopup(simpleMessageInfo);

    }

    function _showBlueToothA2DPConnectionNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "이어폰이 연결되었습니다. 장시간 사용 시 리모컨 배터리 교체가 필요합니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _showBlueToothA2DPDisConnectionNotiMessage() {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , "이어폰 연결이 해제되었습니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);

    }


    function _showBlueToothDisConnectionNotiMessage(deviceName) {
        var simpleMessageInfo = [];

        var info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON_BIG , "images/icon/icon_bt_audio2.png");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        info = _makeSimpleMessageInfo(KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT , deviceName + " 블루투스 장치가 연결 해제되었습니다");
        simpleMessageInfo[simpleMessageInfo.length] = info;

        _activatePopup(simpleMessageInfo);
    }

    function _activatePopup(simpleMessageInfo , isTimeRestricted) {
        var param = {
            simple_message_list : simpleMessageInfo
        };

        var priority = KTW.ui.Layer.PRIORITY.POPUP + 10;
        if (KTW.managers.service.KidsModeManager.isKidsModeAdultAuthPopupOpend()
            || KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            priority = KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 6;
        }

        var layer = KTW.ui.LayerManager.getLayer("SimpleMessagePopup");
        if (layer && layer.isShowing()) {
            _hide();
        }

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "SimpleMessagePopup",
                type: KTW.ui.Layer.TYPE.BACKGROUND,
                priority: priority,
                view : KTW.ui.view.popup.SimpleMessagePopup,
                linkage: true,
                params: {
                    data : param
                }
            },
            new: true,
            visible: true
        });
    }

    function _hide() {
        log.printDbg('_hide()');
        KTW.ui.LayerManager.deactivateLayer({
            id: "SimpleMessagePopup",
            remove: true
        });
    }


    function _makeSimpleMessageInfo(type , data) {
        var info = {
            message_type : type ,
            data : data
        };
        return info;
    }


    return {
        init: _init,
        makeSimpleMessageInfo: _makeSimpleMessageInfo,
        showMessageTextOnly: _showMessageTextOnly ,
        showCaptionServiceNotiMessage : _showCaptionServiceNotiMessage,
        showAddFavChannelNotiMessage : _showAddFavChannelNotiMessage,
        showRemoveFavChannelNotiMessage : _showRemoveFavChannelNotiMessage,
        showAdultCheckPasswordErrorNotiMessage : _showAdultCheckPasswordErrorNotiMessage,
        showUHDChannelErrorNotiMessage : _showUHDChannelErrorNotiMessage,
        showPasswordCheckNetworkErrorNotiMessage : _showPasswordCheckNetworkErrorNotiMessage,
        showBuyPinCheckPasswordErrorNotiMessage : _showBuyPinCheckPasswordErrorNotiMessage,
        showMovieChoicePPVStartNotiMessage : _showMovieChoicePPVStartNotiMessage,
        showAdultCheckPasswordErrorMaxCountNotiMessage : _showAdultCheckPasswordErrorMaxCountNotiMessage,
        showBlueToothConnectionNotiMessage : _showBlueToothConnectionNotiMessage,
        showBlueToothA2DPConnectionNotiMessage : _showBlueToothA2DPConnectionNotiMessage,
        showBlueToothDisConnectionNotiMessage : _showBlueToothDisConnectionNotiMessage,
        showBlueToothA2DPDisConnectionNotiMessage : _showBlueToothA2DPDisConnectionNotiMessage,
        hide : _hide

    }


}());

//define constant.
Object.defineProperty(KTW.managers.service.SimpleMessageManager, "SIMPLE_MESSAGE", {
    value : {
        SIMPLE_MESSAGE_TYPE : {
            ICON : 0 ,
            ICON_BIG : 1 ,
            TEXT : 2,
            CONTEXT_TEXT : 3
        }
    },
    writable : false,
    configurable : false
});


