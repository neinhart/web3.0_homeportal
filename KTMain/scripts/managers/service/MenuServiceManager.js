/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MenuServiceManager</code>
 *
 * menu 정보등을 기준으로 module 간 이동에 대한 manager 역할을 담당한다.
 *
 * @author dhlee
 * @since 2017-03-10
 */

"use strict";

KTW.managers.service.MenuServiceManager = (function () {

    var log = KTW.utils.Log;

    /**
     * 특정 편성 메뉴가 Kids Module을 로딩해야 하는 메뉴인지를 확인한다.
     * 2 depth 편성 정보 중 cat_type이 "KidsSub" or "PWorld" 값을 갖는 카테고리가 존재해야 한다.
     *
     * @param menu
     * @returns {boolean}
     */
    function isKidsMenu (menu) {
        log.printDbg("isKidsMenu()");

        if (menu) {
            var tmpMenu = getTopParentMenu(menu);

            if (tmpMenu.children) {
                var length = tmpMenu.children.length;
                for (var i = 0; i < length; i++) {
                    if (tmpMenu.children[i].catType === KTW.DATA.MENU.CAT.KIDSSUB || tmpMenu.children[i].catType === KTW.DATA.MENU.CAT.PWORLD) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * 특정 편성 메뉴가 AppGm module을 로딩해야 하는 메뉴인지를 확인한다.
     * 2 depth 편성 정보 중 cat_type 이 "AppGm" 값을 갖는 가상 카테고리가 존재해야 한다.
     *
     * @param menu
     * @returns {boolean}
     */
    function _isAppGmMenu (menu) {
        log.printDbg("isAppGmMenu()");

        if (menu) {
            var tmpMenu = getTopParentMenu(menu);

            if (tmpMenu.children) {
                var length = tmpMenu.children.length;
                for (var i = 0; i < length; i++) {
                    if (tmpMenu.children[i].catType === KTW.DATA.MENU.CAT.APPGM) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    function _isChannelGuideMenu (menu) {
        log.printDbg("_isChannelGuideMenu(), menu = " + menu);

        var bChannelGuide = false;

        if (menu) {
            var tmpMenu = getTopParentMenu(menu);

            if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                bChannelGuide = true;
            }
        }

        return bChannelGuide;
    }

    function _isSettingMenu (menu) {
        log.printDbg("_isSettingMenu(), menu = " + menu);

        var bSetting = false;

        if (menu) {
            var tmpMenu = getTopParentMenu(menu);

            if (tmpMenu.catType === KTW.DATA.MENU.CAT.CONFIG) {
                bSetting = true;
            }
        }

        return bSetting;
    }

    /**
     * 특정 메뉴의 최상위 메뉴 return
     */
    function getTopParentMenu (menu) {
        var targetMenu = menu;

        while (targetMenu.parent) {
            targetMenu = targetMenu.parent;
        }

        return targetMenu;
    }

    function _getModuleIdByMenu (menu) {
        log.printDbg("_getModuleIdByMenu()");

        var moduleId = null;

        if (menu) {
            var topParent = getTopParentMenu(menu);

            if (_isChannelGuideMenu(topParent)) {
                // 채널가이드 메뉴
                moduleId = KTW.managers.module.Module.ID.MODULE_FRAMEWORK;
            }
            else if (_isSettingMenu(topParent)) {
                // 설정 메뉴
                moduleId = KTW.managers.module.Module.ID.MODULE_FRAMEWORK;
            }
            else if (isKidsMenu(topParent)) {
                // 키즈 메뉴
                moduleId = KTW.managers.module.Module.ID.MODULE_KIDS;
            }
            else if (_isAppGmMenu(topParent)) {
                // 앱/게임 메뉴
                moduleId = KTW.managers.module.Module.ID.MODULE_APP_GAME;
            }
            else if (topParent.catType === KTW.DATA.MENU.CAT.SUBHOME) {
                // 우리집맞춤TV 메뉴
                moduleId = KTW.managers.module.Module.ID.MODULE_FAMILY_HOME;
            }
            else if (topParent.catType === KTW.DATA.MENU.CAT.SRCH) {
                // 검색 메뉴
                moduleId = KTW.managers.module.Module.ID.MODULE_SEARCH;
            }
            else {
                // 일반 VOD 메뉴 (Subhome 모듈)
                moduleId = KTW.managers.module.Module.ID.MODULE_SUBHOME;
            }
        }

        return moduleId;
    }

    function _getModuleIdByMenuId (menuId) {
        log.printDbg("_getModuleIdByMenuId()");

        if (!menuId || menuId === "") {
            return;
        }

        var menu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (tmpMenu) {
                if (tmpMenu.id === menuId) {
                    return true;
                }
            }
        })[0];

        return _getModuleIdByMenu(menu);
    }

    function _jumpMenu (options) {
        log.printDbg("_jumpMenu()");

        if (!options || !options.menu) {
            return;
        }

        // dhlee 2017.03.23 param 으로 moduleId를 전달 받은 경우는 그 값을 사용한다.
        var moduleId;
        if (!options.moduleId) {
            moduleId = _getModuleIdByMenu(options.menu);
        } else {
            moduleId = options.moduleId;
        }


        if (moduleId === KTW.managers.module.Module.ID.MODULE_FRAMEWORK) {
            if (_isChannelGuideMenu(options.menu)) {
                var isActivateChannelGuide = true;

                /**
                 * jjh1117 2017/11/15
                 * 실시간 인기채널 하위 메뉴로 진입 할 경우
                 * VOD 재생여부 체크 하여 팝업을 띄운다.
                 */
                if (KTW.CONSTANT.IS_OTS === false && KTW.managers.service.StateManager.isVODPlayingState() && (options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_TERRESTRIAL
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_HOMESHOPPING
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_SPORTS
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_MOVIE
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_DRAMA
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ANIMATION
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_KIDS
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ENTERTAINMENT
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_NEWS
                    || options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_DOCUMENTARY)) {
                    // TODO Basic POpup

                    var isCalled = false;
                    var popupData = {
                        arrMessage: [{
                            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                            message: ["알림"],
                            cssObj: {}
                        }, {
                            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                            message: KTW.ERROR.EX_CODE.HOME.EX001.message,
                            cssObj: {}
                        }],
                        arrButton: [{id: "ok", name: "확인"}],
                        cbAction: function (buttonId) {
                            if (!isCalled) {
                                isCalled = true;

                                if (buttonId) {
                                    KTW.ui.LayerManager.deactivateLayer({
                                        id: "RealTimeLayerBlockGuidePopup",
                                        remove: true
                                    });
                                }
                            }
                        }
                    };

                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: "RealTimeLayerBlockGuidePopup",
                            type: KTW.ui.Layer.TYPE.POPUP,
                            priority: KTW.ui.Layer.PRIORITY.POPUP,
                            view : KTW.ui.view.popup.BasicPopup,
                            params: {
                                data : popupData
                            }
                        },
                        visible: true,
                        cbActivate: function () {}
                    });
                    return ;
                }

                if (options.jump) {
                    var channelGuideLayer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.CHANNEL_GUIDE);

                    if (channelGuideLayer && channelGuideLayer.isShowing() && !channelGuideLayer.isJumpAvailable(options.menu.id)) {
                        KTW.ui.LayerManager.deactivateLayer({id: KTW.ui.Layer.ID.CHANNEL_GUIDE});
                        isActivateChannelGuide = false;
                    }
                }

                if (isActivateChannelGuide) {
                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.CHANNEL_GUIDE,
                            type: KTW.ui.Layer.TYPE.NORMAL,
                            priority: KTW.ui.Layer.PRIORITY.NORMAL,
                            linkage: false,
                            params: {
                                menuId: options.menu.id,
                                isHotKey: options.isHotKey
                            }
                        },
                        visible: true,
                        clear_normal: options.jump,
                        skipRequestShow: options.skipRequestShow
                    });
                }
            }
            else if (_isSettingMenu(options.menu)) {
                //2017.03.13 sw.nam 추가
                //KTW.ui.LayerManager.activateLayer({
                //    obj: {
                //        id: KTW.ui.Layer.ID.SETTING_MAIN,
                //        type: KTW.ui.Layer.TYPE.NORMAL,
                //        priority: KTW.ui.Layer.PRIORITY.NORMAL,
                //        params: {
                //            menuId: options.menu.id
                //        }
                //    },
                //    visible: true,
                //    clear_normal: options.jump,
                //    cbActivate: function() {}
                //});
                if (options.menu.catType === KTW.DATA.MENU.CAT.CONFIG && !options.menu.parent) {
                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.SETTING_MAIN,
                            type: KTW.ui.Layer.TYPE.NORMAL,
                            priority: KTW.ui.Layer.PRIORITY.NORMAL
                        },
                        visible: true,
                        clear_normal: options.jump,
                        skipRequestShow: options.skipRequestShow
                    });
                } else {
                    if (!options.updateSubTitle) {
                        // 2017.05.12 dhlee
                        // MessageManager에서 showcategory 를 통해 설정 메뉴 jump 하는 경우 options.menu 가 넘어온다.
                        //options.menuId = options.menu.menuId;
                        //options.name = options.menu.name;
                        log.printDbg("_jumpMenu(), menuId = " + options.menu.menuId + "id = " + options.id);
                        KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                            menuId        : options.menu.id,
                            name          : options.menu.name,
                            jump          : true,
                            skipRequestShow: options.skipRequestShow,
                            updateSubTitle: false  // 설정 메뉴인 경우를 위해 추가
                        });

                    }
                    else {
                        KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID(options);
                    }
                }
            }

            if (options.cbJumpMenu) {
                options.cbJumpMenu();
            }
        }
        else {
            KTW.managers.module.ModuleManager.getModuleForced(moduleId, function (module) {
                if (options.isBoot && moduleId === KTW.managers.module.Module.ID.MODULE_KIDS && KTW.managers.service.KidsModeManager.isKidsMode()) {
                    module.execute({method: "kidsModeBooting", skipRequestShow: options.skipRequestShow});
                    return;
                }

                if (module) {
                    module.activateLayerByMenu(options);
                }

                if (options.cbJumpMenu) {
                    options.cbJumpMenu();
                }
            });
        }
    }

    function _jumpByMenuData (options) {
        log.printDbg("_jumpByMenuData(" + log.stringify(options) + ")");

        if (!options || !options.itemType) {
            log.printDbg("options.itemType is not exist... so return");
            return;
        }

        switch (options.itemType) {
            case KTW.DATA.MENU.ITEM.CATEGORY:
                var menu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    allSearch: false,
                    cbCondition: function (menu) {
                        if (menu.id === options.cat_id) {
                            return true;
                        }
                    }
                })[0];

                /**
                 * [dj.son] 키즈 핫키 점프임을 알리기 위해 bHotKey 추가
                 */
                _jumpMenu({menu: menu, bHotKey: options.bHotKey});

                /**
                 * [dj.son] showCategoryList 함수 내용을 보면 결국 activateLayerByMenu 를 호출함 (subHome 모듈), 따라서 showCategoryList 를 직접 호출하지 않고 jumpMenu 호출하도록 수정
                 **/
//                KTW.managers.module.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_SUBHOME, function (subHomeModule) {
//                    if (subHomeModule) {
//                        subHomeModule.execute({
//                            /*method: "showDetail",
//                            //2017 06.11 sw.nam
//                            // dj.son 가이드 대로 해당 parameter 수정
//*//*                            params: {
//                                cat_id: options.cat_id
//                            }*//*
//                            params: {
//                                cat_id: options.cat_id,
//                                const_id: options.const_id,
//                                req_cd: options.req_cd,
//                                callback: function (result) {
//                                    var param;
//                                    param = result;
//                                    callback(param);
//                                }
//                            }*/
//                            method: "showCategoryList",
//                            cat_id: options.cat_id,
//                            jump: true,
//                            callback: function(result) {
//                                if (options.callback) {
//                                    options.callback(result);
//                                }
//                            }
//                        });
//                    }
//                });
                break;
            case KTW.DATA.MENU.ITEM.SERIES:
            case KTW.DATA.MENU.ITEM.CONTENT:
                KTW.managers.module.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                    if (vodModule) {
                        vodModule.execute({
                            method: "showDetail",
                            //2017 06.11 sw.nam
                            // dj.son 가이드 대로 해당 parameter 수정
/*                            params: {
                                cat_id: options.cat_id,
                                const_id: options.const_id
                            }*/
                            params: {
                                cat_id: options.cat_id,
                                const_id: options.const_id,
                                req_cd: options.req_cd,
                                callback: function (result) {
                                    if (options.callback) {
                                        options.callback(result);
                                    }
                                }

                            }
                        });
                    }
                });
                break;
            case KTW.DATA.MENU.ITEM.INT_M:
                /**
                 * [dj.son] 매시업 차일드 앱은 멀티캐스트 양방향으로 편성됨. 따라서 locator 에 매시업 id 가 있는 경우 예외처리 추가
                 */
                var nextServiceState = KTW.CONSTANT.SERVICE_STATE.OTHER_APP;
                if (options.locator.indexOf(KTW.CONSTANT.APP_ID.MASHUP) > -1) {
                    if (KTW.managers.service.StateManager.isVODPlayingState()) {
                        nextServiceState = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD;
                    }
                    else {
                        nextServiceState = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                    }
                }

                KTW.managers.service.AppServiceManager.changeService({
                    nextServiceState: nextServiceState,
                    obj: {
                        type: KTW.CONSTANT.APP_TYPE.MULTICAST,
                        param: options.locator,
                        ex_param: options.parameter
                    },
                    visible: nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP
                });
                break;
            case KTW.DATA.MENU.ITEM.INT_U:
                var nextState = KTW.managers.service.StateManager.isTVViewingState() ? KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV : KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD;
                KTW.managers.service.AppServiceManager.changeService({
                    nextServiceState: nextState,
                    obj: {
                        /**
                         * [dj.son] itemType 이 UNICAST 인 경우, 실행 어플의 type 도 UNICAST 로 설정하도록 수정
                         */
                        type: KTW.CONSTANT.APP_TYPE.UNICAST,
                        param: options.locator,
                        ex_param: options.parameter
                    },
                    visible: true
                });
                break;
            case KTW.DATA.MENU.ITEM.INT_W:
                /**
                 * [dj.son] 아이템타입이 8 인 경우는 웹뷰 형식으로 changeService 호출 대신 startChildApp 호출하도록 수정 (state 변화 X)
                 * - 참고로 이제까지는 2.0 과 동일하게 사용하는 곳에서 8인 경우를 구분해서 startChildApp 호출했었음
                 */
                //var nextState = KTW.managers.service.StateManager.isTVViewingState() ? KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV : KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD;
                //KTW.managers.service.AppServiceManager.changeService({
                //    nextServiceState: nextState,
                //    obj: {
                //        type: KTW.CONSTANT.APP_TYPE.MULTICAST,
                //        param: options.locator,
                //        ex_param: options.parameter
                //    },
                //    visible: true
                //});
                KTW.managers.service.AppServiceManager.startChildApp(options.locator);
                break;
        }
    }

    /**
     * [dj.son] 앱스토어 핫키 처리를 위해 AppGmMenu 를 찾는 함수 추가
     */
    function _getAppGmMenu () {
        var appGmMenu = null;
        var arrMenu = KTW.managers.data.MenuDataManager.getMenuData();
        for (var i = 0; i < arrMenu.length; i++) {
            if (_isAppGmMenu(arrMenu[i])) {
                appGmMenu = arrMenu[i];
            }
        }

        return appGmMenu;
    }

    return {
        getModuleIdByMenu: _getModuleIdByMenu,
        getModuleIdByMenuId: _getModuleIdByMenuId,

        jumpMenu: _jumpMenu,
        jumpByMenuData: _jumpByMenuData,

        isChannelGuideMenu: _isChannelGuideMenu,
        isSettingMenu: _isSettingMenu,
        isAppGmMenu: _isAppGmMenu,

        getAppGmMenu: _getAppGmMenu
    };

})();
