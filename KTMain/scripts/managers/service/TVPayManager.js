"use strict";

/**
 * manage TV Pay module
 */
KTW.managers.service.TVPayManager = (function () {

    /**
     * [dj.son] tv pay 모듈에서 지정해준 고정값
     */
    var STORE_ID = "KTITVBZ001";
    var STORE = "BIZ-W";

    /**
     * [dj.son] 현재 tv pay 모듈이 개발 버전밖에 지원을 안해서 true 로 설정함
     * TODO LIVE 할때는 false 로 변경해야함
     */
    var isDev = false;

    var callback = null;

    function _makeBHPayment (options) {
        KTW.utils.Log.printDbg("_makeBHPayment() " + KTW.utils.Log.stringify(options));

        if (!options || options.amount === null || options.amount === undefined || !options.goodsName || !options.period || !options.appendData) {
            KTW.utils.Log.printDbg("parameter is not valid... so return...");
            return;
        }

        callback = options.callback;

        try {
            var tvPayElement = document.getElementById("tvpay");

            /**
             * [dj.son] isDev 빼고는 모두 String 으로 주어야함;;;; 안그러면 STB 멈춤
             */
            tvPayElement.makeBHPayment(isDev, STORE_ID, options.amount + "", options.goodsName + "", options.period + "", options.appendData + "", "onPaymentCompleted", STORE);

            /**
             * [dj.son] TV 페이 모듈에서 기본 네비 키 & ok 키 & 이전키 & 나가기키 이외에 키 처리를 안하고 있어서 더미 팝업 생성
             * 
             * - 키 이벤트가 올라오면 해당 팝업 닫으면서 TV 페이 모듈 종료
             */
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.TV_PAY_DUMMY_PUPUP,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP,
                    view : KTW.ui.view.popup.TVPayDummyView,
                    params: {}
                },
                visible: true,
                cbActivate: function () {}
            });
        }
        catch (e) {
            KTW.utils.Log.printErr(e);
        }
    }

    /**
     * [dj.son] 원래는 KTW.managers.service.TVPayManager.onPaymentCompleted 형태로 함수 정의했는데, tv pay 모듈에서 함수 찾을 수 없다고 함;;;; 그냥 window 에 곧바로 함수 정의하니 호출됨;;;;
     */
    window.onPaymentCompleted = function (resultCode, resultMessage) {
        KTW.utils.Log.printDbg("onPaymentCompleted() " + resultCode + " - " + resultMessage);

        if (callback) {
            callback({
                resultCode: resultCode,
                resultMessage: resultMessage
            });
        }
    };


    function getChildListProperty (data){
        var array = Object.getOwnPropertyNames(data);
        var nodeName = null;
        for(var i = 0; i < array.length; i++){
            nodeName = array[i];
            if(data[nodeName].getProperties){
                break;
            }
        }
        return nodeName;
    };

    function _desctoryTVPayModule () {
        KTW.utils.Log.printDbg("_desctoryTVPayModule()");

        try {
            var chList = KTW.oipf.AdapterHandler.serviceAdapter.findApplications("useType", "independent");
            var keyword = "main.mv";

            for (var i = 0; i < chList.length; i++) {
                var child = chList[i];

                var property = getChildListProperty(child);

                if (property && child[property]) {
                    var result = child[property].getProperties();

                    if (result.startUrl.indexOf(keyword) > -1) {
                        child.destroyApplication();
                    }
                }
            }
        }
        catch (e) {
            KTW.utils.Log.printErr(e);
        }
    }
    
    return {
        makeBHPayment: _makeBHPayment,
        desctoryTVPayModule: _desctoryTVPayModule
    };
}());
