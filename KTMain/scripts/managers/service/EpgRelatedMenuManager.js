/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgRelatedMenuManager</code>
 *
 * Full Epg / Mini Epg에서 제공하는 연관메뉴 UI를 제공한다.
 *  var related_menu_info = {
 *        left_menu_info : {
 *            "channel" : cur_channel,
 *            "is_ots_channel" : true/false,
 *            "is_show_favorited_channel" : true/false,
 *            "is_show_program_detail : true/false ,
 *            "is_show_2ch" : true/false,
 *            "is_show_4ch" : true/false,
 *            "is_show_hit" : true/false
 *            "is_show_genre_option" : true/false
 *            "focus_option_index" : 0 / 1
 *            left_menu_callback_func : callback (parameter : menu_index , genre_option_index) // is_show_favorited_channel : 0 , detail : 1
 *                                                                      is_show_2ch : 2 , is_show_4ch : 3,
 *                                                                      is_show_hit : 4
 *                                                                      genre_option_index : 0 / 1
 *        },
 *        right_menu_info : {
 *            caller : 0 or 1 ,(KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER FULL_EPG,MINI_EPG)
 *            full_epg : {
 *                    top_genre : [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    middle_genrc : [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    bottom_genrc ; [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    focus_genrc : top_genrc/bottom_genrc/bottom_genrc
 *                    focus_index : 0 ~ (n-1),
 *                    fullepg_callback : callback(menuId)
 *            },
 *
 *            mini_epg : {
 *                    top_menu : {
 *                        is_show_top_menu : true/false , *
 *                        channel_genre : [{name:"전체채널",menuId:"AAAAA"},{name:"선호채널" , menuId:"BBBBB"}],
 *                    },
 *                    middle_menu : {
 *                        caption : {
 *                            is_show_caption : true/false
 *                            is_dimmed : true/false
 *                        } ,
 *                        voice : {
 *                             is_show_voice : true/false
 *                             is_dimmed : true/false
 *                             voicelist : [],
 *                             voice_focus : focusIndex
 *                        },
 *                        bluetooth_voice_lang {
 *                             is_show_bluetooth_voice_lang : true/false
 *                             is_dimmed : true/false
 *                             bluetooth_voice_lang_list : [],
 *                             bluetooth_voice_lang_focus : focusIndex
 *                        },
 *                        bluetooth_listening  {
 *                             is_show_bluetooth_listening : true/false
 *                             is_dimmed : true/false
 *                             bluetooth_listening_list : []
 *                             bluetooth_listening_focus : focusIndex
 *                        }
 *                    },
 *
 *                    bottom_menu ; {
 *                        is_show_bottom_menu : true/false
 *                        app_list : [{name:북마크,value:BOOKMARK},{name:홈위젯,value:HOME_WIDJECT}]
 *                    },
 *                    miniepg_callback : callback(parameter : menu , index)
 *                            menu : top_menu/middle_menu/bottom_menu
 *                            index : 0 ~ (n-1)
 *            },
 *        }
 *      };
 *
 *
 * @author jjh1117
 * @since 2016-11-28
 */

"use strict";

KTW.managers.service.EpgRelatedMenuManager = (function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;


    function _init() {
    }

    /**
     *
     * @param focusChannel : 포커스 채널 정보
     * @param leftMenuCallbackFunc : callback함수 parameter(index , genreoption_index)
     * @param isShowFavoritedChannel : 선호채널 등록/해제 버튼 노출 여부
     * @param isShowProgramDetail : OTS 프로그램 자세히 버튼 노출 여부
     * @param isShowTwoChannel : 2채널 동시 시청 버튼 노출 여부
     * @param isShowFourChannel : 4채널 동시 시청 버튼 노출 여부
     * @param isShowHitChannel : 실시간 인기채널 버튼 노출 여부
     * @param isShowGenreOption : 장르 옵션 drop box 노출 여부
     * @param rightMenuCallbackFunc : callback함수 parameter(menuid)
     * @param focusMenuId : 편성표 메뉴ID
     * @private
     */
    function _showFullEpgRelatedMenu(focusChannel , leftMenuCallbackFunc , isShowFavoritedChannel , isShowProgramDetail , isShowTwoChannel ,
                                     isShowFourChannel , isShowHitChannel , isShowGenreOption , focusOptionIndex , rightMenuCallbackFunc , focusMenuId) {
        var topGenre = [];
        var middleGenre = [];
        var bottomGenre = [];

        var focusGenre = -1;
        var focusIndex = 0;


        // 채널 가이드 메뉴 가져오기
        var guideMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id == KTW.managers.data.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                    return true;
                }
            }
        })[0];


        if(guideMenu !== undefined && guideMenu !== null) {
            var childrenLength = guideMenu.children.length;

            /**
             * Top genre 구성
             */
            var obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST);
            if(obj !== null) {
                topGenre[topGenre.length] = obj;
            }

            obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL);
            if(obj !== null) {
                topGenre[topGenre.length] = obj;
            }

            if(KTW.CONSTANT.IS_OTS === true) {
                obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.SKYLIFE_UHD_CHANNEL);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }

                obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.MOVIE_CHOICE);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }

            }
            else {
                obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.UHD_CHANNEL);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }
            }

            obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL);
            if(obj !== null) {
                topGenre[topGenre.length] = obj;
            }

            if (KTW.CONSTANT.IS_OTS === true) {
                obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.OLLEH_TV_CHANNEL);
                if(obj !== null) {
                    topGenre[topGenre.length] = obj;
                }
            }

            for(var x=0;x<topGenre.length;x++) {
                if(topGenre[x].menuid === focusMenuId) {
                    focusGenre = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.TOP_GENRE;
                    focusIndex = x;
                    break;
                }
            }

            /**
             * Middle genre 구성
             */
            if(KTW.CONSTANT.IS_OTS === true) {
                for(var i=0;i<childrenLength;i++) {
                    var menu = guideMenu.children[i];
                    if(menu.id === KTW.managers.data.MenuDataManager.MENU_ID.PROGRAM_SEARCH) {
                        var genrechildrenLength = menu.children.length;
                        for(var j=0;j<genrechildrenLength;j++) {
                            var menuObject = _makeListObject(menu.children[j].name,menu.children[j].id);
                            if(menuObject !== null) {
                                middleGenre[middleGenre.length] = menuObject;
                            }
                        }
                        break;
                    }
                }
            }else {
                for(var i=0;i<childrenLength;i++) {
                    var menu = guideMenu.children[i];
                    if(menu.id === KTW.managers.data.MenuDataManager.MENU_ID.GENRE_CHANNEL) {
                        var genrechildrenLength = menu.children.length;
                        for(var j=0;j<genrechildrenLength;j++) {
                            var menuObject = _makeListObject(menu.children[j].name,menu.children[j].id);

                            if(menuObject !== null) {
                                middleGenre[middleGenre.length] = menuObject;
                            }
                        }
                        break;
                    }
                }
            }

            if(focusGenre<0) {
                for(var x=0;x<middleGenre.length;x++) {
                    if(middleGenre[x].menuid === focusMenuId) {
                        focusGenre = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.MIDDLE_GENRE;
                        focusIndex = x;
                        break;
                    }
                }
            }

            /**
             * Bottom genre 구성
             */
            var obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL);
            if(obj !== null) {
                bottomGenre[bottomGenre.length] = obj;
            }

            obj = _searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.TV_APP_CHANNEL);
            if(obj !== null) {
                bottomGenre[bottomGenre.length] = obj;
            }

            if(focusGenre<0) {
                for(var x=0;x<bottomGenre.length;x++) {
                    if(bottomGenre[x].menuid === focusMenuId) {
                        focusGenre = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.BOTTOM_GENRE;
                        focusIndex = x;
                        break;
                    }
                }
            }
        }

        var leftMenuinfo = _makeLeftMenuInfo(focusChannel,leftMenuCallbackFunc , isShowFavoritedChannel, isShowProgramDetail ,
            isShowTwoChannel , isShowFourChannel , isShowHitChannel , isShowGenreOption , focusOptionIndex);

        var fullepgMenuinfo = _makeFullEpgMenuInfo(topGenre , middleGenre,bottomGenre , focusGenre , focusIndex , rightMenuCallbackFunc) ;

        var rightMenuinfo = _makeRightMenuInfo(KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER.FULL_EPG , fullepgMenuinfo , {});


        var menuInfo = _makeMenuInfo(leftMenuinfo ,rightMenuinfo);

        _activateRelatedMenu(menuInfo);

    }

    function _searchMenu(guideMenu , id) {
        var menuObject = null;
        if(guideMenu !== undefined && guideMenu !== null) {
            var childrenLength = guideMenu.children.length;
            for(var i=0;i<childrenLength;i++) {
                var menu = guideMenu.children[i];
                if(menu.id === id) {
                    menuObject = _makeListObject(menu.name,menu.id);
                    break;
                }
            }
        }
        return menuObject;
    }

    function _activateTwoChannel(callbackFuncHide , callbackFuncSwap) {

        var tempChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
        if (tempChannel.isUHD === true && KTW.CONSTANT.IS_OTS === true) {
            KTW.managers.service.SimpleMessageManager.showUHDChannelErrorNotiMessage();
            return;
        }

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "PipMultiViewPopup",
                type: KTW.ui.Layer.TYPE.NORMAL,
                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                view : KTW.ui.view.popup.PipMultiViewPopup,
//                linkage: true,
                params: {
                    data : {
                        callback_hide : callbackFuncHide,
                        callback_swap : callbackFuncSwap
                    }
                }
            },
            visible: true
        });
    }

    function _activateFourChannel() {
        log.printDbg("_activateFourChannel()");

        var result = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id == KTW.managers.data.MenuDataManager.MENU_ID.WATCH_4CHANNEL) {
                    return true;
                }
            }
        })[0];

        var toBeJumpedChannelLocator = "";
        if (result) {

            toBeJumpedChannelLocator = result.locator;
        }
        else {

            toBeJumpedChannelLocator = KTW.CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE;
        }

        if (toBeJumpedChannelLocator !== null && toBeJumpedChannelLocator !== undefined) {
            /**
             * [dj.son] VOD 재생중 여부 상관없이 바로 튠하도록 수정
             */
            KTW.managers.service.MenuServiceManager.jumpByMenuData({
                itemType: KTW.DATA.MENU.ITEM.INT_M,
                locator: toBeJumpedChannelLocator
            });
        }

        return toBeJumpedChannelLocator;
    }

    function _activateRealTimeChannel() {
        log.printDbg("_activateRealTimeChannel()");
        if(KTW.CONSTANT.IS_OTS === false) {
            if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                log.printDbg("_activateRealTimeChannel() current Kids Mode. so return false");
                KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup();
                return ;
            }
        }

        if (KTW.managers.service.StateManager.isVODPlayingState()) {
            // TODO Basic POpup

            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                    message: ["알림"],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                    message: KTW.ERROR.EX_CODE.HOME.EX001.message,
                    cssObj: {}
                }],
                arrButton: [{id: "ok", name: "확인"}],
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        if (buttonId) {
                            KTW.ui.LayerManager.deactivateLayer({
                                id: "RealTimeLayerBlockGuidePopup",
                                remove: true
                            });
                        }
                    }
                }
            };

            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: "RealTimeLayerBlockGuidePopup",
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData
                    }
                },
                visible: true,
                cbActivate: function () {}
            });
        }
        else {


            if(KTW.CONSTANT.IS_OTS === true) {
                KTW.ui.LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.OTS_STITCHING,
                        type: KTW.ui.Layer.TYPE.NORMAL,
                        priority: KTW.ui.Layer.PRIORITY.NORMAL,
                        //linkage: true,
                        params: {
                            fromKey:true
                        }
                    },
                    visible: true,
                    cbActivate: function() {

                    }
                });
            }else {
                var menu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (tmpMenu) {
                        if(KTW.CONSTANT.IS_OTS === true) {
                            if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                                return true;
                            }
                        }else {
                            if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL) {
                                return true;
                            }
                        }
                    }
                })[0];

                if (menu) {
                    var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

                    if (moduleId) {
                        KTW.managers.service.MenuServiceManager.jumpMenu({
                            moduleId: moduleId,
                            menu: menu,
                            jump: KTW.CONSTANT.IS_OTS === true ? true : false,
                            cbJumpMenu: function () {}
                        });

                        return;
                    }
                }
            }
        }
    }

    function _activateFullEpgProgrammeDetailPopup (options) {
        log.printDbg("_activateFullEpgProgrammeDetailPopup()");

        if (!options || !options.channel || !options.program) {
            return;
        }

        var startTime = options.program.startTime * 1000;
        var endTime = (options.program.startTime + options.program.duration) * 1000;
        var nowTime = (new Date()).getTime();

        options.is_show_reservation = true;
        if (startTime <= nowTime && endTime > nowTime) {
            options.is_show_reservation = false;
        }

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "FullEpgProgramDetailPopup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.FullEpgProgramDetailPopup,
                params: {
                    data : options
                }
            },
            visible: true
        });
    }

    function _deactivateFullEpgProgrammeDetailPopup () {
        KTW.ui.LayerManager.deactivateLayer({
            id: "FullEpgProgramDetailPopup"
        });
    }

    function _favoriteChannelOnOff(curChannel , callbackFunc) {
        KTW.managers.service.FavoriteChannelManager.favoriteChannelOnOff(curChannel , callbackFunc);
    }

    function _activateFullEpg() {
        log.printDbg("_activateFullEpg()");
        var menu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (tmpMenu) {
                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                    return true;
                }
            }
        })[0];

        if (menu) {
            var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

            if (moduleId) {
                KTW.managers.service.MenuServiceManager.jumpMenu({
                    moduleId: moduleId,
                    menu: menu,
                    jump: true,
                    cbJumpMenu: function () {}
                });

                return;
            }
        }
    }

    function _activateFullAudioChannelEpg() {
        log.printDbg("_activateFullEpg()");
        var menu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (tmpMenu) {
                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL) {
                    return true;
                }
            }
        })[0];

        if (menu) {
            var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

            if (moduleId) {
                KTW.managers.service.MenuServiceManager.jumpMenu({
                    moduleId: moduleId,
                    menu: menu,
                    jump: true,
                    cbJumpMenu: function () {}
                });

                return;
            }
        }
    }

    function _activateKidsModeFullEpg() {
        log.printDbg("_activateKidsModeFullEpg()");
        var menu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (tmpMenu) {
                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.KIDS_CHANNEL) {
                    return true;
                }
            }
        })[0];

        if (menu) {
            var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

            if (moduleId) {
                KTW.managers.service.MenuServiceManager.jumpMenu({
                    moduleId: moduleId,
                    menu: menu,
                    jump: true,
                    cbJumpMenu: function () {}
                });

                return;
            }
        }else {
            log.printDbg("_activateKidsModeFullEpg() menu is null");
        }
    }


    function _activateFullFavoriteChannelEpg() {
        KTW.managers.service.FavoriteChannelManager.activateFavoriteChannelFullEpg();
    }

    function _activateBookmarkApp() {
        var nextServiceState = KTW.managers.service.StateManager.isVODPlayingState() ?
            KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;

        KTW.managers.service.AppServiceManager.changeService({
            nextServiceState: nextServiceState,
            obj: {
                type: KTW.CONSTANT.APP_TYPE.MULTICAST,
                param: KTW.CONSTANT.APP_ID.MASHUP,
                ex_param: KTW.CONSTANT.APP_CHILD_ID.BOOKMARK_ARCHIVE
            }
        });
    }

    function _activateHomeWidget() {
        var nextServiceState = KTW.managers.service.StateManager.isVODPlayingState() ?
            KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD : KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;

        KTW.managers.service.AppServiceManager.changeService({
            nextServiceState: nextServiceState,
            obj: {
                type: KTW.CONSTANT.APP_TYPE.MULTICAST,
                param: KTW.CONSTANT.APP_ID.MASHUP,
                ex_param: KTW.CONSTANT.APP_CHILD_ID.HOME_WIDGET
            }
        });
    }

    function _activateRelatedMenu(param) {
        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "EpgRelatedMenuPopup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.EpgRelatedMenuPopup,
                params: {
                    data : param
                }
            },
            visible: true
        });
    }

    function _deactivateRelatedMenu() {
        // KTW.ui.LayerManager.deactivateLayer({
        //     id: "EpgRelatedMenuPopup"
        // });
    }

    function _makeMenuInfo(leftMenuInfo , rightMenuInfo) {
        var menuInfo = {
            left_menu_info : leftMenuInfo ,
            right_menu_info : rightMenuInfo
        };

        return menuInfo;
    }

    function _makeLeftMenuInfo(focusChannel , leftMenuCallbackFunc , isShowFavoritedChannel , isShowProgramDetail , isShowTwoChannel ,
                               isShowFourChannel , isShowHitChannel , isShowGenreOption , focusOptionIndex) {
        var leftmenuinfo = {
            channel : focusChannel,
            is_ots_channel : KTW.CONSTANT.IS_OTS ,
            is_show_favorited_channel : (focusChannel === undefined || focusChannel === null) ? false : isShowFavoritedChannel === undefined ? true : isShowFavoritedChannel,
            is_show_program_detail : isShowProgramDetail === undefined ? false : isShowProgramDetail ,
            is_show_2ch : (isShowTwoChannel === undefined  || isShowTwoChannel === null) ? true : isShowTwoChannel,
            is_show_4ch : (isShowFourChannel === undefined || isShowFourChannel === null) ? true : isShowFourChannel,
            is_show_hit : (isShowHitChannel === undefined || isShowHitChannel === null)? true : isShowHitChannel,
            is_show_genre_option : (isShowGenreOption === undefined || isShowGenreOption === null)? true : isShowGenreOption,
            focus_option_index : focusOptionIndex,
            left_menu_callback_func : leftMenuCallbackFunc
        };
        return leftmenuinfo;
    }

    function _makeRightMenuInfo(callerId , fullEpgMenuInfo , miniEpgMenuInfo) {
        var menuinfo = {
            caller : callerId,
            full_epg : fullEpgMenuInfo,
            mini_epg : miniEpgMenuInfo
        };
        return menuinfo;
    }

    function _makeFullEpgMenuInfo(topgenre , middlegenre,bottomgenre , focusGenre , focusIndex , callbackFunc) {
        var menuinfo = {
            top_genre: topgenre,
            middle_genrc: middlegenre,
            bottom_genrc: bottomgenre,
            focus_genrc: focusGenre,
            focus_index: focusIndex,
            fullepg_callback: callbackFunc
        };
        return menuinfo;
    }

    function _makeMiniEpgMenuInfo(topmenuinfo , middlemenuinfo , bottommenuinfo, callbackFunc) {
        var miniepginfo = {
            top_menu : topmenuinfo,
            middle_menu : middlemenuinfo,
            bottom_menu : bottommenuinfo,
            miniepg_callback : callbackFunc
        };
        return miniepginfo;
    }

    function _makeMiniEpgTopMenuInfo(isShowTopMenu , list) {
        var menuinfo = {
            is_show_top_menu : isShowTopMenu ,
            channel_genre : list
        };
        return menuinfo;
    }

    function _makeMiniEpgMiddleMenuInfo(captionmenu , voicemenu , bluetoothvoicemenu , bluetoothlisteningmenu) {
        var menuinfo = {
            caption : captionmenu ,
            voice : voicemenu,
            bluetooth_voice_lang : bluetoothvoicemenu,
            bluetooth_listening : bluetoothlisteningmenu
        };
        return menuinfo;
    }

    function _makeMiniEpgBottomMenuInfo(isShowBottomMenu , list) {
        var menuinfo = {
            is_show_bottom_menu : isShowBottomMenu,
            app_list : list
        };
        return menuinfo;
    }

    function _makeCaptionMenuInfo(isShowCaption , isDimmed) {
        var menuinfo = {
            is_show_caption : isShowCaption ,
            is_dimmed : isDimmed
        };
        return menuinfo;
    }

    function _makeVoiceMenuInfo(isShowVoice , isDimmed , list , focusIndex) {
        var menuinfo = {
            is_show_voice : isShowVoice ,
            is_dimmed : isDimmed,
            voicelist : list,
            voice_focus : focusIndex
        };
        return menuinfo;
    }

    function _makeBlueToothVoiceMenuInfo(isShowBlueToothVoice , isDimmed , list , focusIndex) {
        var menuinfo = {
            is_show_bluetooth_voice_lang : isShowBlueToothVoice ,
            is_dimmed : isDimmed ,
            bluetooth_voice_lang_list : list,
            bluetooth_voice_lang_focus : focusIndex
        };
        return menuinfo;
    }

    function _makeBlueToothListeningMenuInfo(isShowBlueToothListening , isDimmed , list , focusIndex) {
        /**
         * [{name:"혼자듣기" , value : 0} , {name:"같이듣기" , value : 1}]
         */
        var menuinfo = {
            is_show_bluetooth_listening : isShowBlueToothListening ,
            is_dimmed : isDimmed ,
            bluetooth_listening_list : list ,
            bluetooth_listening_focus : focusIndex
        };
        return menuinfo;
    }

    function _makeListObject(objectName , objectValue) {
        var listObject = {
            name: objectName,
            menuid: objectValue
        };
        return listObject;
    }

    return {
        init: _init,
        showFullEpgRelatedMenu: _showFullEpgRelatedMenu,
        activateRelatedMenu: _activateRelatedMenu,
        deactivateRelatedMenu: _deactivateRelatedMenu,
        makeMenuInfo: _makeMenuInfo,
        makeLeftMenuInfo: _makeLeftMenuInfo,
        makeRightMenuInfo: _makeRightMenuInfo,
        makeMiniEpgMenuInfo: _makeMiniEpgMenuInfo,
        makeMiniEpgTopMenuInfo: _makeMiniEpgTopMenuInfo,
        makeMiniEpgMiddleMenuInfo: _makeMiniEpgMiddleMenuInfo,
        makeMiniEpgBottomMenuInfo: _makeMiniEpgBottomMenuInfo,
        makeCaptionMenuInfo: _makeCaptionMenuInfo,
        makeVoiceMenuInfo: _makeVoiceMenuInfo,
        makeBlueToothVoiceMenuInfo: _makeBlueToothVoiceMenuInfo,
        makeBlueToothListeningMenuInfo: _makeBlueToothListeningMenuInfo,
        makeListObject: _makeListObject,
        searchMenu: _searchMenu,
        activateTwoChannel: _activateTwoChannel,
        activateFourChannel: _activateFourChannel,
        activateRealTimeChannel: _activateRealTimeChannel,
        favoriteChannelOnOff: _favoriteChannelOnOff,
        activateFullEpg: _activateFullEpg,
        activateFullAudioChannelEpg: _activateFullAudioChannelEpg,
        activateKidsModeFullEpg: _activateKidsModeFullEpg,
        activateFullFavoriteChannelEpg: _activateFullFavoriteChannelEpg,
        activateBookmarkApp: _activateBookmarkApp,
        activateHomeWidget: _activateHomeWidget,
        activateFullEpgProgrammeDetailPopup: _activateFullEpgProgrammeDetailPopup,
        deactivateFullEpgProgrammeDetailPopup: _deactivateFullEpgProgrammeDetailPopup
    }


}());

//define constant.
Object.defineProperty(KTW.managers.service.EpgRelatedMenuManager, "MENU_INFO", {
    value : {
        LEFT_MENU : {
            FAVORITED : 0,
            DETAIL : 1,
            HIT_CH : 2,
            TWO_CH : 3,
            FOUR_CH : 4,
            GENRE_OPTION : 5,
            OPTION_TYPE : {
                SORT_START_TIME : 0,
                SORT_TEXT : 1
            }
        },
        RIGHT_MENU : {
            CALLER : {
                FULL_EPG  : 0,
                MINI_EPG : 1
            },
            FULL_EPG : {
                TOP_GENRE : 0,
                MIDDLE_GENRE : 1,
                BOTTOM_GENRE : 2
            },
            MINI_EPG : {
                TOP_MENU : 0,
                MIDDLE_MENU : 1,
                BOTTOM_MENU : 2
            },
            TOP_MENU : {
                TOTAL_CHANNEL : 0,
                F_CHANNEL : 1,
                KIDS_CHANNEL : 2,
                AUDIO_CHANNEL : 2,
                KIDS_PROGRAM_DETAIL : 3
            },
            MIDDLE_MENU : {
                CAPTION_MENU : 0,
                VOICE_MENU : 1,
                BLUE_TOOTH_VOICE_MENU : 2,
                BLUE_TOOTH_VOICE_LE_MENU : 3
            },
            BOTTOM_MENU : {
                BOOKMARK : 0,
                HOME_WIDGET : 1,
                KIDS_MODE_OFF : 2
            }
        }
    },
    writable : false,
    configurable : false
});


