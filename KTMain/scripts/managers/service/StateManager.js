"use strict";

/**
 * manage app and service state
 *
 * [dj.son] 단순히 service state 만 관리할 수 있도록 수정 - 추가로 LayerManager 에서 관리하던 other app state, appstore state, play av state 등도 여기서 관리
 * 현재 service state 뿐 아니라 이전 service state 까지 관리
 * 기존 서비스 로직 (exitToService, changeState) 들은 전부 AppServiceManager 에서 처리
 *
 * service state 체크는 직접 값을 비교할 수도 있고, StateManager 에서 제공하는 check 함수를 사용 가능 (_isTVViewingState, _isVODPlayingState, _isOtherAppState 등)
 *
 * service state 의 변경은 AppServiceManager 의 changeState 함수에서만 수행
 *   - changeState -> setServiceState -> listener 호출
 *   - changeState 함수 호출시 넘겨준 param 을 그대로 service change evt listener 호출시 param 으로 넘겨줌
 *   - 추가로 이전 state 상태를 param 에 추가하여 넘겨준
 *   - ex: { nextServiceState: TV_VIEWING, preServiceState: VOD, obj: {...}, .... }
 *
 */
KTW.managers.service.StateManager = (function () {
    
    var CONST_APP_STATE = KTW.CONSTANT.APP_STATE;
    var CONST_SVC_STATE = KTW.CONSTANT.SERVICE_STATE;
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;


    // App State (Initial, Running, Pause, Destroy)
    var appState = CONST_APP_STATE.UNKNOWN;
    
    // Service State (TV Viewing, VOD, DATA, STANDBY, TIME_RESTRICTED)
    var currentServiceState = CONST_SVC_STATE.TV_VIEWING;
    var preServiceState = currentServiceState;
    // TODO 2017.02.16 dhlee 2.0 R7 로직 추가
    var time_restrict_in_state = -1;

    // [dj.son] 기존 LayerManager 에서 관리하던 flag 들을 StateManager 로 옮김
    // 실제로 해당 값 setting 은 AppServiceManager 에서 함
    var isShowAppStore = false;
    var isNotifyStopAV = false;
    
    var serviceStateListener = [];

    function _init() {
        if (KTW.managers.device.PowerModeManager.powerState !== KTW.oipf.Def.CONFIG.POWER_MODE.ON) {
            currentServiceState = CONST_SVC_STATE.STANDBY;
            preServiceState = CONST_SVC_STATE.STANDBY;
        }
        
        log.printDbg("init() currentServiceState = " + _getServiceStateString(currentServiceState));
    }
    
    /**
     * whether or not current service state is TV viewing state
     * if current service state is TV_VIEWING, return true
     */
    function _isTVViewingState () {
        return (currentServiceState === CONST_SVC_STATE.TV_VIEWING || currentServiceState === CONST_SVC_STATE.OTHER_APP_ON_TV);
    }
    function _isVODPlayingState () {
        return (currentServiceState === CONST_SVC_STATE.VOD || currentServiceState === CONST_SVC_STATE.OTHER_APP_ON_VOD);
    }
    function _isOtherAppState () {
        return (currentServiceState === CONST_SVC_STATE.OTHER_APP
                || currentServiceState === CONST_SVC_STATE.OTHER_APP_ON_TV
                || currentServiceState === CONST_SVC_STATE.OTHER_APP_ON_VOD);
    }
    
    /**
     * whether or not app state is STARTED state
     */
    function _isRunningState() {
        return appState === CONST_APP_STATE.STARTED;
    }

    /**
     * check whether or not change to last state
     */
    function _isChangeSameState(options) {
        var is_same_state = false;
        if (currentServiceState === options.nextServiceState) {
            is_same_state = true;
        }

        if (options.forced !== true && is_same_state === true) {
            return true;
        }
        return false;
    }

    /**
     * [dj.son] Set Service State
     *
     * @param options nextServiceState - change state
     *                 visible - ???
     *                 forced - force change state
     */
    function _setServiceState(options) {
        log.printDbg("setServiceState()");

        if (!options || (options.nextServiceState == null || options.nextServiceState == undefined)) {
            log.printErr("nextServiceState is null");
            return;
        }

        log.printDbg("pre state = " + preServiceState + ", cur state = " + currentServiceState + ", change state = " + _getServiceStateString(options.nextServiceState) + ", visible : " + options.visible + ", force : " + options.force);

        if (!_isChangeSameState(options)) {
            preServiceState = currentServiceState;
            currentServiceState = options.nextServiceState;

            options.preServiceState = preServiceState;

            notifyServiceStateChanged(options);
        }
        else {
            log.printDbg("cur state === change state");
        }
    }
    
    function _addServiceStateListener (listener) {
        log.printDbg("_addServiceStateListener()");

        var index = serviceStateListener.indexOf(listener);
        if (index < 0) {
            serviceStateListener.push(listener);
        }
    }
    
    function _removeServiceStateListener (listener) {
        log.printDbg("_removeServiceStateListener()");

        var index = serviceStateListener.indexOf(listener);
        if (index !== -1) {
            serviceStateListener.splice(index, 1);
        }
    }

    function notifyServiceStateChanged (options) {
        log.printDbg("notifyServiceStateChanged(" + log.stringify(options) + ")");

        var arrListener = serviceStateListener.slice();
        var length = arrListener.length;
        for (var i = 0; i < length; i++) {
            if (arrListener[i]) {
                arrListener[i](options);
            }
        }
    }
    
    function _getServiceStateString (state) {
        var state_str;
        
        switch(state) {
            case CONST_SVC_STATE.TV_VIEWING :
                state_str = "TV_VIEWING";
                break;
            case CONST_SVC_STATE.VOD :
                state_str = "VOD";
                break;
            case CONST_SVC_STATE.OTHER_APP :
                state_str = "OTHER_APP";
                break;
            case CONST_SVC_STATE.OTHER_APP_ON_TV :
                state_str = "OTHER_APP_ON_TV";
                break;
            case CONST_SVC_STATE.OTHER_APP_ON_VOD :
                state_str = "OTHER_APP_ON_VOD";
                break;
            case CONST_SVC_STATE.STANDBY :
                state_str = "STANDBY";
                break;
            case CONST_SVC_STATE.TIME_RESTRICTED :
                state_str = "TIME_RESTRICTED";
                break;
            case CONST_SVC_STATE.KIDS_RESTRICTION :
                state_str = "KIDS_RESTRICTION";
                break;
        }
        
        return state_str;
    }
    
    return {
        init : _init,

        get serviceState () {
            return currentServiceState;
        },
        get preServiceState () {
            return preServiceState;
        },

        get appState () {
            return appState;
        },
        set appState (newAppState) {
            appState = newAppState;
        },

        get isShowAppStore () {
            return isShowAppStore;
        },
        set isShowAppStore (isShow) {
            isShowAppStore = isShow;
        },

        get isNotifyStopAV () {
            return isNotifyStopAV;
        },
        set isNotifyStopAV (isStop) {
            isNotifyStopAV = isStop;
        },
        // TODO 2017.02.16 dhlee 2.0 R7 로직 추가
        // time_restrict_in_state 변수에 대한 2.0 R7 소스 확인해서 적용해야 함.
        get time_restrict_in_state() {
            return time_restrict_in_state;
        },

        isTVViewingState: _isTVViewingState,
        isVODPlayingState: _isVODPlayingState,
        isOtherAppState: _isOtherAppState,
        isRunningState: _isRunningState,

        getServiceStateString: _getServiceStateString,
        setServiceState: _setServiceState,
        isChangeSameState: _isChangeSameState,
        
        addServiceStateListener: _addServiceStateListener,
        removeServiceStateListener: _removeServiceStateListener
    };
}());
