"use strict";
/**
 * @fileoverview 시청 예약을 관리하는 모듈;
 * @author ggobugi
 * @version 1.2
 *
 * @description
 *      - 2016.11.21 웹 3.0 용으로 작성중 시리즈 예약 기능이 추가됨
 *
 * <p>Copyright (c) 1997-2015 Alticast, Inc. All rights reserved.
 */
KTW.managers.service.ReservationManager = (function(){
    //alias
    var log = KTW.utils.Log;
    var storageManager = KTW.managers.StorageManager;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    //var util = KTW.utils.util;

    var MAX_RESERVATION_SHORT = 30;	// 시리즈 예약 지원하지 않는 경우 단편 최대 예약가능 프로그램 갯수
    var MAX_RESERVATION_SHORT_SUPPORT_SERIES = 20;     // 시리즈 예약 지원하는 경우 최대 단편 예약 가능 프로그램 갯수
    var MAX_RESERVATION_SERIES = 20;    // 최대 시리즈 예약 가능 갯수

    var MASHUP_TIMEOUT = 5 * 1000;  // 매시업 관련 postMessage 호출시 timeout

    /**
     * videoBroadcastObject
     * 프로그램 예약/취소/목록조회 등의 동작을 담당하는 주체
     */
    var vbo = null;

    /**
     * 중복예약 알림팝업 관련
     * popup id
     * 콜백함수
     * 프로그램 객체
     */
    var limitPopupId = "reservationLimitPopup";
    var duplicationPopupId = "reservationDuplicationPopup";
    var reservationOptionPopupId = "reservationOptionsPopup";
    var callbackFunction = null;
    var is_Show_Reservation_Alarm_Popup = false;
    /** 2016.11.29 시리즈 예약 지원 여부 */
    var is_support_series = undefined;
    /** 2016.11.29 dhlee 시리즈 예약 지원 여부 조회 timer */
    var check_support_series_timer = null;

    /** 2016.12.12 jjh1117 시청예약 팝업 중복 처리를 위한 Id 생성 */
    var alarm_id_offset = 0;

    var featur_timer_id = null;

    /**
     * 홈포탈 부팅 시 최초 1회 호출
     * reservation program list update
     */
    function _init() {
        log.printDbg("_init()");
        //decideSupportingSeries();
        getVBO();
    }

    /**
     * 시리즈 예약 지원 여부 확인
     */
    function _decideSupportingSeries() {
        log.printDbg("decideSupportingSeries(), is_support_series = " + is_support_series + ", KTW.CONSTANT.SERIES_RESERVATION : " + KTW.CONSTANT.SERIES_RESERVATION);

        if (is_support_series === undefined && KTW.CONSTANT.SERIES_RESERVATION) {
            var message = {
                "method": KTW.managers.MessageManager.METHOD.MASHUP.IS_SUPPORT_SERIES,
                "to": KTW.CONSTANT.APP_ID.MASHUP,
                "from": KTW.CONSTANT.APP_ID.HOME
            };
            check_support_series_timer = setTimeout(function() {
                if (is_support_series === undefined) {
                    is_support_series = false;
                }
            }, MASHUP_TIMEOUT);
            KTW.managers.MessageManager.sendMessage(message, function (msg) {
                if (msg && msg.res === "Y") {
                    is_support_series = true;
                }
                else {
                    is_support_series = false;
                }
            });
        }
    }

    /**
     * 메쉬업 앱으로부터 전송 받은 시리즈 예약 지원 여부 메시지 처리
     *
     * @param message
     */
    function _processMashupSeries(message) {
        log.printDbg("_processMashupSeries(), message = " + JSON.stringify(message));
        clearTimeout(check_support_series_timer);
        if (!message) {
            is_support_series = false;
        } else if (!KTW.CONSTANT.SERIES_RESERVATION) {
            is_support_series = false;
        } else {
            if (message.res && message.res === "Y") {
                is_support_series = true;
            } else {
                is_support_series = false;
            }
        }
        log.printDbg("_processMashupSeries(), is_support_series = " + is_support_series);
    }

    /**
     * VideoBroadcastObject 획득
     *
     * @private
     */
    function getVBO() {
        try {
            var channelControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN, false);
            vbo = channelControl.vbo;
            if (vbo != null) {
                vbo.addEventListener("Reminder", _openRemindAlarm);
                oipfObjectFactory.createConfigurationObject().localSystem.reminderTime = 12;
            }
        }
        catch (e) {
            log.printErr(e);
        }
    }

    /**
     * 단편 예약 목록 조회
     *
     * OTS: 단편 예약 기능만 제공
     * OTV: 시리즈 예약 기능도 제공하므로 예약 목록에서 appData != null 인 항목은 제외해야 한다.
     *
     * @returns {*}
     */
    function _getRevList() {
        log.printDbg("getRevList()");

        if (vbo == null) {
            getVBO();
        }

        var reserved_list = [];
        var list = null;

        if (vbo != null) {
            list = vbo.reminders;
        }

        if (list === null) {
            return reserved_list;
        }


        //[sw.nam] 2017.04.04 수정 - OTS, OTV 모두 단편 예약 목록은 array 형태로 return 하도록 수정 ->
        //_getRevListBySortingOption() 내부에서 정렬 시 오브젝트와 array 두가지 형태를 구분할 필요가 없도록 하기 위해
        //       if (KTW.CONSTANT.SERIES_RESERVATION) {
        for (var i = 0; i < list.length; i++) {
            if (!(list[i].appData)) {
                reserved_list.push(list[i]);
            }
        }
        /*        } else {
         reserved_list = list;
         }*/
        // 번호순 시작순 시간 순

        return reserved_list;
    }

    /**
     * [sw.nam] 2017.04.03
     *  단편 예약목록을 정렬 옵션에 따라 조회
     *
     *  @param
     *       - sortingOptions :
     *               default          -> 예약된 순서에 따른 정렬(기본 default 옵션, 옵션을 주지 않는 경우)
     *              "startingTime"    -> 예약된 방송의 시작 시간 순서
     *              "programName"     -> 예약된 방송 이름 순서
     *              "channelName"     -> 예약된 방송 채널번호 순서
     *       - orderBy:
     *             "asc"  : 오름차순
     *             "desc" : 내림차순
     *
     *   //TODO Parameter 정의가 사용자 입장에서 not properly determined 이므로.. 재 정리 필요함..
     */
    function _getRevListBySortingOption(sortingOption,orderBy) {
        log.printDbg("_getRevListBySortingOption()");
        var reservedList = _getRevList();
        var reservedListLength = null;

        if(reservedList){
            reservedListLength = reservedList.length;
            if(reservedListLength < 2)
                return reservedList;
        }
        log.printDbg("reservedListLength ::"+reservedListLength);
        switch(sortingOption) {

            case "startingTime":
                switch(orderBy) {
                    case "desc" :
                        reservedList.sort(function(a, b) {
                            return b.startTime - a.startTime;
                        });
                        break;
                    case "asc":
                    default:
                        reservedList.sort(function(a, b) {
                            return a.startTime - b.startTime;
                        });
                        break;
                }
                break;
            case "programName" :
                switch(orderBy) {
                    case "desc" :
                        reservedList.sort(function(a, b) {
                            return a.name > b.name ? -1 : a.name < b.name ? 1 : 0;
                        });
                        break;
                    case "asc" :
                    default:
                        reservedList.sort(function(a, b) {
                            return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
                        });
                        break;
                }
                break;
            case "channelNum" :
                switch(orderBy) {
                    case "desc" :
                        reservedList.sort(function(a, b) {
                            return b.channel.majorChannel - a.channel.majorChannel;
                        });
                        break;
                    case "asc":
                    default:
                        reservedList.sort(function(a, b) {
                            return a.channel.majorChannel - b.channel.majorChannel;
                        });
                        break;
                }
                break;
        }
        return reservedList;

    }

    /**
     * 시리즈 예약 목록을 조회 & 반환
     * - 시리즈의 마지막 회차가 끝나서 시리즈 예약이 없어지면 체크할 방법이 없기 때문에, 항상 매시업매니저에 postMessage 를 보내서 조회한다.
     *
     * @param - options - preventKey : Loading 호출 & key prevent 설정
     *                   - callback : 조회가 끝나고 호출될 callback, list 가 parameter 로 넘어간다.
     */
    function _getSeriesRevList (options) {
        log.printDbg("_getSeriesRevList()");

        var req_msg = {
            "method": KTW.managers.MessageManager.METHOD.MASHUP.GET_SERIES_RESERVATION_LIST,
            "to": KTW.CONSTANT.APP_ID.MASHUP,
            "from": KTW.CONSTANT.APP_ID.HOME
        };

        var isTimeout = false;

        KTW.ui.LayerManager.startLoading({
            preventKey: options.preventKey,
            timeout: MASHUP_TIMEOUT,
            cbTimeout: function () {
                if (!isTimeout) {
                    isTimeout = true;
                    if (options.callback) {
                        options.callback();
                    }
                }
            }
        });

        KTW.managers.MessageManager.sendMessage(req_msg, function (res_msg) {
            if (!isTimeout) {
                isTimeout = true;
                KTW.ui.LayerManager.stopLoading();

                if (options.callback) {
                    options.callback(res_msg.list);
                }
            }
        });
    }

    /**
     * 2016.11.04 dhlee
     *
     * 프로그램이 예약되어 있는지를 확인한다.
     * 단편/시리즈 예약을 지원한다.
     *
     * @param program
     * @returns KTW.managers.service.ReservationManager.RESERVED_RESULT
     *          0: 예약되어 있지 않음
     *          1: 단편 예약 되어 있음
     *          2: 시리즈 예약 되어 있음
     */
    function _isReserved(program) {
        var reserved_result = KTW.managers.service.ReservationManager.RESERVED_RESULT;

        if (program == null) {
            return reserved_result.NOT_RESERVED;
        }
        log.printDbg("_isReserved(), program = " + JSON.stringify(program));
        var reserved = reserved_result.NOT_RESERVED;
        /*
         // OTS 에서도 hasReminder property 가 유효하다는 회신을 받음으로 인해 코드 변경
         // 2016.11.04 dhlee
         // OTS 에서는 시리즈 예약을 지원하지 않는다.
         // 일단 기존 로직대로 list에서 확인하는 방법대로 처리한다.
         if (KTW.CONSTANT.IS_OTS) {
         var revList = _getRevList();
         for (var i = 0; i < revList.length; i++) {
         if (revList[i].programmeID != null && revList[i].programmeID === program.programmeID) {
         reserved = reserved_result.RESERVED_SHORT;
         break;
         }
         }
         } else {
         // 2016.11.04 dhlee
         // hasReminder property가 true인 경우 예약 프로그램임을 의미한다.
         // 이 중에서 appData (메쉬업에서 저장한 transaction id 값임) 에 정보가 있는 경우 series 예약이다.
         if (program.hasReminder === true) {
         if (!program.appData) {
         reserved = reserved_result.RESERVED_SHORT;
         } else {
         reserved = reserved_result.RESERVED_SERIES;
         }
         }
         }*/
        if (program.hasReminder === true) {
            if (KTW.CONSTANT.IS_OTS) {
                reserved = reserved_result.RESERVED_SHORT;
            } else {
                if (!program.appData) {
                    reserved = reserved_result.RESERVED_SHORT;
                } else {
                    reserved = reserved_result.RESERVED_SERIES;
                }
            }
        }

        return reserved;
    }

    /**
     * 2016.11.17 dhlee
     * 예약 항목 추가 (이미 예약 되어 있는 항목의 경우 예약 취소)
     *
     * @param program 추가하려는 프로그램
     * @param hasMsg 메시지를 표시할 지 여부
     * @param callback 동작을 수행한 뒤 호출할 callback function
     *
     * 각 동작에 따라 아래 결과값을 callback 함수에 parameter로 전달
     * @return KTW.managers.service.ReservationManager.ADD_RESULT
     *          -1 - 예약 실패
     * 			0 - 추가 되지 않음
     * 		   	1 - 단편 예약 추가 됨
     * 		   	2 - 단편 예약 취소 됨
     * 			3 - 중복 프로그램 예약취소 후 해당 프로그램 예약
     *          4 - 예약프로그램 갯수 제한 팝업 노출 후 방속 예약 목록으로 이동
     *          5 - 시리즈 예약 추가 됨
     *          6 - 시리즈 예약 취소 됨
     */
    function _add(program, hasMsg, callback) {
        log.printDbg("_add(), program = " + log.stringify(program) + ", hasMsg = " + hasMsg );

        // program 정보 체크
        if (program === null || program === undefined || program.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL) {
            log.printDbg("_add(), Can't reserve, invalid program");
            if (hasMsg) {
                showSimpleMessage("예약할 수 없는 프로그램입니다", 3000);
            }
            if (callback) {
                callback(KTW.managers.service.ReservationManager.ADD_RESULT.FAIL);
            }

            return;
        }

        var now = new Date().getTime();
        var unit = 1000;
        if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
            now = Math.floor(now / 1000);
            unit = 1;
        }

        // 프로그램 시작 시간 체크
        if (now >= (program.startTime - (60 * unit))) {
            log.printDbg("_add(), Can't reserve, the remain time is less than 1 min or already started");
            if (hasMsg) {
                showSimpleMessage("예약할 수 없는 프로그램입니다", 3000);
            }
            if (callback) {
                callback(KTW.managers.service.ReservationManager.ADD_RESULT.FAIL);
            }

            return;
        }

        // 중복/취소 여부 체크 (이미 추가되어 있는 경우면 예약 취소)
        var isReserved = _isReserved(program);
        if (isReserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SHORT) {
            // 단편 예약 상태 - 단편 예약 삭제 TODO 이 로직이 맞는지 확인 필요함.
            _removeShort(program, true);
            if (callback != null) {
                callback(KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_CANCEL);
            }
        } else if (isReserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
            _removeSeries(program.appData, true, callback); // TODO 추후 확인
        } else {
            // 예약 항목 추가
            if (is_support_series) {
                reqSeriesInfo(program, function(res_msg) {
                    if (checkMashupMessage(res_msg, KTW.managers.MessageManager.METHOD.MASHUP.REQ_SERIES_INFO)) {
                        if (res_msg.res === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.RES.SERIES_N_VOD_N) {
                            addShort(program, hasMsg, callback);
                        } else {
                            showReservationOptionPopup(program, hasMsg, res_msg, callback);
                        }
                    }
                });
            } else {
                addShort(program, hasMsg, callback);
            }
        }
    }

    /**
     * 단편 프로그램 예약 추가
     *
     * @param program   예약하고자 하는 프로그램
     * @param hasMsg
     * @param callback
     */
    function addShort(program, hasMsg, callback) {
        log.printDbg("addShort()");

        var result = KTW.managers.service.ReservationManager.ADD_RESULT.NONE;
        var msg = null;
        callbackFunction = callback;    // TODO 2016.11.18 dhlee 여기서만 callbackFunction 에 set 하는 이유를 알아야 한다. (시리즈는?)
        var revList = _getRevList();
        var revListLength = revList.length;

        if (revListLength === 0) {
            if (hasMsg) {
                msg = "시청 예약되었습니다";
                result = KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_RESERVE;
            }
            vbo.setReminder(program);
        } else {
            // 중복 프로그램 체크
            var isDuplicated = false;
            for (var i = 0; i < revListLength; i++) {
                var difTime = program.startTime - revList[i].startTime;
                if (difTime <= 60 && difTime >= -60) {
                    isDuplicated = true;
                    break;
                }
            }
            if (isDuplicated === true) {
                showDuplicateNoti(program, revList[i]);
                return;
            }

            // 최대 예약 개수 초과 여부 체크
            var maxNum = is_support_series ? MAX_RESERVATION_SHORT_SUPPORT_SERIES : MAX_RESERVATION_SHORT;
            if (revListLength >= maxNum) {
                _showLimitPopup({
                    isSeries: false,
                    maxLength: maxNum
                });
                return;
            }

            // 단편 예약
            if (hasMsg) {
                msg = "시청 예약되었습니다";
                result = KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_RESERVE;
            }
            vbo.setReminder(program);
        }

        if (msg != null) {
            showSimpleMessage(msg, 3000);
        }

        if (callback != null) {
            callback(result);
        }
    }

    /**
     * 시리즈 예약 추가
     *
     * @param transid 시리즈 예약에 필요한 유니크한 ID
     * @param hasMsg 메시지를 표시할 지 여부
     * @param callback 동작을 수행한 뒤 호출할 callback function
     *
     * 각 동작에 따라 아래 결과값을 callback 함수에 parameter로 전달
     * @return OTW.managers.service.ReservationManager.ADD_RESULT
     *            -1 - 예약 실패
     *            0 - 추가 되지 않음
     *            1 - 단편 예약 추가 됨
     *            2 - 단편 예약 취소 됨
     *            3 - 중복 프로그램 예약취소 후 해당 프로그램 예약
     *            4 - 예약프로그램 갯수 제한 팝업 노출 후 방송 예약 목록으로 이동
     */
    function _addSeries(transid, hasMsg, callback) {
        log.printDbg("addSeries(), transid = " + transid);

        if (transid == null) {
            log.printDbg("addSeries(), transid is null");
            showSimpleMessage("예약 할 수 없는 프로그램입니다", 3000);
            if (callback != null) {
                callback(KTW.managers.service.ReservationManager.ADD_RESULT.FAIL);
            }
            return;
        }

        // 시리즈 예약
        var req_msg = {
            "method": KTW.managers.MessageManager.METHOD.MASHUP.SET_SERIES,
            "to": KTW.CONSTANT.APP_ID.MASHUP,
            "from": KTW.CONSTANT.APP_ID.HOME,
            "transid": transid
        };

        var isTimeout = false;
        KTW.ui.LayerManager.startLoading({
            preventKey: true,
            timeout: MASHUP_TIMEOUT,
            cbTimeout: function () {
                if (!isTimeout) {
                    isTimeout = true;

                    showSimpleMessage("서비스가 일시적으로 원활하지 않습니다", 3000);

                    if (callback) {
                        callback();
                    }
                }
            }
        });
        KTW.managers.MessageManager.sendMessage(req_msg, function (res_msg) {
            if (!isTimeout) {
                isTimeout = true;

                KTW.ui.LayerManager.stopLoading();
                if (checkMashupMessage(res_msg, KTW.managers.MessageManager.METHOD.MASHUP.SET_SERIES)) {
                    if (hasMsg) {
                        showSimpleMessage("시리즈 예약되었습니다", 3000);
                    }
                    if (callback != null) {
                        callback(KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_SERIES_RESERVE);
                    }
                }
            }
        });
    }

    /**
     * 무비 초이스 본편 채널 전환 타이머 설정
     * (무비 초이스 예고 프로그램 진행 중 구매 시 본편 시작 시간에 맞춰 채널 이동해야 함)
     * @param prog 본편 프로그램
     * @returns {boolean} 타이머 설정 여부, true : 타이머 설정 됨 / false : 타이머 설정 안됨
     * @private
     */
    function _addFeaturePresentation(prog) {
        log.printDbg("called _addFeaturePresentation() - prog : " + prog);

        //파라메터 체크
        if(!prog) {
            log.printDbg("_addFeaturePresentation() - return false, invaild argument");
            return false;
        }

        // OTS 체크
        if(!KTW.CONSTANT.IS_OTS) {
            log.printDbg("_addFeaturePresentation() - return false, OTV");
            return false;
        }

        // 동일 프로그램 Timer 여부 체크
        if(featur_timer_id && featur_timer_id[prog.programmeID]) {
            log.printDbg("_addFeaturePresentation() - return false, exist equal program");
            return false;
        }

        //시작 시간 체크
        var now = new Date().getTime();
        var unit = 1;
        if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC) {
            now = Math.floor(now / 1000);
            unit = 1000;
        }
        var set_time = prog.startTime - now;
        log.printDbg("_addFeaturePresentation() - set_time : " + set_time);
        if(set_time <= 0) {
            log.printDbg("_addFeaturePresentation() - return false, invaild set_time");
            return false;
        }

        //타이머 설정
        var id = setTimeout(function () {
            log.printDbg("_addFeaturePresentation() - Time out - serviceState : " + KTW.managers.service.StateManager.serviceState);
            if(KTW.managers.service.StateManager.serviceState != KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                if(checkProgramBeforeTune(prog)) {
                    if (!KTW.managers.service.StateManager.isTVViewingState()) {
                        KTW.managers.service.AppServiceManager.changeService({
                            nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING,
                            obj: {
                                channel: prog.channel,
                                tune: "true"
                            },
                            tuneForced: true
                        });
                    }
                    else {
                        navAdapter.changeChannel(prog.channel);
                    }
                }
            }

            if(!featur_timer_id && !featur_timer_id[prog.programmeID]) {
                clearTimeout(featur_timer_id[prog.programmeID]);
                delete featur_timer_id[prog.programmeID];
            }
        }, set_time * unit);

        //타이머 아이디 SET
        if (!featur_timer_id) {
            featur_timer_id = {};
        }
        featur_timer_id[prog.programmeID] = id;
        return true;
    }

    /***
     * 채널 Tune 하기 전 Program을 체크한다.
     * 2017.04.17 dhlee
     * 키즈 모드인 경우 키즈 모드용 채널링을 이용하여 체크한다.
     * 키즈 모드인 경우 PR이 높으면 예약 알림을 보여주지 않는다.
     *
     * @param prog
     * @returns {boolean}
     */
    function checkProgramBeforeTune (prog) {
        log.printDbg("called checkProgramBeforeTune() - prog : " + prog);

        // program check
        if(prog === undefined || prog === null) {
            log.printDbg("checkProgramBeforeTune() - return false, invalid prog");
            return false;
        }

        // serviceState check
        if (KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED
            || KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
            log.printDbg("checkProgramBeforeTune() - return false, invalid serviceState");
            return false;
        }

        var kt_channel_list = null;
        var skylife_channel_list = null;
        if (KTW.managers.service.KidsModeManager.isKidsMode()) {
            if (KTW.utils.epgUtil.isAgeLimitProgram(prog)) {
                log.printInfo("checkProgramBeforeTune() - kids_mode, check P.R limit program");
                return false;
            }
            kt_channel_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_KIDS_MODE);
            skylife_channel_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_MODE);
        } else {
            kt_channel_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_VIDEO);
            skylife_channel_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SURFABLE);
        }

        //2017.08.31 sw.nam
        //예약된 프로그램의 채널 정보가 존재하지 않는 경우 예약알림팝업을 띄우지 않는다.
        if(!prog.channel) {
            return false;
        }

        for (var i = 0; kt_channel_list && i < kt_channel_list.length; i++) {
            if (prog.channel.ccid === kt_channel_list[i].ccid) {
                return true;
            }
        }
        if (KTW.CONSTANT.IS_OTS) {
            for (var i = 0; skylife_channel_list && i < skylife_channel_list.length; i++) {
                if (prog.channel.ccid === skylife_channel_list[i].ccid) {
                    return true;
                }
            }
        }

        /*
        //channel check
        var is_vaild_ch = false;
        var kt_channel_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_VIDEO);
        for (var i = 0; kt_channel_list && i < kt_channel_list.length; i++) {
            if (prog.channel.ccid === kt_channel_list[i].ccid) {
                is_vaild_ch = true;
                break;
            }
        }
        if (!is_vaild_ch && KTW.CONSTANT.IS_OTS) {
            var sky_channel_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SURFABLE);
            for (var i = 0; sky_channel_list && i < sky_channel_list.length; i++) {
                if (prog.channel.ccid === sky_channel_list[i].ccid) {
                    is_vaild_ch = true;
                    break;
                }
            }
        }
        if (!is_vaild_ch) {
            log.printInfo("checkProgramBeforeTune() - return false, invalid channel");
            return false;
        }
        */

        return false;
    }

    /**
     * 시리즈/"지난 VOD" 조회 (OTV에서만 사용한다.)
     *
     * @param program
     * @param callback
     * @returns {boolean}
     */
    function reqSeriesInfo(program, callback) {
        log.printDbg("reqSeriesInfo()");

        var message = {
            "method": KTW.managers.MessageManager.METHOD.MASHUP.REQ_SERIES_INFO,
            "to": KTW.CONSTANT.APP_ID.MASHUP,
            "from": KTW.CONSTANT.APP_ID.HOME,
            "sid": program.channel.sid,
            "stime": program.startTime,
            "duration": program.duration,
            "name": program.name
        };

        var isTimeout = false;
        KTW.ui.LayerManager.startLoading({
            preventKey: true,
            timeout: MASHUP_TIMEOUT,
            cbTimeout: function () {
                if (!isTimeout) {
                    isTimeout = true;

                    if (callback) {
                        callback();
                    }
                }
            }
        });
        KTW.managers.MessageManager.sendMessage(message, function (res_msg) {
            if (!isTimeout) {
                isTimeout = true;

                if (callback) {
                    callback(res_msg);
                }
            }
        });
    }

    /**
     * 메세지 유효 체크.
     * @param res_msg response message from Mashup.
     * @param req_method 요청한 method.
     * @returns {boolean} 메세지 유효 여부. true : 유효한 메세지, false : 유효하지 않는 메세지.
     */
    function checkMashupMessage(res_msg, req_method) {
        log.printDbg("checkMashupMessage() - res_msg : " + res_msg);

        var noti = "서비스가 일시적으로 원활하지 않습니다";
        var is_valid_msg = true;

        KTW.ui.LayerManager.stopLoading();

        if (res_msg === undefined || res_msg === null) {//메세지 체크
            log.printDbg("checkMashupMessage() - invalid res_msg");
            is_valid_msg = false;
        } else if(res_msg.method !== req_method) { //요청한 메소드와 일치한지 체크
            log.printDbg("checkMashupMessage() - not equal method.");
            is_valid_msg = false;
        } else if(res_msg.errcode !== KTW.managers.service.ReservationManager.MASHUP_RESPONSE.ERRCODE.NO_ERROR) {//에러 체크
            var errcode = res_msg.errcode;
            log.printDbg("checkMashupMessage() - error res_msg, error code : " + errcode);
            is_valid_msg = false;
            if (errcode === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.ERRCODE.ERROR_OVER_BOOKING) {
                _showLimitPopup({
                    isSeries: true,
                    maxLength: MAX_RESERVATION_SERIES
                });
                return;
            } else if (errcode === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.ERRCODE.ERROR_SERVICE_MISMATCH ||
                errcode === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.ERRCODE.ERROR_TRANSID_MISMATCH ||
                errcode === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.ERRCODE.ERROR_PROGRAM_NOT_FOUND) {
                noti = "예약 할 수 없는 프로그램입니다";
            }
        }

        log.printDbg("checkMashupMessage() - is_valid_msg : " + is_valid_msg);

        if (is_valid_msg === false) {
            showSimpleMessage(noti, 3000);
            // TODO 2016.11.18 dhlee
            // callbackFunction 이 여기서 불리는 이유가 있을까? 2.0 R7 코드 기준으로 callbackFunction 을 set 하는 곳은
            if (callbackFunction != null) {
                callbackFunction(KTW.managers.service.ReservationManager.ADD_RESULT.FAIL);
            }
        }

        return is_valid_msg;
    }

    /**
     * 시리즈 시청 예약 옵션 팝업 표시 (단편 예약/시리즈 예약/지난 VOD 보기(option)
     *
     * @param programme
     * @param hasMsg
     * @param resMsg
     * @param callback
     */
    function showReservationOptionPopup(programme, hasMsg, resMsg, callback) {
        log.printDbg("showReservationOptionPopup(), resMsg = " + resMsg);

        var isCalled = false;
        var popupData = {
            message: "",
            arrButton: [{
                id: "short", name: "이번만 예약",
                program_name : programme.name,
                message: " 이번 회만 예약합니다"
            }],

            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;

                    if (buttonId) {
                        KTW.ui.LayerManager.deactivateLayer({
                            id: reservationOptionPopupId,
                            remove: true
                        });

                        if (buttonId === "short") {
                            addShort(programme, hasMsg, callback);
                        }
                        else if (buttonId === "series") {
                            _addSeries(resMsg.transid, hasMsg, callback);
                        }
                        else if (buttonId === "vod") {
                            // resMsg.cid 가 카테고리 ID 임
                            // VOD Module을 찾아서 API를 호출한다
                            KTW.managers.module.ModuleManager.getModuleForced(
                                KTW.managers.module.Module.ID.MODULE_VOD,
                                function(vodModule) {
                                    log.printDbg("showReservationOptionPopup$getModuleForced()");
                                    if (vodModule) {
                                        vodModule.execute({
                                            method: "showDetail",
                                            params: {
                                                cat_id: resMsg.cid,
                                                req_cd: null    // TODO 2017.02.01 dhlee 추후 확인할 것. (2.0에서도 정의된 값이 없지만 이런 경우 default 값을 사용한다고 함)
                                            }
                                        });
                                    } else {
                                        // TODO 2017.01.12 something error....how to?
                                    }
                                }
                            );
                        }
                    }
                }
            }
        };

        if (resMsg.res[0] === "1") {
            popupData.arrButton.push({
                id: "series", name: "시리즈 예약",
                program_name : programme.name,
                message: " 시리즈 예약합니다"
            });
        }

        if (resMsg.res[1] === "1") {
            popupData.arrButton.push({
                id: "vod", name: "지난 VOD 보기",
                program_name : programme.name,
                message: " 지난 회차 VOD 이용이 가능합니다"
            });
        }

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: reservationOptionPopupId,
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.SimpleMessageButtonPopup,
                params: {
                    data : popupData
                }
            },
            visible: true
        });
    }

    /**
     * 예약프로그램 갯수 제한 알림팝업 표시
     */
    function _showLimitPopup(options) {

        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["알림"],
                cssObj: {}
            }],
            arrButton: [{id: "ok", name: "바로가기"}, {id: "cancel", name: "취소"}],
            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;
                    _callbackLimitPopup(buttonId);
                }
            }
        };

        if (options.isSeries) {
            popupData.arrMessage.push({
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                message: ["시리즈 시청 예약은 최대 20개까지 가능합니다"],
                cssObj: {}
            });
            popupData.arrMessage.push({
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_32,
                message: ["시청 예약을 원하시면 이전 시리즈 예약 목록", "삭제 후 예약해 주세요"],
                cssObj: {}
            });
        }
        else {
            if (is_support_series) {
                popupData.arrMessage.push({
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                    message: ["단편 시청 예약은 최대 20개까지 가능합니다"],
                    cssObj: {}
                });
                popupData.arrMessage.push({
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_32,
                    message: ["시청 예약을 원하시면 이전 단편 예약 목록", "삭제 후 예약해 주세요"],
                    cssObj: {}
                });
            }
            else {
                popupData.arrMessage.push({
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                    message: ["시청 예약은 최대 30개까지 가능합니다"],
                    cssObj: {}
                });
                popupData.arrMessage.push({
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_32,
                    message: ["시청 예약을 원하시면", "이전 목록 삭제 후 예약해 주세요"],
                    cssObj: {}
                });
            }
        }

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: limitPopupId,
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true,
            cbActivate: function () {}
        });
    }

    /**
     * 예약프로그램 갯수 제한 알림팝업 callback function
     */
    function _callbackLimitPopup(buttonId) {
        if (buttonId) {
            KTW.ui.LayerManager.deactivateLayer({
                id: limitPopupId,
                remove: true
            });
        }

        var result = buttonId === "ok" ? KTW.managers.service.ReservationManager.ADD_RESULT.LIMIT : KTW.managers.service.ReservationManager.ADD_RESULT.NONE;

        if (callbackFunction != null) {
            callbackFunction(result);
        }

        if (buttonId === "ok") {
            // 채널 가이드 - 방송 예약 목록 메뉴로 이동
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.CHANNEL_GUIDE,
                    type: KTW.ui.Layer.TYPE.NORMAL,
                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                    params: {
                        menuId: KTW.managers.data.MenuDataManager.MENU_ID.BROADCAST_RESERVATION_LIST
                    }
                },
                visible: true,
                cbActivate: function() {}
            });
        }
    }

    /**
     * 단편 예약 삭제
     *
     * @param program 프로그램 Object
     * @param hasMsg 메시지가 있는지 여부
     */
    function _removeShort(program, hasMsg){
        if (vbo != null) {
            vbo.removeReminder(program);
        }

        if (KTW.CONSTANT.IS_OTS === true) {
            var list = _getReservAutoList();
            var tmpArr = [];
            for(var i=0; i < list.length; i++){
                if(program.channelID != list[i].channelID || program.startTime != list[i].startTime){
                    tmpArr.push({
                        channelID: list[i].channelID,
                        startTime: list[i].startTime
                    });
                }
            }
            _saveReservAutoList(tmpArr);
        }

        if(hasMsg === true){
            showSimpleMessage("예약 취소되었습니다", 2000);
        }
    }

    /**
     * 시리즈 예약 삭제
     *
     * @param transId
     * @param hasMsg
     * @param callback
     */
    function _removeSeries(transId, hasMsg, callback) {
        log.printDbg("removeSeries(), transId = " + transId);

        if (transId == null) {
            if (hasMsg) {
                showSimpleMessage("예약할 수 없는 프로그램입니다", 3000);
            }
            if (callback != null) {
                callback(KTW.managers.service.ReservationManager.ADD_RESULT.FAIL);
            }
            return;
        }

        var message = {
            "method": KTW.managers.MessageManager.METHOD.MASHUP.DEL_SERIES,
            "to": KTW.CONSTANT.APP_ID.MASHUP,
            "from": KTW.CONSTANT.APP_ID.HOME,
            "transid": transId
        };

        var isTimeout = false;
        KTW.ui.LayerManager.startLoading({
            preventKey: true,
            timeout: MASHUP_TIMEOUT,
            cbTimeout: function () {
                if (!isTimeout) {
                    isTimeout = true;

                    showSimpleMessage("서비스가 일시적으로 원활하지 않습니다", 3000);

                    if (callback) {
                        callback();
                    }
                }
            }
        });
        KTW.managers.MessageManager.sendMessage(message, function (res_msg) {
            if (!isTimeout && checkMashupMessage(res_msg, KTW.managers.MessageManager.METHOD.MASHUP.DEL_SERIES)) {
                isTimeout = true;

                if (hasMsg) {
                    showSimpleMessage("시리즈 예약이 취소되었습니다", 2000);
                }
                if (callback != null) {
                    callback(KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_SERIES_CANCEL);
                }
            }
        });
    }

    /**
     * 예약 중복 알림 팝업 표출
     * @param newProgram 새로 등록된 프로그램 Object
     * @param oldProgram 기존에 있던 프로그램 Object
     */
    function showDuplicateNoti(newProgram, oldProgram){

        var isCalled = false;
        var popupData = {
            programme: oldProgram,
            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;
                    _callbackDuplicateNoti({
                        buttonId: buttonId,
                        newProgramme: newProgram,
                        oldProgramme: oldProgram
                    });
                }
            }
        };

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: duplicationPopupId,
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.ReservationDuplicationPopup,
                params: {
                    data : popupData
                }
            },
            visible: true,
            cbActivate: function () {}
        });
    }

    function _callbackDuplicateNoti (options){
        if (options.buttonId) {
            KTW.ui.LayerManager.deactivateLayer({
                id: duplicationPopupId,
                remove: true
            });
        }

        var result = KTW.managers.service.ReservationManager.ADD_RESULT.NONE;

        // 확인
        if (options.buttonId == "ok") {
            _removeShort(options.oldProgramme);
            vbo.setReminder(options.newProgramme);
            showSimpleMessage("시청 예약되었습니다");
            result = KTW.managers.service.ReservationManager.ADD_RESULT.DUPLICATE_RESERVE;
        }

        if (callbackFunction != null) {
            callbackFunction(result, options.oldProgramme);
        }
    }

    /**
     * 2016.09.07 dhlee
     * simple message 표시 (시청 예약/예약 취소, 예약 불가능 프로그램 등)
     *
     * @param msg
     * @param timeToHide
     * @private
     */
    function showSimpleMessage(msg, timeToHide) {
        log.printDbg("showSimpleMessagePopup()");

        KTW.managers.service.SimpleMessageManager.showMessageTextOnly(msg);
    }

    /**
     * 여기 아래는 container.js 에 있는 것들을
     * 여기로 가져온것 입니다. 일단 이렇게 하는게 더 나을것 같아 옮겼습니다.
     */
    function _openRemindAlarm(evt){
        log.printInfo('_openRemindAlarm()');

        if (checkProgramBeforeTune(evt.programme)) {
            runRemindAlarm(evt.programme);
        }
    }

    /**
     * 예약알림팝업 호출
     * @param message 외부 application이 실행 중인 상태인지 확인하기 위한 message
     * 2016.10.18 dhlee 전달 받는 파라미터는 없음.
     */
    function runRemindAlarm (programme) {
        log.printInfo('runRemindAlarm()');

        var hasAuto = false;
        var list = _getReservAutoList();
        var tmpArray = [];
        for(var i=0; i < list.length; i++){
            if(programme.channelID === list[i].channelID || programme.startTime === list[i].startTime){
                // 자동 전원 켜짐 설정인지 확인
                hasAuto = true;
            }else{
                tmpArray.push({
                    channelID: list[i].channelID,
                    startTime: list[i].startTime
                })
            }
        }
        _saveReservAutoList(tmpArray);

        // stanby mode 처리
        var powerState = KTW.managers.service.StateManager.serviceState;
        if (powerState == KTW.CONSTANT.SERVICE_STATE.STANDBY) {
            // 2017.06.12 dhlee
            // 키즈모드 ON 상태에서는 자동 켜짐 기능을 제공하지 않는다.
            if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                return;
            }
            // 자동 채널 tune 처리
            if(hasAuto === true){
                // showwindow를 띄우기 위해 onlySelect로 호출한다 [KTWHPTZOF-1307]
                navAdapter.changeChannel(navAdapter.getChannelByCCID(programme.channel.ccid), true);
                setTimeout(function(){
                    KTW.oipf.adapters.HWAdapter.setPowerState(KTW.oipf.Def.CONFIG.POWER_MODE.ON);
                }, 1000);
            }
        }
        else {
            // 예약알림팝업 호출
            var ch = programme.channel;

            // 2017.04.17 dhlee
            // 현재 시청중인 채널인 경우 예약 알림 팝업을 보여주지 않는다.
            if (navAdapter.getCurrentChannel().ccid === programme.channel.ccid) {
                return;
            }
            var popupId = KTW.ui.view.popup.ReservationAlarmPopup.id;
            if (is_support_series) {
                alarm_id_offset++;
                popupId = KTW.ui.view.popup.ReservationAlarmPopup.id + alarm_id_offset;
            }

            var popupCount = KTW.utils.util.getLayerCount("#layer_area", "div", KTW.ui.view.popup.ReservationAlarmPopup.id);

            var popup_data = {
                popupid : popupId,
                prog: programme,
                ch: ch,
                callback: _callbackAlarm
            };

            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: popupId,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.REMIND_POPUP + (popupCount*2) ,
                    view : KTW.ui.view.popup.ReservationAlarmPopup,
                    linkage: true,
                    params: {
                        data : popup_data
                    }
                },
                visible: true,
                skipRequestShow: true,
                allPopupDuplicate: true
            });
        }
    }

    function _callbackAlarm(result, ch) {
        if (result === 0) {
            // 바로보기
            if (KTW.managers.service.StateManager.isVODPlayingState() === true) {
                // [dj.son] AppServiceManager 사용하도록 수정
                //KTW.managers.service.StateManager.exitToService( { "channel" : ch, "tune" : "true", "from": "reservation" }, false);
                // KTW.managers.service.AppServiceManager.changeService({
                //     nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING,
                //     obj: {
                //         "channel": ch,
                //         "tune": "true",
                //         "from": "reservation"
                //     }
                // });
                var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                if (vodModule) {
                    var result = vodModule.execute({
                        method: "stopVodWithEndPopup",
                        params: {
                            withPopup : false,
                            notChangeService : false,
                            channelObj: ch
                        }
                    });
                } else {
                    // TODO 2017.01.17 dhlee something error.
                }

            }
            else {
                /**
                 * [dj.son] [WEBIIIHOME-2976] 예약 알림 팝업들을 제외하고 나머지 NORMAL, POPUP Layer clear 처리
                 */
                var arrAlarmPopup = KTW.ui.LayerManager.getLayers(KTW.ui.view.popup.ReservationAlarmPopup.id);
                var arrTargetLayerId = [];
                for (var i = 0; i < arrAlarmPopup.length; i++) {
                    arrTargetLayerId.push(arrAlarmPopup[i].id)
                }
                KTW.ui.LayerManager.clearNormalLayerExceptTarget(arrTargetLayerId);

                navAdapter.changeChannel(ch, true);
            }
        }
    }

    /**
     * OTS 방송예약목록에서 자동켜짐 설정 목록 조회
     */
    function _getReservAutoList(){
        var list = storageManager.ps.load("auto_start_list");
        var result = [];
        if (list != null){
            try {
                result = JSON.parse(list);
            }
            catch (e) {}
        }
        return result;
    }
    /**
     * OTS 방송예약목록에서 자동켜짐 설정 목록 저장
     */
    function _saveReservAutoList(list){
        storageManager.ps.save("auto_start_list", JSON.stringify(list));
    }

    function _showReservationAlarmPopup(){
        is_Show_Reservation_Alarm_Popup = true;
    }

    function _hideReservationAlarmPopup(){
        is_Show_Reservation_Alarm_Popup = false;
    }

    function _isShowReservationAlarmPopup(){
        return is_Show_Reservation_Alarm_Popup;
    }

    /**
     * 시리즈 예약 가능한 프로그램인지 확인한다.
     *
     * @param program
     * @param callback
     * @returns {boolean}
     */
    function _checkSeriesProgram(program, callback) {
        log.printDbg("_checkSeriesProgram()");

        if (program === null || program === undefined || program.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL) {
            if (callback) {
                callback(false, null);
            }
            return;
        }

        if (is_support_series) {
            reqSeriesInfo(program, function (res_msg) {
                var result = true;
                KTW.ui.LayerManager.stopLoading();

                if (!res_msg) {
                    result = false;
                } else if (res_msg.method !== KTW.managers.MessageManager.METHOD.MASHUP.REQ_SERIES_INFO) {
                    result = false;
                } else if (res_msg.errcode !== KTW.managers.service.ReservationManager.MASHUP_RESPONSE.ERRCODE.NO_ERROR) {
                    result = false;
                } else if (res_msg.res === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.RES.SERIES_N_VOD_N ||
                    res_msg.res === KTW.managers.service.ReservationManager.MASHUP_RESPONSE.RES.SERIES_N_VOD_Y) {
                    result = false;
                }

                if (callback) {
                    callback(result, result === true ? res_msg.transid : null);
                }
            });
        } else {
            if (callback) {
                callback(false, null);
            }
        }
    }

    return {
        init: _init,
        decideSupportingSeries: _decideSupportingSeries,
        processMashupSeries: _processMashupSeries,
        getRevList: _getRevList,
        getRevListSortingByOption: _getRevListBySortingOption,
        getSeriesRevList: _getSeriesRevList,
        isReserved: _isReserved,
        add: _add,
        addFeaturePresentation: _addFeaturePresentation,
        //removeCh: _removeCh,
        removeShort: _removeShort,
        removeSeries: _removeSeries,
        getReservAutoList: _getReservAutoList,
        saveReservAutoList: _saveReservAutoList,
        showReservationAlarmPopup: _showReservationAlarmPopup ,
        hideReservationAlarmPopup: _hideReservationAlarmPopup ,
        isShowReservationAlarmPopup : _isShowReservationAlarmPopup,
        checkSeriesProgram: _checkSeriesProgram,
        addSeries: _addSeries
    }
}());

// 예약 상태
Object.defineProperty(KTW.managers.service.ReservationManager, "RESERVED_RESULT", {
    value: {
        NOT_RESERVED: 0,        // 예약 되어 있지 않음
        RESERVED_SHORT: 1,      // 단편 예약 되어 있음
        RESERVED_SERIES: 2      // 시리즈 예약 되어 있음
    },
    writable: false,
    configurable: false
});

Object.defineProperty(KTW.managers.service.ReservationManager, "ADD_RESULT", {
    value: {
        // 프로그램 예약 후 동작에 따른 결과 값
        FAIL : -1,                  // 예약 실패
        NONE : 0,                   // 추가 되지 않음
        SUCCESS_RESERVE : 1,        // 단편 예약 추가 됨
        SUCCESS_CANCEL : 2,         // 단편 예약 취소 됨
        DUPLICATE_RESERVE : 3,      // 중복 프로그램 예약 취소 후 해당 프로그램 예약
        LIMIT : 4,                  // 예약 프로그램 개수 제한 팝업 노출 후 방송 예약 목록으로 이동
        SUCCESS_SERIES_RESERVE: 5,  // 시리즈 예약 추가 됨
        SUCCESS_SERIES_CANCEL: 6    // 시리즈 예약 삭제 됨
    },
    writable: false,
    configurable: false
});

Object.defineProperty(KTW.managers.service.ReservationManager, "MASHUP_RESPONSE", {
    value: {
        //Response error code.
        ERRCODE : {
            NO_ERROR: 200,                      // API 호출이 정상적으로 이루어짐.
            ERROR_ALREADY_RESERVED: 201,    // 이미 시리즈 예약이 됨
            ERROR_SERVICE_MISMATCH: 400,    // 매칭되는 service id가 없음
            ERROR_TRANSID_MISMATCH: 401,    // Transaction ID가 매칭 되지 않음
            ERROR_PROGRAM_NOT_FOUND: 402,   // Programme을 조회 할 수 없음
            ERROR_OVER_BOOKING: 500,        // 예약 초과
            ERROR_UNKNOWN: 501              // 상기 이외의 에러가 발생함
        },
        //시리즈/"지난 VOD" 조회 결과 코드 (0의 자리는 시리즈 가능 여부, 1의 자리는 지난 VOD 가능여부
        RES : {
            SERIES_N_VOD_N: "00",   // 시리즈 없음 & VOD 없음
            SERIES_N_VOD_Y: "01",   // 시리즈 없음 & VOD 있음
            SERIES_Y_VOD_N: "10",   // 시리즈 있음 & VOD 없음
            SERIES_Y_VOD_O: "11"    // 시리즈 있음 & VOD 있음
        }
    },
    writable: false,
    configurable: false
});
