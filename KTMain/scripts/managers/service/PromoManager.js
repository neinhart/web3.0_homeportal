"use strict";

/**
 * [jh.lee] 프로모 트리거를 제어
 */
KTW.managers.service.PromoManager = (function () {
    
    var CONST_APP_MODE = KTW.CONSTANT.APP_MODE;
    var CONST_SVC_STATE = KTW.CONSTANT.SERVICE_STATE;
    
    // [jh.lee] 참조
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var navAdapter = null;
    var appModeManager = null;
    var stateManager = null;
    var layerManager = null;
    var iframeManager = null;

    var channelControl = null;

    // [jh.lee] 트리거 탐색 결과
    var search_result;
    // [jh.lee] 트리거 활성화 상태
    var trigger_visibility = false;
    // [jh.lee] 타이머 변수
    var trigger_timer;
    // [jh.lee] 프로모 트리거 완전 삭제 변수
    var promo_delete = false;
    /** last channel permission **/
    var last_perm = -1;
    
    // [jh.lee] trigger type
    var eed;

    var PROMO_TYPE = [
        {TYPE : "0", REQ_PATH_CD : "P0"},
        {TYPE : "1", REQ_PATH_CD : "P1"},
        {TYPE : "2", REQ_PATH_CD : "P2"},
        {TYPE : "3", REQ_PATH_CD : "P3"},
        {TYPE : "4", REQ_PATH_CD : "P4"},
        {TYPE : "5", REQ_PATH_CD : "P5"},
        {TYPE : "6", REQ_PATH_CD : "P6"},
        {TYPE : "7", REQ_PATH_CD : "P7"},
        {TYPE : "8", REQ_PATH_CD : "P8"},
        {TYPE : "9", REQ_PATH_CD : "P9"},
        {TYPE : "a", REQ_PATH_CD : "PA"}
    ];

    var HIDDEN_PROMO_TYPE = [
        {TYPE : "0", REQ_PATH_CD : "H0"},
        {TYPE : "1", REQ_PATH_CD : "H1"},
        {TYPE : "2", REQ_PATH_CD : "H2"},
        {TYPE : "3", REQ_PATH_CD : "H3"},
        {TYPE : "4", REQ_PATH_CD : "H4"},
        {TYPE : "5", REQ_PATH_CD : "H5"},
        {TYPE : "6", REQ_PATH_CD : "H6"},
        {TYPE : "7", REQ_PATH_CD : "H7"},
        {TYPE : "8", REQ_PATH_CD : "H8"},
        {TYPE : "9", REQ_PATH_CD : "H9"},
        {TYPE : "a", REQ_PATH_CD : "HA"}
    ];

    var current_path = "";
    
    /**
     * [jh.lee] 프로모 매니저 초기화
     */
    function _init() {

        navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        appModeManager = KTW.managers.service.AppModeManager;
        stateManager = KTW.managers.service.StateManager;
        layerManager = KTW.ui.LayerManager;
        iframeManager = KTW.managers.service.IframeManager;
        
        channelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN, false);
        
        stateManager.addServiceStateListener(onServiceStateChange);
        //appModeManager.addAppModeListener(onAppModeChange);
        channelControl.addChannelEventListener(onChannelChangeEvent);
    }
    
    /**
     * [jh.lee] 외부에서 시나리오상 showTrigger를 호출할 수 밖에 없는 상황이 존재하기 때문에 requestTrigger를 추가
     * [jh.lee] 여기서는 특수한 경우 이기 때문에 내부함수로 변경된 showTrigger를 호출하기 전에 특정 상황을 체크하기만 한다.
     */
    function _requestTrigger(channel) {
        // 2016.10.17 dhlee 변경된 stateManager를 사용하도혹 수정
        //if(!appModeManager.isFullMode() && stateManager.state !== CONST_SVC_STATE.VOD &&
        //        iframeManager.isShowIframe() !== true) {
        if(!appModeManager.isFullMode() && stateManager.serviceState !== CONST_SVC_STATE.VOD &&
            iframeManager.isShowIframe() !== true) {

            // [jh.lee] App 모드가 fullMode 가 아닌 경우에만 showTrigger 호출
            showTrigger(channel);
        }
    }
    
    /**
     * [jh.lee] 프로모트리거를 화면에서 표시
     */
    function showTrigger(channel) {
        log.printDbg("showTrigger(), channel = " + JSON.stringify(channel));
        var check_result;

        // 2016.10.17 dhlee
        // LayerManager에서 getOtherAppState()를 제공하지 않는다. 해당 정보는 stateManager.isOtherAppState()를 통해 획득.
        //if (_isPromoChannel(channel) === true && !stateManager.isOtherAppState()) {
        if (_isPromoChannel(channel) === true && !stateManager.isOtherAppState()) {
            // [jh.lee] 현재 채널이 프로모 채널인 경우
            // [jh.lee] 현재 상태에서 otherApp이 없는 경우에만
            check_result = checkTrigger();

            if (check_result === null || check_result === undefined || check_result.length === 0) {
                // [jh.lee] 결과가 존재하지 않는 경우
                drawTrigger(false);
                scheduleTriggerTimer();
            } else {
                if (check_result === true) {
                    // [jh.lee] 프로모 트리거가 존재하는 경우
                    drawTrigger(true);
                } else {
                    // [jh.lee] 프로모 트리거가 존재하지 않는 경우
                    drawTrigger(false);
                }
                scheduleTriggerTimer();
            }
        } else {
            // [jh.lee] 현재 채널이 프로모 채널이 아닌 경우
            _stopTrigger();
        }
    }
    
    /**
     * [jh.lee] 프로모트리거를 화면에서 숨김
     */
    function _stopTrigger() {
        descheduleTriggerTimer();
        drawTrigger(false);
    }
    
    /**
     * [jh.lee] 바로보기 버튼 클릭시 실행할 기능 (VOD 상세화면 으로 이동)
     */
    function actTrigger() {
        log.printDbg("actTrigger()");
        var obj = {};
        var local_eed = eed;
        //var req_type = KTW.vod.EntrancePath.GUIDE_CHANNEL_200;
        //var current_channel;
        if (search_result !== null && search_result !== undefined && search_result !== 0) {
            // [jh.lee] search_result 값이 null, undefined, 0 인 경우에는 바로보기 버튼 작동하면 안됨.
            // [jh.lee] 그래서 해당 값이 먼저 있는지 체크함.
            if (local_eed) {
                log.printDbg("actTrigger(), linkType ====== " + local_eed.linkType);
                
                if (local_eed.linkType === "L") {
                    log.printDbg("actTrigger(), locator ====== " + local_eed.locator);
                    //serviceAdapter.startAppByLocator(eed.locator);
                    // [jh.lee] startAppByLocator를 단독으로 호출하지 않고 exitToService를 호출한다.
                    obj.type = KTW.CONSTANT.APP_TYPE.MULTICAST;
                    obj.param = local_eed.locator;
                    // [dj.son] AppServiceManager 사용하도록 수정
                    //stateManager.exitToService(obj);
                    KTW.managers.service.AppServiceManager.changeService({
                        nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                        obj: obj
                    });
                } else if (local_eed.linkType === "V" || local_eed.linkType === "S") {
                    log.printDbg("actTrigger(), vcId = " + local_eed.vcId + ", vaId = " + local_eed.vaId + ", linkType = " + local_eed.linkType);
                    log.printDbg("actTrigger(), current_path = " + current_path);
                    // VOD Module을 찾아서 API를 호출한다
                    KTW.managers.module.ModuleManager.getModuleForced(
                        KTW.managers.module.Module.ID.MODULE_VOD,
                        function(vodModule) {
                            log.printDbg("actTrigger$getModuleForced(), vcId = " + local_eed.vcId + ", vaId = " + local_eed.vaId + ", linkType = " + local_eed.linkType);
                            if (vodModule) {

                                /**
                                 * [dj.son] 비즈 상품인 경우 프로모에서 점프시 catId 를 "N" 으로 설정하도록 수정
                                 */
                                var catId = local_eed.vcId;
                                if (KTW.CONSTANT.IS_BIZ) {
                                    catId = "N";
                                }

                                vodModule.execute({
                                    method: "showDetail",
                                    params: {
                                        cat_id: catId,
                                        const_id: local_eed.vaId,
                                        req_cd: current_path,
                                        callback: function (result) {
                                            log.printDbg("actTrigger()$vodModule.execute(), result = " + result);
                                        }
                                    }
                                });
                                //통계로그 추가
                                KTW.managers.UserLogManager.collect({
                                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                                    act: KTW.data.EntryLog.JUMP.CODE.PROMO_CH,
                                    jumpType: KTW.data.EntryLog.JUMP.TYPE.VOD_DETAIL,
                                    catId: local_eed.vcId,
                                    contsId: local_eed.vaId,
                                    reqPathCd: current_path
                                });
                            } else {
                                // TODO 2017.01.12 something error....how to?
                            }
                        }
                    );
                }
            }
        }
    }
    
    /**
     * [jh.lee] HDS 개통 화면으로 진입시 App Mode None 이 발생하기 때문에 HDS 개통화면에서는 무조건 프로모트리거를 중지하도록 설정
     */
    function _setStop() {
        promo_delete = true;
    }

    /**
     * channel descriptor 정보를 이용하여 promo channel 인지를 판단한다.
     * channel descriptor 정보 획득을 위해서는 channel.getSIDescriptor 를 사용한다.
     * program descriptor 정보 획득을 위해서는 program.getSIDescriptors (oipf 규격에 있음) 를 사용한다.
     *
     * @param channel
     * @returns {boolean}
     */
    function _isPromoChannel(channel) {
        log.printDbg("_isPromoChannel()" + JSON.stringify(channel));

        var promoDescriptor;
        var promoType;

        if (util.isValidVariable(channel) === false) {
            channel = navAdapter.getCurrentChannel();
        }

        if (channel.getSIDescriptor) {
            // 양방향 채널의 경우 getSIDescriptor object가 존재하지 않음.
            promoDescriptor = channel.getSIDescriptor(0x90);
            log.printDbg("_isPromoChannel(), promoDescriptor = " + promoDescriptor);
            // 2017.06.21 dhlee 조건 추가
            if (promoDescriptor && promoDescriptor.length > 0 && promoDescriptor !== 0) {
                // 16진수 문자열로 변환하여 비교 한다.
                promoType = promoDescriptor[0].toString(16);
                log.printDbg("_isPromoChannel(), promoType = " + promoType);
                current_path = "";
                var definedPromoType;
                if (navAdapter.isHiddenPromoChannel(channel)) {
                    definedPromoType = HIDDEN_PROMO_TYPE;
                } else {
                    definedPromoType = PROMO_TYPE;
                }
                /*
                for (var i = 0; i < PROMO_TYPE.length; i++) {
                    if (promoType === PROMO_TYPE[i].TYPE || promoType.toLowerCase() === PROMO_TYPE[i].TYPE) {
                        current_path = PROMO_TYPE[i].REQ_PATH_CD;
                        log.printDbg("_isPromoChannel(), current_path = " + current_path);
                    }
                }
                */
                for (var i = 0; i < definedPromoType.length; i++) {
                    if (promoType === definedPromoType[i].TYPE || promoType.toLowerCase() === definedPromoType[i].TYPE) {
                        current_path = definedPromoType[i].REQ_PATH_CD;
                        log.printDbg("_isPromoChannel(), current_path = " + current_path);
                    }
                }


                return true;
            }
        }

        return false;
    }
    /**
     * [jh.lee] 현재 채널이 프로모 채널인지 체크
     */
    //function _isPromoChannel(channel) {
    //    log.printInfo("_isPromoChannel");
    //
    //    var is_promo_channel = false;
    //
    //    if (util.isValidVariable(channel) === false) {
    //        channel = navAdapter.getCurrentChannel();
    //    }
    //
    //    is_promo_channel = navAdapter.isFocusChannel(channel);
    //
    //    log.printDbg("_isPromoChannel(), is_promo_channel = " + is_promo_channel);
    //
    //    if (is_promo_channel === false) {
    //     // [jh.lee] 프로모 채널 정보를 가져온다.
    //        var promo_channel = getPromoChannel();
    //        if (util.isValidVariable(promo_channel) === true &&
    //                util.isValidVariable(channel) === true) {
    //            if (promo_channel.ccid === channel.ccid) {
    //                is_promo_channel = true;
    //            }
    //        }
    //    }
    //
    //    log.printDbg("PromoManager", "_isPromoChannel(), after check. is_promo_channel = " + is_promo_channel);
    //
    //    return is_promo_channel;
    //}
    
    //function getPromoChannel() {
    //    return navAdapter.getPromoChannel();
    //}
    
    /**
     * [jh.lee] 현재 시점의 프로모 트리거 객체를 생성 하고 체크
     */
    function checkTrigger() {
        log.printInfo("checkTriger()");
        
        var current_time;
        //var end_time;
        //var surfable_channel;
        //var sid;
        var si_desc;
        
        current_time = new Date().getTime();
        //end_time = new Date();
        //end_time.setHours(end_time.getHours() + 1);
        //end_time = end_time.getTime();
        
        // [jh.lee] 한시간 데이터 요청
        // R5 ~ R6 현재 시간 기준 해당하는 프로그램 정보만 요청. 즉 현재시간 ~ 현재시간 + 1.5초 이내에 해당하는 프로그램 정보 (기존에는 1시간 양을 가져왔었음)
        search_result = KTW.oipf.AdapterHandler.programSearchAdapter.getPrograms(
            current_time, current_time + 1500, null, navAdapter.getCurrentChannel());

        //search_result = programAdapter.getPrograms(current_time, current_time + 1500, null, navAdapter.getCurrentChannel());
        
        if (search_result === null || search_result === undefined || search_result.length === 0) {
            // [jh.lee] 받아온 데이터가 존재하지 않는 경우
            log.printDbg("checkTrigger(), No search_result : " + search_result.length);
            return search_result;
        } else {
            // [jh.lee] 받아온 데이터가 존재하는 경우
            si_desc = search_result[0].getSIDescriptors(0x4E);
            eed = util.getExtendedEventDescriptor(si_desc);
            
            if (eed) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    /**
     * [jh.lee] locator를 파싱
     * 2016.08.04 dhlee 사용하지 않는 함수로 보임
     */
    //function parsingLocator(search_result) {
    //    var locator;
    //
    //    locator = search_result.locator.split(".");
    //    return (parseInt(locator[2], 16)).toString;
    //}
    
    /**
     * [jh.lee] 프로모 트리거를 화면에 노출
     */
    function drawTrigger(on_off) {
        log.printDbg("drawTrigger(), on_off = " + on_off + ", trigger_visibility = " + trigger_visibility);

        if (on_off === true && trigger_visibility === false) {
            // [jh.lee] 활성화
            trigger_visibility = true;

            // [dj.son] activateLayer param 수정
            layerManager.activateLayer({
                obj: {
                    id: "promo_trigger",
                    type: KTW.ui.Layer.TYPE.TRIGGER,
                    priority: 200,
                    linkage : true,
                    view : KTW.ui.view.PromoTriggerView,
                    params : {
                        callback : actTrigger
                    }
                },
                visible: true
            });
        } else if (on_off === false && trigger_visibility === true){
            // [jh.lee] 비활성화
            trigger_visibility = false;

            // [dj.son] deactivateLayer param 수정
            layerManager.deactivateLayer({
                id: "promo_trigger"
            });
        }
    }
    
    /**
     * [jh.lee] 타이머 등록
     */
    function scheduleTriggerTimer() {
        var time;
        
        descheduleTriggerTimer();
        
        time = calculateTime();
      
        trigger_timer = setTimeout(showTrigger, time);
    }
    
    /**
     * [jh.lee] 타이머 해제
     */
    function descheduleTriggerTimer() {
        if (trigger_timer) {
            clearTimeout(trigger_timer);
            trigger_timer = null;
        }
    }
    
    /**
     * [jh.lee] 트리거 활성 및 비활성 시간 계산
     */
    function calculateTime() {
        var duration;
        var current;

        if (search_result.length === 0) {
            // [jh.lee] 가져온 데이터가 존재하지 않을 경우 기본적으로 3초 마다 재요청
            // 10초마다 재요청하도록 수정 (ACAP 시나리오 적용: 기존에는 3초 였음)
            duration = 10;
        } else {
            current = (new Date()).getTime();
            current = Math.floor(current / 1000);
            log.printDbg("calculateTime(), current = " + current);
            duration = search_result[0].startTime + search_result[0].duration - current;
            log.printDbg("calculateTime(), search_result[0].startTime : " + search_result[0].startTime);
            log.printDbg("calculateTime(), search_result[0].duration : " + search_result[0].duration);
            log.printDbg("calculateTime(), duration : " + duration);
        }

        return duration * 1000;
    }
    
    /**
     * [jh.lee] state 변경 시 실행될 콜백
     * 2017.04.20 dhlee
     * 기본적으로 trigger의 실행은 channel event를 기준으로 동작되어야 한다.
     * 다만, promo channel 상에서 다른 unbound app 시작/종료 상황일 때는 Service State 변경에 따라 처리하도록 한다.
     */
    function onServiceStateChange(options) {
        log.printDbg("onServiceStateChange(), options.nextServiceState = " + options.nextServiceState + ", options.preServiceState = " + options.preServiceState);
        // 2017.04.20 dhlee
        // prev state 가 APP_ON_TV -> next state 가 TV_VIEWING 으로 변경 시에만 처리한다.
        //if (options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
        if (options.nextServiceState === CONST_SVC_STATE.TV_VIEWING) {
            if (options.preServiceState === CONST_SVC_STATE.OTHER_APP_ON_TV || options.preServiceState === CONST_SVC_STATE.KIDS_RESTRICTION) {
                //[jh.lee] 채널로의 전환이므로 App 모드 검사 후 결정
                //if (!appModeManager.isFullMode() && iframeManager.isShowIframe() !== true) {
                // 2017.07.19 dhlee
                // APP Mode 는 사용하지 않는다. (단, 추후 사용될 수 있으므로 일단 주석 처리한다.)
                if (iframeManager.isShowIframe() !== true) {
                    // [jh.lee] 풀모드 아닌 경우에만 쇼
                    showTrigger();
                }
            }
        } else {
            // [jh.lee] VOD, 시청시간제한 뿐만아니라 대기모드 등의 처리도 해야하므로 TV_VIEWING 상태가 아니면 무조건 stopTrigger 호출
            _stopTrigger();
        }
    }
    
    /**
     * [jh.lee] App 모드 변경 시 실행될 콜백
     * 2017.07.19 dhlee
     * APP MODE 는 더 이상 사용하지 않는다. (단, 추후 사용될 수 있으므로 주석 처리)
     */
    /*
    function onAppModeChange(mode) {
        log.printDbg("onAppModeChange(), mode = " + mode);
        switch(mode) {
            case CONST_APP_MODE.ICON:
            case CONST_APP_MODE.NONE:
                // 2016.10.17 dhlee 변경된 stateManager 의 serviceState를 참조하도록 변경
                //if (stateManager.state !== CONST_SVC_STATE.VOD &&
                //        iframeManager.isShowIframe() !== true &&
                //        promo_delete !== true) {
                if (stateManager.serviceState !== CONST_SVC_STATE.VOD &&
                    iframeManager.isShowIframe() !== true &&
                    promo_delete !== true) {

                    // [jh.lee] 풀모드가 아니고 현재 상태가 VOD 가 아닌 경우에만 쇼
                    showTrigger();
                }
                break;
            case CONST_APP_MODE.FULL:
                // [jh.lee] 풀모드에서는 무조건 하이드
                _stopTrigger();
                break;
        }
    }
    */

    function onChannelChangeEvent(event) {
        log.printDbg("onChannelChangeEvent(), event = " + JSON.stringify(event));

        // 2017.07.19 dhlee
        // 현재 state가 KIDS_RESTRICTION인 경우 stopTrigger를 호출한다.
        if (KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.KIDS_RESTRICTION) {
            _stopTrigger();
            return;
        }
        // [jh.lee] 채널 변경이 일어나는 경우
        //last_perm = event.permission;
        if (event.type === KTW.nav.Def.CHANNEL.EVENT.REQUESTED) {
            if (!appModeManager.isFullMode() && event.permission < 1) {
                // [jh.lee] 풀모드가 아닐 때만 프로모를 쇼
                // [jh.lee] permission 이 1 ~ 4 인 경우는 모두 showTrigger를 막아야한다.
                showTrigger(event.channel);
            }
            /**
             * [jh.lee] 원래 아래 처럼 event.permission 이 1인 경우(제한아이프레임) 프로모트리거 숨김 처리가 추가되야한다.
             * [jh.lee] LG에는 처리되어있지만 웹홈에서는 프로모트리거가 느리게 사라지는 경우 때문에 ChannelControl.js 의
             * [jh.lee] onChannelSelectionEvent 에서 stopTrigger(); 를 호출한다.
             * [jh.lee] 그래서 아래 코드는 추가만 하고 주석처리. 향 후 필요할 시 주석 해제. 테스트시 아래 코드가 없어도 웹홈은 정상적으로 프로트리거가 사라짐
             */
            // 2017.04.20 dhlee
            // 웹 3.0 에서는 channel event를 받았을 때 이미 service state 변경 이벤트를 통해서는 프로모 채널이 제한 채널일 때 정상적으로 동작되지 않고 있다.
            //else if (event.permission === 1) {
            else if (event.permission >= 1) {
                _stopTrigger();
            }

        } else if (event.type === KTW.nav.Def.CHANNEL.EVENT.PERM_UPDATED) {
            if (event.permission > 0) {
                // [jh.lee] permission 이 1 ~ 4 인 경우는 트리거가 보이면 안되기 때문에 stopTrigger 호출
                _stopTrigger();
            } else if (event.permission < 1 && !appModeManager.isFullMode()) {
                // [jh.lee] UPDATED 가 오는 경우에도 appMode 체크한다.
                showTrigger(event.channel);
            }
        }
    }

	return {
	    init : _init,
	    requestTrigger : _requestTrigger,
	    stopTrigger : _stopTrigger,

        isPromoChannel: _isPromoChannel
	};
}());
