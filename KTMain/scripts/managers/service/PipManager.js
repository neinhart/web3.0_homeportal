/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>PipManager</code>
 *
 * @author dj.son
 * @since 2016-10-26
 */

KTW.managers.service.PipManager = (function () {
    var PIP_TYPE = {
        NONE : 0,
        MINI_EPG : 1,       // 미니가이드 인접채널
        MULTI_VIEW : 2,     // 멀티화면(2채널 동시시청)
        RESERVATION : 3,    // 예약알림팝업,
        FULL_EPG: 4,         // 전체 편성표
        FAV_EPG: 5,          // 선호채널 편성표, size & 로직은 전체 편성표와 동일
        GENRE_EPG: 6   // 장르별 채널 편성표, size & position 은 genreChannelViewExtension 에서 가변적으로 설정.
    };
    var BACK_IFRAME_TYPE = {
        CURRENT : 0, // 현재 채널(miniEpg에서 처리하고 여기에선 사용 안함)
        BLOCKED : 1, // 제한 채널
        AGE_LIMIT : 2, // 시청연령 제한
        NOT_SUBSCRIBED : 3, // 미가입 채널
        AUDIO : 4, // 오디오 채널
        SKY_CHOICE : 5, // skyChoice 채널
        CATCH_ON : 6, // 캐치온 채널
        NO_SD : 7, // sd채널 없음
        UHD : 8, // UHD 재생 불가(OTS)
        MASHUP : 9, // 매쉬업(miniEpg에서 처리하고 여기에선 사용 안함)
        NOW_UHD : 10, // 현재 채널이 UHD(OTS)
        PREVIEW : 11, // 미리보기
        WEAK_SIGNAL : 12, // 신호 미약(OTS)
        HDR_VOD : 13, // HDR VOD 재생 시
        ADULT : 14, // 성인 채널 인 경우
        ETC : 15 // 기타
    };
    var PIP_LAYER_ID = "PipLayer";

    var log = KTW.utils.Log;

    var isHDRVOD = false;

    function _isPipLayerShow () {
        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);
        if (layer && layer.isShowing()) {
            return true;
        }

        return false;
    }

    /**
     * activate PipLayer
     * - options.pipType 에 따라 PipLayer 의 type, priority 값을 변경
     * - options 가 그대로 pipLayer 에 param 으로 세팅
     * - options.pipType 이 없거나 NONE 이면 아무것도 안하고 리턴
     *
     * @param options pipType - pip type
     *                 cbHide - PipLayer 가 hide 될때호출될 callback
     */
    function _activatePipLayer (options) {
        log.printDbg("_activatePipLayer()");

        if (!options || options.pipType === null || options.pipType === undefined) {
            log.printDbg("options or options.pipType is null.... so return");
            return;
        }

        log.printDbg("pipType is " + options.pipType);

        if (options.pipType == PIP_TYPE.NONE) {
            log.printDbg("pipType is NONE... so return");
            return;
        }

        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);
        if (layer && layer.isShowing()) {
            if (layer.getType() == options.pipType) {
                // TODO
                log.printDbg("PipLayer is shown... so return");
                return;
            }

            _deactivatePipLayer();
        }

        isHDRVOD = false;

        if (KTW.managers.service.StateManager.isVODPlayingState() === true) {
            KTW.managers.module.ModuleManager.getModuleForced(
                KTW.managers.module.Module.ID.MODULE_VOD,
                function(vodModule) {
                    log.printDbg("KTW.managers.module.ModuleManager.getModuleForced()");
                    if (vodModule) {
                        var tempHdrVod = vodModule.execute({
                            method: "isHDR",
                            params: {
                            }
                        });
                        if(tempHdrVod === undefined || tempHdrVod === null || tempHdrVod === false) {
                            isHDRVOD = false;
                        }else if(tempHdrVod === true) {
                            isHDRVOD = true;
                        }
                        log.printDbg("KTW.managers.module.ModuleManager.getModuleForced() return temHdrVod : " + tempHdrVod + " , isHDRVOD : " + isHDRVOD);
                    } else {
                        // TODO 2017.01.12 something error....how to?
                    }
                }
            );
        }

        options.is_hdr_vod = isHDRVOD;

        var obj = {
            id: PIP_LAYER_ID,
            params: {
                data: options
            }
        };

        switch (options.pipType) {
            case PIP_TYPE.MINI_EPG:
                obj.type = KTW.ui.Layer.TYPE.BACKGROUND;
                obj.priority = KTW.ui.Layer.PRIORITY.NORMAL + 50;
                break;
            case PIP_TYPE.MULTI_VIEW:
                obj.type = KTW.ui.Layer.TYPE.BACKGROUND;
                obj.priority = KTW.ui.Layer.PRIORITY.NORMAL + 50;
                break;
            case PIP_TYPE.RESERVATION:
                obj.type = KTW.ui.Layer.TYPE.BACKGROUND;
                var popupCount = KTW.utils.util.getLayerCount("#layer_area", "div", KTW.ui.view.popup.ReservationAlarmPopup.id);
                if(popupCount>0) {
                    obj.priority = KTW.ui.Layer.PRIORITY.REMIND_POPUP + ((popupCount-1)*2) + 1;

                }else {
                    obj.priority = KTW.ui.Layer.PRIORITY.REMIND_POPUP + 1;
                }
                break;
            case PIP_TYPE.FULL_EPG:
            case PIP_TYPE.FAV_EPG:
            case PIP_TYPE.GENRE_EPG:
                obj.type = KTW.ui.Layer.TYPE.BACKGROUND;
                obj.priority = KTW.ui.Layer.PRIORITY.NORMAL + 50;
                break;
        }

        $("#pipVBOArea").css({"z-index": obj.priority});

        KTW.ui.LayerManager.activateLayer({
            obj: obj,
            visible: true
        });
    }


    function _deactivatePipLayer () {
        log.printDbg("_deactivatePipLayer()");
        KTW.managers.service.ChannelPreviewManager.stop(false);

        KTW.ui.LayerManager.deactivateLayer({
            id: PIP_LAYER_ID,
            remove: true
        });

        isHDRVOD = false;
    }

    function _setPipSize (options) {
        log.printDbg("_setPipSize()");

        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);

        if (layer) {
            layer.setPipSize(options);
        }
    }

    function _showIframe(if_type , if_data) {
        log.printDbg("_showIframe()");

        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);

        if (layer) {
            layer.showIframe({
                type: if_type,
                data: if_data
            });
        }
    }

    function _changeChannel(pip_ch) {
        log.printDbg("_changeChannel() channel = " + JSON.stringify(pip_ch));
        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);

        if (layer) {
            layer.changeChannel(pip_ch);
        }
    }

    function _reDrawSubArea () {
        log.printDbg("_reDrawSubArea()");

        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);

        if (layer) {
            layer.reDrawSubArea();
        }
    }


    function _activateMultiView(callbackFuncHide , callbackFuncSwap) {

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "PipMultiViewPopup",
                type: KTW.ui.Layer.TYPE.NORMAL,
                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                view : KTW.ui.view.popup.PipMultiViewPopup,
                params: {
                    data : {
                        callback_hide : callbackFuncHide,
                        callback_swap : callbackFuncSwap
                    }
                }
            },
            visible: true
        });
    }

    function _deactivateMultiView() {
        KTW.ui.LayerManager.deactivateLayer({
            id: "PipMultiViewPopup",
            remove: true,
            onlyTarget: KTW.managers.service.StateManager.isVODPlayingState()
        });
    }

    function _clearPipTuneTimer() {
        var layer = KTW.ui.LayerManager.getLayer(PIP_LAYER_ID);

        if (layer) {
            layer.clearPipTuneTimer();
        }
    }


    return {
        PIP_TYPE: PIP_TYPE,
        BACK_IFRAME_TYPE: BACK_IFRAME_TYPE,
        PIP_LAYER_ID: PIP_LAYER_ID,

        activatePipLayer: _activatePipLayer,
        deactivatePipLayer: _deactivatePipLayer,
        isPipLayerShow: _isPipLayerShow,
        setPipSize: _setPipSize,
        showIframe : _showIframe,
        changeChannel : _changeChannel,
        reDrawSubArea: _reDrawSubArea,
        activateMultiView : _activateMultiView,
        deactivateMultiView : _deactivateMultiView,
        clearPipTuneTimer : _clearPipTuneTimer
    };
})();