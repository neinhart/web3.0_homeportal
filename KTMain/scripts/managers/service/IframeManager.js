/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>IframeManager</code>
 *  @author jjh1117
 *  @since 2016-11-28
 */

"use strict";

KTW.managers.service.IframeManager = (function () {
    
    var IFRAME_LAYER_ID = "IframeLayer";
    
    var FULL_MODE_IFRAME_NAME = [
        "" , // Iframe 숨김
        "web_audio.jpg" , // 오디오
        "iframe_bg.jpg", // 시청연령 제한
        "iframe_bg_kids.jpg", // 시청연령 제한 키즈모드
        "", // 시청시간 제한
        "iframe_bg.jpg", // 미가입
        "iframe_bg.jpg", // 시청채널 제한
        "web_vod_loading.jpg", // Default VOD 로딩
        "web_vod_loading.jpg", // VOD 로딩
        "web_vod_loading_kids.jpg", // KIDS VOD 로딩
        "iframe_moviechoice.jpg" // 스카이 초이스
       ];

    var SUBHOME_MODE_IFRAME_NAME = [
        "" , // Iframe 숨김
        "block_audio.jpg" , // 오디오
        "block_age.jpg", // 시청연령 제한
        "block_kids.jpg", // 시청연령 제한 키즈모드
        "block_time.jpg", // 시청시간 제한
        "block_unsub.jpg", // 미가입
        "block_ch.jpg", // 시청채널 제한
        "vod_loading.jpg", // Default VOD 로딩
        "vod_loading.jpg", // VOD 로딩
        "vod_loading.jpg", // KIDS VOD 로딩
        "block_moviechoice.jpg" // 스카이 초이스
    ];

    var FULL_EPG_MODE_IFRAME_NAME = [
        "" , // Iframe 숨김
        "block_audio.jpg" , // 오디오
        "block_age.jpg", // 시청연령 제한
        "block_kids.jpg", // 시청연령 제한 키즈 모드
        "block_time.jpg", // 시청시간 제한
        "block_unsub.jpg", // 미가입
        "block_ch.jpg", // 시청채널 제한
        "vod_loading.jpg", // Default VOD 로딩
        "vod_loading.jpg", // VOD 로딩
        "vod_loading.jpg", // KIDS VOD 로딩
        "block_moviechoice.jpg" // 스카이 초이스
    ];

    var FULL_MODE_DEFAULT_IFRAME_FULL_NAME = [
        "" , // Iframe 숨김
        "images/iframe/web_audio.jpg" , // 오디오
        "", // 시청연령 제한
        "", // 시청연령 제한 키즈모드
        "", // 시청시간 제한
        "", // 미가입
        "", // 시청채널 제한
        "images/iframe/vod_loading.jpg", // DEFAULT VOD 로딩
        "images/iframe/vod_loading.jpg", // VOD 로딩
        "images/iframe/vod_loading.jpg", // KIDS VOD 로딩
        "" // 스카이 초이스
    ];

    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var layerManager = null;
    var ocManager = null;
    
    var navAdapter = null;
    
    var image_path = KTW.CONSTANT.IMAGE_PATH + "iframe/";
    
    var current_iframe_type = 0;
    var focusChannel = null;
    
    var prev_iframe_type = 0;

    function _init() {
        IFRAME_LAYER_ID = KTW.ui.Layer.ID.IFRAME;

        layerManager = KTW.ui.LayerManager;
        ocManager = KTW.managers.data.OCManager;
        navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    }


    /**
     * jjh1117 2016.11.30
     * _checkIframeMappingTable
     * iframemappingtable 에서 sid기준으로 공통 이미지가 있는지 검사한다.
     * @param sid
     */
    function _checkIframeMappingTable(iframeMap , sid) {
        log.printDbg("_checkIframeMappingTable() sid = " + sid);
        var image = null;

        if(iframeMap !== undefined && iframeMap !== null) {
            log.printDbg("_checkIframeMappingTable() iframeMap.length = " + iframeMap.length);
            for(var i = 0 ; i<iframeMap.length;i++) {
                var iframeMapData = iframeMap[i];
                log.printDbg("_checkIframeMappingTable() iframeMapData = " + JSON.stringify(iframeMapData));
                if(iframeMapData.fileName !== undefined && iframeMapData.fileName !== null) {
                    // 2017.08.01 dhlee
                    // https://issues.alticast.com/jira/browse/WEBIIIHOME-3272 이슈 수정
                    // 필드명이 sourceID가 아닌 sourceId 임.
                    // 2.0 에서는 sourceID로 되어 있으나 3.0 에서 규격서 및 송출 정보 기준으로 sourceId 로 변경되었음
                    if(iframeMapData.sourceId !== undefined && iframeMapData.sourceId !== null) {
                        log.printDbg("_checkIframeMappingTable() iframeMapData.sourceID.length = " + iframeMapData.sourceId.length);
                        for(var j = 0 ; j<iframeMapData.sourceId.length;j++) {
                            var srcId = iframeMapData.sourceId[j];
                            log.printDbg("_checkIframeMappingTable() srcId = " + srcId);
                            if(srcId !== undefined && srcId !== null) {
                                if(Number(sid) === Number(srcId)) {
                                    image = iframeMapData.fileName;
                                    break;
                                }
                            }
                        }
                    }
                }
                if(image !== null) {
                    break;
                }
            }
        }

        log.printDbg("_checkIframeMappingTable() return image = " + image);
        return image;
    }
    
    /**
     * set iframe type
     * iframe type을 계속유지하고 변경될 경우에만 set 하도록 함
     * 
     * @param type - iframe type ( @see KTW.managers.service.IframeManager.DEF.TYPE)
     * @param without_change - if true, don't call changeIframe
     * @param forced - if true, update iframe forcily
     */
    function _setType(type, without_change, forced) {
        log.printDbg("_setType() type = " + getTypeString(type));

        if (forced !== true && current_iframe_type === type) {
            return;
        }

        current_iframe_type = type;
        
        if (without_change === true) {
            if(current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.AUDIO) {
                hideIframe();
            }
            log.printDbg("_setType() only set type");
            return;
        }


        if(current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.DEFAULT_VOD_LOADING
            || current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.VOD_LOADING
            || current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.KIDS_VOD_LOADING) {
            KTW.managers.service.AudioIframeManager.setAudioFullEpgExit();
        }
        
        _changeIframe();
    }
    
    function _getType() {
    	return current_iframe_type;
    }
    
    function showIframe (x, y, width , height) {
        log.printDbg("showIframe(" + x + ", " + y + ", " + width + ", " + height + ")");
        
        // 시청시간 제한의 경우 별도 popup에서 iframe 처리를 같이 하도록 함
        // if (current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.TIME_RESTRICTED) {
        //     log.printDbg("showIframe() ignore to draw time restricted iframe");
        //     return;
        // }

        /**
         * OTS Audio Portal Channel에 대한 처리
         * 서비스 시나리오상 OTS 오디오채널은 편성표안에서 동작하게 되어있음.
         * 현재 LastLayer가 편성표 이면 일반 Audio채널이며
         * 편성표가 아닌 경우에는 Audio Portal Channel로 구분함.
         * Audio Portal Channel은 IFrame를 Hide 시킨다.
         */
        // if (KTW.CONSTANT.IS_OTS === true && current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.AUDIO) {
        //     var full_mode = true;
        //
        //     var last_layer = layerManager.getLastLayerInStack();
        //     if (util.isValidVariable(last_layer) === true) {
        //         if (last_layer.id === KTW.ui.Layer.ID.CHANNEL_GUIDE) {
        //              full_mode = false;
        //         }
        //     }
        //    
        //     if (full_mode === true && 
        //             navAdapter.isAudioChannel( navAdapter.getCurrentChannel() , false) === false) {
        //         hideIframe();
        //        
        //         log.printDbg("_changeIframe() full mode audio portal channel... so return");
        //         return;
        //     }
        // }

        /**
         * jjh1117 2106.10.12
         *  현재 main vbo에 대한 video Screen size를 얻어
         *  Mode를 결정한다.
         *
         *  [dj.son] 2017-04-28
         *  대부분의 화면에서 성능을 위해 vbo resize 를 delay 시간을 두어서 수행하는데, changeIframe 도 delay 시간 후에 호출되기 때문에 iframe 이 늦게 resize 되어 보이는 이슈가ㅓ 있음
         *  size 정보를 parameter 로 받고 해당 정보로 resize 수행한다. (size 정보가 하나라도 없으면 vbo size 정보 참조)
         */

        var screen_size = null;

        if ((x !== null && x !== undefined) && (y !== null && y !== undefined) && (width !== null && width !== undefined) && (height !== null && height !== undefined)) {
            screen_size = {
                x: x,
                y: y,
                width: width,
                height: height
            };
        }
        else {
            screen_size = KTW.oipf.AdapterHandler.basicAdapter.getScreenSize();
        }

        var iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;

        if (screen_size !== undefined && screen_size !== null) {
            var left =  screen_size.x;
            var top = screen_size.y;
            var screenWidth = screen_size.width;
            var screenHeight = screen_size.height;

            if (screenWidth !== undefined && screenWidth === KTW.CONSTANT.RESOLUTION.WIDTH
                && screenHeight !== undefined && screenHeight === KTW.CONSTANT.RESOLUTION.HEIGHT) {
                iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;
            }
            else {
                if (left === KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT && top === KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP &&
                    screenWidth === KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH && screenHeight == KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT) {
                    iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE;
                }
                else if (left === KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT && top === KTW.CONSTANT.FULL_EPG_PIG_STYLE.TOP
                        && screenWidth === KTW.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH && screenHeight == KTW.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT) {
                    iframeMode =  KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE;
                }
            }
        }


        var params = {
            iframe : getIframe(iframeMode),
            iframemode : iframeMode,
            type : current_iframe_type
        };

        if(current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.AUDIO) {
            if(iframeMode !== KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
                layerManager.activateLayer({
                    obj: {
                        id: IFRAME_LAYER_ID,
                        type: KTW.ui.Layer.TYPE.BACKGROUND,
                        priority: KTW.ui.Layer.PRIORITY.BACKGROUND+1,
                        params: { data : params }
                    },
                    clear_normal: false,
                    visible: true,
                    cbActivate: function() {

                    }
                });
            }else {
                hideIframe();
            }
        }else {
            layerManager.activateLayer({
                obj: {
                    id: IFRAME_LAYER_ID,
                    type: KTW.ui.Layer.TYPE.BACKGROUND,
                    priority: KTW.ui.Layer.PRIORITY.BACKGROUND+1,
                    params: { data : params }
                },
                clear_normal: false,
                visible: true,
                cbActivate: function() {

                }
            });

        }


        KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChanged);
    }
    
    function hideIframe() {
        log.printDbg("hideIframe()");

        layerManager.deactivateLayer({
            id: IFRAME_LAYER_ID
        });
        KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChanged);
    }

    function onServiceStateChanged (options) {
        if (options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
            hideIframe();
            current_iframe_type = KTW.managers.service.IframeManager.DEF.TYPE.NONE;
            prev_iframe_type = current_iframe_type;

        }
    }
    
    /**
     * check to draw iframe
     * 
     * 보통 full iframe이 그려지는데 showwindow 및 full_epg 화면에서만
     * 별도의 iframe이 그려지므로 해당 Layer의 id를 전달하도록 한다.
     * 해당 처리는 LayerManager에서 처리한다. 
     * 
     * @param layer
     */
    function _changeIframe (x, y, width, height) {
        log.printDbg("_changeIframe(" + x + ", " + y + ", " + width + ", " + height + ")");

		if (current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.NONE) {
            // hide iframe view
            hideIframe();
        }
        else {
            // show iframe view
            showIframe(x, y, width, height);
        }

        var mode = 0;

		if(x !== undefined && y !== undefined && width !== undefined && height !== undefined) {
            KTW.managers.service.ChannelPreviewManager.checkPreviewDisplayMode();
        }else {
            KTW.managers.service.ChannelPreviewManager.checkPreviewDisplayMode();

        }

    }
    
    /**
     * return whether or not showing iframe 
     */
    function _isShowIframe() {
        log.printDbg("_isShowIframe()");
        var show_iframe = current_iframe_type !== KTW.managers.service.IframeManager.DEF.TYPE.NONE;
        
        // OTS STB에서 audio portal 채널일 경우
        // iframe이 노출되지 않는 상태로 처리함
        // OTS audio portal 채널에서 편성표 등의 stack layer 노출상태에서
        // 나가기 키 누를 경우 iframe이 노출되어 있다고 판단하여
        // miniEPG를 activate하게 되어 keyPriority 변경 시 HIDE 처리하지 않고
        // SHOW 상태를 유지함
        // 이로 인해서 obs_notifyHide 메세지가 전달되지 않는 문제 있어서
        // OTS audio portal 상태에서는 iframe이 노출되지 않는 것으로 리턴 처리함
        // if (KTW.CONSTANT.IS_OTS === true &&
        //     current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.AUDIO) {
        //     show_iframe = false;
        // }
        
        return show_iframe;
    }
    
    /**
     * return whether or not showing iframe to need auth
     */
    function _isShowAuthIframe() {
        log.printDbg("_isShowAuthIframe()");
        if (current_iframe_type !== KTW.managers.service.IframeManager.DEF.TYPE.NONE) {
            if (current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED ||
                    current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.TIME_RESTRICTED ||
                    current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.BLOCKED ||
                    current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.SKY_CHOICE) {
                return true;
            }
        }
        
        return false;
    }
    
    function _getAudioIframeFullmodeImage() {
        var default_iframe = FULL_MODE_IFRAME_NAME[KTW.managers.service.IframeManager.DEF.TYPE.AUDIO];
        log.printDbg("_getAudioIframeFullmodeImage() default_iframe : " + default_iframe);

        var audioIframeImage = null;
        var channelIFrameInfo = KTW.managers.data.OCImageDataManager.getChannelIframeInfo();

        if(channelIFrameInfo !== undefined && channelIFrameInfo !== null &&
            channelIFrameInfo.imageList !== null && channelIFrameInfo.imageList.length>0) {
            for(var i=0;i<channelIFrameInfo.imageList.length;i++) {
                if(default_iframe  === channelIFrameInfo.imageList[i] ) {
                    audioIframeImage = channelIFrameInfo.path + channelIFrameInfo.imageList[i] ;
                    break;
                }
            }
        }

        log.printDbg("_getAudioIframeFullmodeImage() channelIFrameInfo search audio Image return  = " + audioIframeImage);
        if(audioIframeImage === null) {
            audioIframeImage = FULL_MODE_DEFAULT_IFRAME_FULL_NAME[KTW.managers.service.IframeManager.DEF.TYPE.AUDIO];
        }
        log.printDbg("_getAudioIframeFullmodeImage() return  value = " + audioIframeImage);
        return audioIframeImage;
    }

    /**
     * get iframe image, if PIG mode, return pip iframe image
     *
     * @param layer     현재 activate 상태인 layer
     * @returns {string} ,
     *           {
     *               background_iframe : backgroundIframe,
     *               foreground_iframe : foregroundIframe,
     *               is_foreground_text : true/false,
     *               is_show_title_background : true/false
     *            }
     */
    function getIframe(iframeMode) {
        log.printDbg("getIframe() iframeMode = " + iframeMode);
        var modeIframe = FULL_MODE_IFRAME_NAME;
        var isDVBChannel = false;
        var isForegroundText = false;
        var backgroundIframe = "";
        var foregroundIframe = "";
        var isShowTitleBg = false;

        if (iframeMode !== undefined && iframeMode !== null) {
            if (iframeMode === KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE) {
                modeIframe = SUBHOME_MODE_IFRAME_NAME;

            }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE) {
                modeIframe = FULL_EPG_MODE_IFRAME_NAME;
            }
        }

        var default_iframe = modeIframe[current_iframe_type];

        /**
         * Full Mode가 아닌 경우 모두 이미지 처리
         */
        if(iframeMode !== KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
            return image_path + default_iframe;
        }

        /**
         * Full Mode 인 경우에 미가입 Iframe , VOD Loading Iframe를 다시 얻는다
         */
        var temp_iframe = null;
        switch (current_iframe_type) {
            case KTW.managers.service.IframeManager.DEF.TYPE.NOT_SUBSCRIBED :
                var channelIFrameInfo = KTW.managers.data.OCImageDataManager.getChannelIframeInfo();
                var channel = navAdapter.getCurrentChannel();
                if (util.isValidVariable(channel) === true) {

                    var sid = channel.sid;
                    if (channel.idType === Channel.ID_DVB_S ||
                            channel.idType === Channel.ID_DVB_S2) {
                        isDVBChannel = true;
                        // switch(channel.sid) {
                        //     case 802 : // skylife 캐치온
                        //         sid = 309;
                        //         break;
                        //     case 265 : // skylife 캐치온 플러스
                        //         sid = 442;
                        //         break;
                        //     case 444 : // skylife VIKI
                        //         sid = 324;
                        //         break;
                        //     case 321 : // skylife 미드나잇
                        //         sid = 349;
                        //         break;
                        //     case 361 : // skylife 플레이보이
                        //         sid = 348;
                        //         break;
                        // }
                    }

                    if(channelIFrameInfo !== undefined && channelIFrameInfo !== null && channelIFrameInfo.iframeMap !== null && isDVBChannel === false) {
                        var mappingTable = channelIFrameInfo.iframeMap;
                        temp_iframe = _checkIframeMappingTable(mappingTable , sid);
                    }

                    if(temp_iframe === null) {
                        if(channelIFrameInfo !== undefined && channelIFrameInfo !== null &&
                            channelIFrameInfo.imageList !== null && channelIFrameInfo.imageList.length>0) {
                            for(var i=0;i<channelIFrameInfo.imageList.length;i++) {
                                var tempSidImageName = "";
                                if(isDVBChannel === true) {
                                    tempSidImageName = sid + ".jpg";
                                }else {
                                    tempSidImageName = "web_" + sid + ".jpg";
                                }
                                if(tempSidImageName  === channelIFrameInfo.imageList[i] ) {
                                    temp_iframe = channelIFrameInfo.path + tempSidImageName;
                                    break;
                                }
                            }
                        }
                    }else {
                        temp_iframe = channelIFrameInfo.path + temp_iframe;
                    }
                }
                break;

            case KTW.managers.service.IframeManager.DEF.TYPE.DEFAULT_VOD_LOADING :
            case KTW.managers.service.IframeManager.DEF.TYPE.VOD_LOADING :
            case KTW.managers.service.IframeManager.DEF.TYPE.KIDS_VOD_LOADING :
                if(current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.VOD_LOADING
                    || current_iframe_type === KTW.managers.service.IframeManager.DEF.TYPE.KIDS_VOD_LOADING) {

                    var vodIFrameInfo = KTW.managers.data.OCImageDataManager.getVodIframeInfo();
                    if(vodIFrameInfo !== undefined && vodIFrameInfo !== null &&
                        vodIFrameInfo.path !== null &&
                        vodIFrameInfo.imageList !== null && vodIFrameInfo.imageList.length>0) {
                        for(var i=0;i<vodIFrameInfo.imageList.length;i++) {

                            if(default_iframe === vodIFrameInfo.imageList[i]) {
                                temp_iframe = vodIFrameInfo.path+vodIFrameInfo.imageList[i];
                                break;
                            }
                        }
                    }
                }

                if(temp_iframe === null) {
                    temp_iframe = FULL_MODE_DEFAULT_IFRAME_FULL_NAME[current_iframe_type];
                }

                break;
        }

        backgroundIframe = image_path + default_iframe;

        log.printDbg("getIframe() temp_iframe = " + temp_iframe);
        if (temp_iframe !== null) {
            backgroundIframe = temp_iframe;
        }else {
            isShowTitleBg = true;
            if(isDVBChannel === false) {
                foregroundIframe = image_path + "iframe_con_package.png";
            }else {
                if(KTW.CONSTANT.IS_OTS_VAT_PRICE === false) {
                    isForegroundText = true;
                }else {
                    foregroundIframe = image_path + "iframe_con_package_ots.png";
                }
            }
        }

        var iframeobj = {
            background_iframe : backgroundIframe,
            foreground_iframe : foregroundIframe,
            is_foreground_text : isForegroundText,
            is_show_title_background : isShowTitleBg
        };
        
        return iframeobj;
    }
    
    function getTypeString(type) {
        
        var type_str = "";
        switch (type) {
            case KTW.managers.service.IframeManager.DEF.TYPE.NONE :
                type_str = "NONE";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.AUDIO :
                type_str = "AUDIO";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED :
                type_str = "PR_BLOCKED";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED_KIDS_MODE :
                type_str = "PR_BLOCKED_KIDS_MODE";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.TIME_RESTRICTED :
                type_str = "TIME_RESTRICTED";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.NOT_SUBSCRIBED :
                type_str = "NOT_SUBSCRIBED";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.BLOCKED :
                type_str = "BLOCKED";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.DEFAULT_VOD_LOADING :
                type_str = "DEFAULT_VOD_LOADING";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.VOD_LOADING :
                type_str = "VOD_LOADING";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.KIDS_VOD_LOADING :
                type_str = "KIDS_VOD_LOADING";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.SKY_CHOICE :
                type_str = "SKY_CHOICE";
                break;
        }
        
        return type_str;
    }
    
    return {
        init: _init,
        setType: _setType,
        getType: _getType,
        getAudioIframeFullmodeImage : _getAudioIframeFullmodeImage,
        changeIframe: _changeIframe,
        
        isShowIframe: _isShowIframe,
        isShowAuthIframe: _isShowAuthIframe
    }
}());

//define constant.
Object.defineProperty(KTW.managers.service.IframeManager, "DEF", {
    value : {
        TYPE : {
            NONE : 0,
            AUDIO : 1,
            PR_BLOCKED : 2,
            PR_BLOCKED_KIDS_MODE : 3,
            TIME_RESTRICTED : 4,
            NOT_SUBSCRIBED : 5,
            BLOCKED : 6,
            DEFAULT_VOD_LOADING : 7,
            VOD_LOADING : 8,
            KIDS_VOD_LOADING : 9,
            SKY_CHOICE : 10
        },
        MODE : {
            FULL_MODE : 0,
            SUB_HOME_MODE : 1,
            FULL_EPG_MODE : 2,
            FULL_MODE_HOME : 3 // Full mode 상태에서 홈 메뉴가 노출 되었을 때 이 부분은 미리보기 가이드 노출 시에만 사용함.
        }
    },
    writable : false,
    configurable : false
});