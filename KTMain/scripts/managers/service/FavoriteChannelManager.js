/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>FavoriteChannelManager</code>
 *
 * 선호채널 등록 / 해제 및 선호채널 편성표 노출
 * @author jjh1117
 * @since 2016-12-30
 */

"use strict";

KTW.managers.service.FavoriteChannelManager = (function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    // 선호채널 설정가능 최대 갯수
    // OTV의 경우 70개, OTS의 경우 각각 40개
    var FAVORITE_MAX = null;

    function _init() {
    }

    function _checkMax() {
        if (FAVORITE_MAX == null) {
            FAVORITE_MAX = KTW.CONSTANT.IS_OTS === true ? 40 : 70;
        }
    }

    /**
     * 특정 채널을 선호채널로 등록
     * @param channel
     * @param callback 등록 성공 시 실행할 callback 함수
     */
    function _addFavChannel(channel, callback) {
        if (channel == null) {
            return false;
        }
        var ch_type = channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ?
            KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE :
            KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE;
        _checkMax();
        if (KTW.oipf.AdapterHandler.navAdapter.getChannelList(ch_type).length < FAVORITE_MAX) {
            //setTimeout(function () {
            //    KTW.oipf.AdapterHandler.navAdapter.changeChannelList(ch_type, undefined, {"ccid":channel.ccid, "majorChannel":channel.majorChannel, "type":0, "update":true });
            //}, 0);
            KTW.oipf.AdapterHandler.navAdapter.changeChannelList(ch_type, undefined, {"ccid":channel.ccid, "majorChannel":channel.majorChannel, "type":0, "update":true });
            if (callback != null) {
                callback();
            }
        }
        else {
            _showFavLimitedPopup(FAVORITE_MAX);
        }
    }
    /**
     * 특정 채널을 선호채널에서 제외
     * @param channel
     * @param callback 등록 성공 시 실행할 callback 함수
     */
    function _removeFavChannel(channel, callback) {
        if (channel == null) {
            return false;
        }
        var ch_type = channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ?
            KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE :
            KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE;
        //setTimeout(function () {
        //    KTW.oipf.AdapterHandler.navAdapter.changeChannelList(ch_type, undefined, {"ccid":channel.ccid, "majorChannel":channel.majorChannel, "type":1, "update":true });
        //}, 0);
        KTW.oipf.AdapterHandler.navAdapter.changeChannelList(ch_type, undefined, {"ccid":channel.ccid, "majorChannel":channel.majorChannel, "type":1, "update":true });
        if (callback != null) {
            callback();
        }
    }
    /**
     * 선호채널 갯수 초과 시 보여줄 안내 팝업
     */
    function _showFavLimitedPopup() {
        KTW.managers.service.SimpleMessageManager.showMessageTextOnly("개수 초과! 선호 채널은 "+FAVORITE_MAX + "개까지 등록 가능합니다");
    }

    function _favoriteChannelOnOff(channel , callbackFunc) {
        if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
            if(callbackFunc !== undefined && callbackFunc !== null) {
                callbackFunc();
            }
        }else {
            if (_isFavoriteChannel(channel) === true) {
                _removeFavChannel(channel, function(){
                    KTW.managers.service.SimpleMessageManager.showRemoveFavChannelNotiMessage();
                    if(callbackFunc !== undefined && callbackFunc !== null) {
                        callbackFunc();
                    }
                });
                return;
            }else {
                _addFavChannel(channel, function(){
                    KTW.managers.service.SimpleMessageManager.showAddFavChannelNotiMessage();
                    if(callbackFunc !== undefined && callbackFunc !== null) {
                        callbackFunc();
                    }
                });
                return;
            }
        }
    }

    function _isFavoriteChannel(channel) {
        if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
            return false;
        }else {
            return KTW.oipf.AdapterHandler.navAdapter.isFavoriteChannel(channel);
        }
    }
    function _activateFavoriteChannelFullEpg() {
        var favorite_channel = null;
        if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
            
        }else {
            favorite_channel = _getFavoriteChannel();
            if(favorite_channel !== null) {
                var menu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (tmpMenu) {
                        if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                            return true;
                        }
                    }
                })[0];

                if (menu) {
                    var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

                    if (moduleId) {
                        KTW.managers.service.MenuServiceManager.jumpMenu({
                            moduleId: moduleId,
                            menu: menu,
                            jump: true,
                            cbJumpMenu: function () {}
                        });

                        return;
                    }
                }
            }
        }
    }

    function _getFavoriteChannel(args) {
        var favorite_channel = null;

        var obj = getFavoriteChannelList();
        var favorite_ch_list = obj.favList;
        var check_index = obj.skylifeLength;

        if (favorite_ch_list.length > 0) {
            favorite_channel = getFavoriteChannel(favorite_ch_list, check_index);
        }
        else {
            if (args) {
                /**
                 * jjh1117 2016.12.30
                 * TODO 해당 부분은 추후 확인 해야 함.
                 */
                // args 값이 존재하는 경우는
                // FullBrowser 상태이거나 양방향 상태에서 핫키가 입력된 경우임
                // 그래서 상태 정리가 끝난 뒤에 처리하기 위해 1초 후 popup이 노출되도록 함
                // 하지만 timeout에 의해 실질적으로 상태 정리가 안되는 경우가 있어서
                // channelEvent를 받아서 처리하도록 수정함


                // var channel_control = adapterHandler.navAdapter.getChannelControl(OTW.nav.Def.CONTROL.MAIN);
                // channel_control.addChannelEventListener(onChannelChangeEvent);
            }
            else {
                _showFavEmptyPopup();
            }
        }
        return favorite_channel;
    }

    function _showFavEmptyPopup () {

        var layer = KTW.ui.LayerManager.getLastLayerInStack({key: "id", value: "favorite_empty_popup"});
        if (layer) {
            return;
        }

        var isExit = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["선호 채널"],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                message: ["등록된 선호 채널이 없습니다", "선호 채널을 등록하시겠습니까?"],
                cssObj: {}
            }],

            arrButton: [{id: "ok", name: "확인"}, {id: "cancel", name: "취소"}],

            cbAction: function (id) {
                if (isExit) {
                    return;
                }
                isExit = true;

                if (id) {
                    KTW.ui.LayerManager.deactivateLayer({id: "favorite_empty_popup"});

                    if (id === "ok") {
                        /**
                         * jjh1117 2017/10/23
                         * 오디오편성표가 존재 할 경우 deactivateLayer 하도록 수정함.
                         */

                        if(KTW.managers.service.AudioIframeManager.getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
                            KTW.ui.LayerManager.deactivateLayer({id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
                        }
                        
                        // 2017.04.21 dhlee
                        // 설정쪽 메뉴로 Jump 해야 하는 상황에서는 activateLayer 대신 SettingMenuManager.activateSettingMenuByMenuID() 를 사용해야 한다.
                        KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                            menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_FAVORITE_CHANNEL,
                            updateSubTitle: false
                        });

                    }
                }
            }
        };

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "favorite_empty_popup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true
        });
    }

    /**
     * 선호채널 획득
     * 전체 선호채널 배열 순서가 [Skylife - KT] 순이기 때문에
     * 현재 채널을 기준으로 검색 시작, 종료 index 중 현재 채널번호 보다 큰 채널을 선택함.
     * 검색을 했음에도 불구하고 찾지 못했다면 가장 처음 선호채널을 선택함
     * 추가로 선호채널 ring이 Skylife 또는 KT 하나만 존재하는 경우
     * 현재 채널과 다른 type의 가장 첫 선호채널을 선택
     *
     * @param list - 전체 선호채널 array (OTS의 경우 : skylife 채널 + kt 채널 순)
     * @param index - skylife 선호채널 개수
     */
    function getFavoriteChannel(list, index) {

        var favorite_channel = null;
        var length = list.length;
        var offset = 0;
        var check_length = length;

        var channel_control = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
        var cur_channel = channel_control.vbo.currentChannel;
        log.printDbg("getFavoriteChannel() cur channel = " + JSON.stringify(cur_channel));

        // 현재 채널이 skylife 채널인지 KT 채널인지 먼저 확인
        var is_sky_channel = false;
        if (util.isValidVariable(cur_channel) === true) {
            if (cur_channel.idType === Channel.ID_DVB_S ||
                cur_channel.idType === Channel.ID_DVB_S2) {
                is_sky_channel = true;
            }
        }

        if (is_sky_channel === true) {
            // 현재채널이 skylife 채널인 경우
            if (index > 0) {
                // skylife 선호채널이 존재하면 검색 종료 index 설정
                check_length = index;
            }
            else {
                // skylife 선호채널이 존재하지 않는다면
                // KT 선호채널 중 첫번째 것을 선택
                favorite_channel = list[0];
            }
        }
        else {
            // 현재채널이 KT 채널인 경우
            if (length - index > 0) {
                // KT 선호채널이 존재하면 검색 시작 index 설정
                offset = index;
            }
            else {
                // KT 선호채널이 존재하지 않는다면
                // Skylife 선호채널 중 첫번째 것을 선택
                favorite_channel = list[0];
            }
        }

        if (favorite_channel === null) {
            // 선호채널이 아직 assign 되어 있지 않다면
            // 검색할 선호채널 시작, 종료 index를 기준으로
            // 인접한 큰 채널번호를 가진 선호채널을 검색
            for (var i = offset; i < check_length; i++) {
                if (list[i].majorChannel > cur_channel.majorChannel) {
                    favorite_channel = list[i];
                    break;
                }
            }

            if (favorite_channel === null) {
                // 만약 검색을 통해서도 찾지 못했다면
                // 현재 채널과 동일한 type의 선호채널 ring에서
                // 인접한 큰 채널번호를 가진 채널이 없다는 것임
                // 결국 looping 에 의해 다음 ring의 가장 첫번째 선호채널을 선택함
                if (check_length === index) {
                    if (length === index) {
                        // 단, 다음 channel ring이 없다면 가장 첫번째 선호채널을 선택함
                        favorite_channel = list[0];
                    }
                    else {
                        favorite_channel = list[index];
                    }
                }
                else {
                    favorite_channel = list[0];
                }
            }
        }

        log.printDbg("getFavoriteChannel() favorite channel = " + JSON.stringify(favorite_channel));

        return favorite_channel;
    }


    function _checkOTSAudioChannel() {
        // OTS 형상에서 현재 채널이 오디오 채널이라면 편성표 상태이기 때문에
        // 의도적으로 lcw 채널로 tune 처리하도록 함
        if (KTW.CONSTANT.IS_OTS === true) {
            var channel_control = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            var cur_channel = channel_control.vbo.currentChannel;

            if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(cur_channel) === true) {
                var prev_ch = channel_control.getBackChannel();
                KTW.oipf.AdapterHandler.navAdapter.changeChannel(prev_ch, true);
            }
        }
    }

    // function onChannelChangeEvent() {
    //     var channel_control = adapterHandler.navAdapter.getChannelControl(OTW.nav.Def.CONTROL.MAIN);
    //     channel_control.removeChannelEventListener(onChannelChangeEvent);
    //
    //     OTW.utils.epgUtil.showFavEmptyPopup(changeLCWChannel);
    // }

    function getFavoriteChannelList () {
        var favorite_ch_list = [];
        var skylifeLength = 0;

        // Skylife 채널이 먼저 추가
        if (KTW.CONSTANT.IS_OTS === true) {
            favorite_ch_list = KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE);
            favorite_ch_list = Array.apply(this, favorite_ch_list).slice();
            skylifeLength = favorite_ch_list.length;
        }

        // 이후 KT 채널을 리스트에 추가함
        var kt_favorite_ch_list =  KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
        kt_favorite_ch_list = Array.apply(this, kt_favorite_ch_list).slice();

        favorite_ch_list = favorite_ch_list.concat(kt_favorite_ch_list);

        return {
            favList: favorite_ch_list,
            skylifeLength: skylifeLength
        };
    }

    function _isFavoriteChannelListExist () {
        var obj = getFavoriteChannelList();

        if (obj && obj.favList && obj.favList.length > 0) {
            return true;
        }

        return false;
    }



    return {
        init: _init,
        favoriteChannelOnOff: _favoriteChannelOnOff,
        isFavoriteChannel : _isFavoriteChannel,
        activateFavoriteChannelFullEpg : _activateFavoriteChannelFullEpg,
        showFavEmptyPopup: _showFavEmptyPopup,
        isFavoriteChannelListExist: _isFavoriteChannelListExist
    }


}());


