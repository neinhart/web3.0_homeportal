/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>IframeManager</code>
 *  @author jjh1117
 *  @since 2016-11-28
 */

"use strict";

KTW.managers.service.AudioIframeManager = (function () {
    
    var AUDIO_IFRAME_LAYER_ID = "AudioIframeLayer";

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var layerManager = null;
    var ocManager = null;
    var navAdapter = null;
    
    
    var current_iframe_type = 0;
    var focusChannel = null;
    
    function _init() {
        AUDIO_IFRAME_LAYER_ID = KTW.ui.Layer.ID.AUDIO_IFRAME;
        layerManager = KTW.ui.LayerManager;
        ocManager = KTW.managers.data.OCManager;
        navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    }

    /**
     * set iframe type
     * iframe type을 계속유지하고 변경될 경우에만 set 하도록 함
     * @param type - iframe type ( @see KTW.managers.service.AudioIframeManager.DEF.TYPE)
     */
    function _setPause() {
        hideIframe(true);
    }
    
    function _setResume() {

        var layer = KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID);
        showIframe();
        if(layer !== undefined && layer !== null && layer.isShowing() === false) {
            showIframe();
        }
    }
    
    function _setType(type , focusChannel) {
        log.printDbg("_setType() type = " + getTypeString(type));

        if (current_iframe_type === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG && current_iframe_type === type) {
            _setAudioFocusChannel(focusChannel);
            
            KTW.ui.LayerManager.clearNormalLayer();

            if(KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID) !== null && KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID) !== undefined) {
                var layer = layerManager.getLastLayerInStack({key: "id", value: AUDIO_IFRAME_LAYER_ID});
                if(layer !== null && layer.isShowing()) {
                    var params = {
                        is_update : true
                    };

                    layer.setParams({data:params});
                    layer.show();
                    layer.showAudioEpgViewIcon();
                }

                return;
            }
        }
        
        current_iframe_type = type;
        if(current_iframe_type === KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE) {
            hideIframe();
            return;
        }

        if(current_iframe_type === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
            KTW.ui.LayerManager.clearNormalLayer();
        }

        _setAudioFocusChannel(focusChannel);
        showIframe();
        if(current_iframe_type === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
            /**
             * jjh1117 2017/10/17
             * 미니가이드가 노출 된 상태에서 DCA/Channel UP/DOWN으로
             * 오디오편성표가 노출 된 경우 미니가이드를 강제적으로 닫도록 처리
             * (이유 : 미니가이드가 자동사라짐 시간까지 노출되는 이슈 때문에)
             */
            KTW.ui.LayerManager.deactivateLayer({
                id:KTW.ui.Layer.ID.MINI_EPG
            });
            
            KTW.managers.service.IframeManager.setType(KTW.managers.service.IframeManager.DEF.TYPE.AUDIO , true);


            if(KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID).div.css("z-index") === "3") {
                KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID).div.css({"z-index":1});
            }
        }
    }
    
    function _setAudioFocusChannel(focus_channel , isUpdate) {
        log.printDbg("_setAudioFocusChannel() focus_channel = " + focus_channel);
        focusChannel = focus_channel;
        if(isUpdate) {
            /**
             * jjh1117 2017/10/19
             * 아래 부분은 추후 처리 되어야 함.
             */
            var layer = KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID);
            if(layer !== undefined && layer !== null && layer.isShowing()) {
                layer.updateFocusAudioChannel();
            }
        }
    }
    
    function _getType() {
        log.printDbg("_getType() current_iframe_type : " + current_iframe_type);
    	return current_iframe_type;
    }
    
    function _getAudioFocusChannel() {
        return focusChannel;
    }

    function _setAudioFullEpgExit() {
        log.printDbg("_setAudioFullEpgExit() current_iframe_type : " + current_iframe_type);

        if(current_iframe_type !== KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE) {
            layerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG
            });
            _setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE);
        }
    }

    
    function showIframe (isUpdate) {
        log.printDbg("showIframe() isUpdate : " + isUpdate);

        var params = {
            iframe_type : current_iframe_type ,
            iframe_img : KTW.managers.service.IframeManager.getAudioIframeFullmodeImage(),
            is_update : ((isUpdate === undefined || isUpdate === null) ? false : isUpdate)
        };

        layerManager.activateLayer({
            obj: {
                id: AUDIO_IFRAME_LAYER_ID,
                type: KTW.ui.Layer.TYPE.BACKGROUND,
                priority: (current_iframe_type === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) ? KTW.ui.Layer.PRIORITY.BACKGROUND : (KTW.ui.Layer.PRIORITY.BACKGROUND + 2) ,
                params: { data : params }
            },
            clear_normal: false,
            visible: true,
            cbActivate: function() {

            }
        });
        KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChanged);
    }
    
    function hideIframe(isPause) {
        log.printDbg("hideIframe()");

        if(isPause === undefined || isPause === null) {
            if(KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID).div.css("z-index") === "1") {
                KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID).div.css({"z-index":3});
            }
        }

        layerManager.deactivateLayer({
            id: AUDIO_IFRAME_LAYER_ID  , remove: isPause === undefined ?  false : isPause
        });
        KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChanged);
        
    }

    function onServiceStateChanged (options) {
        if (options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP || options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED ||
            options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
            current_iframe_type = KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE;
            KTW.ui.LayerManager.deactivateLayer({id:KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
            hideIframe();
        }
    }
    
    function getTypeString(type) {
        
        var type_str = "";
        switch (type) {
            case KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE :
                type_str = "NONE";
                break;
            case KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG :
                type_str = "PREVIEW_AUDIO_FULL_EPG";
                break;
            case KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG :
                type_str = "AUDIO_FULL_EPG";
                break;
        }
        
        return type_str;
    }
    
    function _controlKey(key_code) {
        log.printDbg("_controlKey() key_code : " + key_code);
        var consumed = false;
        
        if(_getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG ) {
         
            var lastLayer = KTW.ui.LayerManager.getLastLayerInStack();
            if (lastLayer && lastLayer.type >= KTW.ui.Layer.TYPE.NORMAL && lastLayer.isShowing()) {
                log.printDbg("_controlKey() lastLayer  not null");
                return consumed;
            }else {
                if(key_code === KTW.KEY_CODE.DOWN || key_code === KTW.KEY_CODE.OK || key_code === KTW.KEY_CODE.FULLEPG || key_code === KTW.KEY_CODE.FULLEPG_OTS) {

                    var mainChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                    var params = {
                        focus_channel : mainChannel
                    };

                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG,
                            type: KTW.ui.Layer.TYPE.NORMAL,
                            priority: KTW.ui.Layer.PRIORITY.NORMAL,
                            view : KTW.ui.view.AudioChannelFullEpgView,
                            linkage : false,
                            params: params
                        },
                        clear_normal: false,
                        visible: true,
                        cbActivate: function() {

                        }
                    });

                    var layer = KTW.ui.LayerManager.getLayer(AUDIO_IFRAME_LAYER_ID);
                    if(layer !== null) {
                        layer.hideAudioEpgViewIcon();
                    }

                    consumed = true;
                }else if(key_code === KTW.KEY_CODE.CONTEXT || key_code === KTW.KEY_CODE.LEFT ||  key_code === KTW.KEY_CODE.UP) {
                    consumed = true;
                }else if(key_code === KTW.KEY_CODE.BACK) {
                    if(KTW.CONSTANT.IS_OTS) {
                        this.returnToBackChannelOnOTS();
                        consumed = true;
                    }
                }
            }
            
        }
        

        return consumed;
    }

    /**
     * [dj.son] 네비게이터가 채널 튠한 것에 대한 채널 이벤트가 안 왔을 경우, 오디오 iframe 이 노출 안되는 이슈 수정 (onlySelect 로 채널 튠 한 건 내부 로직에 의해 오디오 아이프레임이 노출 안됨)
     * 
     * - 현재 채널 여부 & 오디오 iframe 노출 여부 판단 후 오디오 iframe 강제 노출
     */
    function _checkBootingChannel () {
        if (current_iframe_type !== KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
            var mainChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
            if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(mainChannel, true) === true) {
                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , mainChannel);
            }
        }
        
    }
    
    return {
        init: _init,
        setType: _setType,
        setAudioFocusChannel : _setAudioFocusChannel,
        setAudioFullEpgExit : _setAudioFullEpgExit,
        getType: _getType,
        getAudioFocusChannel : _getAudioFocusChannel,
        setPause : _setPause,
        setResume : _setResume,
        controlKey : _controlKey,

        checkBootingChannel: _checkBootingChannel
    }
}());

//define constant.
Object.defineProperty(KTW.managers.service.AudioIframeManager, "DEF", {
    value : {
        TYPE : {
            NONE : 0,
            PREVIEW_AUDIO_FULL_EPG : 1 ,
            AUDIO_FULL_EPG : 2
        }
    },
    writable : false,
    configurable : false
});