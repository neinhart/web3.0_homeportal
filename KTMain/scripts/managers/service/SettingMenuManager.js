/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>SettingMenuManager</code>
 *
 * @author sw.nam
 * @since 2017-03-30
 *
 *  설정 menu 정보등을 기준으로 메뉴간 이동을 관리한다 (주로 jump 시나리오에서 사용)
 *  실제 setting Components( 설정 메뉴별 미리보기화면 관리 뷰) 에서 사용되고 있는 activate 관리.
 *
 */

"use strict";

KTW.managers.service.SettingMenuManager = (function () {

    var log = KTW.utils.Log;

    var actionNextLayer = null;
    var MENU_NAME;
    var LAYER_IDX;
    var MENU_ID;

    var request_object;
    var bMailMenuIdx;
    var subTitleMenuIdx;
    var isForCreate = false;

    // 2017.11.21 sw.na,
    // jump 시나리오에서 성인 인증이 필요한 메뉴도 updateSubTitle 여부를 판단하기 위해 전역으로 사용
    // WEBIIIHOME-3538 이슈 관련 수정
    var updateSubTitle;

    //2018.02.22 sw.nam
    // 자동번역 언어 지원 여부 판단 flag, 비즈 상품이더라도 메시지 안오면  false, 처음
    var isTranslationLanguageCaptionSupport = true;


    /**
     * options 를 통해 전달 받은 menuId 를 이용하여 각 setting 상세 layer를 activate 하는 역할을 수행
     * 단, Jump 하는 상황이 존재하므로 options.updateSubTitle 여부를 함께 전달해줘야 한다.
     * options.updateSubTitle: true(subtitle 업데이트 함)/false(subtitle 업데이트하지 않음)
     *
     * @param options
     */

    function _activateSettingMenuByMenuID(options) {
        log.printDbg("activateSettingMenuByMenuID :: " + log.stringify(options));

        var layerID;
        var isRequiredAuth = false;
        var menuDataManager = KTW.managers.data.MenuDataManager;

        if( !options || !options.menuId ){
            log.printDbg("It's not included menu ID in it");

            return ;
        }
        // 2017.04.24 dhlee
        // sub title update 여부 값을 확인한다.
        updateSubTitle = options.updateSubTitle;


        MENU_ID = options.menuId;
        MENU_NAME = options.name; // jump 시에는 Menu 이름이 넘어오지 않음
        LAYER_IDX = options.menuIdx;

        log.printDbg("MENU_ID ::"+ MENU_ID);

        var SETTING_ID = menuDataManager.getSettingMenuId();

        if(MENU_ID == SETTING_ID) {
            layerID = KTW.ui.Layer.ID.SETTING_MAIN;
        }else {
            layerID = getLayerIdByMenuID(MENU_ID);
        }

        if( MENU_ID == menuDataManager.MENU_ID.SETTING_TIME_LIMIT ||
            MENU_ID == menuDataManager.MENU_ID.SETTING_PARENTAL_RATING ||
            MENU_ID == menuDataManager.MENU_ID.SETTING_BLOCKED_CHANNEL ||
            MENU_ID == menuDataManager.MENU_ID.TRANSPONDER ) {
            isRequiredAuth = true;
        }


        // 2017.08.11 sw.nam
        // 음성비서 관련 layer activate toggling 로직 추가
        // 음성 비서로 특정 설정 메뉴 호출 시 이미 activate 되어있는 경우 deactivate 시킨다 (toggling)
        var settingMainLayer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.SETTING_MAIN);
        var isBeingActivatedLayer = KTW.ui.LayerManager.getLayer(layerID);

        if(isBeingActivatedLayer && isBeingActivatedLayer.isShowing()) {
            var flag = true;
            if(layerID === KTW.ui.Layer.ID.SETTING_MAIN) {
              if(MENU_ID !== settingMainLayer.getCurrentFocusedMenu()) {
                  flag = false;
              }
            }
            if(flag) {
                log.printDbg("This layer is showing");
                KTW.ui.LayerManager.deactivateLayer({
                    id: isBeingActivatedLayer.id,
                    remove: true
                });
                return ;
            }
        }
        // 설정 메뉴 진입 전 인증이 필요한 경우
        if (isRequiredAuth) {
            actionNextLayer = layerID;
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_AUTH_CHECK_PASSWORD_POPUP,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        callback: callbackFuncAuthCheck
                    }
                },
                new: true,
                visible: true,
                skipRequestShow: options.skipRequestShow
            });
        }
        else {
            var layerOptions = {
                obj: {
                    id: layerID,
                    type: KTW.ui.Layer.TYPE.NORMAL,
                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                    params: {
                        name: MENU_NAME, // 점프메뉴라면 menu name 은 undefined 로 넘어감, 설정 상세 메뉴에서 jump 인지 아닌지를 판단하는 key로도 사용 됨.
                        menuId : MENU_ID,
                        complete: function() {
                            if (updateSubTitle) {
                                updateSettingMenuSubtitle({
                                    menuId : MENU_ID,
                                    index : LAYER_IDX
                                });
                            }
                            /* 2017.05.23 dhlee
                             MenuServiceManager() 에서 callback function을 호출하고 있으므로 여기서는 하지 않도록 한다.
                             else {
                             // 2017.05.12 dhlee
                             // TODO
                             // 향후 전체적으로 손볼 때 수정하기로 하고.. 일단은 MessageManager에서 option을 통해 넘어온 callback 함수를
                             // 여기서 호출하도록 해둔다.
                             if (options.cbJumpMenu) {
                             options.cbJumpMenu();
                             }
                             }
                             */
                        }
                    }
                },
                visible: true,
                skipRequestShow: options.skipRequestShow
            };

            if (MENU_ID === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CHANGE_PIN) {
                layerOptions.new = true;

                if (options.isSystemPriority) {
                    layerOptions.obj.type = KTW.ui.Layer.TYPE.POPUP;
                    layerOptions.obj.priority = KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5;
                    layerOptions.obj.linkage = true;
                    layerOptions.obj.params.isSystemPriority = true;
                }
            }

            KTW.ui.LayerManager.activateLayer(layerOptions);
        }
    }

    function callbackFuncAuthCheck(success) {
        log.printDbg("callbackFuncAuthCheck()");
        KTW.ui.LayerManager.deactivateLayer({
            id: KTW.ui.Layer.ID.SETTING_AUTH_CHECK_PASSWORD_POPUP
        });
        if(success !== undefined && success !== null && success === true) {
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: actionNextLayer,
                    type: KTW.ui.Layer.TYPE.NORMAL,
                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                    params: {
                        name: MENU_NAME,
                        complete: function() {
                            if (updateSubTitle) {
                                updateSettingMenuSubtitle({
                                    menuId : MENU_ID,
                                    index : LAYER_IDX
                                });
                            }
                        }
                    }
                },
                visible: true
            });
        }
    }

    /**
     *
     * [sw.nam]  각 설정 상세 화면에서 저장 후 메시지 팝업 및 history back 호출 함수
     * 2017.04.11 클래스 성격을 고려하여 settingDataAdapter -> settingMenuManger 로 이동
     * @param menuId
     * @param message
     * @private
     */
    function _historyBackAndPopup(menuId, message) {

        var MENU_ID = menuId;
        var layerID ;
        if(!message)
            message = "저장되었습니다";

        layerID = getLayerIdByMenuID(MENU_ID);

        if(layerID) {
            log.printDbg("_historyBackAndPopup "+layerID);
             KTW.ui.LayerManager.deactivateLayer({id: layerID, remove: false});
            KTW.managers.service.SimpleMessageManager.showMessageTextOnly(message);
        }else {
            log.printDbg("_historyBackAndPopup withoutID");
            KTW.ui.LayerManager.historyBack();
            KTW.managers.service.SimpleMessageManager.showMessageTextOnly(message);
        }

    }

    /**
     * 2017.04.12 [sw.nam] 설정값을 변경한 화면의 경우 subTitleText 갱신하는 function
     * @param options
     *  options.menuId : Activate 됬던 layer 의 메뉴 ID
     *  options.index : 설정 각 상세메뉴 array index
     */
    function updateSettingMenuSubtitle(options) {
        log.printDbg("updateSettingMenuSubtitle()");

        var menuId = options.menuId;
        var menuIdx = options.index;
        var result;
        var subTitleText;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var layerManager = KTW.ui.LayerManager;
        var menuDataManager = KTW.managers.data.MenuDataManager;
        var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;
        var settingMainLayer = layerManager.getLayer(KTW.ui.Layer.ID.SETTING_MAIN);
        //var language = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.LANGUAGE);
        var language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();

        // 2017.04.17 dhlee Jump 로 실행된 상황에서는 getLayer() 결과가 null 이다.
        // 임시로 이렇게 처리하도록 한다.
        if (settingMainLayer === null || settingMainLayer === undefined) {
            log.printDbg("updateSettingMenuSubTitle(), cannot find settingMainLayer.");
            return;
        }

        result = settingDataAdapter.getConfigurationInfoByMenuId(menuId);

        switch(menuId) {
            case menuDataManager.MENU_ID.SETTING_ALERT_MESSAGE:
                request_object = settingDataAdapter.getBMailInfo(cbGetBMailInfo, KTW.SAID);
                bMailMenuIdx = menuIdx;
                subTitleText = "  ";
                break;
            // 시스템 설정
            case menuDataManager.MENU_ID.SETTING_ASPECT_RATIO :
                if(language == "kor") {
                    subTitleText = result[0] + "　|　" + (result[1] == "stretch" ? "전체 화면" : result[1] == "normal" ? "원본 비율 유지" : "원본 비율 확대");
                }else {
                    subTitleText = result[0] + "　|　" + (result[1] == "stretch" ? "Full Screen" : result[1] == "normal" ? "Keep Original" : "원본 비율 확대");
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_RESOLUTION :
                subTitleText = result[0];
                if(subTitleText == "2160p30") {
                    subTitleText = "2160p (30fps)";
                } else if(subTitleText == "2160p60") {
                    subTitleText = "2160p (60fps)";
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_AUTO_POWER :
                subTitleText = setAutoPowerText(result[0]);
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_STANDBY_MODE :
                if(language == "kor") {
                    subTitleText = result[0] == "ON" ? "자동 대기 모드 ON" : "자동 대기 모드 OFF";
                }else {
                    subTitleText = result[0] == "ON" ? "ON" : "OFF";
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                break;
            case menuDataManager.MENU_ID.SETTING_SOUND :

                log.printDbg("soundType = "+result[0]);
                if(result[0].indexOf("ac3") > -1) {
                    if(language == "kor") {
                        subTitleText = "5.1 채널 (Dolby AC3)";
                    }else {
                        subTitleText = "5.1 ch (Dolby AC3)";
                    }
                }else {
                    if(language == "kor") {
                        subTitleText = "2채널 (Stereo AAC)";
                    }else {
                        subTitleText = "2 ch (Stereo AAC)";
                    }
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;

            case menuDataManager.MENU_ID.SETTING_VOICE_GUIDE :
                // 음성 가이드
                var volumeText;
                var defaultVoice;
                var triggerVoiceType;
                var MANUAL_OFF_RECOGNIZING_WORD = "||starttime=";
                subTitleText = result[0];
                volumeText = result[1];

                var isAvailableTriggerVoice = false;
                triggerVoiceType = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_TRIGGER_VOICE_TYPE);

                if (triggerVoiceType !== null && triggerVoiceType !== "undefined" && triggerVoiceType !== undefined && triggerVoiceType !== "null") {
                    isAvailableTriggerVoice = true;
                    triggerVoiceType = result[2];
                }else {
                    defaultVoice = result[2];
                }

                // 볼륨 설정
                volumeText = Math.floor(result[1]);


                if(result[0] == "OFF") {
                    subTitleText = result[0];
                }else if(result[0] == "ON") {
                    subTitleText = result[0] + "　|　" +volumeText;
                    if(isAvailableTriggerVoice) {
                        // 호출어 text 설정
                        switch(triggerVoiceType) {
                            case "0":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"올레 tv";
                                }else {
                                    subTitleText += "　|　"+"olleh tv";
                                }
                                break;
                            case "1":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"기가지니";
                                }else {
                                    subTitleText += "　|　"+"GIGA Genie";
                                }
                                break;
                            case "2":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"지니야";
                                }else {
                                    subTitleText += "　|　"+"Genieya";
                                }
                                break;
                            case "3":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"친구야";
                                }else {
                                    subTitleText += "　|　"+"chinguya";
                                }
                                break;
                            case "4":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"자기야";
                                }else {
                                    subTitleText += "　|　"+"jagiya";
                                }
                                break;
                        }
                    }else {
                        // 가이드 음성 text 설정
                        if(defaultVoice == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE ) {
                            if(language == "kor") {
                                subTitleText += "　|　"+"여성";
                            }else {
                                subTitleText += "　|　"+"Woman";
                            }
                        }else if(defaultVoice == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD) {
                            if(language == "kor") {
                                subTitleText += "　|　"+"어린이";
                            }else {
                                subTitleText += "　|　"+"Child";
                            }
                        }
                    }

                }else if(result[0].indexOf(MANUAL_OFF_RECOGNIZING_WORD) > -1) {
                    // OFF 수동 설정 case
                    var startTime = result[0].substring(15,20);
                    var endTime = result[0].substring(30,35);

                    subTitleText = "OFF "+startTime + "-" + endTime + "　|　" +volumeText;
                    if(isAvailableTriggerVoice) {
                        // 호출어 text 설정
                        switch(triggerVoiceType) {
                            case "0":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"올레 tv";
                                }else {
                                    subTitleText += "　|　"+"olleh tv";
                                }
                                break;
                            case "1":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"기가지니";
                                }else {
                                    subTitleText += "　|　"+"GIGA Genie";
                                }
                                break;
                            case "2":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"지니야";
                                }else {
                                    subTitleText += "　|　"+"Genieya";
                                }
                                break;
                            case "3":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"친구야";
                                }else {
                                    subTitleText += "　|　"+"chinguya";
                                }
                                break;
                            case "4":
                                if(language == "kor") {
                                    subTitleText += "　|　"+"자기야";
                                }else {
                                    subTitleText += "　|　"+"jagiya";
                                }
                                break;
                        }
                    }else {
                        // 가이드 음성 text 설정
                        if(defaultVoice == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE ) {
                            if(language == "kor") {
                                subTitleText += "　|　"+"여성";
                            }else {
                                subTitleText += "　|　"+"Woman";
                            }
                        }else if(defaultVoice == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD) {
                            if(language == "kor") {
                                subTitleText += "　|　"+"어린이";
                            }else {
                                subTitleText += "　|　"+"Child";
                            }
                        }
                    }
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                break;

            case menuDataManager.MENU_ID.SETTING_GIGA_GINI_AUDIO_OUTPUT :
                switch(result[0]) {
                    case "hdmi" :
                        if(language == "kor") {
                            subTitleText = "TV 연결";
                        }else {
                            subTitleText = "TV";
                        }
                        break;
                    case "speaker" :
                        if(language == "kor") {
                            subTitleText = "기가지니 스피커 연결";
                        }else {
                            subTitleText = "GiGA Genie Speaker";
                        }
                        break;
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_USB :
                break;
            case menuDataManager.MENU_ID.SETTING_BLUETOOTH :
                if (result[0] == "true" || bluetoothDevice.enabled) {
                    if(KTW.CONSTANT.UHD3.BEACON && !KTW.CONSTANT.STB_TYPE.ANDROID) {
                        //2017.06.19 sw.nam 플립북 v1.2.2 920p 참조하여 UHD3 인경우 subTitle 노출 방식 수정
                        var list = bluetoothDevice.getPairedDevices(true,true);
                        if(list) {
                            if(language == "kor") {
                                subTitleText = "연결된 장치 "+list.length+"개";
                            } else {
                                subTitleText = "Device: "+list.length;
                            }
                        }else {
                            if(language == "kor") {
                                subTitleText = "연결된 장치 0개";
                            }else {
                                subTitleText = "Device: 0";
                            }
                        }
                    }else {
                        if(language == "kor") {
                            subTitleText = "블루투스 사용";
                        }else {
                            subTitleText = "ON";
                        }
                    }
                } else {
                    if(language == "kor"){
                        subTitleText = "블루투스 사용 안함";
                    }else {
                        subTitleText = "OFF";
                    }
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_HDMI_CEC :
                var hdmi_cec;
                var hdmi_cec_fn;

                try {
                    if (KTW.CONSTANT.IS_OTS) {
                        hdmi_cec = result[0];
                    } else {
                        hdmi_cec_fn = result[1];
                        if (hdmi_cec_fn === "0") {
                            hdmi_cec = "false";
                        } else {
                            hdmi_cec = "true";
                        }
                    }
                } catch (error) {
                    log.printErr("readConfiguration(), error = " + error.message);
                    hdmi_cec = "false";
                }
                if (hdmi_cec === "true") {
                    subTitleText = "ON";
                } else {
                    subTitleText = "OFF";
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.LANGUAGE :
                if (result[0] == "kor") {
                    subTitleText = "한국어";
                } else {
                    subTitleText = "English";
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_START_CHANNEL :
                var cugData = result[0];
                var cugDataLength;
                var cIdx;
                var currentStartingChannel;
                subTitleText = result[1];
                if(subTitleText !== "가이드 채널" && subTitleText !== "LCW") {
                    //가입된 cug 채널이 있는 경우
                    if(cugData !== null && cugData !== undefined &&
                        cugData.cugList && cugData.cugList.length) {
                        cugDataLength = cugData.cugList.length;
                        for (cIdx = 0; cIdx < cugDataLength; cIdx++) {
                            if (cugData.cugList[cIdx].linkInfo === subTitleText) {
                                currentStartingChannel = cugData.cugList[cIdx].cugName;
                                break;
                            }
                        }
                        // cug channel 의 link info 를 찾지 못한 경우에 대한 예외처리.
                        if(currentStartingChannel){
                            subTitleText = currentStartingChannel;
                        }else {
                            subTitleText = "가이드 채널";
                            if(language !== "kor"){
                                subTitleText = "Promotion Channel";
                            }
                        }
                    }
                }
                else if (subTitleText === "LCW") {
                    if(language == "kor"){
                        subTitleText = "이전 시청 채널";
                    }else {
                        subTitleText = "Last Watched Channel";
                    }
                }
                else if(subTitleText === "가이드 채널"){
                    if(language !== "kor"){
                        subTitleText = "Promotion Channel";
                    }
                }
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.TRANSPONDER : // 중계기(OTS)
                subTitleText= "BS4";
                settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                break;
            //관리자 설정 메뉴
            case menuDataManager.MENU_ID.SKY_LNB:
                subTitleText = "DisEqC-A(무궁화 3호)";
                if(!options.isForCreate) {
                    settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                }
                break;
            case menuDataManager.MENU_ID.SKY_EMERGENCY:

                if (result[0] == "true") {
                    subTitleText = "ON";
                } else {
                    subTitleText = "OFF";
                }
                if(!options.isForCreate) {
                    settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                }

                break;
            case menuDataManager.MENU_ID.SKY_CHANNEL_INFO:
                log.printDbg("menuDataManager.MENU_ID.SKY_CHANNEL_INFO" + result[0]);
                if (result[0] == "true") {
                    subTitleText = "ON";
                } else {
                    subTitleText = "OFF";
                }
                if(!options.isForCreate) {
                    settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                }

                break;
            case menuDataManager.MENU_ID.SKY_CLOSED_CAPTION:
                log.printDbg("menuDataManager.MENU_ID.SKY_CLOSED_CAPTION" + result[0]);
                if (result[0] == "1") {
                    subTitleText = "ON";
                } else {
                    subTitleText = "OFF";
                }
                if(!options.isForCreate) {
                    settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                }

                break;
            case menuDataManager.MENU_ID.SKY_CAS:
                if(!options.isForCreate) {
                    settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                }
                break;

            case menuDataManager.MENU_ID.SETTING_BEACON:
                var stepValue = ["-5dBm", "-1dBm", "3dBm", "7dBm", "10dBm"];
                var sending_power_value = result[0];
                var sending_interval_value = result[1];
                log.printDbg("sending_power_value : "+sending_power_value+" sending_interval_value : "+sending_interval_value);
                subTitleText = stepValue[sending_power_value]+" | "+ sending_interval_value +" ms";
                if(!options.isForCreate) {
                    settingMainLayer.setSubTitle("System", menuIdx, subTitleText);
                }
                break;

            // 설정 > 채널/VOD 설정
            case menuDataManager.MENU_ID.SETTING_FAVORITE_CHANNEL:
                if(KTW.CONSTANT.IS_OTS === true) {
                    if(result !== undefined && result !== null && result.length === 2) {
                        var iptvFavoriteList = result[0];
                        var skyFavoriteList = result[1];

                        if(iptvFavoriteList !== undefined && iptvFavoriteList !== null &&
                            iptvFavoriteList.length>=0 && skyFavoriteList !== undefined &&
                            skyFavoriteList !== null && skyFavoriteList.length>=0) {
                            if (language === "kor") {
                                subTitleText = (iptvFavoriteList.length+skyFavoriteList.length) + "개";
                            }else {
                                subTitleText = (iptvFavoriteList.length+skyFavoriteList.length);
                            }
                        }else {
                            if (language === "kor") {
                                subTitleText = "0개";
                            } else {
                                subTitleText = "0";
                            }
                        }
                    }

                }else {
                    if(result !== undefined && result !== null && result.length === 1) {
                        var iptvFavoriteList = result[0];
                        if(iptvFavoriteList !== undefined && iptvFavoriteList !== null &&
                            iptvFavoriteList.length>=0) {
                            if (language === "kor") {
                                subTitleText = iptvFavoriteList.length + "개";
                            } else {
                                subTitleText = iptvFavoriteList.length;
                            }
                        }else {
                            if (language === "kor") {
                                subTitleText = "0개";
                            }else {
                                subTitleText = "0";
                            }
                        }
                    }
                }
                log.printDbg("subTitle ==="+ subTitleText);
                settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);


                break;
            case menuDataManager.MENU_ID.SETTING_SKIPPED_CHANNEL:
                if(KTW.CONSTANT.IS_OTS === true) {

                    if(result !== undefined && result !== null && result.length === 2) {
                        var iptvJumpingList = result[0];
                        var skyJumpingList = result[1];

                        if(iptvJumpingList !== undefined && iptvJumpingList !== null &&
                            iptvJumpingList.length>=0 && skyJumpingList !== undefined &&
                            skyJumpingList !== null && skyJumpingList.length>=0) {
                            if(language === "kor") {
                                subTitleText = (iptvJumpingList.length+skyJumpingList.length) + "개";
                            } else {
                                subTitleText = (iptvJumpingList.length+skyJumpingList.length);
                            }
                        }else {
                            if(language === "kor") {
                                subTitleText = "0개";
                            } else {
                                subTitleText = "0 ";
                            }
                        }
                    }
                }else {
                    if(result !== undefined && result !== null && result.length === 1) {
                        var hiddenChannelList = result[0];
                        if(hiddenChannelList !== undefined && hiddenChannelList !== null && hiddenChannelList.length>=0) {
                            if(language === "kor") {
                                subTitleText = hiddenChannelList.length + "개";
                            } else {
                                subTitleText = hiddenChannelList.length;
                            }

                        }else {
                            if(language === "kor") {
                                subTitleText = "0개";
                            } else {
                                subTitleText = "0 ";
                            }
                        }
                    }
                }
                log.printDbg("subTitle ==="+ subTitleText);
                settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_CHANNEL_GUIDE:
                if(result !== undefined && result !== null && result.length === 1) {
                    var displayTime = Number(result[0]);
                    if(displayTime === 0) {
                        subTitleText = "0";
                    }else if(displayTime === 3000) {
                        subTitleText = "3";
                    }else if(displayTime === 5000) {
                        subTitleText = "5";
                    }else if(displayTime === 10000) {
                        subTitleText = "10";
                    }else if(displayTime === 15000) {
                        subTitleText = "15";
                    }
                    if(language === "kor") {
                        subTitleText += "초";
                    }else {
                        subTitleText += "s";
                    }
                }
                settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_DESCRIPTIVE_VIDEO_SERVICE:
                if(result !== undefined && result !== null && result.length === 2) {
                    if(result[0] === "0") {
                        subTitleText = "OFF";
                    }else {
                        subTitleText = "ON　|　";
                        if(result[1] === "kor") {
                            if(language == "kor") {
                                subTitleText += "한국어";
                            } else {
                                subTitleText += "Korean";
                            }
                        }else {
                            if(language == "kor") {
                                subTitleText += "영어";
                            } else {
                                subTitleText += "English";
                            }
                        }
                    }
                }else {
                    subTitleText = "OFF";
                }
                settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_CLOSED_CAPTION:
                subTitleMenuIdx = menuIdx;
                if(result !== undefined && result !== null && result.length === 3) {
                    if(result[0] === KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.OFF) {
                        subTitleText = "OFF";
                    }else {

                        if(result[2] === KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.BASIC) {
                            if(language == "kor") {
                                subTitleText = "방송:기본";
                            } else {
                                subTitleText = "Live:Default";
                            }
                        }else if(result[2] === KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.KOREAN) {
                            if(language == "kor") {
                                subTitleText = "방송:한국어";
                            } else {
                                subTitleText = "Live:Korean";
                            }
                        }else if(result[2] === KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.ENGLISH) {
                            if(language == "kor") {
                                subTitleText = "방송:영어";
                            } else {
                                subTitleText = "Live:English";
                            }
                        }

                        if(KTW.CONSTANT.IS_BIZ && isTranslationLanguageCaptionSupport) {
                            // 자동번역 언어에 대한 설정 값은 callback 에서 처리 . OFF 인 경우에는 표시하지 않는다.

                            settingDataAdapter.requestMashup_getCCLanguageList({ subTitleText: subTitleText, callback: cbRequestMashup_getCCLanguageList});
                        }
                    }

                }else {
                    subTitleText = "OFF";
                }

                   settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);
                break;
            case menuDataManager.MENU_ID.SETTING_SERIES_AUTO_PLAY:
                //2017.sw.nam 시리즈 자동 몰아보기 gui 변경, ON , OFF 로 바뀜 ( ON: 시리즈 자동 몰아보기, OFF: 자동 몰아보지 않음)
                if(result) {
                    log.printDbg("series_auto_play result = "+JSON.stringify(result));
                }
                if(result !== undefined && result !== null && result.length === 1) {
                    if(result[0] === "0") {
                        if(language == "kor") {
                            subTitleText = "OFF";
                        }else {
                            subTitleText = "OFF";
                        }
                    }else {
                        if(language == "kor") {
                            subTitleText = "ON";
                        }else {
                            subTitleText = "ON";
                        }
                    }
                }else {
                    if(language == "kor") {
                        subTitleText = "OFF";
                    }else {
                        subTitleText = "OFF";
                    }
                }
                settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);

                break;

            case menuDataManager.MENU_ID.SETTING_POSTER_SHAPE :
                switch (result[0]) {
                    case "default" :
                        if(language == "kor") {
                            subTitleText = "기본 설정";
                        }else {
                            subTitleText = "Default Setting";
                        }
                        break;
                    case "poster" :
                        if(language == "kor") {
                            subTitleText = "포스터로 보기";
                        }else {
                            subTitleText = "Poster View";
                        }
                        break;
                    case "list" :
                        if(language == "kor") {
                            subTitleText = "텍스트로 보기";
                        }else {
                            subTitleText = "Title View";
                        }
                        break;
                    default:
                        if(language == "kor") {
                            subTitleText = "기본 설정";
                        }else {
                            subTitleText = "Default Setting";
                        }
                        break;
                }
                settingMainLayer.setSubTitle("ChannelVod", menuIdx, subTitleText);

                break;

            //자녀안심 설정
            case menuDataManager.MENU_ID.SETTING_CHANGE_PIN:
                break;
            case menuDataManager.MENU_ID.SETTING_PARENTAL_RATING:

                var parentalString = "";
                var adultMenuHiddenString = "";
                if(result !== null && result !== null && result.length === 2) {
                    var parentalRating = result[0];

                    if (parentalRating === 0) {
                        if(language == "kor") {
                            parentalString = "제한 없음";
                        }else {
                            parentalString = "None";
                        }
                    } else if (parentalRating === 19) {
                        parentalString = "19";
                        if(language == "kor") {
                            parentalString += "세";
                        }
                    } else if (parentalRating === 15) {
                        parentalString = "15";
                        if(language == "kor") {
                            parentalString += "세";
                        }
                    } else if (parentalRating === 12) {
                        parentalString = "12";
                        if(language == "kor") {
                            parentalString += "세";
                        }
                    } else if (parentalRating === 7) {
                        parentalString = "7";
                        if(language == "kor") {
                            parentalString += "세";
                        }
                    }
                    var tempData =  result[1];
                    if(tempData === "true") {
                        if(language === "kor"){
                            adultMenuHiddenString = "성인 메뉴 표시";
                        }else {
                            adultMenuHiddenString = "Display Adult Menu";
                        }
                    }else {
                        if(language === "kor"){
                            adultMenuHiddenString = "성인 메뉴 숨김";
                        }else {
                            adultMenuHiddenString = "Hide Adult Menu";
                        }
                    }
                }
                subTitleText = parentalString + "　|　" + adultMenuHiddenString;
                settingMainLayer.setSubTitle("Kids", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_TIME_LIMIT:
                var block_time = [];
                var split_value = null;
                var subtitleString = "";
                if(result !== null && result !== null && result.length === 1) {
                    split_value = result[0].split("|");
                }else {
                    var save_value = "N";
                    save_value += "|" + KTW.utils.util.getFormatDate(new Date, "yyyyMMdd") + "|" + "0000" + "|" + "0000" + "|" + "0" + "|" + "N";
                    split_value = save_value.split("|");
                }

                if(split_value !== undefined && split_value !== null && split_value.length === 6) {
                    block_time[0] = (split_value[0] === "Y") ? true : false;
                    block_time[1] = (split_value[4] === "0") ? true : false;
                    block_time[2] = split_value[2];
                    block_time[3] = split_value[3];

                    block_time[0] = (split_value[5] === "Y") ? true : block_time[0];
                }else {
                    block_time[0] = false;
                    block_time[1] = false;
                    block_time[2] = "0000";
                    block_time[3] = "0000";
                    block_time[4] = "N";
                    block_time[5] = "N";
                }

                if (block_time !== undefined && block_time !== null && block_time[0] === false) {
                    if(language == "kor") {
                        subtitleString = "해제";
                    }else {
                        subtitleString = "OFF";
                    }
                } else if(block_time !== undefined && block_time !== null) {
                    if(language == "kor") {
                        subtitleString = "제한시간 ";
                    }
                    subtitleString += block_time[2].substring(0, 2);
                    subtitleString += ":";
                    subtitleString += block_time[2].substring(3);
                    subtitleString += "-";
                    subtitleString += block_time[3].substring(0, 2);
                    subtitleString += ":";
                    subtitleString += block_time[3].substring(3);
                    subtitleString += "　|　";
                    if (block_time[1] === true) {
                        if(language == "kor") {
                            subtitleString += "매일";
                        }else {
                            subtitleString += "Everyday";
                        }
                    }else {
                        if(language == "kor") {
                            subtitleString += "한번만";
                        }else {
                            subtitleString += "Once";
                        }
                    }
                }
                subTitleText = subtitleString;
                settingMainLayer.setSubTitle("Kids", menuIdx, subTitleText);

                break;
            case menuDataManager.MENU_ID.SETTING_BLOCKED_CHANNEL:
                if(KTW.CONSTANT.IS_OTS === true) {
                    if(result !== undefined && result !== null && result.length === 2) {
                        var iptvBlockList = result[0];
                        var skyBlockList = result[1];

                        if(iptvBlockList !== undefined && iptvBlockList !== null &&
                            iptvBlockList.length>=0 && skyBlockList !== undefined &&
                            skyBlockList !== null && skyBlockList.length>=0) {
                            subTitleText = (iptvBlockList.length+skyBlockList.length);
                        }else {
                            subTitleText = "0";
                        }
                    }
                }else {
                    if(result !== undefined && result !== null && result.length === 1) {
                        var iptvBlockList = result[0];
                        if(iptvBlockList !== undefined && iptvBlockList !== null &&
                            iptvBlockList.length>=0) {
                            subTitleText = iptvBlockList.length;
                        }else {
                            subTitleText = "0";
                        }
                    }
                }
                if(language == "kor") {
                    subTitleText += "개";
                }
                settingMainLayer.setSubTitle("Kids", menuIdx, subTitleText);

                break;

            default:
                break;
        }

        return subTitleText;
    }

    /**
     *  2017.05.08 sw.nam
     *  설정 > 시스템설정 > 언어 설정 메뉴에서 언어 변경 시 설정 전체 메뉴의 Title 과 subTitle 를 refreshing 하기 위한 함수
     */
    function _updateAllSettingMenuSubtitle() {
        log.printDbg("_updateAllSettingMenuSubtitle()");
        var settingMenu;
        var childSettingMenu;
        var menuIdx =0;

        if (!settingMenu) {
            settingMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.catType === KTW.DATA.MENU.CAT.CONFIG) {
                        return true;
                    }
                }
            })[0];
        }
        for(var i =0; i < settingMenu.children.length; i++){
            childSettingMenu = settingMenu.children[i];
            log.printDbg("i__ "+i);
            for(var j =0; j <childSettingMenu.children.length; j++ ){
                log.printDbg("j__ "+j);
                updateSettingMenuSubtitle({menuId: childSettingMenu.children[j].id, index: menuIdx});
                menuIdx++;
                log.printDbg("menuIdx "+menuIdx);
            }
            menuIdx = 0;
        }
    }

    /**
     *  2017.05.26 sw.nam
     *  각 설정 메뉴 카테고리의 children 갯수 만큼 menuList 생성 해 준다.
     *  subTitle을 갱신 할 때 callback 형식으로 된 메뉴들을 처리해야 하는 경우가 있어 일관성이 떨어지므로 먼저 메뉴 list의 구조만
     *  생성 한 뒤 subTitle 를 갱신하는 구조로 변경
     *  * @param settingMenuLength 만들어질 각 설정 메뉴 리스트 아이템의 갯수
     *  return 생성 된 menuList 구조 리턴
     */
    function _createSettingMenuList(settingMenuLength) {
        log.printDbg("_createSettingMenuList() "+settingMenuLength);
        var menuList = [];

        for(var i = 0; i< settingMenuLength; i++) {
            menuList[i] = {id: undefined, title: undefined, subTitle: undefined};
        }
        return menuList;
    }

    /**
     *  2017.05.26 sw.nam
     * 생성된 menuList 에 실제 설정 메뉴 데이터(title, subTitle) 를 update 한다.
     * @param settingMenu : update 할 menu 카테고리
     * @returns {Array} : 결과값이 담긴 menuList
     */
    function _updateSettingMenuItem(settingMenu,list) {
        log.printDbg("_updateSettingMenuItem()"+settingMenu);
        var menuList = list;
        var menuId;
        var language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
        var name;

        for(var i = 0; i < settingMenu.children.length; i++) {
            log.printDbg("language : "+language);
            if (language === "kor") {
                name = settingMenu.children[i].name;
            } else {
                name = settingMenu.children[i].englishItemName;
            }
            menuId = menuList[i].id = settingMenu.children[i].id;
            menuList[i].title = name;
            menuList[i].subTitle = updateSettingMenuSubtitle({menuId: menuId, index: i});
        }
        return menuList;
    }

    /** 2017.05.30 sw.nam 추가
     * 설정 > 시스템 설정 내 히든 메뉴 리스트를 생성한다. ( OTS , UHD3)
     *
     * 설정 hidden 메뉴 구성 규칙
     * ~UHD2 셋탑의 경우 비콘 수신 설정 모드 존재 x
     * UHD3 이상의 셋탑인 경우에만 경우에만 비콘 수신모드가 존재
     * UHD3 이상이면서 OTS 박스인 경우 두가지 메뉴 모두 존재.
     *
     *
     * @param settingMenu : 시스템 설정 메뉴 데이터 object
     * @returns {Array} : 생성 메뉴 리스트 리턴
     * @private
     */
    function _createHiddenSettingMenu(settingMenu) {
        log.printDbg("_createHiddenSettingMenu()");
        var menuList = [];
        var OTSHiddenMenu;
        var UHDBeaconSettingMenu;

        if (KTW.CONSTANT.IS_OTS && !KTW.CONSTANT.UHD3.BEACON) {
            OTSHiddenMenu = settingMenu.hiddenChildren[0].children; // 하위 메뉴 다섯개
        } else if (KTW.CONSTANT.IS_OTS && KTW.CONSTANT.UHD3.BEACON) {
            OTSHiddenMenu = settingMenu.hiddenChildren[0].children; // 하위 메뉴 다섯개
            UHDBeaconSettingMenu = settingMenu.hiddenChildren[1];
        } else if (!KTW.CONSTANT.IS_OTS && KTW.CONSTANT.UHD3.BEACON) {
            UHDBeaconSettingMenu = settingMenu.hiddenChildren[0];
        }

        if(OTSHiddenMenu && KTW.CONSTANT.IS_OTS ) {
            // UHD3 이 아닌 OTS
            for(var i = 0; i < OTSHiddenMenu.length; i++) {
                menuList[i] = {id: OTSHiddenMenu[i].id, title: OTSHiddenMenu[i].name, subTitle: undefined};
                menuList[i].subTitle = updateSettingMenuSubtitle({menuId: OTSHiddenMenu[i].id, index: i, isForCreate: true });
            }
            if(UHDBeaconSettingMenu && KTW.CONSTANT.UHD3.BEACON) {
                // OTS & UHD3.BEACON 인 경우 해당 분기로 들어옴
                menuList[OTSHiddenMenu.length] = {id: UHDBeaconSettingMenu.id, title: UHDBeaconSettingMenu.name, subTitle: undefined};
                menuList[OTSHiddenMenu.length].subTitle = updateSettingMenuSubtitle({ menuId: UHDBeaconSettingMenu.id, index: 0, isForCreate: true });
            }
        } else if(UHDBeaconSettingMenu && !KTW.CONSTANT.IS_OTS && KTW.CONSTANT.UHD3.BEACON) {
            // OTV && UHD3 셋탑인 경우
            menuList[0] = {id: UHDBeaconSettingMenu.id, title: UHDBeaconSettingMenu.name, subTitle: undefined};
            menuList[0].subTitle = updateSettingMenuSubtitle({ menuId: UHDBeaconSettingMenu.id, index: 0, isForCreate: true });
        }
        return menuList;
    }

    /**
     * 2017.07.05 sw.nam 설정 메뉴 index 찾는 함수
     * @param settingmenu : 찾을 메뉴 카테고리 list
     * @param menuId : 타겟 menu ID
     * @returns
     * @private
     */
    function _getSettingMenuIndex(settingMenu,menuId) {
        log.printDbg("_getSettingMenuIndex() " + menuId);
        var catchFlag = false;
        var index;
        for(var i =0; i < settingMenu.children.length; i++) {
            if(settingMenu.children[i].id == menuId) {
                index = i;
                catchFlag = true;
                break;
            }
        }
        if(catchFlag) {
            return index;
        }else {
            return -1;
        }
    }

    /**
     * sw.nam 설정 > 알림메시지 용 callback 함수
     * @param success
     * @param result
     */
    function cbGetBMailInfo(success,result) {
        log.printDbg("cbGetBMailInfo()");

        request_object = undefined;
        var value;
        var subTitle;

        if (success == false && result == "abort") {
            // [jh.lee] ajaxStop 시키는 경우 후처리 중지
            log.printDbg("success : " + success);
            log.printDbg("result : " + result);
            return;
        }

        if (success) {
            if (result.result === "0") {
                if (result.exist === "F") {
                    value = 0;
                } else {
                    value = 1;
                }
            }
        } else {
            value = 1;
        }

        if(!value) {
            subTitle = "ON";
        } else{
            subTitle = "OFF";
        }
        KTW.ui.LayerManager.getLayer("SettingMain").setSubTitle("System", bMailMenuIdx, subTitle);

    }

    function setAutoPowerText(data) {
        log.printDbg("setAutoPowerText");

        var text;
        var result = data;
        var fromHH, fromMM, toHH, toMM, isRepeat;
        //var language = KTW.ui.adaptor.SettingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.LANGUAGE);
        var language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();

        fromHH = result[1].substring(0, 2);
        fromMM = result[1].substring(2);
        toHH = result[2].substring(0, 2);
        toMM = result[2].substring(2);

        if ( (result === undefined || result.length === 0 || result === "") || ( fromHH === toHH && fromMM === toMM )) {

            text = "OFF";
        }
        else {
            if (result[0] === "true") {
                if(language == "kor") {
                    isRepeat = "　|　매일";
                }else {
                    isRepeat = "　|　Everyday";
                }
            } else if(result[0] === "false") {
                if(language == "kor") {
                    isRepeat = "　|　한번만";
                }else {
                    isRepeat = "　|　Once";
                }
            } else if(result[0] === "selective") {
                if(language == "kor") {
                    isRepeat = "　|　요일 선택";
                }else {
                    isRepeat = "　|　By day";
                }
            }

            if(language == "kor") {
                text = "켜짐 "+ fromHH +":"+ fromMM +", 꺼짐 "+ toHH +":"+ toMM + isRepeat;
            }else {
                text = "ON "+ fromHH +":"+ fromMM +", OFF "+ toHH +":"+ toMM + isRepeat;
            }
        }
        return text;
    }

    /**
     * 자동번역언어 받아오는 콜백 함수
     * @param subtitle
     * @param msg
     * @returns {*}
     */
    function cbRequestMashup_getCCLanguageList(options) {
        log.printDbg("cbRequestMashup_getCCLanguageList" +JSON.stringify(options));

        if(!options) {
            log.printDbg("options are null.. return");
        }

        var text = options.subTitleText;
        var message = options.msg;
        var list;
        var onLang;
        var language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();


        if(message) {

            list = message.list;
            onLang = message.onLang;

            if(onLang == "NONE") {
                if(language == "kor") {
                    text += "　|　사용 안함";
                }else {
                    text += "　|　"+onLang;
                }
            }else {
                for(var i =0; i<list.length; i++) {
                    if(list[i].code == onLang) {
                        if(language == "kor") {
                            text += "　|　"+list[i].name;
                        }else {
                            var tmpText = list[i].name.split('(');
                            tmpText = tmpText[1].split(')');
                            text += "　|　Auto:"+tmpText[0];
                        }
                        break;
                    }
                }
            }
        }else {
            //isTranslationLanguageCaptionSupport = false;
        }
        log.printDbg("text = "+text);
        KTW.ui.LayerManager.getLayer("SettingMain").setSubTitle("ChannelVod", subTitleMenuIdx, text);
    }


    function getLayerIdByMenuID(menu_id) {
        var layerID;
        var menuDataManager = KTW.managers.data.MenuDataManager;
        switch(menu_id) {
            case menuDataManager.MENU_ID.KIDS_CARE_SETTING:
                layerID =  KTW.ui.Layer.ID.SETTING_MAIN;
                break;
            case menuDataManager.MENU_ID.CHANNEL_SETTING:
                layerID =  KTW.ui.Layer.ID.SETTING_MAIN;
                break;
            case menuDataManager.MENU_ID.SYSTEM_SETTING:
                layerID =  KTW.ui.Layer.ID.SETTING_MAIN;
                break;
            //설정 > 자녀안심 설정
            case menuDataManager.MENU_ID.SETTING_CHANGE_PIN:
                layerID = KTW.ui.Layer.ID.SETTING_PASSWORD;
                break;
            case menuDataManager.MENU_ID.SETTING_PARENTAL_RATING:
                layerID = KTW.ui.Layer.ID.SETTING_PARENTAL_RATING;
           //     isRequiredAuth = true;
                break;
            case menuDataManager.MENU_ID.SETTING_TIME_LIMIT:
                layerID = KTW.ui.Layer.ID.SETTING_LIMIT_WATCH;
       //         isRequiredAuth = true;
                break;
            case menuDataManager.MENU_ID.SETTING_BLOCKED_CHANNEL:
                layerID = KTW.ui.Layer.ID.SETTING_BLOCKED_CHANNEL;
         //       isRequiredAuth = true;
                break;
            //설정 > 채널/VOD 설정
            case menuDataManager.MENU_ID.SETTING_FAVORITE_CHANNEL:
                layerID = KTW.ui.Layer.ID.SETTING_FAVORITE_CHANNEL;
                break;
            case menuDataManager.MENU_ID.SETTING_SKIPPED_CHANNEL:
                if (KTW.CONSTANT.IS_OTS) {
                    layerID = KTW.ui.Layer.ID.SETTING_JUMPING_CHANNEL;
                } else {
                    layerID = KTW.ui.Layer.ID.SETTING_SKIPPED_CHANNEL;
                }
                break;
            case menuDataManager.MENU_ID.SETTING_CHANNEL_GUIDE:
                layerID = KTW.ui.Layer.ID.SETTING_MINIGUIDE_DISPLAY_TIME;
                break;
            case menuDataManager.MENU_ID.SETTING_DESCRIPTIVE_VIDEO_SERVICE:
                layerID = KTW.ui.Layer.ID.SETTING_SCREEN_NARRATION;
                break;
            case menuDataManager.MENU_ID.SETTING_CLOSED_CAPTION:
                layerID = KTW.ui.Layer.ID.SETTING_SUBTITLE;
                break;
            case menuDataManager.MENU_ID.SETTING_SERIES_AUTO_PLAY:
                layerID = KTW.ui.Layer.ID.SETTING_SERIES_AUTO_PLAY;
                break;
            //설정 > 시스템 설정
            case menuDataManager.MENU_ID.SETTING_ASPECT_RATIO :
                layerID = KTW.ui.Layer.ID.SETTING_SCREEN_RATIO;
                break;
            case menuDataManager.MENU_ID.SETTING_RESOLUTION :
                layerID = KTW.ui.Layer.ID.SETTING_RESOLUTION;
                break;
            case menuDataManager.MENU_ID.SETTING_AUTO_POWER :

                layerID = KTW.ui.Layer.ID.SETTING_AUTO_POWER;
                break;
            case menuDataManager.MENU_ID.SETTING_STANDBY_MODE :

                layerID = KTW.ui.Layer.ID.SETTING_AUTO_STANDBY;
                break;
            case menuDataManager.MENU_ID.SETTING_SOUND :

                layerID = KTW.ui.Layer.ID.SETTING_SOUND_TYPE;
                break;
            case menuDataManager.MENU_ID.SETTING_GIGA_GINI_AUDIO_OUTPUT :

                layerID = KTW.ui.Layer.ID.SETTING_AUDIO_OUTPUT;
                break;
            case menuDataManager.MENU_ID.SETTING_VOICE_GUIDE :
                layerID = KTW.ui.Layer.ID.SETTING_VOICE_GUIDE;
                break;
            case menuDataManager.MENU_ID.SETTING_USB :

                layerID = KTW.ui.Layer.ID.SETTING_USB_CONNECT;
                break;
            case menuDataManager.MENU_ID.SETTING_BLUETOOTH :

                layerID = KTW.ui.Layer.ID.SETTING_BLUETOOTH_CONNECT;
                break;
            case menuDataManager.MENU_ID.SETTING_POSTER_SHAPE :

                layerID = KTW.ui.Layer.ID.SETTING_SHOW_OPTION;
                break;
            case menuDataManager.MENU_ID.SETTING_ALERT_MESSAGE :

                layerID = KTW.ui.Layer.ID.SETTING_ALERT_MESSAGE;
                break;
            case menuDataManager.MENU_ID.SETTING_HDMI_CEC :

                layerID = KTW.ui.Layer.ID.SETTING_HDMI_SYNCHRONIZE;
                break;
            case menuDataManager.MENU_ID.LANGUAGE :

                layerID = KTW.ui.Layer.ID.SETTING_DEFAULT_LANGUAGE;
                break;
            case menuDataManager.MENU_ID.SETTING_SYSTEM_INITIALIZE :

                layerID = KTW.ui.Layer.ID.SETTING_SYSTEM_INITIALIZATION;
                break;
            case menuDataManager.MENU_ID.SETTING_START_CHANNEL :

                layerID = KTW.ui.Layer.ID.SETTING_START_CHANNEL;
                break;
            case menuDataManager.MENU_ID.SETTING_SYSTEM_INFO :

                layerID = KTW.ui.Layer.ID.SETTING_SYSTEM_INFO;
                break;
            case menuDataManager.MENU_ID.TRANSPONDER : // 중계기(OTS)
                layerID = KTW.ui.Layer.ID.SETTING_REPEATER;
                break;
            case menuDataManager.MENU_ID.COLLECT_SIGNAL : // 수신품질 측정 (OTS)

                layerID = KTW.ui.Layer.ID.SETTING_RECEIVE_CHECKING;
                break;
            // 설정 > 시스템설정 > OTS 관리자 설정 메뉴 .. 외부에서 메뉴 점프시 사용하지 않는다.
            case menuDataManager.MENU_ID.SKY_LNB:
                layerID = KTW.ui.Layer.ID.SETTING_LNB;
                break;
            case menuDataManager.MENU_ID.SKY_EMERGENCY:
                layerID = KTW.ui.Layer.ID.SETTING_EMERGENCY_ALERT;
                break;
            case menuDataManager.MENU_ID.SKY_CHANNEL_INFO:
                layerID = KTW.ui.Layer.ID.SETTING_CHANNEL_INFO_OUTPUT;
                break;
            case menuDataManager.MENU_ID.SKY_CLOSED_CAPTION:
                layerID = KTW.ui.Layer.ID.SETTING_CLOSED_CAPTION;
                break;
            case menuDataManager.MENU_ID.SKY_CAS:
                layerID = KTW.ui.Layer.ID.SETTING_CAS_INFO;
                break;
            case menuDataManager.MENU_ID.SETTING_BEACON:
                layerID = KTW.ui.Layer.ID.SETTING_BEACON;
                break;
        }

        return layerID;

    }


    return {
        activateSettingMenuByMenuID : _activateSettingMenuByMenuID,
        historyBackAndPopup: _historyBackAndPopup,
        updateAllSettingMenuSubtitle: _updateAllSettingMenuSubtitle,
        createHiddenSettingMenu: _createHiddenSettingMenu,
        createSettingMenuList: _createSettingMenuList,
        updateSettingMenuItem: _updateSettingMenuItem,
        getSettingMenuIndex: _getSettingMenuIndex
    };

})();