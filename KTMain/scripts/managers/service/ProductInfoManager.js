/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AppServiceManager</code>
 *
 * @author dj.son
 * @since 2016-09-08
 */

KTW.managers.service.ProductInfoManager = (function () {

    function _getCustEnvOnLocalStorage () {
        var custEnv = null;

        try {
            custEnv = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CUST_ENV));
        }
        catch (e) {
            KTW.utils.Log.printErr(e);
        }

        return custEnv;
    }

    /**
     * [dj.son] 국방 상품
     */
    function _isMilitary () {

        var bMilitary = false;

        if (KTW.CONSTANT.IS_BIZ) {
            var custEnv = _getCustEnvOnLocalStorage();

            if (custEnv && custEnv.pkgCode && (custEnv.pkgCode === "2P15" || custEnv.pkgCode === "2S0R")) {
                bMilitary = true;
            }
        }

        return bMilitary;
    }

    /**
     * [dj.son] 숙박 상품
     */
    function _isLodge () {
        var bLodge = false;

        // [kh.kim] 숙박 상품 여부는 상품 코드만 가지고 판단하도록 수정
        // if (KTW.CONSTANT.IS_BIZ) {
            var custEnv = _getCustEnvOnLocalStorage();

            if (custEnv && custEnv.pkgCode && (custEnv.pkgCode === "2P56" || custEnv.pkgCode === "2S60"
                || custEnv.pkgCode === "2S61" || custEnv.pkgCode === "2S62" || custEnv.pkgCode === "2S63" || custEnv.pkgCode === "2S64"
                || custEnv.pkgCode === "2S65" || custEnv.pkgCode === "2S1T" || custEnv.pkgCode === "2S1V" || custEnv.pkgCode === "2S2A"
                || custEnv.pkgCode === "2S2C" || custEnv.pkgCode === "2PHB" || custEnv.pkgCode === "2PHD" || custEnv.pkgCode === "2PHF"
                || custEnv.pkgCode === "2S2E" || custEnv.pkgCode === "2S2G")) {
                bLodge = true;
            }
        // }

        return bLodge;
    }

    /**
     * [dj.son] 비즈형 일반 매장 상품
     */
    function _isStore () {
        var bStore = false;

        // [kh.kim] 일반 매장 상품 여부는 상품 코드만 가지고 판단하도록 수정
        // if (KTW.CONSTANT.IS_BIZ) {
            var custEnv = _getCustEnvOnLocalStorage();

            if (custEnv && custEnv.pkgCode && (custEnv.pkgCode === "2P92" || custEnv.pkgCode === "2S66"
                    || custEnv.pkgCode === "2S67" || custEnv.pkgCode === "2S68" || custEnv.pkgCode === "2S69" || custEnv.pkgCode === "2S70"
                    || custEnv.pkgCode === "2S71" || custEnv.pkgCode === "2S1U" || custEnv.pkgCode === "2S1W" || custEnv.pkgCode === "2S2B"
                    || custEnv.pkgCode === "2S2D" || custEnv.pkgCode === "2PHC" || custEnv.pkgCode === "2PHE" || custEnv.pkgCode === "2PHG"
                    || custEnv.pkgCode === "2S2F" || custEnv.pkgCode === "2S2H")) {
                bStore = true;
            }
        // }

        return bStore;
    }

    /**
     * [dj.son] 교육 상품
     */
    function _isEdu () {

        var bEdu = false;

        // [kh.kim] 교육 상품 여부는 상품 코드만 가지고 판단하도록 수정
        // if (KTW.CONSTANT.IS_BIZ) {
            var custEnv = _getCustEnvOnLocalStorage();

            if (custEnv && custEnv.pkgCode && custEnv.pkgCode === "2P51") {
                bEdu = true;
            }
        // }

        return bEdu;
    }

    /**
     * [dj.son] 멀티룸 패키지
     */
    function _isMultiroomPackage () {

        var bultiroomPackage = false;

        // [kh.kim] multiroomPackage 여부는 Biz 상품 여부와 상관 없음.
        // if (KTW.CONSTANT.IS_BIZ) {
            var custEnv = _getCustEnvOnLocalStorage();

            if (custEnv && custEnv.pkgCode && (custEnv.payType === "2" || custEnv.pkgType === "5")) {
                bultiroomPackage = true;
            }
        // }

        return bultiroomPackage;
    }

    /**
     * [dj.son] ACAP 로직 적용, 아래 조건문 적용시 사용
     *
     * - 홈메뉴 페어링 비활성화
     * - 마이메뉴 쿠폰/포인트 조회 항목 미표시
     * - 상세화면 하단 / 홈메뉴 하단에 포인트 정보 미노출
     * - 상세화면 하단 / 홈메뉴 하단에 포인트 정보 미노출
     * - OTM 페어링 문구 미노출
     */
    function _isBiz () {

        var bBiz = false;

        if (KTW.CONSTANT.IS_BIZ) {
            var custEnv = _getCustEnvOnLocalStorage();

            if ((custEnv && custEnv.pkgCode && custEnv.pkgType !== "1") || _isMilitary()) {
                bBiz = true;
            }
        }

        return bBiz;
    }

    return {
        getCustEnvOnLocalStorage: _getCustEnvOnLocalStorage,

        isMilitary: _isMilitary,
        isLodge: _isLodge,
        isStore: _isStore,
        isEdu: _isEdu,
        isMultiroomPackage: _isMultiroomPackage,
        isBiz: _isBiz
    };
}());