/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>KidsModeManager</code>
 *
 * @author dj.son
 * @since 2017-02-15
 */

KTW.managers.service.KidsModeManager = (function () {

    var log = KTW.utils.Log;
    //var util = KTW.utils.util;

    var Def = null;
    var basicAdapter = null;
    var moduleManager = null;
    var kidsModuleID = null;
    var storageManager = null;

    var kidsMode = undefined;
    var isChanging = false;
    var changingTimer = null;

    /**
     * [dj.son] 채널 이벤트 수신 후 카테고리 점프를 위한 변수
     */
    var jumpData = null;

    /** PR 변경 시간 동안 mmi event 를 무시하기 위한 flag */
    var isChangingPR = false;
    var DEFAULT_VALUE = {
        KIDS_PARENTAL_RATION: 15,
        KIDS_AUTO_STANDBY_MODE: "ON",
        KIDS_VOD_LIST_MODE: "default"
    };

    function _init () {
        log.printDbg("_init()");

        Def = KTW.oipf.Def;
        basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        moduleManager = KTW.managers.module.ModuleManager;
        storageManager = KTW.managers.StorageManager;
        kidsModuleID = KTW.managers.module.Module.ID.MODULE_KIDS;

        kidsMode = basicAdapter.getConfigText(Def.CONFIG.KEY.USE_KIDS_MODE);

        if (kidsMode === Def.KIDS_MODE.ON && !KTW.managers.data.MenuDataManager.isKidsMenuExist()) {
            _changeMode({
                kidsMode: Def.KIDS_MODE.OFF,
                isInit: true
            });
        }
    }

    /**
     * 키즈 모드 ON, OFF
     * - 키즈 모드 변경시, 홈 화면 노출
     * - OTS 일 경우, 서퍼블 채널링 강제 update (서퍼블 채널링이 rebuild 될때, channel list update event 안올라옴)
     * - 모드 변경시 HomeMenu or KidsHome 화면을 activate
     *
     * @param options - kidsMode : 변경될 KidsMode (ON, OFF)
     *                 - isInit : 키즈모드 ON 으로 부팅 상황임을 알려주는 플래그
     *                 - isKidsModule: 키즈 모듈로부터 호출된 상황임을 알려주는 플래그 (기존 시나리오대로 채널 튠 될거라 가정)
     *                 - onlyChangeSetting : 세팅값 변경만을 위한 플래그
     *                 - jumpData : 모드 변경 후 수행될 메뉴 데이터 (MenuServiceManager 의 jumpByMenuData 사용
     *
     *                 ex) changeMode({
     *                      kidsMode: KTW.oipf.Def.KIDS_MODE.ON,
     *                      isInit: true,
     *                      isKidsModule: true,
     *                      onlyChangeSetting: true,
     *                      jumpData: {itemType: 0, cat_id: ".."}
     *                 });
     *
     *
     * [dj.son] [WEBIIIHOME-2135] 네비게이터에서는 키즈모드 ON 으로 전환시 채널 튠 수행, OFF 로 전환시에는 아무런 동작을 하지 않는다.
     * 따라서 ON 으로 전환시에는 channel event 받은 다음 홈메뉴를 띄우고, OFF 로 전환시에는 곧바로 홈메뉴를 띄운다.
     *
     * [dj.son] [2017-06-22] navigator 에서는 키즈모드 전환시 항상 채널 튠하는 것으로 로직 수정
     * 따라서 ON 이든 OFF 든 항상 channel event 받은 다음 홈메뉴 띄우도록 수정
     */
    function _changeMode (options) {
        log.printDbg("_changeMode(" + log.stringify(options) + ")");

        if (!options || !options.kidsMode) {
            log.printDbg("options is null.... so return");
            return;
        }

        if (options.kidsMode !== Def.KIDS_MODE.ON && options.kidsMode !== Def.KIDS_MODE.OFF) {
            log.printDbg("not valid kids mode options.... so return");
            return;
        }

        if (kidsMode === options.kidsMode) {
            log.printDbg("same kids mode... so return");
            return false;
        }

        if (options.isInit) {
            basicAdapter.setConfigText(Def.CONFIG.KEY.USE_KIDS_MODE, options.kidsMode);
            kidsMode = options.kidsMode;
            return;
        }


        isChanging = true;

        jumpData = options.jumpData;

        /**
         * [dj.son] 큰 이슈가 없는 공통 로직은 앞에 위치하도록 수정
         */

        // TODO 2017.06.07 키즈모드로 전환 시와 일반모드로 전환시의 설정을 백업한 후 restore 해야 할 수 있음
        //changeParentalRating(options.kidsMode);
        changeSettings(options.kidsMode);

        /**
         * [dj.son] 키즈모듈 data init 을 위해 호출
         */
        if (options.kidsMode === Def.KIDS_MODE.OFF) {
            var kidsModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_KIDS);

            if (kidsModule) {
                kidsModule.execute({method: "kidsInitSetting"});
            }
        }

        if (options.onlyChangeSetting) {
            /**
             * [dj.son] 오직 세팅값만 변경하고 화면 띄우지 않음
             * - 해당 케이스는 키즈 채널 UP / DOWN 키가 눌렸을때, 네비게이터에서 postMessage 로 전달하는 케이스
             */

            basicAdapter.setConfigText(Def.CONFIG.KEY.USE_KIDS_MODE, options.kidsMode);
            kidsMode = options.kidsMode;

            isChanging = false;
        }
        else {
            /**
             * [dj.son] 기존에는 키즈 모드 전환시 네비게이터에서 무조건 키즈 포커스 채널로 튠 했으나, 키즈 리모컨으로 동작된 경우 경우에 따라서는 튠 안함
             *
             * - 현재 키즈 채널인 경우, 네비게이터에서 튠을 하지 않기로 합의 (그 외에는 포커스 채널로 튠)
             * - 현재 재생중인 VOD 가 키즈 VOD 인 경우, 네비게이터에서 튠 하지 않기로 합의 (그 외에는 포커스 채널로 튠)
             */
            var bTune = true;

            /**
             * [dj.son] [WEBIIIHOME-3776] 기존 키즈 모드 전환 시나리오는 채널 튠이 됨
             */
            if (options.kidsMode === Def.KIDS_MODE.ON && !options.isKidsModule) {
                if (KTW.managers.service.StateManager.isVODPlayingState()) {
                    // TODO 현재 재생중인 VOD 가 키즈 VOD 인지 아닌지 판단
                    var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (vodModule && vodModule.execute({ method: "isKidsVOD" })) {
                        bTune = false;
                    }
                }
                else {
                    if (KTW.oipf.AdapterHandler.navAdapter.isKidsChannel(KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel())) {
                        bTune = false;
                    }
                }
            }

            if (bTune) {
                /**
                 * [dj.son] 기존 로직
                 */
                KTW.ui.LayerManager.clearNormalLayer();
                setTimeout(function () {
                    KTW.ui.LayerManager.startLoading({preventKey: true});
                }, 0);

                /**
                 * jjh1117 2017/08/02
                 * 키즈모드 설정/해제 시 Main VBO의 Permmision를 초기화 해준다.
                 * lastperm 비교 로직상 초기화를 하지 않으면 문제가 발생함.
                 *
                 * [dj.son] [WEBIIIHOME-3168] 동작 모드 전환 후 곧바로 키즈모드 ON / OFF 시, 같은 채널로 튠 될 경우 홈메뉴 늦게 뜨는 이슈 수정
                 * - 동작 모드 전환시 미니 가이드 뜨지 않게 하기 위해 last_only_select 를 true 로 설정하는데,
                 *   이때 같은 채널로 튠 될 경우 _setModeChangingFlag 을 호출하는 로직을 안타게 됨
                 * - 따라서, 키즈 모드 ON / OFF 시 last_only_select 를 false 로 설정하도록 수정
                 */
                KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).resetPerm(true);

                setTimeout(function () {
                    basicAdapter.setConfigText(Def.CONFIG.KEY.USE_KIDS_MODE, options.kidsMode);
                    kidsMode = options.kidsMode;
                }, 10);

                // [dj.son] 키즈 모드 ON 으로 전환시 navigator 에서 강제 tune 을 해줌 (같은 채널이라도)
                // 따라서 isChanging flag 의 값 변환은 ChannelControl 에서 실행
                // 혹시 모르니 channel event 가 올라오는 충분한 시간 후에 flag set 하도록 수정
                changingTimer = setTimeout(function () {
                    _setModeChangingFlag(false);
                }, 10 * 1000);
            }
            else {
                /**
                 * [dj.son] 네비게이터에서 튠 하지 않는 경우, 모드 change 하고 곧바로 화면 노출
                 */

                basicAdapter.setConfigText(Def.CONFIG.KEY.USE_KIDS_MODE, options.kidsMode);
                kidsMode = options.kidsMode;

                _setModeChangingFlag(false);
            }



        }
    }

    /**
     * 2017.06.09 dhlee
     * 일반 모드 <-> 키즈 모드 변경 시에 변경되는 설정 정보등을 Local Storage 영역에 save/restore 하는 역할을 수행한다.
     * Parental Rating, 자동 대기 모드, VOD 보기 옵션, 알림 메시지
     *
     * @param kidsMode
     */
    function changeSettings(kidsMode) {
        log.printDbg("changeSettings(), kidsMode = " + kidsMode);

        changeParentalRating(kidsMode);

        if (kidsMode === Def.KIDS_MODE.ON) {
            // first, 자동전원 기능이 동작하지 않도록 관련 timer reset
            var vodListMode = storageManager.ps.load(storageManager.KEY.VOD_LIST_MODE);
            if (vodListMode === null || vodListMode === undefined) {
                vodListMode = DEFAULT_VALUE.KIDS_VOD_LIST_MODE;
            }
            var standbyMode = basicAdapter.getConfigText(Def.CONFIG.KEY.AUTO_STANDBY);
            if (standbyMode === null || standbyMode === undefined) {
                standbyMode = DEFAULT_VALUE.KIDS_AUTO_STANDBY_MODE;
            }
            KTW.managers.device.PowerModeManager.resetAutoPowerMode(false);
            storageManager.ps.save(storageManager.KEY.NORMAL_STANDBY_MODE, standbyMode);
            basicAdapter.setConfigText(Def.CONFIG.KEY.AUTO_STANDBY, DEFAULT_VALUE.KIDS_AUTO_STANDBY_MODE);
            storageManager.ps.save(storageManager.KEY.NORMAL_VOD_LIST_MODE, vodListMode);
            storageManager.ps.save(storageManager.KEY.VOD_LIST_MODE, DEFAULT_VALUE.KIDS_VOD_LIST_MODE);
            changeAlertMessage(kidsMode);
        }
        else if (kidsMode === Def.KIDS_MODE.OFF) {
            var standbyMode = storageManager.ps.load(storageManager.KEY.NORMAL_STANDBY_MODE);
            if (standbyMode === null || standbyMode === undefined) {
                standbyMode = DEFAULT_VALUE.KIDS_AUTO_STANDBY_MODE;
            }
            basicAdapter.setConfigText(Def.CONFIG.KEY.AUTO_STANDBY, standbyMode);
            var vodListMode = storageManager.ps.load(storageManager.KEY.NORMAL_VOD_LIST_MODE);
            if (vodListMode === null || vodListMode === undefined) {
                vodListMode = DEFAULT_VALUE.KIDS_VOD_LIST_MODE;
            }
            storageManager.ps.save(storageManager.KEY.NORMAL_VOD_LIST_MODE, vodListMode);
        }
    }

    /**
     * 2017.06.09 dhlee
     * 일반 모드 <-> 키즈 모드 전환 시 알림 메시지 설정을 변경한다.
     * 키즈 모드로 전환 시에는 무조건 off를 해야 한다.
     *
     * @param kidsMode
     */
    function changeAlertMessage(kidsMode) {
        log.printDbg("changeAlertMessage(), kidsMode = " + kidsMode);

        var message;

        if (kidsMode === Def.KIDS_MODE.ON) {
            message = {
                "method" : "spc_optout",
                "msgType" : "JSON",
                "type" : "A",
                "id": "",
                "setting" : true,
                "to" : KTW.CONSTANT.APP_ID.SMART_PUSH,
                "from" : KTW.CONSTANT.APP_ID.HOME
            };
        }
        else {
            var value = storageManager.ps.load(storageManager.KEY.NORMAL_ALERT_MESSAGE);
            if (value === null || value === undefined) {
                value === "ON";
            }
            message = {
                "method" : "spc_optout",
                "msgType" : "JSON",
                "type" : "A",
                "id": "",
                "setting" : value === "ON" ? false : true,
                "to" : KTW.CONSTANT.APP_ID.SMART_PUSH,
                "from" : KTW.CONSTANT.APP_ID.HOME
            };
        }

        try {
            KTW.managers.MessageManager.sendMessage(message);
        } catch (error) {
            log.printDbg("saveConfiguration(), error = " + error.message);
        }
    }
    /**
     * 키즈모드와 일반모드간 전환 시 그에 맞는 Parental Rating을 set 한다.
     * Local Storage 에는 일반 모드일 때의 Rating이 저장되어 있다.
     * 키즈모드의 default PR은 15세이다.
     *
     * @param kidsMode
     */
    function changeParentalRating(kidsMode) {
        log.printDbg("changeParentalRating() kidsMode = " + kidsMode);

        var parentalRating;
        var storageManager = KTW.managers.StorageManager;

        if (kidsMode === Def.KIDS_MODE.ON) {
            storageManager.ps.save(storageManager.KEY.NORMAL_PARENTAL_RATING, KTW.oipf.AdapterHandler.casAdapter.getParentalRating());
            parentalRating = DEFAULT_VALUE.KIDS_PARENTAL_RATION;
        }
        else if (kidsMode === Def.KIDS_MODE.OFF) {
            parentalRating = Number(storageManager.ps.load(storageManager.KEY.NORMAL_PARENTAL_RATING));
        }

        // 2017.08.30 dhlee
        // https://issues.alticast.com/jira/browse/WEBIIIHOME-3215 이슈 수정
        // 키즈 모드 ON/OFF 시 PR 변경에 따른 mmi event는 무시하기 위한 flag 추가
        isChangingPR = true;
        KTW.oipf.AdapterHandler.casAdapter.setParentalRating(parentalRating, KTW.managers.auth.AuthManager.getTempPin());
        // 2017.06.23 dhlee
        // CAS에서 2분 rule에 대한 정보를 즉시 reset 하도록 한다.
        // 관련 jira 이슈: https://issues.alticast.com/jira/browse/KTSMARTSTBSECOND-1295
        KTW.ca.CaHandler.checkParentalRatingLock(true);

        /**
         * [dj.son] 부팅 후에는 임시로 저장된 PIN 값이 없기 때 error 발생 및 연령 변경 불가
         */
        var pin = KTW.managers.auth.AuthManager.getTempPin();
        if (!pin || pin.length !== 4) {
            /**
             * [dj.son] 임시로 저장하고 있는 PIN 값이 없을 경우, UP 를 통해 가져옴
             */
            pin = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.STB_PIN_NUMBER);
        }

        KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_CHANGE_AGE_LIMIT, { pin : pin, age_limit : parentalRating }, onCaResponse);

        //onCaResponse(true);
    }

    /**
     * 2017.05.29 dhlee
     * PR 변경 시 수행되는 callback
     *
     * @param success
     * @param data
     */
    function onCaResponse(success, data) {
        log.printDbg("onCaResponse(), success = " + success);
        log.printDbg("onCaResponse(), data = " + JSON.stringify(data));

        if (success === true) {
            KTW.oipf.AdapterHandler.casAdapter.notifyPRChangeListener();
            //if (KTW.managers.service.StateManager.isTVViewingState() === true) {
            //    KTW.ca.CaHandler.checkParentalRatingLock(true);
            //}
        } else {
            log.printErr("onCaResponse(), PR change error");
        }
        // 2017.08.30 dhlee
        // https://issues.alticast.com/jira/browse/WEBIIIHOME-3215 이슈 수정
        // 키즈 모드 ON/OFF 시 PR 변경에 따른 mmi event는 무시하기 위한 flag 추가
        isChangingPR = false;
    }

    /**
     * [dj.son] 키즈 모드 이후 부팅시 키즈 모듈의 성인 인증 팝업을 띄우는데,
     * 해당 팝업은 System Popup 이기 때문에, 그 이후에 보여줘야 되는 팝업들도 System Popup 이어야 한다.
     * 따라서 framework 에서는 위 팝업이 떠 있는 상황을 알아야 하기 때문에, 키즈모듈에 물어본다.
     */
    function _isKidsModeAdultAuthPopupOpend () {
        if (kidsMode === Def.KIDS_MODE.ON) {
            var kidsModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_KIDS);

            if (kidsModule) {
                return kidsModule.execute({method: "isKidsPopupOpened"});
            }
        }

        return false;
    }

    function _setModeChangingFlag (flag) {
        log.printDbg("_setModeChangingFlag(" + flag + ")");

        clearTimeout(changingTimer);
        changingTimer = null;

        KTW.ui.LayerManager.stopLoading();

        if (isChanging !== flag) {
            isChanging = flag;

            /**
             * [dj.son] jumpData 가 존재하면 jumpData 실행, 없으면 홈메뉴 노출
             *
             * - [KTUHDIII-766] 키즈 홈 키를 눌렀을때, 15초 후에 사라지지 않도록 예외처리 추가
             */
            if (jumpData && jumpData.itemType) {
                KTW.managers.service.MenuServiceManager.jumpByMenuData(jumpData);
            }
            else {
                var bAutoExit = true;
                if (jumpData && jumpData.bHotKey) {
                    bAutoExit = false;
                }

                KTW.ui.LayerManager.activateHome({bAutoExit: bAutoExit, skipRequestShow: true});
            }

            // 2017.06.09 dhlee
            // 키즈 모드 -> 일반 모드로 전환 시 자동 전원 기능을 다시 동작하도록 한다.
            // 가장 늦게 해주는 이유는 네비게이터나 홈포털에서 각종 초기화 등 작업이 모두 완료된 후에 재시작함으로써 side-effect를 줄이기 위함이다.
            if (_isKidsMode() === Def.KIDS_MODE.OFF) {
                KTW.managers.device.PowerModeManager.resetAutoPowerMode(true);
            }
        }

        jumpData = null;
    }

    function _isKidsMode () {
        /**
         * 부팅시 init 되기 전에 채널 event 가 와서 미니 epg 에서 해당 함수를 호출하는 경우가 있음 (undefined error)
         *
         * init 을 호출하기 전에는 undefined 를 리턴하도록 수정
         */
        if (kidsMode === undefined) {
            return kidsMode;
        }

        return kidsMode === Def.KIDS_MODE.ON;
    }

    function _isModeChanging () {
        log.printDbg("_isModeChanging(" + isChanging + ")");
        return isChanging;
    }

    function _isPRChanging() {
        log.printDbg("_isPRChanging(" + isChangingPR + ")");
        return isChangingPR;
    }

    /**
     * 2017.07.18 dhlee
     *
     * Configuration Key 값을 반환한다. (KIDS_RESTRICTION_STATE)
     * @returns {*}
     */
    function _getKidsModeRestrictionKey() {
        log.printDbg("getKidsModeRestrictionKey()");

        return KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.KIDS_RESTRICTION_STATE);
    }

    /**
     * 2017.07.18 dhlee
     *
     * Configuration Key 값을 기록한다. (KIDS_RESTRICTION_STATE)
     * @param value "true" | "false"
     */
    function _setKidsModeRestrictionKey(value) {
        log.printDbg("_setKidsModeRestrictionKey() value = " + value);

        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.KIDS_RESTRICTION_STATE, value);
    }

    function _activateCanNotMoveMenuInfoPopup (msg) {
        log.printDbg("_activateCanNotMoveMenuInfoPopup()");

        var layer = KTW.ui.LayerManager.getLastLayerInStack({key: "id", value: "CanNotMoveMenuInfoPopup"});
        if (layer) {
            return;
        }

        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["키즈모드"],
                cssObj: {}
            }],
            arrButton: [{id: "ok", name: "확인"}],
            cbAction: function (id) {
                if (!isCalled && id === "ok") {
                    isCalled = true;

                    KTW.ui.LayerManager.deactivateLayer({
                        id: "CanNotMoveMenuInfoPopup",
                        remove: true
                    });
                }
            }
        };

        popupData.arrMessage.push({
            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
            message: (msg === null || msg === undefined ) ? KTW.ERROR.EX_CODE.KIDS_MODE.EX001 : msg,
            cssObj: {}
        });

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "CanNotMoveMenuInfoPopup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true,
            cbActivate: function () {}
        });
    }

    return {
        init: _init,
        changeMode: _changeMode,

        isKidsMode: _isKidsMode,
        isKidsModeAdultAuthPopupOpend: _isKidsModeAdultAuthPopupOpend,
        isModeChanging: _isModeChanging,
        setModeChangingFlag: _setModeChangingFlag,
        isPRChanging: _isPRChanging,
        getKidsModeRestrictionKey: _getKidsModeRestrictionKey,
        setKidsModeRestrictionKey: _setKidsModeRestrictionKey,

        activateCanNotMoveMenuInfoPopup: _activateCanNotMoveMenuInfoPopup
    };
})();