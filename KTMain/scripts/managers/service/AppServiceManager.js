/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AppServiceManager</code>
 *
 * @author dj.son
 * @since 2016-09-08
 */

KTW.managers.service.AppServiceManager = (function () {

    var CONST_APP_STATE = KTW.CONSTANT.APP_STATE;
    var CONST_SVC_STATE = KTW.CONSTANT.SERVICE_STATE;
    var CONST_APP_TYPE = KTW.CONSTANT.APP_TYPE;

    var KEY_PRIORITY = {
        SHOW : 0,           // 홈포탈 UI 가 보이는 상태 + VOD 재생중 홈포탈 UI 가 안보이는 상태
        HIDE : -2,          // TV 중 홈포탈 UI 가 안보이는 상태 + 풀브라우저 + 데이터 채널 등등
        VOD_HIDE : -1,      // VOD 재생중 다른 앱이 떠 있는 상태 (채널 up/down 키 이벤트 처리 위함)
        SYSTEM_POPUP : 99,  // System Popup 이 떠 있는 상태
        VOD_POPUP : 9,      // VOD 재생중 다른 앱과 겹쳐 보여야 할때 + 각종 핫키들 막을때
        REMIND_POPUP : 5,   // 다른 앱 실행중 예약 알림 팝업 보이는 상태
        POPUP : 1           // TV 중 다른 앱과 겹쳐 보여야 할때
    };
    var OBS_MSG_TYPE = {
        NONE : -1,
        HIDE : 0,
        HIDE_FORCED : 9
    };

    var DP_KEY = "dp_";

    var NAV_CONTROL = KTW.nav.Def.CONTROL;

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var powerModeManager = null;
    var iframeManager = null;
    var layerManager = null;
    var stateManager = null;
    var storageManager = null;
    var amocManager = null;
    var caHandler = null;

    var adapterHandler = null;
    var mainChannelControl = null;
    var wmAdapter = null;	// 2016.09.05 dhlee R6 형상 추가
    var messageManager = null;

    var timeRestrictLayerId = "TimeRestrictLayer";
    var isTimeRestrict = false;

    var childApp = null;

    var lastPriority = {
        focus: true,
        priority: KEY_PRIORITY.HIDE - 1,
        windowPriority: KEY_PRIORITY.HIDE - 1
    };

    var complete_process_first_key = false;
    var processing_first_key = false;
    var callbackProcessFirstKey = null;

    var processing_first_home_key = false;

    var OFFICE_CODE_INTERVAL = 30 * 24 * 60 * 60 * 1000; // 30 day

    function _init() {
        log.printDbg("init()");

        powerModeManager = KTW.managers.device.PowerModeManager;
        iframeManager = KTW.managers.service.IframeManager;
        layerManager = KTW.ui.LayerManager;
        stateManager = KTW.managers.service.StateManager;
        storageManager = KTW.managers.StorageManager;
        amocManager = KTW.managers.http.amocManager;
        caHandler = KTW.ca.CaHandler;

        adapterHandler = KTW.oipf.AdapterHandler;
        wmAdapter = KTW.oipf.AdapterHandler.wmAdapter;

        mainChannelControl = adapterHandler.navAdapter.getChannelControl(NAV_CONTROL.MAIN);

        messageManager = KTW.managers.MessageManager;

        messageManager.addMessageListener(onReceiveMessage);

        powerModeManager.addPowerModeChangeListener(onPowerModeChange);

        _clearTimeRestrictOptions();
    }

    function _destroy() {
        stopAV();
    }

    function onPowerModeChange (mode) {
        log.printDbg("onPowerModeChange() " + mode);

        if (mode == KTW.oipf.Def.CONFIG.POWER_MODE.ON) {
            var options = {
                nextServiceState: CONST_SVC_STATE.TV_VIEWING
            };

            if (isTimeRestrict) {
                options.nextServiceState = CONST_SVC_STATE.TIME_RESTRICTED;
            }
            else {
                // 2017.07.27 dhlee
                // WEBIIIHOME-2856 이슈 해결을 위한 코드 추가
                // 대기 -> 동작 전환 시에 Configuration key 값을 보고 state를 바로 KIDS_RESTRICTION state로 변경해 준다.
                // 이렇게 변경을 하게 되면 미니EPG는 state가 TV_VIEWING state가 아니므로 화면을 노출하지 않게 된다.
                if (KTW.managers.service.KidsModeManager.getKidsModeRestrictionKey() === "true") {
                    options.nextServiceState = CONST_SVC_STATE.KIDS_RESTRICTION;
                }
                /**
                 * Standby 에서 전원 켜졌을때 네비게이터가 채널 튠을 해주는데,
                 * changeService 에서 채널 튠이 일어남으로서 결과적으로 채널 튠이 2번 일어남
                 * changeService 에서 채널 튠을 안하고, resume 만 수행하도록 options 값 조작하여 수정
                 */
                options.obj = {
                    channel: mainChannelControl.getCurrentChannel(),
                    tune: false
                };
            }

            _changeService(options);
        }
        else if (mode == KTW.oipf.Def.CONFIG.POWER_MODE.ACTIVE_STANDBY || mode == KTW.oipf.Def.CONFIG.POWER_MODE.PASSIVE_STANDBY) {
            // standby

            _changeService({
                nextServiceState: CONST_SVC_STATE.STANDBY
            });
        }
    }

    function onReceiveMessage (message) {
        log.printDbg("onReceiveMessage() " + message.method);

        // 기존 홈포탈 LayerManager 의 소스 코드 참고하여 개발한다
        switch(message.method) {
            case messageManager.METHOD.OBS.REQUEST_SHOW :
                if (message.res === "yes") {
                    // requestShow 는 홈포탈에 옵저버에 show 해달라고 요청하는 동작
                    // 따라서 이미 requestShow 요청전에 필요한 동작 (Layer Show 등) 을 하기에 다시 obs_hide 메세지를 보낼 필요는 없음

                    // [2018-03-07 TaeSang] VOD 미리보기 시나리오 위해 requestshow 호출 시, 이전 레이어가 노출되면서 결제팝업이 닫히는 경우가 있음
                    // (미리보기 재생 > 사용자 레드키 입력 > 일시정지 > requestShow 호출 > 결제팝업 노출 > requestShow로 인해 이전 레이어 노출 > 결제팝업 닫힘)
                    // 확인결과, 아래코드는 2.0 코드에서 가져왔지만 실제로 쓰이는 곳이 없으며 주석 처리 후 테스트 시 아무런 이상이 없어 주석처리 함

                    // var lastLayer = layerManager.getLastLayerInStack();
                    // if (lastLayer && (lastLayer.type >= KTW.ui.Layer.TYPE.NORMAL || lastLayer.id === KTW.ui.Layer.ID.MINI_EPG)) {
                    //     if (!lastLayer.isShowing()) {
                    //         layerManager.activateLayer({
                    //             obj: {
                    //                 id: lastLayer.id
                    //             },
                    //             visible: true
                    //         });
                    //     }
                    // }

                    stateManager.isShowAppStore = false;
                    stateManager.isNotifyStopAV = false;
                }
                break;
            case messageManager.METHOD.OBS.SHOW :
                /**
                 * [dj.son] obs_show 가 불리는 경우
                 * 1. Hot Key 가 눌렸을 경우
                 * 2. 데이터 채널 종료시 채널 event 이후에 날라옴 {"method":"obs_show","from":"4e30.3000","args":"PREV_STATE","key":"*"}
                 * 3. 풀 브라우저 종료시 (핫키로 종료되는 경우는 obs_show 먼저 오고 그 다음 채널 이벤트가 옴)
                 *
                 *
                 * - 양방향 서비스 진입 후 로딩 화면이 보일 때 DCA로 tune 가능한 경우가 있는데 이 경우에는 obs_show 가 불리지 않고 channel tune 만 됨
                 *   그래서 기존 홈포탈에서는 onChannelChangeSucceeded 에서 setTimeout(..., 500ms) 으로 상태처리 (exitToService) 함
                 *
                 * - obs_show 가 불릴때는 기본적으로 requestShow 로직을 skip 하도록 함
                 */

                if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING || stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.VOD) {
                    if (message.key) {
                        layerManager.onKeyDown({keyCode: message.key, preventDefault: function() {}, stopPropagation: function() {}}, true);
                    }
                    else {
                        layerManager.activateHome({ skipRequestShow: true });
                    }
                }
                else if (stateManager.isOtherAppState()) {
                    var nextServiceState = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;
                    if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                        nextServiceState = KTW.CONSTANT.SERVICE_STATE.VOD;
                    }

                    /**
                     * [dj.son] 풀브라우저 대응 코드
                     * - 원래 로직은 channel event -> obs_show 순으로 온다고 가정하고 로직이 짜여져 있었으나,
                     *   풀브라우저가 핫키로 실행되는 경우는 obs_show -> channel event 순으로 날라옴
                     *   따라서 위와 같은 경우에서는 changeService 가 500ms 이후에 실행되도록 로직 수정
                     * - 왜 500ms 이냐면, ChannelControl - success event 에서 현재 상태가 OTHER_APP 상태이면 500ms 이후에
                     *   changeService 수행하도록 되어 있기 때문에 500ms 이상의 시간을 주면 순서가 꼬일 이슈가 있음
                     *
                     * - 추가로 현재 obs_show 가 channel event 이후에 온 건지 이전에 온 건지 구분할 방도가 없음
                     *   따라서 여기서는 현재 채널이 null 이고 key parameter 가 포함되어 있으면 풀브라우저에서 핫키 로직이 수행된 것이라 가정
                     *   (풀브라우저 실행시 현재 채널은 null 로 넘어옴)
                     */
                    //var curChannel = KTW.oipf.AdapterHandler.extensionAdapter.getRealCurrentChannel();
                    var curChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();

                    var restoreLayer = false;
                    if ((message.args && message.args === "PREV_STATE") || message.key) {
                        restoreLayer = true;
                    }

                    /**
                     * full browser 일 경우 예외처리
                     */
                    var delayChangeService = false;
                    if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP && !curChannel && layerManager.isHotKey(message.key)) {
                        delayChangeService = true;
                    }

                    /**
                     * [dj.son] [KTUHDII-1002] 키즈모드 -> 양방향 앱 -> 앱스토어키 입력할 경우, obs_show 가 channel success event 보다 빨리 오는 경우
                     * - 풀브라우저와 마찬가지로 500ms 지연 처리
                     */
                    if (!delayChangeService && message.key === KTW.KEY_CODE.APP_STORE &&
                        KTW.managers.service.KidsModeManager.isKidsMode() && stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                        delayChangeService = true;
                    }

                    if (delayChangeService) {
                        setTimeout(function () {
                            _changeService({
                                nextServiceState: nextServiceState,
                                key: message.key,
                                restoreLayer: restoreLayer
                            });
                        }, 500);
                    }
                    else {
                        _changeService({
                            nextServiceState: nextServiceState,
                            key: message.key,
                            skipRequestShow: true,
                            restoreLayer: restoreLayer
                        });
                    }
                }
                break;
            case messageManager.METHOD.OBS.HIDE :
                // 데이터 채널, fullbrowser : obs_hide
                //     -> 데이터 채널은 채널 change evt 를 통해 changeService 실행
                //     -> fullbrowser 는 홈포탈을 통해서만 실행되기 때문에 changeService 실행 가능

                // 일반 앱, appstore : obs_hide -> obs_showOtherApp
                //     -> obs_showOtherApp 에서 다른 앱이 실행되었는지 판단 가능 (changeService 실행)

                _destroyChildApp();

                // observer로 부터 obs_hide 메세지를 받은 경우 stack layer를 hide 처리함
                // DATA 서비스 상태에서는 이미 hideAllLayer()를 했기 때문에 별도 처리하지 않는다.
                /*if (last_state !== OTW.CONSTANT.SERVICE_STATE.DATA && last_state !== OTW.CONSTANT.SERVICE_STATE.DATA) {
                    clearStackLayer();
                 }*/
                // 실시간 인기채널이 DCA 등으로 종료될 때
                // stitching channelControl이 STARTED 된 상태라면
                // DCA 등 다른 경로를 통해서 channel tune 된 경우이다.
                // 그래서 먼저 stitching ChnnaelControl을 stop 처리하고
                // 실제 실시간 인기채널 처리 layer에서는 channel change를 tune only로 처리되지 않도록 하여
                // miniEPG가 노출될 수 있도록 처리하기 위해서 OBS_HIDE 메세지 전달 받았을 때
                // 미리 stop 처리하도록 함
                var stitching_cc = adapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.STITCHING);
                if (stitching_cc !== null && stitching_cc.getState() === KTW.nav.Def.CONTROL.STATE.STARTED) {
                    stitching_cc.stop();
                }

                // appstore가 노출된 상태에서는 history를 지우지 않도록 한다.

                // [dj.son] 앱이 show 상태일때 채널튠이 되면 obs_hide 메세지가 날라온다.
                // 문제는 오디오 편성표에서 dca 로 다른 오디오 채널로 튠 할때, obs_hide 메세지로 인해 ChannelGuideLayer 가 hide 되는데
                // 이때 ChannelGuideLayer 내 AudioEpgView 도 hide 가 되고, 기본 로직에 의해 이전 채널로 튠이 되는 이슈가 발생
                // 이를 해결하기 위해 현재 채널이 오디오 채널이고 ChannelGuideLayer 가 show 상태인 경우에는 obs_hide 에 반응하지 않도록 수정
                var isHide = !stateManager.isShowAppStore;
                if (KTW.CONSTANT.IS_OTS) {
                    var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.CHANNEL_GUIDE);
                    if (layer && layer.isShowing()) {
                        var currentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                        if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(currentChannel)) {
                            isHide = false;
                        }
                    }
                }

                if (isHide) {
                    layerManager.hideNormalLayer();
                }

                // obs_hide 메세지를 받으면 강제적으로 notifyHide message를 전달함
                messageManager.sendMessage({
                    "method": messageManager.METHOD.OBS.NOTIFY_HIDE,
                    "from": KTW.CONSTANT.APP_ID.HOME,
                    "to" : KTW.CONSTANT.APP_ID.OBSERVER
                });

                if (isHide) {
                    /**
                     * [dj.son] [WEBIIIHOME-1663] obs_hide 에서 무조건 HIDE 로 priority 를 설정해서 생긴 이슈 수정
                     * - LayerManager 에서 key priority 를 처리하도록 hideNormalLayer 내에 key priority 설정 함수 호출하도록 수정
                     */
                    /**
                     * [dj.son] [WEBIIIHOME-3689] 현재 앱의 show 상태를 보고, show 상태가 아닌 경우 priority 조정하도록 예외처리 추가
                     */
                    var layer = KTW.ui.LayerManager.getLastLayerInStack();
                    if (!layer || (layer.type > KTW.ui.Layer.TYPE.TRIGGER && layer.isShowing() && layer.id !== KTW.ui.Layer.ID.MINI_EPG)) {
                        _setKeyPriority({
                           priority: KEY_PRIORITY.HIDE
                        });
                    }
                }
                else {
                    /**
                     * [dj.son] OTS 오디오 채널 편성표 때문에 추가
                     *
                     * - [WEBIIIHOME-3780] REQUEST_SHOW 호출 구문 삭제,
                     */
                    // KTW.managers.MessageManager.sendMessage({
                    //     method: KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW,
                    //     from: KTW.CONSTANT.APP_ID.HOME,
                    //     to : KTW.CONSTANT.APP_ID.OBSERVER
                    // });
                }

                break;
            case messageManager.METHOD.OBS.NOTIFY_KEY_EVENT :
                _destroyChildApp();
                layerManager.onKeyDown({keyCode: message.key, preventDefault: function() {}, stopPropagation: function() {}});
                break;
            case messageManager.METHOD.HP.HIDE_OTHER_APP :
                // hp_hideOtherApp 이 오는 경우
                // 1. App Store, Multicast App 같은 AV 위에 뜨는 앱들이 종료될때
                // 2. App Store -> Data Channel
                //   - App Store -> Wiz Game 실행 -> 종료시 TV Viewing 위에 App Store 다시 뜸
                //     - channel evt -> channel evt
                //   - App Store -> Data Channel -> 종료시 TV Viewing 상태 (App Store 안뜸)
                //     - channel evt -> hp_hideOtherApp -> channel evt
                //   - App Store -> Data Channel -> 종료시 TV Viewing 위에 App Store 다시 뜸
                //     - hp_hideOtherApp -> channel evt -> channel evt -> hp_showOtherApp
                //   * 따라서 이 경우는 channel evt 에서 changeService 를 호출하도록 하고, 여기서는 delay 를 주어 판단하도록 함
                // 3. App 이 STANDBY 상태로 전환되고, hp_hideOtherApp 이벤트가 오는 경우 발생 (다른 앱이 실행중일때 전원 OFF 한 경우)
                //   - 현재 상태가 STANDBY 상태이면 아무런 동작 안하도록 if 문 추가


                /**
                 * [dj.son] [WEBIIIHOME-3320] 바운드앱이 살아있는 상태에서 시청시간제한이 걸리면 hp_hideOtherApp 메세지가 날라옴
                 *                            따라서, 시청시간제한 상태일 경우에도 예외처리
                 */
                if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.STANDBY || stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
                    return;
                }

                var nextServiceState = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;
                var restoreLayer = true;
                if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.VOD || stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                    nextServiceState = KTW.CONSTANT.SERVICE_STATE.VOD;
                    restoreLayer = false;
                }
                else if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                    /**
                     * [dj.son] [WEBIIIHOME-3780] 현재 양방향 앱 실행 상태이면 flag reset 하도록 예외처리 추가
                     */
                    stateManager.isShowAppStore = false;
                    stateManager.isNotifyStopAV = false;
                    return;
                }

                if (stateManager.isShowAppStore) {
                    stateManager.isShowAppStore = false;

                    setTimeout(function() {
                        if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV
                            || stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                            _changeService({
                                nextServiceState: nextServiceState,
                                skipRequestShow: true,
                                restoreLayer: restoreLayer
                            });
                        }
                    }, 200);
                }
                else {
                    _changeService({
                        nextServiceState: nextServiceState,
                        skipRequestShow: true,
                        restoreLayer: restoreLayer
                    });
                }
                break;
            case messageManager.METHOD.HP.SHOW_OTHER_APP :
                /**
                 * [dj.son] [WEBIIIHOME-3320] 대기모드, 시청시간제한 상태인 경우, hp_showOtherApp 메세지 무시하도록 예외처리
                 */
                if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.STANDBY || stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
                    return;
                }

                //QAT vod 중 wiz 게임 실행 후 나가기 눌렀을때 mini EPG 안나오는 이슈 수정사항. flag clear 추가.
                stateManager.isNotifyStopAV = false;

                // appstore의 경우 unboundApp이라서 다른 경로를 통해서 뜰 수 있기 때문에
                // showOtherApp의 argument 값을 비교하여 확인
                if (message.args && message.args === KTW.CONSTANT.APP_ID.APPSTORE) {
                    if (!stateManager.isShowAppStore) {
                        //childApp start 하자 마자 stopLoading 이 불릴경우
                        //app store 가 show 될때 loading 을 제거해야 한다.
                        layerManager.stopLoading();
                        log.printDbg("onReceiveMessage() show APPSTORE");
                        stateManager.isShowAppStore = true;
                    }
                }

                if (stateManager.serviceState != KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV
                    && stateManager.serviceState != KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                    var nextServiceState = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                    var visible = false;

                    if (stateManager.isVODPlayingState()) {
                        nextServiceState = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD;
                        visible = true;
                    }

                    _changeService({
                        nextServiceState: nextServiceState,
                        visible: visible
                    });
                }

                // showOtherApp 할 경우 의도적으로 UI를 hide 하도록 함
                // 단, 이때 상태 변경되지 않기 때문에 hideAllLayer(false)로 호출하여
                // iframe은 그대로 유지되도록 한다.
                // TODO HIde All Layer (only hide)
                //hideAllLayer(false);

                var keyPriority = {
                    priority: KEY_PRIORITY.VOD_HIDE,
                    windowPriority: -2,
                    force: true
                };

                // other app이 노출된 상태이기 때문에
                // key priority는 낮추고 window priority는 "1"으로 유지하여
                // appstore 보다 상위에 UI가 노출될 수 있도록 함
                // channel up/down 등의 키 처리를 위해서 hide 시
                // keyPriority 값을 "-1"로 설정한다.
                // TODO 앱스토어 동작 확인해야함
                if (stateManager.serviceState == KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                    if (stateManager.isShowAppStore) {
                        keyPriority.windowPriority = 1;
                    }

                    _setKeyPriority(keyPriority);
                }
                else {
                    // popup 등이 노출된 상태일 수 있기 때문에
                    // 의도적으로 key priority를 HIDE 처리하도록 함
                    // ex) 미가입 채널에서 가입 popup 등이 노출된 상황에서 appstore 등이 뜰 경우
                    _setKeyPriority(keyPriority);

                    // OTS 오디오 채널 편성표 상태에서
                    // appstore 핫키등으로 showOtherApp 메세지가 전달되는 경우
                    // LCW 채널로 tune 하도록 함
                    if (KTW.CONSTANT.IS_OTS === true) {
                        var channel_control = adapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                        var cur_channel = channel_control.vbo.currentChannel;

                        if (adapterHandler.navAdapter.isAudioChannel(cur_channel) === true) {
                            var lwc = channel_control.getBackChannel();
                            adapterHandler.navAdapter.changeChannel(lwc, true);
                        }
                    }
                }

                // otherApp 노출 시 미리보기 icon hide 처리함
                //KTW.managers.service.ChannelPreviewManager.hidePreview();
                break;
            case messageManager.METHOD.HP.NOTIFY_STOP_AV :
                stateManager.isNotifyStopAV = true;

                // 통상적으로 channel 상태일 경우에는 잘 발생하지 않지만
                // 적어도 현재 노출중인 promo trigger 또는 miniEPG 및 PIP 등은 hide 처리해야 한다.
                if (!stateManager.isVODPlayingState()) {
                    KTW.managers.service.PromoManager.stopTrigger();
                    // 의도적으로 현재 떠 있던 miniEPG를 제거한다.
                    // TODO 미니 epg deactivate 처리
                    //_deactivateLayer(OTW.ui.Layer.ID.MINI_EPG);
                }

                // TV_VIEWING 상태에서 notifyStopAV 메세지 받을 경우
                // 현재 채널이 제한 채널일 때 iframe이 제거되지 않아서
                // wizgame 등의 상위에 노출되는 경우가 있음.
                // 그래서 의도적으로 iframe을 지워주고 channel permission도 reset함
                // 이후 wizgame 종료되면서 channel tune이 될 때
                // permission check하면서 iframe은 자연스럽게 그려짐
                if (stateManager.isTVViewingState()) {
                    iframeManager.setType(iframeManager.DEF.TYPE.NONE);
                    adapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).resetPerm();
                }
                break;
            case messageManager.METHOD.OBS.SET_WATCHING_TIME_BLOCK :
                isTimeRestrict = true;

                if (stateManager.serviceState !== KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                    adapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);

                    // changeService 함수 내에서 처리해야 하지만, RELEASE_WATCHING_TIME_BLOCK 과 같은 code level 에서 처리하기 위해서 여기서 처리
                    if (stateManager.isVODPlayingState()) {
                        var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                        if (vodModule) {
                            vodModule.execute({
                                method: "pauseVODPlaying"
                            });
                        }
                    }
                    _changeService({
                        nextServiceState: CONST_SVC_STATE.TIME_RESTRICTED
                    });
                }
                break;
            case messageManager.METHOD.OBS.RELEASE_WATCHING_TIME_BLOCK:
                isTimeRestrict = false;

                if (stateManager.serviceState !== KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                    var options = {nextServiceState: CONST_SVC_STATE.TV_VIEWING};

                    adapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);

                    // TODO check VOD stop - 2017-04-08 IsVODPlaying api 현재 없음, IsVODPlaying 이 pause 상태를 체크하는지 확인
                    var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                    if (vodModule && vodModule.execute({method: "isVODPlaying"}) === "1") {
                        vodModule.execute({
                            method: "playVODPaused"
                        });
                        options.nextServiceState = CONST_SVC_STATE.VOD;
                    }
                    else {
                        var curChannel = mainChannelControl.getCurrentChannel();

                        if (KTW.oipf.AdapterHandler.navAdapter.isDataServiceChannel(curChannel)) {
                            options.nextServiceState = CONST_SVC_STATE.OTHER_APP;
                            options.obj = {
                                type: CONST_APP_TYPE.DATA_CHANNEL,
                                param: curChannel
                            }
                        }
                    }

                    _changeService(options);
                }
                break;
            case messageManager.METHOD.CUSTOM.NOTIFY_PLAY_AV :
                // appstore에서 child app 실행 시 hp_notifyStopAV가 온 뒤
                // hideOtherApp이 오는 것이 정상인데 누락되는 경우가 있음
                // 그래서 hp_notifyStopAV 이후 channel  전환이 발생하는 경우
                // 의도적으로 notify_stop_av flag를 초기화 해주도록 함
                // 이렇게 하지 않으면 child app 및 appstore 종료하더라도
                // key가 계속 block 되기 때문에 내부적으로 처리함
                stateManager.isNotifyStopAV = false;
                break;
        }
    }

    /**
     * [dj.son] 다른 앱 종료시 홈포탈 동작
     *
     * - recheckPermission 호출 -> 미니 epg Layer show
     * - promo trigger check
     * - preview check
     */
    function hideOtherApp() {
        log.printDbg("hideOtherApp() isNotifyStopAV = " + stateManager.isNotifyStopAV);

        // appStore에서 특정 child app을 실행할 경우 hp_showOtherApp이 아니라
        // hp_notifyStopAV만 호출된다.
        // 이 상태에서 hideOtherApp이 불리면 child app 이 떠 있는 상태이므로 무시해야 한다.
        if (stateManager.isNotifyStopAV) {
            return;
        }

        // unbound app 이 비활성화 되었다는 것으로 promo trigger 를 다시 show 처리한다.
        if (stateManager.isTVViewingState()) {
            // appstore 등의 app이 hide 될 때 permission을 다시 체크하도록 함
            //adapterHandler.navAdapter.getChannelControl(OTW.nav.Def.CONTROL.MAIN).recheckPermission(true);

            // request promo trigger
            //OTW.managers.service.PromoManager.requestTrigger();

            // otherApp 닫힐 경우 미리보기 icon 다시 노출하도록 함
            // appstore에서 하위 app이 뜨는 경우 notifyStopAV가 불리거나
            // 다시 showOtherApp이 불리기 때문에 show/hide otherApp 시
            // 미리보기 icon show/hide 처리하도록 함
            //OTW.managers.service.ChannelPreviewManager.showPreview();
        }
    }

    /**
     * [dj.son] set key & window priority
     *
     * @param options focus - activateInput 호출시 focus 점유 여부
     *                 priority - key priority
     *                 windowPriority - window priority
     *                 force - last priority 상관없이 priority set
     */
    function _setKeyPriority (options) {
        if (!options || options.priority === undefined || options.priority === null) {
            return;
        }

        log.printDbg("setKeyPriority() options : " + log.stringify(options) + ", lastPriority : " + log.stringify(lastPriority));

        // SYSTEM popup이 노출되고 있는 상태라면
        // SHOW/HIDE 관계없이 priority를 "99"로 처리한다.
        if (options.priority !== KEY_PRIORITY.SYSTEM_POPUP) {
            var lastLayer = layerManager.getLastLayerInStack();
            if (lastLayer && lastLayer.type === KTW.ui.Layer.TYPE.POPUP && lastLayer.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP && lastLayer.id !== KTW.ui.Layer.ID.LOADING && lastLayer.id !== KTW.ui.Layer.ID.TV_PAY_DUMMY_PUPUP) {
                options.priority = KEY_PRIORITY.SYSTEM_POPUP;
            }
        }

        // 동일한 priority 요청이 오는 경우 skip
        if (lastPriority.priority === options.priority && !options.force) {
            log.printDbg("setKeyPriority() request to change same priority, windowPriority... so return");
            return;
        }

        if (options.focus === undefined) {
            if (options.priority !== KEY_PRIORITY.HIDE) {
                options.focus = true;
            }
            else {
                options.focus = false;
            }
        }

        log.printDbg("setKeyPriority() final options = " + log.stringify(options));

        adapterHandler.basicAdapter.setKeyPriority(options.focus, options.priority, options.windowPriority);
        lastPriority = options;
    }


    /**
     * [dj.son] change service (STANDBY, TV_VIEWING, VOD, DATA, TIME_RESTRICTED, STANDBY)
     *
     * @param options nextServiceState - next state
     *                 obj - next state info
     *                 visible - last layer visible options
     *                 reset - resume main channel object flag for visible mini epg layer
     *                 forced - 현재 state 와 nextServiceState 가 같더라도 강제로 로직 수행
     *                 changeForced - 값이 true 인 경우 state 를 무조건 changing
     *                 tuneForced - TV_VIEWING 상태로 전환시 무조건 tune
     *                 restoreLayer - true 이면 restoreLayer 호출, (플립북 517 페이지 VOD 시청 완료 팝업 시나리오 대응)
     *
     */
    function _changeService (options) {
        log.printDbg("_changeService() current_state = " + stateManager.getServiceStateString(stateManager.serviceState));

        if (!options || (options.nextServiceState == null || options.nextServiceState == undefined)) {
            log.printDbg("options is null.... return");
            return;
        }

        log.printDbg("_changeService() options = " + log.stringify(options));

        if (stateManager.isChangeSameState(options)) {
            log.printDbg("currentState & nextServiceState is equal.... return");
            return;
        }

        // child app 종료
        _destroyChildApp();

        switch (options.nextServiceState) {
            case CONST_SVC_STATE.TV_VIEWING:
                var isTune = true;
                var isChSelect = false;

                if (stateManager.serviceState == CONST_SVC_STATE.VOD || stateManager.serviceState == CONST_SVC_STATE.OTHER_APP_ON_VOD) {
                    // TODO VOD 종료 로직 추가 (? 이건 vod 모듈에서 listener 로 해결 가능할 것 같음... 아님 로직 추가해야 함)
                    stopAV(options);

                    // [dj.son] VOD 에서 복귀한 경우, 이전 화면 혹은 홈메뉴 activate
                    // [dj.son] MiniEpgLayer 를 부르면 안되기 때문에 채널 select 만 수행
                    /**
                     * [dj.son] [WEBIIIHOME-3587] 기가지니에서 VOD 재생중 안드로이드 앱 실행하면 VOD 종료에 의한 resume 처리 때문에 requestShow 가 불리게 되는데,
                     *                            이때 requestShow 가 android state change 이벤트보다 늦게 와 동작이 꼬이는 이슈가 발생함...
                     *                            따라서, VOD -> TV_VIEWING 상태로 전환시 resume 수행할때, 먼저 resume 을 수행하고 requestShow 를 나중에 호출하도록 수정
                     *                            이렇게 해도 문제 없는 이유는 VOD -> TV_VIEWING 상태로 전환시 반드시 채널 튠을 수행하기 때문에
                     *                            다른 앱이 떠 있는 상황이 있을 수가 없기 때문
                     *                            물론 VOD 재생중 양방향 채널, 풀 브라우저 실행 등을 수행할 수 있지만
                     *                            이 경우 OTHER_APP 상태로 빠지고 다른 로직을 수행하기 때문에 문제 없음
                     */
                    restoreLayer({isHome: true, blockRestoreLayer: !options.restoreLayer, skipRequestShow: options.skipRequestShow, delayRequestShow: true});
                    isChSelect = true;
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.OTHER_APP) {
                    // [dj.son] OTHER_APP 에서 복귀한 경우, 이미 외부에서 tune 된 상태이기 때문에 또다시 tune 수행하지 않도록 수정
                    restoreLayer({keyCode: options.key, blockRestoreLayer: !options.restoreLayer, skipRequestShow: options.skipRequestShow, isHome: true});
                    isTune = false;
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.OTHER_APP_ON_TV) {
                    // [dj.son] OTHER_APP 에서 복귀한 경우, 이미 외부에서 tune 된 상태이기 때문에 또다시 tune 수행하지 않도록 수정
                    restoreLayer({keyCode: options.key, blockRestoreLayer: !options.restoreLayer, skipRequestShow: options.skipRequestShow});
                    isTune = false;
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.STANDBY) {
                    isTune = false;
                    isChSelect = true;

                    // 2017.08.01 dhlee
                    // WEBIIIHOME-2983 이슈 수정
                    // 대기 -> 동작 전환 시에 channel event 보다 power state change 이벤트가 먼저 도착한다.
                    // 따라서 데이터방송 상태에서 대기 모드로 진입하게 되면 홈포털에서 가지고 있는 current channel은 데이터 방송 채널 정보이다.
                    // 이를 해결하기 위하여 네비게이터에서 제공하는 getRealCurrentChannel() API를 사용하도록 한다.
                    //var curChannel = mainChannelControl.getCurrentChannel();
                    var curChannel = adapterHandler.extensionAdapter.getRealCurrentChannel();
                    if (!KTW.managers.service.KidsModeManager.isKidsMode()) {
                        if (!adapterHandler.navAdapter.isDataServiceChannel(curChannel)) {
                            // 2017.06.07 dhlee
                            // 대기모드에서 동작모드로 넘어올 때도 skipRequestShow를 하는 것이 맞음.
                            //layerManager.activateHome({ bAutoExit: true });
                            layerManager.activateHome({ bAutoExit: true, skipRequestShow: true });
                        }
                        else if (KTW.CONSTANT.IS_OTS && !adapterHandler.navAdapter.isAudioChannel(curChannel)) {
                            isTune = true;
                        }
                    }

                    /**
                     * [dj.son] [WEBIIIHOME-2139] 스티칭 화면 show 상태에서 대기모드로 갔다가 power on 이 되면, AV 가 20초 후에 풀사이즈로 변하는 이슈가 있음
                     * - STANDBY -> TV_VIEWING 일때, 강제로 풀 사이즈로 resize 하도록 수정
                     * - 정확한 원인은 단말에 문의해야 함
                     */
                    if (!KTW.oipf.AdapterHandler.navAdapter.isDataServiceChannel(curChannel)) {
                        KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT, true);
                    }
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.TIME_RESTRICTED) {
                    /**
                     * [dj.son] 실시간 인기 상태 Layer 가 show 중인 상태에서 시청 시간 제한 상태로 전환시 예외처리 추가
                     * - 실시간 인기 채널 Layer 가 hide 시 main Channel Control 에 튠 수행
                     * - 이후에 시청 시간 제한 해제시 같은 채널로 튠이 되는데, main cc 에서 저장하고 있는 last Perm 과 현재 Perm 이 같이 때문에 iframe 을 호출 안하는 이슈 있음
                     * - 따라서 시청 시간 제한 해제시, resetPerm 을 호출해서 다시 퍼미션 체크하도록 수정
                     */
                    mainChannelControl.resetPerm();

                    hideTimeRestrictPopup();
                    if (processing_first_key) {
                        /**
                         * [dj.son] [WEBIIIHOME-3630] 시청 시간 제한 에서 TV_VIEWING 상태로 전환시 requestShow 호출하도록 수정
                         */
                        restoreLayer({skipRequestShow: true, delayRequestShow: true});
                    }
                    else {
                        layerManager.clearNormalLayer();
                    }
                    isChSelect = true;
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.KIDS_RESTRICTION) {
                    /**
                     * [dj.son] [WEBIIIHOME-3052] KIDS_RESTRICTION 상태에서 TV_VIEWING 상태로 전환될때, 채널 select 만 이루어지도록 수정
                     */
                    isChSelect = true;
                }

                if (!isTune && options.tuneForced) {
                    isTune = true;
                }

                // options 에 channel 정보가 존재하면 tune, 없으면 현재 serviceState 확인 후 promo channel tune 결정
                if (options.obj && util.isValidVariable(options.obj, ["channel", "tune"]) && options.obj.channel) {
                    if (options.obj.tune === "true") {
                        // channel OSD를 노출하지 않도록 함
                        mainChannelControl.changeChannel(options.obj.channel, options.obj.isSelect);
                    }
                    else {
                        // tune 하지 않는 경우는 nav channel control 에 의한 tune이 발생한 경우이다.
                        // 그러므로 channel control을 resume 해서 miniEpg가 노출되도록 한다.
                        // 단, STANDBY 상태에서 넘어오는 경우는 홈 메뉴가 노출되어야 하므로 channel select 만 수행하도록 수정
                        if (stateManager.serviceState === CONST_SVC_STATE.STANDBY) {
                            mainChannelControl.resume(false);
                        }
                        else {
                            mainChannelControl.resume(true);
                        }
                    }
                }
                else {
                    if (isTune) {
                        if (stateManager.serviceState != CONST_SVC_STATE.TV_VIEWING && stateManager.serviceState != CONST_SVC_STATE.OTHER_APP_ON_TV) {
                            // 현재 채널이 DATA 채널이라면 promo 채널로 tune
                            // 이외의 경우에는 LCW 채널로 tune
                            var channel = adapterHandler.navAdapter.getPromoChannel();
                            var curChannel = mainChannelControl.getCurrentChannel();
                            if (curChannel && stateManager.serviceState !== CONST_SVC_STATE.VOD && stateManager.serviceState !== CONST_SVC_STATE.OTHER_APP_ON_VOD) {
                                if (curChannel.channelType !== Channel.TYPE_OTHER) {
                                    channel = curChannel;
                                    // OTS audio 채널의 경우 이전 채널로 tune
                                    if (KTW.CONSTANT.IS_OTS && curChannel.channelType === Channel.TYPE_RADIO) {
                                        channel = mainChannelControl.getBackChannel();
                                    }
                                }
                            }
                            mainChannelControl.changeChannel(channel, isChSelect);
                        }
                    }
                }
                break;
            case CONST_SVC_STATE.VOD:
                if (stateManager.serviceState == CONST_SVC_STATE.TV_VIEWING || stateManager.serviceState == CONST_SVC_STATE.OTHER_APP_ON_TV) {
                    stopAV(options);

                    // TODO other app on av 상태일때는 필요에 따라 requestshow 를 호출해야 할 수도 있음. 확인 필요
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.OTHER_APP) {
                    // TODO 풀브라우져나 데이터 채널 앱에서 바로 홈포탈 VOD 실행하는 경우 없음 (?) 확인 필요
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.OTHER_APP_ON_VOD) {
                    restoreLayer({keyCode: options.key, blockRestoreLayer: !options.restoreLayer, skipRequestShow: options.skipRequestShow});
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.TIME_RESTRICTED) {
                    hideTimeRestrictPopup();
                }

                if (!options.restoreLayer) {
                    _setKeyPriority({priority: KEY_PRIORITY.VOD_HIDE});
                }

                break;
            case CONST_SVC_STATE.OTHER_APP:
                if (stateManager.serviceState == CONST_SVC_STATE.TV_VIEWING) {
                    /**
                     * [dj.son] [WEBIIIHOME-3612] TV_VIEWING 상태에서 OTHER_APP 상태로 전환시 main ChannelControl 의 timer clear
                     */
                    mainChannelControl.clearCheckServiceStateTimer();
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.VOD || stateManager.serviceState == CONST_SVC_STATE.OTHER_APP_ON_VOD) {
                    // TODO VOD 종료 로직 추가 (? 이건 vod 모듈에서 listener 로 해결 가능)
                    stopAV(options);
                }
                else if (stateManager.serviceState == CONST_SVC_STATE.TIME_RESTRICTED) {
                    hideTimeRestrictPopup();
                }

                /**
                 * [dj.son] other app 으로 jump 시, 추후 reume 이 실행되도록 수정
                 */
                //if (options.visible) {
                //    layerManager.hideNormalLayer();
                //}
                //else {
                //    layerManager.clearNormalLayer();
                //}
                layerManager.hideNormalLayer();

                // [dj.son] 데이터 채널이면 채널 튠, 풀브라우저면 나중에 notify stop 이 날라올때 channel pause
                var result = startApplication(options.obj);

                // 2017.11.20 sw.nam
                // date 채널에서 넘어오는 경우 state 변환하지 않아  WEBIIIHOME-3527 이슈가 발생(일반 채널로 전환 시 홈메뉴가 뜨지 않음)
                // changeForced 파라미터를 추가하여 date 채널에서 일반 채널로 넘어오는 경우 홈메뉴가 뜨도록 수정.WEBIIIHOME-3527
                if (!result && !options.changeForced) {
                    log.printDbg("startApplication is faild... so return...");
                    return;
                }

                // 2017.04.04 dhlee
                // STB_POINT_CACHE 영역의 ReqFalg 를 true 로 set
                // 2017.06.02 dhlee
                // 데이터 존재 여부와 상관 없이 ReqFlag를 true로 set 하도록 한다.
                var cacheObj = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.STB_POINT_CACHE);
                if (cacheObj && cacheObj !== "undefined") {
                    var data = JSON.parse(cacheObj);
                    if (!data.ReqFlag) {
                        data.ReqFlag = true;
                        KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
                    }
                }
                else {
                    var data = {};
                    data.ReqFlag = true;
                    KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
                }
                break;
            case CONST_SVC_STATE.OTHER_APP_ON_TV:
            case CONST_SVC_STATE.OTHER_APP_ON_VOD:
                // [dj.son] visible options 에 따라 layer hide or clear
                // [dj.son] 메뉴트리에 따라 AppStore 를 실행했을때는 visible true, 그 외의 경우에는 false
                if (options.visible) {
                    layerManager.hideNormalLayer();
                }
                else {
                    layerManager.clearNormalLayer();
                }

                startApplication(options.obj);
                break;
            case CONST_SVC_STATE.TIME_RESTRICTED:
                stopAV(options);
                showTimeRestrictPopup();
                break;
            case CONST_SVC_STATE.STANDBY:
                layerManager.clearNormalLayer();
                stopAV(options);

                /**
                 * [dj.son] [WEBIIIHOME-3592] 대기모드 진입시, PinMode 종료하도록 예외처리 추가
                 * 2018.01.04 sw.nam  [WEBIIIHOME-3592] 보안 코드 재 적용
                 */
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.checkPinModeStop();

                /**
                 * [dj.son] 첫 홈키 프로세스 초기화
                 */
                processing_first_home_key = false;
                break;
            /**
             * 2017.07.19 dhlee
             * 키즈 모드에서 제한 설정을 한 상태에서 대기 -> 동작 모드로 전환 시 설정되는 state 이다.
             */
            case CONST_SVC_STATE.KIDS_RESTRICTION:
                log.printDbg("_changeService(), KIDS_RESTRICTION state. do nothing.");
                break;
        }

        stateManager.setServiceState(options);
    }

    function retainLayer () {
        log.printDbg("retainLayer()");

        layerManager.hideNormalLayer();
    }

    function restoreLayer (options) {
        log.printDbg("restoreLayer(" + log.stringify(options) + ")");

        options = options || {};

        if (options.blockRestoreLayer) {
            log.printDbg("blockRestoreLayer is true.... so return...");
            return;
        }

        var isHotKey = false;

        if (options.keyCode) {
            isHotKey = layerManager.isHotKey(options.keyCode);
        }

        if (isHotKey) {
            layerManager.handleHotKeyEvent(options.keyCode, options.skipRequestShow);
        }
        else {
            var layer = layerManager.getLastLayerInStack({
                key: "type",
                value: KTW.ui.Layer.TYPE.NORMAL
            });

            if (layer) {
                layerManager.resumeNormalLayer({
                    skipRequestShow: options.skipRequestShow,
                    delayRequestShow: options.delayRequestShow
                });
            }
            else {
                if (options.isHome) {
                    layerManager.activateHome({skipRequestShow: options.skipRequestShow});
                }
            }
        }
    }

    /**
     * check data service parameter
     *
     * @param type
     * @param param
     */
    function isValidDataService(type, param) {
        var result = true;
        var channel;
        if (type === CONST_APP_TYPE.MULTICAST) {
            if (util.isValidVariable(param) === true) {
                if (param.split(".").length === 3) {
                    channel = KTW.oipf.adapters.NavAdapter.getChannelByTriplet(param);
                    if (util.isValidVariable(channel) === false) {
                        result = false;
                    }
                }
            }
            else {
                result = false;
            }
        }
        else if (type === CONST_APP_TYPE.USB) {
            channel = KTW.oipf.adapters.NavAdapter.getChannelByTriplet(param.substring(6));
            if (util.isValidVariable(channel) === false) {
                result = false;
            }
        }

        return result;
    }

    /**
     * start external app
     *
     * - WebBrowser, AppStore, Unicast App, Multicast App, Mashup Child App, USB App, Data Channel App
     *
     * @param options type - Application type
     *                 param - parameter
     *                 ex_param - 추가 parameter
     */
    function startApplication(options) {
        log.printDbg("_startApplication()");

        options = options || {};

        var message;


        /**
         * [dj.son] 채널 튠을 하는 부분을 한군데로 통일하도록 수정  (WEBIIIHOME-3590)
         * 2018.01.04 [sw.nam] 잔존 이슈 다시 수정 코드로 원복 작업 (WEBIIIHOME-3590)
         */
        var channel = null;


        /**
         * [dj.son] [WEBIIIHOME-3417] 다른 Application 실행 성공 여부
         * - postMessage, startAppByLocator 에 의해 실행되는 앱들은 성공여부를 알 수 없기 때문에 true 리턴
         * - Data Channel 로 튠 할때, 채널이 없거나 실제로는 일반 채널인 경우 TV_VIEWING 상태로 전환 후 false 리턴
         *
         * 기존에는 is_data_service 란 변수가 있었으나 실제로는 사용하지 않았고 무조건 true 로 리턴했었기 때문에, 영향 없음
         */
        var result = true;

        switch(options.type) {
            case CONST_APP_TYPE.WEB_BROWSER :
                log.printDbg("_startApplication() WEB_BROWSER");

                message = {
                    "method": messageManager.METHOD.OBS.START_BROWSER,
                    "to" : KTW.CONSTANT.APP_ID.OBSERVER,
                    "args" : options.param
                };
                break;
            case CONST_APP_TYPE.YOUTUBE :
                // 2017.06.26 dhlee
                // KT 요구사항에 따라 추가
                log.printDbg("_startApplication() YOUTUBE");

                /**
                 * jjh1117 2017/09/13
                 * 프로모채널이 시청제한인 경우 Iframe을 안 그리는 이슈 수정함.
                 */
                KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).resetPerm();

                message = {
                    "method" : messageManager.METHOD.OBS.START_YOUTUBE,
                    "to" : KTW.CONSTANT.APP_ID.OBSERVER,
                    "args" : options.param
                };
                break;
            case CONST_APP_TYPE.UNICAST :
                log.printDbg("_startApplication() UNICAST");

                if (options.param.indexOf(".ait") !== -1) {
                    message = {
                        "method": messageManager.METHOD.OBS.START_UNICASTING_APP,
                        "to" : KTW.CONSTANT.APP_ID.OBSERVER,
                        "target" : options.param
                    };
                }
                else {
                    message = {
                        "method": messageManager.METHOD.EXT.RUN_APP_STORE,
                        "to" : KTW.CONSTANT.APP_ID.APPSTORE,
                        "id_flag" : "A",
                        "AssetID" : options.param
                    };
                }
                break;
            case CONST_APP_TYPE.MULTICAST :
                log.printDbg("_startApplication() MULTICAST");

                if (options.param.split(".").length === 3) {
                    // 양방향 app에 parameter를 전달하기 위해서 configuration.setText 처리함
                    if (util.isValidVariable(options.ex_param) === true) {
                        adapterHandler.basicAdapter.setConfigText(DP_KEY + options.param, options.ex_param);
                    }
                    //adapterHandler.serviceAdapter.startAppByLocator(options.param);
                    channel = adapterHandler.navAdapter.getChannelByTriplet(options.param);
                    if (!channel) {
                        result = false;
                    }
                }
                else {
                    options.param = options.param.replaceAll('0x','').toLowerCase();

                    // mashup child app 처리
                    if (util.isValidVariable(options.ex_param) === true && options.param === KTW.CONSTANT.APP_ID.MASHUP) {
                        message = {
                            "method": KTW.managers.MessageManager.METHOD.HP.SHOW_CHILD_APP,
                            "from": KTW.CONSTANT.APP_ID.HOME,
                            "to" : options.param,
                            "child_app" : options.ex_param,
                            "hide_after_send" : true
                        };
                        //postMessage를 통한 Jump
                        KTW.managers.UserLogManager.collect({
                            type: KTW.data.UserLog.TYPE.JUMP_TO,
                            act: KTW.data.EntryLog.JUMP.CODE.POST_MSG,
                            jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                            locator: ""
                        });
                        //TODO sw.nam childapp locator param 알아내서 추가하기
                    }
                    else {
                        // unbound app 런칭
                        message = {
                            "method": messageManager.METHOD.OBS.START_UNBOUND_APPLICATION,
                            "to" : KTW.CONSTANT.APP_ID.OBSERVER,
                            "target" : options.param
                        };
                    }
                }
                break;
            case CONST_APP_TYPE.USB :
                log.printDbg("_startApplication() USB");

                var locator = options.param.substring(6);

                // browser 상태에서는 requestDestroyFullBrowser()를 호출한다.
                var osd_state = adapterHandler.extensionAdapter.getOSDState();
                if (osd_state === 1) {
                    var usbChannel = adapterHandler.navAdapter.getChannelByTriplet(locator);
                    if (usbChannel) {
                        adapterHandler.extensionAdapter.requestDestroyFullBrowser(usbChannel);
                    }
                    else {
                        result = false;
                    }
                }
                else {
                    //adapterHandler.serviceAdapter.startAppByLocator(locator);
                    channel = adapterHandler.navAdapter.getChannelByTriplet(locator);
                    if (!channel) {
                        result = false;
                    }
                }
                break;
            case CONST_APP_TYPE.DATA_CHANNEL :
                channel = options.param;
                if (!channel) {
                    result = false;
                }
                break;
        }

        if (channel) {
            if (adapterHandler.navAdapter.isDataServiceChannel(channel)) {
                mainChannelControl.changeChannel(channel, true, false, false, false);
            }
            else {
                mainChannelControl.changeChannel(channel, false, false, false, false);

                _changeService({
                    nextServiceState: CONST_SVC_STATE.TV_VIEWING,
                    obj: {
                        channel: channel,
                        tune: "false"
                    }
                });

                result = false;
            }
        }

        if (message) {
            message.from = KTW.CONSTANT.APP_ID.HOME;
            messageManager.sendMessage(message);
        }

        return result;
    }

    /**
     * start child app
     *
     * @param url - child app location
     */
    function _startChildApp (url) {
        log.printDbg("_startChildApp(" + url + ")");

        layerManager.startLoading({preventKey: true});

        setTimeout(function() {
            childApp = adapterHandler.serviceAdapter.self.createApplication(url, true);
        }, 500);

        adapterHandler.serviceAdapter.setAppLoadListener(function(type, app) {
            log.printDbg("_startChildApp() type = " + type);

            if (childApp === app) {
                setTimeout(layerManager.stopLoading, 300);

                if (type === "error") {
                    var popupData = {
                        arrMessage: [{
                            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                            message: ["오류"],
                            cssObj: {}
                        }, {
                            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                            message: [KTW.ERROR.EX_CODE.APP.EX001[0], KTW.ERROR.EX_CODE.APP.EX001[1]],
                            cssObj: {}
                        }],

                        arrButton: [{id: "ok", name: "확인"}],

                        cbAction: function (id) {
                            if (id === "ok") {
                                KTW.ui.LayerManager.deactivateLayer({id: "app_loading_fail_popup"});
                            }
                        }
                    };

                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: "app_loading_fail_popup",
                            type: KTW.ui.Layer.TYPE.POPUP,
                            priority: KTW.ui.Layer.PRIORITY.POPUP,
                            view : KTW.ui.view.popup.BasicPopup,
                            params: {
                                data : popupData
                            }
                        },
                        visible: true,
                        cbActivate: function () {}
                    });

                    childApp = null;
                }
                else  if (type === "unloaded") {
                    childApp = null;
                }

                if (childApp === null) {
                    adapterHandler.serviceAdapter.setAppLoadListener(null);
                }
            }
        });
    }

    /**
     * destroy child app
     */
    function _destroyChildApp () {
        try {
            if (childApp !== null) {
                layerManager.stopLoading();

                log.printDbg("_destroyChildApp()");

                adapterHandler.serviceAdapter.setAppLoadListener(null);
                childApp.destroyApplication();
                childApp = null;
            }
        }
        catch(e) {
            log.printErr("_destroyChildApp() occured error");
            log.printExec(e);
        }
    }

    /**
     * stop AV
     *
     * @param options nextServiceState - next state
     *                 obj - next state info
     */
    function stopAV(options) {
        log.printDbg("stopAV() current_state = " + stateManager.getServiceStateString(stateManager.serviceState));

        options = options || {};

        if (stateManager.serviceState === CONST_SVC_STATE.TV_VIEWING || stateManager.serviceState === CONST_SVC_STATE.OTHER_APP_ON_TV) {
            // TV_VIEWING 상태라면 channel 및 channel control 기능 pause
            var stop_channel = true;
            if (options.nextServiceState === CONST_SVC_STATE.OTHER_APP) {
                if (options.obj && options.obj.type === CONST_APP_TYPE.DATA_CHANNEL) {
                    if (util.isValidVariable(options.obj.param) === false) {
                        // DATA 채널로 이동하는데 실제 data channel 정보가 없다면
                        // 이미 해당 채널로 이동한 상태이기 때문에
                        // channel stop 처리하지 않도록 함
                        stop_channel = false;
                    }
                }
            }

            log.printDbg("stopAV() stop_channel = " + stop_channel);

            mainChannelControl.pause(stop_channel);

            // PIP 및 stitch channel control을 stop 해서 main 이외의 vbo가 보여지지 않도록 한다.
            var pip_cc = adapterHandler.navAdapter.getChannelControl(NAV_CONTROL.PIP);
            if (pip_cc !== null && pip_cc.getState() === NAV_CONTROL.STATE.STARTED) {
                pip_cc.stop();
            }
            var stitching_cc = adapterHandler.navAdapter.getChannelControl(NAV_CONTROL.STITCHING);
            if (stitching_cc !== null && stitching_cc.getState() === NAV_CONTROL.STATE.STARTED) {
                stitching_cc.stop();
            }
        }
        else if (stateManager.serviceState === CONST_SVC_STATE.VOD || stateManager.serviceState === CONST_SVC_STATE.OTHER_APP_ON_VOD) {
            // VOD 상태에서 시청시간 제한 상태로 전환되는 경우 VOD pause 하고
            // 이외의 상태로 전환되는 경우에는 VOD stop 처리한다.
            // R5 KTWHPTZOF-1492 이슈 수정사항
            // VOD 재생 중 - 일시정지 - 시청시간제한 - 대기모드 시나리오를 타게되면 current_state가 마지막에 VOD가 아니기 때문에 정상 종료하지 않음.
            // 즉, 정상종료 되지 않고 채널로 돌아가기 때문에 VOD 재생에 필요한 레이어들이 제거되지 않고 화면에 노출됨.
            // 그래서 시청시간제한 걸릴 때 이전 상태를 기억해두는 변수를 체크하여 VOD 일 경우 VOD를 종료하도록 수정

            // TODO 2016.07.08 Deajoong Son
            // vod stop 시키는 함수 추가 해야 함
            if (options.nextServiceState === CONST_SVC_STATE.TIME_RESTRICTED) {
                //vodControl.pause();
            }
            else {
                //adapterHandler.vodAdapter.stopPlayer(KTW.vod.Def.EXIT_TYPE.EXIT_FORCE);
            }
        }
        else {
            mainChannelControl.pause(true);
        }
    }

    /**
     * [jh.lee] 시청시간제한팝업 생성
     */
    function showTimeRestrictPopup () {
        log.printDbg("showTimeRestrictPopup()");

        // 시청시간 제한 popup의 경우 SYSTEM popup 보다
        // 상위에 노출하여 모든 POPUP 및 UI 상위에 노출될 수 있도록 함
        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: timeRestrictLayerId,
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5,
                linkage: true,
                view : KTW.ui.view.popup.TimeRestrictedPopup,
                params: {
                    dimmed: "false",
                    enableMenuKey : "false",
                    enableExitKey : "false",
                    enableBackKey : "false",
                    enableDCA : "false",
                    enableChannelKey : "false",
                    enableHotKey : "false"
                }
            },
            visible: true
        });

        KTW.managers.service.IframeManager.setType(KTW.managers.service.IframeManager.DEF.TYPE.TIME_RESTRICTED);

        /**
         * [dj.son] [WEBIIIHOME-3320] VOD 재생 상태 도중 시청시간제한 팝업을 띄우면,
         *                            로직상 이전 화면이 hide 상태인 경우 requestShow 를 안보내게 되고 이때문에 hp_showOtherApp 이 날라올 수 있음
         *
         * - 시청 시간 제한 팝업은 무조건 띄워야 되는 팝업이므로, requestShow 를 무조건 보내도록 예외처리
         * - 실제로는 LayerManager notifyAppVisible 함수 내용을 손봐야 하지만, 현 상황에서는 위험부담이 너무 크므로 아래 코드로 예외처리함
         * - 이렇게 requestShow 를 보내면 hp_showOtherApp 메세지가 오지는 않지만,
         *   혹시 모르니 hp_showOtherApp 메세지가 오더라도 대기모드, 시청시간제한 상태이면 무시하도록 예외처리 추가
         */
        KTW.managers.MessageManager.sendMessage({
            method: KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW,
            from: KTW.CONSTANT.APP_ID.HOME,
            to : KTW.CONSTANT.APP_ID.OBSERVER
        });
    }

    function hideTimeRestrictPopup () {
        log.printDbg("hideTimeRestrictPopup()");

        hidePopupOnTimeRestrict();

        /**
         * 시청 팝업이 닫혔다는 것은 어떠한 형태로도 시청 시간 제한이 풀렸다는 소리이므로 reset check
         */
        _clearTimeRestrictOptions();

        layerManager.deactivateLayer({
            id: timeRestrictLayerId,
            remove: true,
            onlyTarget: true
        });

        KTW.managers.service.IframeManager.setType(KTW.managers.service.IframeManager.DEF.TYPE.NONE);
    }

    /**
     * [dj.son] 시청 시간 제한 팝업 위에 떠 있는 모든 SYSTEM POPUP 제거
     * 시청 시간 제한 팝업 위에 뜨는 팝업들은 전부 priority 가 KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5 이므로
     * KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5 priority 를 가진 SYSTEM POPUP 들을 전부 제거
     */
    function hidePopupOnTimeRestrict () {
        log.printDbg("hidePopupOnTimeRestrict()");

        /**
         * while 문을 사용하지 않기 위한 최소한의 안전장치 (최대한 동시에 activate 될 수 있는 system popup 이 10개를 넘지 않으므로)
         */
        for (var i = 0; i < 10; i++) {
            var layer = layerManager.getLastLayerInStack({key: "priority", value: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5});

            if (!layer) {
                break;
            }

            layerManager.deactivateLayer({
                id: layer.id,
                remove: true,
                onlyTarget: true
            });
        }
    }

    /**
     * 시간 제한 설정값 reset
     * - 한번만 설정값이 true 일때만 실행
     * - 현재 시간값과 비교 후
     *
     * @param reset
     */
    function _clearTimeRestrictOptions () {
        log.printDbg("_clearTimeRestrictOptions()");

        var restrictOptions = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.RESTRICTION_TIME);

        if (restrictOptions) {
            /**
             *  N|20080411|18:04|18:10|1|Y
             *  해지(N) 여부 |오늘 날짜 | 시작시간 | 끝시간 | 0: 매일, 1:한번만 | 설정(Y),해지(N) 여부
             *  시작시간,끝시간 : HHMM
             */
            var arrOptions = restrictOptions.split("|");
            if (arrOptions && arrOptions.length > 0) {
                if (arrOptions[5] === "Y") {
                    if (arrOptions[4] === "1") {
                        var year = arrOptions[1].substr(0, 4);
                        var month = arrOptions[1].substr(4, 2);
                        var day = arrOptions[1].substr(6, 2);
                        var startHour = arrOptions[2].substr(0, 2);
                        var startMin = arrOptions[2].substr(3, 2);
                        var endHour = arrOptions[3].substr(0, 2);
                        var endMin = arrOptions[3].substr(3, 2);

                        var endDate = new Date(year, parseInt(month) - 1, day, endHour, endMin);
                        if (startHour > endHour || (startHour === endHour && startMin > endMin)) {
                            endDate.setDate(endDate.getDate() + 1);
                        }

                        var nowDate = new Date();

                        if (nowDate >= endDate) {
                            var resetVal = "N|" + KTW.utils.util.getFormatDate(nowDate, "yyyyMMdd") + "|00:00|00:00|1|N";

                            KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.RESTRICTION_TIME, resetVal);
                        }
                    }
                }
            }
        }
    }


    function _checkFirstKey () {
        return complete_process_first_key;
    }

    function _cancelCallbackProcessFirstKey () {
        callbackProcessFirstKey = null;
    }

    function _saveCallbackProcessFirstKey (cbProcessFirstKey) {
        callbackProcessFirstKey = cbProcessFirstKey;
    }

    function _processFirstKey (cbProcessFirstKey) {
        log.printDbg("processFirstKey()");

        /**
         * [dj.son] [WEBIIIHOME-483] 부팅시 시간제한 상태라면 첫키 로직 안타도록 수정
         */
        if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            // 2017.09.28
            layerManager.clearNormalLayer();
            return;
        }

        if (!processing_first_key) {
            processing_first_key = true;

            callbackProcessFirstKey = cbProcessFirstKey;

            var lastLayer = layerManager.getLastLayerInStack();
            if (lastLayer && lastLayer.type > KTW.ui.Layer.TYPE.TRIGGER) {
                // show loading
                layerManager.startLoading({preventKey: true, isHide: true});
            }

            powerModeManager.setFirstKeyEvent();
            amocManager.getCustEnv(function (success, result) {
                onCustEnv(processing_first_key, success, result, function () {
                    /**
                     * [dj.son] 비즈 상품인 경우, 첫키 로직때 비즈 메뉴 업데이트 하도록 수정, 이미 받은 경우에는 버전이 동일하기 때문에 수행 안함
                     */
                    if (KTW.CONSTANT.IS_BIZ) {
                        KTW.managers.data.OCManager.updateBizMenu(true);
                    }

                    KTW.managers.module.ModuleManager.checkModuleData(function () {
                        complete_process_first_key = true;
                        layerManager.stopLoading();

                        if (callbackProcessFirstKey) {
                            callbackProcessFirstKey();
                        }
                    });

                    /**
                     * [dj.son] 첫키 시나리오에서 getCustEnv 호출 후 홈샷 데이터 가져오는 로직 추가
                     */
                    KTW.managers.service.HomeShotDataManager.getHomeMenuHomeShot();

                });
            }, KTW.SAID);
            caHandler.requestCaMessage(caHandler.TAG.REQ_PRODUCT_INFO, undefined, onResponseCaMessage);

            // 2016.11.29 dhlee ReservationManager 초기화 (매쉬업 시리즈 예약 지원 여부 확인)
            // 2.0 R7에서 KT와 첫키 시나리오에 추가하기로 협의되어 3.0에도 반영
            // init()은 main.js 에서 부팅 시에 호출하고, 여기서는 시리즈 지원 여부 확인을 위한 method 를 호출한다.
            KTW.managers.service.ReservationManager.decideSupportingSeries();


        }
    }

    function onCustEnv(flag, success, result, callback) {
        log.printDbg("onCustEnv() success : " + success);

        if (success) {
            try {
                KTW.oipf.adapters.vodAdapter.updateOTMParing(result);

                storageManager.ps.save(storageManager.KEY.CUST_ENV, JSON.stringify(result));
                if (flag) {
                    //storageManager.ps.save(storageManager.KEY.CUST_ENV, JSON.stringify(result));
                        //amocManager.getMyPkgList(onMyPkgList);
                    amocManager.getMyPkgList(function (success, result) {
                        onMyPkgList(success, result, callback, true);
                    });
                } else {
                    var productId = "";
                    try{
                        productId = JSON.parse(storageManager.ps.load(storageManager.KEY.CUST_ENV)).pkgCode;
                    }catch(e){}

                    log.printDbg("onCustEnv() productId : " + productId + " , result.pkgCode : " + result.pkgCode);
                    //storageManager.ps.save(storageManager.KEY.CUST_ENV, JSON.stringify(result));
                    if(productId !== result.pkgCode) {
                        //amocManager.getMyPkgList(onMyPkgList);
                        amocManager.getMyPkgList(function (success, result) {
                            onMyPkgList(success, result, callback, true);
                        });
                    } else {
                        _getOfficeCode(callback);
                    }
                }
            }
            catch(e) {
                log.printErr(e);

                if (callback) {
                    callback();
                }
            }
        }
        else {
            log.printWarn("onCustEnv() fail");

            if (callback) {
                callback();
            }
        }
    }

    /**
     * getMyPkgList API callback 처리 함수
     *
     * @param success
     * @param result
     * @param callback
     * @param updateOfficeCode : boolean
     */
    function onMyPkgList(success, result, callback, updateOfficeCode) {
        log.printDbg("onMyPkgList2() success = " + success);

        if (success) {
            if (!result) {
                // R6 CEMS
                // success 값이 true 이지만 result 값 자체가 null 인 경우 CEMS 리포트
                KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("VODE-00013");
            } else {
                if (result.pkgList) {
                    var pkg_list = result.pkgList;
                    var pkg_code_list = "";
                    var base_pkg_code = "";
                    var all_pkg_code_list = "";

                    // 2017.07.28 dhlee
                    // getMyPkgList() API의 응답 데이터가 항상 JSON Array로 전달되는 것이 아니므로
                    // pkgList 가 Array인지를 검사하여 array인 경우와 object 인 경우를 구분해서 처리하도록 해야 한다.
                    if (result.pkgList instanceof Array) {
                        for (var i = 0; i < pkg_list.length; i++) {
                            all_pkg_code_list += pkg_list[i].pkgCode + ",";
                            if (pkg_list[i].pkgClass == "1") {
                                //기본팩
                                base_pkg_code += pkg_list[i].pkgCode + ",";
                            } else {
                                if (pkg_list[i].pkgClass !== "" && pkg_list[i].pkgClass !== undefined && pkg_list[i].pkgClass !== null) {
                                    // pkgClass가 있는 부가팩
                                    pkg_code_list += pkg_list[i].pkgCode + ",";
                                }
                            }
                        }

                        if (pkg_code_list) {
                            pkg_code_list = pkg_code_list.substring(0, pkg_code_list.length - 1);
                        }
                        if (base_pkg_code) {
                            base_pkg_code = base_pkg_code.substring(0, base_pkg_code.length - 1);
                        }
                        if (all_pkg_code_list) {
                            all_pkg_code_list = all_pkg_code_list.substring(0, all_pkg_code_list.length - 1);
                        }

                    } else {
                        all_pkg_code_list = pkg_list.pkgCode;
                        if (pkg_list.pkgClass == "1") {
                            //기본팩
                            base_pkg_code = pkg_list.pkgCode;
                        } else {
                            if (pkg_list.pkgClass !== "" && pkg_list.pkgClass !== undefined && pkg_list.pkgClass !== null) {
                                pkg_code_list = pkg_list.pkgCode;
                            }
                        }
                    }
                    log.printDbg("onMyPkgList(), pkg_code_list = " + pkg_code_list);
                    log.printDbg("onMyPkgList(), base_pkg_code = " + base_pkg_code);
                    log.printDbg("onMyPkgList(), all_pkg_code_list = " + all_pkg_code_list);

                    storageManager.ms.save(storageManager.MEM_KEY.PKG_LIST, pkg_code_list);
                    storageManager.ms.save(storageManager.MEM_KEY.PKG_BASE_LIST, base_pkg_code);
                    storageManager.ms.save(storageManager.MEM_KEY.PKG_LIST_ALL, all_pkg_code_list);
                } else {
                    log.printWarn("onMyPkgList() package list is null");
                }
            }
        }
        else {
            // R6 CEMS.
            // success 값이 false 전달되어 네트워크 관련 오류인 경우 CEMS 리포트(timeout 등)
            if (result !== "abort") {
                KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("VODE-00013");
            }
            log.printWarn("onMyPkgList() fail");
        }
        if (updateOfficeCode) {
            _getOfficeCode(callback);
        } else {
            if (callback) {
                callback();
            }
        }
    }

    /**
     * 2017.06.14 dhlee
     *
     * office code를 획득한다.
     *
     * @param callback
     */
    function _getOfficeCode(callback) {
        log.printDbg("_getOfficeCode()");

        var officeCd = storageManager.ps.load(storageManager.KEY.OFFICE_CODE);
        if (officeCd !== null && officeCd !== undefined && officeCd.length > 0) {
            // 2017.01.20 dhlee office code 획득
            var lastRequestTime = storageManager.ps.load(storageManager.KEY.OFFICE_CODE_CALL_TIME);
            if (lastRequestTime !== null && lastRequestTime !== undefined) {
                var curTime = (new Date()).getTime();
                if (curTime >= (Number(lastRequestTime) + OFFICE_CODE_INTERVAL)) {
                    log.printDbg("onCustEnv(), interval case, call getMyGlbIpNhp API");
                    amocManager.getMyGlbIpNhp(function (success, result) {
                        onMyGlbIpNhp(success, result, callback);
                    }, "H", "01");  // 2017.08.04 dhlee req_path_cd 01로 변경 AMOC API 테스트 수정 사항 반영
                }
                else {
                    if (callback) {
                        callback();
                    }
                }
            } else {
                log.printDbg("_getOfficeCode(), first case, call getMyGlbIpNhp API");
                amocManager.getMyGlbIpNhp(function (success, result) {
                    onMyGlbIpNhp(success, result, callback);
                }, "H", "01");
            }
        } else {
            log.printDbg("_getOfficeCode(), officeCd is not exist, call getMyGlbIpNhp API");
            amocManager.getMyGlbIpNhp(function (success, result) {
                onMyGlbIpNhp(success, result, callback);
            }, "H", "01");
        }
    }

    /**
     * getMyGlbIpNhp API response callback
     *
     * @param success
     * @param result
     */
    function onMyGlbIpNhp(success, result, callback) {
        log.printDbg("onMyGlbIpNhp(), success = " + success);
        log.printDbg("onMyGlbIpNhp(), result = " + JSON.stringify(result));
        if (success) {
            if (!result) {
                KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("VODE-00013");
            } else {
                if (result.myGlbIpNhp) {
                    if (result.myGlbIpNhp.officeCd) {
                        storageManager.ps.save(storageManager.KEY.OFFICE_CODE, result.myGlbIpNhp.officeCd);
                        storageManager.ps.save(storageManager.KEY.OFFICE_CODE_CALL_TIME, (new Date()).getTime());
                    } else {
                        log.printDbg("onMyGlbIpNhp(), result.myGlbIpNhp.officeCd does not exist");
                    }
                } else {
                    log.printDbg("onMyGlbIpNhp(), result.myGlbIpNhp does not exist");
                }
            }
        } else {
            if (result !== "abort") {
                KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("VODE-00013");
            }
            log.printWarn("onMyGlbIpNhp() fail");
        }

        if (callback) {
            callback();
        }
    }

    function onResponseCaMessage(result, data) {
        log.printDbg("onResponseCaMessage() result : " + result);

        if (result) {
            log.printDbg("onResponseCaMessage() data : " + JSON.stringify(data));

            if ((data.product_info_data & caHandler.PRODUCT_TYPE.B2B_SUBSCRIBER) === caHandler.PRODUCT_TYPE.B2B_SUBSCRIBER) {
                // [jh.lee] product_info_data 의 경우 여러개의 product 를 가질 수 있기 때문에 여러개의 product를 and bit 연산한 값이다
                // [jh.lee] 여기서 and 연산을 하여 0 이아닌 OTW.ca.CaHandler.PRODUCT_TYPE.B2B_SUBSCRIBER(0x2000) 값을 리턴하면
                // [jh.lee] 해당 상품에 가입되어있다는 결과이다.
                // [jh.lee] 가입되어있는 사용자이기 때문에 가입되어있는 CUG 리스트를 확보하기 위해 amoc 통신을 수행한다~
                amocManager.getCustCugInfo(onCustCugInfo, KTW.SAID);
            } else {
                // [jh.lee] 해당 사용자가 CUG 가입되어있지 않은 경우
                // [jh.lee] 또는 해지를 하는 경우인데 이 때는 이전 저장되있던 CUG 정보를 날려줘야 다음 부팅때 CUG채널로 부팅하지 않는다.
                // R6 단! 요구사항으로 LCW 가 생겼기 때문에 여기서 처리하지않고 스토리지 remove 함으로써 발생하는 콜백에서 처리하도록 한다.
                //       즉, menuDataManager 에서 처리한다.
                // [jh.lee] 이전 저장되어 있을 CUG 로컬 데이터를 삭제한다.
                storageManager.ps.remove(storageManager.KEY.CUST_CUG_INFO);
            }
        }
    }

    function onCustCugInfo(result, data) {
        log.printDbg("onCustCugInfo() result : " + result);

        try {
            log.printDbg("onCustCugInfo() result : " + JSON.stringify(data));
        } catch(e) {
            log.printExec(e);
        }

        if (result === true) {
            // [jh.lee] 성공
            if (data !== null && data !== undefined) {
                // [jh.lee] data 가 정상일 경우 로컬스토리지에 저장
                if (data.cugList) {
                    // 만약 data.cugList 값이 null 이나 undefined 이나 공백이 아닐 경우!
                    // 여러개라서 배열로왔는지 아니면 한개라서 객체로 왔는지 확인이 필요!
                    if (data.cugList.constructor !== Array) {
                        log.printDbg("onCustCugInfo get Object");
                        // Array 가아니면 한개의 객체 형태로 왔다는 것이기 때문에 배열화 해서 전달한다.
                        data.cugList = [data.cugList];
                    }
                }
                storageManager.ps.save(storageManager.KEY.CUST_CUG_INFO, JSON.stringify(data));
            }
        }
    }

    /**
     * 2017.05.11 dhlee
     *
     * getCustEnv()를 호출해야 하는 경우 사용할 API
     *
     * @param callback
     * @param forced    강제로 getMyPkgList() 를 수행할지에 대한 여부
     */
    function _requestCustInfo(callback, forced) {
        log.printDbg("_requestCustInfo()");

        amocManager.getCustEnv(function (success, result) {
            onCustEnv((forced === true ? forced : !processing_first_key), success, result, callback);
        }, KTW.SAID);
    }

    /**
     * 2017.06.27 dhlee
     * package 정보를 update 한다.
     *
     * @param callback
     */
    function _updatePkgInfo(callback) {
        log.printDbg("_updatePkgInfo()");

        amocManager.getMyPkgList(function (success, result) {
            onMyPkgList(success, result, callback, false);
        });
    }


    function _checkFirstHomeKey () {
        return processing_first_home_key;
    }

    /**
     * [dj.son] [2017-07-11] 첫 홈키 로직 수행
     * - 공지사항 모듈 로딩 후 API 호출
     */
    function _processFirstHomeKey () {
        log.printDbg("_processFirstHomeKey() : " + processing_first_home_key);

        if (!processing_first_home_key) {
            processing_first_home_key = true;

            // TODO 공지사항 모듈 로딩 및 API 호출
            KTW.managers.module.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_NOTICE, function (noticeModule) {
                if (noticeModule) {
                    noticeModule.execute({method: "getNoticeInfo"});
                }
            });
        }
   }

    return {
        KEY_PRIORITY: KEY_PRIORITY,

        init : _init,
        destroy : _destroy,

        changeService: _changeService,

        startChildApp: _startChildApp,
        destroyChildApp: _destroyChildApp,

        setKeyPriority: _setKeyPriority,

        checkFirstKey: _checkFirstKey,
        processFirstKey: _processFirstKey,
        cancelCallbackProcessFirstKey: _cancelCallbackProcessFirstKey,
        saveCallbackProcessFirstKey: _saveCallbackProcessFirstKey,

        requestCustInfo: _requestCustInfo,
        updatePkgInfo: _updatePkgInfo,
        getOfficeCode: _getOfficeCode,

        checkFirstHomeKey: _checkFirstHomeKey,
        processFirstHomeKey: _processFirstHomeKey,

        clearTimeRestrictOptions: _clearTimeRestrictOptions
    };
}());