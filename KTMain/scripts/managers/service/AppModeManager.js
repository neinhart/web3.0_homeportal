"use strict";

/**
 * manage app mode
 * 
 * KTW.CONSTANT.APP_MODE.NONE : 화면상 UI가 없는 mode
 * KTW.CONSTANT.APP_MODE.ICON : UI가 존재하지만 다른 UI와 동시에 보여질 수 있는 mode
 * KTW.CONSTANT.APP_MODE.FULL : 전체화면 UI가 보여지는 mode
 * 
 * 현재는 homemenu 1depth의 경우에만 ICON mode 이고 이외의 경우에는 NONE 또는 FULL mode 임.
 * NONE mode : LayerManager에서 STACK_TYPE의 UI가 없는 상태일 경우에 설정
 * FULL mode : LayerManager에서 STACK_TYPE의 UI가 시작되는 경우에 설정 
 * 
 */
KTW.managers.service.AppModeManager = (function () {
    
    var log = KTW.utils.Log;
    var const_app_mode = KTW.CONSTANT.APP_MODE;
    
    var current_mode = const_app_mode.NONE;
    
    var listeners = [];
    
    /**
     * whether or not current app mode is FULL
     */
    function _isFullMode() {
        return (current_mode === const_app_mode.FULL);
    }

    /**
     * set current application mode & notify change
     *
     * @param mode
     * @returns {boolean}
     */
    function _setMode(mode) {
        if (current_mode === mode) {
            return false;
        }
        log.printDbg("setMode() mode = " + getModeString(mode));
        current_mode = mode;
        
        var length = listeners.length;
        for (var i = 0; i < length; i++) {
            listeners[i](mode);
        }
        
        return true;
    }
    
    function _addAppModeListener(listener) {
        listeners.push(listener);
    }
    
    function _removeAppModeListener(listener) {
        var index = listeners.indexOf(listener);
        if (index !== -1) {
            listeners.splice(index, 1);
        }
    }
    
    function getModeString() {
        var mode_str;
        
        switch(current_mode) {
            case const_app_mode.NONE :
                mode_str = "NONE";
                break;
            case const_app_mode.ICON :
                mode_str = "ICON";
                break;
            case const_app_mode.FULL :
                mode_str = "FULL";
                break;
        }
        
        return mode_str;
    }
    
    return {
        isFullMode: _isFullMode,
        setMode: _setMode,
        get mode() {
            return current_mode;
        },
        
        addAppModeListener: _addAppModeListener,
        removeAppModeListener: _removeAppModeListener
    };
}());
