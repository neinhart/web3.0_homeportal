/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>HomeShotDataManager</code>
 *
 * - post message 로 오는 홈샷 or 1-depth 홈샷 데이터 관리
 * -- 각 카테고리는 해당 매니저에 홈샷 데이터가 있으면 우선 노출
 * -- 카테고리 홈샷 data 갱신은 get 할때 & 대기모드 진입시 수행
 * -- 데이터는 localStorage 에 저장
 *
 * - 1-depth 홈샷 API 호출 및 데이터 관리 (서버 통신)
 * -- 플래그가 true 일때, 서버 통신해서 데이터 가져옴, 플래그는 menu.js 가 갱신될때 true 로 설정
 * -- 실질적으로 서버 통신하는 시점은 첫키 시나리오 & menu.js 가 갱신된 후 홈메뉴 띄울때
 *
 * TODO MessageManager 에서 add / remove 함수 호출하도록 (신규 API 추가), 전체 가이드...
 *
 * @author dj.son
 * @since 2018-02-02
 */

KTW.managers.service.HomeShotDataManager = (function () {

    var HOMESHOT_TYPE = {
        MENU: 0,     // 메뉴 데이터에 포함된 홈샷
        CATEGORY: 1,     // 스마트 푸시를 통해 온 홈샷
        ADS_CONTENT: 2, // 1-depth 홈샷 (컨텐츠)
        ADS_IMAGE: 3    // 1-depth 홈샷 (이미지)
    };

    var DEFAULT_DURATION_TIME = 3; // 기본 홈샷 노출 시간 -> 3초

    var homeMenuHomeShot = [];
    var categoryHomeShot = [];

    var bGetHomeMenuHomeShot = true;

    /**
     * [dj.son] 서버로부터 1-depth 홈샷 가져옴
     */
    function getHomeMenuHomeShotData (callback) {
        KTW.utils.Log.printDbg("getHomeMenuHomeShotData() - " + bGetHomeMenuHomeShot);

        if (bGetHomeMenuHomeShot) {
            var url = KTW.DATA.HTTP.HOME_MENU_HOMESHOT.LIVE_URL;
            if (KTW.RELEASE_MODE === "BMT") {
                url = KTW.DATA.HTTP.HOME_MENU_HOMESHOT.BMT_URL;
            }

            var data = new Date();
            var now = (data.getYear() + 1900) + "-" +
                KTW.utils.util.numToStr(data.getMonth() + 1, 2) + "-" +
                KTW.utils.util.numToStr(data.getDate(), 2) + " " +
                KTW.utils.util.numToStr(data.getHours(), 2) + ":" +
                KTW.utils.util.numToStr(data.getMinutes(), 2) + ":" +
                KTW.utils.util.numToStr(data.getSeconds(), 2);

            var custEnv = null;
            var dongCd = "";
            try {
                custEnv = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CUST_ENV));
            }
            catch (e) {}

            if (custEnv && custEnv.dongCd) {
                dongCd = custEnv.dongCd;
            }

            var postData = "SAID=" + KTW.SAID + "&CRRTIME=" + now + "&DONGCODE=" + dongCd;

            KTW.managers.http.AjaxFactory.createAjax("getHomeMenuHomeShot", true, "post", "json", url, postData, null, 0, function (success, result) {
                if (success && result && result.RESULT === "1") {
                    homeMenuHomeShot = convertHomeMenuHomeShotData(result);

                    bGetHomeMenuHomeShot = false;
                }

                callback(homeMenuHomeShot);
            });
        }
        else {
            callback(homeMenuHomeShot);
        }
    }

    /**
     * [dj.son] 서버로부터 가져온 1-depth 홈샷 데이터를 컨버트
     */
    function convertHomeMenuHomeShotData (result) {
        KTW.utils.Log.printDbg("convertHomeMenuHomeShotData()");

        var arrHomeMenuHomeShot = [];

        for (var i = 0; i < result.ROLL_ASSET_LIST.length; i++) {
            try {
                var arrTargetData = result.ROLL_ASSET_LIST[i].TRGT_VAL.split("|");
                var hsTargetCatId = "";
                var hsTargetAssetId = "";
                var hsTargetLocator = "";
                var hsTargetParameter = "";

                switch (result.ROLL_ASSET_LIST[i].TRGT_TYPE) {
                    case KTW.DATA.MENU.ITEM.CATEGORY:
                        if (arrTargetData[0]) {
                            hsTargetCatId = arrTargetData[0];
                        }
                        break;
                    case KTW.DATA.MENU.ITEM.SERIES:
                        if (arrTargetData[0]) {
                            hsTargetCatId = arrTargetData[0];
                        }
                        break;
                    case KTW.DATA.MENU.ITEM.CONTENT:
                        if (arrTargetData[0]) {
                            hsTargetCatId = arrTargetData[0];
                        }
                        if (arrTargetData[1]) {
                            hsTargetAssetId = arrTargetData[1];
                        }
                        break;
                    case KTW.DATA.MENU.ITEM.INT_M:
                    case KTW.DATA.MENU.ITEM.INT_U:
                    case KTW.DATA.MENU.ITEM.INT_W:
                        if (arrTargetData[0]) {
                            hsTargetLocator = arrTargetData[0];
                        }
                        if (arrTargetData[1]) {
                            hsTargetParameter = arrTargetData[0];
                        }
                        break;
                }

                var hsDurationTime = DEFAULT_DURATION_TIME;

                if (result.ROLL_ASSET_LIST[i].ADS_DURATION_TIME && result.ROLL_ASSET_LIST[i].ADS_DURATION_TIME !== "") {
                    hsDurationTime = result.ROLL_ASSET_LIST[i].ADS_DURATION_TIME;
                }

                var homeShot = {
                    hsId: result.ROLL_ASSET_LIST[i].ADS_ID,
                    hsDurationTime: hsDurationTime,
                    hsType: result.ROLL_ASSET_LIST[i].ADS_TYPE,
                    hsImgUrl: result.ROLL_ASSET_LIST[i].ADS_URL,
                    hsTargetType: result.ROLL_ASSET_LIST[i].TRGT_TYPE + "",
                    hsTargetCatId: hsTargetCatId,
                    hsTargetAssetId: hsTargetAssetId,
                    hsTargetLocator: hsTargetLocator,
                    hsTargetParameter: hsTargetParameter
                };

                arrHomeMenuHomeShot.push(homeShot);
            }
            catch (e) {
                KTW.utils.Log.printErr(e);
            }
        }

        return arrHomeMenuHomeShot;
    }

    /**
     * [dj.son] 서버로부터 가져온 1-depth 홈샷 데이터를 컨버트
     */
    function convertCategoryHomeShotData (data) {
        KTW.utils.Log.printDbg("convertCategoryHomeShotData()");

        var homeShot = {
            ransactionId: data.transaction_id,
            catId: data.cat_id,
            hsImgUrl: data.hs_img_url,
            hsType: HOMESHOT_TYPE.CATEGORY,
            hsTargetType: data.hs_type + "",
            hsTargetCatId: "",
            hsTargetAssetId: "",
            hsTargetLocator: "",
            hsTargetParameter: "",

            hsStartDate: data.hs_start_date,
            hsExpireDate: data.hs_expire_date,

            hsRealDisplayCnt: 0,

            reqCd: data.req_cd
        };


        var hsDisplayCnt = "A";
        if (data.hs_display_cnt && data.hs_display_cnt !== "A") {
            try {
                hsDisplayCnt = Number(data.hs_display_cnt);
                if (hsDisplayCnt === NaN) {
                    hsDisplayCnt = "A";
                }
            }
            catch (e) {
                KTW.utils.Log.printErr(e);
            }
        }

        homeShot.hsDisplayCnt = hsDisplayCnt;

        var hsDurationTime = DEFAULT_DURATION_TIME;

        if (data.hs_duration_time && data.hs_duration_time !== "") {
            hsDurationTime = Number(data.hs_duration_time);
        }

        homeShot.hsDurationTime = hsDurationTime;

        try {
            var arrTargetData = data.hs_target_val.split("|");

            switch (homeShot.hsTargetType) {
                case KTW.DATA.MENU.ITEM.CATEGORY:
                    if (arrTargetData[0]) {
                        homeShot.hsTargetCatId = arrTargetData[0];
                    }
                    break;
                case KTW.DATA.MENU.ITEM.SERIES:
                    if (arrTargetData[0]) {
                        homeShot.hsTargetCatId = arrTargetData[0];
                    }
                    break;
                case KTW.DATA.MENU.ITEM.CONTENT:
                    if (arrTargetData[0]) {
                        homeShot.hsTargetCatId = arrTargetData[0];
                    }
                    if (arrTargetData[1]) {
                        homeShot.hsTargetAssetId = arrTargetData[1];
                    }
                    break;
                case KTW.DATA.MENU.ITEM.INT_M:
                case KTW.DATA.MENU.ITEM.INT_U:
                case KTW.DATA.MENU.ITEM.INT_W:
                    if (arrTargetData[0]) {
                        homeShot.hsTargetLocator = arrTargetData[0];
                    }
                    if (arrTargetData[1]) {
                        homeShot.hsTargetParameter = arrTargetData[1];
                    }
                    break;
            }
        }
        catch (e) {
            KTW.utils.Log.printErr(e);
        }

        return homeShot;
    }

    /**
     * [dj.son] 카테고리 홈샷 데이터 유효성 체크
     */
    function checkCategoryHomeShot (arrHomeShot) {
        KTW.utils.Log.printDbg("checkCategoryHomeShot()");

        var result = [];

        var now = (new Date()).getTime();

        if (arrHomeShot && arrHomeShot.length > 0) {
            for (var i = 0; i < arrHomeShot.length; i++) {
                var homeShot = arrHomeShot[i];

                /**
                 * [dj.son] 먼저 realDisplayCnt 값 체크
                 */

                if (homeShot.hsDisplayCnt !== "A" && homeShot.hsDisplayCnt <= homeShot.hsRealDisplayCnt) {
                    _removeCategoryHomeShot(homeShot);
                    --i;
                    continue;
                }

                /**
                 * [dj.son] 그 후 만료 일자 체크
                 */

                var startTime = 0;
                var expireTime = 0;
                try {
                    startTime = (new Date(
                        Number(homeShot.hsStartDate.substring(0, 4)),
                        Number(homeShot.hsStartDate.substring(4, 6)) - 1,
                        Number(homeShot.hsStartDate.substring(6, 8)),
                        Number(homeShot.hsStartDate.substring(8, 10)),
                        Number(homeShot.hsStartDate.substring(10, 12)),
                        Number(homeShot.hsStartDate.substring(12, 14)))).getTime();

                    expireTime = (new Date(
                        Number(homeShot.hsExpireDate.substring(0, 4)),
                        Number(homeShot.hsExpireDate.substring(4, 6)) - 1,
                        Number(homeShot.hsExpireDate.substring(6, 8)),
                        Number(homeShot.hsExpireDate.substring(8, 10)),
                        Number(homeShot.hsExpireDate.substring(10, 12)),
                        Number(homeShot.hsExpireDate.substring(12, 14)))).getTime();
                }
                catch (e) {
                    KTW.utils.Log.printErr(e);
                }

                if (expireTime < now) {
                    // 1. endDate 가 현재 시간보다 작을때 (이미 지나갔을때) -> 아예 삭제
                    _removeCategoryHomeShot(homeShot);
                    --i;
                }
                else if (startTime > now) {
                    // 2. startDate 가 현재 시간보다 클때 (아직 미래 홈샷) -> 대상 리스트에선 제외
                }
                else {
                    result.push(homeShot);
                }
            }
        }

        return result;
    }

    function onOCUpdate (source_url) {
        KTW.utils.Log.printDbg("onOCUpdate()");

        if (source_url === undefined || source_url == KTW.managers.data.OCManager.DATA.MENU.URL) {
            bGetHomeMenuHomeShot = true;
        }
    }

    function onServiceStateChange (options) {
        KTW.utils.Log.printDbg("onServiceStateChange()");

        if (options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
            var bSave = false;

            for (var key in categoryHomeShot) {
                var arrHomeShot = categoryHomeShot[key];

                categoryHomeShot[key] = checkCategoryHomeShot(categoryHomeShot[key]);

                if (arrHomeShot.length !== categoryHomeShot[key].length) {
                    bSave = true;
                }
            }

            if (bSave) {
                saveCategoryHomeShot();
            }
        }
    }

    function saveCategoryHomeShot () {
        KTW.utils.Log.printDbg("saveCategoryHomeShot()");

        try {
            KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.CATEGORY_HOMESHOT, JSON.stringify(categoryHomeShot));
        }
        catch (e) {
            KTW.utils.Log.printErr(e);
        }
    }

    /**
     * [dj.son] 홈샷 매니저 초기화
     */
    function _init () {
        KTW.utils.Log.printDbg("_init()");

        try {
            categoryHomeShot = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CATEGORY_HOMESHOT));
        }
        catch (e) {
            KTW.utils.Log.printErr(e);
        }

        if (!categoryHomeShot || categoryHomeShot === "") {
            categoryHomeShot = [];
        }

        KTW.managers.data.OCManager.addOCUpdateListener(onOCUpdate);

        KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);
    }

    /**
     * postMessage 로 온 홈샷 데이터 추가
     */
    function _addCategoryHomeShot (data) {
        KTW.utils.Log.printDbg("_addCategoryHomeShot()");

        if (!data || !data.cat_id) {
            KTW.utils.Log.printDbg("HomeShot data is not exist.... so return....");
            return;
        }

        if (!categoryHomeShot[data.cat_id]) {
            categoryHomeShot[data.cat_id] = [];
        }

        categoryHomeShot[data.cat_id].push(convertCategoryHomeShotData(data));

        saveCategoryHomeShot();
    }

    /**
     * postMessage 로 온 홈샷 데이터 삭제
     */
    function _removeCategoryHomeShot (transactionId) {
        KTW.utils.Log.printDbg("_removeCategoryHomeShot()");

        if (!transactionId) {
            KTW.utils.Log.printDbg("transactionId is not exist.... so return....");
            return;
        }

        for (var key in categoryHomeShot) {
            var arrHomeShot = categoryHomeShot[key];

            for (var i = 0; i < arrHomeShot.length; i++) {
                if (arrHomeShot[i] === transactionId) {
                    arrHomeShot.splice(i, 1);
                    return;
                }
            }
        }

        saveCategoryHomeShot();
    }

    /**
     * [dj.son] get 카테고리별 홈샷
     * - 메뉴 데이터를 같이 넘겨줄 경우, 해당 메뉴 데이터의 홈샷 데이터 검사 후 리스트에 추가
     */
    function _getCategoryHomeShot (options) {
        KTW.utils.Log.printDbg("_getCategoryHomeShot() - " + KTW.utils.Log.stringify(options));

        var result = [];

        if (options) {
            var categoryId = options.categoryId;

            if (!categoryId && options.menu) {
                categoryId = options.menu.id;
            }

            if (categoryId) {
                result = checkCategoryHomeShot(categoryHomeShot[categoryId]);
            }

            if (options.menu && options.menu.hsTargetType && options.menu.hsTargetType !== "") {
                var hsTargetCatId = null;
                var hsTargetAssetId = null;

                if (options.menu.hsTargetType === KTW.DATA.MENU.ITEM.CONTENT) {
                    hsTargetAssetId = options.menu.hsTargetId;
                }
                else {
                    hsTargetCatId = options.menu.hsTargetId;
                }

                result.push({
                    hsImgUrl: options.menu.w3HsImgUrl,
                    hsType: HOMESHOT_TYPE.MENU,
                    hsTargetType: options.menu.hsTargetType + "",
                    hsTargetCatId: hsTargetCatId,
                    hsTargetAssetId: hsTargetAssetId,
                    hsTargetLocator: options.menu.hsLocator,

                    hsDurationTime: 3,

                    reqCd: "25"
                });
            }
        }

        if (!result) {
            result = [];
        }

        return result;
    }

    /**
     * [dj.son] get 1-depth 홈샷, postMessage 로 온 데이터까지 포함
     */
    function _getHomeMenuHomeShot (callback) {
        KTW.utils.Log.printDbg("_getHomeMenuHomeShot()");

        getHomeMenuHomeShotData(function (arrHomeShot) {
            var arrHomeMenuHomeShot = _getCategoryHomeShot({categoryId: "root"});

            arrHomeMenuHomeShot = arrHomeShot.concat(arrHomeMenuHomeShot);

            if (callback) {
                callback(arrHomeMenuHomeShot);
            }
        });
    }

    /**
     * [dj.son] 카테고리 홈샷 실제 노출 횟수 업데이트
     */
    function _updateShowtCategoryHomeShot (options) {
        KTW.utils.Log.printDbg("_updateShowtCategoryHomeShot() - " + KTW.utils.Log.stringify(options));

        if (options && options.catId) {
            var arrHomeShot = categoryHomeShot[options.catId];

            if (arrHomeShot) {
                for (var i = 0; i < arrHomeShot.length; i++) {
                    if (arrHomeShot[i].transactionId === options.transactionId) {
                        arrHomeShot[i].hsRealDisplayCnt = options.hsRealDisplayCnt;
                        break;
                    }
                }
            }
        }
    }

    return {
        get TYPE () {
            return HOMESHOT_TYPE;
        },

        init: _init,

        addCategoryHomeShot: _addCategoryHomeShot,
        removeCategoryHomeShot: _removeCategoryHomeShot,

        getCategoryHomeShot: _getCategoryHomeShot,
        getHomeMenuHomeShot: _getHomeMenuHomeShot,

        updateShowtCategoryHomeShot: _updateShowtCategoryHomeShot
    };
})();