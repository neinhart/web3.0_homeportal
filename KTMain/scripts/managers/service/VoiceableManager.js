"use strict";

/**
 * manage voiceable
 */
KTW.managers.service.VoiceableManager = (function () {

    var nodeMatchListener = null;
    var patternMatchListener = null;

    function _init () {
        KTW.utils.Log.printDbg("_init()");

        nodeMatchListener = new KTW.utils.Listener();
        patternMatchListener = new KTW.utils.Listener();

        Voiceable.init({
            profile: {
                variant: 'ktpoc',  // 고정값
                appName: 'homeportal', // 고정값
                server: 'staging',  // 'production' : 상용, 'staging' : deepth00
                mode: 'sdk',  // 고정값
                lb: 'direct', // 로드밸런싱 ('lb' or 'direct')
                clientInfo: JSON.stringify({env : {SAID: KTW.SAID, STB_MODEL: KTW.STB_MODEL}})
            },
            connectInfo: {
                sessionId: KTW.SAID // 세션으로 사용할 고유값
            },
            callback: {
                nodeMatchCB: cbNodeMatch, // 화면상에 있는 요소가 매칭 되었을 때 호출되는 callback
                patternMatchCB: cbPatternMatch  // 정의한 패턴이 매칭 되었을 때 호출되는 callback
            }
        });
    }

    function cbNodeMatch (element) {
        KTW.utils.Log.printDbg("cbNodeMatch()");

        if (element) {
            nodeMatchListener.notify(element);
        }
    }

    function cbPatternMatch (patternID, pattern, entities, confidence) {
        KTW.utils.Log.printDbg("cbPatternMatch()");

        patternMatchListener.notify({
            patternID: patternID,
            pattern: pattern,
            entities: entities,
            confidence: confidence
        });
    }

    function _addNodeMatchListener (l) {
        if (l) {
            nodeMatchListener.addListener(l);
        }
    }
    function _removeNodeMatchListener (l) {
        if (l) {
            nodeMatchListener.removeListener(l);
        }
    }

    function _addPatternMatchListener (l) {
        if (l) {
            patternMatchListener.addListener(l);
        }
    }
    function _removePatternMatchListener (l) {
        if (l) {
            patternMatchListener.removeListener(l);
        }
    }
    
    return {
        init: _init,

        addNodeMatchListener: _addNodeMatchListener,
        removeNodeMatchListener: _removeNodeMatchListener,

        addPatternMatchListener: _addPatternMatchListener,
        removePatternMatchListener: _removePatternMatchListener,

        testFunc: function (element) {
            nodeMatchListener.notify(element);
        }
    };
}());
