/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>StorageManager.js</code>
 *
 * Local storage 에 대한 관리를 담당한다.
 */

"use strict";

(function () {
    
    var LOG_PREFIX = "StorageManager";
    
    var p_strg,
        t_strg,
        m_strg;

    var log = KTW.utils.Log;
    
    /**
     * TODO migration code 추가 필요 (2.0 부터 있던 comment 임)
     * storage KEY 값이 달라지는 경우 참조가 달라지기 때문에
     * 의도적으로 storege를 clear해야 하는데 이를 자동으로 수행되도록 해야한다. 
     */
    p_strg = (function () {
        
        var ALTERABLE_KEYS = [
            "timeout.iplate",
            "menu.lock_adult",
            "key_my_menu",
            "auto_power",
            "auto_standby",
            "key_cust_cug_info",
            "key_cust_env", // 2016.09.05 dhlee R6 형상 추가
            "key_otn_paring" , // 2017.10.23 dj.son OTM 패어링 여부도 추가,
            "key_ots_dcs_mode"
        ];
        
        var localStorage = null;
        //var fileAdapter = null;
        
        var listeners = [];
        
        var alterable_cache = [];
        
        function getLocalStorage() {
            if (localStorage === null) {
                localStorage = window.localStorage;
            }
            
            return localStorage;
        }
        
        function _load(key) {
            var value, index;
            
            // 변경가능한 key의 경우 cache에서 획득
            index = ALTERABLE_KEYS.indexOf(key);
            if (index !== -1) {
                value = alterable_cache[index];
                //log.printDbg("load from alterable cache. [" + key + ", " + value + "]", LOG_PREFIX);
            }
            
            // cache에 값이 없으면 storage에서 load
            if (!value) {
                value = getLocalStorage().getItem(key);
                log.printDbg("load from permanent storage. [" + key + ", " + value + "]", LOG_PREFIX);
            }
            
            // set default value
            if (value === null || value === undefined || value === "") {
                var temp = KTW.managers.StorageManager.DEFAULT_VALUE[key];
                if (temp) value = temp;
            }
            
            // storage에서 load 할 때 cache값이 없으면 set
            if (index !== -1 && !alterable_cache[index] && value) {
                alterable_cache[index] = value;
            }
            
            return value;
        }
        
        function _save(key, value) {
            log.printDbg("save permanently. [" + key + ", " + value + "]", LOG_PREFIX);
            getLocalStorage().setItem(key, value);
           
            // storage에 save 할 때 cache값도 변경
            var index = ALTERABLE_KEYS.indexOf(key);
            if (index !== -1) {
                alterable_cache[index] = value;
            }
            
            notifyStorageDataChange(key);
        }

        /**
         * 2017.03.30 dhlee local storage 초기화.
         */
        function _initializeLocalStorage() {
            var storageManager = KTW.managers.StorageManager;

            storageManager.ps.remove(storageManager.KEY.PACKAGE_LIST);
            storageManager.ps.remove(storageManager.KEY.PACKAGE_LIST_BASE);
            storageManager.ps.remove(storageManager.KEY.CUST_CUG_INFO);
            storageManager.ps.remove(storageManager.KEY.MVNO_IMG_URL);

            storageManager.ps.remove(storageManager.KEY.RECOMM);

            storageManager.ps.remove(storageManager.KEY.KIDS_RECOMM);

            storageManager.ps.remove(storageManager.KEY.RECOMM_TOP_CONTENT);

            storageManager.ps.remove(storageManager.KEY.RECOMM_FILTER);
            storageManager.ps.remove(storageManager.KEY.MENU_VERSION);

            storageManager.ps.remove(storageManager.KEY.KIDS_MENU_VERSION);
            storageManager.ps.remove(storageManager.KEY.VERSION_MENU_ICON);
            storageManager.ps.remove(storageManager.KEY.VERSION_CH_LOGO);
            storageManager.ps.remove(storageManager.KEY.VERSION_IFRAME);

            storageManager.ps.remove(storageManager.KEY.MINIGUIDE_DISPLAY_TIME);

            storageManager.ps.remove(storageManager.KEY.MENU_LOCK_ADULT);
            storageManager.ps.remove(storageManager.KEY.FONT_SIZE);

            storageManager.ps.remove(storageManager.KEY.AUTO_POWER);

            storageManager.ps.remove(storageManager.KEY.AUTO_STANDBY);
            //storageManager.ps.remove(storageManager.KEY.OTN_PARING);

            storageManager.ps.remove(storageManager.KEY.CAPTION_LANG);

            storageManager.ps.remove(storageManager.KEY.AUDIO_LANG);
            storageManager.ps.remove(storageManager.KEY.BLUETOOTH_DEVICE);

            storageManager.ps.remove(storageManager.KEY.PARENTAL_RATING);

            storageManager.ps.remove(storageManager.KEY.SW_ROOT_FOR_LOCAL);
            storageManager.ps.remove(storageManager.KEY.SW_VERSION_LOCAL);
            storageManager.ps.remove(storageManager.KEY.SW_MODE_LOCAL);
            storageManager.ps.remove(storageManager.KEY.SW_UPDATE_INFO);

            storageManager.ps.remove(storageManager.KEY.SW_MODE_CHANGE_RETRY_FLAG);
            storageManager.ps.remove(storageManager.KEY.SW_MODE_CHANGE_FLAG);
            storageManager.ps.remove(storageManager.KEY.SW_TEST_MODE);
            storageManager.ps.remove(storageManager.KEY.SW_DEBUG_RETRY);

            storageManager.ps.remove(storageManager.KEY.RECOMMEND_ADULT_LIMIT);
            storageManager.ps.remove(storageManager.KEY.PF_DATA);
            storageManager.ps.remove(storageManager.KEY.SAVE_RATING_CHECK);
            storageManager.ps.remove(storageManager.KEY.RECOMM_UPDATE_CHECK);
            storageManager.ps.remove(storageManager.KEY.PF_TIME);
            storageManager.ps.remove(storageManager.KEY.STBPOINT);

            storageManager.ps.remove(storageManager.KEY.HOMEMENU_LIST_MODE);
            storageManager.ps.remove(storageManager.KEY.MENU_LOCK_ADULT_MIGRATION);
            storageManager.ps.remove(storageManager.KEY.SEARCH_GETTREND_DATA);
            storageManager.ps.remove(storageManager.KEY.SEARCH_GETTREND_CALL_TIME);
            storageManager.ps.remove(storageManager.KEY.SERIES_AUTO_PLAY);

            storageManager.ps.remove(storageManager.KEY.STBPOINT);
            storageManager.ps.remove(storageManager.KEY.RECENTLY_VOD);
            storageManager.ps.remove(storageManager.KEY.VOD_LIST_MODE);
            storageManager.ps.remove(storageManager.KEY.OFFICE_CODE);
            storageManager.ps.remove(storageManager.KEY.OFFICE_CODE_CALL_TIME);
            storageManager.ps.remove(storageManager.KEY.INBOX_LAST_READ_TIME);

            storageManager.ps.remove(storageManager.KEY.KIDS_MODE_RESTRICTION);
            storageManager.ps.remove(storageManager.KEY.STB_POINT_CACHE);

            storageManager.ps.remove(storageManager.KEY.USER_LOG_COLLECTOR);

            storageManager.ps.remove(storageManager.KEY.MODULE_VERSION_DATA);

            storageManager.ps.remove(storageManager.KEY.INTROUI_POPUP_DATA);

            storageManager.ps.remove(storageManager.KEY.CATEGORY_HOMESHOT);

            storageManager.ps.remove(storageManager.KEY.OTS_CHANNEL_IFRAME_VERSION);

            /**
             * [dj.son] key 변경으로 인해, 2017-07-05 까지 사용하던 모듈 버전 데이터 삭제
             */
            storageManager.ps.remove("module_data");
        }
        
        function _addStorageDataChangeListener(listener) {
            listeners.push(listener);
        }
        
        function _removeStorageDataChangeListener(listener) {
            var index = listeners.indexOf(listener);
            if (index !== -1) {
                listeners.splice(index, 1);
            }
        }
        
        function notifyStorageDataChange(key) {
            // 변경을 알리는 항목에 대해서만 처리한다.
            var index = ALTERABLE_KEYS.indexOf(key);
            log.printDbg("notifyStorageDataChange index :  [" + index + "]", LOG_PREFIX);
            if (index !== -1) {
                var length = listeners.length;
                log.printDbg("notifyStorageDataChange length :  [" + length + "]", LOG_PREFIX);
                for (var i = 0; i < length; i++) {
                    listeners[i](key);
                }
            }
        }
        
        return {
            save: _save,
            load: _load,
            initializeLocalStorage: _initializeLocalStorage,    // 2017.03.30 dhlee 추가
            remove: function (k) {
                log.printDbg("remove from permanent storage. ["+k+"]", LOG_PREFIX);
                getLocalStorage().removeItem(k);
                // [jh.lee] remove 하는 경우 해당 key 에 대한 cache 값도 삭제해야함
                var index = ALTERABLE_KEYS.indexOf(k);
                if (index !== -1) {
                    alterable_cache[index] = null;
                }
                
                notifyStorageDataChange(k);
            },
            key: function (k) {
                var v = getLocalStorage().key(k);
                //log.printDbg("key from permanent storage. ["+k+"]", LOG_PREFIX);
                return v;
            },
            clear: function () {
                log.printDbg("clear from permanent storage.", LOG_PREFIX);
                getLocalStorage().clear();
            },
            addStorageDataChangeListener: _addStorageDataChangeListener,
            removeStorageDataChangeListener: _removeStorageDataChangeListener
        };
    }());

    t_strg = (function () {
        
        var sessionStorage = null;
        
        function getSessionStorage() {
            if (sessionStorage === null) {
                sessionStorage = KTW.oipf.AdapterHandler.basicAdapter.sessionStorage;
            }
            
            return sessionStorage;
        }
        
        return {
            save: function (k, v) {
                log.printDbg("save temporarily. ["+k+", "+v+"]", LOG_PREFIX);
                getSessionStorage().setItem(k, v);
            },
            load: function (k) {
                var v = getSessionStorage().getItem(k);
                log.printDbg("load from temporary storage. ["+k+", "+v+"]", LOG_PREFIX);
                return v;
            }
        };
    }());

    m_strg = (function () {

        var moduleCacheDataStorage = new KTW.utils.HashMap();

        return {
            save: function (k, v) {
                log.printDbg("save module cache data. ["+k+", "+v+"]", LOG_PREFIX);

                try {
                    var value = JSON.stringify(v);
                }
                catch (e) {}

                moduleCacheDataStorage.put(k, value);
            },
            load: function (k) {
                var v = moduleCacheDataStorage.get(k);

                log.printDbg("load module cache data. ["+k+", "+v+"]", LOG_PREFIX);

                try {
                    var value = JSON.parse(v);
                }
                catch (e) {}

                return value;
            },
            clear: function (k) {
                log.printDbg("clear module cache data. ["+k+"]", LOG_PREFIX);

                var v = moduleCacheDataStorage.get(k);

                if (v !== null && v !== undefined) {
                    moduleCacheDataStorage.remove(k);
                }
                else {
                    moduleCacheDataStorage.clear();
                }
            }
        };
    }());

    KTW.managers.StorageManager = {};
    // ps(permanent storage) == localStorage
    // ts(temporary storage) == sessionStorage
    Object.defineProperties(KTW.managers.StorageManager, {
        ps: { value: p_strg },
        ts: { value: t_strg },
        ms: { value: m_strg }
    });

    Object.defineProperty(KTW.managers.StorageManager, "MEM_KEY", {
        value: {
            PKG_LIST: "mem.key.pkg.list",
            PKG_BASE_LIST: "mem.key.pkg.base.list",
            PKG_LIST_ALL: "mem.key.pkg.list.all"
        }
    });

    // define constant.
    Object.defineProperty(KTW.managers.StorageManager, "KEY", {
        value: {
            CUST_ENV: "key_cust_env",
            PACKAGE_LIST: "key_pkg_list",   // 2017.06.26 dhlee memory cache로 변경. 시스템 초기화를 위해서만 남겨둠
            PACKAGE_LIST_BASE: "key_pkg_list_base", // 2017.06.26 dhlee memory cache로 변경. 시스템 초기화를 위해서만 남겨둠
            CUST_CUG_INFO: "key_cust_cug_info",

            MVNO_IMG_URL : "key_mvno_img_url",

            /**
             * 쇼윈도우 맞춤메뉴 키
             * 2017.01.11 dhlee 3.0 에서는 사용하지 않음
             */
            //SW_FAV_MENU: "key_showwindow_fav",
            /**
             * 추천 맞춤컨텐츠 키
             */
            RECOMM: "key_recomm",
            /**
             * [jh.lee] 어린이 추천 (쇼윈도우에서만 사용)
             * 2017.01.11 dhlee 3.0 에서는 사용하지 않지만 초기화는 해야 함
             */
            KIDS_RECOMM: "key_kids_recomm",
            /**
             * [jh.lee] getTopContent 정보 저장 (쇼윈도우에서만 사용)
             * 2017.01.11 dhlee 3.0 에서는 사용하지 않지만 초기화는 해야 함
             */
            RECOMM_TOP_CONTENT : "key_recomm_top_content",
            /**
             * 추천 맞춤컨텐츠 추천안받기 여부
             */
            RECOMM_FILTER: "key_recomm_filter",
            MENU_VERSION: "key_menu_ver",
            /**
             * [jh.lee] 어린이 추천 데이터 버전
             */
            KIDS_MENU_VERSION: "key_kids_menu_ver",
            VERSION_MENU_ICON: "key_ver_menu_icon",
            VERSION_CH_LOGO: "key_ver_ch_logo",
            VERSION_IFRAME: "key_ver_iframe",
            /**
             * 맞춤메뉴 설정값
             */
            MY_MENU: "key_my_menu", // 2017.01.17 dhlee 사용하지 않음(해당 기능 제공하지 않음) 초기화를 위해 남겨둠
            /**
             * 나만의 메뉴 구분  ("basic" or "fav")
             */
            MY_MENU_TYPE: "key_mymenu_type",    // 2017.01.17 dhlee 사용하지 않음(해당 기능 제공하지 않음) 초기화를 위해 남겨둠
            /**
             * 2016.11.09 dhlee 미니가이드(miniepg) 노출 시간
             * (2.0 CHANNEL_GUIDE_TIME -> MINIGUIDE_DISPLAY_TIME 으로 변경)
             */
            MINIGUIDE_DISPLAY_TIME: "timeout.iplate",
            /**
             * 성인메뉴 잠금 "true" 이면 잠금, "false"이면 해제
             */ 
            MENU_LOCK_ADULT: "menu.lock_adult", // 2017.01.17 dhlee 사용하지 않기로 결정됨. 초기화 시 해당 값 초기화
            FONT_SIZE: "font.size", // 2016.10.13 dhlee 사용하지 않는 key 이나 시스템초기화 시에는 해당 값을 초기화 하고 있음
            /**
             * 자동전원,  repeat;starttime;endtime;실행여부(반복안하는경우)
             * ex. true;1200;2300 / false;0000;0000
             */
            AUTO_POWER: "auto_power",
            /**
             * 자동대기모드 (true / false)
             */
            AUTO_STANDBY: "auto_standby",
            OTN_PARING: "key_otn_paring",
            /**
             * PG서버 쿠폰 개수
             */
            //COUPON_NUM: "key_coupon_num", // 2016.10.13 dhlee 사용하지 않는 key 임.
            //BOOT_CH: "key_boot_ch",       // 2016.11.07 dhlee 사용하지 않는 key 임
            CAPTION_LANG: "key_cap_lng",    // 2016.10.13 dhlee 사용하지 않는 key 이나 시스템초기화 시에는 해당 값을 초기화 하고 있음
            AUDIO_LANG: "key_audio_lng",    // 2016.10.13 dhlee 사용하지 않는 key 이나 시스템초기화 시에는 해당 값을 초기화 하고 있음
            //AUDIO_LANG_B: "key_audio_lng_b",  // 2016.10.13 dhlee 사용하지 않는 key 임.
            BLUETOOTH_DEVICE: "key_bluetooth_device",
            
            STORAGE_MAGIC_NUMBER: "StorageMagicNumber",   // 2016.10.13 dhlee 사용하지 않는 key 임.
            
            /**
             * [jh.lee] PC 테스트용 KEY
             */
            PARENTAL_RATING : "ParentalRating",

            /**
             * Show Window POC 고도화. 
             * 관련 properties.
             * 2016.10.13 dhlee 쇼윈도우 POC 고도화 아래 항목들은 3.0 에서는 필요 없는 key 들이다.
             * 다만, 하위 호환성 등을 이유로 시스템 초기화 시 remove 에는 포함되어야 하는 키들이다.
             */
            SW_ROOT_FOR_LOCAL : "sw_root_for_local",
            // saved local version 
            SW_VERSION_LOCAL: "sw_version_local",
            SW_MODE_LOCAL: "sw_mode_local",
            // 해당 정보로 update 시작 됨.
            SW_UPDATE_INFO : "sw_update_info",
            // 모드 전환 실패시 세팅될 값.
            // 실패 했을경우 재부팅 혹은 대기 에서 올라올 때 마다
            // 다시 체크 해야 함.
            SW_MODE_CHANGE_RETRY_FLAG : "sw_mode_change_retry_flag",
            // showwindow 모드 전환이 rebooting 으로 바뀌면서 reboot 후 모드 전환중임을 알리기 위해
            SW_MODE_CHANGE_FLAG : "sw_mode_change_flag",
            // showwindow loading fail test key.
            SW_TEST_MODE: "sw_test_mode",
            // Debuging 목적.
            SW_DEBUG_RETRY: "sw_debug_retry",
            
            /**
             * [jh.lee] 고도화로 추가된 추천 더보기에서 19세 등급 제한
             */
            RECOMMEND_ADULT_LIMIT : "recommend_adult_limit",
            /**
             * [jh.lee] getMyPreference 호출로 받은 데이터
             */
            PF_DATA : "pf_data",
            /**
             * [jh.lee] saveRating 호출 체크 true or false
             */
            SAVE_RATING_CHECK : "save_rating_check",
            /**
             * 부팅 및 oc update 시 메뉴버전이 변경되서 실제로 추천 서버에 호출이 일어났는지 체크
             * oc update 의 경우 랜덤타임인데 실제로 호출 됬을 때 체크하는 것을 의미함
             */
            RECOMM_UPDATE_CHECK : "recomm_update_check",
            /**
             * [jh.lee] saveRating 수행 후 진입 시 getMyPreference 호출 시간
             * 2017.01.11 dhlee 사용되는 곳 없음
             */
            PF_TIME : "pf_time",
            /**
             * movieChoice PPV 구매목록 저장
             */
            PPV_PURCHASE_INFO : "skyChoiceInfo",

            HOMEMENU_LIST_MODE : "homeMenuListMode",    // 2016.11.16 dhlee 더이상 사용하지 않는 key. 초기화시에 remove
            /**
             * 마이 플레이 리스트 데이터 (2016.09.05 dhlee R6 형상 추가)
             */
            //MY_PLAY_LIST : "myPlayList",  // 2016.11.08 dhlee 실제 사용하는 곳이 없음
            // 2016.10.14 dhlee 웹 3.0 에서는 사용하지 않는 key 임. 단 remove 시에는 추가.
            MENU_LOCK_ADULT_MIGRATION : "menu lock adult migration",
            /**
             * 2016.11.07 dhlee R6 형상 추가 (2개 키)
             * 2016.12.27 dhlee memory cache를 사용하고 시스템 초기화를 위해서만 남겨둔다.
             */
            SEARCH_GETTREND_DATA : "search_gettrend_data",
            SEARCH_GETTREND_CALL_TIME : "search_gettrend_call_time",
            /**
             * 2016.11.07 dhlee 시리즈 몰아 보기(R7 형상 추가)
             */
            SERIES_AUTO_PLAY : "series_auto_play" ,
            /**
             * 2016.11.07 dhlee R7 형상 추가 통합포인트
             */
            STBPOINT : "stb_point",
            /**
             * 2016.11.07 dhlee 최근 확인한 VOD (3.0 형상 추가)
             */
            RECENTLY_VOD: "key.recently.vod",
            /**
             * 2016.11.09 dhlee VOD 보기 옵션 (3.0 형상 추가)
             * HOMEMENU_LIST_MODE 를 승계하지 않기로 함
             * "default" / "poster" / "text"
             */
            VOD_LIST_MODE: "key.vod.list.mode",

            /** 2016.11.30 dhlee 사용자 최근 검색어 목록 (3.0 형상 추가) 메모리 cache를 사용하도록 함 */
            //RECENT_QUERY_LIST: "key.recent.query.list",

            /** 2017.01.20 dhlee getMyGlbIpNhp API 응답 데이터 저장 */
            OFFICE_CODE: "key.office.code",

            /** 2017.01.20 dhlee getMyGlbIpNhp API 요청 시간 정보 저장 */
            OFFICE_CODE_CALL_TIME: "key.office.code.call.time",

            /** 2017.02.20 dhlee 우리집맞춤TV>알림박스의 최근 메시지 유무를 판단하기 위하여 사용자가 마지막으로 확인한 메시지의 발송날짜를 저장함 */
            INBOX_LAST_READ_TIME: "key.inbox.last.read.time",

            /** 2017.03.09 dhlee 키즈 > 부모세상에서 설정하는 시청제한 관련 정보 저장 */
            KIDS_MODE_RESTRICTION: "key.kids.mode.restriction",

            /** 2017.06.08 dhlee 일반 모드 <-> 키즈 모드 전환 시 사용할 임시 local storage key */
            NORMAL_PARENTAL_RATING: "key.normal.parental.rating",
            NORMAL_STANDBY_MODE: "key.normal.standby.mode",
            NORMAL_VOD_LIST_MODE: "key.normal.vod.list.mode",
            NORMAL_ALERT_MESSAGE: "key.normal.alert.message",

            /** 2017.04.03 dhlee TV포인트, 쿠폰 등의 캐시 정보 (2.0과 형상이 달라 별도로 정의) 시스템 초기화 대상 */
            STB_POINT_CACHE: "key.stb.point.cache",

            USER_LOG_COLLECTOR : "key.user.log.collector",

            /** 2017.07.05 dj.son 신규 모듈 버전 데이터 키 */
            MODULE_VERSION_DATA : "key.module.version.data",

            /**공지사항 POPUP DATA */
            INTROUI_POPUP_DATA: "key.introui.popup.data",

            /**
             * [dj.son] 스마트 푸시로 오는 카테고리 홈샷 데이터 키
             */
            CATEGORY_HOMESHOT: "key.category_homeshot",

            /**
             * [dj.son] OTS channel iframe version
             */
            OTS_CHANNEL_IFRAME_VERSION: "key.ots_channel_iframe_version",
            OTS_DCS_MODE : "key_ots_dcs_mode"
        }
   });
    
    Object.defineProperty(KTW.managers.StorageManager, "DEFAULT_VALUE", {
        value: {
            "timeout.iplate": "3000",
            "menu.lock_adult": "false",
            "auto_standby" : "true"
        }
    });
}());
