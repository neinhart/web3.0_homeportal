/**
 *  Copyright (c) 2018 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>KOLManager</code>
 * OTM 통합 페어링 서버 연동 관리 매니져
 * @author sw.nam
 * @since 2018-03-05
 */

"use strict";

KTW.managers.otmPairing.KOLManager = (function() {

    var api;
    var data;
    var log = KTW.utils.Log;

    function getUrl() {
        // 테스트 베드 에서는 테스트 서버로 요청
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {

            return KTW.DATA.HTTP.KOL.LIVE_URL;
        } else {
            return KTW.DATA.HTTP.KOL.TEST_URL;
        }
    }

    /**
     * 홈포탈에서 인증을 받기위한 SMS발송을 OMAS에 요청한다. (OMAS-CO0107)
     * @param callback
     * @param phone_corp 통신사구분
     * @param phone_no 사용자전화번호
     * @param service_key 서비스키 (72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489)
     * @param pairing_hist_id 페어링 히스토리 ID
     */
    function _smsSend(callback, phone_corp, phone_no, pairing_hist_id) {
        api = "/otvstb/api/v3/stb_sms_send";
        data = {
            "said": KTW.SAID,
            "phone_corp": phone_corp,
            "phone_no": phone_no,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489",
            // 페어링 히스토리 ID가 있다면 Y(히스토리 페어링), 없다면 N(일반페어링) 입력
            "hist_pair_yn": pairing_hist_id != null && pairing_hist_id.length > 0 ? "Y" : "N",
            "pairing_hist_id": pairing_hist_id
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 인증을 받기위한 SMS발송을 확인을 OMAS에 요청한다. (OMAS-CO0108)
     * @param callback
     * @param phone_corp 통신사구분
     * @param phone_no 사용자전화번호
     * @param service_key 서비스키
     * @param auth_no_key OAMS인증번호
     * @param sms_num SMS인증번호
     * @private
     */
    function _smsVerify(callback, phone_corp, phone_no, auth_no_key, sms_num) {
        api = "/otvstb/api/v3/stb_sms_verify";
        data = {
            "said": KTW.SAID,
            "phone_corp": phone_corp,
            "phone_no": phone_no,
            "auth_no_key": auth_no_key,
            "sms_num": sms_num,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 페어링을 위한 약관 목록을 조회한다. (OMAS-CO0303, OMAS-CO0304)
     * @param callback
     * @param suid
     * @param phone_corp 통신사구분
     * @param phone_no 사용자전화번호
     * @param service_key 서비스키
     * @param tokn_val 액세스토큰
     * @param mand_agree_yn OTM필수약관동의여부
     * @private
     */
    function _getAgreementList(callback, suid, phone_corp, phone_no, tokn_val, mand_agree_yn) {
        api = "/otvstb/api/v3/stb_get_agreement_list";
        data = {
            "suid": suid,
            "said": KTW.SAID,
            "phone_corp": phone_corp,
            "phone_no": phone_no,
            "tokn_val": tokn_val,
            "mand_agree_yn": mand_agree_yn,
            "client_type": "STB",
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 페어링 요청을 한다(약관동의요청). (OMAS-CO0305)
     * @param callback
     * @param suid
     * @param phone_corp 통신사구분
     * @param phone_no 사용자전화번호
     * @param service_key 서비스키
     * @param tokn_val 액세스토큰
     * @param prcs_main_tokn_val 프로세스토큰
     * @param prcs_sub_tokn_val 서브프로세스토큰
     * @param otv_agree_list OTV약관동의목록(split_no, sel_agree_yn)
     * @param otm_agree_list OTM약관동의목록(split_no, sel_agree_yn)
     * @private
     */
    function _makePairing(callback, suid, phone_corp, phone_no, tokn_val, prcs_main_tokn_val, prcs_sub_tokn_val, otv_agree_list, otm_agree_list) {
        api = "/otvstb/api/v3/stb_make_pairing";
        data = {
            "suid": suid,
            "said": KTW.SAID,
            "phone_corp": phone_corp,
            "phone_no": phone_no,
            "tokn_val": tokn_val,
            "prcs_main_tokn_val": prcs_main_tokn_val,
            "prcs_sub_tokn_val": prcs_sub_tokn_val,
            "otv_agree_list": otv_agree_list,
            "otm_agree_list": otm_agree_list,
            "client_type": "STB",
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489",
            "pair_type" : "tv"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 약관 상세를 요청 한다. (OMAS-CO0202)
     * @param callback
     * @param tokn_val 액세스토큰
     * @param split_no 약관번호
     * @param kind 약관구분
     * @param service_key 서비스키 (1, 2)
     * @param data_type 본문타입 (HTML-1, TEXT-2)
     * @private
     */
    function _getAgreementDetail(callback, tokn_val, stplt_no, kind, data_type) {
        api = "/otvstb/api/v3/stb_get_agreement_detail";
        data = {
            "tokn_val": tokn_val,
            "stplt_no": stplt_no,
            "kind": kind,
            "data_type": data_type,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 페어링 해지를 요청 한다.
     * @param callback
     * @param said 페어링 변경을 위해 기존 페어링을 삭제하는 경우 현재 STB의 SAID / 그 외의 경우 삭제 대상 SAID
     * @param pairing_id 페어링ID
     * @param service_key 서비스키
     * @param chg_pair_yn 페어링변경판단
     * @param del_type 1:계정삭제(완전삭제), 2:연결끊기(페어링만 끊기고 히스토리 내역에는 남아있음)
     * @private
     */
    function _removePairing(callback, said, pairing_id, chg_pair_yn, del_type) {
        api = "/otvstb/api/v3/stb_remove_pairing";
        data = {
            "suid": "",
            "said": KTW.SAID,
            "pairing_id": pairing_id,
            "chg_pair_yn": chg_pair_yn,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489",
            "del_type": del_type
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 페어링 정보를 조회한다.
     * @param callback
     * @param service_key 서비스키
     * @private
     */
    function _getPairingList(callback) {
        api = "/otvstb/api/v3/stb_get_pairing_list";
        data = {
            "said": KTW.SAID,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 페어링 히스토리 목록을 조회한다.
     * @param callback
     * @param service_key 서비스키
     * @private
     */

    function _getPairingHistoryList(callback) {
        api = "/otvstb/api/v3/stb_get_pairing_history_list";
        data = {
            "said": KTW.SAID,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 히스토리 재페어링을 요청한다.
     * @param callback
     * @param pairing_hist_id 페어링 히스토리 ID
     * @param service_key 서비스키
     * @private
     */
    function _makePairingHistory(callback, suid, pairing_hist_id) {
        api = "/otvstb/api/v3/stb_make_pairing_history";
        data = {
            "said": KTW.SAID,
            "suid": suid,
            "pairing_hist_id": pairing_hist_id,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489",
            "pair_type" : "tv"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 히스토리 목록 삭제를 요청한다.
     * @param callback
     * @param pairing_hist_id 페어링 히스토리 ID
     * @param service_key 서비스키
     * @private
     */
    function _removePairingHistory(callback, suid, pairing_hist_id) {
        api = "/otvstb/api/v3/stb_remove_pairing_history";
        data = {
            "said": KTW.SAID,
            "suid": suid,
            "pairing_hist_id": pairing_hist_id,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    /**
     * 홈포탈에서 페어링 마스터 변경을 요청한다.
     * @param callback
     * @param pairing_id 페어링 ID
     * @param service_key 서비스키
     * @private
     */
    function _setPairingMaster(callback, pairing_id, suid) {
        api = "/otvstb/api/v3/stb_set_pairing_master";
        data = {
            "said": KTW.SAID,
            "suid": suid,
            "pairing_id": pairing_id,
            "service_key": "72BFE8F7A830F8354C7BEF537F93E54095F3B0468DE2C585E82C5F75EF327489"
        };
        return _request(callback, getUrl() + api, data);
    }

    function _request(callback, url, postData, async, parameters) {
        try {
            var KOLAgent = "KOL(copamtible;ServiceType/KOL;DeviceType/STB-WEB;";
            KOLAgent += ("DeviceModel/" + KTW.STB_MODEL + ";");
            KOLAgent += "OSType/Linux;OSVersion/0.0.0;AppName/STB;";
            KOLAgent += ("AppVersion/" + KTW.UI_VERSION + ")");
            log.printDbg("[HTTP] createAjax [KOLManager] ###########");
            log.printDbg("[HTTP] url : " + url);
            log.printDbg("[HTTP] sendData : " + JSON.stringify(postData));
            return $.ajax({
                url: url,
                type: "post",
                dataType: 'json',
                async: async == undefined ? true : async,
                timeout: 5e3,
                data: JSON.stringify(postData),
                contentType: "application/json;charset=UTF-8",
                crossOrigin: false,
                headers: {"KOL-Agent": KOLAgent},
                success: function(data) {
                    log.printDbg("[HTTP] ajax success [KOLManager] ###########");
                    log.printLongDbg("[HTTP] result = " + JSON.stringify(data));
                    callback(true, data, parameters);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    log.printErr("[HTTP] Error return [KOLManager] ###########");
                    log.printErr("[HTTP] request : ", JSON.stringify(XMLHttpRequest));
                    log.printErr("[HTTP] status : ", JSON.stringify(textStatus));
                    log.printErr("[HTTP] error", JSON.stringify(errorThrown));
                    callback(false, XMLHttpRequest, parameters);
                }
            });
        } catch (exception) {
            log.printErr("[HTTP] catch exception [KOLManager] ###########");
            log.printErr("[HTTP] exception", JSON.stringify(exception));

            callback(false, exception, parameters);

            return null;
        }
    }

    return {
        smsSend: _smsSend,
        smsVerify: _smsVerify,
        getAgreementList: _getAgreementList,
        getAgreementDetail: _getAgreementDetail,
        makePairing: _makePairing,
        removePairing: _removePairing,
        getPairingList: _getPairingList,
        // 2018.01.05 Lazuli. 1:N 페어링 위해 신규로 추가된 API
        getPairingHistoryList: _getPairingHistoryList,
        makePairingHistory: _makePairingHistory,
        removePairingHistory: _removePairingHistory,
        setPairingMaster: _setPairingMaster
    };


})();