/**
 *  Copyright (c) 2018 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>OTMPManager.js</code>
 * Olleh tv 모바일 1:N 통합 페어링 매니져
 * @author sw.nam
 * @since 2018-03-05
 */

KTW.managers.otmPairing.OTMPManager = (function() {

    var storageManager = KTW.managers.StorageManager;
    var log = KTW.utils.Log;

    var PAIRING_ERROR_CODE = ["F0000", "F0001", "F0204", "F0400", "F0599", "F0101", "F0102",
        "F0103", "F0104", "F0105", "F0106", "F0107", "F0108", "F0109", "F0110", "F0111", "F0112", "F0113", "S0201",
        "S0202", "F0201", "F0202", "F0203", "F0204", "F0205", "F0206", "F0207", "F0208", "F0209", "F0210", "F0211",
        "F0212", "F0213", "F0214"];

    var ERR_SMS_AUTH_FAIL = "F0101";
    var ERR_INVALID_PPHONE_NUMBER = "F0111";

    var OAMS_AUTH_KEY = "";
    var PHONE_NUMBER = "";
    var PHONE_CORP = "";
    var SU_ID, PAIRING_LIST = null, PAIRING_HIST_LIST = [], PAIRING_ID, TOKN_VAL, MAND_AGREE_YN;
    var PAIRING_HIST_ID, PAIRING_HIST_SUID;
    var PRCS_MAIN_TOKN_VAL = null, PRCS_SUB_TOKN_VAL = null;

    var pairingStateListener = [];

    var ERROR_TEXT = {
        ET_REBOOT : ["서비스가 일시적으로 원활하지 않습니다", "셋톱 재부팅 시 해결 될 수 있습니다", "재부팅 버튼을 선택하면 자동 재부팅 됩니다"],
        ET_CALL : ["문의) 국번 없이 100번"],
        ET000 :  ["서비스가 일시적으로 원활하지 않습니다", "잠시 후 이용해 주세요"],
        ET001 :  ["네트워크가 일시적으로 원활하지 않습니다", "잠시 후 다시 시도해 주세요"],
        ET002 :  ["서비스가 일시적으로 원활하지 않습니다", "네트워크 상태를 확인해주세요", "동일현상 반복 시 고객센터로 문의하여주시기 바랍니다"],
        ET003 :  ["선택하신 VOD를 재생할 수 없습니다", "이용에 불편을 드려 죄송합니다"],
        ET004 :  ["해당 콘텐츠의 정보가 없습니다", "편성에서 변경/삭제된 콘텐츠입니다"],
        ET0013 : ["서비스가 일시적으로 원활하지 않습니다", "셋톱 재부팅 시 해결 될 수 있습니다", "재부팅 버튼을 선택하면 자동 재부팅 됩니다", "", "(VODE-00013)"],
        ET0018 : ["서비스가 일시적으로 원활하지 않습니다", "셋톱 재부팅 시 해결 될 수 있습니다", "재부팅 버튼을 선택하면 자동 재부팅 됩니다", "", "(VODE-00018)"]
    };

    var isUpdatedList = false;
    var manualUpdateFlag = false;



    function _init() {
        log.printDbg("_init()");
        //부팅 시 pairing 푸쉬 매시지에 대한 listener 를 등록한다.
        storageManager.ps.addStorageDataChangeListener(onStorageDataChanged);
    }


    function onStorageDataChanged(key) {
        log.printDbg("onStorageDataChanged() "+key);
        if (key === storageManager.KEY.OTN_PARING) {
            isUpdatedList = false;
            _getAllPairingInfo();
        }
    }

    /**
     * 푸쉬메시지에 의해 페어링 정보를 읽어오는 경우 실행되는 함수
     * @private
     */
    function _getAllPairingInfo() {
        log.printDbg("_getAllPairingInfo()");

        _getPairingInfo(_cbGetPairingInfo,false);
        _getPairingHistory(_cbGetPairingHistory);

    }

    function _cbGetPairingInfo(result, list) {
        log.printDbg("_cbGetPairingInfo() = "+result+" "+JSON.stringify(list));
        PAIRING_LIST = list;

        //[sw.nam] 홈메뉴 좌측 아이콘 업데이트 시 사용되는 listener 알림
        notifyPairingStateChanged({listener: "otmIconChangeListener"});
    }

    function _cbGetPairingHistory(result, list) {
        log.printDbg("_cbGetPairingHistory() = "+result+" "+JSON.stringify(list));
        PAIRING_HIST_LIST = list;

        if(!isUpdatedList) {
            // 2018.03.12 sw.nam
            // 푸쉬는 listener 를 등록해놨으므로 매번 오지만
            // 관리 화면 내부에서 loading 이 돌고 있지 않은 경우 화면은 갱신하지 않는다
            if(KTW.ui.LayerManager.isShowLoading() == true || manualUpdateFlag) {
                notifyPairingStateChanged({listener: "pairingStateChangedListener"});
                KTW.ui.LayerManager.stopLoading();
            }
            isUpdatedList = true;
            manualUpdateFlag = false;

        }
    }

    // 통합 페어링 관련 팝업 모두 닫음
    function _closeAllOTMPPopup() {
        var closePopupList = ["OTMPConnecting", "OTMPConnectionInfo", "OTMPMobileAuth", "OTMPLicenseList"];
        for (var i = 0; i < closePopupList.length; i++) {
            if (KTW.ui.LayerManager.getLayer(closePopupList[i])) KTW.ui.LayerManager.deactivateLayer({
                id: closePopupList[i],
                remove: false
            });
        }
    }

    // 기본형 알림 팝업 노출
    function _openBasicPopup(_title, _message, _buttons, _subMessage) {
        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "OTMPBasicPopup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: _title,
                    message: _message,
                    subMessage: _subMessage,
                    buttons: _buttons
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    }

    function _openErrorPopup(params) {
        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "MyHomeError",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                linkage: true,
                params: {
                    title: params.title||"오류",
                    message: params.message,
                    button: params.button||"닫기",
                    reboot: params.reboot,
                    showSAID: params.showSAID,
                    callback: params.callback
                }
            },
            moduleId: "module.family_home",
            visible: true
        });
    }

    // 페어링 에러로직
    function _showPairingErrorPopup(_code, _message, _callback) {
        var findErrorCode = false;
        for (var i = 0; i < PAIRING_ERROR_CODE.length; i++) {
            if (PAIRING_ERROR_CODE[i] == _code) {
                log.printDbg("_showPairingErrorPopup : findErrorCode ! - " + _code);
                findErrorCode = true;
                break;
            }
        }
        if (findErrorCode) {
            if (ERR_SMS_AUTH_FAIL == _code) {
                // SMS 인증번호 오류 : 확인 및 취소 입력 시 인증번호 칸으로 포커스 이동해 줌
                _openBasicPopup("알림", "입력하신 SMS 인증번호가<br>유효하지 않습니다. 다시 입력해주세요",
                    [{
                        name: "확인", callback: function() {
                            _callback(false, "SMS");
                            KTW.ui.LayerManager.deactivateLayer({id: "OTMPBasicPopup", onlyTarget: true});
                        }
                    }, {
                        name: "취소", callback: function() {
                            _callback(false, "SMS");
                            KTW.ui.LayerManager.deactivateLayer({id: "OTMPBasicPopup", onlyTarget: true});
                        }
                    }]);
            }
            else if (ERR_INVALID_PPHONE_NUMBER == _code) {
                // 전화번호 오류 : 확인 및 취소 입력 시 전화번호 칸으로 포커스 이동해 줌
                _openBasicPopup("알림", "입력하신 전화번호가 유효하지 않습니다<br>다시 입력해주세요",
                    [{
                        name: "확인", callback: function() {
                            _callback(false, "PHONE");
                            KTW.ui.LayerManager.deactivateLayer({id: "OTMPBasicPopup", onlyTarget: true});
                        }
                    }, {
                        name: "취소", callback: function() {
                            _callback(false, "PHONE");
                            KTW.ui.LayerManager.deactivateLayer({id: "OTMPBasicPopup", onlyTarget: true});
                        }
                    }]);
            }
            else if (_message != null && _message.length > 0) {
                // 메세지 내용이 있을 경우
                _closeAllOTMPPopup();
                _openBasicPopup("오류", _message, [{name: "닫기", callback: "close"}]);
            }
            else {
                // 메세지 내용이 없을 경우
                _closeAllOTMPPopup();
                if (_code.indexOf("F02") > -1 || _code == "S0201" || _code == "S0202")
                    _openBasicPopup("연결 오류", "올레 tv 모바일과 올레 tv 연결을<br>실패하였습니다<br>잠시 후 다시 시도해주세요", [{name: "확인", callback: "close"}]);
                else _openErrorPopup({message: ERROR_TEXT.ET002});
            }
        } else _openErrorPopup({message: ERROR_TEXT.ET002});
    }

    // 인증번호 요청
    // 히스토리 재연결 중 인증번호 요청인 경우에는 페어링 히스토리 ID 값과 페어링 SUID 값을 넘겨줘야 함
    function _requestAuthNumber(_callback, _corp, _totalPhoneNumber, _pairingHistSuid, _pairingHistId) {
        PHONE_CORP = _corp;
        PHONE_NUMBER = _totalPhoneNumber;
        PAIRING_HIST_SUID = (_pairingHistSuid != null && _pairingHistSuid.length > 0 ? _pairingHistSuid : null);
        PAIRING_HIST_ID = (_pairingHistId != null && _pairingHistId.length > 0 ? _pairingHistId : null);
        KTW.managers.otmPairing.KOLManager.smsSend(function(result, response) {
            if (result) {
                if (response.code == "S0000") {
                    // OAMS로부터 전달 받은 인증키 (인증번호 확인 시 필요함)
                    OAMS_AUTH_KEY = response.data.auth_no_key;
                    if (_callback) _callback(true);
                } else _showPairingErrorPopup(response.code, response.message, _callback);
            } else {
                if (_callback) _callback(false);
                _openErrorPopup({message: ERROR_TEXT.ET002});
            }
        }, PHONE_CORP, PHONE_NUMBER, PAIRING_HIST_ID);
    }

    // SMS 인증번호 확인
    function _smsVerify(_callback, _authNumber) {
        KTW.managers.otmPairing.KOLManager.smsVerify(function(result, response) {
            if (result) {
                if (response.code == "S0000" || response.code == "F0212") {
                    // 히스토리 재연결 여부
                    var isHisotry = !!PAIRING_HIST_ID;
                    // 기존에 올레tv모바일 약관을 동의했는지에 대한 여부
                    // (동의하지 않았다면 올레tv모바일 약관도 노출해야함)
                    MAND_AGREE_YN = response.data.mand_agree_yn;
                    // 액세스 토큰 (계속 사용해야 함)
                    TOKN_VAL = response.data.tokn_val;
                    SU_ID = response.data.suid;

                    // 타 페어링 존재 여부
                    //  02.28 sw.nam
                    // 타 페어링 확인 정책 변경으로 로직 수정
                    // 타 페어링이 존재하는 경우 결과코드 "S000" 이 아닌 "F0212" 로 별도 처리
                    PAIRING_ID = null;
                    PAIRING_ID = response.data.pairing_id;
                    if(response.code == "F0212") {

                        if (PAIRING_ID != null && PAIRING_ID.length > 0) {
                            // 타 페어링이 존재할 경우
                            // 기존 페어링 연결을 해제한 후 약관페이지 노출, 이때 chgPairing 값을 Y로 전송해야 함
                            if (_callback) _callback(true);
                            KTW.ui.LayerManager.activateLayer({
                                obj: {
                                    id: "OTMPOtherPairing",
                                    type: KTW.ui.Layer.TYPE.POPUP,
                                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        callback: function() {
                                            _removePairing(null, true, true, isHisotry);
                                        }
                                    }
                                },
                                moduleId: "module.family_home",
                                visible: true
                            });
                        } else {
                            log.printDbg("no other pairing");
                            if (isHisotry) {
                                // 히스토리 재연결인 경우
                                log.printDbg("makePairingHistory" +PAIRING_HIST_ID+" "+PAIRING_HIST_SUID);
                                _makePairingHistory(_callback, PAIRING_HIST_SUID, PAIRING_HIST_ID);
                            } else {
                                // 약관 목록 요청
                                _getAgreementList(_callback);
                            }
                        }
                    }else {
                        log.printDbg("no other pairing");
                        if (isHisotry) {
                            // 히스토리 재연결인 경우
                            log.printDbg("makePairingHistory" +PAIRING_HIST_ID+" "+PAIRING_HIST_SUID);
                            _makePairingHistory(_callback, PAIRING_HIST_SUID, PAIRING_HIST_ID);
                        } else {
                            // 약관 목록 요청
                            _getAgreementList(_callback);
                        }
                    }

                } else _showPairingErrorPopup(response.code, response.message, _callback);
            } else {
                if (_callback) _callback(false);
                _openErrorPopup({message: ERROR_TEXT.ET002});
            }
        }, PHONE_CORP, PHONE_NUMBER, OAMS_AUTH_KEY, _authNumber);
    }

    // 약관 목록 요청
    function _getAgreementList(_callback) {
        KTW.managers.otmPairing.KOLManager.getAgreementList(function(result, response) {
            if (result) {
                if (response.code == "S0000") {
                    if (_callback) _callback(true);
                    // 올레tv모바일 약관일 경우 존재하는 액세스 토큰, 페어링 시 전송해야 함
                    PRCS_MAIN_TOKN_VAL = response.data.prcs_main_tokn_val;
                    PRCS_SUB_TOKN_VAL = response.data.prcs_sub_tokn_val;
                    // 약관 팝업 노출
                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: "OTMPTerms",
                            type: KTW.ui.Layer.TYPE.POPUP,
                            priority: KTW.ui.Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                termsData: response.data,
                                termsKind: "otv",
                                mandAgreeYn: MAND_AGREE_YN
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                } else _showPairingErrorPopup(response.code, response.message);
            } else _openErrorPopup({message: ERROR_TEXT.ET002});
        }, SU_ID, PHONE_CORP, PHONE_NUMBER, TOKN_VAL, MAND_AGREE_YN);
    }

    // 페어링 요청
    function _makePairing(_callback, _otvAgreeList, _otmAgreeList) {
        KTW.managers.otmPairing.KOLManager.makePairing(function(result, response) {
            log.printDbg("_makePairing result == "+result);
            log.printDbg("_makePairing response == "+JSON.stringify(response));
            if (result) {
                if (response.code == "S0000") {
                    if (_callback) _callback(true);
                    // 필수 약관 단계에서 바로 완료되었을 경우
                    if (MAND_AGREE_YN == "Y") {
                        // 연결 완료 팝업 노출
                        var title = "연결 완료";
                        var msg = "사용 중이신 올레 tv 모바일과<br>올레 tv가 연결되었습니다";
                        var subMsg = "마스터 계정은 모바일 시청을 지원하는 콘텐츠를 자유롭게 이어볼 수 있습니다.<br>" +
                            "마스터 계정이 아닌 경우 월정액 이어보기와 일부 콘텐츠에 대한 이어보기가 제한됩니다";
                        _openBasicPopup(title, msg, [{name: "확인", callback: "close"}],subMsg);
                        _activateLoadingForMessage(null);

                    }
                    else {
                        // 올레tv모바일 약관 단계에서 완료 되었을 경우 (문자메시지 전송 관련 문구가 함께 노출되는 팝업)
                        KTW.ui.LayerManager.activateLayer({
                            obj: {
                                id: "OTMPConnectingComplete",
                                type: KTW.ui.Layer.TYPE.POPUP,
                                priority: KTW.ui.Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: null
                            },
                            moduleId: "module.family_home",
                            visible: true
                        });
                    }
                    _activateLoadingForMessage(null);
                } else _showPairingErrorPopup(response.code, response.message);
            } else _openErrorPopup({message: ERROR_TEXT.ET002});
        }, SU_ID, PHONE_CORP, PHONE_NUMBER, TOKN_VAL, PRCS_MAIN_TOKN_VAL, PRCS_SUB_TOKN_VAL, _otvAgreeList, _otmAgreeList);
    }

    // 페어링 재연결 요청,
    // 기존 연결이 존재해서 페어링 해제 후 재 연결 하는 경우도 포함
    function _makePairingHistory(_callback, _pairingHistSuid, _pairingHistId) {
        PAIRING_HIST_ID = _pairingHistId;
        PAIRING_HIST_SUID = _pairingHistSuid;
        KTW.managers.otmPairing.KOLManager.makePairingHistory(function(result, response) {
            if (result) {
                if (response.code == "S0000" || response.code == "F0212") {
                    PAIRING_ID = response.data.pairing_id;
                    if(response.code == "F0212") {
                        if (PAIRING_ID != null && PAIRING_ID.length > 0) {
                            // 타 페어링이 존재할 경우
                            // 기존 페어링 연결을 해제한 재연결 로직 수행
                            if (_callback) _callback(true);
                            KTW.ui.LayerManager.activateLayer({
                                obj: {
                                    id: "OTMPOtherPairing",
                                    type: KTW.ui.Layer.TYPE.POPUP,
                                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        callback: function() {
                                            _removePairing(null, true, true, true);
                                        }
                                    }
                                },
                                moduleId: "module.family_home",
                                visible: true
                            });
                        }
                    }else {

                        // 2018.03.02 sw.nam
                        // 히스토리 재 연결 완료 후 팝업 노출 시 callback 함수가 먼저 실행되도록 수정
                        // KTF 통신사 연결의 경우 연결하는 동안 loading bar 와 함꼐 연결 팝업이 뜨도록 되어있는데
                        // 연결 완료 후 연결 완료 팝업을 띄운 상태에서 callback 함수를 실행하게 되면
                        // 재 연결 팝업이 닫히면서 상위에 있는 layer 가 모두 deactivate 되므로
                        // callback 이 먼저 실행되도록 예외상황을 둔다.
                        if (_callback) _callback(true);

                        // 연결 완료 팝업 노출
                        var title = "연결 완료";
                        var msg = "사용 중이신 올레 tv 모바일과<br>올레 tv가 연결되었습니다";
                        var subMsg = "마스터 계정은 모바일 시청을 지원하는 콘텐츠를 자유롭게 이어볼 수 있습니다.<br>" +
                            "마스터 계정이 아닌 경우 월정액 이어보기와 일부 콘텐츠에 대한 이어보기가 제한됩니다";
                        _openBasicPopup(title, msg, [{name: "확인", callback: "close"}],subMsg);
                        _activateLoadingForMessage(null);
                    }
                } else {
                    if (_callback) _callback(false);
                    _showPairingErrorPopup(response.code, response.message);
                }
            } else {
                if (_callback) _callback(false);
                _openErrorPopup({message: ERROR_TEXT.ET002});
            }
        }, _pairingHistSuid, _pairingHistId);
    }

    // 약관 상세정보 노출
    function _showDetailTermsPopup(_stpltNo, _kind, _stpltNm) {
        KTW.managers.otmPairing.KOLManager.getAgreementDetail(function(result, response) {
            if (result) {
                if (response.code == "S0000") {
                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: "OTMPTermsDetail",
                            type: KTW.ui.Layer.TYPE.POPUP,
                            priority: KTW.ui.Layer.PRIORITY.POPUP,
                            linkage: true,
                            params: {
                                title: _stpltNm,
                                stplt_text_list: response.data.stplt_text_list
                            }
                        },
                        moduleId: "module.family_home",
                        visible: true
                    });
                } else _showPairingErrorPopup(response.code, response.message);
            } else _openErrorPopup({message: ERROR_TEXT.ET002});
        }, TOKN_VAL, _stpltNo, _kind, "2");
    }

    // 페어링 정보를 요청하고 캐시 데이터를 업데이트 함
    // 첫 키 시나리오 및 푸쉬로 인해 호출하는 경우는 _noShowError를 true로 호출해야 함
    // 정보조회 성공 여부와 페어링 정보를 콜백함
    function _getPairingInfo(_callback, _noShowError) {
        KTW.managers.otmPairing.KOLManager.getPairingList(function(result, response) {
            if (result) {
                if (response.code == "S0000") {
                    // 성공한 경우
                    if (response.data.pairing_list != null && response.data.pairing_list.length > 0) {
                        // 페어링 리스트 저장
                        PAIRING_LIST = [];
                        PAIRING_LIST = response.data.pairing_list;
                        log.printDbg("PAIRING_LIST= " +JSON.stringify(PAIRING_LIST));


                        // 마스터 페어링 아이디가 첫번째가 아닌 경우, 마스터 페어링 아이디가 먼저 오도록 순서 변경
                        if (PAIRING_LIST[0].master_yn != "Y") {
                            PAIRING_LIST = PAIRING_LIST.sort(function(a, b) {
                                var aMatserYn = a.master_yn != null && a.master_yn.length > 0 ? a.master_yn.toUpperCase() : "N";
                                var bMatserYn = b.master_yn != null && b.master_yn.length > 0 ? b.master_yn.toUpperCase() : "N";
                                return (aMatserYn > bMatserYn) ? -1 : (bMatserYn > aMatserYn) ? 1 : 0;
                            });
                        }
                    } else PAIRING_LIST = [];
                    if(_callback) {
                        _callback(true, PAIRING_LIST);
                    }

                } else {
                    if (!_noShowError) _showPairingErrorPopup(response.code, response.message);
                    PAIRING_LIST = null;
                    if(_callback) {
                        _callback(false, PAIRING_LIST);
                    }
                }
            } else {
                if (!_noShowError) _openErrorPopup({message: ERROR_TEXT.ET002});
                PAIRING_LIST = null;
                if(_callback) {
                    _callback(false, PAIRING_LIST);
                }
            }
        });
    }

    // 페어링 히스토리 목록을 요청함
    // 정보조회 성공 여부와 페어링 정보를 콜백함
    function _getPairingHistory(_callback) {
        KTW.managers.otmPairing.KOLManager.getPairingHistoryList(function(result, response) {
            PAIRING_HIST_LIST = [];
            if (result) {
                if (response.code == "S0000") {
                    if (response.data.pairing_hist_list != null && response.data.pairing_hist_list.length > 0) PAIRING_HIST_LIST = response.data.pairing_hist_list;
                    else PAIRING_HIST_LIST = [];
                    _callback(true, PAIRING_HIST_LIST);
                } else {
                    _showPairingErrorPopup(response.code, response.message);
                    _callback(false, PAIRING_HIST_LIST = []);
                }
            } else {
                _openErrorPopup({message: ERROR_TEXT.ET002});
                _callback(false, PAIRING_HIST_LIST = []);
            }
        });
    }

    // 페어링 해지 요청
    // isDelete 값이 true이면 완전삭제, false이면 연결끊기 (히스토리에 내역 남음)
    // smsverify 에서 불리는 경우가 있고
    // makePairingHistory 에서 불리는 경우가 있다.

    //sms verify 에서 불리는 경우
    function _removePairing(callback, isDelete, chgPairYn, isHistory, pairingId) {
        if(pairingId) {
            PAIRING_ID = pairingId;
        }
        KTW.managers.otmPairing.KOLManager.removePairing(function(result, response) {
            if (result) {
                if (response.code == "S0000") {
                    PAIRING_ID = null;
                    _closeAllOTMPPopup();

                    if (chgPairYn) {
                        // 2018.02.09 sw.nam
                        // 연결 해제 후 재 연결 시 1초 waiting 후 재 연결 시도한다 KT 요구사항
                        // 키를 막기 위한 로딩팝업 호출
                        KTW.ui.LayerManager.startLoading({preventKey : true});
                        setTimeout(function() {
                            KTW.ui.LayerManager.stopLoading();
                            if(isHistory) {
                                // 히스토리 재연결 로직
                                //TODO 로딩 팝업 해제 및 재연결 , 여기 파라미터가 이게 맞나 ??
                                _makePairingHistory(null, PAIRING_HIST_SUID, PAIRING_HIST_ID);
                            }else {
                                // 가입 단계에서 넘어온 경우 혹은 다음 단계로 넘어가기 위해 약관 목록을 요청함
                                _getAgreementList();
                            }
                        },1000);

                    }
                    else {
                        // 그렇지 않은 경우 해지 완료 팝업 노출 후 프로세스 종료
                        var ttl;
                        var msg;
                        if(isDelete) {
                            ttl = "연결 삭제 완료";
                            msg = "올레 tv에서 해당 계정이 삭제되었습니다";

                        }else {
                            ttl = "연결 끊기 완료";
                            msg = "올레 tv 모바일 연결 끊기가 완료되었습니다";
                        }
                        // 팝업 노출
                        _openBasicPopup(ttl, msg, [{name: "확인", callback: "close"}]);
                        if (callback) callback(true);
                        _activateLoadingForMessage(null);
                    }
                } else {
                    if (callback) callback(false);
                    _showPairingErrorPopup(response.code, response.message);
                }
            } else {
                if (callback) callback(false);
                _openErrorPopup({message: ERROR_TEXT.ET002});
            }
        }, KTW.SAID, PAIRING_ID, chgPairYn ? "Y" : "N", isDelete ? 1 : 2);
    }

    // 페어링 히스토리 삭제
    function _removePairingHistory(callback,_pairingSuid, _pairingHistId) {
        KTW.managers.otmPairing.KOLManager.removePairingHistory(function(result, response) {
            if (result) {
                if (response.code == "S0000") {
                    // 팝업 노출
                    var ttl = "연결 삭제 완료";
                    var msg = "올레 tv에서 해당 계정이 삭제되었습니다";
                    _openBasicPopup(ttl, msg, [{name: "확인", callback: "close"}]);

                    if(callback) callback(true);

                } else _showPairingErrorPopup(response.code, response.message);
            } else _openErrorPopup({message: ERROR_TEXT.ET002});
        }, _pairingSuid, _pairingHistId);
    }

    // 페어링 마스터 변경
    function _setPairingMaster(callback, pairing_id, suid) {
        log.printDbg("setPairingMaster()");

        KTW.managers.otmPairing.KOLManager.setPairingMaster(function(result, response) {
            if(result) {
                if(response.code == "S0000") {
                    // 마스터 변경 완료
                    var ttl = "변경 완료";
                    var msg = "마스터 계정이 변경되었습니다";
                    _openBasicPopup(ttl, msg, [{name: "확인", callback: "close"}]);

                    if (callback) callback(true);
                    // _activateLoadingForMessage(null);

                } else _showPairingErrorPopup(response.code, response.message);
            } else _openErrorPopup({message: ERROR_TEXT.ET002});
        },pairing_id,suid)
    }


    /** [sw.nam]
     * 심리스 푸시 메시지 응답 대기 및 초기 화면 로딩 시 사용하는 loading dialog activating 함수
     * 푸시 응답이 없는 경우 직접 페어링 조회 API 를 호출한다
     * @param sec : 로딩 시간(초), 없으면 5초로 고정
     */
    function _activateLoadingForMessage(sec) {
        var timeout;
        KTW.ui.LayerManager.stopLoading();
        if(sec) {
            timeout = sec * 1000;
        }else {
            timeout = 3 * 1000;
        }
        var isTimeout = false;
        var loadingData = {
            boxSize : {
                left: 1,
                top: 150,
                width: KTW.CONSTANT.RESOLUTION.WIDTH,
                height: 1310
            },
            preventKey: true,
            timeout: timeout,
            cbTimeout: function () {
                log.printDbg("timeout ! request pairing manually");
                if (!isTimeout) {
                    isTimeout = true;
                    // pairing 요청
                    isUpdatedList = false;
                    manualUpdateFlag = true;
                    _getAllPairingInfo();
                }
            }

        };

        KTW.ui.LayerManager.startLoading(loadingData);
    }

    /**
     * [sw.nam]
     * API 호출 없이 가장 마지막에 호출 된 페어링 리스트 & 페어링 히스토리 리스트를 가져온다.
     * ex) 1-depth 좌측 메뉴에서 사용
     * @returns {Array}
     * @private
     */
    function _getPairingList() {
        log.printDbg("_getPairingList()");
        log.printDbg("PAIRING_LIST= " +JSON.stringify(PAIRING_LIST));
        return PAIRING_LIST;
    }
    function _getPairingHistList() {
        log.printDbg("_getPairingHistList()");

        return PAIRING_HIST_LIST;

    }


    /**
     * OTM 관리 화면 갱신을 위한 listener 를 추가하는 함수
     * @param listener
     * @private
     */
    function _addPairingStateListener (listener) {
        log.printDbg("_addPairingStateListener()");

        var index = pairingStateListener.indexOf(listener);
        if (index < 0) {
            pairingStateListener.push(listener);
        }
    }
    function _removePairingStateListener (listener) {
        log.printDbg("_removePairingStateListener()");

        var index = pairingStateListener.indexOf(listener);
        if (index !== -1) {
            pairingStateListener.splice(index, 1);
        }
    }

    /**
     * OTM 화면 갱신이 필요한 경우에 호출되는 함수 (푸쉬메시지를 받았을 때 호출 한다)
     * @param options
     *         listener : 특정 listener 가 있는 경우 해당 listener 에만 notify
     */
    function notifyPairingStateChanged (options) {
        log.printDbg("notifyPairingStateChanged(" + log.stringify(options) + ")");

        var arrListener = pairingStateListener.slice();
        var length = arrListener.length;

        for(var i = 0; i < length ; i++) {
            if(options !== "" && options !== null && options.listener) {
                if(arrListener[i]) {
                    var stringArr = arrListener[i].toString();
                    if(stringArr.indexOf(options.listener) > -1) {
                        arrListener[i]();
                        break;
                    }
                }
            }else {
                if (arrListener[i]) {
                    arrListener[i]();
                }
            }
        }
    }

    return {
        init: _init,
        requestAuthNumber: _requestAuthNumber,
        showDetailTermsPopup: _showDetailTermsPopup,
        makePairing: _makePairing,
        makePairingHistory: _makePairingHistory,
        removePairing: _removePairing,
        removePairingHistory: _removePairingHistory,
        smsVerify: _smsVerify,
        getPairingInfo: _getPairingInfo,
        getPairingHistory: _getPairingHistory,
        openBasicPopup: _openBasicPopup,
        openErrorPopup: _openErrorPopup,
        setPairingMaster: _setPairingMaster,
        activateLoadingForMessage: _activateLoadingForMessage,
        getPairingList: _getPairingList,
        getPairingHistList: _getPairingHistList,
        getAllPairingInfo:_getAllPairingInfo,
        addPairingStateListener: _addPairingStateListener,
        removePairingStateListener:_removePairingStateListener
    };


})();