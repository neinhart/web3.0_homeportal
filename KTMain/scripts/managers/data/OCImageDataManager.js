"use strict";

/**
 * OC - w3_unicast_version.json 에 명시된 Image zip 파일들에 대한 관리 매니저 (채널 iframe, vod iframe, app logo, cug logo)
 */
KTW.managers.data.OCImageDataManager = (function() {

    var TYPE = {
        BI_LOGO: "bi_logo",
        CHANNEL_IFRAME: "channel_iframe",
        VOD_IFRAME: "vod_iframe",
        APP_LOGO: "app_logo",
        CUG_LOGO: "cug_logo"
    };

    var SHARE_CHANNEL_IMFRAME_MAP = "share_ch_iframe_map.json";

    var ZIP_EXTRACTING_STATUS = {
        STARTED : 0,
        SUCCEEDED : 1,
        CANCELED	: 2,
        FAILED : 3
    };

    var PATH_UNICAST_IMAGES = "live/unicast_images";

    var ZIP_NAME = {};
    ZIP_NAME[TYPE.BI_LOGO] = "w3_bi_logo.zip";
    ZIP_NAME[TYPE.CHANNEL_IFRAME] = "w3_ch_iframe.zip";
    ZIP_NAME[TYPE.VOD_IFRAME] = "w3_vod_iframe.zip";
    ZIP_NAME[TYPE.APP_LOGO] = "w3_app_logo.zip";
    ZIP_NAME[TYPE.CUG_LOGO] = "w3_cug_logo.zip";

    var data = {
        biLogo: {
            version: null,
            downloadUrl: null,
            path: null,
            imageList: []
        },
        channelIframe: {
            version: null,
            downloadUrl: null,
            path: null,
            iframeMap: null,
            imageList: []
        },
        vodIframe: {
            version: null,
            downloadUrl: null,
            path: null,
            imageList: []
        },
        appLogo: {
            version: null,
            downloadUrl: null,
            path: null,
            imageList: []
        },
        cugLogo: {
            version: null,
            downloadUrl: null,
            path: null,
            imageList: []
        }
    };

    var log = KTW.utils.Log;

    var fileManager = null;
    var path_root = null;

    // 다음 버전확인을 위한 timer
    var updateTimer = null;
    // 다음 버전확인 시간을 구하기 위해 사용되는 변수
    var checkTime = -1;

    var initBiLogo = false;
    var initDataChannelIframe = false;
    var initDataVodIframe = false;
    var initDataAppLogo = false;
    var initDataCugLogo = false;


    function _init () {
        log.printDbg("_init()");

        PATH_UNICAST_IMAGES = KTW.CONSTANT.DEFAULT_FILE_PATH + "/unicast_images";

        fileManager = KTW.managers.data.fileManager;

        fileManager.getUserDataPath(function (param) {
            if (param == null) {
                path_root = '/mnt/flash/altibrowser/';
            }
            else {
                path_root = param;
            }

            initData({
                type: TYPE.BI_LOGO,
                callback: function () {
                    initBiLogo = true;
                }
            });
            initData({
                type: TYPE.CHANNEL_IFRAME,
                callback: function () {
                    initDataChannelIframe = true;

                    if (KTW.CONSTANT.IS_OTS) {
                        data.channelIframe.version = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_CHANNEL_IFRAME_VERSION);
                    }
                    else {
                        initDataVodIframe = true;
                    }
                }
            });
            /**
             * [dj.son] OTV 에서는 channel iframe 에 vod iframe 이 포함됨
             */
            if (KTW.CONSTANT.IS_OTS) {
                initData({
                    type: TYPE.VOD_IFRAME,
                    callback: function () {
                        initDataVodIframe = true;
                    }
                });
            }
            initData({
                type: TYPE.APP_LOGO,
                callback: function () {
                    initDataAppLogo = true;
                }
            });
            initData({
                type: TYPE.CUG_LOGO,
                callback: function () {
                    initDataCugLogo = true;
                }
            });
        });
    }

    function initData (options) {
        log.printDbg("initData(" + options.type + ")");

        fileManager.mkDir(PATH_UNICAST_IMAGES + "/" + options.type, function (is_success, msg, path_obj, dirs){
            log.printDbg('##### init() is_success: ' + is_success);
            log.printDbg('##### init() path_obj: ' + JSON.stringify(path_obj));
            log.printDbg('##### init() dirs: ' + dirs);

            if (is_success === true) {
                var src_path = window.location.href.indexOf('mnt') == -1 ? path_obj.path : path_obj.path_dev;
                log.printDbg('##### init() src_path: ' + src_path);

                setDataPath({
                    type: options.type,
                    path: path_root + src_path + PATH_UNICAST_IMAGES + "/" + options.type + "/"
                });

                /**
                 * [dj.son] OTV 에서는 channel iframe 에 vod iframe 이 포함됨
                 */
                if (!KTW.CONSTANT.IS_OTS && options.type === TYPE.CHANNEL_IFRAME) {
                    setDataPath({
                        type: TYPE.VOD_IFRAME,
                        path: path_root + src_path + PATH_UNICAST_IMAGES + "/" + options.type + "/"
                    });
                }

                updateFileList({
                    type: options.type,
                    isInit: true,
                    callback: options.callback
                });
            }
            else {
                if (options.callback) {
                    options.callback();
                }
            }
        });
    }

    function setDataPath (options) {
        log.printDbg("setDataPath(" + log.stringify(options) + ")");

        switch (options.type) {
            case TYPE.BI_LOGO:
                data.biLogo.path = options.path;
                break;
            case TYPE.CHANNEL_IFRAME:
                data.channelIframe.path = options.path;
                break;
            case TYPE.VOD_IFRAME:
                data.vodIframe.path = options.path;
                break;
            case TYPE.APP_LOGO:
                data.appLogo.path = options.path;
                break;
            case TYPE.CUG_LOGO:
                data.cugLogo.path = options.path;
                break;
        }
    }


    function isInitData () {
        var isTune = KTW.main.isReceiveChannelEventOnBooting();
        return initDataChannelIframe && initDataVodIframe && initDataAppLogo && initDataCugLogo && isTune;
    }

    function _checkUpdate () {
        log.printDbg("_checkUpdate()");

        if (!isInitData()) {
            updateTimer = setTimeout(function() {
                _checkUpdate();
            }, 3 * 1000);
            return;
        }

        clearTimeout(updateTimer);
        updateTimer = null;

        try {
            var unicastVersion = JSON.parse(KTW.managers.data.OCManager.getUnicastVersionData());

            checkUpdateBiLogo(unicastVersion);

            updateTimer = setTimeout(function() {
                _checkUpdate();
            }, getNextCheckTime());
        }
        catch (e) {
            log.printErr(e.message);
        }
    }

    function getDownloadUrl (options) {
        var downloadUrl = options.defaultUrl;

        if (KTW.managers.service.PromoManager.isPromoChannel(KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel())) {
            var locator = "";
            var componentTag = "";
            if (KTW.RELEASE_MODE === "LIVE") {
                locator = KTW.DATA.PROMO.LIVE.LOCATOR;
                if (options.type === TYPE.BI_LOGO) {
                    componentTag = KTW.DATA.PROMO.LIVE.COMPONENT_TAG_LOGO;
                }
                else if (options.type === TYPE.CHANNEL_IFRAME) {
                    componentTag = KTW.DATA.PROMO.LIVE.COMPONENT_TAG_IFRAME;
                }
            }
            else if (KTW.RELEASE_MODE === "BMT") {
                locator = KTW.DATA.PROMO.BMT.LOCATOR;
                if (options.type === TYPE.BI_LOGO) {
                    componentTag = KTW.DATA.PROMO.BMT.COMPONENT_TAG_LOGO;
                }
                else if (options.type === TYPE.CHANNEL_IFRAME) {
                    componentTag = KTW.DATA.PROMO.BMT.COMPONENT_TAG_IFRAME;
                }
            }
            else {
                locator = KTW.DATA.PROMO.TEST.LOCATOR;
                if (options.type === TYPE.BI_LOGO) {
                    componentTag = KTW.DATA.PROMO.TEST.COMPONENT_TAG_LOGO;
                }
                else if (options.type === TYPE.CHANNEL_IFRAME) {
                    componentTag = KTW.DATA.PROMO.TEST.COMPONENT_TAG_IFRAME;
                }
            }

            downloadUrl = locator + "." + componentTag + "/" + ZIP_NAME[options.type];
        }

        return downloadUrl;
    }

    /**
     * w3_bi_logo.zip 파일 버전확인 및 attach 요청
     */
    function checkUpdateBiLogo (unicastVersion) {
        log.printDbg('checkUpdateBiLogo()');

        if (!unicastVersion || !unicastVersion.biLogoZip || !unicastVersion.biLogoZip.version
            || !unicastVersion.biLogoZip.downloadUrl || unicastVersion.biLogoZip.downloadUrl.length === 0) {
            resetImageData(TYPE.BI_LOGO);
            checkUpdateChannelIframe(unicastVersion);
            return;
        }

        if (!data.biLogo.version || data.biLogo.version !== unicastVersion.biLogoZip.version) {
            var downloadUrl = getDownloadUrl({
                defaultUrl: unicastVersion.biLogoZip.downloadUrl,
                type: TYPE.BI_LOGO
            });

            var cbAttach = function (options) {
                if (options && options.success) {
                    saveZipFile({
                        xhrData: options.data,
                        type: TYPE.BI_LOGO,
                        callback: function () {
                            checkUpdateChannelIframe(unicastVersion);
                        }
                    });
                }
                else {
                    resetImageData(TYPE.BI_LOGO);
                    checkUpdateChannelIframe(unicastVersion);
                }
            };

            attach({
                url: downloadUrl,
                callback: function (options) {
                    if (options.success) {
                        cbAttach(options);
                    }
                    else {
                        if (downloadUrl === unicastVersion.biLogoZip.downloadUrl) {
                            cbAttach(options);
                        }
                        else {
                            downloadUrl = unicastVersion.biLogoZip.downloadUrl;
                            attach({
                                url: downloadUrl,
                                callback: cbAttach
                            });
                        }
                    }
                }
            });
        }
        else {
            checkUpdateChannelIframe(unicastVersion);
        }
    }

    /**
     * w3_ch_iframe.zip 파일 버전확인 및 attach 요청
     *
     * - OTV 일 경우, oc unicastVersion 으로부터 version 정보 획득 후 promo / oc 로부터 zip 파일 다운로드
     * - OTS 일 경우, IMS 서버로부터 version 정보 획득 API 호출 후 해당 download url 로부터 zip 파일 다운로드
     */
    function checkUpdateChannelIframe (unicastVersion) {
        log.printDbg('checkUpdateChannelIframe()');

        if (!KTW.CONSTANT.IS_OTS) {
            if (!unicastVersion || !unicastVersion.chIframeZip || !unicastVersion.chIframeZip.version
                || !unicastVersion.chIframeZip.downloadUrl || unicastVersion.chIframeZip.downloadUrl.length === 0) {
                resetImageData(TYPE.CHANNEL_IFRAME);
                checkUpdateVodIframe(unicastVersion);
                return;
            }
        }

        var otsVersionObj = null;

        var cbAttach = function (options) {
            if (options && options.success) {
                saveZipFile({
                    xhrData: options.data,
                    type: TYPE.CHANNEL_IFRAME,
                    callback: function (is_success) {

                        /**
                         * [dj.son] OTS 에서 unzip 에 실패했을때를 대비에 최종적으로 callback 에서 버전정보 갱신
                         */
                        if (otsVersionObj && is_success) {
                            data.channelIframe.version = otsVersionObj.version;
                            data.channelIframe.downloadUrl = otsVersionObj.imgpath;

                            KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.OTS_CHANNEL_IFRAME_VERSION, data.channelIframe.version);
                        }

                        checkUpdateVodIframe(unicastVersion);
                    }
                });
            }
            else {
                resetImageData(TYPE.CHANNEL_IFRAME);
                checkUpdateVodIframe(unicastVersion);
            }
        };

        if (KTW.CONSTANT.IS_OTS) {
            /**
             * [dj.son] OTS 인 경우, 특정 서버로부터 channel iframe 다운로드 url 얻은 후 다운로드
             */
            getChannelIframeDownloadUrlOnOTS(function (options) {
                if (options && options.success && options.data && options.data.code === "ok") {
                    otsVersionObj = options.data.data;

                    if (otsVersionObj.version != data.channelIframe.version) {
                        attach({
                            url: otsVersionObj.imgpath,
                            callback: cbAttach
                        });
                    }
                    else {
                        checkUpdateVodIframe(unicastVersion);
                    }
                }
                else {
                    resetImageData(TYPE.CHANNEL_IFRAME);
                    checkUpdateVodIframe(unicastVersion);
                }
            });
        }
        else {
            /**
             * [dj.son] OTV 인 경우, 먼저 promo 로부터 channel iframe 받고, 실패시 unicastVersion 에 있는 url 로 다운로드
             */
            if (!data.channelIframe.version || data.channelIframe.version !== unicastVersion.chIframeZip.version) {
                var downloadUrl = getDownloadUrl({
                    defaultUrl: unicastVersion.chIframeZip.downloadUrl,
                    type: TYPE.CHANNEL_IFRAME
                });

                attach({
                    url: downloadUrl,
                    callback: function (options) {
                        if (options.success) {
                            cbAttach(options);
                        }
                        else {
                            if (downloadUrl === unicastVersion.chIframeZip.downloadUrl) {
                                cbAttach(options);
                            }
                            else {
                                downloadUrl = unicastVersion.chIframeZip.downloadUrl;
                                attach({
                                    url: downloadUrl,
                                    callback: cbAttach
                                });
                            }
                        }
                    }
                });
            }
            else {
                checkUpdateVodIframe(unicastVersion);
            }
        }
    }

    function checkUpdateVodIframe (unicastVersion) {
        log.printDbg('checkUpdateVodIframe()');

        /**
         * [dj.son] OTS 에서만 vod iframe 다운받도록 수정
         */
        if (!KTW.CONSTANT.IS_OTS) {
            checkUpdateAppLogo(unicastVersion);
            return;
        }

        if (!unicastVersion || !unicastVersion.vodIframeZip || !unicastVersion.vodIframeZip.version
            || !unicastVersion.vodIframeZip.downloadUrl || unicastVersion.vodIframeZip.downloadUrl.length === 0) {
            resetImageData(TYPE.VOD_IFRAME);

            checkUpdateAppLogo(unicastVersion);
            return;
        }

        if (!data.vodIframe.version || data.vodIframe.version !== unicastVersion.vodIframeZip.version) {
            attach({
                url: unicastVersion.vodIframeZip.downloadUrl,
                callback: function (options) {
                    if (options.success) {
                        saveZipFile({
                            xhrData: options.data,
                            type: TYPE.VOD_IFRAME,
                            callback: function () {
                                checkUpdateAppLogo(unicastVersion);
                            }
                        });
                    }
                    else {
                        resetImageData(TYPE.VOD_IFRAME);

                        checkUpdateAppLogo(unicastVersion);
                    }
                }
            });
        }
        else {
            checkUpdateAppLogo(unicastVersion);
        }
    }

    function checkUpdateAppLogo (unicastVersion) {
        log.printDbg('checkUpdateAppLogo()');

        if (!unicastVersion || !unicastVersion.appLogoZip || !unicastVersion.appLogoZip.version
            || !unicastVersion.appLogoZip.downloadUrl || unicastVersion.appLogoZip.downloadUrl.length === 0) {
            resetImageData(TYPE.APP_LOGO);

            checkUpdateCugLogo(unicastVersion);
            return;
        }

        if (!data.appLogo.version || data.appLogo.version !== unicastVersion.appLogoZip.version) {
            attach({
                url: unicastVersion.appLogoZip.downloadUrl,
                callback: function (options) {
                    if (options.success) {
                        saveZipFile({
                            xhrData: options.data,
                            type: TYPE.APP_LOGO,
                            callback: function () {
                                checkUpdateCugLogo(unicastVersion);
                            }
                        });
                    }
                    else {
                        resetImageData(TYPE.APP_LOGO);

                        checkUpdateCugLogo(unicastVersion);
                    }
                }
            });
        }
        else {
            checkUpdateCugLogo(unicastVersion);
        }
    }

    function checkUpdateCugLogo (unicastVersion) {
        log.printDbg('checkUpdateCugLogo()');

        if (!unicastVersion || !unicastVersion.cugLogoZip || !unicastVersion.cugLogoZip.version
            || !unicastVersion.cugLogoZip.downloadUrl || unicastVersion.cugLogoZip.downloadUrl.length === 0) {
            resetImageData(TYPE.CUG_LOGO);
            return;
        }

        if (!data.cugLogo.version || data.cugLogo.version !== unicastVersion.cugLogoZip.version) {
            attach({
                url: unicastVersion.cugLogoZip.downloadUrl,
                callback: function (options) {
                    if (options.success) {
                        saveZipFile({
                            xhrData: options.data,
                            type: TYPE.CUG_LOGO,
                            callback: function () {
                            }
                        });
                    }
                    else {
                        resetImageData(TYPE.CUG_LOGO);
                    }
                }
            });
        }
        else {
        }
    }


    function resetImageData (type) {
        log.printDbg('resetImageData(' + type + ')');

        switch (type) {
            case TYPE.BI_LOGO:
                data.biLogo.version = null;
                data.biLogo.downloadUrl = null;
                data.biLogo.imageList = [];
                break;
            case TYPE.CHANNEL_IFRAME:
                data.channelIframe.version = null;
                data.channelIframe.downloadUrl = null;
                data.channelIframe.iframeMap = null;
                data.channelIframe.imageList = [];

                if (!KTW.CONSTANT.IS_OTS) {
                    resetImageData(TYPE.VOD_IFRAME);
                }

                break;
            case TYPE.VOD_IFRAME:
                data.vodIframe.version = null;
                data.vodIframe.downloadUrl = null;
                data.vodIframe.imageList = [];
                break;
            case TYPE.APP_LOGO:
                data.appLogo.version = null;
                data.appLogo.downloadUrl = null;
                data.appLogo.imageList = [];
                break;
            case TYPE.CUG_LOGO:
                data.cugLogo.version = null;
                data.cugLogo.downloadUrl = null;
                data.cugLogo.imageList = [];
                break;
        }

        fileManager.deleteDirectory(PATH_UNICAST_IMAGES + "/" + type, function (success){
            log.printWarn('remove ' + type + " directory" + ' file - success: ' + success);
        });
    }


    /**
     * attch 요청
     */
    function attach (options) {
        log.printInfo("attach()" + options.url);

        $.ajax({
            url: options.url,
            dataType: "text",
            beforeSend: function(xhr) {
                xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
            },
            statusCode: {
                404: function() {
                    log.printDbg("Status 404: page not found");
                }
            }
        })
        .done(function (resultData) {
            log.printDbg(options.url + " download done!!!");

            options.callback({
                success: true,
                data: resultData
            });
        })
        .fail(function (jqXHR, textStatus) {
            log.printDbg(options.url + " download fail...");

            options.callback({
                success: false
            });
        })
        .always();
    };

    /**
     * OTS 인 경우, channel iframe 다운로드 url 획득하는 API
     */
    function getChannelIframeDownloadUrlOnOTS (callback) {
        log.printInfo("getChannelIframeDownloadUrlOnOTS()");

        var url = KTW.DATA.HTTP.IMS_SERVER.LIVE_URL;
        if (KTW.RELEASE_MODE === "BMT") {
            url = KTW.DATA.HTTP.IMS_SERVER.BMT_URL;
        }

        var curVersion = data.channelIframe.version;
        if (!curVersion) {
            curVersion = 0;
        }

        url += "/api/v1/stb/background/400/" + KTW.SMARTCARD_ID + "?version=" + curVersion;

        log.printInfo("url : " + url);

        $.ajax({
            url: url,
            dataType: "json"
        })
        .done(function (resultData) {
            log.printDbg(url + " download done!!!");
            log.printDbg(log.stringify(resultData));

            if (callback) {
                callback({
                    success: true,
                    data: resultData
                });
            }
        })
        .fail(function (jqXHR, textStatus) {
            log.printDbg(url + " download fail...");

            if (callback) {
                callback({
                    success: false
                });
            }
        })
        .always();
    }


    /**
     * 다음 버전확인 시까지 timer delay 계산하는 함수.
     * @returns checkTime timer dalay (단위:ms)
     */
    function getNextCheckTime () {
        // 최초 update 이후에는 고정값(3시간) return
        if (checkTime > 0) {
            return KTW.DATA.ATTS.UNICAST_CHECK_PERIOD;
        }

        // 최초 부팅 시 다음 호출시간은 MAC 기준으로 3시간 ~ 6시간 사이 분산처리
        var now = Date.now();
        checkTime = KTW.DATA.ATTS.BOOTING_EXTRA_TIME + getTryTimeBasedOnMAC(now, KTW.DATA.ATTS.UNICAST_CHECK_PERIOD);
        log.printDbg("getNextCheckTime() - nextCheckTime = " + new Date(checkTime));
        checkTime = checkTime - now;
        log.printDbg("checkTime " + checkTime);

        return checkTime;
    }

    /**
     * 부하 분산처리를 위해서 MAC정보로 처리한다.
     */
    function getTryTimeBasedOnMAC (currentTime, timePeriod) {

        var mac = null;
        if (KTW.PLATFORM_EXE === "PC") {
            mac = "f3e249008a70"; // FOR TEST CODE
        }
        else {
            mac = KTW.oipf.adapters.HWAdapter.networkInterface.getMacAddress();
        }

        // 마지막 4byte 값을 이용하여 접근 시각값을 계산한다.
        var cnt = 0;
        var total = 0;
        for (var i = mac.length - 1; i >= 0 && cnt < 4; i--) {
            var ch = mac.charAt(i);
            try {
                var n = parseInt(ch, 16);
                total = total * 16 + n;
                log.printDbg("ch=" + ch + ",int=" + n + ",sum=" + total);
                cnt++;
            } catch (error) {
                log.printErr(error.message);
            }
        }

        // offset은 STB마다 0시간 ~ timePeriod 사이의 고정 값
        var offset = (total * 1000) % timePeriod;
        // STB마다 특정 시간에 update를 요청하도록 하기 위해서 timePeriod의 배수에 offset을 더한다
        var tryTime = Math.floor(currentTime / timePeriod) * timePeriod + offset;
        if (tryTime < currentTime) {
            tryTime += timePeriod;
        }
        log.printDbg("tryTime= " + new Date(tryTime));

        return tryTime;
    }





    /**
     * zip file 을 parsing 하여 hash 에 저장하는 함수.
     * @param _xhrdata xmlHTTPRequest response data.
     * @param _path zip file name
     */
    function saveZipFile (options) {
        log.printInfo("saveZipFile()");

        var blob = new Blob([ stringToBinary(options.xhrData) ]);

        fileManager.write(PATH_UNICAST_IMAGES + "/" + options.type + ".zip", blob, function (is_success, error_msg, written_path) {
            log.printInfo("saveZipFile callback " + is_success);

            if (is_success === true) {
                var tmp_path = written_path;
                if (tmp_path != null) {
                    parsingZipFile({
                        type: options.type,
                        path: tmp_path,
                        callback: options.callback
                    });
                }
            }
            else {
                log.printDbg('saveZipFile() error_msg: ' + error_msg);

                fileManager.removeFile(PATH_UNICAST_IMAGES + "/" + options.type + ".zip", function (success){
                    log.printWarn('remove ' + options.type + ".zip" + ' file - success: ' + success);
                });

                if (options.callback) {
                    options.callback(is_success);
                }
            }
        });
    }


    /**
     * FileWrite를 위해 xhr response 를 binary array로 변환
     */
    function stringToBinary (response) {
        var byte_array = new Uint8Array(response.length);
        for (var i = 0; i < response.length; i++) {
            byte_array[i] = response.charCodeAt(i) & 0xff;
        }
        return byte_array;
    }

    function parsingZipFile (options) {
        log.printInfo("parsingZipFile(" + log.stringify(options) + ")");

        // Master 경로로 try
        var src_path = options.path.path;
        var src_original_path = options.path.original.path;

        // 압축해제 위치가 동일 폴더이기 때문에
        // 압축해제 성공여부와 상관없이 중간 과정에서 기존 파일들은 모두 삭제된다
        // 따라서 기존 파일들의 경로를 초기화 해준다
        switch (options.type) {
            case TYPE.BI_LOGO:
                data.biLogo.imageList = [];
                break;
            case TYPE.CHANNEL_IFRAME:
                data.channelIframe.imageList = [];

                if (!KTW.CONSTANT.IS_OTS) {
                    data.vodIframe.imageList = [];
                }

                break;
            case TYPE.VOD_IFRAME:
                data.vodIframe.imageList = [];
                break;
            case TYPE.APP_LOGO:
                data.appLogo.imageList = [];
                break;
            case TYPE.CUG_LOGO:
                data.cugLogo.imageList = [];
                break;
        }

        var destDir = path_root + src_path.substr(0, src_path.lastIndexOf(".zip")) + "/";
        var tmpZipPath = decodeURIComponent(path_root + src_path);
        var tmpDestDirPath = decodeURIComponent(destDir);
        KTW.oipf.AdapterHandler.zipAdapter.requestExtraction(tmpZipPath, tmpDestDirPath, function(handle, state) {
            log.printDbg('state_1: ' + state);

            if (state === ZIP_EXTRACTING_STATUS.SUCCEEDED) {
                // success
                destDir = path_root +  src_original_path.substr(0, src_original_path.lastIndexOf(".zip")) + "/";
                setDataPath({
                    type: options.type,
                    path: destDir
                });
                updateFileList({
                    type: options.type,
                    callback: options.callback
                });

                fileManager.removeFile(PATH_UNICAST_IMAGES + "/" + options.type + ".zip", function(success){
                    log.printWarn('remove ' + options.type + ' file - success: ' + success);
                });
            }
            else if (state !== ZIP_EXTRACTING_STATUS.STARTED) {
                // fail
                log.printDbg('parsingZipFile() Master failed!!');

                // Slave 경로로 retry
                src_path = options.path.path_dev;
                src_original_path = options.path.original.path_dev;

                destDir = path_root + src_path.substr(0, src_path.lastIndexOf(".zip")) + "/";
                KTW.oipf.AdapterHandler.zipAdapter.requestExtraction(path_root + src_path, destDir, function(handle, state) {
                    log.printDbg('state_2: ' + state);

                    if (state === ZIP_EXTRACTING_STATUS.SUCCEEDED) {
                        // success
                        destDir = path_root +  src_original_path.substr(0, src_original_path.lastIndexOf(".zip")) + "/";
                        setDataPath({
                            type: options.type,
                            path: destDir
                        });
                        updateFileList({
                            type: options.type,
                            callback: options.callback
                        });

                        fileManager.removeFile(PATH_UNICAST_IMAGES + "/" + options.type + ".zip", function(success){
                            log.printWarn('remove ' + options.type + ' file - success: ' + success);
                        });
                    }
                    else if (state !== ZIP_EXTRACTING_STATUS.STARTED) {
                        // fail
                        log.printDbg('parsingZipFile() Slave failed!!');

                        fileManager.removeFile(PATH_UNICAST_IMAGES + "/" + options.type + ".zip", function(success){
                            log.printWarn('remove ' + options.type + ' file - success: ' + success);
                        });

                        if (options.callback) {
                            options.callback();
                        }
                    }
                });
            }
        });
    }

    function updateFileList (options) {
        log.printInfo("updateFileList(" + log.stringify(options) + ")");

        fileManager.listFiles(PATH_UNICAST_IMAGES + "/" + options.type, function (is_success, fileEntry){
            if (is_success === true) {
                if (fileEntry != null && fileEntry.length > 0) {
                    for (var i in fileEntry) {
                        var fileName = fileEntry[i].name;
                        log.printDbg('updateFileList() fileName: ' + fileName);

                        if (fileName.indexOf('.txt') > -1) {
                            log.printDbg("version check");

                            if (options.isInit) {
                                updateVersionFromFile({
                                    type: options.type,
                                    fileName: fileName
                                })
                            }
                        }
                        else {
                            switch (options.type) {
                                case TYPE.BI_LOGO:
                                    data.biLogo.imageList.push(fileName);
                                    break;
                                case TYPE.CHANNEL_IFRAME:
                                    if (fileName.indexOf(SHARE_CHANNEL_IMFRAME_MAP) !== -1) {
                                        updateChannelIframeMap();
                                    }
                                    /**
                                     * [dj.son] OTV 에서는 vod iframe 이 channel iframe 에 포함되어 옴
                                     */
                                    else if (fileName.indexOf("web_vod_") !== -1) {
                                        data.vodIframe.imageList.push(fileName);
                                    }
                                    else {
                                        data.channelIframe.imageList.push(fileName);
                                    }
                                    break;
                                case TYPE.VOD_IFRAME:
                                    data.vodIframe.imageList.push(fileName);
                                    break;
                                case TYPE.APP_LOGO:
                                    data.appLogo.imageList.push(fileName);
                                    break;
                                case TYPE.CUG_LOGO:
                                    data.cugLogo.imageList.push(fileName);
                                    break;
                            }
                        }
                    }
                }
            }

            if (options.callback) {
                options.callback(is_success);
            }
        });
    }

    function updateVersionFromFile (options) {
        log.printDbg("updateVersionFromFile(" + log.stringify(options) + ")");

        fileManager.readText("/" + PATH_UNICAST_IMAGES + "/" + options.type + "/" + options.fileName , function (is_success, result){
            log.printDbg('fileManager.readText() callback is_success : ' + is_success + ' , result : ' + result);

            if(is_success === true) {
                switch (options.type) {
                    case TYPE.BI_LOGO:
                        data.biLogo.version = result;
                        break;
                    case TYPE.CHANNEL_IFRAME:
                        data.channelIframe.version = result;
                        /**
                         * [dj.son] OTV 에서는 vod iframe 이 channel iframe 에 포함되어 오므로 version 을 동일하게 set
                         */
                        if (!KTW.CONSTANT.IS_OTS) {
                            data.vodIframe.version = result;
                        }
                        break;
                    case TYPE.VOD_IFRAME:
                        data.vodIframe.version = result;
                        break;
                    case TYPE.APP_LOGO:
                        data.appLogo.version = result;
                        break;
                    case TYPE.CUG_LOGO:
                        data.cugLogo.version = result;
                        break;
                }
            }
        });
    }

    function updateChannelIframeMap () {
        log.printDbg("updateChannelIframeMap()");

        fileManager.readText("/" + PATH_UNICAST_IMAGES + "/" + TYPE.CHANNEL_IFRAME + "/" + SHARE_CHANNEL_IMFRAME_MAP , function (is_success, result){
            log.printDbg('fileManager.readText() callback is_success : ' + is_success + ' , result : ' + result);

            if(is_success === true) {
                var iframeMap = null;

                try {
                    var obj = JSON.parse(result);
                    iframeMap = obj.iframeMap;
                }
                catch (e) {
                    log.printErr(e.message);
                }

                data.channelIframe.iframeMap = iframeMap;
            }
        });
    }


    return {
        init: _init,
        checkUpdate: _checkUpdate,

        getBiLogo: function () {
            return data.biLogo;
        },
        getChannelIframeInfo: function () {
            return data.channelIframe;
        },
        getVodIframeInfo: function () {
            return data.vodIframe;
        },
        getAppLogoInfo: function () {
            return data.appLogo;
        },
        getCugLogoInfo: function () {
            return data.cugLogo;
        }
    };
})();