"use strict";

KTW.managers.data.fileManager = (function() {
	
	//alias
	var log = KTW.utils.Log;
	var storageManager = KTW.managers.StorageManager;
	
	var ERROR_CODE = {};
	ERROR_CODE[FileError.QUOTA_EXCEEDED_ERR] = "QUOTA_EXCEEDED_ERR";
	ERROR_CODE[FileError.NOT_FOUND_ERR] = "NOT_FOUND_ERR";
	ERROR_CODE[FileError.SECURITY_ERR] = "SECURITY_ERR";
	ERROR_CODE[FileError.INVALID_MODIFICATION_ERR] = "INVALID_MODIFICATION_ERR";
	ERROR_CODE[FileError.INVALID_STATE_ERR] = "INVALID_STATE_ERR";
	
	window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
	
	function getFsSize() {
        // app.properties 관련 내용 삭제
        // KTW.APP_PROPERTY.FILE_SYSTEM_QUOTA_SIZE 사용하는 것으로 수정
        return KTW.APP_PROPERTY.FILE_SYSTEM_QUOTA_SIZE * 1024 * 1024;
    }
	
	function errorHandlerFactory(_callback) {
		var callback = _callback;
		return function(e) {
			
			try {
				var msg = ERROR_CODE[e.code];
				if(!msg) {
					msg = "Unknown Error";
				}
				
				log.printErr("[FileManager] error code:"+e.code+" msg:"+msg);
			} catch (e) {
				log.printExec(e);
			}
			
			if(callback) {
				callback(false, msg);
			}
		};
	}
	
	function _isExistDirectory(directory, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			log.printDbg("[FileManager] _isExistDirectory directory:" + directory);
			fs.root.getDirectory(directory, {}, function(dir_entry) {
				
				log.printDbg("[FileManager] _isExistDirectory exist " + dir_entry.toURL() + " dir_entry.isDirectory:" + dir_entry.isDirectory);
				if(dir_entry.isDirectory) {
					callback(true);
				} else {
					callback(false);
				}
				
			}, errorHandler);
			
		}, errorHandler);
	}
	
	function _deleteDirectory(directory) {
		var errorHandler = errorHandlerFactory();
		
		getFileSystem(function(fs) {
			
			log.printDbg("[FileManager] deleteDirectory directory:" + directory);
			fs.root.getDirectory(directory, {}, function(dir_entry){
				dir_entry.removeRecursively(function() {
					log.printDbg("[FileManager] Directory removed.");
			    }, errorHandler);
			}, errorHandler);
			
		}, errorHandler);
	}
	
	function _createDirectory(directory, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs){
			
			log.printDbg("[FileManager] _createDirectory directory:" + directory);
			fs.root.getDirectory(directory, {create:true}, function(dir_entry){
				log.printDbg("[FileManager] _createDirectory create success " + fs.root.toURL());
				callback(true);
			}, errorHandler);
			
		}, errorHandler);
	}
	
	function createDir(dir_entry, folders, count, errorHandler, returnSuccess) {
		if (folders[0] == '.' || folders[0] == '') {
			folders = folders.slice(1);
		}
		
		if(folders[0] !== undefined) {
			dir_entry.getDirectory(folders[0], {create: true}, function(dir_entry_sub) {
				//Recursively add the new subfolder (if we still have another to create).
				log.printDbg("[FileManager] createDir create success folders[0]:" + folders[0]);
				
				if (folders.length) {
					createDir(dir_entry_sub, folders.slice(1), count++, errorHandler, returnSuccess);
					
					//모든 createDir 이 끝난 후에 불림.
					if(returnSuccess)
						returnSuccess();
			    }
			  }, errorHandler);
		}
	};
	
	function stringToBinary(response) {
		var byte_array = new Uint8Array(response.length);
		for (var i = 0; i < response.length; i++) {
			byte_array[i] = response.charCodeAt(i) & 0xff;
		}
		return byte_array
	}
	
	var ZIP_EXTRACTING_STARTED = 0;
	var ZIP_EXTRACTING_SUCCEEDED = 1;
	var ZIP_EXTRACTING_CANCELED = 2;
	var ZIP_EXTRACTING_FAILED = 3;

	function _removeZip(zip_path) {
		_removeFile(zip_path,function(is_success, message) {
			if(!is_success) {
				log.printWarn("[FileManager] remove fail zip_path:" + zip_path);
			} else {
				log.printWarn("[FileManager] remove success");
			}
		});
	}
	
	/**
	 * web api 의 path 는 상대 경로이다. (브라우저의 내부 저장소가 루트.) 
	 * zip path 는 절대 경로이다. 
	 */
	function _unzip (zip_path, directory, filename, callback) {
		log.printDbg("[FileManager] zip_path:" + zip_path + " zip filename:" + filename);
				
		if (zip_path.lastIndexOf(".zip") != -1) {
			var dest_path = zip_path.substring(0,zip_path.lastIndexOf(".zip"));
			
			/**
			 * zip file 이 있는 같은 폴더엔 안풀림.
			 */
            var tmpZipPath = decodeURIComponent(zip_path);
            var tmpDestDirPath = decodeURIComponent(dest_path);
			KTW.oipf.AdapterHandler.zipAdapter.requestExtraction(tmpZipPath, tmpDestDirPath, function(handle, state) {
				log.printDbg("[FileManager] state:" + state);
				switch(state) {
					case ZIP_EXTRACTING_STARTED:
						log.printDbg("[ZIP_EXTRACTING_STARTED]");
						break;
						
					case ZIP_EXTRACTING_SUCCEEDED:
						log.printDbg("[ZIP_EXTRACTING_SUCCEEDED]");
						
						_removeZip(filename);
						
						_listFiles(directory, function(is_success, entry) {
                            /**
                             * [dj.son] unzip 시 entry 의 개수만 확인하고, 실제로 모든 파일이 다 있는지는 호출하는 쪽에서 체크
                             */
							if (is_success && entry && entry.length > 0) {
                                callback(true, entry);
							}
                            else {
								callback(false, "list files fail");
							}
						});
						break;
					case ZIP_EXTRACTING_CANCELED:
					case ZIP_EXTRACTING_FAILED:
						log.printDbg("[ZIP_EXTRACTING_CANCELED/ZIP_EXTRACTING_FAILED]");
						//실패
						_removeZip(filename);
						
						callback(false, "zip fail");
						break;
				}//switch
			});
		} else {
			_removeZip(filename);
			
			callback(false, "file path error");
		}
	}
	
	function _saveZipFile (directory, xhr, callback) {
		log.printDbg("[FileManager] _saveZipFile " + Date.now());
		
		//var blob = new Blob([ stringToBinary(xhr.responseText) ], { type: 'application/zip'});
        var blob = new Blob([ stringToBinary(xhr.responseText) ]);
		
		var filename = directory + ".zip";

        log.printDbg("[FileManager] filename :  " + filename);

		/**
		 * 압축해제가 zip file 과 같은 dir 이면 안되어 
		 * filename 을 dir 이름으로 만듬.
		 */
		_write(filename, blob, function(is_success, msg, paths) {
			if (is_success) {
				//zip extract 하기 전에 일단 여기서 stop. 
				log.printWarn("[FileManager] success paths:" + log.stringify(paths));
				
				var zip_path;
				var zip_original_path;
				_getUserDataPath(function(fs_root_path) {
					function onUnzipCallback(is_success, entry) {
						if(is_success) {
							var dest_path = zip_path.substring(0, zip_path.lastIndexOf(".zip"));
							var dest_original_path = zip_original_path.substring(0, zip_original_path.lastIndexOf(".zip"));
							
							var paths = {
								path: dest_path + "/",
								original : {
									path: dest_original_path + "/"
								}
							};
								
							callback(true, entry, paths);
						} else {
							callback(false, entry);
						}
					};
					
					zip_path = fs_root_path + paths.path;
					zip_original_path = fs_root_path + paths.original.path;
					$.get(zip_original_path, function(result) {
						_unzip(zip_path, directory, filename, onUnzipCallback);
					}).fail(function() {
						zip_path = fs_root_path + paths.path_dev;
						zip_original_path = fs_root_path + paths.original.path_dev;
						
						$.get(zip_original_path, function(result) {
							_unzip(zip_path, directory, filename, onUnzipCallback);
						}).fail(function() {
							callback(false);
						});
					});
				});
			}
            else {
				log.printWarn("[FileManager] saveZip fail write file msg:" + msg);
				callback(false, msg);
			}
		});
	}

	
	// 설치시 위치 "data/altibrowser/Master/dvb.appId%3A4e30.3001.widget/3.0.1/index.html
	// USB 개발 위치 data/altibrowser/Slave/default/filesystem/file__mnt_usb1_kthome/3.0.1/index.html 
	// 웹으로는 처리 안되는듯. 읽는것도 안됨.
	function removeDupCode(path) {
		path = decodeURIComponent(path);
		if(path.indexOf("%25") != -1) {
			return decodePath(path);
		} else {
			return path;
		}
	}
	
	function parseLocalPath(file_path, fs_root_path) {
		var delemeter = "filesystem://";
		var mid_path = "";
		var original_path = "";
		
		if(fs_root_path === undefined) {
			fs_root_path = "";
		}
		
		if(file_path.indexOf(delemeter) != -1) {
			original_path = file_path.substring(file_path.indexOf(delemeter) + delemeter.length);
		} else {
			delemeter = "Master/filesystem/";
			if(file_path.indexOf(delemeter) != -1) {
				original_path = file_path.substring(file_path.indexOf(delemeter) + delemeter.length);
			} else {
				delemeter = "Slave/default/filesystem/";
				if(file_path.indexOf(delemeter) != -1) {
					original_path = file_path.substring(file_path.indexOf(delemeter) + delemeter.length);
				} else {
					return null;
				}
			}
		}
		 
		log.printDbg("parseLocalPath file_path:" + file_path);
		log.printDbg("parseLocalPath original_path:" + original_path);
		
		//try {
		//	//2번해야 되야 하는 경우가 있다.
		//	//%253A 인경우 -> %25 가 % 가 되어 %3A 로 변환되어 다시 : 로 변환되어야 한다.
		//	//removeDupCode 는 %25 를 지우는 코드.
		//	//mid_path = decodeURIComponent(original_path);
         //   mid_path = original_path;
		//} catch (e) {
		//	mid_path = original_path;
		//}

        mid_path = original_path;
		
		//dvb.appId :   4e30.3001.widget
		//dvb.appId %3A 4e30.3001.widget <-urlEncoding 필요.
		
		return {
			original : {
				path: fs_root_path + "Master/filesystem/" + original_path,
				path_dev: fs_root_path + "Slave/default/filesystem/" + original_path
			},
			path: fs_root_path + "Master/filesystem/" + mid_path,
			path_dev: fs_root_path + "Slave/default/filesystem/" + mid_path
		};
	}
	
	/**
	 * callback 으로 성공 noti 감.
	 * 만들고자 한 주소의 parent path 가 감.
	 * 하위 path 는 success 면 붙여 쓰세요.
	 * 
	 * callback 은 callback(is_success, message, parent path obj, maked dir); 
	 */
	function _mkSubDir(dir_entry, dirs, errorHandler, callback) {
		if (dirs[0] == '.' || dirs[0] == '') {
			dirs = dirs.slice(1);
		}
		
		if(dirs[0] !== undefined) {
			dir_entry.getDirectory(dirs[0], {create: true}, function(dir_entry_sub) {
				//Recursively add the new subfolder (if we still have another to create).
				log.printDbg("[FileManager] createDir create success dirs[0]:" + dirs[0]);
				
				function returnSuccess() {
					if(callback) {
						var path_obj = parseLocalPath(dir_entry.toURL());
						log.printDbg("[FileManager] on mkdir done:" + path_obj);
						
			    		callback(true, "", path_obj, dirs);
			    		callback = null;
			    	}
				}
				
				if (dirs.length) {
					createDir(dir_entry_sub, dirs.slice(1), 0, errorHandler, returnSuccess);
			    } 
				
				//저 안에서 불렸다면 callback 이 null 일테고 아니라면 여기서 호출함.
				returnSuccess();
				
			  }, errorHandler);
		}
	}
	
	/**
	 * "path/path1/path2 와 같이 전달되어야 하며 
	 * filename 이 오면 안됨 file name 도 폴더명이라 판단함.
	 */
	function _mkDir(dir, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			log.printDbg("[FileManager] _mkDir directory:" + dir);
			
			_mkSubDir(fs.root, dir.split("/"), errorHandler, callback);
		}, errorHandler);
		
		
	}
	
	/**
	 * blob = new Blob([ stringToBinary(data) ]); xhr 을 통해 받은 data mimeType text/plain; charset=x-user-defined
	 * blob = new Blob([ "ddddddd" ]); string 저장.
	 * 이런 형태.. 형태가 다양 할 수 있어. blob 을 parameter 로 받음.
	 * 
	 * callback (is_success, error_msg, path) 
	 */
	function _write(filename, blob, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			fs.root.getFile(filename, {'create': true}, function(file_entry) {
				file_entry.createWriter(function(file_writer) {
					var current_file_url = file_entry.toURL();
					
					log.printDbg("[FileManager] create file: " + filename);
										
					file_writer.onwriteend = function(event) {
						log.printDbg("[FileManager] onwriteend event:" + event + " current_file_url:" + current_file_url);
						
						if(callback) {
							callback(true, undefined, parseLocalPath(current_file_url));
						}
					}; //callback. 
										
					file_writer.onerror = function(e) {
						try {
							console.error('Write failed: ' + ERROR_CODE[e.target.error.code]);
						} catch (e) {
							console.error('Write failed');
						}
						
						//에러가 난 경우 response 없애자.
						//에러가 나도 onwriteend 는 간다.
						if(callback) {
							callback(false);
							callback = null;
						}
				    };
				    
					file_writer.write(blob);
					
				}, errorHandler); //createWriter
			}); //getFile
		}, errorHandler);
	}
	
	function _writeText(filename, text, is_append, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			fs.root.getFile(filename, {'create': (is_append == true) ? false : true}, function(file_entry) {
				file_entry.createWriter(function(file_writer) {
					var blob;
					var current_file_url = file_entry.toURL();
					
					log.printDbg("[FileManager] create file: " + filename);
					
					file_writer.onwriteend = function(event) {
						log.printDbg("[FileManager] onwriteend event:" + event + " current_file_url:" + current_file_url);
						
						if(callback) {
							callback(true, undefined);
						}
					}; //callback.
					
					file_writer.onerror = function(event) {
						log.printDbg("[FileManager] onerror event:" + event + " current_file_url:" + current_file_url);
				        if(callback) {
							callback(true, event);
						}
				    };
										
					if(is_append == true) {
						file_writer.seek(file_writer.length);
					}
					
					file_writer.write(new Blob([ text ]));
				}, errorHandler); //createWriter
			}); //getFile
		}, errorHandler);
	}
	
	function _readText(filename, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			fs.root.getFile(filename, {}, function(file_entry) {
				file_entry.file(function(file) {
					var reader = new FileReader();

			        reader.onloadend = function(event) {
			        	callback(true, this.result);
			        };

			        reader.readAsText(file); // Read the file as plaintext.
			    }, errorHandler);
			}, errorHandler); //getFile
		}, errorHandler); //requestFileSystem
	}
	
	function _removeFile(filename, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			fs.root.getFile(filename, {}, function(file_entry) {
				file_entry.remove(function() {
                    if (callback) {
                        callback(true);
                    }
				}, errorHandler);
			}, errorHandler); //getFile

		}, errorHandler); //requestFileSystem
	}
	
	//이거 너무 일찍 부르면 이상한 값이 올라옴.
	function _getUserDataPath(callback) {
		$.get('/altibrowser/altibrowser_ktsmart_app.cfg', function(result) {
			var cfg = JSON.parse(result);
			var fs_root_path = cfg.generic.UserDataPath;
			
			log.printDbg("[FileManager] fs_root_path:" + fs_root_path);
			
			if(callback)
				callback(fs_root_path);
		}).fail(function() {
			if(callback)
				callback(null);
		});
	}
	
	function _checkFile(path, callback) {
		$.ajax({
			url : path
//			async: false
		})
		.done(function() {
			if (callback != null) {
				callback(true);
			}
		})
		.fail(function() {
			if (callback != null) {
				callback(false);
			}
		});
	}
	
	function toArray(list) {
		return Array.prototype.slice.call(list || [], 0);
	}
	
	/**
	 * directory: directory path. window 내부 path 가 root 로 간주되니 거기서부터 상대 패스
	 * callback(is_success, entry) entry 에 fileEntry 객체가 리턴됨.
	 */
	function _listFiles(directory, callback) {
		var errorHandler = errorHandlerFactory(callback);
		
		getFileSystem(function(fs) {
			fs.root.getDirectory(directory, {}, function(dir_entry) {
				var dirReader = dir_entry.createReader();
				
				var entries = [];
				var readEntries = function() {
					dirReader.readEntries(function(results) {
						if (!results.length) {
							callback(true, entries.sort());
						} else {
							entries = entries.concat(toArray(results));
							readEntries();
						}
					}, errorHandler);
				};
				readEntries();
			}, errorHandler);
		}, errorHandler); //requestFileSystem
	}
	
	var file_system;
	function getFileSystem(callback, errorHandler) {
		if(!file_system) {
			window.requestFileSystem(window.PERSISTENT, getFsSize(), function(fs) {
				file_system = fs;
				callback(fs);
			}, errorHandler);
		} else {
			callback(file_system);
		}
		/*window.requestFileSystem(window.PERSISTENT, getFsSize(), function(fs) {
			file_system = fs;
			callback(fs);
		}, errorHandler);*/
	}
	
	return {
		isExistDirectory: _isExistDirectory, 
		deleteDirectory: _deleteDirectory,
		createDirectory: _createDirectory,
		saveZipFile: _saveZipFile,
		
		//common api. 
		mkDir: _mkDir,
		write: _write,
		writeText: _writeText,
		readText: _readText,
		removeFile: _removeFile,
		
		getUserDataPath:_getUserDataPath,
		checkFile: _checkFile,
		
		listFiles:_listFiles
	}
}());
