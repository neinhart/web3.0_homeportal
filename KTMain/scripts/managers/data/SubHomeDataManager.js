"use strict";

/**
 * Web 3.0 SubHome 데이터 관리 매니저 (event, Mess 추천, subhome, filter 등등)
 *
 * TODO OCManager 에 SubHome data 부분 추가해야 함
 */
KTW.managers.data.SubHomeDataManager = (function() {

    var addSettingRecommendMenu = null;

    var subHomeVersion = null;
    // SUBHOME_ITEM_TYPE 별 데이터 array
    // 첫번째 키 값은 PARENT_CAT_ID 이고, 두번째 키 값은 CAT_ID 이며 만약 CAT_ID 가 null 일 경우는 KTW.DATA.SUBHOME.NULL_ID 값을 키 값으로 함
    // ex: arrHomeShot[parentCatId].list[catId] == [{...}, {...}, {...}, ....];
    var arrHomeShot = null;
    var arrEvent = null;
    var arrRecommend = null;
    var arrMyMenu = null;
    // Home Menu 의 sub data object
    // 위 데이터 array 에 그냥 넣을 수도 있으나, 하위 메뉴 Layer 에서 잘못 참조할 수도 있으므로 따로 구성함
    var homeMenuData = null;

    // Filter 버전 및 데이터 array
    // 키 값을 CAT_ID 로 가짐
    var filterVersion = null;
    var arrFilter = null;
    /** 2017.05.11 dhlee subhome 데이터가 변경되었을 때 getCustEnv() 를 호출 해야 한다. */
    var requestCustEnv = false;

    function decodeData (str) {
        var result = str;

        try {
            if (str) {
                result = decodeURIComponent(str);
            }
        }
        catch (e) {
            result = str;
        }

        return result;

    };

    /**
     * add homeshot data
     *
     * @param args
     */
    function addHomeShot (args) {
        var obj = {
            subHomeItemType: args[0],
            parentCatId: args[1],
            catId: args[2],
            catGb: args[3],
            itemType: args[4],
            itemId: args[5],
            imgUrl: args[6],
            locator: args[7],
            locator2: args[8],
            casLocator: args[9],
            parameter: args[10]
        };

        if (!arrHomeShot[obj.parentCatId]) {
            arrHomeShot[obj.parentCatId] = {
                list: []
            };
        }

        var catId = !obj.catId ? KTW.DATA.SUBHOME.NULL_ID : obj.catId;

        if (!arrHomeShot[obj.parentCatId].list[catId]) {
            arrHomeShot[obj.parentCatId].list[catId] = [];
        }

        arrHomeShot[obj.parentCatId].list[catId].push(obj);
    }

    /**
     * add event data
     * - 홈메뉴 좌측 이벤트 데이터는 따로 구성
     *
     * @param args
     */
    function addEvent (args) {
        var obj = {
            subHomeItemType: args[0],
            parentCatId: args[1],
            catId: args[2],
            catGb: args[3],
            itemType: args[4],
            itemId: args[5],
            imgUrl: args[6],
            locator: args[7],
            locator2: args[8],
            casLocator: args[9],
            parameter: args[10],
            itemName: decodeData(args[11]),
            position: args[12],
            templateType: args[13],
            templateLoc: args[14]
        };

        if (obj.position == KTW.DATA.SUBHOME.POSITION.HOMEMENU_LEFT) {
            if (!homeMenuData.arrEvent[obj.catId]) {
                homeMenuData.arrEvent[obj.catId] = [];
            }

            homeMenuData.arrEvent[obj.catId].push(obj);
        }
        else {
            if (!arrEvent[obj.parentCatId]) {
                arrEvent[obj.parentCatId] = {
                    list: []
                };
            }

            var catId = !obj.catId ? KTW.DATA.SUBHOME.NULL_ID : obj.catId;

            if (!arrEvent[obj.parentCatId].list[catId]) {
                arrEvent[obj.parentCatId].list[catId] = [];
            }

            arrEvent[obj.parentCatId].list[catId].push(obj);
        }
    };

    /**
     * add recommend data
     * - 홈메뉴 좌측 추천 데이터는 따로 구성
     *
     * @param args
     */
    function addRecommend (args) {
        var obj = {
            subHomeItemType: args[0],
            parentCatId: args[1],
            catId: args[2],
            catGb: args[3],
            itemType: args[4],
            itemId: args[5],
            imgUrl: args[6],
            locator: args[7],
            locator2: args[8],
            casLocator: args[9],
            parameter: args[10],
            itemName: decodeData(args[11]),
            position: args[12],
            recomTitle: decodeData(args[13]),
            newHot: args[14],
            prInfo: args[15],
            wonYn: args[16],
            mark: args[17],
            isHdrYn: args[18],
            isDvdYn: args[19],
            resolCd: args[20],
            imgType: args[21]
        };

        if (obj.position == KTW.DATA.SUBHOME.POSITION.HOMEMENU_LEFT) {
            if (!homeMenuData.arrRecommend[obj.catId]) {
                homeMenuData.arrRecommend[obj.catId] = [];
            }

            homeMenuData.arrRecommend[obj.catId].push(obj);
        }
        else {
            if (!arrRecommend[obj.parentCatId]) {
                arrRecommend[obj.parentCatId] = {
                    list: []
                };
            }

            var catId = !obj.catId ? KTW.DATA.SUBHOME.NULL_ID : obj.catId;

            if (!arrRecommend[obj.parentCatId].list[catId]) {
                arrRecommend[obj.parentCatId].list[catId] = [];
            }

            arrRecommend[obj.parentCatId].list[catId].push(obj);
        }
    };

    /**
     * add mymenu data - 우리집 맞춤 TV 기타 편성 정보
     *
     * @param args
     *
     * TODO 데이터 구조상 우리집 맞춤 TV 기타 편성정보는 단 한줄로 내려올것 같음...
     */
    function addMyMenu (args) {
        var obj = {
            subHomeItemType: args[0],
            parentCatId: args[1],
            catId: args[2],
            catGb: args[3],
            itemType: args[4],
            itemId: args[5],
            imgUrl: args[6],
            locator: args[7],
            locator2: args[8],
            casLocator: args[9],
            parameter: args[10],
            itemName: decodeData(args[11]),
            position: args[12]
        };

        arrMyMenu.push(obj);
    };


    /**
     * init subhome
     */
    function _initSubHome () {
        subHomeVersion = null;
        arrHomeShot = [];
        arrEvent = [];
        arrRecommend = [];
        arrMyMenu = [];

        homeMenuData = {
            arrEvent: [],
            arrRecommend: [],
            arrSetting: null,
            arrChannelGuide: null,
            arrMyMenu: null
        };
    }

    /**
     * init filter
     */
    function _initFilter () {
        filterVersion = null;
        arrFilter = [];
    }

    function initHomeMenuData () {

        homeMenuData.arrChannelGuide = [];
        var tempCount = 0;
        KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
            cbCondition: function (tmpMenu) {
                var index = -1;
                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                    index = 0;
                    tempCount++;
                }
                else if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                    index = 1;
                    tempCount++;
                }
                else if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                    index = 2;
                    tempCount++;
                }
                else if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.WATCH_4CHANNEL) {
                    index = 3;
                    tempCount++;
                }

                if (index > -1) {
                    homeMenuData.arrChannelGuide[index] = {
                        menuId: tmpMenu.id,
                        itemName: tmpMenu.name,
                        itemSubText: tmpMenu.parent.name,
                        itemEnglishName: tmpMenu.englishItemName,
                        itemEnglishSubText: tmpMenu.parent.englishItemSubText
                    }
                }

                if (tempCount >= 4) {
                    return true;
                }
            }
        });


        homeMenuData.arrSetting = [];
        // 비밀번호 변경 / 초기화
        homeMenuData.arrSetting.push({
            menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CHANGE_PIN,
            itemName: "비밀번호 변경/초기화",
            itemSubText: "자녀안심 설정",
            itemEnglishName: "Manage PIN",
            itemEnglishSubText: "Parental Control"
        });
        // HDMI 전원 동기화
        homeMenuData.arrSetting.push({
            menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_HDMI_CEC,
            itemName: "HDMI 전원 동기화",
            itemSubText: "시스템 설정",
            itemEnglishName: "HDMI-CEC",
            itemEnglishSubText: "System Setting"
        });
        // 블루투스 장치 연결
        homeMenuData.arrSetting.push({
            menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BLUETOOTH,
            itemName: "블루투스 장치 연결",
            itemSubText: "시스템 설정",
            itemEnglishName: "Bluetooth",
            itemEnglishSubText: "System Setting"
        });

        /**
         * [dj.son] 추가 요구사항에 따라 우리집 맞춤 TV 추천 데이터는 따로 생성
         */
        homeMenuData.arrMyMenu = [];
        tempCount = 0;
        KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
            cbCondition: function (tmpMenu) {
                var index = -1;
                var name = tmpMenu.name;
                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.RECOMMEND_VOD) {
                    // 당신을 위한 VOD 추천
                    index = 0;
                    name = "당신을 위한\nVOD 추천";
                }
                else if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.FAVORITE_MENU) {
                    // 즐겨 찾는 메뉴
                    index = 1;
                    name = "즐겨찾는\n메뉴";
                }
                else if (!tmpMenu.parent && tmpMenu.catType === KTW.DATA.MENU.CAT.SRCH) {
                    // 검색
                    index = 2;
                }
                else if (!tmpMenu.parent && tmpMenu.catType === KTW.DATA.MENU.CAT.CONFIG) {
                    // 설정
                    index = 3;
                }

                if (index > -1) {
                    homeMenuData.arrMyMenu[index] = {
                        menuId: tmpMenu.id,
                        itemName: name,
                        itemSubText: tmpMenu.itemSubText,
                        itemEnglishName: tmpMenu.englishItemName,
                        itemEnglishSubText: tmpMenu.englishItemSubText
                    };

                    tempCount++;
                }

                if (tempCount >= 4) {
                    return true;
                }
            }
        })
    }

    /**
     * set current subhome version
     *
     * @param version
     */
    function _setSubHomeVersion (version) {
        if (subHomeVersion === null) {
            requestCustEnv = false;
        } else if (version !== subHomeVersion) {
            requestCustEnv = true;
        } else {
            requestCustEnv = false;
        }
        subHomeVersion = version;
    }

    /**
     * set current filter version
     *
     * @param version
     */
    function _setFilterVersion (version) {
        filterVersion = version;
    }

    /**
     * add subhome data - subHomeItemType 별로 데이터 구성
     */
    function _addSubHome () {
        try {
            if (arguments[0] == KTW.DATA.SUBHOME.TYPE.HOMESHOT) {
                addHomeShot(arguments);
            }
            else if (arguments[0] == KTW.DATA.SUBHOME.TYPE.EVENT) {
                addEvent(arguments);
            }
            else if (arguments[0] == KTW.DATA.SUBHOME.TYPE.RECOM) {
                addRecommend(arguments);
            }
            else if (arguments[0] == KTW.DATA.SUBHOME.TYPE.MYMENU) {
                addMyMenu(arguments);
            }
        }
        catch (e) {
        }
    }

    /**
     * add filter data - catId 별로 리스트 구성
     */
    function _addFilter (catId, catGb, filterCd, filterText, filterOptionCdList, filterOptionTextList) {
        try {
            if (!arrFilter[catId]) {
                arrFilter[catId] = [];
            }

            arrFilter[catId].push({
                catId: catId,
                catGb: catGb,
                filterCd: filterCd,
                filterText: filterText,
                filterOptionCdList: filterOptionCdList.split(","),
                filterOptionTextList: filterOptionTextList.split(",")
            });
        }
        catch (e) {}
    }

    /**
     * 홈메뉴-세팅 좌측 추천 데이터 중 첫번째 데이터는 편성 데이터 (catType 이 MyMenu2 인 메뉴의 첫번째 자식 메뉴)
     * -> MenuDataManager.initMenuData 에서 호출함
     *
     * @param menu
     * @private
     */
    function _addSettingMenuRecommendData (menu) {
        addSettingRecommendMenu = menu;
    }

    /**
     * get subhome version
     */
    function _getSubHomeVersion () {
        return subHomeVersion;
    }

    /**
     * get filter version
     */
    function _getFilterVersion () {
        return filterVersion;
    }

    /**
     * get homeshot
     * - parentCatId, catId 에 해당하는 홈샷 리스트 리턴
     * - catId 가 존재하지 않을 경우 parentCatId 의 전체 홈샷 리스트 리턴
     * - 데이터가 없으면 undefined 가 리턴됨
     *
     * @param parentCatId
     * @param catId
     * @returns {*}
     */
    function _getHomeShot (parentCatId, catId) {
        if (arrHomeShot[parentCatId]) {
            if (!catId) {
                return arrHomeShot[parentCatId];
            }
            else {
                return arrHomeShot[parentCatId].list[catId];
            }
        }
    }

    /**
     * get event
     * - parentCatId, catId 에 해당하는 이벤트 리스트 리턴
     * - catId 가 존재하지 않을 경우 parentCatId 의 전체 이벤트 리스트 리턴
     * - 데이터가 없으면 undefined 가 리턴됨
     *
     * @param parentCatId
     * @param catId
     * @returns {*}
     */
    function _getEvent (parentCatId, catId) {
        if (arrEvent[parentCatId]) {
            if (!catId) {
                return arrEvent[parentCatId];
            }
            else {
                return arrEvent[parentCatId].list[catId];
            }
        }
    }

    /**
     * get recommend
     * - parentCatId, catId 에 해당하는 추천 리스트 리턴
     * - catId 가 존재하지 않을 경우 parentCatId 의 전체 추천 리스트 리턴
     * - 데이터가 없으면 undefined 가 리턴됨
     *
     * @param parentCatId
     * @param catId
     * @returns {*}
     */
    function _getRecommend (parentCatId, catId) {
        if (arrRecommend[parentCatId]) {
            if (!catId) {
                return arrRecommend[parentCatId];
            }
            else {
                return arrRecommend[parentCatId].list[catId];
            }
        }
    }

    /**
     * get mymenu
     *
     * @returns {*}
     */
    function _getMyMenu () {
        return arrMyMenu;
    }

    /**
     * get all subhome data
     *
     * @param parentCatId
     * @returns {{}}
     */
    function _getSubHome (parentCatId, catId) {
        var obj = {};

        obj.homeShot = _getHomeShot(parentCatId, catId);
        obj.event = _getEvent(parentCatId, catId);
        obj.recommend = _getRecommend(parentCatId, catId);
        if (parentCatId == "VC") {
            obj.arrMyMenu = _getMyMenu();
        }

        // 2017.05.11 dhlee
        // requestCutEnv flag가 set 되어 있는 경우 getCustEnv()를 호출해야 한다.
        if (requestCustEnv) {
            requestCustEnv = false;
            setTimeout(function () {
                KTW.managers.service.AppServiceManager.requestCustInfo(null);
            }, 0);
        }

        return obj;
    }

    function _getHomeMenuData () {

        if (!homeMenuData.arrSetting || !homeMenuData.arrChannelGuide) {
            initHomeMenuData();
        }

        if (addSettingRecommendMenu && homeMenuData.arrSetting.length < 4) {
            homeMenuData.arrSetting.unshift({
                menuId: addSettingRecommendMenu.id,
                itemName: addSettingRecommendMenu.name,
                itemSubText: addSettingRecommendMenu.parent.name,
                itemEnglishName: addSettingRecommendMenu.englishItemName,
                itemEnglishSubText: addSettingRecommendMenu.parent.englishItemName,
                itemType: addSettingRecommendMenu.itemType,
                locator: addSettingRecommendMenu.locator,
                parameter: addSettingRecommendMenu.parameter
            })
        }

        return homeMenuData;
    }

    /**
     * get filter
     * - catId 에 해당하는 필터 리스트 리턴, 데이터가 없으면 undefined 가 리턴됨
     *
     * @param catId
     * @returns {*}
     */
    function _getFilter (catId) {
        return arrFilter[catId];
    }

    return {
        // init data
        initSubHome: _initSubHome,
        initFilter: _initFilter,

        // set version
        setSubHomeVersion: _setSubHomeVersion,
        setFilterVersion: _setFilterVersion,

        // add data
        addSubHome: _addSubHome,
        addFilter: _addFilter,
        addSettingMenuRecommendData: _addSettingMenuRecommendData,

        // get version
        getSubHomeVersion: _getSubHomeVersion,
        getFilterVersion: _getFilterVersion,

        // get subhome
        getHomeShot: _getHomeShot,
        getEvent: _getEvent,
        getRecommend: _getRecommend,
        getMyMenu: _getMyMenu,
        getSubHome: _getSubHome,
        getHomeMenuData: _getHomeMenuData,

        getTestData: function () {
            return {
                subHomeVersion: subHomeVersion,
                arrHomeShot: arrHomeShot,
                arrEvent: arrEvent,
                arrRecommend: arrRecommend,
                arrMyMenu: arrMyMenu,
                homeMenuData: homeMenuData,

                filterVersion: filterVersion,
                arrFilter: arrFilter
            };
        },

        // get filter
        getFilter: _getFilter
    };
})();