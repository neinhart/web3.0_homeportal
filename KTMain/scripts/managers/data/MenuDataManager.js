"use strict";

/**
 * 메뉴데이터 관리
 *
 * TODO 실제로 Web 3.0 버전 OC 데이터가 내려왔을때, 대조해보고 수정해야 함
 * TODO 추후 각 생성 메뉴의 영문명 확인해야 함
 */
KTW.managers.data.MenuDataManager = (function() {
    // [jh.lee] 참조
    var LOG_PREFIX = "MenuDataManager";
    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var ocManager = null;
    var amocManager = null;
    var storageManager = null;
    var hwAdapter = null;

    var DEF = KTW.oipf.Def;
    
    var menu_data_change_listeners = [];
    var notice_data_change_listeners = [];
    // [jh.lee] OC로 부터 받은 원본 데이터
    var menu_arr = null;
    // [jh.lee] 원본 홈메뉴 데이터
    var origin_data = null;
    // [jh.lee] 화면에 보여지는데 사용하는 홈메뉴 데이터
    var menu_data = null;

    // [jh.lee] 스카이 터치 메뉴 생성에 필요한 값
    var sky_touch_locator = null;
    // [jh.lee] 현재 메뉴데이터에 성인19메뉴 존재를 체크하는 변수
    //var removed_adult_cat = "false";
    var removed_adult_cat = false;
    // [jh.lee] menu version
    var menu_version = "";
    // bmt hidden list cache
    var hidden_list = null;
    var emergency_notice = undefined;
    
    var initialized_callback = null;
    
    // [jh.lee] 필수 리스너가 등록되어있는지 확인하는 변수
    var check_listener = false;
    // [jh.lee] OCUpdate로 인한 작업이라는 것을 인지할 수 있는 변수
    var passed_by_oc_update = false;
    
    // [jh.lee] 공지사항 데이터
    var notice_data;

    // 2017.06.14 dhlee 공지사항 버전
    var notice_version = null;

    //
    var adult_menu_data = null;
    var kids_menu_data = null;
    /** 2017.05.12 dhlee setting menu id */
    var setting_menu_id = null;
    var language = "kor";
    var displayAdult = true;

    var my_menu_id = null;

    /**
     * [dj.son] 키즈 리모컨 및 키즈 SubHome 고정 데이터 관련 메뉴
     */
    var kidsHotKeyMenu = null;
    var kidsSHDataMenu = null;
    
    // [jh.lee] Notice object
    function Notice() {
        this.id;
        this.title;
        this.link_type;
        this.link_data1;
        this.link_data2;
        this.start_time;
        this.end_time;
        this.product_code;
        this.desc;
    }

    /**
     * 2017.08.31 dhlee
     * 초기 필요한 설정 값을 읽어온다.
     */
    function initConfiguration() {
        log.printDbg("initConfiguration()");

        var lang = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.LANGUAGE);
        if (lang === "kor" || lang === "eng") {
            language = lang;
        }
        var adult = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY);
        if (adult) {
            displayAdult = (adult === "true");
        }
    }
    
    /**
     * [jh.lee] 메뉴데이터 초기화
     */
    function _initMenuData(callback) {
        log.printDbg("homeportal initMenuData start " + Date.now(), LOG_PREFIX);

        ocManager = KTW.managers.data.OCManager;
        amocManager = KTW.managers.http.amocManager;
        storageManager = KTW.managers.StorageManager;
        hwAdapter = KTW.oipf.adapters.HWAdapter;
        
        if (callback) {
            initConfiguration();
            initialized_callback = callback;
        }
        var temp_data;
         
        // [jh.lee] OC 로 부터 받은 데이터 menu_arr
        temp_data = new KTW.data.Menu();
        menu_arr = temp_data.getData();
        menu_version = temp_data.getVersion();
        temp_data = null;

        if (!KTW.CONSTANT.IS_UHD) {
            // [jh.lee] UHD 가 아닌 경우 서버에서 내려오는 UHD 데이터를 제거하기 위해 CAT UHD를 제거
            //uhd 전용관 cat 삭제.
            delete KTW.DATA.MENU.CAT.UHD;
        }

        if (!KTW.CONSTANT.IS_HDR) {
            // hdr 지원하지 않으면 hdr 전용관 cat 삭제
            delete KTW.DATA.MENU.CAT.HDR;
        }

        checkENotice();

        // 필요없는 메뉴를 삭제.(catType 이 MyMenu 인 것은 따로 저장)
        deleteMenu();

        // 우리집 맞춤 TV 메뉴 구성
        makeMyMenu();

        // Kids 메뉴 구성
        makeKidsMenu();

        // SettingMenu 메뉴 구성 및 menu_arr 에 추가
        makeSettingMenu();

        var search_menu = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                if (!menu.parent && menu.catType === KTW.DATA.MENU.CAT.SRCH) {
                    return true;
                }
            }
        })[0];
        if (!search_menu) {
            search_menu = new VMenu(KTW.managers.data.MenuDataManager.MENU_ID.SEARCH, "검색", null, 0, null, "Search");
            search_menu.catType = KTW.DATA.MENU.CAT.SRCH;
            menu_arr.push(search_menu);
        }

        search_menu.displayHidden = true;

        // 채널 가이드 메뉴 구성 및 menu_arr 에 추가
        makeChannelGuideMenu();

        // 추가 요구사항에 따라 메뉴 생성 및 추가
        // 웹브라우저 추가, Sky Choice 추가
        makeEtcMenu();

        /**
         * [dj.son] 요구사항에 따른 Etc 메뉴 check 및 생성 (전체 메뉴를 다 check 해야 하는 경우 해당 함수 사용
         *
         * - 유튜브 관련 메뉴 생성 (Youtube, YoutubeKids)
         * - 키즈 리모컨 및 키즈 SubHome 고정 데이터 관련 메뉴 따로 저장
         */
        checkEtcMenu();

        // BMT hidden 메뉴 추가
        // hidden_list 데이터가 없을 경우에만 amoc 로부터 hidden 메뉴 정보 받아옴
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LOCAL) {
            if (!hidden_list) {
                amocManager.getItemDetlListNxtW3(onResponseItemList, KTW.managers.data.MenuDataManager.MENU_ID.BMT_HIDDEN_MENU, 0, "A");
            }
            else {
                makeBmtHiddenMenu();
                createOriginalData();
            }
        }
        else {
            // [jh.lee] BMT가 아닌 경우는 OC 데이터를 이용하여 실제 사용할 메뉴를 구성한다.
            createOriginalData();
        }

        //var tmpLanguage = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.LANGUAGE);
        //if (tmpLanguage === "kor" || tmpLanguage === "eng") {
        //    language = tmpLanguage;
        //}
        
        if (check_listener === false) {
            // [jh.lee] false 일 경우 한번도 등록을 안한 경우이므로 필수 리스너 등록
            check_listener = true;
            // [jh.lee] oc data가 업데이트 되면 다시 initMenuData 호출
            ocManager.addOCUpdateListener(onOCUpdate);
            // [jh.lee] 메뉴데이터에 영향을 주는 스토리지 값이 변경되었을 경우 실행될 콜백등록
            // 2016.12.20 dhlee Configuration 사용하도록 변경
            //storageManager.ps.addStorageDataChangeListener(onChangeMenuData);
            KTW.oipf.AdapterHandler.basicAdapter.addConfigChangeEventListener(onChangeMenuData);
            // 2017.06.26 dhlee
            // CUG_INFO에 대한 storage key 값 변경에 대한 처리 추가
            storageManager.ps.addStorageDataChangeListener(onChangeStorageData);
        }
    }
    
    function onOCUpdate(source_url) {
        log.printDbg("is update MenuDataManager " + (source_url == ocManager.DATA.MENU.URL));
        
        if(source_url === undefined || source_url == ocManager.DATA.MENU.URL) {
            // [jh.lee] OC 에서 메뉴데이터가 업데이트 된 경우
            try {
                // [jh.lee] oc update 로 인해서만 쇼윈도우 콜백을 실행시켜 주기 위해 변수 추가
                passed_by_oc_update = true;
                log.printDbg("oc update start, passed by oc update : " + passed_by_oc_update);
                _initMenuData();       
            } catch (e) {
                log.printErr(e);
            }
        } else if (source_url === undefined || source_url === ocManager.DATA.NOTICE.URL) {
            // [jh.lee] OC 에서 공지사항 데이터가 업데이트 된 경우 (공지사항에서 긴급공지 데이터를 파싱한다)
            // [jh.lee] 긴급공지 데이터 파싱이 완료 되면 등록된 리스너 실행
            checkENotice();
            notifyNoticeDataChange();
        }
    }
    
    /**
     * [jh.lee] 긴급공지가 있는지 검사한다.
     */
    function checkENotice() {
        log.printDbg("checkENotice()", LOG_PREFIX);

        var data = [];
        var first_data = [];
        var total_count = 0;
        var notice_list = [];
        var item_count = 0;
        var origin_data = ocManager.getNotice();
        var tmp_data;
        var is_order = false;
        
        if (origin_data != null && origin_data != undefined) {
        	data = ocManager.getNotice().split("|");
            notice_version = data[0];
        	first_data = data[0].split("\n");
            total_count = parseInt(first_data[1]);
            notice_list = [total_count];
            //Item 1개의 갯수 data 마지막에 \n 이 포함됨.
            item_count = (data.length - 1) / total_count;
        }
        
        for (var notice_index = 0; notice_index < total_count; notice_index++) {
            var base_index = (notice_index * item_count);
            var item_index = 0;
            
            notice_list[notice_index] = new Notice();
            
            if (notice_index === 0) {
                notice_list[notice_index].id = first_data[2];
            }
            else {
                notice_list[notice_index].id = data[base_index + item_index];
            }
            
            notice_list[notice_index].title = data[base_index + (++item_index)];
            notice_list[notice_index].link_type = data[base_index + (++item_index)];
            notice_list[notice_index].link_data1 = data[base_index + (++item_index)];
            notice_list[notice_index].link_data2 = data[base_index + (++item_index)];
            notice_list[notice_index].start_time = data[base_index + (++item_index)];
            notice_list[notice_index].end_time = data[base_index + (++item_index)];
            notice_list[notice_index].product_code = data[base_index + (++item_index)];
            
            // R5 관리UI 서버에서 셋탑박스코드(ALL/OTV/OTS) 는 무조건 내려주는 것으로 확인함.
            // 즉, 무조건 item_count 가 10개 이상이라는 것임.
            // item_count 가 11개라는 것은 다음과 같은 경우임...
            // Smart/UHD 구분 필드만 왔거나 공지사항 순서 필드만 온 경우임.
            // 해당 필드가 동시에 열릴 수 도 있지만 따로 열릴 수 도 있기 때문에 처리가 필요.
            // 만약 둘다 모두 오게되면 item_count 는 12개 임. 즉, 10, 11, 12 일 경우 처리가 필요.
            // 그리고 11개일 경우 2가지 케이스를 구분하기 위해 추가 처리가 필요
            log.printDbg("checkENotice item_count : " + item_count);
            
            if (item_count === 10) {
                // 셋탑박스코드(ALL/OTS/OTV) 까지만 내려옴
                notice_list[notice_index].biz_code = data[base_index + (++item_index)];
                notice_list[notice_index].stb_code = undefined;
                notice_list[notice_index].desc = data[base_index + (++item_index)];
                notice_list[notice_index].order = undefined;
            }
            else if (item_count === 11) {
                // Smart/UHD 또는 공지사항순서 필드만 내려온 경우
                notice_list[notice_index].biz_code = data[base_index + (++item_index)];
                
                // 셋탑박스코드 이후의 필드가 무엇인지 체크하기 위함
                // Smart/UHD 필드가 없는 경우이면 desc 값일 것이고 그다음은 공지사항 필드 값임.
                // Smart/UHD 필드가 있는 경우면 해당 필드 값이고 그 다음은 desc 값임. 공지사항 필드 값은 없음.
                tmp_data = data[base_index + (++item_index)];
                
                if (tmp_data === "ALL" || tmp_data === "UHD" || 
                        (tmp_data === "Smart" || tmp_data === "SMART")) {
                    // Smart/UHD 필드가 있는 경우
                    notice_list[notice_index].stb_code = tmp_data;
                    notice_list[notice_index].desc = data[base_index + (++item_index)];
                    notice_list[notice_index].order = undefined;
                } else {
                    // Smart/UHD 필드가 없는 경우
                    notice_list[notice_index].stb_code = undefined;
                    notice_list[notice_index].desc = tmp_data;
                    notice_list[notice_index].order = data[base_index + (++item_index)];
                    is_order = true;
                }
            } else if (item_count === 12) {
                // Smart/UHD, 공지사항순서 필드가 모두 내려온 경우
                notice_list[notice_index].biz_code = data[base_index + (++item_index)];
                notice_list[notice_index].stb_code = data[base_index + (++item_index)];
                notice_list[notice_index].desc = data[base_index + (++item_index)];
                notice_list[notice_index].order = data[base_index + (++item_index)];
                is_order = true;
            }
        }
        
        //표시날짜가 아닌것을 뺀다.
        var curDate = util.getCurrentDateTime();
        curDate = curDate.substring(0, 12);
        
        var isOts = KTW.CONSTANT.IS_OTS;
        var isOtv = !KTW.CONSTANT.IS_OTS;
        var isUHD = KTW.CONSTANT.IS_UHD;
        var isSmart = !KTW.CONSTANT.IS_UHD;

        /**
         * [dj.son] STB 별로 공지사항 노출할 수 있도록 수정
         */
        var stbType = null;
        if (KTW.CONSTANT.STB_TYPE.ANDROID) {
            stbType = "기가지니";
        }
        else {
            if (isOts) {
                if (KTW.CONSTANT.IS_DCS) {
                    if (KTW.CONSTANT.STB_TYPE.UHD1) {
                        stbType = "UHD S1-DCS";
                    }
                    else if (KTW.CONSTANT.STB_TYPE.UHD2) {
                        stbType = "UHD S1-1DCS";
                    }
                }
                else {
                    if (KTW.CONSTANT.STB_TYPE.UHD1) {
                        stbType = "UHD S1";
                    }
                    else if (KTW.CONSTANT.STB_TYPE.UHD2) {
                        stbType = "UHD S1-1";
                    }
                }
            }
            else {
                if (KTW.CONSTANT.STB_TYPE.UHD1) {
                    stbType = "UHD 1";
                }
                else if (KTW.CONSTANT.STB_TYPE.UHD2) {
                    stbType = "UHD 2";
                }
                else if (KTW.CONSTANT.STB_TYPE.UHD3) {
                    stbType = "UHD 3";
                }
            }
        }
        for (var i = 0; i < notice_list.length; i++) {
            log.printDbg("noticeList biz_code = "+notice_list[i].biz_code+" stb_code = "+notice_list[i].stb_code);

            if (curDate >= notice_list[i].start_time && curDate <= notice_list[i].end_time) {
                if ((notice_list[i].biz_code === undefined || notice_list[i].biz_code === "ALL" ||
                    (isOts  && notice_list[i].biz_code === "OTS") || (isOtv  && notice_list[i].biz_code === "OTV"))) {

                    if (notice_list[i].stb_code === undefined || notice_list[i].stb_code === "ALL" || (stbType && notice_list[i].stb_code === stbType)) {
                        
                    }
                    else{
                        log.printDbg("remove notice cause by not suitable for device [" + (isUHD ? "UHD" : "") + "|" + (isSmart ? "Smart" : "") +"] notice for " + notice_list[i].stb_code);
                        notice_list.splice(i, 1);
                        i--;
                    }
                }
                else{
                    log.printDbg("remove notice cause by not suitable for device " + (isOts ? "OTS" : "OTV") +" notice for " + notice_list[i].biz_code);
                    notice_list.splice(i, 1);
                    i--;
                }
            }
            else {
                if (curDate < notice_list[i].start_time) {
                    // R6. 공지사항 OC 업데이트 시 미래 시간이 편성될 수 있다.
                    //     기존에는 걍 지워버렸는데 그렇게되면 업데이트 후 App 사용 중 그 시간에 도래하게되면 그 공지가 나타날 수가없다 그래서
                    //     메뉴 진입 시 마다 제거하여 노출하는 방식으로 수정 그래서 여기서는 제거하지 않는다
                    log.printDbg("dont delete future time notice");
                } else {
                    log.printDbg("remove notice cause by past notice");
                    notice_list.splice(i, 1);
                    i--;
                }
            }
        }

        if (is_order === true) {
            // 순서 필드값 존재
            // 공지사항 순서 필드값을 참조하여 정렬한다.
            notice_list.sort(function(a,b) {
                return a.order - b.order;
            });
        } else {
            // 순서 필드값 미 존재
            // [jh.lee] 공지사항 데이터를 최신순으로 정렬한다.(시작시간 기준으로...)
            notice_list.sort(function(a,b) {
               return b.start_time - a.start_time; 
            });
        }
        
        // [jh.lee] 공지사항 데이터를 복사하여 저장.(마이메뉴에서 사용할 데이)
        notice_data = notice_list.slice();
        
        // [jh.lee] 긴급공지사항이 아닌 것은 제거하여 다시 리스트 생성후 긴급공지만 걸러낸다.
        for(var i = 0; i < notice_list.length; i++) {
            if(notice_list[i].link_type !== "99") {
                notice_list.splice(i,1);
                i--;
            }
        }
        
        if (notice_list.length > 0) {
            emergency_notice = notice_list[0];
        } else {
            emergency_notice = undefined;
        }
    }
    
    /**
     * OC에서 받은 데이터에서 필요 없는 것을 삭제
     *
     * [dj.son] 기존 2-depth 메뉴까지 check 하는 로직에서 전 메뉴를 check 하도록 수정
     */
    function deleteMenu() {
        log.printDbg("deleteMenu()", LOG_PREFIX);

        var arrDelete = _searchMenu({
            menuData: menu_arr,
            allSearch: true,
            cbCondition: function (menu) {
                if (KTW.CONSTANT.IS_OTS) {
                    // [jh.lee] 앱세상(앱/게임/쇼핑)의 CAT_SMART 카테고리 제거. USB player 및 DLNA app 메뉴를 제거하기 위함.
                    if (menu.parent && menu.parent.id === "6910" && menu.catType === KTW.DATA.MENU.CAT.SMART) {
                        return true;
                    }

                    // 2017.06.05 dhlee
                    // KT 요구사항 반영
                    if (menu.catType === KTW.DATA.MENU.CAT.OTV) {
                        return true;
                    }

                    // 2017.06.05 dhlee
                    // KT 요구사항 반영
                    if (menu.catType === KTW.DATA.MENU.CAT.OTVUHD) {
                        return true;
                    }

                    //if (!KTW.CONSTANT.IS_UHD && menu.catType === KTW.DATA.MENU.CAT.OTVUHD) {
                    //    return true;
                    //}
                }
                else {
                    // 2017.06.05 dhlee
                    // KT 요구사항 반영
                    if (menu.catType === KTW.DATA.MENU.CAT.OTS) {
                        return true;
                    }

                    // 2017.06.05 dhlee
                    // KT 요구사항 반영
                    if (menu.catType === KTW.DATA.MENU.CAT.OTSUHD) {
                        return true;
                    }
                    //if (!KTW.CONSTANT.IS_UHD && menu.catType === KTW.DATA.MENU.CAT.OTSUHD) {
                    //    return true;
                    //}
                }

                // 2017.06.05 dhlee
                // KT 요구사항 반영
                if (!KTW.CONSTANT.STB_TYPE.UHD1 && menu.catType === KTW.DATA.MENU.CAT.UHD1_ONLY) {
                    return true;
                }

                // 2017.06.05 dhlee
                // KT 요구사항 반영
                if (!KTW.CONSTANT.STB_TYPE.UHD2 && menu.catType === KTW.DATA.MENU.CAT.UHD2_ONLY) {
                    return true;
                }
                // 2017.06.05 dhlee
                // KT 요구사항 반영
                if (!KTW.CONSTANT.STB_TYPE.UHD3 && menu.catType === KTW.DATA.MENU.CAT.UHD3_ONLY) {
                    return true;
                }

                // 2017.06.05 dhlee
                // KT 요구사항 반영
                if (!KTW.CONSTANT.STB_TYPE.ANDROID && menu.catType === KTW.DATA.MENU.CAT.GBOX_ONLY) {
                    return true;
                }

                // [jh.lee] show category 제거.
                if (menu.parent && menu.catType === KTW.DATA.MENU.CAT.SHOW) {
                    return true;
                }

                // [jh.lee] OC로 부터 받은 데이터에서 "MENU00100(MyMenu)", "MENU00000(채널가이드)" 데이터가 남아있다면 삭제.
                if (menu.id == "MENU00100" || menu.id == "MENU00000") {
                    return true;
                }

                // [dj.son] catType 이 MyMenu 인 메뉴 제거 (MyMenu 가 우리집 맞춤 TV 로 대체됨
                if (menu.catType === KTW.DATA.MENU.CAT.MYMENU) {
                    return true;
                }

                // 2017.05.23 dhlee
                // UHD3 STB에서 CAT_TYPE이 S, D, N 인 category 는 제거
                // RANK category를 삭제하고 있었음 SPECIAL("S")로 변경함
                if (KTW.CONSTANT.STB_TYPE.UHD3) {
                    if (menu.catType === KTW.DATA.MENU.CAT.SEARCH || menu.catType === KTW.DATA.MENU.CAT.SPECIAL || menu.catType === KTW.DATA.MENU.CAT.NEW_HOT) {
                        return true;
                    }
                }

                // 2017.06.26 dhlee
                // youtube를 지원하지 않는 STB의 경우 youtube catType 은 제거
                if (!KTW.CONSTANT.YOUTUBE_SUPPORT && menu.catType === KTW.DATA.MENU.CAT.YOUTUBE) {
                    return true;
                }

                // 2017.10.10 dj.son
                // 키즈플레이리스트 메뉴가 키즈 메뉴 하위에 위치할 경우에만 노출되도록, 나머지 경우에는 삭제
                if (menu.catType === KTW.DATA.MENU.CAT.KIDS_PLAYLIST) {
                    var parent = menu.parent;

                    if (parent) {
                        while (parent.parent) {
                            parent = parent.parent;
                        }
                    }

                    if (!parent || !_isKidsMenu(parent)) {
                        return true
                    }
                }

                // [jh.lee] 정의하지 않은 catType 제거.
                var isExistCatType = false;
                for (var key in KTW.DATA.MENU.CAT) {
                    if (!menu.catType || menu.catType === "" || KTW.DATA.MENU.CAT[key] === menu.catType) {
                        // [jh.lee] 정의된 catType 중에서 하나라도 일치하는 것이 존재하면 check 를 true로 변경
                        isExistCatType = true;
                        break;
                    }
                }
                if (!isExistCatType) {
                    return true;
                }
            }
        });

        for (var i = 0; i < arrDelete.length; i++) {
            var target = arrDelete[i];
            var parent = target.parent;
            var arrChildren = menu_arr;
            var menuIdx;

            if (parent) {
                arrChildren  = parent.children;
            }

            menuIdx = arrChildren.indexOf(target);

            arrChildren.splice(menuIdx, 1);

            if (parent) {
                --parent.itemCount;
                --parent.webItemCount;
            }
        }

/*
        // [jh.lee] check : false(일치하는 catType 없으므로 해당 메뉴 삭제), true(일치하는 catType 존재) 
        var check = false;
        var check_2depth = false;
        var children;
        
        for (var i = 0; i < menu_arr.length; i++) {
            if (KTW.CONSTANT.IS_OTS) {
                // [jh.lee] OTS
                if (menu_arr[i].id === "6910") {
                    // [jh.lee] 앱세상(앱/게임/쇼핑)의 CAT_SMART 카테고리 제거. USB player 및 DLNA app 메뉴를 제거하기 위함.
                    for (var j = 0; j < menu_arr[i].children.length; j++) {
                        if (menu_arr[i].children[j] && menu_arr[i].children[j].catType === KTW.DATA.MENU.CAT.SMART) {
                            menu_arr[i].children.splice(j, 1);
                        }
                    }
                    menu_arr[i].itemCount = menu_arr[i].children.length;
                }
            }

            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.OTV) {
                if (KTW.CONSTANT.IS_OTS) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.OTS) {
                if (!KTW.CONSTANT.IS_OTS) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.OTVUHD) {
                if (KTW.CONSTANT.IS_OTS || !KTW.CONSTANT.IS_UHD) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.OTSUHD) {
                if (!KTW.CONSTANT.IS_OTS || !KTW.CONSTANT.IS_UHD) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.UHD1_ONLY) {
                if (!KTW.CONSTANT.STB_TYPE.UHD1) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.UHD2_ONLY) {
                if (!KTW.CONSTANT.STB_TYPE.UHD2) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.UHD3_ONLY) {
                if (!KTW.CONSTANT.STB_TYPE.UHD3) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }
            // 2017.06.05 dhlee
            // KT 요구사항 반영
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.GBOX_ONLY) {
                if (!KTW.CONSTANT.STB_TYPE.ANDROID) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }

            if (menu_arr[i].children) {
                for (var j = 0; j < menu_arr[i].children.length; j++) {
                    if (menu_arr[i].children[j] && menu_arr[i].children[j].catType === KTW.DATA.MENU.CAT.SHOW) {
                        // [jh.lee] show category 제거.
                        log.printDbg("find show category !! and remove it.", LOG_PREFIX);
                        menu_arr[i].children.splice(j, 1);
                        j--;
                    }
                }
                menu_arr[i].itemCount = menu_arr[i].children.length;
                menu_arr[i].webItemCount = menu_arr[i].children.length;
            }
            
            if (menu_arr[i].id == "MENU00100" || menu_arr[i].id == "MENU00000") {
                // [jh.lee] OC로 부터 받은 데이터에서 "MENU00100(MyMenu)", "MENU00000(채널가이드)" 데이터가 남아있다면 삭제.
                menu_arr.splice(i, 1);
                i--;
                continue;
            }

            // [dj.son] catType 이 MyMenu 인 메뉴 제거 (MyMenu 가 우리집 맞춤 TV 로 대체됨
            if (menu_arr[i].catType === KTW.DATA.MENU.CAT.MYMENU) {
                menu_arr.splice(i, 1);
                i--;
                continue;
            }

            // 2017.05.23 dhlee
            // UHD3 STB에서 CAT_TYPE이 S, D, N 인 category 는 제거
            if (KTW.CONSTANT.STB_TYPE.UHD3) {
                if (menu_arr[i].catType === KTW.DATA.MENU.CAT.SEARCH || menu_arr[i].catType === KTW.DATA.MENU.CAT.RANK || menu_arr[i].catType === KTW.DATA.MENU.CAT.NEW_HOT) {
                    menu_arr.splice(i, 1);
                    i--;
                    continue;
                }
            }

            // [jh.lee] 정의하지 않은 catType 제거.
            for (var j in KTW.DATA.MENU.CAT) {
                if (!(menu_arr[i].catType) || KTW.DATA.MENU.CAT[j] === menu_arr[i].catType) {
                    // [jh.lee] 정의된 catType 중에서 하나라도 일치하는 것이 존재하면 check 를 true로 변경
                    check = true;
                    break;
                }
            }
            
            if (check === false) {
                // [jh.lee] check 값이 false 이면 catType 일치하는 것이 없기 때문에 해당 메뉴 삭제.
                log.printDbg("MenuDataManager - Undefined Category type = " + menu_arr[i].catType, LOG_PREFIX);
                menu_arr.splice(i,1);
                i--;
                check = false;
            } else {
                // [jh.lee] check 가 true 인 경우는 1depth 메뉴가 정의한 메뉴인 것
                // [jh.lee] 하지만 2depth에 정의되지 않은 메뉴가 존재할 수 있기 때문에 2depth 검사
                if (menu_arr[i].children) {
                    for (var k = 0; k < menu_arr[i].children.length; k++) {
                        for (var j in KTW.DATA.MENU.CAT) {
                            if (!(menu_arr[i].children[k].catType) || KTW.DATA.MENU.CAT[j] === menu_arr[i].children[k].catType) {
                                check_2depth = true;
                                break;
                            }
                        }
                        
                        if (check_2depth === false) {
                            log.printDbg("MenuDataManager - Undefined Category child type = " + menu_arr[i].children[k].catType, LOG_PREFIX);
                            menu_arr[i].children.splice(k, 1);
                            k--;
                            check_2depth = false;
                        } else { 
                            check_2depth = false;
                        }
                    }
                    menu_arr[i].itemCount = menu_arr[i].children.length;
                    menu_arr[i].webItemCount = menu_arr[i].children.length;
                }
                check = false;
            }
        }

        */
    }

    /**
     * [dj.son] 요구사항에 따른 Etc 메뉴 check 및 생성 (전체 메뉴를 다 check 해야 하는 경우 해당 함수 사용
     *
     * - 유튜브 관련 메뉴 생성 (Youtube, YoutubeKids)
     * - 키즈 리모컨 및 키즈 SubHome 고정 데이터 관련 메뉴 따로 저장
     */
    function checkEtcMenu () {
        log.printDbg("checkEtcMenu()", LOG_PREFIX);

        kidsHotKeyMenu = null;
        kidsSHDataMenu = null;

        var arrModtel = [];

        _searchMenu({
            menuData: menu_arr,
            cbCondition: function (tmpMenu) {
                if (tmpMenu.catType === KTW.DATA.MENU.CAT.YOUTUBE) {
                    makeYoutubeMenu(tmpMenu);
                }

                if (tmpMenu.catType === KTW.DATA.MENU.CAT.YOUTUBE_KIDS) {
                    makeYoutubeKidsMenu(tmpMenu);
                }

                if (tmpMenu.catType === KTW.DATA.MENU.CAT.KIDS_HOT_KEY) {
                    var parent = tmpMenu.parent;

                    if (parent) {
                        parent.children.splice(parent.children.indexOf(tmpMenu), 1);
                        --parent.children.itemCount;
                    }
                    else {
                        menu_arr.splice(menu_arr.indexOf(tmpMenu), 1);
                    }

                    kidsHotKeyMenu = tmpMenu;
                }

                if (tmpMenu.catType === KTW.DATA.MENU.CAT.KIDS_SH_DATA) {
                    var parent = tmpMenu.parent;

                    if (parent) {
                        parent.children.splice(parent.children.indexOf(tmpMenu), 1);
                        --parent.children.itemCount;
                    }
                    else {
                        menu_arr.splice(menu_arr.indexOf(tmpMenu), 1);
                    }

                    kidsSHDataMenu = tmpMenu;
                }

                /**
                 * [dj.son] catType 이 Motel 인 경우, 하위 메뉴까지 전부 Motel 로 변경
                 */
                if (KTW.CONSTANT.IS_BIZ && tmpMenu.catType === KTW.DATA.MENU.CAT.MOTEL) {
                    arrModtel.push(tmpMenu);
                }
            }
        });

        /**
         * [dj.son] catType 이 Motel 인 경우, 하위 메뉴까지 전부 Motel 로 변경
         */
        _searchMenu({
            menuData: arrModtel,
            cbCondition: function (tmpMenu) {
                tmpMenu.catType = KTW.DATA.MENU.CAT.MOTEL;
            }
        });


        // test
        //kidsSHDataMenu = new VMenu("TestKidsSHDataMenu", "키즈 서브홈 데이터 메뉴", null, 6, null, "Kids SubHome Data Menu");
        //kidsSHDataMenu.catType = "KidsSubHomeData";
        //var arr = [];
        //arr.push(new VCategory("0","10000000000000000001","Test_1", kidsSHDataMenu,"false","0","false","","","","","VC", 0,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","",""));
        //arr[0].hsTargetId = "10000000000000182124";
        //arr[0].hsTargetType = "0";
        //arr.push(new VCategory("0","10000000000000000002","Test_2", kidsSHDataMenu,"false","0","false","","","","","VC", 0,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","",""));
        //arr[1].hsTargetId = "10000000000000205134";
        //arr[1].hsTargetType = "0";
        //arr.push(new VCategory("0","10000000000000000003","Test_3", kidsSHDataMenu,"false","0","false","","","","","VC", 0,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","",""));
        //arr[2].hsTargetId = "10000000000000198455";
        //arr[2].hsTargetType = "0";
        //arr.push(new VCategory("0","10000000000000000004","Test_4", kidsSHDataMenu,"false","0","false","","","","","VC", 0,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","",""));
        //arr[3].hsTargetId = "10000000000000198455";
        //arr[3].hsTargetType = "0";
        //arr.push(new VCategory("0","10000000000000000005","Test_5", kidsSHDataMenu,"false","0","false","","","","","VC", 0,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","",""));
        //arr[4].hsTargetId = "10000000000000110692";
        //arr[4].hsTargetType = "0";
        //arr.push(new VCategory("0","10000000000000000006","Test_6", kidsSHDataMenu,"false","0","false","","","","","VC", 0,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","",""));
        //arr[5].hsTargetId = "10000000000000206255";
        //arr[5].hsTargetType = "0";
        //kidsSHDataMenu.setChildren(arr);
    }

    /**
     * [dj.son] 유튜브 메뉴 생성
     */
    function makeYoutubeMenu (youtubeMenu) {
        log.printDbg("makeYoutubeMenu()", LOG_PREFIX);

        if (youtubeMenu) {
            var parent = youtubeMenu.parent;
            var index = 0;

            if (!parent) {
                log.printDbg("youtube menu is 1-depth..... so delete!!!", LOG_PREFIX);

                index = menu_arr.indexOf(youtubeMenu);
                menu_arr.splice(index, 1);
                return;
            }

            var arrChildren = parent.children;

            if (!KTW.CONSTANT.YOUTUBE_SUPPORT) {
                log.printDbg("youtube not support... so delete!!!", LOG_PREFIX);

                index = arrChildren.indexOf(youtubeMenu);
                arrChildren.splice(index, 1);
                --parent.itemCount;
                return;
            }

            /**
             * TODO [dj.son] 추후 유튜브 이미지는 unicast_version 을 통해 설정할 예정 (mvno 이미지와 같은 로직), 그 전까지는 서버에 하드코딩 된 이미지를 사용하도록 수정
             */
            var imageUrl = KTW.CONSTANT.YOUTUBE_IMAGE.BMT_URL;
            if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
                imageUrl = KTW.CONSTANT.YOUTUBE_IMAGE.LIVE_URL;
            }

            var newYoutubeMenu = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.YOUTUBE, youtubeMenu.name, parent,
                null, null, null, null,
                imageUrl, null, null, null,
                imageUrl, null, null, null, null, null, null, null, "W", null, null,
                imageUrl, imageUrl, null);

            index = arrChildren.indexOf(youtubeMenu);
            arrChildren.splice(index, 1, newYoutubeMenu);
            youtubeMenu.children = null;
        }
        else {
            log.printDbg("youtube menu is not exist...", LOG_PREFIX);
        }
    }

    /**
     * [dj.son] 유튜브 키즈 메뉴 생성
     */
    function makeYoutubeKidsMenu (youtubeKidsMenu) {
        log.printDbg("makeYoutubeKidsMenu()", LOG_PREFIX);

        if (youtubeKidsMenu) {
            var parent = youtubeKidsMenu.parent;
            var index = 0;

            if (!parent) {
                log.printDbg("youtubeKids menu is 1-depth..... so delete!!!", LOG_PREFIX);

                index = menu_arr.indexOf(youtubeKidsMenu);
                menu_arr.splice(index, 1);
                return;
            }

            var arrChildren = parent.children;

            if (!KTW.CONSTANT.YOUTUBE_SUPPORT) {
                log.printDbg("youtube not support... so delete!!!", LOG_PREFIX);

                index = arrChildren.indexOf(youtubeKidsMenu);
                arrChildren.splice(index, 1);
                --parent.itemCount;
                return;
            }

            /**
             * TODO [dj.son] 추후 유튜브 이미지는 unicast_version 을 통해 설정할 예정 (mvno 이미지와 같은 로직), 그 전까지는 서버에 하드코딩 된 이미지를 사용하도록 수정
             */

            // TODO 2017.09.04 dhlee 유튜브 이미지 URL(키즈용)은 별도 URL을 사용할 예정임(아직 미정)
            var imageUrl = KTW.CONSTANT.YOUTUBE_KIDS_IMAGE.BMT_URL;
            if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
                imageUrl = KTW.CONSTANT.YOUTUBE_KIDS_IMAGE.LIVE_URL;
            }

            var newYoutubeMenu = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.YOUTUBE_KIDS, youtubeKidsMenu.name, parent,
                null, null, null, null,
                imageUrl, null, null, null,
                imageUrl, null, null, null, null, null, null, null, "W", null, null,
                imageUrl, imageUrl, null);

            index = arrChildren.indexOf(youtubeKidsMenu);
            arrChildren.splice(index, 1, newYoutubeMenu);
            youtubeKidsMenu.children = null;
        }
        else {
            log.printDbg("youtubeKids menu is not exist...", LOG_PREFIX);
        }
    }
    /**
     * 채널 가이드 메뉴 데이터를 생성하여 OC로 부터 받은 데이터에 추가
     */
    function makeChannelGuideMenu () {
        log.printDbg("makeChannelGuideMenu()", LOG_PREFIX);

        var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;
        // 2017.09.01 dhlee 한글 subtext 추가
        // 2017.09.05 dhlee 영문 subtext 추가
        var channel_guide_menu = new VMenu(MENU_ID.CHANNEL_GUIDE, "채널 가이드", null, 13, null, "Channel Guide", "보고 싶은 채널과 프로그램을 찾는 새로운 방법", "Find out what's on TV right now");

        // 채널 가이드 - child
        var arr0000 = [];

        // [jh.lee] OTS 프로그램 검색 하위 메뉴
        var arr8100 = [];

        // [jh.lee] 장르별 채널 하위 메뉴
        var arr8200 = [];



        // 채널 가이드 subhome 체크
        var chSubMenu = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                if (!menu.parent && menu.catType === KTW.DATA.MENU.CAT.CHSUB) {
                    return true;
                }
            }
        })[0];

        var familyHomeMenu = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                if (!menu.parent && menu.catType === KTW.DATA.MENU.CAT.SUBHOME) {
                    return true;
                }
            }
        })[0];

        /**
         * [dj.son] 오늘의 채널 가이드 메뉴가 존재하면 스위치, 아니면 1-depth list 맨 뒤에 추가
         */
        // 2017.09.01 dhlee
        // UI 개선 사항 반영
        // 오늘의 채널가이드가 편성되어 있지 않은 경우 우리집맞춤 TV 위에 노출되도록 해야 한다.
        // 우리집맞춤TV도 편성되어 있지 않은 경우 마지막에 위치시킨다.
        if (chSubMenu) {
            menu_arr.splice(menu_arr.indexOf(chSubMenu), 1, channel_guide_menu);
            chSubMenu.children = [];
            chSubMenu.itemCount = 0;
            arr0000.push(chSubMenu);
            chSubMenu.parent = channel_guide_menu;

            channel_guide_menu.itemSubText = chSubMenu.itemSubText;
            channel_guide_menu.englishItemSubText = chSubMenu.englishItemSubText;
        }
        else {
            // 2017.09.01 dhlee
            // 우리집맞춤TV가 있는 경우 그 위치를 기준으로 위쪽에 위치하도록 추가
            // 우리집맞춤TV가 0번 index인 경우 menu_arr 마지막에 붙여야 위쪽에 붙게 됨
            if (familyHomeMenu) {
                if (menu_arr.indexOf(familyHomeMenu) === 0) {
                    menu_arr.push(channel_guide_menu);
                } else {
                    menu_arr.splice(menu_arr.indexOf(familyHomeMenu), 0, channel_guide_menu);
                }
            } else {
                menu_arr.push(channel_guide_menu);
            }
        }

        if (KTW.CONSTANT.IS_OTS) {
            // 2017.09.01 dhlee
            // 오늘의채널가이드 > 전체 > 장르 > ... > 선호채널 순서이다.
            arr0000.push(new VMenu(MENU_ID.ENTIRE_CHANNEL_LIST, "전체 채널", channel_guide_menu, 0, null, "Electronic Program Guide"));
            arr0000.push(new VMenu(MENU_ID.MY_FAVORITED_CHANNEL, "선호 채널", channel_guide_menu, 0, null, "Favorite Channels"));
            var programmeSearchMenu = new VMenu(MENU_ID.PROGRAM_SEARCH,"프로그램 검색", channel_guide_menu, 11, null, "By Genre");
            arr0000.push(programmeSearchMenu);
            arr0000.push(new VMenu(MENU_ID.SKYLIFE_UHD_CHANNEL, "SkyLife UHD 채널", channel_guide_menu, 0, null, "SkyLife UHD Channel"));
            arr0000.push(new VMenu(MENU_ID.MOVIE_CHOICE, "무비초이스", channel_guide_menu, 0, null, "Movie Choice"));
            arr0000.push(new VMenu(MENU_ID.AUDIO_CHANNEL, "오디오 채널", channel_guide_menu, 0, null, "Audio Channel"));
            arr0000.push(new VMenu(MENU_ID.COMMUNITY_CHANNEL, "우리만의 채널", channel_guide_menu, 0, null, "Community Channel"));
            arr0000.push(new VMenu(MENU_ID.TV_APP_CHANNEL, "TV 앱/쇼핑 채널", channel_guide_menu, 0, null, "TV Apps & Shopping"));
            arr0000.push(new VMenu(MENU_ID.OLLEH_TV_CHANNEL, "olleh tv", channel_guide_menu, 0, null, "olleh tv"));
            arr0000.push(new VMenu(MENU_ID.BROADCAST_RESERVATION_LIST, "방송 예약 목록", channel_guide_menu, 0, null, "Reserved List"));
            /*
             VInteractive(itemType, id, name, parent, newHot, prInfo, wonYn, notice, imgUrl, locator, delDate, locator2, menuImgUrl,
             parameter, templateNo, templateType, templateLoc, templateImg, templateName, ktCasLocator, platformGubun,
             appImgUrl, webMenuImgUrl, w3ImgUrl, itemSubText, englishItemName)

             VInteractive(KTW.DATA.MENU.ITEM.INT_A, MENU_ID.KT_MEMBERSHIP, "KT 멤버십", myMenu_1, null, null, null, null,
             null, null, null, null, null, null, null, null, null, null, null, null, "W", null, null, null, null, null, "KT Membership"));
             */
            arr0000.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_M, MENU_ID.WATCH_4CHANNEL, "4채널 동시 시청", channel_guide_menu, null, null, null, null,
                "images/homemenu/service_default_01.png", KTW.CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE, null, KTW.CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE,
                "images/homemenu/service_default_01.png", null, null, null, null, null, null, null, "W", null, null,
                "images/homemenu/service_default_01.png", null, "Multi-view"));
            arr0000.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_M, MENU_ID.REAL_TIME_CHANNEL, "실시간 인기 채널", channel_guide_menu, null, null, null, null,
                "images/homemenu/service_default_02.png", KTW.CONSTANT.REALTIME_CHANNEL.LOCATOR.LIVE, null, KTW.CONSTANT.REALTIME_CHANNEL.LOCATOR.LIVE,
                "images/homemenu/service_default_02.png", null, null, null, null, null, null, null, "W", null, null,
                "images/homemenu/service_default_02.png", null, "Popular Channels"));
            //arr0000.push(new VMenu(MENU_ID.MY_FAVORITED_CHANNEL, "선호 채널", channel_guide_menu, 0, null, "Favorite Channels"));
            //arr0000.push(new VMenu(MENU_ID.ENTIRE_CHANNEL_LIST, "전체 채널", channel_guide_menu, 0, null, "Entire Lists"));
            channel_guide_menu.setChildren(arr0000);
            channel_guide_menu.itemCount = arr0000.length;

            arr8100.push(new VMenu(MENU_ID.PROGRAM_MOVIE, "영화/드라마", programmeSearchMenu, 0, null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_NEWS, "뉴스/시사", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_ENTERTAINMENT, "쇼/오락", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_SPORTS, "스포츠", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_KIDS, "어린이/청소년", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_MUSIC, "음악/무용", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_ART, "예술/문화", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_SOCIETY, "사회/정치/경제", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_EDU, "교육/과학/정보", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_LEISURE, "여가/취미", programmeSearchMenu, 0 , null, null));
            arr8100.push(new VMenu(MENU_ID.PROGRAM_ETC, "기타", programmeSearchMenu, 0 , null, null));
            programmeSearchMenu.setChildren(arr8100);
            programmeSearchMenu.itemCount = arr8100.length;
        }
        else {
            // [dj.son] 실시간 인기 채널 메뉴
            var popularChannelsMenu = null;

            // 2017.09.01 dhlee
            // 오늘의채널가이드 > 전체 > 장르 > ... > 선호채널 순서이다.
            arr0000.push(new VMenu(MENU_ID.ENTIRE_CHANNEL_LIST, "전체 채널", channel_guide_menu, 0, null, "Electronic Program Guide"));
            arr0000.push(new VMenu(MENU_ID.MY_FAVORITED_CHANNEL, "선호 채널", channel_guide_menu, 0, null, "Favorite Channels"));
            var genreMenu = new VMenu(MENU_ID.GENRE_CHANNEL, "장르별 채널", channel_guide_menu, 0, null, "By Genre");
            arr0000.push(genreMenu);
            arr0000.push(new VMenu(MENU_ID.UHD_CHANNEL , "UHD 채널", channel_guide_menu, 0, null, "UHD Channel"));
            arr0000.push(new VMenu(MENU_ID.AUDIO_CHANNEL, "오디오 채널", channel_guide_menu, 0, null, "Audio Channel"));
            arr0000.push(new VMenu(MENU_ID.COMMUNITY_CHANNEL, "우리만의 채널", channel_guide_menu, 0, null, "Community Channel"));
            arr0000.push(new VMenu(MENU_ID.TV_APP_CHANNEL, "TV 앱/쇼핑 채널", channel_guide_menu, 0, null, "TV Apps & Shopping"));
            arr0000.push(new VMenu(MENU_ID.BROADCAST_RESERVATION_LIST, "방송 예약 목록", channel_guide_menu, 0, null, "Reserved List"));
            arr0000.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_M, MENU_ID.WATCH_4CHANNEL, "4채널 동시 시청", channel_guide_menu, null, null, null, null,
                "images/homemenu/service_default_01.png", KTW.CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE, null, KTW.CONSTANT.FOUR_CHANNEL.LOCATOR.LIVE,
                "images/homemenu/service_default_01.png", null, null, null, null, null, null, null, "W", null, null,
                "images/homemenu/service_default_01.png", null, "Multi-view"));
            popularChannelsMenu = new VMenu(MENU_ID.REAL_TIME_CHANNEL, "실시간 인기 채널", channel_guide_menu, 0, null, "Popular Channels");
            arr0000.push(popularChannelsMenu);
            //arr0000.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_M, MENU_ID.REAL_TIME_CHANNEL, "실시간 인기 채널", channel_guide_menu, null, null, null, null,
            //    "images/homemenu/service_default_02.png", KTW.CONSTANT.REALTIME_CHANNEL.LOCATOR.LIVE, null, KTW.CONSTANT.REALTIME_CHANNEL.LOCATOR.LIVE,
            //    "images/homemenu/service_default_02.png", null, null, null, null, null, null, null, "W", null, null,
            //    "images/homemenu/service_default_02.png", null, "Popular Channels"));
            //arr0000.push(new VMenu(MENU_ID.MY_FAVORITED_CHANNEL, "선호 채널", channel_guide_menu, 0, null, "Favorite Channels"));
            //arr0000.push(new VMenu(MENU_ID.ENTIRE_CHANNEL_LIST, "전체 채널", channel_guide_menu, 0, null, "Entire Lists"));
            channel_guide_menu.setChildren(arr0000);
            channel_guide_menu.itemCount = arr0000.length;

            arr8200.push(new VMenu(MENU_ID.GENRE_TERRESTRIAL, "지상파/종편/홈쇼핑", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_DRAMA, "드라마/오락/음악", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_MOVIE, "영화/시리즈", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_SPORTS, "스포츠/레져", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_ANIMATION, "애니/유아/교육", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_DOCUMENTARY, "다큐/교양", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_NEWS, "뉴스/경제", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_SOCIAL, "공공/공익/정보", genreMenu, 0, null, null));
            arr8200.push(new VMenu(MENU_ID.GENRE_RELIGION, "종교/오픈", genreMenu, 0, null, null));
            genreMenu.setChildren(arr8200);
            genreMenu.itemCount = arr8200.length;

            var arrPopularChannels = [];
            /**
             * jjh1117 2017/10/31
             * 실시간인기채널 하위메뉴 생성 시 parent Menu가 잘못 설정되어 이 부분 수정함.
             */
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_ALL, "전체 채널", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_TERRESTRIAL, "지상파/종편", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_HOMESHOPPING, "홈쇼핑", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_SPORTS, "스포츠/레져", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_MOVIE, "영화/시리즈", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_DRAMA, "드라마/여성", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_ANIMATION, "애니메이션", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_KIDS, "유아/교육", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_ENTERTAINMENT, "오락/음악", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_NEWS, "뉴스/경제", popularChannelsMenu, 0, null, null));
            arrPopularChannels.push(new VMenu(MENU_ID.POPULAR_DOCUMENTARY, "다큐/교양", popularChannelsMenu, 0, null, null));
            popularChannelsMenu.setChildren(arrPopularChannels);
            popularChannelsMenu.itemCount = arrPopularChannels.length;
        }

    }

    function isAppGmMenu (menu) {
        if (menu.children) {
            var length = menu.children.length;
            for (var i = 0; i < length; i++) {
                if (menu.children[i].catType === KTW.DATA.MENU.CAT.APPGM) {
                    return true;
                }
            }
        }

        return false;
    }

    function _isKidsMenu (menu) {
        if (menu && menu.children) {
            var length = menu.children.length;
            for (var i = 0; i < length; i++) {
                if (menu.children[i].catType === KTW.DATA.MENU.CAT.KIDSSUB || menu.children[i].catType === KTW.DATA.MENU.CAT.PWORLD) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 요구사항에 따라 추가되는 메뉴
     * 1. 웹브라우저 2. 스카이터치
     *
     * TODO 추후 추가되는 메뉴 확인 후 수정해야 함
     */
    function makeEtcMenu() {
        var web_browser;
        var sky_touch;

        for (var i = 0; i < menu_arr.length; i++) {
            if (isAppGmMenu(menu_arr[i])) {
                // [jh.lee] 웹 브라우저 메뉴 추가
                web_browser = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.TV_INTERNET, "TV인터넷", menu_arr[i], null, null, null, null,
                    "images/app_img_internet.jpg", null, null, null,
                    "images/app_img_internet.jpg", null, null, null, null, null, null, null, "W", null, null,
                    "images/app_img_internet.jpg", "images/app_img_internet.jpg", null);

                /**
                 * [dj.son] [WEBIIIHOME-3671] TV 인터넷 / 스카이터치 메뉴를 앱/게임 메뉴 가장 하위에 추가하도록 수정
                 */
                //menu_arr[i].children.splice(5, 0, web_browser);
                menu_arr[i].children.push(web_browser);

                if (KTW.CONSTANT.IS_OTS) {
                    // [jh.lee] OTS, 스카이 터치 메뉴 추가
                    //sky_touch = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.SKY_TOUCH, "스카이터치", menu_arr[i], null, null, null, null,
                    //    "images/homemenu/skytouch.png", sky_touch_locator, null, sky_touch_locator,
                    //    "images/homemenu/skytouch.png", null, null, null, null, null, null, null, "W", null, null,
                    //    "images/homemenu/skytouch.png", "images/homemenu/skytouch.png", null);
                    sky_touch = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.SKY_TOUCH, "스카이터치", menu_arr[i], null, null, null, null,
                        "images/app_img_skytouch.png", sky_touch_locator, null, sky_touch_locator,
                        "images/app_img_skytouch.png", null, null, null, null, null, null, null, "W", null, null,
                        "images/app_img_skytouch.png", "images/app_img_skytouch.png", null);

                    //menu_arr[i].children.splice(5, 0, sky_touch);
                    menu_arr[i].children.push(sky_touch);
                }
                menu_arr[i].itemCount = menu_arr[i].children.length;
            }
        }


    }
        /*
            if (menu_arr[i].id === "6910") {
                // [jh.lee] 앱세상(앱/게임/쇼핑)

                // [jh.lee] 웹 브라우저 메뉴 추가
                web_browser = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.TV_INTERNET, "TV인터넷", menu_arr[i], null, null, null, null,
                                            "images/app_img_internet.jpg", null, null, null,
                                            "images/app_img_internet.jpg", null, null, null, null, null, null, null, "W", null, null,
                                            "images/app_img_internet.jpg", "images/app_img_internet.jpg", null);
                menu_arr[i].children.splice(5, 0, web_browser);

                if (KTW.CONSTANT.IS_OTS) {
                    // [jh.lee] OTS, 스카이 터치 메뉴 추가
                    sky_touch = new VInteractive(KTW.DATA.MENU.ITEM.INT_M, KTW.managers.data.MenuDataManager.MENU_ID.SKY_TOUCH, "스카이터치", menu_arr[i], null, null, null, null,
                                            "images/homemenu/skytouch.png", sky_touch_locator, null, sky_touch_locator,
                                            "images/homemenu/skytouch.png", null, null, null, null, null, null, null, "W", null, null,
                                            "images/homemenu/skytouch.png", "images/homemenu/skytouch.png", null);
                    menu_arr[i].children.splice(5, 0, sky_touch);
                }
                menu_arr[i].itemCount = menu_arr[i].children.length;
        */

                /*
                if (KTW.SUPPORT_DEMO === true) {
                    var children = menu_arr[i].children;
                    if (children) {
                        for (var j = 0; j < children.length; j++) {
                            if (children[j].id === "10000000000000102649") { // 스마트 서비스

                            	if (KTW.DEMO.MSTB === true) {
                                    var app1 = new VInteractive(KTW.DATA.MENU.ITEM.INT_A, "A000000000003", "EA Game", children[j], null, null, null, null,
                                                            "images/demo/eagames.png", "com.eamobile.nbajam_row_wf", null, "com.eamobile.nbajam_row_wf",
                                                            "images/demo/eagames.png", null, null, null, null, null, null, null, "W", null, null,
                                                            "images/demo/eagames.png", "images/demo/eagames.png", null);
                                    var app2 = new VInteractive(KTW.DATA.MENU.ITEM.INT_A, "A000000000001", "Internet", children[j], null, null, null, null,
                                                            "images/demo/browser.png", "com.android.browser", null, "com.android.browser",
                                                            "images/demo/browser.png", null, null, null, null, null, null, null, "W", null, null,
                                                            "images/demo/browser.png", "images/demo/browser.png", null);
                                    var app3 = new VInteractive(KTW.DATA.MENU.ITEM.INT_A, "A000000000002", "TV AR", children[j], null, null, null, null,
                                        "images/demo/ar.png", "com.sum.Dino", null, "com.sum.Dino",
                                        "images/demo/ar.png", null, null, null, null, null, null, null, "W", null, null,
                                        "images/demo/ar.png", "images/demo/ar.png", null);

                                    children[j].children.splice(2, 0, app1);
                                    children[j].children.splice(3, 0, app2);
                                    children[j].children.splice(4, 0, app3);
                            	}
                            	if (KTW.DEMO.NFX === true) {
                                    var app1 = new VInteractive(KTW.DATA.MENU.ITEM.INT_NFX, "A000000000101", "Netflix", children[j], null, null, null, null,
                                        "images/demo/netflix.png", null, null, null,
                                        "images/demo/netflix.png", null, null, null, null, null, null, null, "W", null, null,
                                        "images/demo/netflix.png", "images/demo/netflix.png", null);

                            		children[j].children.splice(2, 0, app1);
                            	}

                                children[j].itemCount = children[j].children.length;
                            }
                        }
                    }
                }
                */
            //}
        //}
    //}

    /**
     * 우리집 맞춤 TV 메뉴 데이터 구성
     *
     * [dj.son] 5월향 고도화 버전에 따른 마이 메뉴 정리 (자세한 사항은 메뉴 정리한 엑셀 문서 참조)
     */
    function makeMyMenu() {

        var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;
        var my_menu = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                if (!menu.parent && menu.catType === KTW.DATA.MENU.CAT.SUBHOME) {
                    return true;
                }
            }
        })[0];

        if (!my_menu) {
            // 2017.09.04 dhlee
            // 메뉴명 변경 (from KT)
            /**
             * [dj.son] [WEBIIIHOME-3318] 하드 코딩 메뉴 itemSubText, englishItemSubText 추가
             */
            my_menu = new VMenu(MENU_ID.MY_MENU, "마이 메뉴", null, 13, null, "My Menu","마이메뉴와 맞춤추천 VOD가 한자리에!", "My menu & VOD recommendations");
            my_menu.catType = KTW.DATA.MENU.CAT.SUBHOME;
            menu_arr.push(my_menu);
        }

        my_menu.displayHidden = true;

        // 우리집 맞춤 TV 자식 메뉴 구성
        var arr = [];

        // 마이 메뉴 2
        var myMenu_2 = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                if (menu.catType === KTW.DATA.MENU.CAT.MYMENU2) {
                    return true;
                }
            }
        })[0];
        if (!myMenu_2) {
            myMenu_2 = new VMenu(MENU_ID.MY_MENU_2, "마이 메뉴", my_menu, 8, null, "My Menu");
            myMenu_2.catType = KTW.DATA.MENU.CAT.MYMENU2;
            /**
             * [dj.son] 하단에서 children 과 arrMyMenu_2 를 concat 하는데, children 이 undefined 이면 최종 리스트에 undefined 가 추가되는 이슈가 있음
             */
            myMenu_2.children = [];
        }
        else {
            var parentChildren = menu_arr;
            if (myMenu_2.parent && myMenu_2.parent.children) {
                parentChildren = myMenu_2.parent.children;
            }
            parentChildren.splice(parentChildren.indexOf(myMenu_2), 1);
            myMenu_2.parent = my_menu;
        }
        arr.push(myMenu_2);

        var arrMyMenu_2 = [];

        var purchaseInfoMenu = new VMenu(MENU_ID.PURCHASED_LIST_MENU,"구매내역 보기", myMenu_2, 3, null, "Purchased");
        arrMyMenu_2.push(purchaseInfoMenu);

        // 구매 내역 자식 메뉴 구성
        var arrPurchasInfoChildMenu = [];
        arrPurchasInfoChildMenu.push(new VMenu(MENU_ID.PURCHASED_LIST, "VOD 구매목록", purchaseInfoMenu, 0, null, "VOD"));
        arrPurchasInfoChildMenu.push(new VMenu(MENU_ID.OTN_LIST, "모바일 공유목록", purchaseInfoMenu, 0, null, "Mobile Content"));
        if (KTW.CONSTANT.IS_OTS) {
            arrPurchasInfoChildMenu.push(new VMenu(MENU_ID.MOVIE_CHOICE_PURCHASED_LIST, "무비초이스 구매목록", purchaseInfoMenu, 0, null, "Movie Choice"));
        }
        purchaseInfoMenu.children = arrPurchasInfoChildMenu;
        purchaseInfoMenu.itemCount = arrPurchasInfoChildMenu.length;

        arrMyMenu_2.push(new VMenu(MENU_ID.MY_VOD_COLLECTION, "마이 소장용VOD", myMenu_2, 0, null, "My VOD Collection")); // 2017.02.08 명칭변경 (한글/영문)
        arrMyMenu_2.push(new VMenu(MENU_ID.MY_PLAY_LIST, "마이 플레이리스트", myMenu_2, 0, null, "My Playlist"));

        var couponPointMenu = new VMenu(MENU_ID.COUPON_POINT,"쿠폰, 포인트 등록/충전", myMenu_2, 4, null, "Coupons & Points");
        arrMyMenu_2.push(couponPointMenu);

        // 쿠폰, 포인트 등록/추천 자식 메뉴 구성
        var arrCouponPointChildMenu = [];
        arrCouponPointChildMenu.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_A, MENU_ID.TV_COUPON, "TV쿠폰", couponPointMenu, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, "W", null, null, null, null,  "TV Coupon"));
        arrCouponPointChildMenu.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_A, MENU_ID.KT_MEMBERSHIP, "KT 멤버십", couponPointMenu, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, "W", null, null, null, null, "Membership"));
        arrCouponPointChildMenu.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_A, MENU_ID.POINT, "TV포인트", couponPointMenu, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, "W", null, null, null, null, "TV Point"));
        arrCouponPointChildMenu.push(new VInteractive(KTW.DATA.MENU.ITEM.INT_A, MENU_ID.CONTENT_COUPON, "콘텐츠이용권", couponPointMenu, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, "W", null, null, null, null, "VOD Voucher"));
        couponPointMenu.setChildren(arrCouponPointChildMenu);

        arrMyMenu_2.push(new VMenu(MENU_ID.SUBSCRIPTION_INFOMATION, "가입 정보", myMenu_2, 0, null, "Package Info"));

        // MyMenu2 편성 메뉴 추가
        arrMyMenu_2 = arrMyMenu_2.concat(myMenu_2.children);

        myMenu_2.itemCount = arrMyMenu_2.length;
        myMenu_2.setChildren(arrMyMenu_2);


        arr.push(new VMenu(MENU_ID.WATCHED_LIST, "최근 시청 목록", my_menu, 0, null, "Recently Watched"));

        arr.push(new VMenu(MENU_ID.FAVORITE_VOD_LIST, "찜한 목록", my_menu, 0, null, "Wish List"));

        var recommendVodMenu = new VMenu(MENU_ID.RECOMMEND_VOD, "당신을 위한 VOD 추천", my_menu, 0, null, "VOD for You");
        arr.push(recommendVodMenu);

        var arrRecommendVodChildMenu = [];
        arrRecommendVodChildMenu.push(new VMenu(MENU_ID.MY_RECOMMEND_ONLY_MINE, "나의 취향 알아보기", recommendVodMenu, 0, null, null));
        arrRecommendVodChildMenu.push(new VMenu(MENU_ID.MY_CURATION, "내가 평가한 콘텐츠", recommendVodMenu, 0, null, null));
        recommendVodMenu.children = arrRecommendVodChildMenu;
        recommendVodMenu.itemCount = arrRecommendVodChildMenu.length;



        var alertMenu = new VMenu(MENU_ID.MSG_BOX, "공지/알림", my_menu, 0, null, "Inbox");
        alertMenu.displayHidden = true;
        arr.push(alertMenu);

        // 알림박스 자식 메뉴 구성
        var arrAlertChildMenu = [];
        arrAlertChildMenu.push(new VMenu(MENU_ID.MSG_NOTICE, "공지사항", my_menu, 0, null, "Notice"));
        arrAlertChildMenu.push(new VMenu(MENU_ID.MSG_ALERT, "알림", my_menu, 0, null, "Alert"));
        arrAlertChildMenu.push(new VMenu(MENU_ID.MSG_EVENT, "이벤트", my_menu, 0, null, "Event"));
        alertMenu.children = arrAlertChildMenu;
        alertMenu.itemCount = arrAlertChildMenu.length;

        var favoriteMenu = new VMenu(MENU_ID.FAVORITE_MENU, "즐겨찾는 메뉴", my_menu, 0, null, "Favorite Menu")
        favoriteMenu.displayHidden = true;
        arr.push(favoriteMenu);

        my_menu.itemCount = arr.length;
        my_menu.setChildren(arr);

        my_menu_id = my_menu.id;


        /**
         * [dj.son] 기존에는 홈메뉴 좌측에 추천 데이터 존재하였으나, 5월향에는 홈메뉴 좌측에 추천 메뉴 사라짐... 따라서 데이터 구성할 필요 없음
         */
        // children 으로 셋하기 전에, 첫번째 편성 메뉴를 홈메뉴 좌측 세팅 데이터 추가
        //KTW.managers.data.SubHomeDataManager.addSettingMenuRecommendData(myMenu_2.children ? myMenu_2.children[0] : null);
    }
    
    /**
     * Setting 메뉴 구성 및 추가
     */
    function makeSettingMenu() {
        log.printDbg("makeSettingMenu()", LOG_PREFIX);

        var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;
        var setting_menu = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                if (!menu.parent && menu.catType === KTW.DATA.MENU.CAT.CONFIG) {
                    return true;
                }
            }
        })[0];

        if (!setting_menu) {
            setting_menu = new VMenu(MENU_ID.SETTING, "설정", undefined, 3, null, "Preference");
            setting_menu.catType = KTW.DATA.MENU.CAT.CONFIG;
            menu_arr.push(setting_menu);
        }

        setting_menu.displayHidden = true;

        // 세팅 최상위 메뉴 3개
        var arr5500 = [];
        var child_safe_setting = new VMenu(MENU_ID.KIDS_CARE_SETTING, "자녀안심 설정", setting_menu, 4, null, "Parental Control");
        var channel_setting = new VMenu(MENU_ID.CHANNEL_SETTING, "채널/VOD 설정", setting_menu, 7, null, "Channel/VOD Setting");
        var system_setting = new VMenu(MENU_ID.SYSTEM_SETTING, "시스템 설정", setting_menu, 15, null, "System Setting");
        arr5500.push(child_safe_setting);
        arr5500.push(channel_setting);
        arr5500.push(system_setting);

        setting_menu.itemCount = arr5500.length;
        setting_menu.setChildren(arr5500);


        // 자녀안심 설정 하위 메뉴
        var arr5600 = [];
        arr5600.push(new VMenu(MENU_ID.SETTING_CHANGE_PIN, "비밀번호 변경/초기화", child_safe_setting, 0, null, "Manage PIN"));
        arr5600.push(new VMenu(MENU_ID.SETTING_PARENTAL_RATING, "시청연령 제한", child_safe_setting, 0, null, "Rating Locks"));
        arr5600.push(new VMenu(MENU_ID.SETTING_TIME_LIMIT, "시청시간 제한", child_safe_setting, 0, null, "Time Restrictions"));
        arr5600.push(new VMenu(MENU_ID.SETTING_BLOCKED_CHANNEL, "시청채널 제한", child_safe_setting, 0, null, "Channels Blocked"));
        child_safe_setting.itemCount = arr5600.length;
        child_safe_setting.setChildren(arr5600);


        // 채널/VOD 설정 하위 메뉴
        // 2018.01.22 sw.nam
        // 메뉴 위치 및 이름 변경 (VOD 보기 옵션 -> VOD 리스트 보기 방식)
        // 채널 정보 표시 시간이 채널 숨김 / 건너뛰기 채널 위로 올라옴
        var arr6200 = [];
        arr6200.push(new VMenu(MENU_ID.SETTING_FAVORITE_CHANNEL, "선호 채널", channel_setting, 0, null, "Favorite Channels"));
        //2018.03.16 sw.nam 메뉴 이름변경(채널 정보 표시시간 -> 채널 미니가이드 표시시간)
        arr6200.push(new VMenu(MENU_ID.SETTING_CHANNEL_GUIDE, "채널 미니가이드 표시시간", channel_setting, 0, null, "Mini EPG Display"));
        if (KTW.CONSTANT.IS_OTS) {
            arr6200.push(new VMenu(MENU_ID.SETTING_SKIPPED_CHANNEL, "건너뛰기 채널", channel_setting, 0, null, "Hide Channels"));
        }
        else {
            arr6200.push(new VMenu(MENU_ID.SETTING_SKIPPED_CHANNEL, "채널 숨김", channel_setting, 0, null, "Hide Channels"));
        }
        arr6200.push(new VMenu(MENU_ID.SETTING_DESCRIPTIVE_VIDEO_SERVICE, "화면 해설 방송", channel_setting, 0, null, "Descriptive Video Service"));
        arr6200.push(new VMenu(MENU_ID.SETTING_CLOSED_CAPTION, "자막 방송", channel_setting, 0, null, "Subtitles"));
        // 2017.05.04 dhlee 메뉴 위치 이동

        arr6200.push(new VMenu(MENU_ID.SETTING_POSTER_SHAPE, "VOD 리스트 보기 방식", channel_setting, 0, null, "Menu View Options"));    // 2017.06.01 영문 메뉴명 원복

        /**
         * [dj.son] ACAP 로직 적용, 국방 상품인 경우 해당 메뉴 미노출
         */
        if (!KTW.managers.service.ProductInfoManager.isMilitary()) {
            arr6200.push(new VMenu(MENU_ID.SETTING_SERIES_AUTO_PLAY, "시리즈 몰아보기", channel_setting, 0, null, "Series Autoplay"));
        }

        channel_setting.itemCount = arr6200.length;
        channel_setting.setChildren(arr6200);

        // 시스템 설정 하위 메뉴
        var arr7000 = [];
        arr7000.push(new VMenu(MENU_ID.SETTING_ASPECT_RATIO, "화면 비율", system_setting, 0, null, "Aspect Ratio"));
        arr7000.push(new VMenu(MENU_ID.SETTING_RESOLUTION, "해상도", system_setting, 0, null, "Resolution"));
        arr7000.push(new VMenu(MENU_ID.SETTING_AUTO_POWER, "자동 전원", system_setting, 0, null, "Manual Standby"));
        arr7000.push(new VMenu(MENU_ID.SETTING_STANDBY_MODE, "자동 대기 모드", system_setting, 0, null, "Auto Standby"));
        arr7000.push(new VMenu(MENU_ID.SETTING_SOUND, "사운드", system_setting, 0, null, "Sound"));
        if (KTW.CONSTANT.STB_TYPE.ANDROID) {
            // 2017.02.08 기가지니 STB 에서만 사용함.
            // sw.nam 메뉴 이름 변경 : 오디오 출력설정 -> 오디오 출력 기기(WEBIIIHOME-3798)
            arr7000.push(new VMenu(MENU_ID.SETTING_GIGA_GINI_AUDIO_OUTPUT, "오디오 출력 기기", system_setting, 0, null, "Audio Output"));
        }

        if (KTW.CONSTANT.UHD3.EX_VOICE && KTW.CONSTANT.UHD3.EX_SPEAKER) {
            var tmpMenuTitle = null;
            // 2018.02.25 sw.nam
            // 가이드 음성 호출어 기능을 제공하는 경우, "가이드 음성" 으로,
            // 제공하지 않는 경우 "음성가이드" 로 메뉴 명을 노출한다.
            var triggerVoiceType = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_TRIGGER_VOICE_TYPE);

            if (triggerVoiceType !== null && triggerVoiceType !== "undefined" && triggerVoiceType !== undefined && triggerVoiceType !== "null") {
                // 제공하는 경우
                tmpMenuTitle = "가이드 음성"
            }else {
                tmpMenuTitle = "음성 가이드"
            }

            arr7000.push(new VMenu(MENU_ID.SETTING_VOICE_GUIDE, tmpMenuTitle, system_setting, 0, null, "Speech Recognition"));
        }
        // 2017.05.22 dhlee
        // UHD3 STB의 경우 이동식 디스크 연결 메뉴 및 기능을 제공하지 않는다. (KT 요구사항 반영)
        if (!KTW.CONSTANT.STB_TYPE.UHD3) {
            /**
             * [dj.son] ACAP 로직 적용, 국방 상품인 경우 해당 메뉴 미노출
             */
            if (!KTW.managers.service.ProductInfoManager.isMilitary()) {
                arr7000.push(new VMenu(MENU_ID.SETTING_USB, "이동식 디스크 연결", system_setting, 0, null, "USB Play"));
            }
        }
        arr7000.push(new VMenu(MENU_ID.SETTING_BLUETOOTH, "블루투스 장치 연결", system_setting, 0, null, "Bluetooth"));
        arr7000.push(new VMenu(MENU_ID.SETTING_ALERT_MESSAGE, "알림 메시지", system_setting, 0, null, "Push Notification"));
        arr7000.push(new VMenu(MENU_ID.SETTING_HDMI_CEC, "HDMI 전원 동기화", system_setting, 0, null, "HDMI-CEC"));
        // 2018.01.22 sw.nam 메뉴 이름 변경 (기본 언어 -> 메뉴 언어)
        arr7000.push(new VMenu(MENU_ID.LANGUAGE, "메뉴  언어", system_setting, 0, null, "Language"));
        arr7000.push(new VMenu(MENU_ID.SETTING_SYSTEM_INITIALIZE, "전체 초기화", system_setting, 0, null, "Reset All Settings"));
        arr7000.push(new VMenu(MENU_ID.SETTING_START_CHANNEL, "시작 채널", system_setting, 0, null, "Guide Ch. Setting"));
        if (KTW.CONSTANT.IS_OTS) {
            arr7000.push(new VMenu(MENU_ID.TRANSPONDER, "중계기", system_setting, 0, null, "Transponder"));
            arr7000.push(new VMenu(MENU_ID.COLLECT_SIGNAL, "수신품질 측정", system_setting, 0, null, "Reception Quality"));
        }
        arr7000.push(new VMenu(MENU_ID.SETTING_SYSTEM_INFO, "시스템 정보", system_setting, 0, null, "System Info"));
        system_setting.itemCount = arr7000.length;
        system_setting.setChildren(arr7000);

        setting_menu_id = setting_menu.id;

        // 설정 hidden 메뉴 생성
        makeHiddenMenu(system_setting);
    }

    /**
     * Kids 메뉴 구성
     * 2017.05.09 dhlee 메뉴 구성 변경
     */
    function makeKidsMenu () {
        var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;
        var kidsMenu = null;
        var todayKidsMenu = _searchMenu({
            menuData: menu_arr,
            cbCondition: function (menu) {
                //if (menu.catType === KTW.DATA.MENU.CAT.KIDSSUB || menu.catType === KTW.DATA.MENU.CAT.PWORLD) {
                if (menu.catType === KTW.DATA.MENU.CAT.KIDSSUB) {
                    return true;
                }
            }
        })[0];

        if (todayKidsMenu) {
            var parent = todayKidsMenu.parent;

            while (parent.parent) {
                parent = parent.parent;
            }

            kidsMenu = parent;

            /**
             * [dj.son] 키즈 메뉴 내 첫번째 서브홈 메뉴인지 아닌지를 구분하기 위해 property 추가
             */
            todayKidsMenu.isFirstKidsSubHome = true;
        }

        if (kidsMenu) {
            // 키즈 플레이 리스트, 키즈 채널 편성표 메뉴 추가
            var kidsEpgMenu = new VMenu(MENU_ID.KIDS_CHANNEL, "키즈 채널 편성표", kidsMenu, 0, null, "Kids Channel");
            var kidsPlayListMenu = new VMenu(MENU_ID.KIDS_PLAY_LIST, "키즈 플레이리스트", kidsMenu, 0, null, "Kids Playlist");

            var tmpKidsPlayListMenu = _searchMenu({
                menuData: [kidsMenu],
                cbCondition: function (menu) {
                    if (menu.catType === KTW.DATA.MENU.CAT.KIDS_PLAYLIST) {
                        return true;
                    }
                }
            })[0];

            if (tmpKidsPlayListMenu) {
                tmpKidsPlayListMenu.parent.children.splice(tmpKidsPlayListMenu.parent.children.indexOf(tmpKidsPlayListMenu), 1, kidsPlayListMenu);
            }
            else {
                if (todayKidsMenu) {
                    kidsMenu.children.splice(kidsMenu.children.indexOf(todayKidsMenu) + 1, 0, kidsPlayListMenu);
                }
                else {
                    kidsMenu.children.splice(kidsMenu.children.length, 0, kidsPlayListMenu);
                }
            }

            var parentWorldMenu = _searchMenu({
                menuData: menu_arr,
                cbCondition: function (menu) {
                    if (menu.catType === KTW.DATA.MENU.CAT.PWORLD) {
                        return true;
                    }
                }
            })[0];

            if (parentWorldMenu) {
                kidsMenu.children.splice(kidsMenu.children.indexOf(parentWorldMenu) + 1, 0, kidsEpgMenu);
            } else {
                kidsMenu.children.splice(kidsMenu.children.length, 0, kidsEpgMenu);
            }

            kidsMenu.itemCount += 2;
        }

        kids_menu_data = kidsMenu;
    }

    /**
     * setting 메뉴 내부에 히든 메뉴 구성
     * 시스템 설정 메뉴의 hiddenChildren 으로 추가
     */
    function makeHiddenMenu (setting_menu) {
        var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;

        var hidden_setting_menu_arr = [];

        if (KTW.CONSTANT.IS_OTS) {
            var adminSystemSetting = new VMenu(MENU_ID.ADMIN_SYSTEM_SETTING, "시스템 설정", setting_menu, 5, null);
            hidden_setting_menu_arr.push(adminSystemSetting);

            var arr6000 = [];
            var lnb_setting = new VMenu(MENU_ID.SKY_LNB, "LNB 설정", adminSystemSetting, 4, null);
            arr6000.push(lnb_setting);
            arr6000.push(new VMenu(MENU_ID.SKY_EMERGENCY, "긴급 메시지 알림 설정", adminSystemSetting, 0, null));
            arr6000.push(new VMenu(MENU_ID.SKY_CHANNEL_INFO, "채널정보 출력설정", adminSystemSetting, 0, null));
            arr6000.push(new VMenu(MENU_ID.SKY_CLOSED_CAPTION, "Closed 캡션 설정", adminSystemSetting, 0, null));
            arr6000.push(new VMenu(MENU_ID.SKY_CAS, "CAS 정보", adminSystemSetting, 0, null));
            adminSystemSetting.setChildren(arr6000);

            var arr7000 = [];
            arr7000.push(new VMenu(MENU_ID.SKY_LNB_A, "DiSEqC-A(무궁화 3호)", lnb_setting, 0, null));
            arr7000.push(new VMenu(MENU_ID.SKY_LNB_B, "DiSEqC-B", lnb_setting, 0, null));
            arr7000.push(new VMenu(MENU_ID.SKY_LNB_C, "DiSEqC-C", lnb_setting, 0, null));
            arr7000.push(new VMenu(MENU_ID.SKY_LNB_D, "DiSEqC-D", lnb_setting, 0, null));
            lnb_setting.setChildren(arr7000);
        }

        if (KTW.CONSTANT.UHD3.BEACON) {
            hidden_setting_menu_arr.push(new VMenu(MENU_ID.SETTING_BEACON, "비콘 송신 설정", setting_menu, 0, null));
        }

        setting_menu.hiddenChildren = hidden_setting_menu_arr;
    }
    
    function createOriginalData() { 
        // [jh.lee] OC에서 받은 데이터로 구성한 원본 메뉴 데이터
        origin_data = util.homeMenuArrayCopy(menu_arr);
        // [jh.lee] OC에서 받은 데이터로 구성한 화면에 보여질 메뉴 데이터(메뉴의 변경사항이 반영될 수 있음)
        menu_data = util.homeMenuArrayCopy(menu_arr);
        // [jh.lee] 성인19 메뉴 설정 값 로드("true" : 성인19메뉴 숨김, "false" : 성인19베뉴 오픈)
        // 2016.12.19 dhlee Configuration (UP) 만 사용하도록 한다.
        //removed_adult_cat = storageManager.ps.load(storageManager.KEY.MENU_LOCK_ADULT);
        // 2017.08.31 dhlee
        // Configuration 을 사용하면서 값이 반대가 되어야 한다.
        //removed_adult_cat = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY);
        removed_adult_cat = displayAdult;
        log.printDbg("MenuDataManager initMenuData Complete", LOG_PREFIX);
        
        try {
            if (removed_adult_cat === false) {
                // [jh.lee] 성인19+ 메뉴가 숨김 상태면 성인메뉴 제거. 메뉴데이터를 받아오는 시점이기 때문에 함수 호출시 true
                removeAdultCat(true);
            }
        } catch(error) {
            log.printDbg(error.message, LOG_PREFIX);
        }

        log.printDbg("homeportal initMenuData end "+ Date.now(), LOG_PREFIX);
        // [jh.lee] 메뉴데이터 변경 후 생성이 최종적으로 마무리 되면 메뉴데이터가 변경되었다는 사실을 등록된 리스너들에게 전달
        notifyMenuDataChange(KTW.managers.data.MenuDataManager.UPDATE.ORIGIN);

        if (initialized_callback) {
            initialized_callback();
            initialized_callback = null;
        }
    }

    /**
     * menu_data 내 전체 메뉴 탐사
     *
     * 메뉴 탐사 로직은 1-Depth 메뉴 리스트 부터 n-Depth 메뉴 리스트까지 차례대로 순방함
     * (메뉴 id 같은게 뭔가 특별한 규칙으로 생성된 거라면 다르게 구현할 수도 있겠지만... 규칙을 모르기에 이렇게 하나하나 다 검사하는 방식으로 구현)
     * 조건에 맞는 메뉴를 찾았을때, 순방을 멈출수도 있고 전체 메뉴를 끝까지 순방할 수도 있음
     * 실제로 전체 메뉴는 대략 2000 개 정도 됨
     */
    function _searchMenu(options) {
        if (!options || !options.cbCondition || !options.menuData) {
            return;
        }

        var targetList = [];
        var result = [];

        var length = options.menuData.length;
        for (var i = 0; i < length; i++) {
            targetList.push(options.menuData[i]);
        }

        while (targetList.length > 0) {
            var menu = targetList.shift();

            if (menu) {
                var isOk = options.cbCondition(menu);

                if (isOk) {
                    result.push(menu);
                    if (!options.allSearch) {
                        break;
                    }
                }

                if (menu.children && menu.children.length > 0) {
                    for (var j = 0; j < menu.children.length; j++) {
                        targetList.push(menu.children[j]);
                    }
                }
            }
        }

        return result;
    }
    
    /**
     * 제거된 성인 카테고리 다시 메뉴 데이터에 추가
     */
    function addAdultCat() {
        //if (removed_adult_cat == "true" && adult_menu_data && adult_menu_data.length > 0) {
        if (removed_adult_cat === true && adult_menu_data && adult_menu_data.length > 0) {
            var length = adult_menu_data.length;
            for (var i = 0; i < length; i++) {
                var adultMenu = adult_menu_data[i];
                var parentMenu = adultMenu.parent;

                _searchMenu({
                    menuData: menu_data,
                    cbCondition: function(menu) {
                        if (menu && menu.id && menu.id === parentMenu.id) {
                            for (var i = 0; i < parentMenu.itemCount; i++) {
                                if (parentMenu.children[i].id == adultMenu.id) {
                                    return;
                                }
                            }

                            if (adultMenu.index == null || adultMenu.id == undefined) {
                                parentMenu.children.push(adultMenu);
                            }
                            else {
                                parentMenu.children.splice(adultMenu.index, 0, adultMenu);
                            }

                            parentMenu.itemCount = parentMenu.children.length;
                        }
                    }
                });
            }

            adult_menu_data = null;
            //removed_adult_cat = "false";
            removed_adult_cat = false;
        }
    }
    
    /**
     * 성인 카테고리 검색 & 삭제
     */
    function removeAdultCat(oc_updated) {
        //if (!oc_updated  && removed_adult_cat === "true") {
        if (!oc_updated  && removed_adult_cat === true) {
            // [jh.lee] oc_updated 가 true 인 경우는 App 부팅시점이므로 무조건 검사
            return;
        }

        adult_menu_data = _searchMenu({
            allSearch: true,
            menuData: menu_data,
            cbCondition: function(menu) {
                if (menu && menu.catType && menu.catType === "Adult") {
                    var parentMenu = menu.parent;
                    var index = parentMenu.children.findIndex(function(compareMenu) {
                        return menu.id == compareMenu.id;
                    });

                    parentMenu.children.slice(index, 1);
                    parentMenu.itemCount = parentMenu.children.length;

                    menu.index = index;

                    return true;
                }
            }
        });

        //removed_adult_cat = "true";
        removed_adult_cat = true;
    }
    
    /**
     * 부팅 후 OC 업데이트로 Bmt 히든메뉴를 생성하는 함수
     * 부팅 때는 서버에 요청해서 만들지만, 그 후에는 이미 서버에서 요청한 Bmt 히든메뉴데이터를 사용
     *
     * 1-depth 메뉴 리스트에 추가한다.
     */
    function makeBmtHiddenMenu() {
        log.printDbg("makeBmtHiddenMenu");
        var cat;
        var length;
        
        cat = new VCategory("0",KTW.managers.data.MenuDataManager.MENU_ID.BMT_HIDDEN_MENU,"Hidden Menu", null,"false","0","false","","","","","VC", hidden_list.length ,null,"false","false","false","","","","","","","","","false","","","","","","","","","","","","","","","","","","","","","","");
        cat.children = util.getVCategoryList("ITEM_DETAIL", hidden_list, cat);
        cat.itemCount = hidden_list.length;

        menu_arr.push(cat);
        // [jh.lee] 4400 은 오늘의 추천 메뉴. 해당 메뉴의 가장 마지막에 히든 메뉴를 추가한다.
        //for (var i = 0; i < menu_arr.length; i++) {
        //    if (menu_arr[i].id === "4400") {
        //        menu_arr[i].children[menu_arr[i].children.length] = cat;
        //        menu_arr[i].itemCount = menu_arr[i].children.length;
        //        break;
        //    }
        //}
    }
    
    function onResponseItemList(result, data) {
        log.printDbg("onResponseItemList()", LOG_PREFIX);

        if (result && data) {
            try {
                hidden_list = data.itemDetailList;

                makeBmtHiddenMenu();
            }
            catch (e) {
                hidden_list = null;
            }
        }

        createOriginalData();
    }
    
    /**
     * [jh.lee] Passive_Standby Check UHD 전용
     */
    function checkPassiveStandBy() {
        var supported_power_states = hwAdapter.getSupportedPowerStates();

        log.printDbg("supported_power_states ========== " + JSON.stringify(supported_power_states), LOG_PREFIX);
        
        if(jQuery.inArray(DEF.CONFIG.POWER_MODE.PASSIVE_STANDBY, supported_power_states) > 0){
            log.printDbg("is_support_passive_standby == true", LOG_PREFIX);
            return true;
        }

        log.printDbg("is_support_passive_standby == false", LOG_PREFIX);
        return false;
    }

    /**
     * Storage 에 저장된 메뉴 관련 변수값이 변경되었을때 호출되는 리스너 함수
     * @deprecated
     */
    /*
    function onChangeMenuData(key) {
        if (key === storageManager.KEY.MENU_LOCK_ADULT) {
            // [jh.lee] 성인19 메뉴 데이터가 변경되었을 경우
            // [jh.lee] 성인19 메뉴에서 메뉴데이터가 변경되는 경우

            var adult_menu_lock = storageManager.ps.load(storageManager.KEY.MENU_LOCK_ADULT);

            if (removed_adult_cat === "true" && adult_menu_lock === "false") {
                // [jh.lee] 현재 성인19메뉴가 제거되어있는 상태인데 성인19메뉴를 활성화 시키는 경우
                addAdultCat();
            } else if (removed_adult_cat === "false" && adult_menu_lock === "true") {
                // [jh.lee] 현재 성인19메뉴가 활성화되어있는 상태인데 성인19메뉴를 비활성화 시키는 경우
                removeAdultCat();
            }

            // [jh.lee] 메뉴데이터가 변경되었다는 사실을 등록된 리스너로 전달.
            // [jh.lee] TODO 등록된 리스너를 구분하여 전달하는 코드 추가 필요.
            notifyMenuDataChange();

        }
    }
    */
    
    /**
     * oipf configuration 값이  변경되었을때 호출되는 리스너 함수
     */
    function onChangeMenuData(evt) {
        log.printDbg("onChangeMenuData(), evt.key = " + evt.key);
        if (evt !== undefined && evt !== null) {
            log.printDbg("textChanged() evt.key : " + evt.key + " , evt.value : " + evt.value);
            if (evt.key === KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY) {
                // 2017.08.31 dhlee
                // evt.value === "true" : 성인 메뉴 노출
                // evt.value === "false" : 성인 메뉴 숨김
                // 2.0 에서는 별도 local storage에 저장하고 있었으나 3.0 에서는 Configuration 으로 통합되었다.
                // 2.0 에서는 설정 메뉴 자체가 의미상 반대였음
                //var adult_menu_lock = evt.value;
                displayAdult = (evt.value === "true");
                if (displayAdult === true && removed_adult_cat === true) {
                    addAdultCat();
                } else if (displayAdult === false && removed_adult_cat === false) {
                    removeAdultCat();
                }
                /*
                if (removed_adult_cat === "true" && adult_menu_lock === "false") {
                    // [jh.lee] 현재 성인19메뉴가 제거되어있는 상태인데 성인19메뉴를 활성화 시키는 경우
                    addAdultCat();
                } else if (removed_adult_cat === "false" && adult_menu_lock === "true") {
                    // [jh.lee] 현재 성인19메뉴가 활성화되어있는 상태인데 성인19메뉴를 비활성화 시키는 경우
                    removeAdultCat();
                }
                */
                // 2016.12.20 dhlee 필요시 리스너를 구분하여 전달하도록 변경한다.
                notifyMenuDataChange(KTW.managers.data.MenuDataManager.UPDATE.LOCK_ADULT);
            }
            else if (evt.key === KTW.oipf.Def.CONFIG.KEY.LANGUAGE) {
                if (evt.value === null || evt.value === undefined || evt.value.length <= 0) {
                    language = "kor"
                } else {
                    language = evt.value;
                }
            }
        }
    }

    /**
     * 2017.06.26 dhlee
     * Local Storage 데이터 변경 시 호출되는 callback
     * 2017.08.30 dhlee
     * Local Storage 변경 시에는 key 가 바로 전달 된다.
     *
     * @param key
     */
    function onChangeStorageData(key) {
        log.printDbg("onChangeStorageData(), key = " + key);

        if (key !== undefined && key !== null) {
            if (key === storageManager.KEY.CUST_CUG_INFO) {
                var list_length = 0;
                var value;
                var data;
                // [jh.lee] cug 업데이트로 데이터가 변경됨.
                //cug_update_flag = true;

                try {
                    data = JSON.parse(storageManager.ps.load(storageManager.KEY.CUST_CUG_INFO));
                } catch(e) {
                    data = undefined;
                }

                value = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL);

                if(data) {
                    // [jh.lee] 업데이트 된 데이터 확인하여 UP에 해당하는 것이 없으면 UP값 초기화
                    if (data.cugList && data.cugList.length) {
                        list_length = data.cugList.length;

                        for (var i = 0; i < list_length; i++) {
                            if (data.cugList[i].linkInfo === value) {
                                break;
                            }

                            if (i+1 === list_length) {
                                // R6 단! 요구사항으로 LCW 추가되어서...현재 설정 값을 무조건 공백으로 하면안됨!
                                //       설정된 값이 LCW 이면 그것으로 세팅
                                if (value === "LCW") {
                                    KTW.oipf.AdapterHandler.basicAdapter.setConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL, "LCW");
                                } else {
                                    KTW.oipf.AdapterHandler.basicAdapter.setConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL, "");
                                }
                            }
                        }
                    } else {
                        // [jh.lee] 이전 CUG와 달라진 경우이기 때문에 초기화면에서 설정한 값을 초기화한다.
                        // R6 단! 요구사항으로 LCW 추가되어서...현재 설정 값을 무조건 공백으로 하면안됨!
                        //       설정된 값이 LCW 이면 그것으로 세팅
                        if (value === "LCW") {
                            KTW.oipf.AdapterHandler.basicAdapter.setConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL, "LCW");
                        } else {
                            KTW.oipf.AdapterHandler.basicAdapter.setConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL, "");
                        }
                    }
                } else {
                    // [jh.lee] 데이터가 없기 때문에 UP값 초기화
                    // R6 단! 요구사항으로 LCW 추가되어서...현재 설정 값을 무조건 공백으로 하면안됨!
                    //       설정된 값이 LCW 이면 그것으로 세팅
                    if (value === "LCW") {
                        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL, "LCW");
                    } else {
                        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL, "");
                    }
                }
            }
        }
    }

    /**
     * [jh.lee] 메뉴데이터가 변경되었을 때 등록한 listener 실행
     */
    function notifyMenuDataChange(event) {
        var length = menu_data_change_listeners.length;
        for (var i = 0; i < length; i++) {
            menu_data_change_listeners[i](event);
        }
    }
    
    /**
     * [jh.lee] 메뉴데이터가 변경되었을 때 실행될 listener 등록
     */
    function _addMenuDataChangeListener(listener) {
        menu_data_change_listeners.push(listener);
    }
    
    /**
     * [jh.lee] 메뉴데이터가 변경되었을 때 실행될 listener 제거
     */
    function _removeMenuDataChangeListener(listener) {
        var index = menu_data_change_listeners.indexOf(listener);
        menu_data_change_listeners.splice(index, 1);
    }
    
    /**
     * [jh.lee] 공지사항 데이터가 변경되었을 때 등록한 listener 실행
     */
    function notifyNoticeDataChange() {
        var length = notice_data_change_listeners.length;
        for (var i = 0; i < length; i++) {
            notice_data_change_listeners[i]();
        }
    }
    
    /**
     * [jh.lee] 공지사항 데이터가 변경되었을 때(실제로는 긴급공지사항 파싱이 완료되었을 때) 실행할 listener 등록
     */
    function _addNoticeDataChangeListener(listener) {
        notice_data_change_listeners.push(listener);
    }
    
    /**
     * [jh.lee] 공지사항 데이터가 변경되었을 때(실제로는 긴급공지사항 파싱이 완료되었을 때) 실행할 listener 제거
     */
    function _removeNoticeDataChangeListener(listener) {
        var index = notice_data_change_listeners.indexOf(listener);
        notice_data_change_listeners.splice(index, 1);
    }
    
    /**
     * [jh.lee] 원본 홈메뉴 데이터
     */
    function _getOriginMenuData() {
        return origin_data;
    }
    /**
     * [jh.lee] 화면에 사용할 실제 홈메뉴 데이터
     * 키즈 모드일 경우, 키즈 메뉴 리턴
     */
    function _getMenuData() {
        return KTW.managers.service.KidsModeManager.isKidsMode() ? [kids_menu_data] : menu_data;
    }

    /**
     * 실제 홈메뉴 데이터
     * @returns {*}
     */
    function _getNormalMenuData() {
        return menu_data;
    }

    /**
     * 키즈 메뉴 데이터
     * @returns {*}
     */
    function _getKidsModeMenuData() {
        return kids_menu_data;
    }
    
    /**
     * [jh.lee] 메뉴 버전을 리턴
     */
    function _getMenuVersion() {
        return menu_version;
    }
    
    /**
     * [jh.lee] 긴급공지 리턴
     */
    function _getEmergencyNotice() {
        return emergency_notice;
    }
    
    /**
     * [jh.lee] 공지사항 데이터 반환
     */
    function _getNoticeData() {
        return notice_data;
    }

    /**
     * 2017.06.14 dhlee
     * 공지 사항 버전 데이터 반환
     *
     * @returns {*}
     */
    function _getNoticeVersion() {
        return notice_version;
    }

    function _isKidsMenuExist () {
        return kids_menu_data ? true : false;
    }

    function _getCurrentMenuLanguage() {
        return language;
    }

    /**
     * 2017.08.31 dhlee
     * 성인 메뉴 노출 여부 설정 값 반환
     *
     * @returns {*}
     * @private
     */
    function _isAdultMenuDisplay() {
        return displayAdult;
    }

    function _getSettingMenuId() {
        return setting_menu_id;
    }

    /**
     * [dj.son] 홈메뉴 1-depth 추천 영역에서 우리집 맞춤 TV 메뉴를 구분해야 하는데,
     *          매번 searchMenu 를 호출하지 않고 메뉴 ID 로 구분하기 위해 따로 menu id 를 저장 후 제공
     */
    function _getMyMenuId () {
        return my_menu_id;
    }

    return {
        initMenuData : _initMenuData,

        isKidsMenuExist: _isKidsMenuExist,
        isKidsMenu : _isKidsMenu,

        getSettingMenuId: _getSettingMenuId,

        getOriginMenuData : _getOriginMenuData,
        getMenuData : _getMenuData,
        getNormalMenuData: _getNormalMenuData,
        getKidsModeMenuData: _getKidsModeMenuData,
        getMenuVersion : _getMenuVersion,
        getNoticeData : _getNoticeData,
        getEmergencyNotice : _getEmergencyNotice,
        getNoticeVersion: _getNoticeVersion,
        getCurrentMenuLanguage: _getCurrentMenuLanguage,
        isAdultMenuDisplay: _isAdultMenuDisplay,

        getMyMenuId: _getMyMenuId,

        addMenuDataChangeListener : _addMenuDataChangeListener,
        removeMenuDataChangeListener : _removeMenuDataChangeListener,
        addNoticeDataChangeListener : _addNoticeDataChangeListener,
        removeNoticeDataChangeListener : _removeNoticeDataChangeListener,

        searchMenu: _searchMenu,

        getKidsHotKeyMenu: function () {
            return kidsHotKeyMenu;
        },
        getKidsSHDataMenu: function () {
            return kidsSHDataMenu;
        }
    }
}());

/**
 * [jh.lee] MyMenu, SettingMenu 에서 사용하는 메뉴 ID
 *          실시간, 4채널 등의 이유로 홈메뉴에서 사용하는 하드코딩 메뉴 ID도 추가
 *
 * TODO 추후 KT & AMOC 와 메뉴 ID 확인 후 수정사항 있으면 수정해야 함
 */
Object.defineProperty(KTW.managers.data.MenuDataManager, "MENU_ID", {
    value: {
        // Setting
        SETTING : "MENU07002", // Config catType 이 없을 경우, 생성되는 VMenu ID

        KIDS_CARE_SETTING : "MENU00800", // 자녀안심 설정
        SETTING_CHANGE_PIN : "MENU00801", // 비밀번호 변경/초기화
        SETTING_PARENTAL_RATING : "MENU00802", // 시청연령 제한
        SETTING_TIME_LIMIT : "MENU00804", // 시청시간 제한
        SETTING_BLOCKED_CHANNEL : "MENU00902", // 시청채널 제한
        //SETTING_KIDS_CARE_CH : "MENU00805", // OTS - 자녀안심 채널 -> 사용 안함

        CHANNEL_SETTING : "MENU00900", // 채널 설정
        SETTING_FAVORITE_CHANNEL : "MENU00901", // 선호 채널
        SETTING_SKIPPED_CHANNEL : "MENU00904", // OTV - 채널 숨김 : OTS - 건너뛰기 채널
        SETTING_CHANNEL_GUIDE : "MENU00903", // 채널정보 표시시간
        SETTING_DESCRIPTIVE_VIDEO_SERVICE : "MENU00906", // 화면 해설 방송
        SETTING_CLOSED_CAPTION : "MENU00907", // 자막 방송
        SETTING_SERIES_AUTO_PLAY: "MENU00908",  // 시리즈 몰아보기

        SYSTEM_SETTING : "MENU01000", // 시스템 설정
        SETTING_ASPECT_RATIO : "MENU01001", // 화면 비율
        SETTING_RESOLUTION : "MENU01002", // 해상도
        SETTING_AUTO_POWER : "MENU01003", // 자동 전원
        SETTING_LOW_POWER : "MENU01022", // 저전력 전원
        SETTING_STANDBY_MODE : "MENU01004", // 자동 대기 모드
        SETTING_SOUND : "MENU01005", // 사운드
        SETTING_GIGA_GINI_AUDIO_OUTPUT: "MENU01027", // 기가지니 only - 오디오 출력 설정 2017.02.28 추가
        SETTING_SPEAKER : "MENU01024", // UHD3 only - 스피커 볼륨 설정 => 음성가이드 볼륨 (메뉴명 변경) (2017.07.06 사용하지 않음: 하나로 합쳐짐)
        SETTING_VOICE_RECOG : "MENU01025", // UHD3 only  - 원거리 음성 인식 => 음성가이드 (메뉴명 변경) (2017.07.06 사용하지 않음: 하나로 합쳐짐
        SETTING_VOICE_GUIDE: "MENU01029",   // UHD3 only - 음성가이드 (기존 2개의 메뉴가 하나로 합쳐짐)
        SETTING_USB : "MENU00606", // 이동식 디스크 연결
        SETTING_BLUETOOTH : "MENU01023", // 블루투스 장치 연결
        SETTING_POSTER_SHAPE : "MENU01028", // VOD 보기 옵션 (2017.03.29 명칭 변경)
        SETTING_ALERT_MESSAGE : "MENU01006", // 알림 메시지
        SETTING_HDMI_CEC : "MENU01015", // HDMI 전원 동기화
        LANGUAGE : "MENU01018", // 기본 언어
        SETTING_SYSTEM_INITIALIZE : "MENU01007", // 전체 초기화
        SETTING_START_CHANNEL : "MENU01012", // 시작 채널
        TRANSPONDER : "MENU01016", // OTS - 중계기
        COLLECT_SIGNAL : "MENU01017", // OTS - 수신품질 측정
        SETTING_SYSTEM_INFO : "MENU01008", // 시스템 정보

        ADMIN_SYSTEM_SETTING : "MENU06000", // OTS Hidden Menu - Admin System Setting (SKY_LNB ~ SKY_CAS 까지 자식으로 가지고 있는 wrapper menu 같은 메뉴)
        SKY_LNB : "MENU06001", // OTS Hidden Menu - LNB 설정
        SKY_LNB_A : "MENU06010", // OTS Hidden Menu - LNB 설정 - DiSEqC-A
        SKY_LNB_B : "MENU06011", // OTS Hidden Menu - LNB 설정 - DiSEqC-B
        SKY_LNB_C : "MENU06012", // OTS Hidden Menu - LNB 설정 - DiSEqC-C
        SKY_LNB_D : "MENU06013", // OTS Hidden Menu - LNB 설정 - DiSEqC-D
        SKY_EMERGENCY : "MENU06004", // OTS Hidden Menu - 긴급 메시지 알림 설정
        SKY_CHANNEL_INFO : "MENU06005", // OTS Hidden Menu - 채널정보 출력설정
        SKY_CLOSED_CAPTION : "MENU06002", // OTS Hidden Menu - Closed 캡션 설정
        SKY_CAS : "MENU06006", // OTS Hidden Menu - CAS 정보

        SETTING_BEACON : "MENU01026",   // UHD3 - OTS Hidden Menu - 비콘 송신 설정


        // 우리집 맞춤 TV
        MY_MENU : "MENU00100", // 1 depth이면서 "SubHome" catType 이 없을 경우, 생성되는 VMenu ID

        MY_MENU_1 : "T000000000006",    // 우리집 맞춤 TV - 마이 메뉴 1
        WATCHED_LIST : "MENU00601", // 우리집 맞춤 TV - 최근 시청 목록
        FAVORITE_VOD_LIST : "MENU01200", // 우리집 맞춤 TV - 찜한 목록
        FAVORITE_MENU : "T000000000013", // 우리집 맞춤 TV - 즐겨 찾는 메뉴
        MY_MENU_2 : "T000000000014",    // 우리집 맞춤 TV - 마이 메뉴 2
        RECOMMEND_VOD : "MENU07003", // 우리집 맞춤 TV - 당신을 위한 VOD 추천

        // 마이 메뉴 1 하위 메뉴들
        MSG_BOX : "MENU00700",   // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스
        TV_COUPON : "T000000000008",   // 우리집 맞춤 TV - 마이 메뉴 1 - TV 쿠폰
        KT_MEMBERSHIP : "T000000000009",   // 우리집 맞춤 TV - 마이 메뉴 1 - KT 멤버십
        POINT : "MENU00605", // 우리집 맞춤 TV - 마이 메뉴 1 - TV 포인트
        CONTENT_COUPON: "T000000000010", // 우리집 맞춤 TV - 마이 메뉴 1 - 컨텐츠 이용권

        // 알림 박스 하위 메뉴들
        MSG_NOTICE :"MENU00608", // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스 - 공지사항
        MSG_ALERT :"MENU00710", // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스 - 알림
        MSG_EVENT :"MENU00711", // 우리집 맞춤 TV - 마이 메뉴 1 - 알림 박스 - 이벤트

        // 마이 메뉴 2 하위 메뉴들
        MY_VOD_COLLECTION : "MENU00610", // 우리집 맞춤 TV - 마이 메뉴 2 - 마이 소장용VOD (2017.02.08 메뉴명 변경)
        MY_PLAY_LIST : "MENU00603", // 우리집 맞춤 TV - 마이 메뉴 2 - 마이 플레이리스트
        PURCHASED_LIST_MENU : "MENU00602", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매 내역 보기
        SUBSCRIPTION_INFOMATION : "MENU01400", // 우리집 맞춤 TV - 마이 메뉴 2 - 가입 정보

        // 구매 내역 보기 하위 메뉴들
        PURCHASED_LIST : "MENU00611", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매내역 보기 - VOD 구매목록
        OTN_LIST : "MENU00609", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매내역 보기 - 모바일 공유목록
        MOVIE_CHOICE_PURCHASED_LIST : "MENU00612", // 우리집 맞춤 TV - 마이 메뉴 2 - 구매내역 보기 - 무비초이스 구매목록

        // 당신을 위한 VOD 추천 하위 메뉴들
        MY_RECOMMEND_ONLY_MINE : "MENU07004", // 우리집 맞춤 TV - 당신을 위한 VOD 추천 - 나의 취향 알아보기
        MY_CURATION : "MENU07005", // 우리집 맞춤 TV - 당신을 위한 VOD 추천 -  내가 평가한 콘텐츠

        COUPON_POINT: "MENU00607",  // 쿠폰, 포인트 등록/충전 메뉴


        // Channel Guide
        CHANNEL_GUIDE : "MENU00000" ,               // ChSub catType 이 없을 경우, 생성되는 VMenu ID
        TODAY_CHANNEL_GUIDE : "MENU00013",          // 오늘의 채널 가이드
        ENTIRE_CHANNEL_LIST : "MENU00001",          // 전체 채널
        UHD_CHANNEL : "MENU00012",                  // OTV - UHD 채널
        SKYLIFE_UHD_CHANNEL : "MENU00002",          // OTS - UHD 채널
        MOVIE_CHOICE : "MENU00003",                 // OTS - 무비 초이스
        MY_FAVORITED_CHANNEL : "MENU00006",         // 선호 채널
        AUDIO_CHANNEL : "MENU00004",                // 오디오 채널
        COMMUNITY_CHANNEL : "MENU00005",            // 우리만의 채널
        TV_APP_CHANNEL : "MENU00010",               // TV 앱 / 쇼핑 채널
        OLLEH_TV_CHANNEL : "MENU00009" ,            // OTS - olleh tv
        BROADCAST_RESERVATION_LIST : "MENU00007",   // 방송 예약 목록
        WATCH_4CHANNEL : "T000000000003",           // 4채널 동시 시청
        REAL_TIME_CHANNEL : "T000000000004",        // 실시간 인기 채널

        PROGRAM_SEARCH : "MENU00008",               // OTS - 프로그램 검색
        PROGRAM_MOVIE : "MENU09001",                // OTS 프로그램 - 영화/드라마
        PROGRAM_NEWS : "MENU09002",                 // OTS 프로그램 - 뉴스/시사
        PROGRAM_ENTERTAINMENT : "MENU09003",        // OTS 프로그램 - 쇼/오락
        PROGRAM_SPORTS : "MENU09004",               // OTS 프로그램 - 스포츠
        PROGRAM_KIDS : "MENU09005",                 // OTS 프로그램 - 어린이/청소년
        PROGRAM_MUSIC : "MENU09006",                // OTS 프로그램 - 음악/무용
        PROGRAM_ART : "MENU09007",                  // OTS 프로그램 - 예술/문화
        PROGRAM_SOCIETY : "MENU09008",              // OTS 프로그램 - 사회/정치/경제
        PROGRAM_EDU : "MENU09009",                  // OTS 프로그램 - 교육/과학/정보
        PROGRAM_LEISURE : "MENU09010",              // OTS 프로그램 - 여가/취미
        PROGRAM_ETC : "MENU09011",                  // OTS 프로그램 - 기타

        GENRE_CHANNEL : "MENU00011",                // OTV - 장르별 채널
        GENRE_TERRESTRIAL : "MENU09101",            // OTV 장르 - 지상파/종편/홈쇼핑
        GENRE_DRAMA : "MENU09102",          // OTV 장르 - 드라마/오락/음악
        GENRE_MOVIE : "MENU09103",                  // OTV 장르 - 영화/시리즈
        GENRE_SPORTS : "MENU09104",                 // OTV 장르 - 스포츠/레져
        GENRE_ANIMATION : "MENU09105",                   // OTV 장르 - 애니/유아/교육
        GENRE_DOCUMENTARY : "MENU09106",            // OTV 장르 - 다큐/교양
        GENRE_NEWS : "MENU09107",                   // OTV 장르 - 뉴스/경제
        GENRE_SOCIAL : "MENU09108",                 // OTV 장르 - 공공/공익/정보
        GENRE_RELIGION : "MENU09109",                // OTV 장르 - 종교/오픈,

        // 실시간 인기 채널 하위 메뉴들
        POPULAR_ALL : "MENU09110",          // 실시간 인기 채널 - 전체
        POPULAR_TERRESTRIAL : "MENU09111",          // 실시간 인기 채널 - 지상파/종편
        POPULAR_HOMESHOPPING : "MENU09112",         // 실시간 인기 채널 - 홈쇼핑
        POPULAR_SPORTS : "MENU09113",               // 실시간 인기 채널 - 스포츠/레져
        POPULAR_MOVIE : "MENU09114",                // 실시간 인기 채널 - 영화/시리즈
        POPULAR_DRAMA : "MENU09115",                // 실시간 인기 채널 - 드라마/여성
        POPULAR_ANIMATION : "MENU09116",            // 실시간 인기 채널 - 애니
        POPULAR_KIDS : "MENU09117",                 // 실시간 인기 채널 - 유아/교육
        POPULAR_ENTERTAINMENT : "MENU09118",        // 실시간 인기 채널 - 오락/음악
        POPULAR_NEWS : "MENU09119",                 // 실시간 인기 채널 - 뉴스/경제
        POPULAR_DOCUMENTARY : "MENU09120",          // 실시간 인기 채널 - 다큐/교양


        // 1-Depth 검색 메뉴
        SEARCH : "T000000000005",    // Srch catType 이 없을 경우, 생성되는 VMenu ID


        // Kids 관련 메뉴
        KIDS_CHANNEL: "MENU08001",
        KIDS_PLAY_LIST: "MENU08002",


        // TV인터넷
        TV_INTERNET: "T000000000001",
        SKY_TOUCH: "T000000000002",

        // 유튜브 메뉴
        YOUTUBE: "T000000000101",

        // 유튜브 키즈 메뉴 (2017.09.04)
        YOUTUBE_KIDS: "T000000000102",

        // BMT Hidden Menu
        BMT_HIDDEN_MENU: "0470"

    },
    writable: false,
    configurable: false
});

Object.defineProperty(KTW.managers.data.MenuDataManager, "UPDATE", {
    value: {
        ORIGIN : 0,
        LOCK_ADULT : 1,
        CUG : 2
    },
    writable: false,
    configurable: false
});