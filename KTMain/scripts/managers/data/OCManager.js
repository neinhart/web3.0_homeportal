"use strict";

/**
 * DM서버에서 받아오는 데이터 관리를 목적으로 하는 클래스.
 *
 * TODO 추가로 ch_iframe.zip 내 share_ch_iframe_map.json (공용 채널 아이프레임 이미지 들에 대한 매핑 정보) 를 처리 & 제공 로직 추가해야 함
 */
KTW.managers.data.OCManager = (function () {
	var log = KTW.utils.Log;

    var LOCAL_PATH = "./assets/oc";
    var DATA = {
        MENU: {
            PATH: "w3_menu.js",
            VERSION: 0,
            URL: ""
        },
        SUB_HOME: {
            PATH: "w3_subhome.js",
            VERSION: 0,
            URL: ""
        },
        FILTER: {
            PATH: "w3_filter.js",
            VERSION: 0,
            URL: ""
        },
        MODULE_VERSION: {
            PATH: "w3_module_version.json",
            VERSION: 0,
            URL: ""
        },
        NOTICE: {
            PATH: "w3_notice.txt",
            VERSION: 0,
            URL: ""
        },
        UNICAST: {
            PATH: "w3_unicast_version.json",
            VERSION: 0,
            URL: ""
        },
        VS: {
            PATH: "w3_vs.dat",
            VERSION: 0,
            URL: ""
        },
        WM: {
            PATH: "wm.properties",
            VERSION: 0,
            URL: ""
        }
    };
	
	// OC 로 받은 데이터의 원본 txt 저장하는 변수들
    // TODO 메뉴 데이터, 서브홈 데이터는 사이즈가 커서 메모리 이슈가 있을 수 있음. 일단 저장하고 문제시 삭제
    var menu_data = undefined;
    var notice_data = undefined;
    var vs_data = undefined;
    var wm_properties_data = undefined;
    var subHomeData = undefined;
    var filterData = undefined;
    var moduleVersionData = undefined;
    var unicastVersionData = undefined;
    var packageInfoData = undefined;
    var packageMapData = undefined;

    // app 부팅 시 menu 를 받아왔는지 확인하기 위한 변수
    var complete_menu = false;
    // app 부팅 시 subhome 을 받아왔는지 확인하기 위한 변수
    var complete_subhome = false;
    // app 부팅 시 unicast_version 을 받아왔는지 확인하기 위한 변수
    var complete_unicast_version = false;
    var retry_count_unicast = 0;
    

    // oc update 되면 notify 알림을 받기 위해 외부 모듈에서 등록하는 listener  
    var onOCUpdateListener = new KTW.utils.Listener();

    /**
     * [dj.son] 비즈 상품에서 메뉴 업데이트 시, 분산 처리를 위한 flag 변수 (곧바로 호출하지 않고 홈 메뉴가 뜰때 업데이트 로직 호출)
     */
    var bUpdateBizMenu = true;

    /* 2017.04.20 dhlee
     * 전체 펌웨어에 아래 Configuration key 가 적용되면 아래 init()을 사용하도록 한다.
     */
    function _init() {
        log.printDbg('init()');

        var atts_path = null;
        //var atts_locator = null;
        var atts_component_tag = null;

         var atts_locator = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.INFO_ATTS_LOCATOR);
         log.printDbg("_init(), atts_locator = " + atts_locator);

        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            atts_component_tag = KTW.DATA.ATTS.LIVE.COMPONENT_TAG;
        }
        else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT) {
            atts_component_tag = KTW.DATA.ATTS.BMT.COMPONENT_TAG;
        }
        else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.TEST) {
            atts_component_tag = KTW.DATA.ATTS.TEST.COMPONENT_TAG;
        }

        atts_path = KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LOCAL ?
            LOCAL_PATH : atts_locator + "." + atts_component_tag;

        if (KTW.PLATFORM_EXE === "PC") {
            atts_path = LOCAL_PATH;
        }

        DATA.MENU.URL = atts_path + "/" + DATA.MENU.PATH;
        DATA.SUB_HOME.URL = atts_path + "/" + DATA.SUB_HOME.PATH;
        DATA.FILTER.URL = atts_path + "/" + DATA.FILTER.PATH;
        DATA.MODULE_VERSION.URL = atts_path + "/" + DATA.MODULE_VERSION.PATH;
        DATA.NOTICE.URL = atts_path + "/" + DATA.NOTICE.PATH;
        DATA.UNICAST.URL = atts_path + "/" + DATA.UNICAST.PATH;
        DATA.VS.URL = atts_path + "/" + DATA.VS.PATH;
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LOCAL) {
            DATA.WM.URL = atts_path + "/" + DATA.WM.PATH;
        }
        else {
            DATA.WM.URL = atts_locator + ".c/" + DATA.WM.PATH;
        }

        KTW.managers.data.OCImageDataManager.init();
    }

    /**
     * w3_unicast_version.json 획득 후 package 코드 있는지 검사
     * - package 코드가 존재하면 해당 url 로부터 package map 다운로드
     * - 없거나 잘못된 데이터일 경우, 부팅 과정이면 다시 풀패키지 모드로 전환하고 그 외에는 그냥 기존 데이터 사용
     */
    function getPackageMap (options) {
        options = options || {};

        log.printInfo("getPackageMap() " + bUpdateBizMenu + ", " + options.forced);

        /**
         * [dj.son] 비즈 상품에서 메뉴 업데이트시, 분산 처리를 위한 지연 로직 추가 
         */
        if (!bUpdateBizMenu && !options.forced) {
            log.printInfo("bUpdateBizMenu is false and options.forced is false... so return...");
            return;
        }
        bUpdateBizMenu = false;

        var obj = null;

        try {
            obj = JSON.parse(options.data);
        }
        catch (e) {
            log.printErr(e);
        }

        if (obj && obj.packageCode && obj.packageCode.downloadUrl && obj.packageCode.downloadUrl !== "") {
            if (!packageInfoData || packageInfoData.version !== obj.packageCode.version) {
                packageInfoData = obj.packageCode;
                attach(packageInfoData.downloadUrl);
            }
        }
        else {
            log.printErr("pagkage info is not valid...");

            /**
             * [dj.son] Biz 메뉴 가져오는 도중 error 발생시 그냥 기본 메뉴 (채널 가이드 세팅 등) 만 노출하도록 수정
             */
            if (KTW.managers.service.StateManager.appState === KTW.CONSTANT.APP_STATE.INITIALIZED) {
                handleMenuJS("var arr0 = []; var curVer = null;");
                handleSubHomeJS("");
                handleFilterJS("");
            }
        }
    }

    /**
     * package map 획득 후 데이터 저장 & path 설정 후 메뉴 & 서브홈 & 필터 데이터 가져오기
     */
    function updatePackageMap (data) {
        log.printInfo("updatePackageMap()");

        var obj = null;

        try {
            obj = JSON.parse(data);
        }
        catch (e) {
            log.printErr(e);
        }

        /**
         * [dj.son] 데이터 유효성 검사 후 저장
         */
        if (obj && obj.subhome && obj.filter && obj.bizMenu) {
            /**
             * [dj.son] 자신에게 맞는 패키지 정보 찾은 후 path 설정, 이후 attach
             */
            var custEnv = null;
            var pkgCode = null;

            try {
                custEnv = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CUST_ENV));
            }
            catch (e) {
                log.printErr(e);
            }

            if (custEnv && custEnv.pkgCode) {
                pkgCode = custEnv.pkgCode;
            }

            var targetBizMenuInfo = null;
            var orgBizMenuInfo = null;

            for (var i = 0; i < obj.bizMenu.length; i++) {
                var item = obj.bizMenu[i];

                if (item[pkgCode]) {
                    targetBizMenuInfo = item[pkgCode];
                    break;
                }
            }
            if (packageMapData) {
                for (var i = 0; i < packageMapData.bizMenu.length; i++) {
                    var item = packageMapData.bizMenu[i];

                    if (item[pkgCode]) {
                        orgBizMenuInfo = item[pkgCode];
                        break;
                    }
                }
            }

            if (targetBizMenuInfo && targetBizMenuInfo.downloadUrl) {
                /**
                 * [dj.son] 버전 체크 후 attach
                 */
                if (!orgBizMenuInfo || orgBizMenuInfo.version !== targetBizMenuInfo.version) {
                    packageMapData = obj;

                    DATA.MENU.URL = targetBizMenuInfo.downloadUrl;
                    DATA.SUB_HOME.URL = packageMapData.subhome.downloadUrl;
                    DATA.FILTER.URL = packageMapData.filter.downloadUrl;

                    /**
                     * [dj.son] 부팅때를 제외하고는 notify 하도록 수정
                     */
                    attach(DATA.MENU.URL, function () {
                        if (KTW.managers.service.StateManager.appState !== KTW.CONSTANT.APP_STATE.INITIALIZED) {
                            onOCUpdateListener.notify(DATA.MENU.URL);
                        }
                    });
                    attach(DATA.SUB_HOME.URL, function () {
                        if (KTW.managers.service.StateManager.appState !== KTW.CONSTANT.APP_STATE.INITIALIZED) {
                            onOCUpdateListener.notify(DATA.SUB_HOME.URL);
                        }
                    });
                    attach(DATA.FILTER.URL, function () {
                        if (KTW.managers.service.StateManager.appState !== KTW.CONSTANT.APP_STATE.INITIALIZED) {
                            onOCUpdateListener.notify(DATA.FILTER.URL);
                        }
                    });
                }
            }
            else {
                log.printErr("package map data is not vaild....");

                /**
                 * [dj.son] Biz 메뉴 가져오는 도중 error 발생시 그냥 기본 메뉴 (채널 가이드 세팅 등) 만 노출하도록 수정
                 */
                if (KTW.managers.service.StateManager.appState === KTW.CONSTANT.APP_STATE.INITIALIZED) {
                    handleMenuJS("var arr0 = []; var curVer = null;");
                    handleSubHomeJS("");
                    handleFilterJS("");
                }
            }
        }
        else {
            log.printErr("package map data is not vaild....");

            /**
             * [dj.son] Biz 메뉴 가져오는 도중 error 발생시 그냥 기본 메뉴 (채널 가이드 세팅 등) 만 노출하도록 수정
             */
            if (KTW.managers.service.StateManager.appState === KTW.CONSTANT.APP_STATE.INITIALIZED) {
                handleMenuJS("var arr0 = []; var curVer = null;");
                handleSubHomeJS("");
                handleFilterJS("");
            }
        }
    }

	/**
	 * attch요청
	 */
	function attach (path, callback, attachRetryCount) {
		log.printInfo("OCManager attach start path:" + path);
		
		var ajaxReq = null;
		ajaxReq = $.ajax({
			url: path,
			dataType: "text",
			cache: true,
//			crossDomain: true,
			beforeSend: function(xhr) {
				if (path.indexOf(".zip") != -1) {
					xhr.overrideMimeType("text/plain; charset=x-user-defined");
				}
			},
			statusCode: {
				404: function() {
					log.printDbg("Status 404: page not found");
				}
			}
		})
		.done(function(data) {
			log.printDbg("done!!");
			log.printDbg("data: " + data);
			handleResponse(data, path);
			if (callback != null) {
				callback(path);
			}
		})
		.fail(function(jqXHR, textStatus) {
			log.printDbg("fail!! ? = " + textStatus);
			
			if (attachRetryCount === undefined) {
			    attachRetryCount = 0;
			}	
			if (attachRetryCount < 100) {
                setTimeout( function() {
                	log.printDbg("OCManager attach Retry : " + attachRetryCount + ', path: ' + path);

                	attachRetryCount++;
                	// watermark 파일은 update 실패하면 급한대로 local data 사용
                    if (path === DATA.WM.URL) {
                        path = LOCAL_PATH + '/' + DATA.WM.PATH;
                    }
                	// notice 및 show_window_switch의 경우
                	// hidden menu인 appinfo에서 oc정보를 표시하기 위해 아래 동작 수행
                	else if (path === DATA.NOTICE.URL) {
                		if (notice_data == null) {
                			notice_data = 'fail[' + jqXHR.status + ']';
                		}
                	}
                    else if (path === DATA.UNICAST.URL) {
                        if (KTW.CONSTANT.IS_BIZ && KTW.managers.service.StateManager.appState === KTW.CONSTANT.APP_STATE.INITIALIZED && attachRetryCount > 10) {
                            getPackageMap();
                        }
                    }
                    else if (packageInfoData && packageInfoData.downloadUrl && path === packageInfoData.downloadUrl) {
                        if (KTW.CONSTANT.IS_BIZ && KTW.managers.service.StateManager.appState === KTW.CONSTANT.APP_STATE.INITIALIZED && attachRetryCount > 10) {
                            updatePackageMap();
                        }
                    }

                    // memory 개선을 위해 attachRetryCount 변수를 전역변수에서 제거하고
                	// attach() parameter에 추가하여 호출할 때 값을 넣어 호출하도록 수정함.
                    attach(path, callback, attachRetryCount);
                }, 5000);
            }
		})
		.always();
	};
	
	/**
	 * XMLHttpRequest 에 대한 response 를 처리한다.
	 * xhr.responseText 를 통해 서버에서 받은 data 를 처리한다.
	 * 서버와 통신을 성공한 경우(xhr.status == 200) 내용을 update 하고 local data 도 같이 update 한다.
	 * 서버와 통신을 실패한 경우(xhr.status == 0) local data 에서 data 를 가져온다.
	 * local data를 가져오는 경우도 xhr.status == 0 이다.
	 * 서버와 통신을 실패한 경우 xhr.responseText = ""로 온다.
	 */
	function handleResponse(data, path) {
		log.printInfo("##### handleResponse complete #####");
		
		if (data !== "") {
		    log.printDbg("Success to load data from " + path);
		    
	        if (path === DATA.MENU.URL || path === LOCAL_PATH + '/' + DATA.MENU.PATH) {
	            menu_data = data;
                handleMenuJS(menu_data);
                // [jh.lee] 테스트를 위해 임시로 주석처리
                //KTW.managers.data.MenuDataManager.notifyMenuDataChanged();
	        }
	        else if (path === DATA.NOTICE.URL || path === LOCAL_PATH + '/' + DATA.NOTICE.PATH) {
	            notice_data = data;
                //checkENotice();
	        }
	        else if (path === DATA.VS.URL || path === LOCAL_PATH + '/' + DATA.VS.PATH) {
                if (data) {
                    vs_data = data;
                }
                else {
                    vs_data = "실시간 인기채널 1위-9위|dvb://5.1B9.1B9\n실시간 인기채널 10위-18위|dvb://5.1BB.1BB\n실시간 인기채널 19위-27위|dvb://5.1BE.1BE";
                }
	        }
	        else if (path === DATA.UNICAST.URL || path === LOCAL_PATH + '/' + DATA.UNICAST.PATH) {
                retry_count_unicast = 0;
                unicastVersionData = data;

                // test
                // try {
                //     var temp = JSON.parse(data);
                //     temp.packageCode = {"version":"201711061509","downloadUrl":"http://bizmenubmt.ktipmedia.co.kr:8080/w3_package_code_map/w3_package_code_map.json"};
                //     if (window.testVal) {
                //         temp.packageCode.version = (201711061509 + window.testVal) + "";
                //         ++window.testVal;
                //     }
                //     else {
                //         temp.packageCode.version = "201711061509";
                //         window.testVal = 1;
                //     }
                //     unicastVersionData = JSON.stringify(temp);
                // }
                // catch (e) {}

                if (KTW.CONSTANT.IS_BIZ) {
                    if (bUpdateBizMenu) {
                        getPackageMap({data: unicastVersionData});
                    }
                    else {
                        bUpdateBizMenu = true;
                    }
                }

                updateVersion(data);
	        }
	        else if (path === DATA.WM.URL || path === LOCAL_PATH + '/' + DATA.WM.PATH) {
	            wm_properties_data = data;
	        }
            else if (path === DATA.SUB_HOME.URL || path === LOCAL_PATH + '/' + DATA.SUB_HOME.PATH) {
                subHomeData = data;
                handleSubHomeJS(subHomeData);
            }
            else if (path === DATA.FILTER.URL || path === LOCAL_PATH + '/' + DATA.FILTER.PATH) {
                filterData = data;
                handleFilterJS(filterData);
            }
            else if (path === DATA.MODULE_VERSION.URL || path === LOCAL_PATH + '/' + DATA.MODULE_VERSION.PATH) {
                moduleVersionData = data;
            }
            else if (packageInfoData && packageInfoData.downloadUrl && path === packageInfoData.downloadUrl) {
                updatePackageMap(data);
            }
		}
		else {
		    log.printWarn("Failed to load data from " + path);
		    
		    if (path === DATA.MENU.URL || path === LOCAL_PATH + '/' + DATA.MENU.PATH) {
		        if (!complete_menu) {
                    setTimeout(function(){
                        attach(DATA.MENU.URL);
                    }, 5000);
                }
		    }
            else if (path === DATA.SUB_HOME.URL || path === LOCAL_PATH + '/' + DATA.SUB_HOME.PATH) {
                if (!complete_subhome) {
                    setTimeout(function(){
                        attach(DATA.SUB_HOME.URL);
                    }, 5000);
                }
            }
		    else if (path === DATA.UNICAST.URL || path === LOCAL_PATH + '/' + DATA.UNICAST.PATH) {
		        // 3~10분 정도의 짧은 시간마다 3회 정도 retry
                
                var delay = complete_unicast_version ? 180000 : 3000,
                    retry = complete_unicast_version ? 3 : 100;

                if (KTW.CONSTANT.IS_BIZ && KTW.managers.service.StateManager.appState === KTW.CONSTANT.APP_STATE.INITIALIZED && retry_count_unicast > 10) {
                    getPackageMap();
                }
                
                if (retry_count_unicast < retry) {
                    log.printDbg("Retry loading versions from " + path);
                    retry_count_unicast++;
                    setTimeout(function(){
                        attach(DATA.UNICAST.URL);
                    }, delay);
                }
                else {
                    log.printDbg("Stop to retry loading versions from " + path);
                    retry_count_unicast = 0;
                }
            }
            else if (packageInfoData && packageInfoData.downloadUrl && path === packageInfoData.downloadUrl) {
                updatePackageMap();
            }
		}
		
		log.printInfo("##### End of handleResponse #####");
	}
	/**
	 * menu.js 를 처리하는 부분.
	 */
	function handleMenuJS(data) {
        log.printInfo("### evaluating menu.js ###");
        
        var func1 = "this.getData = function() { return arr0; };";
        var func2 = "this.getVersion = function() { return curVer; };";
        eval("KTW.data.Menu = function () { " + data + func1 + func2 + " };")
        
    	complete_menu = true;
	}
    /**
     * subhome.js 를 처리하는 부분.
     */
    function handleSubHomeJS(data) {
        log.printInfo("### evaluating subhome.js ###");

        KTW.managers.data.SubHomeDataManager.initSubHome();

        eval(
            "var setSubHomeVersion = KTW.managers.data.SubHomeDataManager.setSubHomeVersion;" +
            "var addSubHome = KTW.managers.data.SubHomeDataManager.addSubHome;" +
            data
        );

        complete_subhome = true;
    }
    /**
     * filter.js 를 처리하는 부분.
     */
    function handleFilterJS(data) {
        log.printInfo("### evaluating filter.js ###");

        KTW.managers.data.SubHomeDataManager.initFilter();

        eval(
            "var setFilterVersion = KTW.managers.data.SubHomeDataManager.setFilterVersion;" +
            "var addFilter = KTW.managers.data.SubHomeDataManager.addFilter;" +
            data
        );
    }
	
	/**
	 * [dj.son] unicast_version.json 파일에 chIframeZip 정보만 사용
	 */
	function updateVersion(data) {
		log.printInfo("updateVersion()");
		
		if (data == null) {
			log.printWarn("unicastData(version info) is empty!");
			return;
		}

        try {
            var obj = JSON.parse(data);

            if (!complete_unicast_version) {
                // 최초 update 시에만 바로 ch_iframe update를 요청하고 이후에는 OCImageDataManager 자체 timer 에 의해서만 확인
                setTimeout(function () {
                    KTW.managers.data.OCImageDataManager.checkUpdate();
                }, 3 * 1000);
            }

            if(obj.mvnoPng !== undefined && obj.mvnoPng !== null) {
                var imageUrl = obj.mvnoPng.downloadUrl;

                if(imageUrl !== undefined && imageUrl !== null && imageUrl !== "") {
                    KTW.CONSTANT.MVNO_PNG.URL = imageUrl;
                }
            }
        }
        catch (e) {
            log.printErr("unicast_version parse error : " + e.message);
        }

        complete_unicast_version = true;
	}
	

	
	/**
	 * 각 항목별로 업데이트가 필요한지 확인하고,
	 * 업데이트 할 때 어떤 source 에서 data 를 받아올지 판단한다.
	 */
	function _load() {
		log.printInfo("_load()");


        attach(DATA.UNICAST.URL);
        attach(DATA.NOTICE.URL);
        attach(DATA.MODULE_VERSION.URL);
        attach(DATA.WM.URL);
        attach(DATA.VS.URL);

        if (!KTW.CONSTANT.IS_BIZ) {
            attach(DATA.MENU.URL);
            attach(DATA.SUB_HOME.URL);
            attach(DATA.FILTER.URL);
        }
		
		if (KTW.RELEASE_MODE !== "LOCAL") {
		    addObjectChangeListener();
		}
	}
	
	function addObjectChangeListener() {
	    
	    var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
	    
        try {
            basicAdapter.addObjectChangeEventListener(DATA.MENU.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.NOTICE.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.UNICAST.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.WM.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.VS.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.SUB_HOME.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.FILTER.URL, onObjectChangeEvent);
            basicAdapter.addObjectChangeEventListener(DATA.MODULE_VERSION.URL, onObjectChangeEvent);
        } catch (e) {
            log.printExec(e);
            log.printDbg("add listener error .. retry...");
            setTimeout( function() {addObjectChangeListener();}, 5000);
        }
    }
	
	function _addOCUpdateListener(l) {
		onOCUpdateListener.addListener(l);
	}
	function _removeOCUpdateListener(l) {
		onOCUpdateListener.removeListener(l);
	}
	
	function onObjectChangeEvent (event) {
	    log.printDbg("onObjectChangeEvent()");
	    
	    var status = event.status;
	    var source_url = event.sourceUrl;
	    
	    log.printDbg("onObjectChangeEvent() status = " + status);
	    log.printDbg("onObjectChangeEvent() source_url = " + source_url);
	    
	    if (status !== "error") {
            /**
             * [dj.son] 비즈 상품인 경우, menu.js 가 업데이트 될때 getPackageMap 함수를 호출하도록 수정
             * 
             * - 어차피 w3_unicast_version.json 가 업데이트 될때에만 비즈 메뉴가 바뀌므로, 메뉴 데이터가 바뀌는 경우에는 아무것도 안하도록 수정
             */
            if (KTW.CONSTANT.IS_BIZ) {
                if (source_url.indexOf(DATA.MENU.PATH) > -1) {
                    // getPackageMap(unicastVersionData);
                    return;
                }
            }

	    	attach(source_url, function (path) {
	    		onOCUpdateListener.notify(path);
	    	});
        }
	}

	 
	return {
	    get DATA () {
	    	return DATA;
	    },

        init: _init,
        load: _load,

        getMenuData : function() {
            return menu_data;
        },
        getNotice : function() {
            return notice_data;
        },
        getWmPropertiesData : function() {
            return wm_properties_data;
        },
        getVSData : function() {
            return vs_data;
        },
        getSubHomeData : function() {
            return subHomeData;
        },
        getFilterData : function() {
            return filterData;
        },
        getModuleVersionData : function () {
            return moduleVersionData;
        },
        getUnicastVersionData : function () {
            return unicastVersionData;
        },
        getPackageInfoData: function () {
            return packageInfoData;
        },
        getPackageMapData: function () {
            return packageMapData;
        },

        // 최초 부팅시 필수 데이터 (메뉴, 서브홈) attach 했는지 확인하는 함수
        isCompleteInitAttach : function() {
            return complete_menu && complete_subhome;
        },

        addOCUpdateListener: _addOCUpdateListener,
        removeOCUpdateListener: _removeOCUpdateListener,


	    //////////////////////////////
	    // XXX START FOR TEST
	    setUrl : function (name, url) {
            if (DATA[name]) {
                DATA[name].URL = url;
            }
	    },
	    attach: attach,
	    onObjectChangeEvent: onObjectChangeEvent,
	    test: function() {
            attach(DATA.UNICAST.URL);
            attach(DATA.NOTICE.URL, function(){
                onOCUpdateListener.notify(DATA.NOTICE.URL);
            });
            attach(DATA.MENU.URL, function(){
                onOCUpdateListener.notify(DATA.MENU.URL);
            });
            attach(DATA.SUB_HOME.URL);
            attach(DATA.FILTER.URL);
            attach(DATA.MODULE_VERSION.URL);
            attach(DATA.WM.URL);
            attach(DATA.VS.URL);
            attach(DATA.CH_IFRAME.URL);
	    },
	    updateVersion: updateVersion,
	    //checkUpdateChIframe: checkUpdateChIframe,
	    // END FOR TEST
	    ///////////////////////////////

        /**
         * [dj.son] 비즈 메뉴 업데이트
         */
	    updateBizMenu: function (forced) {
            getPackageMap({data: unicastVersionData, forced: forced});
	    },
        isUpdateBizMenu: function () {
	        return bUpdateBizMenu;
        },

        testFunc: function (path) {
            attach(path);
        }
	}
}());
