"use strict";

/**
 * manage User Log 
 * 
 * collect log for user action and navigation and send log data periodically 
 */
KTW.managers.UserLogManager = (function() {

    var RANDOM_COMP_TIME = 60 * 1000; // 1분 보정
    var LOG_SEND_PERIOD = 6 * 60 * 60 * 1000; // 6시간
    var LOG_SEND_LIMIT =  100; // 로그 전송 제한 수 (100개)
    var TEST_LOG_SEND_LIMIT = 1; // 테스트용 로그 전송 제한 수

    var LOG_SERVER_URL = {
        LIVE : "http://homelog.ktipmedia.co.kr:8080/homeportal",
        BMT : "http://homelogbmt.ktipmedia.co.kr:8080/homeportal",
        TEST : ""
    }

    var WORKER_PATH = "scripts/managers/worker/UserLogWorker.js";
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var storageManager = KTW.managers.StorageManager;
    var menuDataManager = KTW.managers.data.MenuDataManager;
    
    var logWorker;
    var working_count = 0;
    
    var log_list = null;
    
    var last_send_time = -1;
    var log_send_timer = null;
    
    var is_running = false;
    
    function _init () {
        log.printDbg("init()");

        if(KTW.USER_LOG_TEST) {
            start();
        } else {
            storageManager.ps.addStorageDataChangeListener(onStorageDataChanged);
        }

    }

    function start () {
        if (is_running === true) {
            return;
        }
        
        log.printDbg("start()");
        
        logWorker = new Worker(WORKER_PATH);
        logWorker.addEventListener("message", function (e) {
            switch (e.data.type) {
                case "res" :
                    if (e.data.method === "create") {
                        // log 생성 시 working_count를 차감
                        working_count--;
                        createLog(e.data.message);
                    }
                    else if (e.data.method === "send") {
                        resetTimer();
                    }
                    return;
                case "debug" :
                    log.printInfo("onWorkerDebug ==> " + e.data.message);
                    var n = e.data.message.indexOf("SAVE_ON_LOCAL_STORAGE");
                    if(n > 0) {
                        KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.USER_LOG_COLLECTOR, e.data.message);
                    }
                    return;
                case "error" :
                    log.printWarn("onWorkerError ==> " + e.data.message);
                    // log 수집대상에서 제외되는 경우에 해당하므로
                    // error 발생 시 working_count를 차감
                    working_count--;
                    return;
            }
            
        }, false);
        logWorker.addEventListener("error", function (e) {
            log.printWarn("onWorkerError ==> " + e.message);
        }, false);

        KTW.managers.data.MenuDataManager.addMenuDataChangeListener(onMenuDataChanged);
        
        log_list = [];
        is_running = true;
        
        // 다시 시작되는 경우 마지막 전송 시간 기준으로
        // 다음 주기 시간을 보정한 후 timer 시작하도록 해야함
        if (last_send_time >= 0) {
            adjustLogSendTime();
            scheduleTimer();
        }
    }
    
    function stop () {
        if (is_running === false) {
            return;
        }
        
        log.printDbg("stop()");
        
        // stop 될 경우 현재 남아 있는 log list 전송
        sendLog(true);
        
        descheduleTimer();
        
        logWorker.terminate();
        logWorker = undefined;

        KTW.managers.data.MenuDataManager.removeMenuDataChangeListener(onMenuDataChanged);

        log_list = null;
        is_running = false;
        working_count = 0;
    }
    
    /**
     * 마지막 log 전송 시간 기준으로 6시간 주기상
     * 가장 빨리 도래하는 시간으로 다시 보정
     */
    function adjustLogSendTime () {
        var current_time = Date.now();
        var diff = current_time - last_send_time;
        if (diff >= LOG_SEND_PERIOD) {
            last_send_time =+ (diff / LOG_SEND_PERIOD) * LOG_SEND_PERIOD;
        }
    }
    
    /**
     * 유형별 로그 수집
     *
     * @param options - logData - type : 로그 유형 (KTW.data.UserLog.TYPE 참조)
     *                             act : 진입 방법
     *                                  1. 진입 키코드 (keyCode 전달)
     *                                  2. 진입 수단 (KTW.data.EntryLog.JUMP.TYPE 참조)
     *                                  3. 메뉴 update (KTW.data.UserLog.MENU_UPDATE 참조)
     *                             catId : 상위 카테고리 id
     *                             catName : 상위 카테고리 name
     *                             contsId : 컨텐츠 id (카테고리 id, 시리즈 id, 대표화질 id)
     *                             contsName : 컨텐츠 name (카테고리 name, 시리즈 name, 단편 name)
     *                             buttonId : 버튼 구분 값
     *                             buttonValue : 버튼 데이터 값
     *                             menuId : 메뉴 ID
     *                             menuName : 메뉴 명
     *                             jumpType: 진입 화면 유형 (KTW.data.EntryLog.JUMP.TYPE 참조)
     *                             locator: 진입 화면 유형이 양방향일 경우 locator 값
     *                             chIdx: 장르별 멀티채널(실시간 인기채널) 순위 index 값
     *                             reqPathCd: VOD 진입 경로 (postMessage 에 의해 진입하는 경우)
     *                             version:
     *                - forced : if true, do not process by worker
     */
    function _collect (logData) {
        log.printDbg("collect user log : "+JSON.stringify(logData));

        if (!is_running) {
            log.printDbg("collect() not running state... so return");
            return;
        }

        if (!logData || !logData.type || !logData.act) {
            log.printDbg("collect() not valid log data... so return");
            return;
        }
   
        //log.printInfo("collect() working count = " + working_count);
        // 현재 worker에 의해 처리되고 있는 개수를 반영
        // 만약 불필요한 log라서 제외되더라도 실제 log_list에 splice 될 때
        // 자연스럽게 빈 index는 채워져서 쌓이게 되므로 문제 없음 
        //var index = log_list.length + working_count;
        var index = log_list.length;
        log.printInfo("collect#" + index + ":(" + log.stringify(logData) + ")");
        
        try {

            // [dj.son] 2.0 특화 시나리오에 의한 보정 로직
            //// JUMP 기능의 경우 act 값은 총 3가지를 고려해야 한다.
            //// 1. VOD 상세화면으로 접근하는 경우(reqPathCd 값 전달)
            ////    VodUtil#showVodDetailLayer()을 통해서 로그 수집
            //// 2. 양방향 app으로 접근하는 경우("" or undefined 값 전달)
            ////    StateManager#changeState()를 통해서 로그 수집
            ////    이 경우 마지막 layer의 id 값을 체크하여 경로 처리하도록 함
            //// 3. postMessage를 통해 VOD 카테고리나 상세화면으로 접근하는 경우
            ////    MessageManager에서 postMessage로 요청이 온 경우 로그 수집
            ////    (hp_showCategory, hp_showContentDetail, hp_showSeriesDetail)
            ////    showCategory의 경우 명시적으로 전달되기 때문에 바로 수집
            ////    이 경우 VodUtil#showVodDetailLayer()을 통해서 요청이 들어오는데
            ////    layer는 존재하지 않기 때문에 의도적으로 JUMP_TYPE과 act를 변경하여 처리함
            ////    TODO 단, layer가 존재하는 경우에도 호출될 수 있기 때문에 구분 필요함
            ////    결국 post message에 의한 처리는 방안을 고민해야 함
            //var layer_id = undefined;
            //
            //if (type === KTW.data.UserLog.TYPE.JUMP_TO && forced !== true) {
            //    var layer = KTW.ui.LayerManager.getLastLayer();
            //    // 만약 현재 참조하는 layer가 loading 이라면
            //    // stack layer로 간주하여 처리하도록 함
            //    if (layer && layer.id === KTW.ui.Layer.ID.LOADING) {
            //        layer = KTW.ui.LayerManager.getLastStackLayer();
            //    }
            //
            //    if (layer) {
            //        log.printInfo("collect#" + index + ": layer = " + layer);
            //
            //        // VodUtil로부터 전달되었는데 마지막 layer가 홈메뉴이고 reqPathCd가
            //        // 홈메뉴에 해당하는 경우에는 이미 수집된 로그이므로 제외하도록 함
            //        if (layer.id === KTW.ui.Layer.ID.HOME_MENU &&
            //            (act === KTW.vod.EntrancePath.HOME_MENU ||
            //                act === KTW.vod.EntrancePath.HOME_RANK)) {
            //            log.printInfo("collect#" + index + ": this is HOME_MENU log... so don't collect log");
            //            return;
            //        }
            //        else {
            //            if (!act) {
            //                // 연관메뉴의 경우에는 layer id 값이 일정하지 않기 때문에
            //                // layer의 view 인스턴스로 확인
            //                if (layer.child instanceof KTW.ui.popup.ContextMenu) {
            //                    act = KTW.data.EntryLog.JUMP.CODE.CONTEXT;
            //                    forced = true;
            //                }
            //                else {
            //                    act = layer.id;
            //                }
            //                log.printInfo("collect#" + index + ": act = " + act);
            //            }
            //            layer_id = layer.id;
            //        }
            //    }
            //}

            logData.timestamp = KTW.utils.util.getCurrentDateTime();

            createLog(logData);

            //working_count++;
            //logWorker.postMessage(["COLLECT", index, type, act, timestamp, data, layer_id]);
            
        }
        catch (e) {
            log.printErr("collect() occured error");
            log.printExec(e);
        }
    }
    
    function createLog (data) {
        log.printInfo("createLog#" + log.stringify(data) + ")");
        
        var collect_log = undefined;
        switch (data.type) {
            case KTW.data.UserLog.TYPE.HOT_KEY :
                collect_log = new KTW.data.UserLog(data);
                break;
            case KTW.data.UserLog.TYPE.MENU_UPDATE :
                collect_log = new KTW.data.UpdateLog(data);
                break;
            case KTW.data.UserLog.TYPE.ETC_MENU :
            case KTW.data.UserLog.TYPE.JUMP_TO :
                collect_log = new KTW.data.EntryLog(data);
                break;
            case KTW.data.UserLog.TYPE.HOME_MENU :
            case KTW.data.UserLog.TYPE.VOD_DETAIL :
                //2017.06.16 sw.nam 모듈 내부 navigation type 코드 반영 (규격서 기준)
            case KTW.data.UserLog.TYPE.SH_MENU :
            case KTW.data.UserLog.TYPE.FH_MENU :
            case KTW.data.UserLog.TYPE.KIDS_MENU :
                collect_log = new KTW.data.NavLog(data);
                break;
        }

        if (!collect_log) {
            log.printInfo("create log failed... re return");
            return;
        }
        
        // 로그 수집 순서대로 쌓이도록 하기 위해서
        // index에 맞게 list에 추가되도록 함
        //log_list.splice(index, 0, collect_log.toJsonObject());
        log_list.push(collect_log.toJsonObject());
        log.printInfo("createLog#" + collect_log.toString());
        
        var log_count = log_list.length;
        var log_limit = LOG_SEND_LIMIT;
        if(KTW.USER_LOG_TEST) {
            log_limit = TEST_LOG_SEND_LIMIT;
        }
        if (log_count >= log_limit) {
            // log가 100개 수집되면 바로 전송
            log.printDbg("createLog() log count reached limit count... so send log data");
            sendLog(true);
        }
        else {
            if (log_count === 1 && last_send_time === -1) {
                // 부팅 후 최초 log가 쌓이는 시점에
                // random 시간 생성 후 1분 보정하여 전송 시간으로 설정
                // oc 및 iframe 등의 다른 update 분산처리 로직과
                // 중첩될 수 있기 때문에 별도로 보정함
                last_send_time = getRandomTimeOnBasedMAC() + RANDOM_COMP_TIME;
                scheduleTimer(true);
            }
        }
    }
    
    function scheduleTimer (is_first) {
        var cur_time = Date.now();
        var timeout = 0;

        if (is_first === true) {
            timeout = last_send_time - cur_time;
        }
        else {
            timeout = last_send_time + LOG_SEND_PERIOD - cur_time;
        }

        log.printDbg("scheduleTimer() last_send_time = " + new Date(last_send_time));
        log.printDbg("scheduleTimer() cur_time = " + new Date(cur_time));
        log.printDbg("scheduleTimer() timeout = " + timeout);
        
        descheduleTimer();
        
        log_send_timer = setTimeout(sendLog, timeout);
    }
    
    function descheduleTimer () {
        if (log_send_timer !== null) {
            log.printDbg("descheduleTimer()");
            
            clearTimeout(log_send_timer);
            log_send_timer = null;
        }
    }
    
    /**
     * log 전송 timer reset
     */
    function resetTimer () {
        scheduleTimer();
    }
    
    function sendLog (forced) {
        log.printDbg("sendLog"+JSON.stringify(forced));
        
        // log list가 유효하지 않은 경우 
        if (log_list === null || log_list === undefined) {
            log.printDbg("sendLog() log data is not valid... don't send log data");
            return;
        }
        
        // log 개수가 "0"인 경우 전송 시도하지 않아야 함
        if (log_list && log_list.length === 0) {
            log.printDbg("sendLog() log count is 0... don't send log data");
            return;
        }
        
        // version 정보는 설정>시스템 정보상 노출되는 것과 동일하게 처리}
        // 2017.09.06 dhlee
        // 버전 정보 규격 통일
        var meta_data = {
            "version" : "W3_"+KTW.UI_VERSION + "_" + KTW.UI_DATE + "-" + KTW.RELEASE_MODE,
            "said" : KTW.SAID,
            "url" : getUrl(),
            "testFlag" : KTW.USER_LOG_TEST
        };

        /*if(KTW.USER_LOG_TEST) {
            log.printDbg("test logging version");
            // 통계로그 수집 테스트용 조건문
            // LOG_TEST_LIMIT 단위로만 key 에 저장하여 로그를 확인
            KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.USER_LOG_COLLECTOR, JSON.stringify(log_list));
        }else {
            logWorker.postMessage(["SEND", meta_data, log_list]);
        }*/
        logWorker.postMessage(["SEND", meta_data, log_list]);
        
        // worker에 array 전달 시 자연스럽게 array copy가 일어남
        // 그러므로 postMessage로 전달한 후 log_list를 초기화 함
        log_list = [];
        
        // 100개 쌓이거나 통계 수집 비활성화 되어 log 전송이 된 경우
        // 해당 전송완료 시점 기준으로 다시 6시간 주기로 처리되어야 하기 때문에 
        // 마지막 전송 시간을 현재 시간으로 설정
        if (forced === true) {
            last_send_time = Date.now();
        }
    }
    
    function onStorageDataChanged (key) {
        if (key === storageManager.KEY.CUST_ENV) {
            log.printDbg("onStorageDataChanged() key = " + key);

            var loadData = storageManager.ps.load(storageManager.KEY.CUST_ENV);
            var cust_env;
            try{
                cust_env = JSON.parse(loadData);
            }catch(e) {
                log.printErr(e);
            }
            if (cust_env) {
                log.printDbg("statYn = "+cust_env.statYn);
                // 통계 수집 대상이면 start 아니라면 stop 처리 
                if (cust_env.statYn === "Y") {
                    start();
                }
                else {
                    stop();
                }
            }
        }else if(key === storageManager.KEY.CUST_CUG_INFO) {
            onMenuDataChanged(KTW.managers.data.MenuDataManager.UPDATE.CUG);
        }
    }

    function onMenuDataChanged(reason) {
        var menu_update_type = undefined;
        switch (reason) {
            case KTW.managers.data.MenuDataManager.UPDATE.ORIGIN :
                // OC update
                menu_update_type = KTW.data.UpdateLog.UPDATE.MENU.OC;
                break;
            case KTW.managers.data.MenuDataManager.UPDATE.LOCK_ADULT :
                // [설정 > 자녀안심 설정 > 시청연령 제한 > 성인19+ 메뉴 숨김] 변경
                menu_update_type = KTW.data.UpdateLog.UPDATE.MENU.ADULT;
                break;
            case KTW.managers.data.MenuDataManager.UPDATE.CUG :
                // [설정 > 시스템 설정 > 시작채널] 변경
                menu_update_type = KTW.data.UpdateLog.UPDATE.MENU.CUG;
                break;

        }

        if (menu_update_type !== undefined) {
            var version = KTW.managers.data.MenuDataManager.getMenuVersion();
            _collect({
                type: KTW.data.UserLog.TYPE.MENU_UPDATE,
                act: menu_update_type,
                version: version
            });
        }
    }

    function getRandomTimeOnBasedMAC () {
        
        var current_time = Date.now();
        var mac = KTW.oipf.adapters.HWAdapter.networkInterface.getMacAddress();
    
        // 마지막 4byte 값을 이용하여 접근 시각값을 계산한다.
        var cnt = 0;
        var total = 0;
        for (var i = mac.length - 1; i >= 0 && cnt < 4; i--) {
            var ch = mac.charAt(i);
            try {
                var n = parseInt(ch, 16);
                total = total * 16 + n;
                cnt++;
            } catch (error) {
                log.printErr(error.message);
            }
        }
    
        // offset은 STB마다 0시간 ~ timePeriod 사이의 고정 값
        var offset = (total * 1000) % LOG_SEND_PERIOD;
        // STB마다 특정 시간에 update를 요청하도록 하기 위해서 timePeriod의 배수에 offset을 더한다
        var time = Math.floor(current_time / LOG_SEND_PERIOD) * LOG_SEND_PERIOD + offset;
        if (time < current_time) {
            time += LOG_SEND_PERIOD;
        }
        log.printDbg("getRandomTimeOnBasedMAC() time= " + time);
        log.printDbg("getRandomTimeOnBasedMAC() time= " + new Date(time));
    
        return time;
    }
    
    function getUrl () {
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            return LOG_SERVER_URL.LIVE;
        } else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT) {
            return LOG_SERVER_URL.BMT;
        } else if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.TEST) {
            return LOG_SERVER_URL.TEST;
        }
    }
    
    return {
        init : _init,
        collect : _collect,

        get logList() { // debugging 용
            return log_list;
        },

        testStart: function () {
            start();
        },
        testStop: function () {
            stop();
        }
    }

})();