"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
KTW.managers.http.ImsManager = (function(){
	var log = KTW.utils.Log;
	var groupId = "400";
	/**
	 * ajax를 이용하여 json 데이터 수집<br>
	 * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
	 * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
	var ajax = function(async, timeout, url, /**Function*/callback){
		return KTW.managers.http.AjaxFactory.createAjax("ImsManager", async, "get", "json", url, undefined, undefined, timeout, callback);
	};
	
	function _ajaxStop(request){
	    if(request){
	        log.printDbg("Ims AJAX request stop!!");
	        request.abort();
	        request = null;
	    }
	}

	function _callbackFuncBackgroundImageZip(result, data) {
		log.printDbg('_callbackFuncBackgroundImageZip() result: ' + result + " , data : " + log.stringify(data));

		KTW.ui.LayerManager.stopLoading();
		if (result === true) {
			log.printDbg("[_callbackFuncCPMSSearchInfo] success");
			log.printDbg('_callbackFuncBackgroundImageZip() result.code : ' + data.code);
			if(data.code === "ok") {
				log.printDbg('_callbackFuncBackgroundImageZip() result.data.version: ' + data.data.version);
				log.printDbg('_callbackFuncBackgroundImageZip() result.data.moddate: ' + data.data.moddate);
				log.printDbg('_callbackFuncBackgroundImageZip() result.data.imgpath: ' + data.data.imgpath);
				log.printDbg('_callbackFuncBackgroundImageZip() result.data.delaytm: ' + data.data.delaytm);
			}
		}else {
			if(data === "timeout") {
				util.showCommonErrorPopup();
			}
		}
	}

	function _getBackgroundImageZip(callback,  scId){
		if(KTW.CONSTANT.IS_UHD == true) {
			groupId = "400";
		}else {
			groupId = "100";
		}
		var api = "/api/v1/stb/background/" + groupId + "/" + scId + "?version=0";
		return ajax(true, 0, KTW.DATA.HTTP.IMS_SERVER.LIVE_URL + api, _callbackFuncBackgroundImageZip);
    }
	
	
	
    
    
    return {
    	ajaxStop: _ajaxStop,
		getBackgroundImageZip: _getBackgroundImageZip
    };
}());