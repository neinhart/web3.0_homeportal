"use strict";
/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
KTW.managers.http.cpmsManager = (function(){
	var log = KTW.utils.Log;
    
	/**
	 * ajax를 이용하여 json 데이터 수집<br>
	 * jsonp 형태의 호출은 비동기 방식으로만 동작 하므로 <br>
	 * 콜백을 잘 활용해야 하며 프로세스를 잘 확인 해야 한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
	var ajax = function(async, timeout, url, /**Function*/callback){
		
		return KTW.managers.http.AjaxFactory.createAjax("cpmsManager", async, "get", "json", url, undefined, undefined, timeout, callback);
		
//	    log.printDbg("########### CPMS Manager AJAX ###########");
//        log.printDbg("url" + url);
//        var ajaxRequest = null;
//        
//		try{
//			ajaxRequest = $.ajax({
//				type: "get",
//				async : async,
//				url : url,
//				dataType : "json",
//				timeout:SERVER_TIMEOUT,
//				cache:false,
//                success:function(result){
//                	if(result.res == "failed"){
//                		log.printWarn("########### CPMS Manager Error return ###########");
//    					log.printWarn("## error code : ",result.errcode);
//    					log.printWarn("## error message : ",result.errmsg);
//    					log.printWarn("##########################################");
//                		if(timeout > 0){
//					        setTimeout(function(){callback(false, result);},timeout);
//					    }else{
//					        if(callback){ callback(false, result); }
//					    }
//                	}else{
//					    if(timeout > 0){
//					        setTimeout(function(){callback(true, result);},timeout);
//					    }else{
//					        if(callback){ callback(true, result); }
//					    }
//                	}
//				},
//				error:function(request, status, error){
//					log.printWarn("########### CPMS Manager Error return ###########");
//					log.printWarn("## request : ",request);
//					log.printWarn("## status : ",status);
//					log.printWarn("## error",error);
//					log.printWarn("##########################################");
//					// status 처리 필요 .. abort, timeout, 관련 처리 필요
//					if(callback){ callback(false,error); }
//				}
//			});
//		}catch(exception){
//		    log.printWarn("########### CPMS Manager catch exception ###########");
//            log.printWarn("## exception",exception);
//            log.printWarn("##########################################");
//			if(callback){ callback(false,exception); }
//		}
//		return ajaxRequest;
	}
	
	function _ajaxStop(request){
	    if(request){
	        log.printDbg("CPMS AJAX request stop!!");
	        request.abort();
	        request = null;
	    }
	} 

	function _searchInfo(callback, sourceId, scId){
    	var api = "/cpms-intf/json/chsrcid/" + sourceId + "/scid/" + scId + "/groupid/100";
        return ajax(true, 0, KTW.DATA.HTTP.CPMS_SERVER.LIVE_URL+api, callback);
    }
    
    function _getBakcgroundZip(callback){
        var api = "/cpms-intf/json/background";
        return ajax(true, 0, KTW.DATA.HTTP.CPMS_SERVER.LIVE_URL+api, callback);
    }
    
    return {
    	ajaxStop: _ajaxStop,
    	searchInfo: _searchInfo,
    	getBakcgroundZip: _getBakcgroundZip
    };
}());