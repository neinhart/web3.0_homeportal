"use strict";
/**
 * @class TV포인트 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * 
 */
KTW.managers.http.PointManager = (function(){

    function getUrl() {
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            return KTW.DATA.HTTP.LUPIN.LIVE_URL; //개발용 [라이브: ktpay.kt.com]
        } else {
            return KTW.DATA.HTTP.LUPIN.TEST_URL;
        }
    }

	function getOldUrl() {
		if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
			return KTW.DATA.HTTP.KTPG.LIVE_URL; //개발용 [라이브: ktpay.kt.com]
		} else {
			return KTW.DATA.HTTP.KTPG.TEST_URL;
		}
    }
    	
    /**
	 * ajax를 이용하여 데이터 수집<br>
	 * 응답은 json 데이터로 오지만 요청헤더 contentType은 text로 보내야한다.
	 * @param successCallback 성공 시 호출 함수
	 * @param errorCallback 실패 시 호출 함수
	 * @param callbackParam 성공 시 넘겨 받을 parameter
	 */
	function ajax(async, timeout, url, postData, /**Function*/callback) {		
		//(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
		return KTW.managers.http.AjaxFactory.createAjax("PointManager", async, "post", "json", url, postData, true, -1, callback, null);
	};

	function ajax2(async, timeout, url, postData, /**Function*/callback) {
		//(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
		return KTW.managers.http.AjaxFactory.createAjaxServerTimeout("PointManager", async, "post", "json", url, postData, true, 6000, callback, null);
	};
	
	function _ajaxStop(request){
	    if(request){
	        KTW.utils.Log.printDbg("AJAX request stop!!");
	        request.abort();
	    }
	};
	
	function getIP(){
		var configuration = KTW.oipf.oipfObjectFactory.createConfigurationObject();
		KTW.utils.Log.printDbg("ip:" + configuration.localSystem.networkInterfaces[0].ipAddress);
		return configuration.localSystem.networkInterfaces[0].ipAddress;
	}

	/**
	 *
	 * @param point_type : "1": myMenu 에서, 이용권 포함 호출
	 *                     "9": 결제 진행에서 호출, 포인트, 쿠폰 만 호출
	 * @param callback
	 * @returns {*}
	 * @private
	 */
	function _getTvPoint(point_type, callback) {
		var postData = {
				'UserIDType':'20',	
				 'UserID': KTW.SAID,
				 'SubType':'1',
				 'PointType':point_type,
				 'AuthKey':KTW.SAID,
				 'IPAddress': getIP(),
				 'SuppInfo':''
		};		
		KTW.utils.Log.printDbg("TVPoint!!");
		return ajax2(true,0,getOldUrl() + "getStbPoint.do",postData, callback);
	};

    // Lupin 서버 신규 API
    function _getPoint(point_type, callback) {
        var postData = {
            settlIdTypeCd: "7", // 결제 아이디 타입
            settlId      : KTW.SAID, // 결제 아이디
            storId       : "KTPGMTV002", // 가맹점 ID (현재는 사용하지 않음)
            retvTypeCd   : point_type? point_type : "9", // 조회 유형 코드
            ipadr        : getIP(), // IP address
            adtnInfo     : "" // 부가 정보
        };

        KTW.utils.Log.printDbg("getTvPoint");
        return ajax2(true, 0, getUrl()+"retvPoint.json", postData, callback);
    }

	function _getStbContentCouponInfo(callback, categoryId, contentId, contentName, price) {
		var postData = {
				'UserIDType':'20',	
				 'UserID': KTW.SAID,
				 'DomainId' : '10000012',
				 'PointType':'3',
				 'AuthKey':KTW.SAID,
				 'IPAddress': getIP(),
				 'CategoryId' : categoryId,
				 'ContentId' : contentId,
				 'ContentName' : contentName,
				 'ContentPrice' : price
		};		
		KTW.utils.Log.printDbg("getStbContentCouponInfo !!");
		return ajax(true,0,getOldUrl() + "StbContentCouponInfo.do",postData, callback);
	};
	
	return {
		ajaxStop: _ajaxStop,
		getStbContentCouponInfo: _getStbContentCouponInfo,
		getTvPoint: _getTvPoint,
        getPoint: _getPoint
	};
}());