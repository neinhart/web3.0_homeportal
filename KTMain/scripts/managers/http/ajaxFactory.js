"use strict";
KTW.managers.http.AjaxFactory = (function(){
	function _onAjaxDone(callback, result, param, timeout){
		if(result == true && timeout > 0) {
			setTimeout(function(){callback.apply(this, param);},timeout);
		}else{
			if(callback){ callback.apply(this, param); }
		}
	};

    function _createAjax(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters){
        return _createAjaxInner(serviceName, async, type, dataType, url, postData, crossDomain, timeout, undefined, callback, parameters);
    }

    function _createAjaxServerTimeout(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters){
        return _createAjaxInner(serviceName, async, type, dataType, url, postData, crossDomain, undefined, timeout, callback, parameters);
    }
	
	function _createAjaxInner(serviceName, async, type, dataType, url, postData, crossDomain, timeout, serverTimeout, callback, parameters){
	    var index = 0;
	    	    
		KTW.utils.Log.printDbg("[HTTP] createAjax [" + serviceName + "]");
		KTW.utils.Log.printDbg("[HTTP] async:" + async + " type:" + type + " dataType:" + dataType + " crossDomain:" + crossDomain + " timeout:" + timeout + " postData:" + JSON.stringify(postData));
		KTW.utils.Log.printDbg("[HTTP] url:" + url);
	
		var requestServiceName = serviceName;
	    var ajaxRequest = null;
	    
	    try{
	        var ajaxParam = {
	                type: type, //get, post.
                    async: async,
                    url: url,
                    dataType: dataType, //json, xml, jsonp, text
                    data: postData,
                    timeout: serverTimeout ? serverTimeout : KTW.DATA.AMOC.TIMEOUT,
                    cache:false,
                    success: function(result){
                        KTW.utils.Log.printDbg("[HTTP] ajax success [" + requestServiceName + "] ###########");
                        if (KTW.LOG_OUTPUT === true) {
                            if (dataType === "json") {
                                KTW.utils.Log.printLongDbg("[HTTP] result = " + JSON.stringify(result));
                            }
                        }
                        _onAjaxDone(callback, true, (parameters ? [true, result].concat(parameters) : [true, result]), timeout);
                    },
                    error:function(request, status, error){
                        KTW.utils.Log.printErr("[HTTP] Error return [" + requestServiceName + "] ###########");
                        KTW.utils.Log.printErr("[HTTP] request : ", JSON.stringify(request));
                        KTW.utils.Log.printErr("[HTTP] status : ", JSON.stringify(status));
                        KTW.utils.Log.printErr("[HTTP] error", JSON.stringify(error));
                        
                        // status 처리 필요 .. abort, timeout, 관련 처리 필요
                      _onAjaxDone(callback, true, (parameters ? [false,error].concat(parameters) : [false,error]));
                    },
	        };
	        
	        if(crossDomain)
	            ajaxParam.crossDomain = crossDomain;
	        
	    	return $.ajax(ajaxParam);
	    }catch(exception){
	    	KTW.utils.Log.printErr("[HTTP] catch exception [" + requestServiceName + "] ###########");
            KTW.utils.Log.printErr("[HTTP] exception",JSON.stringify(exception));
            
            _onAjaxDone(callback, true, (parameters ? [false,exception].concat(parameters) : [false,exception]));
            
            return null;
	    }
	}
	
	function _createAjaxForHds(async, timeout, url, postData, /**Function*/callback, param){
	    var prefix = "HDS AJAX";
	    
	    KTW.utils.Log.printDbg("[HTTP] createAjax [" + prefix + "]");
        KTW.utils.Log.printDbg("[HTTP] async:" + async + " timeout:" + timeout + " postData:" + JSON.stringify(postData));
        KTW.utils.Log.printDbg("[HTTP] url:" + url);
        
        var ajaxRequest = null;
        try{
            ajaxRequest = $.ajax({
                type:"post",
                async: async,
                url: url,
                data: postData,
                timeout: timeout === 0 ? KTW.DATA.AMOC.TIMEOUT : timeout,
                cache: false,
                success: function(data, status, xhr){
                    KTW.utils.Log.printDbg("########### HDSManager result ###########");
                    var xmlString = (new XMLSerializer()).serializeToString(xhr.responseXML);
                    KTW.utils.Log.printDbg("## result = " + xmlString);
                    KTW.utils.Log.printDbg("##########################################");

                    if(callback){
                        callback(true, xhr, param);
                    }
                },
                error:function(request, status, error){
                    KTW.utils.Log.printErr("########### HDSManager Error return ###########");
                    KTW.utils.Log.printErr("## url : " + url);
                    KTW.utils.Log.printErr("## data : " + postData);
                    KTW.utils.Log.printErr("## request : " + request);
                    KTW.utils.Log.printErr("## status : " + status);
                    KTW.utils.Log.printErr("## error : ",error);
                    KTW.utils.Log.printErr("##########################################");
                    // status 처리 필요 .. abort, timeout, 관련 처리 필요
                    param = param ? param : {};
                    param.error = true;
                    if(callback){ callback(false, null, param); }
                }
            });
        }catch(err){
            KTW.utils.Log.printErr("########### HDSManager catch exception ###########");
            KTW.utils.Log.printErr("## url : " + url);
            KTW.utils.Log.printErr("## data : " + postData);
            KTW.utils.Log.printErr("##########################################");
            
            KTW.utils.Log.printExec(err);
            
            param = param ? param : {};
            param.error = true;
            if(callback){ callback(false, null, param); }
        }
        
        return ajaxRequest;
	}
	
	function _stopAjax(request, name) {
		if(request){
	        KTW.utils.Log.printDbg("[HTTP] " + (name ? name : "") + " AJAX abort !");
	        request.abort();
	    }
	}
	
	return {
		createAjax:_createAjax,
		createAjaxForHds:_createAjaxForHds,
        createAjaxServerTimeout:_createAjaxServerTimeout,
		stopAjax:_stopAjax,
	}
}());
