/**
 *  Copyright (c) ${YEAR} Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>HDSManager.js</code>
 *
 * <p>
 * 제한 PIN/구매 PIN 인증 등을 위한 HDS API
 * </p>
 *
 * @author dhlee
 * @since 2016-08-01
 */

"use strict";

KTW.managers.http.HDSManager = (function(){
    var log = KTW.utils.Log;
    //var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
    //var storageManager = KTW.managers.StorageManager;
    var util = KTW.utils.util;
    var IS_RELEASE = ((KTW.UI_VERSION == "0.01.000") ? false : true);

    var HDS_URL_TYPE = {
            NORMAL:0,
            ON_SCREEN:1,
            PIN_UPDATE:2
    };
    
    var PIN_TYPE ={
        ADULT:0,
        BUY:1
    };
    
    var REQUIRE_PIN_AUTH = 1;
    /*var REQUIRE_BUYPIN_AUTH = 2;*/
    
    function getUrl(appendType){
        var url = "";
        
        if (KTW.RELEASE_MODE === KTW.CONSTANT.ENV.BMT || KTW.RELEASE_MODE === KTW.CONSTANT.ENV.LIVE) {
            url = KTW.DATA.HTTP.HDS.LIVE_URL; //개발용 [라이브: ktpay.kt.com]
        }else{
            url = KTW.DATA.HTTP.HDS.TEST_URL; //test.
        }
        
        switch(appendType){
            case HDS_URL_TYPE.NORMAL:
                url += "HDSIMNPWebSVC/HDSIMNPWebSVC.asmx";
                break;
            case HDS_URL_TYPE.ON_SCREEN:
                url+= "HDSOnScreenWebSvc/HDSOnScreenWebSvc.asmx";
                break;
            case HDS_URL_TYPE.PIN_UPDATE:
                url+= "HDSSTBPINWebSVC/HDSSTBPINWebSVC.asmx";
                break;
        }
        
        return url;
    }
            
    var hds_info = (function getInfo(){
    	var sso_client_obj = KTW.oipf.oipfObjectFactory.createSSOClient();
    	   	
        var smartcard_id = getSmartCardId();
        
        var service_id = "HOMEPORTAL"; //상수
        var call_type = "HOMEPORTAL"; //상수
        var sys_info = "신규홈포털"; //상수
        var host_name = "dbsrp.ktipmedia.co.kr"; //상수
        var service_kind_cd = "0701"; 
        var proc_sub_service = "2P34"; // 양방향 부가상품 ID
        var proc_sub_service_ch = "2P05"; //채널 부가상품 ID
        
        var sso_validation = "";
        var hds_token = "";
        var stb_token = "";
        
        //주기적 Timer
        var logout_time = null;
                   
        function getSAID() {
            return KTW.SAID;
        }
        
        function getIP() {
            return KTW.oipf.AdapterHandler.hwAdapter.networkInterface.getIpAddress();
        }
        
        function getSmartCardId() {
        	return KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.CHIP_ID); //mook_t smart card id. ? 뭐가 맞지 ?.
        }
        
        function onLogon(event) {
            log.printDbg("[HDS] onLogon()");
            
            if(event !== undefined && event !== null) {
                log.printDbg("[HDS] onLogon event.success:" +event.success);
                log.printDbg("[HDS] onLogon event.result:" +event.result);
            }
        	
        	if (event.success) {
        		if(event.result) {
        			log.printDbg("onLogon message:" + event.result);
        			stb_token = sso_client_obj.getSTBToken();
            		hds_token = sso_client_obj.getHDSToken();
            		
            		var start_index = event.result.indexOf("PinAuthValidate") + 16;
            		
            		if (start_index > -1) {
    					var endIndex =  event.result.indexOf("_", start_index);
    					
    					sso_validation = event.result.substring(start_index, endIndex);
    					
    					if(logout_time) {
                        	clearTimeout(logout_time);
                        }
                        logout_time = null;
    					logout_time = setTimeout(_logout, 3000);
    					
    					log.printDbg("stb_token:" + stb_token);
    					log.printDbg("hds_token:" + hds_token);
    					log.printDbg("sso_validation:" + sso_validation);
    					
    					if(logonParam && logonParam.fn) {
    						logonParam.fn.apply(undefined, logonParam.params);
    						logonParam.fn = null;
    						return;
    					} 
            		} else {
            			start_index = event.result.indexOf("returnDESC^") + 11 ;
            			if(logonParam && logonParam.callback) {
    						logonParam.callback(false, undefined, event.result.substring(start_index));
                        }
            			return;
            		}
        		}
        	}
        	
        	//fail. 
        	if(logonParam && logonParam.callback) {
                var param = {};
                param.error = true;
                //KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , ["서비스가 일시적으로 원활하지 않습니다" , "셋톱 재부팅 시 해결 될 수 있습니다" , "재부팅 버튼을 선택하면 자동 재부팅 됩니다"], null, null, true,"reboot");
                logonParam.callback(false , null , param);
            }
        }
        
        var logonParam;
        function _logon(_fn, _callback, _params) {
        	log.printDbg("[HDS] _logon");
        	
        	logonParam = {
                    fn:_fn,
                    callback:_callback,
                    params:_params
            };
        	
        	sso_client_obj.terminalLogon(service_id);
        }
        
        //사실 callback 만 remove 시키는것.
        function _cancelLogon() {
        	log.printDbg("[HDS] _cancelLogon");
        	
        	//login 중일때만..
        	logonParam = undefined;
        	_logout();
        }
        
        function _logout() {
        	log.printDbg("[HDS] _logout");
        	
        	stb_token = undefined;
    		hds_token = undefined;
    		sso_validation = undefined;
    		
    		sso_client_obj.logout();
            if(logout_time) {
                clearTimeout(logout_time);
            }
    		logout_time = null;
        }
        
        //booting 후 1 회 HDS_TOKEN 및 SSO_VALIDATION 값 얻는다.
        function init(){
        	sso_client_obj.addUserLogonEventListener(onLogon);
        }
        
        init();
        
        return {
            get SAID(){
              return getSAID();  
            },
            get host_name(){
                return host_name;  
            },
            get ip(){
                return getIP();
            },
            get mac_addr(){
                return mac_address;
            },
            get smartcard_id(){
                if (KTW.TEST_CODE === true) {
                    return "";
                }

                return smartcard_id;
            },
            get service_kind_cd(){
                return service_kind_cd;
            },            
            get call_type() {
                return call_type;
            },
            get proc_sub_service() {
                return proc_sub_service;
            },
            get proc_sub_service_ch() {
                return proc_sub_service_ch;
            },
            get sys_info(){
                return sys_info;
            },                   
            set sso_validation(v){
                sso_validation = v;
            },
            get sso_validation(){
                return sso_validation;
            },
            
            get stb_token() {
                return stb_token;
            },
            
            get hds_token() {
                return hds_token;
            },
            
            get callType() {
                return call_type;
            },
                        
            logon: _logon,
            logout: _logout,
            cancelLogon:_cancelLogon
        }
    }());
    
    /**
     * ajax를 이용하여 데이터 수집<br>
     * 응답은 json 데이터로 오지만 요청헤더 contentType은 text로 보내야한다.
     * @param successCallback 성공 시 호출 함수
     * @param errorCallback 실패 시 호출 함수
     * @param callbackParam 성공 시 넘겨 받을 parameter
     */
    function ajax(async, timeout, url, postData, /**Function*/callback, param) {
        if(!IS_RELEASE) {
            log.printForced("HDS AJAX request url+postData:" + url + "\n[" + JSON.stringify(postData) + "]");
        }

        return KTW.managers.http.AjaxFactory.createAjaxForHds(async, timeout, url, postData, /**Function*/callback, param);
    }
    
    function _ajaxStop(request){
        if(request){
            log.printDbg("AJAX request stop!!");
            request.abort();
        }
    }
    

            
    /**
     * pinType. 0: 양방향 서비스, 1: 구매핀
     * pinno: 성인핀 / 구매핀
     * callback callback(isSuccess, errorDesc) 
     * 
     * 성인 핀은 pinList API 를 이용하고, 
     * 구매 핀은 DBLogon 의 calltype = authbuypin 을 이용함.
     * 
     */
    var ajax_request;
    function _abortAuthPin() {
    	log.printDbg("[HDS] _abortAuthPin");
        
    	if(ajax_request) {
    		KTW.managers.http.AjaxFactory.stopAjax(ajax_request);
    		ajax_request = null;
    	}
    	
    	//위 ajax_request 자체가 안갔을 수 있다. 
    	//해서 if 문 안에 넣으면 안된다.
    	hds_info.cancelLogon();
    }

    /**
     * 성인 핀 조회 및 구매 핌 조회
     */
    function _hdsAuthPin(pinType, pinno, now , callback){
        log.printDbg("[HDS] _hdsAuthPin now : " + now);
        
        if (!hds_info.hds_token) {
            hds_info.logon(_hdsAuthPin, callback, [pinType, pinno, now , callback]);
        }else{
            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _hdsAuthPin startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _hdsAuthPin apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);

            if(pinType == PIN_TYPE.ADULT){
                //pin list 로 인증 함.
                var api = getUrl(HDS_URL_TYPE.NORMAL)+"/HDSPinList";
                var postData = "SAID="+hds_info.SAID+"&PinNo="+pinno+"&HDSToken="+hds_info.hds_token+"&STBToken="+hds_info.stb_token;
                ajax_request = ajax(true,apiTimeout,api,postData, onResponseAuthPin, {callback:callback, pinType:pinType});
            }else if(pinType == PIN_TYPE.BUY){
                var api = getUrl(HDS_URL_TYPE.NORMAL)+"/HDSDBLogon";
                //pinno = "6666";
                var postData = "Calltype=authbuypin&input1="+hds_info.SAID+"&input2="+pinno+"&input3=Reserved"+"&HDSToken="+hds_info.hds_token+"&STBToken="+ hds_info.stb_token;
                ajax_request = ajax(true,apiTimeout,api,postData, onResponseAuthPin, {callback:callback, pinType:pinType});
            }
        }
    }

    /**
     * 성인 핀 조회 및 구매 핌 조회 결과 CallbackFunc
     */
    function onResponseAuthPin(success, xhr, params) {
        log.printDbg("[HDS] onResponseAuthPin");

        ajax_request = null;
        if (success) {
            //파싱
            if(params.pinType == PIN_TYPE.ADULT){
                var x = xhr.responseXML.getElementsByTagName("returnVal");
                if(x[0].textContent == "True" || x[0].textContent == "true"){
                    log.printDbg("[HDS] onResponseAuthPin() success");

                    if(params.callback)
                        params.callback(true, xhr);
                }else{
                    var x = xhr.responseXML.getElementsByTagName("returnDESC");

                    var error_desc =  x[0].textContent;

                    log.printDbg("[HDS] onResponseAuthPin() error_desc = " +  error_desc);
                    if(params.callback)
                        params.callback(false, xhr, util.stringToArray(error_desc));
                }

            }else if(params.pinType == PIN_TYPE.BUY){
                var x = xhr.responseXML.getElementsByTagName("string");
                var startIndex = x[0].textContent.indexOf("PinAuthValidate") + 16;

                if (startIndex > 16) {
                    log.printDbg("[HDS] onResponseAuthPin() success");

                    if(params.callback)
                        params.callback(true, xhr);
                } else {
                    startIndex = x[0].textContent.indexOf("returnDESC^") + 11 ;

                    var error_desc =  x[0].textContent.substring(startIndex);

                    log.printDbg("[HDS] onResponseAuthPin() error_desc = " +  error_desc);
                    if(params.callback)
                        params.callback(false, xhr, util.stringToArray(error_desc));
                }
            }
        } else {
            log.printDbg("[HDS] onResponseAuthPin() error");
            KTW.ui.LayerManager.stopLoading();
            if(params){
                if(params.error) {
                    KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                }

                if (params.callback) {
                    params.callback(false, xhr);
                }
            }
        }

        //구매 pin 인증 후는 다시 logon 하도록
        //왜 ?
        hds_info.logout();
    }

      
    //5. PIN 조회
    /**
     * Web 2.0 / Web 3.0 해당 API는 현재 사용 하고 있지 않음.
     */
    function _hdsPINList(pinno, callback){
        log.printDbg("[HDS] _hdsPINList");
        
        if (!hds_info.hds_token) {
            hds_info.logon(_hdsPINList, callback, [pinno, callback]);
        }else{
            var api = getUrl(HDS_URL_TYPE.NORMAL)+"/HDSPinList";
            var postData = "SAID="+hds_info.SAID+"&PinNo="+pinno+"&HDSToken="+hds_info.hds_token+"&STBToken="+hds_info.stb_token;
            ajax(true,0,api,postData,callback);
        }
    }

    /**
     * PIN 변경
     */
    function _hdsPINUpdate(pinType, oldPin, newPin, now , callback){
        log.printDbg("[HDS] _hdsPINUpdate");

        if (!hds_info.hds_token) {
            hds_info.logon(_hdsPINUpdate, callback, [pinType, oldPin, newPin, now , callback]);
        }else{
            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _hdsAuthPin startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _hdsAuthPin apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);


            var param = '';

            switch(pinType){
                case PIN_TYPE.ADULT:
                    param += 'PinNo';
                    break;
                case PIN_TYPE.BUY:
                    param += 'BuyPinNo';
                    break;
            }

            var sValue = param + '^' + newPin;

            var api = getUrl(HDS_URL_TYPE.NORMAL)+"/HDSPinUpdate";

            var postData = "SAID="+hds_info.SAID+"&PinNo="+oldPin+"&sValue="+sValue+"&HDSToken="+hds_info.hds_token+"&STBToken="+hds_info.stb_token;

            ajax_request = ajax(true,apiTimeout,api,postData,onUpdatePin,{newPin: newPin, pinType:pinType, callback:callback});
        }
    }

    /**
     * 6. PIN 변경 CallbackFunc
     */
    function onUpdatePin(success, xhr, params){
        log.printDbg("[HDS] onUpdatePin");
        ajax_request = null;
        if(params.pinType == PIN_TYPE.ADULT){
            hds_info.logout();
        }
        
        if (success) {      
            try {
                var x = xhr.responseXML.getElementsByTagName("string");
                var startIndex = x[0].textContent.indexOf("returnVal^True") ;
                if (startIndex >= 0) {
                    if(params.pinType == PIN_TYPE.ADULT){
                        log.printDbg("[HDS] update HDS Token params.newPin:"+params.newPin);
                        //PIN 변경에 선공하면 HDS Token 을 새로 가져오자.
                        //pin auth 는 이렇게 하면 되는지 확인 buy pin auth 는 왠지 필요 없는듯. sso validation 을 못얻음.
                        /*hds_info.getHDSToken(params.newPin, doNothing, doNothing, [], REQUIRE_PIN_AUTH);*/
                    }
                    
                    log.printDbg("[HDS] update HDS success");
                    
                    if(params.callback)
                        params.callback(true, xhr);
                    return;
                }else{
                    var messageIndex = x[0].textContent.indexOf("returnDESC^") + 11 ;
                    var message = x[0].textContent.substring(messageIndex);
                    
                    log.printDbg("[HDS] update HDS error desc:"+message);
                    
                    if(params.callback)
                        params.callback(false, xhr, util.stringToArray(message));
                    return;
                }
            }catch(e){
                log.printErr(e);
            }
        }else {
            log.printDbg("[HDS] onUpdatePin() error");
            KTW.ui.LayerManager.stopLoading();
            if(params){
                if(params.error) {
                    KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E027.split("\n"), null, null, true , "reboot");
                }
                params.callback(false, xhr);
            }
         }
    }
    

    
    /**
     * OnlineChVodSvcCnclbPIN VOD 부가상품 온라인 해지
     * 임시 구매핀
     * OnlineChVodMSVCbPIN 채널 VOD 부가상품 월정액 온라인 가입
     * OnlineChVodSvcRegbPIN 채널 VOD 부가상품 가입
     * 
     * OnlineRegSubMSVCbPIN 양방향 부가상품 월정액 가입
     * OnlineRegSubSVCbPIN 양방향 부가상품 온라인 가입
     * 
     * 
     */
    
    
    /**
     * 7.양방향 부가상품 온라인 가입 
     * 
     * jm.kang. 
     * 미사용. 확인필요. sso validate 도 logout 상태일때 체크 필요함.
     * (구매핀)양방향 부가상품 온라인 가입(OnlineRegSubSVCbPIN) 
    function _onlineRegSubSVC(pinno, product_id, callback){
    	log.printDbg("[HDS] _onlineRegSubSVC");
    	
    	if (!hds_info.sso_validation) {
            hds_info.logon(_onlineRegSubSVC, callback, [pinno, callback], REQUIRE_PIN_AUTH);
        }else{
        	var serviceType = 'iCOD';
            var svckindcd = hds_info.service_kind_cd;
            var ssovalidate = hds_info.sso_validation;
            var hostname = hds_info.host_name;
            var smartCardID = hds_info.smartcard_id;
            var stbIP = hds_info.ip;
            var procSubSVC = product_id;
            
            var api = getUrl(HDS_URL_TYPE.ON_SCREEN)+"/OnlineRegSubSVCbPIN";
            
            var postData = "serviceType="+serviceType+"&SAID="+hds_info.SAID+"&PinNo="+pinno+"&svckindcd="+svckindcd +
                    "&ssovalidate="+ssovalidate+"&hostname="+hostname+"&stbip="+stbIP+"&procSubSVC="+procSubSVC;
            
            //setTimeout(hds_info.logout, 3000);
            
            return ajax(true,0,api,postData, callback);    
        }
    };*/

    //8 채널/VOD 부가상품 온라인 가입
    //(구매핀)채널/VOD 부가상품 온라인 가입(OnlineChVodSvcRegbPIN)
    function _onlineChVodSvcReg(pinno, productId, now , callback){
        log.printDbg("[HDS] _onlineChVodSvcReg");

        if (!hds_info.sso_validation) {
            hds_info.logon(_onlineChVodSvcReg, callback, [pinno, productId, now , callback], REQUIRE_PIN_AUTH);
        }else{
            var api = getUrl(HDS_URL_TYPE.ON_SCREEN)+"/OnlineChVodSvcRegbPIN";

            var serviceType = 'iCOD';
            var svckindcd = hds_info.service_kind_cd;
            var ssovalidate = hds_info.sso_validation;
            var hostname = hds_info.host_name;
            var smartCardID = hds_info.smartcard_id;
            var stbIP = hds_info.ip;
            var procSubSVC = productId;

            var postData = "serviceType="+serviceType+"&SAID="+hds_info.SAID+"&PinNo="+pinno+"&svckindcd="+svckindcd +
                "&ssovalidate="+ssovalidate+"&hostname="+hostname+"&SmartcardID="+smartCardID+
                "&stbip="+stbIP+"&procSubSVC="+procSubSVC;


            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _onlineChVodSvcReg startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _onlineChVodSvcReg apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);

            //setTimeout(hds_info.logout, 3000);

            ajax_request = ajax(true,apiTimeout,api,postData,_reponseOnlineChVodSvcReg,{callback:callback});
        }
    }

    function _reponseOnlineChVodSvcReg(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                util.showCommonErrorPopup();
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }
        }
        hds_info.logout();
    }




    //9.채널/VOD 부가상품 월정액 온라인 가입
    //(구매핀)채널/VOD 부가상품 월정액 온라인 가입(OnlineChVodMSVCbPIN)
    function _onlineChVodMSVCbPIN(pinno, productId, callback, contractYN, contractPeriod , now){
        log.printDbg("[HDS] _onlineChVodMSVCbPIN");


        if (!hds_info.sso_validation) {
            hds_info.logon(_onlineChVodMSVCbPIN, callback, [pinno, productId, callback, contractYN, contractPeriod , now], REQUIRE_PIN_AUTH);
        } else {
            var api = getUrl(HDS_URL_TYPE.ON_SCREEN)+"/OnlineChVodMSVCbPIN"; // 확인.

            var serviceType = 'iCOD';
            var svckindcd = hds_info.service_kind_cd;
            var ssovalidate = hds_info.sso_validation;
            var hostname = hds_info.host_name;
            var smartCardID = hds_info.smartcard_id;
            var stbIP = hds_info.ip;
            var procSubSVC = productId;

            //SAID 필요 없음.
            var postData = "serviceType="+serviceType+"&SAID="+hds_info.SAID+"&PinNo="+pinno+"&svckindcd="+svckindcd+
                "&ssovalidate="+ssovalidate+"&hostname="+hds_info.host_name+"&SmartcardID="+smartCardID+"&stbip="+stbIP+
                "&procSubSVC="+procSubSVC + "&contractYN=" + contractYN + "&contractPeriod=" + contractPeriod;

            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _onlineChVodMSVCbPIN startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _onlineChVodMSVCbPIN apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);

            ajax_request = ajax(true,apiTimeout,api,postData,_reponseOnlineChVodMSVCbPIN,{callback:callback});


        }
    }

    function _reponseOnlineChVodMSVCbPIN(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                util.showCommonErrorPopup();
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }
        }
        hds_info.logout();
    }

    // 18. 온스크린 유료채널 상위상품 upselling
    function _onlineChangeProdbPIN(pinno, newProdCd, now , callback) {
        log.printDbg("[HDS] _onlineChangeProdbPIN");

        if (!hds_info.sso_validation) {
            hds_info.logon(_onlineChangeProdbPIN, callback, [pinno, newProdCd, now , callback], REQUIRE_PIN_AUTH);
        }else{

            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _onlineChangeProdbPIN startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _onlineChangeProdbPIN apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);


            var storageManager = KTW.managers.StorageManager;

            var api = getUrl(HDS_URL_TYPE.ON_SCREEN)+"/OnlineChangeProdbPIN";

            var serviceType = 'iCOD';
            var sellType = 1; // upselling
            var ssovalidate = hds_info.sso_validation;
            var hostname = hds_info.host_name;
            var stbIp = hds_info.ip;
            var oldProdCd = JSON.parse(storageManager.ps.load(storageManager.KEY.CUST_ENV)).pkgCode;

            var postData = "serviceType="+serviceType+"&sellType="+sellType+"&said="+hds_info.SAID+"&pinno="+pinno+
                "&ssovalidate="+ssovalidate+"&hostname="+hostname+"&stbip="+stbIp+"&oldProdCd="+oldProdCd+"&newProdCd="+newProdCd;

            ajax_request = ajax(true,apiTimeout,api,postData,_responseOnlineChangeProdbPIN,{callback:callback});
        }
    }

    function _responseOnlineChangeProdbPIN(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                util.showCommonErrorPopup();
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }
        }
        hds_info.logout();
    }


    /**
     * 10.양방향 부가상품 온라인 해지
     * 
     * jm.kang 사용하지 않음.
     */
    function _onlineCnclSubSVC(pinno, productId, callback){
        log.printDbg("[HDS] _onlineCnclSubSVC");
        
        if (!hds_info.sso_validation) {
            hds_info.logon(_onlineCnclSubSVC, callback, [pinno, productId, callback], REQUIRE_PIN_AUTH);
        } else {
            var serviceType = 'iCOD';
            var svckindcd = hds_info.service_kind_cd;
            var ssovalidate = hds_info.sso_validation;
            var hostname = hds_info.host_name;
            //var smartCardID = hds_info.smartcard_id;
            var stbIP = hds_info.ip;
            var procSubSVC = productId;
            
            var api = getUrl(HDS_URL_TYPE.ON_SCREEN)+"/OnlineCnclSubSVC";
            
            var postData = "serviceType="+serviceType+"&SAID="+hds_info.SAID+"&PinNo="+pinno+"&svckindcd="+svckindcd +
                           "&ssovalidate="+ssovalidate+"&hostname="+hostname+/*"&SmartcardID="+smartCardID+*/
                           "&stbip="+stbIP+"&procSubSVC="+procSubSVC;
            
            //setTimeout(hds_info.logout, 3000);
            
            return ajax(true,0,api,postData, callback);
        }
    }

    //11.채널 부가상품 온라인 해지  
    function _onlineChVodSvcCncl(pinno, productId, callback){
        log.printDbg("[HDS] _onlineChVodSvcCncl");
        
        if (!hds_info.sso_validation) {
            hds_info.logon(_onlineChVodSvcCncl, callback, [pinno, productId, callback], REQUIRE_PIN_AUTH);
        } else {
            var serviceType = 'iCOD';
            var svckindcd = hds_info.service_kind_cd;
            var ssovalidate = hds_info.sso_validation;
            var hostname = hds_info.host_name;
            var smartCardID = hds_info.smartcard_id;
            var stbIP = hds_info.ip;
            var procSubSVC = productId;
            
            var api = getUrl(HDS_URL_TYPE.ON_SCREEN)+"/OnlineChVodSvcCnclbPIN";
            
            var postData = "serviceType="+serviceType+"&SAID="+hds_info.SAID+"&PinNo="+pinno+"&svckindcd="+svckindcd +
                           "&ssovalidate="+ssovalidate+"&hostname="+hostname+"&SmartcardID="+smartCardID+
                           "&stbip="+stbIP+"&procSubSVC="+procSubSVC;
            
            //setTimeout(hds_info.logout, 3000);
            
            return ajax(true,0,api,postData, callback);
        }        
    }
    
    //12.가입자 본인확인 
    function _reqSMSComm (birthday, gender, nation, name, ttype, mobile, now , callback) {
        log.printDbg("[HDS] _reqSMSComm now : " + now);
        
        if (!hds_info.sso_validation) {
            hds_info.logon(_reqSMSComm, callback, [birthday, gender, nation, name, ttype, mobile,  now , callback], REQUIRE_PIN_AUTH);
        } else {
            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _reqSMSComm startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _reqSMSComm apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);

        	var api = getUrl(HDS_URL_TYPE.PIN_UPDATE) + "/ReqSMSComm";
            var postData = "QID=" + hds_info.SAID + "&SSOValidate=" + hds_info.sso_validation + "&BirthYear=" + birthday.substring(0,4) + "&BirthMon=" + birthday.substring(4,6) +
                           "&BirthDay=" + birthday.substring(6) + "&Gender=" + gender + "&Nation=" + nation + "&Uname=" + name + "&TTYPE=" + ttype + 
                           "&MOBILE=" + mobile + "&SvcTYPE=" + (KTW.CONSTANT.IS_OTS ? "2" : "1");


            ajax_request = ajax(true,apiTimeout,api,postData, onReqSMSComm, {callback:callback});
        }
    }

    //14.본인확인용 가입자정보 조회
    function _reqCertUserInfo(now , callback) {
        log.printDbg("[HDS] _reqCertUserInfo");

        if (!hds_info.sso_validation) {
            hds_info.logon(_reqCertUserInfo, callback, [now , callback], REQUIRE_PIN_AUTH);
        }else{
            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _reqCertUserInfo startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _reqCertUserInfo apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);

            var api = getUrl(HDS_URL_TYPE.PIN_UPDATE) + "/ReqCertUserInfo";
            var postData = "QID=" + hds_info.SAID + "&SSOValidate=" + hds_info.sso_validation;

            ajax_request = ajax(true,apiTimeout,api,postData, onReqCertUserInfo, {callback:callback});
        }
    }

    /**
     *  본인확인용 가입자정보 조회
     */
    function onReqCertUserInfo(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                var message = ["가입자 인증 오류입니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다"  ];
                KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , message, null, null,  true);
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }

        }
        hds_info.logout();
    }

    /**
     *  가입자 본인 조회 결과
     */
    function onReqSMSComm(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                var message = [ "인증번호 요청에 실패했습니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다" ];
                KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , message, null, null, true);
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }
        }
        hds_info.logout();
    }
    
    //13.인증번호 확인
    function _reqSMSCnfrmComm(cnum, ckey, now , callback) {
        log.printDbg("[HDS] _reqSMSCnfrmComm");
        
        if (!hds_info.sso_validation) {
            hds_info.logon(_reqSMSCnfrmComm, callback, [cnum, ckey, now , callback], REQUIRE_PIN_AUTH);
        } else {
            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _reqSMSCnfrmComm startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _reqSMSCnfrmComm apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);
            
            
        	var api = getUrl(HDS_URL_TYPE.PIN_UPDATE) + "/ReqSMSCnfrmComm";
            var postData = "QID=" + hds_info.SAID + "&SSOValidate=" + hds_info.sso_validation + "&CNUM=" + cnum +"&CKEY=" + ckey;

            ajax_request = ajax(true,apiTimeout,api,postData, onReqSMSCnfrmComm, {callback:callback});
        }
    }

    /**
     *  인증번호 확인
     */
    function onReqSMSCnfrmComm(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                var message = [ "인증번호 확인에 실패했습니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다" ];
                KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , message, null, null,  true);
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }
        }
        hds_info.logout();
    }
    
    


    //17. initBuynAdPin
    function _initBuyAndAdultPin(cnum, ckey, now , callback){
        log.printDbg("[HDS] _initBuyAndAdultPin");
        
        if (!hds_info.sso_validation) {
            hds_info.logon(_initBuyAndAdultPin, callback, [cnum, ckey, now , callback], REQUIRE_PIN_AUTH);
        }else{
            /**
             * hds_info.logon 호출 한 시점부터 시간을 계산 하여
             * 남은 시간만큼 timeout을 주도록 수정함.
             * logon 호출 시점 부터 API 호출 시점이 30초가 넘어 가면 0초를 AJAX로 전달 한다.
             * AJAX는 내부적으로 0초이면 Default 값으로 5초를 설정하여 API 호출 한다.
             */
            var apiCallTime =  new Date().getTime();
            apiCallTime = Math.floor(apiCallTime/1000);
            log.printDbg("[HDS] _initBuyAndAdultPin startTime : " + now + " , API CallTime : " + apiCallTime);
            apiCallTime-=now;

            var apiTimeout = (apiCallTime>30 ? 0 : 30 - apiCallTime) * 1000;
            log.printDbg("[HDS] _initBuyAndAdultPin apiTimeout apiTimeout : " + apiTimeout + " , apiCallTime : " + apiCallTime);
            
            //https://서비스관리서버/HDSSTBPINWebSVC/HDSSTBPINWebSVC.asmx/initBuynAdPin
            var api = getUrl(HDS_URL_TYPE.PIN_UPDATE) + "/initBuynAdPin";
            var postData = "QID=" + hds_info.SAID + "&SSOValidate=" + hds_info.sso_validation + "&CNUM=" + cnum + "&CKEY=" + ckey;


            ajax_request = ajax(true,apiTimeout,api,postData, onInitBuyAndAdultPin, {callback:callback});
        }
    }
    
    function onInitBuyAndAdultPin(success, xhr, params) {
        ajax_request = null;
        if (success) {
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(success, xhr);
            }
        } else {
            KTW.ui.LayerManager.stopLoading();
            if(params !== undefined && params !== null && params.error !== undefined && params.error !== null && params.error === true){
                var message = ["가입자 인증 오류입니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다"  ];
                KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , message, null, null,  true);
            }
            if(params !== undefined && params !== null && params.callback !== null) {
                params.callback(false, xhr);
            }
        }
        hds_info.logout();
    }
    

    return {
        PIN_TYPE: PIN_TYPE,
        ajaxStop: _ajaxStop,
        hdsPINList: _hdsPINList,
        hdsPINUpdate: _hdsPINUpdate,
        onlineChVodSvcReg:_onlineChVodSvcReg,
        onlineChVodMSVCbPIN:_onlineChVodMSVCbPIN,
        onlineChVodSvcCncl:_onlineChVodSvcCncl,
        onlineChangeProdbPIN: _onlineChangeProdbPIN,
        
        hdsAuthPin:_hdsAuthPin,
        abortAuthPin:_abortAuthPin,
        
        reqSMSComm:_reqSMSComm,
        reqSMSCnfrmComm:_reqSMSCnfrmComm,
        reqCertUserInfo:_reqCertUserInfo,
        
        //initIcodPin:_initIcodPin,
        //initIcodSLPin:_initIcodSLPin,
        initBuyAndAdultPin:_initBuyAndAdultPin
    };

}());