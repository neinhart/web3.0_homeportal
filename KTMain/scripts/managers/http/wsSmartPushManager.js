"use strict";

/**
 * @class 서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 * @example var posterDm = new DataManager("VOD_CATEGORY", categoryId); 
 * posterDm.ajaxMenu(createPoster, failAjax);
 * 
 */
KTW.managers.http.wsSmartPushManager = (function(){
    
    var ajax = function(async, timeout, url, postData, /**Function*/callback, parameters){
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return  KTW.managers.http.AjaxFactory.createAjax("SmartPushManager", async, "post", "json", url, postData, undefined, timeout, callback, parameters);
    };
    
    function _ajaxStop(request){
        if(request){
            KTW.utils.Log.printDbg("SmartPushManager AJAX request stop!!");
            request.abort();
            request = null;
        }
    };
    
    function _checkOptOut(callback, sa_id) {
        var api;
        var data;
        
        api = "/spis/all_check_opt_out.do";
        data = "saId=" + sa_id;
        
        if (KTW.RELEASE_MODE === "LIVE" || KTW.RELEASE_MODE === "BMT") {
            return ajax(true, 0, KTW.DATA.HTTP.SMART_PUSH.LIVE_URL + api, data, callback);
        } else {
            return ajax(true, 0, KTW.DATA.HTTP.SMART_PUSH.TEST_URL + api, data, callback);
        }
    }
    
    return {
        ajaxStop:_ajaxStop,
        checkOptOut:_checkOptOut
    };
}());