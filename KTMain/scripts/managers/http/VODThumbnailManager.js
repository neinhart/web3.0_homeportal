/**
 * Created by user on 2015-12-22.
 */
"use strict";
/**
 * @class 추천서버 연동 데이터 관리 클래스
 * @constructor
 * @param target 서버 연동 대상
 * @param param 연동 parameter1
 * @param param2 연동 parameter2
 *
 */
KTW.managers.http.VodThumbnailManager = (function() {
    function getUrl() {
        return KTW.DATA.AMOC.WAPI_URL;
    }

    function ajax(async, timeout, url, postData, /**Function*/callback) {
        //(serviceName, async, type, dataType, url, postData, crossDomain, timeout, callback, parameters)
        return KTW.managers.http.AjaxFactory.createAjax("VOD Thumbnail Manager", async, "get", "json", url, postData, true, -1, callback, null);
    }

    function _ajaxStop(request) {
        if (request) {
            KTW.utils.Log.printDbg("AJAX request stop!!");
            request.abort();
            request = null;
        }
    };

    /**
     * thumbnail list
     *
     * response
     *
     * @param callback
     * @param contId
     * @returns {*}
     * @private
     */
    function _getThumbnailList(callback, contId) {
        return ajax(true, 0, getUrl() + "/api/thumbnail/merge.json", "content_id=" + contId + "&said=" + KTW.SAID, callback);
    }

    function _getCorner(callback, corner_grp_id, content_id) {
        if (KTW.TEST_CODE == "thumbnail") {
            callback(true, {
                "result"    : "200",
                "result_msg": "SUCCESS",
                "count"     : 9,
                "items"     : [
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "1",
                        "cornername"   : "루시드 폴-M/V: 아직, 있다.",
                        "content_id"   : "MCLFC03BSGL150000100",
                        "cc_content_id": "",
                        "file_id"      : "MCLFC03BSGL150000110",
                        "file_url"     : "http://125.144.104.41/postimg/MCLFC03BSGL150000110.jpg",
                        "cornerstart"  : "00:10:30.153",
                        "cornerend"    : "00:13:30.224",
                    },
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "2",
                        "cornername"   : "XXX 2회",
                        "content_id"   : "MCLF703QSGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLF703QSGL150000110.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    },
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "3",
                        "cornername"   : "트와이스-M/V: OOH-AHH하게",
                        "content_id"   : "MCLFA03PSGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLFA03PSGL150000110.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    },
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "4",
                        "cornername"   : "씨스타-M/V: SHAKE IT",
                        "content_id"   : "MCLF7033SGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLF7033SGL150000110.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    },
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "5",
                        "cornername"   : "최인영＆구혜선-M/V: 소리없이 날",
                        "content_id"   : "MCLFC044SGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLFC044SGL150000110.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    } ,
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "7",
                        "cornername"   : "별이 되어 빛나리(동우-너에게 살고싶",
                        "content_id"   : "MCLFC041SGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/CL000000000000023483.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    }  ,
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "8",
                        "cornername"   : "백지영-M/V: 새벽 가로수길",
                        "content_id"   : "MCLF306ASGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLF306ASGL150000110.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    }  ,
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "9",
                        "cornername"   : "규현-M/V: 광화문에서 (At Gwanghwamun)",
                        "content_id"   : "MCLEB03CSGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLEB03CSGL150000210.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    }  ,
                    {
                        "cornergrpid"  : "0000000001",
                        "cornerid"     : "10",
                        "cornername"   : "에이핑크-M/V: Remember",
                        "content_id"   : "MCLF704VSGL150000100",
                        "cc_content_id": "10021698470001",
                        "file_id"      : "fff",
                        "file_url"     : "http://125.144.104.41/postimg/MCLF704VSGL150000110.jpg",
                        "cornerstart"  : "00:15:30.153",
                        "cornerend"    : "00:18:30.224",
                    }
                ]
            });
            return;
        }
        //

        return ajax(true, 0, getUrl() + "/api/corner/list.json", "content_id=" + content_id + "&cornergrpid=" + corner_grp_id + "&said=" + KTW.SAID, callback);
    }

    /**
     * content ID 에 해당되는 북마크 정보 조회.
     *
     * response
     * time_stamp / title / file_url / cc_content_id / content_id / nid
     *
     * @param callback
     * @param content_id
     * @returns {*}
     * @private
     */
    function _getBookmark(callback, content_id) {
        return ajax(true, 0, getUrl() + "/api/t_vod_bookmark/list.json", "content_id=" + content_id + "&said=" + KTW.SAID, callback);
    }

    return {
        getThumbnailList: _getThumbnailList,
        getCorner: _getCorner,
        getBookmark: _getBookmark,
    }
}());