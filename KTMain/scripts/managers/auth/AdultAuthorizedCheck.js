"use strict";

/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>AdultAuthorizedCheck.js</code>
 *
 * 성인인증 상태인지를 체크한다.
 * 사용자가 입력한 성인인증 PIN 이 성공적으로 인증되면 인증 상태로 변경한다.
 * listener 를 통해 변경 사항을 전달한다.
 * 현재 메뉴를 기준으로 성인인증이 이루어져야 하는 메뉴인 경우 성인인증을 수행
 * 성인인증 수행 후 하위 카테고리, VOD 상세화면, 구매 시 등에 다시 성인인증을 하지 않는다.
 * 단, 현재 메뉴 기준으로 상위 메뉴로 돌아가는 경우나 홈메뉴를 다시 실행하는 경우 다시 성인인증을 해야 한다.
 * 이를 위해 setAdultAuthorized(val, from) 에서 val을 false 로 set 하도록 호출해야 한다.
 *
 * @author dhlee
 * @since 2016-07-26
 */

KTW.managers.auth.AdultAuthorizedCheck = (function() {

    var adult_authed_flag  = false;
    var adult_authed_from = "";

    var authChangeListener = new KTW.utils.Listener();
    var log = KTW.utils.Log;

    /**
     * 성인 인증 상태인지를 확인한다.
     *
     * @returns {boolean}
     */
    function _isAdultAuthorized() {
        log.printDbg("_isAdultAuthorized(), adult_authed_flag = " + adult_authed_flag);
        if(adult_authed_flag === true) {
            return true
        }

        return false;
    }

    /**
     * 성인 인증 상태를 set/reset 한다.
     *
     *
     * @param val   boolean (true/false)
     * @param from  String
     */
    function _setAdultAuthorized(val, from) {
        log.printDbg("_setAdultAuthorized(), val = " + val + ", from = " + from);
        if(adult_authed_flag !== val) {
            adult_authed_flag = val;
            authChangeListener.notify(val);

            adult_authed_from = (from ? from : "");
        }
    }

    /**
     * callback listener 추가
     *
     * @param listener  callback function
     */
    function _addOnAuthChangeListener(listener) {
        authChangeListener.addListener(listener);
    }

    /**
     * callback listener 제거
     *
     * @param listener  callback function
     */
    function _removeOnAuthChangeListener(listener) {
        authChangeListener.removeListener(listener);
    }

    return {
        setAdultAuthorized: _setAdultAuthorized,
        isAdultAuthorized: _isAdultAuthorized,
        addOnAuthChangeListener: _addOnAuthChangeListener,
        removeOnAuthChangeListener: _removeOnAuthChangeListener,
        getAuthorizeOwner: function() {
            return adult_authed_from;
        }
    };
}());