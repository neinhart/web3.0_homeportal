"use strict";

/**
 * [jh.lee] 비밀번호 관련 처리를 담당
 * TODO 2016.10.10 dhlee loading 에 대한 부분 어떻게 처리할지 확정 후 관련 코드 정리해야 함.
 */
KTW.managers.auth.AuthManager = (function() {
    // [jh.lee] 참조
    var log = KTW.utils.Log;
    //var util = KTW.utils.util;
    var layerManager = KTW.ui.LayerManager;
    //var caHandler = KTW.ca.CaHandler;
    //var menuDataManager = KTW.managers.data.MenuDataManager;
    var hdsManager = KTW.managers.http.HDSManager;
    var casAdapter = KTW.oipf.AdapterHandler.casAdapter;
    var serviceAdapter = KTW.oipf.AdapterHandler.serviceAdapter;
    var networkManager = KTW.managers.device.NetworkManager;
    //var extensionAdapter = KTW.oipf.AdapterHandler.extensionAdapter;
    
    // [jh.lee] 메뉴 id
    //var menu_id;
    // [jh.lee] 팝업 id
    //var popup_id;
    // [jh.lee] 인증 절차 타입
    var type;
    // [jh.lee] callback
    var listener;
    // [jh.lee] 로딩 관련 데이터
    var loading;
    // [jh.lee] 추가 data
    var data;
    
    // [jh.lee] 비밀번호
    var input = "";
    var new_input = "";
    var confirm_input = "";
    
    // [jh.lee] 비밀번호 오류 횟수
    var pw_check_count = 0;
    // [jh.lee] 최종 CAS pin Update 에서 실패할 경우 재 요청 횟수(1번만 더 요청한다)
    var request_pin_update_count = 0;
    // 2016.09.05 dhlee R6 형상 추가
    // [eunsik] 비밀번호 3회 오류시 구매실패/성인인증실패 구분
    var pw_type = null;
    
    // [jh.lee] hidden 비밀번호
    var HDS_PASSWORD = "1473591232580";
    var OTS_PASSWORD = "***2485";
    //var OC_PASSWORD = "***7777";
    
    // [jh.lee] 생성하려는 팝업 ID
    var ID = {
            ADMIN_LOGIN : "admin_login",
            ADVICE_INIT : "advice_init",
            HDS_ERROR : "hds_error"
    };
    
    var RESULT = {
            SUCCESS : 0,
            FAIL : 1
    };
    
    // [jh.lee] checkUserPW 타임아웃
    var check_pw_timer = null;
    // [jh.lee] 임시로 사용할 마스터핀(성인인증비밀번호)
    var temp_pin = "";
    // OTS 에서 나그러 cas가 아닌 ktcas를 요청 true
    var ktcas_request_flag = false;
    
    var handling_result = false;

    var parental_rating;

    /**
     * 초기화 함수 main.js 에서 호출된다.
     * AuthManager 에서 초기화가 필요한 경우 init()에 추가한다.
     *
     * @private
     */
    function _init() {
        layerManager =  KTW.ui.LayerManager;

        hdsManager = KTW.managers.http.HDSManager;
        casAdapter = KTW.oipf.AdapterHandler.casAdapter;
        serviceAdapter = KTW.oipf.AdapterHandler.serviceAdapter;
        networkManager = KTW.managers.device.NetworkManager;
        // 2017.05.26 dhlee
        casAdapter.addPRChangeListener(onChangePR);
        onChangePR();
    }

    /**
     * 2017.05.29 dhlee
     * PR 변경 시 수행되는 callback
     * CAS로부터 변경된 PR 값을 읽어온다.
     */
    function onChangePR() {
        parental_rating = casAdapter.getParentalRating();
    }
    
    /**
     * [jh.lee] 4자리 비밀번호 검증
     * [jh.lee] 객체를 넘겨 받는 방식으로 변경
     *          var obj = {
     *              type: type (AUTH_TYPE 참조)
     *              callback: onCallback,
     *              loading: {
     *                  type : 로딩의 위치 타입(LoadingDialog 참조),
     *                  lock : 키 블락 여부 (true, false),
     *                  callback : 타임아웃 콜백(콜백함수),
     *                  on_off : 로딩을 그리는지 여부 (true, false)
     *              },
     *              data: {
     *                  menu_id: id,
     *                  ... 추가적으로 필요한 data
     *              }
     *          };
     */
    function _checkUserPWAsync(obj) {
        log.printDbg("_checkUserPWAsync handling_result : " + handling_result + " , check_pw_timer : " + check_pw_timer);

        if (!check_pw_timer && handling_result === false) {
            log.printDbg("checkUserPWAsync handling_result : " + handling_result);
            handling_result = true;
            log.printDbg("checkUserPWAsync change handling_result : " + handling_result);
            KTW.ui.LayerManager.startLoading({
                preventKey: true
            });
            check_pw_timer = setTimeout(_checkUserPW, 300, obj);   
        }
    }
    
    function _checkUserPW(obj) {
        type = obj.type;
        listener = obj.callback;
        loading = obj.loading;
        data = obj.data;

        if (check_pw_timer) {
            clearTimeout(check_pw_timer);
            check_pw_timer = null;
        }
        
        layerManager.stopLoading();

        KTW.ui.LayerManager.startLoading({
            preventKey: true
        });
        
        if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.CHANGE_BUY_PIN ||
                type === KTW.managers.auth.AuthManager.AUTH_TYPE.AUTH_BUY_PIN ||
                type === KTW.managers.auth.AuthManager.AUTH_TYPE.AUTH_CURRENT_BUY_PIN) {
            // [jh.lee] 구매인증, 구매인증 비밀번호 변경의 경우 CAS 절차 없이 바로 HDS를 통해 검증 및 변경한다.
            // [jh.lee] 구매인증 비밀번호 변경의 경우 CAS 인증 절차가 완료되어야 진행할 수 있다.
            // [jh.lee] AUTH_CURRENT_BUY_PIN 은 현재 비밀번호만 검증하는 로직

            if (networkManager.isNetworkConnected() !== true) {
                // [jh.lee] 구매인증비밀번호 변경에서 현재 비밀번호 입력창 검증 또는
                // [jh.lee] 구매인증 비밀번호 변경에서 확인 누르는 경우 또는
                // [jh.lee] 결제 또는 해지 인 경우
                // [jh.lee] 통신 에러 문구로 변경해서 보여줘야함
                layerManager.stopLoading();
                listener(KTW.managers.auth.AuthManager.RESPONSE.DISCONNECTED_NETWORK);

                handling_result = false;
                log.printDbg("Done handling_result : " + handling_result);
            }
            else {
                var now = new Date().getTime();
                now = Math.floor(now/1000);
                log.printDbg("hdsManager.hdsAuthPin() now : " + now);
                hdsManager.hdsAuthPin(hdsManager.PIN_TYPE.BUY, input, now , onHDSBuyAuthPinResponse);
            }
        } else {
                /**
                 * [jh.lee] 구매인증 비밀번호 변경 전 성인인증 타입
                 */
                //AUTH_ADULT_PIN_FOR_CHANGE : 1,
                /**
                 * [jh.lee] 구매인증 비밀번호 변경 타입
                 */
               // CHANGE_BUY_PIN : 2,
                /**
                 * [jh.lee] 성인인증
                 */
                //AUTH_ADULT_PIN : 3,
                /**
                 * [jh.lee] 구매인증에 대한 현재 비밀번호 검증
                 */
                //AUTH_CURRENT_BUY_PIN : 6,
                /**
                 * [jh.lee] MINI EPG 에서 제한, 연령 등 제한 아이프레임에서 Smart Card 가 빠진 경우 검증
                 */
                //AUTH_ADULT_PIN_FOR_MINIEPG : 7
            // [jh.lee] 나머지 인증 및 변경은 CAS 절차를 진행한다.
            if (KTW.CONSTANT.IS_OTS && KTW.SMARTCARD_ID && Number(KTW.SMARTCARD_ID) === 0) {
                // [jh.lee] OTS 이고 Smart Card 가 단절인 경우 verifyPIN 이 아닌 KT CAS에 요청한다.

                if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.AUTH_ADULT_PIN_FOR_MINIEPG) {
                    // [jh.lee] MINIEPG 의 제한채널, 연령제한 등의 상태에서 S/C 카드 누락된 상태로 인증 시도
                    // [jh.lee] 무조건 비밀번호 틀린것으로 처리
                    onCaResponse(false);
                } else {
                    KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_PIN_QUERY,
                        { old_pin : input }, onCaResponse);
                }
            }
            else {
                if (casAdapter.verifyPIN(input) === true) {
                    // R5 성인인증 비밀번호 변경에서 모든 입력란에 값을 입력 후 확인 버튼을 누르면 유효성 체크 순서를 변경
                    //    우선 4자리 유효한지 인증 -> 유효하면 4자리 인지, 새로운 비밀번호, 확인 비밀번호 유효성 확인
                    if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.CHANGE_ADULT_PIN) {
                        var check;
                        check = _confirmNewPW();

                        if (check === KTW.managers.auth.AuthManager.RESPONSE.CORRECT_PASSWORD) {
                            // R5 유효성 체크도 정상이면 다음 진행
                            onCaResponse(true);
                        } else {
                            handling_result = false;
                            layerManager.stopLoading();
                            listener(check);

                            log.printDbg("Done handling_result : " + handling_result);
                        }
                    } else {
                        onCaResponse(true);
                    }
                } else {
                    onCaResponse(false);
                }
            }
        }
    }
    
    /**
     * [jh.lee] 새비밀번호와 비밀번호 확인 검증
     */
    function _confirmNewPW() {
        if (new_input.length !== 4 || confirm_input.length !== 4) {
            // [jh.lee] 4자리가 아닐 경우
            //layerManager.stopLoading();
            return KTW.managers.auth.AuthManager.RESPONSE.WRONG_LENGTH;
        } else if (new_input !== confirm_input) {
            // [jh.lee] 서로 다른 경우
            //layerManager.stopLoading();
            return KTW.managers.auth.AuthManager.RESPONSE.MISMATCH_PASSWORD;
        } else {
            // [jh.lee] 입력 값이 정상인 경우
            return KTW.managers.auth.AuthManager.RESPONSE.CORRECT_PASSWORD;
        }
    }
    
    /**
     * [jh.lee] CAS 및 HDS로 부터 받는 응답 부분 ******************************************
     */
    
    /**
     * [jh.lee] CAS 로 부터 성공, 실패에 대한 응답을 받는 콜백
     * @param success
     */
    function onCaResponse(success) {
        if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.AUTH_CURRENT_ADULT_PIN) {
            layerManager.stopLoading();
            // [jh.lee] CAS 성인인증 비밀번호 변경에서 현재 비밀번호만 검증하는 로직
            handling_result = false;
            if (success) {
                listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_AUTH_SUCCESS);
            } else {
                listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_AUTH_FAIL);
            }
            log.printDbg("Done handling_result : " + handling_result);
        } else if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.CHANGE_ADULT_PIN) {
            // [jh.lee] 성인인증 비밀번호 변경 인 경우
            if (success === true) {
                // [jh.lee] CAS 성인인증 비밀번호 인증 성공
                // [jh.lee] HDS 성인인증 비밀번호 인증 시작
                if (KTW.CONSTANT.IS_OTS && KTW.SMARTCARD_ID && Number(KTW.SMARTCARD_ID) === 0) {
                    // [jh.lee] Smart Card 가 제거되있으면 KT CAS 를 통해 인증은 성공하지만 변경은 성공하면 안되기 때문에 팝업 생성
                    handling_result = false;
                    layerManager.stopLoading();
                    _showHDSPasswordErrorPopup(onShowHDSPasswordErrorPopup);

                    log.printDbg("Done handling_result : " + handling_result);
                } else {
                    var now = new Date().getTime();
                    now = Math.floor(now/1000);
                    log.printDbg("hdsManager.hdsAuthPin() now : " + now);
                    hdsManager.hdsAuthPin(hdsManager.PIN_TYPE.ADULT, input, now , onHDSAuthPinResponse);
                }
            } else {
                // [jh.lee] CAS 성인인증 비밀번호 인증 실패
                handling_result = false;
                layerManager.stopLoading();
                listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_AUTH_FAIL);
                
                log.printDbg("Done handling_result : " + handling_result);
            }    
        } else {
            // [jh.lee] 성인인증 비밀번호 변경이 아닌 성인인증 비밀번호 인증인 경우
            // [jh.lee] 4자리 비밀번호 인증 로직
            layerManager.stopLoading();
            handling_result = false;
            
            if (success === true) {
                // [jh.lee] CAS 성인인증 비밀번호 인증 성공
                if (listener) {
                    temp_pin = input;
                    listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_AUTH_SUCCESS);
                }
                
                log.printDbg("Done handling_result : " + handling_result);
            } else {
                // [jh.lee] CAS 성인인증 비밀번호 인증 실패
                if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.AUTH_ADULT_PIN_FOR_CHANGE) {
                    // [jh.lee] HDS 구매인증 비밀번호 변경 시도 전에 수행하는 CAS 성인인증 비밀번호 인증에 실패한 경우
                    // [jh.lee] 이 경우 틀린횟수를 증가시키지 않는다.
                    listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_AUTH_FAIL);
                } else {
                    // [jh.lee] 일반적인 CAS 성인인증 비밀번호 인증 실패한 경우 틀린 횟수를 증가시킨다.
                    pw_check_count++;
                    
                    if (pw_check_count >= 3) {
                        // 2016.09.05 dhlee R6 형상 적용
                        pw_type = "adult";
                        // [jh.lee] 비밀번호 오류 횟수가 3회 이상인 경우 팝업 생성
                        // [jh.lee] 시청시간제한 과 나머지의 경우로 구분
                        showAdvicePasswordInitPopup();
                    } else {
                        // [jh.lee] 비밀번호 오류 횟수가 3회 이상이 아닌 경우
                        if (listener) {
                            listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_AUTH_FAIL);
                        }
                    }
                }
                log.printDbg("Done handling_result : " + handling_result);
            }
        }
    }
    
    /**
     * [jh.lee] 성인인증 비밀번호 변경 시 예외 팝업 콜백
     */
    function onShowHDSPasswordErrorPopup(buttonId) {
        if (buttonId !== undefined && buttonId !== null) {
            KTW.ui.LayerManager.deactivateLayer({
                id: "hds_auth_timeout_error",
                remove: true
            });

            if (buttonId === "ok") {
                listener(KTW.managers.auth.AuthManager.RESPONSE.DISCONNECTED_SMART_CARD);
            } else {
                
            }
        } else {
            // [jh.lee] menu, back, exit 키가 입력 될 시 팝업 갱신 후 해당 키처리
            listener(KTW.managers.auth.AuthManager.RESPONSE.DISCONNECTED_SMART_CARD);
        }
    }
    
    /**
     * [jh.lee] 성인인증 비밀번호 인증 성공 후 HDS 성인인증 비밀번호 인증에 대한 응답 (성인인증 비밀번호 변경)
     */
    function onHDSAuthPinResponse(result, xhr, error_message) {
        if (result === true) {
            // [jh.lee] HDS 성인인증 비밀번호 인증 성공
            // [jh.lee] HDS 성인인증 비밀번호 변경 시작
            var now = new Date().getTime();
            now = Math.floor(now/1000);
            log.printDbg("onHDSAuthPinResponse() now : " + now);
            hdsManager.hdsPINUpdate(hdsManager.PIN_TYPE.ADULT, input, new_input, now , onHDSPINUpdate);

        } else {
            // [jh.lee] HDS 비밀번호 검증 실패
            // [jh.lee] HDS 오류 팝업 생성 후 팝업 닫으면 비밀번호 변경 팝업까지 닫고 좌측 메뉴로 이동
            layerManager.stopLoading();
            handling_result = false;

            if(error_message !== undefined && error_message !== null) {
                if(error_message instanceof Array) {
                    showHDSErrorPopup(error_message);
                }else {
                    if(error_message.error === true) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                    }
                }
            }else {
                showHDSErrorPopup();
            }
                
            log.printDbg("Done handling_result : " + handling_result);
        }
    }
    
    /**
     * [jh.lee] HDS 성인인증 비밀번호 변경에 대한 응답 (성인인증 비밀번호 변경)
     */
    function onHDSPINUpdate(result, xhr, error_message) {
        layerManager.stopLoading();
        if (result === true) {

            // [jh.lee] HDS 성인인증 비밀번호 변경 성공
            // [jh.lee] CAS 성인인증 비밀번호 변경 시작
            if (casAdapter.changePIN(input, new_input) === true) {
                // [jh.lee] CAS 성인인증 비밀번호 변경 성공
                if (KTW.CONSTANT.IS_OTS) {
                    // [jh.lee] OTS 의 경우 changePIN 결과 후 추가로 CAS에 변경을 요청해야한다.
                    ktcas_request_flag = true;
                    KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_PIN_CHANGE,
                        { old_pin : input, new_pin : new_input }, onCaUpdateResponse);
                } else {
                    onCaUpdateResponse(true);
                }
            } else {
                onCaUpdateResponse(false);
            }
        } else {
            // [jh.lee] HDS 비밀번호 변경 실패
            // [jh.lee] HDS 오류 팝업 생성 후 팝업 닫으면 비밀번호 변경 팝업까지 닫고 좌측 메뉴로 이동
            if(error_message !== undefined && error_message !== null) {
                if(error_message instanceof Array) {
                    showHDSErrorPopup(error_message);
                }else {
                    if(error_message.error === true) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                    }
                }
            }
            handling_result = false;
            log.printDbg("Done handling_result : " + handling_result);
        }
    }
    
    /**
     * [jh.lee] CAS 성인인증 비밀번호 변경에 대한 응답 (성인인증 비밀번호 변경)
     */
    function onCaUpdateResponse(success) {
        if (success === true) {
            // [jh.lee] CAS 성인인증 변경이 성공
            layerManager.stopLoading();
            // [jh.lee] pin 관련 변수 초기화
            request_pin_update_count = 0;
            ktcas_request_flag = false;
            // [jh.lee] 임시로 성인인증 비밀번호를 저장하고 있어야한다.
            // [jh.lee] PWcheck 메뉴 해제 후 성인연령제한 설정하는 경우 등등...
            temp_pin = new_input;
            handling_result = false;
            input = 0;
            new_input = 0;
            confirm_input = 0;
            listener(KTW.managers.auth.AuthManager.RESPONSE.CAS_CHANGE_SUCCESS);
            
            log.printDbg("Done handling_result : " + handling_result);
        } else {
            // [jh.lee] HDS 성인인증 비밀번호 성공 후 CAS 성인인증 비밀번호 변경이 실패하는 경우
            if (request_pin_update_count === 1) {
                // [jh.lee] 이미 재요청을 한 상태이기 때문에 콜백없이 HDS 성인인증 비밀번호를 이전 상태로 원복
                // [jh.lee] 그리고 오류 팝업 생성
                // [jh.lee] 오류 팝업을 닫으면 비밀번호 변경 팝업도 닫고 좌측 메뉴로 포커스 이동. 최종적으로 비밀번호 변경 실패
                request_pin_update_count = 0;
                var now = new Date().getTime();
                now = Math.floor(now/1000);
                log.printDbg("onHDSAuthPinResponse() now : " + now);

                hdsManager.hdsPINUpdate(hdsManager.PIN_TYPE.ADULT, new_input, input, now , undefined);
                if (ktcas_request_flag === true) {
                    // ktcas까지 요청한 후 실패가 난 케이스이기 때문에 changePin 원복 필요
                    casAdapter.changePIN(new_input, input);
                    ktcas_request_flag = false;
                }
                layerManager.stopLoading();
                handling_result = false;
                showHDSErrorPopup();
                
                log.printDbg("Done handling_result : " + handling_result);
            } else {
                // [jh.lee] 다시 CAS 성인인증 비밀번호 변경 요청하고 요청 횟수를 증가
                request_pin_update_count++;
                if (casAdapter.changePIN(input, new_input) === true) {
                    if (KTW.CONSTANT.IS_OTS) {
                        // [jh.lee] OTS 의 경우 changePIN 결과 후 추가로 CAS에 변경을 요청해야한다.
                        ktcas_request_flag = true;
                        KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_PIN_CHANGE,
                                { old_pin : input, new_pin : new_input }, onCaUpdateResponse);     
                    } else {
                        onCaUpdateResponse(true);   
                    }
                } else {
                    onCaUpdateResponse(false);
                }
            }
        }
    }
    
    /**
     * [jh.lee] HDS 구매인증 비밀번호 인증 응답 (구매인증 비밀번호 변경, 구매시 구매인증 비밀번호 인증)
     */
    function onHDSBuyAuthPinResponse(result, xhr, error_message) {
        log.printDbg("onHDSBuyAuthPinResponse() result : " + result);
        var message_check = false;
        if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.AUTH_CURRENT_BUY_PIN) {
            // [jh.lee] HDS 구매인증 비밀번호 변경에서 현재 비밀번호만 검증하는 로직
            layerManager.stopLoading();
            handling_result = false;
            if (result) {
                listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_SUCCESS);
            } else {
                // 2017.10.10 dhlee
                // WEBIIIHOME-3403 이슈 수정
                // AUTH Fail 상황에서 HDS로부터 수신된 에러 메시지가 있는 경우 AUTH_FAIL이 아닌 AUTH_ERROR를 전달하도록 한다.
                // AUTH_ERROR를 전달 받은 경우 토스트 팝업을 실행하지 않도록 한다.
                /*
                listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);
                if(error_message !== undefined && error_message !== null && error_message.error !== undefined) {
                    KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                }
                */
                if(error_message !== undefined && error_message !== null && error_message.error !== undefined) {
                    listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_ERROR);
                    KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                } else {
                    listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);
                }

            }
            
            log.printDbg("Done handling_result : " + handling_result);
        } else if (type === KTW.managers.auth.AuthManager.AUTH_TYPE.CHANGE_BUY_PIN) {
            // [jh.lee] HDS 구매인증 비밀번호 인증 성공
            // [jh.lee] HDS 구매인증 비밀번호 변경 시작
            if (result) {
                // [jh.lee] HDS 비밀번호 검증 성공
                var check;

                check = _confirmNewPW();
                // R5 성인인증 비밀번호 변경에서 모든 입력란에 값을 입력 후 확인 버튼을 누르면 유효성 체크 순서를 변경
                //    우선 4자리 유효한지 인증 -> 유효하면 4자리 인지, 새로운 비밀번호, 확인 비밀번호 유효성 확인
                if (check === KTW.managers.auth.AuthManager.RESPONSE.CORRECT_PASSWORD) {
                    var now = new Date().getTime();
                    now = Math.floor(now/1000);
                    log.printDbg("onHDSBuyAuthPinResponse() now : " + now);

                    hdsManager.hdsPINUpdate(hdsManager.PIN_TYPE.BUY, input, new_input, now , onHDSBuyPINUpdate);
                } else {
                    layerManager.stopLoading();
                    handling_result = false;
                    listener(check);
                    log.printDbg("Done handling_result : " + handling_result);
                }
            } else {
                // [jh.lee] HDS 비밀번호 검증 실패
                // [jh.lee] HDS 오류 팝업 생성 후 팝업 닫으면 비밀번호 변경 팝업까지 닫고 좌측 메뉴로 이동
                layerManager.stopLoading();
                handling_result = false;
                if(error_message !== undefined && error_message !== null && error_message.error !== undefined) {
                    KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                }else {
                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        // R5 HDSE036(비밀번호 미입력) HDSE019(비밀번호 1~3자리) 메세지를 받으면 에러팝업 생성없이 변경 팝업 상단에
                        // 불일치 메세지 노출
                        for(var i = 0; i < error_message.length; i++) {
                            if (error_message[i].search("[HDSE036]") > -1 ||
                                error_message[i].search("[HDSE019]") > -1) {
                                message_check = true;
                                listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);
                                break;
                            }
                        }

                        if (message_check === false) {
                            showHDSErrorPopup(error_message);
                        }
                    }
                }

                log.printDbg("Done handling_result : " + handling_result);
            }
        } else {
            // [jh.lee] 구매시 HDS 구매인증 비밀번호 인증 성공
            // [jh.lee] 구매인증 비밀번호 검증만 하는 경우
            layerManager.stopLoading();
            handling_result = false;
            if (result) {
                // [jh.lee] 구매인증 비밀번호 검증이 성공하는 경우
                listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_SUCCESS);
            } else {
                // [jh.lee] 구매인증 비밀번호 검증이 실패하는 경우
                pw_check_count++;
                
                if (pw_check_count >= 3) {
                    // [jh.lee] 비밀번호 오류 횟수가 3회 이상인 경우 팝업 생성
                    // [jh.lee] 시청시간제한 과 나머지의 경우로 구분
                    showAdvicePasswordInitPopup();   
                } else {
                    // 2017.10.10 dhlee
                    // WEBIIIHOME-3403 이슈 수정
                    // AUTH Fail 상황에서 HDS로부터 수신된 에러 메시지가 있는 경우 AUTH_FAIL이 아닌 AUTH_ERROR를 전달하도록 한다.
                    // AUTH_ERROR를 전달 받은 경우 토스트 팝업을 실행하지 않도록 한다.
                    /*
                    // [jh.lee] 비밀번호 오류 횟수가 3회 이상이 아닌 경우
                    listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);

                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        // R5 HDSE036(비밀번호 미입력) HDSE019(비밀번호 1~3자리) 메세지를 받으면 에러팝업 생성없이 변경 팝업 상단에
                        // 불일치 메세지 노출
                        for(var i = 0; i < error_message.length; i++) {
                            if (error_message[i].search("[HDSE036]") > -1 ||
                                error_message[i].search("[HDSE019]") > -1) {
                                message_check = true;
                                break;
                            }
                        }

                        if (message_check === false) {
                            showHDSErrorPopup(error_message);
                        }
                    }else {
                        if(error_message !== undefined && error_message !== null && error_message.error !== undefined) {
                            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                        }
                    }
                    */

                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        // R5 HDSE036(비밀번호 미입력) HDSE019(비밀번호 1~3자리) 메세지를 받으면 에러팝업 생성없이 변경 팝업 상단에
                        // 불일치 메세지 노출
                        for(var i = 0; i < error_message.length; i++) {
                            if (error_message[i].search("[HDSE036]") > -1 ||
                                error_message[i].search("[HDSE019]") > -1) {
                                message_check = true;
                                break;
                            }
                        }

                        if (message_check === false) {
                            listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_ERROR);
                            showHDSErrorPopup(error_message);
                        } else {
                            // [jh.lee] 비밀번호 오류 횟수가 3회 이상이 아닌 경우
                            listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);
                        }
                    }else {
                        if(error_message !== undefined && error_message !== null && error_message.error !== undefined) {
                            listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_ERROR);
                            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                        } else {
                            listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);
                        }
                    }
                }
            }
            log.printDbg("Done handling_result : " + handling_result);
        }
    }
    
    /**
     * [jh.lee] HDS 구매인증 비밀번호 변경 응답 (HDS 구매인증 비밀번호 변경)
     */
    function onHDSBuyPINUpdate(result, xhr, error_message) {
        layerManager.stopLoading();
        handling_result = false;
        if (result === true) {
            // [jh.lee] HDS 구매인증 비밀번호 변경 성공
            input = 0;
            new_input = 0;
            confirm_input = 0;
            listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_CHANGE_SUCCESS);
        } else {
            // [jh.lee] HDS 구매인증 비밀번호 변경 실패
            // [jh.lee] HDS 오류 팝업 생성 후 팝업 닫으면 비밀번호 변경 팝업까지 닫고 좌측 메뉴로 이동
            //showHDSErrorPopup(error_message);
            if(error_message !== undefined && error_message !== null) {
                if(error_message instanceof Array) {
                    showHDSErrorPopup(error_message);
                }else {
                    if(error_message.error === true) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                    }
                }
            }
        }
        log.printDbg("Done handling_result : " + handling_result);
    }
    
    /*************************************************************************/
    
    /**
     * [jh.lee] 관리자 비밀번호 인증 관련 및 HDS 개통 ******************************************
     */
    
    /**
     * [jh.lee] 메뉴에서 관리자 로그인 팝업을 생성할 때 관리자 비밀번호를 검증하는 함수
     */
    function _checkStep1AdminPW() {
        if (input === HDS_PASSWORD) {
            log.printDbg("Correct hds_password");
            input = "";
            showAdminLoginPopup();
            return true;
        } else {
            input = "";
            return false;
        }
    }
    
    /**
     * [jh.lee] 관리자 로그인 팝업을 생성하는 함수
     */
    function showAdminLoginPopup() {
        log.printInfo("Show AdminLoginPopup");
        var popup_data;
        
        popup_data = {
                type: KTW.ui.popup.InputDialog.TYPE.DIALOG,
                popup_type: KTW.ui.popup.InputDialog.POPUP_TYPE.ADMIN_LOGIN,
                callback: showHDSOpeningPage
        }
        
        layerManager.activateLayer(
                {
                    id: ID.ADMIN_LOGIN, type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    view : KTW.ui.popup.InputDialog,
                    params: {
                        data : popup_data,
                        enableDCA : "false"
                    }
                });
        
        popup_data = null;
    }
    
    /**
     * [jh.lee] 관리자 로그인 팝업에서 HDS 개통 화면으로 이동시 관리자 비밀번호를 검증하는 함수
     */
    function _checkStep2AdminPW() {
        if (input === HDS_PASSWORD) {
            input = "";
            return true;
        } else {
            input = "";
            return false;
        }
    }
    
    /**
     * [jh.lee] result 값에 따라 HDS 개통 화면으로 이동할지 여부를 확인
     */
    function showHDSOpeningPage(result) {
        var app;
        var url = "file:///altibrowser/nav_webapp/apps/hds_app/hds_ui.html?start=home";
        var prop = {"useType": "host", "name": "hds" };
        
        log.printInfo("Show HDSOpeningPage");
        
        if (result === RESULT.SUCCESS) {
            if (_checkStep2AdminPW() === true) {
                // [jh.lee] checkStep2AdminPW 값이 true 인경우 팝업을 닫고 HDS 개통화면으로 진입.
                app = serviceAdapter.self;
                app.ref = app.createApplication(url, true, prop);
                // [jh.lee] 개통화면 진입 전 종료해야할 것들을 전부 처리
                KTW.main.destroy();
            } else {
                // [jh.lee] checkStep2AdminPW 값이 false 인 경우
                input = "";
                layerManager.deactivateLayer(ID.ADMIN_LOGIN, true);
            }
        } else {
            input = "";
            layerManager.deactivateLayer(ID.ADMIN_LOGIN, true);
        }
    }
    
    /*************************************************************************/

    /**
     * [jh.lee] OTS 전용 히든 메뉴 **********************************************
     */
    
    /**
     * [jh.lee] 시스템 설정 메뉴 위치에서 OTS 전용 히든메뉴 진입 비밀번호 검증
     */
    function _checkOTSHiddenMenuPassword() {
        if (input === OTS_PASSWORD) {
            // [jh.lee] 히든메뉴 성공하면 히든메뉴 상태 on
            return true;
        } else {
            return false;
        }
    }
    /************************************************************************/
    
    
    /**
     * [jh.lee] 입력, 삭제, 초기화, 얻어오기 외부 함수 부분 *********************************
     */

    /**
     *
     * [jh.lee] 비밀번호 입력을 처리하는 함수
     * index 정보를 기준으로 단순 PIN 인증인지 비밀번호 변경인지를 판단한다.
     *
     * @param number 입력된 비밀번호 숫자
     * @param max_length    전체 비밀번호 길이
     * @param index         비밀번호 입력 위치(0: 기존 비밀번호, 1: 신규 비밀번호, 2: 신규 확인 비밀번호) 단순 PIN 인증에는 사용하지 않는다.
     * @returns {*}
     */
    function _inputPW(number, max_length, index) {
        log.printDbg("_inputPW(), number = " + number + ", max_length = " + max_length + ", index = " + index + ", handling_result = " + handling_result);
        
        if (handling_result === true) {
            // [jh.lee] handling_result 가 true 인 경우 비밀번호 인증 및 변경 처리 중이므로 return
            log.printDbg("_inputPW(handling_result) input : " + input);
            log.printDbg("_inputPW(handling_result) new input : " + new_input);
            log.printDbg("_inputPW(handling_result) confirm input : " + confirm_input);
            
            return false;
        }
        
        log.printDbg("_inputPW input : " + input);
        log.printDbg("_inputPW new input : " + new_input);
        log.printDbg("_inputPW confirm input : " + confirm_input);

        if (index !== null && index !== undefined) {
            // [jh.lee] 멀티 인풋
            if (index === 0) {
                if (input.length < (max_length - 1)) {
                    input += number;
                    return false;
                } else if (input.length === (max_length - 1)){
                    input += number;
                    return true;
                } else {
                    log.printDbg("-1");
                    return -1;
                }
            } else if (index === 1) {
                if (new_input.length < (max_length - 1)) {
                    new_input += number;
                    return false;
                } else if (new_input.length === (max_length - 1)){
                    new_input += number;
                    return true;
                } else {
                    return -1;
                }
            } else if (index === 2) {
                if (confirm_input.length < (max_length - 1)) {
                    confirm_input += number;
                    return false;
                } else if (confirm_input.length === (max_length - 1)){
                    confirm_input += number;
                    return true;
                } else {
                    return -1;
                }
            }
        } else {
            // [jh.lee] 싱글 인풋
            if (max_length) {
                if (input.length < (max_length - 1)) {
                    input += number;
                    return false;
                } else if (input.length === (max_length - 1)){
                    input += number;
                    return true;
                } else {
                    return true;
                }
            } else {
                input = input + number;   
            }   
        }
    }

    /**
     * [dj.son] _inputPW 와 동일한 로직 & 동작이나, 첫번째 parameter 인 password 를 한꺼번에 처리함 (기존 입력된 input 을 replace)
     */
    function _replacePW (password, max_length, index) {
        log.printDbg("_replacePW(), password = " + password + ", max_length = " + max_length + ", index = " + index + ", handling_result = " + handling_result);

        if (handling_result === true) {
            // [jh.lee] handling_result 가 true 인 경우 비밀번호 인증 및 변경 처리 중이므로 return
            log.printDbg("_replacePW(handling_result) input : " + input);
            log.printDbg("_replacePW(handling_result) new input : " + new_input);
            log.printDbg("_replacePW(handling_result) confirm input : " + confirm_input);

            return false;
        }

        log.printDbg("_replacePW input : " + input);
        log.printDbg("_replacePW new input : " + new_input);
        log.printDbg("_replacePW confirm input : " + confirm_input);

        if (index !== null && index !== undefined) {
            if (index === 0) {
                input = password;

                if (input.length < (max_length - 1)) {
                    return false;
                }
                else if (input.length === (max_length - 1)){
                    return true;
                }
                else {
                    log.printDbg("-1");
                    return -1;
                }
            }
            else if (index === 1) {
                new_input = password;

                if (new_input.length < (max_length - 1)) {
                    return false;
                }
                else if (new_input.length === (max_length - 1)){
                    return true;
                }
                else {
                    return -1;
                }
            }
            else if (index === 2) {
                confirm_input = password;

                if (confirm_input.length < (max_length - 1)) {
                    return false;
                }
                else if (confirm_input.length === (max_length - 1)){
                    return true;
                }
                else {
                    return -1;
                }
            }
        }
        else {
            input = password;

            if (max_length) {
                if (input.length < (max_length - 1)) {
                    return false;
                }
                else if (input.length === (max_length - 1)){
                    return true;
                }
                else {
                    return true;
                }
            }
        }
    }
    
    /**
     * [jh.lee] 비밀번호 삭제를 처리하는 함수
     * index 값에 따라 인증/변경 상태를 판단한다.
     * 단순 PIN 인증인 경우 전달하지 않는다.
     *
     * @param index         입력 위치 (0: 기존 비밀번호, 1: 신규 비밀번호, 2: 신규 확인 비밀번호)
     * @returns {boolean}
     */
    function _deletePW(index) {
        log.printDbg("_deletePW(), index = " + index);

        if (index) {
            // [jh.lee] 멀티 인풋
            if (index === 0) {
                if (input.length > 0) {
                    input = input.substring(0, input.length - 1);
                    return true;
                }
            } else if (index === 1) {
                if (new_input.length > 0) {
                    new_input = new_input.substring(0, new_input.length - 1);
                    return true;
                }
            } else {
                if (confirm_input.length > 0) {
                    confirm_input = confirm_input.substring(0, confirm_input.length - 1);
                    return true;
                }
            }
        } else {
            if (input.length > 0) {
                input = input.substring(0, input.length - 1);
                return true;
            }
        }
        return false;
    }
    
    /**
     * [jh.lee] 입력된 비밀번호를 가져오는 함수
     * index 값에 따라 인증/변경 상태를 판단한다.
     * 단순 PIN 인증인 경우, index 값은 전달하지 않는다.
     *
     * @param index     입력 위치 (0: 기존 비밀번호, 1: 신규 비밀번호, 2: 신규 확인 비밀번호)
     * @returns {string}
     */
    function _getPW(index) {
        log.printDbg("_getPW(), index = " + index);
        if (index) {
            // [jh.lee] 멀티 인풋
            if (index === 0) {
                return input;
            } else if (index === 1) {
                return new_input;
            } else {
                return confirm_input;
            }
        } else {
            // [jh.lee] 싱글 인풋
            return input;   
        }
    }
    
    /**
     * [jh.lee] 입력된 비밀번호를 초기화하는 함수
     * index 값에 따라 인증/변경 상태를 판단한다.
     * 단순 PIN 인증인 경우, index 값은 전달하지 않는다.
     *
     * @param index     입력 위치 (0: 기존 비밀번호, 1: 신규 비밀번호, 2: 신규 확인 비밀번호)
     * @private
     */
    function _initPW(index) {
        log.printDbg("_initPW(), index = " + index);
        // [jh.lee] initPW 호출 시점은 비밀번호를 새로 입력해야하는 상황
        // [jh.lee] 비밀번호 진행중을 표시하는 락변수를 초기화한다.
        handling_result = false;
        if (index !== undefined && index !== null) {
            // [jh.lee] 특정 인풋박스 초기화
            if (index === 0) {
                input = "";
            } else if (index === 1) {
                new_input = "";
            } else {
                confirm_input = ""; 
            }
        } else {
            // [jh.lee] 모든 인풋박스 초기화
            input = "";
            new_input = "";
            confirm_input = ""; 
        }
        
        log.printDbg("_initPW input : " + input);
        log.printDbg("_initPW new input : " + new_input);
        log.printDbg("_initPW confirm input : " + confirm_input);
    }
    
    /**
     * [jh.lee] 비밀번호 오류 횟수를 초기화 하는 함수
     */
    function _resetPWCheckCount() {
        pw_check_count = 0;
    }
    
    /**
     * [jh.lee] 비밀번호 오류 횟수를 얻는 함수
     */
    function _getPWCheckCount(){
        return pw_check_count;
    }
    
    /**
     * [jh.lee] temp_pin 리턴
     */
    function _getTempPin() {
        if (temp_pin) {
            return temp_pin;
        }
    }

    function _reqCertUserInfo(now , callback) {
        log.printDbg("_reqCertUserInfo()");
        if (callback !== null) {


            hdsManager.reqCertUserInfo(now , callback);
            return temp_pin;
        }
    }

    function _reqSMSComm(birthday, gender, nation, name, ttype, mobile, now , callback) {
        log.printDbg("_reqSMSComm()");
        if (callback !== null) {
            hdsManager.reqSMSComm(birthday, gender, nation, name, ttype, mobile, now , callback);
            return temp_pin;
        }
    }

    function _reqSMSCnfrmComm(cnum, ckey, now , callback) {
        log.printDbg("_reqSMSCnfrmComm()");
        if (callback !== null) {
            hdsManager.reqSMSCnfrmComm(cnum, ckey, now ,callback);
            return temp_pin;
        }
    }


    
    /*************************************************************************/
    
    /**
     * [jh.lee] 팝업 관련 부분 ********************************************************
     */
    
    /**
     * [jh.lee] HDS 관련 에러 팝업 생성
     */
    function showHDSErrorPopup(error_message) {

        log.printDbg("showHDSErrorPopup(), error_message = " + error_message);
        var desc = [];
        var isShowRebootBtn = false;

        if (error_message) {
            if(error_message instanceof Array){
                // [jh.lee] error_message 가 존재하는 경우
                for(var i = 0; i < error_message.length; i++) {
                    desc[i] = error_message[i];
                }
            }else{
                desc[0] = error_message;
            }

        } else {
            isShowRebootBtn = true;
            // [jh.lee] error_message 가 존재하지 않는 경우
            var messages = KTW.ERROR.CODE.E027.split("\n");

            for (var i = 0; i < messages.length; i++) {
                desc[i] = messages[i];
            }
        }


        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["알림"],
                cssObj: {}
            }],

            arrButton: [{id: "ok", name: "확인"}],

            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;
                    onShowHDSErrorPopup(buttonId);
                }
            }
        };

        if(isShowRebootBtn === true) {
            popupData.arrButton.push({id: "reboot", name: "재부팅"});

        }

        for (var i = 0; i < desc.length; i++) {
            popupData.arrMessage.push({
                type: "MSG_45",
                message: [desc[i]],
                cssObj: {}
            });
        }

        var layerOptions = {
            obj: {
                id: "hds_error",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData,
                    enableHotKey : "false"
                }
            },
            visible: true,
            cbActivate: function () {}
        };

        if (KTW.managers.service.KidsModeManager.isKidsModeAdultAuthPopupOpend()
            || KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            layerOptions.obj.priority = KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5;
            layerOptions.obj.params.enableChannelKey = "false";
            layerOptions.obj.params.enableMenuKey = "false";
            layerOptions.obj.params.enableHotKey = "false";
        }

        layerManager.activateLayer(layerOptions);
    }
    
    /**
     * [jh.lee] onShowHDSErrorPopup 에 대한 콜백
     */
    function onShowHDSErrorPopup(buttonId) {

        // [jh.lee] 확인 버튼 또는 back, exit 키로 팝업이 종료되는 경우이기 때문에 팝업 닫음
        // [jh.lee] result 값이 무엇이던 상관없이 모든 팝업 제거.
        // [jh.lee] HDS 관련된 부분에서 실패한 것이기 때문에 listener에 전달하는 값은 공용으로 사용

        KTW.ui.LayerManager.deactivateLayer({
            id: "hds_error",
            remove: true
        });
        listener(KTW.managers.auth.AuthManager.RESPONSE.HDS_BUY_AUTH_FAIL);

        if (buttonId === "reboot") {
            // [jh.lee] 만약 해당 팝업에 재부팅 버튼이 존재하고 재부팅 버튼을 누르는 경우
            // [jh.lee] 재부팅
            KTW.oipf.AdapterHandler.extensionAdapter.reboot();
        }
    }
    
    /**
     * [jh.lee] 비밀번호 3회 이상 오류 팝업 생성
     */
    function showAdvicePasswordInitPopup() {
        log.printDbg("showAdvicePasswordInitPopup()");

        //if (KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
        //    // [jh.lee] 시청시간제한 팝업인 경우
        //    // [jh.lee] 웹홈포탈의 경우 팝업을 생성하지 않고 하단 심플 팝업을 생성한다.
        //    // [jh.lee] 시청시간제한팝업에서만 사용하기 때문에 콜백신호는 CANCEL로 그대로 사용한다.
        //    listener(KTW.managers.auth.AuthManager.RESPONSE.CANCEL);
        //} else {
        //}


        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["알림"],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_40,
                message: ["입력하신 번호가 3회 연속 일치하지 않았습니다"],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_30,
                message: ["비밀번호를 잊어버렸을 경우," , "비밀번호 변경/초기화를 이용해 주세요"],
                cssObj: {}
            }],

            arrButton: [{id: "ok", name: "바로가기"}, {id: "cancel", name: "취소"}],

            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;
                    var consumed = onGoPasswordInit(buttonId);
                    if(consumed !== true) {
                        isCalled = false;
                    }
                    return consumed;
                }
            }
        };

        var layerOptions = {
            obj: {
                id: "PasswordCheckErrorPopup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData,
                    enableDCA : "false"
                }
            },
            visible: true,
            cbActivate: function () {}
        };

        if (KTW.managers.service.KidsModeManager.isKidsModeAdultAuthPopupOpend()
            || KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            layerOptions.obj.priority = KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5;
            layerOptions.obj.params.enableChannelKey = "false";
            layerOptions.obj.params.enableMenuKey = "false";
            layerOptions.obj.params.enableHotKey = "false";
        }

        layerManager.activateLayer(layerOptions);
    }
   
    /**
     * [jh.lee] 비밀번호 3회 이상오류시 발생하는 팝업에서 버튼입력에 따라 실행될 콜백
     */
    function onGoPasswordInit(buttonId) {
        log.printDbg("onGoPasswordInit(), buttonId = " + buttonId);


        if (buttonId === "ok") {
            // [jh.lee] 바로가기 버튼
            input = "";
            pw_check_count = 0;

            var isSystemPriority = false;
            if (KTW.managers.service.KidsModeManager.isKidsModeAdultAuthPopupOpend()
                || KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
                isSystemPriority = true;
            }

            KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CHANGE_PIN,
                isSystemPriority: isSystemPriority,
                updateSubTitle: false
            });

            listener(KTW.managers.auth.AuthManager.RESPONSE.GOTO_INIT);

            KTW.ui.LayerManager.deactivateLayer({
                id: "PasswordCheckErrorPopup",
                remove: true
            });

            return true;
        } else if (buttonId === "cancel") {
            KTW.ui.LayerManager.deactivateLayer({
                id: "PasswordCheckErrorPopup",
                remove: true
            });
            listener(KTW.managers.auth.AuthManager.RESPONSE.CANCEL);
            return true;
        }else {
            if(buttonId === KTW.KEY_CODE.BACK || buttonId === KTW.KEY_CODE.EXIT) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: "PasswordCheckErrorPopup",
                    remove: true
                });
                listener(KTW.managers.auth.AuthManager.RESPONSE.CANCEL);
                return true;
            }
            return false;
        }
    }
    
    /**
     * [jh.lee] HDS 단계에서 예외상황의 오류가 발생해 타임아웃이 걸릴 때 생성할 팝업
     */
    function _showHDSPasswordErrorPopup(callback) {
        log.printDbg("_showHDSPasswordErrorPopup()");
        var desc = [];
        var messages;
        var title;

        // [jh.lee] ajax timeout 신호 보다 로딩 timeout 이 먼저 도달하는 경우 ajax 요청을 abort 한다.
        hdsManager.abortAuthPin();

        messages = KTW.ERROR.CODE.E027.split("\n");

        var isCalled = false;
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: ["오류"],
                cssObj: {}
            }],

            arrButton: [{id: "ok", name: "확인"}, {id: "reboot", name: "재부팅"}],

            cbAction: function (buttonId) {
                if (!isCalled) {
                    isCalled = true;
                    if(callback !== undefined && callback !== null) {
                        callback(buttonId);
                    }
                }
            }
        };

        for (var i = 0; messages && messages instanceof Array && i < messages.length; i++) {
            popupData.arrMessage.push({
                type: "MSG_45",
                message: [messages[i]],
                cssObj: {}
            });
        }

        var layerOptions = {
            obj: {
                id: "hds_auth_timeout_error",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true,
            cbActivate: function () {}
        };

        if (KTW.managers.service.KidsModeManager.isKidsModeAdultAuthPopupOpend()
            || KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            layerOptions.obj.priority = KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5;
            layerOptions.obj.params.enableChannelKey = "false";
            layerOptions.obj.params.enableMenuKey = "false";
            layerOptions.obj.params.enableHotKey = "false";
        }

        layerManager.activateLayer(layerOptions);
    }
    
    /**
     * [jh.lee] 비밀번호 처리 중 플래그를 초기화
     * [jh.lee] 비밀번호 인증 및 변경 부분에서 async 처리를 위해 로딩 팝업을 사용.
     * [jh.lee] 보통 로딩팝업 타임아웃시 실행할 콜백을 걸고 있지 않기 때문에 로딩팝업 콜백이 없을 시 비밀번호 락을 해제하도록 호출
     */
    function _resetHandlingResult() {
        handling_result = false;
    }
    
    /*************************************************************************/

    /**
     * [jh.lee] 월정액 단절 시 예외사항으로 3회 카운트를 감소
     */
    function _decreaseCheckCount() {
        pw_check_count--;
    }

    /**
     * 2017.05.26 dhlee
     * 현재 설정된 PR 값을 전달한다.
     *
     * @returns {*}
     */
    function _getPR() {
        if (parental_rating === null || parental_rating === undefined) {
            parental_rating = casAdapter.getParentalRating();
        }
        return parental_rating;
    }
    
    return {
        init: _init,
        inputPW : _inputPW,
        replacePW: _replacePW,
        deletePW : _deletePW,
        getPW : _getPW,
        initPW : _initPW,
        resetPWCheckCount : _resetPWCheckCount,
        resetHandlingResult : _resetHandlingResult,
        getPWCheckCount:_getPWCheckCount,
        getTempPin: _getTempPin,

        reqCertUserInfo : _reqCertUserInfo, // 휴대폰 인증을 위한 사용자 정보 조회
        reqSMSComm : _reqSMSComm,           // 휴대폰 인증 번호 전송 요청 및 인증번호 확득
        reqSMSCnfrmComm : _reqSMSCnfrmComm,  // 휴대폰 인증번호 체크
        
        checkUserPW : _checkUserPWAsync,//_checkUserPW,
        confirmNewPW:_confirmNewPW,
        
        checkStep1AdminPW : _checkStep1AdminPW,
        checkStep2AdminPW:_checkStep2AdminPW,
        checkOTSHiddenMenuPassword: _checkOTSHiddenMenuPassword,
        
        showHDSPasswordErrorPopup: _showHDSPasswordErrorPopup,
        decreaseCheckCount : _decreaseCheckCount,

        getPR: _getPR
    };
    
}());

//[jh.lee] 인증 타입
Object.defineProperty(KTW.managers.auth.AuthManager, "AUTH_TYPE", {
    value: {
        /**
         * [jh.lee] 성인인증 비밀번호 변경 타입
         */
        CHANGE_ADULT_PIN : 0,
        /**
         * [jh.lee] 구매인증 비밀번호 변경 전 성인인증 타입
         */
        AUTH_ADULT_PIN_FOR_CHANGE : 1,
        /**
         * [jh.lee] 구매인증 비밀번호 변경 타입
         */
        CHANGE_BUY_PIN : 2,
        /**
         * [jh.lee] 성인인증 
         */
        AUTH_ADULT_PIN : 3,
        /**
         * [jh.lee] 구매인증
         */
        AUTH_BUY_PIN : 4,
        /**
         * [jh.lee] 성인인증에 대한 현재 비밀번호 검증
         */
        AUTH_CURRENT_ADULT_PIN : 5,
        /**
         * [jh.lee] 구매인증에 대한 현재 비밀번호 검증
         */
        AUTH_CURRENT_BUY_PIN : 6,
        /**
         * [jh.lee] MINI EPG 에서 제한, 연령 등 제한 아이프레임에서 Smart Card 가 빠진 경우 검증
         */
        AUTH_ADULT_PIN_FOR_MINIEPG : 7
    },
    writable: false,
    configurable: false
});

Object.defineProperty(KTW.managers.auth.AuthManager, "RESPONSE", {
    value: {
        // [jh.lee] 3회 오류 팝업에서 바로가기 버튼 입력시
        GOTO_INIT : -1,
        // [jh.lee] CAS 성인인증 비밀번호 인증 성공시
        CAS_AUTH_SUCCESS : 5,
        // [jh.lee] CAS 성인인증 비밀번호 인증 실패시
        CAS_AUTH_FAIL : 6,
        // [jh.lee] 3회 오류 팝업에서 취소 버튼 입력시 또는 Back, Exit 등의 키로 종료시
        CANCEL : 7,
        // [jh.lee] HDS 성인인증 비밀번호 인증 실패시 (dhlee 2016.10.07 현재는 사용하지 않는다)
        HDS_ADULT_AUTH_FAIL : 8,
        // [jh.lee] CAS 성인인증 비밀번호 변경 성공시
        CAS_CHANGE_SUCCESS : 9,
        // [jh.lee] HDS 구매인증 비밀번호 변경 성공시
        HDS_BUY_CHANGE_SUCCESS : 10,
        // [jh.lee] HDS 구매인증 비밀번호 인증 성공시
        HDS_BUY_AUTH_SUCCESS : 11,
        // [jh.lee] HDS 구매인증 비밀번호 인증 실패시
        HDS_BUY_AUTH_FAIL : 12,
        // [jh.lee] 입력 비밀번호 검증시 4자리가 아닌 경우 (비밀번호 변경시에만 사용됨)
        WRONG_LENGTH : 13,
        // [jh.lee] 새로입력한 비밀번호와 확인 비밀번호가 다른 경우 (비밀번호 변경시에만 사용됨)
        MISMATCH_PASSWORD : 14,
        // [jh.lee] 정상적으로 비밀번호가 입력된 경우 (비밀번호 변경시에만 사용됨)
        CORRECT_PASSWORD : 15,
        // [jh.lee] 구매인증번호 변경시 확인, 구매인증번호 변경시 현재비밀번호 검증, 결제 및 해지 할 때 네트워크 단선 응답
        DISCONNECTED_NETWORK : 16,
        // [jh.lee] Smart Card 가 제거된 경우 (성인인증 변경 팝업에서 확인 키 누르는 경우)
        DISCONNECTED_SMART_CARD : 17,
        // 2017.09.29 dhlee
        // 에러 상황일 때(에러 팝업을 보여주는 상황) 전달할 RESPONSE CODE
        HDS_BUY_AUTH_ERROR: 99
    },
    writable: false,
    configurable: false
});

// [jh.lee] 인풋박스 갯수에 따라 타입을 분리
Object.defineProperty(KTW.managers.auth.AuthManager, "FAIL_TYPE", {
    value: {
        CAS : 0,
        CHECK_LENGTH : 1,
        CHECK_PIN : 2,
        HDS : 3 ,
        INIT_PIN : 5
    },
    writable: false,
    configurable: false
});
