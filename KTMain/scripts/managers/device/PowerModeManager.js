"use strict";

/**
 * manage STB power state
 *
 * TODO 2016.08.05 auto reboot 시나리오가 적용 되어 있는데 UHD STB도 대상인지 확인할 것
 * 자동 재부팅 시나리오는 UHD STB은 대상이 아님
 * 
 */
KTW.managers.device.PowerModeManager = (function () {
    
    var ACTION = {
        AUTO_STANDBY : 0,
        AUTO_POWER : 1
    };

    // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
    /*
    var AUTO_STANDBY = {
        POPUP_ID : "auto_standby_popup",
        USER_WATING_TIME : 30 * 60 * 1000, // 30 minute
        DURATION : 3 * 60 * 60 * 1000 // 3 hours
    };
    */

    var AUTO_POWER = {
        POPUP_ID : "auto_power_popup",
        MODE : {
            NONE : 0,
            ON : 1,
            OFF : 2
        },
        USER_WATING_TIME : 30 * 1000, // 30 second
        REBOOT_TIME : {
            ON_STATE : 60 * 1000, // 1 minute
            OFF_STATE : 10 * 1000 // 10 second
        }
    };

    var AUTO_POWER_REPEAT = {
        ONCE: "false",
        EVERYDAY: "true",
        SELECTIVE: "selective"
    };

    var isSelectedDay = false;
    var repeat_dayArray = [];


    
    var CONFIG_DEF = KTW.oipf.Def.CONFIG;
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var hwAdapter = null;
    var amocManager = null;
    var storageManager = null;
    var stateManager = null;
    var layerManager = null;
    var caHandler = null;
    //var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
    var networkManager = null;
    
    var listeners = null;
    
    /**
     * 초기 power state는 ON
     * 하지만 의도적으로 init 과정에서 현재 상태를 획득한다.
     */
    var power_state = CONFIG_DEF.POWER_MODE.ON;

    // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
    // 자동대기 기능 on/off 여부
    //var enable_auto_standby = false;
    
    var auto_power_mode = AUTO_POWER.MODE.NONE;
    var is_showing_auto_power_popup = false;

    // 자동 전원 설정 값
    var auto_power_setting = {
        repeat : false, // 반복 여부
        on_time : "0000",  // 켜짐 시간
        off_time : "0000", // 꺼짐 시간
        executed : false, // 실행 여부 (동작 하지 않았는데 리부팅 될 경우 다시 시도하기 위한 값)
        last_action_mode : AUTO_POWER.MODE.NONE, // 마지막 수행한 동작
        completed : [ false, false ] // on/off 수행완료 여부
    };

    // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
    //var auto_standby_time = -1;
    //var is_showing_auto_standby_popup = false;
    //
    //var auto_standby_timer = null;
    //var auto_standby_user_waiting_timer = null;
    var auto_power_timer = null;
    var auto_power_user_waiting_timer = null;
    //var reboot_timer = null;
    var api_call_timer = null;
    
    // [jh.lee] 첫키 입력 flag
    var received_first_key_event = false;

    var last_state = undefined;
    var android_inactive_state_on_standby = false;

    function _init() {
    	// 테스트용 코드
    	if (KTW.PLATFORM_EXE === "PC") {
    		KTW.CONSTANT.IS_UHD = true;
    	}
    	
        listeners = [];

        hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        amocManager = KTW.managers.http.amocManager;
        storageManager = KTW.managers.StorageManager;
        stateManager = KTW.managers.service.StateManager;
        layerManager = KTW.ui.LayerManager;
        caHandler = KTW.ca.CaHandler;
        networkManager = KTW.managers.device.NetworkManager;
        
        hwAdapter.addEventListener(CONFIG_DEF.EVENT.POWER_STATE_CHANGE, onPowerStateChange);
        storageManager.ps.addStorageDataChangeListener(onStorageDataChanged);
        // [jh.lee] state가 변경될 시 실행할 리스너 등록
        stateManager.addServiceStateListener(onServiceStateChange);
        //stateManager.removeServiceStateListener(onServiceStateChange);

        // 2017.02.15 dhlee 기가지니 관련 내용 추가
        if (KTW.CONSTANT.STB_TYPE.ANDROID) {
            KTW.oipf.AdapterHandler.androidAdapter.setStateChangedListener(onAndroidStateChanged);
        }
        
        if (KTW.PLATFORM_EXE !== "PC") {
            power_state = hwAdapter.getPowerState();
            log.printDbg("_init() power_state = " + getPowerStateString(power_state));
        }

        // app.properties 관련 내용 삭제 후 loadProperty 에서 하는게 없어짐 -> loadProperty 삭제
        // loadProperty();
        
        // 부팅 시 power state가 running 모드인 경우에만 자동대기 처리를 하도록 함.
        // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
        /*
        var clear_auto_standby_timer = undefined;
        if (power_state === CONFIG_DEF.POWER_MODE.ON) {
        	clear_auto_standby_timer = false;

            // 2017.01.04 dhlee
            // 기가지니 형상에서는 init 과정에서 자동대기모드 timer를
            // 활성화 하지 않기 위해서 의도적으로 해당 값을 true 로 처리함
            // 기가지니의 경우 부팅되더라도 홈포탈 상태가 아니기 때문에
            // 홈포탈 실행하기 전에 자동대기모드 timer가 동작되는 문제가 있어서 수정함
            if (KTW.CONSTANT.STB_TYPE.ANDROID === true) {
                clear_auto_standby_timer = true;
            }
        }
        else {
        	clear_auto_standby_timer = true;
        }
        setAutoStandbyMode(true, clear_auto_standby_timer);
        */
        setAutoPowerMode(true);

        last_state = stateManager.serviceState;
    }

    // app.properties 관련 내용 삭제 후 loadProperty 에서 하는게 없어짐 -> loadProperty 삭제
    //function loadProperty() {
        // app.properties 관련 내용 삭제
        // 위에 정의한 AUTO_STANDBY.DURATION 값과 util.getProperty("AUTO_STANDBY_TIME") 으로 얻어온 시간값이 같음
        // 필요 없어 보임
        //var property_as_time = util.getProperty("AUTO_STANDBY_TIME");
        //if (property_as_time) {
        //    log.printDbg("loadProperty() property_as_time = " + property_as_time);
        //    AUTO_STANDBY.DURATION = parseInt(property_as_time) * 60 * 1000;
        //}

        // app.properties 관련 내용 삭제
        // 위에 정의한 AUTO_STANDBY.USER_WATING_TIME 값과 util.getProperty("AUTO_STANDBY_POPUP_WATING_TIME") 으로 얻어온 시간값이 같음
        // 필요 없어 보임
        //var property_as_popup_wating_time = util.getProperty("AUTO_STANDBY_POPUP_WATING_TIME");
        //if (property_as_popup_wating_time) {
        //    log.printDbg("loadProperty() property_as_popup_wating_time = " + property_as_popup_wating_time);
        //    AUTO_STANDBY.USER_WATING_TIME = parseInt(property_as_popup_wating_time) * 60 * 1000;
        //}
    //}
    
    /**
     * 2017.04.10 dhlee 자동대기모드 진입 기능은 네비게이터에서 처리한다.
     * KeyEvent에 의해 standby time 값을 reset 하는 경우
     * LayerManger에서 KeyEvent를 받으면 무조건 호출하도록 함
     * 단, home에서 받지 못하는 KeyEvent가 있기 때문에
     * 완벽한 standby mode 진입을 위한 KeyEvent check를 하지 못한다.
     */
    /*
    function _resetAutoStandbyTime() {
        
        // power on 상태이고 자동대기모드 popup이 뜨지 않은 상태에서만 처리한다.
        if (stateManager.isRunningState() === true &&
                !stateManager.isOtherAppState() &&
                power_state === CONFIG_DEF.POWER_MODE.ON &&
                is_showing_auto_standby_popup === false) {
            setAutoStandbyMode(false);
        }
    }
    */
    
    function _addPowerModeChangeListener(listener) {
        listeners.push(listener);
    }
    
    function _removePowerModeChangeListener(listener) {
        var index = listeners.indexOf(listener);
        if (index !== -1) {
            listeners.splice(index, 1);
        }
    }
    
    /**
     * 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리한다.
     * @param reset - if booting or changing setting, true otherwise false
     * @param clear_timer - if booting in standby mode, true otherwise undefined or false
     * (대기모드로 부팅하는 경우에만 true 값으로 assign 됨)
     */
    /*
    function setAutoStandbyMode(reset, clear_timer) {
        
        // 부팅 시 또는 설정 변경 시점에만 설정 값을 읽어와서 처리한다.
        if (reset === true) {
            var enabled = storageManager.ps.load(storageManager.KEY.AUTO_STANDBY);
            log.printDbg("setAutoStandbyMode() enabled = " + enabled);
            enable_auto_standby = enabled === "true" ? true : false;
        }
        
        if (enable_auto_standby === true && clear_timer !== true) {
            auto_standby_time = new Date().getTime();
            
            checkAutoStandbyTime();
        }
        else {
            clearAutoStandbyTimer();
            auto_standby_time = -1;
        }
    }
    */

    /**
     * 자동 전원 몯를 설정한다.
     * @param reset  새로 설정된 경우. 값이 true
     * @param next
     */
    function setAutoPowerMode(reset, next) {
        
        if (reset === true) {
            // 저장된 configuration 값 로드
            var stored_data = storageManager.ps.load(storageManager.KEY.AUTO_POWER);
            log.printDbg("setAutoPowerMode() stored_data = " + stored_data);
            
            if (util.isValidVariable(stored_data) === true) {
                var temp = stored_data.split(";");

                //2018.01.24 sw.nam 바뀐 포맷 적용 (켜짐, 꺼짐 이 따로 분리...)
                // repeat ; startTime ; endTime ; executed ; 요일(월 화 수 목 금 토 일)..
                auto_power_setting = {
                    //repeat : temp[0] === "true" ? true : false,
                    on_time : temp[1],
                    off_time : temp[2],
                    executed: temp[3] === "true" ? true : false,
                    last_action_mode : AUTO_POWER.MODE.NONE,
                    completed : [ false, false ] // [0] 은꺼짐 , [1] 은 켜짐
                };


                // sw.nam 요일이 따로 선택되어 있는 경우.
                // 매일 반복되는 설정 로직을 그대로 두고, 실제 설정된 시간에 타이머가 동작 될 때 요일을 확인해서 실행 시킬지 말지 결정한다.
                // repeat 프로퍼티 검사..
                var repeat = temp[0];
                switch(repeat) {
                    case AUTO_POWER_REPEAT.ONCE :
                        // "한번만" 설정인 경우
                        auto_power_setting.repeat = false;
                        isSelectedDay = false;

                        break;
                    case AUTO_POWER_REPEAT.EVERYDAY :
                        // "매일" 설정인 경우
                        auto_power_setting.repeat = true;
                        isSelectedDay = false;
                        break;
                    case AUTO_POWER_REPEAT.SELECTIVE :
                        // "요일선택" 설정인 경우
                        // repeat 을 true 로 바꾸고, 뒤에 요일이 따라오므로, 파라미터를 확인한다. temp[4] ~ temp[10]
                        auto_power_setting.repeat = true;
                        isSelectedDay = true;
                        for(var i = 0; i < 7 ; i++) {
                            // 요일 배열에 넣는다.
                            log.printDbg("temp_"+4+i+" "+temp[4+i]);
                            repeat_dayArray[i] = temp[4+i];
                        }
                        log.printDbg("repeat_dayArray =="+repeat_dayArray);
                        break;
               }

                // 이전에 수행된 mode 가 있는지 체크. 한번만 일 경우에 사용된다. (요일선택일떄는 간섭 안받음)
                if (auto_power_setting.executed === false) {
                    // 이전에 수행된 mode가 있는지 check
                    if (util.isValidVariable(temp[4]) === true) {
                        if (temp[4] === "off") {
                            auto_power_setting.completed[1] = true;
                        }
                        else if (temp[4] === "on") {
                            auto_power_setting.completed[0] = true;
                        }
                    }
                }
            }
            // auto_power_mode 값 초기화
            auto_power_mode = AUTO_POWER.MODE.NONE;
        }
        
        log.printDbg("setAutoPowerMode() auto_power_setting = " + JSON.stringify(auto_power_setting));
        
        clearAutoPowerTimer();

        // [sw.nam] 자동 전원 설정은 크게 4가지
        // 1. 켜짐만 있는경우, 2. 꺼짐만 있는 경우, 3. 둘 다 있는 경우 , 4. 둘다 안되는 경우 (구현 안 하면 됨)
        // 한개씩 차례대로 구현한다.
        if(auto_power_setting.on_time !== "----" && auto_power_setting.off_time === "----") {
            // 자동 켜짐만 설정되어 있는 경우
            log.printDbg("켜짐만");
            auto_power_mode = AUTO_POWER.MODE.ON;
            if(auto_power_setting.repeat === true) {
                // 매일 반복하는 경우 매일 자동 켜짐 되도록 수행
                setAutoTimer(checkAutoPowerTimeEach(next,auto_power_setting.on_time), ACTION.AUTO_POWER);
            }else if(auto_power_setting.repeat === false && auto_power_setting.executed === false) {
                // 한번만 수행되는 경우, 설정된 시간 값으로 모드 수행
                if(auto_power_setting.completed[0] === false) {
                     var checkTime = checkAutoPowerTimeEach(false,auto_power_setting.on_time);
                    setAutoTimer(checkTime, ACTION.AUTO_POWER);
                }else {
                }

            }
        }else if(auto_power_setting.on_time == "----" && auto_power_setting.off_time !== "----") {
            // 자동 꺼짐만 설정되어 있는 경우
            log.printDbg("꺼짐만");
            auto_power_mode = AUTO_POWER.MODE.OFF;
            if(auto_power_setting.repeat === true) {
                // 매일 반복하는 경우 매일 자동 꺼짐 되도록 수행
                setAutoTimer(checkAutoPowerTimeEach(next,auto_power_setting.off_time), ACTION.AUTO_POWER);
            }else if(auto_power_setting.repeat === false && auto_power_setting.executed === false) {
                if(auto_power_setting.completed[1] === false) {
                    var checkTime = checkAutoPowerTimeEach(false,auto_power_setting.off_time);
                        setAutoTimer(checkTime, ACTION.AUTO_POWER);
                }else {

                }
            }
        }else if(auto_power_setting.on_time !== "----" || auto_power_setting.off_time !== "----") {
            // 둘 다 설정되어 있는 경우
            if (auto_power_setting.repeat === true) {
                // 매일 반복하는 경우 무조건 timer 수행
                setAutoTimer(checkAutoPowerTime(next), ACTION.AUTO_POWER);
            }  else if (auto_power_setting.repeat === false &&
                auto_power_setting.executed === false) {
                // 한번만 처리되는 경우 완료가 되지 않은 상태면
                // 수행된 mode 에 따라서 timer가 동작되도록 한다.
                var check_time = checkAutoPowerTime();
                if (auto_power_mode === AUTO_POWER.MODE.ON &&
                    auto_power_setting.completed[0] === false) {
                    setAutoTimer(check_time, ACTION.AUTO_POWER);
                }
                else if (auto_power_mode === AUTO_POWER.MODE.OFF &&
                    auto_power_setting.completed[1] === false) {
                    setAutoTimer(check_time, ACTION.AUTO_POWER);
                }else {

                }
            }
        }

/*            ///////////////////////////////////////////////////////////////////
        if (auto_power_setting.on_time !== "0000" ||
                auto_power_setting.off_time !== "0000") {

            if (auto_power_setting.repeat === true) {
                // 매일 반복하는 경우 무조건 timer 수행
                setAutoTimer(checkAutoPowerTime(next), ACTION.AUTO_POWER);
            }
            else if (auto_power_setting.repeat === false &&
                    auto_power_setting.executed === false) {
                // 한번만 처리되는 경우 완료가 되지 않은 상태면
                // 수행된 mode 에 따라서 timer가 동작되도록 한다.
                var check_time = checkAutoPowerTime();
                if (auto_power_mode === AUTO_POWER.MODE.ON &&
                        auto_power_setting.completed[0] === false) {
                    setAutoTimer(check_time, ACTION.AUTO_POWER);
                }
                else if (auto_power_mode === AUTO_POWER.MODE.OFF &&
                        auto_power_setting.completed[1] === false) {
                    setAutoTimer(check_time, ACTION.AUTO_POWER);
                }
            }
        }
        ////////////////////////////////////////////////////////*/
    }
    
    /**
     * 자동전원 동작 수행여부 정리
     * 
     * @param cancel - 사용자 취소 여부
     */
    function updateAutoPowerMode(cancel) {
        log.printDbg("updateAutoPowerMode() auto_power_mode = " + getAutoPowerModeString(auto_power_mode));

        var changed = false;
        
        if (auto_power_setting.repeat === false) {
            if (auto_power_mode === AUTO_POWER.MODE.ON &&
                    auto_power_setting.completed[0] === false) {
                changed = true;
                auto_power_setting.completed[0] = true;
            }
            
            if (auto_power_mode === AUTO_POWER.MODE.OFF &&
                    auto_power_setting.completed[1] === false) {
                changed = true;
                auto_power_setting.completed[1] = true;
            }
            
            if (auto_power_setting.completed[0] === true &&
                    auto_power_setting.completed[1] === true) {
                auto_power_setting.executed = true;
            }
            
            // 변경사항이 있는 경우에만 storage에 저장한다.
            if (changed === true) {
                var setting_value = "";
                setting_value += auto_power_setting.repeat === true ? "true;" : "false;";
                setting_value += auto_power_setting.on_time + ";" + auto_power_setting.off_time;
                setting_value += auto_power_setting.executed === true ? ";true" : ";false";

                if (auto_power_setting.executed === false) {
                    if (auto_power_setting.completed[0] === true) {
                        setting_value += ";on";
                    }
                    else if (auto_power_setting.completed[1] === true) {
                        setting_value += ";off";
                    }
                }
                log.printDbg("setting_Value "+setting_value);
                storageManager.ps.save(storageManager.KEY.AUTO_POWER, setting_value);
            }
        }
        else {
        	// 매일 반복하는 경우에는 다시 timer check 처리함
        	setAutoPowerMode(false, cancel);
        }
    }

    /*
    function setAutoTimer(period, action) {
        log.printDbg("setAutoTimer(" + action + ") period = " + 
        		(period / (60 * 1000)).toFixed(1) + " minute(s)");
        
        if (action === ACTION.AUTO_STANDBY) {
            clearAutoStandbyTimer();
            auto_standby_timer = setTimeout(checkAutoStandbyTime, period);
        }
        else if (action === ACTION.AUTO_POWER) {
            if (auto_power_mode === AUTO_POWER.MODE.OFF) {
                period = period - AUTO_POWER.USER_WATING_TIME;
            }
            auto_power_timer = setTimeout(onAutoPowerTimeWentOff, period);
        }
    }
    */

    /**
     * 자동 전원을 위한 timer를 set 한다.
     * 자동 대기 모드 진입 기능은 네비게이터에서 처리하므로 제외한다.
     *
     * @param period
     * @param action
     */
    function setAutoTimer(period, action) {
        log.printDbg("setAutoTimer(" + action + ") period = " +
        (period / (60 * 1000)).toFixed(1) + " minute(s)");

        if (action === ACTION.AUTO_POWER) {
            if (auto_power_mode === AUTO_POWER.MODE.OFF) {
                period = period - AUTO_POWER.USER_WATING_TIME;
            }
            auto_power_timer = setTimeout(onAutoPowerTimeWentOff, period);
        }
    }
    
    /**
     * 주기적으로 자동대기모드 시간을 확인하는 function
     * 
     */
    /*
    function checkAutoStandbyTime() {
        var cur_time = new Date().getTime();
        
        var diff = cur_time - auto_standby_time;
        log.printDbg("checkAutoStandbyTime() diff = " + diff);
        
        var auto_standby_period = AUTO_STANDBY.DURATION;
        var user_waiting_time = AUTO_STANDBY.USER_WATING_TIME;
        
        log.printDbg("checkAutoStandbyTime() auto_standby_period = " + auto_standby_period);
        log.printDbg("checkAutoStandbyTime() user_waiting_time = " + user_waiting_time);
        
        var check_time = auto_standby_period - user_waiting_time;
        
        log.printDbg("checkAutoStandbyTime() check_time = " + check_time);
        
        if (diff >= check_time) {
            clearAutoStandbyUserWaitingTimer();
            auto_standby_user_waiting_timer = setTimeout(changePowerStateToStandby, user_waiting_time);
            showStandbyPopup(ACTION.AUTO_STANDBY);
        }
        else {
            // check 주기
            // 1. 1시간 이상 남았을 경우 1시간 뒤 check
            // 2. 30분 남았을 경우 30분마다 check
            // 3. 10분 남았을 경우 10분마다 check
            // 4. 마지막으로 1분마다 check
            // 이렇게 하는 이유는 javascript timer가 정확하지 않기 때문임
            var period = 60 * 1000;
            if (check_time - diff >= 60 * 60 * 1000) {
                period = 60 * 60 * 1000; 
            }
            else if (check_time - diff >= 30 * 60 * 1000) {
                period = 30 * 60 * 1000;
            }
            else if (check_time - diff >= 10 * 60 * 1000) {
                period = 10 * 60 * 1000;
            }
            
            setAutoTimer(period, ACTION.AUTO_STANDBY);
        }
    }
    */


    /**
     * 자동 전원 켜짐 및 꺼짐 각가의 동작 시간을 체크하는 함수
     * @param next
     */
    function checkAutoPowerTimeEach(next,selectedTime) {
        var cur_time_org = KTW.utils.util.getFormatDate(new Date(), "HHmmss");

        var cur_time = getMilliseconds(cur_time_org);
        var selected_time = getMilliseconds(selectedTime);

        log.printDbg("checkAutoPowerTime() cur_time = " + cur_time + ", selected_time = " + selected_time + ",");

        var check_time = 0;
        if (next === true) {
            // 매일 설정에서 선택된 시간이 지난 경우와 지나지 않은 경우 두가지가 존재하므로
            // 두가지 경우에 대해 timer 을 계산해야 함
            // 이 경우는 시작 시간을 지난 경우와
            // 종료 시간을 지난 경우 두 가지만 존재함
            if (cur_time < selected_time) {
                // 선택된 설정의 시간을 지나지 않은 경우
                // 오늘 실행되도록 한다.
                check_time = selected_time - cur_time

            }else {
                // 24시간 범위에 대한 계산을 처리한다.
                check_time = (24 * 3600 * 1000) - cur_time + selected_time;
            }
        }
        else {
            if (cur_time < selected_time) {
                // 자동 전원 설정 시간 범위 전
                check_time = selected_time - cur_time;
            }
            else {
                // 자동 전원 설정 시간을 이미 지난 경우
                // 다음날 시간이 도래하기 때문에
                // 오늘 기준으로 남은 시간 + 첫번째 도래하는 시간으로 재계산
                check_time = (24 * 3600 * 1000) - cur_time + selected_time;
            }
        }

        log.printDbg("checkAutoPowerTime() auto_power_mode = " + getAutoPowerModeString(auto_power_mode));
        log.printDbg("checkAutoPowerTime() check_time = " +
        (check_time / (60 * 1000)).toFixed(1) + " minute(s)");

        return check_time;
    }


    /**
     * 자동 전원 동작 mode 및 timer 시간 값 확인
     * 
     * @param next - 다음 동작 모드 설정 여부
     * 사용자가 의도적으로 취소한 경우 이미 해당 동작이 수행되었다고
     * 간주해서 처리해야 함(단, 매일 반복 설정 시에만 해당함)
     */
    function checkAutoPowerTime(next) {
        
        var cur_time_org = KTW.utils.util.getFormatDate(new Date(), "HHmmss");
        
        var cur_time = getMilliseconds(cur_time_org);
        var on_time = getMilliseconds(auto_power_setting.on_time);
        var off_time = getMilliseconds(auto_power_setting.off_time);
        
        log.printDbg("checkAutoPowerTime() cur_time = " + cur_time + ", on_time = " + on_time + ", off_time = " + off_time);

        var reverse = false;
        var first_time = on_time;
        var second_time = off_time;

        // 시작시간이 종료 시간보다 크면(종료가 먼저 실행되는거면)
        if (on_time > off_time) {
            reverse = true;
            first_time = off_time;
            second_time = on_time;
        }
       
        // 가장 빨리 도래하는 mode로 설정되도록 함(꺼짐이 빠르면 꺼짐이 먼저, 켜짐이 빠르면 켜짐이 먼저 . reverse 가 true 이면 종료가 먼저 실행
        auto_power_mode = reverse === false ?
                AUTO_POWER.MODE.ON : AUTO_POWER.MODE.OFF;
        
        // 현재 시간을 기준으로 가장 빨리 도래하는 시간까지를 계산하여 처리한다.
        // 단, 꺼짐 시간이 켜짐보다 나중일 경우를 고려해서
        // timer가 종료될 때 동작해야 하는 POWER_MODE를 설정한다.
        // 
        // 즉, 켜짐:꺼짐이 "2300;1100" 으로 설정되어 있다고 가정했을 때
        // 현재 시간이 꺼짐시간(1100)보다 작은 경우 - OFF 모드 동작
        // 현재 시간이 켜짐시간(2300)보다 큰 경우 - 다음날 OFF 모드 동작
        // 현재 시간이 켜짐과 꺼짐 시간 사이에 있는 경우 - ON 모드 동작
        // 
        // 만약 켜짐:꺼짐이 "0700;2200" 으로 설정되어 있다면
        // 현재 시간이 켜짐시간(0700)보다 작은 경우 - ON 모드 동작
        // 현재 시간이 켜짐시간(2200)보다 큰 경우 - 다음날 ON 모드 동작
        // 현재 시간이 켜짐과 꺼짐 시간 사이에 있는 경우 - OFF 모드 동작
        var check_time = 0;
        if (next === true) {
        	// 매일 설정에서 skip 한 경우 다음 시간 동작이 되도록 함
        	// 이 경우는 시작 시간을 지난 경우와
        	// 종료 시간을 지난 경우 두 가지만 존재함
        	if (cur_time < first_time) {
        		// 종료시간 지난 후 다음 시작시간 도래하는 경우로
        		// 무조건 오늘 중으로 도래하는 시간임
        		check_time = second_time - cur_time;
            }
            else {
            	// 시작시간 지난 후 종료 시간이 도래하는 경우로
            	// 하루를 걸쳐서 시간이 설정될 수 있기 때문에
            	// 24시간 범위에 대한 계산을 처리한다.
                check_time = (24 * 3600 * 1000) - cur_time + first_time;
            }
        }
        else {
        	if (cur_time < first_time) {
        		// 자동 전원 설정 시간 범위 전
                check_time = first_time - cur_time;
            }
            else if (cur_time >= second_time) {
                // 자동 전원 설정 시간 범위를 벗어난 경우
                // 다음날 시간이 도래하기 때문에
                // 오늘 기준으로 남은 시간 + 첫번째 도래하는 시간으로 재계산
                check_time = (24 * 3600 * 1000) - cur_time + first_time;
            }
            else {
            	// 자동 전원 시간 범위 내에 있는 경우
                check_time = second_time - cur_time;
                // 켜짐/꺼짐 모두 설정되어 있는 경우
                // 현재 시간 기준으로 다음 시간에 대한 동작 mode를 설정한다.
                auto_power_mode = reverse === false ?
                        AUTO_POWER.MODE.OFF : AUTO_POWER.MODE.ON;
            }
        }
        
        log.printDbg("checkAutoPowerTime() auto_power_mode = " + getAutoPowerModeString(auto_power_mode));
        log.printDbg("checkAutoPowerTime() check_time = " + 
        		(check_time / (60 * 1000)).toFixed(1) + " minute(s)");
        
        return check_time;
    }


    /**
     * 자동전원 타이머 설정 후 실행되는 callback 함수
     * 2018.01.24 sw,nam
     * 자동전원에 요일 선택 기능이 추가 됨, 어떻게 하지 ?
     * 매일 반복하도록 설정 한 상태에서 해당 요일이 아닌경우는 skip 하도록 구현하는게 제일 간단할 듯
     */
    function onAutoPowerTimeWentOff() {
        log.printDbg("onAutoPowerTimeWentOff() auto_power_mode = " + getAutoPowerModeString(auto_power_mode));

        // 2018.01.24 sw.nam 요일 체크 로직 추가
        // 이 타임아웃 callback 함수가 실행될 떄, 요일선택 모드라면 자동켜짐/꺼짐 뭐든 상관없이 요일을 체크해서
        // 설정된 요일이 아니면 그냥 튕겨내면 된다.
        // 그리고 "한번만" 으로 설정된 경우에는 이 로직을 탈 일이 없다.
        // 요일 체크
        if(isSelectedDay) {
            log.printDbg("isSelected day is true");
            // 요일이 선택되어 있는 경우
            var d  = new Date();
            var today = d.getDay(); // 일 : 0 , 월: 1 .... 토 : 6 의 순서로 저장

            log.printDbg("today is "+today);
            if(today == 0) {
                today = 6;
            }else {
                today = today -1;
            }
            log.printDbg("repeat_dayArray[today] == "+ repeat_dayArray[today]);
            if(repeat_dayArray[today] !== "true") {
                log.printDbg("today is not the day. so return.");
                // 실행되는 날이 아니면 수행 안하고 리턴. 다시 타이머 세팅하는 로직 타러 간다.
                clearAutoPowerUserWaitingTimer();
                auto_power_user_waiting_timer = setTimeout(function() {
                    updateAutoPowerMode();
                }, AUTO_POWER.USER_WATING_TIME);
                return ;
            }
        }


        if (auto_power_mode === AUTO_POWER.MODE.ON) {
            // 자동 켜짐 모드가 설정되어있는 경우
            if (power_state !== CONFIG_DEF.POWER_MODE.ON) {
                // 꺼져있다면 바로 키도록 한다.
                // STB on ACTION 의 경우 즉시 ON 상태로 set 처리한다.

                hwAdapter.setPowerState(CONFIG_DEF.POWER_MODE.ON);
            }
            // 실제 ON 동작 수행여부와 관계없이
            // 저장 값 기준으로 ON 동작에 대한 update 처리함
            updateAutoPowerMode();
        }
        else if (auto_power_mode === AUTO_POWER.MODE.OFF) {
            if (power_state === CONFIG_DEF.POWER_MODE.ON) {
                // 대기모드 전환 ACTION 의 경우
                // popup을 노출하고 30초 후에 대기모드로 이동한다.
                clearAutoPowerUserWaitingTimer();
                auto_power_user_waiting_timer = setTimeout(function() {
                    changePowerStateToStandby(ACTION.AUTO_POWER);
                }, AUTO_POWER.USER_WATING_TIME);
                showStandbyPopup(ACTION.AUTO_POWER);
            }
            else {
                // 대기모드 전환 ACTION인데 이미 대기모드 상태라면
                // smart OTV STB일 경우 10초 후 reboot 처리한다.
                //TODO  이 케이스는 뭐지
                var is_reboot = reboot(AUTO_POWER.REBOOT_TIME.OFF_STATE);
                if (is_reboot === false) {
                	auto_power_user_waiting_timer = setTimeout(function() {
                		// 실제 OFF 동작 수행여부와 관계없이
           				// 저장 값 기준으로 ON 동작에 대한 update 처리함
                		updateAutoPowerMode();
                    }, AUTO_POWER.USER_WATING_TIME);
                }
            }
        }
    }
    
    function changePowerStateToStandby(action) {
        log.printDbg("changePowerStateToStandby(" + action + ")");
        
        if (power_state === CONFIG_DEF.POWER_MODE.ON) {
            // 저전력 기능은 설정화면의 저전력 메뉴를 통해서 MW에 전달된다.
            // 그래서 app에서는 별도로 PASSIVE_STANDBY 요청을 처리할 필요없다.
            setTimeout(function() {
            	hwAdapter.setPowerState(CONFIG_DEF.POWER_MODE.ACTIVE_STANDBY);
            }, 100);
        }
        
        if (action === ACTION.AUTO_POWER) {
        	reboot(AUTO_POWER.REBOOT_TIME.ON_STATE);
        	updateAutoPowerMode();
        }
    }

    // TODO 2016.08.05 dhlee
    // UHD STB이 아닌 경우만 대상이므로 아래 코드는 불필요해함
    // 2016.11.07 dhlee
    // 관련 코드 모두 삭제 필요함
    function reboot(timeout) {
        // OTV STB 중 UHD가 아닌 경우에는 대기모드로 간 이후
        // 또는 대기모드 상태인 경우 리부팅 하는데
        // 기본적으로 부팅 이후 LCW로 tune 처리해야 한다.
        // 단, data service channel 상태일 경우 promo channel 로 tune 처리해야한다.
        // 그래서 channel sid 값을 저장하고 rebooting 한다.
        // 2016.11.07 dhlee 일단 아래 코드만 주석 처리하고 항상 false를 return 하도록 한다.
        // 자동 재부팅 기능 외에 자동 대기 모드 등의 동작과 관련이 되어 있어 함수 자체를 주석 처리하는 경우 문제가 발생될 수 있다.
        // 추후 관련 로직 및 시나리오 완전 분석 후 코드 정리하도록 한다.
        /*
        if (KTW.CONSTANT.IS_UHD === false && KTW.CONSTANT.IS_OTS === false) {
            log.printDbg("reboot() timeout = " + timeout);
            
            var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
            var extensionAdapter = KTW.oipf.AdapterHandler.extensionAdapter;
            var current_channel = navAdapter.getCurrentChannel();
            
            var sid = "";
            if (navAdapter.isDataServiceChannel(current_channel) === true) {
                sid = navAdapter.getPromoChannel().sid;
            }
            else {
                sid = current_channel.sid;
            }
            storageManager.ps.save(storageManager.KEY.BOOT_CH, sid);
            
            clearRebootTimer();
            reboot_timer = setTimeout(function() {
                extensionAdapter.reboot();
            }, timeout);
            
            return true;
        }
        */
        return false;
    }

    function showStandbyPopup (action) {
    	log.printDbg("showStandbyPopup(" + action + ")");

        var layer_id;

        /*
        if (action === ACTION.AUTO_STANDBY) {
        	
        	if (is_showing_auto_power_popup === true) {
        		log.printDbg("showStandbyPopup(" + action + ") already showing auto power off popup... so return");
        		return;
        	}

            // 2017.01.04 dhlee util에 있는 함수를 이용하도록 변경
            util.changeMouseControlStatus(false);
            // 2016.09.05 dhlee R6 형상 적용
            //enableMouseControl(false);
        	
            layer_id = AUTO_STANDBY.POPUP_ID;
            
            popup_data.position = { top : 172, left : 385, width : 511, height : 328 };
            popup_data.title = KTW.ERROR.EX_CODE.POWER.EX001.title;
            popup_data.type = KTW.ui.popup.BaseDialog.TYPE.DIALOG;
            popup_data.desc = [
                {"size":"22", "color":"rgb(0,0,0)", "text":KTW.ERROR.EX_CODE.POWER.EX001.message[0]},
                {"size":"22", "color":"rgb(0,0,0)", "text":KTW.ERROR.EX_CODE.POWER.EX001.message[1]},
                {"size":"5", "color":"rgb(0,0,0)", "text":" "},
                {"size":"20", "color":"rgba(0,0,0,0.7)", "text":KTW.ERROR.EX_CODE.POWER.EX001.message[2]},
                {"size":"20", "color":"rgba(0,0,0,0.7)", "text":KTW.ERROR.EX_CODE.POWER.EX001.message[3]},
            ];
            popup_data.buttons = [ "확인", "설정 변경" ];
           
            is_showing_auto_standby_popup = true;
        }
        */
        if (action === ACTION.AUTO_POWER) {
            // TODO 2017.04.10 dhlee
            // 자동 대기 모드 전환 기능을 네비게이터에서 제공함에 따라 아래 케이스는 발생하지 않을텐데
            // 실제로는 네비게이터에서 띄운 UI가 떠 있을 수 있다. 어떻게 하지? 일단 아래 부분은 주석 처리 한다.
            /*
            if (is_showing_auto_standby_popup === true) {
                log.printDbg("showStandbyPopup(" + action + ") already showing auto standby popup... so return");
                return;
            }
            */

            // 2017.01.04 dhlee util에 있는 함수를 이용하도록 변경
            util.changeMouseControlStatus(false);
            // 2016.09.05 dhlee R6 형상 적용
            //enableMouseControl(false);

            layer_id = AUTO_POWER.POPUP_ID;

            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                    message: [KTW.ERROR.EX_CODE.POWER.EX002.title],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                    message: [KTW.ERROR.EX_CODE.POWER.EX002.message[0], KTW.ERROR.EX_CODE.POWER.EX002.message[1] + " " + KTW.ERROR.EX_CODE.POWER.EX002.message[2]],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_30,
                    message: [KTW.ERROR.EX_CODE.POWER.EX002.message[3]],
                    cssObj: {
                        color: "rgba(255, 255, 255, 0.5)"
                    }
                }],

                arrButton: [{id: "ok", name: "확인"}],

                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;
                        if (buttonId && buttonId === "ok") {
                            hideStandbyPopup({action: ACTION.AUTO_POWER, buttonId: buttonId});
                        }
                        else {
                            hideStandbyPopup({action: ACTION.AUTO_POWER, autoExit: true});
                        }
                    }
                }
            };

            layerManager.activateLayer({
                obj: {
                    id: layer_id,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP,
                    view: KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData,
                        hideAnyKey : "true"
                    }
                },
                visible: true
            });
        }
    }

    function hideStandbyPopup (options) {
        log.printDbg("hideStandbyPopup(" + log.stringify(options) + ")");

        if (!options) {
            return;
        }

        /*
        if (action === ACTION.AUTO_STANDBY) {
            // 2017.01.04 dhlee util에 있는 함수를 이용하도록 변경
            util.changeMouseControlStatus(true);
            // 2016.09.05 dhlee R6 형상 적용
            //enableMouseControl(false);
            is_showing_auto_standby_popup = false;
            
            clearAutoStandbyUserWaitingTimer();
            setAutoStandbyMode(false);
            layerManager.deactivateLayer(AUTO_STANDBY.POPUP_ID, true);
            
            if (index === 1) {
                var obj = {
                    id : KTW.ui.Layer.ID.SETTING,
                    params : { menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_STANDBY_MODE }
                };
                layerManager.activateLayer(obj);
            }
            
            if (index !== -1) {
            	// 자동대기모드 popup을 취소했을 때
            	// 자동대기모드 대기 중이면 자동대기모드 안내 popup을 띄운다. 
            	if (auto_power_user_waiting_timer !== null) {
            		showStandbyPopup(ACTION.AUTO_POWER);
            	}
            }
        }
        else if (action === ACTION.AUTO_POWER) {
            // 2017.01.04 dhlee util에 있는 함수를 이용하도록 변경
            util.changeMouseControlStatus(true);
            // 2016.09.05 dhlee R6 형상 적용
            //enableMouseControl(false);
        	is_showing_auto_power_popup = false;
        	
            clearAutoPowerUserWaitingTimer();
            layerManager.deactivateLayer(AUTO_POWER.POPUP_ID, true);
            
            // 사용자가 자동전원 on/off 동작을 취소하는 경우
            // 저장 값 기준으로 ON/OFF 동작에 대한 update 처리함
            if (index !== -1) {
            	// 자동전원 popup을 취소했을 때
            	// 자동대기 모드 대기 중이면 자동대기모드 안내 popup을 띄운다. 
            	if (auto_standby_user_waiting_timer !== null) {
            		showStandbyPopup(ACTION.AUTO_STANDBY);
            	}
            	
            	// 사용자가 취소했을 경우 cancel 값을 true 로 전달
            	updateAutoPowerMode(true);
            }
        }
        */

        if (options.action === ACTION.AUTO_POWER) {
            // 2017.01.04 dhlee util에 있는 함수를 이용하도록 변경
            util.changeMouseControlStatus(true);
            // 2016.09.05 dhlee R6 형상 적용
            //enableMouseControl(false);
            //is_showing_auto_power_popup = false;

            if (options.buttonId && options.buttonId === "ok") {
                layerManager.deactivateLayer({id: AUTO_POWER.POPUP_ID});
            }

            // 사용자가 자동전원 on/off 동작을 취소하는 경우
            // 저장 값 기준으로 ON/OFF 동작에 대한 update 처리함
            if (!options.autoExit) {
                // 자동전원 popup을 취소했을 때
                // 자동대기 모드 대기 중이면 자동대기모드 안내 popup을 띄운다.
                // TODO 2017.04.10 dhlee
                // 자동대기모드 기능을 네비게이터에서 처리함에 따라 일단 주석 처리하는데....
                // 네비게이터가 UI를 띄운 상황을 어떻게 알아야 할까?
                /*
                if (auto_standby_user_waiting_timer !== null) {
                    showStandbyPopup(ACTION.AUTO_STANDBY);
                }
                */
                // 2018.01.29 sw.nam
                // 자동 전원 대기모드 전환 시 띄우는 팝업에서
                // 확인 버튼 외에 다른 키를 눌러도 팝업이 닫히면서 자동전원이 실행되지 않는 이슈가 있었음 ( 요일 테스트 하면서 기존 이슈 발견)
                // OK 버튼을 눌렀을 경우에만 팝업 닫히고 자동 전원 타이머 클리어 하도록 수정
                clearAutoPowerUserWaitingTimer();

                // 사용자가 취소했을 경우 cancel 값을 true 로 전달
                // 2018.01.29 sw.nam
                // 팝업이 뜨자마자 바로 취소하게 되면 다시 자동 전원 설정 로직을 타게 되면서 시간이 지날때까지 무한으로 팝업이 띄워지는 현상 발생 (요일 테스트하면서 기존 이슈 발견)
                // 확인 키 눌러 대기모드 진입 timer 해제 후 다시 전원 설정 시 현재 설정하는 시간을 지나서 다음 시간에 설정 가능하도록 setTimeout 으로 감싸서 실행시킨다.
                auto_power_user_waiting_timer = setTimeout(function() {
                    updateAutoPowerMode(true);
                }, AUTO_POWER.USER_WATING_TIME);
            }
        }
    }

    /**
     * 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리한다.
     */
    /*
    function clearAutoStandbyTimer() {
        if (auto_standby_timer !== null) {
            clearTimeout(auto_standby_timer);
            auto_standby_timer = null;
        }
    }
    */
    
    function clearAutoPowerTimer() {
        if (auto_power_timer !== null) {
            clearTimeout(auto_power_timer);
            auto_power_timer = null;
        }
    }

    /**
     * 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리한다.
     */
    /*
    function clearAutoStandbyUserWaitingTimer() {
        if (auto_standby_user_waiting_timer !== null) {
            clearTimeout(auto_standby_user_waiting_timer);
            auto_standby_user_waiting_timer = null;
        }
    }
    */
    
    function clearAutoPowerUserWaitingTimer() {
        if (auto_power_user_waiting_timer !== null) {
            clearTimeout(auto_power_user_waiting_timer);
            auto_power_user_waiting_timer = null;
        }
    }

    /**
     * 2016.11.07 dhlee
     *
     * UHD STB이 아닌 경우에만 사용하므로 3.0 에서는 사용하지 않는다.
    function clearRebootTimer() {
        if (reboot_timer !== null) {
            clearTimeout(reboot_timer);
            reboot_timer = null;
        }
    }
     */

    /**
     * 2017.06.09 dhlee
     * 일반 모드 <-> 키즈 모드 전환 시에 자동 전원 기능을 On/Off 한다.
     *
     * @param flag true: 자동 전원 기능 동작(키즈 모드 -> 일반 모드), false: 자동 전원 기능 해제(일반 모드 -> 키즈 모드)
     */
    function _resetAutoPowerMode(flag) {
        log.printDbg("_resetAutoPowerMode() flag = " + flag);

        if (flag === true) {
            setAutoPowerMode(true);
        } else {
            clearAutoPowerTimer();
            clearAutoPowerUserWaitingTimer();
        }
    }

    /**
     * 2017.04.06 dhlee
     * 키즈 모드 일때 * 키를 누르거나 시청 편수, 시청 시간이 도달할 때 대기모드로 진입해야 한다.
     * 이를 위해 별도 API를 정의하여 제공한다.
     * TODO 일단 ACTIVE_STANDBY_MODE 로 전환시키되 뭔가 초기화 등의 작업이 부가적으로 필요한지는 추후 확인하자.
     */
    function _changeStandbyMode() {
        log.printDbg("_changeStandbyMode()");

        if (power_state === CONFIG_DEF.POWER_MODE.ON) {
            // 저전력 기능은 설정화면의 저전력 메뉴를 통해서 MW에 전달된다.
            // 그래서 app에서는 별도로 PASSIVE_STANDBY 요청을 처리할 필요없다.
            setTimeout(function() {
                hwAdapter.setPowerState(CONFIG_DEF.POWER_MODE.ACTIVE_STANDBY);
            }, 100);
        }
    }
    
    function onPowerStateChange(event) {
        
        var state = event.powerState;

        log.printDbg("onPowerStateChange() state = " + state + " stateManager.appState:" + stateManager.appState);
        
        if (state === CONFIG_DEF.POWER_MODE.ON) {
            // running 상태
            if (power_state === CONFIG_DEF.POWER_MODE.ON) {
                log.printDbg("onPowerStateChange() already running mode... so return");
                return;
            }
            power_state = state;

            /* 2017.04.10 dhlee 자동대기모드 전환 기능은 네비게이터에서 처리
            // 자동대기 모드 초기화
            if (KTW.CONSTANT.STB_TYPE.ANDROID === false) {
                setAutoStandbyMode(false);
            } else {
                // 이미 대기모드 상태에서 clear되었기 때문에 굳이 할 필요는 없지만
                // 보완코드의 의미로 추가함.
                clearAutoStandbyTimer();
            }
            */
            
            // 자동 전원 모드 timer clear
            clearAutoPowerUserWaitingTimer();
            //clearRebootTimer();
            
            notifyPowerModeChange(power_state);

            // change state to running mode
            // [dj.son] AppServiceManager 에서 listener 등록해서 처리
            //stateManager.exitToService();
            
            // 자동 전원 모드 동작 recheck
            setAutoPowerMode(false);
            
            //showwindow update 취소.
            //KTW.ui.layer.showwindow.showwindowUpdateManager.stopUpdateTimer();
        }
        else if (state === CONFIG_DEF.POWER_MODE.PASSIVE_STANDBY || 
                state === CONFIG_DEF.POWER_MODE.ACTIVE_STANDBY) {
            // standby 상태
            if (power_state === CONFIG_DEF.POWER_MODE.PASSIVE_STANDBY || 
                    power_state === CONFIG_DEF.POWER_MODE.ACTIVE_STANDBY) {
                log.printDbg("onPowerStateChange() already standby mode... so return");
                return;
            }
            power_state = state;

            layerManager.deactivateLayer({id: AUTO_POWER.POPUP_ID});

            //hideStandbyPopup(-1, { ext : ACTION.AUTO_STANDBY });
            //hideStandbyPopup(-1, { ext : ACTION.AUTO_POWER });
            //
            //clearAutoStandbyTimer();
            //clearAutoStandbyUserWaitingTimer();
            
            // 자동 전원 모드 timer clear
            clearAutoPowerUserWaitingTimer();
            //clearRebootTimer();
            
            //KTW.ca.CaHandler.checkParentalRatingLock(true);
            
            notifyPowerModeChange(power_state);
            
            // change state to standby mode
            // [dj.son] AppServiceManager 에서 listener 등록해서 처리
            //stateManager.changeState(KTW.CONSTANT.SERVICE_STATE.STANDBY);
            
            // 자동 전원 모드 동작 recheck
            setAutoPowerMode(false);
            
            log.printDbg("onPowerStateChange() received_first_key_event : " + received_first_key_event);
            // standby mode에서 cust env 갱신
            // [jh.lee] 첫키가 입력된 상태에서만 대기모드 진입 시 getCustEnv, getCustCugInfo API를 호출할 수 있다.
            // [jh.lee] 첫키 조건에 해당 하면 10분 룰을 체크한다. "대기모드 진입하여 API를 호출할 때 10분 룰적용하고, 이 안에서 다시 대기모드 진입시에는 호출하지 않음"
            if (networkManager.isNetworkConnected() === true && received_first_key_event === true) {
                if (!api_call_timer) {
                    // 2017.04.04 dhlee
                    // STB_POINT_CACHE 영역의 ReqFalg 를 true 로 set
                    // 2017.06.02 dhlee
                    // 데이터 존재 여부와 상관 없이 ReqFlag를 true로 set 하도록 한다.
                    var cacheObj = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.STB_POINT_CACHE);
                    if (cacheObj && cacheObj !== "undefined") {
                        var data = JSON.parse(cacheObj);
                        if (!data.ReqFlag) {
                            data.ReqFlag = true;
                            KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
                        }
                    }
                    else {
                        var data = {};
                        data.ReqFlag = true;
                        KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
                    }
                    // [jh.lee] 10분 타이머가 존재하지 않은 경우에만 cust 관련 API 호출
                    callCustApi();
                    api_call_timer = setTimeout(function() {
                        log.printDbg("clear api_call_timer");
                        clearTimeout(api_call_timer);
                        api_call_timer = null;
                    }, 10*60*1000);
                }
            }
            
            // showwindow update timer.
            //KTW.ui.layer.showwindow.showwindowUpdateManager.startUpdateTimer();
        }
    }
    
    /**
     * [jh.lee] getCustEnv, getCustCugInfo API 호출
     */
    function callCustApi() {
        log.printDbg("callCustApi");

        // [dj.son] 2.0 최신 소스와 동일하게 대기모드 진입시 getCustEnv 호출 안하도록 수정
        // [dj.son] 추가로 사용하지 않는 onCustEnv 함수 주석처리

        //amocManager.getCustEnv(onCustEnv, KTW.SAID);
        //amocManager.getMyPkgList(onMyPkgList);
        caHandler.requestCaMessage(caHandler.TAG.REQ_PRODUCT_INFO, undefined, onRequestCaMessage);   
    }


    //function onCustEnv(success, result) {
    //    if (success === true) {
    //        log.printDbg("onCustEnv() result = " + result);
    //        try {
    //            //var SWDef = KTW.ui.layer.showwindow.Def;
    //            //var old_cust_env = storageManager.ps.load(storageManager.KEY.CUST_ENV);
    //
    //            //if(SWDef.TEST.SW_JOINPACK_ALWAYS_CHANGED === true) {
    //            //    result.pkgCode = "2P02";
    //            //}
    //
    //            KTW.oipf.adapters.vodAdapter.updateOTMParing(result);
    //
    //            storageManager.ps.save(storageManager.KEY.CUST_ENV, JSON.stringify(result));
    //
    //            //if(old_cust_env && result && result.pkgCode) {
    //            //    old_cust_env = JSON.parse(old_cust_env);
    //            //
    //            //    if(result.pkgCode != old_cust_env.pkgCode) {
    //            //        //var UPDATE_MODE = KTW.ui.layer.showwindow.showwindowUpdateManager.UPDATE_MODE;
    //            //        //KTW.ui.layer.showwindow.showwindowUpdateManager.updateShowwindow(UPDATE_MODE.JOINPACK);
    //            //    }
    //            //}
    //        }
    //        catch(e) {
    //            log.printErr(e);
    //        }
    //    }
    //    else {
    //        log.printWarn("onCustEnv() fail");
    //    }
    //}
    /*
    function onMyPkgList(success, result) {
        var extensionAdapter = KTW.oipf.AdapterHandler.extensionAdapter;

        if (success === true) {
            if (!result) {
                // R6 CEMS
                // success 값이 true 이지만 result 값 자체가 null 인 경우 CEMS 리포트
                extensionAdapter.notifySNMPError("VODE-00013");
            }
            if (result.pkgList) {
                if (result.pkgList.length){ //배열로 1개이상넘어온 경우
                    var pkg_list = result.pkgList;
                    var pkg_code = "";
                    var base_pkg_code = "";
                    
                    for (var i = 0; i < pkg_list.length; i++) {
                        if (pkg_list[i].pkgClass == "1") {
                            //기본팩
                            base_pkg_code += pkg_list[i].pkgCode + ",";
                        } else if (pkg_list[i].pkgClass != "1") {
                            //부가팩
                            pkg_code += pkg_list[i].pkgCode + ",";
                        }
                    }
                    
                    if (pkg_code) {
                        pkg_code = pkg_code.substring(0, pkg_code.length -1);
                    }
                    if (base_pkg_code) {
                        base_pkg_code = base_pkg_code.substring(0, base_pkg_code.length -1);
                    }
                    storageManager.ps.save(storageManager.KEY.PACKAGE_LIST, pkg_code);
                    storageManager.ps.save(storageManager.KEY.PACKAGE_LIST_BASE, base_pkg_code);
                }
            } else {
                log.printWarn("onMyPkgList() package list is null");
            }
        }
        else {
            // R6 CEMS.
            // success 값이 false 전달되어 네트워크 관련 오류인 경우 CEMS 리포트(timeout 등)
            if (result !== "abort") {
                extensionAdapter.notifySNMPError("VODE-00013");
            }
            log.printWarn("onMyPkgList() fail");
        }
    }
    */
    
    function onRequestCaMessage(result, data) {
        log.printDbg("onRequestCaMessage");
        if (result === true) {
            // [jh.lee] 성공
            log.printDbg("onRequestCaMessage result : " + result);
            log.printDbg("onRequestCaMessage data : " + JSON.stringify(data));
            
            if ((data.product_info_data & caHandler.PRODUCT_TYPE.B2B_SUBSCRIBER) === caHandler.PRODUCT_TYPE.B2B_SUBSCRIBER) {
                // [jh.lee] product_info_data 의 경우 여러개의 product 를 가질 수 있기 때문에 여러개의 product를 and bit 연산한 값이다
                // [jh.lee] 여기서 and 연산을 하여 0 이아닌 caHandler.PRODUCT_TYPE.B2B_SUBSCRIBER(0x2000) 값을 리턴하면
                // [jh.lee] 해당 상품에 가입되어있다는 결과이다.
                // [jh.lee] 가입되어있는 사용자이기 때문에 가입되어있는 CUG 리스트를 확보하기 위해 amoc 통신을 수행한다~
                amocManager.getCustCugInfo(onGetCustCugInfo, KTW.SAID);
            } else {
                // [jh.lee] 해당 사용자가 CUG 가입되어있지 않은 경우
                // [jh.lee] 또는 해지를 하는 경우인데 이 때는 이전 저장되있던 CUG 정보를 날려줘야 다음 부팅때 CUG채널로 부팅하지 않는다.
                // R6 단! 요구사항으로 LCW 가 생겼기 때문에 여기서 처리하지않고 스토리지 remove 함으로써 발생하는 콜백에서 처리하도록 한다.
                //       즉, menuDataManager 에서 처리한다.
                storageManager.ps.remove(storageManager.KEY.CUST_CUG_INFO);
            }
        }
    }
    
    function onGetCustCugInfo(result, data) {
        log.printDbg("onGetCustCugInfo()");
        
        if (result === true) {
            // [jh.lee] 성공
            /*
            if (data && data.cugList && data.cugList.length) {
                // 만약 data.cugList 값이 null 이나 undefined 이나 공백이 아닐 경우!
                // 여러개라서 배열로왔는지 아니면 한개라서 객체로 왔는지 확인이 필요!
                if (data.cugList.constructor !== Array) {
                    log.printDbg("onGetCustCugInfo get Object");
                    // Array 가아니면 한개의 객체 형태로 왔다는 것이기 때문에 배열화 해서 전달한다.
                    data.cugList = [data.cugList];
                }
                storageManager.ps.save(storageManager.KEY.CUST_CUG_INFO, JSON.stringify(data));
            }
            */
            if (data && data.cugList) {
                // [jh.lee] 데이터가 존재하는 경우

                // 만약 data.cugList 값이 null 이나 undefined 이나 공백이 아닐 경우!
                // 여러개라서 배열로왔는지 아니면 한개라서 객체로 왔는지 확인이 필요!
                if (data.cugList.constructor !== Array) {
                    log.printDbg("onGetCustCugInfo get Object");
                    // Array 가아니면 한개의 객체 형태로 왔다는 것이기 때문에 배열화 해서 전달한다.
                    data.cugList = [data.cugList];
                }

                storageManager.ps.save(storageManager.KEY.CUST_CUG_INFO, JSON.stringify(data));
            }
        }
    }

    /*
    function onStorageDataChanged(key) {
        log.printDbg("onStorageDataChanged() key = " + key);
        
        if (key === storageManager.KEY.AUTO_STANDBY) {
            setAutoStandbyMode(true);
        }
        else if (key === storageManager.KEY.AUTO_POWER) {
            setAutoPowerMode(true);
        }
    }
    */

    /**
     * 2017.04.10 h
     * Local Storage Data 변경 시 수행되는 callback
     * 자동 전원 값이 변경된 경우 처리한다.
     *
     * @param key
     */
    function onStorageDataChanged(key) {
        log.printDbg("onStorageDataChanged() key = " + key);

        if (key === storageManager.KEY.AUTO_POWER) {
            setAutoPowerMode(true);
        }
    }

    /**
     * 2017.01.04 dhlee
     * 기가지니를 위한 함수
     * TODO 내용 파악해서 추가 변경 필요함
     *
     * [dj.son] [2017-07-13]
     * - "G 메뉴" 키로 안드로이드 앱을 실행할 경우, "hp_stopVODPlaying" -> INACTIVE state -> "obs_hide" 순으로 이벤트 발생
     * - 안드로이드 앱 종료시, ACTIVE state -> channel change event (ChannelControl 에서는 같은 채널이기 때문에 아무것도 안함)
     *
     * - 전원버튼으로 대기모드로 전환시, power state change -> "hp_stopVODPlaying" -> INACTIVE state 순으로 이벤트 발생
     * - 전원버튼으로 동작모드로 전환시, power state change -> ACTIVE state -> channel change event
     *
     * - 어차피 안드로이드 앱 실행 / 종료시, obs_hide / channel change 가 일어나기 때문에, 홈포탈의 기본적인 동작은 "아무것도 안한다" 이다.
     * - 여기서는 특정 이슈에 대해 예외처리만 추가한다.
     *
     * @param res
     */
    function onAndroidStateChanged (res) {
        // 런처 진입 시 카운트 중단 -> 홈포탈로 다시 진입시 시 카운트 리셋 후 재 카운트 시작
        // by kingsae1
        log.printDbg("onAndroidStateChanged() state = " + res.state);

        if (res && res.state === KTW.CONSTANT.ANDROID_STATE.ACTIVE) {
            // android 상태에서 홈포탈로 진입하는 경우
            // 기존 channel permission을 clear 하도록 한다
            // 이렇게 하지 않으면 상태 전환 후 obs_hide 메세지에 의해 keyPriority가 "-2"로 변경되는데
            // promo channel이 제한채널로 설정된 경우 숫자키 입력 시
            // 비밀번호 입력창에서 동작하는 것이 아니라 DCA 처리가 되는 문제가 있음
            // 그리고 안드로이드 상태로 전환할 경우 miniEPG를 hide 하는데
            // 이후에 다시 miniEPG를 그리도록 하기 위해서 다시 홈포탈 상태로 전환되는 시점에
            // 해당 처리를 하도록 함 (참고로 ChannelControl에서 miniEPG 활성화 flag값도 reset 처리되도록 함)

            // 대기-동작에 의해 ACTIVE 되는 경우에만 permission reset 처리하도록 함
            // 왜냐하면 양방향 채널 진입-탈출 시에도 INACTIVE-ACTIVE 상태로 전환되기 때문에
            // 대기-동작인 경우에 한해서만 처리될 수 있도록 함
            // 추가로 2가지 경우 모두 쇼윈도가 노출되지만
            // android STB에서 대기-동작 후 홈포탈이 아닌 런처가 먼저 노출되고
            // background에서 쇼윈도가 노출되었다가 사라진 다음에 홈포탈로 진입하면
            // miniEPG가 활성화 되어야 하는데 현재 채널의 permission이 동일한 문제로
            // miniEPG가 비활성화 되거나 숫자키가 DCA로 동작하기 때문에 이렇게 처리해야 함
            // 단, 양방향 채널 진입-탈출 시에는 무조건 쇼윈도가 노출되기 때문에
            // 쇼윈도가 종료되는 시점에 miniEPG 및 keyPriority가 시나리오에 맞게 처리되므로 문제 없음

            var last_stack_layer = layerManager.getLastLayerInStack();
            log.printDbg("onAndroidStateChanged() last_stack_layer = " + last_stack_layer);

            if (android_inactive_state_on_standby === true) {
                android_inactive_state_on_standby = false;

                // TODO 2017.02.15 dhlee 2.0 R7 로직 추가
                // 현재 기가지니 형상 기준으로 대기-동작 이후 바로 홈포탈 전환되면서
                // android state ACTIVE 상태가 되는데 해당 상황이면 항상 쇼윈도가 노출되므로
                // permission reset 하지 않도록 해야 한다.
                // 그런데 기가지니 형상에서 대기-동작 시나리오 변경될 수 있는 상황을 고려하여
                // (대기-동작 후 기가지니가 먼저 노출되었다가 홈포탈로 전환하는 시나리오 등)
                // 대기-동작 이후 stack layer가 노출되어 있는 상황이라면
                // permission reset 처리하지 않도록 한다.
                if (last_stack_layer === null) {
                    KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).resetPerm();
                }
            }

            // 타앱-> ollehTv App 실행
            // TODO 2017.02.15 dhlee 2.0 R7 로직 추가 (확인 필요함)
            // 양방향 상태인 상태에서 ACTIVE 전환 시 자동대기모드 동작 하지 않도록 함
            // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리한다.
            //if (last_state !== KTW.CONSTANT.SERVICE_STATE.DATA) {
            //    setAutoStandbyMode(false);   // 자동 대기모드 재 실행
            //}
            setAutoPowerMode(false);     // 자동 전원모드 재 실행
        }
        else if (res && res.state === KTW.CONSTANT.ANDROID_STATE.INACTIVE) {
            if (power_state === CONFIG_DEF.POWER_MODE.PASSIVE_STANDBY ||
                power_state === CONFIG_DEF.POWER_MODE.ACTIVE_STANDBY) {
                log.printDbg("onAndroidStateChanged() already standby mode... so return");

                // 대기동작에 의한 INACTIVE 상태여부 flag 설정
                android_inactive_state_on_standby = true;
                return;
            }

            // 2017.08.10 sw.nam
            // layer 가 stack 에 남아있는 상태에서 miniEPG 를 통해 REQUEST_SHOW 가 실행 되는 경우
            // 기존 stack 에 남아있는 layer 가 불리는 경우가 발생(  ex) 블루투스 설정 런처 진입 case )
            // solved : 런처에서 홈포탈로 다시 진입 시 기존 stack 에 남아있는 normal layer 를 전부 clear 한다.
            /**
             * 2017.08.10 sw.nam
             * - 설정 -> 블루투스 -> 블루투스 안드로이드 앱 실행 -> clean AV 복귀 후 미니 epg 좌측 채널 리스트 띄울때, 블루투스 화면이 보이는 이슈 수정
             * - layer 가 stack 에 남아있는 상태에서 miniEPG 를 통해 REQUEST_SHOW 가 실행 되는 경우, 기존 stack 에 남아있는 layer 가 불리는 경우가 발생
             * - solved : 런처에서 홈포탈로 다시 진입 시 기존 stack 에 남아있는 normal layer 를 전부 clear 한다.
             *
             * [dj.son] [WEBIIIHOME-3132]
             * - 동작 모드 진입시 홈 화면 activate -> clearNormalLayer 호출됨으로서 이슈 발생
             * - 안드로이드 런처가 실행될때, clearNormalLayer 를 호출하도록 수정
             */
            layerManager.clearNormalLayer();


            //[hw.boo] Android state INACTIVE 일 때 PIP 종료 추가
            // - Jira : KTWEB-205
            // - 이슈 시나리오 : OTV PIP 이용 중 > 타 서비스 이용 > 재진입하게되면 PIP 잔상화면 남음
            // TODO 2017.02.15 dhlee 웹 3.0 형상으로 맞춰야 함
            var pip_cc = KTW.oipf.adapters.NavAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
            log.printDbg("onAndroidStateChanged() - pip_cc : " + pip_cc);
            if (pip_cc) {
                log.printDbg("onAndroidStateChanged() - pip state : " + pip_cc.getState() + ", pip mode : " + pip_cc.getPipMode());
                if (pip_cc.getState() === KTW.nav.Def.CONTROL.STATE.STARTED) {
                    KTW.managers.service.PipManager.deactivatePipLayer();
                    KTW.managers.service.PipManager.deactivateMultiView();

                    // miniEPG 인접채널 pip 노출시에만 miniEPG hide 처리함
                    // 무조건 miniEPG를 hide 처리하면 런처-홈포탈 이동 시나리오에서
                    // miniEPG가 늦게 그려지는 문제가 있어서 PIP가 노출된 경우에만 hide 처리함
                    layerManager.deactivateLayer({ id: KTW.ui.Layer.ID.MINI_EPG });
                }
            }

            // ollehTv App -> 타앱 실행
            // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
            //clearAutoStandbyTimer();    // 자동 대기모드 정지
            //clearAutoStandbyUserWaitingTimer();

            //clearAutoPowerTimer();      // 자동 전원 정지
            //clearAutoPowerUserWaitingTimer();
            //clearRebootTimer();

            // VOD play요청 이후 즉시 android 상태로 돌아올 경우
            // 채널전환에 의해 "hp_stopVODPlaying" 메세지가 전달되지만
            // 실제 VOD 상태전환이 되지 않았거나 VOD play 상태가 아니라면
            // VOD stop을 바로 처리할 수 없기 때문에 INACTIVE 전환되는 callback을 받고서
            // vod stop 처리를 의도적으로 하도록 함
            if (stateManager.isVODPlayingState()) {
                //var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                //if (vodModule) {
                //    //vodModule.execute({
                //    //    method: "pauseVODPlaying"
                //    //});
                //}

                /**
                 * [dj.son] vod 재생중이라면, state 변경을 통해 vod 종료 처리를 한다.
                 * 단, 안드로이드 앱이 실행되는 상황이기 때문에 따로 채널 튠을 수행하지는 않는다.
                 */
                KTW.managers.service.AppServiceManager.changeService({
                    nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING,
                    obj: {
                        channel: {},
                        tune: false
                    }
                });
            }
        }

        /**
         * [dj.son] 안드로이드 앱 시작시, obs_hide 에 의해 이미 일반 popup 들은 정리가 되므로, 여기서는 자동 종료 팝업에 대해서만 처리
         */
        if (res) {
            // 이벤트 발생 시 노출 중인 팝업 Close
            // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
            //layerManager.deactivateLayer(AUTO_STANDBY.POPUP_ID, true);
            layerManager.deactivateLayer({id: AUTO_POWER.POPUP_ID});
        }
    }

    // [dj.son] listener 에 전달되는 param 형식 수정
    function onServiceStateChange(options) {
        log.printDbg("onServiceStateChange(), serviceState = " + log.stringify(options));

        if (stateManager.isOtherAppState()) {
            // TODO 2017.02.15 dhlee 2.0 형상에서는 param 으로 전달 받은 state 값을 저장하고 있음.
            // 아래 처럼 하는게 맞을까? 확인 필요함.
            last_state = options.nextServiceState;
            // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
            //clearAutoStandbyTimer();
            return;
        }

        if (options.nextServiceState !== KTW.CONSTANT.SERVICE_STATE.VOD) {
            log.printDbg("serviceState !== KTW.CONSTANT.SERVICE_STATE.VOD");
            // R6. 1차 검증 이슈. KTWHPTZOF-2023
            // 문제가 되는 시나리오는 다음과 같음.
            // state가 변경 되면 PowerModeManager 에서 등록한 stateManager 리스너가 실행되는데...
            // vod 재생하면 state가 vod로 오고 _resetAutoStandbyTime이 호출된다...
            // 평상시에는 상관없지만... 찜목록 또는 몰아보기 같은 경우...다음 컨텐츠가 자동으로 재생 될 때(구매나 인증 이런거 없어서 키입력없을때...)
            // state 가 vod로 또 오는데 이러면... 결국 자동대기모드 ON일 때 타이머 시간이 초기화된다..
            // 그러면 짧은 시간의 컨텐츠를 여러개 찜목록에 등록 후 재생하면 계속해서 타이머 시간이 초기화되면서 자동대기모드팝업이 노출되지 않는것이다...
            // 그래서 state가 VOD로 넘어오는 경우는 _reset 하지 않도록 한다... 어짜피 VOD 재생하기전에 키입력으로 reset 될것이기 때문에...

            // TODO 2017.01.04 dhlee
            // 단, 기가지니 형상에서 대기-동작으로 넘어온 뒤
            // 홈포탈 실행되지 상태(android state INACTIVE)를 유지할 경우
            // 자동대기모드가 아래 코드에 의해 동작을 할 거라서
            // 대기-동작 전환인 경우에는 reset 되지 않도록 함
            var reset = true;

            // TODO 2017.02.15 dhlee 2.0 R7 Logic 추가
            // 단, EOF로 VOD 상태가 종료되는 경우에는 key event 를 받지 않았기 때문에 종료처리하지 않는다.
            // 물론 사용자가 의도적으로 VOD를 종료하거나 할 경우 이미 LayerManger 상에서
            // KeyEvent를 받아서 reset 처리할 것이라서 여기에서는 상태여부만 확인하여 처리한다.
            if (last_state === KTW.CONSTANT.SERVICE_STATE.VOD) {
                reset = false;
            }

            if (KTW.CONSTANT.STB_TYPE.ANDROID === true) {
                if (last_state === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                    reset = false;
                }
            }

            // 2017.04.10 dhlee 자동 대기 모드 전환 기능은 네비게이터에서 처리
            /*
            if (reset) {
                _resetAutoStandbyTime();
            }
            */
        }

        // TODO 2017.02.15 dhlee 2.0 R7 내용 추가
        last_state = options.nextServiceState;
    }
    
    function notifyPowerModeChange(mode) {
        log.printErr("notifyPowerModeChange()");

        var copy = listeners.slice();
        var length = copy.length;
        for (var i = 0; i < length; i++) {
            try {
                copy[i](mode);
            }
            catch(e) {
                log.printErr("notifyPowerModeChange() occured error")
                log.printExec(e);
            }
        }
    }
    
    function getPowerStateString(state) {
        var power_state_str = "";
        
        switch(state) {
            case CONFIG_DEF.POWER_MODE.ON :
                power_state_str = "ON";
                break;
            case CONFIG_DEF.POWER_MODE.PASSIVE_STANDBY :
                power_state_str = "PASSIVE_STANDBY";
                break;
            case CONFIG_DEF.POWER_MODE.ACTIVE_STANDBY :
                power_state_str = "ACTIVE_STANDBY";
                break;
        }
        
        return power_state_str;
    }
    
    function getAutoPowerModeString(mode) {
        var auto_power_mode_str = "";
        
        switch(mode) {
            case AUTO_POWER.MODE.NONE :
                auto_power_mode_str = "NONE";
                break;
            case AUTO_POWER.MODE.ON :
                auto_power_mode_str = "ON";
                break;
            case AUTO_POWER.MODE.OFF :
                auto_power_mode_str = "OFF";
                break;
        }
        
        return auto_power_mode_str;
    }
    
    function getMilliseconds(time_str) {
        var hours = parseInt(time_str.substring(0, 2));
        var minutes = parseInt(time_str.substring(2, 4));
        var seconds = 0;
        if (time_str.length > 4) {
            seconds = parseInt(time_str.substring(4, 6));
        }
        
        var time = (hours * 60 * 60) + (minutes * 60) + seconds;
        
        return time * 1000;
    }
    
    function _setPowerState(mode) {
    	// 테스트용 코드
    	if (KTW.PLATFORM_EXE === "PC") {
    		var event = { "powerState" : mode };
        	
        	onPowerStateChange(event);
    	}
    }
    
    function _setFirstKeyEvent() {
        received_first_key_event = true;
    }
    
    return {
        init: _init,
        
        get powerState() {
            return power_state;
        },
        
        //resetAutoStandbyTime: _resetAutoStandbyTime,
        
        addPowerModeChangeListener: _addPowerModeChangeListener,
        removePowerModeChangeListener: _removePowerModeChangeListener,
        
        setPowerState: _setPowerState,
        setFirstKeyEvent: _setFirstKeyEvent,
        changeStandbyMode: _changeStandbyMode,

        resetAutoPowerMode: _resetAutoPowerMode
    };
}());