"use strict";

/**
 * monitoring network connection state
 * 
 */
KTW.managers.device.DeviceManager = (function () {
    
    var CONFIG = KTW.oipf.Def.CONFIG;
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var hwAdapter = null;
    var navAdapter = null;
    var stateManager = null;
    var layerManager = null;
    
    var storage_device = null;
    var bluetooth_device = null;
    
    var wizgame_response_timer = null;
    
    function _init() {
        hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        stateManager = KTW.managers.service.StateManager;
        layerManager = KTW.ui.LayerManager;

        if (storage_device === null) {
            storage_device = new StorageDevice();
            storage_device.init();
        }
        if (bluetooth_device === null) {
            bluetooth_device = new BTDevice();
            bluetooth_device.init();
        }
        
        hwAdapter.addEventListener(CONFIG.EVENT.DEVICE, onDeviceEvent);
    }
    
    function onDeviceEvent(device_type, event) {
        
        var type = event.type;
        log.printDbg("onDeviceEvent() type = " + type);
        log.printDbg("onDeviceEvent() device_type = " + device_type);
        
        if (device_type === CONFIG.DEVICE_TYPE.STORAGE) {
            storage_device.changeState(type);
        }
        else if (device_type === CONFIG.DEVICE_TYPE.BLUE_TOOTH) {
            bluetooth_device.changeState(type, event.device);
        }
    }
    
    /**
     * StorageDevice Object
     * 
     * control removable disk device (like USB)
     */
    var StorageDevice = function() {
        
        var USB_APP = KTW.CONSTANT.USB_APP;
        var STORAGE = {
            POPUP_ID : "storage_popup",
            APP_CCID : KTW.RELEASE_MODE === "BMT" ?
                    USB_APP.CCID.BMT : USB_APP.CCID.LIVE,
            APP_LOCATOR : KTW.RELEASE_MODE === "BMT" ?
            		USB_APP.LOCATOR.BMT : USB_APP.LOCATOR.LIVE
        };
        
        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        
        var connected = false;
        var connection_listener = null;
        var is_show_popup = false;
        
        function _init () {
            var storage_list = hwAdapter.getStorages();
            var length = storage_list.length;
            for (var i = 0; i < length; i++) {
                if (storage_list[i].type === window.Storage.STORAGE_TYPE_USB) {
                    connected = true;
                    break;
                }
            }
            
            log.printDbg("StorageDevice#init() connected = " + connected);
        }
        
        function _changeState (type) {
            
            if (type === CONFIG.EVENT.DEVICE_ERROR) {
                // error event에 대해서는 처리하지 않음
                return;
            }
            // 2017.05.23 dhlee
            // UHD3 STB에서는 USB play 기능을 제공하지 않음



            connected = type === CONFIG.EVENT.DEVICE_CONNECT;
            log.printDbg("StorageDevice#changeState() connected = " + connected);

            if (KTW.CONSTANT.STB_TYPE.UHD3 && connected !== true) {
                return;
            }

            /**
             * [dj.son] 국방 상품인 경우, USB play 기능 미제공
             */
            if (KTW.managers.service.ProductInfoManager.isMilitary()) {
                return;
            }

            // 2017.06.09 dhlee
            // 키즈모드일 때는 사용하지 못하도록 하되 안내 팝업을 보여준다.
            if (KTW.managers.service.KidsModeManager.isKidsMode() && connected) {
                KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup();
                return;
            }


            if (connection_listener !== null) {
                try {
                    connection_listener(connected);
                }
                catch(e) {
                    log.printErr("StorageDevice#changeState() notify connection state occured error");
                    log.printExec(e);
                }
            }
            
            // 시청시간제한 상태에서는 popup을 노출하지 않는다.
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            	log.printDbg("StorageDevice#changeState() TIME_RESTRICTED state... so return");
            	return;
            }
            
            // 이미 USB app 상태에 있는 경우 popup을 노출하지 않는다.
            var channel = navAdapter.getCurrentChannel();
            if (util.isValidVariable(channel) === true) {
                if (channel.ccid.indexOf(STORAGE.APP_CCID) !== -1) {
                    
                    if (connected === true) {
                        log.printDbg("StorageDevice#changeState() already launched USB app... so return");
                    }
                    else {
                        log.printDbg("StorageDevice#changeState() already launched USB app... so exit USB app state");
                        // [dj.son] AppServiceManager 사용하도록 수정
                        //stateManager.exitToService({ "keyCode" : 999 });
                        KTW.managers.service.AppServiceManager.changeService({
                            nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING,
                            obj: {
                                "keyCode": 999
                            }
                        });
                    }
                    return;
                }
            }
            
            if (connected === true) {
                showPopup();
            }
            else {
                _hidePopup({force: true});
            }
        }
        
        function enableMouseControl (enable) {
        	// DATA 상태 중 full browser 상태일 경우 mouse disable 처리함
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                if (KTW.oipf.AdapterHandler.extensionAdapter.getOSDState() === 1) {
                	if (enable === true) {
                		hwAdapter.mouseControl.restoreMouseControl();
                	}
                	else {
                		hwAdapter.mouseControl.disableMouseControl();
                	}
                }
            }
        }
        
        function showPopup () {
            if (!KTW.managers.device.NetworkManager.isNetworkConnected()) {
                return;
            }

            enableMouseControl(false);

            var isCalled = false;
            var popupData = {};
            if (KTW.CONSTANT.STB_TYPE.UHD3) {
                popupData = {
                    arrMessage: [{
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                        message: ["알림"],
                        cssObj: {}
                    }, {
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                        message: ["이동식 디스크 연결을 지원하지 않습니다"],
                        cssObj: {}
                    }, {
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_30,
                        message: ["olleh tv에서 사진, 음악 및 영상을 재생하시려면" , "스마트 플레이(ch.726) 서비스를 이용해 주세요"],
                        cssObj: {}
                    }],

                    arrButton: [{id: "ok", name: "확인"}],

                    cbAction: function (buttonId) {
                        if (!isCalled) {
                            isCalled = true;
                            _hidePopup({
                                buttonId: buttonId , is_uhd3_popup : true
                            });
                        }
                    }
                };
            }else {
                popupData = {
                    arrMessage: [{
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                        message: [KTW.ERROR.EX_CODE.DEVICE.EX001.title],
                        cssObj: {}
                    }, {
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                        message: [KTW.ERROR.EX_CODE.DEVICE.EX001.message[0]],
                        cssObj: {}
                    }, {
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_32,
                        message: [KTW.ERROR.EX_CODE.DEVICE.EX001.message[1]],
                        cssObj: {}
                    }],

                    arrButton: [{id: "ok", name: "확인"}, {id: "cancel", name: "취소"}],

                    cbAction: function (buttonId) {
                        if (!isCalled) {
                            isCalled = true;
                            _hidePopup({
                                buttonId: buttonId
                            });
                        }
                    }
                };
            }

            layerManager.activateLayer({
                obj: {
                    id: STORAGE.POPUP_ID,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData,
                        enableDCA : "false"
                    }
                },
                visible: true,
                cbActivate: function () {}
            });

            is_show_popup = true;
        }
        
        function _hidePopup (options) {
            if (is_show_popup === false) {
                return;
            }
            
            enableMouseControl(true);

            if (options.buttonId || options.force) {
                layerManager.deactivateLayer({
                    id: STORAGE.POPUP_ID,
                    remove: true
                });
            }
            
            if (options.buttonId === "ok") {
                var isuhd3popup = options.is_uhd3_popup;
                if(isuhd3popup !== undefined && isuhd3popup !== null) {
                    is_show_popup = false;
                    return;
                }

                // USB app 실행
                var obj = {
                    type : KTW.CONSTANT.APP_TYPE.USB,
                    param : STORAGE.APP_LOCATOR
                };

                // wiz-game 상태라면 app 실행되지 않도록 처리
                // 이는 USB app으로 이동하는 과정에 wiz-game에서
                // lcw로 tune처리하는 로직에 의해 USB app으로 이동하지
                // 못하기 때문에 wiz-game 상태에서는
                // 실질적으는 popup만 닫는형태로 처리함 (정상시나리오라고 일단 간주)
                // wizgame 실행여부를 명시적으로 확인하여 동작하도록 수정함
                var message = {
                    "from" : KTW.CONSTANT.APP_ID.HOME,
                    "method" : KTW.managers.MessageManager.METHOD.EXT.AS_WIZGAME,
                    "to" : KTW.CONSTANT.APP_ID.APPSTORE
                };

                wizgame_response_timer = setTimeout(function() {
                    wizgame_response_timer = null;
                    // [dj.son] AppServiceManager 로 수정
                    //stateManager.exitToService(obj);
                    KTW.managers.service.AppServiceManager.changeService({
                        nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                        obj: obj,
                        forced: true
                    });
                }, 3000);

                KTW.managers.MessageManager.sendMessage(message, function(response) {
                    if (wizgame_response_timer !== null) {
                        clearTimeout(wizgame_response_timer);
                        wizgame_response_timer = null;

                        if (response && response.result == 0) {
                            // [dj.son] AppServiceManager 로 수정
                            //stateManager.exitToService(obj);
                            KTW.managers.service.AppServiceManager.changeService({
                                nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                                obj: obj,
                                forced: true
                            });
                        } else {
                            log.printWarn("don't launch because of running wizGame");
                        }
                    }
                });
            }
            
            is_show_popup = false;
        }
        
        return {
            init: _init,
            
            changeState: _changeState,
            
            /**
             * callback when changing connection state for storage device
             * if do not want to callback, set listener to "null"
             * 
             * @param is_connected - if true connected, otherwise disconnected 
             */
            setConnectionListener: function(listener) {
                connection_listener = listener;
            },
            get isConnect() {
                return connected;
            },
            
            hidePopup: function () {
                _hidePopup({force: true});
            }
        }
    }
    
    /**
     * BTDevice Object
     * 
     * search and manage bluetooth device
     */
    var BTDevice = function() {
        
        var SUPPORT_CONTEXT_MENU = false;
        var MAX_DEVICE_COUNT = 10;
        var RCU_AUDIO_DEVICE_NAME = "KT-UHD3-RS-AUDIO";
        var RCU_DEVICE_NAME = "KT-UHD3-RS";
        
        if(Window.BluetoothDevice) {
        	var TEST_DEVICES = [
                {name:"Logitech_M185", type:BluetoothDevice.TYPE_MOUSE, connected:false, lastConnectedTime:101001},
                {name:"LENOVO-THINK", type:BluetoothDevice.TYPE_HEADPHONE, connected:true, lastConnectedTime:105001},
                {name:"Voyager PRO+", type:BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN, connected:false, lastConnectedTime:102001},
                {name:"등록한 장치 이름 4", type:BluetoothDevice.TYPE_KEYBOARD, connected:false, lastConnectedTime:106001},
                {name:"AAAAAAAAAA", type:BluetoothDevice.TYPE_KEYBOARD, connected:true, lastConnectedTime:103001},
                {name:"BBBBBBBB", type:BluetoothDevice.TYPE_MOUSE, connected:true, lastConnectedTime:107001},
            ];
        }
        
        var storageManager = KTW.managers.StorageManager;
        
        var bluetooth_interface = null;
        var event_listener = null;
        var connection_listener = null;
        
        var devices = null;
        
        var pairing_timer = null;
        
        /**
         * BluetoothDevice 검색 및 추가를 위한 event 등록
         */
        function _init() {
            bluetooth_interface = hwAdapter.bluetoothInterface;
            if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                
                bluetooth_interface.addEventListener(
                        KTW.oipf.Def.CONFIG.EVENT.BLUETOOTH_DISCOVERY, onDiscovery);
                bluetooth_interface.addEventListener(
                        KTW.oipf.Def.CONFIG.EVENT.BLUETOOTH_ADD_DEVICE, onAddDevice);
                
                // 현재 enable 상태라면
                // 기존에 연결되어 있었던 bluetooth device의 상태를 확인하고 미리 연결한다.
                // [jh.lee] 페어링 상태에서 재부팅 시 다시 오디오 기기에 연결하기 위한 부분
                if (bluetooth_interface.enabled === true) {
                    log.printDbg("BTDevice _init bluetooth_interface.enabled : " + bluetooth_interface.enabled);
                    var device_names = getDeviceNames();
                    
                    log.printDbg("BTDevice _init device_names : " + device_names);
                    
                    if (device_names && device_names.length > 0) {
                        var device_list = _getPairedDevices();
                        var device_length = device_list.length;
                        
                        var connected = false;
                        log.printDbg("BTDevice _init device_list : " + device_list);
                        log.printDbg("BTDevice _init device_length : " + device_length);
                        for (var i = 0; i < device_length; i++) {
                            if (device_list[i].name === device_names[0]) {
                                // [jh.lee] 페어링 된 기기 명(device_list)과 등록된 첫번째 오디오 기기(device_names[0])를 비교
                                if (device_list[i].type === BluetoothDevice.TYPE_HEADPHONE ||
                                        device_list[i].type === BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                                    try {
                                        if (device_list[i].connected == true) {
                                            // 일단 UHD2에서 나온 이슈로 인한 방어 코드
                                            // 셋탑에서 먼저 재부팅시 이전페어링 되어있던 기기를 연결하여 성공한경우 true 때문에
                                            // 홈포탈에서 다시 연결 요청을 하지 않도록 처리. (만약 홈포탈에서 재연결 처리를하면 연결이 끊어진다고 하더라...)
                                            log.printDbg("already device connected");
                                        } else {
                                            // 셋탑이 자체적으로 자동 연결을 하지 않은 경우 이기 때문에 홈포탈이 연결 시도
                                            // 홈포탈이 연결 시도 후 다시 셋탑이 연결 시도하는 경우는 연결이 끊어지지 않기 때문에 문제없다고 함...
                                            log.printDbg("connect start");
                                            bluetooth_interface.connect(device_list[i]);   
                                        }                                       
                                    }
                                    catch(e) {
                                        log.printExec(e);
                                    }
                                    
                                    log.printDbg("initBluetoothDevices() " + device_names[0] + "connected");
                                    
                                    connected = true;
                                    break;
                                }
                            }
                            
                            // 전체를 연결하려면 여기에서 break 하면 안될 것 같은데
                            // 이게 의미가 있는지 모르겠음
                            if (connected === true) break;
                        }
                    }
                }
            }
        }
        
        function _resetDevices() {
            // sw.nam 	WEBIIIHOME-3863
            // 전체 초기화시 이어폰 페어링은 그대로 유지하기 위해 _getPairedDevices  flag 전달
            var device_list = _getPairedDevices(false,true);
            var device_length = device_list.length;
            for (var i = 0; i < device_length; i++) {
                _removeDevice(device_list[i]);
            }
            
            storageManager.ps.save(storageManager.KEY.BLUETOOTH_DEVICE, "");
            if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                bluetooth_interface.enabled = false;
            }
        }
        
        function getDeviceNames() {
            var device_names;
            
            try {
                device_names = JSON.parse(storageManager.ps.load(storageManager.KEY.BLUETOOTH_DEVICE));
            }
            catch(e) {
                log.printErr("BluetoothDevice#getDeviceNames() fail to get bluetooth device names from storage");
                log.printExec(e);
            }
            
            return device_names;
        }

        // 블루투스 이슈 확인용 임시 로그 추가
        function _getPairedDevices(is_connect,isRemoveUHD3) {
            log.printDbg("_getPairedDevices() "+ is_connect);
            var list;
            if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                list = bluetooth_interface.devices;
                log.printDbg("paired devices list : "+JSON.stringify(list));
                if (list) {
                    var length = list.length;
                    var connected_list = [];
                    var disconnected_list = [];

                    for (var i = 0; i < length; i++) {
                        log.printDbg(list[i].name+" is connected ? " + JSON.stringify(list[i].connected));
                        //2017. 11.29 sw.nam
                        // 신규리모컨 이어폰 장치의 경우 리스트에 나타타지 않도록 적용
                        if(isRemoveUHD3 == true) {
                            if(list[i].name === RCU_AUDIO_DEVICE_NAME) {
                                log.printDbg("Don't push UHD3 RCU audio device");
                                continue;
                            }
                        }

                        if (list[i].connected === true) {
                            connected_list.push(list[i]);
                        }
                        else {
                            if (is_connect !== true) {
                                disconnected_list.push(list[i]);
                            }
                        }
                    }
                    connected_list = connected_list.sort(function(obj1, obj2) {
                        if (obj1.lastConnectedTime < obj2.lastConnectedTime) {
                            return -1;
                        };
                        if (obj1.lastConnectedTime > obj2.lastConnectedTime) {
                            return 1;
                        };
                        return 0;
                    });
                    log.printDbg("connected paired devices list : "+JSON.stringify(connected_list));
                    disconnected_list = disconnected_list.sort(function(obj1, obj2) {
                        if (obj1.lastConnectedTime < obj2.lastConnectedTime) {
                            return -1;
                        };
                        if (obj1.lastConnectedTime > obj2.lastConnectedTime) {
                            return 1;
                        };
                        return 0;
                    });
                    log.printDbg("disconnected paired devices list : "+JSON.stringify(disconnected_list));
                    list = connected_list.concat(disconnected_list);
                }
            }

            log.printDbg("paired devices after concatenation list : "+JSON.stringify(list));
            return list;
        }
        
        function _addDevice(device, pin) {
            try {
                var added = bluetooth_interface.addDevice(device, pin);
                if (added === true) {
                    log.printDbg("BluetoothDevice#addDevice() success to add " + device.name);
                }
                else {
                    log.printDbg("BluetoothDevice#addDevice() fail to add " + device.name);
                }
            }
            catch(e) {
                log.printExec(e);
            }
        }
        
        function _removeDevice(device, pin) {
            try {
                var removed = bluetooth_interface.removeDevice(device);
                if (removed === true) {
                    log.printDbg("BluetoothDevice#addDevice() success to remove " + device.name);
                }
                else {
                    log.printDbg("BluetoothDevice#addDevice() fail to remove " + device.name);
                }
            }
            catch(e) {
                log.printExec(e);
            }
        }
        
        function _changeState(type, device) {
            log.printDbg("_changeState type : " + type);
            log.printDbg("_changeState device : " + device);
            if (type === CONFIG.EVENT.DEVICE_ERROR) {
                log.printDbg("_changeState CONFIG.EVENT.DEVICE_ERROR");
                // 현재 device error event 는 bluetooth 만 처리함
               if (event_listener !== null) {
                   event_listener(KTW.managers.device.DeviceManager.BLUETOOTH.CONNECT_ERROR);
               }
               return;
           }

            if (connection_listener !== null) {
                try {
                    connection_listener();
                } catch (e) {
                    log.printErr("BluetoothDevice#changeState() notify connection state occured error");
                    log.printExec(e);
                }
            }
            
            showMessage(device, type === CONFIG.EVENT.DEVICE_CONNECT);
        }
        
        function showMessage(device, is_connected) {

            var isAudioDevice = false;
            var deviceName = device.name;
            if(is_connected === true) {
                if (device.type === BluetoothDevice.TYPE_HEADPHONE ||
                    device.type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                    isAudioDevice = true;
                }
                if (!KTW.CONSTANT.STB_TYPE.ANDROID) {
                    if(deviceName === RCU_AUDIO_DEVICE_NAME) {
                        KTW.managers.service.SimpleMessageManager.showBlueToothA2DPConnectionNotiMessage();
                    }else if(deviceName !== RCU_DEVICE_NAME) {
                        KTW.managers.service.SimpleMessageManager.showBlueToothConnectionNotiMessage(deviceName , isAudioDevice);
                    }
                }
                savePairingBluetoothDevice(device.name);
            }else {
                if (!KTW.CONSTANT.STB_TYPE.ANDROID) {
                    if(deviceName === RCU_AUDIO_DEVICE_NAME) {
                        KTW.managers.service.SimpleMessageManager.showBlueToothA2DPDisConnectionNotiMessage();
                    }else if(deviceName !== RCU_DEVICE_NAME) {
                        KTW.managers.service.SimpleMessageManager.showBlueToothDisConnectionNotiMessage(deviceName);
                    }
                }
            }
        }
        
        /**
         * [jh.lee] 페어링된 오디오 기기를 저장
         * @param name
         * @returns
         */
        function savePairingBluetoothDevice(name) {
            log.printDbg("call savePairingBluetoothDevice");
            var tmp;
            var first_device;

            if(name == RCU_AUDIO_DEVICE_NAME) {
                // sw.nam 신규리모컨 이어폰 장치의 경우 추가하지 않는다 WEBIIIHOME-3666
                log.printDbg("RCU_AUDIO_DEVICE_NAME is not pushed in devices");
                return ;
            }
            
            devices = JSON.parse(storageManager.ps.load(storageManager.KEY.BLUETOOTH_DEVICE));
            
            log.printDbg("savePairingBluetoothDevice devices : " + devices);
            
            if (devices) {
                // [jh.lee] 한번이라도 등록되어 오디오 기기 정보가 존재하는 경우
                tmp = false;
                for(var i = 0; i < devices.length; i++) {
                    if (devices[i] === name) {
                        // [jh.lee] 오디오 기기 등록 리스트에 페어링 될 기기가 이미 존재하면 등록 리스트에 추가는 하지 않는다
                        // [jh.lee] 페어링이 되었기 때문에 페어링 된 오디오 기기를 가장 앞으로 이동시킨다.
                        first_device = devices[i];
                        devices.splice(i, 1);
                        devices.unshift(first_device);
                        tmp = true;
                    }
                }
                if (!tmp) {
                    log.printDbg("savePairingBluetoothDevice push device : " + name);
                    devices.unshift(name);
                }
            } else {
                // [jh.lee] 한번도 등록된 적이없는 경우
                log.printDbg("savePairingBluetoothDevice first push device : " + name);
                devices = [name];
            }
            
            storageManager.ps.save(storageManager.KEY.BLUETOOTH_DEVICE, JSON.stringify(devices));
        }
        
        function _getIconImage(type, is_focus) {
            
            var image = null;
            if (type === BluetoothDevice.TYPE_MOUSE) {
                image = "icon_bluetooth_01";
            }
            else if (type === BluetoothDevice.TYPE_HEADPHONE) {
                image = "icon_bluetooth_02";
            }
            else if (type === BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                image = "icon_bluetooth_03";
            }
            else if (type === BluetoothDevice.TYPE_KEYBOARD) {
                image = "icon_bluetooth_04";
            } else {
                // [jh.lee] 정의 되어 있지 않은 디폴트 기기의 경우
                image = "";
            }
            
            return is_focus === true ? image + "_f.png" : image + ".png";
        }
        
        function _startScan() {
            if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                var start = bluetooth_interface.startDiscovery();
                if (start === true) {
                    log.printDbg("BluetoothDevice#startScan() started discovery");
                }
                return;
            }
            log.printDbg("BluetoothDevice#startScan() fail to start discovery");
        }
        
        function _stopScan() {
            if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                var cancel = bluetooth_interface.cancelDiscovery();
                if (cancel === true) {
                    log.printDbg("BluetoothDevice#startScan() canceled discovery");
                }
                return;
            }
            log.printDbg("BluetoothDevice#startScan() fail to cancel discovery");
        }
        
        function onDiscovery(event) {
            log.printDbg("event__ : "+JSON.stringify(event));
            try {
                var state = event.state;
                log.printDbg("BluetoothDevice#onDiscovery() state = " + state);
                
                var completed = false;
                var notify = true;
                var current_device_list;
                var flag = false;
                
                switch(state) {
                    case BluetoothInterface.DISCOVERY_STARTED:
                        devices = [];
                        notify = false;
                        break;
                    case BluetoothInterface.DISCOVERY_DEVICE_FOUND:
                        log.printDbg("BluetoothDevice#onDiscovery() device found");
                        log.printDbg("BluetoothDevice#onDiscovery() name = " + event.device.name);
                        log.printDbg("BluetoothDevice#onDiscovery() mac = " + event.device.macAddress);
                        log.printDbg("BluetoothDevice#onDiscovery() profile = " + event.device.profile);
                        log.printDbg("BluetoothDevice#onDiscovery() connected = " + event.device.connected);
                        log.printDbg("BluetoothDevice#onDiscovery() lastConnectedTime = " + event.device.lastConnectedTime);

                        log.printDbg("BluetoothDevice#onDiscovery() type = "+ event.device.type);
                        if (event.device.type === BluetoothDevice.TYPE_MOUSE || 
                        		event.device.type === BluetoothDevice.TYPE_HEADPHONE ||
                        		event.device.type === BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN ||
                        		event.device.type === BluetoothDevice.TYPE_KEYBOARD) {
                            // TODO 확인필요
                            // listener가 등록되어 있는 경우에만 추가하도록 되어 있었는데
                            // 굳이 그렇게 할 필요가 있었을까?
                            // [jh.lee] PL 단에서 사용하지 않거나 유효하지 않은 TYPE을 올려주는 경우를 대비해 방어코드 추가
                            // [jh.lee] 위 4가지 type의 경우만 검색되도록 추가
                            if(event.device.connected == false) {
                                //2017.06.07 sw.nam 현재 paired 되어있지 않고  경우만 검색되도록 로직 추가(2.0 확인 결과 사용되고 있었음..)
                                current_device_list = _getPairedDevices(false, true);

                                if (current_device_list) {
                                    try {
                                        log.printDbg("current_device_list : " + JSON.stringify(current_device_list));
                                        for (var i = 0; i < current_device_list.length; i++) {
                                            if (event.device.macAddress === current_device_list[i].macAddress) {
                                                flag = true;
                                                break;
                                            }
                                        }
                                        if (flag === false) {
                                            devices.push(event.device);
                                        }else {
                                            log.printDbg(event.device.name+" is already paired.. ")
                                        }
                                    } catch(e) {
                                        log.printExec(e);
                                    }
                                } else {
                                    devices.push(event.device);
                                }

                            }

                        }
                        completed = false;
                        break;
                    case BluetoothInterface.DISCOVERY_CANCELED:
                        completed = true;
                        break;
                    case BluetoothInterface.DISCOVERY_DONE:
                        completed = true;
                        break;
                }
                
                if (notify === true && event_listener !== null) {
                    event_listener(KTW.managers.device.DeviceManager.BLUETOOTH.DISCOVERY, completed);
                }
            }
            catch(e) {
                log.printExec(e);
            }
            
            if (devices !== null && devices.length === MAX_DEVICE_COUNT) {
                _stopScan();
            }
        }
        
        function onAddDevice(event) {
            var state = event.state;
            log.printDbg("BluetoothDevice#onAddDevice() state = " + state);
            
            if (state === BluetoothInterface.STATE_STARTED) {
                pairing_timer = setTimeout(function() {
                    if (event_listener !== null) {
                        event_listener(KTW.managers.device.DeviceManager.BLUETOOTH.ADD_DEVICE, event);
                    }
                }, 60 * 1000);
            }
            else {
                clearPairingTimer();
                if (event_listener !== null) {
                    event_listener(KTW.managers.device.DeviceManager.BLUETOOTH.ADD_DEVICE, event);
                }
            }
        }
        
        /**
         * [jh.lee] 블루투스 장치 연결 메뉴에서 음성 기기 연결
         * @returns
         */
        function _audioToggle(device) {
            if (bluetooth_interface.connect(device)) {
                log.printDbg("Audio Device Toggle Successed !!");
            } else {
                log.printDbg("Audio Device Toggle Failed !!");
            }
        }
        
        /**
         * [jh.lee] 현재 오디오 블루투스 장치가 연결 되어 있는지 확인
         * @returns
         */
        function _isAudioDeviceConnected() {
            log.printDbg("_isAudioDeviceConnected()");
            var result = false;
            var connected_audio_list;
            
            if(KTW.CONSTANT.IS_UHD && bluetooth_interface && bluetooth_interface.enabled){
                log.printDbg("_isAudioDeviceConnected in KTW.CONSTANT.IS_UHD");
                connected_audio_list = _getPairedDevices(true);
                if(connected_audio_list && connected_audio_list.length > 0){
                    for(var i = 0; i < connected_audio_list.length; i++) {
                        log.printDbg("_isAudioDeviceConnected connected_audio_list == "+connected_audio_list[i].type);
                        if(connected_audio_list[i].type === BluetoothDevice.TYPE_HEADPHONE || 
                                connected_audio_list[i].type === BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                            result = true;
                            break;
                        }
                    }
                }
            }
            log.printDbg("_isAudioDeviceConnected's result is : "+result);
            return result;
        }

        function _checkKTBlueToothA2DPDevice() {
            log.printDbg("_isAudioDeviceConnected()");
            var result = false;
            var connected_audio_list;

            if(KTW.CONSTANT.IS_UHD && bluetooth_interface && bluetooth_interface.enabled){
                log.printDbg("_isAudioDeviceConnected in KTW.CONSTANT.IS_UHD");
                connected_audio_list = _getPairedDevices(true);
                if(connected_audio_list && connected_audio_list.length > 0){
                    for(var i = 0; i < connected_audio_list.length; i++) {
                        log.printDbg("_checkKTBlueToothA2DPDevice connected_audio_list.name == "+connected_audio_list[i].name);
                        if(connected_audio_list[i].name === RCU_AUDIO_DEVICE_NAME) {
                            result = true;
                            break;
                        }
                    }
                }
            }
            log.printDbg("_checkKTBlueToothA2DPDevice result is : "+result);
            return result;
        }
        
        
        /**
         * [jh.lee] TODO 화면해설방송 관련 함수. R3에서 미구현 상태. 확인 필요
         * @returns
         */
        function _screenCommentary() {
            log.printDbg("Screen Commentary !!!");
            
        }
        
        /**
         * [jh.lee] 현재 연결된 오디오 블루투스 장치를 반환
         * @returns
         */
        function _getBtAudioDevice(is_not_connected) {
            var device = null;
            var device_list;
            
            if(KTW.CONSTANT.IS_UHD) {
                device_list = bluetooth_interface.devices;
                
                if(device_list && device_list.length > 0) {
                    for(var i = 0; i < device_list.length; i++) {
                        if(device_list[i].type === BluetoothDevice.TYPE_HEADPHONE || 
                                device_list[i].type === BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                            if(is_not_connected && !device_list[i].connected) {
                                device = device_list[i];
                                break;
                            }else if(!is_not_connected && device_list[i].connected) {
                                device = device_list[i];
                                break;
                            }
                        }
                    }
                }
            }
            return device;
        }
        
        /**
         * [jh.lee] 음성 블루투스 기기 사운드 On
         * @returns
         */
        function _soundOn() {
            log.printDbg("bluetooth Device sound ON !!!");
            var device;
            var multi_component;
            var channel_control;
            
            device = _getBtAudioDevice();
            channel_control = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            multi_component = channel_control.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO)[0];
            channel_control.selectComponent(multi_component, device);
        }
        
        /**
         * [jh.lee] 음성 블루투스 기기 사운드 Off
         * @returns
         */
        function _soundOff() {
            log.printDbg("bluetooth Device sound OFF !!!");
            var device;
            var multi_component;
            var channel_control;
            
            device = _getBtAudioDevice();
            channel_control = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            multi_component = channel_control.getCurrentActiveComponents(MediaExtension.COMPONENT_TYPE_AUDIO, device)[0];
            channel_control.unselectComponent(multi_component, device);
        }
        
        /**
         * [jh.lee] 오디오 블루투스 기기 멀티오디오 변경
         * @param lang
         * @param isBluetooth
         * @returns
         */
        function _changeMultiAudio(lang, isBluetooth) {
            log.printDbg("changeMultiAudio lang : " + lang);
            log.printDbg("changeMultiAudio isBluetooth : " + isBluetooth);
            
            var channel_control;
            var multi_component;
            
            channel_control = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
            multi_component = channel_control.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO);
            
            if (multi_component.length > 1) {
                for(var i = 0; i < multi_component.length; i++) {
                    if(lang === multi_component[i].language) {
                        if(isBluetooth) {
                            log.printDbg("change Multi Audio [language] ====== > " + multi_component[i].language);
                            channel_control.selectComponent(multi_component[i], _getBtAudioDevice());
                        }else{
                            log.printDbg("change Multi Audio [language] ====== > " + multi_component[i].language);                            
                            channel_control.selectComponent(multi_component[i]);
                        }                        
                        break;
                    }
                }
            }
            
        }
        
        function clearPairingTimer() {
            if (pairing_timer !== null) {
                clearTimeout(pairing_timer);
                pairing_timer = null;
            }
        }

        function _isListenAloneMode() {
            log.printDbg("isListenAloneMode()");
            if(bluetooth_interface !== undefined && bluetooth_interface !== null) {
                if(bluetooth_interface.listenAloneMode !== undefined && bluetooth_interface.listenAloneMode !== null) {
                    return bluetooth_interface.listenAloneMode;
                }else {
                    log.printDbg("isListenAloneMode() (bluetooth_interface.listenAloneMode === null or undefined");
                    return false;
                }

            }
        }

        function _setListenAloneMode(aloneMode) {
            log.printDbg("_setListenAloneMode() aloneMode : " + aloneMode);
            if(bluetooth_interface !== undefined && bluetooth_interface !== null) {
                if(bluetooth_interface.listenAloneMode !== undefined && bluetooth_interface.listenAloneMode !== null) {
                    bluetooth_interface.listenAloneMode = aloneMode;
                }else {
                    log.printDbg("_setListenAloneMode() (bluetooth_interface.listenAloneMode === null or undefined");
                }
            }

        }
        
        return {
            
            init: _init,
            
            changeState: _changeState,
            
            /**
             * callback when changing connection or adding status or upcomming result of discovery
             * if do not want to callback, set listener to "null"
             * 
             * @param event_type - KTW.managers.device.DeviceManager.BLUETOOTH 참조
             * @param obj - 각 상황에 추가적으로 필요한 parameter 전달
             *             CONNECT_ERROR : none
             *             DISCOVERY : true/false
             *             ADD_DEVICE : event Object
             */
            setEventListener: function(listener) {
                event_listener = listener;
            },
            setConnectionListener: function(listener) {
                connection_listener = listener;
            },
            
            get enabled() {
                if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                    return bluetooth_interface.enabled;
                }
                return false;
            },
            set enabled(enable) {
                if (bluetooth_interface !== null && bluetooth_interface !== undefined) {
                    bluetooth_interface.enabled = enable;
                }
            },
            
            /**
             * 초기화
             */
            resetDevices: _resetDevices,
            
            addDevice: _addDevice,
            removeDevice: _removeDevice,
            startScan: _startScan,
            stopScan: _stopScan,
            
            get deviceList() {
                return devices;
            },
            getPairedDevices: _getPairedDevices,
            getIconImage: _getIconImage,
            
            audioToggle: _audioToggle,
            isAudioDeviceConnected: _isAudioDeviceConnected,
            checkKTBlueToothA2DPDevice : _checkKTBlueToothA2DPDevice,
            screenCommentary: _screenCommentary,
            getBtAudioDevice: _getBtAudioDevice,
            soundOff: _soundOff,
            soundOn: _soundOn,
            changeMultiAudio: _changeMultiAudio,

            isListenAloneMode: _isListenAloneMode,
            setListenAloneMode: _setListenAloneMode
            
            // TODO 아래 사항 구현 필요
            //isAudioToggle: _isAudioToggle,
        }
    }
    
    return {
        init: _init,
        
        get storageDevice() {
            return storage_device;
        },
        
        get bluetoothDevice() {
            return bluetooth_device;
        }
    }
}());

Object.defineProperty(KTW.managers.device.DeviceManager, "BLUETOOTH", {
    value: {
        CONNECT_ERROR : 0,
        DISCOVERY : 1,
        ADD_DEVICE : 2
    },
    writable: false,
    configurable: false
});