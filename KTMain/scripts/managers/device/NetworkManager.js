"use strict";

/**
 * monitoring network connection state
 * 
 */
KTW.managers.device.NetworkManager = (function () {
    var NETWORK = {
        STATE : {
            CONNECTED : 0,
            DISCONNECTED : 1
        },
        TYPE : {
            SATELLITE : 0,
            LAN : 1
        }
    };
    
    var CONFIG_EVENT = KTW.oipf.Def.CONFIG.EVENT;

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;

    // network이 연결되지 않은 상태에서는 HomePortal이 시작되지 않는다.
    // 그래서 초기 값은 무조건 connect 상태로 간주한다.
    // 위성방송의 경우 tune 시점마다 tuning over event에 의해서 올라오지만
    // LAN network의 경우 connect/disconnect 상태만 존재하므로
    // LAN network 상태만을 관리함
    var network_state = NETWORK.STATE.CONNECTED;
    
    var enable_weak_signal = true;
    
    var weak_signal_listeners = null;

    var wait_appstore_response_timer = null;

    /**
     * 신호미약/네트웍 오류 팝업 Layer ID
     */
    var networkErrorPopupLayerId = "NetworkErrorPopup";
    var weakSignalErrorPopupLayerId = "WeakSignalPopup";

    function _init() {
        weak_signal_listeners = [];
        
        hwAdapter.addEventListener(CONFIG_EVENT.NETWORK, onNetworkEvent);
        checkNetworkState();
    }
    
    function _isNetworkConnected() {
        return network_state === NETWORK.STATE.CONNECTED;
    }
    
    function _addWeakSignalListener(listener) {
        weak_signal_listeners.push(listener);
    }
    
    function _removeWeakSignalListener(listener) {
        var index = weak_signal_listeners.indexOf(listener);
        if (index !== -1) {
            weak_signal_listeners.splice(index, 1);
        }
    }
    
    function showNetworkErrorPopup(network_type) {
        // 2017.02.15 dhlee
        // [KTWEB-4] TV인터넷 상태에서 network error popup 노출 시
        // mouseControl을 disable 처리
        util.changeMouseControlStatus(false);

        var layer_id;
        var view_obj;
        var popup_data = {};

        if (network_type === NETWORK.TYPE.LAN) {
            layer_id = networkErrorPopupLayerId;
            view_obj = KTW.ui.view.popup.NetworkErrorPopup;
            if(KTW.CONSTANT.IS_OTS === true) {
                popup_data.callback = hideNetworkErrorPopup;
            }
        }
        else if (network_type === NETWORK.TYPE.SATELLITE) {
            layer_id = weakSignalErrorPopupLayerId;
            view_obj = KTW.ui.view.popup.WeakSignalPopup;
            popup_data.callback = hideWeakSignalErrorPopup;

            /**
             * jjh1117 2017.03.27
             * TODO DCS 인 경우 팝업 문구 변경 되어야 함.
             */
        }

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: layer_id,
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP ,
                view : view_obj,
                linkage: true,
                params: {
                    data : popup_data,
                    enableDCA : "false",
                    enableChannelKey : (network_type === NETWORK.TYPE.SATELLITE) ? "true" : "false",
                    enableMenuKey : "false",
                    enableExitKey : "false",
                    enableBackKey : "false",
                    enableHotKey : "false"
                }
            },
            visible: true,
            cbActivate: function () {
            }
        });
    }

    function hideNetworkErrorPopup(index) {
        log.printDbg("hideNetworkErrorPopup() index = " + index);

        KTW.ui.LayerManager.deactivateLayer({
            id: networkErrorPopupLayerId
        });
        if (index === 1) {
            KTW.oipf.AdapterHandler.extensionAdapter.reboot();
        }
    }
    
    function hideWeakSignalErrorPopup(index) {
        log.printDbg("hideWeakSignalErrorPopup() index = " + index);

        KTW.ui.LayerManager.deactivateLayer({
            id: weakSignalErrorPopupLayerId
        });

        util.changeMouseControlStatus(true);

        if (index === 0) { // 확인버튼 선택한 경우
            // 위성 신호미약 popup 에서 확인키를 누른 경우
            // TODO 이 상황에서 다시 weak signal 상황이 발생하는 경우
            // 무시하도록 하기 위해서 timeout을 써야 하는데
            // 과연 이렇게까지 tight 하게 올라올까?

            var channel = null;
            var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
            var cur_channel = navAdapter.getCurrentChannel();
            if (util.isValidVariable(cur_channel) === true) {
                var same_channel = navAdapter.getSameKTChannel(cur_channel);
                if (util.isValidVariable(same_channel) === true) {
                    channel = same_channel;
                }
            }

            if (util.isValidVariable(channel) === false) {
                channel = navAdapter.getPromoChannel();
            }

            // OTS STB에서 만약 이때 PIP가 노출되어 있는 상태라면 PIP를 종료한다.
            if (KTW.CONSTANT.IS_OTS === true) {
                var pip_cc = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
                if (pip_cc !== null && pip_cc.getState() === KTW.nav.Def.CONTROL.STATE.STARTED) {
                    pip_cc.stop();
                }
            }

            // 현재 채널이 없는 경우가 없을텐데 혹시 없다면
            // 위에서 획득한 채널로 설정해야 함(그런데 일단 무시)
            navAdapter.changeChannel(channel);
        }
    }
    
    function onNetworkEvent(event) {
        
        var type = event.type;
        var device_id = event.device.id;
        log.printDbg("onNetworkEvent() type = " + type);
        log.printDbg("onNetworkEvent() device = " + device_id + "(" + event.device.name + ")");
        var stateManager = KTW.managers.service.StateManager;
        
        var network_type = getNetworkType(event.device.idTypes);

        log.printDbg("getNetworkType() network_type = " + network_type);
        if (type === CONFIG_EVENT.DEVICE_CONNECT) {
            if (network_type === NETWORK.TYPE.LAN) {
                if (network_state === NETWORK.STATE.CONNECTED) {
                    log.printDbg("onNetworkEvent() already connected state(LAN)... so return");
                    return;
                }
                network_state = NETWORK.STATE.CONNECTED;
            }
            else if (network_type === NETWORK.TYPE.SATELLITE) {
                if (enable_weak_signal === false) {
                    log.printDbg("onNetworkEvent() CONNECT - disabled checking weak signal... so return");
                    return;
                }
                
                // PIP tuner
                if (device_id === 1) {
                	if (isSatelliteChannel(true) === true) {
                		notifyWeakSignal(NETWORK.STATE.CONNECTED);
                	}
                    return;
                }
                
                // 신호미약 상태가 정상적으로 돌아온 경우
                // 여전히 wiz-game 응답 message를 기다리고 있다면
                // 해당 timer를 제거하여 신호미약 popup이 노출되지 않도록 함(KTWHPTZOF-1338)
                if (wait_appstore_response_timer !== null) {
                    clearTimeout(wait_appstore_response_timer);
                    wait_appstore_response_timer = null;
                }
            }

            if(network_type === NETWORK.TYPE.LAN) {
                // 2017.07.13 dhlee
                // WEBIIIHOME-2789 이슈 수정
                // 네트웍 에러 팝업 종료 시 mouse enable 해 줘야 함
                util.changeMouseControlStatus(true);
                KTW.ui.LayerManager.deactivateLayer({
                    id: networkErrorPopupLayerId
                });
            }else if(network_type === NETWORK.TYPE.SATELLITE) {
                // 2017.07.13 dhlee
                // WEBIIIHOME-2789 이슈 수정
                // 신호 미약 팝업 종료 시 mouse enable 해 줘야 함
                util.changeMouseControlStatus(true);
                KTW.ui.LayerManager.deactivateLayer({
                    id: weakSignalErrorPopupLayerId
                });
            }
            
        }
        else if (type === CONFIG_EVENT.DEVICE_DISCONNECT) {
            if (network_type === NETWORK.TYPE.LAN) {
                if (network_state === NETWORK.STATE.DISCONNECTED) {
                    log.printDbg("onNetworkEvent() already disconnected state(LAN)... so return");
                    return;
                }

                // LAN cable 해제 시 TV_VIEWING 상태라면
                // 현재 채널이 IPTV인 경우에만 network error popup을 노출함
                // 이외의 경우에는 모두 노출하도록 함.
                // 2017.02.15 2.0 R7 내용 반영
                // LAN cable 해제 시 TV_VIEWING 및 VOD 상태이고
                // 현재 채널이 IPTV인 경우에는 network error popup을 노출함
                var show_popup = true;
                if (stateManager.isTVViewingState() === true || stateManager.isVODPlayingState() === true) {
                    var channel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                    if (util.isValidVariable(channel) === false) {
                        show_popup = false;
                    }
                    else {
                        if (channel.idType === Channel.ID_DVB_S ||
                                channel.idType === Channel.ID_DVB_S2) {
                            show_popup = false;
                        }
                    }
                }

                if (show_popup === true) {
                    showNetworkErrorPopup(network_type);
                }
                network_state = NETWORK.STATE.DISCONNECTED;
            }
            else if (network_type === NETWORK.TYPE.SATELLITE) {
                // weak signal 체크가 disable 된 상태에서는 disconnect event를 무시한다.
                if (enable_weak_signal === false) {
                    log.printDbg("onNetworkEvent() DISCONNECT - disabled checking weak signal... so return");
                    return;
                }
                
                // PIP tuner
                if (device_id === 1) {
                	if (isSatelliteChannel(true) === true) {
                		notifyWeakSignal(NETWORK.STATE.DISCONNECTED);
                	}
                    return;
                }
                
                // 위성 cable 해제 시
                // 시청시간 제한 및 VOD, full browser 상태인 경우에는 popup 노출하지 않음
                // full browser 상태에서도 popup 노출하지 않음
                // 2016.10.19 dhlee serviceState 로 변경됨
                //var cur_svc_state = stateManager.state;
                var cur_svc_state = stateManager.serviceState;
                if (cur_svc_state === KTW.CONSTANT.SERVICE_STATE.VOD ||
                        cur_svc_state === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
                	log.printDbg("onNetworkEvent() VOD or Time Restrict state... so return");
                    return;
                }
                else if (cur_svc_state === KTW.CONSTANT.SERVICE_STATE.DATA) {
                    if (KTW.oipf.AdapterHandler.extensionAdapter.getOSDState() === 1) {
                    	log.printDbg("onNetworkEvent() full browser state... so return");
                    	return;
                    }
                }
                
                // 위성방송 상태일 경우 weak signal을 띄워준다
                // RF 제거 상태로 부팅 시에는 checkWeakSignal()에서 생성한 event.channel을 통해 채널정보를 전달받는다
                // 그러나 이후 event에는 event.channel이 undefined로 넘어온다
                // 부팅 이후에는 getCurrentChannel 또는 vbo.currentChannel을 통해 채널정보를 가져올 수 있지만
                // 부팅 시에는 위의 방법으로는 채널정보를 가져올 수 없어 event.channel로 전달받아 신호미약 팝업을 호출한다
                if (isSatelliteChannel(false, event) === true) {
                    // wizgame app이 실행중인 경우 popup 노출하지 않는다.
                    // 그런데 postmessage 방식이라서 문제가 될 텐데 2초간만 check 하면 될까?
                    // 그리고 callback과 timeout 이 서로 엇갈리면 문제가 되지 않을까?
                    var message = {"from":KTW.CONSTANT.APP_ID.HOME};
                    message.method = KTW.managers.MessageManager.METHOD.EXT.AS_WIZGAME;
                    message.to = KTW.CONSTANT.APP_ID.APPSTORE;

                    //Transponder 즉 수신 감도 측정중엔 팝업 뜨면 안되는데
                    //그 조건은 처리 되는지 확인 필요.
                    //timeout 에 의해 처리 된 경우 sendMessage response 처리 안하도록 추가 필요.
                    wait_appstore_response_timer = setTimeout(function() {
                        wait_appstore_response_timer = null;
                        
                        // 신호미약 노출하려고 할 때 현재 채널이 olleh 채널이면
                        // 무시하도록 함
                        var show_popup = true;
                        var channel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                        if (util.isValidVariable(channel) === true) {
                            if (channel.idType === Channel.ID_IPTV_SDS ||
                                    channel.idType === Channel.ID_IPTV_URI) {
                            	show_popup = false;
                            }
                        }
                        
                        if (show_popup === true) {
                        	showNetworkErrorPopup(network_type);
                        	log.printWarn("onNetworkEvent() showNetworkErrorPopup cause by timeout");
                        }
                    }, 3000);

                    KTW.managers.MessageManager.sendMessage(message, function(response) {
                        //timeout 에 걸렸다면 해당 timer 는 clear 되고 null 처리
                        //되었을거기 때문에 처리하지 않음.
                        if (wait_appstore_response_timer !== null) {
                            clearTimeout(wait_appstore_response_timer);
                            wait_appstore_response_timer = null;

                            if(response && response.result == 0) {
                                showNetworkErrorPopup(network_type);
                            } else {
                                log.printWarn("onNetworkEvent() showNetworkErrorPopup skipped by wizGame " + (response && response.result));
                            }
                        }
                    });
                }
            }
        }
    }
    
    /**
     * network type 을 확인
     * 
     * callback 받은 event object에서 Tuner#idTypes 기준으로 구분한다.
     * - LAN cable type : Channel.ID_IPTV_URI or Channel.ID_IPTV_SDS
     * - SATELLITE cable type : Channel.ID_DVB_S or Channel.ID_DVB_S2
     */
    function getNetworkType(id_types) {
        log.printDbg("getNetworkType() id_types = " + id_types.toString());
        
        var index = id_types.indexOf(Channel.ID_DVB_S);
        if (index === -1) {
            index = id_types.indexOf(Channel.ID_DVB_S2);
        }
        
        return index === -1 ? NETWORK.TYPE.LAN : NETWORK.TYPE.SATELLITE;
    }
    
    /**
     * 현재 채널이 위성채널인지 확인
     * RF제거 상태로 부팅 시에는 navAdapter 혹은 channelControl을 통해 채널정보를 가져올 수 없으며
     * 이 때에는 event.channel로 전달되는 채널정보를 가져온다
     * @param is_pip 채널정보를 가져오기 위해 pip 여부 확인
     * @param event (optional) event.channel이 존재하는 경우 해당 채널 정보를 참조
     */
    function isSatelliteChannel(is_pip, event) {
    	var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    	var channelControl = is_pip === true ?
    			navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP) :
				navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
		var channel = null;
        
        // OTS STB에서 RF 제거후 부팅 시 current channel이 없기 때문에
        // 이미 정의된 channel 정보를 이용해서 처리하도록 함
        if (KTW.CONSTANT.IS_OTS === true) {
        	// event.channel이 존재하는 경우 해당 채널 정보를 참조
        	if (event != null && event.channel != null) {
        		channel = event.channel;
        	}
        	// event.channel이 없으면 currentChannel을 참조
        	else {
        		channel = channelControl.vbo.currentChannel;
        	}
        }
        
        if (channel && (channel.idType === Channel.ID_DVB_S || channel.idType === Channel.ID_DVB_S2)) {
            return true;
        }
        return false;
    }
    
    function checkNetworkState() {
        var enable_tuner = hwAdapter.enableTuner;
        
        log.printDbg("checkNetworkState() enable_tuner = " + enable_tuner);
        
        if (enable_tuner === false) {
        	// OTV, OTS 여부에 따라 초기 network 오류 상태를 전달한다.
        	var id_types = [ Channel.ID_IPTV_SDS, Channel.ID_IPTV_URI ];
        	if (KTW.CONSTANT.IS_OTS === true) {
        		id_types = [ Channel.ID_DVB_S, Channel.ID_DVB_S2 ]
        	}
        	
            var event = {
                type : CONFIG_EVENT.DEVICE_DISCONNECT,
                device : {
                    idTypes : id_types
                },
                channel : {  "idType" : id_types[0] }
            };
            onNetworkEvent(event);
        }
    }
    
    /**
     * enable or disable checking weak signal for satellite 
     */
    function _enableWeakSignal(enable) {
        log.printDbg("_enableWeakSignal() enable = " + enable);
        
        enable_weak_signal = enable;
    }
    
    function notifyWeakSignal(state) {
        var copy = weak_signal_listeners.slice();
        var length = copy.length;
        for (var i = 0; i < length; i++) {
            try {
                copy[i](state);
            }
            catch(e) {
                log.printErr("notifyWeakSignal() occured error");
                log.printExec(e);
            }
        }   
    }
    
    return {
        init: _init,
        
        isNetworkConnected: _isNetworkConnected,
        
        enableWeakSignal: _enableWeakSignal,
        
        addWeakSignalListener: _addWeakSignalListener,
        removeWeakSignalListener: _removeWeakSignalListener
    };
}());