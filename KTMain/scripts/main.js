"use strict";

/**
 * process to do until initializing.
 * checking cas status, make channels and programs and so on.
 */
KTW.main = (function () {
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var caHandler = KTW.ca.CaHandler;
    var mmiHandler = KTW.ca.MmiHandler;

    // [dj.son] AppServiceManager 추가 & init 호출
    var appServiceManager = KTW.managers.service.AppServiceManager;
    var layerManager = KTW.ui.LayerManager;
    var stateManager = KTW.managers.service.StateManager;
    var ocManager = KTW.managers.data.OCManager;
    var adapterHandler = KTW.oipf.AdapterHandler;
    var menuDataManager = KTW.managers.data.MenuDataManager;
    var messageManager = KTW.managers.MessageManager;
    //var storageManager = KTW.managers.StorageManager;
    var promoManager = KTW.managers.service.PromoManager;
    var deviceManager = KTW.managers.device.DeviceManager;
    var networkManager = KTW.managers.device.NetworkManager;
    var powerModeManager = KTW.managers.device.PowerModeManager;
    //var viewHistoryManager = KTW.managers.data.ViewHistoryManager;

    var moduleManager = KTW.managers.module.ModuleManager;
    var userLogManager = KTW.managers.UserLogManager;

    // 2018.03.05 sw.nam  listener 등록을 위한 통합페어링 매니져 추가
    var otmpManager = KTW.managers.otmPairing.OTMPManager;

    var bStartActivateHome = false;
    var bProcessFirstKey = false;
    var bStartSvc = false;


    
    /**
     *부팅 시 menu data 를 270초간 받지 못한 경우 부팅 실패로 간주
     */
    var booting_time = 0;

    var bootingChannelEventNumber = 0;
     
    /**
     * step1 : load modules (CAS > HDS )
     * step2 : check auth
     * step3 : build channels and programs
     * step4 : start main layer
     */
    function _init() {
        log.printForced(KTW.TITLE + " app v" + KTW.UI_VERSION + "_" + KTW.UI_DATE + "(" + KTW.RELEASE_MODE + ") start");
        log.printInfo("_init()");

        initData();
        initKeySet();

        KTW.oipf.oipfObjectFactory.ready();

        layerManager.init();
        caHandler.init();
        mmiHandler.init();

        adapterHandler.hwAdapter.init();
        powerModeManager.init();
        deviceManager.init();

        userLogManager.init();

        otmpManager.init();

        //KTW.managers.auth.AuthManager.init(); // dhlee 2016.11.03 AuthManager 초기화
        homeImpl.init();

        showDebugView();
    }
    
    function _start() {
        log.printInfo("start()");
        
        // cas 및 개통 확인 후 nav, svc init/start 처리함
        initNav();
        initSvc();

        checkBizUser();
        //loadData();
        startNav();
        
        // oc attach 시작 후 network 초기화를 수행한다.
    	// network 연결되지 않은 상태에서 appReady 시점에 초기화 할 경우
    	// network 오류 popup이 제대로 노출되지 않아서 시점을 변경함
        networkManager.init();
        checkMenuData();

        KTW.managers.service.VoiceableManager.init();
        
        stateManager.appState = KTW.CONSTANT.APP_STATE.INITIALIZED;
    }
    
    function _resume(state) {
        log.printInfo("_resume()");
        
        // STARTED 상태 이후에 처리되도록 함
        if (stateManager.appState > KTW.CONSTANT.APP_STATE.INITIALIZED) {
        	stateManager.appState = KTW.CONSTANT.APP_STATE.STARTED;
        }
    }
    
    function _pause(state) {
        log.printInfo("_pause()");
        
        // STARTED 상태 이후에 처리되도록 함
        if (stateManager.appState > KTW.CONSTANT.APP_STATE.INITIALIZED) {
        	stateManager.appState = KTW.CONSTANT.APP_STATE.PAUSED;
        }
    }
    
    function _destroy(restart) {
        log.printInfo("_destroy()");
        
        _pause();
        
        stateManager.appState = KTW.CONSTANT.APP_STATE.DESTROYED;
        appServiceManager.destroy();
        
        if (restart) {
            KTW.oipf.AdapterHandler.extensionAdapter.reboot();
        }
        else {
            //basicAdapter.exitApp();
        }
    }
    
    function initData() {
        // TODO local data migration
        if (KTW.PLATFORM_EXE === "PC") {
            KTW.SAID = "TT150413021";
            KTW.SMARTCARD_ID = "";
        }
        else {
            var config = KTW.oipf.Def.CONFIG;
            var basicAdapter = adapterHandler.basicAdapter;
            
            KTW.SAID =  basicAdapter.getConfigText(config.KEY.SAID);
            KTW.SMARTCARD_ID = basicAdapter.getConfigText(config.KEY.SMARTCARD_ID);
            
            var mbs = basicAdapter.getConfigText(config.KEY.MBS);
            var uhd = basicAdapter.getConfigText(config.KEY.UHD);
            var hdr = basicAdapter.getConfigText(config.KEY.HDR);
            var skylife = basicAdapter.getConfigText(config.KEY.SKYLIFE);
            var series_reservation = basicAdapter.getConfigText(config.KEY.SERIES_RESERVATION); // 2016.11.21 dhlee 시리즈 예약 지원 여부
            var stb_type = basicAdapter.getConfigText(config.KEY.STB_TYPE);

            // 2016.11.21 UHD3 관련 세부 property
            var uhd3_beacon = basicAdapter.getConfigText(config.KEY.UHD3.BEACON);
            var uhd3_tts    = basicAdapter.getConfigText(config.KEY.UHD3.TTS);
            var uhd3_speaker= basicAdapter.getConfigText(config.KEY.UHD3.SPEAKER);
            var uhd3_voice  = basicAdapter.getConfigText(config.KEY.UHD3.VOICE);

            // 2017.06.26 dhlee youtube 지원 여부
            var youtube_support = basicAdapter.getConfigText(config.KEY.YOUTUBE_SUPPORT);

            var tv_pay_support = basicAdapter.getConfigText(config.KEY.TV_PAY_SUPPORT);
            
            if (mbs === "true") {
                KTW.CONSTANT.IS_DCS = true;
            }
            
            // skylife_support 값에 따라 OTS STB 여부 확인
            // undefined 인 경우 : OTS 펌웨어 업그레이드 이전, OTS로 동작
            // true인 경우 : OTS 펌웨어 업그레이드 이후, OTS로 동작
            if (skylife === undefined || skylife === "undefined" || skylife === "true") {
                KTW.CONSTANT.IS_OTS = true;
            }
            
            if (uhd === "true") {
                // OTS UHD에서는 아직 UHD 기능이 활성화 되지 않도록 처리함 
                //if (KTW.CONSTANT.IS_OTS === false && KTW.CONSTANT.IS_DCS === false) {
                // [jh.lee] 기존 웹홈 소스에 있는 부분을 주석처리함.
                // [jh.lee] OTS UHD를 실행시키기 위해 주석처리.
                KTW.CONSTANT.IS_UHD = true;
                //}
            } else {
            	//uhd 전용관 cat 삭제.
            	delete KTW.DATA.MENU.CAT.UHD;
            }

            KTW.STB_MODEL = KTW.oipf.adapters.HWAdapter.getModelName();

            // 2017.01.04 dhlee for 기가지니
            // 2017.05.22 dhlee STB TYPE 추가 (CAT_TYPE 추가에 따라 STB 별로 메뉴 노출이 가능하도록 함)
            if (stb_type === "android") {
                KTW.CONSTANT.STB_TYPE.ANDROID = true;

                if (KTW.STB_MODEL === "CT1100") {
                    KTW.CONSTANT.STB_TYPE.GIGAGENIE1 = true;
                }
                else if (KTW.STB_MODEL === "CT1101") {
                    KTW.CONSTANT.STB_TYPE.GIGAGENIE2 = true;
                }
            }
            else if (stb_type === "UHD1") {
                KTW.CONSTANT.STB_TYPE.UHD1 = true;
            }
            else if (stb_type === "UHD2") {
                KTW.CONSTANT.STB_TYPE.UHD2 = true;
            }
            else if (stb_type === "UHD3") {
                KTW.CONSTANT.STB_TYPE.UHD3 = true;
            }

            if (hdr === "true") {
                KTW.CONSTANT.IS_HDR = true;
            }

            if (uhd3_beacon === "true") {
                KTW.CONSTANT.UHD3.BEACON = true;
            }

            if (uhd3_tts === "true") {
                KTW.CONSTANT.UHD3.TTS = true;
            }

            if (uhd3_speaker === "true") {
                KTW.CONSTANT.UHD3.EX_SPEAKER = true;
            }

            if (uhd3_voice === "true") {
                KTW.CONSTANT.UHD3.EX_VOICE = true;
            }

            if (youtube_support === "true") {
                KTW.CONSTANT.YOUTUBE_SUPPORT = true;
            }

            if (tv_pay_support === "true") {
                KTW.CONSTANT.TV_PAY_SUPPORT = true;
            }

            // 2016.11.21 dhlee 시리즈 예약 지원 여부 추가
            // 시리즈 예약 지원 여부가 아닌 reminder에서 appdata를 지원하는지에 대한 여부이다.
            // OTS에서는 이 값이 true 일까? 확인해 보자.
            if (series_reservation === "true" && KTW.CONSTANT.IS_OTS === false) {
                KTW.CONSTANT.SERIES_RESERVATION = true;
            }

            /**
             * 고도화 시 OTS 신규 서버 연동 여부에 따라 부가세 포함 문구 표시를 할지 말지 결정함.
             */
            KTW.CONSTANT.IS_OTS_VAT_PRICE = false;

            // 2017.04.04 dhlee
            // STB_POINT_CACHE 영역의 ReqFalg 를 true 로 set
            // 2017.06.02 dhlee
            // 데이터 존재 여부와 상관 없이 ReqFlag를 true로 set 하도록 한다.
            var cacheObj = KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE);
            if (cacheObj && cacheObj !== "undefined") {
                var data = JSON.parse(cacheObj);
                if (!data.ReqFlag) {
                    data.ReqFlag = true;
                    KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
                }
            }
            else {
                var data = {};
                data.ReqFlag = true;
                KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.STB_POINT_CACHE, JSON.stringify(data));
            }

            /**
             * [dj.son] 파일 저장 영역을 live - bmt 로 구분하여, 서버 전환시 남아있는 directory & file 삭제하도록 로직 추가
             *
             * - 결국 fileSystem 내 path 는 "live/~~" 혹은 "bmt/~~" 형태로 존재
             * - "live" or"bmt" directory 의 생성은 OCImageManager init 시 수행됨
             */
            KTW.CONSTANT.DEFAULT_FILE_PATH = KTW.RELEASE_MODE.toLocaleLowerCase();

            KTW.managers.data.fileManager.isExistDirectory(KTW.CONSTANT.DEFAULT_FILE_PATH, function (success) {
                /**
                 * [dj.son] default directory 가 없다는 말은 live <-> bmt 로 전환되었음을 의미하므로, 현재 fileSystem 내 모든 directory & file 삭제
                 */
                if (!success) {
                    /**
                     * [dj.son] live & bmt 송출앱 버전이 같을 경우 다른 path 의 모듈을 참조할 수 있음
                     *          따라서 모듈 버전 정보를 초기화 할 수 있도록, module version data 를 삭제함
                     */
                    KTW.managers.StorageManager.ps.remove(KTW.managers.StorageManager.KEY.MODULE_VERSION_DATA);

                    KTW.managers.data.fileManager.listFiles("/", function (success, list) {
                        if (list && list.length > 0) {
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].isDirectory) {
                                    KTW.managers.data.fileManager.deleteDirectory(list[i].fullPath);
                                }
                                else {
                                    KTW.managers.data.fileManager.removeFile(list[i].fullPath);
                                }
                            }
                        }
                    });
                }
            });
        }


        /**
         * [dj.son] 기가지니에서 1360, 40, 560, 135 사이즈로 av resize 시, resize 가 안되고 해당 상태에서 튠 시도하면 블랙 화면이 되는 이슈가 있음 (WEBIIIHOME-3509, WEBIIIHOME-3470)
         *          단말쪽 이슈이긴 하나, 일정상 해결이 어려워 홈에서 하드코딩으로 resize 가 잘되는 위치로 변경 (-4px)
         */
        if (KTW.CONSTANT.STB_TYPE.ANDROID) {
            KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT = 1356;
        }
        
        log.printInfo("initData() SAID = " + KTW.SAID);
        log.printInfo("initData() SMARTCARD_ID = " + KTW.SMARTCARD_ID);
        log.printInfo("initData() IS_DCS = " + KTW.CONSTANT.IS_DCS);
        log.printInfo("initData() IS_OTS = " + KTW.CONSTANT.IS_OTS);
        log.printInfo("initData() IS_UHD = " + KTW.CONSTANT.IS_UHD);
        log.printInfo("initData() IS_HDR = " + KTW.CONSTANT.IS_HDR);
        log.printInfo("initData() YOUTUBE_SUPPORT = " + KTW.CONSTANT.YOUTUBE_SUPPORT);
    }
    
    function initKeySet() {
        if (KTW.PLATFORM_EXE === "STB") {
            KTW.KEY_CODE = KTW.KEY.STB;
        }
        
        // 홈포탈에서 사용하는 기본적인 keyset
        KTW.KEY_SET.NORMAL = Object.keys(KTW.KEY_CODE).map(function(k) { return KTW.KEY_CODE[k] });
        // POWER_KEY를 제외한 모든 keyset
        KTW.KEY_SET.ALL = [];
        
        if (KTW.PLATFORM_EXE === "STB") {
        	var key_codes = acba("js:keycodes");
            var length = key_codes.length;
        	for (var i = 0; i < length; i++) {
        		if (key_codes[i].name == "VK_POWER") {
        			continue;
        		}
        		KTW.KEY_SET.ALL.push(key_codes[i].code);
        	}
        }
        else {
        	KTW.KEY_SET.ALL = KTW.KEY_SET.NORMAL.slice();
        }
    }
    
    function initNav() {
        adapterHandler.navAdapter.init();
    }
    
    function initSvc() {
        log.printForced("initSvc()");

        appServiceManager.init();
        stateManager.init();
        messageManager.init();
        adapterHandler.init();
        KTW.managers.service.ReservationManager.init(); // dhlee 2016.12.13 ReservationManager 초기화
        KTW.managers.auth.AuthManager.init();    // dhl22 2016.12.13 AuthManager 초기화
        KTW.managers.service.IframeManager.init();

        KTW.managers.service.AudioIframeManager.init();

        /**
         * [dj.son] HomeShotDataManager init 호출,
         */
        KTW.managers.service.HomeShotDataManager.init();
    }
    
    function startNav() {
        log.printForced("startNav()");
        adapterHandler.navAdapter.start();
        /**
         * [dj.son] 반드시 channel event 를 받고 홈메뉴를 띄우기 때문에, ChannelControl init 과 동시에 promoManager 를 init 함
         */
        promoManager.init();
    }

    function _checkAppReadyOnBooting () {
        log.printForced("_checkAppReadyOnBooting() " + bootingChannelEventNumber);

        if (stateManager.isRunningState()) {
            return;
        }

        /*++bootingChannelEventNumber;

        *//**
         * [dj.son] 키즈 모드일 경우 channel change event 가 1번 오고 일반 모드일 경우 2번 전달됨
         * - 키즈 모드일 경우에는 첫키 로직 수행 여부까지 체크
         *//*
        if (bootingChannelEventNumber > 1) {
            if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                if (appServiceManager.checkFirstKey()) {
                    startActivateHome();
                }
            }
            else {
                startActivateHome();
            }
        }*/

        ++bootingChannelEventNumber;

        if (!bStartActivateHome && bStartSvc) {
            checkBootingChannel();
        }
    }
    
    function startSvc() {
        log.printForced("startSvc()");

        bStartSvc = true;

        // [jh.lee] 시청시간제한이 아니면 정상 시나리오
        //networkManager.init();

        // [jh.lee] 대기모드 진입시 최근시청목록 데이터 캐쉬를 제거해야하기 때문에 리스너 등록
        // 2016.10.10 dhlee 최근 시청 목록 캐시 모듈은 지니에서 별도 제작할 것이므로 여기서는 일단 주석 처리한다.
        // viewHistoryManager.init();

        // [dj.son] 안쓰는 함수 주석처리
        //checkDefaultChannel();

        // [dj.son] 모듈 버전 데이터 초기화는 oc 데이터 다 받은 다음에 해야 하므로 여기서 수행
        moduleManager.init();

        // [dj.son] 키즈 메뉴 존재 여부 체크하기 위해, 여기서 수행
        KTW.managers.service.KidsModeManager.init();
        //layerManager.activateMiniEpg();

        sendHomePortalVersionToCEMS();

        /**
         * [dj.son] 기가지니 STB 에서 애니메이션 제거를 위한 css 파일 import
         */
        if (KTW.CONSTANT.STB_TYPE.ANDROID) {
            $("head").append($("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/RemoveAnimationForGigaGinie.css"
            }));
        }
        
        // 시청시간 제한이나 대기모드 상태라면 home ui를 activate 하지 않고
        // permisson check를 위한 channel tune 처리도 하지 않는다.
        if (stateManager.serviceState !== KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED &&
            stateManager.serviceState !== KTW.CONSTANT.SERVICE_STATE.STANDBY) {

            // [dj.son] sendAppReady 를 먼저 수행한 후 channel tune 을 하면 나중에 옵저버로부터 obs_hide 가 날라오고 All Layer Hide 를 수행하게 되고 HomeMenu Layer 가 hide 된다.
            // 이를 방지하기 위해 channel tune 이후 sendAppReady 수행하도록 하여 obs_hide 가 안날라오도록 한다.
            // 부팅시 channel permission check 를 위해 의도적으로 tune 처리를 하는데, (아래 코드 참조)
            // 이를 이용해 main vbo 에 listener 를 등록, listener 가 실행될때 sendAppReady 를 수행하여 위 이슈를 해결한다. listener 는 일회성이므로 호출 후 바로 해제한다.
            // 단, 부팅시 navigater 에서 channel tune 한 것에 대한 change success evt 가 먼저 날라올 수 있는데, (timing 이슈 있음)
            // 이후 의도적으로 channel tune 한 것에 대한 evt 가 날라오고 이때는 ChannelControl 내부 로직에 의해 listener 가 호출 안된다
            // 따라서 timer 를 생성하여 500ms 마다 10 번 수행하도록 하여 listener 가 호출 되었는지 아닌지 검사 후 sendAppReady 를 수행한다.

            /**
             * [dj.son] 부팅 시나리오 정리
             *
             * 1. 옵저버는 홈포털을 실행한 후 channel tune 을 한고, 이후 이에 대한 channel event 가 홈포탈에 전달된다.
             * 2. 홈포탈은 channel permission check 를 위해 의도적으로 tune 처리를 수행한다.
             *
             * 기존에는 옵저버가 수행한 channel change event 가 AppReady 를 호출하기 전에 올 것이라 가정하고 로직을 수행하였지만,
             * SI 송출 대역폭이 200kbps -> 1Mbps 로 늘어나면서 STB 부팅시간이 줄어들었고, 이때문에 channel change event 가 늦게 오는 현상이 종종 발생하였다.
             * 따라서 channel change event 가 AppReady 이전이든 이후든, 언제든지 올 수 있다고 가정하고 로직을 구성해야 함.
             *
             * 일반 모드일 경우, channel change event 를 2번 받은 후 AppReady 를 수행하도록 로직 수정
             * - 홈포탈이 실행된 후 channel tune 이 2번 일어나는 거이므로, 홈포탈은 channel change event 를 2번 받는다고 생각하고 로직 구성
             * - 실제로 테스트시 channel change event 를 한번만 받는 경우는 못봤음
             * - 다만 만일을 위해 AppReady 호출 여부를 500ms 마다 10번 체크하고, 10번 이후에도 AppReady 가 체크가 안되어 있으면 강제 호출하도록 구성
             *
             * 키즈 모드일 경우, channel change event 2번, 첫키 로직 수행 여부 이후 AppReady 를 수행하도록 로직 수정
             * - 키즈 모듈을 로딩하기 위해서는 첫키 로직이 반드시 필요하므로, 첫키 로직 여부도 동시 체크한 후 AppReady 호출하도록 수정
             * - 또한 일반 모드와 마찬가지로 만일을 위해 AppReady 호출 여부를 500ms 마다 10번 체크 로직 적용
             *
             * ** 일반 모드에서 채널이 block 채널일 경우, select 이벤트는 2번 오는데 seccess 혹은 error 이벤트는 1번만 오는 경우가 있음
             * ** 500ms 체크 룰에 의해 5초 뒤에 홈포탈이 뜨긴 뜨는데, 단말쪽에 확인해봐야 할 이슈라 생각됨
             */
            /*var channel = adapterHandler.navAdapter.getCurrentChannel();
            if (util.isValidVariable(channel) === true) {
                if (adapterHandler.navAdapter.isDataServiceChannel(channel)) {
                    sendAppReady(false);
                }
                else if (KTW.CONSTANT.IS_OTS && adapterHandler.navAdapter.isAudioChannel(channel)) {
                    sendAppReady(true);
                }
                else {
                    if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                        startActiveKidsHome();
                    }
                    else {
                        checkStartActivateHome();
                    }

                    // default channel permission check를 위해서 의도적으로 tune 처리함
                    // [dj.son] [WEBIIIHOME-503] 아무래도 미들웨어쪽 api 는 false 와 undefined 를 구분하는 것 같음. (정확히는 false 만 처리하는 것 같음)
                    // 정확하게 값 추가해서 우상단 채널 osd 가 뜨도록 수정
                    adapterHandler.navAdapter.changeChannel(channel, true, false, false, false);
                }
            }
            else {
                startActivateHome();
            }*/

            /**
             * [dj.son] [2017-06-22] 부팅 시나리오 정리 2
             */
            var navigatorChannel = adapterHandler.extensionAdapter.getRealCurrentChannel();
            if (util.isValidVariable(navigatorChannel) === true) {
                if (bootingChannelEventNumber > 0) {
                    // 네비게이터가 튠한거에 대한 채널 이벤트가 한 번 온 상황이므로 check 채널 & sendready
                    checkBootingChannel();
                }
                else {
                    // 네비게이터가 튠은 했는데, 아직 채널 이벤트가 안온 상황이므로, 채널 이벤트 기다렸다가 수행 -> 타이머
                    checkChannelEvent();
                    // 키즈모드일때, 첫키 시나리오를 채널 event 와 병렬로 처리하기 위한 호출
                    if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                        startActiveKidsHome();
                    }
                }
            }
            else {
                // 아직 네비게이터에서 채널 튠 안한 상황이므로 채널 이벤트 기다렸다가 수행 -> 타이머
                checkChannelEvent();
                // 키즈모드일때, 첫키 시나리오를 채널 event 와 병렬로 처리하기 위한 호출
                if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                    startActiveKidsHome();
                }
            }
        }
        else {
            sendAppReady(false);
        }
    }

    function checkBootingChannel () {
        log.printForced("checkBootingChannel() ");

        var channel = adapterHandler.navAdapter.getCurrentChannel();
        if (util.isValidVariable(channel) === true) {
            if (adapterHandler.navAdapter.isDataServiceChannel(channel)) {
                sendAppReady(false);
            }
            else if (KTW.CONSTANT.IS_OTS && adapterHandler.navAdapter.isAudioChannel(channel)) {
                sendAppReady(true);
            }
            else {
                if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                    startActiveKidsHome();
                }
                else {
                    //checkStartActivateHome();
                    startActivateHome();
                }

                // default channel permission check를 위해서 의도적으로 tune 처리함
                // [dj.son] [WEBIIIHOME-503] 아무래도 미들웨어쪽 api 는 false 와 undefined 를 구분하는 것 같음. (정확히는 false 만 처리하는 것 같음)
                // 정확하게 값 추가해서 우상단 채널 osd 가 뜨도록 수정
                //adapterHandler.navAdapter.changeChannel(channel, true, false, false, false);
            }
        }
        else {
            startActivateHome();
        }
    }

    function checkChannelEvent (checkNum) {
        log.printForced("checkChannelEvent() " + checkNum);

        if (!checkNum) {
            checkNum = 0;
        }

        setTimeout(function () {
            if (!stateManager.isRunningState() && !bStartActivateHome) {
                if (checkNum < 20) {
                    ++checkNum;
                    checkChannelEvent(checkNum);
                }
                else {
                    log.printForced("call startActivateHome by checkStartActivateHome");
                    startActivateHome();
                }
            }
        }, 500);
    }

    function startActiveKidsHome () {
        log.printForced("startActiveKidsHome() ");

        if (appServiceManager.checkFirstKey()) {
            if (!stateManager.isRunningState() && bootingChannelEventNumber > 0) {
                startActivateHome();
            }
            return;
        }

        if (bProcessFirstKey) {
            return;
        }
        bProcessFirstKey = true;

        appServiceManager.processFirstKey(function () {
            if (!stateManager.isRunningState() && bootingChannelEventNumber > 0) {
                startActivateHome();
            }
        });
    }

    function startActivateHome () {
        log.printForced("startActivateHome() " + bStartActivateHome);

        if (bStartActivateHome) {
            return;
        }
        bStartActivateHome = true;

        // 2017.06.08 dhlee
        // 시작채널 설정에서 LCW로 설정해 놓은 경우 부팅 시 채널이 데이터 채널일 수 있음
        var curChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();

        if (!adapterHandler.navAdapter.isDataServiceChannel(curChannel)) {
            /**
             * [dj.son] appState 가 STARTED 이하인 경우 - 메뉴 데이터가 다 받기 전에 NORMAL Layer 를 띄우면
             *          아무것도 하지 않고 return 하는데, 이 때문에 부팅 후 홈메뉴를 못띄우는 side effect 수정
             */
            stateManager.appState = KTW.CONSTANT.APP_STATE.STARTED;
            layerManager.activateHome({isBoot: true, bAutoExit: true, skipRequestShow: true});
        }

        sendAppReady(true);
    }

    function sendHomePortalVersionToCEMS () {
        log.printForced("sendHomePortalVersionToCEMS()");

        /* 2017.09.06 dhlee 버전 정보 규격 통일
        var fullVersion = KTW.UI_VERSION +"_" + KTW.FRAMEWORK_VERSION;

        for (var key in KTW.managers.module.Module.ID) {
            if (key !== KTW.managers.module.Module.ID.MODULE_FRAMEWORK) {
                var module = KTW.managers.module.ModuleDataManager.getModuleData(KTW.managers.module.Module.ID[key]);

                if (module && module.version) {
                    fullVersion += "_" + module.version;
                }
            }
        }
        */
        var fullVersion = "W3_"+KTW.UI_VERSION + "_" + KTW.UI_DATE + "-" + KTW.RELEASE_MODE;

        log.printForced("fullVersion : " + fullVersion);

        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.CEMS_VERSION_HOEMPORTAL, fullVersion);
    }

    function checkBizUser () {
        log.printForced("checkBizUser()");

        caHandler.requestCaMessage(caHandler.TAG.REQ_PRODUCT_INFO, undefined, function (result, data) {
            log.printDbg("checkBizUser() result : " + result);

            if (result && data && data.product_info_data) {
                log.printDbg("checkBizUser() data : " + log.stringify(data));

                /**
                 * [dj.son] 비즈 상품인 경우, localStorage 에 있는 getCustEnv 데이터를 보고 메뉴 구성하도록 수정
                 * - 현재 localStorage 에 데이터가 없으면 기본 메뉴 (채널 가이드 등) 만 노출됨
                 */
                if ((data.product_info_data & caHandler.PRODUCT_TYPE.FULL_PACKAGE_SUBSCRIBER) === caHandler.PRODUCT_TYPE.FULL_PACKAGE_SUBSCRIBER) {
                    log.printDbg("Full Package! Home User");
                }
                else {
                    log.printDbg("Biz User");

                    KTW.CONSTANT.IS_BIZ = true;
                }

                loadData();
            }
            else {
                /**
                 * TODO cas 로부터 메세지 획득에 실패했을 경우에는 어뜩하지?
                 * TODO cas 가 메세지를 아주 늦게 주면 어뜩하지?
                 */
                log.printErr("cas event fail..... ");

                loadData();
            }
        });
    }
    
    /**
     * data loading
     */
    function loadData() {
        ocManager.init();
        ocManager.load();
    }

    function checkMenuData() {
    	// OC 메뉴데이터 attach가 다 되었는지 확인한다.
        // 안되었다면 3초마다 다시 확인한다.
        var result = ocManager.isCompleteInitAttach();
        if (result === false) {
            log.printDbg("unComplete attach menu data " + booting_time);
            booting_time += 3;
            
            if (booting_time >= 270) {
                // 메뉴 데이터 업데이트 실패 시 CEMS report 수행
                KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("VODE-00015");
                showBootingFailPopup();
            }
            else { 
                setTimeout(function() { checkMenuData(); }, 3000);
            }
            return;
        }
        
        log.printForced("olleh tv oc attach complete");

        menuDataManager.initMenuData(startSvc);
    }
    
    function showBootingFailPopup() {
        log.printWarn("showBootingFailPopup()");
        
        sendAppReady(false);

        // TODO 임시로 UI 구성함. 확인 필요부팅 실패 팝업
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: [KTW.ERROR.EX_CODE.BOOT.EX001.title],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                message: [KTW.ERROR.EX_CODE.BOOT.EX001.message[0]],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_32,
                message: [KTW.ERROR.EX_CODE.BOOT.EX001.message[1], KTW.ERROR.EX_CODE.BOOT.EX001.message[2]],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_28,
                message: [KTW.ERROR.EX_CODE.BOOT.EX001.message[3]],
                cssObj: { color: "rgba(255, 255, 255, 0.5)"}
            }],

            arrButton: [{id: "ok", name: "확인"}],

            cbAction: function (id) {
                if (id === "ok") {
                    layerManager.deactivateLayer({id: "boot_fail_popup"});
                    _destroy(true);
                }
            }
        };

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "boot_fail_popup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.SYSTEM_POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData,
                    enableDCA : "false",
                    enableChannelKey : "false",
                    enableMenuKey : "false",
                    enableExitKey : "false",
                    enableBackKey : "false",
                    enableHotKey : "false"
                }
            },
            visible: true,
            cbActivate: function () {
                // menu data 수신 실패시 30초 동안 입력 없으면 자동 리부팅 하도록 함
                setTimeout(function () {
                    layerManager.deactivateLayer({id: "boot_fail_popup"});
                    _destroy(true);
                }, 30 * 1000);
            }
        });
    }
    
    function sendAppReady(show) {
    	
    	var message = {
            "method": messageManager.METHOD.OBS.APP_READY,
            "from": KTW.CONSTANT.APP_ID.HOME,
            "to" : KTW.CONSTANT.APP_ID.OBSERVER
        };
        messageManager.sendMessage(message);
        
        // obs_requestShow
        // 부팅 시점에는 무조건 home UI가 노출되어야 하기 때문에
        // response 여부와 관계없이 무조건 home을 activate 하도록 함
        // 단, 부팅 fail popup이 노출되거나 시청시간 제한 및 대기모드 상태에서
        // 부팅하는 경우에는 request show를 호출하지 않도록 함
        if (show === true) {
            message.method = messageManager.METHOD.OBS.REQUEST_SHOW;
            messageManager.sendMessage(message, function () {
                // [dj.son] sendAppReady 함수 호출전에 activateHome 을 먼저 부르기 때문에, obs_requestShow response 가 오면 다시 activateHome 이 호출되는 이슈가 있다.
                // activateHome -> obs_requestShow -> response -> AppServiceManager 에서 다시 activateHome 호출함
                // activateHome 이 중복 호출되지 않도록 빈 callback 함수를 param 으로 넘겨준다.
            });

            /**
             * [dj.son] [WEBIIIHOME-503] [WEBIIIHOME-2581]
             * - 부팅 프로세스를 변경하면서 생긴 side effect 수정 (부팅시 채널 OSD 안보이는 이슈)
             * - 추가로 AV Black 상태로 홈메뉴가 실행되는 경우 발생
             * - AppReady 를 수행한 이후에 채널 튠을 해서 채널 OSD 가 노출되도록 수정
             * - 이전 로직대로 일반 AV 채널일 경우에만 다시 채널 튠을 하도록 수정
             *
             * [dj.son] [WEBIIIHOME-2622]
             * - 채널 튠시 옵저버 이벤트가 block 되도록 block_event parameter 를 true 로 전달 (kt_olleh_tv_web_application_guide_v1.0.7.pdf setChannel 부분 참조)
             * - 채널 OSD 는 노출이 되어야 하므로, block_osd parameter 를 false 로 전달
             */
            var channel = adapterHandler.navAdapter.getCurrentChannel();
            adapterHandler.navAdapter.changeChannel(channel, true, false, true, false);
        }

        stateManager.appState = KTW.CONSTANT.APP_STATE.STARTED;

        /**
         * [dj.son] OCImageManager init 함수에서는 채널 튠 여부를 bootingChannelEventNumber 값을 보고 판단하는데,
         *          채널 event 가 안오거나 아주 늦게 오면 해당 플래그 값이 세팅이 안될 수 있음.
         *          어떤 상황이든 sendAppReady 를 보내는 시점은 채널이 터진 시점이라고 판단하기 때문에,
         *          여기서 bootingChannelEventNumber 값을 세팅하는 예외처리를 추가함
         */
        bootingChannelEventNumber++;

        /**
         * [dj.son] 네비게이터가 채널 튠한 것에 대한 채널 이벤트가 안 왔을 경우, 오디오 iframe 이 노출 안되는 이슈 수정 (onlySelect 로 채널 튠 한 건 내부 로직에 의해 오디오 아이프레임이 노출 안됨)
         */
        KTW.managers.service.AudioIframeManager.checkBootingChannel();
    }

    function showDebugView() {
        /**
         * [dj.son] LIVE / BMT 상관없이 로그 버전일 경우에만 버전 표시하도록 수정
         */
        if (KTW.LOG_OUTPUT === true) {
            var version_info = "";
            if (KTW.UI_UPDATE_TIME) {
                version_info = "app v" + KTW.UI_VERSION + "_v" + KTW.FRAMEWORK_VERSION + "_" + KTW.UI_DATE + "_" + KTW.UI_UPDATE_TIME + " (" + KTW.RELEASE_MODE + (KTW.LOG_OUTPUT === true ? "-LOG" : "") +")<br>";
            }
            else {
                version_info = "app v" + KTW.UI_VERSION + "_v" + KTW.FRAMEWORK_VERSION + "_" + KTW.UI_DATE + "(" + KTW.RELEASE_MODE + (KTW.LOG_OUTPUT === true ? "-LOG" : "") +")<br>";
            }

            var system_info = KTW.oipf.AdapterHandler.hwAdapter.networkInterface.getIpAddress() + "(" + KTW.SAID + ") with Voiceable " + Voiceable.version;

            // jjh1117 2016.10.13
            // 출력 위치를 조정함.
            var debug_div = util.makeElement({tag: "<div>", attrs: {id: "debug", class: "arrange_frame", css: {visibility: "inherit", "z-index": 9998, left: 0, top: 0}}, parent: $("body")});
            util.makeElement({tag: "<span>", attrs: {id: "ip", class: "f_19", css: {position: "absolute", left: 200, top: 50, color: "rgb(255, 0, 0)", "white-break": "normal"}}, text: version_info + system_info, parent: debug_div});

            var debug_vod_div = util.makeElement({tag: "<div>", attrs: {id: "debug_vod", class: "arrange_frame", css: {visibility: "inherit", "z-index": 9999, "text-align": "center", left: 0, top: 0}}, parent: $("body")});
            util.makeElement({tag: "<span>", attrs: {id: "debug_vod_span", class: "f_19", css: {color: "rgb(255, 255, 0)", "white-break": "normal"}}, parent: debug_vod_div});
        }
        /*
        if (KTW.LOG_OUTPUT === true) {
            var version_info = "app v" + KTW.UI_VERSION + "_" + KTW.UI_DATE + "(" + KTW.RELEASE_MODE + " - " + (KTW.LOG_OUTPUT === true ? "Log" : "") +")<br>";
            var system_info = KTW.oipf.AdapterHandler.hwAdapter.networkInterface.getIpAddress() + "(" + KTW.SAID + ")";

            // jjh1117 2016.10.13
            // 출력 위치를 조정함.
            var debug_div = util.makeElement({tag: "<div>", attrs: {id: "debug", class: "arrange_frame", css: {visibility: "inherit", "z-index": 9998}}, parent: $("body")});
            util.makeElement({tag: "<span>", attrs: {id: "ip", class: "f_19", css: {position: "absolute", left: 200, top: 50, color: "rgb(255, 0, 0)", "white-break": "normal"}}, text: version_info + system_info, parent: debug_div});

            var debug_vod_div = util.makeElement({tag: "<div>", attrs: {id: "debug_vod", class: "arrange_frame", css: {visibility: "inherit", "z-index": 9999, "text-align": "center"}}, parent: $("body")});
            util.makeElement({tag: "<span>", attrs: {id: "debug_vod_span", class: "f_19", css: {color: "rgb(255, 255, 0)", "white-break": "normal"}}, parent: debug_vod_div});
        }
        */
    }
    
    return {
        ready: function() {
            log.printForced("ready()");
            
            _init();
            _start();
        },
        load: function() {
            log.printInfo("load()");
        },
        beforeunload: function() {
            log.printInfo("beforeunload()");
        },
        unload: function() {
            log.printInfo("unload()");
            
            _destroy();
        },
        resume: _resume,
        pause: _pause,
        destroy: _destroy,

        checkAppReadyOnBooting: _checkAppReadyOnBooting,
        isReceiveChannelEventOnBooting: function () {
            return bootingChannelEventNumber > 0 ? true : false;
        },
        
        // for test
        destroyApp: function() {
            if (KTW.LOG_OUTPUT === true) {
                KTW.oipf.AdapterHandler.serviceAdapter.self.destroyApplication();
            }
        },
        getCh: function() {
            if (KTW.LOG_OUTPUT === true) {
                return KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
            }
        },
        stopCh: function() {
            if (KTW.LOG_OUTPUT === true) {
                KTW.oipf.AdapterHandler.navAdapter.stopChannel();
            }
        },
        changeCh: function(ch, only, forced) {
            if (KTW.LOG_OUTPUT === true) {
                KTW.oipf.AdapterHandler.navAdapter.changeChannel(ch, only, forced);
            }
        },
        getPIPCC: function() {
            if (KTW.LOG_OUTPUT === true) {
                return KTW.oipf.AdapterHandler.navAdapter.getChannelControl("pip", true);
            }
        }
    };
}());
