"use strict";

/**
 * control CAS client
 * 
 * ca request를 해야 되는 경우 CaHandler#requestCaMessage()를 이용해서 호출한다.
 * 
 */
KTW.ca.CaHandler = (function () {
    
    var CAS_ERROR_POPUP_ID = "cas_error_popup";
    var CAS_UK_ERROR_POPUP_ID = "cas_unknown_error_popup";
    
    var CA_MSG = {
        TRANSACTION_ID : 0x3001,
        NOTIFY_TRANSACION_ID : -1,
        TAG : {
            RES_SYSTEM_INFO: 0x01,
            RES_CHANNEL_STATUS : 0x03,
            RES_PIN_QUERY : 0x0B,
            RES_CHANGE_AGE_LIMIT : 0x0D,
            RES_PIN_CHANGE : 0x0F,
            RES_PARENTAL_RATING_LOCK : 0x11,
            NOTIFY_OSD : 0x18,
            NOTIFY_ALTI_CAS_STATUS : 0x19,
            RES_PRODUCT_INFO : 0x25,
            RES_STOP_PREVIEW : 0x29
        },
        ATTRIBUTE : {
            PROCESSING_STATUS : 0,
            CHANNEL_STATUS : 1,
            CAS_STATUS : 3,
            CHANNEL_ID : 15,
            AGE_LIMIT : 24,
            PROGRAM_STRUCTURE : 42,
            STRING : 43,
            ORG_PIN : 51,
            NEW_PIN : 52,
            MASTER_PIN : 53,
            PARENTAL_RATING_LOCK_STATUS : 54,
            USER_ID : 55,
            PRODUCT_INFO_DATA : 62,
            CHANNEL_ATTRIBUTE_TYPE : 65,
            CHANNEL_PREVIEW_TIME : 66,
            COOL_TIME : 67,
            PREVIEW_TIME : 68,
            SERVICE_NUMBER : 69
        },
        PROCESSING_STATUS : {
            OK : 0, // 처리 성공
            UNKNOWN_ERROR : 1, // 알 수 없는 이유의 에러
            TIME_OUT : 2, // CAS Client 응답 없음
            NOT_INITIALIZED : 3, // CAS Client 발급전이거나 폐기되었음
            NOT_EXIST : 4, // CAS Client가 존재하지 않음
            SERVER_NOT_FOUND : 5, // CAS 서버와의 통신 에러(망 접속 에러)
            INVALID_PARAMETER : 6, // 하나 이상의 파라미터 이상
            MISSING_PARAMETER : 7, //하나 이상의 필수 파라미터가 빠져있음
            UNKNOWN_ID : 8, // CAS Client가 식별할 수 없는 채널, PPV, VOD, ISU ID
            PURCHASE_DENIED : 9, // 구매 불가
            NO_CREDIT : 10, // credit이 부족하여 구매 불가
            CANCEL_IS_TOO_LATE : 11, //일정시간 시청하였으므로 취소 불가
            INVALID_PIN : 12, // PIN이 맞지 않음
            INVALID_USER_ID : 13, // User ID가 맞지 않음
            VOD_ERROR : 0x3000
        },
        CAS_STATUS : {
            NOT_INITIALIZED : 0, // 미개통
            IS_PAIRING : 1, // CAS와 STB간의 pairing 완료
            NORMAL : 2, // 개통 완료된 정상적인 상태
            SUSPENDED : 3, // 일시 정지
            EXPIRED : 4, // 권한 만료
            ERROR : 5, // Unknown error
            VOD_ERROR : 10, // VOD error
            SYSTEM_ERROR : 11, // System error
            VGS_NO_MATCHING_VECM_FOUND : 23, // CEMS report
            VGS_NO_MATCHING_VEMM_FOUND : 24, // CEMS report
            VGS_FIRST_REG_ERROR : 10, // CEMS report
            VGS_SECOND_REG_ERROR : 11, // CEMS report
            VGS_EMMG_COMM_ERROR : 17, // CEMS report
            VGS_NO_VGS_SERVER : 18, // CEMS report
            VGS_ERROR_IN_VGS_SERVER : 19, // CEMS report
            KT_CAS_ERROR_MESSAGE_01 : 4865, // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
            KT_CAS_ERROR_MESSAGE_02 : 4866, // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
            KT_CAS_ERROR_MESSAGE_03 : 4867, // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
            KT_CAS_ERROR_MESSAGE_04 : 4868, // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
            KT_CAS_ERROR_MESSAGE_05 : 4869, // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
            KT_CAS_ERROR_MESSAGE_06 : 4870, // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
            KT_CAS_ERROR_MESSAGE_07 : 4871 // 0x1301 ~ 0x1307 의 에러는 CEMS 로 전송
        },
        CHANNEL_STATUS : {
            NOT_SCRAMBLED : 0x00000001, // clear 채널
            CAN_DESCRAMBLE : 0x00000002, // 시청 가능 채널
            BLACK_OUT : 0x00000004, // 현재 지역에서 Black out
            SPOT_ENABLE : 0x00000008, // 현재 지역에서 spot
            NOT_SUBSCRIBED : 0x00000010, // 미가입 채널
            PURCHASED_VOD : 0x00000020, // VOD 채널이고 현재 시청 가능
            NOT_PURCHASED_VOD : 0x00000040,//  VOD 채널이고 현재 구매 가능
            PURCHASED_PPV : 0x00000800, // PPV 채널이고 현재 시청 가능
            NOT_PURCHASED_PPV : 0x00001000, //PPV 채널이고 현재 구매 가능
            BONUS : 0x00002000, //보너스 채널
            PARENTAL_RATING_LIMIT : 0x00004000, // 시청연령 제한 채널
            PREVIEW : 0x00008000 // 채널 미리보기
        },
        PRODUCT_INFO : {
            B2B : 0x2000,
            FULL_PACKAGE : 0x1000
        }
    };
    
    // 2분 rule 시간
    var TIME_2MINUTES = 2 * 60 * 1000;
    
    var log = KTW.utils.Log;
    
    var casAdapter = null;
    
    var application_id;
    
    var listeners = [];
    
    var ca_callback = null;
    var ca_req_tag = -1;
    
    // 시청연령제한용 2분 rule 변수
    // 시청연령제한 해제 시간
    var parental_rating_unlock_time = 0;
    
    function _init() {
        casAdapter = KTW.oipf.AdapterHandler.casAdapter;
        application_id = casAdapter.setCaEventListener(onCaEventArrived);
        
        log.printDbg("_init() application_id = " + application_id);
        
        // 부팅 시 CAS status check
        _requestCaMessage(KTW.ca.CaHandler.TAG.REQ_SYSTEM_INFO);
    }
    
    function _addCaEventListener(listener) {
        listeners.push(listener);
        
        log.printDbg("_addCaEventListener() listener count = " + listeners.length);
    }
    
    function _removeCaEventListener(listener) {
        var length = listeners.length;
        for (var i = 0; i < length; i++) {
            if (listeners[i] === listener) {
                listeners.splice(i, 1);
                break;
            }
        }
        
        log.printDbg("_removeCaEventListener() listener count = " + listeners.length);
    }
    
    /**
     * request ca message
     * 
     * @params tag - request tag @see { KTW.ca.CaHandler.TAG }
     * @params data - ca parameters (JSON object)
     * @params callback
     */
    function _requestCaMessage(tag, data, callback) {
        var msg = getMessage(tag, data);
        
        if (callback !== null && callback !== undefined) {
        	ca_req_tag = tag;
            ca_callback = callback;
        }
        
        casAdapter.sendCaMsg(application_id, CA_MSG.TRANSACTION_ID, tag, msg);
    }
    
    function getMessage(tag, data) {
        
        var message;
        
        if (tag === KTW.ca.CaHandler.TAG.REQ_SYSTEM_INFO ||
                tag === KTW.ca.CaHandler.TAG.REQ_PRODUCT_INFO) {
            message = [];
            message[0] = 0x00;
            message[1] = 0x00;
        }
        else if (tag === KTW.ca.CaHandler.TAG.REQ_STOP_PREVIEW) {
            message = [];
            message[0] = 0x00;
            message[1] = 0x07;
            message[2] = CA_MSG.ATTRIBUTE.SERVICE_NUMBER;
            message[3] = 0x00;
            message[4] = 0x04;

            var sn = intToByte(data.service_number);
            message.push.apply(message, sn);
        }
        else {
            message = [ 0x00, , , 0x00, 0x01, , , 0x00, 0x08];
            
            var pin;
            var length;
            var index = message.length;
            
            switch(tag) {
                case KTW.ca.CaHandler.TAG.REQ_PIN_QUERY :
                    message[1] = 0x0F;
                    message[2] = CA_MSG.ATTRIBUTE.USER_ID;
                    message[5] = 0x00; // master user
                    
                    // master pin = old_pin
                    message[6] = CA_MSG.ATTRIBUTE.ORG_PIN;
                    pin = convertToByte(data.old_pin, 8);
                    message.push.apply(message, pin);
                    break;
                case KTW.ca.CaHandler.TAG.REQ_CHANGE_AGE_LIMIT :
                    
                    var age = data.age_limit;
                    if (age === 0) {
                        age = 0xFF;
                    }
                    
                    message[1] = 0x0F;
                    message[2] = CA_MSG.ATTRIBUTE.AGE_LIMIT;
                    message[5] = (age & 0xFF); // age
                    
                    message[6] = CA_MSG.ATTRIBUTE.ORG_PIN;
                    pin = convertToByte(data.pin, 8);
                    message.push.apply(message, pin);
                    break;
                case KTW.ca.CaHandler.TAG.REQ_PIN_CHANGE :
                    message[1] = 0x25;
                    message[2] = CA_MSG.ATTRIBUTE.USER_ID;
                    message[5] = 0x00; // master user
                    
                    // master pin = old_pin
                    message[6] = CA_MSG.ATTRIBUTE.MASTER_PIN;
                    pin = convertToByte(data.old_pin, 8);
                    length = pin.length;
                    message.push.apply(message, pin);
                    index += length;

                    // org_pin = old_pin
                    message[index++] = CA_MSG.ATTRIBUTE.ORG_PIN;
                    message[index++] = 0x00;
                    message[index++] = 0x08;
                    message.push.apply(message, pin);
                    index += length;

                    // new_pin
                    message[index++] = CA_MSG.ATTRIBUTE.NEW_PIN;
                    message[index++] = 0x00;
                    message[index++] = 0x08;
                    pin = convertToByte(data.new_pin, 8);
                    message.push.apply(message, pin);
                    break;
                case KTW.ca.CaHandler.TAG.REQ_PARENTAL_RATING_LOCK :
                    message[1] = 0x0F;
                    message[2] = CA_MSG.ATTRIBUTE.PARENTAL_RATING_LOCK_STATUS;
                    message[5] = data.lock === true ? 0x00 : 0x01; // lock
                    
                    message[6] = CA_MSG.ATTRIBUTE.ORG_PIN;
                    pin = convertToByte(data.pin, 8);
                    message.push.apply(message, pin);
                    break;
            }
        }
        
        return message;
    }
    
    function onCaEventArrived(/*long*/ transaction, /*int*/ tag, /*byte[]*/ message) {
        
        var data, processing_status;
        var transaction_id = transaction[1];
        
        log.printDbg("onCaEventArrived() transaction_id = " + transaction_id);
        log.printDbg("onCaEventArrived() tag = " + getTagString(tag));
        
        data = parseCaMessage(message);
        
        if (transaction_id === CA_MSG.TRANSACTION_ID) {
            switch (tag) {
                case CA_MSG.TAG.RES_SYSTEM_INFO :
                    if (data !== undefined) {
                        processing_status = data.processing_status;
                        if (isValidProcessingStatus(processing_status) === true) {
                            var cas_status = data.cas_status;
                            log.printDbg("onCaEventArrived#RES_SYSTEM_INFO : cas_status = " + cas_status);
                            checkCasStatus(cas_status);
                        }
                    }
                    break;
                case CA_MSG.TAG.RES_PIN_QUERY :
                case CA_MSG.TAG.RES_CHANGE_AGE_LIMIT :
                case CA_MSG.TAG.RES_PIN_CHANGE :
                case CA_MSG.TAG.RES_PARENTAL_RATING_LOCK :
                    fireCaCallback(tag, data);
                    break;
                case CA_MSG.TAG.RES_PRODUCT_INFO :
                    var product_info_data;
                    if (data !== undefined) {
                        product_info_data = data.product_info_data;
                        if (product_info_data) {
                            log.printDbg("onCaEventArrived#RES_PRODUCT_INFO : product_info_data = " + product_info_data);
                        }
                    }
                    fireCaCallback(tag, data);
                    break;
            }
        }
        else if (transaction_id === CA_MSG.NOTIFY_TRANSACION_ID) {
            switch (tag) {
                case CA_MSG.TAG.RES_CHANNEL_STATUS :
                    receiveSvcStatus(data);
                    break;
                case CA_MSG.TAG.NOTIFY_ALTI_CAS_STATUS:
                    if (data !== undefined) {
                        processing_status = data.processing_status;
                        if (isValidProcessingStatus(processing_status) === true) {
                            var cas_status = data.cas_status;
                            log.printDbg("onCaEventArrived#NOTIFY_ALTI_CAS_STATUS : cas_status = " + cas_status);
                            checkCasStatus(cas_status);
                        }
                        else {
                            if (processing_status !== undefined && processing_status !== null) {
                                if (processing_status !== CA_MSG.PROCESSING_STATUS.OK) {
                                    
                                    // processing status 가 OK가 아닌 경우
                                    // 기본적으로 SYSTEM_ERROR 라고 간주하여 처리
                                    var error_status = CA_MSG.CAS_STATUS.SYSTEM_ERROR;
                                    var error_code = Number(processing_status).toString(16).toUpperCase();
                                    
                                    // 단, processing status 값이 VOD_ERROR와 동일하다면
                                    // VOD error 상태로 처리함
                                    if ((processing_status & CA_MSG.PROCESSING_STATUS.VOD_ERROR) 
                                        === CA_MSG.PROCESSING_STATUS.VOD_ERROR) {
                                        error_status = CA_MSG.CAS_STATUS.VOD_ERROR;
                                    }
                                    
                                    checkCasStatus(error_status, error_code);
                                }
                            }
                        }

                        // 2016.09.05 dhlee 2.0 R6 CEMS report 처리 부분 추가
                        if (data.cas_status) {
                            checkCemsError(data.cas_status);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * R6 CEMS 리포트
     */
    function checkCemsError(cas_status) {
        switch(cas_status) {
            case CA_MSG.CAS_STATUS.VGS_NO_MATCHING_VECM_FOUND :
            case CA_MSG.CAS_STATUS.VGS_NO_MATCHING_VEMM_FOUND :
            case CA_MSG.CAS_STATUS.VGS_FIRST_REG_ERROR :
            case CA_MSG.CAS_STATUS.VGS_SECOND_REG_ERROR :
            case CA_MSG.CAS_STATUS.VGS_EMMG_COMM_ERROR :
            case CA_MSG.CAS_STATUS.VGS_NO_VGS_SERVER :
            case CA_MSG.CAS_STATUS.VGS_ERROR_IN_VGS_SERVER :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_01 :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_02 :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_03 :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_04 :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_05 :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_06 :
            case CA_MSG.CAS_STATUS.KT_CAS_ERROR_MESSAGE_07 :
                KTW.oipf.AdapterHandler.extensionAdapter.notifySNMPError("0x" + cas_status.toString(16));
                break;
        }
    }

    function isValidProcessingStatus(processing_status) {
        log.printDbg("isValidProcessingStatus() processing_status = " + processing_status);
        
        if (processing_status !== undefined && processing_status !== null) {
            if (processing_status === CA_MSG.PROCESSING_STATUS.OK) {
                return true;
            }
        }
        return false;
    }
    
    function fireCaCallback(tag, data) {
        var success = false;
        if (data !== undefined) {
            var processing_status = data.processing_status;
            success = isValidProcessingStatus(processing_status);
        }
        
        if (ca_callback !== null && ca_req_tag !== -1) {
        	// ca_callback 에 대한 response tag를 확인하고 처리한다.
        	if (tag !== (ca_req_tag + 1)) {
        		return;
        	}
        		
            // callback parameter : true/false
            try {
                ca_callback(success, data);
            }
            catch(e) {
                log.printErr("fireCaCallback occured error");
                log.printExec(e);
            }
            ca_req_tag = -1;
            ca_callback = null;
        }
    }
    
    function receiveSvcStatus(data) {
        if (data !== undefined) {
            var processing_status = data.processing_status;
            
            if (isValidProcessingStatus(processing_status) === true) {
                var channel_id = data.channel_id;
                var channel_status = data.channel_status;
                log.printDbg("receiveSvcStatus() channel_id = " + channel_id);
                log.printDbg("receiveSvcStatus() channel_status = " + channel_status);
                
                var preview_data;
                if (channel_status === CA_MSG.CHANNEL_STATUS.PREVIEW) {
                    preview_data = {
                        "ch_attr_type" : data.channel_attr_type,
                        "ch_preview_time" : data.channel_preview_time,
                        "cool_time" : data.cool_time,
                        "preview_time" : data.preview_time,
                        "service_number" : data.service_number
                    };
                    log.printDbg("receiveSvcStatus() channel attribute type = " + data.channel_attr_type);
                    log.printDbg("receiveSvcStatus() channel preview time = " + data.channel_preview_time);
                    log.printDbg("receiveSvcStatus() cool time = " + data.cool_time);
                    log.printDbg("receiveSvcStatus() preview time = " + data.preview_time);
                    log.printDbg("receiveSvcStatus() service number = " + data.service_number);
                }
                
                var copy = listeners.slice();
                var length = copy.length;
                for (var i = 0; i < length; i++) {
                    log.printDbg("fireSvcStatus listener[" + i + "]");
                    
                    try {
                        copy[i](channel_id, channel_status, preview_data);
                    }
                    catch(e) {
                        log.printErr("fireSvcStatus occured error");
                        log.printExec(e);
                        
                        _removeCaEventListener(copy[i]);
                    }
                }
            }
        }
    }
    
    function checkCasStatus(status, err_code) {
        if (status === undefined || status === null) {
            return;
        }
        
        // NORMAL 상태가 되면 기존에 떠 있던 모든 popup을 제거한다.
        if (status === CA_MSG.CAS_STATUS.NORMAL) {
            hidePopup();
            return;
        }
        
        showPopup(status, err_code);
    }
    
    function showPopup(status, err_code) {
        var message = null;


        message = getStatusMessage(status, err_code);
        log.printDbg("showPopup() message = " + message);
        
        if (message === null) {
            return;
        }

        var popupType = KTW.ui.Layer.PRIORITY.SYSTEM_POPUP;
        var title = "오류";
        message = message.split("\n");
        var layer_id = CAS_ERROR_POPUP_ID;
        // CAS_STATUS error 인 경우 버튼 없는 popup을 띄우고 사용자가
        // 의도적으로 닫지 못하도록 한다.
        var type = "";
        if (status === CA_MSG.CAS_STATUS.ERROR) {
            // 현재 노출되어 있는 모든 cas error popup을 hide
            hidePopup();
            layer_id = CAS_UK_ERROR_POPUP_ID;
            type = "no_btn";
        }
        else {
            // cas error popup만 hide.
            // 그러므로 CAS_STATUS error popup과 중첩해서 노출될 수 있음.
            hidePopup(CAS_ERROR_POPUP_ID);
            type = "confirm";
        }
        
        enableMouseControl(false);

        KTW.utils.util.showCasErrorPopup(popupType , layer_id , message , title , _popupCallbackFunc , type , undefined , undefined , undefined , undefined , undefined , undefined ,undefined , "true");

    }

    function _popupCallbackFunc(buttonId) {
        log.printDbg("_popupCallbackFunc() buttonId = " + buttonId);
        if(buttonId === KTW.KEY_CODE.BACK) {
            return true;
        }else if(buttonId === KTW.KEY_CODE.EXIT) {
            return true;
        }else {
            return false;
        }
    }
    
    function hidePopup(id) {
        
        enableMouseControl(true);
        
        if (id) {
            KTW.ui.LayerManager.deactivateLayer({id:id});
            return;
        }
        
        KTW.ui.LayerManager.deactivateLayer({id:CAS_ERROR_POPUP_ID});
        KTW.ui.LayerManager.deactivateLayer({id:CAS_UK_ERROR_POPUP_ID});
    }
    
    function enableMouseControl(enable) {
        // DATA 상태 중 full browser 상태일 경우 mouse disable 처리함
        if (KTW.managers.service.StateManager.state === KTW.CONSTANT.SERVICE_STATE.DATA) {
            if (KTW.oipf.AdapterHandler.extensionAdapter.getOSDState() === 1) {
                if (enable === true) {
                    KTW.oipf.AdapterHandler.hwAdapter.mouseControl.restoreMouseControl();
                }
                else {
                    KTW.oipf.AdapterHandler.hwAdapter.mouseControl.disableMouseControl();
                }
            }
        }
    }
    
    function parseCaMessage(/*byte[]*/ message) {
        
        var result;
        
        var attr_name, data;
        var index = 0;
        var attr_length = 0;
        var data_length = message[index] < 0 ? message[index] + 256 : message[index];
        index++;
        data_length <<= 8;
        data_length += message[index++];
        log.printDbg("parseCaMessage() data_length = " + data_length);
        log.printDbg("parseCaMessage() index = " + index);
        
        while ((index - 1) < data_length) {
            try {
                attr_name = getAttributeName(message[index++]);
                
                attr_length = 0;
                attr_length = message[index] < 0 ? message[index] + 256 : message[index];
                index++;
                attr_length <<= 8;
                attr_length += message[index++];
                
                if (attr_name === undefined) {
                    index += attr_length;
                    continue;
                }

                log.printDbg("parseCaMessage() ===============================");
                log.printDbg("parseCaMessage() attr_name = " + attr_name);
                log.printDbg("parseCaMessage() attr_length = " + attr_length);
                
                data = [];
                for (var i = 0; i < attr_length; i++) {
                    data[i] = message[index++];
                }
                log.printDbg("parseCaMessage() data = " + data.toString());
                
                if (result === undefined) {
                    result = {};
                }
                
                result[attr_name] = convertByteArray(data, attr_length);
                log.printDbg("parseCaMessage() ===============================");
            }
            catch(e) {
                log.printErr("parseCaMessage() failed to parse ca message");
                log.printExec(e);
            }
        }
        
        log.printDbg("parseCaMessage() result = " + JSON.stringify(result));
        
        return result;
    }
    
    function convertToByte(/*String */value, size) {
        var result = [];

        var length = value.length;
        var index = (length < size) ? (size - length) : 0;

        for (var i = 0; i < length; i++) {
            if (i > size) break;
            
            result[index + i] = value.charCodeAt(i); //cast to byte
        }

        return result;
    }
    
    function convertByteArray(/* Byte[] */ value, size) {
        var result = 0;

        for (var i = 0; i < size; i++) {
            result += (value[i] < 0 ? value[i] + 256 : value[i]);
            if (i < size - 1) {
                result <<= 8;
            }
        }

        return result;
    }
    
    function intToByte(value) {
        var result = [];
        for (var i = 0; i < 4; i++) {
            var offset = (4 - 1 - i) * 8;
            result[i] = (value >>> offset) & 0xFF;
        }
        return result;
    }
    
    function getAttributeName(/*byte*/ data) {
        var attr_str;
        
        switch (data) {
            case CA_MSG.ATTRIBUTE.PROCESSING_STATUS :
                attr_str = "processing_status";
                break;
            case CA_MSG.ATTRIBUTE.CHANNEL_STATUS :
                attr_str = "channel_status";
                break;
            case CA_MSG.ATTRIBUTE.CAS_STATUS :
                attr_str = "cas_status";
                break;
            case CA_MSG.ATTRIBUTE.CHANNEL_ID :
                attr_str = "channel_id";
                break;
            case CA_MSG.ATTRIBUTE.CHANNEL_ATTRIBUTE_TYPE :
                attr_str = "channel_attr_type";
                break;
            case CA_MSG.ATTRIBUTE.CHANNEL_PREVIEW_TIME :
                attr_str = "channel_preview_time";
                break;
            case CA_MSG.ATTRIBUTE.COOL_TIME:
                attr_str = "cool_time";
                break;
            case CA_MSG.ATTRIBUTE.PREVIEW_TIME :
                attr_str = "preview_time";
                break;
            case CA_MSG.ATTRIBUTE.SERVICE_NUMBER :
                attr_str = "service_number";
                break;
            case CA_MSG.ATTRIBUTE.PRODUCT_INFO_DATA :
                attr_str = "product_info_data";
                break;
            default:
                break;
        }
        
        return attr_str;
    }
    
    function getTagString(tag) {
        var tag_str;
        
        var req_tag = KTW.ca.CaHandler.TAG;
        var res_tag = CA_MSG.TAG;
        
        switch(tag) {
            case req_tag.REQ_SYSTEM_INFO :
                tag_str = "SYSTEM_INFO_REQUEST";
                break;
            case res_tag.RES_SYSTEM_INFO :
                tag_str = "SYSTEM_INFO_RESPONSE";
                break;
            case res_tag.RES_CHANNEL_STATUS :
                tag_str = "CHANNEL_STATUS";
                break;
            case req_tag.REQ_PIN_QUERY :
                tag_str = "PIN_QUERY_REQUEST";
                break;
            case res_tag.RES_PIN_QUERY :
                tag_str = "PIN_QUERY_RESPONSE";
                break;
            case req_tag.REQ_CHANGE_AGE_LIMIT :
                tag_str = "CHANGE_AGE_LIMIT_REQUEST";
                break;
            case res_tag.RES_CHANGE_AGE_LIMIT :
                tag_str = "CHANGE_AGE_LIMIT_RESPONSE";
                break;
            case req_tag.REQ_PIN_CHANGE :
                tag_str = "PIN_CHANGE_REQUEST";
                break;
            case res_tag.RES_PIN_CHANGE :
                tag_str = "PIN_CHANGE_RESPONSE";
                break;
            case req_tag.REQ_PARENTAL_RATING_LOCK :
                tag_str = "PARENTAL_RATING_LOCK_REQUEST";
                break;
            case res_tag.RES_PARENTAL_RATING_LOCK :
                tag_str = "PARENTAL_RATING_LOCK_RESPONSE";
                break;
            case res_tag.RES_STOP_PREVIEW :
                tag_str = "STOP_PREVIEW_RESPONSE";
                break;
            case res_tag.NOTIFY_OSD :
                tag_str = "OSD_NOTIFY";
                break;
            case res_tag.NOTIFY_ALTI_CAS_STATUS :
                tag_str = "ALTI_CAS_STATUS_NOTIFY";
                break;
            case req_tag.REQ_PRODUCT_INFO :
                tag_str = "PRODUCT_INFO_REQUEST";
                break;
            case res_tag.RES_PRODUCT_INFO :
                tag_str = "PRODUCT_INFO_RESPONSE";
                break;
        }
        
        return tag_str;
    }
    
    function getStatusMessage(status, err_code) {
        var message = null;
        
        switch(status) {
            case CA_MSG.CAS_STATUS.NOT_INITIALIZED :
                message = "CAS 시스템이 미개통 상태 입니다";
                break;
            case CA_MSG.CAS_STATUS.IS_PAIRING :
                message = "CAS 시스템에 이상이 있습니다";
                break;
            case CA_MSG.CAS_STATUS.SUSPENDED :
                message = "CAS 시스템이 일시 정지 상태입니다";
                break;
            case CA_MSG.CAS_STATUS.EXPIRED :
                message = "CAS 시스템이 권한 만료 상태입니다";
                break;
            case CA_MSG.CAS_STATUS.ERROR :
                message = "CAS 시스템에 이상이 있습니다";
                break;
            case CA_MSG.CAS_STATUS.VOD_ERROR :
                message = "VOD를 시청하실 수 없습니다(KTCAE" + err_code + ")";
                break;
            case CA_MSG.CAS_STATUS.SYSTEM_ERROR :
                message = "CAS 시스템 오류가 발생하였습니다\n(KTCAE" + err_code + ")";
                break;
        }
        
        if (message !== null) {
            message += "\n\n문의) 국번없이 100번\nolleh tv사용에 불편을 드려서 죄송합니다";
        }
        
        return message;
    }
    
    function _setParentalRatingUnlockTime() {
        parental_rating_unlock_time = (new Date()).getTime();
        
        log.printInfo("_setParentalRatingUnlockTime() parental_rating_unlock_time = " + parental_rating_unlock_time);
    }
    
    /**
     * 2분 rule 체크
     */
    function underParentalRatingUnlock() {
        var rule_time = parental_rating_unlock_time;
        if (rule_time > 0) {
            var cur_time = (new Date()).getTime();
            var diff_time = cur_time - rule_time;
            log.printDbg("underParentalRatingUnlock() cur_time = " + cur_time);
            log.printDbg("underParentalRatingUnlock() diff_time = " + diff_time);
            
            return diff_time <= TIME_2MINUTES;
        }
        
        return false;
    }
    
    function resetParentalRatingUnlockTime(forced) {
        log.printInfo("resetParentalRatingUnlockTime(" + forced + ")");
        log.printInfo("resetParentalRatingUnlockTime() parental_rating_unlock_time = " + parental_rating_unlock_time);
        if (forced === true || parental_rating_unlock_time > 0) {
            _requestCaMessage(KTW.ca.CaHandler.TAG.REQ_PARENTAL_RATING_LOCK,
                    { pin : "0", lock : true });
            
            parental_rating_unlock_time = 0;
        }
    }
    
    function _checkParentalRatingLock(reset) {
        
        if (reset === true) {
            resetParentalRatingUnlockTime(reset);
            return;
        }
        
        var unlock = underParentalRatingUnlock();
        if (unlock === true) {
            _setParentalRatingUnlockTime();
        }
        else {
            resetParentalRatingUnlockTime();
        }
    }
    
    return {
        init: _init,
        
        requestCaMessage: _requestCaMessage,
        
        addCaEventListener: _addCaEventListener,
        removeCaEventListener: _removeCaEventListener,
        
        setParentalRatingUnlockTime: _setParentalRatingUnlockTime,
        checkParentalRatingLock: _checkParentalRatingLock
        
        // 테스트 code
        //checkCasStatus: checkCasStatus,
        //testEvent: onCaEventArrived,
    }
}());

//define constant.
Object.defineProperty(KTW.ca.CaHandler, "TAG", {
    value:  {
        /**
         * cas status 확인용
         */
        REQ_SYSTEM_INFO : 0x00,
        /**
         * 시청연령제한 설정
         * @param age_limit - 연령제한 값 (number type)
         * @param pin - pin number(string type)
         */
        REQ_CHANGE_AGE_LIMIT : 0x0C,
        /**
         * 비밀번호 인증
         * @param old_pin 이전 비밀번호(string type)
         * @param new_pin 변경할 비밀번호(string type)
         */
        REQ_PIN_QUERY : 0x0A,
        /**
         * 비밀번호 변경
         * @param old_pin 이전 비밀번호(string type)
         * @param new_pin 변경할 비밀번호(string type)
         */
        REQ_PIN_CHANGE : 0x0E,
        /**
         * 연령제한 lock/unlock
         * @param pin 비밀번호(string type)
         * @param lock 해제했다면 false, 다시 lock 상태가 되면 true
         */
        REQ_PARENTAL_RATING_LOCK : 0x10,
        /**
         * 상품 정보 확인용
         */
        REQ_PRODUCT_INFO : 0x24,
        /**
         * 채널미리보기 stop
         * @param service_number - 미리보기 채널
         */
        REQ_STOP_PREVIEW : 0x28
   },
    writable: false,
    configurable: false
});

//define constant.
Object.defineProperty(KTW.ca.CaHandler, "CHANNEL_STATUS", {
    value:  {
        NOT_SUBSCRIBED : 0x00000010,
        PARENTAL_RATING_LIMIT: 0x00004000,
        CAN_DESCRAMBLE : 0x00000002,
        NOT_SCRAMBLED : 0x00000001,
        PREVIEW : 0x00008000
   },
    writable: false,
    configurable: false
});

//define constant.
Object.defineProperty(KTW.ca.CaHandler, "PRODUCT_TYPE", {
    value:  {
        PPV_ISU_SVC : 0x8000,
        VOD_SVC : 0x4000,
        B2B_SUBSCRIBER : 0x2000,
        FULL_PACKAGE_SUBSCRIBER : 0x1000
   },
    writable: false,
    configurable: false
});