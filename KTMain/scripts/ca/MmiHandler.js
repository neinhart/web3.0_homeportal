"use strict";

/**
 * MMI event control
 */
KTW.ca.MmiHandler = (function () {
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var casAdapter = null;
    var basicAdapter = null;
    var layerManager = null;
    
    var received_event_type = null;
    
    var _notifyFullEpgChange = function() {};
    
    var listeners = [];
    
    var cur_event = null;
    
    var timer_fireMMI = null;
    
    function _init() {
        received_event_type = [ -1, -1 ];
        
        casAdapter = KTW.oipf.AdapterHandler.casAdapter;
        casAdapter.setMmiEventListener(onMmiEventArrived);
        
        basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        layerManager = KTW.ui.LayerManager;
    }
    
    function _addListener(listener) {
        listeners.push(listener);
        
        log.printDbg("_addListener() listener count = " + listeners.length);
    }
    
    function _removeListener(listener) {
        var length = listeners.length;
        for (var i = 0; i < length; i++) {
            if (listeners[i] === listener) {
                listeners.splice(i, 1);
                break;
            }
        }
        
        log.printDbg("_removeListener() listener count = " + listeners.length);
    }

    /**
     * [dj.son] [WEBIIIHOME-3258] 스마크 카드 오류 팝업
     * - 기존 R3 부터 스마트 카드 오류 팝업은 홈메뉴 화면 등 NORMAL 화면 밑에 위치해야 하며, 항상 화면에 표시되어야 함
     * - 기존에는 Popup 형태로 띄웠지만, 해당 팝업의 특성상 BACKGROUND 타입이 더 적절하다고 판단하여 변경
     * - 다만 이럴 경우 vboBackgroundArea 보다 z-index 상 위에 위치하므로, 2-depth 화면이 "포스터 - 스마트카드 오류 팝업 - 배경" 순으로 화면이 배치됨
     *   이를 해결하기 위해 vboBackgroundArea 의 z-index 를 50 으로 설정하고 스마트카드 오류 팝업은 40 으로 설정하여 화면 순서 조절 ("포스터 - 배경 - 스마트카드 오류 팝업")
     *   vboBackgroundArea 의 경우, 이미 모든 화면에서 clip-path 를 사용하여 배경 이미지를 뚫기 때문에 이슈 없음
     */
    function showMmiImagePopup(dialogue_id) {
        var popup_data = {
            'dialogue_id': dialogue_id
        };

        log.printDbg("showMmiImagePopup() ");

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "SmartCardEjectPopup",
                type: KTW.ui.Layer.TYPE.BACKGROUND,
                priority: KTW.ui.Layer.PRIORITY.BACKGROUND + 40,
                view : KTW.ui.view.popup.SmartCardEjectPopup,
                linkage: true,
                params: {
                    data : popup_data
                }
            },
            visible: true
        });
    }

    
    function hideMmiPopup() {
        log.printDbg("hideMmiPopup()");
        KTW.ui.LayerManager.deactivateLayer({
            id: "SmartCardEjectPopup",
            remove: true
        });
    }
    
    function setSmartCardId(id) {
        KTW.SMARTCARD_ID = id;
    }
    
    function onMmiEventArrived(event) {
        
        var event_type = event.getEventType();
        log.printDbg("onMmiEventArrived() event_type = " + getEventTypeString(event_type));
        
        // 상위 16비트를 tunerID로 사용한다.
        // Main Tuner ID : 0, PIP Tuner ID : 1
        var tuner_id = event.getDialogueId() >>> 16;
        log.printDbg("onMmiEventArrived() tuner_id = " + tuner_id);
        
        if (isValidEvent(event_type, tuner_id) === false) {
            log.printDbg("onMmiEventArrived() not pair event... so skip");
            return;
        }
        
        received_event_type[tuner_id] = event_type;
        if (timer_fireMMI != null) {
        	clearTimeout(timer_fireMMI);
        	timer_fireMMI = null;
        }
        
        try {
            // 하위 16비트를 DialogueId로 사용한다.
            var dialogue_id = event.getDialogueId() & 0xFFFF;
            log.printDbg("onMmiEventArrived() dialogue_id = " + getDialogueIdString(dialogue_id));
            
            var message;
            var notify_event = true;
            
            if (event_type === KTW.ca.MmiHandler.MMI_EVENT.TYPE.CLOSE) {
                if (tuner_id === 0) {
                    hideMmiPopup();
                }
                
                if (dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.INSERT_SMARTCARD) {
                    var id = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.SMARTCARD_ID);
                    setSmartCardId(id);
                    notify_event = false;
                }
                
                if (tuner_id === 0) {
                    if (dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.RATING_LIMIT_EXCEEDED) {
                        cur_event = null;
                    }
                }
            }
            else if (event_type === KTW.ca.MmiHandler.MMI_EVENT.TYPE.START) {
                if (dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.INSERT_SMARTCARD) {
                    setSmartCardId("0000000000");
                    notify_event = false;
                }
                
                if (dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.CONFIRM_PURCHASE_PREVIEW) {
                    if (tuner_id === 0) {
                        hideMmiPopup();
                    }
                }
                else {
                    if (tuner_id === 0) {
                        if (dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.RATING_LIMIT_EXCEEDED) {
                            cur_event = event;
                        }
                    }
                    
                    if (dialogue_id !== KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.RATING_LIMIT_EXCEEDED &&
                        dialogue_id !== KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.CANT_BUY_THE_CONTENT) {
                        message = getMmiMessage(event.getMMIObject());
                        log.printDbg("onMmiEventArrived() mmi message = " + JSON.stringify(message));
                        if (tuner_id === 0) {
                            if (util.isValidVariable(message, ["type"]) === true) {
                                if (message.type === KTW.ca.MmiHandler.MMI_EVENT.MESSAGE_TYPE.TEXT) {
                                    
                                    // [jh.lee] 2015.12.17 R5 요구사항으로 스마트카드 관련 오류 시 새로운 팝업이 도입
                                    //          히스토리 유지를 위해 아래 원본 코드 유지.
                                    //          dialogue 16 또는 6,7 이 오는 경우 이미지 오류 팝업을 노출 시킴(기존과 다르게 전체편성표에서도 같은 팝업이 생성됨
                                    if (dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.INSERT_SMARTCARD ||
                                            dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.DIALOG_ID_CARD_IS_NOT_PAIRED ||
                                            dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.DIALOG_ID_CARD_NOT_AUTHORIZED) {
                                        // [jh.lee] 16 또는 6,7 로 오는 경우 이미지 팝업 생성
                                        log.printDbg("showMmiImagePopup dialogue_id : " + dialogue_id);
                                        showMmiImagePopup(dialogue_id);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            log.printDbg("onMmiEventArrived() notify_event = " + notify_event + " , KTW.SMARTCARD_ID : " + KTW.SMARTCARD_ID);
            if (notify_event === true && Number(KTW.SMARTCARD_ID) > 0) {
            	// MMI timer 추가
            	// 제한이 걸린 상태에서 C/S 제거하면
            	// 제한 CLOSE event가 먼저 올라오고 뒤이어 C/S 제거 event가 올라온다
            	// 이 때 제한 상태가 풀리면 안되기 때문에 timer를 걸어서 처리
            	if (event_type === KTW.ca.MmiHandler.MMI_EVENT.TYPE.CLOSE &&
            			(dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.RATING_LIMIT_EXCEEDED ||
            			dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.CANT_BUY_THE_CONTENT ||
            			dialogue_id === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.CONFIRM_PURCHASE_PREVIEW)) {
            		timer_fireMMI = setTimeout(function(){
            			fireMmiEvent(event_type, dialogue_id, tuner_id === 0, message);
            			timer_fireMMI = null;
            		}, 500);
            	}
            	else {
            		fireMmiEvent(event_type, dialogue_id, tuner_id === 0, message);
            	}
            }
        }
        catch(e) {
            log.printErr("onMmiEventArrived() error ocurred");
            log.printExec(e);
        }
    }
    
    /**
     * event 유효성 여부 check
     * START/STOP pair로 올라오는 경우 유효함. 
     */
    function isValidEvent(type, id) {
        var valid = true;
        
        if (type === KTW.ca.MmiHandler.MMI_EVENT.TYPE.CLOSE) {
            if (received_event_type[id] !== KTW.ca.MmiHandler.MMI_EVENT.TYPE.START) {
                valid = false;
            }
        }
        
        return valid;
    }
    
    function _releaseParentalRating(pw) {
        try {
            if (cur_event !== null) {
                var mm_object = cur_event.getMMIObject();
                if (util.isValidVariable(mm_object) === true) {
                    if (mm_object.getObjectType() ===  KTW.ca.MmiHandler.MMI_EVENT.MESSAGE_TYPE.ENQUIRY) {
                        mm_object.setAnswer(pw);
                    }
                }
            }
        } catch(e) {
            log.printErr("_releaseParentalRating() occured error");
            log.printExec(e);
        }
    }
    
    function getMmiMessage(obj) {
        var message = {};
        if (util.isValidVariable(obj) === true) {
            var type = obj.getObjectType();
            
            var message_type = KTW.ca.MmiHandler.MMI_EVENT.MESSAGE_TYPE;
            
            message.type = type;
            
            switch(type) {
                case message_type.ENQUIRY :
                case message_type.TEXT :
                    message.text = obj.getText();
                    break;
                case message_type.MENU :
                    break;
                case message_type.LIST :
                    message.text += obj.getTitleText() + ",";
                    message.text += obj.getSubTitleText() + ",";
                    message.text += obj.getBottomText() + ",";
                    
                    var list = obj.getList();
                    var length = list ? list.length : 0;
                    for (var i = 0; i < length; i++) {
                        message.text += list[i];
                        if (i < length - 1) {
                            message.text += "|";
                        }
                    }
                    break;
            }
        }
        
        return message;
    }
    
    /**
     * notify MMI event
     * 
     * @param type - START/CLOSE
     * @param dialogue_id
     * @param is_main - if true this is main tuner, otherwise pip tuner
     * @param message
     */
    function fireMmiEvent(type, dialogue_id, is_main, message) {
        log.printDbg("fireMmiEvent() type = " + type + " , dialogue_id : " + dialogue_id + " , is_main : " + is_main + " , message : " + message);
    	var copy = listeners.slice();
        var length = copy.length;
        log.printDbg("fireMmiEvent() length = " + length);
        for (var i = 0; i < length; i++) {
            try {
                copy[i](type, dialogue_id, is_main, message);
            }
            catch(e) {
                log.printErr("fireMmiEvent() occured error");
                log.printExec(e);
                
                _removeListener(copy[i]);
            }
            
        }
    }
    
    function getEventTypeString(type) {
        var type_str = "";
        
        var event_type = KTW.ca.MmiHandler.MMI_EVENT.TYPE;
        
        switch (type) {
            case event_type.START :
                type_str = "START";
                break;
            case event_type.CLOSE :
                type_str = "CLOSE";
                break;
        }
        
        return type_str;
    }
    
    function getDialogueIdString(id) {
        var id_str = "";
        
        var event_id = KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID;
        
        switch (id) {
            case event_id.CANT_BUY_THE_CONTENT :
                id_str = "CANT_BUY_THE_CONTENT";
                break;
            case event_id.CAS_MESSAGE :
            	id_str = "CAS_MESSAGE";
            	break;
            case event_id.INSERT_SMARTCARD :
                id_str = "INSERT_SMARTCARD";
                break;
            case event_id.RATING_LIMIT_EXCEEDED :
                id_str = "RATING_LIMIT_EXCEEDED";
                break;
            case event_id.SERVICE_BLACK_OUT :
                id_str = "SERVICE_BLACK_OUT";
                break;
            case event_id.CONFIRM_PURCHASE_PREVIEW :
                id_str = "CONFIRM_PURCHASE_PREVIEW";
                break;
            case event_id.DIALOG_ID_CARD_IS_NOT_PAIRED :
                id_str = "DIALOG_ID_CARD_IS_NOT_PAIRED";
                break;
            case event_id.DIALOG_ID_CARD_NOT_AUTHORIZED :
                id_str = "DIALOG_ID_CARD_NOT_AUTHORIZED";
                break;
            default:
                break;
        }
        
        return id_str;
    }
    
    return {
        init: _init,
        
        addListener: _addListener,
        removeListener: _removeListener,
        
        releaseParentalRating: _releaseParentalRating

    }
}());

//define constant.
Object.defineProperty(KTW.ca.MmiHandler, "MMI_EVENT", {
    value:  {
        TYPE : {
            START : 0,
            CLOSE : 1
        },
        MESSAGE_TYPE : {
            TEXT : 0,
            ENQUIRY : 1,
            LIST : 2,
            MENU : 3
        },
        DIALOGUE_ID : {
            CANT_BUY_THE_CONTENT : 4,
            DIALOG_ID_CARD_IS_NOT_PAIRED : 6,
            DIALOG_ID_CARD_NOT_AUTHORIZED : 7,
            CAS_MESSAGE : 8,
            INSERT_SMARTCARD: 16,
            RATING_LIMIT_EXCEEDED : 29,
            SERVICE_BLACK_OUT : 32,
            CONFIRM_PURCHASE_PREVIEW : 43
        }
   },
    writable: false,
    configurable: false
});