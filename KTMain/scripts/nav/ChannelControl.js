"use strict";

/**
 * channel controller
 * 
 */
KTW.nav.ChannelControl = function(ctype, vbo, ccc, csem, excb) {
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var OIPF_DEF_VB = KTW.oipf.Def.BROADCAST;
    var NAV_DEF_CONTROL = KTW.nav.Def.CONTROL;
    var NAV_DEF_CHANNEL = KTW.nav.Def.CHANNEL;
    
    var layerManager = KTW.ui.LayerManager;
    var iframeManager = KTW.managers.service.IframeManager;
    var appServiceManager = KTW.managers.service.AppServiceManager;
    var stateManager = KTW.managers.service.StateManager;
    var caHandler = KTW.ca.CaHandler;
    var mmiHandler = KTW.ca.MmiHandler;
    
    var navAdapter = null;
    
    var this_obj = this;
    
    this.vbo = vbo;
    this.cc_control = ccc;
    this.ch_se_mananger = csem;
    this.ex_ch_db = excb;
    
    var control_type = ctype;
    var log_prefix = ctype;
    
    var state = -1;
    
    var curr_channel = null;
    /**
     * back channel은 총 3개를 유지 (단, OTS 오디오 채널은 제외)
     * OTV에서는 index 0번째 항목만 사용하면 됨
     * OTS에서는 상황에 따라 index1, 2를 구분하여 처리하는데
     * OTS PIP의 경우 현재 main 채널과 동일한 type의 이전채널을 획득해야 하기때문에
     * 구분해서 처리하도록 관리함
     * 
     * back_channel[0] : 조건에 관계없이 이전채널
     * back_channel[1] : OTS 채널에 해당하는 이전채널
     * back_channel[2] : OTV 채널에 해당하는 이전채널
     */
    var back_channel = null;
    /**
     * lcw channel은 총 2개를 유지
     * 
     * lcw_channel[0] : AV 채널에 대한 lcw 채널
     * lcw_channel[1] : Audio 채널에 대한 lcw 채널
     */
    var lcw_channel = null;
    
    var last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
    /**
     * channel change를 요청할 때 layer clear 여부 체크하기 위한 변수
     */
    var last_only_select;
    var skip_channel_event = false;
    /**
     * 사용자의 요청의 의한 channel change 여부
     */
    var request_channel_change = false;
    
    var channel_event_listeners = null;
    
    var pip_control_mode = KTW.nav.Def.PIP_MODE.NONE;

    var is_stop_av = false;

    /**
     * 2017.02.15 dhlee 2.0 R7 적용
     * playChannel async 관련 variables
     */
    var last_app_mode = KTW.CONSTANT.APP_MODE.NONE;
    var delay_play_channel = undefined;
    var delay_play_timer = null;

    var checkServiceStateTimer = null;
    
    this._init = function() {
    	log.printDbg("init() control_type = " + control_type, log_prefix);
    	
        channel_event_listeners = [];
        back_channel = [];
        lcw_channel = [];
        
        navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        
        if (control_type === NAV_DEF_CONTROL.MAIN) {
            curr_channel = this.vbo.bindToCurrentChannel();
            log.printDbg("_init() current_channel = " + curr_channel, log_prefix);
            
            // 부팅 시점에 제한 및 연령제한 체크를 위해서
            // setChannel을 의도적으로 하는데 이때 miniEPG가 활성화 되기 때문에
            // 의도적으로 only_select flag를 true를 변경하여 처리하도록 함.
            //request_channel_change = true;
            //last_only_select = true;
        }
        else if (control_type === NAV_DEF_CONTROL.PIP) {
            this.vbo.setParentElement(KTW.managers.service.PipManager.PIP_LAYER_ID);
        }
        
        state = KTW.nav.Def.CONTROL.STATE.INITED;
    };
    
    this._start = function(pip_mode, style_obj) {
        log.printDbg("start(" + pip_mode + ")", log_prefix);
        
        if (state === KTW.nav.Def.CONTROL.STATE.STARTED) {
            log.printDbg("start() already start state... so return", log_prefix);
            return;
        }
        
        // main control 만 적용
        if (control_type === NAV_DEF_CONTROL.MAIN) {
            // add channel list update listener
            navAdapter.addChannelListUpdateListener(onChannelListUpdate);
            
            if (this.ch_se_mananger) {
                //this.ch_se_mananger.addChannelSelectionEventListener(onChannelSelectionEvent);
                // ChannelSelectionEventListener 의 경우 callback 안되는 경우가 있기 때문에
                // DirectChannelEventListener 를 사용하도록 함
                this.ch_se_mananger.addDirectChannelEventListener({ "onChannelSelecting" : onChannelSelectionEvent });
            }
            
            // MAIN channel change succeeded 및 error event listener 등록
            this.vbo.addEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_SUCCEEDED, onMainChannelChangeSucceeded);
            this.vbo.addEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_ERROR, onMainChannelChangeError);
            
            caHandler.addCaEventListener(onReceiveChannelStatus);
            mmiHandler.addListener(onReceiveMmiEvent);

            // TODO 2017.02.16 dhlee 2.0 R7 로직 추가
            KTW.managers.service.AppModeManager.addAppModeListener(onAppModeChanged);
        }
        else if (control_type === NAV_DEF_CONTROL.PIP) {
        	if (style_obj) {
                if (style_obj.position) this.vbo.style.position = style_obj.position;
                if (style_obj.left) this.vbo.style.left = style_obj.left;
                if (style_obj.top) this.vbo.style.top = style_obj.top;
                if (style_obj.width) this.vbo.style.width = style_obj.width;
                if (style_obj.height) this.vbo.style.height = style_obj.height;
                //if (style_obj.visibility) this.vbo.style.visibility = style_obj.visibility;
            }
        	pip_control_mode = pip_mode;
            this.vbo.changeMode(pip_mode);
            
            // PIP channel change succeeded 및 error event listener 등록
            this.vbo.addEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_SUCCEEDED, onPipChannelChangeSucceeded);
            this.vbo.addEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_ERROR, onPipChannelChangeError);
            
            caHandler.addCaEventListener(onReceiveChannelStatus);
            mmiHandler.addListener(onReceiveMmiEvent);
        } else if (control_type === NAV_DEF_CONTROL.STITCHING) {
        	this.vbo.enableAudio();
        	this.vbo.style.visibility="inherit";
        	
        	// PIP channel change succeeded 및 error event listener 등록
            this.vbo.addEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_SUCCEEDED, onStitchingChannelChangeSucceeded);
            this.vbo.addEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_ERROR, onStitchingChannelChangeError);
        }
        
        state = KTW.nav.Def.CONTROL.STATE.STARTED;
    };
    
    this._stop = function() {
        log.printDbg("stop()", log_prefix);
        
        if (state === KTW.nav.Def.CONTROL.STATE.STOPPED) {
            log.printDbg("stop() already stop state... so return", log_prefix);
            return;
        }
        
        if (control_type === NAV_DEF_CONTROL.MAIN) {
            log.printDbg("stop() this is main control... DO NOT STOP", log_prefix);
            return;
        }
        else if (control_type === NAV_DEF_CONTROL.PIP) {
            //layerManager.deactivateLayer(KTW.nav.ui.PipView.id);
            
            this.vbo.removeEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_SUCCEEDED, onPipChannelChangeSucceeded);
            this.vbo.removeEventListener(OIPF_DEF_VB.EVENT.CHANNEL_CHANGE_ERROR, onPipChannelChangeError);
            
            caHandler.removeCaEventListener(onReceiveChannelStatus);
            mmiHandler.removeListener(onReceiveMmiEvent);
            
            curr_channel = null;
            last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
            last_only_select = undefined;
            request_channel_change = false;
            channel_event_listeners.length = 0;
            
            pip_control_mode = KTW.nav.Def.PIP_MODE.NONE;
        }
        else if (control_type === NAV_DEF_CONTROL.STITCHING) {
            if(this.vbo && this.vbo.disableAudio) {
                this.vbo.disableAudio();
            }
        }
        
        this_obj.stopChannel();
        
        state = KTW.nav.Def.CONTROL.STATE.STOPPED;
    };
    
    this._resume = function(reset) {
        log.printDbg("resume(" + reset + ")", log_prefix);
        
        if (reset === true) {
            // 의도적으로 miniEPG가 떠야 하는 경우를 위해서
            // reset 여부에 따라서 처리하도록 한다.
            if (control_type === NAV_DEF_CONTROL.MAIN) {
                request_channel_change = false;
                last_only_select = false;
            }
        }
        // TODO 2017.02.16 dhlee 2.0 R7 로직 적용
        else {
            // 다시 resume 되는 경우
            // miniEPG 노출되지 않도록 last_only_select 값을 true로 변경함
            // /**
            //  * jjh1117 2017/12/13
            //  * JIRA	WEBIIIHOME-3613 이슈 사항 수정함.
            //  */
            request_channel_change = true;
            last_only_select = true;
        }
        
        state = KTW.nav.Def.CONTROL.STATE.STARTED;
    };
    
    this._pause = function(stop_channel) {
        log.printDbg("pause()", log_prefix);
        
        // STANDBY 및 DATA 상태에서 돌아오는 경우
        // home UI만 노출되어야 하기 때문에 miniEPG를 활성화 하지
        // 않도록 하기 위해서 only_select true 상태로 만들어 준다.
        if (control_type === NAV_DEF_CONTROL.MAIN) {
            request_channel_change = true;
            last_only_select = true;
        }
        
        if (stop_channel !== false) {
            this_obj.stopChannel();
        }
        
        caHandler.checkParentalRatingLock(true);
        
        last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
        
        state = KTW.nav.Def.CONTROL.STATE.PAUSED;
    };
    
    this._getState = function() {
        return state;
    };
    
    this._getPipMode = function() {
        return pip_control_mode;
    };
    
    this._getCurrentChannel = function() {
        return curr_channel;
    };
    
    this._getBackChannel = function(is_seperate) {
    	var channel = null;
    	var is_ots_channel_ring = false;
    	
        if (is_seperate === true) {
            // OTV/OTS 구분해서 이전채널을 확인해야 하는 경우
            // PIP 멀티화면에서 현재 채널의 channel ring 기준으로
            // 이전채널 정보를 리턴한다.
            if (stateManager.isVODPlayingState() === true ||
                    (curr_channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS &&
                    curr_channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI)) {
                is_ots_channel_ring = true;
                channel = back_channel[1];
            }
            else {
                channel = back_channel[2];
            }
        }
        else {
        	channel = back_channel[0];
        	if (KTW.CONSTANT.IS_OTS === true) {
        		is_ots_channel_ring = true;
        	}
        }
        
        // 만약 이전채널이 유효하지 않다면
        // 현재채널 기준의 channel ring에 해당하는 promo channel로 리턴한다.
        if (util.isValidVariable(channel) === false) {
        	channel = is_ots_channel_ring === true ?
                    navAdapter.getSkyPlusChannel() : navAdapter.getPromoChannel();
    	}
        
        return channel;
    };
    
    this._getLcwChannel = function(is_audio) {
        var channel = lcw_channel[0];
        
        if (is_audio === true) {
            channel = lcw_channel[1];
        }
        
        return channel;
    };
    
    this._recheckPermission = function(forced) {
    	// permission이 blocked 이상이거나
    	// OTV audio 채널의 경우 다시 miniEPG를 그려주도록 한다.
        if (last_perm > NAV_DEF_CHANNEL.PERM.OK) {
            var channel_event = {
                "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
                "channel" : curr_channel,
                "permission" : last_perm
            };
            setIframe(channel_event, forced === false);
            
            if (forced === true) {
                // [dj.son] activate Layer 수정
                layerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.MINI_EPG,
                        type: KTW.ui.Layer.TYPE.TRIGGER,
                        priority: KTW.ui.Layer.PRIORITY.NORMAL,
                        linkage: false,
                        params: {
                            data : channel_event,
                            visible: true
                        }
                    },
                    clear_normal: true,
                    visible: true,
                    cbActivate: function() {

                    }
                });
                
                KTW.managers.service.AudioIframeManager.setAudioFullEpgExit();
            }
        }
    };

    this._resetPerm = function( forced ) {
        last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;

        if (forced === true) {
            // [R7] 기가지니 OSD
            request_channel_change = false;
            last_only_select = false;
        }
    };
    
    function playChannel (channel, is_blocked, blocked_reason) {
        if (channel === null) {
            log.printDbg("playChannel() channel is null", log_prefix);
            return;
        }
        
        // var perm = checkPermission(is_blocked); // 2016.09.05 dhlee R6 형상에서는 아래와 같이 호출하고 있음
		var perm = checkPermission(channel, is_blocked);
        
        log.printDbg("playChannel() perm = " + getPermString(perm), log_prefix);
        log.printDbg("playChannel() last_perm = " + getPermString(last_perm), log_prefix);
        
        if (perm !== last_perm) {
            // permission이 변경되는 경우
            // background iframe 변경 및 channel select/stop 처리하도록 함
            var channel_event = {
                "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
                "channel" : channel,
                "permission" : perm,
                "blocked_reason" : blocked_reason
            };
            
            // 우선 main control 경우에만 iframe 처리하도록 함
            // 단, 시청시간 제한 상태에서는 처리하지 않도록 함
            // TODO 별도 iframe 처리를 해야 함
            if (control_type === NAV_DEF_CONTROL.MAIN ) {
                setIframe(channel_event);
            }
            fireChannelEvent(channel_event);
            
            last_perm = perm;
        }
        else {
            /**
             * jjh1117 2016.10.07
             * web 2.0에서 이 부분 처리 하던 부분이
             * Jira 이슈(KTWHPTZOF-802:동일한 permission인 경우 miniEPG activate 처리를 하지 않도록 함)
             * 사항으로 주석 처리 된 상태임.
             * 따라서, 주석 처리 부분 제거함.
             */
            // TODO 2017.04.30 dhlee
            // 최초 부팅시 channel event가 promomanager에 전달이 되지 않음
            // main.startSvc() 가 호출되기 전(PromoManager.init() 호출 전)에 첫번째 채널 이벤트가 발생되고
            // main.startSvc() 호출 이후 2번째 채널 이벤트가 발생되는데, 2번째 발생되는 채널 이벤트는 홈포털에서 only_select == true로
            // 호출하는 channel event 이다.
            // 따라서 promo manager의 init 이후에 채널 이벤트가 호출되지 않아 문제가 발생된다.
            // 임시 방편으로 여기서 requestTrigger()를 직접 호출하도록 한다.
            // 어디까지나 work-around 이므로 부팅 상황에서 miniepg 실행되지 않도록 하는 로직을 정리하면서 함께 정리되어야 한다.
            if (control_type === NAV_DEF_CONTROL.MAIN && perm === KTW.nav.Def.CHANNEL.PERM.OK) {
                log.printDbg("playChannel(), call PromoManager.requestTrigger()");
                KTW.managers.service.PromoManager.requestTrigger(channel);
            }


            if (KTW.managers.service.KidsModeManager.isModeChanging()) {
                KTW.managers.service.KidsModeManager.setModeChangingFlag(false);
            }
        }
    }


    /**
     * Permission 을 반환한다.
     *
     * @param is_blocked
     * @returns {KTW.nav.Def.CHANNEL.PERM.UNKNOWN|*}
     */
    function checkPermission(channel, is_blocked) {
        log.printDbg("checkPermission()", log_prefix);
        
        var perm = last_perm;
        
        if (is_blocked === true) {
            perm = NAV_DEF_CHANNEL.PERM.BLOCKED;
        }
        else {
            // 최초 진입한 채널이거나 (permisson unknown)
            // 이전에 제한 채널인 경우 (permission blocked) 에만 OK로 변경
            // 시청연령제한이나 미가입 채널의 경우 cas event callback에 의해서
            // 처리되기 때문에 별도로 처리함
            if (last_perm === NAV_DEF_CHANNEL.PERM.UNKNOWN ||
                    last_perm === NAV_DEF_CHANNEL.PERM.BLOCKED) {
                perm = NAV_DEF_CHANNEL.PERM.OK;
            }
        }
        
        return perm;
    }
    
    function setIframe(channel_event, set_only, forced) {
        
        var iframe_type;
        switch(channel_event.permission) {
            case NAV_DEF_CHANNEL.PERM.OK :
            case NAV_DEF_CHANNEL.PERM.PREVIEW :
                if (navAdapter.isAudioChannel(channel_event.channel, true) === true) {
                    iframe_type = iframeManager.DEF.TYPE.AUDIO;
                }
                else {
                    iframe_type = iframeManager.DEF.TYPE.NONE;
                }
                break;
            case NAV_DEF_CHANNEL.PERM.BLOCKED :
                iframe_type = iframeManager.DEF.TYPE.BLOCKED;
                break;
            case NAV_DEF_CHANNEL.PERM.PR_BLOCKED :
                /**
                 * 시청연령 제한인 경우 KIDS 모드 여부
                 * 체크 하여 Iframe 노출 함.
                 */
                if(KTW.managers.service.KidsModeManager.isKidsMode()) {
                    iframe_type = iframeManager.DEF.TYPE.PR_BLOCKED_KIDS_MODE;
                }else {
                    iframe_type = iframeManager.DEF.TYPE.PR_BLOCKED;
                }
                break;
            case NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED :
            	// OTS STB에서 sky choice channel의 경우 movie choice iframe 처리하도록 한다.
                if (KTW.CONSTANT.IS_OTS === true &&
                        navAdapter.isSkyChoiceChannel(channel_event.channel) === true) {
                    iframe_type = iframeManager.DEF.TYPE.SKY_CHOICE;
                }
                else {
                    iframe_type = iframeManager.DEF.TYPE.NOT_SUBSCRIBED;
                }
                break;
        }
        
        if (iframe_type !== undefined) {

            if(iframe_type === iframeManager.DEF.TYPE.AUDIO) {
                /**
                 * OTS Audio Portal인 경우 IframeManager에 Audio Iframe 노출 하도록 수정함.
                 */
                if (KTW.CONSTANT.IS_OTS === true) {
                    if (navAdapter.isAudioChannel(channel_event.channel, false) === false) {
                        iframeManager.setType(iframe_type, set_only, forced);
                        return;
                    }
                }
                KTW.managers.service.PromoManager.stopTrigger();
                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , channel_event.channel);
            }else {
                iframeManager.setType(iframe_type, set_only, forced);
            }
        }
    }

    /**
     * listening 하고 있는 모듈에게 channel event를 전달한다.
     *
     * @param channel_event
     */
    function fireChannelEvent(channel_event) {
        log.printDbg("fireChannelEvent()", log_prefix);
        
        if (last_perm !== NAV_DEF_CHANNEL.PERM.UNKNOWN) {
            channel_event.type = NAV_DEF_CHANNEL.EVENT.PERM_UPDATED;
        }
        
        log.printDbg("fireChannelEvent() last_only_select = " + last_only_select, log_prefix);
        // channel select만 하는 경우 stack을 clear하지 않고 유지하도록 함
        // full EPG 등에서 channel tune 하는 경우에 해당함


        var isShowMiniEpg = true;

        if (isVisibleMultiPIP() === true) {
            isShowMiniEpg = false;
            if (channel_event.permission === NAV_DEF_CHANNEL.PERM.BLOCKED ||
                channel_event.permission === NAV_DEF_CHANNEL.PERM.PR_BLOCKED ||
                channel_event.permission === NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED) {
                isShowMiniEpg = true;
            }
        }
        else {
            if (last_only_select) {
                isShowMiniEpg = false;
            }
        }
        
        if (isShowMiniEpg === true && stateManager.isRunningState() === false) {
            isShowMiniEpg = false;
        }
        
        notifyChannelEvent(channel_event, isShowMiniEpg);
    }
    
    function notifyChannelEvent(channel_event, isShowMiniEpg , preLastPerm) {
        log.printDbg("notifyChannelEvent() channel event = " + 
                getChannelEventTypeString(channel_event.type) + 
                "("+ getPermString(channel_event.permission) + " , "+isShowMiniEpg + " )", log_prefix);
        
        if (isShowMiniEpg === true) {
            if (KTW.managers.service.KidsModeManager.isModeChanging()) {
                isShowMiniEpg = false;
                KTW.managers.service.KidsModeManager.setModeChangingFlag(false);
            }
        }

        /**
         * [dj.son] 2017.01.26
         * 전체 편성표에서 main control channel 변경했을때, 전체 편성표 종료되고 미니 epg Layer 가 activate 됨
         * - channel 이 select 만 된 경우, clearNormal 안함 -> last_only_select 값 보고 판단
         * - isShowMiniEpg 가 false 이면 미니 epg Layer 가 hidden 인 상태이므로 clearNormal 할 필요가 없음 -> clearNormal 초기값을 isShowMiniEpg 로 수정
         */

        log.printDbg("notifyChannelEvent()  isShowMiniEpg : " + isShowMiniEpg);

        if (isShowMiniEpg === true) {
            // 먼저 miniEPG에 event 전달
            // permission update 시에는 stack layer를 clear 하지 않도록 함
            // main channel control일 경우에만 miniEPG에 알림
            if (control_type === NAV_DEF_CONTROL.MAIN) {
                /**
                 * jjh1117 2016.10.07
                 * MiniEpgLayer activateLayer 호출 부분 추가함.
                 *
                 * [dj.son] 시간이 흘러서 자동으로 퍼미션이 변경된 경우 체크 (채널 미리보기 시간 소비, 현재 프로그램 종료 등등)
                 */
                var clearNormal = true;
                if (preLastPerm !== undefined && preLastPerm === NAV_DEF_CHANNEL.PERM.PREVIEW) {
                    if (channel_event !== undefined && channel_event !== null &&
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED) {
                        /**
                         * jjh1117 2017/0810
                         * 멀티화면상에서 미리보기 상태에서 미가입 상태로 변경 되면 멀티화면
                         * 종료 되면서 미니가이드가 노출 되지 않은 이슈 수정함.
                         */
                        if (isVisibleMultiPIP() === true) {
                            clearNormal = true;
                        }else {
                            clearNormal = false;
                        }
                    }
                }
                else if (preLastPerm !== undefined && preLastPerm === NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
                    if (channel_event !== undefined && channel_event !== null &&
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.OK) {
                        clearNormal = false;
                    }
                }
                else if (preLastPerm !== undefined && preLastPerm === NAV_DEF_CHANNEL.PERM.OK) {
                    if (channel_event !== undefined && channel_event !== null &&
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
                        /**
                         * jjh1117 2017/0810
                         * 멀티화면상에서 채널UP/DOWN으로 MainVBO 채널을 시청연령제한 채널으로 접근 시
                         * 멀티화면이 종료 되면서 미니가이드가 노출 되지 않은 이슈 사항 수정함.
                         */
                        if (isVisibleMultiPIP() === true) {
                            clearNormal = true;
                        }else {
                            clearNormal = false;
                        }
                    }
                }

                if(navAdapter.isAudioChannel(channel_event.channel) !== true) {
                    layerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.MINI_EPG,
                            type: KTW.ui.Layer.TYPE.TRIGGER,
                            priority: KTW.ui.Layer.PRIORITY.NORMAL,
                            linkage: false,
                            params: {
                                data : channel_event,
                                visible:true
                            }
                        },
                        clear_normal: clearNormal,
                        visible: true
                    });

                    if(clearNormal === true) {
                        KTW.managers.service.AudioIframeManager.setAudioFullEpgExit();
                    }
                }
            }
        }
        else {
            if (control_type === NAV_DEF_CONTROL.MAIN) {
                var clearNormal = false;

                /**
                 * clearNormal 현재 Layer 상태 유지하도록 하기 위해서 false로 전달 하며
                 * params.visible은 MiniEpg에서 visible 여부를 판단하여 보여줄지 아닌지를 결정함.
                 */
                if(navAdapter.isAudioChannel(channel_event.channel) !== true) {
                    layerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.MINI_EPG,
                            type: KTW.ui.Layer.TYPE.TRIGGER,
                            priority: KTW.ui.Layer.PRIORITY.NORMAL,
                            linkage: false,
                            params: {
                                data : channel_event,
                                visible:false
                            }
                        },
                        clear_normal: clearNormal,
                        visible: true
                    });

                    if(clearNormal === true) {
                        KTW.managers.service.AudioIframeManager.setAudioFullEpgExit();
                    }
                }
            }
        }

        // 이후 별도 channel event listener를 add한 곳에 전달
        var length = channel_event_listeners.length;
        for (var i = 0; i < length; i++) {
            channel_event_listeners[i](channel_event);
        }
    }
    
    function getLastStackLayer() {
        
        //var last_layer = layerManager.getLastStackLayer();
        var last_layer = layerManager.getLastLayerInStack();
        if (util.isValidVariable(last_layer) === true) {
            log.printDbg("getLastStackLayer() last_layer = " + last_layer, log_prefix);
            return last_layer;
        }
        
        return null;
    }
    
    function isVisibleMultiPIP() {
        log.printDbg("isVisibleMultiPIP()");
        var visible = false;
        if (control_type === NAV_DEF_CONTROL.MAIN) {
            var pip_cc = navAdapter.getChannelControl(NAV_DEF_CONTROL.PIP);
            if (pip_cc !== null &&
                    pip_cc.getPipMode() === KTW.nav.Def.PIP_MODE.MULTI_VIEW &&
                    pip_cc.getState() === NAV_DEF_CONTROL.STATE.STARTED) {
                log.printDbg("isVisibleMultiPIP() basic pip mode exist!!");
                visible = true;
            }
        }
        
        return visible;
    }
    
    this._addChannelEventListener = function(listener) {
        channel_event_listeners.push(listener);
    }
    
    this._removeChannelEventListener = function(listener) {
        var length = channel_event_listeners.length;
        for (var i = 0; i < length; i++) {
            if (channel_event_listeners[i] === listener) {
                channel_event_listeners.splice(i, 1);
                break;
            }
        }
    }
    
    this._setOnlySelect = function(only_select, forced, req_ch) {
        log.printDbg("_setOnlySelect(" + only_select + ", " + forced + ")" , log_prefix);
        
        if (forced === true) {
            last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
        }
        
        request_channel_change = true;
        if (req_ch === false) {
            request_channel_change = false;
        }
        
        last_only_select = only_select;
    }
    
    this._setSkipChannelEvent = function(enable) {
        log.printDbg("_setSkipChannelEvent() enable = " + enable, log_prefix);
        
        skip_channel_event = enable;
    }
    
    /**
     * reset only select value
     * 
     * channel tune 시점에 의도적으로 select만 요청한 경우가 아니라면
     * last_only_select를 false로 설정해서 miniEPG 가 노출되도록 처리한다.
     * 보통 onChannelChangeSucceeded 또는 onChannelChangeError event를
     * 받았을 때 이를 확인해서 처리하도록 해야 한다. 
     */
    function resetOnlySelect() {
        // channel tune만 요청한 경우에는 last_only_select를 유지
        log.printDbg("resetOnlySelect() request_channel_change = " + request_channel_change, log_prefix);
        if (request_channel_change === false) {
            log.printDbg("resetOnlySelect()", log_prefix);
            last_only_select = false;
        }
    }
    
    function isSameChannel(channel) {
    	// 간혹 channel 에서도 error가 나는 경우가 있어 조건 추가
        if (util.isValidVariable(curr_channel) === true && util.isValidVariable(channel) === true) {
            if (curr_channel.ccid === channel.ccid) {
                log.printDbg("isSameChannel() return true", log_prefix);
                return true;
            }
        }
        return false;
    }
    
    this._isSameChannel = function(channel) {
        // 외부 요청에 의한 동일채널 확인 시에는
        // TV_VIEWING 상태에서만 유효여부를 체크해서 리턴하도록 한다.
        // 이렇게 하는 이유는 promo channel에서 VOD play 이후 종료할 때
        // 여전히 promo channel 이기 때문에 videoBroadcastObject#setChannel() 호출을
        // 무시할 수 있기 때문이다. 
        if (stateManager.isTVViewingState() === true) {
            return isSameChannel(channel);
        }
        return false;
    }
    
    /**
     * AV 채널인지 판단
     */
    function isAVChannel(channel) {
        return (channel && channel.channelType !== Channel.TYPE_OTHER);
    }

    /**
     * OTS audio channel 여부를 반환한다.
     *
     * @param channel
     * @returns {boolean}
     */
    function isSkyAudioChannel(channel) {
        if (KTW.CONSTANT.IS_OTS === true) {
            return navAdapter.isAudioChannel(channel) === true;
        }
        return false;
    }

    /**
     */
    function showAudioFullEpg(new_channel) {
        log.printDbg("showAudioFullEpg()");
    	last_perm = NAV_DEF_CHANNEL.PERM.OK;
        /**
         * jjh1117 2017/10/18
         * 오디오 편성표 노출 시 기존 Promo TRIGER 닫음.
         */
        KTW.managers.service.PromoManager.stopTrigger();
        KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , new_channel);
    }

    /**
     * ChannelSelectionEventManager#onChannelSelecting Event callback
     *
     * @param event
     */
    function onChannelSelectionEvent(event) {
        
        var channel = event.channel;
        log.printDbg("onChannelSelectionEvent() channel = " + JSON.stringify(channel), log_prefix);
        
        if (skip_channel_event === true) {
            log.printDbg("onChannelSelectionEvent() skip onChannelSelectionEvent", log_prefix);
            return;
        }

        if(control_type === NAV_DEF_CONTROL.MAIN) {
            if(KTW.managers.service.ChannelPreviewManager.isstart(true)) {
                // 2017.04.25 dhlee
                // 현재 control_type은 MAIN 이므로 조건문 삭제
                // var ch = KTW.managers.service.ChannelPreviewManager.getChannel(control_type === NAV_DEF_CONTROL.MAIN?true:false);
                var ch = KTW.managers.service.ChannelPreviewManager.getChannel(true);
                if(ch !== undefined && ch !== null && ch.ccid !== channel.ccid) {
                    KTW.managers.service.ChannelPreviewManager.stop(true);
                }
            }
        }

        // channel 전환 시 notifyStopAV에 대한 flag reset을 위해서 호출
        // 이는 다시 한번 검토해 보고 좋은 방법을 찾아야 할 듯...
        KTW.managers.MessageManager.sendInternalMessage({
        	method : KTW.managers.MessageManager.METHOD.CUSTOM.NOTIFY_PLAY_AV
        });
        
        if (isSameChannel(channel) === true) {
            var new_channel = navAdapter.getChannelBySID(channel.sid, channel.idType);
            
            // VOD 상태에서 DCA로 tune 한 경우
            if (stateManager.isVODPlayingState() === true) {
            	//'immediately': true DCA 로 종료시는 VOD 상태를 바로 정리 하기 위함임.
                //ots sky audio channel 인 경우 fullepg 를 띄워야 해서 채널 튠을 한번 더해서 이 callback 을 한번 더 태운다.
                // [dj.son] AppServiceManager 사용
                //stateManager.exitToService({"channel" : new_channel, "tune": isSkyAudioChannel(new_channel) ? "true" : "false", "from": "dca"}, false);
                appServiceManager.changeService({
                    nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING,
                    obj: {"channel" : new_channel, "tune":  navAdapter.isAudioChannel(new_channel) ? "true" : "false", "from": "dca"},
                    visible: false
                });

               
            }
            // 2017.09.28 dhlee
            // 아래 코드에 의해 유튜브를 프로모채널에서 실행 > 종료 시 이전 홈 화면이 실행되지 않고 있다.
            // 해당 코드는 전체적으로 봤을 때 의미 없는 코드로 보이므로 주석 처리 한다.
            /*
            else if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
            	// DATA 상태에서 channel tune이 발생한 경우 TV_VIEWING 상태로 전환
            	// 보통 unicasting app 등인데 추후 독립형 서비스와 동일한 형태로 처리되면
            	// 이 로직은 타지 않게 됨.
            	// 단, hot key에 의해 종료되는 경우에 대비해서 delay 처리함
            	// DATA 상태에서 channel 전환 발생 시 
            	// AV 채널이 아니라면 TV_VIEWING 상태로 전환하지 않도록 함

                var isavChannel = isAVChannel(channel);
                if (isavChannel === true) {
                    setTimeout(function() {
                        // [dj.son] AppServiceManager 사용
                        // TODO 2017.02.16 dhlee 2.0 R7 로직 반영
                        //if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                        if (isavChannel === true &&
                            stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                            appServiceManager.changeService({
                                nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING
                            });
                        }
                    }, 100);
                }
            }
            */
            else {
            	// OTS 형상에서 VOD 상태에서 DCA 등으로 종료될 경우
            	// 마지막 채널과 동일한 audio channel로 tune되면
            	// audio full epg를 호출하여 띄운다.
            	if (navAdapter.isAudioChannel(new_channel)=== true) {
                    if (last_only_select !== true && last_perm === NAV_DEF_CHANNEL.PERM.UNKNOWN) {
                        showAudioFullEpg(new_channel);
                    }
                }
                /**
                 * [dj.son] [WEBIIIHOME-1663] AV 채널일때 같은 채널로 튠시,
                 * clearNormalLayer 를 호출해 Layer Stack 상 미니 epg 가 맨 위쪽에 위치하도록 수정
                 * (추후 자연스럽게 미니 epg 가 resume 될 수 있도록)
                 */
                else if (isAVChannel(channel)) {
                    /**
                     * [dj.son] [WEBIIIHOME-3568] 편성표에서 select 로 튠 한 후 dca 로 같은 채널로 튠했을때,
                     *                            last_only_select 값이 그대로 true 로 남아있어 아래 if 문에 안걸리는 이슈 수정
                     *                            request_channel_change 는 success / error 이벤트에서 refresh 해주고 있음
                     */
                    if (stateManager.isTVViewingState() && (!request_channel_change || !last_only_select)) {
                        layerManager.clearNormalLayer();
                    }

                }
                else {
                    log.printDbg("onChannelSelectionEvent() same channel... so return", log_prefix);
                    // 2017.08.25 dhlee
                    // CUG 서비스를 시작채널로 설정한 상태에서 부팅 후 CUG 종료하게 되면 홈메뉴가 떠야 하는데 뜨지 않는 이슈가 있다.
                    // 이 이슈를 해결하기 위한 work-around를 아래와 같이 적용한다.
                    // 2번의 channel selection이 일어나는데 한번은 ChannelControl이 초기화되기 전 navigator에 의해 tune 되고
                    // 이후 홈에 의해서 only select로 tune이 되는데 이미 같은 채널이므로(?)
                    if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
                        appServiceManager.changeService({
                            nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                            obj: {"type" : KTW.CONSTANT.APP_TYPE.DATA_CHANNEL },
                            changeForced: true,
                            visible: true
                        });
                    }
                }
            }

            return;
		}

        // TODO 2017.02.16 dhlee 2.0 R7 로직 추가
        // 단, 쇼윈도우를 위한 코드로 보이므로 일단 주석 처리함.
        /*
        if (control_type === NAV_DEF_CONTROL.MAIN) {
            log.printDbg("onChannelSelectionEvent() control_type : " + control_type);

            var last_layer = getLastStackLayer();
            log.printDbg("onChannelSelectionEvent() last_layer : " + last_layer);
            if(last_layer !== null) {
                log.printDbg("onChannelSelectionEvent() last_layer.id : " + last_layer.id);
            }
            if (last_layer !== null && last_layer.id === OTW.ui.Layer.ID.SHOW_WINDOW) {
                OTW.managers.service.IframeManager.setType(
                    OTW.managers.service.IframeManager.TYPE.NONE, false, true);

            }
        }
        */

        // tune 요청을 한 경우가 아니라면 only select reset
        resetOnlySelect();
        
        // 시청제한 채널 2분rule check
        caHandler.checkParentalRatingLock();
        
        log.printDbg("onChannelSelectionEvent() last_only_select = " + last_only_select, log_prefix);
        
        // trigger가 늦게 사라지는 경우가 있어서 적용함
        // promo channel만 비교해서 처리해도 되지만 무작정 호출해도 사실상 문제가 없다.
        // 2017.04.27 dhlee
        // 아래 코드로 인해 stopTrigger()가 수행되어 부팅 시 나타나지 않는 이슈가 있다.
        // 일단 주석 처리한다.
        //KTW.managers.service.PromoManager.stopTrigger();
        KTW.managers.device.DeviceManager.storageDevice.hidePopup();
        is_stop_av = false;

        // 다른 채널로 tune 할 경우에만 permission 초기화
        last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
        var select_channel = navAdapter.getChannelBySID(channel.sid, channel.idType);

        // 단, channel tune만 하는 경우에는 현재 stack layer를 hide 하지 않음
        if (last_only_select !== true) {
            // selection event를 전달받았을 때
            // stack layer가 떠 있다면 빨리 사라지도록 하기 위해서
            // 해당 layer를 hide 처리함 (보완코드로써 추후 삭제될 수도 있음)
            // TODO Dongho Lee 2016.06.22
            // 오디오 채널의 경우 OTS 도 OTV와 동일하게 서비스 해야 하므로 아래 코드는 다시 살펴봐야 한다.
            if (stateManager.isTVViewingState() === true) {
                var last_layer = getLastStackLayer();
                if (last_layer !== null) {
                	// OTS audio channel의 경우 last_layer를 hide 처리함
                	// 이렇게 해야지 clearStack이 호출되지 않아서
                	// 정상적으로 일반 전체편성표에서 오디오 채널 dca tune시
                	// 오디오 편성표가 노출된다.
                    // 2017.04.25 dhlee 코드 순서 변경
                    if (navAdapter.isAudioChannel(channel) === true) {
                        iframeManager.changeIframe();
                        if (KTW.CONSTANT.IS_OTS) {
                            last_layer.hide();
                        }
                    }
                    /*
                	if (isSkyAudioChannel(select_channel) === true) {
                		last_layer.hide();
                	}
                    if (navAdapter.isAudioChannel(channel) === true) {
                        iframeManager.changeIframe();
                    }
                    */
                }
            }
        }


        // AV 채널이 아닌 곳으로 channel 전환 시
        // PIP channel control을 의도적으로 stop 한다.
        // 이렇게 하면 자연스럽게 멀티화면 PIP가 종료된다.
        if (isAVChannel(channel) === false) {
            var pip_cc = navAdapter.getChannelControl(NAV_DEF_CONTROL.PIP);
            if (pip_cc !== null && pip_cc.getState() === NAV_DEF_CONTROL.STATE.STARTED) {
                pip_cc.stop();
            }

            /**
             * [dj.son] [WEBIIIHOME-919] DATA 채널로 튠시 deactivate 처리
             */
            layerManager.deactivateLayer({id: KTW.ui.Layer.ID.MINI_EPG});

            /**
             * [dj.son] [WEBIIIHOME-407] event 로 넘어오는 데이터는 channel object 가 아니라 channel.data 가 넘어오기 때문에,
             * 추후 해당 data 로 getSIDescriptor 등의 API 호출시 undefined 가 발생됨
             * 따라서 event 로 넘어오는 channel.data 가 아니라, 해당 channel 을 설정해야함
             */
            curr_channel = select_channel;

            // [dj.son] AppServiceManager 사용
            //stateManager.changeState(KTW.CONSTANT.SERVICE_STATE.DATA, {"type" : KTW.CONSTANT.APP_TYPE.DATA_CHANNEL });
            appServiceManager.changeService({
                nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                obj: {"type" : KTW.CONSTANT.APP_TYPE.DATA_CHANNEL },
                changeForced: true,
                visible: true
            });
            return;
        }
        else {
            
        }
        
        // VOD 상태에서 DCA로 tune 한 경우
        var need_exit_service = false;
        var exit_obj = undefined;
        if (stateManager.isVODPlayingState() === true) {
            need_exit_service = true;
            exit_obj = { "channel" : select_channel, "tune" :  navAdapter.isAudioChannel(select_channel) ? "true" : "false", "from": "dca"};
        }
        /*else if (stateManager.state === KTW.CONSTANT.SERVICE_STATE.DATA) {
            need_exit_service = true;
        }*/
        
        if (need_exit_service === true) {
            curr_channel = select_channel;
            // [dj.son] AppServiceManager 사용
            //stateManager.exitToService(exit_obj, false);
            appServiceManager.changeService({
                nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING,
                obj: exit_obj,
                visible: false
            });
            return;
        }
        
        // Audio, Video channel 인 경우에 back channel 로 설정
        // 독립형 서비스 채널 및 OTS audio 채널은 제외함
        // 2017.04.24 dhlee 맞춤 프로모 채널도 제외함
        if (isAVChannel(curr_channel) === true) {
            if (navAdapter.isAudioChannel(curr_channel) === false && navAdapter.isHiddenPromoChannel(curr_channel) === false) {
                back_channel[0] = curr_channel;
                if (curr_channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS &&
                        curr_channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                    back_channel[1] = curr_channel;
                    log.printDbg("onChannelSelectionEvent() back_sky_channel = " + back_channel[1].majorChannel, log_prefix);
                }
                else {
                    back_channel[2] = curr_channel;
                    log.printDbg("onChannelSelectionEvent() back_olleh_channel = " + back_channel[2].majorChannel, log_prefix);
                }
                log.printDbg("onChannelSelectionEvent() back_channel = " + back_channel[0].majorChannel, log_prefix);
            }
        }
        
        curr_channel = select_channel;
        
        // Audio, Video channel 을 구분하여 lcw channel 로 설정
        // 독립형 서비스 채널은 제외함
        // 2017.04.24 dhlee 맞춤 프로모 채널은 제외함
        if (isAVChannel(select_channel) === true && navAdapter.isHiddenPromoChannel(select_channel) === false) {
            if (navAdapter.isAudioChannel(select_channel) === true) {
                lcw_channel[1] = select_channel;
                log.printDbg("onChannelSelectionEvent() lcw_audio_channel = " + lcw_channel[1].majorChannel, log_prefix);
            }
            else {
                lcw_channel[0] = select_channel;
                log.printDbg("onChannelSelectionEvent() lcw_video_channel = " + lcw_channel[0].majorChannel, log_prefix);
            }
        }
        
        if (navAdapter.isAudioChannel(select_channel) === true) {
            if (last_only_select === true) {
                // 만약 편성표 등에서 only_select 하는 경우에는 iframe만 변경하도록 한다.
                KTW.managers.service.PromoManager.stopTrigger();
                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , select_channel);
            }
        }else {
            if(KTW.managers.service.AudioIframeManager.getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
                KTW.managers.service.AudioIframeManager.setAudioFullEpgExit();
            }
        }

    }
    
    /**
     * VideoBrocastObject#ChannelChangeSucceeded Event callback
     */
    function onMainChannelChangeSucceeded(event) {
        onChannelChangeSucceeded(event, true);

        if (!stateManager.isRunningState()) {
            setTimeout(function () {
                KTW.main.checkAppReadyOnBooting();
            }, 10);
        }
    }
    
    function onPipChannelChangeSucceeded(event) {
        onChannelChangeSucceeded(event, false);
    }
    
    function onStitchingChannelChangeSucceeded(event) {
    	
    	var channel = event.channel;
        log.printDbg("onStitchingChannelChangeSucceeded() channel = " + JSON.stringify(channel), log_prefix);
        
        var channel_event = {
            "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
            "channel" : channel,
            "permission" : NAV_DEF_CHANNEL.PERM.OK
        }
    	
        var length = channel_event_listeners.length;
        for (var i = 0; i < length; i++) {
            channel_event_listeners[i](channel_event);
        }
    }

    /**
     * VideoBroadcastObject#ChannelChangeSucceeded event callback
     *
     * @param event
     * @param is_main
     */
    function onChannelChangeSucceeded(event, is_main) {
        
        if (skip_channel_event === true) {
            log.printDbg("onChannelChangeSucceeded() skip onChannelChangeSucceeded", log_prefix);
            return;
        }
        
        var channel = event.channel;
        log.printDbg("onChannelChangeSucceeded() channel = " + JSON.stringify(channel), log_prefix);
        
        // Video/Audio 채널이 아닌  경우무시하도록 함.
        if (isAVChannel(channel) === false) {
            log.printDbg("onChannelChangeSucceeded() not AV channel... so return", log_prefix);
            // 2017.08.25 dhlee
            // CUG 서비스를 시작채널로 설정한 상태에서 부팅 후 CUG 종료하게 되면 홈메뉴가 떠야 하는데 뜨지 않는 이슈가 있다.
            // 이 이슈를 해결하기 위한 work-around를 아래와 같이 적용한다.
            // 2번의 channel selection이 일어나는데 한번은 ChannelControl이 초기화되기 전 navigator에 의해 tune 되고
            // 이후 홈에 의해서 only select로 tune이 되는데 이미 같은 채널이므로(?)
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
                appServiceManager.changeService({
                    nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: {"type" : KTW.CONSTANT.APP_TYPE.DATA_CHANNEL },
                    changeForced: true,
                    visible: true
                });
            }
            return;
        }
        
        var is_same_channel = isSameChannel(channel);
        log.printDbg("onChannelChangeSucceeded() isSameChannel = " + is_same_channel + ", last_perm = " + last_perm, log_prefix);
        
        if (is_main === true) {
            if (is_same_channel === true && last_perm < NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
            	
            	// 양방향 서비스에서 나가기 및 핫키 등에 의해 복귀할 경우
            	// onChannelChangeSucceeded 전달된 이후에 obs_show event 가 전달된다.
            	// 그런데 양방향 서비스 진입 후 로딩 화면이 보일 때 DCA로 tune 가능한 경우가 있는데
            	// 이 경우에는 obs_show가 불리지 않고 channel tune이 된다.
            	// 그래서 핫키에 의해서 종료되는 경우에는 LayerManager에서 obs_show로 처리하도록 하고
            	// 이외의 경우에는 timing 을 고려하여 여전히 DATA 상태일 경우
            	// TV_VIEWING 상태로 전환하도록 한다.
            	// 기존에는 onChannelSelectionEvent 에서 처리하면서 UHD STB 등에서 오히려 빨리
            	// 다음 로직이 수행되는 문제로 인해서 onChannelChangeSucceeded 처리되도록 했다.
            	if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                    this_obj._clearCheckServiceStateTimer();

                    checkServiceStateTimer = setTimeout(function() {
                        checkServiceStateTimer = null;

                        // 물론 내용 확인 필요
                        // 실제 tune 하는 채널이 AV 채널일 경우에만 상태 전환하도록 함
                        // (이렇게 하지 않으면 무작정 상태전환할 수 있기 때문에
                        // exitToService() 호출 시점에 현재채널로 재확인)
                        if (isAVChannel(curr_channel) === true && stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                            /**
                             * [dj.son] obs_show 메세지가 전달되지 않은 경우, service state 처리
                             * - 특별히 AppStore 뿐만 아니라, 일반적인 바운드 앱들에 대해서 예외처리 되도록 수정
                             * - 이전 state 가 OTHER_APP_ON_XXX 상태이면, OTHER_APP_ON_TV 상태로 변경
                             * - 실제로 바운드 앱이 뜨지 않더라도, TV_VIEWING 상태로 전환하는 예외처리가 LayerManager 에 있으므로 이슈 없을것이라 판단됨
                             */
                            var nextStage = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;
                            var isVisible = false;

                            if (stateManager.preServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV
                                || stateManager.preServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                                nextStage = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                                isVisible = true;
                            }

                            appServiceManager.changeService({
                                nextServiceState: nextStage,
                                visible: isVisible
                            });
                        }

                        // DATA 상태에서 exit 할 때
                        // 실패한 경우라면 changeChannel을 호출하지 않도록 함.
                        // 이렇게 해야 TV viewing 상태가 아닌데 miniEPG가 노출되거나
                        // 하는 증상을 해결할 수 있음.

                        // 데이터 채널에서 핫키에 의해 tune 되는 경우
                        // obs_show 에서 Normal Layer (홈메뉴, 편성표 등) 를 activate 하므로,
                        // last layer 의 type 및 showing 여부 검사 후 channelChanged 수행
                        var lastLayer = KTW.ui.LayerManager.getLastLayerInStack();
                        if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING
                            && !(lastLayer && lastLayer.isShowing() && lastLayer.type > KTW.ui.Layer.TYPE.TRIGGER)) {
                            channelChanged(true);
                        }
                        else {
                            /**
                             * [dj.son] 미니 epg 에 channel 변경을 알리기 위해 visible false 로 강제 호출하도록 수정
                             * - 제한 채널 -> 데이터 채널 -> 핫키 -> 이때 미니 epg 는 이전 채널에 대한 data 만 가지고 있음
                             * - 미니 epg 가 trigger 타입이기 때문에, Channel change 상황에서 타 Layer 가 떠 있어도 linkage 값에 따라 show 가 안불리게 됨
                             */
                            if(navAdapter.isAudioChannel(channel) !== true) {
                                layerManager.activateLayer({
                                    obj: {
                                        id: KTW.ui.Layer.ID.MINI_EPG,
                                        type: KTW.ui.Layer.TYPE.TRIGGER,
                                        priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                        linkage: false,
                                        params: {
                                            data : event
                                        }
                                    },
                                    clear_normal: false,
                                    visible: true
                                });
                                
                            }

                            // 2017.07.19 dhlee
                            // 이후 별도 channel event listener를 add한 곳에 전달
                            var length = channel_event_listeners.length;
                            var channel_event = {
                                "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
                                "channel" : channel,
                                "permission" : last_perm
                            };
                            for (var i = 0; i < length; i++) {
                                channel_event_listeners[i](channel_event);
                            }
                        }
                        //if (exit_service !== false) {
                        //	// state 전환은 obs_show 메세지에 의해 처리되더라도
                        //    // playChannel은 무조건 호출되도록 처리함
                        //    channelChanged(true);
                        //}
                        //channelChanged(true);

                        /*
                        if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                            // obs_show 에 의해 이미 visible 처리는 되었기 때문에
                            // 이곳에서는 channelControl#resume 시 의도적으로 reset을
                            // 하지않도록 reset 값을 fasle로 처리함

                            // TODO exit_service 에 대해 고민해봐야 함
                            // [dj.son] AppServiceManager 사용
                            //exit_service = stateManager.exitToService(undefined, false, false);
                            var nextStage = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;
                            if (stateManager.isShowAppStore) {
                                nextStage = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                            }
                            appServiceManager.changeService({
                                nextServiceState: nextStage
                            });
                        }

                        // DATA 상태에서 exit 할 때
                        // 실패한 경우라면 changeChannel을 호출하지 않도록 함.
                        // 이렇게 해야 TV viewing 상태가 아닌데 miniEPG가 노출되거나
                        // 하는 증상을 해결할 수 있음.

                        // TODO 일단은 channelChanged 수행, exit_service 에 대해 고민해봐야 함
                        if (stateManager.serviceState != KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                            channelChanged(true);
                        }
                        //if (exit_service !== false) {
                        //	// state 전환은 obs_show 메세지에 의해 처리되더라도
                        //    // playChannel은 무조건 호출되도록 처리함
                        //    channelChanged(true);
                        //}
                        //channelChanged(true);
                        */
                        request_channel_change = false;
                    }, 500);
            	}
            	else {
            		channelChanged(false);
            		request_channel_change = false;
            	}
            }
            else {
                /**
                 * [dj.son] [WEBIIIHOME-3568] request_channel_change reset 구문 추가
                 */
                request_channel_change = false;
            }
        }
        else {
//            var playable = false;
//            if (is_same_channel === false) {
//                playable =  true;
//            }
//            
//            last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
//            if (playable === true) {
//                curr_channel = navAdapter.getChannelBySID(channel.sid, channel.idType);
//                playChannel(curr_channel);
//            }

            // TODO 2017.02.15 dhlee 2.0 R7 로직 반영
        	//KTW.managers.service.ChannelPreviewManager.stop(false);
        	
        	// R3 Live 펌웨어(7월)에서는 멀티화면에서 SD채널이 없는 경우 tune이 안되는 현상이 있다
        	// 이를 보정하기 위한 코드를 멀티화면(pipView)에 추가하다보니 
        	// 채널변경 시에 동일 채널을 다시 tune 하는 경우가 생길 수 있다
        	// 그러나 이러한 경우에도 멀티화면 event가 전달되어야 보정코드를 정상적으로 수행할 수 있어서
        	// event로 전달받은 채널이 현재 채널과 동일하더라도 event를 전달하도록 수정함
        	var playable = true;
            //this_obj.vbo.addEventListener();
            var pip_cur_channel = this_obj.vbo.currentChannel;
            if (util.isValidVariable(pip_cur_channel) === true) {
            	log.printDbg("onChannelChangeSucceeded() pip_cur_channel = " + pip_cur_channel.majorChannel, log_prefix);
            	
            	if (isSameChannel(pip_cur_channel) === true) {
            		// caEvent를 먼저 전달받아서 이미 PERM 값이 PR_BLOCKED 이상인 경우에는
            		// channelChangeSucceeded event가 나중에 올라오는 경우가 있기 때문에
            		// 현재 채널과 pip vbo 채널이 동일할 경우 epg에 channel event를 전달하지 않도록 함
            		// 기존에는 PERM 값이 OK로 update되면서 미가입 iframe이 사라지는 문제 있었음.
                    // TODO 2017.02.15 dhlee 미리보기의 경우에도 동일하게 playable 하지 않도록 처리함
                    if (last_perm < NAV_DEF_CHANNEL.PERM.PR_BLOCKED || last_perm === NAV_DEF_CHANNEL.PERM.PREVIEW) {
                        playable = true;
                    }
                    /* TODO 2017.02.15 dhlee 2.0 R7 에서는 위와 같이 처리함
            		if (last_perm < NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
            			playable = false;
            		}
            		*/
            	}
            	else {
            		last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
            	}
            	
                curr_channel = pip_cur_channel;
            }
            else {
            	if (is_same_channel === false) {
                    curr_channel = navAdapter.getChannelBySID(channel.sid, channel.idType);
                }
            }
            
            if (playable === true) {
            	playChannel(curr_channel);
            }
            else {
            	// 빠르게 채널전환 할 경우 ChannelChangeSucceeded 보다
            	// cas svcStatus event가 빨리 올라오는 경우가 있음.
            	// 이렇게 되면 caEvent에 의해 PIP 채널은 tune한 채널과 동일해지는데
            	// last_perm 값은 UNKNOWN 이거나 OK 상태이다.
            	// 이렇게 되면 playChannel을 호출하지 않도록 하는데
            	// 적어도 PipView에는 event를 전달하여
            	// channel 전환이 되었음을 알려주도록 한다.
            	if (last_perm < NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
            		var channel_event = {
                        "type" : NAV_DEF_CHANNEL.EVENT.PERM_UPDATED,
                        "channel" : curr_channel,
                        "permission" : NAV_DEF_CHANNEL.PERM.OK
                    };
                	notifyChannelEvent(channel_event, false);
            	}
            }
            
            request_channel_change = false;
        }
    }
    
    /**
     * MAIN vbo상 onChannelChangeSucceeded event callback 시
     * miniEPG 및 audio FullEPG 처리를 위한 별도 함수
     */
    function channelChanged(is_sync) {
        log.printDbg("channelChanged() is_sync = " + is_sync + ", last_only_select : " + last_only_select, log_prefix);
    	// selection event를 받은 channel인 경우에만 처리하도록 함
        // 실질적으로 빠른 zapping 시점에는 selection event만 전달되기 때문에
        // 문제는 없으나 혹시 중간에 succeeded event가 전달되는 경우
        // miniEPG가 update 되는 상황을 줄이기 위해서 이렇게 처리함
        
        // PR_BLOCKED 이상의 값은 caEvent를 통해서 변경되는 값이므로
        // 채널 전환 자체적으로는 해당 permission 변경 외에는 처리하지 않도록 함
        if (navAdapter.isAudioChannel(curr_channel) === false) {
        	if (is_sync === false) {
        		setTimeout(function() {
                    playChannel(curr_channel);
                }, 0);
        	}
        	else {
        		playChannel(curr_channel);
        	}
        }
        else {
        	// 정상적으로 select 하는 경우에만 동작하도록 함
        	if (last_only_select !== true) {
        		// OTS 형상에서 audio channel로 tune 시 전체편성표를 노출한다.
		        // 단, DCA 로 tune 하거나 tune한 채널이 audio 채널일 경우이고
		        // tune만 하는 경우에는 처리하지 않는다.
            	showAudioFullEpg(curr_channel);
        	}
        	else {
        		var channel_event = {
                    "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
                    "channel" : curr_channel,
                    "permission" : NAV_DEF_CHANNEL.PERM.OK
                }
        		var length = channel_event_listeners.length;
                for (var i = 0; i < length; i++) {
                    channel_event_listeners[i](channel_event);
                }
        	}
        }
    }
    
    function onMainChannelChangeError(event) {
        onChannelChangeError(event, true);

        if (!stateManager.isRunningState()) {
            setTimeout(function () {
                KTW.main.checkAppReadyOnBooting();
            }, 10);
        }
    }
    
    function onPipChannelChangeError(event) {
        onChannelChangeError(event, false);
    }
    
    function onStitchingChannelChangeError(event) {
    	
    	var channel = event.channel;
        log.printDbg("onStitchingChannelChangeError() channel = " + JSON.stringify(channel), log_prefix);
        
        var channel_event = {
            "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
            "channel" : channel,
            "permission" : NAV_DEF_CHANNEL.PERM.OK,
            "blocked_reason" : event.errorState
        }
    	
        var length = channel_event_listeners.length;
        for (var i = 0; i < length; i++) {
            channel_event_listeners[i](channel_event);
        }
    }

    /**
     * VideoBrocastObject#ChannelChangeError Event callback
     *
     * @param event
     * @param is_main
     */
    function onChannelChangeError(event, is_main) {
        
        if (skip_channel_event === true) {
            log.printDbg("onChannelChangeError() skip onChannelChangeSucceeded", log_prefix);
            return;
        }
        
        var channel = event.channel;
        log.printDbg("onChannelChangeError() channel = " + JSON.stringify(channel), log_prefix);
        
        // Video/Audio 채널이 아닌  경우무시하도록 함.
        if (isAVChannel(channel) === false) {
            log.printDbg("onChannelChangeError() not AV channel... so return", log_prefix);
            return;
        }
        
        var error_state = event.errorState;
        log.printDbg("onChannelChangeError() error_state = " + error_state, log_prefix);
        
        var is_same_channel = isSameChannel(channel);
        log.printDbg("onChannelChangeError() is_same_channel = " + is_same_channel, log_prefix);
        log.printDbg("onChannelChangeError() is_main = " + is_main, log_prefix);
        if (is_main === true) {
            if (is_same_channel === true) {
                
                var playable = false;
                var is_blocked = false;
            	// selection event를 받은 channel인 경우에만 처리하도록 함
                // 실질적으로 빠른 zapping 시점에는 selection event만 전달되기 때문에
                // 문제는 없으나 혹시 중간에 succeeded event가 전달되는 경우
                // miniEPG가 update 되는 상황을 줄이기 위해서 이렇게 처리함
                // error_state가 3(blocked)이외에 AV stream이 없는 경우 등을 포함해서
                // miniEPG에 error event가 전달될 수 있도록 함
                error_state = checkChannelChangeErrorState(error_state);
                if (error_state !== -1) {
                	playable = true;
                	
                	if (error_state === NAV_DEF_CHANNEL.BLOCKED_REASON.USER_BLOCKED) {
                		is_blocked = true;
                	}
                }

                // [dhlee] 2017.02.15 우선 에러만 안나도록 주석처리 할건 해 두고 수정할건 해두자...
                // DATA 상태인 경우 state 전환 이후 playChannel 되도록 함
                // [17/01/12] "obs_show" 메세지 지연을 고려하여 timeout 값을 500ms로 조정함
                // 이미 ChannelChangeSucceded event에서는 적용되어 있는 상태라 그대로 적용함(KTWEB-263)
                if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                    this_obj._clearCheckServiceStateTimer();

                    checkServiceStateTimer = setTimeout(function() {
                        checkServiceStateTimer = null;

                        // 실제 tune 하는 채널이 AV 채널일 경우에만 상태 전환하도록 함
                        // (이렇게 하지 않으면 무작정 상태전환할 수 있기 때문에
                        // exitToService() 호출 시점에 현재채널로 재확인)
                        if (isAVChannel(curr_channel) === true && stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                            /**
                             * [dj.son] obs_show 메세지가 전달되지 않은 경우, service state 처리 (ChannelChangeSucceded 와 동일 로직으로 처리)
                             * - 특별히 AppStore 뿐만 아니라, 일반적인 바운드 앱들에 대해서 예외처리 되도록 수정
                             * - 이전 state 가 OTHER_APP_ON_XXX 상태이면, OTHER_APP_ON_TV 상태로 변경
                             * - 실제로 바운드 앱이 뜨지 않더라도, TV_VIEWING 상태로 전환하는 예외처리가 LayerManager 에 있으므로 이슈 없을것이라 판단됨
                             */
                            var nextStage = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;
                            var isVisible = false;

                            if (stateManager.preServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV
                                || stateManager.preServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                                nextStage = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                                isVisible = true;
                            }

                            appServiceManager.changeService({
                                nextServiceState: nextStage,
                                visible: isVisible
                            });
                        }

                        /**
                         * [dj.son] state 전환은 obs_show 메세지에 의해 처리되더라도 blocked 처리는 되어야 하므로 playChannel 은 무조건 호출,
                         *          단, OTHER_APP 에서 TV_VIEWING 상태로 온 상황은 기존 홈포탈 화면이 resume 되어 있을 수 있으므로 (obs_show)
                         *          현재 보여지고 있는 Layer 의 상태를 보고 clear_normal 여부를 결정한다.
                         */
                        if (playable === true) {
                            var lastLayer = KTW.ui.LayerManager.getLastLayerInStack();

                            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING
                                && !(lastLayer && lastLayer.isShowing() && lastLayer.type > KTW.ui.Layer.TYPE.TRIGGER)) {
                                last_only_select = false;
                            }
                            else {
                                last_only_select = true;
                            }

                            playChannel(curr_channel, is_blocked, error_state);
                            resetOnlySelect();
                        }
                    }, 500);
                }
                else {
                    if (playable === true) {
                        // blocked channel
                        // R5 이슈 KTWHPTZOF-1663
                        // 타이밍 이슈. miniEpg 로 전달되는 콜백을 async 로 변경하여 수정
                        //
                        // 만약 ChanneChangeError event를 받은 뒤 timeout이 걸린 상태에서
                        // 편성표 핫키를 누를 경우 편성표가 실행된 이후에
                        // playChannel이 실행되면 miniEPG가 활성화 되면서 편성표가 종료되는데
                        // 이때 av resize가 정상적으로 되지 않는 문제가 발생한다.
                        // 그래서 이곳에서 timeout을 등록해 두고
                        // 만약 편성표 핫키 등에 의해 appMode가 FULL 모드 상태로 변경되면
                        // timeout을 제거하고 바로 playChannel이 수행되어
                        // miniEPG가 활성화 되지 않도록 하여 편성표가 정상적으로 노출되고
                        // 이후 채널 전환에 의해 편성표가 종료될 수 있도록 한다.
                        // 해당 증상은 사실 채널 전환 후 아주 빠른 타이밍에 편성표를
                        // 실행하는 경우에만 재현되는 것인데 1/5 확률로 발생하기 때문에
                        // 일단 workaround를 적용하도록 했다.
                        //delay_play_channel = undefined;
                        //clearDelayPlayTimer();
                        //
                        //// playChannel 함수 call 을 위한 property 설정
                        //delay_play_channel = {
                        //    channel : curr_channel,
                        //    blocked : is_blocked,
                        //    error : error_state
                        //};

                        //log.printDbg("onChannelChangeError() delay_play_timer start");
                        //delay_play_timer = setTimeout(function() {
                        //    playChannel(delay_play_channel.channel,
                        //        delay_play_channel.blocked,
                        //        delay_play_channel.error);
                        //
                        //    // playChannel 이후 timer 및 변수값 초기화
                        //    delay_play_channel = undefined;
                        //    delay_play_timer = null;
                        //}, 500);

                        /**
                         * [dj.son] 기존 2.0 로직은 KTWHPTZOF-1663 이슈로 인해 onChannelChangeError 에서 delay 를 주었지만,
                         *          3.0 은 현재 Layer 상태를 보고 keyPriority 설정을 하기 때문에 위 이슈가 발생되지 않음
                         *          따라서 delay 시간을 주는 로직 삭제하고 바로 playChannel 을 호출하도록 수정
                         */
                        playChannel(curr_channel, is_blocked, error_state);
                    }

                    request_channel_change = false;
                    resetOnlySelect();
                }
            	
                // TODO network 예외 시나리오
                // OTS STB에서 sky channel을 보는 상태에서 network 연결 해제후
                // olleh channel로 tune 할 경우 network error popup이 노출되어야 할 것으로 보이는데
                // 실질적으로 network error를 인지할 수 없다
                // 가능하다면 channel tune 되었는데 IPTV channel이고 network이 단절되어 있다면
                // network 오류 popup을 띄울 수는 있을 것 같다.
                // 또는 VOD 재생 시점에도 동일하게 처리를 해야 될 것으로 보인다.
                // network error를 정확하기 인지할 수 없어서 일단 그냥 무시하기로 함
                /*else if (error_state === 12) {
                    // CONTENT NOT FOUND error event
                    // tune fail by network disconnect
                    KTW.managers.device.NetworkManager.showNetworkErrorPopup();
                }*/
            }
        }
        else {
            last_perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
            KTW.managers.service.ChannelPreviewManager.stop(false);
            var playable = false;
//            if (is_same_channel === false) {
//                curr_channel = navAdapter.getChannelBySID(channel.sid, channel.idType);
//                playable = true;
//            }
//            else {
//                // 예전 펌웨어에서는 pip vbo 상 onChannelChangeError event 발생 시
//                // 이전에 succeeded channel값이 전달되는 문제가 있어서 
//                // 실제 PIP vbo의 current channel과 비교하여 처리하도록 workaround 적용
//                var pip_cur_channel = this_obj.vbo.currentChannel;
//                if (util.isValidVariable(pip_cur_channel) === true) {
//                	  if (channel.ccid === pip_cur_channel.ccid) {
//                        log.printDbg("onChannelChangeError() different pip current channel", log_prefix);
//                        
//                        curr_channel = pip_cur_channel;
//                        playable = true;
//                    }
//                    curr_channel = pip_cur_channel;
//                    playable = true;
//                }
//            }

            // 7월 펌웨어에서는 멀티화면 채널이 SD채널이 없는 경우
        	// tune을 실패로 인해서 channelChangeError event가 오는데
        	// channel 매개변수가 이전 채널로 전달된다.
        	// 이렇게 되면 event상 전달되는 channel로는 pip의 현재채널을
        	// 판단할 수 없기 때문에 vbo객체의 현재 채널로 miniEPG에
        	// 전달해야 정상적으로 iframe 등을 노출할 수 있다.
        	// 단, 10월 이후 적용되는 펌웨어에서는 channelChangeError event상
        	// 현재 채널이 넘어오기 때문에 workaround 로직이 아니더라도
        	// 정상동작한다.
            var pip_cur_channel = this_obj.vbo.currentChannel;
            if (util.isValidVariable(pip_cur_channel) === true) {
                curr_channel = pip_cur_channel;
                playable = true;
            }
            else {
            	if (is_same_channel === false) {
                    curr_channel = navAdapter.getChannelBySID(channel.sid, channel.idType);
                    playable = true;
                }
            }
            
            
            log.printDbg("onChannelChangeError() playable = " + playable, log_prefix);
            if (playable === true) {
            	error_state = checkChannelChangeErrorState(error_state);
                if (error_state !== -1) {
                    playChannel(curr_channel, true, error_state);
                }
            }
        }
        
        request_channel_change = false;

    }
    
    function checkChannelChangeErrorState (error_state) {
    	var blocked_state = -1;
        switch (error_state) {
            case 1 : // 신호미약
            case NAV_DEF_CHANNEL.BLOCKED_REASON.USER_BLOCKED :
            case NAV_DEF_CHANNEL.BLOCKED_REASON.UHD_CHANNEL :
            	blocked_state = error_state;
                break;
            case NAV_DEF_CHANNEL.BLOCKED_REASON.NO_SD_CHANNEL :
                /******* 펌웨어 수정되면 제거 *******/
            case 12 :
            	blocked_state = NAV_DEF_CHANNEL.BLOCKED_REASON.NO_SD_CHANNEL;
                break;
        }
        
        log.printDbg("checkChannelChangeErrorState() blocked_state = " + blocked_state, log_prefix);
        
        return blocked_state;
    }
    
    /**
     * KTW.nav.si.ChannelDatabaseManager channel list update callback
     */
    function onChannelListUpdate(id) {
        log.printDbg("onChannelListUpdate(" + id + ")", log_prefix);
        
        // VOD 상태에서 update 시 제한채널 체크하지 않도록 함
        if (stateManager.isTVViewingState() === false) {
        	log.printDbg("onChannelListUpdate() not tv viewing state... so return", log_prefix);
        	return;
        }
        
        // 제한 채널 ring 변경 시에만 제한 채널 여부를 판단하여 처리하도록 함
        // 자녀안심 채널 설정의 경우 현재 채널과 다르면
        // MW에서 채널전환이 되기 때문이다.
        
        var need_blocked_check = false;
        if (id !== undefined) {
            // 2017.06.05 dhlee
            // 3.0 에서는 키즈케어 ring을 사용하지 않음 (키즈 케어가 아닌 키즈 모드 전환 기능을 제공함)
            if (id === OIPF_DEF_VB.CHANNEL_CONFIG.FAVOURITE_BLOCKED ||
                id === OIPF_DEF_VB.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED) {
                need_blocked_check = true;
            }

            /*
        	if (id === OIPF_DEF_VB.CHANNEL_CONFIG.FAVOURITE_KIDS_CARE) {
        		//this_obj._setOnlySelect(true, false);
        	}
        	else if (id === OIPF_DEF_VB.CHANNEL_CONFIG.FAVOURITE_BLOCKED ||
        			id === OIPF_DEF_VB.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED) {
        		need_blocked_check = true;
        	}
        	*/
        }
        
        if (need_blocked_check === true) {
        	// channel list update 이후 현재 채널이 제한채널로 설정되면
            // stop channel 호출하고 blocked 처리함
            // 반대로 제한채널에서 해제되는 경우에는 다시 tune 처리함
            var is_blocked = navAdapter.isBlockedChannel(curr_channel);
            
            log.printDbg("onChannelListUpdate() is_blocked = " + is_blocked, log_prefix);
            log.printDbg("onChannelListUpdate() last_perm = " + getPermString(last_perm), log_prefix);
            
            if ((is_blocked === true && last_perm !== NAV_DEF_CHANNEL.PERM.BLOCKED) ||
                    (is_blocked === false && last_perm === NAV_DEF_CHANNEL.PERM.BLOCKED)) {
                log.printDbg("onChannelListUpdate() changed permission for current channel...", log_prefix);
                /**
                 * 2017/0605 jjh1117
                 *  미리보기 상태이면 미리보기  화면 Stop하도록 수정함.
                 *  Jira WEBIIIHOME-1829 이슈 사항
                 */
                if(last_perm === NAV_DEF_CHANNEL.PREVIEW) {
                    KTW.managers.service.ChannelPreviewManager.stop(control_type === NAV_DEF_CONTROL.MAIN?true:false);
                }
                this_obj.changeChannel(curr_channel, true);
            }
        }
    }
    
    /**
     * KTW.ca.CaHandler channel status callback
     */
    function onReceiveChannelStatus(sid, channel_status, preview_data) {
        log.printDbg("onReceiveChannelStatus(" + sid + ", " + getChannelStatusString(channel_status) + ")", log_prefix);
        
        // VOD 상태에서는 main 화면에 대한 CaEvent 처리를 하지 않도록 함
        // 만약 LCW channel이 미가입 채널 상태에서 VOD 진입 후
        // 멀티화면에서 LCW 채널로 tune 할 경우 권한 처리를 하면서
        // 미가입 iframe이 노출되는 문제가 있어서 수정함
        if (control_type === NAV_DEF_CONTROL.MAIN) {
        	if (stateManager.isVODPlayingState() === true) {
        		log.printDbg("onReceiveChannelStatus() current vod state... so return", log_prefix);
        		return;
        	}
        }
        
        // PIP vbo의 경우 caEvent가 onChannelChangeSucceeded 보다
        // 빨리 callback 되는 경우가 있어서 의도적으로 vbo#currentChannel 값을
        // 비교하여 channel 값을 수정하도록 함
        if (control_type === NAV_DEF_CONTROL.PIP) {
        	var pip_cur_channel = this_obj.vbo.currentChannel;
            if (curr_channel === null) {
            	curr_channel = pip_cur_channel;
            }
            else if (util.isValidVariable(pip_cur_channel) === true && curr_channel !== null) {
            	if (curr_channel.ccid !== pip_cur_channel.ccid) {
            		curr_channel = pip_cur_channel;
            	}
            }
        }
        
        if (isValidCaChannel(false) === false) {
            log.printDbg("onReceiveChannelStatus() not valid current channel... so return", log_prefix);
            return;
        }
        
        if (curr_channel !== null && sid === curr_channel.sid) {
            var perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
            
            // cas channel_status와 다른 상태로 변경되는 경우에만 처리
            if (channel_status === caHandler.CHANNEL_STATUS.CAN_DESCRAMBLE ||
                    channel_status === caHandler.CHANNEL_STATUS.NOT_SCRAMBLED) {
                if (last_perm !== NAV_DEF_CHANNEL.PERM.OK) {
                    if (last_perm === NAV_DEF_CHANNEL.PERM.PREVIEW) {
                        // 채널 미리보기 stop
                        if (control_type === NAV_DEF_CONTROL.MAIN) {
                            KTW.managers.service.ChannelPreviewManager.stop(control_type === NAV_DEF_CONTROL.MAIN?true:false);
                        }
                    }

                    perm = NAV_DEF_CHANNEL.PERM.OK;
                    if (is_stop_av === true) {
                        // 미가입 event 상태에서 descramble 된 경우
                        // 미리보기 채널에서 가입된 상태로 전환된 경우도 포함되기 때문에
                        // 의도적으로 startVideoAndAudio 호출하여 AV재생
                        navAdapter.startAV();
                        is_stop_av = false;
                    }
                }
            }
            else if (channel_status === caHandler.CHANNEL_STATUS.NOT_SUBSCRIBED) {
                if (last_perm !== NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED) {
                    perm = NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED;
                    if (control_type === NAV_DEF_CONTROL.MAIN) {
                        navAdapter.stopAV();
                        is_stop_av = true;
                    }
                    if (last_perm === NAV_DEF_CHANNEL.PERM.PREVIEW) {
                        // 채널 미리보기 stop
                        if (control_type === NAV_DEF_CONTROL.MAIN) {
                            KTW.managers.service.ChannelPreviewManager.stop(control_type === NAV_DEF_CONTROL.MAIN?true:false);
                        }
                    }
                }
            }
            else if (channel_status === caHandler.CHANNEL_STATUS.PARENTAL_RATING_LIMIT) {
                if (control_type === NAV_DEF_CONTROL.MAIN) {
                    KTW.managers.service.ChannelPreviewManager.stop(control_type === NAV_DEF_CONTROL.MAIN?true:false);
                }
                if (last_perm !== NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
                    perm = NAV_DEF_CHANNEL.PERM.PR_BLOCKED;
                }
                
                // 연령제한 걸리는 경우 현재채널 stop 처리함
                // 이후 해제할 경우 miniEPG 쪽에서 changeChannel()을 호출함
                // 이렇게 해야 연령제한에 대한 cas callback이 빨리 오더라...
                // 그런데 기본적인 동작상황으로 보았을 때는 이해안감
                // 단, PIP 멀티화면의 경우에는 vbo#stop 할 경우
                // DCA로 채널 tune 시 PIP 채널 변경이 되지 않기 때문에
                // 처리하지 않도록 함
                // 연령제한 값을 변경하는 경우 다시 setChannel을 하기 때문에
                // 다시 PR_BLOCK caEvent가 올라오므로 무조건 stop 처리하도록 함
                // 예를 들어 현재 15세 제한 채널인데 7세 설정에서 12세 설정으로 변경시에도
                // 다시 PR_BLOCK event가 올라오기 때문에 stop 처리 필요함
                var stop = true;
                if (control_type === NAV_DEF_CONTROL.PIP &&
                		this_obj.getPipMode() === KTW.nav.Def.PIP_MODE.MULTI_VIEW) {
                	stop = false;
                }
                
                if (stop === true) {
                	this_obj.stopChannel();
                }
            }
            else if (channel_status === caHandler.CHANNEL_STATUS.PREVIEW) {
                if (last_perm !== NAV_DEF_CHANNEL.PERM.PREVIEW) {
                    perm = NAV_DEF_CHANNEL.PERM.PREVIEW;
                    
                    // 채널 미리보기 start
                    if (control_type === NAV_DEF_CONTROL.MAIN) {
                        KTW.managers.service.ChannelPreviewManager.start(true , preview_data,curr_channel);
                    }else {
                        KTW.managers.service.ChannelPreviewManager.start(false , preview_data,curr_channel);
                    }
                }else {
                    perm = NAV_DEF_CHANNEL.PERM.PREVIEW;
                    if (control_type === NAV_DEF_CONTROL.MAIN) {
                        if(KTW.managers.service.ChannelPreviewManager.isstart() === false) {
                            KTW.managers.service.ChannelPreviewManager.start(true , preview_data,curr_channel);
                        }else {
                            KTW.managers.service.ChannelPreviewManager.start(false , preview_data,curr_channel);
                        }
                    }
                }
            }
            
            checkCaPermission(perm, preview_data);
        }
    }
    
    function onReceiveMmiEvent(type, dialogue_id, is_main, message) {
        log.printDbg("onReceiveMmiEvent() type = " + type, log_prefix);
        log.printDbg("onReceiveMmiEvent() dialogue_id = " + dialogue_id, log_prefix);
        log.printDbg("onReceiveMmiEvent() is_main = " + is_main, log_prefix);
        log.printDbg("onReceiveMmiEvent() last_perm = " + last_perm, log_prefix);

        if (isValidCaChannel(true) === false) {
            log.printDbg("onReceiveMmiEvent() not valid current channel... so return", log_prefix);
            return;
        }
        
        // MAIN or PIP event를 구분해서 처리하도록 함
        if ((control_type === NAV_DEF_CONTROL.MAIN && is_main === false) ||
                (control_type !== NAV_DEF_CONTROL.MAIN && is_main === true)) {
            return;
        }
        
        // skip_channel_event 값이 true 면 mmi event 처리하지 않도록 함
        if (skip_channel_event === true) {
            log.printDbg("onReceiveMmiEvent() skip mmi event", log_prefix);
            return;
        }
        
        // 시청시간 제한 시 mmiEvent가 올 수 있는데 해당 경우는 무시하도록 함
        if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
	        log.printDbg("onReceiveMmiEvent() time restricted mode... so return", log_prefix);
            return;
        }

        // 2017.08.30 dhlee
        // https://issues.alticast.com/jira/browse/WEBIIIHOME-3215 이슈 수정
        // 키즈 모드 ON/OFF 시 PR 변경에 따른 mmi event는 무시하기 위한 flag 추가
        if (KTW.managers.service.KidsModeManager.isPRChanging()) {
            log.printDbg("onReceiveMmiEvent() change KIDS MODE... so return", log_prefix);
            return;
        }

        /**
         * jjh1117 2017.01.19
         * TODO 아래 부분 추후 확인 해봐야 함.
         */
        // // stitching service 상태에서는 mmiEvent를 무시하도록 함
        // // stop 처리되면서 AV black 되는 이슈 있었음
        // if (homeImpl.stitching_prev_channel !== null) {
        // 	log.printDbg("onReceiveMmiEvent() stitching service state... so return", log_prefix);
        // 	return;
        // }
        
        var perm = NAV_DEF_CHANNEL.PERM.UNKNOWN;
        
        if (type === mmiHandler.MMI_EVENT.TYPE.CLOSE) {
        	// CLOSE type event callback 시
        	// 이전 Permission 상태일 때에만 PERM_OK 처리하도록 함
            switch(dialogue_id) {
                case mmiHandler.MMI_EVENT.DIALOGUE_ID.CANT_BUY_THE_CONTENT :
                	if (last_perm === NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED) {
                		perm = NAV_DEF_CHANNEL.PERM.OK;
                	}
                	break;
                case mmiHandler.MMI_EVENT.DIALOGUE_ID.RATING_LIMIT_EXCEEDED :
                	if (last_perm === NAV_DEF_CHANNEL.PERM.PR_BLOCKED) {
                		perm = NAV_DEF_CHANNEL.PERM.OK;
                	}
                	break;
                case mmiHandler.MMI_EVENT.DIALOGUE_ID.CONFIRM_PURCHASE_PREVIEW :
                	if (last_perm === NAV_DEF_CHANNEL.PERM.PREVIEW) {
                		perm = NAV_DEF_CHANNEL.PERM.OK;
                	}
                    break;
            }
            

        }
        else {
            switch(dialogue_id) {
                case mmiHandler.MMI_EVENT.DIALOGUE_ID.CANT_BUY_THE_CONTENT :
                    // non subscription
                    perm = NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED;
                    break;
                case mmiHandler.MMI_EVENT.DIALOGUE_ID.RATING_LIMIT_EXCEEDED :
                    // parental rating
                    perm = NAV_DEF_CHANNEL.PERM.PR_BLOCKED;
                    break;
                case mmiHandler.MMI_EVENT.DIALOGUE_ID.CONFIRM_PURCHASE_PREVIEW :
                    // sky choice PP
                    // sky choice channel인 경우에만 처리하도록 함
                    if (navAdapter.isSkyChoiceChannel(curr_channel) === true) {
                        perm = NAV_DEF_CHANNEL.PERM.PREVIEW;
                    }
                    break;
            }
        }
        
        log.printDbg("onReceiveMmiEvent() perm = " + getPermString(perm), log_prefix);
        
        checkCaPermission(perm);
    }
    
    function isValidCaChannel(is_mmi_event) {
        if (util.isValidVariable(curr_channel) === false) {
            return false;
        }
        
        if (isAVChannel(curr_channel) === false) {
            return false;
        }
        
        // mmi CLOSE event가 timeout에 의해 전달되기 때문에
        // olleh channel로 전환했을 때 제한이 걸리는 채널이라면
        // 이전 채널에 대해서 늦게 올라온 mmiEvent에 의해서
        // iframe이 제거되는 문제가 있어서
        // 현재 채널 기준으로 skylife 채널이 아니라면 무시하도록 처리함.
        if (is_mmi_event === true) {
        	if (curr_channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S &&
        			curr_channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) {
        		return false;
        	}
        }
        
        return true;
    }

    /**
     * 2017.02.15 dhlee 2.0 R7 로직 적용
     */
    function clearDelayPlayTimer() {
        if (delay_play_timer !== null) {
            log.printDbg("clearDelayPlayTimer()");

            clearTimeout(delay_play_timer);
            delay_play_timer = null;
        }
    }

    /**
     * 2017.02.16 dhlee
     * AppModeManager listener callback
     *
     * @param mode
     */
    function onAppModeChanged(mode) {
        // appMode가 NONE에서 다른 상태로 변경 시에
        // delay playChannel timer가 동작 중이면 timer를 중지하고
        // channelEvent를 miniEPG에 전달한다.
        if (last_app_mode === KTW.CONSTANT.APP_MODE.NONE &&
            mode !== KTW.CONSTANT.APP_MODE.NONE) {
            clearDelayPlayTimer();

            if (delay_play_channel) {
                log.printDbg("onAppModeChanged() delay_play_channel execute!!");
                var perm = checkPermission(delay_play_channel.channel, delay_play_channel.blocked);

                log.printDbg("onAppModeChanged() delay_play_channel : perm = " + getPermString(perm), log_prefix);
                log.printDbg("playChannel() delay_play_channel : last_perm = " + getPermString(last_perm), log_prefix);

                if (perm !== last_perm) {
                    // permission이 변경되는 경우
                    // background iframe 변경 및 channel select/stop 처리하도록 함
                    var channel_event = {
                        "type" : NAV_DEF_CHANNEL.EVENT.REQUESTED,
                        "channel" : delay_play_channel.channel,
                        "permission" : perm,
                        "blocked_reason" : delay_play_channel.channel.error
                    };

                    // 우선 main control 경우에만 iframe 처리하도록 함
                    // 단, 시청시간 제한 상태에서는 처리하지 않도록 함
                    // TODO 별도 iframe 처리를 해야 함
                    if (control_type === NAV_DEF_CONTROL.MAIN &&
                        stateManager.time_restrict_in_state === -1) {
                        setIframe(channel_event);
                    }
                    notifyChannelEvent(channel_event, false);

                    last_perm = perm;

                    delay_play_channel = undefined;
                }
            }
        }

        last_app_mode = mode;
    }
    
    function checkCaPermission (perm, preview_data) {
        log.printDbg("checkCaPermission() last_perm = " + getPermString(last_perm), log_prefix);
        
        // permission 처리가 안되었거나 마지막 permission 기준으로 변경이 없다면 처리하지 않음
        if (perm !== NAV_DEF_CHANNEL.PERM.UNKNOWN && last_perm !== perm) {
            
            var clear_stack = true;
            var channel_event = {
                    "type" : NAV_DEF_CHANNEL.EVENT.PERM_UPDATED,
                    "channel" : curr_channel,
                    "permission" : perm,
                    "preview_data" : preview_data
                };
            
            // onChannelSucceeded event 보다 빨리 올라오는 상황이 있다.
            // 이전 채널이 sky channel일 경우 CLOSE event가 다른 채널로 tune 되면서
            // 올라오는 경우에 발생할 수 있다.
            // 물론 근본적으로 이는 mmiEvent의 경우에도 channel 속성을 비교해서
            // 처리해야 하지만 현재는 일단 그대로 두고 permission비교처리한다.
            if (last_perm === NAV_DEF_CHANNEL.PERM.UNKNOWN) {
                channel_event.type = NAV_DEF_CHANNEL.EVENT.REQUESTED;
            }
            
            if (control_type === NAV_DEF_CONTROL.MAIN) {
				// OTS 채널에서 channel event 보다 caEvent가 먼저 전달되는 경우
				// permission이 unknown 상태라서 iframe이 update 되지 않는 문제가 있어서
				// 이 경우에도 의도적으로 처리될 수 있도록 forced 파라미터를 추가함
                var forced = last_perm === NAV_DEF_CHANNEL.PERM.UNKNOWN ? true : false;
                setIframe(channel_event, undefined, forced);
            }
            
            log.printDbg("checkCaPermission() last_only_select = " + last_only_select, log_prefix);

            var isShowMiniEpg = true;
            // channel select만 하는 경우에는 miniEPG를 활성화 하지 않도록 함
            if (last_only_select === true) {
                if (isVisibleMultiPIP() === true) {
                    isShowMiniEpg = false;
                    if (channel_event.permission === NAV_DEF_CHANNEL.PERM.BLOCKED ||
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.PR_BLOCKED ||
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED) {
                        isShowMiniEpg = true;
                    }
                }
                else {
                    isShowMiniEpg = false;
                }

                /**
                 * [dj.son] 어차피 ChannelChangeSucceeded 나 ChannelChangeError 에서 reset 해주는데, 여기서 굳이 해줘야 할 이유가 없음
                 * - channel change 없이 퍼미션만 변경된 경우에는, last_only_select 가 true 가 될 일이 없음
                 */
                //last_only_select = false;
            }
            else {
                if (isVisibleMultiPIP() === true) {
                    isShowMiniEpg = false;
                    if(channel_event.permission === NAV_DEF_CHANNEL.PERM.BLOCKED ||
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.PR_BLOCKED ||
                        channel_event.permission === NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED) {
                        isShowMiniEpg = true;
                    }
                }
                else {
                    isShowMiniEpg = true;
                }
            }

            /*
            // DATA 상태에서 채널 전환 시 onChannelChangeSucceeded event 보다
            // caEvent가 먼저 전달되는 경우가 있는데
            // 특정 채널로 setChannel 하거나 DCA로 tune 하는 경우이다.
            // 이때에는 마지막 화면이 노출되지 않는 상태이고
            // 핫키에 의한 처리도 하지 않아도 되기 때문에 바로 상태전환 후
            // channelEvent가 전달되어 miniEPG가 노출되도록 clear_stack 값을 true로 처리한다.
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                // TODO 고민해봐야 함
            	//stateManager.exitToService(undefined, false);
            	clear_stack = true;
            }

            last_perm = perm;

            notifyChannelEvent(channel_event, isShowMiniEpg);
            */
            // TODO 2017.02.15 dhlee 2.0 R7 로직 반영
            // DATA 상태에서 채널 전환 시 onChannelChangeSucceeded event 보다
            // caEvent가 먼저 전달되는 경우가 있는데
            // 특정 채널로 setChannel 하거나 DCA로 tune 하는 경우이다.
            // cas event가 전달되는 경우는 async하게 처리할 필요가 없기 때문에
            // 바로 상태 전환하면서 last_perm정보를 update 하도록 함
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                /**
                 * 곧바로 상태 변경, last_perm 변경, event notify 수행
                 * 추후 onChannelChangeSucceeded 가 와도 last_perm 에 따라서 iframe 표시됨
                 */
                appServiceManager.changeService({
                    nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING
                });
                last_perm = perm;
                notifyChannelEvent(channel_event, true);
            }
            else {
                var pre_last_perm = last_perm;
                last_perm = perm;
                notifyChannelEvent(channel_event, isShowMiniEpg , pre_last_perm);
            }
        }
    }
    
    function getChannelEventTypeString(type) {
        
        var type_string = "";
        switch (type) {
            case NAV_DEF_CHANNEL.EVENT.REQUESTED :
                type_string = "REQUESTED";
                break;
            case NAV_DEF_CHANNEL.EVENT.PERM_UPDATED :
                type_string = "PERM_UPDATED";
                break;
        }
        
        return type_string;
    }
    
    function getPermString(perm) {
        
        var perm_string = "";
        switch (perm) {
            case NAV_DEF_CHANNEL.PERM.UNKNOWN :
                perm_string = "PERM_UNKNOWN";
                break;
            case NAV_DEF_CHANNEL.PERM.OK :
                perm_string = "PERM_OK";
                break;
            case NAV_DEF_CHANNEL.PERM.BLOCKED :
                perm_string = "PERM_BLOCKED";
                break;
            case NAV_DEF_CHANNEL.PERM.PR_BLOCKED :
                perm_string = "PERM_PR_BLOCKED";
                break;
            case NAV_DEF_CHANNEL.PERM.NOT_SUBSCRIBED :
                perm_string = "PERM_NOT_SUBSCRIBED";
                break;
            case NAV_DEF_CHANNEL.PERM.PREVIEW :
                perm_string = "PREVIEW";
                break;
        }
        
        return perm_string;
    }
    
    function getChannelStatusString(status) {
        
        var status_string = "";
        switch (status) {
            case caHandler.CHANNEL_STATUS.CAN_DESCRAMBLE :
                status_string = "CAN_DESCRAMBLE";
                break;
            case caHandler.CHANNEL_STATUS.NOT_SCRAMBLED :
                status_string = "NOT_SCRAMBLED";
                break;
            case caHandler.CHANNEL_STATUS.NOT_SUBSCRIBED :
                status_string = "NOT_SUBSCRIBED";
                break;
            case caHandler.CHANNEL_STATUS.PARENTAL_RATING_LIMIT :
                status_string = "PARENTAL_RATING_LIMIT";
                break;
            case caHandler.CHANNEL_STATUS.PREVIEW :
                status_string = "PREVIEW";
                break;
        }
        
        return status_string;
    }
    
    this.getControlType = function() {
        return control_type;
    }

    /**
     * [dj.son] [WEBIIIHOME-3612] 타이머 clear 함수 추가
     */
    this._clearCheckServiceStateTimer = function () {
        clearTimeout(checkServiceStateTimer);
        checkServiceStateTimer = null;
    }
};

KTW.nav.ChannelControl.prototype = {
    init: function() {
        this._init();
    },
    start: function(mode, obj) {
        this._start(mode, obj);
    },
    stop: function() {
        this._stop();
    },
    resume: function(reset) {
        this._resume(reset);
    },
    pause: function(stop_channel) {
        this._pause(stop_channel);
    },
    getState: function() {
        return this._getState();
    },
    recheckPermission: function(forced) {
        this._recheckPermission(forced);
    },
    resetPerm: function (forced) {
        this._resetPerm(forced);
    },
    /**
     * return current channel
     */
    getCurrentChannel: function() {
        return this._getCurrentChannel();
    },
    getBackChannel: function(is_seperate) {
        return this._getBackChannel(is_seperate);
    },
    getLcwChannel: function(is_audio) {
        return this._getLcwChannel(is_audio);
    },
    
    /**
     * return components
     * 
     * @param type - component type (MediaExtension#COMPONENT_TYPE 참조)
     */
    getComponents: function(type) {
        return this.vbo.getComponents(type);
    },
    /**
     * select other av component (visual impaired, multi audio)
     * 
     * @param component - getComponent()통해서 획득한 component object
     */
    selectComponent: function(component, device) {
        return this.vbo.selectComponent(component, device);
    },
    unselectComponent: function(component, device) {
        // [jh.lee] 블루투스 오디오 관련 추가
        return this.vbo.unselectComponent(component, device);
    },
    /**
     * get value for closed caption info
     */
    getClosedCaptionInfo: function() {
        var cc_info;
        
        try {
            cc_info = this.cc_control.getClosedCaptionInfo();
        }
        catch(e) {
            log.printExec(e);
        }
        
        return cc_info;
    },
    /**
     * return current active component
     * 현재 play 중인 AV component 획득 가능
     * 주로 채널의 multi audio 중 현재 play 중인 Audio Component를 확인하기 위해서 사용
     * 
     * @param type - component type (MediaExtension#COMPONENT_TYPE 참조)
     */
    getCurrentActiveComponents: function(type, device) {
        return this.vbo.getCurrentActiveComponents(type, device);
    },  
    addChannelEventListener: function(listener) {
        this._addChannelEventListener(listener);
    },
    removeChannelEventListener: function(listener) {
        this._removeChannelEventListener(listener);
    },
    
    /**
     * change channel
     * 
     * @param channel - channel to tune
     * @param only_select - if do selection only true, otherwise false
     * @param forced - if true, forcely selection whether or not same channel with current channel
     */
    changeChannel: function(channel, only_select, forced, block_event, block_osd, use_excd) {
        //if (forced !== true) {
            this._setOnlySelect(only_select, forced);
        //}
        
        if (only_select === true) {
            // channel tune만 하길 원하는 경우 
            // VBO#setChannel() 상 parameter 기준으로 true/true로 전달한다.
            // 전체 편성표에서 특정 채널을 선택하는 경우 편성표는 유지된 상태에서
            // 채널만 tune 되어야 하기 때문에 channel event callback 및
            // channel osd 노출을 block 처리한다.
            
            // videoBroadcastObject#setChannel(channel, is_block_event, is_block_osd)
            // is_block_event - event callback 여부
            // is_block_osd - channel OSD를 노출할지 여부
            if (block_event === undefined) {
                block_event = true;
            }
            if (block_osd === undefined) {
                block_osd = true;
            }
            
            if (use_excd === true && this.ex_ch_db) {
            	this.ex_ch_db.selectPromoChannel(block_event, block_osd);
            } else {
                // 2017.06.07 dhlee
                // 2.0 로직 추가
                if (this.getControlType() === KTW.nav.Def.CONTROL.PIP) {
                    KTW.ca.CaHandler.checkParentalRatingLock();
                }
                this.vbo.setChannel(channel, block_event, block_osd);
            }

        }
        else {
        	// 동일채널을 선택하는 경우 miniEPG만 활성화 하도록 처리함.
            // 단, forced 하게 호출하는 경우에는 무조건 setChannel 하도록 함
            // (연령제한 해제 하는 경우)
            if (forced !== true && this._isSameChannel(channel) === true) {
                if (this.getControlType() === KTW.nav.Def.CONTROL.MAIN) {
                    if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(channel) !== true) {
                        var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.MINI_EPG);
                        var dataParams = null;
                        if (layer) {
                            dataParams = layer.getParams();

                            if(dataParams !== undefined && dataParams !== null) {
                                var tempParams = {};
                                tempParams.fromKey = true;

                                if(dataParams.data !== undefined && dataParams.data !== null) {
                                    tempParams.data = dataParams.data;
                                }
                                dataParams = tempParams;
                            }else {
                                dataParams = {fromKey:true};
                            }
                        }else {
                            dataParams = {fromKey:true};
                        }
                        
                        KTW.ui.LayerManager.activateLayer({
                            obj: {
                                id: KTW.ui.Layer.ID.MINI_EPG,
                                type: KTW.ui.Layer.TYPE.TRIGGER,
                                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                linkage: false,
                                params: dataParams
                            },
                            clear_normal: true,
                            visible: true,
                            cbActivate: function() {

                            }
                        });
                        KTW.managers.service.AudioIframeManager.setAudioFullEpgExit();
                    }
                }
                return;
            }
            
            // PIP vbo의 경우 visible이 켜지 있지 않으면 setChannel을 호출할 수 없다.
            // 그래서 visible check해서 꺼져있으면 visible 상태로 변경한다.
            if (this.getControlType() === KTW.nav.Def.CONTROL.PIP) {
                if (this.vbo.style.visibility !== "visible") {
                    this.vbo.style.visibility = "visible";
                }
            }
            
            // 사용자가 채널 전환 시 의도적으로 iframe을 NONE 상태로 처리하도록 함
            // stitching service 상태에서 특정 채널 tune 시 이전 iframe이
            // 잠시 보이는 문제가 있어서 미리 지우고 처리하도록 함.
            if (this.getControlType() === KTW.nav.Def.CONTROL.MAIN) {
            	KTW.managers.service.IframeManager.setType(
            			KTW.managers.service.IframeManager.DEF.TYPE.NONE);
            }

            if (use_excd === true && this.ex_ch_db) {
                this.ex_ch_db.selectPromoChannel();
            } else {
                this.vbo.setChannel(channel);
            }
        }
    },
    stopChannel: function() {
        if (this.getControlType() === KTW.nav.Def.CONTROL.MAIN ||
            this.getControlType() === KTW.nav.Def.CONTROL.STITCHING) {
            if(this.vbo && this.vbo.stop)
                this.vbo.stop();
        }
        else {
            if (this.vbo.style.visibility === "visible") {
            	this.vbo.stop();
                this.vbo.style.visibility = "hidden";
            }
        }
    },
    /**
     * set PIP video size
     * 
     * @param x
     * @param y
     * @param width
     * @param height
     */
    setVideoSize: function(x, y, width, height , stylewidth , styleheight) {
        KTW.utils.Log.printDbg(this.getControlType() + " vbo setVideoSize(" + x + ", " + y + ", " + width + ", " + height);
        //if (this.getControlType() === KTW.nav.Def.CONTROL.PIP ||
        //	this.getControlType() === KTW.nav.Def.CONTROL.STITCHING) {
        //    this.vbo.x = x;
        //    this.vbo.y = y;
        //    this.vbo.width = width;
        //    this.vbo.height = height;
        //}
        if (this.getControlType() === KTW.nav.Def.CONTROL.PIP) {
            this.vbo.style.left = x + "px";
            this.vbo.style.top = y + "px";

            if(stylewidth !== undefined && stylewidth !== null) {
                this.vbo.style.width = stylewidth + "px";
            }else {
                this.vbo.style.width = width + "px";
            }

            if(styleheight !== undefined && styleheight !== null) {
                this.vbo.style.height = styleheight + "px";
            }else {
                this.vbo.style.height = height + "px";
            }
        }
        else {
            //if (width === KTW.CONSTANT.RESOLUTION.WIDTH) {
            //    this.vbo.style.visibility = "hidden";
            //}
            //else {
            //    this.vbo.style.visibility = "";
            //}

            //this.vbo.style.left = x + "px";
            //this.vbo.style.top = y + "px";
            //this.vbo.style.width = width + "px";
            //this.vbo.style.height = height + "px";
        }

        if (this.getControlType() === KTW.nav.Def.CONTROL.MAIN) {
            if (width == KTW.CONSTANT.RESOLUTION.WIDTH) {
                KTW.utils.Log.printDbg(this.getControlType() + " vbo setVideoSize(true)");

                this.vbo.x = x;
                this.vbo.y = y;
                this.vbo.width = width;
                this.vbo.height = height;

                this.vbo.setFullScreen(true);

                return;
            }
            else {
                KTW.utils.Log.printDbg(this.getControlType() + " vbo setVideoSize(false)");

                /**
                 * [dj.son] 작은 사이즈 -> 작은 사이즈로 리사이즈시, 리사이즈가 안되는 이슈 발견
                 * - x, y, width, height 설정 구문이 이쪽으로 오면 문제없이 리사이즈 됨을 확인
                 * TODO 일단 구문상 & 로직상 문제는 없으므로, 작은 사이즈로 resize 시 사이즈를 setFullScreen 앞뒤로 설정하도록 수정
                 * TODO 추후 정확한 원인 규명 및 로직 수정 요망
                 */
                this.vbo.x = x;
                this.vbo.y = y;
                this.vbo.width = width;
                this.vbo.height = height;

                this.vbo.setFullScreen(false);
            }
        }

        this.vbo.x = x;
        this.vbo.y = y;
        this.vbo.width = width;
        this.vbo.height = height;
    },
    getVideoSize: function() {
        var obj = {
          x :   this.vbo.x,
          y :   this.vbo.y,
          width : this.vbo.width,
          height : this.vbo.height             
        };
        return obj;
    },

    
    getPipMode: function() {
        return this._getPipMode();
    }, 
    
    enableAudio: function() {
    	try {
    		this.vbo.enableAudio();
    	} catch (e) {
    		log.printExec(e);
    	}
    },
    
    disableAudio: function() {
    	try {
    		this.vbo.disableAudio();
    	} catch (e) {
    		log.printExec(e);
    	}
    },
    
    addEventListener: function (name, callback) {
    	this.vbo.addEventListener(name, callback);
    },
    
    removeEventListener: function(name, callback) {
    	this.vbo.removeEventListener(name, callback);
    },
    
    skipChannelEvent: function(enable) {
        this._setSkipChannelEvent(enable);
    },
    releaseChannelBlock: function() {
        // 대기 모드에서 돌아올 때 동일 채널 tune 되기 때문에
        // only select 값이 초기화 되지 않는 문제가 있다.
        // 그래서 실제 miniEPG에서 시청제한 또는 시청연령제한 해제를 할때
        // 의도적으로 이 값을 초기화 하도록 해야 한다.
        this._setOnlySelect(false, false, false);
    },

    clearCheckServiceStateTimer: function () {
        this._clearCheckServiceStateTimer();
    }
};
