"use strict";

/**
 * managing channel database
 * 
 */
KTW.nav.si.ChannelDatabaseManager = (function() {
    
    var DEF_CHANNEL_CONFIG = KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG;
    
    var FAV_IDS = {
        AUDIO : [
            DEF_CHANNEL_CONFIG.FAVOURITE_AUDIO,
            DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_AUDIO],
            // OTS에서 audio portal의 경우 일반 채널로 처리
            //DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_AUDIO_PORTAL],
        FAVORITE : [
            DEF_CHANNEL_CONFIG.FAVOURITE_FAVORITE,
            DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE ],
        BLOCKED : [
            DEF_CHANNEL_CONFIG.FAVOURITE_BLOCKED,
            DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED ],
        SKIPPED : [
            DEF_CHANNEL_CONFIG.FAVOURITE_SKIPPED,
            DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_SKIPPED ],
        KIDS_CARE : [
            DEF_CHANNEL_CONFIG.FAVOURITE_KIDS_CARE,
            DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_CARE ],
        SKY_CHOICE : [ DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_PPV ],
        SKY_HD : [ DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_SKYHD ],
        DATA_SERVICE : [ DEF_CHANNEL_CONFIG.FAVOURITE_DATA ],
        // 2017.04.17 dhlee KIDS MODE channel FAV_ID 추가
        KIDS_MODE : [
            DEF_CHANNEL_CONFIG.FAVOURITE_KIDS_MODE,
            DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_MODE ],
        HIDDEN_PROMO: [DEF_CHANNEL_CONFIG.FAVOURITE_HIDDEN_PROMO]
    };
    
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    
    var navAdapter = null;
    var channelConfig = null;
    
    var channel_list = {};
    
    //var channel_list_update_listeners = null;
    //var favourite_list_update_listeners = null;
    
    var update_listeners = null;
    
    var update_list_id = undefined;
    
    function _init(obj) {
        
        channelConfig = obj;
        navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        
        update_listeners = [];
        
        // listener add
        if (channelConfig !== null && channelConfig !== undefined) {
            channelConfig.addEventListener(DEF_CHANNEL_CONFIG.CHANNEL_LIST_UPDATE, onChannelListUpdated);
            channelConfig.addEventListener(DEF_CHANNEL_CONFIG.FAVOURITE_LIST_UPDATE, onFavouriteListUpdated);
        }
    }
    
    function _start() {
        updateChannelList(true);
    }
    
    function updateChannelList(all) {
        log.printDbg("updateChannelList()");
        
        var favourite_list_collection, favourite_list, fav_id, collection_length, list_length;
        
        try {
            if (channelConfig !== null && channelConfig !== undefined) {
                favourite_list_collection = channelConfig.favouriteLists;
            }
            
            collection_length = favourite_list_collection ? favourite_list_collection.length : 0;
            for (var i = 0; i < collection_length; i++) {
                favourite_list = favourite_list_collection[i];
                fav_id = favourite_list.favID;
                list_length = favourite_list.length;
                log.printDbg("updateChannelList() fav_id = " + fav_id + " / length = " + list_length);
                
                if (fav_id === DEF_CHANNEL_CONFIG.FAVOURITE_DATA) {
                    setDataChannelList(favourite_list);
                }
                else {
                    channel_list[fav_id] = favourite_list;
                }
            }
            
            if (all === true) {
                // 전체 편성표용 channel update
                // skipped channel 제외한 channel 및 UHD channel list 구성
                setFullEpgChannelList();
				// 건너뛰기채널 audio sort list
                channelSortList(); // 2016.09.05 dhlee R6 형상 추가
                // OTV 오디오채널 편성표용 channel update (OTS에서는 Audio Channel 은 건너뜀 채널에 포함되지 않는다)
                // skipped channel 제외한 audio channel list 구성
                if (!KTW.CONSTANT.IS_OTS) {
                    setSurfableAudioChannelList();
                }
            }
        } catch(e) {
            log.printErr("updateChannelList() failed update channel list");
            log.printExec(e);
        }
    }
    
	// 2016.09.05 dhlee R6 형상 추가
	function channelSortList() {
    	log.printInfo("channelSortList()");
        var length = 0;
        var video_list_length;

        var promo_channel = KTW.oipf.AdapterHandler.navAdapter.getPromoChannel();
        var video_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_VIDEO);
        var audio_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_AUDIO);
        var temp_list = [];

        video_list = Array.apply(this, video_list).slice();
        audio_list = Array.apply(this, audio_list).slice();

        // promo channel을 제거한다.
        video_list_length = video_list ? video_list.length : 0;
        for (var i = 0; i < video_list_length; i++) {
            if (video_list[i].ccid === promo_channel.ccid) {
                video_list.splice(i, 1);
                break;
            }
        }

        // [jh.lee] video 리스트 뒤에 audio 리스트 추가
        video_list = video_list.concat(audio_list);
        video_list.sort( function(a, b) { return a.majorChannel - b.majorChannel; } );
        // 리스트 생성
        if (video_list) {
            length = video_list.length;

            for (var i = 0; i < length; i++) {
                temp_list.push(video_list[i]);
            }
        }
        channel_list[DEF_CHANNEL_CONFIG.FAVOURITE_SORT_LIST] = temp_list.slice();
    }

    function setDataChannelList(list) {
        log.printInfo("setDataChannelList()");
        
        var app_list = [];
        var cug_list = [];
        var genre, genre_hex, length;
        
        length = list.length;
        for (var i = 0; i < length; i++) {
            try {
                genre = list[i].getSIDescriptor(0x54);
                genre_hex = genre[0].toString(16);
                
                //log.printDbg("setDataChannelList() genre_hex = " + genre_hex);
                
                if (genre_hex === "52") {
                    app_list[app_list.length] = list[i];
                }
                else  if (genre_hex === "53") {
                    cug_list[cug_list.length] = list[i];
                }
            } catch(e) {
                log.printExec(e);
            }
        }
        channel_list[DEF_CHANNEL_CONFIG.FAVOURITE_TV_APP] = app_list.slice();
        channel_list[DEF_CHANNEL_CONFIG.FAVOURITE_CUG] = cug_list.slice();
        
        log.printDbg("setDataChannelList() fav_id = " + DEF_CHANNEL_CONFIG.FAVOURITE_TV_APP + " / length = " + app_list.length);
        log.printDbg("updateChannelList() fav_id = " + DEF_CHANNEL_CONFIG.FAVOURITE_CUG + " / length = " + cug_list.length);
    }
    
    /**
     * video channel 중 skipped channel을 제외한 것만을  
     */
    function setFullEpgChannelList() {
        log.printInfo("setFullEpgChannelList()");
        
        var length = 0;
        //var count = 0;
        var skipped_list = [];
        var video_list;
        var full_epg_list = [];
        var uhd_list  = [];
        var temp_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_SKIPPED);
        
        if (temp_list) {
            length = temp_list.length;
            for (var i = 0; i < length; i++) {
                skipped_list.push(temp_list[i]);
            }
        }
        
        if (KTW.LOG_OUTPUT === true) {
            length = skipped_list.length;
            log.printDbg("setFullEpgChannelList() skipped_list length = " + length);
        }
        
        if (KTW.CONSTANT.IS_OTS === true) {
            video_list = _getChannels(DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);
            if (video_list) {
                length = video_list.length;
                
                for (var i = 0; i < length; i++) {
                    // ots 채널 리스트에서 Channel.isUHD 속성을 이용해서 구성함 
                    if (video_list[i].isUHD === true && skipped_list.indexOf(video_list[i]) === -1) {
                        uhd_list.push(video_list[i]);
                        
                        log.printDbg("setFullEpgChannelList() skylife UHD ch = " + getChannelInfo(video_list[i]));
                    }
                }
            }
        }
        
        video_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_VIDEO);
        
        if (video_list) {
            length = video_list.length;
            
            for (var i = 0; i < length; i++) {
                if (skipped_list.indexOf(video_list[i]) === -1) {
                    full_epg_list.push(video_list[i]);
                    // OTV STB에서만 otv UHD 채널 리스트를 구성함
                    if (KTW.CONSTANT.IS_OTS === false) {
                    	// otv 채널 리스트에서 Channel.isUHD 속성을 이용해서 구성함 
                        if (video_list[i].isUHD === true) {
                            uhd_list.push(video_list[i]);
                            
                            log.printDbg("setFullEpgChannelList() olleh UHD ch = " + getChannelInfo(video_list[i]));
                        }
                    }
                }
            }
        }
        channel_list[DEF_CHANNEL_CONFIG.FAVOURITE_FULL_EPG] = full_epg_list.slice();
        channel_list[DEF_CHANNEL_CONFIG.FAVOURITE_UHD] = uhd_list.slice();
        
        log.printDbg("setDataChannelList() fav_id = " + DEF_CHANNEL_CONFIG.FAVOURITE_FULL_EPG + " / length = " + full_epg_list.length);
        log.printDbg("setDataChannelList() fav_id = " + DEF_CHANNEL_CONFIG.FAVOURITE_UHD + " / length = " + uhd_list.length);
    }

    /**
     * Audio Channel list 에서 skipped channel 을 제외한다.
     */
    function setSurfableAudioChannelList() {
        log.printDbg("setSurfableAudioChannelList()");
        var audio_list;
        var skipped_list = [];
        var surfable_audio_list = [];
        var temp_list;

        audio_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_AUDIO);
        temp_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_SKIPPED);
        if (temp_list) {
            length = temp_list.length;
            for (var i = 0; i < length; i++) {
                skipped_list.push(temp_list[i]);
            }
        }
        if (audio_list) {
            var length = audio_list.length;
            for (var i = 0; i < length; i++) {
                if (skipped_list.indexOf(audio_list[i]) === -1) {
                    surfable_audio_list.push(audio_list[i]);
                }
            }
        }

        channel_list[DEF_CHANNEL_CONFIG.FAVOURITE_SURFABLE_AUDIO] = surfable_audio_list.slice();
    }

    /**
     * return channel list for ring
     */
    function _getChannels(channel_ring) {
        log.printDbg("_getChannels(" + channel_ring + ")");
        
        return channel_list[channel_ring];
    }
    
    function _getChannelByNum(num1, num2) {
        log.printDbg("_getChannelByNum(" + num1 + ", " + num2 + ")");
        
        //var channel = null;
        var channel_num = num2;
        //var channel_ring = null;
        var channel_list = null;
        var length = 0;
         
        // OTS STB에서는 sky channel이 존재하면 sky channel을 확인하다.
        // 만약 sky channel이 없다면 olleh channel을 확인한다.
        if (KTW.CONSTANT.IS_OTS === true) {
            // 2016.09.07 dhlee 코드 정리
        	//channel_ring = DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO;
        	//channel_list = _getChannels(channel_ring);
            channel_list = _getChannels(DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);
        	
        	if (util.isValidVariable(channel_list) === true) {
        		length = channel_list.length;
        		
        		// num2(sky channel) 이 존재하지 않으면 무조건 num1으로 처리
        		if (util.isValidVariable(num2) === false) {
        			channel_num = num1;
        		}
        	}
        }
        
        if (length === 0) {
        	// sky channel 이 존재하지 않으면 num1으로 처리
        	channel_num = num1;
            // 2016.09.07 dhlee 코드 정리
            //channel_ring = DEF_CHANNEL_CONFIG.FAVOURITE_VIDEO;
            //channel_list = _getChannels(channel_ring);
            channel_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_VIDEO);
            length = channel_list !== undefined ? channel_list.length : 0;
        } 
        
        for (var i = 0; i < length; i++) {
            if (channel_list[i].majorChannel === channel_num) {
                return channel_list[i];
                //channel = channel_list[i];
                //break;
            }
        }
        
        //return channel;
        return null;
    }

    /**
     * 우리집 맞춤 TV -> 알림박스 에서 채널 연동시 채널 번호 기준으로 channel obj 를 찾는데,
     * _getChannelByNum 로는 data channel 를 찾을 수가 없어서 전체 채널링에서 채널 번호를 기준으로 channel obj 를 찾는 함수 추가
     */
    function _getChannelByNumOnAll (num , isRealTimeChannel) {
        log.printDbg("_getChannelByNumOnAll(" + num + ")");

        var channel_list, length;

        if(isRealTimeChannel !== undefined && isRealTimeChannel !== null && isRealTimeChannel === true) {
            channel_list = _getChannels(DEF_CHANNEL_CONFIG.ALL);
        }else {
            channel_list = _getChannels(DEF_CHANNEL_CONFIG.ALL_SKY_KT);
        }
        length = channel_list !== undefined ? channel_list.length : 0;

        for (var i = 0; i < length; i++) {
            if (channel_list[i].majorChannel === num) {
                return channel_list[i];
            }
        }

        return null;
    }

    /**
     * channel.ccid 와 동일한 채널을 전달한다.
     *
     * @param ccid
     * @returns {*}
     * @private
     */
    function _getChannelByCCID(ccid) {
        log.printDbg("_getChannelByCCID() ccid = " + ccid);
        
        var channel_list, length;
        
        channel_list = _getChannels(DEF_CHANNEL_CONFIG.ALL_SKY_KT);
        length = channel_list !== undefined ? channel_list.length : 0;
        
        for (var i = 0; i < length; i++) {
            if (channel_list[i].ccid === ccid) {
                return channel_list[i];
            }
        }
        
        return null;
    }
    
    function _getChannelBySID(sid, id_type) {
        log.printDbg("_getChannelBySID() sid = " + sid);
        
        var channel_list, length;
        
        if (id_type === undefined) {
            id_type = KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS;
        }
        
        channel_list = _getChannels(DEF_CHANNEL_CONFIG.ALL_SKY_KT);
        length = channel_list !== undefined ? channel_list.length : 0;
        
        for (var i = 0; i < length; i++) {
            if (channel_list[i].sid === sid && channel_list[i].idType === id_type) {
                return channel_list[i];
            }
        }
        
        return null;
    }
    
    function _getChannelByTriplet(locator) {
        log.printDbg("_getChannelByTriplet() locator = " + locator);
        
        var locator_info;
        var onid, tsid, sid;
        var channel_list;
        
        if (util.isValidVariable(locator) === true) {
            if (locator.indexOf("dvb://") !== -1) {
                locator = locator.substring(6);
            }   
        }
        
        channel_list = _getChannels(DEF_CHANNEL_CONFIG.ALL_SKY_KT);
        
        locator_info = locator.split(".");
        onid = parseInt(locator_info[0], 16);
        tsid = parseInt(locator_info[1], 16);
        sid = parseInt(locator_info[2], 16);
        
        return channel_list.getChannelByTriplet(onid, tsid, sid);
    }

    /**
     * locator 정보를 이용하여 맞춤(Hidden) 프로모 채널을 찾는다.
     *
     * @param locator
     * @returns {*}
     */
    function _getHiddenPromoChannelByTriplet(locator) {
        log.printDbg("_getHiddenPromoChannelByTriplet(), locator = " + locator);

        var locator_info;
        var onid, tsid, sid;
        var channel_list;

        if (util.isValidVariable(locator) === true) {
            if (locator.indexOf("dvb://") !== -1) {
                locator = locator.substring(6);
            }
        }

        channel_list = _getChannels(DEF_CHANNEL_CONFIG.FAVOURITE_HIDDEN_PROMO);

        locator_info = locator.split(".");
        onid = parseInt(locator_info[0], 16);
        tsid = parseInt(locator_info[1], 16);
        sid = parseInt(locator_info[2], 16);

        log.printDbg("_getHiddenPromoChannelByTriplet(), locator = " + locator + " , onid : " + onid + " , tsid : " + tsid + " , sid : " + sid);
        return channel_list.getChannelByTriplet(onid, tsid, sid);
    }
    
    function _changeChannelList(id, list, obj) {
        
        var favourite_list = channel_list[id];
        if (favourite_list) {
            if (obj) {
                log.printDbg("_changeChannelList() change channel = " + JSON.stringify(obj));
                
                var length = favourite_list.length;
                var index = length;
                for (var i = 0; i < length; i++) {
                    if (favourite_list.item(i).majorChannel >= obj.majorChannel) {
                        index = i;
                        break;
                    }
                }
                
                // add or remove 처리
                if (obj.type === 0) { // insert
                    favourite_list.insertBefore(index, obj.ccid);
                }
                else { // remove
                    favourite_list.remove(index);
                }
                
                // favourite_list 의 변경사항을 완료
                if (obj.update === false) {
                    // update 이벤트를 호출하지 않는 commit
                    favourite_list.commit(true);
                }
                else {
                    favourite_list.commit(false, true);
                }
                
                // 의도적으로 변경되는 list_id 값을 cache 처리하고
                // notifyUpdate 시점에 해당 id가 전달되도록 함
                update_list_id = id;
            }
            else {
                replaceChannelList(id, list, favourite_list)
            }
        }
    }
    
    function replaceChannelList(id, list, favourite_list) {
        log.printDbg("replaceChannelList(" + id + ")");
        
        try {
            var length = favourite_list.length;
            while (length > 0) {
                favourite_list.remove(0);
                length--;
            }
            if (list) {
                length = list.length;
                var count = 0;
                for (var i = 0; i < length; i++) {
                    if (util.isValidVariable(list[i], [ "ccid" ]) === true) {
                        favourite_list.insertBefore(count, list[i].ccid);
                        count++;
                    }
                }
            }
            
            // 의도적으로 변경되는 list_id 값을 cache 처리하고
            // notifyUpdate 시점에 해당 id가 전달되도록 함
            update_list_id = id;
            
            // channel list commit 규칙
            // channelList#commit(blockEvent, blockChannelListEvent)
            // blockEvent - if true don't callback update event
            // blockChannelListEvent - if true only callback FavouriteListUpdated event
            //
            // 기본적으로 block 개념이라서 update callback을 받고자 하는 경우 false로 설정
            // 보통 선호, 제한 채널등을 변경하는 경우 FavouriteListUpdated event만 받기를 원하므로
            // commit(false, true) 로 처리한다.
            favourite_list.commit(false, true);
            
        } catch(e) {
            log.printExec(e);
        }
    }
    
    function _isAudioChannel(channel, include_audio_portal) {
        
        var fav_ids = FAV_IDS.AUDIO.slice();
        if (include_audio_portal === true) {
            fav_ids.push(DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_AUDIO_PORTAL);
        }
        
        return matchFavId(channel, fav_ids);
    }
    
    function _isFavoriteChannel(channel) {

        if (KTW.managers.service.KidsModeManager.isKidsMode()) {
            return false;
        }
        var result = false;
        var id = DEF_CHANNEL_CONFIG.FAVOURITE_FAVORITE;
        if (util.isValidVariable(channel) === true) {
            if (channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS &&
                    channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                id = DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE;
            }
            
            var favourite_list = channel_list[id];
            var length = favourite_list == null ? 0 : favourite_list.length;
            for (var i = 0; i < length; i++) {
                if (favourite_list[i].ccid === channel.ccid){
                    result = true;
                    break;
                }
            }
        }
        return result;
        //return matchFavId(channel, FAV_IDS.FAVORITE);
    }
    
    function _isBlockedChannel(channel) {

        if (KTW.managers.service.KidsModeManager.isKidsMode()) {
            return false;
        }
        var result = false;
        var id = DEF_CHANNEL_CONFIG.FAVOURITE_BLOCKED;
        if (util.isValidVariable(channel) === true) {
            if (channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS &&
                    channel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                id = DEF_CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED;
            }
            
            var blocked_list = channel_list[id];
            var length = blocked_list == null ? 0 : blocked_list.length;
            for (var i = 0; i < length; i++) {
                if (blocked_list[i].ccid === channel.ccid){
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * [dj.son] 키즈 채널 여부 리턴
     */
    function _isKidsChannel (channel) {
        return matchFavId(channel, FAV_IDS.KIDS_MODE);
    }

    /**
     * 2017.04.18 dhlee 맞춤프로모채녈 여부를 반환한다.
     *
     * @param channel
     */
    function _isHiddenPromoChannel(channel) {
        return matchFavId(channel, FAV_IDS.HIDDEN_PROMO);
    }
    
    function _isSkippedChannel(channel) {
        return matchFavId(channel, FAV_IDS.SKIPPED);
    }
    
    function _isDataServiceChannel(channel) {
        return matchFavId(channel, FAV_IDS.DATA_SERVICE);
    }
    
    function _isSkyChoiceChannel(channel) {
        return matchFavId(channel, FAV_IDS.SKY_CHOICE);
    }
    
    function _isSkyHDChannel(channel) {
        return matchFavId(channel, FAV_IDS.SKY_HD);
    }
    
    function matchFavId(channel, fav_ids) {
        
        var matched = false;
        if (util.isValidVariable(channel) === true) {
            var list = channel.favIDs;
            
            if (util.isValidVariable(list) === true) {
                var list_length = list.length;
                var fav_ids_length = fav_ids.length;
                for (var i = 0; i < list_length; i++) {
                    for (var j = 0; j < fav_ids_length; j++) {
                        if (list[i] === fav_ids[j]) {
                            matched = true;
                            break;
                        }
                    }
                    if (matched === true) {
                        break;
                    }
                }
            }
        }
        return matched;
    }
    
    function _addEventListener(listener) {
        log.printDbg("_addEventListener()");
        
        update_listeners.push(listener);
    }
    
    function _removeEventListener(listener) {
        var length = update_listeners.length;
        log.printDbg("_removeEventListener()");
        
        for (var i = 0; i < length; i++) {
            if (update_listeners[i] === listener) {
                update_listeners.splice(i, 1);
                break;
            }
        }
    }
    
    function notifyUpdate(id) {
        var length = update_listeners.length;
        for (var i = 0; i < length; i++) {
            update_listeners[i](id);
        }
    }
    
    function onChannelListUpdated(event) {
        log.printDbg("onChannelListUpdated() event = " + JSON.stringify(event));
        
        updateChannelList(true);
        
        // [jh.lee] 10/19 745 이슈로 onFavouriteListUpdated에 추가했던 것을 onChannelListUpdated로 이동
        // [jh.lee] 자녀안심채널 설정시 onChannelListUpdated, onFavouriteListUpdated 이 각각 한번씩 오고,
        // [jh.lee] 자녀안심 모드 ON 설정 시 onFavouriteListUpdated 만 온다. refresh 콜백 수행을 위해 옮김
        if (update_list_id !== DEF_CHANNEL_CONFIG.FAVOURITE_KIDS_CARE) {
            notifyUpdate(update_list_id);
        }
        
        update_list_id = undefined;
    }
    
    function onFavouriteListUpdated(event) {
        log.printDbg("onFavouriteListUpdated() event = " + JSON.stringify(event));
        
        updateChannelList(true);
        
        // 자녀안심 설정의 경우 채널 설정 시 onFavouriteListUpdated callback 되고
        // 이후에 자녀안심 설정으로 다시 onChannelListUpdated callback이 올라온다.
        // 그래서 한번만 처리하기 위해서 자녀안심 채널 ring 변경 시에는
        // listener callback 처리하지 않도록 한다.
        // [jh.lee] 10/19 자녀안심 모드 ON 설정 시 onFavouriteListUpdated 만 온다. refresh 콜백 수행을 위해 옮김
        notifyUpdate(update_list_id);
        update_list_id = undefined;
    }
    
    function getChannelInfo(ch) {
        var info = "[Channel: ch #=" + ch.majorChannel + 
            ", name=" + ch.name + ", type=" + ch.channelType + 
            ", sid=" + ch.sid + ", ccid=" + ch.ccid + 
            ", service_type= " + ch.idType + "]";
    }
    
    return {
        init: _init,
        start: _start,
        
        getChannels: _getChannels,
        
        getChannelByNum: _getChannelByNum,
        getChannelByNumOnAll: _getChannelByNumOnAll,
        getChannelByCCID: _getChannelByCCID,
        getChannelBySID: _getChannelBySID,
        getChannelByTriplet: _getChannelByTriplet,
        getHiddenPromoChannelByTriplet: _getHiddenPromoChannelByTriplet,
        
        changeChannelList: _changeChannelList,
        
        isAudioChannel: _isAudioChannel,
        isFavoriteChannel: _isFavoriteChannel,
        isBlockedChannel: _isBlockedChannel,
        isSkippedChannel: _isSkippedChannel,
        isDataServiceChannel: _isDataServiceChannel,
        isSkyChoiceChannel: _isSkyChoiceChannel,
        isSkyHDChannel: _isSkyHDChannel,
        isHiddenPromoChannel: _isHiddenPromoChannel,
        isKidsChannel: _isKidsChannel,
        
        addEventListener: _addEventListener,
        removeEventListener: _removeEventListener
    };
}());