"use strict";

/**
 * The definitions for navigator
 */
KTW.nav.Def = {
    CONTROL : {
       MAIN : "main",
       PIP : "pip",
       STITCHING : "stitching",
       STATE : {
           INITED : 0,
           STARTED : 1,
           PAUSED : 2,
           STOPPED : 3,
       }
    },
    PIP_MODE : {
        NONE: -1,
        MINI_EPG : 0,       // 미니가이드 인접채널
        MULTI_VIEW : 1,     // 멀티화면(2채널 동시시청)
        RESERVATION : 2,    // 예약알림팝업,
        FULL_EPG: 3,         // 전체 편성표
        FAV_EPG: 4          // 선호채널 편성표, size & 로직은 전체 편성표와 동일
    },
    CHANNEL : {
        PERM : {
            UNKNOWN : -1,
            OK : 0,
            BLOCKED : 1,
            PR_BLOCKED : 2,
            NOT_SUBSCRIBED : 3,
            PREVIEW : 4,
        },
        EVENT : {
            REQUESTED : 0,
            PERM_UPDATED : 1,
        },
        BLOCKED_REASON : {
            USER_BLOCKED : 3, // 제한 채널로 설정한 경우
            NO_SD_CHANNEL : 8, // SD 채널이 없는 경우
            UHD_CHANNEL : 11, // UHD 채널인 경우
        },
        ID_TYPE : {  
            DVB_S : window.Channel ? window.Channel.ID_DVB_S : 11,
            DVB_S2 : window.Channel ? window.Channel.ID_DVB_S2 : 15,
            IPTV_SDS : window.Channel ? window.Channel.ID_IPTV_SDS : 40,
            IPTV_URI : window.Channel ? window.Channel.ID_IPTV_URI : 41,
        },
        GENRE : {
        	TERRESTRIAL : 55,
        	DRAMA : 81,
        	MOVIE : 56,
        	SPORTS : 58,
        	ANIMATION : 76,
        	DOCUMENTARY : 59,
        	NEWS : 79,
        	SOCIAL : 73,
        	RELIGION : 72,
        }
    },
};