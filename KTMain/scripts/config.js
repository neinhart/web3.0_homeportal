"use strict";

/**
 * create the root namespace and making sure we're not overwriting it
 * KTW === KT TV Web 3.0
 */
var KTW = KTW || {};


// app.properties 내용을 config 로 옮김
// 사실 app.properties 는 필요없다고 판단되나, 만약을 위해 현재 사용중인 property 만 config 로 옮김
KTW.APP_PROPERTY = {
    FILE_SYSTEM_QUOTA_SIZE: 6,
    APP_DEBUG_MODE: true,
    UNICAST_URL: "http://homedist.ktipmedia.co.kr/"
};

KTW.CONSTANT = {
    ENV : {
        LIVE : "LIVE",
        BMT : "BMT",
        TEST : "TEST",
        LOCAL : "LOCAL"
    },
    APP_STATE : {
        UNKNOWN : -1,
        INITIALIZED : 0,
        STARTED : 1,
        PAUSED : 2,
        DESTROYED : 3
    },
    SERVICE_STATE : {
        //TV_MASK : 0x100,
        //APP_STORE_MASK : 0x1000,
        //
        //TV_VIEWING : 0x101,
        //VOD : 0x004,
        //DATA : 0x010,
        //STANDBY : 0x020,
        //TIME_RESTRICTED : 0x030,

        // [dj.son] Service State 정의 수정
        // - STB 에서 보여지는 Service State 수가 많지 않아, 각각의 Service State 에 대해서 정의
        // - 헥사값이 의미가 없어 Number 값으로 변경
        TV_VIEWING: 0,              // TV 상태
        VOD: 1,                     // VOD 상태
        OTHER_APP: 2,               // 다른 앱 떠 있는 상태 - All AV Paused (Data Channel, FullBrowser 등등)
        OTHER_APP_ON_TV: 3,         // 다른 앱 떠 있는 상태 - On TV_VIEWING (App Store, Unicast App 등등)
        OTHER_APP_ON_VOD: 4,        // 다른 앱 떠 있는 상태 - On VOD (App Store, Unicast App 등등)
        STANDBY: 5,                 // 대기 모드
        TIME_RESTRICTED: 6,         // 시간 제한
        KIDS_RESTRICTION: 7         // 부팅(대기 -> 동작) 시 키즈 제한 상태 (키즈 모드 제한 해제를 위한 인증 팝업이 떠 있는 상태)
    },
    APP_MODE : {
        NONE : 0,
        ICON : 1,
        FULL : 2
    },
    STB_TYPE: {
        UHD1: false,
        UHD2: false,
        UHD3: false,
        ANDROID: false,
        GIGAGENIE1: false,
        GIGAGENIE2: false
    },
    IS_OTS : false,
    IS_DCS : false,
    IS_UHD : false,
    UHD3: {
        BEACON: false,
        TTS: false,
        EX_SPEAKER: false,
        EX_VOICE: false
    },
    IS_HDR: false,
    IS_BIZ: false,  // 2018.01.17 dj.son Biz 유저 flag
    TV_PAY_SUPPORT: false, // 2018.02.20 kh.kim TV페이 모듈 지원 여부
    YOUTUBE_SUPPORT: false,         // 2017.06.26 dhlee UHD2 이상 단말에서 youtube app 지원 여부에 대한 flag 임
    SERIES_RESERVATION: false,
    SUBTITLE_ENABLED : true,
    SUPPORT_NUMERIC_OSK : false,
    IMAGE_PATH : "images/",
    RESOLUTION : {
        WIDTH : 1920,
        HEIGHT : 1080
    },
    // jjh1117 2016.10.13
    // Subhome PIG 위치 및 사이즈
    SUBHOME_PIG_STYLE : {
        LEFT : 1365,
        TOP : 148,
        WIDTH : 555,
        HEIGHT : 316
    },

    // jjh1117 2016.10.13
    // Full Epg PIG 위치 및 사이즈
    FULL_EPG_PIG_STYLE : {
        LEFT : 1360,
        TOP : 40,
        WIDTH : 560,
        HEIGHT : 315
    },

    POSTER_TYPE : {
        VERTICAL: 1,
        HORIZONTAL: 2,
        SQUARE: 3,
        THUMBNAIL: 4
    },

    // 2017.05.24 dhlee
    // 서버로부터 로딩하는 세로형 포스터 사이즈
    VERTICAL_POSTER_SIZE : {
        WIDTH: 210,
        HEIGHT: 300
    },

    // 2017.05.24 dhlee
    // 서버로부터 로딩하는 가로형 포스터 사이즈 (사용하지 않는 이미지 유형인데 추후 확인 필요함)
    HORIZONTAL_POSTER_SIZE : {
        WIDTH: 210,
        HEIGHT: 157
    },

    DEFAULT_FONT : "RixHead M", //font.css 에 상단 3부분도 같이 변경 필요. // 2016.11.21 dhlee default font 변경
    /*
     * Storage Data magic number
     * storage data 초기화가 필요한 경우 magic number를 update
     * 단, 특수한 경우에만 초기화 하도록 하고 live 이후에는 update 되면 안됨
     */
    STORAGE_MAGIC_NUM : "20150407",

    /**
     * [dj.son] default directory path 정보, main.js 에서 초기화됨 ("live" or "bmt")
     */
    DEFAULT_FILE_PATH: "live",
    
    APP_ID: {
    	HOME: "4e30.3001",
    	OBSERVER: "4e30.3000", //옵저버
    	OAM: "4e30.3012", //OAM
    	APPSTORE:"4e30.3009", //앱스토어.임시 
    	SMART_POC:"4e30.3003", //연관정보보기 
    	SMART_PUSH:"4e30.3015",//스마트 푸쉬
    	MASHUP:"4e30.3020" //매쉬업메니저
    },
    APP_CHILD_ID: {
    	HOME_WIDGET : 700, // 홈위젯
    	BOOKMARK : 804, // 북마크(VOD 재생 중인 경우 사용) TODO 최종 804 로 적용해야 함
    	BOOKMARK_ARCHIVE : 803 //북마크 보관함(마이메뉴 및 AV 시청 중인 경우 사용)
    },
    APP_TYPE: {
        WEB_BROWSER : 0,
        UNICAST : 1,
        CHILD_APP : 2,
        MULTICAST : 3,
        USB : 4,
        ANDROID_APP : 5,
        DATA_CHANNEL : 6,
        NFX : 7,
        YOUTUBE: 8      // 2017.06.26 dhlee youtube 추가
    },
    USB_APP: {
        CCID : {
            LIVE : "1.539.539",
            BMT : "1.687.687"
        },
        LOCATOR : {
            LIVE : "dvb://1.21B.21B",
            //BMT : "dvb://1.2af.2af"
            BMT : "dvb://1.21B.21B"
        }
    },
    STAR_POINT: {
        LOCATOR : {
            LIVE : "dvb://1.20e.20e",
            BMT : "dvb://1.20e.20e"
        }
    },
    TV_POINT: {
        LOCATOR : {
            LIVE : "dvb://1.24d.24d",
            BMT : "dvb://1.24d.24d"
        }
    },
    TV_COUPON: {
        LOCATOR : {
            LIVE : "dvb://1.1fb.1fb",
            BMT : "dvb://1.1fb.1fb"
        }
    },
    CONTENT_COUPON: {
        LOCATOR : {
            LIVE : "dvb://1.231.231",
            BMT : "dvb://1.2AC.2AC"
        }
    }, 
    FOUR_CHANNEL : {
        LOCATOR : {
            LIVE : "dvb://1.F6.F6",
            BMT : "dvb://1.F6.F6"
        }
    },
    REALTIME_CHANNEL : {
        LOCATOR : {
            LIVE : "dvb://5.1B9.1B9",
            BMT : "dvb://5.1B9.1B9"
        }
    },
    ANDROID_STATE : {
        ACTIVE	 : 1,
        INACTIVE : 2
    },
    MVNO_PNG : {
        URL : ""
    },
    YOUTUBE_IMAGE : {
        BMT_URL: "http://homedistbmt.ktipmedia.co.kr/ktep4/unicast/data/w3_youtube/1496393497021/youtube.png",
        LIVE_URL: "http://homedist.ktipmedia.co.kr/w3_youtube/1496393497021/youtube.png"
    },

    YOUTUBE_KIDS_IMAGE : {
        BMT_URL: "http://homedistbmt.ktipmedia.co.kr/ktep4/unicast/data/w3_youtube/1496393497021/youtubekids.png",
        LIVE_URL: "http://homedist.ktipmedia.co.kr/w3_youtube/1496393497021/youtubekids.png"
    },

    /**
     * [dj.son] 키즈 서브홈 내 고정 icon 데이터 서버
     * TODO 추후에 서버 확인 후 추가
     */
    KIDS_SUBHOME_MENU_ICON : {
        BMT_URL: "",
        LIVE_URL: ""
    },

    ENTRANCE_PATH: {
        UNKNOWN                       : "00",//Unknown
        HOME_MENU                     : "01", //홈메뉴
        WATCH_LIST                    : "02", //시청목록
        PURCHASE_LIST                 : "03", //구매목록
        SEARCH                        : "04",
        QUEUE                         : "05",
        NAVER_SEARCH                  : "06",
        FOCUS_MOVIE                   : "07",
        FOCUS_ANI                     : "08",
        FOCUS_KIDS                    : "09",
        FOCUS_DOCUMENTARY             : "10",
        FOCUS_ENTERTAINMENT           : "11",
        GUIDE_CHANNEL_200             : "12",//200번 가이드 채널
        POC                           : "13",//시청순위 어플
        CLEAR_SKIN                    : "14", //clear skin 전용관
        AD_CHANNEL_APP                : "15",//광고전용채널어플
        DOWNLOAD_LIST                 : "16",//다운로드목록(D&P 전용)
        HOME_PORTAL_FAV               : "17",//홈포털 즐겨찾기
        HOME_RANK                     : "18",//홈포털 시청순위 카테고리
        VOD_RECOMMAND                 : "19",//VOD 추천 컨텐츠
        LIVE_CHANNEL_VOD              : "20",//실시간채널 VOD 연동
        MAIN_PAGE                     : "21",//메인페이지(N스크린 전용)
        VOD_EXIT_RECOMMAND            : "22",//VOD 종료 추천
        OLLEH_TV_FRAME_AD             : "23",//올레TVFrame광고
        OTN_SHARE_LIST                : "24",//OTN 공우 목록(심리스 이어보기 서비스)
        HOMESHOT_SHORTCUT             : "25",//홈샷바로가기
        HOME_WIDGET                   : "26",//홈위젯
        CONTENT_SHORTCUT_IN_MINI_GUIDE: "27",//미니가이드내 컨텐츠 바로가기
        PREVIEW_VOD                   : "28",//예고편 컨텐츠
        SHOWWINDOW_SERVICE            : "30",//쇼윈도우 서비스 (웹 홈퍼털 전용:1뎁스 이미지 템프릿)
        SHOWWINDOW_2_0_RECOMM         : "31",//쇼윈도우 2.0 - 추천 컨텐츠(쇼윈도 노출 추천 콘텐츠)
        HOMEPOTAL_PLUS                : "32", // 폼포털 플러스
        MY_THEMA                      : "33", //쇼윈도의 추천더보기 및 마이메뉴의 당신을 위한 VOD 추천
        ACAP_1                        : "34", //ACAP 홈포털 컨텐츠 프로모션
        TODAY_RECOMMAND               : "35", //ACAP 홈포털 당신을 위한 VOD 추천 (오늘의 추천 하위 2뎁스)
        MASH_UP_MANAGER               : "36", //메쉬업 매니저 서비스 전용 미니 EPG
        POSTER_PREVIEW_CONTENT        : "38", //포스터 이미지..
        BOOKMARK                      : "43", // 북마킹서비스
        CORNER                        : "44", // 몰아보기 서비스
        MYPLAYLIST                    : "46", // 마이 플레이리스트
        SEARCH_RECOMMAND_THEME        : "48", // 검색창_하단_추천테마 콘텐츠로 진입 시 (ACAP/WEB)
        //SHOWWINDOW_KID_MODE_RECOMMAND_THEMA: "49", //쇼윈도_키즈모드에서 추천테마 콘텐츠로 진입 시 (WEB)
        CHANNEL_PPT                   : "50", // 채널PPT
        NORMAL_RECOMMAND_CATEGORY     : "52", // 홈포털 일반메뉴 내 편성되는 추천 카테고리
        MONTHLY_RECOMMAND_CATEGORY    : "53", // 월정액 메뉴 내 편성되는 추천 카테고리
        GUIDE_CHANNEL_3               : "P0", // 가이드 3번 채널
        GUIDE_CHANNEL_44              : "P1", // 가이드 44번 채널
        FOCUS_TV_CHANNEL              : "P2", // 포커스 TV 채널
        FOCUS_MOVIE_CHANNEL           : "P3", // 포커스 무비 채널
        FOCUS_KIDS_CHANNEL            : "P4" // 포커스 키즈 채널
    }
};

KTW.DATA = {
    ATTS : {
        LIVE : {
            LOCATOR : "dvb://5.ffbf.263",
            COMPONENT_TAG : "a"
        },
        BMT : {
            // 2017.02.20 dhlee 신규 ATTS SI locator로 변경
            // TSID: 5.65345.190 을 HEX 값으로 변경한 값임.
            //LOCATOR : "dvb://5.ffa1.277",
            LOCATOR : "dvb://5.ff41.be",
            COMPONENT_TAG : "a"
        },
        TEST : {
            LOCATOR : "dvb://5.ff78.be",
            COMPONENT_TAG : "a"
        },
        UNICAST_CHECK_PERIOD : 6 * 60 * 60 * 1000,
        BOOTING_EXTRA_TIME : 5 * 60 * 1000
    },
    PROMO : {
        LIVE : {
            LOCATOR: "dvb://1.258.258",
            COMPONENT_TAG_LOGO: "d",
            COMPONENT_TAG_IFRAME: "e"
        },
        BMT : {
            LOCATOR: "dvb://1.2ee.2ee",
            COMPONENT_TAG_LOGO: "d",
            COMPONENT_TAG_IFRAME: "e"
        },
        TEST: {
            LOCATOR: "dvb://1.258.258",
            COMPONENT_TAG_LOGO: "d",
            COMPONENT_TAG_IFRAME: "e"
        },
    	LOCATOR : "dvb://1.258.258.b/",
        LOCATOR_240_BMT: "dvb://1.2ee.2ee.d/"
    },
    UNICAST : {
        LIVE_URL : "http://homedist.ktipmedia.co.kr",
        //BMT_URL : "http://homedistbmt.ktipmedia.co.kr/ktep4/unicast/data",
        BMT_URL : "http://homedistbmt.ktipmedia.co.kr",
        TEST_URL : "http://125.147.35.237/ktep4/unicast/data"
    },
    HTTP : {
    	CURATION : {
    		LIVE_URL : "http://profile.ktipmedia.co.kr:7003/wasProfile",
    		TEST_URL : "http://125.140.114.151:7002/wasProfile"
    	},
    	HDS : {
    		LIVE_URL : "https://svcm.homen.co.kr/",
    		TEST_URL : "https://125.147.28.132/"
    	},
    	KTPG : { //각종 point 이름이 맞나 ?.
    		LIVE_URL : "http://ktpay.kt.com:10088/webapi/json/stb/",
    		TEST_URL : "http://221.148.188.212:10088/webapi/json/stb/"
    	},
        LUPIN: {
            LIVE_URL: "https://ktpay.kt.com/adaptor/ktpg/dcb/",
            TEST_URL: "http://etbips.olleh.com:10088/adaptor/ktpg/dcb/"
        },
    	SMLS : {
    		LIVE_URL : "http://222.122.121.80:8080",
    		TEST_URL : "http://203.255.241.154:8080"
    	},
    	RECOMMEND : {
    		LIVE_URL : "http://recommend.ktipmedia.co.kr:7002/",
    		TEST_URL : "http://125.140.114.151:7002/"
    	},
        SMART_PUSH : {
            LIVE_URL : "http://spis.paran.com",
            TEST_URL : "http://211.37.171.71"
        },
        // [jh.lee] 무비초이스 관련
        SKY_RP_SERVER : {
            LIVE_URL : "http://220.73.134.62",
            TEST_URL : "http://220.73.134.30"
        },
        // OTS 미가입채널 구매 서버
        CPMS_SERVER : {
        	LIVE_URL : "http://220.73.134.50:20431"
        },
        // CPMS 고도화 서버 (추후 적용 예정임)
        IMS_SERVER : {
            LIVE_URL : "https://imsapi.skylife.co.kr",
            BMT_URL : "https://imsapi.skylife.co.kr",
            TEST_URL : "https://imsapi.skylife.co.kr"
        },
        SEARCH : {
            LIVE_URL : "http://semantic.ktipmedia.co.kr:8080/ksearch/web/searchTOTAL",
            TEST_URL : "http://210.183.241.130:8080/kdialog/web/searchTOTAL"
        },
        SEARCH_AUTO : {
            LIVE_URL : "http://semantic.ktipmedia.co.kr:8080/ksearch/web/getTotalAutoWords",
            TEST_URL : "http://210.183.241.130:8080/kdialog/web/getTotalAutoWords"
        },
        MODULE_SERVER : {   // TODO 2017.01.25 dhlee 추후 url 적용
            LIVE_URL : "http://125.159.3.183:8080",
            LIVE_HTTPS_URL: "https://web3api.ktipmedia.co.kr",
            BMT_HTTPS_URL: "https://web3apibmt.ktipmedia.co.kr",
            TEST_URL : ""
        },
        /**
         * [dj.son] 모듈이 위치한 ston 서버
         */
        MODULE_LOCATION_DOMAIN : {
            LIVE_URL : "http://homemodule.ktipmedia.co.kr",
            BMT_URL: "http://homemodulebmt.ktipmedia.co.kr",
            TEST_URL : "http://125.147.31.146"
        },
        THUMBNAIL_IMAGE_SERVER : "http://image.ktipmedia.co.kr/channel/CH_",

        /**
         * [dj.son] 1-depth 홈샷 서버
         */
        HOME_MENU_HOMESHOT: {
            LIVE_URL : "http://adinf.adat.co.kr/INFMAIN/ReqHomeWebAdInfo.aspx",
            BMT_URL : "http://14.63.248.244/INFMAIN/ReqHomeWebAdInfo.aspx"
        },

        /**
         * [sw.nam] 1:N 통합 페어링 호출 서버
         */
        KOL: {
            LIVE_URL: "https://pairing.ktipmedia.co.kr",
            TEST_URL: "http://tb.pairing.kt.com"
        }
    },
    /*
    SHOWWINDOW : {
        LIVE_URL : "http://ollehswapi.paran.com/showwindow/api/common/getVersion.do",
		BMT_URL : "http://211.45.130.96/showwindow/api/common/getVersion.do",
        TEST_URL : "http://211.42.151.111/showwindow/api/common/getVersion.do"
	},
	*/
    AMOC : {
        AUTH_KEY : "OTVHome",
        HTTP_URL : "http://webui.ktipmedia.co.kr:8080",
        HTTPS_URL : "https://webui.ktipmedia.co.kr",
        WAPI_URL : "http://wapi.ktipmedia.co.kr",
        NEW_WAPI_URL : "http://172.16.93.11",
        TIMEOUT : 10 * 1000 // [dj.son] [WEBIIIHOME-3577] 로 인해 timeout 시간 10초로 늘림
    },
    HDS_SDP_CERTIFICATION_SYSTEM : {
		LIVE_URL : "https://usbsso.megatvdnp.co.kr/HDSollehtvnowSDP.asmx?opReqMSaidSDP"
	},
    WEATHER : {
        SUPPORT : true,
        //URL : "http://ollehweather.paran.com:8080/weather-api/locate/",
        URL : "http://mo.fortune82.com/weather/locate.jsp"
    },
    MENU : {
        ITEM : {
            CATEGORY : "0",
            SERIES : "1",
            CONTENT : "2",
            INT_M : "3",
            SERIES_EP : "4",
            INT_U : "7",
            INT_W : "8",
            INT_A : "9",
            INT_NFX : "10"
        },
        CAT : {
            ADULT : "Adult",
            SPECIAL : "S",
            SEARCH : "D",
            RANK : "Rank",
            NEW_HOT : "N",
            MYMENU : "MyMenu",
            MOTEL : "Motel",
            PROMO : "Promo",
            RECOM : "Recom",
            EDU : "Educa",
            SMART : "WEBIP",
            SMART_AD : "Smart",
            RECOM_POWER : "Recom_Power_",
            RECOM_POWER_CONTENT : "Recom_Power_C_",
            RECOM_TOP : "Recom_Top_",
            RECOM_VOD : "Recom_Vod_",
            RECOM_VOD_P : "Recom_Vod_P_",
            HYBRID : "Hybrid",
            SEARCH_VOD : "Search_Vod_",
            PROMO_TRIGGER : "Promo_Trigger_",
            ONE_SELECT_MENU : "ONE_SELECT_MENU_",
            CUSTOM_MENU : "Custom_Menu_",
            POSTER_LIST : "Poster_List_",
            MYDVD : "MyDvd",
            UHD : "UhdCa",
            SHOW : "show",
            HDR : "HdrCa",
            // 추천카테고리 일반메뉴 catType 값 추가
            RECOM_CAT : "RECAT",
            RECOM_PRODUCT : "REPRD",
            VOD_PLUS : "VODPLUS",

            RANKW3: "RankW3",   // w3 rank
            SUBHOME: "SubHome", // w3 subhome
            PWORLD: "PWorld",   // w3 부모세상
            SRCH: "Srch",       // w3 검색
            APPGM: "AppGm",     // w3 앱/게임
            CONFIG: "Config",   // w3 설정
            CHSUB: "ChSub",     // w3 채널 가이드
            APPCA: "AppCa",     // w3 앱 전용 카테고리
            PKGVOD: "PkgVod",   // w3 패키지 카테고리
            MYMENU2: "MyMenu2", // w3 마이메뉴 2
            KIDSSUB: "KidsSub", // w3 키즈 서브홈

            OTV: "OTV",         // OTV STB에서만 노출되는 카테고리 (기가지니 포함)
            OTS: "OTS",         // OTS STB에서만 노출되는 카테고리 (DCS 포함)
            OTVUHD: "OTVUHD",   // OTV UHD 전기종에서 노출되는 카테고리 (기가지니 포함)
            OTSUHD: "OTSUHD",   // OTS UHD 전기종에서 노출되는 카테고리 (DCS 포함)
            UHD1_ONLY: "UHD1",  // UHD1 기종만 노출되는 카테고리 (OTS S1, 1-1, DCS 포함)
            UHD2_ONLY: "UHD2",  // UHD2 기종만 노출되는 카테고리
            UHD3_ONLY: "UHD3",  // UHD3 기종만 노출되는 카테고리
            GBOX_ONLY: "GBOX",   // 기가지니만 노출되는 카테고리
            YOUTUBE: "youtube",  // youtube 카테고리
            YOUTUBE_KIDS: "youtubekids",    // youtubue kids 카테고리 (2017.09.04 추가)

            KIDS_PLAYLIST: "KidsPlay",   // [dj.son] 키즈 플레이 리스트 카테고리 (2017.10.10 추가 - 가제)

            /**
             * [dj.son] 키즈 리모컨 관련 카테고리 추가
             */
            KIDS_HOT_KEY: "KidsHotKey",
            KIDS_SH_DATA: "KidsSHData"
        }
    },
    SUBHOME: {
        TYPE: {
            HOMESHOT: 1,
            EVENT: 2,
            RECOM: 3,
            MYMENU: 4
        },
        POSITION: {
            HOMEMENU_LEFT: 0,
            SUBHOME_TOP: 1,
            SUBHOME_BOTTOM: 2
        },
        IMG_TYPE: {
            VERTICAL: 0,
            HORIZONTAL: 1,
            SQUARE: 2
        },
        NULL_ID: "NULL"
    },
    FILTER: {}
};

/**
 * KeyCode definition
 */
KTW.KEY = {
    COMMON : {
        LEFT : 37, // Keyboard : "ArrowLeft"
        RIGHT : 39, // Keyboard : "ArrowRight"
        UP : 38, // Keyboard : "ArrowUp"
        DOWN : 40, // Keyboard : "ArrowDown"
        OK : 13, // Keyboard : "Enter"
        ENTER : 13, // Keyboard : "Enter"
        BACK : 8, // Keyboard : "BackSpace"
        EXIT : 35, // Keyboard : "End"
        DETAIL : 187, // Keyboard : "="
        RED : 82, // Keyboard : "R"
        GREEN : 71, // Keyboard : "G"
        YELLOW : 89, // Keyboard : "Y"
        BLUE : 66, // Keyboard : "B"
        REW : 188, // Keyboard : ","
        FF : 190, // Keyboard : "."
        PLAY : 186, // Keyboard : ";"
        PAUSE : 222, // Keyboard : "'"
        STOP : 191, // Keyboard : "/"
        SEEK_LEFT : 424, // Keyboard : ""
        SEEK_RIGHT : 425, // Keyboard : ""
        CH_UP : 33, // Keyboard : "PageUp"
        CH_DOWN : 34, // Keyboard : "PageDown"
        PREV_CHANNEL : 45, // Keyboard : "Insert"
        MENU : 77, // Keyboard : "M"
        FULLEPG : 69, // Keyboard : "E"
        // Keyboard : "Numpad0" ~ "Numpad9"
        NUM_0 : 48, NUM_1 : 49, NUM_2 : 50, NUM_3 : 51, NUM_4 : 52, 
        NUM_5 : 53, NUM_6 : 54, NUM_7 : 55, NUM_8 : 56, NUM_9 : 57,
        DELETE : 46, // Keyboard : ""
        DEL : 127, // Keyboard : ""
        CHANGE_CHAR : 121, // Keyboard : ""
        VOLUME_UP : 447, // Keyboard : ""
        VOLUME_DOWN : 448, // Keyboard : ""
        MUTE : 449, // Keyboard : ""
        
        CONTEXT : 71, // Keyboard : "G"
        MOVIE : 79, // Keyboard : "O"
        REWATCH : 65, // Keyboard : "A"
        SEARCH : 83, // Keyboard : "S"
        FAVORITE : 70, // Keyboard : "F"
        RECOMMEND : 67, // Keyboard : "C"
        WATCH_LIST : 87, // Keyboard : "W"
        PURCHASE_LIST : 80, // Keyboard : "P"
        WISH_LIST : 72, // Keyboard : "H"
        SETTING : 84, // Keyboard : "T"
        STAR : 189, // Keyboard : "-"

        //UHD2 셋탑 처리 추가 코드. 빈데가 없네.
        MYMENU : 78,// Keyboard : "N"
        MONTHLY : 90// Keyboard : "Z"
    },
    STB : {
        LEFT : window.window.VK_LEFT, // 37
        RIGHT : window.window.VK_RIGHT, // 39
        UP : window.window.VK_UP, // 38
        DOWN : window.VK_DOWN, // 40
        OK : window.VK_ENTER, // 13
        ENTER : window.VK_ENTER, // 13
        BACK : window.VK_BACK, // 461
        EXIT : window.VK_ESCAPE, // 27
        DETAIL : window.VK_RESERVE_5, // 617
        RED : window.VK_RED, // 403
        GREEN : window.VK_GREEN, // 404
        YELLOW : window.VK_YELLOW, // 405
        BLUE : window.VK_BLUE, // 406
        REW : window.VK_REWIND, // 412
        FF : window.VK_FAST_FWD, // 417
        PLAY : window.VK_PLAY, // 415
        STOP : window.VK_STOP, // 413
        CH_UP : window.VK_CHANNEL_UP, // 427
        CH_DOWN : window.VK_CHANNEL_DOWN, // 428
        MENU : window.VK_HOME, // 36
        FULLEPG : window.VK_GUIDE, // 458
        FULLEPG_OTS : window.VK_RESERVE_1, // 613
        NUM_0 : window.VK_0, // 48
        NUM_1 : window.VK_1, // 49
        NUM_2 : window.VK_2, // 50
        NUM_3 : window.VK_3, // 51
        NUM_4 : window.VK_4, // 52
        NUM_5 : window.VK_5, // 53
        NUM_6 : window.VK_6, // 54
        NUM_7 : window.VK_7, // 55
        NUM_8 : window.VK_8, // 56
        NUM_9 : window.VK_9, // 57
        DELETE : window.VK_PERIOD, // 46
        DEL : window.VK_DELETE, // 127
        CHANGE_CHAR : window.VK_F10, // 121
        //VOLUME_UP : window.VK_VOLUME_UP, // 447
        //VOLUME_DOWN : window.VK_VOLUME_DOWN, // 448
        //MUTE : window.VK_MUTE, // 449
        
        CONTEXT : window.VK_GREEN, // 404
        MOVIE : window.VK_RECALL_FAVORITE_0, // 433
        REWATCH : window.VK_RECALL_FAVORITE_1, // 434
        SEARCH :window.VK_RECALL_FAVORITE_2, //435
        APP_STORE :window.VK_RECALL_FAVORITE_3, //436
        FAVORITE : window.VK_F3, // 114
        SHOPPING : window.VK_CLEAR_FAVORITE_0, // 437
        WINK : window.VK_WINK, // 411 (음성검색)
        WIDGET : window.VK_INFO, // 457 (위젯))

        SKY_TOUCH : window.VK_F6, // 117
        SKY_CHOICE : window.VK_F7, // 118
        SKY_PLUS : window.VK_F8, // 119
        STAR : window.VK_F11, // 122
        SHOP: window.VK_F12,

        //UHD2 셋탑 처리 추가 코드
        MYMENU : window.VK_CLEAR_FAVORITE_1, // 438 (마이메뉴)
        MONTHLY: window.VK_CLEAR_FAVORITE_2, // 439 (월정액)
        
        // 실제로 사용되지는 않는 것으로 보임
        PAGE_UP : window.VK_PAGE_UP, // 33
        PAGE_DOWN : window.VK_PAGE_DOWN, // 34
        SEEK_LEFT : window.VK_TRACK_PREV, // 424
        SEEK_RIGHT : window.VK_TRACK_NEXT, // 425


        KIDS : window.VK_STORE_FAVORITE_0, // UHD3 신규 리모컨 : 키즈
        RECOMMEND : window.VK_STORE_FAVORITE_1, // UHD3 신규 리모컨 : 추천

        /**
         * [dj.son] 키즈 리모컨에 추가된 신규 키 추가
         */
        KIDS_MENU: window.VK_KIDS_HOME, // 501
        KIDS_CH_UP : window.VK_KIDS_CHANNEL_UP, // 502
        KIDS_CH_DOWN : window.VK_KIDS_CHANNEL_DOWN, // 503
        KIDS_SERVICE_HOT_KEY_1: window.VK_KIDS_PORORO,  // 504
        KIDS_SERVICE_HOT_KEY_2: window.VK_KIDS_PINKPONG,    // 505
        KIDS_SERVICE_HOT_KEY_3: window.VK_KIDS_CARRIE,   // 506
        KIDS_SERVICE_HOT_KEY_4: window.VK_KIDS_DAEKYO   // 507
    }
};

KTW.KEY_CODE = KTW.KEY.COMMON;
KTW.KEY_SET = {};

KTW.ENV_CONSTANT = KTW.CONSTANT;
KTW.DATA_MENU = KTW.DATA.MENU;
KTW.DATA_SUBHOME = KTW.DATA.SUBHOME;
KTW.DATA_FILTER = KTW.DATA.FILTER;

KTW.COMMON_IMAGE = {
    BG_DEFAULT: "images/bg_default.jpg",
    HOMESHOT_DEFAULT: "images/homeshot_default.png",
    STAR_POINT_PATH: "images/starpoint/",
    BANNER_DEFAULT: "images/banner/"
};


//create a general purpose namespace method
//this will allow us to create namespace a bit easier
KTW.createNS = function(namespace) {
    var nsparts = namespace.split(".");
    var parent = KTW;

    // we want to be able to include or exclude the root namespace
    // So we strip it if it's in the namespace
    if (nsparts[0] === "KTW") {
        nsparts = nsparts.slice(1);
    }

    // loop through the parts and create
    // a nested namespace if necessary
    for (var i = 0; i < nsparts.length; i++) {
        var partname = nsparts[i];
        // check if the current parent already has
        // the namespace declared, if not create it
        if (typeof parent[partname] === "undefined") {
            parent[partname] = {};
        }
        // get a reference to the deepest element
        // in the hierarchy so far
        parent = parent[partname];
    }
    // the parent is now completely constructed
    // with empty namespaces and can be used.
    return parent;
};

KTW.createNS("KTW.ca");
KTW.createNS("KTW.data");
KTW.createNS("KTW.exception");
KTW.createNS("KTW.homeImpl");

KTW.createNS("KTW.managers");
KTW.createNS("KTW.managers.auth");
KTW.createNS("KTW.managers.data");
KTW.createNS("KTW.managers.device");
KTW.createNS("KTW.managers.http");
KTW.createNS("KTW.managers.module");
KTW.createNS("KTW.managers.service");
KTW.createNS("KTW.managers.otmPairing");

KTW.createNS("KTW.nav");
KTW.createNS("KTW.nav.si");

KTW.createNS("KTW.oipf");
KTW.createNS("KTW.oipf.abs");
KTW.createNS("KTW.oipf.adapters");
KTW.createNS("KTW.oipf.interface");
KTW.createNS("KTW.oipf.interface.abstract");
KTW.createNS("KTW.oipf.interface.impl");
KTW.createNS("KTW.oipf.interface.util");
KTW.createNS("KTW.oipf.navigator");

KTW.createNS("KTW.ui");
KTW.createNS("KTW.ui.adaptor");
KTW.createNS("KTW.ui.component");
KTW.createNS("KTW.ui.layer");
KTW.createNS("KTW.ui.layer.setting");
KTW.createNS("KTW.ui.layer.setting.popup");
KTW.createNS("KTW.ui.view");
KTW.createNS("KTW.ui.view.channelGuide");
KTW.createNS("KTW.ui.view.epgRelatedMenu");
KTW.createNS("KTW.ui.view.homemenu");
KTW.createNS("KTW.ui.view.miniEpg");
KTW.createNS("KTW.ui.view.popup");

KTW.createNS("KTW.utils");
