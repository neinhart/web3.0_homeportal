/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>loader</code>
 *
 * @author Dongho Lee
 * @since 2016-04-22
 */

"use strict";


$(document).ready(function () {
	KTW.main.ready();
});

/**
 *  callback after loaded HTML completely
 */
$(window).bind('load', function() {
	KTW.main.load();
});

/**
 * callback before unload
 */
$(window).bind('onbeforeunload', function(e) {
    KTW.main.beforeunload();
    return '';
});

/**
 * callback after unloaded
 */
$(window).bind('unload', function() {
    KTW.main.unload();
});
