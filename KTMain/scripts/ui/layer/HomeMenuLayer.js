"use strict";

(function() {
    /**
     * HomeMenuLayer Layer
     */
    KTW.ui.layer.HomeMenuLayer = function HomeMenuLayer (options) {
        KTW.ui.Layer.call(this, options);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var homeMenuView = null;
        var homeWeatherClockView = null;
        var homeShotArea = null;

        var exitTimer = null;
        var isAutoExitTimerStart = false;

        var homeShotData = null;
        var focusHomeShotIndex = -1;
        var homeShotTimer = null;

        var logoSrc = null;
        
        var GUITESTCODE = false;
        
        var otmPairingNotiArea = null;
        var notiText = null;

        var _this = this;
        
        function createBackground () {
            var background = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "home_area_backgound",
                    css: {
                        position: "absolute", overflow: "visible"
                    }
                }
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    class : "shortcut_icon_area",
                    src: "images/bg_1d_1.png",
                    css: {
                        position: "absolute"
                    }
                },
                parent: background
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class : "menu_area",
                    src: "images/bg_2d_2.png",
                    css: {
                        position: "absolute"
                    }
                },
                parent: background
            });
            
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "top_background_area",
                    css: {
                        position: "absolute"
                        
                    }
                },
                parent: background
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "bottom_background_area",
                    css: {
                        position: "absolute"
                    }
                },
                parent: background
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    name : "logo",
                    css: {
                        position: "absolute", left:132 , top:52 
                    }
                },
                parent: background
            });

            return background;
        }

        function createOtmPairingNoti() {
            
            otmPairingNotiArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "root_div",
                    css: {
                        position: "absolute",
                        left: 0, top: 914, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 166,display:"none"
                    }
                },
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_img",
                    src: KTW.CONSTANT.IMAGE_PATH + "caption_bg.png",
                    css: {
                        position: "absolute", left: 0, top: 166-141, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 141
                    }
                },
                parent: otmPairingNotiArea
            });


            notiText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left : 0 , top: 983-914 , width : KTW.CONSTANT.RESOLUTION.WIDTH, height: 32,
                        color: "rgba(255,255,255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "올레 tv에서 구매한 콘텐츠를 올레 tv 모바일에서도 시청 가능합니다",
                parent: otmPairingNotiArea
            });


            return otmPairingNotiArea;
        }

        function createHomeShot () {
            homeShotArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1390, top: 630, width: 530, height: 450, visibility: "hidden"

                    }
                }
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "home_shot_img",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 530, height: 450
                    }
                },
                parent: homeShotArea
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "home_shot_keyguide_img",
                    src: "images/home/btn_quickview.png",
                    css: {
                        position: "absolute", left: 269, top: 357, width: 184, height: 55
                    }
                },
                parent: homeShotArea
            });

            return homeShotArea;
        }

        function drawLogo (_this) {
            log.printDbg("drawLogo()");

            var biLogo = KTW.managers.data.OCImageDataManager.getBiLogo();
            logoSrc = "images/h_ollehtv.png";
            if (KTW.CONSTANT.IS_OTS) {
                logoSrc = "images/h_ollehtvskylife.png";
            }
            if (biLogo && biLogo.path && biLogo.imageList && biLogo.imageList.length > 0) {
                for (var i = 0; i < biLogo.imageList.length; i++) {
                    if (KTW.CONSTANT.IS_OTS && biLogo.imageList[i].indexOf("ots") > -1) {
                        logoSrc = biLogo.path + biLogo.imageList[i];
                        break;
                    }
                    else if (!KTW.CONSTANT.IS_OTS && biLogo.imageList[i].indexOf("otv") > -1) {
                        logoSrc = biLogo.path + biLogo.imageList[i];
                        break;
                    }
                }
            }
            _this.div.find(".home_area_backgound [name=logo]").attr({src: logoSrc});
        }

        function initHomeShotData (bAutoExit) {
            log.printDbg("initHomeShotData()");

            homeShotArea.css({visibility: "hidden"});

            if (!bAutoExit && KTW.managers.service.AppServiceManager.checkFirstKey()) {
                KTW.managers.service.HomeShotDataManager.getHomeMenuHomeShot(function (arrHomeShot) {
                    homeShotData = arrHomeShot;

                    focusHomeShotIndex = 0;


                    // test
                    //homeShotData = [];
                    //homeShotData.push({
                    //    transactionId: 0,
                    //    catId: "root",
                    //    hsImgUrl: "http://125.144.104.41/HOMESHOT/201802/W3H0000000000000181803.png",
                    //    hsType: 0,
                    //    hsTargetType: "0",
                    //    hsTargetCatId: "10000000000000210664",
                    //    //hsTargetAssetId: data.hs_asset_id,
                    //    //hsTargetLocator: data.hs_locator,
                    //    //hsTargetParameter: data.hs_param,
                    //
                    //    hsStartDate: null,
                    //    hsExpireDate: null,
                    //
                    //    hsDurationTime: 5,
                    //    hsDisplayCnt: "A",
                    //
                    //    hsRealDisplayCnt: 0,
                    //
                    //    reqCd: null
                    //});
                    //homeShotData.push({
                    //    transactionId: 0,
                    //    catId: "root",
                    //    hsImgUrl: "http://125.144.104.41/HOMESHOT/201802/W3H0000000000000182413.png",
                    //    hsType: 0,
                    //    hsTargetType: "1",
                    //    hsTargetCatId: "CV000000000027119684",
                    //    //hsTargetAssetId: data.hs_asset_id,
                    //    //hsTargetLocator: data.hs_locator,
                    //    //hsTargetParameter: data.hs_param,
                    //
                    //    hsStartDate: null,
                    //    hsExpireDate: null,
                    //
                    //    hsDurationTime: 5,
                    //    hsDisplayCnt: 2,
                    //
                    //    hsRealDisplayCnt: 0,
                    //
                    //    reqCd: null
                    //});
                    //homeShotData.push({
                    //    transactionId: 0,
                    //    catId: "root",
                    //    hsImgUrl: "http://125.144.104.41/HOMESHOT/201802/W3H0000000000000182804.png",
                    //    hsType: 0,
                    //    hsTargetType: "2",
                    //    hsTargetCatId: "10000000000000024417",
                    //    hsTargetAssetId: "MV6E6006SGL150000100",
                    //    //hsTargetLocator: data.hs_locator,
                    //    //hsTargetParameter: data.hs_param,
                    //
                    //    hsStartDate: null,
                    //    hsExpireDate: null,
                    //
                    //    hsDurationTime: 5,
                    //    hsDisplayCnt: 4,
                    //
                    //    hsRealDisplayCnt: 0,
                    //
                    //    reqCd: null
                    //});
                    //homeShotData.push({
                    //    transactionId: 0,
                    //    catId: "root",
                    //    hsImgUrl: "http://125.144.104.41/HOMESHOT/201802/W3H0000000000000181810.png",
                    //    hsType: 0,
                    //    hsTargetType: "3",
                    //    //hsTargetCatId: "10000000000000024417",
                    //    //hsTargetAssetId: "MV6E6006SGL150000100",
                    //    hsTargetLocator: "1.20F.20F",
                    //    hsTargetParameter: null,
                    //
                    //    hsStartDate: null,
                    //    hsExpireDate: null,
                    //
                    //    hsDurationTime: 5,
                    //    hsDisplayCnt: 6,
                    //
                    //    hsRealDisplayCnt: 0,
                    //
                    //    reqCd: null
                    //});
                    //homeShotData.push({
                    //    transactionId: 0,
                    //    catId: "root",
                    //    hsImgUrl: "http://125.144.104.41/HOMESHOT/201710/W3H0000000000000136408.png",
                    //    hsType: 0,
                    //    hsTargetType: "3",
                    //    //hsTargetCatId: "10000000000000024417",
                    //    //hsTargetAssetId: "MV6E6006SGL150000100",
                    //    hsTargetLocator: "4e30.3020",
                    //    hsTargetParameter: "805",
                    //
                    //    hsStartDate: null,
                    //    hsExpireDate: null,
                    //
                    //    hsDurationTime: 5,
                    //    hsDisplayCnt: 8,
                    //
                    //    hsRealDisplayCnt: 0,
                    //
                    //    reqCd: null
                    //});
                    //homeShotData.push({
                    //    transactionId: 0,
                    //    catId: "root",
                    //    hsImgUrl: "http://125.144.104.41/HOMESHOT/201802/W3H0000000000000182806.png",
                    //    hsType: 0,
                    //    hsTargetType: 8 + "",
                    //    //hsTargetCatId: "10000000000000024417",
                    //    //hsTargetAssetId: "MV6E6006SGL150000100",
                    //    hsTargetLocator: "http://wimg.ktipmedia.co.kr/img/mms/mashupapp/gift/index.html?btnCode=G011&contsId=MW0I10MSSGL150000100",
                    //    hsTargetParameter: null,
                    //
                    //    hsStartDate: null,
                    //    hsExpireDate: null,
                    //
                    //    hsDurationTime: 5,
                    //    hsDisplayCnt: 9,
                    //
                    //    hsRealDisplayCnt: 0,
                    //
                    //    reqCd: null
                    //});



                    startHomeShot();
                });
            }
            else {
                homeShotData = [];
            }
        };

        function startHomeShot () {
            log.printDbg("startHomeShot()");

            clearTimeout(homeShotTimer);
            homeShotTimer = null;

            if (homeShotData.length > 0) {
                homeShotArea.css({visibility: "inherit"});
            }

            updateHomeShot();
        }

        function updateHomeShot () {
            log.printDbg("updateHomeShot()");

            if (homeShotData.length === 0) {
                stopHomeShot();
                return;
            }

            var homeShot = homeShotData[focusHomeShotIndex];

            var time = 3 * 1000;

            if (homeShot) {
                homeShotArea.find("[name=home_shot_img]").attr("src", homeShot.hsImgUrl);

                if (homeShot.hsDurationTime) {
                    time = homeShot.hsDurationTime;
                }
            }

            if (homeShotData.length > 0) {
                homeShotTimer = setTimeout(function () {
                    if (homeShot.hsType === KTW.managers.service.HomeShotDataManager.TYPE.CATEGORY && homeShot.hsDisplayCnt !== "A") {
                        ++homeShot.hsRealDisplayCnt;

                        KTW.managers.service.HomeShotDataManager.updateShowtCategoryHomeShot({
                            catId: "root",
                            transactionId: homeShot.transactionId,
                            hsRealDisplayCnt: homeShot.hsRealDisplayCnt
                        });

                        //log.printDbg(focusHomeShotIndex + " - " + homeShot.hsDisplayCnt + " - " + homeShot.hsRealDisplayCnt);

                        if (homeShot.hsDisplayCnt <= homeShot.hsRealDisplayCnt) {
                            log.printDbg("remove homeShot.... " + homeShot.hsDisplayCnt + " - " + homeShot.hsRealDisplayCnt);

                            homeShotData.splice(focusHomeShotIndex, 1);
                            --focusHomeShotIndex;
                        }
                    }

                    ++focusHomeShotIndex;

                    if (focusHomeShotIndex > homeShotData.length - 1) {
                        focusHomeShotIndex = 0;
                    }

                    updateHomeShot();
                }, time * 1000);
            }
            else {
                homeShotArea.css({visibility: "hidden"});
            }
        }

        function stopHomeShot () {
            log.printDbg("stopHomeShot()");

            clearTimeout(homeShotTimer);
            homeShotTimer = null;

            homeShotArea.css({visibility: "hidden"});
        }

        function actHomeShot () {
            log.printDbg("actHomeShot()");

            if (homeShotData.length > 0) {
                var homeShot = homeShotData[focusHomeShotIndex];

                if (homeShot) {
                    var options = {
                        itemType: homeShot.hsTargetType,
                        req_cd: homeShot.reqCd ? homeShot.reqCd : 25
                    };

                    switch (homeShot.hsTargetType) {
                        case KTW.DATA.MENU.ITEM.CATEGORY:
                        case KTW.DATA.MENU.ITEM.SERIES:
                            options.cat_id = homeShot.hsTargetCatId;
                            break;
                        case KTW.DATA.MENU.ITEM.CONTENT:
                            options.const_id = homeShot.hsTargetAssetId;
                            options.cat_id = homeShot.hsTargetCatId;
                            break;
                        case KTW.DATA.MENU.ITEM.INT_M:
                        case KTW.DATA.MENU.ITEM.INT_U:
                            options.locator = homeShot.hsTargetLocator;
                            options.parameter = homeShot.hsTargetParameter;
                            break;
                        case KTW.DATA.MENU.ITEM.INT_W:
                            options.locator = homeShot.hsTargetLocator;
                            break;
                    }

                    KTW.managers.service.MenuServiceManager.jumpByMenuData(options);

                    // TODO 통계 로그 작성 요망

                    return true;
                }
            }

            return false;
        }

        function onNodeMatchListener (element) {
            var layer = KTW.ui.LayerManager.getLastLayerInStack();
            if (layer.id !== _this.id) {
                return;
            }

            if (homeMenuView) {
                homeMenuView.onNodeMatchListener(element)
            }
        }


        this.startAutoExit = function () {
            if (!exitTimer) {
                var _this = this;
                exitTimer = setTimeout(function () {
                    KTW.ui.LayerManager.deactivateLayer({
                        id: _this.id
                    });
                }, 15 * 1000);

                isAutoExitTimerStart = true;
            }
        };

        this.endAutoExit = function () {
            clearTimeout(exitTimer);
            exitTimer = null;
            isAutoExitTimerStart = false;
        };
        
        this.menuFocus = function () {
            this.div.find(".home_area_backgound").addClass("focus");
        };

        this.menuUnFocus = function () {
            this.div.find(".home_area_backgound").removeClass("focus");
        };

        this.showOtmPairingNotiMessage = function () {
            otmPairingNotiArea.css("display" , "");
        };

        this.hideOtmPairingNotiMessage = function () {
            otmPairingNotiArea.css("display" , "none");

        };


        // this.getGuiTestCode = function() {
        //   return GUITESTCODE;  
        // };

        this.init = function () {
            this.div.attr({class: "arrange_frame"});

            this.div.append(createBackground());

            homeMenuView = new KTW.ui.view.HomeMenuView(this);
            homeMenuView.create();

            homeWeatherClockView = new KTW.ui.view.HomeWeatherClockView(this);
            homeWeatherClockView.create();

            this.div.append(createHomeShot());
            
            this.div.append(createOtmPairingNoti());
        };

        this.showView = function (options) {
            drawLogo(this);

            homeMenuView.show(options);
            homeWeatherClockView.show(options);

            if (!options || !options.resume) {
                this.menuFocus();

                initHomeShotData(this.params.bAutoExit);
            }
            else {
                startHomeShot();
            }

            /**
             * 미리보기 가이드 노출을 위해 ChannelPreviewManager API 호출함.
             */
            // KTW.managers.service.ChannelPreviewManager.notifyEventShowHomeMenu(true);


            if (this.params && this.params.bAutoExit) {
                this.params.bAutoExit = false;
                this.startAutoExit();
            }

            KTW.managers.service.VoiceableManager.addNodeMatchListener(onNodeMatchListener);
        };

        this.hideView = function (options) {
            log.printDbg("hideView() ");

            homeMenuView.hide(options);
            homeWeatherClockView.hide(options);

            stopHomeShot();

            // if (!options || !options.pause) {
            //     KTW.managers.service.ChannelPreviewManager.notifyEventShowHomeMenu(false);
            // }

            this.endAutoExit();

            KTW.managers.service.VoiceableManager.removeNodeMatchListener(onNodeMatchListener);
        };

        this.destroyView = function () {
            homeMenuView.destroy();
            homeWeatherClockView.destroy();

            homeMenuView = null;
            homeWeatherClockView = null;
        };

        this.controlKey = function (key_code) {
            log.printDbg("controlKey() key_code : " + key_code);
            this.endAutoExit();

            var ret = false;

            switch (key_code) {
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:
                case KTW.KEY_CODE.OK:
                    ret = homeMenuView.controlKey(key_code);
                    break;
                case KTW.KEY_CODE.BLUE:
                    // TODO Test Code 주석 처리, 개발시에만 주석 풀고 사용하
                    // test code
                    //KTW.ui.LayerManager.deactivateLayer({
                    //    id: this.id
                    //});
                    //
                    //setTimeout(function () {
                    //    $("object").remove();
                    //    window.location.reload(true);
                    //}, 1000);
                    //
                    //ret = true;

                    break;
                case KTW.KEY_CODE.PLAY :
                    ret = actHomeShot();
                    break;
            }

            switch (key_code) {
/*                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:*/
                case KTW.KEY_CODE.BACK:
/*                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.HOME_MENU,
                        act: key_code,
                        catId: "",
                        catName: ""
                    });*/
                    break;
            }

            return ret;
        }
    };

    KTW.ui.layer.HomeMenuLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.HomeMenuLayer.prototype.constructor = KTW.ui.layer.HomeMenuLayer;

    //Override create function
    KTW.ui.layer.HomeMenuLayer.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        
        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.HomeMenuLayer.prototype.show = function (options) {
        KTW.ui.Layer.prototype.show.call(this, options);

        this.showView(options);
    };

    KTW.ui.layer.HomeMenuLayer.prototype.hide = function (options) {
        this.hideView(options);

        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.HomeMenuLayer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();

