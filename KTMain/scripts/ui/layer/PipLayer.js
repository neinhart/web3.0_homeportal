/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>PipLayer</code>
 *
 * @author dj.son
 * @since 2016-10-26
 */

"use strict";

(function() {
    var PIP_TYPE = KTW.managers.service.PipManager.PIP_TYPE;
    var BACK_IFRAME_TYPE = KTW.managers.service.PipManager.BACK_IFRAME_TYPE;

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var networkManager = KTW.managers.device.NetworkManager;

    var CHANNEL_CONFIG = KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG;
    var type = PIP_TYPE.NONE;
    var iframeType = BACK_IFRAME_TYPE.ETC;

    var pip_channel_control = null;
    var pipChangeChannel = null;
    var pipTuneTimer = null;
    var previewtenSecTimer = null;
    var DELAY_FAST_TUNE = 500;

    var iframeArea = null;
    var miniEpgSubArea = null;
    var multiViewSubArea = null;
    var reservationSubArea = null;
    var fullEpgSubArea = null;

    var _this = null;

    var remainTime = 0;

    var isShowIframe = false;
    var isHdrVOD = false;
    var hdrBlockHideTimer = null;

    var pipPosition = null;

    function createElement () {
        log.printDbg("createElement()");

        _this.div.css({visibility: "visible"}).addClass("arrange_frame");

        iframeArea = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "iframe_area",
                css: {
                    position: "absolute"
                }
            },
            parent: _this.div
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "iframe_block_area",
                css: {
                    position: "absolute" , left: 0, top: 0, "background-color": "rgba(0, 0, 0, 1)"
                }
            },
            parent: iframeArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "iframe_img",
                css: {
                    position: "absolute", left: 0, top: 0
                }
            },
            parent: iframeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "hdr_iframe_img",
                src: "images/iframe/block_hdr.png",
                css: {
                    position: "absolute", left: 0, top: 0
                }
            },
            parent: iframeArea
        });

        var pipPreviewDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "pip_iframe_preview_area",
                css: {
                    position: "absolute" , top: 12 , width: 106, height: 94
                }
            },
            parent: iframeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "preview_background",
                src: "images/pipview/bg_1min_preview_s.png",
                css: {
                    position: "absolute", left: 0, top: 0, width: 94, height: 94
                }
            },
            parent: pipPreviewDiv
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "preview_foreground",
                src: "images/pipview/img_progress_1min_preview_w_s.png",
                css: {
                    position: "absolute", left: 0, top: 0, width: 94, height: 94,display:""
                }
            },
            parent: pipPreviewDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "preview_time",
                class: "font_m",
                css: {
                    position: "absolute", left: 0 , top : 36, width:0,height: 27,
                    color: "rgba(255, 255, 255, 1)", "font-size": 25 , "text-align" : "left", "letter-spacing":-1.25
                }
            },
            parent: pipPreviewDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "preview_unit",
                class: "font_m",
                css: {
                    position: "absolute", left: 0 , top : 40, width:20,height: 20,
                    color: "rgba(255, 255, 255, 1)", "font-size": 18 , "text-align" : "left", "letter-spacing":-0.45
                }
            },
            parent: pipPreviewDiv
        });

        miniEpgSubArea = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "mini_epg_sub_area",
                css: {
                    position: "absolute"
                }
            },
            parent: _this.div
        });

        multiViewSubArea = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "multi_view_sub_area",
                css: {
                    position: "absolute", overflow: "visible"
                }
            },
            parent: _this.div
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: -2, top: -2, border: "solid 2px"
                }
            },
            parent:  multiViewSubArea
        });

        reservationSubArea = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "reservation_sub_area",
                css: {
                    position: "absolute"
                }
            },
            parent: _this.div
        });

        fullEpgSubArea = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "full_epg_sub_area",
                css: {
                    position: "absolute"
                }
            },
            parent: _this.div
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/pipview/shot_shadow.png",
                css: {
                    position: "absolute", left: 0, top: 80, width: 416, height: 154
                }
            },
            parent: fullEpgSubArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "channel_icon",
                src: "images/icon/icon_sky_epg.png",
                css: {
                    position: "absolute", top: 180
                }
            },
            parent: fullEpgSubArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_b",
                name: "channel_num",
                css: {
                    position: "absolute", left: 61, top: 177, "font-size": 32, color: "rgba(255, 255, 255, 1)", height : 34,
                    "letter-spacing": -1.6
                }
            },
            parent: fullEpgSubArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_l",
                name: "channel_name",
                css: {
                    position: "absolute", left: 137, top: 177, "font-size": 30, color: "rgba(255, 255, 255, 1)", height : 32,
                    "letter-spacing": -1.5
                }
            },
            parent: fullEpgSubArea
        });
    }

    function initUI () {
        log.printDbg("initUI()");

        iframeArea.css({visibility: "inherit"});
        iframeArea.children().css({"visibility": "hidden"});

        switch (type) {
            case PIP_TYPE.NONE:
                miniEpgSubArea.css({visibility: "hidden"});
                multiViewSubArea.css({visibility: "hidden"});
                reservationSubArea.css({visibility: "hidden"});
                fullEpgSubArea.css({visibility: "hidden"});
                break;
            case PIP_TYPE.MINI_EPG:
                miniEpgSubArea.css({visibility: "inherit"});
                multiViewSubArea.css({visibility: "hidden"});
                reservationSubArea.css({visibility: "hidden"});
                fullEpgSubArea.css({visibility: "hidden"});
                break;
            case PIP_TYPE.MULTI_VIEW:
                miniEpgSubArea.css({visibility: "hidden"});
                multiViewSubArea.css({visibility: "inherit"});
                reservationSubArea.css({visibility: "hidden"});
                fullEpgSubArea.css({visibility: "hidden"});
                break;
            case PIP_TYPE.RESERVATION:
                miniEpgSubArea.css({visibility: "hidden"});
                multiViewSubArea.css({visibility: "hidden"});
                reservationSubArea.css({visibility: "inherit"});
                fullEpgSubArea.css({visibility: "hidden"});
                break;
            case PIP_TYPE.FULL_EPG:
            case PIP_TYPE.FAV_EPG:
                miniEpgSubArea.css({visibility: "hidden"});
                multiViewSubArea.css({visibility: "hidden"});
                reservationSubArea.css({visibility: "hidden"});
                fullEpgSubArea.css({visibility: "inherit"});
                break;
        }
    }

    function drawSubArea (options) {
        log.printDbg("drawSubArea()");

        options = options || {};

        switch (type) {
            case PIP_TYPE.MINI_EPG:
                // TODO mini epg sub area draw
                break;
            case PIP_TYPE.MULTI_VIEW:
                // TODO multi view sub area draw
                break;
            case PIP_TYPE.RESERVATION:
                // TODO reservation sub area draw
                break;
            case PIP_TYPE.FULL_EPG:
            case PIP_TYPE.FAV_EPG:
                // TODO full epg sub area draw

                if (options.channel) {
                    fullEpgSubArea.find("span[name=channel_num]").text(util.numToStr(options.channel.majorChannel, 3));

                    var name = options.channel.name;

                    var arrChName = util.splitText(name, "RixHead L", 30, 270, -1.5);
                    if (arrChName.length > 1) {
                        name = "";
                        for (var j = 0; j < arrChName.length; j++) {
                            name += arrChName[j] + "\n";
                        }
                        name = name.slice(0, name.length - 1);

                        fullEpgSubArea.find("span[name=channel_name]").css({top: 161, "line-height": 1.1}).text(name);
                    }
                    else {
                        fullEpgSubArea.find("span[name=channel_name]").css({top: 177, "line-height": 1}).text(name);
                    }

                    var iconSrc = "";
                    var iconCssObj = {left: 27};
                    if (KTW.CONSTANT.IS_OTS && type === PIP_TYPE.FAV_EPG) {
                        if (options.channel.favIDs.indexOf(CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE) > -1) {
                            iconSrc = "images/icon/icon_sky_epg.png";
                        }
                        else if (options.channel.favIDs.indexOf(CHANNEL_CONFIG.FAVOURITE_FAVORITE) > -1) {
                            iconSrc = "images/icon/icon_olleh_epg.png";
                        }

                        iconCssObj.left = 10;
                    }
                    else {
                        if (navAdapter.isBlockedChannel(options.channel)) {
                            iconSrc = "images/icon/icon_block.png";
                        }
                        else {
                            // 선호채널 확인 후 fav icon visible
                            if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(options.channel)) {
                                iconSrc = "images/icon/icon_fav.png";
                            }
                        }
                    }

                    fullEpgSubArea.find("img[name=channel_icon]").attr({src: iconSrc}).css(iconCssObj);
                }
                else {
                    fullEpgSubArea.find("span[name=channel_num]").text("");
                    fullEpgSubArea.find("span[name=channel_name]").text("");
                    fullEpgSubArea.find("img[name=channel_icon]").attr({src: ""});
                }
                break;
        }
    }

    function setSubAreaSize (options) {
        log.printDbg("setSubAreaSize(" + log.stringify(options) + ")");

        iframeArea.css(options);
        iframeArea.children().css({width: options.width, height: options.height});
        iframeArea.find("#pip_iframe_preview_area").css({
            left: (options.width - 105)
        });

        switch (type) {
            case PIP_TYPE.MINI_EPG:
                miniEpgSubArea.css(options);
                break;
            case PIP_TYPE.MULTI_VIEW:
                multiViewSubArea.css(options);
                multiViewSubArea.css({height: (options.height - 2)});
                multiViewSubArea.children().css({width: options.width, height: (options.height - 2)});
                break;
            case PIP_TYPE.RESERVATION:
                reservationSubArea.css(options);
                break;
            case PIP_TYPE.FULL_EPG:
            case PIP_TYPE.FAV_EPG:
                fullEpgSubArea.css(options);
                break;
        }
    }

    function startPip () {
        log.printDbg("startPip()");

        var mode, left, top, width, height;

        if (type === PIP_TYPE.MINI_EPG) {
            mode = KTW.nav.Def.PIP_MODE.MINI_EPG;
            left = 65;
            top = 742;
            width = 336;
            height = 189;
        }
        else if (type === PIP_TYPE.MULTI_VIEW) {
            mode = KTW.nav.Def.PIP_MODE.MULTI_VIEW;
            left = 1266;
            top = 640;
            width = 592;
            height = 333;
        }
        if (type === PIP_TYPE.RESERVATION) {
            mode = KTW.nav.Def.PIP_MODE.RESERVATION;
            left = 1424;
            top = 497;
            width = 391;
            height = 220;
        }
        else if (type === PIP_TYPE.FULL_EPG || type === PIP_TYPE.FAV_EPG) {
            mode = type === PIP_TYPE.FULL_EPG ? KTW.nav.Def.PIP_MODE.FULL_EPG : KTW.nav.Def.PIP_MODE.FAV_EPG;
            left = 174;
            top = 356;
            width = 416;
            height = 234;
        }

        setSubAreaSize({
            left: left, top: top, width: width, height: height
        });

        pip_channel_control.start(mode, {
            position: "absolute", left: left + "px", top: top + "px", width: width + "px", height: height + "px"
        });

        _setPipSize({
            left: left, top: top, width: width, height: height
        });

        pip_channel_control.vbo.style.opacity = 1;

        if(KTW.CONSTANT.IS_OTS === true && (type === PIP_TYPE.MULTI_VIEW || type === PIP_TYPE.RESERVATION || type === PIP_TYPE.FULL_EPG ||  type === PIP_TYPE.FAV_EPG) ) {
            networkManager.addWeakSignalListener(_weakSignalListener);
        }

        if(type !== PIP_TYPE.MULTI_VIEW) {
            iframeArea.find("#hdr_iframe_img").attr("src", "images/iframe/dim_hdr2.png");
        }else {
            iframeArea.find("#hdr_iframe_img").attr("src", "images/iframe/block_hdr.png");
        }
        iframeArea.find("#hdr_iframe_img").css({visibility: "hidden"});
    }

    function _setPipSize (options) {
        log.printDbg("_setPipSize(" + log.stringify(options) + ")");


        pipPosition = options;

        if (!options) {
            log.printDbg("options is null... so return;");
            return;
        }

        var left = options.left || 0;
        var top = options.top || 0;
        var width = options.width || 0;
        var height = options.height || 0;

        // pip_channel_control.vbo.style.left = left + "px";
        // pip_channel_control.vbo.style.top = top + "px";
        // pip_channel_control.vbo.style.width = width + "px";
        // pip_channel_control.vbo.style.height = height + "px";
        var stylewidth = width;
        var styleheight = height;

        if (type === PIP_TYPE.MULTI_VIEW) {

        }else if(type === PIP_TYPE.RESERVATION || type === PIP_TYPE.MINI_EPG) {
            stylewidth = width-1;
            styleheight = height-1;
            setSubAreaSize({
                left: left, top: top, width: stylewidth, height: styleheight
            });
        }

        pip_channel_control.setVideoSize(left, top, width, height , stylewidth , styleheight);

        var layer = KTW.ui.LayerManager.getLayer("PipMultiViewPopup");
        if(layer !== undefined && layer !== null) {
            log.printDbg('PipMultiViewPopup Search OK');
            layer.child.showGuideView();
        }

    }



    /**
     * pip channel event listener
     */
    function _pipChEventListener (evt) {
        log.printDbg('_pipChEventListener() evt: ' + log.stringify(evt));

        if (evt != null) {
            var ch = evt.channel;

            _clearPreviewTimer();

            if (ch != null) {
                var iframeData = null;
                iframeType = BACK_IFRAME_TYPE.ETC;
                if (evt.permission === KTW.nav.Def.CHANNEL.PERM.BLOCKED && evt.blocked_reason === KTW.nav.Def.CHANNEL.BLOCKED_REASON.USER_BLOCKED) {
                    iframeType = BACK_IFRAME_TYPE.BLOCKED;
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.PR_BLOCKED) {
                    iframeType = BACK_IFRAME_TYPE.AGE_LIMIT;
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED) {
                    if (util.isSkyChoiceChannel(ch) === true) {
                        iframeType = BACK_IFRAME_TYPE.SKY_CHOICE;
                    }
                    else if (ch.name.indexOf('캐치온') > -1) {
                        iframeType = BACK_IFRAME_TYPE.NOT_SUBSCRIBED;
                    }
                    else {
                        iframeType = BACK_IFRAME_TYPE.NOT_SUBSCRIBED;
                    }
                }
                else if (navAdapter.isAudioChannel(ch, true)) {
                    iframeType = BACK_IFRAME_TYPE.AUDIO;
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.BLOCKED) {
                    if (KTW.CONSTANT.IS_OTS && !KTW.oipf.AdapterHandler.hwAdapter.enableTuner &&
                        (ch.idType === Channel.ID_DVB_S || ch.idType === Channel.ID_DVB_S2)) {
                        iframeType = BACK_IFRAME_TYPE.WEAK_SIGNAL;
                    }
                    else if (evt.blocked_reason === KTW.nav.Def.CHANNEL.BLOCKED_REASON.NO_SD_CHANNEL) {
                        iframeType = BACK_IFRAME_TYPE.NO_SD;
                    }
                    else if (evt.blocked_reason === KTW.nav.Def.CHANNEL.BLOCKED_REASON.UHD_CHANNEL) {
                        iframeType = BACK_IFRAME_TYPE.UHD;
                    }
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.PREVIEW && !util.isSkyChoiceChannel(ch)) {
                    iframeType = BACK_IFRAME_TYPE.PREVIEW;
                    iframeData = getPreviewDuration(evt.preview_data);
                }

                /**
                 * Iframe으로 안 막혀져 있는 상태에서 성인채널이면 성인채널 Iframe 노출
                 */

                if(iframeType === BACK_IFRAME_TYPE.ETC || iframeType === BACK_IFRAME_TYPE.PREVIEW) {
                    ch = KTW.utils.epgUtil.reconstructionChList([ch] , true)[0];
                    if(ch.desc === 2) {
                        if(iframeType === BACK_IFRAME_TYPE.ETC) {
                            iframeType = BACK_IFRAME_TYPE.ADULT;
                        }
                    }
                }

                _showIframe({
                    type: iframeType,
                    data: iframeData
                });
            }

            drawSubArea({channel: ch});

            iframeArea.find("#iframe_block_area").css({visibility: "hidden"});
        }
    }


    function getPreviewDuration (params) {
        var duration = 0;
        var ch_preview_time = params.ch_preview_time;
        var preview_time = params.preview_time;

        if (ch_preview_time <= preview_time) {
            duration = ch_preview_time;
        }
        else if (ch_preview_time > preview_time) {
            duration = preview_time;
        }

        return duration;
    }

    function _showIframe (options) {
        log.printDbg("_showIframe(" + log.stringify(options) + ")");

        var img = null;

        iframeArea.find("#pip_iframe_preview_area").css({visibility: "hidden"});

        switch (options.type) {
            case BACK_IFRAME_TYPE.CURRENT :
                img = "block_now.jpg";
                break;
            case BACK_IFRAME_TYPE.BLOCKED :
                if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                    img = "block_kids.jpg";
                }else {
                    img = "block_ch.jpg";
                }
                break;
            case BACK_IFRAME_TYPE.AGE_LIMIT :
                img = "block_age.jpg";
                break;
            case BACK_IFRAME_TYPE.NOT_SUBSCRIBED :
                img = "block_unsub.jpg";
                break;
            case BACK_IFRAME_TYPE.AUDIO :
                img = "block_audio.jpg";
                break;
            case BACK_IFRAME_TYPE.SKY_CHOICE :
                img = "block_moviechoice.jpg";
                break;
            case BACK_IFRAME_TYPE.CATCH_ON :
                /**
                 * TODO 해당 부분 UX팀에 확인 필요함.
                 */
                break;
            case BACK_IFRAME_TYPE.NO_SD :
                if(type === PIP_TYPE.FULL_EPG || type === PIP_TYPE.FAV_EPG || type === PIP_TYPE.GENRE_EPG) {
                    img = "block_no_sd2.jpg";
                }else {
                    img = "block_no_sd.jpg";
                }
                break;
            case BACK_IFRAME_TYPE.UHD :
                if(type === PIP_TYPE.MULTI_VIEW) {
                    img = "block_no_uhd.jpg";
                }else {
                    if(type === PIP_TYPE.FULL_EPG || type === PIP_TYPE.FAV_EPG || type === PIP_TYPE.GENRE_EPG) {
                        img = "block_no_sd2.jpg";
                    }else {
                        img = "block_no_sd.jpg";
                    }
                }
                break;
            case BACK_IFRAME_TYPE.PREVIEW :
                if (options.data != null) {
                    if (type === PIP_TYPE.MINI_EPG || type === PIP_TYPE.RESERVATION || type === PIP_TYPE.FULL_EPG || type === PIP_TYPE.FAV_EPG || type === PIP_TYPE.MULTI_VIEW) {
                        var time = "";
                        var unit = "";
                        if (options.data > 0) {
                            if(options.data % 60 === 0) {
                                time = ""+ (options.data / 60);
                                unit = "분";
                            }else {
                                time = "" + options.data;
                                unit = "초";
                            }

                            remainTime = options.data;
                            var timeTextLength = 0;
                            var timeUnitTextLength = 0;

                            timeTextLength = util.getTextLength(time , "RixHead M" , 25,-1.25);
                            timeUnitTextLength = util.getTextLength(unit , "RixHead M" , 18 , -0.45);

                            var startLeft = (94 - (timeTextLength+4+timeUnitTextLength))/2;

                            iframeArea.find("#pip_iframe_preview_area #preview_foreground").attr("src", "images/pipview/img_progress_1min_preview_w_s.png");


                            iframeArea.find("#pip_iframe_preview_area #preview_time").text(time);
                            iframeArea.find("#pip_iframe_preview_area #preview_unit").text(unit);
                            iframeArea.find("#pip_iframe_preview_area #preview_time").css({left:startLeft,width:timeTextLength});
                            iframeArea.find("#pip_iframe_preview_area #preview_time").css({ color: "rgba(255, 255, 255, 1)"});
                            startLeft+=timeTextLength;
                            startLeft+=4;
                            iframeArea.find("#pip_iframe_preview_area #preview_unit").css({left:startLeft,width:timeUnitTextLength});
                            iframeArea.find("#pip_iframe_preview_area").css({visibility: "inherit"});


                            var checkTime = remainTime-10;
                            log.printDbg("show() checkTime : " + checkTime);
                            previewtenSecTimer = setTimeout(checkPreViewTime, checkTime*1000);

                        }else {
                            iframeArea.find("#pip_iframe_preview_area").css({visibility: "hidden"});
                        }
                    }
                }
                break;
            case BACK_IFRAME_TYPE.WEAK_SIGNAL :
                img = "block_signal.jpg";
                break;
            case BACK_IFRAME_TYPE.ADULT :
                img = "block_adult_ch.jpg";
                break;
            default :
                break;
        }

        if (img === null) {
            iframeArea.find("#iframe_img").attr("src", "").css({visibility: "hidden"});
            iframeArea.find("#iframe_block_area").css({visibility: "hidden"});
            isShowIframe = false;
        }
        else {
            iframeArea.find("#iframe_img").attr("src", KTW.CONSTANT.IMAGE_PATH + "iframe/"+ img).css({visibility: "inherit"});
            iframeArea.find("#iframe_block_area").css({visibility: "hidden"});
            isShowIframe = true;
        }

    }

    function checkPreViewTime() {
        log.printDbg("checkPreViewTime(), remainTime = " + remainTime);

        previewtenSecTimer = null;
        if(remainTime>10) {
            remainTime = 10;
        }else {
            remainTime--;
        }
        var time = "" + remainTime;
        var unit = "초";
        var timeTextLength = util.getTextLength(time , "RixHead M" , 25,-1.25);
        var timeUnitTextLength = util.getTextLength(unit , "RixHead M" , 18 , -0.45);

        var startLeft = (94 - (timeTextLength+4+timeUnitTextLength))/2;


        iframeArea.find("#pip_iframe_preview_area #preview_foreground").attr("src", "images/pipview/sample_progress_1min_preview_w_s.png");

        iframeArea.find("#pip_iframe_preview_area #preview_time").text(time);
        iframeArea.find("#pip_iframe_preview_area #preview_unit").text(unit);
        iframeArea.find("#pip_iframe_preview_area #preview_time").css({left:startLeft,width:timeTextLength});
        iframeArea.find("#pip_iframe_preview_area #preview_time").css({ color: "rgba(255, 96, 108, 1)"});

        startLeft+=timeTextLength;
        startLeft+=4;
        iframeArea.find("#pip_iframe_preview_area #preview_unit").css({left:startLeft,width:timeUnitTextLength});
        iframeArea.find("#pip_iframe_preview_area").css({visibility: "inherit"});

        if (remainTime >0) {
            previewtenSecTimer = setTimeout(checkPreViewTime, 1000);
        }else {
            KTW.managers.service.ChannelPreviewManager.stop(false,true);
            iframeArea.find("#pip_iframe_preview_area").css({visibility: "hidden"});
        }
    }

    /**
     * OTS 신호미약 event listener
     */
    var isShowingSignalIframe = false;
    function _weakSignalListener(state) {
        log.printDbg('_weakSignalListener() state = ' + state);

        var iframeType =  BACK_IFRAME_TYPE.ETC;
        var iframeData = null;
        //CONNECTED
        if (state === 0) {
            if (isShowingSignalIframe === true) {
                isShowingSignalIframe = false;
                _showIframe({
                    type: iframeType,
                    data: iframeData
                });

                var layer = KTW.ui.LayerManager.getLayer("PipMultiViewPopup");
                if(layer !== undefined && layer !== null) {
                    log.printDbg('PipMultiViewPopup Search OK');
                    layer.child.showGuideView();
                }
            }
        }
        // DISCONNECTED
        else {
            isShowingSignalIframe = true;
            iframeType = BACK_IFRAME_TYPE.WEAK_SIGNAL;
            _showIframe({
                type: iframeType,
                data: iframeData
            });
        }
    }


    function _getCurrentChannelProgram(pipCh) {
        var programManager = KTW.oipf.adapters.ProgramSearchAdapter;
        var prog_index = 0;
        var prog_list = programManager.getPrograms(null, null, null, pipCh);
        if(prog_list !== undefined ) {
            prog_list = KTW.utils.epgUtil.revisePrograms(pipCh, prog_list, true);

            var now = new Date().getTime();
            if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                now = Math.floor(now/1000);
            }

            var prog = prog_list[prog_index];
            while(prog.startTime + prog.duration < now && prog_index < prog_list.length - 1) {
                prog_index++;
                prog = prog_list[prog_index];
                if (prog == null) {
                    prog_index = -1;
                    break;
                }
            }

            return prog_list[prog_index];
        }else {
            return null;
        }
    }


    function _changeChannel (pip_ch) {
        log.printDbg("changeChannel()  type : " + type + " , pip_ch : " + JSON.stringify(pip_ch));

        if (type === PIP_TYPE.MINI_EPG || type === PIP_TYPE.RESERVATION || type === PIP_TYPE.FULL_EPG || type === PIP_TYPE.FAV_EPG) {
            drawSubArea ({channel: pip_ch});

            var ch = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();

            log.printDbg("_changeChannel() KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel() = " + JSON.stringify(ch));


            if (pip_ch.ccid === ch.ccid) {
                if (KTW.managers.service.StateManager.isTVViewingState() === true) {
                    _clearPipTuneTimer();
                    pipChangeChannel = pip_ch;
                    setTimeout(function() {
                        iframeArea.find("#iframe_block_area").css({visibility: "inherit"});
                        _showIframe({type: BACK_IFRAME_TYPE.CURRENT});

                        if(pip_channel_control !== null) {
                            pip_channel_control.stopChannel();
                        }
                    } , 1);
                    _clearPreviewTimer();

                    if(isHdrVOD === true) {
                        _clearHdrBlockIframeTimer();
                        if(type !== PIP_TYPE.MULTI_VIEW) {
                            iframeArea.find("#hdr_iframe_img").attr("src", "images/iframe/dim_hdr2.png");
                        }else {
                            iframeArea.find("#hdr_iframe_img").attr("src", "images/iframe/block_hdr.png");
                        }
                        iframeArea.find("#hdr_iframe_img").css({visibility: "inherit"});
                        _setHdrBlockIframeTimer();
                        isHdrVOD = false;
                    }else {
                        iframeArea.find("#hdr_iframe_img").css({visibility: "hidden"});
                    }

                    return;
                }
            }
            else if (pipChangeChannel !== null && pipChangeChannel.ccid === ch.ccid) {
                setTimeout(function() {
                    iframeArea.find("#iframe_block_area").css({visibility: "inherit"});
                } , 1);
            }
        }

        pipChangeChannel = pip_ch;

        _setFastTuneTimer();

        if(isHdrVOD === true) {
            _clearHdrBlockIframeTimer();
            if(type !== PIP_TYPE.MULTI_VIEW) {
                iframeArea.find("#hdr_iframe_img").attr("src", "images/iframe/dim_hdr2.png");
            }else {
                iframeArea.find("#hdr_iframe_img").attr("src", "images/iframe/block_hdr.png");
            }
            iframeArea.find("#hdr_iframe_img").css({visibility: "inherit"});
            _setHdrBlockIframeTimer();
            isHdrVOD = false;
        }else {
            iframeArea.find("#hdr_iframe_img").css({visibility: "hidden"});
        }
    }

    function _setFastTuneTimer() {
        _clearPipTuneTimer();

        if(type !== PIP_TYPE.MULTI_VIEW) {
            pipTuneTimer = setTimeout(function() {
                iframeArea.find("#iframe_block_area").css({visibility: "inherit"});

                log.printDbg("_setFastTuneTimer() channel = " + JSON.stringify(pipChangeChannel));
                
                pip_channel_control.changeChannel(pipChangeChannel , undefined , true);
                pipTuneTimer = null;
                _clearPreviewTimer();
            } , DELAY_FAST_TUNE) ;

        }else {
            iframeArea.find("#iframe_block_area").css({visibility: "inherit"});
            pip_channel_control.changeChannel(pipChangeChannel , undefined , true);
            pipTuneTimer = null;
            _clearPreviewTimer();
        }

    }

    function _clearPipTuneTimer() {
        if (pipTuneTimer != null) {
            clearTimeout(pipTuneTimer);
            pipTuneTimer = null;
        }
    }

    function _clearPreviewTimer() {
        log.printDbg("_clearPreviewTimer()");
        if (previewtenSecTimer !== null) {
            clearTimeout(previewtenSecTimer);
            previewtenSecTimer = null;
        }
    }

    function _clearHdrBlockIframeTimer() {
        if(hdrBlockHideTimer !== null) {
            clearTimeout(hdrBlockHideTimer);
            hdrBlockHideTimer = null;
        }
    }

    function _setHdrBlockIframeTimer() {
        _clearHdrBlockIframeTimer();
        hdrBlockHideTimer = setTimeout(function() {
            iframeArea.find("#hdr_iframe_img").css({visibility: "hidden"});
            _clearHdrBlockIframeTimer();
        } , 5000) ;
    }

    KTW.ui.layer.PipLayer = function PipLayer(options) {
        KTW.ui.Layer.call(this, options);

        _this = this;

        this.init = function() {
            log.printDbg("init()");

            this.div = $("#" + this.id);

            if (!pip_channel_control) {
                pip_channel_control = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP, true);

                createElement();
            }
        };

        this._show = function (options) {
            log.printDbg("_show()");

            try {
                type = this.params.data.pipType;
                isHdrVOD = this.params.data.is_hdr_vod;
            }
            catch (e) {
                type = PIP_TYPE.NONE;
                log.printDbg(e);
            }

            log.printDbg("pip type: " + type);
            if (type === PIP_TYPE.NONE) {
                // TODO NONE 일때 어떻게 처리해야 할지 고민
                KTW.managers.service.PipManager.deactivatePipLayer();
                return;
            }

            // listener 등록 & 상태 변수 초기화
            pip_channel_control.addChannelEventListener(_pipChEventListener);

            // drawSubArea
            initUI();
            drawSubArea();

            // startPip
            startPip();
        };

        this._hide = function (options) {
            log.printDbg("_hide()");

            iframeArea.find("#iframe_block_area").css({visibility: "inherit"});

            _clearPipTuneTimer();
            _clearPreviewTimer();
            _clearHdrBlockIframeTimer();
            pip_channel_control.stop();
            pipChangeChannel = null;


            if(KTW.CONSTANT.IS_OTS === true && (type === PIP_TYPE.MULTI_VIEW || type === PIP_TYPE.RESERVATION || type === PIP_TYPE.FULL_EPG ||  type === PIP_TYPE.FAV_EPG) ) {
                networkManager.removeWeakSignalListener(_weakSignalListener);
            }

            type = PIP_TYPE.NONE;
            // listener 해제 & 상태 변수 초기화
            pip_channel_control.removeChannelEventListener(_pipChEventListener);


            iframeArea.css({visibility: "hidden"});
            iframeArea.children().css({visibility: "hidden"});
            miniEpgSubArea.css({visibility: "hidden"});
            multiViewSubArea.css({visibility: "hidden"});
            reservationSubArea.css({visibility: "hidden"});
            fullEpgSubArea.css({visibility: "hidden"});
        };

        this._remove = function () {
            log.printDbg("_remove()");
        };

        this.getType = function () {
            return type;
        };

        this.setPipSize = function (options) {
            if (!options.onlySetPipSize) {
                iframeArea.children().css({visibility: "hidden"});
                iframeArea.find("#iframe_block_area").css({visibility: "inherit"});
            }

            _setPipSize(options);
            setSubAreaSize(options);
        };

        this.changeChannel = function (pip_ch) {
            _changeChannel(pip_ch);
        };

        this.reDrawSubArea = function () {
            var pipChannel = pipChangeChannel;

            if (!pipChannel) {
                pipChannel = pip_channel_control.getCurrentChannel();
            }

            drawSubArea({channel: pipChannel});
        };


        this.clearPipTuneTimer = function () {
            _clearPipTuneTimer();
        };

        this.showIframe = function (options) {
            _showIframe(options);
        };


    };

    KTW.ui.layer.PipLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.PipLayer.prototype.constructor = KTW.ui.layer.PipLayer;

    //Override create function
    KTW.ui.layer.PipLayer.prototype.create = function(cbCreate) {
        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.PipLayer.prototype.show = function(options) {
        this._show(options);
    };

    KTW.ui.layer.PipLayer.prototype.hide = function(options) {
        this._hide(options);
    };

    KTW.ui.layer.PipLayer.prototype.remove = function() {
        this._remove();
    };

    KTW.ui.layer.PipLayer.prototype.handleKeyEvent = function(key_code) {
        return false;
    };
})();

