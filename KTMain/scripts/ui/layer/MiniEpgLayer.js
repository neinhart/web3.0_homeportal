/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>MiniEpgLayer</code>
 *
 * 채널 미니 가이드 화면 Layer
 * @author jjh1117
 * @since 2016-10-07
 */
"use strict";

(function() {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var epgUtil = KTW.utils.epgUtil;
    /**
     * MiniEpgLayer Layer
     */
    KTW.ui.layer.MiniEpgLayer = function MiniEpgLayer(options) {
        KTW.ui.Layer.call(this, options);

        var miniEpgMainView = null;
        var channelListView = null;
        var relatedContentView = null;
        var otsChannelProgramDetailView = null;

        var isInit = false;

        function createBackground() {
            var element = null;
            return element;
        }

        this.init = function() {
            this.div.attr({class: "arrange_frame"});
            this.div.append(createBackground());
        };

        this.showView = function(options) {
            log.printDbg("showView()");

            if(isInit === false) {
                isInit = true;
                miniEpgMainView = new KTW.ui.view.MiniEpgMainView(this);
                miniEpgMainView.create();
                channelListView = new KTW.ui.view.ChannelListView(this);
                channelListView.create();
                relatedContentView = new KTW.ui.view.RelatedContentView(this);
                relatedContentView.create();
                otsChannelProgramDetailView = new KTW.ui.view.OTSProgramDetailView(this);
                otsChannelProgramDetailView.create();
            }

            if(miniEpgMainView !== null) {
                return miniEpgMainView.show(options);
            }
        };

        // this.displayHide = function() {
        //     this.div.css({"display" : "none"});
        // };
        // this.displayShow = function() {
        //     this.div.css({"display" : ""});
        // };

        this.hideView = function(options) {
            miniEpgMainView.hide(options);
        };

        this.destroyView = function() {
            miniEpgMainView = null;
            channelListView = null;
        };
        
        this.controlKey = function(key_code) {
            var ret = false;

            ret = miniEpgMainView.controlKey(key_code);
            return ret;
        };
        this.getChannelListView = function() {
            return channelListView;
        };

        this.getMiniEpgMainView = function() {
            return miniEpgMainView;
        };

        this.getRelatedContentView = function() {
            return relatedContentView;
        };

        this.getOTSChannelProgramDetailView = function() {
            return otsChannelProgramDetailView;
        };

        this.getLastPermission = function() {
            log.printDbg("getLastPermission()");
            if(miniEpgMainView !== null) {
                return miniEpgMainView.getPermission();
            }
        };
    };

    KTW.ui.layer.MiniEpgLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.MiniEpgLayer.prototype.constructor = KTW.ui.layer.MiniEpgLayer;

    //Override create function
    KTW.ui.layer.MiniEpgLayer.prototype.create = function(cbCreate) {
        log.printDbg("create()");
        KTW.ui.Layer.prototype.create.call(this);
        this.init();
        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.MiniEpgLayer.prototype.show = function(options) {
        log.printDbg("show()");
        if(options !== undefined && options !== null) {
            log.printDbg("show() options : "+JSON.stringify(options));
        }

        var ret = this.showView(options);
        if(ret === true) {
            KTW.ui.Layer.prototype.show.call(this, options);
        }
    };

    KTW.ui.layer.MiniEpgLayer.prototype.hide = function(options) {
        log.printDbg("hide()");
        if(options !== undefined && options !== null) {
            log.printDbg("hide() options : "+JSON.stringify(options));
        }

        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.MiniEpgLayer.prototype.handleKeyEvent = function(key_code) {
        log.printDbg("handleKeyEvent()");
        return this.controlKey(key_code);
    };

})();

Object.defineProperty(KTW.ui.layer.MiniEpgLayer, "DEF", {
    value: {
        // 현재 채널/프로그램에 따른 미니가이드의 상태
        PROGRAM_STATE : {
            UNKNOWN : -1,
            NORMAL : 0,
            BLOCKED : 1,
            AGE_LIMIT : 2,
            NOT_SUBSCRIBED : 3,
            NOT_SUBSCRIBED_NORMAL : 4,
            PREVIEW : 5
        },

        // skyChoiceStyle 구성 시 보여줄 안내문구 종류
        PURCHASE_TYPE : {
            DEFAULT : 0,
            CONFIRM : 1,
            INPUT_PW : 2,
            SUCCESS : 3,
            LOADING_INFO : 4,
            TIME_OVER : 10,
            FAIL : 11,
        }

    },
    writable: false,
    configurable: false
});

