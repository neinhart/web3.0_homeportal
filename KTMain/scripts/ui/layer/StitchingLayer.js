/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>StitchingLayer.js</code>
 *
 * 실시간 인기 채널 UI를 제공한다.
 *
 * @author dhlee
 * @since 2016-08-01
 */

"use strict";

(function() {
    KTW.ui.layer.StitchingLayer = function StitchingLayer(options) {
        KTW.ui.Layer.call(this, options);

        var log = KTW.utils.Log;
        var stitchingView = null;

        this.init = function() {
            log.printDbg("init()");

            this.div.attr({class : "arrange_frame"});
            stitchingView = new KTW.ui.view.StitchingView(this);
            stitchingView.create();
        };

        this.showView = function() {
            log.printDbg("showView()");
            var vsData = _loadVSData();
            if (vsData != null) {
                stitchingView.show(vsData);
            }
        };

        this.hideView = function (options) {
            log.printDbg("hideView()");

            if (options && options.pause) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: this.id,
                    onlyTarget: true
                });
            }
            else {
                stitchingView.hide();
            }
        };

        this.destroyView = function() {
            log.printDbg("destroyView()");

            stitchingView = null;
        };

        this.controlKey = function(key_code) {
            return stitchingView.controlKey(key_code);
        };

        /**
         * 실시간 인기 채널 데이터를 OCManager로부터 가져온다.
         *
         * @private
         */
        function _loadVSData() {
            log.printDbg("_loadVSData()");

            /**
             * example
             *
             * 실시간 인기채널 1위-9위|dvb://5.1B9.1B9
             * 실시간 인기채널 10위-18위|dvb://5.1BB.1BB
             * 실시간 인기채널 19위-27위|dvb://5.1BE.1BE
             */
            var originalData = KTW.managers.data.OCManager.getVSData();
            log.printDbg("_loadVSData(), originalData = " + originalData);
            if (originalData != null && originalData != undefined) {
                var parsingData = originalData.split("\n");
                if (parsingData != null && parsingData.length > 0) {
                    var length = parsingData.length;
                    var temp = null;
                    var vsData = [];
                    for (var i = 0; i < length; i++) {
                        temp = parsingData[i].split("|");
                        if (temp && temp.length === 2) {
                            vsData[i] = {
                                title: temp[0],
                                locator: temp[1]
                            };
                            temp = null;
                        }else {
                            break;
                        }
                    }
                } else {
                    log.printDbg("_loadVSData(), parsingData is null or length <= 0, so return");
                    return null;
                }
            } else {
                log.printDbg("_loadVSData(), originalData is null, so return");
                return null;
            }

            log.printDbg("_loadVSData(), vsData = " + JSON.stringify(vsData));

            return vsData;
        }
    };

    KTW.ui.layer.StitchingLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.StitchingLayer.prototype.constructor = KTW.ui.Layer.StitchingLayer;

    KTW.ui.layer.StitchingLayer.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.StitchingLayer.prototype.show = function(options) {
        KTW.ui.Layer.prototype.show.call(this, options);
        this.showView();
    };

    KTW.ui.layer.StitchingLayer.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.StitchingLayer.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    }
})();
