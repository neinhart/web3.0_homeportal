/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelPreViewLayer</code>
 *
 * @author jjh1117
 * @since 2016-12-02
 */

"use strict";

(function() {
    var log = KTW.utils.Log;

    /**
     * MiniEpgLayer Layer
     */
    KTW.ui.layer.ChannelPreViewLayer = function ChannelPreViewLayer(options) {
        KTW.ui.Layer.call(this, options);

        var channelPreView = null;
        var log = KTW.utils.Log;

        this.init = function() {
            this.div.attr({class: "arrange_frame"});
            channelPreView = new KTW.ui.view.ChannelPreView(this);
            channelPreView.create();
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if(channelPreView !== null) {
                channelPreView.show(options);
            }

        };

        this.hideView = function(options) {
            channelPreView.hide(options);
        };

        this.destroyView = function() {
            channelPreView = null;
        };

        this.changeIframeMode = function(iframeMode) {
            log.printDbg("changeIframeMode() iframeMode : " + iframeMode);
            if(channelPreView !== null) {
                channelPreView.changeIframeMode(iframeMode);
            }
        };

        this.displayHide = function() {
            log.printDbg("displayHide()");
            if(channelPreView !== null) {
                this.div.css("display","none");
            }
        };

        this.controlKey = function(key_code) {
            var ret = false;

            if(channelPreView !== null) {
                ret = channelPreView.handleKeyEvent(key_code);
            }
            return ret;
        };

    };

    KTW.ui.layer.ChannelPreViewLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.ChannelPreViewLayer.prototype.constructor = KTW.ui.layer.ChannelPreViewLayer;

    //Override create function
    KTW.ui.layer.ChannelPreViewLayer.prototype.create = function(cbCreate) {
        log.printDbg("create()");
        KTW.ui.Layer.prototype.create.call(this);
        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.ChannelPreViewLayer.prototype.show = function(options) {
        log.printDbg("show()");
        if(options !== undefined && options !== null) {
            log.printDbg("show() options : "+JSON.stringify(options));
        }

        this.showView(options);
        KTW.ui.Layer.prototype.show.call(this, options);
    };

    KTW.ui.layer.ChannelPreViewLayer.prototype.hide = function(options) {
        log.printDbg("hide()");
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.ChannelPreViewLayer.prototype.handleKeyEvent = function(key_code) {
        log.printDbg("handleKeyEvent()");
        return this.controlKey(key_code);
    };

})();