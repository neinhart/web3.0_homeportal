/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>FullEpgView</code>
 *
 * 채널 가이드 화면 Layer
 * @author dj.son
 * @since 2016-08-08
 */

"use strict";

(function() {
    KTW.ui.layer.ChannelGuideLayer = function ChannelGuideLayer(options) {
        KTW.ui.Layer.call(this, options);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var guideMenu = null;

        var bgArea = null;
        var channelGuideMenuView = null;
        var epgContainerView = null;
        var focusView = null;
        
        var curBGMode = null;

        var vboTimer = null;
        var left  = -1;
        var top = -1;
        var width = -1;
        var height = -1;
        var DEFAULT_RESIZE_DELAY_TIME = 300;

        this.language = "kor";

        function createBackground () {
            bgArea = util.makeElement({
                tag: "<div />"
            });

            var bgListArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "background_list_area",
                    css: {
                        position: "absolute", overflow: "visible", visibility: "hidden"
                    }
                },
                parent: bgArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "background_menu",
                    src: "images/bg_2d_1_L.png",
                    class : "channel_guide_menu_background_area",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 576, height: 1080
                    }
                },
                parent: bgListArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    name : "background_epg_r1" ,
                    src: "images/bg_2d_1_R1.png",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 919, height: 1080
                    }
                },
                parent: bgListArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    name : "background_epg_r2" ,
                    src: "images/bg_2d_1_R2.png",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                    }
                },
                parent: bgListArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "background_epg_r3",
                    src: "images/bg_2d_1_R3.png",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                    }
                },
                parent: bgListArea
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "bg_menu_dim_up",
                    src: "images/bg_menu_dim_up.png",
                    css: {
                        position: "absolute",left: 0, top: 0, width: 1920, height: 280
                    }
                },
                parent: bgArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "bg_menu_dim_dw",
                    src: "images/bg_menu_dim_dw.png",
                    css: {
                        position: "absolute",left: 0, top: 912, width: 1920, height: 168
                    }
                },
                parent: bgArea
            });


            
            return bgArea;
        }

        this.init = function () {
            this.div.attr({class: "arrange_frame font_l"});

            // create background
            this.div.append(createBackground());

            // create ChannelGuideMenuView
            epgContainerView = new KTW.ui.view.EpgContainerView(this);
            epgContainerView.create();

            // create ChannelGuideMenuView
            channelGuideMenuView = new KTW.ui.view.ChannelGuideMenuView(this);
            channelGuideMenuView.create();
        };

        this._show = function(options) {

            this.language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();

            guideMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                        return true;
                    }
                }
            })[0];

            if (this.params && this.params.menuId) {
                // 채널 가이드 메뉴 가져오기
                var menuId = this.params.menuId;
                var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: guideMenu.children,
                    cbCondition: function (menu) {
                        if (menu.id === menuId) {
                            return true;
                        }
                    }
                })[0];

                if (epgMenu) {
                    options  = options || {};
                    options.jumpObj = {
                        targetMenu: epgMenu,
                        isLayerHide: true,
                        isHotKey: this.params.isHotKey
                    };
                }
            }

            this.params = null;


            this.showView(options);

            // [dj.son] jumpObj 가 있을 경우, 어떤 view 에 포커스를 줘야하는지에 대한 판단은 epgContainerView 에서 한다.
            if (!options || !options.resume) {
                if (options && options.jumpObj) {
                    channelGuideMenuView.jumpTargetMenu(options.jumpObj);
                }
                else {
                    this.setFocusView(channelGuideMenuView);
                }
            }
        };

        this._hide = function(options) {
            if (!options || !options.pause) {
                KTW.ui.LayerManager.hideVBOBackground();
                this.setFocusView(null);
            }

            this.hideView(options);
        };

        this.showView = function(options) {
            channelGuideMenuView.show(options);
            epgContainerView.show(options);
        };

        this.hideView = function(options) {
            channelGuideMenuView.hide(options);
            epgContainerView.hide(options);
        };

        this.destroyView = function() {
            channelGuideMenuView.destroy();
            epgContainerView.destroy();
        };

        this.controlKey = function(key_code) {
            var ret = false;

            switch (key_code) {
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.BACK:
                case KTW.KEY_CODE.CONTEXT:
                case KTW.KEY_CODE.FULLEPG :
                case KTW.KEY_CODE.FULLEPG_OTS :
                case KTW.KEY_CODE.EXIT:
                case KTW.KEY_CODE.RED:
                case KTW.KEY_CODE.BLUE:
                case KTW.KEY_CODE.PLAY:
                    ret = focusView.controlKey(key_code);
                    break;
            }

            /**
             * [dj.son] 홈샷 처리를 위해 red 키에 대해 예외처리 추가
             */
            if (key_code === KTW.KEY_CODE.RED && !ret && focusView !== epgContainerView) {
                ret = epgContainerView.controlKey(key_code);
            }

            return ret;
        };

        this.getChannelGuideMenuView = function () {
            return channelGuideMenuView;
        };

        this.getEpgContainerView = function () {
            return epgContainerView;
        };

        this.setFocusView = function (view) {
            log.printDbg("setFocusView()");

            if (focusView) {
                focusView.blur();
            }

            focusView = view;

            if (focusView) {
                focusView.focus();
            }
        };

        this.getGuideMenu  = function () {
            return guideMenu;
        };

        this.isJumpAvailable = function (menuId) {
            var result = true;
            if (menuId) {
                result = focusView.isJumpAvailable(menuId);
            }

            return result;
        };

        this.setBackground = function (bgMode) {
            bgArea.find("[name=background_list_area]").css({visibility: "inherit"});

            switch (bgMode) {
                case KTW.ui.layer.ChannelGuideLayer.BG_MODE.R1:
                    bgArea.find("[name=background_list_area] [name=background_epg_r1]").css({visibility: "inherit"});
                    bgArea.find("[name=background_list_area] [name=background_epg_r2]").css({visibility: "hidden"});
                    bgArea.find("[name=background_list_area] [name=background_epg_r3]").css({visibility: "hidden"});
                    break;
                case KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2:
                    bgArea.find("[name=background_list_area] [name=background_epg_r1]").css({visibility: "hidden"});
                    bgArea.find("[name=background_list_area] [name=background_epg_r2]").css({visibility: "inherit"});
                    bgArea.find("[name=background_list_area] [name=background_epg_r3]").css({visibility: "hidden"});
                    break;
                case KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3:
                    bgArea.find("[name=background_list_area] [name=background_epg_r1]").css({visibility: "hidden"});
                    bgArea.find("[name=background_list_area] [name=background_epg_r2]").css({visibility: "hidden"});
                    bgArea.find("[name=background_list_area] [name=background_epg_r3]").css({visibility: "inherit"});
                    break;
            }
        };

        this.checkSetVideoScreenPositonTimer = function() {
            if(vboTimer !== null) {
                if (vboTimer) {
                    clearTimeout(vboTimer);
                    vboTimer = null;

                    if(left !== -1 && top !== -1 && width !== -1 && height !== -1) {
                        KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(left, top, width, height);
                        KTW.managers.service.IframeManager.changeIframe();
                    }
                }
            }
        };

        this.setVideoScreenPosition = function (options) {
            log.printDbg("setVideoScreenPosition() curBGMode : " + curBGMode + " , options.mode : " + options.mode);
            
            if (curBGMode === options.mode && !options.forced) {
                return;
            }

            if (vboTimer) {
                clearTimeout(vboTimer);
                vboTimer = null;
                left = -1;
                top = -1;
                width = -1;
                height = -1;
            }

            var delayTime = options.delayTime ? delayTime : DEFAULT_RESIZE_DELAY_TIME;

            left = 0;
            top = 0;
            width = KTW.CONSTANT.RESOLUTION.WIDTH;
            height = KTW.CONSTANT.RESOLUTION.HEIGHT;

            var isResize = true;

            if (options.mode === KTW.ui.layer.ChannelGuideLayer.VBO_MODE.PIP_FULL_EPG) {
                left = KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT;
                top = KTW.CONSTANT.FULL_EPG_PIG_STYLE.TOP;
                width = KTW.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH;
                height = KTW.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT;
            }

            var _this = this;

            if (isResize) {
                KTW.managers.service.IframeManager.changeIframe(left, top, width, height);

                vboTimer = setTimeout(function () {
                    if (_this.isShowing()) {
                        KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(left, top, width, height);
                        KTW.managers.service.IframeManager.changeIframe();
                        left = -1;
                        top = -1;
                        width = -1;
                        height = -1;
                    }

                    vboTimer = null;
                }, delayTime);
            }

            curBGMode = options.mode;
        };
    };

    KTW.ui.layer.ChannelGuideLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.ChannelGuideLayer.prototype.constructor = KTW.ui.layer.ChannelGuideLayer;

    //Override create function
    KTW.ui.layer.ChannelGuideLayer.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init();

        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.ChannelGuideLayer.prototype.show = function(options) {
        KTW.ui.Layer.prototype.show.call(this, options);
        this._show(options);
    };

    KTW.ui.layer.ChannelGuideLayer.prototype.hide = function(options) {
        this._hide(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.ChannelGuideLayer.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };

    Object.defineProperty(KTW.ui.layer.ChannelGuideLayer, "BG_MODE", {
        value: {
            R1: "R1",
            R2: "R2",
            R3: "R3"
        },
        writable: false,
        configurable: false
    });

    Object.defineProperty(KTW.ui.layer.ChannelGuideLayer, "VBO_MODE", {
        value: {
            FULL_SCREEN: 0,
            PIP_FULL_EPG: 1
        },
        writable: false,
        configurable: false
    });

})();

