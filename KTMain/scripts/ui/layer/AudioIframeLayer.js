/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>IframeLayer</code>
 *
 * Iframe 화면 Layer
 * @author jjh1117
 * @since 2016-10-07
 */
"use strict";

(function() {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    /**
     * IframeLayer Layer
     */
    KTW.ui.layer.AudioIframeLayer = function IframeLayer(options) {
        KTW.ui.Layer.call(this, options);

        var audioIframeView = null;

        function createBackground() {
            var element = null;

            return element;
        }
        
        this.init = function() {
            this.div.attr({class: "arrange_frame"});
            this.div.append(createBackground());

            audioIframeView = new KTW.ui.view.AudioIframeView(this);
            audioIframeView.create();
        };

        this.showView = function(options) {
            if(audioIframeView !== null) {
                audioIframeView.show(options);
            }
        };
        
        this.hideView = function(options) {
            if(audioIframeView !== null) {
                audioIframeView.hide(options);
            }
        };

        this.destroyView = function() {
            audioIframeView = null;
        };
        
        this.controlKey = function(key_code) {
            var ret = false;
            return ret;
        };
        
        this.updateFocusAudioChannel = function() {
            audioIframeView.updateFocusAudioChannel();
        };
        
        this.showAudioEpgViewIcon = function() {
            audioIframeView.showAudioEpgViewIcon();
        };

        this.hideAudioEpgViewIcon = function() {
            audioIframeView.hideAudioEpgViewIcon();
        }

    };

    KTW.ui.layer.AudioIframeLayer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.AudioIframeLayer.prototype.constructor = KTW.ui.layer.IframeLayer;

    //Override create function
    KTW.ui.layer.AudioIframeLayer.prototype.create = function(cbCreate) {
        log.printDbg("create()");
        KTW.ui.Layer.prototype.create.call(this);
        this.init();
        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.AudioIframeLayer.prototype.show = function(options) {
        log.printDbg("show()");
        this.showView(options);
        KTW.ui.Layer.prototype.show.call(this, options);
    };

    KTW.ui.layer.AudioIframeLayer.prototype.hide = function(options) {
        log.printDbg("hide()");
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.AudioIframeLayer.prototype.handleKeyEvent = function(key_code) {
        log.printDbg("handleKeyEvent()");
        return false;
//        return this.controlKey(key_code);
    };

})();

