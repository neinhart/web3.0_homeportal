/**
 * Created by Yun on 2016-11-23.
 * 설정 > 자녀안심 설정 > 시청채널 제한
 */

(function () {

    KTW.ui.layer.setting.setting_limitChannel  = function LimitChannel(options) {
        KTW.ui.Layer.call(this, options);


        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        //var storageManager = KTW.managers.StorageManager;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var MAX_OTV_IPTV_COUNT = 120;
        var MAX_OTS_SKYLIFE_COUNT = 90;
        var MAX_OTS_IPTV_COUNT = 30;

        var INSTANCE = this;


        var styleSheet;

        var selectData_skylife = [];
        var selectData_otv = [];
        //var indicator;
        var totalPage = 0;
        var curPage = 0;
        var isSelectList = false;

        var isFocusSkyChannelRing = false;

        var iptvBlockList = null;
        var skyBlockList = null;

        var iptvChannelList = null;
        var skyChannelList = null;

        var iptvBlockedChannelCount = 0;
        var skylifeBlockedChannelCount = 0;

        var selectedIPTVChannelCount = 0;
        var selectedSkylifeChannelCount = 0;

        var selectedChIndexList = []; // 선택된 채널의 실제 채널에서의 인덱스를 저장, 선택된 채널 화면에서 저장 시 필요하다.
        var selectChannelList = [];
        var selectChannelListCurrentPage = 0;
        var selectChannelListTotalPage = 0;

        var menuType = 0;

        var focusBlockList = null;
        var focusChannelList = null;

        var focusPageItemCount = 0;
        var focusRowIndex = 0;
        var focusXIdx = 0;
        var focusRightMenuIdx = 0;

        var selectedPageItemCount = 0;

        var lastRowIndex = 0;
        var lastFocusXidx = 0;

        var channelCellArea = [];

        //scrollArea
        var scroll = null;
        var scrollArea = null;

        var isScrollArea = false;

        var clock;
        var clockArea;

        var data;

        var colorKeyArea;

        var hasSelectedChannel = false;


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setting_layer limitChannel"});


            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>시청채널 제한</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            if (KTW.CONSTANT.IS_OTS) {
                createComponent_OTS();
            } else {
                createComponent_OTV();
            }
            createScrollArea();
            createColorKeyArea();
        //    createClock();
            cbCreate(true);
        };

        this.show = function (options) {
            log.printDbg("show() ");

            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            selectData_skylife = [];
            selectData_otv = [];

            _getChannelList();

            if(KTW.CONSTANT.IS_OTS === true) {
                isFocusSkyChannelRing = true;
                focusBlockList = skyBlockList;
                focusChannelList = skyChannelList;
            }else {
                isFocusSkyChannelRing = false;
                focusBlockList = iptvBlockList;
                focusChannelList = iptvChannelList;
            }
            menuType = 0;
            curPage = 0;
            totalPage = Math.ceil(focusChannelList.length/20);
            /*
             if(totalPage <=1) {
             indicator.getView().css("display" , "none");
             }else {
             indicator.getView().css("display" , "");
             indicator.setSize(totalPage, curPage);
             }*/
            log.printDbg("show() curPage : " + curPage + " , totalPage : " + totalPage );

            if(totalPage > 2) {
                setColorKeyArea(true);
            }else {
                setColorKeyArea(false);
            }

            scroll.show({
                maxPage: totalPage,
                curPage: curPage
            });

            if(isScrollArea){
                setScrollFocus(false);
            }



            _refreshChannelDataList();
            channelDataListChangePage(curPage);

            focusRowIndex = 0;
            focusXIdx = 0;

            if(focusPageItemCount>0) {
                _buttonFocusRefresh((focusRowIndex*2)+focusXIdx, menuType);
            }

            if(KTW.CONSTANT.IS_OTS === true) {
                if(isFocusSkyChannelRing === false) {
                    INSTANCE.div.find("#rightMenu_btn_area_0_ots .rightMenu_btn_div:eq(2) .rightMenu_btn_text").text("SkyLife 채널 목록");
                }else {
                    INSTANCE.div.find("#rightMenu_btn_area_0_ots .rightMenu_btn_div:eq(2) .rightMenu_btn_text").text("olleh tv 채널 목록");
                }
            }
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BLOCKED_CHANNEL) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
/*            KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(
                KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT, KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP,
                KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH, KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            KTW.managers.service.IframeManager.changeIframe();*/

        //    clock.show(clockArea);
        };
        this.hideView = function (options) {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

         //   clock.hide();

            if(options === undefined) {
                KTW.managers.auth.AuthManager.initPW();
                KTW.managers.auth.AuthManager .resetPWCheckCount();
            }
        };

        this.controlKey = function (key_code) {
            if (key_code == KEY_CODE.BACK) {

                if(isSelectList) {
                    if(isScrollArea) {
                        setScrollFocus(false);
                    }
                    _restoreChannelList();

                } else {
                    LayerManager.historyBack();
                }
                return true;
            }
            return keyAction(key_code);
        };

        /**
         * OTS 시청제한 화면 구성
         */
        function createComponent_OTS() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append($("<div/>", {id:"selectMenu_title"}).text("SkyLife 채널"));
            INSTANCE.div.find("#contents").append($("<div/>", {id:"noSelectOTVChannel_area"}));
            INSTANCE.div.find("#noSelectOTVChannel_area").append($("<img/>", {id:"noSelectOTVChannel_icon_img", src: "images/icon/icon_noresult.png"}));
            INSTANCE.div.find("#noSelectOTVChannel_area").append($("<div/>", {id:"noSelectOTVChannel_text"}).text("시청채널 제한으로 선택된 채널이 없습니다"));
            INSTANCE.div.find("#contents").append("<div id='settingTable'></div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text1'>현재 olleh 채널 0개,<br>SkyLife 채널 0개<br>선택되었습니다<br>[최대 20개]</div>" +
            "<div id='rightMenu_btn_area_0_ots'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>설정 초기화</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>선택한 채널 확인</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>olleh tv 채널 목록</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div id='rightMenu_btnLine'></div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "<div id='rightMenu_btn_area_1' class='hide'>" +
                "<div class='rightMenu_btn_div'>" +
                    "<div class='rightMenu_btn_text'>저장</div>" +
                    "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                    "<div class='rightMenu_btn_text'>취소</div>" +
                    "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
            "</div>" +
            "</div>");

            INSTANCE.div.find("#contents").append($("<div/>", {id: "settingTableFocus_div"}));
            addTableCellObject();
        }

        /**
         * OTV 시청제한 화면 구성
         */
        function createComponent_OTV() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append($("<div/>", {id:"selectMenu_title"}).text(""));
            INSTANCE.div.find("#contents").append($("<div/>", {id:"noSelectOTVChannel_area"}));
            INSTANCE.div.find("#noSelectOTVChannel_area").append($("<img/>", {id:"noSelectOTVChannel_icon_img", src: "images/icon/icon_noresult.png"}));
            INSTANCE.div.find("#noSelectOTVChannel_area").append($("<div/>", {id:"noSelectOTVChannel_text"}).text("선택 가능한 olleh tv 채널이 없습니다"));
            INSTANCE.div.find("#contents").append("<div id='settingTable'></div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text1'>시청채널 제한으로 설정된<br>채널은 성인인증 비밀번호를<br>입력해야 이용 가능합니다</div>" +
            "<div id='rightMenu_line'></div>" +
            "<div id='rightMenu_text2'>현재 0개 채널이<br>선택되었습니다<br>[최대 30개]</div>" +
            "<div id='rightMenu_btn_area_0'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>설정 초기화</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>선택한 채널 확인</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div id='rightMenu_btnLine'></div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
                "<div id='rightMenu_btn_area_1' class='hide'>" +
                    "<div class='rightMenu_btn_div'>" +
                        "<div class='rightMenu_btn_text'>저장</div>" +
                        "<div class='rightMenu_btn_line'></div>" +
                    "</div>" +
                    "<div class='rightMenu_btn_div'>" +
                        "<div class='rightMenu_btn_text'>취소</div>" +
                        "<div class='rightMenu_btn_line'></div>" +
                    "</div>" +
                "</div>" +
            "</div>");

            INSTANCE.div.find("#contents").append($("<div/>", {id: "settingTableFocus_div"}));
            addTableCellObject();
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }
        function createColorKeyArea() {
            log.printDbg("createColorKeyArea()");

            colorKeyArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "colorKey",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 1920, height: 1080, visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: 1343, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: 1371, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "keyText",
                    css: {
                        position: "absolute", left: 1343, top: 939, width: 109, height: 26, "letter-spacing" : -1.1,
                        "font-size" : 22, color: "rgba(255,255,255,0.7)", "font-family" : "RixHead L"
                    }
                },
                text: "페이지",
                parent: colorKeyArea
            });

        }
        function setColorKeyArea(b) {
            log.printDbg("setColorKeyArea");
            if(b) {
                colorKeyArea.css({visibility: "inherit"});
            }else {
                colorKeyArea.css({visibility: "hidden"});
            }
        }

        function createScrollArea() {
            log.printDbg("createScrollArea()");

            //scrollArea
            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "scroll",
                    css: {
                        position: "absolute", left: 1369, top: 135, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 428
            });

        }
        function setScrollFocus(b){
            log.printDbg("setFocus()");

            if(b) {
               INSTANCE.div.find("#settingTable .channelCell_div").removeClass("focus");
                scroll.setFocus(true);
                isScrollArea = true;
            }
            else{
                scroll.setFocus(false);
                isScrollArea = false;
            }
        }

        function addTableCellObject() {
            var cellDiv;

            for (var idx = 0; idx< 10; idx ++) {
                channelCellArea[idx] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "channelCell_area",
                        css: {
                            position: "absolute", top: 83*idx, width: 1168, height: 83
                        }
                    },
                    parent : INSTANCE.div.find("#contents #settingTable")
                });
                var channelCellImgDiv;
                for(var i = 0; i < 2; i++) {
                    cellDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "channelCell_div",
                            css: {
                                position: "absolute", left: 601*i, top: 0, width: 567, height: 81
                            }
                        },
                        parent : channelCellArea[idx]
                    });
                    channelCellImgDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "cellCheck_img",
                            css: {
                                /* position: "absolute",*/ "margin-top": 18, width: 48, height: 40, float: "left"
                            }
                        },
                        parent : cellDiv
                    });
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            class: "cellCheck_img_uncheck",
                            src: "images/pop_cb_dim_uncheck.png",
                            css: {
                                position: "absolute", /*width: 48, height: 40, */float: "left"
                            }
                        },
                        parent : channelCellImgDiv
                    });
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            class: "cellCheck_img_check",
                            src: "images/pop_cb_dim_check.png",
                            css: {
                                position: "absolute", /*width: 48, height: 40, */float: "left"
                            }
                        },
                        parent : channelCellImgDiv
                    });
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            class: "cellCheck_img_check_red",
                            src: "images/pop_cb_dim_check_red.png",
                            css: {
                                position: "absolute", /*width: 48, height: 40, */float: "left"
                            }
                        },
                        parent : channelCellImgDiv
                    });

                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            class: "channelIcon_img",
                            css: {
                                "margin-top": 25,/* width: 48, height: 40,*/ float: "left"
                            }
                        },
                        parent : cellDiv
                    });
                    util.makeElement({
                        tag: "<span />",
                        attrs: {
                            class: "cellChannel_number",
                            css: {
                                "margin-left": 12, height: 40, float: "left"
                            }
                        },
                        parent : cellDiv
                    });
                    util.makeElement({
                        tag: "<span />",
                        attrs: {
                            class: "cellChannel_name",
                            css: {
                                "margin-left": 26, height: 40, float: "left", "white-space": "nowrap"
                            }
                        },
                        parent : cellDiv
                    });
                }
            };
        }

        function _getChannelList() {
            log.printDbg("_getChannelList()");
            if (KTW.CONSTANT.IS_OTS) {
                skyChannelList = KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);
                if(skyChannelList !== undefined && skyChannelList !== null && skyChannelList.length>0) {
                    log.printDbg("_getChannelList() skyChannelList Length : " + skyChannelList.length);
                }
            }
            iptvChannelList = KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_VIDEO);
            if(iptvChannelList !== undefined && iptvChannelList !== null && iptvChannelList.length>0) {
                log.printDbg("_getChannelList() iptvChannelList Length : " + iptvChannelList.length);
            }
            _getBlockedChannelList();
        }

        function _getBlockedChannelList() {
            log.printDbg("_getBlockedChannelList()");
            // [jh.lee] 제한된 olleh 채널 리스트 로드
            var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BLOCKED_CHANNEL);
            if(KTW.CONSTANT.IS_OTS === true) {
                if(settingData !== undefined && settingData !== null && settingData.length === 2) {
                    iptvBlockList = settingData[0];
                    skyBlockList = settingData[1];
                }
            }else {
                if(settingData !== undefined && settingData !== null && settingData.length === 1) {
                    iptvBlockList = settingData[0];
                }
            }

            if(iptvBlockList !== undefined && iptvBlockList !== null && iptvBlockList.length>0) {
                log.printDbg("_getBlockedChannelList() iptvBlockList Length : " + iptvBlockList.length);
            }
            if (util.isValidVariable(iptvBlockList) === true) {
                iptvBlockedChannelCount = iptvBlockList.length;

                for(var i=0;i<iptvBlockList.length;i++) {
                    for (var j = 0; j < iptvChannelList.length; j++) {
                        if (iptvChannelList[j].ccid === iptvBlockList[i].ccid) {
                            selectData_otv[j] = true;
                        }
                    }
                }
            }

            if (KTW.CONSTANT.IS_OTS) {
                if(skyBlockList !== undefined && skyBlockList !== null && skyBlockList.length>0) {
                    log.printDbg("_getBlockedChannelList() skyBlockList Length : " + skyBlockList.length);
                }
                if (util.isValidVariable(skyBlockList) === true) {
                    skylifeBlockedChannelCount = skyBlockList.length;

                    for(var i=0;i<skyBlockList.length;i++) {
                        for (var j = 0; j < skyChannelList.length; j++) {
                            if (skyChannelList[j].ccid === skyBlockList[i].ccid) {
                                selectData_skylife[j] = true;
                            }
                        }
                    }
                }
            }
        }

        /**
         * 채널리스트 페이지 변경시 사용되는 함수.
         * @param page
         */
        function channelDataListChangePage(page) {
            log.printDbg("channelDataListChangePage() isFocusSkyChannelRing : " + isFocusSkyChannelRing);
            log.printDbg("channelDataListChangePage() page : " + page);

            curPage = page;
            /**
             * 화면 초기화 시킴
             */
            INSTANCE.div.find("#settingTable .channelCell_div").removeClass("select");
            for (var idx = 0 ; idx < 20 ;idx ++) {
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display" , "none");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").css("display" , "none");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").css("display" , "none");

                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("margin-left", "0px");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("margin-right", "0px");
            }

            // 변경될 페이지에 표현된 최대 element 개수를 확인
            focusPageItemCount = 0;
            if(((curPage*20) + 20)<=focusChannelList.length) {
                focusPageItemCount = 20;
            }else {
                focusPageItemCount = (focusChannelList.length - (curPage*20));
            }

            log.printDbg("channelDataListChangePage() page : " + page + " , itemCount : " + focusPageItemCount + " , totalItemCount : " + focusChannelList.length);

            // 채널이 아무것도 없는 경우 보여지는 화면
            if(focusPageItemCount<=0) {
                INSTANCE.div.find("#settingTable").addClass("hide");
                INSTANCE.div.find("#noSelectOTVChannel_area").addClass("show");
                if(isFocusSkyChannelRing === true) {
                    INSTANCE.div.find("#noSelectOTVChannel_area #noSelectOTVChannel_text").text("선택 가능한 SkyLife 채널이 없습니다");
                }else {
                    INSTANCE.div.find("#noSelectOTVChannel_area #noSelectOTVChannel_text").text("선택 가능한 olleh tv 채널이 없습니다");
                }

                menuType = 1;
                if(KTW.CONSTANT.IS_OTS===true) {
                    focusRightMenuIdx = 3;
                }else {
                    focusRightMenuIdx = 2;
                }
                _buttonFocusRefresh(focusRightMenuIdx, menuType);

            }else {
                INSTANCE.div.find("#settingTable").removeClass("hide");
                INSTANCE.div.find("#noSelectOTVChannel_area").removeClass("show");

                for (var idx = 0; idx < focusPageItemCount; idx++) {
                    if(focusChannelList[(curPage*20) + idx]) {
                        //INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").attr("src", "images/pop_cb_dim_uncheck.png");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").text(util.numToStr(focusChannelList[(curPage*20) + idx].majorChannel, 3));
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").text(focusChannelList[(curPage*20) + idx].name);
                    }
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display" , "");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").css("display" , "");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").css("display" , "");


                    if(isFocusSkyChannelRing === false) {
                        if(selectData_otv[(curPage*20) + idx] !== undefined && selectData_otv[(curPage*20) + idx] !== null && selectData_otv[(curPage*20) + idx] === true) {

                           // INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").attr("src", "images/pop_cb_dim_check.png");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ")").addClass("select");
                        }
                    }else {
                        if(selectData_skylife[(curPage*20) + idx] !== undefined && selectData_skylife[(curPage*20) + idx] !== null && selectData_skylife[(curPage*20) + idx] === true) {
                            //INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").attr("src", "images/pop_cb_dim_check.png");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ")").addClass("select");
                        }
                    }
                }

                window.selectData_otv = selectData_otv;
                window.selectData_skylife = selectData_skylife;
            }

            if(KTW.CONSTANT.IS_OTS) {
                if(isFocusSkyChannelRing) {
                    INSTANCE.div.find("#rightMenu_text1").html("현재 olleh 채널 " + iptvBlockedChannelCount + "개,<br>SkyLife 채널 " + skylifeBlockedChannelCount + "개<br>선택되었습니다<br>[최대 "+MAX_OTS_SKYLIFE_COUNT +"개]");

                }else {
                    INSTANCE.div.find("#rightMenu_text1").html("현재 olleh 채널 " + iptvBlockedChannelCount + "개,<br>SkyLife 채널 " + skylifeBlockedChannelCount + "개<br>선택되었습니다<br>[최대 "+MAX_OTS_IPTV_COUNT +"개]");
                }
            }else {
                INSTANCE.div.find("#rightMenu_text2").html("현재 " + iptvBlockedChannelCount + "개 채널이<br>선택되었습니다<br>[최대 " + MAX_OTV_IPTV_COUNT + "개]");
            }

            // indicator.setPos(curPage);
            scroll.setCurrentPage(curPage);
        }

        function _refreshChannelDataList() {
            log.printDbg("_refreshChannelDataList() isFocusSkyChannelRing : " + isFocusSkyChannelRing);

            INSTANCE.div.find("#noSelectOTVChannel_area").css({visibility: "hidden"});
            INSTANCE.div.find("#settingTable").css({visibility: "inherit"});

            INSTANCE.div.find("#settingTable .channelIcon_img").addClass("hide");
            INSTANCE.div.find("#settingTable .cellCheck_img").removeClass("hide");
            INSTANCE.div.find("#settingTable .channelCell_div").removeClass("selectList");

            if(KTW.CONSTANT.IS_OTS === true) {
                INSTANCE.div.find("#rightMenu_btn_area_0_ots").removeClass("hide");
            }else {
                INSTANCE.div.find("#rightMenu_btn_area_0").removeClass("hide");
            }
            INSTANCE.div.find("#rightMenu_btn_area_1").addClass("hide");

            INSTANCE.div.find("#selectMenu_title").text("");
            if(KTW.CONSTANT.IS_OTS) {
                if(isFocusSkyChannelRing === true) {
                    INSTANCE.div.find("#selectMenu_title").text("SkyLife 채널");
                }else {
                    INSTANCE.div.find("#selectMenu_title").text("olleh tv 채널");
                }
            }
            isSelectList = false;
            // "모두 선택" 영역 원상태로 복구
            INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_number").css("display" , "");
            INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_name").css({"margin-left": 26 });
        }


        function _resetSelectChannelData() {
            KTW.ui.adaptor.SettingDataAdaptor.showInitConfirmPopup("시청채널 제한 초기화" , _callbackInitConfirmPopup);
            // selectData_skylife = [];
            // selectData_otv = [];
            // iptvBlockedChannelCount = 0;
            // skylifeBlockedChannelCount = 0;
            // channelDataListChangePage(curPage);
            // if(KTW.CONSTANT.IS_OTS) {
            //     focusRightMenuIdx = 3;
            // } else {
            //     focusRightMenuIdx = 2;
            // }
            // _buttonFocusRefresh(focusRightMenuIdx, menuType);
            // KTW.managers.service.SimpleMessageManager.showMessageTextOnly("선택된 채널목록이 초기화되었습니다");
        }

        function _callbackInitConfirmPopup(buttonId) {
            if(buttonId === "save") {
                selectData_skylife = [];
                selectData_otv = [];
                iptvBlockedChannelCount = 0;
                skylifeBlockedChannelCount = 0;
                //KTW.managers.service.SimpleMessageManager.showMessageTextOnly("선택된 채널목록이 초기화되었습니다");
                _saveChannel();
                data.complete();
                settingMenuManager.historyBackAndPopup(null, "선택된 채널목록이 초기화되었습니다");
            }
        }

        /**
         * 선택된 채널 혹인하는곳으로 보임
         * @param key_code
         * @returns {boolean}
         */
        function keyAction(key_code) {
            var isFocusMove = false;
            var isPageMove = false;
            if(isSelectList === true) {
                if(key_code === KEY_CODE.ENTER) {
                    if(menuType === 0){
                        _selectedListEnterEventHandler(((focusRowIndex*2) + focusXIdx), menuType);
                    }else {
                        _selectedListEnterEventHandler(focusRightMenuIdx, menuType);
                    }
                    return true;
                }else if(key_code === KEY_CODE.LEFT) {
                    if(menuType == 0) {
                        if(focusXIdx === 0) {
                            _restoreChannelList();
                        }else if(focusXIdx === 1) {
                            focusXIdx--;
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }
                    }else {

                        if(selectChannelList.length-1 < 1) {
                            _restoreChannelList();
                            return true;
                        }
                        if(selectedPageItemCount > 0) {
                            menuType = 0;
                           // setColorKeyArea(true);
                            _selectListButtonFocusRefresh(((focusRowIndex*2) + focusXIdx), menuType);
                        }
                    }
                    return true;
                }
                else if(key_code === KEY_CODE.RIGHT) {
                    if(menuType == 0) {
                        if(focusXIdx === 0) {
                            focusXIdx++;
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }else if(focusXIdx === 1) {
                            menuType = 1;
                            focusRightMenuIdx = 0;
                           // setColorKeyArea(true);
                            _selectListButtonFocusRefresh(focusRightMenuIdx, menuType);
                        }
                    }
                    return true;
                }
                else if(key_code === KEY_CODE.UP) {
                    if(menuType === 1) {
                        if(!hasSelectedChannel) {
                            // 선택된 채널이 없는 경우 상하키 동작 없음 (버튼이 한개이므로)
                            return true;
                        }
                        focusRightMenuIdx = KTW.utils.util.getIndex(focusRightMenuIdx, -1, 2);
                        _selectListButtonFocusRefresh(focusRightMenuIdx,menuType);
                    }else {
                        focusRowIndex --;
                        if(focusRowIndex < 0) {
                            isPageMove = true;
                        }else {
                            isFocusMove = true;
                        }
                        if(isFocusMove === true) {
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                           // return true;
                        }else if(isPageMove === true) {
                            if(selectChannelListTotalPage > 1) {
                                selectChannelListCurrentPage--;
                                if(selectChannelListCurrentPage < 0) {
                                    selectChannelListCurrentPage = selectChannelListTotalPage -1;
                                }
                                _selectChannelDataListChangePage();
                            }
                            // Rowindex 수정
                            focusRowIndex = Math.floor(selectedPageItemCount /2);
                            if(selectedPageItemCount % 2 !== 0) {
                                focusRowIndex++;
                            }
                            focusRowIndex -=1;
                            if (((focusRowIndex * 2) + (focusXIdx)) < selectedPageItemCount) {
                            } else {
                                focusXIdx = 0;
                            }
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }
                    }
                    return true;
                }
                else if(key_code === KEY_CODE.DOWN) {
                    if(menuType === 1) {
                        if(!hasSelectedChannel) {
                            // 선택된 채널이 없는 경우 상하키 동작 없음 (버튼이 한개이므로)
                            return true;
                        }
                        focusRightMenuIdx = KTW.utils.util.getIndex(focusRightMenuIdx, 1, 2);
                        _selectListButtonFocusRefresh(focusRightMenuIdx,menuType);
                    }else {
                        if(selectedPageItemCount === 20) {
                            focusRowIndex++;
                            if(focusRowIndex > 9) {
                                isPageMove = true;
                                focusRowIndex = 0;
                            }else {
                                isFocusMove  = true;
                            }
                        }else {
                            focusRowIndex ++;
                            var tempRowCount = Math.floor(selectedPageItemCount / 2);
                            if (selectedPageItemCount % 2 !==0) {
                                tempRowCount++;
                            }
                            if (tempRowCount < (focusRowIndex + 1)) {
                                isPageMove = true;
                                focusRowIndex = 0;
                            } else {
                                isFocusMove = true;
                                if (((focusRowIndex * 2) + (focusXIdx)) < focusPageItemCount) {
                                } else {
                                    focusXIdx = 0;
                                }
                            }
                        }
                        if(isFocusMove === true) {
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        } else if(isPageMove === true) {
                            if(selectChannelListTotalPage > 1) {
                                selectChannelListCurrentPage++;
                                if(selectChannelListTotalPage <=selectChannelListCurrentPage){
                                    selectChannelListCurrentPage = 0;
                                }
                                _selectChannelDataListChangePage();
                            }
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }
                    }
                    return true;
                }
                else if(key_code === KEY_CODE.RED) {
                    if(menuType == 1) {
                        // 버튼 영역에 있으면 동작 안함
                        return true;
                    }else {
                        if(focusXIdx == 0 && focusRowIndex == 0) {
                            //이미 가장 첫번째 포커스라면 페이지를 이동 시킴
                            selectChannelListCurrentPage--;
                            if(selectChannelListCurrentPage < 0) {
                                // 첫번째 페이지였다면
                                selectChannelListCurrentPage = selectChannelListTotalPage -1;
                            }
                            _selectChannelDataListChangePage();
                        }else {
                            //현재 화면의 가장 첫번쨰 인덱스로 포커스 이동
                            focusXIdx = 0;
                            focusRowIndex = 0;
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }
                    }
                    return true;
                }
                else if(key_code === KEY_CODE.BLUE) {
                    if(menuType == 1) {
                        return true;
                    }else {
                        // 현재 페이지가 마지막 페이지 인지 확인
                        if(selectChannelListCurrentPage == selectChannelListTotalPage -1) {
                            // 현재 페이지가 마지막 페이지라면 포커스 끝처리 적용
                            if( (focusRowIndex *2) + focusXIdx == selectedPageItemCount -1 ) {
                                // 이미 마지막 인덱스에 포커스가 가 있는 경우 첫번째 페이지로 전환
                                selectChannelListCurrentPage = 0;
                                if(selectChannelListTotalPage < 2) {
                                    // 1페이지만 존재하는 경우
                                    return true;
                                }
                                _selectChannelDataListChangePage();
                                focusRowIndex = 9;
                                focusXIdx = 1;
                            }else {
                                // 마지막 포커스로 이동
                                focusRowIndex = Math.floor(selectedPageItemCount / 2) -1;
                                if(selectedPageItemCount %2 !==0) {
                                    focusRowIndex++;
                                    focusXIdx = 0;
                                }else {
                                    focusXIdx = 1;
                                }
                            }
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }else {
                            //현재 페이지가 마지막 페이지가 아닌 경우
                            if(focusRowIndex == 9 && focusXIdx == 1) {
                                // 마지막 인덱스에 위치해있는지 확인
                                // 페이지 이동
                                selectChannelListCurrentPage++;
                                _selectChannelDataListChangePage();
                                if(selectChannelListCurrentPage == selectChannelListTotalPage -1) {
                                    //이동한 페이지가 마지막 페이지인지 확인
                                    //마지막 포커스로 이동
                                    focusRowIndex = Math.floor(selectedPageItemCount / 2) -1;
                                    if(focusPageItemCount %2 !==0) {
                                        focusRowIndex++;
                                        focusXIdx = 0;
                                    }else {
                                        focusXIdx = 1;
                                    }
                                }
                            }else {
                                focusRowIndex = 9;
                                focusXIdx = 1;
                            }
                            _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);

                            return true;
                        }
                    }
                }else {
                    return false;
                }
                return true;
            }

            switch (key_code) {
                case KEY_CODE.UP:
                    if(menuType === 1) {
                        if(KTW.CONSTANT.IS_OTS) {
                            focusRightMenuIdx = KTW.utils.util.getIndex(focusRightMenuIdx, -1, 5);
                        }else {
                            focusRightMenuIdx = KTW.utils.util.getIndex(focusRightMenuIdx, -1, 4);
                        }
                        _buttonFocusRefresh(focusRightMenuIdx, menuType);
                    }else {
                        if (isScrollArea) {
                            focusRowIndex = focusRowIndex - (10 + (focusRowIndex % 10));
                            isPageMove = true;
                        } else {
                            focusRowIndex--;
                            if (focusRowIndex < 0) {
                                isPageMove = true;
                            } else {
                                isFocusMove = true;
                            }
                        }
                        if (isFocusMove === true) {
                            _buttonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        } else if (isPageMove === true) {
                            curPage--;
                            if (curPage >= 0) {
                                if(isScrollArea) {
                                    focusRowIndex =0;
                                } else {
                                    focusRowIndex = 9;
                                }
                                channelDataListChangePage(curPage);
                                if(!isScrollArea){
                                    _buttonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                                }
                            } else {
                                curPage = totalPage - 1;
                                channelDataListChangePage(curPage);

                                focusRowIndex = Math.floor(focusPageItemCount / 2);
                                if (focusPageItemCount % 2 !== 0) {
                                    focusRowIndex++;
                                }
                                focusRowIndex -= 1;

                                if (((focusRowIndex * 2) + (focusXIdx)) < focusPageItemCount) {
                                } else {
                                    focusXIdx = 0;
                                }

                                if(!isScrollArea) {
                                    _buttonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                                }
                            }
                        }
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if(menuType === 1) {
                        if(KTW.CONSTANT.IS_OTS) {
                            focusRightMenuIdx = KTW.utils.util.getIndex(focusRightMenuIdx, 1, 5);
                        }else {
                            focusRightMenuIdx = KTW.utils.util.getIndex(focusRightMenuIdx, 1, 4);
                        }
                        _buttonFocusRefresh(focusRightMenuIdx, menuType);
                    }else {
                        if (isScrollArea) {
                            focusRowIndex = focusRowIndex + (10 - (focusRowIndex % 10));
                            if (focusPageItemCount === 20) {
                                isPageMove = true;
                            } else {
                                var tempRowCount = Math.floor(focusPageItemCount / 2);
                                if (focusPageItemCount % 2 !== 0) {
                                    tempRowCount++;
                                }
                                log.printDbg("keyAction() focusPageItemCount : " + focusPageItemCount + " , tempRowCount : " + tempRowCount + " , tempRowCount : " + tempRowCount + " , focusXIdx : " + focusXIdx);
                                if (tempRowCount < (focusRowIndex + 1)) {
                                    isPageMove = true;
                                    focusRowIndex = 0;
                                } else {
                                    isFocusMove = true;

                                    if (((focusRowIndex * 2) + (focusXIdx)) < focusPageItemCount) {
                                    } else {
                                        focusXIdx = 0;
                                    }
                                }
                            }

                        } else {
                            if (focusPageItemCount === 20) {
                                focusRowIndex++;
                                if (focusRowIndex > 9) {
                                    isPageMove = true;
                                    focusRowIndex = 0;
                                } else {
                                    isFocusMove = true;
                                }

                            } else {
                                focusRowIndex++;

                                var tempRowCount = Math.floor(focusPageItemCount / 2);
                                if (focusPageItemCount % 2 !== 0) {
                                    tempRowCount++;
                                }
                                log.printDbg("keyAction() focusPageItemCount : " + focusPageItemCount + " , tempRowCount : " + tempRowCount + " , tempRowCount : " + tempRowCount + " , focusXIdx : " + focusXIdx);
                                if (tempRowCount < (focusRowIndex + 1)) {
                                    isPageMove = true;
                                    focusRowIndex = 0;
                                } else {
                                    isFocusMove = true;

                                    if (((focusRowIndex * 2) + (focusXIdx)) < focusPageItemCount) {
                                    } else {
                                        focusXIdx = 0;
                                    }
                                }
                            }
                        }
                        if (isFocusMove === true) {
                            _buttonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        } else if (isPageMove === true) {
                            curPage++;
                            if (totalPage <= curPage) {
                                curPage = 0;
                            }
                            channelDataListChangePage(curPage);
                            focusRowIndex = 0;
                            if (focusPageItemCount !== 20) {
                                if (((focusRowIndex * 2) + (focusXIdx)) < focusPageItemCount) {
                                } else {
                                    focusXIdx = 0;
                                }
                            }
                            if(!isScrollArea)
                                _buttonFocusRefresh(((focusRowIndex * 2) + focusXIdx), menuType);
                        }


                    }
                    return true;
                case KEY_CODE.RED:
                    if(menuType == 1) {
                        return true;
                    }
                    if(isScrollArea) {
                        // 스크롤 포커스 해제
                        setScrollFocus(false);
                        // 현재 화면의 가장 첫번째 인덱스로 포커스 이동
                        focusXIdx = 0;
                        focusRowIndex = 0;
                        _buttonFocusRefresh(0, 0);
                    }else {
                        if(focusXIdx == 0 && focusRowIndex == 0) {
                            //이미 가장 첫번째 포커스라면 페이지를 이동시킴
                            curPage --;
                            if(curPage < 0) {
                                curPage = totalPage -1;
                            }
                            channelDataListChangePage(curPage);
                        }else {
                            // 현재 화면의 가장 첫번째 인덱스로 포커스 이동
                            focusXIdx = 0;
                            focusRowIndex = 0;
                            _buttonFocusRefresh(0, 0);
                        }
                    }

                    return true;
                case KEY_CODE.BLUE:
                    if(menuType == 1) {
                        return true;
                    }
                    if(isScrollArea) {
                        setScrollFocus(false);
                        // 현재 화면의 가장 마지막 인덱스로 포커스 이동
                        // 현재 페이지가 마지막 페이지인지 확인
                        if(curPage == totalPage -1) {
                            // 현재 페이지가 마지막 페이지라면 포커스 끝처리 필요함.
                            focusRowIndex = Math.floor(focusPageItemCount / 2) -1;
                            if(focusPageItemCount % 2 !== 0) {
                                focusRowIndex ++;
                                focusXIdx =0;
                            }else {
                                focusXIdx =1;
                            }
                            // focusRowIndex = tmpRowCount -1;
                        }else {
                            // 마지막 페이지가 아닌 경우 채널 리스트가 20개 보여지므로, 현재 페이지 20번째 채널로 포커스 이동
                            focusRowIndex = 9;
                            focusXIdx = 1;
                        }
                        _buttonFocusRefresh((focusRowIndex *2 + focusXIdx),0);
                    }else {
                        // 현재 페이지가 마지막 페이지 인지 확인
                        if(curPage == totalPage -1) {
                            // 현재 페이지가 마지막 페이지라면 포커스 끝처리 적용
                            if( (focusRowIndex *2) + focusXIdx == focusPageItemCount -1 ) {
                                // 이미 마지막 인덱스에 포커스가 가 있는 경우 첫번째 페이지로 전환
                                curPage = 0;
                                channelDataListChangePage(curPage);
                                focusRowIndex = 9;
                                focusXIdx = 1;
                            }else {
                                // 마지막 포커스로 이동
                                focusRowIndex = Math.floor(focusPageItemCount / 2) -1;
                                if(focusPageItemCount %2 !==0) {
                                    focusRowIndex++;
                                    focusXIdx = 0;
                                }else {
                                    focusXIdx = 1;
                                }
                            }
                            _buttonFocusRefresh((focusRowIndex *2 + focusXIdx),0);
                        }else {
                            //현재 페이지가 마지막 페이지가 아닌 경우
                            if(focusRowIndex == 9 && focusXIdx == 1) {
                                // 마지막 인덱스에 위치해있는지 확인
                                // 페이지 이동
                                curPage++;
                                channelDataListChangePage(curPage);
                                if(curPage == totalPage -1) {
                                    //이동한 페이지가 마지막 페이지인지 확인
                                    //마지막 포커스로 이동
                                    focusRowIndex = Math.floor(focusPageItemCount / 2) -1;
                                    if(focusPageItemCount %2 !==0) {
                                        focusRowIndex++;
                                        focusXIdx = 0;
                                    }else {
                                        focusXIdx = 1;
                                    }
                                }
                            }else {
                                focusRowIndex = 9;
                                focusXIdx = 1;
                            }
                            _buttonFocusRefresh((focusRowIndex *2 + focusXIdx),0);
                        }
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if(isScrollArea) {
                        LayerManager.historyBack();
                    }
                    else if (menuType == 0) {
                        if(focusXIdx === 0) {
          /*                  setScrollFocus(true);
                            INSTANCE.div.find("#settingTableFocus_div").addClass("hide");*/
                            LayerManager.historyBack();
                        }
                        if(focusXIdx === 1) {
                            focusXIdx--;
                            _buttonFocusRefresh(((focusRowIndex*2) + focusXIdx), menuType);
                        }
                    } else {
                        if(focusPageItemCount>0) {
                            menuType = 0;
                            setColorKeyArea(true);
                            _buttonFocusRefresh(((focusRowIndex*2) + focusXIdx), menuType);
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(isScrollArea) {
                        if(focusPageItemCount>0) {
                            menuType = 0;
                            _buttonFocusRefresh(((focusRowIndex*2) + focusXIdx), menuType);
                        }
                        setScrollFocus(false);
                    }
                    else if (menuType == 0) {
                        if(focusXIdx === 1) {
                            menuType = 1;
                            setColorKeyArea(false);
                        }else {
                            if(((focusRowIndex*2)+(focusXIdx+1))<focusPageItemCount){
                                focusXIdx++;
                            }else {
                                if(focusXIdx %2 == 0) {
                                    focusXIdx++;
                                    focusRowIndex--;
                                }else {
                                    menuType = 1;
                                    setColorKeyArea(false);
                                    if(KTW.CONSTANT.IS_OTS) {
                                        focusRightMenuIdx = 3;
                                    } else {
                                        focusRightMenuIdx = 2;
                                    }
                                }

                            }
                        }
                        if(menuType ===0) {
                            _buttonFocusRefresh(((focusRowIndex*2) + focusXIdx), menuType);
                        }else {
                            if(KTW.CONSTANT.IS_OTS) {
                                focusRightMenuIdx = 3;
                            } else {
                                focusRightMenuIdx = 2;
                            }
                            _buttonFocusRefresh(focusRightMenuIdx, menuType);
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if(isScrollArea) {
                        if(focusPageItemCount>0) {
                            menuType = 0;
                            _buttonFocusRefresh(((focusRowIndex*2) + focusXIdx), menuType);
                        }
                        setScrollFocus(false);
                        return true;
                    }
                    if(menuType === 0){
                        _enterEventHandler(((focusRowIndex*2) + focusXIdx), menuType);
                    }else {
                        _enterEventHandler(focusRightMenuIdx, menuType);

                    }
                    return true;
                default:
                    return false;
            }
        }


        function _buttonFocusRefresh(index, _menuType) {
            var left;
            var top;
            if(KTW.CONSTANT.IS_OTS) {
                INSTANCE.div.find("#rightMenu_btn_area_0_ots .rightMenu_btn_div").removeClass("focus");
            }else {
                INSTANCE.div.find("#rightMenu_btn_area_0 .rightMenu_btn_div").removeClass("focus");
            }
            INSTANCE.div.find("#settingTableFocus_div").addClass("hide");
            INSTANCE.div.find("#settingTable .channelCell_div").removeClass("focus");
            if (_menuType == 0) {
                INSTANCE.div.find("#settingTableFocus_div").removeClass("hide");
                if(index == 0 || index == 1 ) {
                    if(index == 0) {
                        left = 133;
                    }else {
                        left = (INSTANCE.div.find("#settingTable .channelCell_div:eq(" + (index) + ")").offset().left);
                    }
                    top = 123;
                }else {
                    left = (INSTANCE.div.find("#settingTable .channelCell_div:eq(" + (index) + ")").offset().left);
                    top =  (INSTANCE.div.find("#settingTable .channelCell_div:eq(" + (index) + ")").offset().top)- 5;
                }
                INSTANCE.div.find("#settingTableFocus_div").css({
                    "left": left+ "px",
                    "top": top + "px"
                });
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("focus");
            } else {
                if(KTW.CONSTANT.IS_OTS) {
                    INSTANCE.div.find("#rightMenu_btn_area_0_ots .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                }else {
                    INSTANCE.div.find("#rightMenu_btn_area_0 .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                }
            }
        }

        function _selectListButtonFocusRefresh(index, _menuType) {
            log.printDbg("_selectListButtonFocusRefresh "+index+"  "+_menuType);
            var left;
            var top;
            INSTANCE.div.find("#rightMenu_btn_area_1 .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#settingTableFocus_div").addClass("hide");
            INSTANCE.div.find("#settingTable .channelCell_div").removeClass("focus");
            if (_menuType == 0) {
                INSTANCE.div.find("#settingTableFocus_div").removeClass("hide");
                if(index == 0 || index == 1 ) {
                    if(index == 0) {
                        left = 133;
                    }else {
                        left = (INSTANCE.div.find("#settingTable .channelCell_div:eq(" + (index) + ")").offset().left);
                    }
                    top = 123;
                }else {
                    left = (INSTANCE.div.find("#settingTable .channelCell_div:eq(" + (index) + ")").offset().left);
                    top =  (INSTANCE.div.find("#settingTable .channelCell_div:eq(" + (index) + ")").offset().top)- 5;
                }
                INSTANCE.div.find("#settingTableFocus_div").css({
                    "left": left+ "px",
                    "top": top + "px"
                });
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("focus");
            } else {
                INSTANCE.div.find("#rightMenu_btn_area_1 .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
            }
        }

        /**
         * 선택된 채널 안에서 OK 키 눌렀을 떄 처리
         * @param index
         * @param _menutype
         * @private
         */
        function _selectedListEnterEventHandler(index, _menutype) {
            log.printDbg("_selectedListEnterEventHandler "+index+"  menutype "+_menutype);
            if(_menutype == 0) {
                //채널 리스트에서 체크 한 경우 , 체크 혹은 체크 해제 해야함 (전체 체크는 일단 제외)
                if(INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").hasClass("select")){
                    if(selectChannelListCurrentPage == 0 && index == 0) {
                       // 모두 선택 체크 해제 로직
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").removeClass("select");
                        for(var i = 0; i < selectedChIndexList.length; i++) {
                            selectedChIndexList[i].isSelect = false;
                        }
                        selectedSkylifeChannelCount = 0;
                        selectedIPTVChannelCount = 0;
                        // 페이지 업데이트
                        _selectChannelDataListChangePage();
                    }else {
                        // 일반 체크 해제 로직
                        // 체크 되어 있으면 체크해제
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").removeClass("select");
                        // 리스트에 체크가 빠졌다는걸 확인시켜줘야 함
                        selectedChIndexList[(selectChannelListCurrentPage*20)+index].isSelect = false;

                        // 모두 선택 체크 해제 (체크가 하나 해제되었으므로)
                        selectedChIndexList[0].isSelect = false;
                        if(selectChannelListCurrentPage == 0) {
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(0)").removeClass("select");
                        }
                        // 현재 선택된 갯수 업데이트
                        if(selectedChIndexList[(selectChannelListCurrentPage*20)+index].isSkylife == true) {
                            selectedSkylifeChannelCount --;
                        }else {
                            selectedIPTVChannelCount--;
                        }
                    }
                }else {

                    if(selectChannelListCurrentPage == 0 && index == 0) {
                        //모두 선택 체크 로직
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("select");
                        for(var i = 0; i < selectedChIndexList.length; i++) {
                            selectedChIndexList[i].isSelect = true;
                        }
                        selectedIPTVChannelCount = iptvBlockedChannelCount;
                        selectedSkylifeChannelCount = skylifeBlockedChannelCount;
                        // 페이지 업데이트
                        _selectChannelDataListChangePage();
                    }else {
                        // 체크 안되어 있으면 체크
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("select");
                        // 리스트에 뭔가 체크가 되었다는걸 확인시켜줘야 함
                        selectedChIndexList[(selectChannelListCurrentPage*20)+index].isSelect = true;

                        // 현재 선택된 갯수 업데이트
                        if(selectedChIndexList[(selectChannelListCurrentPage*20)+index].isSkylife == true) {
                            selectedSkylifeChannelCount ++;
                        }else {
                            selectedIPTVChannelCount++;
                        }
                    }
                }
                if(KTW.CONSTANT.IS_OTS) {
                    INSTANCE.div.find("#rightMenu_text1").html("현재 olleh 채널 " + selectedIPTVChannelCount + "개,<br>SkyLife 채널 " + selectedSkylifeChannelCount + "개<br>선택되었습니다<br>[최대 "+ (MAX_OTS_SKYLIFE_COUNT+MAX_OTS_IPTV_COUNT) +"개]");
                }else {
                    INSTANCE.div.find("#rightMenu_text2").html("현재 " + selectedIPTVChannelCount + "개 채널이<br>선택되었습니다<br>[최대 " + MAX_OTV_IPTV_COUNT + "개]");
                }


            }else {

                // 저장 / 취소 로직.
                if(index == 0) {
                    // 저장
                    for(var i = 1; i < selectedChIndexList.length; i++) {
                        if(KTW.CONSTANT.IS_OTS) {
                            if(selectedChIndexList[i] !== null && selectedChIndexList[i].isSelect == false && selectedChIndexList[i].isSkylife == true) {
                                // 체크 해제된 skylife 채널의 true 값을 false 로 바꾼다
                                selectData_skylife[selectedChIndexList[i].index] = false;
                            }
                        }
                        if(selectedChIndexList[i] !== null && selectedChIndexList[i].isSelect == false && selectedChIndexList[i].isSkylife == false) {
                            selectData_otv[selectedChIndexList[i].index] = false;
                        }
                    }
                    _saveChannel();
                    data.complete();
                    settingMenuManager.historyBackAndPopup();
                }else {
                    // 취소
                    _restoreChannelList();
                }
            }
        }

        function _enterEventHandler(index, _menuType) {
            if (_menuType == 0) {
                if(isFocusSkyChannelRing === true) {
                    if(selectData_skylife[(curPage*20) + index] !== undefined && selectData_skylife[(curPage*20) + index] !== null && selectData_skylife[(curPage*20) + index] === true) {
                        selectData_skylife[(curPage*20) + index] = false;
                        skylifeBlockedChannelCount--;

                        //체크 해제 로직
                        //INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ") .cellCheck_img").attr("src", "images/pop_cb_dim_uncheck.png");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").removeClass("select");

                    }else {
                        selectData_skylife[(curPage*20) + index] = true; //
                        skylifeBlockedChannelCount++;

                        if(skylifeBlockedChannelCount>MAX_OTS_SKYLIFE_COUNT) {
                            selectData_skylife[(curPage*20) + index] = false;
                            skylifeBlockedChannelCount--;
                            KTW.managers.service.SimpleMessageManager.showMessageTextOnly("개수 초과! 시청채널 제한은 SkyLife 채널"+MAX_OTS_SKYLIFE_COUNT + "개, olleh tv 채널 "+MAX_OTS_IPTV_COUNT+"개까지 등록 가능합니다");

                        }else {
                            //INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ") .cellCheck_img").attr("src", "images/pop_cb_dim_check.png");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("select");
                        }
                    }
                }else {
                    if(selectData_otv[(curPage*20) + index] !== undefined && selectData_otv[(curPage*20) + index] !== null && selectData_otv[(curPage*20) + index] === true) {
                        selectData_otv[(curPage*20) + index] = false; // 선택 해제
                        iptvBlockedChannelCount--;

                       // INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ") .cellCheck_img").attr("src", "images/pop_cb_dim_uncheck.png");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").removeClass("select");

                    }else {
                        selectData_otv[(curPage*20) + index] = true; // 선택
                        iptvBlockedChannelCount++;

                        if(KTW.CONSTANT.IS_OTS) {
                            if(iptvBlockedChannelCount>MAX_OTS_IPTV_COUNT) {
                                selectData_otv[(curPage*20) + index] = false;
                                iptvBlockedChannelCount--;
                                KTW.managers.service.SimpleMessageManager.showMessageTextOnly("개수 초과! 시청채널 제한은 SkyLife 채널"+MAX_OTS_SKYLIFE_COUNT + "개, olleh tv 채널 "+MAX_OTS_IPTV_COUNT+"개까지 등록 가능합니다");
                            }else {
                               // INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ") .cellCheck_img").attr("src", "images/pop_cb_dim_check.png");
                                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("select");
                            }
                        }else {
                            if(iptvBlockedChannelCount>MAX_OTV_IPTV_COUNT) {
                                selectData_otv[(curPage*20) + index] = false;
                                iptvBlockedChannelCount--;
                                KTW.managers.service.SimpleMessageManager.showMessageTextOnly("개수 초과! 시청채널 제한은 "+MAX_OTV_IPTV_COUNT + "개까지 등록 가능합니다");
                            }else {
                               // INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ") .cellCheck_img").attr("src", "images/pop_cb_dim_check.png");
                                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + index + ")").addClass("select");
                            }
                        }

                    }

                }

                if(KTW.CONSTANT.IS_OTS) {
                    if(isFocusSkyChannelRing === true) {
                        INSTANCE.div.find("#rightMenu_text1").html("현재 olleh 채널 " + iptvBlockedChannelCount + "개,<br>SkyLife 채널 " + skylifeBlockedChannelCount + "개<br>선택되었습니다<br>[최대 "+MAX_OTS_SKYLIFE_COUNT +"개]");
                    }else {
                        INSTANCE.div.find("#rightMenu_text1").html("현재 olleh 채널 " + iptvBlockedChannelCount + "개,<br>SkyLife 채널 " + skylifeBlockedChannelCount + "개<br>선택되었습니다<br>[최대 "+MAX_OTS_IPTV_COUNT +"개]");
                    }
                }else {
                    INSTANCE.div.find("#rightMenu_text2").html("현재 " + iptvBlockedChannelCount + "개 채널이<br>선택되었습니다<br>[최대 " + MAX_OTV_IPTV_COUNT + "개]");
                }

            } else {
                switch (index) {
                    case 0:
                        _resetSelectChannelData();
                        break;
                    case 1:
                        refreshSelectChannelDataList();
                        break;
                    case 2:
                        if(KTW.CONSTANT.IS_OTS) {
                            if(isFocusSkyChannelRing === true) {
                                isFocusSkyChannelRing = false;
                                focusBlockList = iptvBlockList;
                                focusChannelList = iptvChannelList;

                            }else {
                                isFocusSkyChannelRing = true;
                                focusBlockList = skyBlockList;
                                focusChannelList = skyChannelList;
                            }
                            menuType = 0;
                            curPage = 0;
                            totalPage = Math.ceil(focusChannelList.length/20);

                            scroll.show({
                                maxPage: totalPage,
                                curPage: curPage
                            });


                            log.printDbg("show() curPage : " + curPage + " , totalPage : " + totalPage );

                            _refreshChannelDataList();
                            channelDataListChangePage(curPage);

                            focusRowIndex = 0;
                            focusXIdx = 0;

                            if(focusPageItemCount>0) {
                                _buttonFocusRefresh((focusRowIndex*2)+focusXIdx, menuType);
                                setColorKeyArea(true);
                            }

                            if(isFocusSkyChannelRing === false) {
                                INSTANCE.div.find("#rightMenu_btn_area_0_ots .rightMenu_btn_div:eq(2) .rightMenu_btn_text").text("SkyLife 채널 목록");
                            }else {
                                INSTANCE.div.find("#rightMenu_btn_area_0_ots .rightMenu_btn_div:eq(2) .rightMenu_btn_text").text("olleh tv 채널 목록");
                            }

                        }else {

                            _saveChannel();
                            data.complete();
                            settingMenuManager.historyBackAndPopup();

                        }
                        break;
                    case 3:
                        if(KTW.CONSTANT.IS_OTS) {

                            _saveChannel();
                            data.complete();
                            settingMenuManager.historyBackAndPopup();

                        }else {
                            LayerManager.historyBack();
                        }
                        break;
                    case 4:
                        if(KTW.CONSTANT.IS_OTS) {
                            LayerManager.historyBack();
                        }
                        break;

                }
            }
        }




        function _restoreChannelList() {
            log.printDbg("restoreChannelList() curPage : " + curPage + " , totalPage : " + totalPage  + " , focusRowIndex : " + focusRowIndex + " , focusXIdx : " + focusXIdx);

            menuType = 0;
            /*            if(totalPage <=1) {
             indicator.getView().css("display" , "none");
             }else {
             indicator.getView().css("display", "");
             indicator.setSize(totalPage, curPage);
             }*/

            focusRowIndex = lastRowIndex;
            focusXIdx = lastFocusXidx;

            scroll.show({
                maxPage: totalPage,
                curPage: curPage
            });

            _refreshChannelDataList();
            channelDataListChangePage(curPage);
            if(focusPageItemCount>0) {
                // _buttonFocusRefresh((focusRowIndex*2)+focusXIdx, menuType);
                menuType = 1;
                focusRightMenuIdx = 1;
                _buttonFocusRefresh(focusRightMenuIdx, menuType);
                setColorKeyArea(false);
            }
            isSelectList = false;
        }


        /**
         * 선택된 채널 리스트 보여준다. 채널 화면 전환할 때 1회 실행되는 함수
         */
        function refreshSelectChannelDataList() {
            log.printDbg("refreshSelectChannelDataList()");
            selectChannelList = [];
            selectedChIndexList = [];

            // 선택 채널 영역에서 모두 선택 키로 사용
            var selectAll  = {name: "모두 선택", majorChannel: "00", idType: "none"};
            selectChannelList[selectChannelList.length] = selectAll;
            // 첫번째 영역에는 dummy data 저장,
            selectedChIndexList[selectedChIndexList.length] = { index: "dummy", inSelect: false, isSkylife : false};

            lastRowIndex = focusRowIndex;
            lastFocusXidx = focusXIdx;

            selectedIPTVChannelCount = iptvBlockedChannelCount;
            selectedSkylifeChannelCount = skylifeBlockedChannelCount;


            // OTS 인 경우.
            if (KTW.CONSTANT.IS_OTS) {
                for (var i = 0; i < skyChannelList.length; i++) {
                    if (selectData_skylife[i] !== undefined && selectData_skylife[i] !== null && selectData_skylife[i] === true) {
                        selectChannelList[selectChannelList.length] = skyChannelList[i];
                        // sw.nam 선택 채널 페이지에서 리스트 저장하기 -
                        // 선택된 채널 리스트 배열을 저장 하면서, 선택된 채널 리스트가 실제 리스트에서 몇번째 인덱스인지 같이 저장한다. skylife 여부도 같이 저장
                        selectedChIndexList[selectedChIndexList.length] = { index : i, isSelect: true, isSkylife : true};
                    }
                }
            }

            for (var j = 0; j < iptvChannelList.length; j++) {
                if (selectData_otv[j] !== undefined && selectData_otv[j] !== null && selectData_otv[j] === true) {
                    selectChannelList[selectChannelList.length] = iptvChannelList[j];
                    // sw.nam 선택 채널 페이지에서 리스트 저장하기 -
                    // 선택된 채널 리스트 배열을 저장 하면서, 선택된 채널 리스트가 실제 리스트에서 몇번째 인덱스인지 같이 저장한다. isSelect 도 처음에 다 setting 되어 으므로 setting.
                    selectedChIndexList[selectedChIndexList.length] = { index : j, isSelect: true, isSkylife : false};
                }
            }
            isSelectList = true;

            //선택된 채널리스트의 갯스로 페이지를 구함
            selectChannelListCurrentPage = 0;
            if (selectChannelList.length <= 20) {
                selectChannelListTotalPage = 1;
            } else {
                selectChannelListTotalPage = Math.ceil(selectChannelList.length / 20);
            }

            scroll.show({
                maxPage: selectChannelListTotalPage,
                curPage: selectChannelListCurrentPage
            });

            //INSTANCE.div.find("#settingTable .cellCheck_img").addClass("hide");
            if(selectChannelListTotalPage > 1) {
                setColorKeyArea(true);
            }else {
                setColorKeyArea(false);
            }

            if (selectChannelList.length-1 < 1) {
                // 선택된게 없는 경우 (모두 선택 필드 제외)
                INSTANCE.div.find("#noSelectOTVChannel_area").css({visibility: "inherit"});
                INSTANCE.div.find("#noSelectOTVChannel_area #noSelectOTVChannel_text").text("시청채널 제한으로 선택된 채널이 없습니다");
                INSTANCE.div.find("#settingTable").css({visibility: "hidden"});

                // 선택된 채널 없음 flag set
                hasSelectedChannel = false;
                // 선택된 채널 저장버튼 hide
                INSTANCE.div.find("#rightMenu_btn_area_1 .rightMenu_btn_div:eq(0)").css({visibility: "hidden"});
                // 취소버튼 text 확인 으로 변경
                INSTANCE.div.find("#rightMenu_btn_area_1 .rightMenu_btn_div:eq(1) .rightMenu_btn_text").text("확인");


                menuType = 1;
                focusRightMenuIdx = 1;
                _selectListButtonFocusRefresh(focusRightMenuIdx,menuType);
               // INSTANCE.div.find("#rightMenu_area #rightMenu_btn_area_1").addClass("focus")
            } else {
                // 선택된게 있는 경우, 보여준다.
                INSTANCE.div.find("#noSelectOTVChannel_area").css({visibility: "hidden"});
                INSTANCE.div.find("#settingTable").css({visibility: "inherit"});

                INSTANCE.div.find("#settingTableFocus_div").removeClass("hide");


                log.printDbg("refreshSelectChannelDataList() selectChannelListTotalPage : " + selectChannelListTotalPage + " , selectChannelListCurrentPage : " + selectChannelListCurrentPage);
                selectedPageItemCount = 0;
                if (((selectChannelListCurrentPage * 20) + 20) <= selectChannelList.length) {
                    selectedPageItemCount = 20;
                } else {
                    selectedPageItemCount = (selectChannelList.length - (selectChannelListCurrentPage * 20));
                }

                for (var idx = 0; idx < 20; idx++) {
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").text("");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").text("");

                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display", "none");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").css("display", "none");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").css("display", "none");
                }

                for (var idx = 0; idx < selectedPageItemCount; idx++) {
                    if (KTW.CONSTANT.IS_OTS === true) {
                        if (selectChannelList[(selectChannelListCurrentPage * 20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS || selectChannelList[(selectChannelListCurrentPage * 20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").removeClass("hide");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").attr("src", "images/icon_olleh.png");

                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-left", "10px");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-right", "9px");
                        } else if (selectChannelList[(selectChannelListCurrentPage * 20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || selectChannelList[(selectChannelListCurrentPage * 20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) {
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").removeClass("hide");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").attr("src", "images/icon_sky.png");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-left", "19px");
                            INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-right", "18px");
                        }

                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("margin-top", "22px");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display", "");
                    } else {

                    }
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").text(util.numToStr(selectChannelList[(selectChannelListCurrentPage * 20) + idx].majorChannel, 3));
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").text(selectChannelList[(selectChannelListCurrentPage * 20) + idx].name);
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").css("display", "");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").css("display", "");

                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display", "");
                }

                // 현재 페이지가 첫번째 페이지면 첫번째 인덱스는 "모두 선택" 키 이므로 채널과 아이콘 영역의 display 를 none 으로 변경시킨다.
                if(selectChannelListCurrentPage == 0) {
                    //  INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellCheck_img").css("display" , "none");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_number").css("display" , "none");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_name").css({"margin-left": 14});
                }else {
                    //   INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellCheck_img").css("display" , "");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_number").css("display" , "");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_name").css({"margin-left": 26});
                }

                // 선택된 채널 없음 flag set
                hasSelectedChannel = true;
                // 선택된 채널 저장버튼 showing
                INSTANCE.div.find("#rightMenu_btn_area_1 .rightMenu_btn_div:eq(0)").css({visibility: "inherit"});
                // 확인버튼 text 취소 로 변경
                INSTANCE.div.find("#rightMenu_btn_area_1 .rightMenu_btn_div:eq(1) .rightMenu_btn_text").text("취소");


                focusRowIndex = 0;
                focusXIdx = 0;
                menuType = 0;
                _selectListButtonFocusRefresh(((focusRowIndex * 2) + focusXIdx),menuType);
            }

            //INSTANCE.div.find("#settingTable .channelCell_div").addClass("selectList");
            //
            INSTANCE.div.find("#settingTable .channelCell_div").addClass("select");
            if(KTW.CONSTANT.IS_OTS === true) {
                INSTANCE.div.find("#rightMenu_btn_area_0_ots").addClass("hide");
            }else {
                INSTANCE.div.find("#rightMenu_btn_area_0").addClass("hide");
            }
            INSTANCE.div.find("#rightMenu_btn_area_1").removeClass("hide");

            INSTANCE.div.find("#selectMenu_title").text("선택한 채널 확인");

            if(KTW.CONSTANT.IS_OTS) {
                INSTANCE.div.find("#rightMenu_text1").html("현재 olleh 채널 " + selectedIPTVChannelCount + "개,<br>SkyLife 채널 " + selectedSkylifeChannelCount + "개<br>선택되었습니다<br>[최대 "+ (MAX_OTS_SKYLIFE_COUNT+MAX_OTS_IPTV_COUNT) +"개]");
            }else {
                INSTANCE.div.find("#rightMenu_text2").html("현재 " + selectedIPTVChannelCount + "개 채널이<br>선택되었습니다<br>[최대 " + MAX_OTV_IPTV_COUNT + "개]");
            }
        }

        /**
         * 선택된 채널 리스트 체인지
         * @private
         */
        function _selectChannelDataListChangePage() {
            log.printDbg("refreshSelectChannelDataList() selectChannelListCurrentPage : " + selectChannelListCurrentPage);

            selectedPageItemCount = 0;
            if(((selectChannelListCurrentPage*20) + 20)<=selectChannelList.length) {
                selectedPageItemCount = 20;
            }else {
                selectedPageItemCount = (selectChannelList.length - (selectChannelListCurrentPage*20));
            }

            for (var idx = 0 ; idx < 20 ;idx ++) {
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").text("");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").text("");

                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display" , "none");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").css("display" , "none");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").css("display" , "none");
            }

            for (var idx = 0 ; idx < selectedPageItemCount ;idx ++) {
                if(KTW.CONSTANT.IS_OTS === true) {
                    if(selectChannelList[(selectChannelListCurrentPage*20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS || selectChannelList[(selectChannelListCurrentPage*20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").removeClass("hide");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").attr("src", "images/icon_olleh.png");

                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-left", "10px");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-right", "9px");
                    }else if(selectChannelList[(selectChannelListCurrentPage*20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || selectChannelList[(selectChannelListCurrentPage*20) + idx].idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) {
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").removeClass("hide");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").attr("src", "images/icon_sky.png");

                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-left", "19px");
                        INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .channelIcon_img").css("margin-right", "18px");
                    }

                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("margin-top", "22px");
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display" , "");
                }else {

                }
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").text(util.numToStr(selectChannelList[(selectChannelListCurrentPage*20) + idx].majorChannel, 3));
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").text(selectChannelList[(selectChannelListCurrentPage*20) + idx].name);
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_number").css("display" , "");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellChannel_name").css("display" , "");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display" , "");

                //현 인덱스의 채널이 선택되어 있으면 체크, 아니면 체크 해제
               // INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ") .cellCheck_img").css("display" , "");
                if(selectedChIndexList[(selectChannelListCurrentPage*20) + idx].isSelect == true) {
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ")").addClass("select");
                }else {
                    INSTANCE.div.find("#settingTable .channelCell_div:eq(" + idx + ")").removeClass("select");
                }

            }
            // 현재 페이지가 첫번째 페이지면 첫번째 인덱스는 "모두 선택" 키 이므로 채널과 아이콘 영역의 display 를 none 으로 변경시킨다.
            if(selectChannelListCurrentPage == 0) {
              //  INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellCheck_img").css("display" , "none");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_number").css("display" , "none");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_name").css({"margin-left": 14});
            }else {
             //   INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellCheck_img").css("display" , "");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_number").css("display" , "");
                INSTANCE.div.find("#settingTable .channelCell_div:eq(0) .cellChannel_name").css({"margin-left": 26});
            }

            // indicator.setPos(selectChannelListCurrentPage);
            scroll.setCurrentPage(selectChannelListCurrentPage);
        }


        function _saveChannel() {
            var saveiptvList = [];
            var saveskylifeList = [];
            var itemiptvCount = 0;
            var itemskylifeCount = 0;

            if(KTW.CONSTANT.IS_OTS) {
                for(var i=0;i<skyChannelList.length;i++) {
                    if(selectData_skylife[i] !== undefined && selectData_skylife[i] !== null && selectData_skylife[i] === true) {
                        saveskylifeList[itemskylifeCount] = {};
                        saveskylifeList[itemskylifeCount].ccid  = skyChannelList[i].ccid;
                        itemskylifeCount++;
                    }
                }

                var skylifelimitchannellist = util.arrayCopy(saveskylifeList);

                KTW.oipf.AdapterHandler.navAdapter.changeChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED, skylifelimitchannellist);
            }

            for(var j=0;j<iptvChannelList.length;j++) {
                if(selectData_otv[j] !== undefined && selectData_otv[j] !== null && selectData_otv[j] === true) {
                    saveiptvList[itemiptvCount] = {};
                    saveiptvList[itemiptvCount].ccid  = iptvChannelList[j].ccid;
                    itemiptvCount++;
                }
            }
            var ollehtvlimitchannellist = util.arrayCopy(saveiptvList);
            KTW.oipf.AdapterHandler.navAdapter.changeChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_BLOCKED, ollehtvlimitchannellist);
        }
    };

    KTW.ui.layer.setting.setting_limitChannel.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_limitChannel.constructor = KTW.ui.layer.setting.setting_limitChannel;

    KTW.ui.layer.setting.setting_limitChannel.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_limitChannel.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.setting.setting_limitChannel.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();