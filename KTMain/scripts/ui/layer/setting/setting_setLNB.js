/**
 * Created by Yun on 2017-01-25.
 *
 * 설정 > 시스템설정 (OTS관리자 hidden menu) > LNB 설정
 */

(function () {
    KTW.ui.layer.setting.setting_setLNB  = function SetLNB(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var INSTANCE = this;
        var styleSheet;

        var log = KTW.utils.Log;
        //var util = KTW.utils.util;
        var hwAdapter = KTW.oipf.adapters.HWAdapter;

        var focusIdx = 0;
        var selectIdx = 0;
        var menuIdx = 0;
        //var toastPopup = null;
        //var SettingDataAdaptor = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var data;
        // 2017.3.16 [sw.nam] 2.0 에서도 port(0~3) 기준으로 LNB를 set 하고, name 은 view 에서 직접 하드코딩 하므로 2.0 로직을 그대로 적용 함
        var LNB_ID = [{name: "DisEqC-A(무궁화 3호)", port: "0"},{name: "DiSEqC-B", port: "1"},{name: "DiSEqC-C", port: "2"},{name: "DiSEqC-D", port: "3"}];
        var selectedLnb_info;

        var clock;
        var clockArea;


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setLNB"});
            //toastPopup = new SettingToastPopup();
            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>LNB 설정</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            focusIdx = 0;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + focusIdx + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + focusIdx + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
            buttonFocusRefresh(focusIdx, 0);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                INSTANCE.div.find("#backgroundTitle").text(data.name);
            }


          //  clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            var consumed = false;

            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, (menuIdx == 0 ? 4 : 2)), menuIdx);
                    consumed = true;
                    return consumed;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, (menuIdx == 0 ? 4 : 2)), menuIdx);
                    consumed = true;
                    return consumed;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    }else if(menuIdx ==0) {
                        LayerManager.historyBack();
                    }
                    consumed = true;
                    return consumed;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    consumed = true;
                    return consumed;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    consumed = true;
                    return consumed;
            }

            return consumed;

        };


        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    selectIdx = focusIdx;
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {

                        if(KTW.PLATFORM_EXE === "PC") {
                            selectedLnb_info = [3];
                            selectedLnb_info[0] = 0; // LNB power
                            selectedLnb_info[1] = 1; // 22kHz Tone
                            selectedLnb_info[2] = "10750"; // frequency
                        } else {
                            selectedLnb_info = hwAdapter.getLNB(parseInt(LNB_ID[selectIdx].port));
                        }


                        LayerManager.activateLayer({
                            obj: {
                                id: KTW.ui.Layer.ID.SETTING_LNB_POPUP,
                                type: Layer.TYPE.POPUP,
                                priority: Layer.PRIORITY.POPUP,
                                linkage: true,
                                params: {
                                    selectedLNB: {
                                        name : LNB_ID[selectIdx].name,
                                        port : LNB_ID[selectIdx].port,
                                        info : selectedLnb_info
                                    },
                                 //   selectLNB: INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectIdx + ") .table_radio_text").text(),

                                    complete: function() {
                                        //save 함수 실행.
                                        data.complete();
                                        settingMenuManager.historyBackAndPopup();

                                    }
                                }
                            },

                            new: true,
                            visible: true
                        });
                    } else {
                        LayerManager.historyBack();
                    }
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>LNB 설정</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>DisEqC-A(무궁화 3호)</div>" +
                "</div>" +
                "<div class='table_radio_btn'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>DiSEqC-B</div>" +
                "</div>" +
                "<div class='table_radio_btn'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>DiSEqC-C</div>" +
                "</div>" +
                "<div class='table_radio_btn'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>DiSEqC-D</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>위성을 선택하거나<br>설정할 수 있습니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>보기</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }
        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }
    };

    KTW.ui.layer.setting.setting_setLNB.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_setLNB.constructor = KTW.ui.layer.setting.setting_setLNB;

    KTW.ui.layer.setting.setting_setLNB.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_setLNB.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();