/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 자동 대기 모드
 */

(function() {
    KTW.ui.layer.setting.setting_autoStandbyMode  = function AutoStandbyMode(options) {
        KTW.ui.Layer.call(this, options);
        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var log = KTW.utils.Log;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var layerIdx;
        var data;

        var clock;
        var clockArea;

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame autoStayMode"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>자동 대기 모드</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function(options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show()");

            selectedIdx = focusIdx = readConfiguration();

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectedIdx + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectedIdx + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
            buttonFocusRefresh(focusIdx, 0);
            if(!options || !options.resume) {
                data = this.getParams();
                layerIdx = data.layerIdx;
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_STANDBY_MODE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

         //   clock.show(clockArea);
        };

        this.hide = function() {
            log.printDbg("hide");
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

        //    clock.hide();

        };

        this.controlKey = function(key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    selectedIdx = index;
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        saveConfiguration();
                        data.complete();
                        settingMenuManager.historyBackAndPopup();
                        return;
                    } else {
                        LayerManager.historyBack();
                    }
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>자동 대기 모드</div>" +
            "<div class='table_subTitle'>3시간 이상 리모컨 조작이 없을 경우 자동으로 대기 모드로 전환됩니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='top:20px;'>" +
            "<div class ='table_radio_btn_bg'></div>"+
            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>ON</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='top: 117px;'>" +
            "<div class ='table_radio_btn_bg'></div>"+
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>OFF</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>자동 대기 모드 [셋톱박스<br>전면 전원 OFF 상태]를<br>설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 저장된 설정 값을 읽어온다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");
            var autoStandbyMode = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_STANDBY_MODE);

            log.printDbg("autoStandbyMode ==   "+autoStandbyMode);
            return autoStandbyMode[0] === "ON" ? 0 : 1;
        }

        /**
         * 사용자가 설정한 값을 저장한다.
         */
        function saveConfiguration() {
            log.printDbg("saveConfiguration(), selectedIdx = " + selectedIdx);

            // 2017.04.10 dhlee
            // 자동대기모드 기능은 네비게이터에서 제공함에 따라 저장 위치가 변경됨
            KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.AUTO_STANDBY, selectedIdx === 0 ? "ON" : "OFF");

            //storageManager.ps.save(storageManager.KEY.AUTO_STANDBY, selectedIdx === 0 ? "true" : "false");
        }
    };
    KTW.ui.layer.setting.setting_autoStandbyMode.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_autoStandbyMode.constructor = KTW.ui.layer.setting.setting_autoStandbyMode;

    KTW.ui.layer.setting.setting_autoStandbyMode.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_autoStandbyMode.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();