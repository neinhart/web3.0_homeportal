/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 사운드
 * OTV와 OTS 형상이 다르다.
 * OTV: Dolby AC3 (5.1 채널) / Stereo (PCM) / 광출력 OFF 옵션 선택
 * OTS: Dolby AC3 (5.1 채널) / Stereo (PCM) 옵션 선택
 *
 * Dolby AC3 옵션 설정 시 팝업이 보여진다. UI 문서에는 저장하고 설정 안내 팝업 보여준다고 되어 있으나
 * 실제로는 팝업을 보여주고 팝업에서 확인 버튼을 선택하면 실제 저장이 이루어져야 한다.
 */

(function () {
    KTW.ui.layer.setting.setting_soundType  = function SoundType(options) {
        KTW.ui.Layer.call(this, options);


        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var lastFocusedIdx = 0;
        var isDolbySelect = false;

        // 초기 default 값 설정
        var selectedSoundFormat = 0; // 2017.02.24 dhlee 선택한 옵션 index
        var selectedHdmi = 2; // hdmi 출력단자 on / off 선택한 index
        var selectedSpdif = 5; // hdmi 출력단자 on / off 선택한 index

        var IS_SUPPORTED_OUTPUT_SET = false;

        var clock;
        var clockArea;

        var OPTIONS_LENGTH = {
            OTS : 2,
            OTV : 3,
            DEFALUT : 2
        };

        var NUMBER_OF_OPTIONS = OPTIONS_LENGTH.DEFALUT;
        var NUMBER_OF_RADIO_BUTTONS = 6;

        //var layerIdx;
        var data;

        var soundTypeSelectingErrorPopup = "soundTypeSelectingErrorPopup";

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame soundType"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>사운드</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );


         //   createClock();

            //cbCreate(true);

            initConfiguration();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show SoundType");

            readConfiguration();

            //data = this.getParams();
            //log.printDbg("thisMenuName:::::::::::"+data.name);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::"+data.name);

                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SOUND) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide SoundType");
        };


        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:

                    if(IS_SUPPORTED_OUTPUT_SET) {
                        if(menuIdx == 0) {
                            focusIdx = focusIdx -2;
                            if(focusIdx < 0) {
                                if(focusIdx == -1) {
                                    focusIdx = NUMBER_OF_RADIO_BUTTONS -2;
                                }else {
                                    focusIdx = NUMBER_OF_RADIO_BUTTONS -1;
                                }
                            }
                            buttonFocusRefresh(focusIdx,menuIdx);
                        }else {
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                        }
                    }else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    }

                    return true;
                case KEY_CODE.DOWN:
                    if(IS_SUPPORTED_OUTPUT_SET) {
                        if(menuIdx == 0) {
                            focusIdx = focusIdx +2;
                            if(focusIdx > NUMBER_OF_RADIO_BUTTONS-1) {
                                if(focusIdx == NUMBER_OF_RADIO_BUTTONS) {
                                    focusIdx = 0;
                                }else {
                                    focusIdx = 1;
                                }
                            }
                            buttonFocusRefresh(focusIdx,menuIdx);
                        }else {
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                        }
                    }else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        //TODO lastfocusedIDx 으로 이동.

                        if(lastFocusedIdx  == 0 || lastFocusedIdx == 1) {
                            // 포맷 설정 영역
                            focusIdx = selectedSoundFormat;
                        }else if(lastFocusedIdx == 2 || lastFocusedIdx == 3) {
                            focusIdx = selectedHdmi;
                        }else {
                            focusIdx = selectedSpdif;
                        }

                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        if(IS_SUPPORTED_OUTPUT_SET) {
                            if(focusIdx % 2 == 0) {
                                LayerManager.historyBack();
                            }else {
                                focusIdx --;
                                buttonFocusRefresh(focusIdx, 0);
                            }
                        }else {
                            LayerManager.historyBack();
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(menuIdx == 0) {
                        if(IS_SUPPORTED_OUTPUT_SET) {
                            lastFocusedIdx = focusIdx;
                            focusIdx ++;
                            if(focusIdx % 2 == 0 ) {
                                menuIdx = 1;

                                focusIdx = 0;
                                buttonFocusRefresh(focusIdx,menuIdx);
                            }else {

                                menuIdx = 0;
                                buttonFocusRefresh(focusIdx,menuIdx);
                            }
                        }else {
                            lastFocusedIdx = focusIdx;
                            menuIdx = 1;
                            focusIdx = 0;
                            buttonFocusRefresh(focusIdx,menuIdx);
                        }

                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {

            isDolbySelect = false;
            switch(menuIndex) {
                case 0:
                    if(index == 0 || index == 1) {
                        if(index == 1) {
                            isDolbySelect = true;
                        }
                        selectSoundFormat(index);

                        if(IS_SUPPORTED_OUTPUT_SET) {
                            // hdmi 출력 단자 설정 으로 이동
                            focusIdx = selectedHdmi;
                            buttonFocusRefresh(focusIdx, 0);
                        }else {
                            // 지원 안하면 바로 우측 버튼 영역으로 이동
                            lastFocusedIdx = index;
                            focusIdx = 0;
                            buttonFocusRefresh(0,1);
                        }

                    }else if( index == 2 || index == 3) {

                        selectHDMI(index);

                        // s/pdif 단자 설정으로 이동
                        focusIdx = selectedSpdif;
                        buttonFocusRefresh(focusIdx,0);

                    }else if(index == 4 || index == 5) {
                        // s/pdif 설정
                        selectSPDIF(index);

                        // 우측 버튼영역으로 이동
                        lastFocusedIdx = focusIdx;
                        focusIdx = 0;
                        buttonFocusRefresh(0,1);
                    }
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장

                        // 먼저 팝업을 띄워야 하는지 검사
                            // HDMI/ S/PDIF 설정 상태 확인
                        if(selectedHdmi == 3 && selectedSpdif == 5) {
                            // 팝업 띄운다.
                            var isCalled = false;
                            var popupData = {
                                arrMessage: [{
                                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                                    message: ["알림"],
                                    cssObj: {}
                                }],
                                arrButton: [{id: "ok", name: "확인"}],
                                cbAction: function (buttonid) {

                                    if (!isCalled) {
                                        isCalled = true;
                                        if(buttonid == "ok") {
                                            log.printDbg("ok");
                                            KTW.ui.LayerManager.deactivateLayer({
                                                id: soundTypeSelectingErrorPopup
                                            });
                                        }
                                    }
                                }
                            };

                            popupData.arrMessage.push({
                                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                                message: ["HDMI나 S/PDIF 중 최소 한 개 단자는", "ON 으로 설정되어야 합니다"] ,
                                cssObj: {}
                            });

                            KTW.ui.LayerManager.activateLayer({
                                obj: {
                                    id: soundTypeSelectingErrorPopup,
                                    type: KTW.ui.Layer.TYPE.POPUP,
                                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                                    view : KTW.ui.view.popup.BasicPopup,
                                    params: {
                                        data : popupData
                                    }
                                },
                                visible: true,
                                cbActivate: function () {}
                            });

                            return;
                        }

                        // AC3 설정 여부 확인
                        /*
                           [sw.nam] AC 설정이 되었을 때 3가지 타입으로 저장이 될 수 있다.

                           1. AC 설정이 되고 hdmi, s/pdif 출력 단자가 모두 on 인 경우
                              : "ac3" 으로 저장
                           2. AC 설정이 되고 hdmi 만 on 인 경우
                              : "ac3_hdmi" 로 저장
                           3. AC 설정이 되고 s/pdif 만 on 인경우
                              : "ac3_spdif" 로 저장

                           3가지 케이스 모두 3.1 dolby 설정 팝업을 띄운다.
                          */
                        if( ( selectedSoundFormat == 1 && selectedHdmi == 2 && selectedSpdif == 4 ) ||
                            ( selectedSoundFormat == 1 && selectedHdmi == 2 && selectedSpdif == 5 ) ||
                            ( selectedSoundFormat == 1 && selectedHdmi == 3 && selectedSpdif == 4 )) {
                            // 5.1 채널 설정 된 경우
                            // 팝업을띄운다.
                            LayerManager.activateLayer({
                                obj: {
                                    id: KTW.ui.Layer.ID.SETTING_SOUND_AUDIO_POPUP,
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        complete: function() {
                                            saveConfiguration();
                                            data.complete();
                                            settingMenuManager.historyBackAndPopup();
                                        }
                                    }
                                },
                                new: true,
                                visible: true
                            });
                            return ;
                        }else if(selectedSoundFormat == 0) {
                            // 2채널 설정 된 경우
                            saveConfiguration();
                            data.complete();
                            settingMenuManager.historyBackAndPopup();
                            return ;
                        }


                    } else {
                        // 취소 case
                        LayerManager.historyBack();
                    }
                    break;
            }
        }

        /**
         * 선택된 라디오 버튼으로 저장될 mode의 string 을 결정한다.
         * @param format
         * @param hdmi
         * @param spdif
         * @returns {*}
         */
        function setAudioModeValue(format,hdmi,spdif) {
            log.printDbg("setAudioModeValue "+format+" "+hdmi+" "+spdif);

            var value;

            if(IS_SUPPORTED_OUTPUT_SET) {
                if(format == 0) {
                    value = "uncompressed";
                }else if(format == 1) {
                    if(hdmi == 2 && spdif == 4) {
                        // 둘 다 ON
                        value = "ac3";
                    }else if(hdmi == 2 && spdif == 5) {
                        // hdmi 만
                        value = "ac3_hdmi";
                    }else if(hdmi == 3 && spdif == 4) {
                        // spdif 만
                        value = "ac3_spdif";
                    }
                }else {
                    // parameter 누락 혹은 에러가 있는경우
                    value = "uncompressed";
                }
            }else {
                // 출력단자 설정을 지원하지 않는 경우는 선택 2가지
                if(format == 0) {
                    value = "uncompressed";
                }else {
                    value = "ac3";
                }
            }

            log.printDbg("value == "+value);
            return value;
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") ").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function removeFocus() {
            log.printDbg("removeFocus()");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
        }

        function selectSoundFormat(index) {
            selectedSoundFormat = index;
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }
        function selectHDMI(index) {
            selectedHdmi = index;
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index%2 + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index%2 + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }
        function selectSPDIF(index) {
            selectedSpdif = index;
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(2) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(2) .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + index%2 + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + index%2 + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }

        function createComponent_new() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>사운드</div>" +
            "<div class='table_subTitle'>오디오 출력 포맷을 설정합니다<br>5.1채널을 지원하지 않는 오디오 기기 사용시 2채널로 설정해 주세요</div>" +
            "<div id='table_radio_area'>" +
                "<div class='table_radio_btn'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>2채널 (Stereo AAC)</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='left: 385px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>5.1 채널 (Dolby AC3)</div>" +
                "</div>" +
            "</div>" +
            "</div>" +

            "<div class='table_divisionLine'style='position: absolute; top:391px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div'style='position: absolute; top:393px; width: 1247px; height: 360px;'>" +
                "<div class='table_title' style='position: absolute; top: 45px; font-size: 33px;letter-spacing: -1.65px;'>출력 단자 설정</div>" +
                "<div class='table_subTitle' style='top: 83px;'>TV와 연결된 HDMI 혹은 S/PDIF 단자의 출력 여부를 설정할 수 있습니다</div>" +

                "<div style='position: absolute; top: 199px; font-size: 26px; line-height: 36px ;color:white; opacity: 0.4; letter-spacing: -1.3px;'>HDMI</div>"+
                "<div id='table_radio_area' style=' left: 129px; top: 177px;'>" +
                    "<div class='table_radio_btn' style='top:0px; width: 212px; height: 77px;'>" +
                        "<div class ='table_radio_btn_bg' style='width: inherit; height: inherit;'></div>"+
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>ON</div>" +
                    "</div>" +
                    "<div class='table_radio_btn' style='top:0px; left:212px; width: 212px; height: 77px;'>" +
                       "<div class ='table_radio_btn_bg' style='width: inherit; height: inherit;'></div>"+
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>OFF</div>" +
                    "</div>" +
                "</div>"+

                "<div style='position: absolute; top: 269px; font-size: 26px; line-height: 36px ;color:white; opacity: 0.4; letter-spacing: -1.3px;'>S/PDIF</div>"+
                "<div id='table_radio_area' style=' left: 129px; top: 247px;'>" +
                    "<div class='table_radio_btn' style='top:0px; width: 212px; height: 77px;'>" +
                        "<div class ='table_radio_btn_bg' style='width: inherit; height: inherit;'></div>"+
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>ON</div>" +
                    "</div>" +
                    "<div class='table_radio_btn' style='top:0px; left:212px; width: 212px; height: 77px;'>" +
                        "<div class ='table_radio_btn_bg' style='width: inherit; height: inherit;'></div>"+
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>OFF</div>" +
                    "</div>" +
                "</div>"+

            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>연결 케이블을 확인한 다음<br>사운드를 설정하세요</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>사운드</div>" +
            "<div class='table_subTitle'>오디오 출력 포맷을 설정합니다<br>5.1 지원하지 않는 오디오 기기 사용시 Stereo로 설정하시기 바랍니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' >" +
            "<div class ='table_radio_btn_bg'></div>"+
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>2채널 (Stereo AAC)</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='top: 117px;'>" +
            "<div class ='table_radio_btn_bg'></div>"+
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>5.1 채널 (Dolby AC3)</div>" +
            "</div>" +
            //"<div class='table_radio_btn' style='top:214px;'>" +
            //"<div class ='table_radio_btn_bg'></div>"+
            //"<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
            //"<div class='table_radio_text'>광출력 OFF</div>" +
            //"</div>" +
            "</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>연결 케이블을 확인 후<br>사운드를 설정하세요</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }


        /**
         * spdif / hdmi 출력설정 지원여부 확인
         */
        function initConfiguration() {
            log.printDbg("initConfiguration()");
            var supportedModes = hwAdapter.getSupportedDigitalAudioModes();

            // sw.nam
            // 신규FW 에서는 리턴되는 modes 에 "ac_hdmi","ac3_spdif" 값을 포함하므로 스트링 검사를 해서 값이 있으면 flag를 true 로 셋팅한다.
            if(supportedModes !== null && supportedModes !== undefined && supportedModes !== "null" && supportedModes !== "undefined") {
                IS_SUPPORTED_OUTPUT_SET = false;
                 for(var i = 0 ; i < supportedModes.length; i++) {
                     log.printDbg("suppertedmode__ = "+supportedModes[i]);
                     if(supportedModes[i].indexOf("ac3_hdmi") > -1 ||supportedModes[i].indexOf("ac3_spdif") > -1) {
                         log.printDbg("hit");
                         IS_SUPPORTED_OUTPUT_SET = true;
                     }
                 }
            }else {
                log.printDbg("supportedMode is not valid...");
            }

            log.printDbg("isSupportedOutoutSet  = "+IS_SUPPORTED_OUTPUT_SET);

            if(IS_SUPPORTED_OUTPUT_SET) {
                // 아래 영역 dim 처리 혹은?

                createComponent_new();
            }else {
                createComponent();
            }

        }

        /**
         * STB에 설정된 값을 읽어온다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var audioMode = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SOUND);

            log.printDbg("audioMode = "+audioMode);


            // 2018.02.21 sw.nam
            // 사운드 설정 시 출력 단자 까지 설정 할 수 있도록 기능 확장

            // ac3, ac3_hdmi, ac3_spdif, uncompressed, OFF
            if(audioMode != null && audioMode != "undefined" && audioMode !== undefined && audioMode !=="null") {

                if(audioMode == "uncompressed") {
                    //2채널
                    selectedSoundFormat = 0;
                }else if(audioMode == "ac3") {
                    selectedSoundFormat = 1;
                    selectedHdmi = 2;
                    selectedSpdif = 4;
                }else if(audioMode == "ac3_hdmi") {
                    selectedSoundFormat = 1;
                    selectedHdmi = 2;
                    selectedSpdif = 5;
                }else if(audioMode == "ac3_spdif") {
                    selectedSoundFormat = 1;
                    selectedHdmi = 3;
                    selectedSpdif = 4;
                }else {
                    selectedSoundFormat = 0;
                    selectedHdmi = 2;
                    selectedSpdif = 5;
                }

            }else {
                // 오디오 string 이 없는 경우
                log.printDbg(" mode doesn't exist");
                selectedSoundFormat = 0;
                selectedHdmi = 2;
                selectedSpdif = 5;
            }

            // 라디오, 포커스 셋팅
            log.printDbg("index "+selectedSoundFormat+" "+selectedHdmi+" "+selectedSpdif);
            selectSoundFormat(selectedSoundFormat);
            selectHDMI(selectedHdmi);
            selectSPDIF(selectedSpdif);
            buttonFocusRefresh(selectedSoundFormat,0);

        }

        /**
         * API 를 통해 사용자가 선택한 설정 값을 저장한다.
         */
        function saveConfiguration() {
            log.printDbg("saveConfiguration()");

            var mode = setAudioModeValue(selectedSoundFormat,selectedHdmi,selectedSpdif);

            hwAdapter.setAVOutputDigitalAudioMode(mode);
        }
    };

    KTW.ui.layer.setting.setting_soundType.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_soundType.constructor = KTW.ui.layer.setting.setting_soundType;

    KTW.ui.layer.setting.setting_soundType.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_soundType.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();