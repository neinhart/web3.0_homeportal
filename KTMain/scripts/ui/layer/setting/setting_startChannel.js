/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템설정 > 시작 채널
 */

(function () {

    KTW.ui.layer.setting.setting_startChannel  = function StartChannel(options) {
        KTW.ui.Layer.call(this, options);

        var CHANNEL_PER_PAGE = 6;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;

        var SettingDataAdaptor = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;

        var styleSheet;

        var focusIdx = 0;
        var menuType = 0;
        var selectIdx = 0;
        var channel_data =[];

        var is_lcw_supported;
        var INSTANCE = this;
        var div;

        var clock;
        var clockArea;

        var scrollArea;
        var scroll;
        var colorKeyArea;
        var data;

        var totalPage;
        var curPage;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame startChannel"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>시작 채널</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();

       //     createClock();

            createScrollArea();

            cbCreate(true);
        };

        this.show = function (options) {

            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show StartChannel");


            initChannelList();

            menuType = 0;
            focusIdx = selectIdx;
            log.printDbg("starting channel List ::" + channel_data);
            log.printDbg("focusIdx" + focusIdx);

            makeChannelElements();
            setNumberOfChannelListPage();
            setFocus();

            if (!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_START_CHANNEL) {
                                return true;
                            }

                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    }
                    else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


            scroll.show({
                maxPage: totalPage,
                curPage: curPage
            });
            if(totalPage > 1) {
                colorKeyArea.css({visibility: "inherit"});
            }else {
                colorKeyArea.css({visibility: "hidden"});
            }

        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    if(menuType == 0) {
                        curPage = Math.floor(focusIdx / CHANNEL_PER_PAGE);
                        focusIdx = KTW.utils.util.getIndex(focusIdx, -1, channel_data.length);
                        if (Math.floor(focusIdx / CHANNEL_PER_PAGE) != curPage) {
                            makeChannelElements();
                            setNumberOfChannelListPage();
                            scroll.setCurrentPage(curPage);
                        }
                    }
                    else if (menuType == 1) {
                        focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2);
                    }

                    setFocus();
                    return true;
                case KEY_CODE.DOWN:
                    if(menuType == 0) {
                        curPage = Math.floor(focusIdx / CHANNEL_PER_PAGE);
                        focusIdx = KTW.utils.util.getIndex(focusIdx, 1, channel_data.length);
                        if (Math.floor(focusIdx / CHANNEL_PER_PAGE) != curPage) {
                            makeChannelElements();
                            setNumberOfChannelListPage();
                            scroll.setCurrentPage(curPage);
                        }
                    }
                    else if (menuType == 1) {
                        focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2);
                    }

                    curPage = Math.floor(focusIdx / CHANNEL_PER_PAGE);
                    if (Math.floor(focusIdx / CHANNEL_PER_PAGE) != curPage) {
                        makeChannelElements();
                        setNumberOfChannelListPage();
                        scroll.setCurrentPage(curPage);
                    }
                    setFocus();
                    return true;
                case KEY_CODE.RED:
                    // 현재 포커스된 인덱스가 현재 페이지의 가장 상위 인덱스면 페이지 이동, 아니면 상위로 가져온다.
                    if(menuType == 0) {
                        var tmpPageIdx = curPage;
                        if(focusIdx == curPage * CHANNEL_PER_PAGE) {
                            // 현재 포커스된 이미지가 현재 페이지의 가장 상위 인덱스인 경우 ex) 0, 6, 12 ...
                            if(tmpPageIdx-1 < 0) {
                                // 이미 첫번째 페이지인 경우.
                                tmpPageIdx = totalPage-1;
                            }else {
                                tmpPageIdx--;
                            }
                        }
                        // 이동한 페이지의 가장 첫번째 인덱스로 포커스
                        focusIdx = tmpPageIdx * CHANNEL_PER_PAGE;

                        //curPage = Math.floor(focusIdx / CHANNEL_PER_PAGE);
                        if (tmpPageIdx != curPage) {
                            makeChannelElements();
                            setNumberOfChannelListPage();
                            scroll.setCurrentPage(curPage);
                        }
                    }else {
                        menuType =0;
                        focusIdx = curPage * CHANNEL_PER_PAGE;
                    }
                    log.printDbg("focusIdx == "+focusIdx);
                    setFocus();
                    return true;
                case KEY_CODE.BLUE:

                    if(menuType == 0) {
                        var tmpPageIdx = curPage;
                        if(tmpPageIdx == totalPage -1) {
                            //현재 인덱스가 마지막 페이지에 있는 경우
                            if(focusIdx == channel_data.length-1) {
                                // 마지막 인덱스를 가리키고 있는 경우
                                // 첫 페이지의 마지막 인덱스로 이동
                                if(totalPage == 1) {
                                    // 1페이지만 존재하는 경우
                                    focusIdx = channel_data.length-1;
                                }else {
                                    focusIdx = CHANNEL_PER_PAGE -1;
                                }

                                tmpPageIdx = 0;
                            }else {
                                // 리스트 가장 마지막 인덱스로 이동
                                focusIdx = channel_data.length-1;
                            }
                        }else {
                            if(focusIdx == (tmpPageIdx * CHANNEL_PER_PAGE) + (CHANNEL_PER_PAGE) -1) {
                                // 현재 페이지의 마지막 인덱스를 가리키고 있는 경우, 다음 페이지의 마지막 인덱스로이동
                                tmpPageIdx ++;
                                if(tmpPageIdx = totalPage -1){
                                    // 이동된 임시 페이지 인덱스가 마지막 페이지인 경우 처리
                                    focusIdx = channel_data.length -1;
                                }
                            }else {
                                focusIdx = (tmpPageIdx * CHANNEL_PER_PAGE) + (CHANNEL_PER_PAGE -1);
                            }
                        }
                        if (tmpPageIdx != curPage) {
                            makeChannelElements();
                            setNumberOfChannelListPage();
                            scroll.setCurrentPage(curPage);
                        }
                    }else {
                        if(curPage == totalPage -1) {
                            //마지막 페이지인 경우에는 리스트의 마지막 인덱스로 포커스
                            focusIdx = channel_data.length -1;
                        }else {
                            // 현재 페이지의 마지막 인덱스로 이동
                            focusIdx = (curPage * CHANNEL_PER_PAGE) + (CHANNEL_PER_PAGE -1);
                        }
                        menuType =0;
                    }
                    log.printDbg("focusIdx == "+focusIdx);
                    setFocus();
                    return true;

                case KEY_CODE.LEFT:
                    if (menuType == 1) {
                        focusIdx = selectIdx;
                        menuType = 0;
                        makeChannelElements();
                        setNumberOfChannelListPage();
                        setFocus();
                    }
                    else {
                        LayerManager.historyBack();
                    }

                    return true;
                case KEY_CODE.RIGHT:
                    if(menuType == 0) {
                        focusIdx = 0;
                        menuType = 1;
                        setFocus();
                    }
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEventHandler();
                    return true;
                default:
                    return false;
            }

        };

        function setFocus () {
            var arrRadio = INSTANCE.div.find("#contents #setting_start_channel_area #start_channel_radio_area").children();
            var arrButton = INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area").children();

            arrRadio.removeClass("focus");
            arrButton.removeClass("focus");

            arrRadio.find("img").attr("src", "images/rdo_btn_d.png");

            var selectRadio = $(arrRadio[selectIdx % CHANNEL_PER_PAGE]);
            if (selectIdx == selectRadio.attr("index")) {
                selectRadio.find("img").attr("src", "images/rdo_btn_select_d.png");
            }

            if (menuType === 0) {
                $(arrRadio[focusIdx % CHANNEL_PER_PAGE]).addClass("focus");

                var focusRadio = $(arrRadio[focusIdx % CHANNEL_PER_PAGE]);
                if (selectIdx === focusIdx) {
                    focusRadio.find("img").attr("src", "images/rdo_btn_select_f.png");
                }
                else {
                    focusRadio.find("img").attr("src", "images/rdo_btn_f.png");
                }
            }
            else {
                $(arrButton[focusIdx]).addClass("focus");
            }
        }

        function setNumberOfChannelListPage () {
            INSTANCE.div.find("#settingTable .table_radio_btn").removeClass("hide");

            totalPage = Math.ceil(channel_data.length / CHANNEL_PER_PAGE);
            curPage = Math.floor(focusIdx / CHANNEL_PER_PAGE);
        }

        function enterKeyEventHandler () {
            log.printDbg("enterKeyEventHandler()");

            if (menuType == 0) {
                selectIdx = focusIdx;
                focusIdx = 0;
                menuType = 1;
                setFocus();
            }
            else {
                if (focusIdx == 0) {
                    //저장
                    if (selectIdx === 0) {
                        basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.DEFAULT_CHANNEL, "");
                    }
                    else {
                        if (is_lcw_supported === "true" && selectIdx === 1) {
                            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.DEFAULT_CHANNEL, "LCW");
                        }
                        else {
                            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.DEFAULT_CHANNEL, channel_data[selectIdx].linkInfo);
                        }
                    }
                    data.complete();
                    settingMenuManager.historyBackAndPopup();
                    return;
                }
                LayerManager.historyBack();
            }
        }

        function createComponent() {
            log.printDbg("createComponent");

            var contentsDiv = INSTANCE.div.find("#contents");

            var startChannelArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "setting_start_channel_area",
                    css: {
                        position: "absolute", left: 133, top: 123, width: 1168, height: 825,
                        "border-collapse": "collapse", "border-top": "5px solid rgba(178, 177, 177, 0.25)",
                        "border-bottom": "5px solid rgba(178, 177, 177, 0.25)"
                    }
                },
                parent: contentsDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 49, width: 896, height: 60,
                        "font-size": 48, "font-family": "RixHead M", color: "white", opacity: 0.7, "letter-spacing": -2.4
                    }
                },
                text: "시작 채널",
                parent: startChannelArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 120, width: 886, height: 90,
                        "font-size": 26, "font-family": "RixHead L", color: "white", opacity: 0.4, "letter-spacing": -1.3,
                        "line-height": "35px", "white-space": "pre"
                    }
                },
                text: "원하시는 채널을 시작 채널로 설정해 주세요\nMY 커뮤니티 가입 시 CUG 채널로 설정 가능합니다",
                parent: startChannelArea
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "start_channel_radio_area",
                    css: {
                        position: "absolute", left: 0, top: 218, overflow: "visible"
                    }
                },
                parent: startChannelArea
            });

/*            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "startChannelList_scroll_BG",
                    css: {
                        position: "absolute", left: 882, top: 268, width: 3, height: 465,
                        "background-color": "rgba(255, 255, 255, 0.15)", visibility: "hidden"
                    }
                },
                parent: startChannelArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "startChannelList_scroll_focus",
                    css: {
                        position: "absolute", left: 881, top: 268, width: 5, height: 211, "background-color": "#AAAAAA"
                    }
                },
                parent: startChannelArea
            });*/

            var rightMenuArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "rightMenu_area",
                    css: {
                        position: "absolute", left: 1433, top: 87, overflow: "visible"
                    }
                },
                parent: contentsDiv
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/set_bg_btn_t.png",
                    css: {
                        position: "absolute", left: 0, top: 0
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/set_bg_btn.png",
                    css: {
                        position: "absolute", left: -1, top: 65, width: 420, height: 775
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/set_bg_btn_b.png",
                    css: {
                        position: "absolute", left: 0, top: 840
                    }
                },
                parent: rightMenuArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    src: "images/set_bg_btn_b.png",
                    css: {
                        position: "absolute", left: 39, top: 79, width: 274, height: 90,
                        "font-size": 26, "font-family": "RixHead L", color: "white", opacity: 0.4, "letter-spacing": -1.3,
                        "line-height": "35px", "white-space": "pre"
                    }
                },
                text: "셋톱박스를 켤 때 나오는\n시작 채널을 설정합니다",
                parent: rightMenuArea
            });

            var rightMenuButtonArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "rightMenu_btn_area",
                    css: {
                        position: "absolute", left: 39, top: 688, overflow: "visible"
                    }
                },
                parent: rightMenuArea
            });
            var saveButton = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66
                    }
                },
                parent: rightMenuButtonArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_btn_line",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66
                    }
                },
                parent: saveButton
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 20, width: 282, height: 40,
                        "font-size": 30, "letter-spacing": -1.5, "text-align": "center"
                    }
                },
                text: "저장",
                parent: saveButton
            });

            var cancelButton = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_btn",
                    css: {
                        position: "absolute", left: 0, top: 79, width: 282, height: 66
                    }
                },
                parent: rightMenuButtonArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_btn_line",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66
                    }
                },
                parent: cancelButton
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 20, width: 282, height: 40,
                        "font-size": 30, "letter-spacing": -1.5, "text-align": "center"
                    }
                },
                text: "취소",
                parent: cancelButton
            });
        }
        function createScrollArea() {
            log.printDbg("createScrollArea()");

            //scrollArea
            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "scroll",
                        css: {
                        position: "absolute", left: 1371, top: 141, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 723,
                forceHeight: true
            });

            colorKeyArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "colorKey",
                    css: {
                        position: "absolute", left: 0, top: 0,width: 1920, height: 1080, visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: 1343, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: 1371, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "keyText",
                    css: {
                        position: "absolute", left: 1343, top: 939, width: 60, height: 25, "letter-spacing" : -1.1,
                        "font-size" : 22, color: "rgba(255,255,255,0.77)", "font-family" : "RixHead L"
                    }
                },
                text: "페이지",
                parent: colorKeyArea
            });

        }
        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * LCW 지원여부, CUG data 리스트를 확인하는 로직.
         */

        function initChannelList () {
            log.printDbg("initChannelList()");

            // data 긁어와서 리스트 업데이트..
            var value ;
            var cug_data;
            var settingData = SettingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_START_CHANNEL,"specific");

            channel_data = [];
            cug_data = settingData[0];
            value = settingData[1];
            is_lcw_supported = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.LCW_BOOTING_SUPPORT);
            selectIdx = 0;

            channel_data.push("가이드 채널");
            if (is_lcw_supported === "true") {
                channel_data.push("이전 시청 채널");
            }

            if (cug_data && cug_data.cugList && cug_data.cugList.length) {
                for (var i = 0; i < cug_data.cugList.length; i++) {
                    channel_data.push(cug_data.cugList[i]);
                }
            }

            if (value === null || value === undefined || value === "") {
                selectIdx = 0;
            }
            else {
                if (is_lcw_supported === "true" && value === "LCW") {
                    selectIdx = 1;
                }
                else {
                    for (var i = 0; i < channel_data.length; i++) {
                        var item = channel_data[i];

                        if (item && item.linkInfo === value) {
                            selectIdx = i;
                            break;
                        }
                    }
                }
            }

            log.printDbg("channel_data : " + channel_data);
            log.printDbg("channel_data.length : " + channel_data.length);
            log.printDbg("selectIdx : " + selectIdx);
            log.printDbg("select channel_data : " + (channel_data[selectIdx].linkInfo ? channel_data[selectIdx].linkInfo : channel_data[selectIdx]));
        }

        function makeChannelElements () {
            log.printDbg("makeChannelElements()");

            var radioArea = INSTANCE.div.find("#start_channel_radio_area");
            radioArea.children().remove();

            var startIdx = Math.floor(focusIdx / CHANNEL_PER_PAGE) * CHANNEL_PER_PAGE;
            var top = 0;

            for (var i = 0; i < CHANNEL_PER_PAGE; i++) {
                if (startIdx + i < channel_data.length) {
                    var item = channel_data[startIdx + i];

                    var guideButton = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "start_channel_radio",
                            index: startIdx + i,
                            css: {
                                position: "absolute", left: 0, top: top, width: 499, height: 77
                            }
                        },
                        parent: radioArea
                    });
                    util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "table_radio_btn_bg",
                            css: {
                                position: "absolute", width: 499, height: 77
                            }
                        },
                        parent: guideButton
                    });
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            src: "images/rdo_btn_d.png",
                            css: {
                                position: "absolute", left: 18, top: 20
                            }
                        },
                        parent: guideButton
                    });
                    var channelName = item.cugName ? item.cugName : item;
                    util.makeElement({
                        tag: "<span />",
                        attrs: {
                            class: "table_radio_text",
                            css: {
                                position: "absolute", left: 74, "letter-spacing": -1.45
                            }
                        },
                        text: channelName,
                        parent: guideButton
                    });

                    top += 97;
                }
            }
        }
    };


    KTW.ui.layer.setting.setting_startChannel.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_startChannel.constructor = KTW.ui.layer.setting.setting_startChannel;
    KTW.ui.layer.setting.setting_startChannel.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };
    KTW.ui.layer.setting.setting_startChannel.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();