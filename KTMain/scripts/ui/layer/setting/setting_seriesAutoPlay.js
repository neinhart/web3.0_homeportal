/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 채널/VOD 설정 > 시리즈 몰아보기
 */

(function () {
    KTW.ui.layer.setting.setting_seriesAutoPlay  = function SeriesAutoPlay(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;
        var StorageManager = KTW.managers.StorageManager;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuType = 0;
        var selectIdx = 0;

        var clock;
        var clockArea;

        var data;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame seriesAutoPlay"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>시리즈 몰아보기</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            _readConfiguration();
            focusIdx = selectIdx;
            menuType = 0;
            buttonFocusRefresh(focusIdx, menuType);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SERIES_AUTO_PLAY) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

       //     clock.hide();
        };

        this.controlKey = function (key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2), menuType);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2), menuType);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuType == 0) {
                        LayerManager.historyBack();
                        return true;
                    }
                    if (menuType == 1) {
                        focusIdx = selectIdx;
                        menuType = 0;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.RIGHT:
                    focusIdx = 0;
                    menuType = 1;
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEventHandler(focusIdx, menuType);
                    return true;
                default:
                    return false;
            }
        };

        function enterKeyEventHandler(index, _menuType) {
            INSTANCE.div.find("#settingTable .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_radio_btn img").attr("src","images/rdo_btn_d.png");
            INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            if (_menuType == 0) {
                selectIdx = index;
                INSTANCE.div.find("#settingTable .table_radio_btn:eq(" + selectIdx + ") img").attr("src","images/rdo_btn_select_d.png");
                focusIdx = 0;
                menuType = 1;
                buttonFocusRefresh(focusIdx, menuType);
            } else {
                if (focusIdx == 0) {
                    _saveConfiguration();
                    data.complete();
                    settingMenuManager.historyBackAndPopup();
                } else {
                    LayerManager.historyBack();
                }
            }
        }

        function buttonFocusRefresh(index, _menuType) {
            INSTANCE.div.find("#settingTable .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_radio_btn img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#settingTable .table_radio_btn:eq(" + selectIdx + ") img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            if (_menuType == 0) {
                INSTANCE.div.find("#settingTable .table_radio_btn:eq(" + index + ")").addClass("focus");
                INSTANCE.div.find("#settingTable .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == selectIdx? "rdo_btn_select_f.png":"rdo_btn_f.png"));
            } else {
                INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>시리즈 몰아보기</div>" +
                "<div class='table_subTitle'>ON 설정 시 모든 시리즈 VOD의 다음 회차가 팝업 없이 자동으로 재생됩니다<br>OFF 설정 시 다음 회차 이어보기 팝업이 노출됩니다 (일부 장르 제외)</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='top:20px;'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>ON</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='top:117px;'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>OFF</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>기능 설정 시 시리즈 VOD를<br>자동으로 몰아볼 수 있습니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }
        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }


        function _readConfiguration() {
            log.printDbg("readConfiguration()");
            var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SERIES_AUTO_PLAY);
            if(settingData !== undefined && settingData !== null && settingData.length>0 && settingData[0] === "1") {
                log.printDbg("readConfiguration() settingData : " + settingData);
                selectIdx = 0;
            }else if(settingData !== null && settingData.length>0 && settingData[0] === "0") {
                log.printDbg("readConfiguration() settingData : " + settingData);
                selectIdx = 1;
            }else {
                selectIdx = 0;
            }
        }

        function _saveConfiguration() {
            StorageManager.ps.save(StorageManager.KEY.SERIES_AUTO_PLAY, selectIdx === 0 ? "1" : "0");

        }
    };
    KTW.ui.layer.setting.setting_seriesAutoPlay.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_seriesAutoPlay.constructor = KTW.ui.layer.setting.setting_seriesAutoPlay;

    KTW.ui.layer.setting.setting_seriesAutoPlay.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_seriesAutoPlay.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();