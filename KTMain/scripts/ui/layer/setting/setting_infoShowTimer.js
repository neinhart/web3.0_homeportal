/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 채널/VOD 설정 > 채널 미니가이드 표시시간 설정
 */

(function () {
    KTW.ui.layer.setting.setting_infoShowTimer  = function InfoShowTimer(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var storageManager = KTW.managers.StorageManager;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;

        var displayTime = 0;

        var data;

        var clock;
        var clockArea;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame infoShowTimer"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>채널 미니가이드 표시시간</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
       //     createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            _readConfiguration();

            if (displayTime === 0) {
                focusIdx = 0;
            } else if (displayTime === 3000) {
                focusIdx = 1;
            } else if (displayTime === 5000) {
                focusIdx = 2;
            } else if (displayTime === 10000) {
                focusIdx = 3;
            } else if (displayTime === 15000) {
                focusIdx = 4;
            }

            menuIdx = 0;

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + focusIdx + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + focusIdx + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
            selectedIdx = focusIdx;
            buttonFocusRefresh(focusIdx, menuIdx);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CHANNEL_GUIDE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

         //   clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

         //   clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, (menuIdx == 0 ? 5 : 2)), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, (menuIdx == 0 ? 5 : 2)), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 0) {
                        LayerManager.historyBack();
                        return true;
                    }
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    selectedIdx = index;

                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx === 0) {
                        _saveConfiguration();
                        data.complete();
                        settingMenuManager.historyBackAndPopup();
                    } else {
                        LayerManager.historyBack();
                    }

                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>채널 미니가이드 표시시간</div>" +
                "<div class='table_subTitle'>채널 변경 시 화면 하단에 표시되는 채널/프로그램 정보를 설정된 시간 동안 유지합니다</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>0초</div>" +
                "</div>" +
                "<div class='table_radio_btn'style ='top: 97px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>3초</div>" +
                "</div>" +
                "<div class='table_radio_btn'style ='top: 194px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>5초</div>" +
                "</div>" +
                "<div class='table_radio_btn'style ='top: 291px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>10초</div>" +
                "</div>" +
                "<div class='table_radio_btn'style ='top: 388px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>15초</div>" +
                "</div>" +
                "</div>" +

                // 2018.01.22 sw.nam 이미지 영역 추가 (고도화 gui 변경 내역)
                "<img class= 'table_img_area' src='images/setting/13_set_ex01.png' style ='position: absolute; left: 573px; top: 284px; width: 574px; height: 326px; '></div>"+

                "</div>" +
                "</div>");



            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>채널 정보의 화면<br>노출시간을 설정합니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 사용자가 선택한 설정 값을 저장한다.
         */
        function _saveConfiguration() {
            log.printDbg("saveConfiguration(), selectedIdx = " + selectedIdx);

            var value;

            if (selectedIdx === 0) {
                value = "0";
            } else if (selectedIdx === 1) {
                value = "3000";
            } else if (selectedIdx === 2) {
                value = "5000";
            } else if (selectedIdx === 3) {
                value = "10000";
            } else if (selectedIdx === 4) {
                value = "15000";
            } else {
                // 그럴 일은 없겠지만, 혹시나 그 외의 값인 경우 default 값 3초로 set
                value = "3000";
            }

            try {
                KTW.managers.StorageManager.ps.save(storageManager.KEY.MINIGUIDE_DISPLAY_TIME, value);
            } catch (error) {
                log.printErr("saveConfiguration(), error = " + error.message);
                KTW.managers.StorageManager.ps.save(storageManager.KEY.MINIGUIDE_DISPLAY_TIME, "3000");
            }
        }

        /**
         * 설정된 값을 읽어온다.
         */
        function _readConfiguration() {
            var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CHANNEL_GUIDE);

            if(settingData !== undefined && settingData !== null && settingData.length === 1) {
                displayTime = Number(settingData[0]);
            }
        }
    };
    KTW.ui.layer.setting.setting_infoShowTimer.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_infoShowTimer.constructor = KTW.ui.layer.setting.setting_infoShowTimer;

    KTW.ui.layer.setting.setting_infoShowTimer.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_infoShowTimer.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();