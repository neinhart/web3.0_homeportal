

/**
 * <code>setting_audioOutput</code>
 *
 * 설정 > 시스템설정 > 오디오 출력 설정 ( Only for GIGA Genie )
 * @author sw.nam
 * @since 2017-03-20
 */

"use strict";


(function () {
    KTW.ui.layer.setting.setting_audioOutput  = function AutioOutput(options) {
        KTW.ui.Layer.call(this, options);


        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var settingDataAdaptor = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;
        var androidAdapter = KTW.oipf.adapters.AndroidAdapter;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;

        var OPTIONS_LENGTH = {
            OTS : 2,
            OTV : 3,
            DEFAULT : 2
        };

        var NUMBER_OF_OPTIONS = OPTIONS_LENGTH.DEFAULT;

        var data;

        var clock;
        var clockArea;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame genieAudioOutput"}); // otv/ots 사운드 설정과 ui 가 달라져 css 분리 해둠  (soundType.css) 참조

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });


            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>오디오 출력 기기</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );
            createComponent();
        //    createClock();

            cbCreate(true);
            initConfiguration();

        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show AudioOutput");


            readConfiguration();
            focusIdx =  selectedIdx;

            selectAudioOutput(focusIdx);
            buttonFocusRefresh(focusIdx, 0);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_GIGA_GINI_AUDIO_OUTPUT) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

          //  clock.show(clockArea);

        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide AudioOutput");

        //    clock.hide();

        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            var value = "";
            switch(menuIndex) {
                case 0:

                    selectedIdx = index;
                    selectAudioOutput(selectedIdx);
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장
                        if (selectedIdx == 1) {
                            value = "speaker";
                            LayerManager.activateLayer({
                                obj: {
                                    id: KTW.ui.Layer.ID.SETTING_SOUND_AUDIO_POPUP,
                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.POPUP,
                                    linkage: true,
                                    params: {
                                        requester: "GIGAGENIE",
                                        complete: function() {
                                            saveConfiguration(value);
                                            settingMenuManager.historyBackAndPopup();
                                            return ;
                                        }
                                    }
                                },
                                new: true,
                                visible: true
                            });
                            return;
                        } else {
                            value= "hdmi";
                        }

                        // 팝업
                        saveConfiguration(value);
                        settingMenuManager.historyBackAndPopup();
                        return ;

                    } else {
                        // 취소 case

                    }
                    LayerManager.historyBack();
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function selectAudioOutput(index) {
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>오디오 출력 기기</div>" +
            "<div class='table_subTitle'>올레 tv 오디오의 출력 기기를 설정할 수 있습니다<br>기가지니 스피커로 듣기를 선택하면 올레 tv 영상은 TV화면으로,오디오는 기가지니 스피커로 출력됩니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='top:20px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>TV로 듣기(기본 설정)</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='top: 117px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>기가지니 스피커로 듣기</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>오디오와 화면 싱크가<br>맞지 않을 경우 TV로 듣기<br>(기본 설정)로 설정하세요</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function initConfiguration() {
            log.printDbg("initConfiguration()");


            if(KTW.CONSTANT.IS_OTS) {
                // 광출력 OFF div visibility 제거
                NUMBER_OF_OPTIONS = OPTIONS_LENGTH.OTS;
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").eq(2).css({visibility: "hidden"});
            } else {
                NUMBER_OF_OPTIONS = OPTIONS_LENGTH.OTV;
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").eq(2).css({visibility: "inherit"});
            }

        }

        /**
         * STB에 설정된 값을 읽어온다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var audioMode = settingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_GIGA_GINI_AUDIO_OUTPUT);
            log.printDbg("audio mode = "+JSON.stringify(audioMode));
            if (audioMode == "hdmi") { // 기본설정
                selectedIdx = 0;
            } else if (audioMode == "speaker") { // 기가지니 스피커
                selectedIdx = 1;
            } else {
                log.printDbg("didn't hit()...=="+audioMode);
                selectedIdx = 0;
            }
        }

        /**
         * API 를 통해 사용자가 선택한 설정 값을 저장한다.
         *
         * @param mode
         */
        function saveConfiguration(mode) {
            log.printDbg("saveConfiguration()");
            androidAdapter.setAudioOutputDevice(mode);
            data.complete();
        }


    };

    KTW.ui.layer.setting.setting_audioOutput.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_audioOutput.constructor = KTW.ui.layer.setting.setting_audioOutput;

    KTW.ui.layer.setting.setting_audioOutput.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_audioOutput.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();