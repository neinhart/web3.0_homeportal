/**
 * Created by Yun on 2016-11-22.
 *
 * 설정 > 자녀안심 설정 > 시청연령 제한 설정
 */

(function () {

    KTW.ui.layer.setting.setting_parentalRating  = function ParentalRating (options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var casAdapter = KTW.oipf.AdapterHandler.casAdapter;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;

        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;
        var focusXIdx = 0;
        var focusYIdx = 0;
        var lastFocusedYIdx;
        var focusButtonIdx = 0;
        var isRightFocus = false;
        //var toastPopup = null;
        var selectedRatingIdx = 0;
        var selectedAdultMenuIdx = 0;
        var prevAdultMenuIdx = 0;

        var prevPR;
        var parentalRating;
        var adultMenuDisplay;

        var clock;
        var clockArea;

        var data;

        var PR = {
            PR_NONE: 0, // 제한 없음
            PR_19: 1,
            PR_15: 2,
            PR_12: 3,
            PR_7: 4
        };

        var PR_NONE = 0;
        var PR_19 = 19;
        var PR_15 = 15;
        var PR_12 = 12;
        var PR_7 = 7;

        var adultMenuSubTitle = [["메뉴 숨김을 선택하면","성인 전용 메뉴(19+)가 표시되지 않습니다"]
            , ["메뉴 숨김을 선택하면","성인 전용 메뉴(19+)가 표시되지 않습니다"]];

        this.init = function (cbCreate) {
            log.printDbg("init()");
            this.div.attr({class: "arrange_frame"});
            //toastPopup = new SettingToastPopup();

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>시청연령 제한</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );
            createComponent();
        //    createClock();
            cbCreate(true);
        };

        this.showView = function (options) {
            log.printDbg("showView()");

            readConfiguration();

            $("head").append(styleSheet);

            isRightFocus = false;
            focusXIdx = selectedRatingIdx;
            focusYIdx = 0;
            focusButtonIdx = 0;

            log.printDbg("show() focusXIdx : " + focusXIdx + " , focusYIdx : " + focusYIdx + " , isRightFocus : " + isRightFocus);

            setFocus();

            if (!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::"+data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_PARENTAL_RATING) {
                                return true;
                            }
                        }
                    })[0];

                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();

                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    }
                    else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }
                }
                else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


        //    clock.show(clockArea);
        };

        this.hideView = function (options) {
            log.printDbg("hideView() options : " + (options !== undefined ? JSON.stringify(options) : ""));

            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
        //    clock.hide();

            if (!options || !options.pause) {
                KTW.managers.auth.AuthManager.initPW();
                KTW.managers.auth.AuthManager .resetPWCheckCount();
            }
        };

        this.controlKey = function (key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    if (isRightFocus) {
                        focusButtonIdx = --focusButtonIdx >= 0 ? focusButtonIdx : 1;
                        setButtonFocus();
                    }
                    else {
                        if (focusYIdx === 0) {
                            focusXIdx = selectedAdultMenuIdx;
                        }
                        else {
                            focusXIdx = selectedRatingIdx;
                        }

                        focusYIdx = --focusYIdx >= 0 ? focusYIdx : 1;

                        setRadioFocus();
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (isRightFocus) {
                        focusButtonIdx = ++focusButtonIdx < 2 ? focusButtonIdx : 0;
                        setButtonFocus();
                    }
                    else {
                        if (focusYIdx === 0) {
                            focusXIdx = selectedAdultMenuIdx;
                        }
                        else {
                            focusXIdx = selectedRatingIdx;
                        }

                        focusYIdx = ++focusYIdx < 2 ? focusYIdx : 0;

                        setRadioFocus();
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (isRightFocus) {
                        isRightFocus = false;
                        if(focusYIdx == 0) {
                            focusXIdx = selectedRatingIdx
                        }else {
                            focusXIdx = selectedAdultMenuIdx;
                        }
                        setFocus();
                    }
                    else {
                        if (focusXIdx == 0) {
                            LayerManager.historyBack();
                        }
                        else {
                            if (focusYIdx === 0) {
                                focusXIdx = --focusXIdx >= 0 ? focusXIdx : 4;
                            }
                            else {
                                focusXIdx = --focusXIdx >= 0 ? focusXIdx : 1;
                            }

                            setRadioFocus();
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (!isRightFocus){
                        if (focusXIdx == (focusYIdx == 0 ? 4:1)) {
                            isRightFocus = true;
                            setFocus();
                        }
                        else {
                            ++focusXIdx;
                            setRadioFocus();
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (isRightFocus) {
                        if (focusButtonIdx == 0) {
                            saveConfiguration();
                        }
                        else {
                            LayerManager.historyBack();
                        }
                    }
                    else {
                        if (focusYIdx === 0) {
                            selectedRatingIdx = focusXIdx;
                            ++focusYIdx;
                            focusXIdx = selectedAdultMenuIdx;
                        }
                        else {
                            selectedAdultMenuIdx = focusXIdx;
                            isRightFocus = true;
                        }

                        setFocus();
                    }
                    return true;
                default:
                    return false;
            }
        };

        function setFocus () {
            setButtonFocus();
            setRadioFocus();
        }

        function setRadioFocus () {
            var arrlimitAgeRadio = INSTANCE.div.find("#contents #setting_limit_age_area #limit_age_radio_area").children();
            var arrAdultHideRadio = INSTANCE.div.find("#contents #setting_limit_age_area #adult_hide_radio_area").children();
            var focusRadio = null;
            var isSameIdx = false;

            if (focusYIdx === 0) {
                focusRadio = $(arrlimitAgeRadio[focusXIdx]);

                if (selectedRatingIdx === focusXIdx) {
                    isSameIdx = true;
                }
            }
            else {
                focusRadio = $(arrAdultHideRadio[focusXIdx]);

                if (selectedAdultMenuIdx === focusXIdx) {
                    isSameIdx = true;
                }
            }

            INSTANCE.div.find("#contents #setting_limit_age_area .limit_age_radio").removeClass("focus");
            INSTANCE.div.find("#contents #setting_limit_age_area .limit_age_radio img").attr("src", "images/rdo_btn_d.png");

            $(arrlimitAgeRadio[selectedRatingIdx]).find("img").attr("src", "images/rdo_btn_select_d.png");
            $(arrAdultHideRadio[selectedAdultMenuIdx]).find("img").attr("src", "images/rdo_btn_select_d.png");

            if (!isRightFocus) {
                if (isSameIdx) {
                    focusRadio.find("img").attr("src", "images/rdo_btn_select_f.png");
                }
                else {
                    focusRadio.find("img").attr("src", "images/rdo_btn_f.png");
                }

                focusRadio.addClass("focus");
            }
        }

        function setButtonFocus () {
            var buttonArea = INSTANCE.div.find("#contents #rightMenu_area #rightMenu_button_area");

            buttonArea.children().removeClass("focus");

            if (isRightFocus) {
                $(buttonArea.children()[focusButtonIdx]).addClass("focus");
            }
        }

        function createComponent() {
            var settingLimitAgeArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "setting_limit_age_area",
                    css: {
                        position: "absolute", left: 133, top: 123, width: 1247, height: 825, "border-collapse": "collapse",
                        "border-top": "5px solid rgba(178, 177, 177, 0.25)", "border-bottom": "5px solid rgba(178, 177, 177, 0.25)"
                    }
                },
                parent: INSTANCE.div.find("#contents")
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 55, width: 886, height: 60,
                        "font-size": 48, "font-family": "RixHead M", color: "white", opacity: 0.7, "letter-spacing": -2.4
                    }
                },
                text: "시청연령 제한",
                parent: settingLimitAgeArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 125, width: 886, height: 80,
                        "font-size": 26, "font-family": "RixHead L", color: "white", opacity: 0.4, "letter-spacing": -1.3,
                        "line-height": "35px", "white-space": "pre"
                    }
                },
                text: "선택한 연령등급 이상의 콘텐츠 시청을 제한합니다\n제한된 프로그램은 성인인증 비밀번호를 입력해야 시청할 수 있습니다",
                parent: settingLimitAgeArea
            });

            var limitAgeRadioArea = createRadioButton({
                arrData: ["제한 없음", "19세", "15세", "12세", "7세"],
                radioWidth: 193
            });
            limitAgeRadioArea.attr("id", "limit_age_radio_area");
            limitAgeRadioArea.css({ position: "absolute", left: 0, top: 223, overflow: "visible" });
            settingLimitAgeArea.append(limitAgeRadioArea);

            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 411, width: 1247, height: 2,
                        "background-color": "rgba(255, 255, 255, 0.1)"
                    }
                },
                parent: settingLimitAgeArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 468, width: 886, height: 60,
                        "font-size": 48, "font-family": "RixHead M", color: "white", opacity: 0.7, "letter-spacing": -2.4
                    }
                },
                text: "성인 전용 메뉴 (19+) 숨김",
                parent: settingLimitAgeArea
            });

            var subTitle = adultMenuSubTitle[0];
            if (KTW.CONSTANT.IS_OTS) {
                subTitle = adultMenuSubTitle[1];
            }
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 536, width: 886, height: 80,
                        "font-size": 26, "font-family": "RixHead L", color: "white", opacity: 0.4, "letter-spacing": -1.3,
                        "line-height": "35px", "white-space": "pre"
                    }
                },
                text: subTitle[0] + " " + subTitle[1],
                parent: settingLimitAgeArea
            });

            var adultHideRadioArea = createRadioButton({
                arrData: ["성인 메뉴 숨김", "성인 메뉴 표시"],
                radioWidth: 285
            });
            adultHideRadioArea.attr("id", "adult_hide_radio_area");
            adultHideRadioArea.css({ position: "absolute", left: 0, top: 605, overflow: "visible" });
            settingLimitAgeArea.append(adultHideRadioArea);

            var rightMenuArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "rightMenu_area",
                    css: {
                        position: "absolute", left: 1433, top: 87, overflow: "visible"
                    }
                },
                parent: INSTANCE.div.find("#contents")
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/set_bg_btn_t.png",
                    css: {
                        position: "absolute", left: 1, top: 0
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/set_bg_btn.png",
                    css: {
                        position: "absolute", left: 0, top: 66, width: 420, height: 775
                    }
                },
                parent: rightMenuArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/set_bg_btn_b.png",
                    css: {
                        position: "absolute", left: 1, top: 841
                    }
                },
                parent: rightMenuArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 39, top: 79, width: 284,
                        "font-size": 26, color: "white", "font-family": "RixHead L", opacity: 0.4,
                         "line-height": "35px", "letter-spacing": -1.3
                    }
                },
                text: "시청 제한 연령등급 지정 및 성인 전용 메뉴 (19+) 숨김을 설정합니다",
                parent: rightMenuArea
            });

            var buttonArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "rightMenu_button_area",
                    css: {
                        position: "absolute", left: 39, top: 688, overflow: "visible"
                    }
                },
                parent: rightMenuArea
            });
            var saveButton = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_button_div",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66,
                        "background-color": "rgba(160,160,160,0.3)"
                    }
                },
                parent: buttonArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66, "line-height": "68px",
                        "text-align": "center", "font-size": 30, "letter-spacing": -1.5
                    }
                },
                text: "저장",
                parent: saveButton
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_button_line",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66
                    }
                },
                parent: saveButton
            });
            var cancelButton = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_button_div",
                    css: {
                        position: "absolute", left: 0, top: 81, width: 282, height: 66,
                        "background-color": "rgba(160,160,160,0.3)", border: "thin rgba(160,160,160,0.2) solid"
                    }
                },
                parent: buttonArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66, "line-height": "68px",
                        "text-align": "center", "font-size": 30, "letter-spacing": -1.5
                    }
                },
                text: "취소",
                parent: cancelButton
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rightMenu_button_line",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 282, height: 66
                    }
                },
                parent: cancelButton
            });
        }

        function createRadioButton (options) {
            var element = util.makeElement({
                tag: "<div />"
            });
            var radioLeft = 0;

            for (var i = 0; i < options.arrData.length; i++) {
                var width = options.radioWidth;
                if (options.arrData[i] === "제한 없음") {
                    width = 214;
                }
                var radio = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "limit_age_radio",
                        css: {
                            position: "absolute", left: radioLeft +(20*i), top: 0, width: width, height: 77,
                            overflow: "visible"
                        }
                    },
                    parent: element
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "limit_age_radio_bg",
                        css: {
                            position: "absolute", top: 0, width: width, height: 77
                        }
                    },
                    parent: radio
                });
                radioLeft += width;
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/rdo_btn_d.png",
                        css: {
                            position: "absolute", left: 18, top: 20
                        }
                    },
                    parent: radio
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: {
                            position: "absolute", left: 72, height: 40, "white-space": "nowrap"
                        }
                    },
                    text: options.arrData[i],
                    parent: radio
                });
            }

            return element;
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 시청연령 정보와 성인메뉴 노출 여부에 대한 설정 값을 읽어온다.
         * 성인 메뉴 노출 여부에 대한 설정 값은 UI의 내용과 반대로 정의되어 있다.
         * UI는 성인메뉴 숨김의 의미인데 반해 설정 값은 성인 메뉴 노출 여부이다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_PARENTAL_RATING);
            var tempData = "";
            if(settingData !== null && settingData !== null && settingData.length === 2) {
                parentalRating = settingData[0];
                tempData =  settingData[1];
            }

            log.printDbg("readConfiguration() parentalRating : " + parentalRating);
            if (parentalRating === PR_NONE) {
                selectedRatingIdx = PR.PR_NONE;
            } else if (parentalRating === PR_19) {
                selectedRatingIdx = PR.PR_19;
            } else if (parentalRating === PR_15) {
                selectedRatingIdx = PR.PR_15;
            } else if (parentalRating === PR_12) {
                selectedRatingIdx = PR.PR_12;
            } else if (parentalRating === PR_7) {
                selectedRatingIdx = PR.PR_7;
            }

            prevPR = parentalRating;

            // adultMenuDisplay === true: 성인메뉴 노출
            // adultMenuDisplay === false: 성인 메뉴 비노출
            if(tempData === "true") {
                adultMenuDisplay = true;
                selectedAdultMenuIdx = 1;
            }else {
                adultMenuDisplay = false;
                selectedAdultMenuIdx = 0;
            }
            prevAdultMenuIdx = selectedAdultMenuIdx;
        }

        /**
         * 변경된 설정 값을 저장한다.
         * 단, 연령, 성인메뉴 숨김 설정 각각 기존 설정 값과 변경된 경우메만 실제 저장이 이루어진다.
         */
        function saveConfiguration() {
            var ischanged = false;
            log.printDbg("saveConfiguration(), selectedRatingIdx = " + selectedRatingIdx +
            ",selectedAdultMenuIdx = " + selectedAdultMenuIdx + ", prevAdultMenuIdx = " + prevAdultMenuIdx);

            if (prevAdultMenuIdx !== selectedAdultMenuIdx) {
                // 기존 설정되어 있는 값과 변경된 경우에만 저장한다.
                // 이 key 에 대한 값을 저장하게 되면 MenuDataManager 에서 메뉴를 재구성하기 때문이다.
                basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY, selectedAdultMenuIdx === 0 ? "false" : "true");
                ischanged = true;
            }

            // [jh.lee] 시청 연령 제한 저장
            if (selectedRatingIdx === 0) {
                parentalRating = PR_NONE;
            } else if (selectedRatingIdx === 1) {
                parentalRating = PR_19;
            } else if (selectedRatingIdx === 2) {
                parentalRating = PR_15;
            } else if (selectedRatingIdx === 3) {
                parentalRating = PR_12;
            } else if (selectedRatingIdx === 4) {
                parentalRating = PR_7;
            }

            log.printDbg("saveConfiguration(), prevPR = " + prevPR +
            ",parentalRating = " + parentalRating);

            if (prevPR !== parentalRating) {
                casAdapter.setParentalRating(parentalRating, KTW.managers.auth.AuthManager.getTempPin());
                // 2017.06.23 dhlee 2분 rule은 즉시 reset 하도록 한다.
                KTW.ca.CaHandler.checkParentalRatingLock(true);
                KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_CHANGE_AGE_LIMIT,
                    { pin : KTW.managers.auth.AuthManager.getTempPin(), age_limit : parentalRating }, onCaResponse);
                //onCaResponse(true);
            }else {
                if(ischanged === true) {
                    //LayerManager.historyBack();
                    // settingDataAdapter.historyBackAndPopup(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_PARENTAL_RATING);
                }
                data.complete();
                settingMenuManager.historyBackAndPopup();
            }
        }

        /**
         * PR 변경된 경우 수행해야 할 후처리
         *
         * @param success
         */
        function onCaResponse(success, response_data) {
            log.printDbg("onCaResponse(), success = " + success);
            log.printDbg("onCaResponse(), data = " + JSON.stringify(response_data));

            var navAdapter = KTW.oipf.AdapterHandler.navAdapter;

            if (success === true) {
                // 2017.05.29 dhlee
                // 성공 시 listener 를 호출한다.
                KTW.oipf.AdapterHandler.casAdapter.notifyPRChangeListener();
                if (KTW.managers.service.StateManager.isTVViewingState() === true) {
                    var currentChannel = navAdapter.getCurrentChannel();
                    if (currentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ||
                        currentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                        // 시청연령 제한 설정 변경 시 PR unlock 하도록 함 (2분 rule에 대한 내용임)
                        //KTW.ca.CaHandler.checkParentalRatingLock(true);

                        log.printDbg("onCaResponse(), KTW.managers.service.IframeManager.getType() = " + KTW.managers.service.IframeManager.getType());

                        if (KTW.managers.service.IframeManager.getType() === KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED) {
                            // 시청연령제한 상태인 경우 설정 변경으로 인해서
                            // PR blocked가 해제될 수 있으므로 다시 동일채널을 tune 하는데
                            // 동일 permission으로 인해 miniEPG가 노출되지 않는 증상을 해결하기 위해
                            // 의도적으로 permission을 reset 처리하도록 함
                            navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).resetPerm();
                            navAdapter.changeChannel(currentChannel, true);
                        }
                    }
                }
                prevPR = parentalRating;

                data.complete();
                settingMenuManager.historyBackAndPopup();
            } else {
                LayerManager.historyBack();
            }


        }
    };

    KTW.ui.layer.setting.setting_parentalRating.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_parentalRating.constructor = KTW.ui.layer.setting.setting_parentalRating;

    KTW.ui.layer.setting.setting_parentalRating.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_parentalRating.prototype.show = function (options) {
        KTW.ui.Layer.prototype.show.call(this, options);

        this.showView(options);
    };

    KTW.ui.layer.setting.setting_parentalRating.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };


    KTW.ui.layer.setting.setting_parentalRating.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();