/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 해상도 설정
 * UHD1 STB 에서는 일반TV, HD TV, 4K UHD TV 설정 기능 제공
 * UHD2 이상 STB 에서는 HD TV, 4K UHD TV 설정 기능 제공
 * 이 정보는 hwAdapter 에서 제공하는 isSupportedHdVideoFormat() 을 이용하여 확인 가능
 */

(function () {
    KTW.ui.layer.setting.setting_resolution  = function Resolution(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var StorageManager = KTW.managers.StorageManager;
        var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var settingDataAdapter =  KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;

        var focusXIdx = 0;
        var focusYIdx = 0;
        var isRightFocus = false;
        var is_4K_UHDSelect = false;
        var selectText = "";
        var selectedIdx;
        var selectedX;
        var selectedY;

        var HAS_LOW_RESOLUTION = true;
        var HAS_CHANGE_API = false;

        var hd_video_format = 0;
        var prev_video_format =0;
        var original_format = 0;

        var resolution_timer = null;
        var RESOLUTION_TIMEOUT = 15;


        var clock;
        var clockArea;

        var data;

        var stbCtrl = KTW.oipf.AdapterHandler.extensionAdapter.getNavStbCtrl();
        var listener;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setResolution"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>해상도</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );
            initConfiguration();
            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.destroy = function() {

            if (resolution_timer) {
                // [sw.nam] 타이머가 존재하는 상태에서 메뉴가 종료되는 상황
                descheduleResolutionTimer();
                KTW.ui.LayerManager.deactivateLayer({
                    id: KTW.ui.Layer.ID.SETTING_RESOLUTION_CHANGE_POPUP,
                    remove: true
                });
                if (HAS_CHANGE_API) {
                    hwAdapter.setHDVideoFormat(original_format);
                } else {
                    hwAdapter.setAVOutputHDVideoFormat(original_format);
                }
            }
            //    $("#setting_layer_background", parent_div).remove();
        };
        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show()");

            readConfiguration();

            selectedX = focusXIdx;
            selectedY = focusYIdx;
            selectIconRefresh(focusXIdx, focusYIdx);
            menuFocusChange();
            buttonFocusRefresh(selectedX, selectedY);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_RESOLUTION) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
        //    clock.show(clockArea);


            listener = { "notifyScreenChanged" : function(resolution) {
                log.printDbg("notifyScreenChanged2 "+ resolution);

                descheduleResolutionTimer();
                deactivatePopupRelatedWithResolution();

                readConfiguration();
                selectedX = focusXIdx;
                selectedY = focusYIdx;
                selectIconRefresh(focusXIdx, focusYIdx);
                menuFocusChange();
                buttonFocusRefresh(selectedX, selectedY);
            } };
            try {
                stbCtrl.addScreenResolutionChangeListener(listener);
            } catch(e) {
                log.printErr("failed addScreenResolutionChangeListener(");
                log.printExec(e);
            }
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide resolution");

            if (resolution_timer) {
                // [sw.nam] 타이머가 존재하는 상태에서 메뉴가 종료되는 상황
                descheduleResolutionTimer();
                KTW.ui.LayerManager.deactivateLayer({
                    id: KTW.ui.Layer.ID.SETTING_RESOLUTION_CHANGE_POPUP,
                    remove: true
                });

                if (HAS_CHANGE_API) {
                    hwAdapter.setHDVideoFormat(original_format);
                } else {
                    hwAdapter.setAVOutputHDVideoFormat(original_format);
                }
            }
       //     clock.hide();

            try {
                stbCtrl.addScreenResolutionChangeListener(null);
            } catch(e) {
                log.printErr("failed addScreenResolutionChangeListener(");
                log.printExec(e);
            }


        };

        this.controlKey = function (key_code) {
            var len;
            switch(key_code) {
                case KEY_CODE.UP:
                    focusXIdx = 0;
                    if(isRightFocus) {
                        len =2;
                        buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, -1, len));
                    }
                    else {
                        focusYIdx--;
                        if(HAS_LOW_RESOLUTION) {
                            len =3;
                            if(focusYIdx < 0) {
                                focusYIdx =2;
                            }
                        } else {
                            len =2;
                            if(focusYIdx < 1) {
                                focusYIdx = 2;
                            }
                        }
                        if(selectedY == focusYIdx) {
                            focusXIdx = selectedX;
                        }
                        buttonFocusRefresh(focusXIdx,focusYIdx);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    focusXIdx = 0;
                    if(isRightFocus) {
                        len =2;
                        buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, 1, len));
                    }
                    else {
                        focusYIdx ++;
                        if(HAS_LOW_RESOLUTION) {
                            len =3;
                            if(focusYIdx > 2) {
                                focusYIdx = 0;
                            }
                        } else {
                            len =2;
                            if(focusYIdx > 2) {
                                focusYIdx = 1;
                            }
                        }
                        if(selectedY == focusYIdx) {
                            focusXIdx = selectedX;
                        }
                        buttonFocusRefresh(focusXIdx,focusYIdx);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (isRightFocus) {
                        menuFocusChange();
                    } else {
                        if(focusXIdx == 0 )
                            LayerManager.historyBack();
                        else
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, -1, (focusYIdx == 1? 3:2)), focusYIdx);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(!isRightFocus) {
                        if (focusXIdx == (focusYIdx == 1? 2:1)) {
                            menuFocusChange();
                        } else {
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, 1, (focusYIdx == 1? 3:2)), focusYIdx);
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:

                    var connected_hdmi;

                    if (isRightFocus) {
                        if (focusYIdx == 0) {
                            // 시스템 저장

                            // while resolution_timer is running, do not do anything.
                            if(resolution_timer){
                                log.printDbg("while resolution_timer is running, do not do anything.");
                                return;
                            }

                            prev_video_format = hwAdapter.getAVOutputHDVideoFormat();

                            // UHD TV 인데 컴포넌트로 연결되어 있으면서 1080 이상의 해상도를 선택 한 경우.
                            // 2017.07.10 sw.nam  플립북 v1.23 933p 기준 컴포넌트 연결시 띄워주는 알림팝업 case 가 삭제 되어 있음, 확인 필요
                            if (KTW.CONSTANT.IS_UHD) {
                                connected_hdmi = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.HDMI_IS_CONNECTED);

                                if (connected_hdmi === "false" &&  (selectedIdx == 3 || selectedIdx == 4 || selectedIdx == 5 || selectedIdx == 6 ) ) {
                                    // popup
                                    showResolutionAlertPopup();
                                    return ;
                                }

                            }

                            log.printDbg("Current Resolution === " + original_format);
                            log.printDbg("selectedIndex ========== " + selectedIdx);


                            // sw.nam 4K 해상도 설정의 경우 설정 알림 팝업을 먼저 띄워줘야 함
                            if (is_4K_UHDSelect) {
                                log.printDbg("UHD4K is selected ");
                                showUHDCheckingPopup();

                                return;
                            }
                            if(prev_video_format === hd_video_format ||  (hd_video_format === "480i" || hd_video_format === "480p" || hd_video_format === "720p")) {
                                // 현재 설정값이 기존 설정 값과 같다면  변경 팝업 로직을 탈 필요가 없음.

                                // WEBIIIHOME-94 Jira 이슈넘버
                                // 2017.04.06 [sw.nam] 480i 480p 720p 해상도는 변경 확인 팝업(*표 누르는 팝업)이  시나리오에 포함되지 않으므로 바로 변경하도록 수정 (2.0 코드 기준)
                                hwAdapter.setAVOutputHDVideoFormat(hd_video_format);
                                menuFocusChange();
                                //LayerManager.getLayer("SettingMain").setSubTitle("System", layerIdx, hd_video_format);
                                data.complete();
                                settingMenuManager.historyBackAndPopup();

                                // LayerManager.historyBack();
                            } else {
                                // In case the setting value is going to change is different from prev_video_format, go to rfirm()
                                requireConfirm();
                            }
                        } else { // 취소 로직
                            menuFocusChange();
                            LayerManager.historyBack();
                        }


                    } else {

                        is_4K_UHDSelect = false;
                        if(focusXIdx == 0 && focusYIdx == 0) {
                            hd_video_format = "480i";
                        } else if(focusXIdx == 1 && focusYIdx == 0) {
                            hd_video_format = "480p";
                        } else if(focusXIdx == 0 && focusYIdx == 1) {
                            hd_video_format = "720p";
                        } else if(focusXIdx == 1 && focusYIdx == 1) {
                            hd_video_format = "1080i";
                        } else if(focusXIdx == 2 && focusYIdx == 1) {
                            hd_video_format = "1080p";
                        } else if(focusXIdx == 0 && focusYIdx == 2) {
                            hd_video_format = "2160p30";
                            is_4K_UHDSelect = true;
                        } else if(focusXIdx == 1 && focusYIdx == 2) {
                            hd_video_format = "2160p60";
                            is_4K_UHDSelect = true;
                        }
                        selectedX = focusXIdx;
                        selectedY = focusYIdx;
                        selectIconRefresh(focusXIdx, focusYIdx);
                    }
                    return true;
                default:
                    return false;
            }
        };

        function menuFocusChange() {
            if (isRightFocus) {
                isRightFocus = false;
                // focusXIdx = 0;
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                buttonFocusRefresh(selectedX, selectedY);

            } else {
                INSTANCE.div.find("#contents #settingTable .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0)").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_text").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_line").addClass("focus");

                focusYIdx = 0;
                isRightFocus = true;
            }
        }

        //라디오 버튼 선택 처리 함수
        function selectIconRefresh(index_X, index_Y) {
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
            focusXIdx = 0;
            selectText =  INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + (index_Y+1) + ") #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ") .table_radio_text").text();
            menuFocusChange();
        }

        // 버튼 영역 포커스 처리 함수
        function buttonFocusRefresh(index_X, index_Y) {
            focusXIdx = index_X;
            focusYIdx = index_Y;
            if (isRightFocus) {
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +")").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +") .rightMenu_btn_text").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +") .rightMenu_btn_line").addClass("focus");
            } else {
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");

                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ")").addClass("focus");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(" + (index_Y) + ") .table_radio_btn:eq(" + index_X + ") .table_radio_text").addClass("focus");
            }
            setSelectedIdx(focusXIdx,focusYIdx);
        }

        function createComponent() {

            INSTANCE.div.find("#contents").html("");
            if(HAS_LOW_RESOLUTION) {
                INSTANCE.div.find("#contents").append("<div id='settingTable'>" +

                "<div class='table_item_div' style='position: absolute; top: 0px; width: 1247px; height: 187px; '>" +
                "<div class='table_title'>해상도</div>" +
                "<div class='table_infoText'>사용중인 TV가 지원하는 최대 해상도까지 설정 가능합니다</div>" +
                "</div>" +

                "<div class='table_divisionLine'style='position: absolute; top:183px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div' style='position: absolute; top: 185px; width: 1247px; height: 187px;'>" +
                "<div class='table_subTitle'>일반 TV</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='position: absolute; left: 0px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>480i</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 285px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>480p</div>" +
                "</div>" +
                "</div>" +
                "</div>" +

                "<div class='table_divisionLine'style='position: absolute; top:396px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div' style='position: absolute; top: 398px; width: 1247px; height: 187px;'>" +
                "<div class='table_subTitle'>HD TV</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='position: absolute; left: 0px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>720p</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 285px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>1080i</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 570px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>1080p</div>" +
                "</div>" +
                "</div>" +
                "</div>" +

                "<div class='table_divisionLine'style='position: absolute; top:609px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div' style='position: absolute; top: 611px; width: 1247px; height: 187px;'>" +
                "<div class='table_subTitle'>4K UHD TV</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='position: absolute; left: 0px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>2160p (30fps)</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 285px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>2160p (60fps)</div>" +
                "</div>" +
                "</div>" +
                "</div>" +

                "</div>");
            }else {
                INSTANCE.div.find("#contents").append("<div id='settingTable'>" +

                "<div class='table_item_div' style='position: absolute; top: 0px; width: 1247px; height: 187px; '>" +
                "<div class='table_title'>해상도</div>" +
                "<div class='table_infoText'>사용중인 TV가 지원하는 최대 해상도까지 설정 가능합니다</div>" +
                "</div>" +

                "<div class='table_item_div' style='position: absolute; top: 185px; width: 1247px; height: 187px; visibility: hidden;'>" +
                    "<div id='table_radio_area'></div>" +
                "</div>"+

                "<div class='table_divisionLine'style='position: absolute; top:183px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div' style='position: absolute; top: 185px; width: 1247px; height: 187px;'>" +
                "<div class='table_subTitle'>HD TV</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='position: absolute; left: 0px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>720p</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 285px;' >" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>1080i</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 570px;' >" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>1080p</div>" +
                "</div>" +
                "</div>" +
                "</div>" +

                "<div class='table_divisionLine'style='position: absolute; top:396px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div' style='position: absolute; top: 398px; width: 1247px; height: 187px;'>" +
                "<div class='table_subTitle'>4K UHD TV</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='position: absolute; left: 0px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>2160p (30fps)</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='position: absolute; left: 285px;'>" +
                "<div class ='table_radio_btn_bg'></div>"+
                "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>2160p (60fps)</div>" +
                "</div>" +
                "</div>" +
                "</div>" +

                "</div>");
            }

            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text1'>사용중인 TV 해상도를<br>확인 후 설정을 저장해주세요</div>" +
            "<div id='rightMenu_line'></div>" +
            "<div id='rightMenu_text2'>셋톱박스 전면부 전원버튼을 5초 이상 누르면 해상도 1080i로 설정됩니다</div>" +
            "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                    "<div class='rightMenu_btn_text'>저장</div>" +
                    "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                    "<div class='rightMenu_btn_text'>취소</div>" +
                    "<div class='rightMenu_btn_line'></div>" +
                    "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function setSelectedIdx(focusX, focusY) {

            if(focusX == 0 && focusY ==0) {
                selectedIdx = 0;
            } else if(focusX == 1 && focusY == 0) {
                selectedIdx = 1;
            } else if(focusX == 0 && focusY == 1) {
                selectedIdx = 2;
            } else if(focusX == 1 && focusY == 1) {
                selectedIdx = 3;
            } else if(focusX == 2 && focusY == 1) {
                selectedIdx = 4;
            } else if(focusX == 0 && focusY == 2) {
                selectedIdx = 5;
            } else if(focusX == 1 && focusY == 2) {
                selectedIdx = 6;
            }
            // selectedIdx += "";
        }

        function initConfiguration() {
            log.printDbg("initConfiguration()");


            var supported_format;
            var count = 0;

            supported_format = hwAdapter.isSupportedHdVideoFormats();
            log.printDbg("supported_format == " + JSON.stringify(supported_format));
            if (supported_format) {
                for (var i = 0; i < supported_format.length; i++) {
                    if (supported_format[i] === "480i" || supported_format[i] === "480p") {
                        count++;
                    }
                }
            }

            if (count !== 2) {
                // 저해상도 지원여부
                HAS_LOW_RESOLUTION = false;
                log.printDbg("HAS_LOW_RESOLUTION!!!!!");
            }


            try {
                if(hwAdapter.getHDVideoFormat()) {
                    HAS_CHANGE_API = true;
                } else {
                    HAS_CHANGE_API = false;
                }
                log.printDbg("HAS_CHANGE_API : " + HAS_CHANGE_API);
            } catch (error) {
                log.printDbg(error.message);
            }

        }

        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var resolution_data = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_RESOLUTION);
            hd_video_format = resolution_data[0];

            switch(hd_video_format) {
                case "480i" :
                    focusXIdx = 0;
                    focusYIdx = 0;
                    selectedIdx =0;
                    break;
                case "480p" :
                    focusXIdx = 1;
                    focusYIdx = 0;
                    selectedIdx =1;
                    break;
                case "720p" :
                    focusXIdx = 0;
                    focusYIdx = 1;
                    selectedIdx =2;
                    break;
                case "1080i" :
                    focusXIdx = 1;
                    focusYIdx = 1;
                    selectedIdx =3;
                    break;
                case "1080p" :
                    focusXIdx = 2;
                    focusYIdx = 1;
                    selectedIdx =4;
                    break;
                case "2160p30" :
                    focusXIdx = 0;
                    focusYIdx = 2;
                    selectedIdx =5;
                    break;
                case "2160p60" :
                    focusXIdx = 1;
                    focusYIdx = 2;
                    selectedIdx =6;
                    break;
                default :
                    focusXIdx = 1;
                    focusYIdx = 1;
                    selectedIdx =3;
                    break;
            }
            original_format = hd_video_format;
            isRightFocus = false;
        }

        function showResolutionChangePopup() {
            log.printDbg("showResolutionChangePopup()");
            var convertingVideoFormat = hd_video_format;

            if(convertingVideoFormat == "2160p30") {
                convertingVideoFormat = "2160p (30fps)";
            } else if(convertingVideoFormat == "2160p60") {
                convertingVideoFormat = "2160p (60fps)";
            }

            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_RESOLUTION_CHANGE_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        waitingSecond : RESOLUTION_TIMEOUT,
                        resolutionValue : convertingVideoFormat,
                        complete: function() {
                            descheduleResolutionTimer();
                            //[sw.nam] 2017-04-13 변경 팝업 호출 후 변경 된 해상도로 단말에 set 하려면 confirmHDVideoFormat(); 호출 필요
                            if (HAS_CHANGE_API) {
                                hwAdapter.confirmHDVideoFormat();
                            }
                            data.complete();
                            settingMenuManager.historyBackAndPopup();
                        }
                    }
                },
                new: true,
                visible: true
            });
            return;

        }

        function showUHDCheckingPopup() {
            log.printDbg("showUHDCheckingPopup()");

            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_UHD_CHECKING_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        complete: function() {
                            requireConfirm();
                        }
                    }
                },
                new: true,
                visible: true
            });
        }

        function showResolutionAlertPopup() {
            log.printDbg("showResolutionAlertPopup()");
            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_RESOLUTION_ALERT_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true
                },
                new: true,
                visible: true
            });
            return;
        }
        /**
         * [sw.nam] 설정 저장 후 확인 로직 (기존 설정에서 변경된 경우 해당 로직을 수행)
         */
        function requireConfirm() {
            log.printDbg("requireConfirm()");
            if(is_4K_UHDSelect) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: KTW.ui.Layer.ID.SETTING_UHD_CHECKING_POPUP,
                    remove: true
                });
                RESOLUTION_TIMEOUT = 15;
            } else {
                RESOLUTION_TIMEOUT = 15;
            }
            scheduleResolutionTimer(onBackPrevResolution, RESOLUTION_TIMEOUT * 1000);
            showResolutionChangePopup();

            if (HAS_CHANGE_API) {
                hwAdapter.setHDVideoFormat(hd_video_format);
            } else {
                hwAdapter.setAVOutputHDVideoFormat(hd_video_format);
            }
        }

        /**
         * [sw.nam] 타이머 설정
         */
        function scheduleResolutionTimer(callback, time) {
            descheduleResolutionTimer();
            resolution_timer = setTimeout(callback, time);
        }

        /**
         * [sw.nam] 타이머 해제
         */
        function descheduleResolutionTimer() {
            if (resolution_timer) {
                clearTimeout(resolution_timer);
                resolution_timer = null;
            }
        }

        function onBackPrevResolution() {
            log.printDbg("onBackPrevResolution");

            descheduleResolutionTimer();

            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_RESOLUTION_CHANGE_POPUP,
                remove: true
            });
            setTimeout( function() {
                if (HAS_CHANGE_API) {
                    hwAdapter.setHDVideoFormat(original_format);
                } else {
                    hwAdapter.setAVOutputHDVideoFormat(original_format);
                }

                hd_video_format = original_format;

                log.printDbg("Rollback prevFormat === " + original_format);
                if(is_4K_UHDSelect) {
                    is_4K_UHDSelect = false;
                }
                switch(hd_video_format) {
                    case "480i" :
                        focusXIdx = 0;
                        focusYIdx = 0;
                        selectedIdx =0;
                        break;
                    case "480p" :
                        focusXIdx = 1;
                        focusYIdx = 0;
                        selectedIdx =1;
                        break;
                    case "720p" :
                        focusXIdx = 0;
                        focusYIdx = 1;
                        selectedIdx =2;
                        break;
                    case "1080i" :
                        focusXIdx = 1;
                        focusYIdx = 1;
                        selectedIdx =3;
                        break;
                    case "1080p" :
                        focusXIdx = 2;
                        focusYIdx = 1;
                        selectedIdx =4;
                        break;
                    case "2160p30" :
                        focusXIdx = 0;
                        focusYIdx = 2;
                        selectedIdx =5;
                        is_4K_UHDSelect = true;
                        break;
                    case "2160p60" :
                        focusXIdx = 1;
                        focusYIdx = 2;
                        selectedIdx =6;
                        is_4K_UHDSelect = true;
                        break;
                    default :
                        focusXIdx = 1;
                        focusYIdx = 1;
                        selectedIdx =3;
                        break;
                }

                selectedX = focusXIdx;
                selectedY = focusYIdx;

                selectIconRefresh(focusXIdx, focusYIdx);
                buttonFocusRefresh(selectedX, selectedY);

            }, 1000);
        }

        /**
         * 2017.07.07 sw.nam
         * 해상도 설정 메뉴 화면에서 팝업이 띄워져 있을 때 , 하드웨어 해상도 리셋이 일어나는 경우 포커스가 꼬이는 현상이 일어나므로
         * 하드웨어 리셋을 하는 경우 해상도 설정 초기화면을 유지하기 위해 노출될 수 있는 모든 팝업을 deactivating 시킨다
         */
        function deactivatePopupRelatedWithResolution() {

            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_RESOLUTION_CHANGE_POPUP,
                remove: true
            });
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_UHD_CHECKING_POPUP,
                remove: true
            });
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_RESOLUTION_ALERT_POPUP,
                remove: true
            });


        }
    };

    KTW.ui.layer.setting.setting_resolution.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_resolution.constructor = KTW.ui.layer.setting.setting_resolution;

    KTW.ui.layer.setting.setting_resolution.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_resolution.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();