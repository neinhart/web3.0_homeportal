/**
 * Created by Yun on 2017-03-02.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_allResettingSystemPopup  = function allResettingSystemPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;

        //var KEY_CODE = KTW.KEY_CODE;
        //var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        var data;
        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup allResettingSystemPopup");
            div.append($("<title/>").css({"position":"absolute", "top":"469px"}).text("전체 초기화"));
            div.append($("<span/>", {class:"text_white_center_52M"}).text("시스템을 초기화 중입니다"));
            div.append($("<span/>", {class:"text_white_center_38L"}).text("전원을 끄지 마세요"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            data = this.getParams();
            //var result = 0;
            data.complete();

        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {

            return true;

        };
    };
    KTW.ui.layer.setting.popup.setting_allResettingSystemPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_allResettingSystemPopup.constructor = KTW.ui.layer.setting.popup.setting_allResettingSystemPopup;

    KTW.ui.layer.setting.popup.setting_allResettingSystemPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_allResettingSystemPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();