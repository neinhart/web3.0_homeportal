/**
 * Created by Taesang on 2016-12-30.
 */

(function() {

    KTW.ui.layer.setting.popup.setting_resolutionStarKey  = function resolutionStarKeyPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        
        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup");
            div.append($("<title/>").css({"position": "absolute", "top": "459px"}).text("해상도"));
            div.append($("<span/>", {class: "text_white_center_38M"}).css({"position": "absolute", "line-height": "46px", "top": "529px"}).html("해상도 2160p (60fps) 설정을 하시려면<br>15초 이내 리모컨의 별표키(*)를 누르세요"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };
    KTW.ui.layer.setting.popup.setting_resolutionStarKey.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_resolutionStarKey.constructor = KTW.ui.layer.setting.popup.setting_resolutionStarKey;

    KTW.ui.layer.setting.popup.setting_resolutionStarKey.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_resolutionStarKey.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();