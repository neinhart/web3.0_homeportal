/**
 * Created by skkwon on 2017-01-18.
 *
 * 설정 > 시스템 설정 > 블루투스 장치 검색 팝업
 */

(function () {
    KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup  = function BluetoothDeviceSearchPopup(options) {
        KTW.ui.Layer.call(this, options);

        //  장치검색 결과 state
        var STATE = {
            SEARCHING_HAS_BT: -1, // 검색중, 찾은 장치 있음..
            SEARCHING: 0, // 검색중, 찾은 장치 없음
            HAS_RESULT: 1, // 검색 결과
            NO_RESULT: 2 // 아무것도 찾지 못함
        };

        var div = $("<div/>", {class: "popup_contents"});
        var device_list = [];

        var state;
        var page = 0, totalPage;
        var mainFocus = 0, listFocus = 0, btnFocus = 0;

        var log = KTW.utils.Log;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;

        var SettingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;

        var device_list_length = 0;

        var selectedDevice;  // 현재 선택된 장치

        var data;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setting gniPopup bluetooth_device_search_popup"});

            div.append($("<title/>").text("장치 검색"));
            div.append($("<span/>", {class:"text1"}).text("블루투스 장치는 최대 6개까지 등록됩니다"));
            div.append($("<div/>", {class:"contents_box"}));
            div.append($("<div/>", {class:"btn_area"}));

            makeContentsBox();
            makeButtonArea();

            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };

        function makeContentsBox() {
            var box = div.find(".contents_box");
            box.append($("<div/>", {class:"bg_area"}));
            //box.append($("<div/>", {class:"loading_area contents"}));
            box.append($("<div/>", {class:"list_area contents"}));
            box.append($("<div/>", {class:"notice_area contents"}));

            /*box.find(".bg_area").html("<div class='bg_left'/><div class='bg_middle'/><div class='bg_right'/>");*/
            //box.find(".loading_area").html("<div class='loading_center'/><!--<img class='loading_img'>-->");
            box.find(".notice_area").html("<div class='notice_icon'/><span class='notice_text'>주변에 등록할 장치가 없습니다</span>");
            var item = "<li><div class='item'><div class='item_icon'/><div class='item_name'/></div></li>";
            box.find(".list_area").append("<ul class='item_list'>" + item + item + item + "</ul>");
            box.find(".list_area").append("<div class='focus_box'/><div class='page_indicator_bg'/><div class='page_indicator_bar'/>");
        }

        function makeButtonArea() {
            var d = div.find(".btn_area");
            var btn1,btn2;
            d.append($("<div/>", {class:"button1"}));
            d.append($("<div/>", {class:"button2"}));
            d.find(".button1").css({position: "relative", float: "left"});
            d.find(".button2").css({position: "relative", float: "left"});

            btn1 = d.find(".button1");
            btn2 = d.find(".button2");

            btn1.append($("<div/>", {class:"buttonLine"}));
            btn1.append($("<btn/>", {class:"btn_1"}).text("재검색"));
            btn2.append($("<div/>", {class:"buttonLine"}));
            btn2.append($("<btn/>", {class:"btn_2"}).text("취소"));

        }

        this.show = function() {
            log.printDbg("show()");
            Layer.prototype.show.apply(this, arguments);
            data = this.getParams();
            // 블루투스 eventListener 초기화
            bluetoothDevice.setEventListener(null);
            search();
        };
        this.hide = function() {
            log.printDbg("hide()");
            Layer.prototype.hide.call(this);

            if(state == 0 || LayerManager.isShowLoading() === true) {
                bluetoothDevice.stopScan();
                LayerManager.stopLoading();
            }
            listFocus = 0;
        };

        function setData(devices, _page, isSearching) {

            if(devices) device_list = devices;
            if(_page!=null) page = _page;

            if(!device_list) {
                device_list_length =0;
            }
            if(device_list_length == 0) { // device 를 하나도 못찾은 경우..
                setState(STATE.NO_RESULT);
            }else {
                var i = 0;
                var item = div.find(".list_area li");
                var type;

                for (; i < 3 && page * 3 + i < device_list.length; i++) {
                    type = SettingDataAdapter.getConvertedType(device_list[page * 3 + i].type);
                    item.eq(i).show();
                    item.eq(i).find(".item_icon").removeClass("audio headset keyboard mouse").addClass(type);
                    item.eq(i).find(".item_name").text(device_list[page * 3 + i].name);
                }
                for (; i < 3; i++) {
                    item.eq(i).hide();
                }
                if(isSearching) {
                    setState(STATE.SEARCHING_HAS_BT);
                } else {
                    setState(STATE.HAS_RESULT);
                }

                // 갯수 파악하기
                totalPage = Math.ceil(device_list.length / 3);
                div.find(".page_indicator_bar").css("height", 234/totalPage);
                div.find(".page_indicator_bar").css("-webkit-transform", "translateY(" + 234/totalPage*page + "px)");
                if(totalPage < 2) {
                    div.find(".page_indicator_bg").css({visibility: "hidden"});
                    div.find(".page_indicator_bar").css({visibility: "hidden"});
                }else {
                    div.find(".page_indicator_bg").css({visibility: "inherit"});
                    div.find(".page_indicator_bar").css({visibility: "inherit"});
                }
            }
        }

        /**
         * button 영역과 list 영역을 이동할 때 사용되는 function
         * @param _focus
         * 0 : list 포커스 영역으로 이동
         * 1 : button 포커스 영역으로 이동
         * @returns {boolean}
         */
        function setMainFocus(_focus) {
            if(mainFocus == _focus) return false;
            mainFocus = _focus;
            switch (_focus) {
                case 0:
                    if(state!=1) return false;
                    div.find(".focus_box").show();
                    div.find(".btn_area .focus").removeClass("focus");
                    setListFocus(listFocus);
                    break;
                case 1:
                    div.find(".focus_box").hide();
                    div.find(".list_area .focus").removeClass("focus");
                    setButtonFocus(btnFocus);
                    break;
            }
            return true;
        }

        function setListFocus(_focus) {
            div.find(".list_area .focus").removeClass("focus");
            div.find(".list_area .item").eq(_focus).addClass("focus");
            div.find(".focus_box").css("-webkit-transform", "translateY(" + 78*_focus + "px)");
            listFocus = _focus;
        }

        function setButtonFocus(_focus) {
            div.find(".btn_area .focus").removeClass("focus");
            div.find(".btn_area btn").eq(_focus).addClass("focus");
            div.find(".btn_area .buttonLine").eq(_focus).addClass("focus");
            btnFocus = _focus;
        }

        function setState(_state) {
            state = _state;
            switch (_state) {

                case STATE.SEARCHING :
                    div.find(".text1").text("장치를 검색 중입니다");
                    div.find(".contents").removeClass("show");
                    //div.find(".loading_area").addClass("show");
                    div.find(".btn_1").text("검색 중지");
                    setMainFocus(1);
                    setButtonFocus(0);
                    break;
                case STATE.SEARCHING_HAS_BT:
                    div.find(".text1").text("장치를 검색 중입니다");
                    div.find(".contents").removeClass("show");
                    div.find(".list_area").addClass("show");
                    div.find(".btn_1").text("검색 중지");
                    setMainFocus(1);
                    break;
                case STATE.HAS_RESULT:
                    div.find(".text1").text("블루투스 장치는 최대 6개까지 등록됩니다");
                    div.find(".contents").removeClass("show");
                    div.find(".list_area").addClass("show");
                    div.find(".btn_1").text("재검색");
                    setMainFocus(0);
                    break;
                case STATE.NO_RESULT:
                    div.find(".text1").text("다시 검색하려면 재검색 버튼을 선택하세요");
                    div.find(".contents").removeClass("show");
                    div.find(".notice_area").addClass("show");
                    div.find(".btn_1").text("재검색");
                    setMainFocus(1);
                    setButtonFocus(0);
                    break;
            }
        }

        this.controlKey = function(keyCode) {
            if(keyCode == KEY_CODE.BACK){
                stopSearching();
                LayerManager.stopLoading();
                KTW.ui.LayerManager.deactivateLayer({
                    id: KTW.ui.Layer.ID.SETTING_BLUETOOTH_DEVICE_SEARCH_POPUP,
                    remove: true
                });
                return true;
            } else if(keyCode == KEY_CODE.EXIT) {
                /*                if(state == 0 || LayerManager.isShowLoading() === true) {
                 bluetoothDevice.stopScan();
                 LayerManager.stopLoading();
                 }*/
            }
            else switch (mainFocus) {
                    case 0: return onKeyForList(keyCode);
                    case 1: return onKeyForButton(keyCode);
                }
        };

        function onKeyForList(keyCode) {
            switch (keyCode) {
                case KEY_CODE.UP:
                    if(listFocus == 0) {
                        if(page==0) {
                            setMainFocus(1)
                        }else {
                            setData(null, page-1);
                            setListFocus(2);
                        }
                    }else {
                        setListFocus(listFocus-1);
                    }
                    return true;
                case KEY_CODE.DOWN:
                  listFocus++;
                    // 장치 검색 결과 1페이지 이내이거나 마지막 페이지인 경우
                    if(totalPage == 1 || (totalPage > 1 && page == totalPage -1)) {
                        // 페이지 내 가장 아래 포커스
                        if( listFocus > (device_list.length-1) %3 ) {
                            setMainFocus(1);
                        }else {
                            setListFocus(listFocus);
                        }
                    }else {
                        // 중간 페이지 포커스 처리
                        if(listFocus > 2) {
                            setData(null, page+1);
                            setListFocus(0);
                        } else {
                            setListFocus(listFocus);
                        }
                    }
            return true;
        case KEY_CODE.ENTER:
            log.printDbg("list Enter process");
            // 장치 등록 프로세스..
            selectedDevice = device_list[page * 3 + listFocus];

            //TODO  [sw.nam] Loading bar timeout,
            LayerManager.startLoading({
                preventKey: true,
                params: {
                    boxSize: {
                        left: 652, top: 399, width: 616, height: 296
                    }
                },
                cbTimeout: onStopLoading,
                timeout: 90 *1000
            });

            // [jh.lee] 디바이스 연결 응답에 대한 콜백 등록
            bluetoothDevice.setEventListener(onAddDevice);
            bluetoothDevice.addDevice(selectedDevice);

            return true;
        }
    }

    function onKeyForButton(keyCode) {
        switch (keyCode) {
            case KEY_CODE.LEFT:
            case KEY_CODE.RIGHT:
                setButtonFocus(btnFocus^1);
                log.printDbg("btnFocus =="+btnFocus);
                return true;
            case KEY_CODE.UP:
                if(device_list_length > 0 && LayerManager.isShowLoading() === false) {
                    //   if(setMainFocus(0)) setListFocus(page == Math.floor((device_list.length-1)/3)?(device_list.length-1)%3:2);
                    setMainFocus(0);
                    setListFocus(page == Math.floor((device_list.length-1)/3)?(device_list.length-1)%3:2);
                }
                return true;
            case KEY_CODE.DOWN:
                if(device_list_length > 0 && LayerManager.isShowLoading() === false) {
                    setMainFocus(0);
                    setListFocus(0);
                    //if(setMainFocus(0)) setListFocus(0);
                }
                return true;
            case KEY_CODE.ENTER:
                if(btnFocus==0) {
                    // 재검색 또는 중지
                    if(state == 0 || LayerManager.isShowLoading() === true) {
                        log.printDbg("중지합니다");
                        stopSearching();
                    }
                    else {
                        log.printDbg("재검색 합니다.");
                        listFocus = 0;
                        search();
                    }
                }else {
                    //검색 중 취소
                    if(state == 0 || LayerManager.isShowLoading() === true) {
                        bluetoothDevice.stopScan();
                        LayerManager.stopLoading();
                    }
                    KTW.ui.LayerManager.deactivateLayer({
                        id: KTW.ui.Layer.ID.SETTING_BLUETOOTH_DEVICE_SEARCH_POPUP,
                        remove: true
                    });
                }
                return true;
        }
    }

    var searchIdx;
    function search() {

        // 검색완료 후 받을 콜백 등록
        // 블루투스 기기 검색 로딩의 경우 최대 1분까지 검색
        // 블루투스 기기가 최대 1분 검색하다가 안되면 신호를 보내주는 것이 정상. 고로 App 에서 생성하는 팝업은 그 신호가 오지 못할 경우를 대비해 1분30초로 변경
        // 대신 팝업 생성은 하지 않고, stopScan 함으로써 그동안 검색된 리스트가 있으면 보여주고 없으면 검색안된 문구로 갱신한다. 신호는 onSearchResult 로 이동한다
        //TODO  [sw.nam] Loading bar timeout 이 제대로 안되는것 같은데 블루투스 등록 과정에서의 문제인지, startLoading API 와의 충돌인지 확인해야함.
        LayerManager.startLoading({
            preventKey: false,
            params: {
                boxSize: {
                    left: 652, top: 399, width: 616, height: 296
                }
            },
            cbTimeout: onStopLoading,
            timeout: 60 * 1000
        });

        setState(STATE.SEARCHING);
        bluetoothDevice.startScan();
        bluetoothDevice.setEventListener(onSearchResult);
    }

    function stopSearching() {
        log.printDbg("stopSearching()");

        bluetoothDevice.stopScan();
    }

    /**
     * [jh.lee] 검색 결과에 대한 신호가 로딩 타임아웃 (최대1분30초)을 넘어가는 경우 로딩이 닫히면서 에러팝업이 생성됨
     * [jh.lee] 이러한 경우 에러팝업을 생성과 동시에 블루투스 검색 로직을 중지시켜야한다.
     */
    function onStopLoading() {
        // [jh.lee] 로딩 타임아웃이 종료될 때까지 검색에 성공하지 못한것 이기 때문에 종료 직전까지의 결과를 바탕으로 리스트 갱신한다.
        log.printDbg("onStopLoading()");
        bluetoothDevice.stopScan();
    }

    /**
     * [jh.lee] 검색결과에 대한 콜백
     * @param event_type
     * @param completed
     */
    function onSearchResult(event_type, completed) {
        log.printDbg("onSearchResult");
        log.printDbg("event_type : " + event_type);
        log.printDbg("completed : " + completed);

        window.deviceLst = device_list = bluetoothDevice.deviceList; // 이건 탐색된 전체 리스트를 말하는거 같다.
        //[sw.nam] TODO 탐색 결과 리스트 확인 시 블루투스 리모컨 장치가 검색되는 경우에 대한 추가 예외처리 필요 (WEBIIIHOME-3666)

        if ( device_list && device_list.length !== undefined && device_list.length !== null) {
            device_list_length = device_list.length;
            log.printDbg("device_list.length : " + device_list.length);
        }

        if (completed === true) {
            // completed 가 true 이면 무조건 검색이 끝난 것이기 때문에 화면 갱신
            log.printDbg("검색 완료");
            setData(device_list, 0);

            if (bluetoothDevice.deviceList) {
                log.printDbg("list length : " + device_list_length);
            }
            // [jh.lee] 도중에 중지하거나 검색이 완료되거나 한 경우에만 completed 가 true
            bluetoothDevice.setEventListener(null);
            LayerManager.stopLoading();

            if (device_list_length === 0) {
                setState(STATE.NO_RESULT);
            } else {
                setState(STATE.HAS_RESULT);
            }
        } else {
            // complited가 true 가 아니면 검색이 진행 중이기 때문에 갯수에 따라 갱신 여부를 정한다
            if (device_list && device_list.length !== undefined && device_list.length !== null &&
                device_list.length > 0) {
                log.printDbg("찾았다, 검색 지속");
                setData(device_list, 0, true);
            } else {
                //TODO 해당 상황에 대한 예외 케이스 확인해야 함
                log.printDbg("TODO fHave to find this case that what it is ");
            }
        }
    }
    function onAddDevice(type, event) {
        var state;
        state = event.state;

        if (state === BluetoothInterface.STATE_STARTED) {
            // 타임아웃이 될 때까지 페어링이 안되서 연결 성공 처리하는 부분
            // 실제로 연결 성공 콜백을 호출하지만 해당 파라미터로 데이터를 전송하지 않음
            log.printDbg("onAddDevice BluetoothInterface.STATE_STARTED");
            //TODO sw.nam 이런 경우가 실제로 일어나나 ? 그럼 실제 연결은 이루어지지 않는다..
            onConnectSuccess();
        } else if (state === BluetoothInterface.STATE_COMPLETE) {
            // 실제로 디바이스 연결 성공하는 경우. event 정보를 전달해준다.(device 를 전달하기 위해)
            log.printDbg("onAddDevice BluetoothInterface.STATE_COMPLETE");
            onConnectSuccess(event);
        } else if (state === BluetoothInterface.STATE_FAILED) {
            // 디바이스 연결이 실패한 경우. 검색팝업은 숨기고 실패 팝업을 생성한다.
            log.printDbg("onAddDevice BluetoothInterface.STATE_FAILED");
            onConnectFail(event);
        } else {
            log.printDbg("onAddDevice other state : " + state);
        }
    }

    /**
     * [jh.lee] 연결 성공 콜백(단 device 가 존재하는 경우만)
     * @param device
     */
    function onConnectSuccess(event) {
        log.printDbg("onConnectSuccess");

        if (event && event.device) {
            log.printDbg("onConnectSuccess event.device : " + event.device);
            log.printDbg("onConnectSuccess device : " + event.device.name);
        }

        // [jh.lee] 성공 후 종료해야할 로직
        // [jh.lee] 콜백 제거, 팝업 제거, 로딩 제거

        LayerManager.stopLoading();
        bluetoothDevice.setEventListener(null);
        data.complete(event);
        // LayerManager.historyBack();

    }

    function onConnectFail(event) {
        log.printDbg("onConnectFail");

        if (event && event.device) {
            log.printDbg("onConnectFail event.device : " + event.device);
            log.printDbg("onConnectFail device : " + event.device.name);
        }
        // 실패 후 처리해야할 로직
        // 로딩 제거, 콜백 제거, 팝업 숨김
        LayerManager.stopLoading();
        bluetoothDevice.setEventListener(null);

        var device = event.device;
        var key = "CONNECTION_FAIL";
        //data.complete(event.device,key);
        KTW.managers.service.SimpleMessageManager.showMessageTextOnly(event.device.name+" 블루투스 장치 연결에 실패하였습니다");
        //실패하면 다시 재 검색 시작
        search();
    };

};

KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup.prototype = new KTW.ui.Layer();
KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup.constructor = KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup;

KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup.prototype.create = function(cbCreate) {
    KTW.ui.Layer.prototype.create.call(this);
    this.init(cbCreate);

};

KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup.prototype.handleKeyEvent = function (key_code) {
    return this.controlKey(key_code);
};

})();