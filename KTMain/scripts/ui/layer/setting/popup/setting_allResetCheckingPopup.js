/**
 * Created by Yun on 2017-03-02.
 */


(function() {
    KTW.ui.layer.setting.popup.setting_allResetCheckingPopup  = function allResetCheckingPopup(options) {
        KTW.ui.Layer.call(this, options);
        var div, btnText = ["확인", "취소"];
        var focusIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var data;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup allResetCheckingPopup");
            div.append($("<title/>").css({"position": "absolute", "top": "403px"}).text("전체 초기화"));
            div.append($("<span/>", {class: "text_white_center_52M"}).text("초기화하시겠습니까?"));
            div.append($("<span/>", {class: "text_white_center_38L"}).text("확인버튼을 선택하면 모든 개인 정보가 삭제되고 재부팅됩니다"));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
            data = this.getParams();
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusIdx == 0) {
                        LayerManager.historyBack();
                        data.complete();

                    } else {
                        LayerManager.historyBack();
                    }
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }
    };

    KTW.ui.layer.setting.popup.setting_allResetCheckingPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_allResetCheckingPopup.constructor = KTW.ui.layer.setting.popup.setting_allResetCheckingPopup;

    KTW.ui.layer.setting.popup.setting_allResetCheckingPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_allResetCheckingPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})();