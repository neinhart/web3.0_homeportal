/**
 * Created by Yun on 2017-02-01.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_setLNB  = function setLNBPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div, data;
        var focusRow = 0;
        var focusIdx = 0;
        var btnText = ["저장", "취소"];
        //var selectTxt = ["설정", "해제"];
        //var tmpFrequency = ["10750", "10750"];

        var selectedLNB;
        var selectedTONE;
        var inputFrequency;


        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var hwAdapter = KTW.oipf.adapters.HWAdapter;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var LNB_POWER = [
            {index: 0, name : "설정"},
            {index: 1, name : "해제"}
        ];
        var TONE = [
            {index: 0, name : "설정"},
            {index: 1, name : "해제"}
        ];

        var ROW_VAL = {
            POWER : 0,
            TONE : 1,
            FREQUENCY: 2,
            SAVE_CANCEL:3
        };



        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup setLNBPopup");
            div.append($("<title/>").text("LNB 설정"));
            div.append($("<span/>", {id:"selectLNBPopup_text", class: "text_white_center_45M"}));
            div.append($("<span/>", {class: "text_white_center_30L"}).text("위성의 세부 내용을 변경할 수 있습니다"));
            div.append($("<div/>", {id: "setLNBPopup_area"}));
            div.find("#setLNBPopup_area").append($("<div/>", {id:"setLNBPopup_optionTitle_area"}));
            div.find("#setLNBPopup_optionTitle_area").append($("<div/>", {class:"setLNBPopup_optionDiv"}).html("<div class='setLNBPopup_optionText'>LNB Power</div>"));
            div.find("#setLNBPopup_optionTitle_area").append($("<div/>", {class:"setLNBPopup_optionDiv"}).html("<div class='setLNBPopup_optionText'>22kHz Tone</div>"));
            div.find("#setLNBPopup_optionTitle_area").append($("<div/>", {class:"setLNBPopup_optionDiv"}).html("<div class='setLNBPopup_optionText'>Frequency</div>"));
            div.find("#setLNBPopup_area").append($("<div/>", {id:"setLNBPopup_optionSelect_area"}));
            div.find("#setLNBPopup_optionSelect_area").append($("<div/>", {class:"setLNBPopup_optionSelectDiv"}).html("<div class='setLNBPopup_optionSelectText'>설정</div>"));
            div.find("#setLNBPopup_optionSelect_area").append($("<div/>", {class:"setLNBPopup_optionSelectDiv"}).html("<div class='setLNBPopup_optionSelectText'>해제</div>"));
            div.find("#setLNBPopup_optionSelect_area").append($("<div/>", {class:"setLNBPopup_optionSelectDiv"}).html("<div class='setLNBPopup_optionSelectText'>10750</div>"));
            div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv").append($("<div/>", {class:"setLNBPopup_optionSelectLine"}));
            div.find("#setLNBPopup_area").append($("<div/>", {id:"setLNBPopup_focus_area"}));
            div.find("#setLNBPopup_focus_area").append($("<img>", {id:"setLNBPopup_focusArrow_left", src:"images/setting/vod_end_ar_l.png"}));
            div.find("#setLNBPopup_focus_area").append($("<img>", {id:"setLNBPopup_focusArrow_right", src:"images/setting/vod_end_ar_r.png"}));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            log.printDbg("show()");
            log.printDbg(data.selectedLNB);
            div.find("#selectLNBPopup_text").text(data.selectedLNB.name);
            div.find(".setLNBPopup_optionSelectText").eq(0).text(data.selectedLNB.info[0] == 0 ? LNB_POWER[0].name : LNB_POWER[1].name);
            div.find(".setLNBPopup_optionSelectText").eq(1).text(data.selectedLNB.info[1] == 0 ? TONE[0].name : TONE[1].name);
            div.find(".setLNBPopup_optionSelectText").eq(2).text(data.selectedLNB.info[2]);

            inputFrequency = data.selectedLNB.info[2].toString();
            //div.find(".setLNBPopup_optionSelectText").eq(2).text(inputFrequency);
            focusIdx = 0;  // 가로
            focusRow = ROW_VAL.POWER;
            focusRowChange(focusRow);
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            var consumed = false;
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2);
                    focusIdxChange(keyCode);
                    consumed = true;
                    return consumed;
                case KEY_CODE.RIGHT:
                    focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2);
                    focusIdxChange(keyCode);
                    consumed = true;
                    return consumed;
                case KEY_CODE.ENTER:
                    if (focusRow == 3) {
                        if (focusIdx == 0) {
                            //저장
                            if (inputFrequency.length === 0) {
                                inputFrequency = "0";
                            }

                            if(KTW.PLATFORM_EXE === "STB") {
                                // [sw.nam] STB 형상에서만 저장한다. PC에서는 저장안함

                                log.printDbg("data.selectedLNB.port"+data.selectedLNB.port);
                                hwAdapter.setLNB(parseInt(data.selectedLNB.port), [selectedLNB, selectedTONE, parseInt(inputFrequency)]);
                                data.complete();
                            }

                        } else {
                            LayerManager.historyBack();
                        }
                    } else{
                        focusIdx = 0;
                        focusRow++;
                        if(focusRow>3) {
                            focusRow = 3;
                        }
                        focusRowChange(focusRow);
                    }
                    consumed = true;
                    return consumed;
                case KEY_CODE.UP:

                    focusRow = KTW.utils.util.getIndex(focusRow, -1 , 4);
                    focusRowChange(focusRow);
                    consumed = true;
                    return consumed;
                case KEY_CODE.DOWN:
                    focusRow = KTW.utils.util.getIndex(focusRow, 1 , 4);
                    focusRowChange(focusRow);
                    consumed = true;
                    return consumed;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    consumed = true;
                    return consumed;

            }
            // [sw.nam] frequency 항목만  숫자키 소모하도록
            if(focusRow == ROW_VAL.FREQUENCY) {
                if (consumed === false) {
                    var number = util.keyCodeToNumber(keyCode);
                    if (number >= 0) {
                        keyNumber(number);
                        consumed = true;
                    }
                    return consumed;
                }
            }

            return consumed;

        };

        function keyNumber(number) {
            log.printDbg("keyNumber()");

            if (("" + inputFrequency).length < 15) {
                inputFrequency += "" + number;
            }
            div.find(".setLNBPopup_optionSelectText").eq(2).text(inputFrequency);

        }
        function keyDel() {
            log.printDbg("keyDel()");
            log.printDbg("inputFrequncy == "+inputFrequency);
            if (inputFrequency.length > 0) {
                log.printDbg("inputFrequncy hit== ");
                inputFrequency = inputFrequency.substring(0, inputFrequency.length - 1);
            }
            div.find(".setLNBPopup_optionSelectText").eq(2).text(inputFrequency);
        }

        function focusIdxChange(keyCode) {
            log.printDbg("focusIdxChange()");

            switch(focusRow) {
                case ROW_VAL.POWER:
                    div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv:eq(" + focusRow + ") .setLNBPopup_optionSelectText").text(LNB_POWER[focusIdx].name);
                    selectedLNB = focusIdx;
                    log.printDbg("selectedLNB ::" + selectedLNB);
                    break;
                case ROW_VAL.TONE:
                    div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv:eq(" + focusRow + ") .setLNBPopup_optionSelectText").text(TONE[focusIdx].name);
                    selectedTONE = focusIdx;
                    log.printDbg("selectedLNB ::" + selectedTONE);
                    break;
                case ROW_VAL.FREQUENCY:
                    if(keyCode == KEY_CODE.LEFT){
                        keyDel();
                    }
                    break;

                case ROW_VAL.SAVE_CANCEL:
                    // 저장 % 취소 버튼
                    div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
                    div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
                    break;
            }
        }

        function focusRowChange(_row) {
            div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv").removeClass("focus");
            div.find("#setLNBPopup_focus_area").removeClass("focus");
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv .setLNBPopup_optionSelectText").removeClass("focus");
            div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv .setLNBPopup_optionSelectLine").removeClass("focus");
            if (_row == 3) {
                focusIdx = 0;
                focusIdxChange();
            } else {

                div.find("#setLNBPopup_focus_area").addClass("focus");
                div.find("#setLNBPopup_focus_area").css("top", (26 + (_row * 85)) + "px");
                div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv:eq(" + _row + ")").addClass("focus");
                div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv:eq(" + _row + ") .setLNBPopup_optionSelectText").addClass("focus");
                div.find("#setLNBPopup_optionSelect_area .setLNBPopup_optionSelectDiv:eq(" + _row + ") .setLNBPopup_optionSelectLine").addClass("focus");
            }
        }
    };
    KTW.ui.layer.setting.popup.setting_setLNB.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_setLNB.constructor = KTW.ui.layer.setting.popup.setLNB;

    KTW.ui.layer.setting.popup.setting_setLNB.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_setLNB.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();