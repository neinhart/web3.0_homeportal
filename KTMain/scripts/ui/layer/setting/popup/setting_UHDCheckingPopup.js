/**
 * Created by Yun on 2017-03-03.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_uhdCheckingPopup  = function uhdCheckingPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;
        var btnText = ["확인", "취소"];
        var focusIdx = 0;

        var data;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        
        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup uhdCheckingPopup");
            div.append($("<title/>").css({"position":"absolute", "top":"358px"}).text("알림"));
            div.append($("<span/>", {class:"text_white_center_52M"}).text("선택하신 해상도는 UHD TV에서만 지원됩니다"));
            div.append($("<span/>", {class:"text_white_center_38L"}).html("계속 진행하시겠습니까?"));
            div.append($("<span/>", {class:"text_white_center_30L"}).html("※ UHD TV일 경우 지원되는 최대 프레임수를 확인하세요<br>예시) 30fps, 60fps"));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
            data = this.getParams();
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusIdx == 0) {
                        data.complete();
                    } else {
                        //data.cancel();
                        LayerManager.historyBack();
                    }
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }
    };

    KTW.ui.layer.setting.popup.setting_uhdCheckingPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_uhdCheckingPopup.constructor = KTW.ui.layer.setting.popup.setting_uhdCheckingPopup;

    KTW.ui.layer.setting.popup.setting_uhdCheckingPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_uhdCheckingPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();