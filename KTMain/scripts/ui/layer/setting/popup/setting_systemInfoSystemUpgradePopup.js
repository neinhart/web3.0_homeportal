/**
 * Created by sw.nam on 2017-03-12.
 */


(function() {
    KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup  = function systemInfoSystemUpgradePopup(options) {
        KTW.ui.Layer.call(this, options);
        var div;
        var focusIdx = 0;

        var log = KTW.utils.Log;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var data;

        this.init = function(cbCreate) {
            log.printDbg("init()");

            data = this.getParams();

            div = this.div;
            div.addClass("arrange_frame setting gniPopup systemInfoPopup");
            div.append($("<title/>").css({"position": "absolute", "top": "403px"}).text("시스템 업그레이드"));

            createComponents(data.result);
            cbCreate(true);
        };

        this.show = function() {
            log.printDbg("show()");

            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            log.printDbg("hide()");
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(data.result == 0)
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    if(data.result == 0)
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if(focusIdx == 0) {
                        data.complete(data.result);
                    }
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }

        function createComponents(result) {
            log.printDbg("createComponents()");

            // 2017.07.24 sw.nam 시스템 업그레이드 응답에 따른 처리 로직 수정
            // result
            // 0 : AC_FWDN_RESULT_OK
            // 1 : AC_FWDN_RESULT_NONED
            // 2 : AC_FWDN_RESULT_FAIL
            // 규격서에는 navAdapter.requestFirmwareDownload API에 대해 3가지 응답 case 를 가지고 있으나
            // 실제로 홈포탈에 전달 될 수 있는 값은 AC_FWDN_RESULT_OK or AC_FWDN_RESULT_NONED 2가지 임
            if(result == 0) {
                // 업그레이드 될 펌웨어가 있는 경우
                // 해당 팝업은 네비에서 띄우므로 no action
                // 펌웨어 업그레이드를 실행 한 경우 성공 여부는 홈포탈에서 알 수 없음
                // 성공 여부에 관계 없이 자동으로 리부팅이 이루어짐
                return ;
            } else {
                // 현재 소프트웨어가 최신인 경우 팝업을 띄운다
                div.append($("<span/>", {class: "text_white_center_52M"}).text("현재 소프트웨어가 최신 버전입니다"));
                div.append($("<div/>", {class: "whiteBtn"}).css({"position": "absolute" }));
                div.find(".whiteBtn").append($("<div/>", {class: "btn_line"}));
                div.find(".whiteBtn").append($("<span/>", {class: "text_black_center_30L"}).text("확인"));
                //div.find(".whiteBtn .btn_line").css({position: "absolute", width:280, height:62, "border-top" : "solid 5px rgb(209,67,63)", "border-bottom" :"solid 5px rgb(209,67,63)", "box-sizing": "border-box"})
            }
        }
    };

    KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup.constructor = KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup;

    KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})();