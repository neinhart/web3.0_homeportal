/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_resolutionUHD  = function resolutionUHDPopup(options) {
        KTW.ui.Layer.call(this, options);

        var data, div, btnText = ["확인", "취소"], idx;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        function changeFocus() {
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(idx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup resolutionUHD");
            div.append($("<title/>").text("알림"));
            div.append($("<span/>", {class: "text_white_center_52L"}).text("선택하신 해상도는 UHD TV에서만 지원됩니다"));
            div.append($("<span/>", {class: "text_white_center_38L"}).text("계속 진행하시겠습니까?"));
            div.append($("<span/>", {class: "text_white_center_30L"}).html("※ UHD TV일 경우 지원되는 최대 프레임수를 확인하세요<br>예시) 30fps, 60fps"));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            idx = 0;
            changeFocus();
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    idx = KTW.utils.util.indexMinus(idx, 2);
                    changeFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    idx = KTW.utils.util.indexPlus(idx, 2);
                    changeFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if (idx == 0 && data.callback) data.callback();
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_resolutionUHD.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_resolutionUHD.constructor = KTW.ui.layer.setting.popup.setting_resolutionUHD;

    KTW.ui.layer.setting.popup.setting_resolutionUHD.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_resolutionUHD.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();