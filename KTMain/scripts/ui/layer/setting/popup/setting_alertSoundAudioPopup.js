/**
 * Created by Yun on 2017-03-05.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup  = function AlertSoundAudioPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;
        var data;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup alertSoundAudioPopup");
            div.append($("<title/>").css({"position":"absolute", "top":"180px"}).text("알림"));
            div.append($("<img/>", {class:"alertPopupBG_l_img", src:"images/pop_bg_noti_l.png"}));
            div.append($("<img/>", {class:"alertPopupBG_m_img", src:"images/pop_bg_noti_m.png"}));
            div.append($("<img/>", {class:"alertPopupBG_r_img", src:"images/pop_bg_noti_r.png"}));
            div.append($("<img/>", {class:"alertPopupNum_1_img", src:"images/popup/pop_num_1.png"}));
            div.append($("<img/>", {class:"alertPopupNum_2_img", src:"images/popup/pop_num_2.png"}));
            div.append($("<span/>", {class:"text_white_center_42M"}).text("Dolby AC3 설정안내"));
            div.append($("<span/>", {class:"text_white_left_30L"}).html("Dolby AC3 설정 시, 셋톱박스 음량이 조절되지 않으며<br>TV 음량만 조절이 가능합니다"));
            div.append($("<span/>", {class:"text_white_left_30L"}).html("TV/스피커 성능에 따라 영상과 음성의 Sync가<br>맞지 않을 수 있습니다"));
            div.append($("<span/>", {class:"text_white_center_27L"}).text("(문제 발생 시 2채널 (Stereo AAC)로 설정하시기 바랍니다)"));
            div.append($("<div/>", {class:"oneBtnArea_280"}));
            div.find(".oneBtnArea_280").append($("<div/>", {class: "btn focus"}));
            div.find(".oneBtnArea_280 .btn").append($("<div/>", {class: "whiteBox"}));
            div.find(".oneBtnArea_280 .btn").append($("<span/>", {class: "btnText"}).text("확인"));

            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            Layer.prototype.show.call(this);
            if(data.requester == "GIGAGENIE") {
                div.find(".text_white_center_42M").text("기가지니 오디오 출력 안내");
                div.find(".text_white_left_30L").eq(0).html("블루투스 오디오 기기와 기가지니 스피커를 같이<br>사용할 경우 블루투스 기기에서만 음성이 나올 수 있습니다");
                div.find(".text_white_left_30L").eq(1).html("TV 모델에 따라 영상과 음성의 Sync가<br>맞지 않을 수 있습니다");
                div.find(".text_white_center_27L").text("(문제 발생 시, 'TV로 듣기'로 설정하시기 바랍니다)");
            }
            else {
                div.find(".text_white_center_42M").text("Dolby AC3 설정안내");
                div.find(".text_white_left_30L").eq(0).html("Dolby AC3 설정 시, 셋톱박스 음량이 조절되지 않으며<br>TV 음량만 조절이 가능합니다");
                div.find(".text_white_center_27L").text("(문제 발생 시 2채널 (Stereo AAC)로 설정하시기 바랍니다)");
            }

        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    data.complete();
                    LayerManager.historyBack();
                    //data.complete();
                    return true;
                case KEY_CODE.BACK:
                   // data.cancel();
                    LayerManager.historyBack();
                    return true;

                default:
                    return false;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup.constructor = KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup;

    KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})();