/**
 * Created by Taesang on 2016-12-30.
 */

(function() {

    KTW.ui.layer.setting.popup.setting_changePassword  = function changePasswordPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div, data, btnText = ["저장", "취소"];
        var focusRow = 0, focusIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var changePasswordType = 0;

        var CURRENT_PASSWORD_TYPE = 0;
        var NEW_PASSWORD_TYPE = 1;
        var NEW_CONFIRM_PASSWORD_TYPE = 2;

        var isSystemPriority = false;

        var isStartSSpeechRecognizer = false;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup changePassword");
            div.append($("<title/>"));
            div.append($("<span/>", {class: "text_white_center_38M"}));
            div.append($("<div/>", {class: "passwordArea nowPassword"}));
            div.append($("<div/>", {class: "passwordArea newPassword1"}));
            div.append($("<div/>", {class: "passwordArea newPassword2"}));
            /*
            div.find(".passwordArea.nowPassword").append($("<span/>", {class: "text_white_left_30L"}).text("현재 비밀번호"));
            div.find(".passwordArea.newPassword1").append($("<span/>", {class: "text_white_left_30L"}).text("새 비밀번호"));
            div.find(".passwordArea.newPassword2").append($("<span/>", {class: "text_white_left_30L"}).text("새 비밀번호 확인"));
            */
            div.find(".passwordArea").append($("<div/>", {class: "inputBox"}));
            div.find(".passwordArea").append($("<span/>", {class: "password"}).text("* * * *"));
            div.find(".passwordArea").append($("<span/>", {class: "password fill"}));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            log.printDbg('show()');
            data = this.getParams();

            if(data !== undefined && data !== null) {
                changePasswordType = data.change_password_type;
                isSystemPriority = data.isSystemPriority;
                log.printDbg('show() changePasswordType : ' + changePasswordType);
            }

            KTW.managers.auth.AuthManager.initPW(CURRENT_PASSWORD_TYPE);
            KTW.managers.auth.AuthManager.initPW(NEW_PASSWORD_TYPE);
            KTW.managers.auth.AuthManager.initPW(NEW_CONFIRM_PASSWORD_TYPE);

            _fillPassword();
            focusRow = focusIdx = 0;
            _focusRowChange();
            // data.mode가 0이면 성인인증 프로세스, 1이면 구매인증 프로세스
            if (changePasswordType === 0) {
                div.find("title").text("olleh tv 비밀번호(성인인증) 변경");
                div.find(".passwordArea.nowPassword").append($("<span/>", {class: "text_white_left_30L"}).text("현재 비밀번호"));
                div.find(".passwordArea.newPassword1").append($("<span/>", {class: "text_white_left_30L"}).text("새 비밀번호"));
                div.find(".passwordArea.newPassword2").append($("<span/>", {class: "text_white_left_30L"}).text("새 비밀번호 확인"));
                div.find(".text_white_center_38M").html("현재 비밀번호와 변경하실 비밀번호를<br>입력해 주세요");
            } else {
                div.find("title").text("구매인증 번호 변경");
                div.find(".passwordArea.nowPassword").append($("<span/>", {class: "text_white_left_30L"}).text("현재 인증번호"));
                div.find(".passwordArea.newPassword1").append($("<span/>", {class: "text_white_left_30L"}).text("새 인증번호"));
                div.find(".passwordArea.newPassword2").append($("<span/>", {class: "text_white_left_30L"}).text("새 인증번호 확인"));
                div.find(".text_white_center_38M").html("현재 인증번호와 변경하실 인증번호를<br>입력해 주세요");
            }
            //div.find("title").text(changePasswordType === 0 ? "olleh tv 비밀번호(성인인증) 변경" : "구매인증 번호 변경");
            //div.find(".text_white_center_38M").html("현재 비밀번호와 변경하실 비밀번호를<br>입력해 주세요");
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);
            }
            Layer.prototype.show.call(this);
        };


        this.hide = function() {
            Layer.prototype.hide.call(this);
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);
            }

            //_stopSpeechRecognizer();
        };

        this.controlKey = function(keyCode) {
            log.printDbg('controlKey() keyCode : ' + keyCode + " , focusRow : " + focusRow);
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (focusRow === 3) {
                        focusIdx = util.indexMinus(focusIdx, 2);
                        _focusIdxChange();
                    } else {
                        var pw = KTW.managers.auth.AuthManager.getPW(focusRow);
                        if(pw !== null && pw.length>0) {
                            _deleteNum(focusRow);
                            return true;
                        }else {
                            return false;
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (focusRow == 3) {
                        focusIdx = util.indexPlus(focusIdx, 2);
                        _focusIdxChange();
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 3) {
                        if (focusIdx == 0) {
                            _changePassword(changePasswordType);
                        }else {
                            _cancelAction();
                        }
                    }
                    return true;
                case KEY_CODE.UP:
                    focusRow--;
                    if (focusRow < 0) focusRow = 0;
                    _focusRowChange();
                    return true;
                case KEY_CODE.DOWN:
                    focusRow++;
                    if (focusRow > 3) focusRow = 3;
                    _focusRowChange();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    return true;
     /*           case KEY_CODE.EXIT:
/                    var consume = false;
                    if (this.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                        LayerManager.historyBack();
                        consume = true;
                    }
                    return consume;*/
                default:
                    var number = util.keyCodeToNumber(keyCode);
                    if (number >= 0) {
                        if (focusRow !== 3) {
                            _inputNum(number , 4 , focusRow);
                        }
                        return true;
                    }
                    if (isSystemPriority) {
                        return true;
                    }
                    break;
            }
        };


        function _getStarText(_passwordCnt) {
            var starText = "";
            if (_passwordCnt > 0) {
                for (var i = 0; i < _passwordCnt; i++) {
                    if (_passwordCnt == 4 && i == 3) starText += "*";
                    else starText += "* "
                }
            }
            return starText;
        }

        function _focusIdxChange() {
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        function _focusRowChange(_row) {
            if (_row != null) focusRow = _row;
            log.printDbg('_focusRowChange() focusRow : ' + focusRow);

            focusIdx = 0;
            div.find(".passwordArea.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            if (focusRow == CURRENT_PASSWORD_TYPE) {
                div.find(".passwordArea.nowPassword").addClass("focus");
                //_startSpeechRecognizer();
            }else if (focusRow == NEW_PASSWORD_TYPE) {
                div.find(".passwordArea.newPassword1").addClass("focus");
                //_startSpeechRecognizer();
            }else if (focusRow == NEW_CONFIRM_PASSWORD_TYPE) {
                div.find(".passwordArea.newPassword2").addClass("focus");
                //_startSpeechRecognizer();
            } else if (focusRow == 3) {
                _focusIdxChange();
                //_stopSpeechRecognizer();
            }
        }

        // 비밀번호 입력 (GUI)
        function _fillPassword(type) {
            log.printDbg('_fillPassword() type : ' + type);
            if(type === undefined) {
                div.find(".passwordArea.nowPassword .password.fill").text(_getStarText(KTW.managers.auth.AuthManager.getPW(CURRENT_PASSWORD_TYPE).length));
                div.find(".passwordArea.newPassword1 .password.fill").text(_getStarText(KTW.managers.auth.AuthManager.getPW(NEW_PASSWORD_TYPE).length));
                div.find(".passwordArea.newPassword2 .password.fill").text(_getStarText(KTW.managers.auth.AuthManager.getPW(NEW_CONFIRM_PASSWORD_TYPE).length));
            }else {
                if(type === CURRENT_PASSWORD_TYPE) {
                    div.find(".passwordArea.nowPassword .password.fill").text(_getStarText(KTW.managers.auth.AuthManager.getPW(CURRENT_PASSWORD_TYPE).length));
                }else if(type === NEW_PASSWORD_TYPE) {
                    div.find(".passwordArea.newPassword1 .password.fill").text(_getStarText(KTW.managers.auth.AuthManager.getPW(NEW_PASSWORD_TYPE).length));
                }else if(type === NEW_CONFIRM_PASSWORD_TYPE) {
                    div.find(".passwordArea.newPassword2 .password.fill").text(_getStarText(KTW.managers.auth.AuthManager.getPW(NEW_CONFIRM_PASSWORD_TYPE).length));
                }

            }
        }

        function _inputNum(num ,maxLength , type) {
            log.printDbg('_inputNum() num : ' + num + " , type : " + type);
            var authManager = KTW.managers.auth.AuthManager;

            var authMaxLength = 4;
            if(maxLength !== undefined && maxLength !== null) {
                authMaxLength = maxLength;
            }

            if (authManager.inputPW(num, authMaxLength , type) === true) {
                _fillPassword(type);
                if(type === CURRENT_PASSWORD_TYPE) {
                    _checkPW();
                }else if(type === NEW_PASSWORD_TYPE){
                    focusRow++;
                    _focusRowChange();
                }else if(type === NEW_CONFIRM_PASSWORD_TYPE) {
                    var result = authManager.confirmNewPW();
                    if (result === authManager.RESPONSE.CORRECT_PASSWORD) {
                        // [jh.lee] 4자리 체크, 새로운 비밀번호와 확인 비밀번호 체크가 성공적인 경우
                        // [jh.lee] 저장 버튼으로 이동
                        focusRow++;
                        _focusRowChange();
                    } else {
                        if(changePasswordType === 0) {
                            _changeErrorMessage(result);
                        } else {
                            _changeErrorBuyMessage(result);
                        }
                    }
                }
            }
            else {
                _fillPassword(type);
            }
        }

        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum(type) {
            log.printDbg('_deleteNum() type : ' + type);
            var authManager = KTW.managers.auth.AuthManager;
            authManager.deletePW(type);
            _fillPassword(type);
        }


        /**
         * 입력한 비밀번호 일치여부 확인
         */
        function _checkPW() {
            log.printDbg('_checkPW() ');
            var authManager = KTW.managers.auth.AuthManager;

            if(changePasswordType === 0) {
                var obj = {
                    type : authManager.AUTH_TYPE.AUTH_CURRENT_ADULT_PIN,
                    callback : _callbackFuncAuthCurrentPassword,
                    loading : {
                        type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                        lock : true,
                        callback : null,
                        on_off : false
                    }
                };

            }else {
                var obj = {
                    type : authManager.AUTH_TYPE.AUTH_CURRENT_BUY_PIN,
                    callback : _callbackFuncAuthBuyPassword,
                    loading : {
                        type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                        lock : true,
                        callback : null,
                        on_off : false
                    }
                };

            }
            authManager.checkUserPW(obj);
        }

        /**
         * 성인 인증 번호 체크 결과
         */
        function _callbackFuncAuthCurrentPassword(result) {
            log.printDbg('_callbackFuncAuthCurrentPassword() result : ' + result);
            var authManager = KTW.managers.auth.AuthManager;

            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_SUCCESS) {
                div.find(".text_white_center_38M").html("현재 비밀번호와 변경하실 비밀번호를<br>입력해 주세요");

                focusRow++;
                _focusRowChange();
            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_FAIL ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_ERROR) {
                // 2017.10.10 dhlee
                // WEBIIIHOME-3403 이슈 수정
                // AUTH Fail 상황에서 HDS로부터 수신된 에러 메시지가 있는 경우 AUTH_FAIL이 아닌 AUTH_ERROR를 전달하도록 한다.
                // AUTH_ERROR를 전달 받은 경우 토스트 팝업을 실행하지 않도록 한다.
                div.find(".text_white_center_38M").html("잘못된 비밀번호입니다" + "<br>" + "현재 비밀번호를 다시 입력해 주세요");

                authManager.initPW(focusRow);
                _fillPassword(focusRow);
            } else if (result === authManager.RESPONSE.DISCONNECTED_NETWORK) {
                div.find(".text_white_center_38M").html("통신 서버 오류입니다" + "<br>" + "잠시 후 다시 입력해주세요");

                authManager.initPW(focusRow);
                _fillPassword(focusRow);
            }
        }


        /**
         * 구매 인증 번호 체크 결과
         */
        function _callbackFuncAuthBuyPassword(result) {
            log.printDbg('_callbackFuncAuthBuyPassword() result : ' + result);
            var authManager = KTW.managers.auth.AuthManager;

            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_SUCCESS) {
                div.find(".text_white_center_38M").html("현재 인증번호와 변경하실 인증번호를<br>입력해 주세요");

                focusRow++;
                _focusRowChange();
            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_FAIL ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_ERROR) {
                // 2017.10.10 dhlee
                // WEBIIIHOME-3403 이슈 수정
                // AUTH Fail 상황에서 HDS로부터 수신된 에러 메시지가 있는 경우 AUTH_FAIL이 아닌 AUTH_ERROR를 전달하도록 한다.
                // AUTH_ERROR를 전달 받은 경우 토스트 팝업을 실행하지 않도록 한다.
                div.find(".text_white_center_38M").html("잘못된 인증번호입니다" + "<br>" + "현재 인증번호를 다시 입력해 주세요");

                authManager.initPW(focusRow);
                _fillPassword(focusRow);
            } else if (result === authManager.RESPONSE.DISCONNECTED_NETWORK) {
                div.find(".text_white_center_38M").html("통신 서버 오류입니다" + "<br>" + "잠시 후 다시 입력해주세요");

                authManager.initPW(focusRow);
                _fillPassword(focusRow);
            }
        }

        /**
         * 새로운 비밀번호와 확인 비밀번호 검증이 실패하는 경우 문구 변경 또는 포커스 위치 변경
         */
        function _changeErrorMessage(result) {
            log.printDbg('_changeErrorMessage() result : ' + result);

            var authManager = KTW.managers.auth.AuthManager;
            if (result === authManager.RESPONSE.WRONG_LENGTH
                || result === authManager.RESPONSE.MISMATCH_PASSWORD) {
                authManager.initPW(NEW_PASSWORD_TYPE);
                _fillPassword(NEW_PASSWORD_TYPE);
                authManager.initPW(NEW_CONFIRM_PASSWORD_TYPE);
                _fillPassword(NEW_CONFIRM_PASSWORD_TYPE);
                focusRow = NEW_PASSWORD_TYPE;
                _focusRowChange();
            }

            if (result === authManager.RESPONSE.MISMATCH_PASSWORD) {
                div.find(".text_white_center_38M").html("입력한 새 비밀번호가 서로 다릅니다" + "<br>" + "다시 입력해 주세요");
            }
        }

        /**
         * 새로운 비밀번호와 확인 비밀번호 검증이 실패하는 경우 문구 변경 또는 포커스 위치 변경
         */
        function _changeErrorBuyMessage(result) {
            log.printDbg('_changeErrorBuyMessage() result : ' + result);
            var authManager = KTW.managers.auth.AuthManager;
            if (result === authManager.RESPONSE.WRONG_LENGTH
                || result === authManager.RESPONSE.MISMATCH_PASSWORD) {
                authManager.initPW(NEW_PASSWORD_TYPE);
                _fillPassword(NEW_PASSWORD_TYPE);
                authManager.initPW(NEW_CONFIRM_PASSWORD_TYPE);
                _fillPassword(NEW_CONFIRM_PASSWORD_TYPE);
                focusRow = NEW_PASSWORD_TYPE;
                _focusRowChange();
            }

            if (result === authManager.RESPONSE.MISMATCH_PASSWORD) {
                div.find(".text_white_center_38M").html("입력한 새 인증번호가 서로 다릅니다" + "<br>" + "다시 입력해 주세요");
            }
        }

        /**
         * 성인인증 비밀번호 변경인지/구매 인증 번호 변경에 따라 구분 하여 호출 해야 함.
         * @param type
         */
        function _changePassword(type) {
            log.printDbg('_changePassword() type : ' + type);
            var authManager = KTW.managers.auth.AuthManager;
            var obj;
            var auth_type;

            if (type === 0) {
                auth_type = authManager.AUTH_TYPE.CHANGE_ADULT_PIN;
            } else if (type === 1) {
                auth_type = authManager.AUTH_TYPE.CHANGE_BUY_PIN;
            }

            obj = {
                type : auth_type,
                callback : _callbackFunconAuth,
                loading : {
                    type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock : true,
                    callback : null,
                    on_off : false
                }
            };

            authManager.checkUserPW(obj);
        }


        function _callbackFunconAuth(result) {
            log.printDbg('_callbackFunconAuth() result : ' + result);
            var authManager = KTW.managers.auth.AuthManager;

            if (result === authManager.RESPONSE.CAS_CHANGE_SUCCESS ||
                result === authManager.RESPONSE.HDS_BUY_CHANGE_SUCCESS) {
                _successAction(result);
            } else if (result === authManager.RESPONSE.HDS_BUY_AUTH_FAIL ||
                result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.DISCONNECTED_NETWORK ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_ERROR) {
                _failAction(result);
            } else if (result === authManager.RESPONSE.DISCONNECTED_SMART_CARD) {
                // [jh.lee] S/C 카드가 제거된 경우
                _failAction(result);
            } else if (result === authManager.RESPONSE.WRONG_LENGTH ||
                result === authManager.RESPONSE.MISMATCH_PASSWORD) {
                _changeErrorMessage(result);
            }
        }

        function _successAction(result) {
            log.printDbg('_successAction() result : ' + result);

            var authManager = KTW.managers.auth.AuthManager;
            authManager.initPW(CURRENT_PASSWORD_TYPE);
            authManager.initPW(NEW_PASSWORD_TYPE);
            authManager.initPW(NEW_CONFIRM_PASSWORD_TYPE);

            KTW.managers.service.SimpleMessageManager.showMessageTextOnly("저장되었습니다");

            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_CHANGE_PASSWORD_POPUP
            });
        }

        function _cancelAction() {
            log.printDbg('_cancelAction() ');

            var authManager = KTW.managers.auth.AuthManager;
            authManager.initPW(CURRENT_PASSWORD_TYPE);
            authManager.initPW(NEW_PASSWORD_TYPE);
            authManager.initPW(NEW_CONFIRM_PASSWORD_TYPE);

            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_CHANGE_PASSWORD_POPUP
            });
        }

        function _failAction(result) {
            log.printDbg('_failAction() result : ' + result);
            var authManager = KTW.managers.auth.AuthManager;

            if (result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_FAIL ||
                result === authManager.RESPONSE.HDS_BUY_AUTH_ERROR) {
                if(changePasswordType === 0) {
                    div.find(".text_white_center_38M").html("잘못된 비밀번호입니다" + "<br>" + "현재 비밀번호를 다시 입력해 주세요");
                }else {
                    div.find(".text_white_center_38M").html("잘못된 인증번호입니다" + "<br>" + "현재 인증번호를 다시 입력해 주세요");
                }
                authManager.initPW(CURRENT_PASSWORD_TYPE);
                focusRow = CURRENT_PASSWORD_TYPE;
                _focusRowChange();
            } else if (result === authManager.RESPONSE.DISCONNECTED_NETWORK) {
                div.find(".text_white_center_38M").html("통신 서버 오류입니다" + "<br>" + "잠시 후 다시 입력해주세요");
                authManager.initPW(CURRENT_PASSWORD_TYPE);
                focusRow = CURRENT_PASSWORD_TYPE;
                _focusRowChange();
            } else if (result === authManager.RESPONSE.DISCONNECTED_SMART_CARD) {
                // [jh.lee] S/C 카드가 단절된 경우
                div.find(".text_white_center_38M").html("현재 비밀번호와 변경하실 비밀번호를" + "<br>" + "입력해 주세요");
                authManager.initPW(CURRENT_PASSWORD_TYPE);
                authManager.initPW(NEW_PASSWORD_TYPE);
                authManager.initPW(NEW_CONFIRM_PASSWORD_TYPE);

                focusRow = CURRENT_PASSWORD_TYPE;
                _focusRowChange();
            }
        }



        function _startSpeechRecognizer() {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() isStartSSpeechRecognizer : " + isStartSSpeechRecognizer);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                _stopSpeechRecognizer();
            }
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.start(true);
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            isStartSSpeechRecognizer = true;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() isStartSSpeechRecognizer :  " + isStartSSpeechRecognizer);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.stop();
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);

                isStartSSpeechRecognizer = false;
            }
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );

            if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                KTW.managers.auth.AuthManager.initPW(focusRow);
                _fillPassword(focusRow);

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>4) {
                        length = 4;
                    }

                    for(var i=0;i<length;i++) {
                        _inputNum(input[i] , length , focusRow);
                    }
                }
            }
        }

    };

    KTW.ui.layer.setting.popup.setting_changePassword.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_changePassword.constructor = KTW.ui.layer.setting.popup.setting_changePassword;

    KTW.ui.layer.setting.popup.setting_changePassword.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };

    KTW.ui.layer.setting.popup.setting_changePassword.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();