/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_btSearchFail  = function btSearchFailPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup btOneBtnPopup");
            div.append($("<title/>").text("알림"));
            div.append($("<span/>", {class: "text_white_center_52M"}).text("블루투스 장치는 6개까지 등록이 가능합니다"));
            div.append($("<span/>", {class: "text_white_center_38L"}).text("기존 장치를 삭제하고 등록하세요"));
            div.append($("<div/>", {class: "whiteBtn"}));
            div.find(".whiteBtn").append($("<span/>", {class: "text_black_center_30L"}).text("닫기"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_btSearchFail.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_btSearchFail.constructor = KTW.ui.layer.setting.popup.setting_btSearchFail;

    KTW.ui.layer.setting.popup.setting_btSearchFail.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_btSearchFail.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();