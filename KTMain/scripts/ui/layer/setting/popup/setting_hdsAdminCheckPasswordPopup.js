/**
 * Created by Taesang on 2016-12-30.
 */

(function() {

    KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup  = function hdsAdminCheckPasswordPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div, data, passwordCnt = 0, btnText = ["취소"];
        var focusRow = 0, focusIdx = 0;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var log = KTW.utils.Log;

        var maxPasswordCount = 13;


        var callbackFuncSuccess = null;

        var HDS_PASSWORD = "1473591232580";

        function getStarText(_passwordCnt) {
            var starText = "";
            if (_passwordCnt > 0) {
                for (var i = 0; i < _passwordCnt; i++) {
                    if (_passwordCnt == 4 && i == 3) starText += "*";
                    else starText += "*"
                }
            }
            return starText;
        }

        function focusIdxChange() {
            div.find(".oneBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".oneBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        function focusRowChange(_row) {
            if (_row != null) focusRow = _row;
            focusIdx = 0;
            div.find(".passwordArea.focus").removeClass("focus");
            div.find(".oneBtnArea_280 .btn.focus").removeClass("focus");
            if (focusRow == 0) div.find(".passwordArea").addClass("focus");
            else if (focusRow == 1) focusIdxChange();
        }

        function fillPassword() {
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup auchCheckPassword");
            div.append($("<title/>").text("관리자 로그인"));


           div.append($("<span/>", {class: "text_white_center_42M"}).html("관리자 비밀번호를 입력하여 주십시오"));
            div.append($("<div/>", {class: "passwordArea"}));
            div.find(".passwordArea").append($("<div/>", {class: "inputBox"}));
            div.find(".passwordArea").append($("<span/>", {class: "password"}).text("*************"));
            div.find(".passwordArea").append($("<span/>", {class: "password fill"}));

            div.find(".passwordArea .password").css({"letter-spacing":3.7});
            div.find(".passwordArea .password.fill").css({"letter-spacing":3.7});

            div.find(".passwordArea .password").css({left:35});
            div.find(".passwordArea .password.fill").css({left:35});

            div.append($("<div/>", {class: "oneBtnArea_280"}));

            div.find(".oneBtnArea_280").append($("<div/>", {class: "btn"}));
            div.find(".oneBtnArea_280 .btn").append($("<div/>", {class: "whiteBox"}));
            div.find(".oneBtnArea_280 .btn").append($("<span/>", {class: "btnText"}).text(btnText[0]));


            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            Layer.prototype.show.call(this);
            if(data !== undefined && data !== null) {
                callbackFuncSuccess = data.callback;
            }
            KTW.managers.auth.AuthManager.initPW();

            passwordCnt = 0;
            fillPassword();

            focusIdx = 0;
            focusRowChange(0);


        };

        this.hideView = function(options) {
            if(options === undefined) {
                KTW.managers.auth.AuthManager.initPW();
                KTW.managers.auth.AuthManager .resetPWCheckCount();
            }
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (focusRow == 0) {
                        var pw = KTW.managers.auth.AuthManager.getPW();
                        if(pw !== null && pw.length>0) {
                            _deleteNum();
                            return true;
                        }else {
                            return false;
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 1) {
                        callbackFuncSuccess(false);
                    }
                    return true;
                case KEY_CODE.UP:
                    focusRowChange(0);
                    return true;
                case KEY_CODE.DOWN:
                    focusRowChange(1);
                    return true;
                case KEY_CODE.BACK:
                    callbackFuncSuccess(false);
                    return true;
                default:
                    var number = KTW.utils.util.keyCodeToNumber(keyCode);
                    if (number >= 0) {
                        if (focusRow == 0) {
                            _inputNum(number);
                        }
                        return true;
                    }

                    break;
            }
            return false;
        };

        function _inputNum(num) {
            log.printDbg('_inputNum() num : ' + num);
            var authManager = KTW.managers.auth.AuthManager;

            if (authManager.inputPW(num, maxPasswordCount) === true) {
                passwordCnt =  authManager.getPW().length;
                log.printDbg('_inputNum() authManager.inputPW() return true passwordCnt : ' + passwordCnt);
                passwordCnt =  authManager.getPW().length;
                fillPassword();
                setTimeout(function(){
                    _checkPW();
                }, 10);
            }
            else {
                passwordCnt =  authManager.getPW().length;
                log.printDbg('_inputNum() authManager.inputPW() return false passwordCnt : ' + passwordCnt);
                fillPassword();
            }
        }

        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum() {
            log.printDbg('_deleteNum() ');
            var authManager = KTW.managers.auth.AuthManager;

            authManager.deletePW();
            passwordCnt =  authManager.getPW().length;
            fillPassword();
        }

        /**
         * 입력한 비밀번호 일치여부 확인
         */
        function _checkPW() {
            var authManager = KTW.managers.auth.AuthManager;

            log.printDbg('_checkPW() ');

            if(HDS_PASSWORD === authManager.getPW()) {
                if(callbackFuncSuccess !== null) {
                    callbackFuncSuccess(true);
                }
            }else {
                if(callbackFuncSuccess !== null) {
                    callbackFuncSuccess(false);
                }
            }
        }
    };
    KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup.constructor = KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup;

    KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };

    KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();