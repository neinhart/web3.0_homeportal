/**
 * Created by sw.nam on 2017-03-12.
 */


(function() {
    KTW.ui.layer.setting.popup.setting_receiveCheckingPopup  = function receiveCheckingPopup(options) {
        KTW.ui.Layer.call(this, options);
        var div;
        //var btnText = ["확인", "취소"];
        //var focusIdx = 0;

        var log = KTW.utils.Log;
        var util= KTW.utils.util;

        //var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var data;
        var DEF = KTW.oipf.Def;
        var hwAdapter = KTW.oipf.adapters.HWAdapter;
        //var navAdapter = KTW.oipf.AdapterHandler.navAdapter;

        var timer = null;
        var result_data;

        var progressDiv = null;
        var progressText = null;

        this.init = function(cbCreate) {
            log.printDbg("init()");

            data = this.getParams();

            div = this.div;
            div.addClass("arrange_frame setting gniPopup systemInfoPopup");
            div.append($("<title/>").css({"position": "absolute", "top": "403px"}).text("수신품질 측정"));

            createComponents();
            cbCreate(true);
        };

        this.show = function() {
            log.printDbg("show()");

            KTW.managers.device.PowerModeManager.addPowerModeChangeListener(onPowerModeChange);
            timer = setTimeout(startCollect, 500);


            Layer.prototype.show.call(this);
        };

        function onPowerModeChange(mode) {
            log.printDbg("CollectSignalDialog");
            log.printDbg("CollectSignalDialog mode : " + mode);
            if (mode === DEF.CONFIG.POWER_MODE.ACTIVE_STANDBY) {
                // [jh.lee] active_standby 인 경우 강제로 시스템 팝업인 품질측정 팝업을 종료하고 리스너를 제거한다.
                LayerManager.historyBack();
             //   LayerManager.deactivateLayer("collect_signal", true);
                KTW.managers.device.PowerModeManager.removePowerModeChangeListener(onPowerModeChange);
            }
        }

        this.hide = function() {
            log.printDbg("hide()");
            Layer.prototype.hide.call(this);

            //[sw.nam] 비 정상적 종료에 대한 exception 을 hide에서 일괄적으로 처리.
            clearTimeout(timer);
            //[]
            //var channel = navAdapter.getCurrentChannel();
            //navAdapter.changeChannel(channel, true);
        };

        this.controlKey = function(keyCode) {
            return true;

        };


        function createComponents(result) {
            log.printDbg("createComponents()");

            div.append($("<span/>", {class: "text_white_center_52M"}).text("잠시만 기다려 주십시오"));


            progressDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "receiveCheckingEntireProgressBar",
                    css: {
                        position: "absolute", left: 700, top: 585, width: 510, height: 4, "background-color" : "rgba(255,255,255,0.3)"
                    }
                },
                parent: div

            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "receiveCheckingDynamicProgressBar",
                    css: {
                        position: "absolute", height: 4, "background-color" : "rgba(255,255,255,1.0)"
                    }
                },
                parent: progressDiv
            });

            progressText = util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 887, top: 625, height: 34 ,"font-size": 38, "letter-spacing" : -1.9,
                        "font-family" : "RixHead L", color : "rgba(255,255,255,1.0)"
                    }
                },
                parent: div
            })

        }

        function startCollect() {
            log.printDbg("start startCollect()");
            var collect_list;
            //수집 시작 getCollectibleList
            collect_list = hwAdapter.getCollectibleList();
            log.printDbg("start startCollect() collect_list : " + collect_list);
            result_data = [];

            getTransponderInfo(0, collect_list);
        }

        function getTransponderInfo(idx, collect_list) {
            tuneTS(collect_list[idx]);
            clearTimeout(timer);
            timer = setTimeout(
                function() {
                    collectInfo(idx, collect_list);
                }, 2000);
        }

        function tuneTS(ts_info) {
            if (ts_info) {
                hwAdapter.tune(ts_info);
            }
        }

        function collectInfo(idx, collect_list) {
            result_data[idx] = hwAdapter.collectOneTransponder(collect_list[idx].name, collect_list[idx].transportStreamId);

            log.printDbg("collectInfo()....");
            if (idx === collect_list.length - 1) {
                // [jh.lee] 품질 측정 성공인 경우 listener 함수 실행
                //listener(result_data);
                log.printDbg("success...!!!!");
                data.complete(result_data);
            } else {
                idx++;
                getTransponderInfo(idx, collect_list);
                log.printDbg("progressing.... . . .");

            }
            drawProgress(idx + 1, collect_list.length);

        }
        function drawProgress(idx, total) {
            progressDiv.find(".receiveCheckingDynamicProgressBar").css("width", parseInt(510 * ((idx + 1) / total)));
            progressText.text(parseInt(100 * ((idx) / total))+ "% 완료");
        }

    };

    KTW.ui.layer.setting.popup.setting_receiveCheckingPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_receiveCheckingPopup.constructor = KTW.ui.layer.setting.popup.setting_receiveCheckingPopup;

    KTW.ui.layer.setting.popup.setting_receiveCheckingPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_receiveCheckingPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})();