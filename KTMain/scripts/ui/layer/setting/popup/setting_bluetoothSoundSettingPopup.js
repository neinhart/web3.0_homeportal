/**
 * Created by skkwon on 2017-01-18.
 *
 * 설정 > 시스템 설정 > 블루투스 설정 > 장치 검색 후 오디오 장치인 경우 음성 듣기 옵션 선택 팝업
 */

(function () {
    KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup  = function BluetoothSoundSettingPopup(options) {
        KTW.ui.Layer.call(this, options);


        var div = $("<div/>", {class: "popup_contents"});
        var focus = 0;
        var bottomMent = ["※TV와 연결된 장치에서 동시에 음성 듣기가 가능합니다", "※연결된 장치에서만 음성 듣기가 가능합니다"];

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;
        var log = KTW.utils.Log;
        var data;
        
        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setting gniPopup bluetooth_sound_setting_popup"});

            div.append($("<title/>").text("연결 완료"));
            div.append($("<span/>", {class:"text1"}).html("블루투스 장치가 연결되었습니다<br>음성 듣기 옵션을 선택하세요"));
            div.append($("<div/>", {class:"btn_area"}));
            div.append($("<span/>", {class:"text2"}));
            
            makeBtnArea();
            this.div.append(div);
            if(cbCreate) cbCreate(true);
        };
        
        function makeBtnArea() {
            var d = $("<div/>", {class: "btn_list"});
            d.append("<div class='btn btn_1'><div class ='bg' style='position: absolute; width: 255px; height: 228px; box-sizing: border-box;'/><div class='icon'/><div class='label'>같이 듣기</div></div>");
            d.append("<div class='btn btn_2'><div class ='bg' style='position: absolute; width: 255px; height: 228px; box-sizing: border-box;'/><div class='icon'/><div class='label'>혼자 듣기</div></div>");
            div.find(".btn_area").append(d);
        }

        function setFocus(_focus) {
            focus = _focus;
            div.find(".btn.focus").removeClass("focus");
            div.find(".btn").eq(focus).addClass("focus");
            div.find(".text2").text(bottomMent[focus]);
        }

        this.show = function() {
            Layer.prototype.show.apply(this, arguments);

            data = this.getParams();
            var isListenAlone =  bluetoothDevice.isListenAloneMode();

            if(isListenAlone) {
                focus = 1;
            } else {
                focus = 0;
            }
            setFocus(focus);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                case KEY_CODE.RIGHT:
                    setFocus(focus^1);
                    log.printDbg("focus === "+focus);
                    return true;
                case KEY_CODE.ENTER:
                    var listenAloneMode;
                    //TODO 선택시 동작.
                    if(focus == 0) {
                        listenAloneMode = false;
                    } else {
                        listenAloneMode = true;
                    }
                    log.printDbg("listenAloneMode==  "+listenAloneMode);
                    bluetoothDevice.setListenAloneMode(listenAloneMode);
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup.constructor = KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup;

    KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();