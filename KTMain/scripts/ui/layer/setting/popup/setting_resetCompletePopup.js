/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_resetComplete  = function resetCompletePopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        var isSystemPriority = false;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup resetComplete");
            div.append($("<title/>").text("비밀번호 초기화 완료"));
            div.append($("<span/>", {class: "text_white_center_45M"}).text("비밀번호가 '0000'으로 초기화되었습니다"));
            div.append($("<span/>", {class: "text_white_center_33L"}).html("설정>자녀안심설정>비밀번호 변경/초기화에서<br>비밀번호를 변경할 수 있습니다"));
            div.append($("<div/>", {class: "whiteBtn"}));
            div.find(".whiteBtn").append($("<span/>", {class: "text_white_center_30L"}).text("확인"));
            var params = this.getParams();
            if (params) {
                isSystemPriority = params.isSystemPriority;
            }

            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);
            }
        };

        this.hide = function() {
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);
            }
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.EXIT:
                    var consume = false;
                    if (this.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                        LayerManager.historyBack();
                        consume = true;
                    }
                    return consume;
                default:
                    if (isSystemPriority) {
                        return true;
                    }
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_resetComplete.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_resetComplete.constructor = KTW.ui.layer.setting.popup.setting_resetComplete;

    KTW.ui.layer.setting.popup.setting_resetComplete.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };
    KTW.ui.layer.setting.popup.setting_resetComplete.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})();