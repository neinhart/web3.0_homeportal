/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_btDeviceDelete  = function btDeviceDeletePopup(options) {
        KTW.ui.Layer.call(this, options);

        var data, div, btnText = ["확인", "취소"], idx;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        function changeFocus() {
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(idx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup btDeviceDelete");
            div.append($("<title/>").text("장치 삭제"));
            div.append($("<span/>", {class: "text_white_center_42M"}));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            idx = 0;
            changeFocus();
            // 장치 개수
            div.find(".text_white_center_42M").html("선택된 " + data.deviceCnt + "개 장치를<br>블루투스 목록에서 삭제합니다");
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    idx = KTW.utils.util.indexMinus(idx, 2);
                    changeFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    idx = KTW.utils.util.indexPlus(idx, 2);
                    changeFocus();
                    return true;
                case KEY_CODE.ENTER:
                    // 확인 입력시 처리할 Callback 함수 호출
                    if (idx == 0 && data.callback) data.callback();
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };
    KTW.ui.layer.setting.popup.setting_btDeviceDelete.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_btDeviceDelete.constructor = KTW.ui.layer.setting.popup.setting_btDeviceDelete;

    KTW.ui.layer.setting.popup.setting_btDeviceDelete.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_btDeviceDelete.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();