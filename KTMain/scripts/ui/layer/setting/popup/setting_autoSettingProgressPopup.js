"use strict";

(function () {
    //alias
    var util = KTW.utils.util;
    var log = KTW.utils.Log;

    var arrButton = [{id: "cancel", name: "취소"}];

    KTW.ui.view.popup.AutoSettingProgressPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        var mmiHandler = KTW.ca.MmiHandler;
        var HashMap = KTW.utils.HashMap;

        this.data = null;
        this.focusIdx = -1;
        var progressCurrentPositionDiv = null;
        var currentProgressText = null;

        var runningCollectInfo = false;
        var result;

        var ccid = "";
        var idx = 0;
        var complete;

        var currentChannel = null;
        var skipSkyVideoList;
        var skyVideoLength;

        var timer = null;

        var hashBlockList = new HashMap();

        var skyBlockList = [];

        var isRemoveListener = false;

        var callbackFunc = null;

        function createElements (_this) {
            var parentDiv = _this.parent.div;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parentDiv
            });

            var containerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, overflow: "visible"
                    }
                },
                parent: parentDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 331, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 94, "text-align": "center",
                        "font-size": 33, "letter-spacing": -1.65, color: "rgba(221, 175, 120, 1)", "font-family": "RixHead B"
                    }
                },
                text: "건너뛰기 채널 자동 설정",
                parent: containerDiv
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 401, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 53, "text-align": "center",
                        "font-size": 52, "letter-spacing": -2.1, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M"
                    }
                },
                text: "가입하지 않은 채널을 자동으로",
                parent: containerDiv
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 473, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 53, "text-align": "center",
                        "font-size": 52, "letter-spacing": -2.1, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M"
                    }
                },
                text: "건너뛰도록 설정 중입니다",
                parent: containerDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "progress_background_div",
                    css: {
                        position: "absolute", left: 705, top: 584, width: 495, height: 4,
                        "background-color": "rgba(255,255,255,0.3)"
                    }
                },
                parent: containerDiv
            });

            progressCurrentPositionDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "current_position_div",
                    css: {
                        position: "absolute", left: 705, top: 584, width: 0, height: 4,
                        "background-color": "rgba(255,255,255,1)"
                    }
                },
                parent: parentDiv
            });


            currentProgressText = util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 613, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 40, "text-align": "center",
                        "font-size": 38, "letter-spacing": -2.1, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead L"
                    }
                },
                text: "0% 완료",
                parent: containerDiv
            });


            // button
            var buttonLeft = 820;
            for (var i = 0; i < arrButton.length; i++) {
                var buttonData = arrButton[i];

                var button = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "rd_popup_button",
                        css: {
                            position: "absolute", left: buttonLeft, top: 687, width: 280, height: 62,
                            "background-image": "url(./images/pop_btn_w280.png)"
                        }
                    },
                    parent: containerDiv
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "rd_popup_button_line",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "border-bottom" : "5px solid rgb(209,67,63)", "border-top": "5px solid rgb(209,67,63)","box-sizing" : "border-box"
                        }
                    },
                    parent: button

            });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280, height: 35, "text-align": "center",
                            "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead M"

                        }
                    },
                    text: buttonData.name,
                    parent: button
                });

                buttonLeft += 294;
            }
        }

        function setButtonFocus (_this) {
            var arrButton = _this.parent.div.find("[name=rd_popup_button]");
            var focusButton = $(arrButton[_this.focusIdx]);

            arrButton.css({ "background-color": "", "background-image": "url(./images/pop_btn_w280.png)" });
            arrButton.children().css({ color: "rgba(255, 255, 255, 0.7)" });

            focusButton.css({ "background-color": "rgba(255, 255, 255, 1)", "background-image": "" });
            focusButton.children().css({ color: "rgba(73, 68, 60, 1)" });
        }

        this.createView = function() {
            this.parent.div.attr({class: "arrange_frame"});
            callbackFunc = this.parent.getParams().callback;

            createElements(this);
        };

        this.showView = function (options) {
            log.printDbg("showView()");
            if (!options || !options.resume) {
                this.focusIdx = 0;
                setButtonFocus(this);

                runningCollectInfo = true;
                result = [];

                ccid = "";
                complete = false;
                idx = 0;

                currentChannel = navAdapter.getCurrentChannel();
                log.printDbg("showView() currentChannel : " + JSON.stringify(currentChannel));

                mmiHandler.addListener(notifyMmiEventForSkipCh);

                navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).skipChannelEvent(true);

                _deleteSkyGuideChannel();

                timer = setTimeout(function() {startSelect();}, 500);

                try {
                    //제한채널 해제
                    _checkBlockChannelList();
                } catch (error) {
                    log.printDbg(error.message);
                }

            }
        }

        this.hideView = function (options) {
            if (!options || !options.pause) {
                // if (this.data.cbAction) {
                //     this.data.cbAction();
                // }
            }
        };

        this.destroyView = function() {
            log.printDbg("destoryView() isRemoveListener : " + isRemoveListener);
            if(isRemoveListener === false) {
                clearTimeout(timer);
                try {
                    mmiHandler.removeListener(notifyMmiEventForSkipCh);
                } catch (error) {
                    //printlog(error.message);
                }
                navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).skipChannelEvent(false);

                if (sky_block_list !== undefined) {
                    navAdapter.changeChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED, skyBlockList);
                }

            }
        };

        this.controlKey = function (keyCode) {
            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.OK:
                    keyOk();
                    break;
            }

            return consumed;
        }


        function notifyMmiEventForSkipCh(type, id) {
            log.printDbg("Out notifyMmiEventForSkipCh in type : " + type);
            log.printDbg("Out notifyMmiEventForSkipCh in id : " + id);
            log.printDbg("Out notifyMmiEventForSkipCh in idx : " + idx);
            log.printDbg("Out notifyMmiEventForSkipCh in ccid : " + ccid);
            log.printDbg("Out notifyMmiEventForSkipCh in result[idx] : " + result[idx]);

            if (type === mmiHandler.MMI_EVENT.TYPE.START && (id === mmiHandler.MMI_EVENT.DIALOGUE_ID.CANT_BUY_THE_CONTENT ||
                id === mmiHandler.MMI_EVENT.DIALOGUE_ID.SERVICE_BLACK_OUT)) {
                log.printDbg("In notifyMmiEventForSkipCh  : id " + mmiHandler.MMI_EVENT.DIALOGUE_ID.CANT_BUY_THE_CONTENT);
                log.printDbg("In notifyMmiEventForSkipCh receive id : " + id);
                log.printDbg("In notifyMmiEventForSkipCh  : type " + type);
                log.printDbg("In notifyMmiEventForSkipCh ccid : " + ccid);
                result[idx] = {"ccid":""};
                result[idx].ccid = ccid;
                log.printDbg("In notifyMmiEventForSkipCh result[idx].ccid : " + result[idx].ccid);
                log.printDbg("In notifyMmiEventForSkipCh result idx : " + idx);
                log.printDbg("In notifyMmiEventForSkipCh result : " + JSON.stringify(result));
            }
        }

        /**
         * 스카이 채널리스트에서 스카이가이드 채널 제거
         */
        function _deleteSkyGuideChannel() {
            var tmp_sky_video_list;
            var sky_guide_channel;

            tmp_sky_video_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);
            skipSkyVideoList = Array.apply(this, tmp_sky_video_list).slice();

            sky_guide_channel = navAdapter.getSkyPlusChannel();
            skyVideoLength = skipSkyVideoList ? skipSkyVideoList.length : 0;

            if (sky_guide_channel && sky_guide_channel.ccid) {
                for (var i = 0; i < skyVideoLength; i++) {
                    if (skipSkyVideoList[i].ccid === sky_guide_channel.ccid) {
                        skipSkyVideoList.splice(i, 1);
                        skyVideoLength = skyVideoLength - 1;
                        break;
                    }
                }
            }
        }

        /**
         * [jh.lee] 블락채널 백업 저장하고 기존 블락채널 초기화
         */
        function _checkBlockChannelList() {
            var tmp_sky_block_list;
            var tmp_sky_list;
            var tmp_sky_list_length = 0;
            var selected_count = 0;

            tmp_sky_block_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED);
            tmp_sky_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);

            if (util.isValidVariable(tmp_sky_block_list) === true) {
                // [jh.lee] block 채널 갯수 저장
                selected_count = tmp_sky_block_list.length;
            }

            if (util.isValidVariable(tmp_sky_list) === true) {
                // [jh.lee] sky 전체 채널 저장
                tmp_sky_list_length = tmp_sky_list.length;
            }

            hashBlockList.clear();

            for (var i = 0; i < selected_count; i++) {
                hashBlockList.put(tmp_sky_block_list[i].ccid, tmp_sky_block_list[i].ccid);
            }

            for (var i = 0; i < tmp_sky_list_length; i++) {
                if (hashBlockList.get(tmp_sky_list[i].ccid) !== null) {
                    skyBlockList[i] = {};
                    skyBlockList[i].ccid = tmp_sky_list[i].ccid;
                }
            }

            navAdapter.changeChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED, []);
        }


        function startSelect() {
            if (idx < skyVideoLength) {
                //초이스채널이면 패스
                if (util.isFavID(skipSkyVideoList[idx], ["SKYLIFE_CHANNELS_PPV"])) {
                    // [jh.lee] 스카이 초이스채널이면 패스. isFavID 로 스카이 초이스 채널을 찾아낸다.
                    result[idx] = undefined;
                    if(timer !== null) {
                        clearTimeout(timer);
                        timer = null;
                    }
                    timer = setTimeout(function() {
                        drawProgress(idx, skyVideoLength);
                        startSelect(++idx);
                    }, 10);
                } else {
                    // [jh.lee] 채널 재생
                    navAdapter.changeChannel(skipSkyVideoList[idx], true);
                    result[idx] = undefined;
                    ccid = skipSkyVideoList[idx].ccid;
                    log.printDbg("startSelect ccid : " + ccid);
                    log.printDbg("startSelect majorChannel : " + skipSkyVideoList[idx].majorChannel);
                    if(timer !== null) {
                        clearTimeout(timer);
                        timer = null;
                    }

                    timer = setTimeout(function() {
                        drawProgress(idx, skyVideoLength);
                        startSelect(++idx);
                    }, 3000);
                }

            } else {
                // [jh.lee] 팝업 정상 종료
                // [jh.lee] 정상 종료시 팝업 문구 갱신을 위해 호출
                completeCall();
            }
        }

        function drawProgress(idx, total) {
            progressCurrentPositionDiv.css("width", parseInt(495 * ((idx + 1) / total)));
            currentProgressText.text(parseInt(100 * ((idx+1) / total)) + "%");
        }

        /**
         *  완료 토스트 팝업은 설정 화면에서 띄워야 함.
         */
        function completeCall() {
            log.printDbg("completeCall");
            complete = true;
            runningCollectInfo = false;
            keyOk();
        }

        function changeLastChannel() {

            try {
                mmiHandler.removeListener(notifyMmiEventForSkipCh);
            } catch (error) {
                log.printDbg(error.message);
            }

            // 채널 전환 전에 skipChannelEvent 를 false 처리한다.
            navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).skipChannelEvent(false);
            navAdapter.changeChannel(currentChannel, true);

            isRemoveListener = true;
        }


        function keyOk() {
            // [jh.lee] TODO 채널튠, mmi 정합 필요, 팝업 콜백 처리 필요
            if(timer !== null) {
                clearTimeout(timer);
                timer = null;
            }
            changeLastChannel();

            navAdapter.changeChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED, skyBlockList);
            skyBlockList = undefined;

            if (complete === true) {
                // [jh.lee] 성공한 상태에서 확인 버튼 입력하는 경우
                if (callbackFunc) {
                    callbackFunc(0, result);
                }
            } else {

                if (callbackFunc) {
                    callbackFunc(0);
                }
            }
        }
    };

    KTW.ui.view.popup.AutoSettingProgressPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.AutoSettingProgressPopup.prototype.constructor = KTW.ui.view.popup.AutoSettingProgressPopup;

    KTW.ui.view.popup.AutoSettingProgressPopup.prototype.create = function () {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.AutoSettingProgressPopup.prototype.show = function (options) {
        this.showView(options);
    };
    KTW.ui.view.popup.AutoSettingProgressPopup.prototype.hide = function (options) {
        this.hideView(options);
    };
    KTW.ui.view.popup.AutoSettingProgressPopup.prototype.destroy = function () {
        this.destroyView();
        KTW.ui.View.prototype.destroy.call(this);
    };


    KTW.ui.view.popup.AutoSettingProgressPopup.prototype.handleKeyEvent = function (key_code) {
        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };



})();
