/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_resolutionAlertPopup  = function resolutionAlertPopup(options) {
        KTW.ui.Layer.call(this, options);
        
        var div;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup btOneBtnPopup");
            div.append($("<title/>").css({"position": "absolute", "top": "393px"}).text("알림"));
            div.append($("<span/>", {class: "text_white_center_38M"}).css({"position": "absolute", "line-height": "45px", "top": "465px"}).html("컴포넌트로 연결된 경우,<br>해상도를 1080i 이상으로 설정할 수 없습니다"));
            div.append($("<div/>", {class: "whiteBtn"}).css({"position": "absolute", "top": "625px"}));
            div.find(".whiteBtn").append($("<span/>", {class: "text_black_center_30L"}).text("확인"));
            div.find(".whiteBtn").append($("<div/>", {class: "btn_line"}));
            div.find(".whiteBtn .btn_line").css({position: "absolute",width:280, height:62, "border-top" : "solid 5px rgb(209,67,63)", "border-bottom" :"solid 5px rgb(209,67,63)", "box-sizing": "border-box"});

            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_resolutionAlertPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_resolutionAlertPopup.constructor = KTW.ui.layer.setting.popup.setting_resolutionAlertresolutionAlert;

    KTW.ui.layer.setting.popup.setting_resolutionAlertPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_resolutionAlertPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();