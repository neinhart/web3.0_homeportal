/**
 * Created by Yun on 2017-03-05.
 */


(function () {

    KTW.ui.layer.setting.popup.setting_resolutionChangePopup  = function resolutionChangePopup(options) {
        KTW.ui.Layer.call(this, options);
        var div;
        var data;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;


        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup resolutionChangePopup");
            div.append($("<title/>").css({"position":"absolute", "top":"484px"}).text("해상도"));
            div.append($("<span/>", {class:"text_white_center_38M"}).html("해상도 2160p (60fps) 설정을 하시려면<br>15초 이내 리모컨의 별표키(*)를 누르세요"));
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            Layer.prototype.show.call(this);
            div.find(".text_white_center_38M").html("해상도 " + data.resolutionValue + " 설정을 하시려면<br>"+data.waitingSecond+"초 이내 리모컨의 별표키(*)를 누르세요");
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    return true;
                case KEY_CODE.STAR:
                    LayerManager.historyBack();
                    data.complete();
                    return true;
                default:
                    return false;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_resolutionChangePopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_resolutionChangePopup.constructor = KTW.ui.layer.setting.popup.setting_resolutionChangePopup;

    KTW.ui.layer.setting.popup.setting_resolutionChangePopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_resolutionChangePopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();