/**
 * Created by Yun on 2017-03-03.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_wrongPasswordPopup  = function wrongPasswordPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;
        var btnText = ["바로가기", "취소"];
        var focusIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup wrongPasswordPopup");
            div.append($("<title/>").css({"position":"absolute", "top":"370px"}).text("알림"));
            div.append($("<span/>", {class:"text_white_center_42M"}).text("비밀번호가 3회 연속 일치하지 않았습니다"));
            div.append($("<span/>", {class:"text_white_center_33L"}).html("비밀번호를 잊어버렸을 경우,<br>비밀번호 변경/초기화를 이용해 주세요"));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.RIGHT:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.ENTER:
                    if (focusIdx == 0) {
                        LayerManager.historyBack();
                        // 비밀번호 변경/초기화로 이동
                    } else {
                        LayerManager.historyBack();
                    }
                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }
    };

    KTW.ui.layer.setting.popup.setting_wrongPasswordPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_wrongPasswordPopup.constructor = KTW.ui.layer.setting.popup.setting_wrongPasswordPopup;

    KTW.ui.layer.setting.popup.setting_wrongPasswordPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_wrongPasswordPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();