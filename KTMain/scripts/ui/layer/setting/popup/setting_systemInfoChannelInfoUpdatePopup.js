/**
 * Created by sw.nam on 2017-04-18.
 */


(function() {
    KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup  = function systemInfoChannelInfoUpdatePopup(options) {
        KTW.ui.Layer.call(this, options);
        var div;
        //var btnText = ["확인", "취소"];
        var focusIdx = 0;

        var log = KTW.utils.Log;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var data;

        this.init = function(cbCreate) {
            log.printDbg("init()");

            data = this.getParams();

            div = this.div;
            div.addClass("arrange_frame setting gniPopup systemInfoPopup");
            var top;

            if(data.result === undefined || data.result === null) {
                top = 445;
            } else {
                top = 403;
            }
            div.append($("<title/>").css({"position": "absolute", "top": top}).text("채널 정보 업데이트"));

            createComponents(data.result);
            cbCreate(true);
        };
        this.show = function() {
            log.printDbg("show()");

            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            Layer.prototype.show.call(this);
        };
        this.hide = function() {
            log.printDbg("hide()");
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                case KEY_CODE.BACK:
                    if (data.result !== undefined && data.result !== null) {
                        LayerManager.historyBack();
                    }
                    return true;

                    return true;
                default:
                    return false;
            }
        };

        function buttonFocusRefresh(index) {
            div.find(".twoBtnArea_280 .btn").removeClass("focus");
            div.find(".twoBtnArea_280 .btn:eq(" + index + ")").addClass("focus");
        }

        function createComponents(result) {
            log.printDbg("createComponents()__"+result);

            if (result === undefined || result === null) {
                div.append($("<span/>", {class: "text_white_center_52M"}).css({ top:529 }).text("채널 정보 업데이트 중입니다"));
            } else if (result === true) {
                div.append($("<span/>", {class: "text_white_center_52M"}).text("업데이트가 완료되었습니다"));
                div.append($("<div/>", {class: "whiteBtn"}).css({"position": "absolute" }));
                div.find(".whiteBtn").append($("<div/>", {class: "btn_line"}));
                div.find(".whiteBtn").append($("<span/>", {class: "text_black_center_30L"}).text("확인"));
                div.find(".whiteBtn .btn_line").css({position: "absolute",width:280, height:62, "border-top" : "solid 5px rgb(209,67,63)", "border-bottom" :"solid 5px rgb(209,67,63)", "box-sizing": "border-box"})
            } else if (result === false) {
                div.append($("<span/>", {class: "text_white_center_52M"}).text("업데이트에 실패하였습니다"));
                div.append($("<div/>", {class: "whiteBtn"}).css({"position": "absolute" }));
                div.find(".whiteBtn").append($("<div/>", {class: "btn_line"}));
                div.find(".whiteBtn").append($("<span/>", {class: "text_black_center_30L"}).text("확인"));
                div.find(".whiteBtn .btn_line").css({position: "absolute",width:280, height:62, "border-top" : "solid 5px rgb(209,67,63)", "border-bottom" :"solid 5px rgb(209,67,63)", "box-sizing": "border-box"})
            }
        }
    };

    KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup.constructor = KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup;

    KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();