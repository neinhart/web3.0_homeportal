/**
 * Created by skkwon on 2017-01-18.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup  = function MobileBrandsPopup(options) {
        KTW.ui.Layer.call(this, options);
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var title = null;
        var showImageName = null;
        var callbackFunc = null;
        var isSystemPriority = false;

        this.init = function(cbCreate) {
            log.printDbg("init()");
            params = this.getParams();
            if(params !== null) {
                callbackFunc = params.callback;
                isSystemPriority = params.isSystemPriority;
            }
            div = this.div;
            title = "알뜰폰 사업자";

            showImageName = KTW.CONSTANT.MVNO_PNG.URL;
            _createView(div);
            if(cbCreate) cbCreate(true);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);
            }
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hideView()");
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);
            }
            _hideView();
        };


        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KTW.KEY_CODE.ENTER:
                case KTW.KEY_CODE.BACK:
                    if(callbackFunc !== null){
                        callbackFunc();
                    }
                    return true;
                default:
                    if (isSystemPriority) {
                        return true;
                    }
            }
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 119 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:""
                    }
                },
                text: title,
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn" ,
                    class : "focus_box_red_line" ,
                    css: {
                        position: "absolute",
                        left: 820, top: 898, width:280, height: 62,
                        "background-color": "rgb(210,51,47)",
                        display: ""
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 820 , top: 898+18 , width: 280, height: 32,
                        color: "white", "font-size": 30 , "text-align": "center"
                    }
                },
                text: "확인",
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: showImageName,
                    css: {
                        position: "absolute",
                        left: 497,
                        top: 199,
                        width: 926,
                        height: 636
                    }
                },
                parent: popup_container_div
            });
        }

        function _showView() {

        }

        function _hideView() {

        }
    };

    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup.constructor = KTW.ui.layer.setting.popup.setting_mobileBrandsPopup;

    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup.prototype.show = function(options) {
        KTW.ui.Layer.prototype.show.call(this);
        this.showView(options);
    };

    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup.prototype.hide = function() {
        this.hideView();
    };

    KTW.ui.layer.setting.popup.setting_mobileBrandsPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();