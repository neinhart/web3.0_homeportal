/**
 * Created by skkwon on 2017-01-18.
 *  * params : {
 *    telNo :
 *    birth :
 *    gender :
 *    nation :
 *    uname :
 *    callback :
 * }
 *
 */

(function () {
    KTW.ui.layer.setting.popup.setting_resetPasswordPopup  = function ResetPasswordPopup(options) {
        KTW.ui.Layer.call(this, options);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var FOCUS_MOBILE_TYPE_KT = 0;
        var FOCUS_MOBILE_TYPE_SKT = 1;
        var FOCUS_MOBILE_TYPE_LG = 2;
        var FOCUS_MOBILE_TYPE_MVNO = 3;
        var FOCUS_MOBILE_FIRST_NUMBER = 4;
        var FOCUS_MOBILE_MIDDLE_NUMBER = 5;
        var FOCUS_MOBILE_LAST_NUMBER = 6;

        var FOCUS_BIRTH_YEAR = 7;
        var FOCUS_BIRTH_MONTH = 8;
        var FOCUS_BIRTH_DAY = 9;


        var FOCUS_AUTH_BTN = 10;
        var FOCUS_INPUT_BOX = 11;
        var FOCUS_OK = 12;
        var FOCUS_CANCEL = 13;

        var MOBILE_TYPE_NAME = ["KT" , "SKT" , "LG U+" , "알뜰폰"];

        var MVNO_TYPE_NAME = ["KT 알뜰폰" , "SKT 알뜰폰" , "LG U+ 알뜰폰" , "알뜰폰 사업자 확인"];

        var TYPE_VALUE = ["KT", "SKT", "LGT"];
        var TYPE_VALUE_MVNO = ["KTM", "SKM", "LGM"];

        var initShow = false;

        var userTelNo = "";
        var userBirth = "";
        var userGender = "";
        var userNation = "";
        var userName  = "";

        var callbackFunc = null;

        var rootDiv = null;

        var focusIndex = -1;

        var mobileType = [];
        var selectMobileType = 0;

        var dropbox = null;
        var isShowDropBox = false;
        var selectMVNOType = -1;
        var focusMVNOIndex = -1;

        var mobileNumberTitle = null;
        var mobileNumberDelimiter1 = null;
        var mobileNumberDelimiter2 = null;

        var mobileNumber = [];

        var mobileNumberInputString = ["","",""];
        var birthDayInputString = ["","",""];

        var birthDayTitle = null;
        var birthDayYearUnit = null;
        var birthDayMonthUnit = null;
        var birthDayDayUnit = null;
        var birthDay = [];

        var mobileAuthBtnTitle = null;
        var mobileAuthBtn = null;
        var mobileAuthInputBox = null;

        var mobileAuthString = "";

        var btnOk = null;
        var btnCancel = null;

        var ckey = null;

        var resourcePath = "images/popup/";

        var resetErrorPopupLayerId = "ErrorPopup";

        var isSystemPriority = false;
        var current_year = new Date();

        this.init = function(cbCreate) {

            params = this.getParams();

            if (params !== null) {
                userTelNo = params.data.telNo;
                userBirth = params.data.birth;
                userGender = params.data.gender;
                userNation = params.data.nation;
                userName = params.data.uname;

                isSystemPriority = params.isSystemPriority;

                callbackFunc = params.callback;
            }
            else {
                isSystemPriority = false;
            }

            div = this.div;
            _createView(div);
            focusIndex = 0;

            if(cbCreate) cbCreate(true);
        };


        this.showView = function() {
            log.printDbg("show()");
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);
            }
            _showView();
        };

        this.hideView = function() {
            log.printDbg("hide()");
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);
            }
            _hideView();
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };


        this.controlKey = function(key_code) {
            var consumed = false;
            switch (key_code) {
                case KTW.KEY_CODE.BACK:
                case KTW.KEY_CODE.EXIT:
                    if(callbackFunc !== null) {
                        callbackFunc(false);
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    if (focusIndex >= FOCUS_MOBILE_TYPE_KT && focusIndex <= FOCUS_MOBILE_TYPE_MVNO) {
                        if (focusIndex !== FOCUS_MOBILE_TYPE_MVNO) {
                            selectMobileType = focusIndex;
                            selectMVNOType = -1;

                            focusIndex = FOCUS_MOBILE_FIRST_NUMBER;

                            _focusUpdate();
                        }
                        else {
                            if (isShowDropBox === false) {
                                isShowDropBox = true;
                                focusMVNOIndex = 0;
                                mobileType[FOCUS_MOBILE_TYPE_MVNO].root_div.css("display" , "none");
                                _showDropBox();
                            }
                            else {
                                _hideDropBox();
                                isShowDropBox = false;
                                if (focusMVNOIndex === 3) {
                                    mobileType[FOCUS_MOBILE_TYPE_MVNO].root_div.css("display" , "");

                                    var priority = isSystemPriority ? KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5 : KTW.ui.Layer.PRIORITY.POPUP;
                                    KTW.ui.LayerManager.activateLayer({
                                        obj: {
                                            id: KTW.ui.Layer.ID.SETTING_MOBILE_BRANDS_POPUP,
                                            type: KTW.ui.Layer.TYPE.POPUP,
                                            priority: priority,
                                            linkage: isSystemPriority === true ? true : false,
                                            params: {
                                                callback : _callbackLicenseListPopup,
                                                isSystemPriority: isSystemPriority
                                            }
                                        },
                                        new: true,
                                        visible: true
                                    });
                                }
                                else {
                                    selectMVNOType = focusMVNOIndex;
                                    mobileType[FOCUS_MOBILE_TYPE_MVNO].default_text.text(MVNO_TYPE_NAME[selectMVNOType]);
                                    mobileType[FOCUS_MOBILE_TYPE_MVNO].focus_text.text(MVNO_TYPE_NAME[selectMVNOType]);
                                    selectMobileType = FOCUS_MOBILE_TYPE_MVNO;
                                    mobileType[FOCUS_MOBILE_TYPE_MVNO].root_div.css("display" , "");

                                    focusIndex = FOCUS_MOBILE_FIRST_NUMBER;

                                    _focusUpdate();
                                }

                            }
                        }
                    }
                    else if (focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        focusIndex++;
                        _focusUpdate();
                    }
                    else if (focusIndex >= FOCUS_BIRTH_YEAR && focusIndex <= FOCUS_BIRTH_DAY) {
                        focusIndex++;
                        _focusUpdate();
                    }
                    else if (focusIndex >= FOCUS_AUTH_BTN && focusIndex <= FOCUS_INPUT_BOX) {
                        if (focusIndex === FOCUS_AUTH_BTN) {

                            var tType = null;
                            if (selectMobileType === FOCUS_MOBILE_TYPE_MVNO) {
                                tType=TYPE_VALUE_MVNO[selectMVNOType];
                            }
                            else {
                                tType=TYPE_VALUE[selectMobileType];
                            }
                            var birth = birthDayInputString[0] + birthDayInputString[1] + birthDayInputString[2];

                            var telNo = mobileNumberInputString[0] + mobileNumberInputString[1] + mobileNumberInputString[2];

                            var now = new Date().getTime();
                            now = Math.floor(now/1000);
                            log.printDbg("onHDSAuthPinResponse() now : " + now);
                            KTW.ui.LayerManager.startLoading();
                            KTW.managers.auth.AuthManager.reqSMSComm(birth, userGender, userNation, userName, tType, telNo, now , _callbackFuncReqSMSComm);

                        }
                        focusIndex++;
                        _focusUpdate();

                    }
                    else if (focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        if(focusIndex === FOCUS_OK) {
                            //checkEmptyElement
                            var isEmptyElement = false;
                            if(mobileNumberInputString[0] === "") {
                                focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                                isEmptyElement = true;
                            }else if(mobileNumberInputString[1] === "") {
                                focusIndex = FOCUS_MOBILE_MIDDLE_NUMBER;
                                isEmptyElement = true;
                            }else if(mobileNumberInputString[2] === "") {
                                focusIndex = FOCUS_MOBILE_LAST_NUMBER;
                                isEmptyElement = true;
                            }else if(birthDayInputString[0] === "") {
                                focusIndex = FOCUS_BIRTH_YEAR;
                                isEmptyElement = true;
                            }else if(birthDayInputString[1] === "") {
                                focusIndex = FOCUS_BIRTH_MONTH;
                                isEmptyElement = true;
                            }else if(birthDayInputString[2] === "") {
                                focusIndex = FOCUS_BIRTH_DAY;
                                isEmptyElement = true;
                            }else if(mobileAuthString === "") {
                                focusIndex = FOCUS_INPUT_BOX;
                                isEmptyElement = true;
                            }

                            if(isEmptyElement) {
                                _focusUpdate();
                                return true;
                            }
                            var now = new Date().getTime();
                            now = Math.floor(now/1000);
                            log.printDbg("onHDSAuthPinResponse() now : " + now);
                            KTW.ui.LayerManager.startLoading();

                            KTW.managers.auth.AuthManager.reqSMSCnfrmComm(mobileAuthString, ckey, now , _callbackFuncReqSMSCnfrmComm);
                        }else if(focusIndex === FOCUS_CANCEL) {
                            if(callbackFunc !== null) {
                                callbackFunc(false);
                            }
                        }

                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    if(isShowDropBox === false) {
                        if(focusIndex >= FOCUS_MOBILE_TYPE_KT && focusIndex <= FOCUS_MOBILE_TYPE_MVNO) {
                            focusIndex--;
                            if(focusIndex<FOCUS_MOBILE_TYPE_KT) {
                                focusIndex = FOCUS_MOBILE_TYPE_MVNO;
                            }
                        }else if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {

                            if(mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER].length<=0){
                                focusIndex--;
                                if(focusIndex<FOCUS_MOBILE_FIRST_NUMBER) {
                                    focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                                }
                            }else {
                                mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER] = mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER].substring(0,mobileNumberInputString[focusIndex-FOCUS_MOBILE_FIRST_NUMBER].length-1);
                                mobileNumber[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].default_text.text(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER]);
                            }


                        }else if(focusIndex >= FOCUS_BIRTH_YEAR && focusIndex <= FOCUS_BIRTH_DAY) {

                            if(birthDayInputString[focusIndex-FOCUS_BIRTH_YEAR].length<=0){
                                focusIndex--;
                                if(focusIndex<FOCUS_BIRTH_YEAR) {
                                    focusIndex = FOCUS_BIRTH_YEAR;
                                }
                            }else {
                                birthDayInputString[focusIndex-FOCUS_BIRTH_YEAR] = birthDayInputString[focusIndex-FOCUS_BIRTH_YEAR].substring(0,birthDayInputString[focusIndex-FOCUS_BIRTH_YEAR].length-1);
                                birthDay[focusIndex - FOCUS_BIRTH_YEAR].default_text.text(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR]);
                            }

                        }else if(focusIndex >= FOCUS_AUTH_BTN && focusIndex <= FOCUS_INPUT_BOX) {
                            if(focusIndex === FOCUS_INPUT_BOX) {
                                if(mobileAuthString.length<=0) {
                                    focusIndex = FOCUS_AUTH_BTN;
                                }else {
                                    mobileAuthString = mobileAuthString.substring(0,mobileAuthString.length-1);
                                    var starString = "";
                                    for(var i=0;i<mobileAuthString.length;i++) {
                                        starString += "*";
                                        if(i !== (mobileAuthString.length-1)) {
                                            starString += " ";
                                        }
                                    }
                                    mobileAuthInputBox.focus_text.text(starString);
                                }
                            }
                        }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                            if(focusIndex === FOCUS_CANCEL) {
                                focusIndex = FOCUS_OK;
                            }
                        }
                        _focusUpdate();
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    if(isShowDropBox === false) {
                        if(focusIndex >= FOCUS_MOBILE_TYPE_KT && focusIndex <= FOCUS_MOBILE_TYPE_MVNO) {
                            focusIndex++;
                            if(focusIndex>FOCUS_MOBILE_TYPE_MVNO) {
                                focusIndex = FOCUS_MOBILE_TYPE_KT;
                            }
                        }else if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                            focusIndex++;
                            if(focusIndex>FOCUS_MOBILE_LAST_NUMBER) {
                                focusIndex = FOCUS_MOBILE_LAST_NUMBER;
                            }
                        }else if(focusIndex >= FOCUS_BIRTH_YEAR && focusIndex <= FOCUS_BIRTH_DAY) {
                            focusIndex++;
                            if(focusIndex>FOCUS_BIRTH_DAY) {
                                focusIndex = FOCUS_BIRTH_DAY;
                            }
                        }else if(focusIndex >= FOCUS_AUTH_BTN && focusIndex <= FOCUS_INPUT_BOX) {
                            focusIndex++;
                            if(focusIndex>FOCUS_INPUT_BOX) {
                                focusIndex = FOCUS_AUTH_BTN;
                            }
                        }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                            if(focusIndex === FOCUS_OK) {
                                focusIndex = FOCUS_CANCEL;
                            }
                        }
                        _focusUpdate();

                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.UP:
                    if(isShowDropBox === true) {
                        focusMVNOIndex--;
                        if(focusMVNOIndex<0) {
                            focusMVNOIndex = 3;
                        }
                        _focusUpdateMVNO();
                    }else {
                        if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                            focusIndex = selectMobileType;
                        }else if(focusIndex >= FOCUS_BIRTH_YEAR && focusIndex <= FOCUS_BIRTH_DAY) {
                            focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                        }else if(focusIndex >= FOCUS_AUTH_BTN && focusIndex <= FOCUS_INPUT_BOX) {
                            focusIndex = FOCUS_BIRTH_YEAR;
                        }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                            focusIndex = FOCUS_AUTH_BTN;
                        }
                        _focusUpdate();
                    }
                    break;
                case KTW.KEY_CODE.DOWN:
                    if(isShowDropBox === true) {
                        focusMVNOIndex++;
                        if(focusMVNOIndex>3) {
                            focusMVNOIndex = 0;
                        }
                        _focusUpdateMVNO();
                    }else {
                        if(focusIndex >= FOCUS_MOBILE_TYPE_KT && focusIndex <= FOCUS_MOBILE_TYPE_MVNO) {
                            focusIndex = FOCUS_MOBILE_FIRST_NUMBER;
                        }else if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                            focusIndex = FOCUS_BIRTH_YEAR;
                        }else if(focusIndex >= FOCUS_BIRTH_YEAR && focusIndex <= FOCUS_BIRTH_DAY) {
                            focusIndex = FOCUS_AUTH_BTN;
                        }else if(focusIndex >= FOCUS_AUTH_BTN && focusIndex <= FOCUS_INPUT_BOX) {
                            focusIndex = FOCUS_OK;
                        }else if(focusIndex >= FOCUS_OK && focusIndex <= FOCUS_CANCEL) {
                        }
                        _focusUpdate();

                    }
                    break;
            }

            if(consumed === false) {
                var number = util.keyCodeToNumber(key_code);
                if (number >= 0) {
                    if(focusIndex >= FOCUS_MOBILE_FIRST_NUMBER && focusIndex <= FOCUS_MOBILE_LAST_NUMBER) {
                        var maxlength = 0;
                        if((focusIndex - FOCUS_MOBILE_FIRST_NUMBER) === 0) {
                            maxlength = 3;
                        }else {
                            maxlength = 4;
                        }
                        if(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].length>=maxlength) {
                            mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER] = ""+ number;
                        }else {
                            mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER] += number;
                        }

                        mobileNumber[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].default_text.text(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER]);

                        if(mobileNumberInputString[focusIndex - FOCUS_MOBILE_FIRST_NUMBER].length>=maxlength) {
                            focusIndex++;
                            if(focusIndex>FOCUS_MOBILE_LAST_NUMBER) {
                                focusIndex = FOCUS_BIRTH_YEAR;
                            }
                            _focusUpdate();
                        }

                    }else if(focusIndex >= FOCUS_BIRTH_YEAR && focusIndex <= FOCUS_BIRTH_DAY) {
                        var maxlength = 0;
                        if((focusIndex - FOCUS_BIRTH_YEAR) === 0) {
                            maxlength = 4;
                        }else {
                            maxlength = 2;
                        }

                        if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length>=maxlength) {
                            if(focusIndex === FOCUS_BIRTH_YEAR) {
                                if(number == 2 || number === 1) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "";
                                }
                            }else if(focusIndex === FOCUS_BIRTH_MONTH) {
                                if(number === 0) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else if(number === 1) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "0" + number;
                                }

                            }else if(focusIndex === FOCUS_BIRTH_DAY) {
                                if(number === 0) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else if(number === 1) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else if(number === 2) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else if(number === 3) {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                }else {
                                    birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "0" + number;
                                }
                            }
                        }else {
                            if(focusIndex === FOCUS_BIRTH_YEAR) {
                                if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length === 0) {
                                    if(number === 2 || number === 1) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "";
                                    }
                                }else if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length === 1) {
                                    if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] === "1") {
                                        if(number === 9) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }else {
                                        if(number === 0) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }
                                }else if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length === 2) {
                                    if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] === "20") {
                                        if(number === 0 || number === 1) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }else {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                    }
                                }else if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length === 3) {
                                    var tempString  = birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] + number;
                                    var year = Number(tempString);
                                    if(year<(current_year.getYear()+1900)) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                    }
                                }
                            }else if(focusIndex === FOCUS_BIRTH_MONTH) {
                                if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length === 0) {
                                    if(number === 0) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else if(number === 1) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "0" + number;
                                    }
                                }else {
                                    if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] === "0") {
                                        if(number !== 0) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }else {
                                        if(number === 0 || number === 1 || number === 2) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }
                                }
                            }else if(focusIndex === FOCUS_BIRTH_DAY) {
                                if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length === 0) {
                                    if(number === 0) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else if(number === 1) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else if(number === 2) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else if(number === 3) {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "" + number;
                                    }else {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] = "0" + number;
                                    }
                                }else {
                                    if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] === "0") {
                                        if(number !== 0) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }else if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] === "3"){
                                        if(number === 0 || number === 1) {
                                            birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                        }
                                    }else {
                                        birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR] += number;
                                    }
                                }
                            }
                        }

                        birthDay[focusIndex - FOCUS_BIRTH_YEAR].default_text.text(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR]);

                        if(birthDayInputString[focusIndex - FOCUS_BIRTH_YEAR].length>=maxlength) {
                            focusIndex++;
                            if(focusIndex>FOCUS_BIRTH_DAY) {
                                focusIndex = FOCUS_AUTH_BTN;
                            }
                            _focusUpdate();
                        }


                    }else if(focusIndex === FOCUS_INPUT_BOX) {
                        var maxlength = 6;
                        if(mobileAuthString.length>=maxlength) {
                            mobileAuthString = "" + number;
                        }else {
                            mobileAuthString += number;
                        }
                        var starString = "";
                        for(var i=0;i<mobileAuthString.length;i++) {
                            starString += "*";
                            if(i !== (mobileAuthString.length-1)) {
                                starString += " ";
                            }
                        }

                        mobileAuthInputBox.focus_text.text(starString);

                        if(mobileAuthString.length>=maxlength) {
                            focusIndex = FOCUS_OK;
                            _focusUpdate();
                        }
                    }
                    consumed = true;
                }
            }

            if (isSystemPriority) {
                return true;
            }
            return consumed;
        };

        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 177 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:""
                    }
                },
                text: "비밀번호 초기화",
                parent: popup_container_div
            });

            rootDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "pw_root_div",
                    css: {
                        position: "absolute",
                        left: 483, top: 237, width: 954, height: 566
                    }
                },
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_l_img",
                    src: resourcePath + "pop_bg_pwreset_l.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 49, height: 566
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_m_img",
                    src: resourcePath + "pop_bg_pwreset_m.png",
                    css: {
                        position: "absolute", left: 49, top: 0, width: 856, height: 566
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_r_img",
                    src: resourcePath + "pop_bg_pwreset_r.png",
                    css: {
                        position: "absolute", left: 954-49, top: 0, width: 49, height: 566
                    }
                },
                parent: rootDiv
            });




            var usernameString = "가입자 성함 : " + _createSpanForName(userName);
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "user_name",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 78 , width: 954, height: 40,
                        color: "rgba(255, 255, 255, 1)", "font-size": 38 , "text-align": "center",
                        display:""
                    }
                },
                text: usernameString,
                parent: rootDiv
            });



            _createMobileTypeElement();
            _createMobileNumberElement();
            _createBirthDayElement();
            _createMobileAuthBtnElement();
            _createBtnElement();

            _createMVNODropBoxElement();
        }


        function _createSpanForName(user_name) {
            var tmp_name = "";
            var star_text = "";
            var front_text = "";
            var back_text = "";
            var name = "";
            var length;

            length = user_name.length;

            if (length <= 1) {
                return name = user_name;
            } else if (length === 2) {
                front_text = user_name.substr(0,1);
                tmp_name = front_text + "<strong style=" + "\"vertical-align:sub;\"" + "> *</strong>"
                return name = tmp_name;
            } else if (length > 2) {
                front_text = user_name.substr(0,1);
                back_text = user_name.substr(length-1, 1);
                for(var i = 0; i < length-2; i++) {
                    star_text = star_text + " *";
                }
                tmp_name = front_text + "<strong style=" + "\"vertical-align:sub;\">" + star_text + "</strong>" + " " + back_text;
                return name = tmp_name;
            }
        }


        function _createMobileTypeElement() {
            /**
             * LEFT 483 , TOP 237
             */
            var leftStart = 584-483;
            for(var i=0;i<4;i++) {
                var mobileTypeWidth = 0;

                if(i==3) {
                    mobileTypeWidth = 254;
                }else {
                    mobileTypeWidth = 158;
                }
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "mobile_type_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 388-237, width: mobileTypeWidth, height: 70
                        }
                    },
                    parent: rootDiv
                });

                var imgname = "";
                if(i === 3) {
                    imgname = "pop_btn_w254.png";

                }else {
                    imgname = "pop_btn_w158.png";
                }

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + imgname,
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: mobileTypeWidth,
                            height: 70
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        class : "focus_box_red_line" ,
                        css: {
                            position: "absolute", left: 0, top: 0, width: mobileTypeWidth, height: 70,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 19, top: 22, width: 200 , height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "left"
                        }
                    },
                    text: MOBILE_TYPE_NAME[i],
                    parent: btnDiv
                });

                var focusText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 19, top: 22, width: 200 , height: 32,
                            color: "white", "font-size": 30,"text-align" : "left"
                        }
                    },
                    text: MOBILE_TYPE_NAME[i],
                    parent: btnDiv
                });


                var checkImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "check_drop_img_" + (i+1),
                        src: resourcePath + "option_dropbox_select.png",
                        css: {
                            position: "absolute",
                            left: mobileTypeWidth - 51,
                            top: 12,
                            width: 45,
                            height: 45
                        }
                    },
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    default_text : defaultText,
                    focus_text : focusText,
                    check_drop_img : checkImg
                };

                mobileType[mobileType.length] = btnobj;

                leftStart+=mobileTypeWidth;
                leftStart+=8;
            }

        }


        function _createMobileNumberElement() {

            mobileNumberTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_mobile_number_title",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 585-483 , top: 497-237 , width: 150, height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "left",
                        display:""
                    }
                },
                text: "휴대폰 번호",
                parent: rootDiv
            });

            mobileNumberDelimiter1 =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 969-483, top: 509-237, width:12, height: 4,
                        "background-color": "rgba(255, 255, 255, 0.55)"
                    }
                },
                parent: rootDiv
            });

            mobileNumberDelimiter2 =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1157-483, top: 509-237, width:12, height: 4,
                        "background-color": "rgba(255, 255, 255, 0.55)"
                    }
                },
                parent: rootDiv
            });

            /**
             * LEFT 483 , TOP 237
             */
            var leftStart = 802-483;
            for(var i=0;i<3;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "mobile_number_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 476-237, width: 158, height: 70
                        }
                    },
                    parent: rootDiv
                });

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + "pop_input_box_w158.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 158,
                            height: 70
                        }
                    },
                    parent: btnDiv
                });


                var defaultTitle = "";
                if(i=== 0) {
                    defaultTitle = "000";
                }else {
                    defaultTitle = "0000";
                }

                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 20, width: 158 , height: 35,
                            color: "rgba(255, 255, 255, 01)", "font-size": 33,"text-align" : "center"
                        }
                    },
                    text: defaultTitle,
                    parent: btnDiv
                });


                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    default_text : defaultText
                };

                mobileNumber[mobileNumber.length] = btnobj;

                leftStart+=158;
                leftStart+=30;
            }


        }

        function _createBirthDayElement() {
            birthDayTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_birthday_title",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 585-483 , top: 585-237 , width: 150, height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "left",
                        display:""
                    }
                },
                text: "생년월일",
                parent: rootDiv
            });


            birthDayYearUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 967-483 , top: 585-237 , width: 32, height: 32,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 30 , "text-align": "left",
                        display:""
                    }
                },
                text: "년",
                parent: rootDiv
            });

            birthDayMonthUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 1138-483 , top: 585-237 , width: 32, height: 32,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 30 , "text-align": "left",
                        display:""
                    }
                },
                text: "월",
                parent: rootDiv
            });

            birthDayDayUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 1311-483 , top: 585-237 , width: 32, height: 32,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 30 , "text-align": "left",
                        display:""
                    }
                },
                text: "일",
                parent: rootDiv
            });

            /**
             * LEFT 483 , TOP 237
             */
            var leftStart = 802-483;
            for(var i=0;i<3;i++) {
                var divWidth = 0;
                if(i===0) {
                    divWidth = 158;
                }else {
                    divWidth = 128;
                }
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "birth_day_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 564-237, width: divWidth, height: 70
                        }
                    },
                    parent: rootDiv
                });

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + "pop_input_box_w"+divWidth+".png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: divWidth,
                            height: 70
                        }
                    },
                    parent: btnDiv
                });

                var defaultTitle = "";
                if(i=== 0) {
                    defaultTitle = "0000";
                }else {
                    defaultTitle = "00";
                }
                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 20, width: divWidth , height: 35,
                            color: "rgba(255, 255, 255, 01)", "font-size": 33,"text-align" : "center"
                        }
                    },
                    text: defaultTitle,
                    parent: btnDiv
                });


                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    default_text : defaultText
                };

                birthDay[birthDay.length] = btnobj;

                leftStart+=divWidth;
                leftStart+=45;
            }

        }

        function _createMobileAuthBtnElement() {
            mobileAuthBtnTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_mobile_auth_btn_title",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 585-483 , top: 673-237 , width: 150, height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "left",
                        display:""
                    }
                },
                text: "인증번호",
                parent: rootDiv
            });

            var btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_mobile_auth_div",
                    css: {
                        position: "absolute", left: 802-483, top: 652-237, width: 240, height: 70
                    }
                },
                parent: rootDiv
            });

            var defaultBtn = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "default_btn",
                    src: resourcePath + "pop_btn_w240.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 240,
                        height: 70
                    }
                },
                parent: btnDiv
            });

            var focusBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn",
                    class : "focus_box_red_line" ,
                    css: {
                        position: "absolute", left: 0, top: 0, width: 240, height: 70,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnDiv
            });

            var defaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 674-652, width: 240 , height: 30,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 28,"text-align" : "center"
                    }
                },
                text: "인증번호 받기",
                parent: btnDiv
            });

            var focusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 674-652, width: 240 , height: 30,
                        color: "white", "font-size": 28,"text-align" : "center"
                    }
                },
                text: "인증번호 받기",
                parent: btnDiv
            });

            mobileAuthBtn = {
                root_div : btnDiv,
                default_btn : defaultBtn,
                focus_btn : focusBtn,
                default_text : defaultText,
                focus_text : focusText
            };


            btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_mobile_auth_input_box_div",
                    css: {
                        position: "absolute", left: 1052-483, top: 652-237, width: 284, height: 70
                    }
                },
                parent: rootDiv
            });

            var defaultBtn = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "default_btn",
                    src: resourcePath + "pop_pw_box_w284.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 284,
                        height: 70
                    }
                },
                parent: btnDiv
            });

            var defaultText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 45, top: 23, width: 195 , height: 44,
                        color: "rgba(255, 255, 255, 0.2)", "font-size": 42,"text-align" : "left"
                    }
                },
                text: "* * * * * *",
                parent: btnDiv
            });

            var focusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 45, top: 23, width: 195 , height: 44,
                        color: "rgba(255, 255, 255, 1)", "font-size": 42,"text-align" : "left"
                    }
                },
                text: "",
                parent: btnDiv
            });


            mobileAuthInputBox = {
                root_div : btnDiv,
                default_btn : defaultBtn,
                default_text : defaultText,
                focus_text : focusText
            };
        }

        function _createBtnElement() {
            var leftStart = 673;
            for(var i=0;i<2;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "btn_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 820, width: 280, height: 62
                        }
                    },
                    parent: popup_container_div
                });

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + "pop_btn_w280.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 280,
                            height: 62
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        class : "focus_box_red_line" , 
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center"
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var focusText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "white", "font-size": 30,"text-align" : "center"
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    default_text : defaultText,
                    focus_text : focusText
                };


                if(i === 0) {
                    btnOk = btnobj;
                    btnOk.default_text.text("확인");
                    btnOk.focus_text.text("확인");
                }else {
                    btnCancel = btnobj;
                    btnCancel.default_text.text("취소");
                    btnCancel.focus_text.text("취소");
                }

                leftStart+=280;
                leftStart+=12;
            }
        }


        function _createMVNODropBoxElement() {
            var dropboxDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "mvno_type_dropbox_div",
                    css: {
                        position: "absolute", left: 1040-483, top: 351-237, width: 338, height: 358
                    }
                },
                parent: rootDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "drop_box_top_img",
                    src: resourcePath + "pop_btn_w254_drop_sdw_top.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 338, height: 107
                    }
                },
                parent: dropboxDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "drop_box_m_img",
                    src: resourcePath + "pop_btn_w254_drop_sdw_m.png",
                    css: {
                        position: "absolute", left: 0, top: 107, width: 338, height: 136
                    }
                },
                parent: dropboxDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "drop_box_btm_img",
                    src: resourcePath + "pop_btn_w254_drop_sdw_btm.png",
                    css: {
                        position: "absolute", left: 0, top: 243, width: 338, height: 107
                    }
                },
                parent: dropboxDiv
            });

            var dropboxbtnlist = _createMVNODropBoxBtnElement(dropboxDiv);

            var dropboxobj = {
                root_div :   dropboxDiv,
                drop_box_btn_list : dropboxbtnlist
            };

            dropbox = dropboxobj;
        }

        function _resetFocusMobileType() {
            for(var i=0;i<mobileType.length;i++) {
                var btnobj = mobileType[i];
                btnobj.default_btn.css("display" , "");
                btnobj.default_text.css("display" , "");

                btnobj.focus_btn.css("display" , "none");
                btnobj.focus_text.css("display" , "none");

                if(selectMobileType === i) {
                    btnobj.check_drop_img.attr("src", resourcePath + "option_dropbox_select.png");
                    btnobj.check_drop_img.css("display" , "");
                }else {
                    if(i === FOCUS_MOBILE_TYPE_MVNO) {
                        btnobj.check_drop_img.attr("src", resourcePath + "arw_option_popup_dw.png");
                        btnobj.check_drop_img.css("display" , "");
                    }else {
                        btnobj.check_drop_img.css("display" , "none");
                    }
                }
            }

            dropbox.root_div.css("display" , 'none');
        }


        function _createMVNODropBoxBtnElement(dropboxDiv) {
            /**
             * LEFT 1082 , TOP 338
             */
            var dropboxbtnList = [];
            var topStart = 388-351;
            for(var i=0;i<4;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "mobile_type_div_"+(i+1),
                        css: {
                            position: "absolute", left: 1082-1040, top: topStart, width: 254, height: 68
                        }
                    },
                    parent: dropboxDiv
                });


                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: resourcePath + "pop_btn_w254_drop.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 254,
                            height: 68
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        class : "focus_box_red_line" ,
                        css: {
                            position: "absolute", left: 0, top: 0, width: 254, height: 68,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var fontSize = 0;
                if(i === 3) {
                    fontSize = 26;
                }else {
                    fontSize = 30;
                }
                var defaultText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 19, top: 22, width: 200 , height: fontSize+2,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": fontSize,"text-align" : "left"
                        }
                    },
                    text: MVNO_TYPE_NAME[i],
                    parent: btnDiv
                });

                var focusText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 19, top: 22, width: 200 , height: fontSize+2,
                            color: "rgb(255,255,255)", "font-size": fontSize,"text-align" : "left"
                        }
                    },
                    text: MVNO_TYPE_NAME[i],
                    parent: btnDiv
                });

                var checkImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "check_drop_img_" + (i+1),
                        src: resourcePath + "option_dropbox_select.png",
                        css: {
                            position: "absolute",
                            left: 254 - 51,
                            top: 12,
                            width: 45,
                            height: 45
                        }
                    },
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    default_text : defaultText,
                    focus_text : focusText,
                    check_drop_img : checkImg
                };

                dropboxbtnList[dropboxbtnList.length] = btnobj;

                topStart+=68;
            }

            return dropboxbtnList;
        }


        function _resetFocusMobileNumber() {

            for(var i=0;i<mobileNumber.length;i++) {
                var mobilenumberobj = mobileNumber[i];
                mobilenumberobj.default_btn.css({opacity: 0.3});
                mobilenumberobj.default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                if(focusIndex !== FOCUS_MOBILE_FIRST_NUMBER && focusIndex !== FOCUS_MOBILE_MIDDLE_NUMBER && focusIndex !== FOCUS_MOBILE_LAST_NUMBER){
                    if(mobileNumberInputString[i] === "") {
                        if(i === 0) {
                            mobilenumberobj.default_text.text("000");
                        }else {
                            mobilenumberobj.default_text.text("0000");
                        }
                    }
                }
            }
        }

        function _resetFocusBirthDay() {
            for(var i=0;i<birthDay.length;i++) {
                var birthdayobj = birthDay[i];
                birthdayobj.default_btn.css({opacity: 0.3});

                var divWidth = 0;
                if(i===0) {
                    divWidth = 158;
                }else {
                    divWidth = 128;
                }
                birthdayobj.default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w"+ divWidth +".png");


                if(focusIndex !== FOCUS_BIRTH_YEAR && focusIndex !== FOCUS_BIRTH_MONTH && focusIndex !== FOCUS_BIRTH_DAY){
                    if(birthDayInputString[i] === "") {
                        if(i === 0) {
                            birthdayobj.default_text.text("0000");
                        }else {
                            birthdayobj.default_text.text("00");
                        }
                    }
                }
            }
        }

        function _resetFocusMobileAuthBtnInputBox() {
            mobileAuthBtn.default_btn.css("display" , "");
            mobileAuthBtn.default_text.css("display" , "");
            mobileAuthBtn.focus_btn.css("display" , "none");
            mobileAuthBtn.focus_text.css("display" , "none");

            mobileAuthInputBox.default_btn.css({opacity: 0.3});
            mobileAuthInputBox.default_btn.attr("src" , "images/inputbox_unfocus/pop_pw_box_w284.png");
        }


        function _resetFocusBtn() {

            btnOk.default_btn.css("display" , "");
            btnOk.default_text.css("display" , "");
            btnOk.focus_btn.css("display" , "none");
            btnOk.focus_text.css("display" , "none");


            btnCancel.default_btn.css("display" , "");
            btnCancel.default_text.css("display" , "");
            btnCancel.focus_btn.css("display" , "none");
            btnCancel.focus_text.css("display" , "none");

        }

        function _focusUpdate() {
            _resetFocusMobileType();
            _resetFocusMobileNumber();
            _resetFocusBirthDay();
            _resetFocusMobileAuthBtnInputBox();
            _resetFocusBtn();

            if(focusIndex === FOCUS_MOBILE_TYPE_KT || focusIndex === FOCUS_MOBILE_TYPE_SKT || focusIndex === FOCUS_MOBILE_TYPE_LG || focusIndex === FOCUS_MOBILE_TYPE_MVNO) {
                var btnobj = mobileType[focusIndex];

                btnobj.default_btn.css("display" , "none");
                btnobj.default_text.css("display" , "none");

                btnobj.focus_btn.css("display" , "");
                btnobj.focus_text.css("display" , "");

                if(selectMobileType === focusIndex) {
                    btnobj.check_drop_img.attr("src", resourcePath + "option_dropbox_select_f.png");
                    btnobj.check_drop_img.css("display" , "");
                }else {
                    if(focusIndex === FOCUS_MOBILE_TYPE_MVNO) {
                        btnobj.check_drop_img.attr("src", resourcePath + "arw_option_popup_dw_f.png");
                        btnobj.check_drop_img.css("display" , "");
                    }else {
                        btnobj.check_drop_img.css("display" , "none");
                    }
                }
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        if(i === 0) {
                            mobileNumber[i].default_text.text("000");
                        }else {
                            mobileNumber[i].default_text.text("0000");
                        }
                    }
                }

                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

            }else if(focusIndex === FOCUS_MOBILE_FIRST_NUMBER){
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        mobileNumber[i].default_text.text("");
                    }
                }
                mobileNumber[0].default_btn.css({opacity: 1});
                mobileNumber[0].default_btn.attr("src" , resourcePath+"pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

            }else if(focusIndex === FOCUS_MOBILE_MIDDLE_NUMBER){
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        mobileNumber[i].default_text.text("");
                    }
                }
                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 1});
                mobileNumber[1].default_btn.attr("src" , resourcePath+"pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 0.3});
                mobileNumber[2].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");
            }else if(focusIndex === FOCUS_MOBILE_LAST_NUMBER){
                for(var i=0;i<3;i++) {
                    if(mobileNumberInputString[i] === "") {
                        mobileNumber[i].default_text.text("");
                    }
                }
                mobileNumber[0].default_btn.css({opacity: 0.3});
                mobileNumber[0].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[1].default_btn.css({opacity: 0.3});
                mobileNumber[1].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                mobileNumber[2].default_btn.css({opacity: 1});
                mobileNumber[2].default_btn.attr("src" , resourcePath+"pop_input_box_w158.png");

            }else if(focusIndex === FOCUS_BIRTH_YEAR){
                for(var i=0;i<3;i++) {
                    if(birthDayInputString[i] === "") {
                        birthDay[i].default_text.text("");
                    }
                }

                birthDay[0].default_btn.css({opacity: 1});
                birthDay[0].default_btn.attr("src" , resourcePath + "pop_input_box_w158.png");

                birthDay[1].default_btn.css({opacity: 0.3});
                birthDay[1].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w128.png");

                birthDay[2].default_btn.css({opacity: 0.3});
                birthDay[2].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w128.png");

            }else if(focusIndex === FOCUS_BIRTH_MONTH){
                for(var i=0;i<3;i++) {
                    if(birthDayInputString[i] === "") {
                        birthDay[i].default_text.text("");
                    }
                }
                birthDay[0].default_btn.css({opacity: 0.3});
                birthDay[0].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                birthDay[1].default_btn.css({opacity: 1});
                birthDay[1].default_btn.attr("src" , resourcePath + "pop_input_box_w128.png");

                birthDay[2].default_btn.css({opacity: 0.3});
                birthDay[2].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w128.png");
            }else if(focusIndex === FOCUS_BIRTH_DAY){
                for(var i=0;i<3;i++) {
                    if(birthDayInputString[i] === "") {
                        birthDay[i].default_text.text("");
                    }
                }
                birthDay[0].default_btn.css({opacity: 0.3});
                birthDay[0].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w158.png");

                birthDay[1].default_btn.css({opacity: 0.3});
                birthDay[1].default_btn.attr("src" , "images/inputbox_unfocus/pop_input_box_w128.png");

                birthDay[2].default_btn.css({opacity: 1});
                birthDay[2].default_btn.attr("src" , resourcePath + "pop_input_box_w128.png");
                
            }else if(focusIndex === FOCUS_AUTH_BTN){
                mobileAuthBtn.default_btn.css("display" , "none");
                mobileAuthBtn.default_text.css("display" , "none");
                mobileAuthBtn.focus_btn.css("display" , "");
                mobileAuthBtn.focus_text.css("display" , "");
            }else if(focusIndex === FOCUS_INPUT_BOX){
                mobileAuthInputBox.default_btn.css({opacity: 1});
                mobileAuthInputBox.default_btn.attr("src" , resourcePath + "pop_pw_box_w284.png");
            }else if(focusIndex === FOCUS_OK){
                btnOk.default_btn.css("display" , "none");
                btnOk.default_text.css("display" , "none");
                btnOk.focus_btn.css("display" , "");
                btnOk.focus_text.css("display" , "");
            }else if(focusIndex === FOCUS_CANCEL){
                btnCancel.default_btn.css("display" , "none");
                btnCancel.default_text.css("display" , "none");
                btnCancel.focus_btn.css("display" , "");
                btnCancel.focus_text.css("display" , "");
            }
        }

        function _showDropBox() {
            if(dropbox !== null) {
                var dropboxlist = dropbox.drop_box_btn_list;

                if(dropboxlist !== null && dropboxlist.length>0) {

                    for(var i=0;i<dropboxlist.length;i++) {
                        var btnobj = dropboxlist[i];

                        btnobj.default_btn.css("display" , "");
                        btnobj.default_text.css("display" , "");

                        btnobj.focus_btn.css("display" , "none");
                        btnobj.focus_text.css("display" , "none");

                        if(selectMVNOType === i) {
                            btnobj.check_drop_img.attr("src", resourcePath + "option_dropbox_select.png");
                            btnobj.check_drop_img.css("display" , "");
                        }else {
                            btnobj.check_drop_img.css("display" , "none");
                        }
                    }
                    var focusbtnobj = dropboxlist[focusMVNOIndex];
                    focusbtnobj.default_btn.css("display" , "none");
                    focusbtnobj.default_text.css("display" , "none");

                    focusbtnobj.focus_btn.css("display" , "");
                    focusbtnobj.focus_text.css("display" , "");

                    if(selectMVNOType === focusMVNOIndex) {
                        focusbtnobj.check_drop_img.attr("src", resourcePath + "option_dropbox_select_f.png");
                        focusbtnobj.check_drop_img.css("display" , "");
                    }else {
                        focusbtnobj.check_drop_img.css("display" , "none");
                    }

                }
                dropbox.root_div.css("display" , "");

                mobileNumberTitle.css({opacity: 0.3});
                mobileNumberDelimiter1.css({opacity: 0.3});
                mobileNumberDelimiter2.css({opacity: 0.3});

                for(var i=0;i<mobileNumber.length;i++) {
                    var mobilenumberobj = mobileNumber[i];
                    mobilenumberobj.root_div.css({opacity: 0.3});
                }

                birthDayTitle.css({opacity: 0.3});
                birthDayYearUnit.css({opacity: 0.3});
                birthDayMonthUnit.css({opacity: 0.3});
                birthDayDayUnit.css({opacity: 0.3});

                for(var i=0;i<birthDay.length;i++) {
                    var birthdayobj = birthDay[i];
                    birthdayobj.root_div.css({opacity: 0.3});
                }

                mobileAuthBtnTitle.css({opacity: 0.3});

                mobileAuthBtn.root_div.css({opacity: 0.3});
                mobileAuthInputBox.default_btn.css({opacity: 0.3});
            }
        }

        function _hideDropBox() {
            mobileNumberTitle.css({opacity: 1});
            mobileNumberDelimiter1.css({opacity: 1});
            mobileNumberDelimiter2.css({opacity: 1});

            for(var i=0;i<mobileNumber.length;i++) {
                var mobilenumberobj = mobileNumber[i];
                mobilenumberobj.root_div.css({opacity: 1});
            }

            birthDayTitle.css({opacity: 1});
            birthDayYearUnit.css({opacity: 1});
            birthDayMonthUnit.css({opacity: 1});
            birthDayDayUnit.css({opacity: 1});

            for(var i=0;i<birthDay.length;i++) {
                var birthdayobj = birthDay[i];
                birthdayobj.root_div.css({opacity: 1});
            }

            mobileAuthBtnTitle.css({opacity: 1});
            mobileAuthBtn.root_div.css({opacity: 1});

            dropbox.root_div.css("display" , "none");
        }



        function _showView() {
            log.printDbg("_showView()");

            if(initShow === false) {
                initShow = true;
                _focusUpdate();
            }
        }

        function _focusUpdateMVNO() {
            var dropboxlist = dropbox.drop_box_btn_list;

            if(dropboxlist !== null && dropboxlist.length>0) {

                for(var i=0;i<dropboxlist.length;i++) {
                    var btnobj = dropboxlist[i];

                    btnobj.default_btn.css("display" , "");
                    btnobj.default_text.css("display" , "");

                    btnobj.focus_btn.css("display" , "none");
                    btnobj.focus_text.css("display" , "none");

                    if(selectMVNOType === i) {
                        btnobj.check_drop_img.attr("src", resourcePath + "option_dropbox_select.png");
                        btnobj.check_drop_img.css("display" , "");
                    }else {
                        btnobj.check_drop_img.css("display" , "none");
                    }
                }
                var focusbtnobj = dropboxlist[focusMVNOIndex];
                focusbtnobj.default_btn.css("display" , "none");
                focusbtnobj.default_text.css("display" , "none");

                focusbtnobj.focus_btn.css("display" , "");
                focusbtnobj.focus_text.css("display" , "");

                if(selectMVNOType === focusMVNOIndex) {
                    focusbtnobj.check_drop_img.attr("src", resourcePath + "option_dropbox_select_f.png");
                    focusbtnobj.check_drop_img.css("display" , "");
                }else {
                    focusbtnobj.check_drop_img.css("display" , "none");
                }

            }

        }


        function _keyOk() {
            log.printDbg("_keyOk()");

        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _callbackLicenseListPopup() {
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_MOBILE_BRANDS_POPUP
            });
        }

        function _callbackFuncReqSMSComm(result, xhr, error_message) {
            log.printDbg("_callbackFuncReqSMSComm");
            var code;
            var message_ori;
            var message;
            var desc = [];
            var width_flag = false;

            KTW.ui.LayerManager.stopLoading();

            log.printDbg("_callbackFuncReqSMSComm result : " + result);
            log.printDbg("_callbackFuncReqSMSComm xhr : " + xhr);
            log.printDbg("_callbackFuncReqSMSComm error_message : " + error_message);

            if (result) {
                code = xhr.responseXML.getElementsByTagName("RCode");

                if (code[0].textContent === "SHD0000100") {
                    ckey = xhr.responseXML.getElementsByTagName("CKEY")[0].textContent;
                } else {
                    var message_ori = xhr.responseXML.getElementsByTagName("RMsg")[0].textContent;
                    message = message_ori.split("\\n");
                    showErrorPopup(message);
                }
            } else {
                /**
                 * HDSManager에서 팝업 띄우도록 수정함.
                 */
                if(error_message !== undefined && error_message !== null) {
                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , error_message, null, null, true);
                    }else {
                        if(error_message.error === true) {
                            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                        }
                    }
                }
                //showErrorPopup([ "인증번호 요청에 실패했습니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다" ]);
            }
        }

        function _callbackFuncReqSMSCnfrmComm (result, xhr, error_message) {
            log.printDbg("_callbackFuncReqSMSCnfrmComm");
            var code;
            var message_ori;
            var message;
            var desc = [];

            log.printDbg("_callbackFuncReqSMSCnfrmComm result : " + result);
            log.printDbg("_callbackFuncReqSMSCnfrmComm xhr : " + xhr);
            log.printDbg("_callbackFuncReqSMSCnfrmComm error_message : " + error_message);

            KTW.ui.LayerManager.stopLoading();

            if (result) {
                code = xhr.responseXML.getElementsByTagName("RCode");
                if (code[0].textContent === "SHD0000100") {
                    KTW.ui.LayerManager.startLoading();

                    var now = new Date().getTime();
                    now = Math.floor(now/1000);
                    log.printDbg("onHDSAuthPinResponse() now : " + now);

                    KTW.managers.http.HDSManager.initBuyAndAdultPin(mobileAuthString, ckey, now , _callbackFuncInitBuyAndAdultPin);

                } else {
                    message_ori = xhr.responseXML.getElementsByTagName("RMsg")[0].textContent;
                    message = message_ori.split("\\n");
                    showErrorPopup(message);
                }
            } else {
                /**
                 * HDSManager에서 팝업 띄우도록 수정함.
                 */
                if(error_message !== undefined && error_message !== null) {
                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , error_message, null, null, true);
                    }else {
                        if(error_message.error === true) {
                            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                        }
                    }
                }
            }
        }


        function _callbackFuncInitBuyAndAdultPin(result, xhr, error_message) {
            log.printDbg("_callbackFuncInitBuyAndAdultPin");

            var code;
            var message_ori;
            var message;
            var desc = [];

            log.printDbg("_callbackFuncInitBuyAndAdultPin result : " + result);
            log.printDbg("_callbackFuncInitBuyAndAdultPin xhr : " + xhr);
            log.printDbg("_callbackFuncInitBuyAndAdultPin error_message : " + error_message);

            KTW.ui.LayerManager.stopLoading();

            if (result) {
                code = xhr.responseXML.getElementsByTagName("RCode");
                if (code[0].textContent === "SHD0000100") {
                    KTW.oipf.AdapterHandler.casAdapter.resetPIN();

                    if(callbackFunc !== null) {
                        callbackFunc(true);
                    }
                } else {
                    message_ori = xhr.responseXML.getElementsByTagName("RMsg")[0].textContent;
                    message = message_ori.split("\\n");
                    showErrorPopup(message);
                }
            } else {
                /**
                 * HDSManager에서 팝업 띄우도록 수정함.
                 */
                if(error_message !== undefined && error_message !== null) {
                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , error_message, null, null, true);
                    }else {
                        if(error_message.error === true) {
                            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E019.split("\n"), null, null, true);
                        }
                    }
                }
            }
        }



        function showErrorPopup(message , code) {
            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                    message: ["인증오류"],
                    cssObj: {}
                }],
                arrButton: [{id: "ok", name: "확인"}],
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        if (buttonId === "ok") {
                            KTW.ui.LayerManager.deactivateLayer({
                                id: resetErrorPopupLayerId
                            });
                        }
                    }
                }
            };

            var arrMessage = [];
            for(var i=0;i<message.length;i++) {
                arrMessage[arrMessage.length] = message[i];
            }
            popupData.arrMessage.push({
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                message: arrMessage,
                cssObj: {}
            });

            if(code !== undefined && code !== null) {
                popupData.arrMessage.push({
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_32,
                    message: [code],
                    cssObj: {}
                });
            }

            /**
             * TODO 이 부분 추후 처리 해야 함.
             */
            var priority = isSystemPriority ? KTW.ui.Layer.PRIORITY.SYSTEM_POPUP + 5 : KTW.ui.Layer.PRIORITY.POPUP;
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: resetErrorPopupLayerId,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: priority,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData,
                        enableDCA : isSystemPriority === true ? "false" : "true",
                        enableChannelKey : isSystemPriority === true ? "false" : "true",
                        enableMenuKey : isSystemPriority === true ? "false" : "true",
                        enableHotKey : isSystemPriority === true ? "false" : "true"
                    }
                },
                visible: true,
                cbActivate: function () {}
            });
        }
    };

    KTW.ui.layer.setting.popup.setting_resetPasswordPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_resetPasswordPopup.constructor = KTW.ui.layer.setting.popup.setting_resetPasswordPopup;

    KTW.ui.layer.setting.popup.setting_resetPasswordPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_resetPasswordPopup.prototype.show = function(options) {
        //log.printDbg("show()");
        this.showView(options);
        KTW.ui.Layer.prototype.show.call(this, options);
    };


    KTW.ui.layer.setting.popup.setting_resetPasswordPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();