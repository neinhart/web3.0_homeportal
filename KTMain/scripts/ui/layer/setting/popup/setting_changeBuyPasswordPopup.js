/**
 * Created by Taesang on 2016-12-30.
 */

(function() {

    KTW.ui.layer.setting.popup.setting_changeBuyPassword  = function changeBuyPasswordPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div, data, passwordCnt = 0;
        var focusRow = 0, focusIdx = 0;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var log = KTW.utils.Log;

        var callbackFuncSuccess = null;
        var isSystemPriority = false;

        var isStartSSpeechRecognizer = false;

        function getStarText(_passwordCnt) {
            var starText = "";
            if (_passwordCnt > 0) {
                for (var i = 0; i < _passwordCnt; i++) {
                    if (_passwordCnt == 4 && i == 3) starText += "*";
                    else starText += "* "
                }
            }
            return starText;
        }

        function focusIdxChange() {
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        function focusRowChange(_row) {
            if (_row != null) focusRow = _row;
            focusIdx = 0;
            div.find(".passwordArea.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            if (focusRow == 0) {
                div.find(".passwordArea").addClass("focus");
                _startSpeechRecognizer();
            }
            else if (focusRow == 1) {
                focusIdxChange();
                _stopSpeechRecognizer();
            }
        }

        function fillPassword() {
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
        }

        function changePassword() {
            // TODO
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup changeBuyPassword");
            div.append($("<title/>").text("인증"));
            div.append($("<span/>", {class: "text_white_center_38M"}).html("구매인증 번호를 변경하시려면" + "<br>" + "olleh tv 비밀번호(성인인증)를 입력해 주세요"));
            //div.append($("<span/>", {class: "text_white_center_33L"}).text("성인인증 비밀번호를 입력해 주세요"));
            div.append($("<div/>", {class: "passwordArea"}));
            div.find(".passwordArea").append($("<div/>", {class: "inputBox"}));
            div.find(".passwordArea").append($("<span/>", {class: "password"}).text("* * * *"));
            div.find(".passwordArea").append($("<span/>", {class: "password fill"}));
            div.append($("<div/>", {class: "twoBtnArea_280"}));

            div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
            div.find(".twoBtnArea_280 .btn").append($("<div/>", {class: "whiteBox"}));
            div.find(".twoBtnArea_280 .btn").append($("<span/>", {class: "btnText"}).text("취소"));

            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            Layer.prototype.show.call(this);
            if(data !== undefined && data !== null) {
                callbackFuncSuccess = data.callback;
                isSystemPriority = data.isSystemPriority;
            }
            KTW.managers.auth.AuthManager.initPW();
            // 2017.05.03 dhlee 오류 횟수 초기화
            KTW.managers.auth.AuthManager.resetPWCheckCount();

            passwordCnt = 0;
            fillPassword();

            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);
            }
            focusIdx = 0;
            focusRowChange(0);
            
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);
            }
            _stopSpeechRecognizer();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(focusRow != 1) {
                        var pw = KTW.managers.auth.AuthManager.getPW();
                        if(pw !== null && pw.length>0) {
                            _deleteNum();
                            return true;
                        }else {
                            return false;
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 1) {
                        LayerManager.historyBack();
                    }else {
                        _checkPW();
                    }
                    return true;
                case KEY_CODE.UP:
                    focusRowChange(0);
                    return true;
                case KEY_CODE.DOWN:
                    focusRowChange(1);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    LayerManager.historyBack();
                    callbackFuncSuccess(false);
                    return true;

/*                    var consume = false;
                    if (this.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                        LayerManager.historyBack();
                        consume = true;
                    }*//*
                    LayerManager.historyBack();
                    callbackFuncSuccess(false);*/
                    return true;
                default:
                    var number = KTW.utils.util.keyCodeToNumber(keyCode);
                    if (number >= 0) {
                        if (focusRow == 0) {
                            _inputNum(number);
                        }
                        return true;
                    }
                    if (isSystemPriority) {
                        return true;
                    }
                    break;
            }
        };

        function _inputNum(num , maxLength) {
            log.printDbg('_inputNum() num : ' + num);
            var authManager = KTW.managers.auth.AuthManager;


            var authMaxLength = 4;
            if(maxLength !== undefined && maxLength !== null) {
                authMaxLength = maxLength;
            }

            if (authManager.inputPW(num, authMaxLength) === true) {
                passwordCnt =  authManager.getPW().length;
                log.printDbg('_inputNum() authManager.inputPW() return true passwordCnt : ' + passwordCnt);
                passwordCnt =  authManager.getPW().length;
                fillPassword();
                _checkPW();
            }
            else {
                passwordCnt =  authManager.getPW().length;
                log.printDbg('_inputNum() authManager.inputPW() return false passwordCnt : ' + passwordCnt);
                fillPassword();
            }
        }

        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum() {
            log.printDbg('_deleteNum() ');
            var authManager = KTW.managers.auth.AuthManager;

            authManager.deletePW();
            passwordCnt =  authManager.getPW().length;
            fillPassword();
        }

        /**
         * 입력한 비밀번호 일치여부 확인
         */
        function _checkPW() {
            var authManager = KTW.managers.auth.AuthManager;

            log.printDbg('_checkPW() ');

            var auth_type = (KTW.CONSTANT.IS_OTS === true && Number(KTW.SMARTCARD_ID) === 0) ?
                authManager.AUTH_TYPE.AUTH_ADULT_PIN_FOR_MINIEPG : authManager.AUTH_TYPE.AUTH_ADULT_PIN;
            var obj = {
                type : auth_type,
                callback : callbackFuncAuthResult,
                loading : {
                    type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock : true,
                    callback : null,
                    on_off : false
                }
            };

            authManager.checkUserPW(obj);
        }

        /**
         * AuthManager로 부터 인증 결과 Callback 함수
         */
        function callbackFuncAuthResult(result) {
            var authManager = KTW.managers.auth.AuthManager;

            log.printInfo('callbackFuncAuthResult() result: ' + result);
            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS) {
                // 2017.05.03 dhlee 오류 횟수 초기화
                authManager.resetPWCheckCount();
                if(callbackFuncSuccess !== null) {
                    callbackFuncSuccess(true);
                }

            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.CANCEL ||
                result === authManager.RESPONSE.GOTO_INIT) {
                authManager.initPW();

                setTimeout(function(){
                    passwordCnt =  authManager.getPW().length;
                    fillPassword();
                }, 300);

               // if(authManager.getPWCheckCount()<3) {
                    div.find(".text_white_center_38M").html("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
                //}
                //[sw.nam] 2017.05.23 구매 확인 체크시에는 입력횟수 제한을 두지 않음 (플립북1.21 p867)
                KTW.managers.auth.AuthManager.resetPWCheckCount();
            } else if (result === authManager.RESPONSE.DISCONNECTED_NETWORK) {
                KTW.managers.service.SimpleMessageManager.showPasswordCheckNetworkErrorNotiMessage();
            }
        }


        function _startSpeechRecognizer() {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() isStartSSpeechRecognizer : " + isStartSSpeechRecognizer);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                _stopSpeechRecognizer();
            }
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.start(true);
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            isStartSSpeechRecognizer = true;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() isStartSSpeechRecognizer :  " + isStartSSpeechRecognizer);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.stop();
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);

                isStartSSpeechRecognizer = false;
            }
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
            // mode = KTW.oipf.Def.SR_MODE.PIN;
            // type = "recognized";
            // input = "000";
            if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                KTW.managers.auth.AuthManager.initPW();
                passwordCnt =  KTW.managers.auth.AuthManager.getPW().length;
                fillPassword();

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>4) {
                        length = 4;
                    }

                    for(var i=0;i<length;i++) {
                        _inputNum(input[i] , length);
                    }
                }
            }
        }

    };
    KTW.ui.layer.setting.popup.setting_changeBuyPassword.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_changeBuyPassword.constructor = KTW.ui.layer.setting.popup.setting_changeBuyPassword;

    KTW.ui.layer.setting.popup.setting_changeBuyPassword.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_changeBuyPassword.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();