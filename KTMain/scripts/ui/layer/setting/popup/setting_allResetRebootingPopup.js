/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>setting_allResetRebootingPopup</code>
 *
 * @author sw.nam
 * @since 2017-03-08
 */

/**
 * Created by Yun on 2017-03-02.
 */

(function () {
    KTW.ui.layer.setting.popup.setting_allResetRebootingPopup  = function allResetRebootingPopup(options) {
        KTW.ui.Layer.call(this, options);



        var div;
        var log = KTW.utils.Log;
        //var KEY_CODE = KTW.KEY_CODE;
        //var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        log.printDbg("setting_allResetRebootingPopup");
        //var data;
        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup allResettingSystemPopup");
            div.append($("<title/>").css({"position":"absolute", "top":"469px"}).text("전체 초기화"));
            div.append($("<span/>", {class:"text_white_center_52M"}).text("시스템을 초기화 했습니다"));
            div.append($("<span/>", {class:"text_white_center_38L"}).text("시스템을 다시 시작합니다"));
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);

        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            return true;
        };
    };
    KTW.ui.layer.setting.popup.setting_allResetRebootingPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_allResetRebootingPopup.constructor = KTW.ui.layer.setting.popup.setting_allResetRebootingPopup;

    KTW.ui.layer.setting.popup.setting_allResetRebootingPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_allResetRebootingPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();