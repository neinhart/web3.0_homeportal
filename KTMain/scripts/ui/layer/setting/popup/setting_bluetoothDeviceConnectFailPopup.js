/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_btDeviceConnectFail  = function btDeviceConnectFailPopup(options) {
        KTW.ui.Layer.call(this, options);

        var data, div, btnText = ["재검색", "취소"], idx;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        //var settingBluetoothConnect = KTW.ui.layer.setting.setting_bluetoothConnect;

        function changeFocus() {
            div.find(".twoBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".twoBtnArea_280 .btn").eq(idx).addClass("focus");
        }

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup btDeviceConnectFail");
            div.append($("<title/>").text("알림"));
            div.append($("<span/>", {class: "text_white_center_52M"}));
            div.append($("<span/>", {class: "text_white_center_38L"}));
            div.append($("<span/>", {class: "text_white_center_30L"}).text("다시 시도하시겠습니까?"));
            div.append($("<div/>", {class: "twoBtnArea_280"}));
            for (var i = 0; i < 2; i++) {
                div.find(".twoBtnArea_280").append($("<div/>", {class: "btn"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<div/>", {class: "whiteBox"}));
                div.find(".twoBtnArea_280 .btn").eq(i).append($("<span/>", {class: "btnText"}).text(btnText[i]));
            }
            cbCreate(true);
        };

        this.show = function() {
            data = this.getParams();
            idx = 0;
            changeFocus();
            // 장치명
            div.find(".text_white_center_52M").text(data.device.name);
            // 에러 문구 (errorMode 값에 따라 다르게 출력)
            div.find(".text_white_center_38L").text(data.errorCode == 0 ? "장치 연결에 실패했습니다" : "입력한 번호가 일치하지 않아 장치 연결에 실패했습니다");
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    idx = KTW.utils.util.indexMinus(idx, 2);
                    changeFocus();
                    return true;
                case KEY_CODE.RIGHT:
                    idx = KTW.utils.util.indexPlus(idx, 2);
                    changeFocus();
                    return true;
                case KEY_CODE.ENTER:
                    if (idx == 0) {
                        // 재검색 시도
                        LayerManager.historyBack();
                        data.complete();
                    } else
                        LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_btDeviceConnectFail.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_btDeviceConnectFail.constructor = KTW.ui.layer.setting.popup.setting_btDeviceConnectFail;

    KTW.ui.layer.setting.popup.setting_btDeviceConnectFail.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.popup.setting_btDeviceConnectFail.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})();