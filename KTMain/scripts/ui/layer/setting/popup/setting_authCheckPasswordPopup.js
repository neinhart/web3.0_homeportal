/**
 * Created by Taesang on 2016-12-30.
 */

(function() {

    KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup  = function authCheckPasswordPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div, data, passwordCnt = 0, btnText = ["취소"];
        var focusRow = 0, focusIdx = 0;
        var KEY_CODE = KTW.KEY_CODE;
        //var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var log = KTW.utils.Log;

        var callbackFuncSuccess = null;
        var kidmodeoff = false;

        var isStartSSpeechRecognizer = false;

        function getStarText(_passwordCnt) {
            var starText = "";
            if (_passwordCnt > 0) {
                for (var i = 0; i < _passwordCnt; i++) {
                    if (_passwordCnt == 4 && i == 3) starText += "*";
                    else starText += "* "
                }
            }
            return starText;
        }

        function focusIdxChange() {
            div.find(".oneBtnArea_280 .btn.focus").removeClass("focus");
            div.find(".oneBtnArea_280 .btn").eq(focusIdx).addClass("focus");
        }

        function focusRowChange(_row) {
            if (_row != null) focusRow = _row;
            focusIdx = 0;
            div.find(".passwordArea.focus").removeClass("focus");
            div.find(".oneBtnArea_280 .btn.focus").removeClass("focus");
            if (focusRow == 0) {
                div.find(".passwordArea").addClass("focus");
                _startSpeechRecognizer();
            }
            else if (focusRow == 1)  {
                focusIdxChange();
                _stopSpeechRecognizer();
            }
        }

        function fillPassword() {
            div.find(".passwordArea .password.fill").text(getStarText(passwordCnt));
        }

        this.init = function(cbCreate) {
            data = this.getParams();

            if(data !== undefined && data !== null) {
                callbackFuncSuccess = data.callback;
                if(data.is_kids_mode_off === undefined || data.is_kids_mode_off === null) {
                    kidmodeoff = false;
                }else {
                    if(data.is_kids_mode_off === true) {
                        kidmodeoff = true;
                    }
                }
            }


            div = this.div;
            div.addClass("arrange_frame setting gniPopup auchCheckPassword");
            if(kidmodeoff === true) {
                //div.css("color" , "rgba(18,17,22,0.9)");
                div.addClass("onKidsMode");
                div.append($("<img/>", { class: "kids_mode_exit_logo_img" , src: "images/popup/img_kidsmode_set_pop.png"}));
                div.append($("<kids_title/>").text("키즈모드 종료"));
            }else {
                div.append($("<title/>").text("인증"));
            }


            if(kidmodeoff === true) {
                div.append($("<span/>", {class: "text_white_center_42M"}).html("키즈모드를 종료하시겠습니까?"));
                div.find(".text_white_center_42M").css({top:443});

                div.append($("<span/>", {class: "text_white_center_27L"}).html("olleh tv 비밀번호(성인인증)를 입력해 주세요"));
                div.find(".text_white_center_27L").css({top:513});
                div.find(".text_white_center_27L").css("color" , "rgba(254,254,254,0.3)");
            }else {
                div.append($("<span/>", {class: "text_white_center_42M"}).html("olleh tv 비밀번호(성인인증)를<br>입력해 주세요"));
            }
            div.find(".text_white_center_42M").css({"line-height":1.2});
            div.append($("<div/>", {class: "passwordArea"}));
            if(kidmodeoff === true) {
                div.find(".passwordArea").css({top:578});
            }
            div.find(".passwordArea").append($("<div/>", {class: "inputBox"}));
            div.find(".passwordArea").append($("<span/>", {class: "password"}).text("* * * *"));
            div.find(".passwordArea").append($("<span/>", {class: "password fill"}));
            div.append($("<div/>", {class: "oneBtnArea_280"}));

            if(kidmodeoff === true) {
                div.find(".oneBtnArea_280").css({top:718});
            }
            div.find(".oneBtnArea_280").append($("<div/>", {class: "btn"}));
            div.find(".oneBtnArea_280 .btn").append($("<div/>", {class: "whiteBox"}));
            div.find(".oneBtnArea_280 .btn").append($("<span/>", {class: "btnText"}).text(btnText[0]));


            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
            KTW.managers.auth.AuthManager.initPW();
            // dhlee 2017.05.03 오류 횟수 초기화
            KTW.managers.auth.AuthManager.resetPWCheckCount();

            passwordCnt = 0;
            fillPassword();

            focusIdx = 0;
            focusRowChange(0);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
            _stopSpeechRecognizer();
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if (focusRow == 0) {
                        var pw = KTW.managers.auth.AuthManager.getPW();
                        if(pw !== null && pw.length>0) {
                            _deleteNum();
                            return true;
                        }else {
                            return false;
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.ENTER:
                    if (focusRow == 0) {
                        _checkPW();
                    }else {
                        callbackFuncSuccess(false);
                    }
                    return true;
                case KEY_CODE.UP:
                    focusRowChange(0);
                    return true;
                case KEY_CODE.DOWN:
                    focusRowChange(1);
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    callbackFuncSuccess(false);
                    return true;
                default:
                    var number = KTW.utils.util.keyCodeToNumber(keyCode);
                    if(number >= 0) {
                        if (focusRow == 0) {
                            _inputNum(number);
                        }
                        return true;
                    }
                    break;
            }
            return false;
        };

        function _inputNum(num , maxLength) {
            log.printDbg('_inputNum() num : ' + num);
            var authManager = KTW.managers.auth.AuthManager;

            var authMaxLength = 4;
            if(maxLength !== undefined && maxLength !== null) {
                authMaxLength = maxLength;
            }

            if (authManager.inputPW(num, authMaxLength) === true) {
                passwordCnt =  authManager.getPW().length;
                log.printDbg('_inputNum() authManager.inputPW() return true passwordCnt : ' + passwordCnt);
                //passwordCnt =  authManager.getPW().length;
                fillPassword();
                setTimeout(function(){
                    _checkPW();
                }, 10);
            }
            else {
                passwordCnt =  authManager.getPW().length;
                log.printDbg('_inputNum() authManager.inputPW() return false passwordCnt : ' + passwordCnt);
                fillPassword();
            }
        }

        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum() {
            log.printDbg('_deleteNum() ');
            var authManager = KTW.managers.auth.AuthManager;

            authManager.deletePW();
            passwordCnt =  authManager.getPW().length;
            fillPassword();
        }

        /**
         * 입력한 비밀번호 일치여부 확인
         */
        function _checkPW() {
            var authManager = KTW.managers.auth.AuthManager;

            log.printDbg('_checkPW() ');

            var auth_type = (KTW.CONSTANT.IS_OTS === true && Number(KTW.SMARTCARD_ID) === 0) ?
                authManager.AUTH_TYPE.AUTH_ADULT_PIN_FOR_MINIEPG : authManager.AUTH_TYPE.AUTH_ADULT_PIN;
            var obj = {
                type : auth_type,
                callback : callbackFuncAuthResult,
                loading : {
                    type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock : true,
                    callback : null,
                    on_off : false
                }
            };

            authManager.checkUserPW(obj);
        }

        /**
         * AuthManager로 부터 인증 결과 Callback 함수
         */
        function callbackFuncAuthResult(result) {
            var authManager = KTW.managers.auth.AuthManager;

            log.printInfo('callbackFuncAuthResult() result: ' + result);
            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS) {
                if(callbackFuncSuccess !== null) {
                    // 2017.05.03 dhlee 오류 횟수 초기화
                    authManager.resetPWCheckCount();
                    callbackFuncSuccess(true);
                }

            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.CANCEL ||
                result === authManager.RESPONSE.GOTO_INIT) {
                authManager.initPW();

                setTimeout(function(){
                    passwordCnt =  authManager.getPW().length;
                    fillPassword();
                }, 300);

                if(authManager.getPWCheckCount()<3) {
                    if(kidmodeoff === true) {
                        div.find(".text_white_center_27L").html("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
                    }else {
                        div.find(".text_white_center_42M").html("비밀번호가 일치하지 않습니다<br>다시 입력해 주세요");
                    }
                }
            } else if (result === authManager.RESPONSE.DISCONNECTED_NETWORK) {
                KTW.managers.service.SimpleMessageManager.showPasswordCheckNetworkErrorNotiMessage();
            }
        }

        function _startSpeechRecognizer() {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() isStartSSpeechRecognizer : " + isStartSSpeechRecognizer);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                _stopSpeechRecognizer();
            }
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.start(true);
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            isStartSSpeechRecognizer = true;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() isStartSSpeechRecognizer :  " + isStartSSpeechRecognizer);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.stop();
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);

                isStartSSpeechRecognizer = false;
            }
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
            // mode = KTW.oipf.Def.SR_MODE.PIN;
            // type = "recognized";
            // input = "000";
            if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                KTW.managers.auth.AuthManager.initPW();
                passwordCnt =  KTW.managers.auth.AuthManager.getPW().length;
                fillPassword();

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>4) {
                        length = 4;
                    }

                    for(var i=0;i<length;i++) {
                        _inputNum(input[i] , length);
                    }
                }
            }
        }


    };
    KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup.constructor = KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup;

    KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };
    KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();