/**
 * Created by Taesang on 2016-12-30.
 */

(function() {
    KTW.ui.layer.setting.popup.setting_btConnectFail  = function btConnectFailPopup(options) {
        KTW.ui.Layer.call(this, options);

        var div;
        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;

        this.init = function(cbCreate) {
            div = this.div;
            div.addClass("arrange_frame setting gniPopup btOneBtnPopup");
            div.append($("<title/>").text("연결 오류"));
            div.append($("<span/>", {class: "text_white_center_52M"}).text("장치가 응답하지 않습니다"));
            div.append($("<span/>", {class: "text_white_center_38L"}).text("장치 전원 및 장치-셋톱박스 간 거리를 확인해 주세요"));
            div.append($("<div/>", {class: "whiteBtn"}));
            div.find(".whiteBtn").append($("<div/>", {class: "btnLine"}));
            div.find(".whiteBtn").append($("<span/>", {class: "text_black_center_30L"}).text("확인"));
         //   div.find(".whiteBtn .btnLine").css({ position: "absolute", width: 280, height: 62, "border-top": " 5px solid rgb(210,51,47)", "border-bottom" : "5px solid rgb(210,51,47)", "box-sizing": "border-box"});
            cbCreate(true);
        };

        this.show = function() {
            Layer.prototype.show.call(this);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
        };

        this.controlKey = function(keyCode) {
            switch (keyCode) {
                case KEY_CODE.ENTER:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                    LayerManager.historyBack();
                    return true;
            }
        };
    };

    KTW.ui.layer.setting.popup.setting_btConnectFail.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.popup.setting_btConnectFail.constructor = KTW.ui.layer.setting.popup.setting_btConnectFail;

    KTW.ui.layer.setting.popup.setting_btConnectFail.prototype.create = function(cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);

    };

    KTW.ui.layer.setting.popup.setting_btConnectFail.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();