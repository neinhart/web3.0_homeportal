/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 음성가이드 (UHD3)
 */

(function () {
    KTW.ui.layer.setting.setting_voiceGuide  = function VoiceGuide(options) {
        KTW.ui.Layer.call(this, options);

        var AV_OUTPUT_DEVICE = KTW.oipf.Def.AV_OUTPUT.DEVICE;
        var AUDIO_TTS = KTW.oipf.Def.AUDIO_TTS;
        var VOLUM_STEP = 5;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var audioTtsAdapter = KTW.oipf.AdapterHandler.audioTtsAdapter;
        var hwAdapter = KTW.oipf.adapters.HWAdapter;
        var localSystem = oipfObjectFactory.createConfigurationObject().localSystem;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var INSTANCE = this;
        var styleSheet;

        var onoffIndex = 0;
        var volSet = 0;
        var voiceTypeIdx = 0;

        var volumeModule = null;

        var originalVoiceSet;
        var originalVolSet;
        var volumeValue = 5;

        var originalTriggerSet;

        /** TTS 재생 ID.*/
        var speech_id = null;


        var focusIdx = 0;
        var menuType = 0;
        var lastMenuType = 0;

        var clock;
        var clockArea;

        var data;


        //2017 11.3 sw.nam
        // 음성가이드 OFF 수동설정을 지원하지 않는 경우 시간 설정 관련 텝을 보여주지 않는다.
        var IS_AVAILABLE_MANUAL_OFF = false;
        // OFF 수동설정 지원하는 경우 or 그렇지 않은경우 사용되는 버튼 인덱스
        var NUMBER_OF_RADIO_ON_OFF;

        var MANUAL_OFF_RECOGNIZING_WORD = "||starttime=";

        var offStartTimeHH = "00";
        var offStartTimeMM = "00";
        var offEndTimeHH = "00";
        var offEndTimeMM = "00";

        // 0 or 1, 입력받은 시간 값을 처리할 위치.
        var loc = 0;

        // 2018.02.02 sw.nam
        ///호출어 설정 가능 , 불가능 구분하는 flag
        var IS_AVAILABLE_TRIGGER_VOICE = false;
        var NUMBER_OF_RADIO_WHETHER_TRIGGER;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame voiceGuide"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });
            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>음성 가이드</span>" +

                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponentForManualOffSet();

            _initConfiguration();

         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            audioTtsAdapter.addSpeechListener(onSpeechStateChange);

            _readConfiguration();

            // ON/OFF key 선택
            enterKeyEventHandler(onoffIndex, 0);
            // 가이드 볼륨 선택
            INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + volSet + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");

            // 가이드 음성 or 호출어 선택
            enterKeyEventHandler(voiceTypeIdx, 2);

            focusIdx = onoffIndex;
            menuType = 0;
            buttonFocusRefresh(focusIdx, menuType);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_VOICE_GUIDE) {
                                return true;
                            }

                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }

            }


        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            audioTtsAdapter.removeSpeechListener(onSpeechStateChange);
            stopSampleSound(true);
            KTW.ui.LayerManager.hideVBOBackground();
        //    clock.hide();

        };

        this.controlKey = function (key_code) {

            switch(key_code) {
                case KEY_CODE.UP:
                    if (menuType == 0 && onoffIndex === 1) {
                        return true;
                    }
                    if (menuType == 3) {
                        buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2), menuType);
                        return true;
                    } else if (menuType == 2) {
                        menuType = 1;
                        focusIdx = volSet;

                    } else if (menuType == 1) {
                        if(onoffIndex == 2) {
                            menuType = 4;
                            focusIdx = 0;
                        }else {
                            menuType = 0;
                            focusIdx = onoffIndex;
                        }
                    } else if (menuType == 0) {
                        menuType = 2;
                        focusIdx = voiceTypeIdx;
                    } else if (menuType == 4) {
                        menuType = 0;
                        focusIdx = onoffIndex;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.DOWN:
                    if (menuType == 0 && onoffIndex == 1) {
                        return true;
                    }
                    if (menuType == 3) {
                        buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2), menuType);
                        return true;
                    } else if (menuType == 2) {
                        menuType = 0;
                        focusIdx = onoffIndex;
                    } else if (menuType == 1) {
                        menuType = 2;
                        focusIdx = voiceTypeIdx;
                    } else if (menuType == 0) {
                        if(onoffIndex == 2) {
                            menuType = 4;
                            focusIdx = 0;
                        }else {
                            menuType = 1;
                            focusIdx = volSet;
                        }
                    } else if (menuType == 4) {
                        menuType = 1;
                        focusIdx =volSet;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuType == 3) {
                        menuType = lastMenuType;
                        switch(menuType){
                            case 0:
                                focusIdx = onoffIndex;
                                break;
                            case 1:
                                if(volSet == 10) return true;
                                focusIdx = volSet;
                                break;
                            case 2:
                                focusIdx = voiceTypeIdx;
                                break;
                            case 4:
                                focusIdx =  0;
                                break;
                        }
                    } else {
                        if (focusIdx == 0) {
                            resetConfiguration();
                            LayerManager.historyBack();
                            return true;
                        }
                        if (focusIdx > 0) {
                            focusIdx--;
                        }
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.RIGHT:
                    var limitIdx;
                    if(menuType == 3) {
                        // 우측 버튼 영역
                        return true;
                    }

                    if(menuType == 4) {
                        // OFF 시간 설정 영역
                        limitIdx = 3;
                    }else if(menuType == 0 ) {
                        // ON/OFF/수동설정 선택
                        limitIdx = NUMBER_OF_RADIO_ON_OFF -1;
                    }else if(menuType == 1) {
                        // 볼륨설정
                        limitIdx = 5;
                    }
                    else if(menuType == 2 ) {
                        // 호출어 설정 or 가이드 음성 설정
                        limitIdx = NUMBER_OF_RADIO_WHETHER_TRIGGER -1;
                    }
                    // 각 메뉴에 맞는 limitation index 를 넘어가면 우측 버튼 영역으로 포커스
                    if( focusIdx < limitIdx ) {
                        focusIdx++;
                    }else {
                        lastMenuType = menuType;
                        focusIdx = 0;
                        menuType = 3;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.ENTER:
                    lastMenuType = menuType;
                    enterKeyEventHandler(focusIdx, menuType);
                    return true;
                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    var number = KTW.utils.util.keyCodeToNumber(key_code);
                    if(menuType == 4) {
                        keyNumber(number);
                    }
                    return true;

                default:
                    // sw.nam 2017.07.12 WEBIIIHOME-2778
                    // 이전키, 나가기 키 포함 기타 핫키로 메뉴를 벗어나는 경우 기존 볼륨 설정을 유지하도록 한다.
                    resetConfiguration();
                    return false;
            }

        };
        function enterKeyEventHandler(index, _menuType) {
            log.printDbg("enterKeyEventHandler");
            switch(_menuType) {
                case 0:
                    INSTANCE.div.find("#settingTable .table_item_div").removeClass("disable");
                    INSTANCE.div.find("#settingTable .table_time_area").removeClass("focus");

                    onoffIndex = index;
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" +onoffIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");
                    if (onoffIndex === 1) {
                        INSTANCE.div.find("#settingTable .table_item_div:eq(1)").addClass("disable");
                        INSTANCE.div.find("#settingTable .table_item_div:eq(2)").addClass("disable");

                        resetOffTime();

                        focusIdx = 0;
                        menuType = 3;
                    } else if(onoffIndex == 0) {
                        focusIdx = volSet;
                        menuType = 1;
                    } else if(onoffIndex == 2) {
                        // 수동 OFF 설정 선택한 경우
                        // 시간 설정 영역 활성화
                        // 시간 설정 메뉴로 가야함
                        INSTANCE.div.find("#settingTable .table_time_area").addClass("focus");
                        focusIdx = 0;
                        menuType = 4;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    break;
                case 1:
                    // volume set

                    log.printDbg("volumeSet ! ?"+volSet);
                    volSet = index;
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + volSet + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");
                    volSet = volSet < 1 ? 0 : volSet;
                    startSampleSound();
                    focusIdx = voiceTypeIdx;
                    menuType = 2;
                    buttonFocusRefresh(focusIdx, menuType);
                    break;
                case 2:
                    voiceTypeIdx = index;
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + voiceTypeIdx + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");
                    focusIdx = 0;
                    menuType = 3;
                    buttonFocusRefresh(focusIdx, menuType);
                    if(!IS_AVAILABLE_TRIGGER_VOICE) {
                        // 가이드 음성 설정 기능을 제공하는 경우, 볼륨 설정 시 샘플 사운드 테스트 과정에서 선택해 둔 voice 가 바로 호출되므로  음성 설정 라디오 버튼 선택 시 바로바로 setting
                        setDefaultVoiceMode(voiceTypeIdx);
                    }
                    break;
                case 3:
                    if (index === 0) {
                        saveConfiguration();
                        data.complete();
                        settingMenuManager.historyBackAndPopup();
                    } else {
                        // 볼륨 저장을 안하는 경우 진입 전 설정되어있던 값으로 초기화
                        resetConfiguration();
                        LayerManager.historyBack();
                    }
                    break;
                case 4:
                    INSTANCE.div.find("#settingTable .table_time_area").addClass("focus");
                    if(focusIdx == 0 || focusIdx == 1) {
                        focusIdx = 2;
                    } else if(focusIdx == 2 || focusIdx == 3) {
                        focusIdx = 0;
                        menuType = 3;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    break;
            }
        }

        function buttonFocusRefresh(index, _menuType) {
            INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + onoffIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + volSet + ") .table_radio_img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + voiceTypeIdx + ") .table_radio_img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div .timeText").removeClass("focus");
            switch (_menuType) {
                case 0:
                    INSTANCE.div.find("#settingTable .table_item_div").removeClass("disable");
                    INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == onoffIndex ? "rdo_btn_select_f.png" : "rdo_btn_f.png"));
                    if (onoffIndex == 1) {
                        INSTANCE.div.find("#settingTable .table_item_div:eq(1)").addClass("disable");
                        INSTANCE.div.find("#settingTable .table_item_div:eq(2)").addClass("disable");
                    }
                    break;
                case 1:
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == volSet ? "rdo_btn_select_f.png" : "rdo_btn_f.png"));
                    break;
                case 2:
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == voiceTypeIdx ? "rdo_btn_select_f.png" : "rdo_btn_f.png"));
                    break;
                case 3:
                    INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    break;
                case 4:
                    // OFF 수동설정 time set 하는 메뉴
                    log.printDbg("index : "+index);
                    if(index == 0 || index == 1) {
                        INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(0)").addClass("focus");
                        INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(0) .timeText:eq(1)").addClass("focus");
                        if(index == 0) {
                            loc = 0;
                            INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(0) .timeText:eq(0)").addClass("focus");
                        }else {
                            INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(0) .timeText:eq(2)").addClass("focus");
                        }
                    }else if(index == 2 || index == 3) {
                        INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(1)").addClass("focus");
                        INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(1) .timeText:eq(1)").addClass("focus");
                        if(index == 2) {
                            loc = 0;
                            INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(1) .timeText:eq(0)").addClass("focus");
                        }else {
                            INSTANCE.div.find("#settingTable .table_time_area .table_timeInput_div:eq(1) .timeText:eq(2)").addClass("focus");
                        }
                    }
                    // 여기서 시간 설정 함수

                    break;
            }
        }

        /**
         * sw.nam
         * OFF 수동 설정이 가능한 FW 의 경우 사용하는 component 생성 함수이다.
         */
        function createComponentForManualOffSet() {
            log.printDbg("createComponentForManualOffSet");
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div' style = 'position: absolute; width: 1247px; height: 281px; height: 379px; '>" +
            "<div class='table_title'>가이드 음성</div>" +
            "<div class='table_infoText' style='top: 119px; top: 107px;'>대기모드에서 셋톱박스를 향해 설정한 호출어(ex.올레 tv)를 부르면 가이드 음성 기능이 동작합니다</div>" +
            "<div class='table_infoText' id='manualOff' style='top: 147px; opacity: 0.2; font-size: 24px; letter-spacing: -1.2px; line-height: 33px; '>OFF 시간 설정 시 해당 시간에는 가이드 음성 기능이 동작하지 않습니다</div>" +
                "<div id='table_radio_area' style ='top: 174px; top: 192px;'>" +
                    "<div class='table_radio_btn'>" +
                    "<div class='table_radio_btn_bg'></div>" +
                    "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                    "<div class='table_radio_text'>ON</div>" +
                    "</div>" +
                    "<div class='table_radio_btn' >" +
                    "<div class='table_radio_btn_bg'></div>" +
                    "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                    "<div class='table_radio_text'>OFF</div>" +
                    "</div>" +
                    "<div class='table_radio_btn' id='manualOff'>" +
                    "<div class='table_radio_btn_bg' ></div>" +
                    "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                    "<div class='table_radio_text'>OFF 시간 설정</div>" +
                    "</div>" +
                "</div>" +
                "<div class='table_time_area' id='manualOff' style='position: absolute; top:286px; width: 712px; height: 65px;'>"+
                    "<div class= 'table_timeInput_div' style='position: absolute; left: 0px; top: 0px; width: 330px; height:65px;'>"+
                        "<span class='timeText'id='voiceGuide_startHH' style='position: absolute; left: 120px; top: 13px; width: 50px; height: 31px; color: rgb(255,255,255); font-size: 29px; letter-spacing: -1.45px;'>00</span>"+
                        "<span class='timeText'id='voiceGuide_startMiddle' style='position: absolute; left: 155px; top: 13px; width: 50px; height: 31px; color: rgb(255,255,255); font-size: 29px; letter-spacing: -1.45px;'>:</span>"+
                        "<span class='timeText'id='voiceGuide_startMM' style='position: absolute; left: 170px; top: 13px; width: 50px; height: 31px; color: rgb(255,255,255); font-size: 29px; letter-spacing: -1.45px;'>00</span>"+
                    "</div>"+
                    "<span class='timeText' style= 'position: absolute; left: 346px; top: 18px; width: 19px; height: 36px; color: white; opacity: 0.4; font-size: 34px;  letter-spacing: -1.7px;'>~</span>"+
                    "<div class= 'table_timeInput_div' style='position: absolute; left: 382px; top: 0px; width: 330px; height:65px;'>"+
                        "<span class='timeText' id='voiceGuide_endHH' style='position: absolute; left: 120px; top: 13px; width: 50px; height: 31px; color: rgb(255,255,255); font-size: 29px; letter-spacing: -1.45px;'>00</span>"+
                        "<span class='timeText' id='voiceGuide_endMiddle' style='position: absolute; left: 155px; top: 13px; width: 50px; height: 31px; color: rgb(255,255,255); font-size: 29px; letter-spacing: -1.45px;'>:</span>"+
                        "<span class='timeText' id='voiceGuide_endMM' style='position: absolute; left: 170px; top: 13px; width: 50px; height: 31px; color: rgb(255,255,255); font-size: 29px; letter-spacing: -1.45px;'>00</span>"+
                    "</div>"+
                "</div>"+
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute;  top: 385px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute;   top: 387px; width: 1247px; height: 245px;'>" +
            "<div class='table_subTitle'>볼륨 설정</div>" +
            "<div class='table_infoText' style = 'top: 85px; top: 59px; '>셋톱박스의 스피커에서 나오는 음성가이드 볼륨을 조절합니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg' style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>0</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>1</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>2</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>3</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>4</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>5</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute; top: 526px; top: 607px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div variable' style = 'position: absolute;  top: 526px; top: 609px; width: 1247px; height: 224px;'>" +
                "<div class='table_subTitle'>음성설정</div>" +
                "<div class='table_infoText' style = 'top: 85px; top: 59px; '>가이드 음성을 설정할 수 있습니다</div>" +
                "<div id='table_radio_area'>" +
                    "<div class='table_radio_btn'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                        "<div class='table_radio_text'>올레 tv</div>" +
                    "</div>" +
                    "<div class='table_radio_btn'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>기가지니</div>" +
                    "</div>" +
                    "<div class='table_radio_btn triggerOnly'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
                        "<div class='table_radio_text'>지니야</div>" +
                    "</div>" +
                    "<div class='table_radio_btn triggerOnly'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>친구야</div>" +
                    "</div>" +
                    "<div class='table_radio_btn triggerOnly'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>자기야</div>" +
                    "</div>" +
                "</div>" +
            "</div></div>");
            // 우측 버튼 영역
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>셋톱박스 내장 스피커에서<br>나오는 가이드 음성 기능과<br>호출어를 설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        /**
         * OFF 수동 설정이 가능하지 않은 FW 의 경우 사용하는 component 생성 함수.
         * 나중에 또 딴얘기 나올수 있으니까 일단 컴포넌트 구조 만드는 함수는 지우지 않고 보관.....
         */
        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div' style = 'position: absolute; width: 886px; height: 281px;'>" +
            "<div class='table_title'>가이드 음성</div>" +
            "<div class='table_infoText' style='top: 119px';>대기모드에서 셋톱박스를 향해 설정한 호출어(ex.올레 tv)를 부르면 가이드 음성 기능이 동작합니다</div>" +
            "<div id='table_radio_area' style ='top: 174px;'>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>ON</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>OFF</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute; top:281px; width: 886px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute; top: 281px; width: 886px; height: 245px;'>" +
            "<div class='table_subTitle'>볼륨 설정</div>" +
            "<div class='table_infoText' style = 'top: 85px;'>셋톱박스의 스피커에서 나오는 음성가이드 볼륨을 조절합니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg' style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>0</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>1</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>2</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>3</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>4</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 147px;'>" +
            "<div class='table_radio_btn_bg'style='width: 147px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>5</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute; top: 526px; width: 886px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div variable' style = 'position: absolute;  top: 526px; width: 886px; height: 224px;'>" +
            "<div class='table_subTitle'>음성설정</div>" +
            "<div class='table_infoText' style = 'top: 85px;'>가이드 음성을 설정할 수 있습니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>여성</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>어린이</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>음성가이드 기능을<br>설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 화면 최초 로딩 시 결정되는 configuaration 값에 의한 변경사항 관리
         * @private
         */
        function _initConfiguration() {
            log.printDbg("_initConfiguration()");

            //수동 OFF 설정 가능 여부 확인,
            // 가능하지 않은 경우 시간 설정 영역이 hide 된다.
            // 2017.11.3 수동 OFF 설정 가능 여부 확인
            IS_AVAILABLE_MANUAL_OFF = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_AVAILABLE_MANUAL_OFF);

            log.printDbg("IS_AVAILABLE_MANUAL_OFF? " + IS_AVAILABLE_MANUAL_OFF);

            if(IS_AVAILABLE_MANUAL_OFF === "true") {
                INSTANCE.div.find("#settingTable #manualOff").css({visibility: "inherit"});
                NUMBER_OF_RADIO_ON_OFF = 3;
            }else {
                INSTANCE.div.find("#settingTable #manualOff").css({visibility: "hidden"});
                NUMBER_OF_RADIO_ON_OFF = 2;
            }

            /*
             * 2018.02.02. sw.nam
             * 가이드 음성 설정이 없어지고 호출어 설정기능으로 변경 되었다.
             * 먼저 호출어 기능을 제공하는 펌웨어 인지 확인을 해야 한다.
             * 호출어를 제공하는 펌웨어라면 가이드 음성 설정 선택 기능을 없애고, 호출어 설정이 보이도록 메뉴를 구성하고
             * 호출어 기능을 제공하는 펌웨어가 아니라면 가이드 음성 선택 기능을 유지한다.
             * */
            var configuration  =  basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_TRIGGER_VOICE_TYPE);
            if (configuration !== null && configuration !== "undefined" && configuration !== undefined && configuration !== "null") {
                // 사용 가능
                log.printDbg("Trigger mode is available");
                IS_AVAILABLE_TRIGGER_VOICE = true;
                // 사용 가능하면 사용 가능할 때 보여지는 view를 보여준다.
                INSTANCE.div.find("#settingTable .table_title").text("가이드 음성");
                INSTANCE.div.find("#settingTable .table_infoText:eq(0)").text("대기모드에서 셋톱박스를 향해 설정한 호출어(ex.올레 tv)를 부르면 가이드 음성 기능이 동작합니다");
                INSTANCE.div.find("#rightMenu_area #rightMenu_text").html("셋톱박스 내장 스피커에서<br>나오는 가이드 음성 기능과<br>호출어를 설정합니다");
                INSTANCE.div.find("#settingTable .table_infoText:eq(2)").text("셋톱박스의 스피커에서 나오는 가이드 볼륨을 조절합니다");

                INSTANCE.div.find(".variable .table_subTitle").text("호출어 설정");
                INSTANCE.div.find(".variable .table_infoText").text("가이드 호출어를 설정할 수 있습니다");
                INSTANCE.div.find(".variable #table_radio_area .table_radio_btn:eq(0) .table_radio_text").text("올레 tv");
                INSTANCE.div.find(".variable #table_radio_area .table_radio_btn:eq(1) .table_radio_text").text("기가지니");

                INSTANCE.div.find(".variable .table_radio_btn").css({width: 245});
                INSTANCE.div.find(".variable .table_radio_btn .table_radio_btn_bg").css({width: 245});

                INSTANCE.div.find(".variable #table_radio_area .triggerOnly").css({visibility: "inherit"});

                NUMBER_OF_RADIO_WHETHER_TRIGGER = 5;
            }else {
                // 사용 불가
                log.printDbg("Trigger mode couldn't being used");
                IS_AVAILABLE_TRIGGER_VOICE = false;
                INSTANCE.div.find("#settingTable .table_title").text("음성가이드");
                INSTANCE.div.find("#settingTable .table_infoText").text("셋톱박스를 향해 '올레 tv' 라고 부르면 음성가이드 기능이 동작합니다");

                INSTANCE.div.find("#settingTable .table_infoText:eq(1)").text("OFF 수동 설정 시 해당 시간에는 음성가이드 기능이 동작하지 않습니다");
                INSTANCE.div.find("#settingTable .table_item_div:eq(0) .table_radio_btn:eq(2) .table_radio_text").text("OFF 수동 설정");

                INSTANCE.div.find("#rightMenu_area #rightMenu_text").html("음성가이드 기능을<br>설정합니다");
                INSTANCE.div.find(".variable .table_subTitle").text("음성설정");
                INSTANCE.div.find(".variable .table_infoText").text("가이드 음성을 설정할 수 있습니다");
                INSTANCE.div.find(".variable #table_radio_area .table_radio_btn:eq(0) .table_radio_text").text("여성");
                INSTANCE.div.find(".variable #table_radio_area .table_radio_btn:eq(1) .table_radio_text").text("어린이");

                INSTANCE.div.find(".variable .table_radio_btn").css({width: 285});
                INSTANCE.div.find(".variable .table_radio_btn .table_radio_btn_bg").css({width: 285});

                INSTANCE.div.find(".variable #table_radio_area .triggerOnly").css({visibility: "hidden"});

                NUMBER_OF_RADIO_WHETHER_TRIGGER = 2;
            }
        }

        function _readConfiguration() {
            log.printDbg("_readConfiguration()");

            var voiceGuide = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_VOICE_GUIDE);
            var defaultVoiceMode;

            // 가이드 ON / OFF 설정
            if(voiceGuide[0] === "ON") {
                onoffIndex = 0;
            } else if( voiceGuide[0] == "OFF") {
                onoffIndex = 1;
            } else if(voiceGuide[0].indexOf(MANUAL_OFF_RECOGNIZING_WORD) > -1){
                //OFF 수동 설정 set 되어있는 상태
                onoffIndex = 2;
                setOffTime(voiceGuide[0]);

            } else {
                basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE, "ON");
                onoffIndex = 0;
            }

            // 가이드 음성 or 호출어 설정 (호출어 기능 설정이 가능한 경우 음성은 여성으로 고정 된다.

            if(IS_AVAILABLE_TRIGGER_VOICE) {
                audioTtsAdapter.setDefaultVoiceMode(KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE);
                // 호출어 설정
                var configuration  =  basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_TRIGGER_VOICE_TYPE);
                log.printDbg("configurarion is = "+configuration);
                voiceTypeIdx = configuration;

            }else {
                defaultVoiceMode = audioTtsAdapter.getDefaultVoiceMode();

                if(defaultVoiceMode == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE ) {
                    voiceTypeIdx = 0;
                }else if(defaultVoiceMode == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD) {
                    voiceTypeIdx = 1;
                }else {
                    audioTtsAdapter.setDefaultVoiceMode(KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE);
                    defaultVoiceMode = audioTtsAdapter.getDefaultVoiceMode();
                    voiceTypeIdx = 0;
                }
                originalVoiceSet = defaultVoiceMode;
            }


            // 가이드 볼륨 설정
            if( !volumeModule ){
                try {
                    volumeModule = KTW.oipf.AdapterHandler.extensionAdapter.getEmbeddedSpeakerVolumeController();
                    volumeValue = volumeModule.getVolume() * VOLUM_STEP;
                }catch(e){
                    log.printErr(e);
                    log.printErr("Cannot load embedded volume controller!");
                }
            } else {
                log.printDbg("readConfiguration() - volumeModule.getVolume() : "+volumeModule.getVolume());
                volumeValue = volumeModule.getVolume() * VOLUM_STEP;
            }
            volumeValue = Math.floor(volumeValue);
            log.printDbg("readConfiguration() - volumeValue : " + volumeValue);

            volSet = volumeValue;
            originalVolSet = volSet;

        }

        function saveConfiguration() {
            log.printDbg("saveConfiguration()");
            var saveText;
            var defaultVoiceMode;

            //ON / OFF 설정 저장
            switch (onoffIndex) {
                case 0:
                    saveText = "ON";
                    break;
                case 1:
                    saveText = "OFF";
                    break;
                case 2:
                    // [sw.nam] 수동 OFF 설정 저장
                    // “OFF||starttime=--:--||endtime=--:--“
                    if(offStartTimeHH == offEndTimeHH && offStartTimeMM == offEndTimeMM) {
                        resetOffTime();
                        saveText = "OFF";
                    }else {
                        saveText = "OFF||starttime="+offStartTimeHH+":"+offStartTimeMM+"||endtime="+offEndTimeHH+":"+offEndTimeMM;
                    }
                    break;
            }
            log.printDbg("saveConfiguration saveText == "+saveText);
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE, saveText);

            // 가이드 음성 or 호출어 설정 저장
            if(IS_AVAILABLE_TRIGGER_VOICE) {
                setTriggerVoiceType(voiceTypeIdx);
            }else {
                // 가이드 음성 설정
                if(voiceTypeIdx == 0) {
                    defaultVoiceMode = KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE;
                }else {
                    defaultVoiceMode = KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD;
                }
                var result = audioTtsAdapter.setDefaultVoiceMode(defaultVoiceMode);
                log.printDbg("set default voice mode is _+ "+result);
            }

            // 가이드 볼륨 설정 저장
            log.printDbg("keyOk() - volumeValue : " + volSet);
            volumeModule.setVolume(volSet / VOLUM_STEP);
            volumeValue = volSet;
        }

        /**
         * 샘플 사운드를 재생한다.
         */
        function startSampleSound() {
            log.printDbg("called startSampleSound() - speech_id : " + speech_id + ", Volume : " + (volSet / VOLUM_STEP));
            if (speech_id) {//샘플 사운드 재생 중인 경우
                //기존 샘플 사운드 재생 종료
                audioTtsAdapter.stopSpeech(speech_id);
            } else {//샘플 사운드 재생 안되고 있는 경우
                //AV 오디오 OFF
                if (localSystem.mediaAudioEnabled !== undefined) {
                    localSystem.mediaAudioEnabled = false;
                }
                log.printDbg("fjalkfjaskfjasfkj");
                //내장 스피커 ON
                hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.INTERNAL_SPEAKER, true);

                //2017.07.11 sw.nam 수정 -  내장 스피커 output 이 true, false 이냐에 따라 메인 스피커는 자동으로 ON/OFF 되므로
                // 추가적으로 메인스피커의 enable 값을 컨트롤할 필요는 없으므로 아래 코드는 삭제해도 된다.
                // 메인 스피커 enable 값을 컨트롤하게 되면 mute 처리가 되어 실제 뮤트 핫키를 이용해 mute 된 상황과 구별할 수 없음 (WEBIIIHOME-2716)
                // hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.MAIN, false);
            }
            //내장 스피커 ON
            hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.INTERNAL_SPEAKER, true);

            //볼륨 설정
            volumeModule.setVolume(volSet / VOLUM_STEP);
            //TTS 재생
            // sw.nam setVolume 값이 0 인경우 미리듣기 재생 막음
            if((volSet / VOLUM_STEP) !== 0 ) {
                speech_id = audioTtsAdapter.startSpeech("올레티비");
            }

            log.printDbg("startSampleSound() - startSpeech, speech_id : " + speech_id);
            //TTS 재생 실패 시 원복
            if(speech_id === 0) {
                stopSampleSound();
            }
        }

        /**
         * 샘플 사운드 재생을 중지한다.
         */
        function stopSampleSound(forced) {
            log.printDbg("called stopSampleSound() - speech_id : " + speech_id);
            //샘플 사운드 재생 안되고 있는 경우
            if (!speech_id && !forced) {
                log.printDbg("stopSampleSound() - return, invalid speech_id");
                return;
            }
            //TTS 재생 중지
            if (speech_id) {
                audioTtsAdapter.stopSpeech(speech_id);
            }
            speech_id = null;
            //내장 스피커 OFF
            hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.INTERNAL_SPEAKER, false);

            //2017.07.11 sw.nam 수정 -  내장 스피커 output 이 true, false 이냐에 따라 메인 스피커는 자동으로 ON/OFF 되므로
            // 추가적으로 메인스피커의 enable 값을 컨트롤할 필요는 없으므로 아래 코드는 삭제해도 된다.
            // 메인 스피커 enable 값을 컨트롤하게 되면 mute 처리가 되어 실제 뮤트 핫키를 이용해 mute 된 상황과 구별할 수 없음 (WEBIIIHOME-2716)
            //hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.MAIN, true);

            //AV 오디오 ON
            if (localSystem.mediaAudioEnabled !== undefined) {
                localSystem.mediaAudioEnabled = true;
            }
        }
        /**
         * 음성 재생 동작의 상태가 변경되는 경우 발생하는 이벤트 Listener
         *
         * @param id 음성 재생 동작을 나타내는 유니크 아이디
         * @param state 음성 재생 동작 상태
         * @see KTW.oipf.Def.AUDIO_TTS.STATE
         */
        function onSpeechStateChange(id, state) {
            log.printDbg("called onSpeechStateChange() - id : " + id + ", state : " + state);
            if (speech_id === id &&
                (state === AUDIO_TTS.STATE.STATE_ERROR || state === AUDIO_TTS.STATE.STATE_END)) {
                stopSampleSound();
            }
        }

        /**
         * sw.nam
         * 볼륨 및 음성 기존 설정 값으로 리셋한다
         */
        function resetConfiguration() {
            log.printDbg("resetConfiguration()");
            //볼륨 리셋
            volumeModule.setVolume(originalVolSet / VOLUM_STEP);
            volumeValue = originalVolSet;
            //음성 리셋
            audioTtsAdapter.setDefaultVoiceMode(originalVoiceSet);
        }

        function generateTime(hour, current_value, num) {
            // 숫자를 입력하고 나서 loc 값이 0 이면 포커스 이동
            if (num === null || num === undefined) {
                return current_value;
            }

            if (loc === 0) {
                if (hour) {
                    // 시간 영역에서 0,1,2를 제외한 숫자가 입력되면 앞은 0으로 자동 채움.
                    if (num > 2) {
                        current_value = "0" + num;
                    } else {
                        current_value = num + "0";
                        loc++;
                    }
                } else {
                    // 0~5 를 제외한 숫자가 입력될 경우 앞은 0 으로 자동 채움.
                    if (num > 5) {
                        current_value = "0" + num;
                    } else {
                        current_value = num + "0";
                        loc++;
                    }
                }
            }
            else if (loc === 1) {
                if (hour) {
                    if (current_value === "20") {
                        log.printDbg("num === "+num);
                        // 시간 영역에서 2를 먼저 입력했을 경우 0~4 까지만 입력 가능
                        if (num > 4) {
                            log.printDbg("num === "+num);
                            return current_value;
                        } else if (num === 4) {
                            // 24 입력시 00으로 자동 변경.
                            log.printDbg("num === "+num);
                            current_value = "00";
                            loc = 0;
                        } else {
                            current_value = "2" + num;
                            loc = 0;
                        }
                    } else {
                        log.printDbg("num === "+num);
                        current_value = current_value[0] + num;
                        loc = 0;
                    }
                } else {
                    current_value = current_value[0] + num;
                    loc = 0;
                }
            }


            return current_value;
        }

        /**
         * OFF 수동 설정 시간을 set 한다.
         * @param number
         * @private
         */
        function keyNumber(number) {
            log.printDbg("keyNumber : "+number);
            if(menuType !== 4) {
                return ;
            }
            if(number >=0  && 9) {
                if(number >= 0 && number <= 9) {
                    // [jh.lee] 0~9 숫자가 입력 된 경우
                    if (focusIdx === 0 || focusIdx === 1) {
                        if (focusIdx === 0) {
                            offStartTimeHH = generateTime(true, offStartTimeHH, number);
                            if (loc === 0) {
                                focusIdx = 1;
                                buttonFocusRefresh(focusIdx, menuType);
                            }
                            INSTANCE.div.find(".table_timeInput_div #voiceGuide_startHH").text(offStartTimeHH);
                        } else {
                            offStartTimeMM = generateTime(false, offStartTimeMM, number);
                            if (loc === 0) {
                                focusIdx = 2;
                                buttonFocusRefresh(focusIdx, menuType);
                            }
                            INSTANCE.div.find(".table_timeInput_div #voiceGuide_startMM").text(offStartTimeMM);
                        }
                    } else if(focusIdx === 2 || focusIdx === 3) {
                        if (focusIdx == 2) {
                            offEndTimeHH = generateTime(true, offEndTimeHH, number);
                            if (loc === 0) {
                                focusIdx = 3;
                                buttonFocusRefresh(focusIdx, menuType);
                            }
                            INSTANCE.div.find(".table_timeInput_div #voiceGuide_endHH").text(offEndTimeHH);
                        } else {
                            offEndTimeMM = generateTime(false, offEndTimeMM, number);
                            if (loc === 0) {
                                focusIdx = 0;
                                menuType = 3;
                                buttonFocusRefresh(focusIdx, menuType);
                            }
                            INSTANCE.div.find(".table_timeInput_div #voiceGuide_endMM").text(offEndTimeMM);
                        }
                    }
                }
            }

        }


        function setOffTime(time) {
            log.printDbg("setOffTime()");

            offStartTimeHH = time.substring(15,17);
            offStartTimeMM = time.substring(18,20);
            offEndTimeHH =   time.substring(30,32);
            offEndTimeMM =   time.substring(33,35);

            INSTANCE.div.find(".table_timeInput_div #voiceGuide_startHH").text(offStartTimeHH);
            INSTANCE.div.find(".table_timeInput_div #voiceGuide_startMM").text(offStartTimeMM);
            INSTANCE.div.find(".table_timeInput_div #voiceGuide_endHH").text(offEndTimeHH);
            INSTANCE.div.find(".table_timeInput_div #voiceGuide_endMM").text(offEndTimeMM);
        }
        /**
         * 셋팅된 시간 값을 reset 시킨다.
         */
        function resetOffTime() {
            log.printDbg("resetOffTime()");

            offStartTimeHH = "00";
            offStartTimeMM = "00";
            offEndTimeHH = "00";
            offEndTimeMM = "00";

            INSTANCE.div.find(".table_timeInput_div #voiceGuide_startHH").text(offStartTimeHH);
            INSTANCE.div.find(".table_timeInput_div #voiceGuide_startMM").text(offStartTimeMM);
            INSTANCE.div.find(".table_timeInput_div #voiceGuide_endHH").text(offEndTimeHH);
            INSTANCE.div.find(".table_timeInput_div #voiceGuide_endMM").text(offEndTimeMM);
        }

        function setDefaultVoiceMode() {
            log.printDbg("setDefaultVoiceMode()");
            switch(voiceTypeIdx) {
                case 0:
                    audioTtsAdapter.setDefaultVoiceMode(KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE);
                    break;
                case 1:
                    audioTtsAdapter.setDefaultVoiceMode(KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD);
                    break;
            }
        }

        function setTriggerVoiceType(idx) {
            log.printDbg("setTriggerVoiceType() "+idx);
            if(idx === 0) {
                log.printDbg("convert idx" +idx);
                idx = "0";
            }
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_TRIGGER_VOICE_TYPE,idx);
        }
    };

    KTW.ui.layer.setting.setting_voiceGuide.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_voiceGuide.constructor = KTW.ui.layer.setting.setting_voiceGuide;

    KTW.ui.layer.setting.setting_voiceGuide.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_voiceGuide.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();