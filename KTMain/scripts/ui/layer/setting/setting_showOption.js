/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 채널/VOD 설정 > VOD 리스트 보기 방식
 */

(function () {

    KTW.ui.layer.setting.setting_showOption  = function ShowOption(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;

        var log = KTW.utils.Log;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = null;
        var SettingDataAdaptor = KTW.ui.adaptor.SettingDataAdaptor;
        var StorageManager = KTW.managers.StorageManager;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var clock;
        var clockArea;

        var data;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame showOption"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>VOD 리스트 보기 방식</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            readConfiguration();

            data = this.getParams();
            log.printDbg("thisMenuName:::::::::::"+data.name);
            if(!options || !options.resume) {
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_POSTER_SHAPE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, (menuIdx == 0 ? 3 : 2)), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, (menuIdx == 0 ? 3 : 2)), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        if(menuIdx == 0 && focusIdx == 2) {
                            focusIdx--;
                            buttonFocusRefresh(focusIdx,menuIdx);
                        }else {
                            LayerManager.historyBack();
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        if(focusIdx == 1) {
                            focusIdx = 2;
                            buttonFocusRefresh(focusIdx, 0);
                        }else {
                            focusIdx = 0;
                            buttonFocusRefresh(focusIdx, 1);
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            var key;
            switch(menuIndex) {
                case 0:
                    setSelectedButton(index);
                    selectedIdx = index;
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장
                        switch(selectedIdx) {
                            case 0 :
                                key = "default";
                                break;
                            case 1:
                                key = "poster";
                                break;
                            case 2:
                                key = "list";
                                break;
                        }

                        saveConfiguration(key);
                        //팝업
                        data.complete();
                        settingMenuManager.historyBackAndPopup();

                        break;
                    }
                    LayerManager.historyBack();
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
                "<div class='table_title'>VOD 리스트 보기 방식</div>" +
                "<div class='table_subTitle'>원하는 VOD 리스트 보기 옵션을 설정해 주세요</div>" +
                "<div id='table_radio_area'>" +
                    "<div class='table_radio_btn' style='top:20px;'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                        "<div class='table_radio_text'>기본 설정</div>" +
                    "</div>" +
                    "<div class='table_radio_btn' style='top: 117px;'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>포스터로 보기</div>" +
                    "</div>" +
                    "<div class='table_radio_btn' style='top:117px; left: 600px;'>" +
                        "<div class='table_radio_btn_bg'></div>" +
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>텍스트로 보기</div>" +
                    "</div>" +
                "</div>" +
                "<img class='posterImg' src='images/setting/13_set_ex02.png' style='position: absolute; left: -3px; top: 415px; width: 548px; height: 312px;'>"+
                "<img class='posterImg' src='images/setting/13_set_ex03.png' style='position: absolute; left: 597px; top: 415px; width: 548px; height: 312px;'>"+
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>기본 설정은 VOD 메뉴 장르<br>특성에 따라 포스터와<br>텍스트 보기 방식이 혼용될<br>수 있습니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var showOption = SettingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_POSTER_SHAPE);

            switch(showOption[0]) {
                case "default":
                    focusIdx = 0;
                    break;
                case "poster" :
                    focusIdx = 1;
                    break;
                case "list" :
                    focusIdx = 2;
                    break;
                default :
                    StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, "default");
                    focusIdx = 0;
                    break;
            }
            selectedIdx = focusIdx;
            buttonFocusRefresh(focusIdx, 0);
            setSelectedButton(focusIdx);
        }

        function saveConfiguration(key) {
            log.printDbg("saveConfiguration()++++"+key);
            StorageManager.ps.save(StorageManager.KEY.VOD_LIST_MODE, key);
        }

        function setSelectedButton(index) {

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }

    };

    KTW.ui.layer.setting.setting_showOption.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_showOption.constructor = KTW.ui.layer.setting.setting_showOption;

    KTW.ui.layer.setting.setting_showOption.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_showOption.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();