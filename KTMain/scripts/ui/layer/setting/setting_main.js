/**
 * Created by Yun on 2016-11-15.
 * 설정 > 메인 설정 메뉴 (세부 메뉴 관리 Layer - 자녀안심설정, 채널/VOD 설정, 시스템 설정
 */

(function() {
    KTW.ui.layer.setting.setting_main  = function SettingMain(options) {
        KTW.ui.Layer.call(this, options);
        var INSTANCE = this;
        var START_MENU_INDEX = 0;
        var styleSheet;
        var focusIdx = 0;
        var curComp = 0;
        var compList = [];
        var hiddenAdminPwd = "";
        var isStarStartWord = false;

        var Layer = KTW.ui.Layer;
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var language;
        var LANG = {
            KOR : "kor",
            ENG : "ENG"
        };

        var clockArea;
        var clock;

        var settingMenu = null;
        var menuList = [];

        var layerData;
        var DIRECTION = {
            NONE : "NONE",
            UP   : "UP",
            DOWN : "DOWN"
        };

        var listener;
        var stbCtrl = KTW.oipf.AdapterHandler.extensionAdapter.getNavStbCtrl();

        // TODO [sw.nam] hidden 비밀번호 재 확인 필요
        var OTS_ADMIN_PASSWORD = "***2485";
        var UHD_BEACON_PASSWORD = "***2580";
        var HDS_PASSWORD = "1473591232580";
        var OC_PASSWORD = "***7777";

        this.init = function() {
            this.div.attr({class: "arrange_frame setting_layer settingMenu"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_2d_1' style='position: absolute; left:0px; top: 0px; width: 1661px; height: 1080px;' src='images/bg_2d_1.png'>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<img id='settingMainBackIcon' src ='images/ar_back.png' style='position: absolute; left: 53px; top: 522px; width: 22px; height: 36px;'>"+
                "<span id='backgroundTitle'>설정</span></div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

          //  createClock();

        };

        this.showView = function(options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show()");
            hiddenAdminPwd = "";
            isStarStartWord = false;
            layerData =  this.getParams();
            if (!settingMenu) {
                settingMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.catType === KTW.DATA.MENU.CAT.CONFIG) {
                            return true;
                        }
                    }
                })[0];
            }
            language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
            var name;
            for (var i = 0; i < settingMenu.children.length; i++) {
                if (language === LANG.KOR) {
                    name = settingMenu.children[i].name;
                } else {
                    name = settingMenu.children[i].englishItemName;
                }
                menuList[i] = {title: name, index: i, id: settingMenu.children[i].id };
            }

            if (!options || !options.resume) {

                createComponent();
                if(KTW.CONSTANT.STB_TYPE.ANDROID) {
                    // 안드로이드 셋탑(기가지니)인 경우 focus line 의 transition 속성을 0s 로 변경
                    INSTANCE.div.find("#menuFocus_line").css({"-webkit-transition" : "0s"});
                }
                setMainMenuUnFocus();
                for(var i = 0; i < compList.length; i++ ) {
                    compList[i].unFocus();
                }
                focusIdx = 0;
                if(layerData) {
                    switch(layerData.menuId) {
                        case KTW.managers.data.MenuDataManager.MENU_ID.KIDS_CARE_SETTING:
                            focusIdx =0;
                            break;
                        case KTW.managers.data.MenuDataManager.MENU_ID.CHANNEL_SETTING:
                            focusIdx =1;
                            break;
                        case KTW.managers.data.MenuDataManager.MENU_ID.SYSTEM_SETTING:
                            focusIdx =2;
                            break;
                    }
                }
            } else {
                //resume.
                INSTANCE.div.find("#backgroundTitle").text(menuList[focusIdx].title);
/*                var name;
                if(language === LANG.KOR) {
                    name = settingMenu.name;
                } else {
                    name = settingMenu.englishItemName;
                }
                INSTANCE.div.find("#backgroundTitle").text(name);*/
            }
           // menuFocusing(focusIdx);
            menuFocusingForMovable(focusIdx);
            changeSubMenu(focusIdx);
            if(layerData) {
                setMainMenuFocus();
                compList[focusIdx].focus();
            }
            // 2017.07.04 sw.nam 해상도 변경 감지르 위한 listener 추가.
            listener = { "notifyScreenChanged" : function(resolution) {
                checkSetResolution();
            } };
            try {
                stbCtrl.addScreenResolutionChangeListener(listener);
            } catch(e) {
                log.printErr("failed addScreenResolutionChangeListener(");
                log.printExec(e);
            }


            // 2017.07.14 sw.nam
            // UHD3 셋탑의 경우 subTitle 에서 연결된 블루투스 장치의 갯수를 표현하므로 Listener 등록 필요
            if(KTW.CONSTANT.UHD3.BEACON && !KTW.CONSTANT.STB_TYPE.ANDROID) {
                KTW.oipf.AdapterHandler.hwAdapter.addEventListener(KTW.oipf.Def.CONFIG.EVENT.DEVICE, checkConnectedBluetoothInfo);

                //설정 상세 메뉴에 진입되어 있는 상태에서 블루투스 장치 연결 상태가 변경되는 경우 subTitle update 를 위한 check 로직 추가.
                checkConnectedBluetoothInfo();
            }
            //2017.07.07 sw.nam
            //해상도 설정 메뉴를 포함한 모든 설정 상세 메뉴에 진입되어 있는 상태에서 해상도 시스템 초기화 되는 경우 이전 키 혹은 저장 키를 눌러 setting mainlayer 로 빠져 나올 때
            //subTitle 이 update 되지 않아 해상도가 변경되었음에도 불구하고 화면상의 오류가 생기기 떄문에 setting main layer 가 show 될 때마다 해상도를 check 해 주어야 함.
            checkSetResolution();
        };

        this.hideView = function(options) {
            log.printDbg("hideView() options : " + (options !== undefined ? JSON.stringify(options) : ""));
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

            //[sw.nam] 이전 layer 혹은 AV로 화면을 빠져나가는 경우에만 VBO background() 실행
/*            if(!options){
                log.printDbg("hideVBOBg");
                KTW.ui.LayerManager.hideVBOBackground();
            }*/
          //  clock.hide();

/*            INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");
            INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list #menuFocus_text").removeClass();
            INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list #menuFocus_text").eq(1).addClass("bottom");

            INSTANCE.div.find("#settingMenu_area #menuUnFocus_area .menuUnFocus_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");
            INSTANCE.div.find("#settingMenu_area #menuUnFocus_area .menuUnFocus_list #menuUnFocus_text").removeClass();
            INSTANCE.div.find("#settingMenu_area #menuUnFocus_area .menuUnFocus_list #menuUnFocus_text").eq(0).addClass("top");
            INSTANCE.div.find("#settingMenu_area #menuUnFocus_area .menuUnFocus_list #menuUnFocus_text").eq(1).addClass("middle");
            INSTANCE.div.find("#settingMenu_area #menuUnFocus_area .menuUnFocus_list #menuUnFocus_text").eq(2).remove();*/

            INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text").removeClass("under focus");
            INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text").removeClass("focus");

            try {
                stbCtrl.addScreenResolutionChangeListener(null);
            } catch(e) {
                log.printErr("failed addScreenResolutionChangeListener(");
                log.printExec(e);
            }
            if(KTW.CONSTANT.UHD3.BEACON && !KTW.CONSTANT.STB_TYPE.ANDROID) {
                KTW.oipf.AdapterHandler.hwAdapter.removeEventListener(KTW.oipf.Def.CONFIG.EVENT.DEVICE, checkConnectedBluetoothInfo);
            }
        };

        this.controlKey = function(key_code) {
            var KEY_CODE = KTW.KEY_CODE;
            if (compList[focusIdx].getIsFocus()) {
                var keyAction = compList[focusIdx].onKeyAction(key_code);
                if (keyAction.prevMenu) {
                    if(layerData) {
                        historyBack();
                    }else {
                        setMainMenuUnFocus();
                        compList[focusIdx].unFocus();
                    }
                }
                return keyAction.isUsed;
            } else {
                if (key_code === KEY_CODE.BACK) {

                    historyBack();
                    return true;
                }
                switch (key_code) {
                    case KEY_CODE.UP:
                        //2017.10.31 sw.nam
                        //메뉴 리스트 이동포커스 적용
                        menuFocusingForMovable(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 3));
                        changeSubMenu(focusIdx);
                        return true;
                    case KEY_CODE.DOWN:
                        //2017.10.31 sw.nam
                        //메뉴 리스트 이동포커스 적용
                        menuFocusingForMovable(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 3));
                        changeSubMenu(focusIdx);
                        return true;
                    case KEY_CODE.LEFT:
                        // 2017.03.09 dhlee
                        // 메뉴 리스트에 focus가 있는 상태에서 Left 키를 누르면 이전화면으로 돌아가야 한다.(historyBack)
                        historyBack();
                        return true;
                    case KEY_CODE.RED:
                        //2017.11.06 sw.nam
                        //red 키 동작 추가, 설정 메뉴는 3개 뿐이므로 부가적인 예외처리는 추가하지 않았음
                        if(focusIdx === 0) {
                            return true;
                        }else {
                            focusIdx = 0;
                            menuFocusingForMovable(focusIdx);
                            changeSubMenu(focusIdx);
                        }
                        return true;
                    case KEY_CODE.BLUE:
                        //2017.11.06 sw.nam
                        //blue 키 동작 추가, 설정 메뉴는 3개 뿐이므로 부가적인 예외처리는 추가하지 않았음
                        if(focusIdx === compList.length-1) {
                            return true;
                        }else {
                            focusIdx = compList.length-1;
                            menuFocusingForMovable(focusIdx);
                            changeSubMenu(focusIdx);
                        }
                        return true;
                    case KEY_CODE.RIGHT:
                    case KEY_CODE.ENTER:
                        if (focusIdx == 2 && hiddenAdminPwd.length>0)  {
                            if (hiddenAdminPwd === HDS_PASSWORD) {
                                showHDSAdminCheckPopup();
                                hiddenAdminPwd = "";
                                return true;
                            }
                        }
                        setMainMenuFocus();
                        compList[focusIdx].focus();
                        KTW.managers.UserLogManager.collect({
                            type: KTW.data.UserLog.TYPE.ETC_MENU,
                            act: key_code,
                            menuId: menuList[focusIdx].id,
                            menuName: menuList[focusIdx].title
                        });
                        INSTANCE.div.find("#settingMenu .backIcon").css({visibility: "hidden"});
                        return true;
                    case KEY_CODE.NUM_0:
                    case KEY_CODE.NUM_1:
                    case KEY_CODE.NUM_2:
                    case KEY_CODE.NUM_3:
                    case KEY_CODE.NUM_4:
                    case KEY_CODE.NUM_5:
                    case KEY_CODE.NUM_6:
                    case KEY_CODE.NUM_7:
                    case KEY_CODE.NUM_8:
                    case KEY_CODE.NUM_9:
                        /**
                         * 시스템 설정 focus Index : 2
                         */
                        if (focusIdx != 2)  {
                            return false;
                        }
                        var tmpKey = key_code-48;
                        hiddenAdminPwd += tmpKey;
                        if(isStarStartWord === true) {
                            if(KTW.CONSTANT.IS_OTS) {
                                if (hiddenAdminPwd === OTS_ADMIN_PASSWORD) {
                                    compList[focusIdx].menuListChange();
                                    setMainMenuFocus();
                                    compList[focusIdx].focus();
                                    hiddenAdminPwd = "";
                                }
                            }
                            //TODO [sw.nam] UHD3 박스에서만 동작하도록 조건문 걸어줘야 함 TBD
                            if(KTW.CONSTANT.UHD3.BEACON) {
                                log.printDbg("UHD_BEACON_PASSWORD");
                                if(hiddenAdminPwd === UHD_BEACON_PASSWORD){
                                    log.printDbg("UHD_BEACON_PASSWORD_____1");
                                    compList[focusIdx].menuListChange();
                                    setMainMenuFocus();
                                    compList[focusIdx].focus();
                                    hiddenAdminPwd = "";
                                }
                            }
                        }
                        log.printDbg("hiddenMenuPwd == " +hiddenAdminPwd);
                        return true;
                    case KEY_CODE.STAR:
                        /**
                         * 시스템 설정 focus Index : 2
                         */
                        if (focusIdx != 2) {
                            return false;
                        }
                        if(hiddenAdminPwd.length === 0) {
                            isStarStartWord = true;
                        }
                        if(hiddenAdminPwd.length > 7) {
                            hiddenAdminPwd = "";
                        }
                        hiddenAdminPwd += "*";

                        log.printDbg("hiddenMenuPwd == " +hiddenAdminPwd);
                        return true;
                    default:
                        return false;
                }
            }
        };

        this.setSubTitle = function(category, index, subtitle) {
            switch (category) {
                case "System":
                    compList[2].setSubTitle(index, subtitle);
                    break;
                case "Kids":
                    compList[0].setSubTitle(index, subtitle);
                    break;
                case "ChannelVod":
                    compList[1].setSubTitle(index, subtitle);
                    break;
                default:
                    break;
            }
        };

        // 2017.08.11 sw.nam
        // layer activate toggling 관련 로직 추가
        this.getCurrentFocusedMenu = function() {
            var menuId = undefined;
            if(INSTANCE.div.find("#settingMenu_area").hasClass("focus")) {
                switch(focusIdx) {
                    case 0:
                        menuId = KTW.managers.data.MenuDataManager.MENU_ID.KIDS_CARE_SETTING;
                        break;
                    case 1:
                        menuId = KTW.managers.data.MenuDataManager.MENU_ID.CHANNEL_SETTING;
                        break;
                    case 2:
                        menuId = KTW.managers.data.MenuDataManager.MENU_ID.SYSTEM_SETTING;
                        break;
                }
            }
            return menuId;
        };

        function showHDSAdminCheckPopup() {
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_HDS_ADMIN_CHECK_PASSWORD_POPUP,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    linkage: false,
                    params: {
                        callback: _callbackFuncHDSAdminCheck
                    }
                },
                new: true,
                visible: true
            });
        }

        function _callbackFuncHDSAdminCheck(result) {
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_HDS_ADMIN_CHECK_PASSWORD_POPUP
            });
            if(result !== undefined && result !== null && result === true) {
                var app;
                var url = "file:///altibrowser/nav_webapp/apps/hds_app/hds_ui.html?start=home";
                var prop = {"useType": "host", "name": "hds" , "initWindow.screenWidth":KTW.CONSTANT.RESOLUTION.WIDTH , "initWindow.screenHeight":KTW.CONSTANT.RESOLUTION.HEIGHT };

                log.printInfo("Show HDSOpeningPage");
                app = KTW.oipf.AdapterHandler.serviceAdapter.self;
                app.ref = app.createApplication(url, true, prop);
                // [jh.lee] 개통화면 진입 전 종료해야할 것들을 전부 처리
                KTW.main.destroy();
            }
        }

        function setMainMenuUnFocus() {
            log.printDbg("setMainMenuUnFocus()");
            var name;
            if(language === LANG.KOR) {
                name = settingMenu.name;
            } else {
                name = settingMenu.englishItemName;
            }
            INSTANCE.div.find("#backgroundTitle").text(name);
            INSTANCE.div.find("#settingMenu_area").removeClass("focus");
            INSTANCE.div.find("#menuBox_area").removeClass("focus");
            INSTANCE.div.find("#settingMenu_area #focusArrow_img").attr({ src: "images/arw_menu_focus.png"});
            INSTANCE.div.find("#settingMainBackIcon").css({visibility: "inherit"});
            for(var i=0;i<compList.length;i++) {
                compList[i].menuTypeReset();
            }


        }

        function setMainMenuFocus() {
            log.printDbg("setMainMenuFocus()");
            INSTANCE.div.find("#backgroundTitle").text(menuList[focusIdx].title);
            INSTANCE.div.find("#settingMenu_area").addClass("focus");
            INSTANCE.div.find("#menuBox_area").addClass("focus");
            INSTANCE.div.find("#settingMenu_area #focusArrow_img").attr({ src: "images/arw_menu_dim.png"});
            INSTANCE.div.find("#settingMainBackIcon").css({visibility: "hidden"});
        }

        function changeSubMenu(index) {
            log.printDbg("changeSubMenu()");
            for (var idx = 0; idx < compList.length; idx++) {
                compList[idx].hide();
                //compList[idx].menuTypeReset();
            }
            compList[index].show(settingMenu.children[index]);
            hiddenAdminPwd = "";
        }

        /**
         * 메뉴 리스트에서 Left 키를 눌렀을 때 이전 화면으로 돌아가도록 한다.
         */
        function historyBack() {
            log.printDbg("historyBack()");

            setMainMenuUnFocus();
            compList[focusIdx].unFocus();
            focusIdx = 0;
           // menuFocusing(focusIdx);
            menuFocusingForMovable(focusIdx);
            changeSubMenu(focusIdx);
            KTW.ui.LayerManager.historyBack();
        }


        var menuFocusing = function(index,direction) {

            if(direction) {
                switch(direction) {
                    case "UP":
/*                        //top
                        INSTANCE.div.find("#menuTop_area #menuUnFocus_text.top").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
                        INSTANCE.div.find("#menuTop_area #menuUnFocus_text.bottom").text(menuList[index].title);*/
                        //focus
                        INSTANCE.div.find("#menuFocus_area #menuFocus_text.top").text(menuList[index].title);
                        INSTANCE.div.find("#menuFocus_area #menuFocus_text.bottom").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);
/*
                        //bottom
                        INSTANCE.div.find("#menuBottom_area #menuUnFocus_text.top").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);
                        INSTANCE.div.find("#menuBottom_area #menuUnFocus_text.bottom").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
*/
                        //unFocus
                        INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.top").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);
                        INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.middle").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
                        INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.bottom").text(menuList[index].title);

                        break;
                    case "DOWN":
/*                        //top
                        INSTANCE.div.find("#menuTop_area #menuUnFocus_text.top").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);
                        INSTANCE.div.find("#menuTop_area #menuUnFocus_text.bottom").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);*/
                        //focus
                        INSTANCE.div.find("#menuFocus_area #menuFocus_text.top").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
                        INSTANCE.div.find("#menuFocus_area #menuFocus_text.bottom").text(menuList[index].title);
/*                        //bottom
                        INSTANCE.div.find("#menuBottom_area #menuUnFocus_text.top").text(menuList[index].title);
                        INSTANCE.div.find("#menuBottom_area #menuUnFocus_text.bottom").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);*/

                        //unFocus
                        INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.top").text(menuList[index].title);
                        INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.middle").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);
                        INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.bottom").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
                        break;
                    default:
                        INSTANCE.div.find("#menuTop_area #menuUnFocus_text").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
                        break;
                }
            }else {
                if (index == focusIdx) {
                    INSTANCE.div.find("#menuFocus_area #menuFocus_text").text(menuList[index].title);
                }
/*                INSTANCE.div.find("#menuTop_area #menuUnFocus_text").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
                INSTANCE.div.find("#menuBottom_area  #menuUnFocus_text").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);*/

                INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.top").text(index + 1 > 2 ? menuList[0].title : menuList[index + 1].title);
                INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text.middle").text(index - 1 < 0 ? menuList[2].title : menuList[index - 1].title);
            }
            // width 를 flexible 하게 조절
            setFocusAreaElementsPosition(index);
        };

        /**
         * 메뉴 리스트 포커싱을 이동식으로 처리하는 함수
         * @param index 이동할 index
         * @param direction 이동 방향
         */
        function menuFocusingForMovable(index) {
            log.printDbg("menuFocusingForMovable()");

            INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text").removeClass("under");
            INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text").removeClass("focus");

            INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text:eq("+index+")").addClass("focus");
            for(var i = 0; i < menuList.length; i++) {
                INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text:eq("+i+")").text(menuList[i].title);
            }
            INSTANCE.div.find("#menuFocus_area #menuFocus_text").text(menuList[index].title);

/*            switch(index) {
             case 0:
             INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text:eq(1)").addClass("under");
             INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text:eq(2)").addClass("under");
             break;
             case 1:
             INSTANCE.div.find("#menuUnFocus_area #menuUnFocus_text:eq(2)").addClass("under");
             break;
             case 2:
             break;
             }*/

          //  INSTANCE.div.find("#menuShadowArea").css({"-webkit-transform": "translateY(+"+(83*index)+"px)"});
            INSTANCE.div.find("#menuFocus_area").css({"-webkit-transform": "translateY(+"+(94*index)+"px)"});
            INSTANCE.div.find("#menuFocus_subArea").css({"-webkit-transform": "translateY(+"+(94*index)+"px)"});

           // setFocusAreaElementsPosition(index);
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append(

                "<div id='settingMenu_area'>"+

/*                "<div id='menuShadowArea' style='position: absolute; width: 950px; top: -105px;'>"+
                "<img src='images/sdw_menu_focus_l.png'>"+
                "<img id='shadow' style='width:190px; height: 317px;' src='images/sdw_menu_focus_b.png'>"+
                "<img src='images/sdw_menu_focus_r.png'>"+
                "</div>"+*/
                "<div id='menuFocus_area'>"+
                "<div class='menuFocus_list' style='position: absolute; width: 800px; height: 160px;'>"+
                "<span id='menuFocus_text'>"+ menuList[START_MENU_INDEX].title +"</span>" +
               // "<span id='menuFocus_text' class='bottom'>"+ menuList[START_MENU_INDEX].title +"</span>" +
                "</div>"+
                "</div>" +
                "<div id='menuFocus_subArea'>"+
                "<div id='menuFocus_line' style=''> </div>" +
                "<img id='focusArrow_img' src='images/arw_menu_focus.png'>" +
                "<div id='menuFocus_line' style='margin-top: 72px;'> </div>" +
                "</div>"+
                 // 2017.09.05 sw.nam 변경 gui 적용
                "<div id='menuUnFocus_area' style='position: absolute; left: 133px;  top: 25px; width: 800px; height: 190px; '>"+
                    "<div class= 'menuUnFocus_list' style='position: absolute;  width: 800px; height: 320px;'>"+
                        // 2017.10.31 sw.nam 이동포커스 gui 적용
                    /*
                     "<span id='menuUnFocus_text' class='top'>"+menuList[START_MENU_INDEX+1].title+"</span>"+
                     "<span id='menuUnFocus_text' class='middle'>"+menuList[START_MENU_INDEX].title+"</span>"+
                     */
                        "<span id='menuUnFocus_text' style = 'top: 0px;'>"+menuList[START_MENU_INDEX].title+"</span>"+
                        "<span id='menuUnFocus_text' style = 'top: 94px;'>"+menuList[START_MENU_INDEX+1].title +"</span>"+
                        "<span id='menuUnFocus_text' style = 'top: 188px;'>"+menuList[START_MENU_INDEX+2].title+"</span>"+

                    "</div>"+
                "</div>"+

                "</div>");

            // 메뉴데이터를 같이 넘겨줘야 함.
            //({parent: _this, superParent: parent, menuId: menuId});
            compList = [];
            addComponent(new SubMenu_Kids({instance : INSTANCE, menuData: settingMenu.children[0]}));
            addComponent(new SubMenu_ChannelVod({instance : INSTANCE, menuData: settingMenu.children[1]}));
            compList[1].hide();
            addComponent(new SubMenu_System({instance : INSTANCE, menuData: settingMenu.children[2]}));
            compList[2].hide();

            INSTANCE.div.find("#contents").append(
                "<img id='settingMenu_sdw' src='images/dim_vodlist_btm.png' style='position: absolute; left:576px; top: 764px; width:1344px; height: 168px; z-index: 1;'>"
            );
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function addComponent(component) {
            INSTANCE.div.find("#contents").append(component.getView());
            compList.push(component);
        }

        /**
         * focus 영역의 element width 를 flexible 하게 조절한다.
         * @param index
         */
        function setFocusAreaElementsPosition(index) {

        //    var width = INSTANCE.div.find("#menuFocus_text").eq(1).width();
            var width = INSTANCE.div.find("#menuFocus_text").width();
            if(width < 310) {
                width = 310;
                INSTANCE.div.find("#settingMenu_area #focusArrow_img").css({ "margin-left":width+ 47});
                INSTANCE.div.find("#menuFocus_line").width(width+ 70);
          //      INSTANCE.div.find("#menuShadowArea #shadow").width(width -90);
            }else {
                INSTANCE.div.find("#settingMenu_area #focusArrow_img").css({ "margin-left":width+ 25});
                INSTANCE.div.find("#menuFocus_line").width(width+ 48);
        //        INSTANCE.div.find("#menuShadowArea #shadow").width(width -112);
            }
        }

        function doMenuSlideAnimation(direction) {
            log.printDbg("dbMenuSlideAnimation()_"+ direction);
         //   var topArea = INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list");
            var focusArea = INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list");
         //   var bottomArea = INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list");
            var unFocusListArea = INSTANCE.div.find("#settingMenu_area #menuUnFocus_area .menuUnFocus_list");

            switch (direction) {

                case "UP" :
                    // topListArea
/*

                    INSTANCE.div.find("#settingMenu_area #menuTop_area #menuUnFocus_text").eq(0).remove();

                    INSTANCE.div.find("#settingMenu_area #menuTop_area #menuUnFocus_text").eq(0).removeClass("top").addClass("bottom");
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");

                    if (!(INSTANCE.div.find("#settingMenu_area #menuTop_area  #menuUnFocus_text").hasClass("top"))) {
                        INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list").append("<span id='menuUnFocus_text' class='top'></span>");
                    }

                    topArea[0].offsetWidth = topArea[0].offsetWidth;
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list").addClass("settingList_down");
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list .top").addClass("settingList_add");
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list .bottom").addClass("settingList_delete");
*/

                    //focusArea
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area #menuFocus_text").eq(0).remove();

                    INSTANCE.div.find("#settingMenu_area #menuFocus_area  #menuFocus_text").eq(0).removeClass("top").addClass("bottom");
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");

                    if (!(INSTANCE.div.find("#settingMenu_area #menuFocus_area  #menuFocus_text").hasClass("top"))) {
                        INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").append("<span id='menuFocus_text' class='top'></span>");
                    }
                    focusArea[0].offsetWidth = focusArea[0].offsetWidth;
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").addClass("settingList_down");
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list .top").addClass("settingList_add");
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list .bottom").addClass("settingList_delete");

/*
                    //bottomList
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area #menuUnFocus_text").eq(0).remove();

                    INSTANCE.div.find("#settingMenu_area #menuBottom_area  #menuUnFocus_text").eq(0).removeClass("top").addClass("bottom");
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");

                    if (!(INSTANCE.div.find("#settingMenu_area #menuBottom_area  #menuUnFocus_text").hasClass("top"))) {
                        INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list").append("<span id='menuUnFocus_text' class='top'></span>");
                    }
                    bottomArea[0].offsetWidth = bottomArea[0].offsetWidth;
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list").addClass("settingList_down");
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list .top").addClass("settingList_add");
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list .bottom").addClass("settingList_delete");
*/


                    //unFocusList

                    //최초 애니메이션 시에는 삭제 없음
                    if(unFocusListArea.find(".bottom").hasClass("settingList_delete")) {
                        unFocusListArea.find(".settingList_delete").remove();
                        unFocusListArea.find(".top").removeClass("settingList_add");
                    }
                    // slide down 이 먼저 일어난 경우 element 정리
                    if(unFocusListArea.find(".top").hasClass("settingList_delete")) {
                        unFocusListArea.find(".top").removeClass("settingList_delete");
                        unFocusListArea.find(".bottom").remove();
                    }

                    unFocusListArea.removeClass("settingList_up settingList_down settingList_add settingList_delete");
/*                    unFocusListArea.removeClass("settingList_up settingList_down settingList_add settingList_delete");
                    unFocusListArea.find("#menuUnFocusText").removeClass("settingList_add settingList_delete");*/
                    unFocusListArea.append("<span id='menuUnFocus_text' class='bottom'></span>");
                    //3. slide up
                    unFocusListArea[0].offsetWidth = unFocusListArea[0].offsetWidth;
                    unFocusListArea.addClass("settingList_down");
                    unFocusListArea.find(".top").addClass("settingList_add");
                    unFocusListArea.find(".bottom").addClass("settingList_delete");
                    break;

                case "DOWN" :

               /*     // topListArea
                    INSTANCE.div.find("#settingMenu_area #menuTop_area #menuUnFocus_text").eq(0).remove();

                    INSTANCE.div.find("#settingMenu_area #menuTop_area  #menuUnFocus_text").eq(0).removeClass("bottom").addClass("top");
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list").removeClass("settingList_up settingList_down settingList_add");

                    if (!(INSTANCE.div.find("#settingMenu_area #menuTop_area  #menuUnFocus_text").hasClass("bottom"))) {
                        INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list").append("<span id='menuUnFocus_text' class='bottom'></span>");
                    }

                    topArea[0].offsetWidth = topArea[0].offsetWidth;
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list").addClass("settingList_up");
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list .top").addClass("settingList_delete");
                    INSTANCE.div.find("#settingMenu_area #menuTop_area .menuTop_list .bottom").addClass("settingList_add");
*/
                    //focusArea
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area #menuFocus_text").eq(0).remove();

                    INSTANCE.div.find("#settingMenu_area #menuFocus_area  #menuFocus_text").eq(0).removeClass("bottom").addClass("top");
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");

                    if(!(INSTANCE.div.find("#settingMenu_area #menuFocus_area  #menuFocus_text").hasClass("bottom"))) {
                        INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").append("<span id='menuFocus_text' class='bottom'></span>");
                    }
                    focusArea[0].offsetWidth = focusArea[0].offsetWidth;
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list").addClass("settingList_up");
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list .top").addClass("settingList_delete");
                    INSTANCE.div.find("#settingMenu_area #menuFocus_area .menuFocus_list .bottom").addClass("settingList_add");

               /*     //bottomArea
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area #menuUnFocus_text").eq(0).remove();

                    INSTANCE.div.find("#settingMenu_area #menuBottom_area  #menuUnFocus_text").eq(0).removeClass("bottom").addClass("top");
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list").removeClass("settingList_up settingList_down settingList_add settingList_delete");

                    if(!(INSTANCE.div.find("#settingMenu_area #menuBottom_area  #menuUnFocus_text").hasClass("bottom"))) {
                        INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list").append("<span id='menuUnFocus_text' class='bottom'></span>");
                    }
                    topArea[0].offsetWidth = topArea[0].offsetWidth;
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list").addClass("settingList_up");
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list .top").addClass("settingList_delete");
                    INSTANCE.div.find("#settingMenu_area #menuBottom_area .menuBottom_list .bottom").addClass("settingList_add");*/


                    //unfocusArea

                    // 추가 엘레먼트 생성이 안되고 최초 애니메이션 시 삭제 여부 결정
                    if(unFocusListArea.find(".top").hasClass("settingList_delete")) {
                        unFocusListArea.find(".settingList_delete").remove();

                        unFocusListArea.find(".middle").removeClass("middle").addClass("top");
                        unFocusListArea.find(".bottom").removeClass("settingList_add");
                        unFocusListArea.find(".bottom").removeClass("bottom").addClass("middle");
                    }
                    // slide up 이 먼저 일어난 경우 element 정리
                    if(unFocusListArea.find(".bottom").hasClass("settingList_delete")) {
                        unFocusListArea.find(".settingList_delete").remove();
                    }
                    unFocusListArea.removeClass("settingList_up settingList_down settingList_add settingList_delete");

                    unFocusListArea.append("<span id='menuUnFocus_text' class='bottom'></span>");

                    //3. slide up;
                    unFocusListArea[0].offsetWidth = unFocusListArea[0].offsetWidth;
                    unFocusListArea.addClass("settingList_up");
                    unFocusListArea.find(".top").addClass("settingList_delete");
                    unFocusListArea.find(".bottom").addClass("settingList_add");
                    break;
            }
        }

        /**
         * sw.nam
         * 현재 해상도로 subTitle 을 변경 한다
         */
        function checkSetResolution() {
            log.printDbg("checkSetResolution");

            var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
            var result = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_RESOLUTION);
            var subTitleText = result[0];
            if(subTitleText == "2160p30") {
                subTitleText = "2160p (30fps)";
            } else if(subTitleText == "2160p60") {
                subTitleText = "2160p (60fps)";
            }
            var index = KTW.managers.service.SettingMenuManager.getSettingMenuIndex(settingMenu.children[2],KTW.managers.data.MenuDataManager.MENU_ID.SETTING_RESOLUTION);
            if(index) {
                compList[2].setSpecificSubTitle(index, subTitleText);
            }

        }

        /**
         * sw.nam
         * For checking paired bluetooth devices are modified by connecting action.
         * 외부 환경에 의해 변경된 블루투스 페어링 정보를 subTitle 에 반영하는 함수(only UHD3)
         */
        function checkConnectedBluetoothInfo() {
            log.printDbg("checkConnectedBluetoothInfo");
            var subTitleText;
            var list = KTW.managers.device.DeviceManager.bluetoothDevice.getPairedDevices(true,true);
            if(list) {
                if(language == LANG.KOR) {
                    subTitleText = "연결된 장치 "+list.length+"개";
                } else {
                    subTitleText = "Device: "+list.length;
                }
            }else {
                if(language == LANG.KOR) {
                    subTitleText = "연결된 장치 0개";
                }else {
                    subTitleText = "Device: 0";
                }
            }
            var index = KTW.managers.service.SettingMenuManager.getSettingMenuIndex(settingMenu.children[2],KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BLUETOOTH);
            if(index) {
                compList[2].setSpecificSubTitle(index, subTitleText);
            }
        }
    };

    KTW.ui.layer.setting.setting_main.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_main.constructor = KTW.ui.layer.setting.setting_main;

    KTW.ui.layer.setting.setting_main.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init();
        if (cbCreate) {
            cbCreate(true);
        }
    };

    KTW.ui.layer.setting.setting_main.prototype.show = function(options) {
        KTW.ui.Layer.prototype.show.call(this, options);
        this.showView(options);
    };

    KTW.ui.layer.setting.setting_main.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.setting.setting_main.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };


})(); //settingMain