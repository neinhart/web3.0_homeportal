/**
 * Created by Yun on 2016-11-21.
 * 설정 > 자녀안심 설정 > 비밀번호 변경 / 초기화
 */

(function() {
    KTW.ui.layer.setting.setting_password  = function PasswordChange(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var INSTANCE = this;
        var styleSheet;
        var focusIdx = 0;
        //var toastPopup = null;
        //var bluetoothToast = null;
        var isSystemPriority = false;

        var clock;
        var clockArea;

        var settingPasswordErrorPopupLayerId = "settingPasswordErrorPopup";

        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame settingPassword"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>비밀번호 변경/초기화</span>" +
                //"<div id='pig_dim'></div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );
            createComponent();
         //   createClock();


            /**
             * 키즈모드 부팅시 성인 인증 팝업을 통해 activate 될 경우, 아래 2가지 이유로 배경 div 가 필요
             * 1. av 의 까만 화면이 LayerManager 에서 관리하는 background 를 가림
             * 2. 키즈모드 부팅시 키즈 모드 성인 인증 팝업 등은 system popup 이기 때문에, 다른 system popup 이 띄워졌다고 해서 hide 가 되지 않음
             */
            var kidsModeBackgroundArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "setting_password_system_popup_bg_area",
                    css: {
                        display: "none"
                    }
                }
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/bg_default.jpg",
                    css: {
                        position: "absolute", left: 0, top: 0
                    }
                },
                parent: kidsModeBackgroundArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "setting_password_system_popup_block_img",
                    src: "images/iframe/block_kids.jpg",
                    css: {
                        position: "absolute", left: KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT, top: KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP, width: KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH, height: KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT
                    }
                },
                parent: kidsModeBackgroundArea
            });
            this.div.prepend(kidsModeBackgroundArea);

            cbCreate(true);
        };

        this.show = function(options) {
            log.printDbg("show()");

            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            dataInit();
            var data = this.getParams();
            if (data) {
                isSystemPriority = data.isSystemPriority;
            }
            //isSystemPriority = data.isSystemPriority;

            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.ALL);
                this.div.find("#setting_password_system_popup_bg_area").css({display: "block"});

                if (KTW.managers.service.KidsModeManager.isKidsModeAdultAuthPopupOpend()) {
                    this.div.find("#setting_password_system_popup_block_img").attr("src", "images/iframe/block_kids.jpg");
                }
                else if (KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
                    this.div.find("#setting_password_system_popup_block_img").attr("src", "images/iframe/block_time.jpg");
                }
            }
            else {
                this.div.find("#setting_password_system_popup_bg_area").css({display: "none"});
            }

            if(!options || !options.resume) {
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CHANGE_PIN) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

       //     clock.show(clockArea);
        };

        this.hideView = function(options) {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

            if (!options || !options.pause) {
                this.div.find("#setting_password_system_popup_bg_area").css({display: "none"});
            }

            if (isSystemPriority) {
                KTW.oipf.AdapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);
            }
            log.printDbg("hide()");
       //     clock.hide();

            if(options === undefined) {
                KTW.managers.auth.AuthManager.initPW();
                KTW.managers.auth.AuthManager .resetPWCheckCount();
            }
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey() key_code : " + key_code);

            switch (key_code) {
                case KEY_CODE.ENTER:
                    var layerID = null;
                    var param = {};

                    if (focusIdx === 0) {
                        layerID = KTW.ui.Layer.ID.SETTING_CHANGE_PASSWORD_POPUP;
                        param.change_password_type = 0;
                        param.isSystemPriority = isSystemPriority;

                        var priority = isSystemPriority ? Layer.PRIORITY.SYSTEM_POPUP + 5 : Layer.PRIORITY.POPUP;

                        LayerManager.activateLayer({
                            obj: {
                                id: layerID,
                                type: Layer.TYPE.POPUP,
                                priority: priority,
                                linkage: isSystemPriority === true ? true : false,
                                params: param
                            },
                            new: true,
                            visible: true
                        });
                    }
                    else if (focusIdx === 1) {
                        param.callback = callbackFuncAuthCheck;
                        param.isSystemPriority = isSystemPriority;
                        layerID = KTW.ui.Layer.ID.SETTING_CHANGE_BUY_PASSWORD_POPUP;
                        var priority = isSystemPriority ? Layer.PRIORITY.SYSTEM_POPUP + 5 : Layer.PRIORITY.POPUP;

                        LayerManager.activateLayer({
                            obj: {
                                id: layerID,
                                type: Layer.TYPE.POPUP,
                                priority: priority,
                                linkage: false,
                                params: param
                            },

                            new: true,
                            visible: true
                        });
                    }else if(focusIdx === 2) {
                        _requestUserInfo();

                    }
                    return true;
                case KEY_CODE.UP:
                    focusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 3));
                    return true;
                case KEY_CODE.DOWN:
                    focusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 3));
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.LEFT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    var consume = false;
                    if (this.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                        LayerManager.historyBack();
                        consume = true;
                    }
                    return consume;
                default:
                    if (isSystemPriority) {
                        return true;
                    }
                    return false;
            }
        };

        function dataInit() {
            focusIdx = 0;
            focusRefresh(0);
        }

        function focusRefresh(index) {
            focusIdx = index;
            INSTANCE.div.find("#contents #settingTable .table_btn_bg").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable .table_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable .table_contentsArea:eq(" + index + ") .table_btn_bg").addClass("focus");
            INSTANCE.div.find("#contents #settingTable .table_contentsArea:eq(" + index + ") .table_btn_text").addClass("focus");
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_contentsArea' style='position: absolute; top: 0px; width: 1654px; height: 274px;'>"+
                    "<div class='table_title'>olleh tv 비밀번호(성인인증) 변경</div>" +
                    "<div class='table_subTitle'>시청제한 등 성인인증에 필요한 비밀번호를 변경합니다</div>" +
                    "<div class='table_btn_area'>" +
                        "<div class='table_btn_bg'></div>" +
                        "<div class='table_btn_text'>비밀번호 변경</div>" +
                    "</div>" +
                "</div>"+
                "<div class='table_divisionLine'style='position: absolute; top: 274px; width: 1654px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_contentsArea' style='position: absolute; top: 276px; width: 1654px; height: 274px;'>"+
                    "<div class='table_title'>구매인증 번호 변경</div>" +
                    "<div class='table_subTitle'>VOD, 유료채널 구매에 사용되는 인증번호를 변경합니다</div>" +
                    "<div class='table_btn_area'>" +
                        "<div class='table_btn_bg'></div>" +
                        "<div class='table_btn_text'>인증번호 변경</div>" +
                    "</div>" +
                "</div>" +
                "<div class='table_divisionLine'style='position: absolute; top: 550px; width: 1654px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_contentsArea' style='position: absolute; top: 552px; width: 1654px; height: 274px;'>"+
                    "<div class='table_title' style= 'top: 73px; '>비밀번호 초기화</div>" +
                    "<div class='table_subTitle' style='top: 141px; '>가입 명의자의 휴대폰 인증을 통해 성인인증 비밀번호," +
                        "구매인증 번호 모두 초기화 합니다<br>" +
                        "※휴대폰, 셋톱박스 명의가 법인인 가입자는 사용 불가합니다</div>" +
                    "<div class='table_btn_area' style='top: 105px;'>" +
                        "<div class='table_btn_bg'></div>" +
                        "<div class='table_btn_text'>초기화</div>" +
                    "</div>" +
                "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function callbackFuncAuthCheck(success) {
            var param = {
                change_password_type : 1,
                isSystemPriority: isSystemPriority
            };
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_CHANGE_BUY_PASSWORD_POPUP
            });
            if (success !== undefined && success !== null && success === true) {
                var priority = isSystemPriority ? Layer.PRIORITY.SYSTEM_POPUP + 5 : Layer.PRIORITY.POPUP;
                LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.SETTING_CHANGE_PASSWORD_POPUP,
                        type: Layer.TYPE.POPUP,
                        priority: priority,
                        linkage: isSystemPriority === true ? true : false,
                        params: param
                    },

                    new: true,
                    visible: true
                });
            }
        }

        function _requestUserInfo() {
            log.printDbg("requestUserInfo()");
            KTW.ui.LayerManager.startLoading();

            var now = new Date().getTime();
            now = Math.floor(now/1000);
            log.printDbg("onHDSAuthPinResponse() now : " + now);

            KTW.managers.auth.AuthManager.reqCertUserInfo(now , _callbackFuncHdsCertUserInfo);
        }

        function _callbackFuncHdsCertUserInfo(result, xhr, error_message) {
            log.printDbg("_callbackFuncHdsCertUserInfo() result : " + result);
            KTW.ui.LayerManager.stopLoading();

            if (result) {
                // [jh.lee] reqCertUserInfo 요청이 정상이고 받아온 데이터가 존재하는 경우
                var code = xhr.responseXML.getElementsByTagName("RCode");

                log.printDbg("CertUserInfo code : " + code);

                if (code[0].textContent == "SHD0000100") {
                    // [jh.lee] 성공
                    var gender = xhr.responseXML.getElementsByTagName("Gender")[0].textContent;
                    var nation = xhr.responseXML.getElementsByTagName("Nation")[0].textContent;
                    var uname = xhr.responseXML.getElementsByTagName("Uname")[0].textContent;
                    var data_param = {"telNo" : "", "birth" : "", "gender":gender, "nation":nation, "uname":uname , callback:_callbackMobilePhoneAuth};
                    log.printDbg("Success param : " + JSON.stringify(data_param));

                    var priority = isSystemPriority ? Layer.PRIORITY.SYSTEM_POPUP + 5 : Layer.PRIORITY.POPUP;
                    LayerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.SETTING_RESET_PASSWORD_POPUP,
                            type: Layer.TYPE.POPUP,
                            priority: priority,
                            linkage: false,
                            params: {
                                data : data_param,
                                isSystemPriority: isSystemPriority,
                                callback : _callbackMobilePhoneAuth
                            }
                        },
                        visible: true
                    });

                } else {
                    /**
                     * HDS 연동은 성공 하였으나 Response 정보 분석 결과 오류로 전달 된 경우
                     */
                    var message = xhr.responseXML.getElementsByTagName("RMsg")[0].textContent;
                    showErrorPopup(message , code[0].textContent);
                }
            } else {
                /**
                 * HDSManager에서 팝업 띄우도록 수정함.
                 */
                if(error_message !== undefined && error_message !== null) {
                    if(error_message !== undefined && error_message !== null && error_message instanceof Array) {
                        KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , error_message, null, null, true);
                    }else {
                        if(error_message.error === true) {
                            var message = ["가입자 인증 오류입니다", "잠시 후 다시 시도해 주세요", "※동일 현상 반복 시 국번 없이 100번으로 문의 바랍니다"  ];
                            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , message, null, null,  true);
                        }
                    }
                }
            }
        }

        function _callbackMobilePhoneAuth(result) {
            LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_RESET_PASSWORD_POPUP
            });

            if(result !== undefined && result !== null && result === true) {
                var priority = isSystemPriority ? Layer.PRIORITY.SYSTEM_POPUP + 5 : Layer.PRIORITY.POPUP;
                LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.SETTING_PASSWORD_INIT_COMPLETE_POPUP,
                        type: Layer.TYPE.POPUP,
                        priority: priority,
                        linkage: false,
                        params: {
                            isSystemPriority: isSystemPriority
                        }
                    },

                    new: true,
                    visible: true
                });
            }
        }

        function showErrorPopup(message , code) {
            var isCalled = false;

            var popupData = {
                arrMessage: [{
                    type: "Title",
                    message: ["인증오류"],
                    cssObj: {}
                }],
                arrButton: [{id: "ok", name: "확인"}],
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;

                        if (buttonId && buttonId === "ok") {
                            LayerManager.deactivateLayer({
                                id: settingPasswordErrorPopupLayerId
                            });
                        }
                    }
                }
            };


            popupData.arrMessage.push({
                type: "MSG_45",
                message: [message],
                cssObj: {}
            });

            popupData.arrMessage.push({
                type: "MSG_38",
                message: [code],
                cssObj: {}
            });

            var priority = isSystemPriority ? Layer.PRIORITY.SYSTEM_POPUP + 5 : Layer.PRIORITY.POPUP;
            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: settingPasswordErrorPopupLayerId,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: priority,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData,
                        enableDCA : isSystemPriority === true ? "false" : "true",
                        enableChannelKey : isSystemPriority === true ? "false" : "true",
                        enableMenuKey : isSystemPriority === true ? "false" : "true",
                        enableHotKey : isSystemPriority === true ? "false" : "true"
                    }
                },
                visible: true,
                cbActivate: function () {}
            });
        }
    };
    KTW.ui.layer.setting.setting_password.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_password.constructor = KTW.ui.layer.setting.setting_password;

    KTW.ui.layer.setting.setting_password.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_password.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.setting.setting_password.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();