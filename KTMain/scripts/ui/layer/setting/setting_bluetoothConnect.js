/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 블루투스 장치 연결
 */

(function () {
    // 팝업 붙음
    KTW.ui.layer.setting.setting_bluetoothConnect  = function BluetoothConnect(options) {
        KTW.ui.Layer.call(this, options);

        var MAX_NUMBER_OF_DEVICE = 6;

        var log  = KTW.utils.Log;
        var util = KTW.utils.util;

        var INSTANCE = this;
        var styleSheet;


        var focusMode = 0;
        var rdoFocus = 0, listFocus = 0, btnFocus = 2;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager = KTW.ui.LayerManager;
        var storageManager = KTW.managers.StorageManager;
        var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        //var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager =KTW.managers.service.SettingMenuManager;

        //var DEF = KTW.oipf.Def;
        var CONFIG = KTW.oipf.Def.CONFIG;
        var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;

        var data;

        var useBluetooth = false; // 블루투스 사용 여부 변수
        //var isFaring = false;

        // [sw.nam] UHD2 확인 변수
        var check_rcu_ble;
        //var btnCount = 2;
        //var check_android = false;//안드로이드 박스 여부

        // Fairing list
        var list = null;
        var list_length = null;
        var latestPairedAudioDevices;

        var isFairing = false;

        var subArea = null;

        var clock;
        var clockArea;

        //[sw.nam] 2017.04.20 추가 - 오디오 장치 연결 여부에 따른 연관메뉴 키 flag
        //var isConnectedAudioDevice = false;

        var MODE = {
            RADIO : 0,
            LIST : 1,
            BUTTON : 2,
            ANDROID: 3
        };

        var prevFocusedArea;
        var prevFocusedIdx;

        //[sw.nam] 2017.04.30 추가 - 예외처리를 위한 bluetooth 타이머
        var smallLoadingTimer = null;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setting bluetoothConnect"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>블루투스 장치 연결</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            INSTANCE.div.find("#contents").css({display: "block"});
            log.printDbg("show()");

            // [sw.nam] 외부 요인에 의해 상태가 바뀌는 경우에 대한 화면 callback showing function
            hwAdapter.addEventListener(CONFIG.EVENT.DEVICE, onChangeDeviceConnect);

            //[sw.nam] 블루투스 초기 화면 설명 여부 init - UHD1, UHD2, 안드로이드 STB, UHD3 형상 구분
            initConfiguration();

            readConfiguration();
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BLUETOOTH) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
            stopSmallLoading();

        //    clock.show(clockArea);
        };


        function onChangeDeviceConnect(device_type, event) {
            log.printDbg("onChangeDeviceConnect()...");

            var type = event.type;
            var device = event.device;
            //var is_add = false;
            var RCU_AUDIO_DEVICE_NAME = "KT-UHD3-RS-AUDIO";
            log.printDbg("change device connection !!");
            log.printDbg(device.name + " is connection " + JSON.stringify(device));
            log.printDbg(device.name + " is connection " + device.connected);
            log.printDbg(device.name + "'s mac address is  " + device.macAddress);
            log.printDbg("state changed device_ type is :" + device_type);
            log.printDbg("state changed device's type is :" + type);

            if(device.name === RCU_AUDIO_DEVICE_NAME) {
                log.printDbg("np action on this event_ " + device.name );
                // 2017.12.05 sw.nam
                // 신규 리모컨 이어폰 장치의 경우 블루투스 설정에서 제어하지 않으므로
                // 해당 장치 관련 이벤트는 무시하도록 예외처리 추가
                return ;
            }

            if (device_type === CONFIG.DEVICE_TYPE.BLUE_TOOTH) {
                switch (type) {
                    case CONFIG.EVENT.DEVICE_CONNECT:
                        log.printDbg("onChangeDeviceConnect DEVICE_CONNECT");
                        refreshListArea(device.macAddress);
                        //2017.07.14 sw.nam 연결된 장치가 오디오 장치인 경우, 연결 이력 유무를 검사하여 혼자/같이 듣기 팝업 노출 여부를 결정해야 함.
                        if(device.type == BluetoothDevice.TYPE_HEADPHONE || device.type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                            var popFlag = false;
                            if(latestPairedAudioDevices) {
                                for(var i=0; i < latestPairedAudioDevices.length; i++) {
                                        if(device.macAddress === latestPairedAudioDevices[i].macAddress) {
                                            //팝업 flag on
                                            log.printDbg("hit the device");
                                            popFlag = true;
                                            break;
                                        }
                                }
                                if(popFlag) {
                                    showDeviceSoundSettingPopup();
                                    // 최초 연결이 이루어진 장치는 연결 이력 관리 리스트에서 삭제
                                    log.printDbg("Delete the device has been connected");
                                    latestPairedAudioDevices.splice(i,1);
                                }
                            }
                        }
                        // [sw.nam] 오디오 기기인 경우 연결여부 판단하여 메뉴키 show & hide
                        setSubArea();
                        stopSmallLoading();
                        break;
                    case CONFIG.EVENT.DEVICE_DISCONNECT:
                        log.printDbg("onChangeDeviceConnect DEVICE_DISCONNECT");
                        setDeviceList(list);
                        if(INSTANCE.div.find(".list_item.checked").length <=0) {
                            INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).addClass("disable");
                            if(focusMode === MODE.BUTTON && btnFocus === 0) {
                                btnFocus = 2;
                                setBtnFocus(btnFocus);
                            }
                        }
                        for(var i = 0; i < list_length; i ++) {
                            if(list[i].connected) {
                                connectDevice(i);
                            }
                        }
                        // [sw.nam] 오디오 기기인 경우 연결여부 판단하여 메뉴키 show & hide
                        setSubArea();
                        break;
                    case CONFIG.EVENT.DEVICE_ERROR:
                        log.printDbg("onChangeDeviceConnect DEVICE_ERROR");
                        // sw.nam 이미 페어링된 오디오 장치 연관메뉴 키 눌러 연결 실패한 경우.
                        setDeviceList(list);
                        if(INSTANCE.div.find(".list_item.checked").length <=0) {
                            INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).addClass("disable");
                        }
                        // 연결 실패 팝업 띄움
                        showFairedDeviceConnnectionFailPopup();
                        stopSmallLoading();
                        break;
                }
            }else {

            }
        }
        this.hide = function () {
            styleSheet = styleSheet.detach();
            Layer.prototype.hide.call(this);
            INSTANCE.div.find("#contents").css({display: "none" });
            log.printDbg("hide BluetoothConnect");
            hwAdapter.removeEventListener(CONFIG.EVENT.DEVICE, onChangeDeviceConnect);

            stopSmallLoading();

      //      clock.hide();

        };

        this.controlKey = function (key_code) {
            log.printDbg("controlKey()");
            subArea.css({visibility: "hidden"});
            switch (key_code) {
                case KEY_CODE.BACK:
                    data.complete();
                    LayerManager.historyBack();
                    return true;
            }
            switch (focusMode) {
                case MODE.RADIO: return onKeyForRadioBtn(key_code);
                case MODE.LIST: return onKeyForList(key_code);
                case MODE.BUTTON: return onKeyForButton(key_code);
                case MODE.ANDROID: return onKeyForAndroid(key_code);
            }
        };

        function onKeyForAndroid(key_code) {
            switch (key_code) {
                case KEY_CODE.LEFT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.OK:
                    //[sw.nam] 안드로이드 블루투스 설정 화면으로 이동
                    KTW.oipf.adapters.AndroidAdapter.launchApplication("com.kt.gigagenie.launcher/com.kt.gigagenie.launcher.ui.BluetoothDummyActivity");
                    return true;

                default:
                    return false;
            }
        }
        function onKeyForRadioBtn(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    if(rdoFocus==1) setRadioFocus(0);
                    else {
                        data.complete();
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(rdoFocus==0) {
                        setRadioFocus(1);
                    }else if(rdoFocus ==1) {
                        prevFocusedArea = MODE.RADIO;
                        prevFocusedIdx = rdoFocus;
                        btnFocus = 2;
                        setFocusMode(MODE.BUTTON);
                    }
                    return true;
                case KEY_CODE.UP:
                    return true;
                case KEY_CODE.DOWN:
                    if(!bluetoothDevice.enabled) {
                        return true;
                    }
                    listFocus = 0;
                    setFocusMode(1);
                    return true;
                case KEY_CODE.ENTER:
                    toggleUseBluetooth(rdoFocus==0);
                    /*
                     [sw.nam] 블루투스 사용 선택 옵션에 따라서 UI 구성 및 키 navigation 이 달라진다
                     case1 : 실제 설정된 값도 "사용" 이고 선택한 옵션도 "사용" 인 경우. -> 저장된 리스트가 있으면 리스트 모드로 포커스, 리스트가 없으면 "검색" 으로 포커스 된다.
                     case2 : 실제 설정된 값은 "사용 안함" 인데 선택한 옵션은 "사용" 인 경우, -> 저장된 리스트 여부에 관계없이 "저장" 으로 포커스, 장치삭제, 검색 버튼은 활성화 하지 않는다.
                     case3 : 실제 설정된 값도 "사용 안함" 이고 선택한 옵션도 "사용 안함" 인경우  -> 저장된 리스트 여부에 관게없이 "저장" 으로 포커스
                     */
                    if(rdoFocus == 0) {
                        if(bluetoothDevice.enabled) {
                            //case 1
                            if(list_length > 0) {
                                listFocus =0;
                                setFocusMode(MODE.LIST);
                            } else {
                                prevFocusedArea = MODE.RADIO;
                                prevFocusedIdx = rdoFocus;
                                btnFocus = 1;
                                setFocusMode(MODE.BUTTON);

                            }
                        } else {
                            //case 2
                            prevFocusedArea = MODE.RADIO;
                            prevFocusedIdx = rdoFocus;
                            btnFocus = 2;
                            setFocusMode(MODE.BUTTON);
                        }
                    } else if (rdoFocus == 1) {
                        prevFocusedArea = MODE.RADIO;
                        prevFocusedIdx = rdoFocus;
                        btnFocus = 2;
                        stopSmallLoading();
                        setFocusMode(MODE.BUTTON);
                    }
                    return true;
            }
        }

        function onKeyForList(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    data.complete();
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.RIGHT:
                    if(INSTANCE.div.find(".list_item.checked").length >0){
                        btnFocus = 0;
                    }else {
                        btnFocus = 2;
                    }
                    prevFocusedArea = MODE.LIST;
                    prevFocusedIdx = listFocus;
                    setFocusMode(MODE.BUTTON);
                    return true;
                case KEY_CODE.UP:
                    if(check_rcu_ble) {
                        log.printDbg("check_rcu_ble UP");
                        if(listFocus < 1) {
                            setSubArea();
                            return true;
                        }
                        else {
                            setListFocus(listFocus-1);
                        }
                    } else {
                        if(listFocus==0) {
                            if(useBluetooth) {
                                rdoFocus =0;
                            } else {
                                rdoFocus =1;
                            }
                            setFocusMode(MODE.RADIO);
                        }
                        else {
                            setListFocus(listFocus-1);
                        }
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if(listFocus == list_length-1){
                        setSubArea();
                    }else {
                        setListFocus(listFocus+1);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    INSTANCE.div.find(".list_item").eq(listFocus).toggleClass("checked");
                    INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).toggleClass("disable", INSTANCE.div.find(".list_item.checked").length <=0);
                    setListFocus(listFocus);
                    return true;
                case KEY_CODE.CONTEXT:
                    // [sw.nam] 오디오 기기인 경우 연결여부 판단하여 메뉴키 show & hide
                    setSubArea();
                    if( !isFairing && !list[listFocus].connected && (list[listFocus].type == BluetoothDevice.TYPE_HEADPHONE || list[listFocus].type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) ) {
                        bluetoothDevice.audioToggle(list[listFocus]);
                        startSmallLoading();
                    } else if(list[listFocus].connected && (list[listFocus].type == BluetoothDevice.TYPE_HEADPHONE || list[listFocus].type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) ) {
                        showDeviceSoundSettingPopup();
                    }
                    return true;
            }
        }

        function onKeyForButton(keyCode) {
            switch (keyCode) {
                case KEY_CODE.LEFT:
                    log.printDbg(("onKeyForButton LEFT"));
                    if(check_rcu_ble) {
                        if(list_length > 0) {
                            listFocus = prevFocusedIdx;
                            setFocusMode(MODE.LIST);
                        }
                        else{
                            data.complete();
                            LayerManager.historyBack();
                        }
                    } else {
                        if(prevFocusedArea == MODE.RADIO) {
                            rdoFocus = prevFocusedIdx;
                            setFocusMode(MODE.RADIO);
                        }else if(prevFocusedArea == MODE.LIST) {
                            listFocus = prevFocusedIdx;
                            setFocusMode(MODE.LIST);
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    return true;
                case KEY_CODE.UP:
                    btnFocus--;
                    if(bluetoothDevice.enabled && useBluetooth) {
                        if (INSTANCE.div.find(".list_item.checked").length > 0) {
                            if (btnFocus < 0) {
                                btnFocus = 3;
                            }
                        } else {
                            if (btnFocus < 1) {
                                btnFocus = 3;
                            }
                        }
                    }else {
                        if(btnFocus < 2) {
                            btnFocus = 3;
                        }
                    }
                    setBtnFocus(btnFocus);
                    return true;
                case KEY_CODE.DOWN:
                    btnFocus++;
                    if(bluetoothDevice.enabled && useBluetooth) {
                        if(btnFocus > 3) {
                            if (INSTANCE.div.find(".list_item.checked").length > 0) {
                                btnFocus = 0;
                            } else {
                                btnFocus = 1;
                            }
                        }
                    }else {
                        if(btnFocus > 3) {
                            btnFocus = 2;
                        }
                    }
                    setBtnFocus(btnFocus);
                    return true;
                case KEY_CODE.ENTER:
                    stopSmallLoading();
                    switch (btnFocus) {
                        case 0:
                            LayerManager.activateLayer({
                                obj: {
                                    id: "BtDeviceDeletePopup",
                                    type: Layer.TYPE.POPUP,
                                    linkage: true,
                                    priority: Layer.PRIORITY.POPUP,
                                    params: {
                                        deviceCnt: INSTANCE.div.find(".list_item.checked").length,
                                        callback: function () {
                                            onShowDeletePopup()
                                        }
                                    }
                                },

                                visible: true
                            });
                            return true;
                        case 1:
                            // 장치 검색
                            if(list_length >= MAX_NUMBER_OF_DEVICE) {
                                // 6개 이상 등록 불가
                                showLimitPopup();
                            } else {
                                showSearchingPopup();
                            }
                            return true;
                        case 2:
                            // 저장
                            /*
                             [sw.nam] 2017.04.20 UHD1 STB 현재 실제 설정된 블루투스 옵션에 따라 Action 을 다르게 해야 함
                             case 1 : 실제 설정된 블루투스 옵션은 "사용안함" 이고 선택한 옵션이 "사용" 인 경우 현재 설정 저장 후 "블루투스 사용" 으로 포커스 (**새롭게 추가된 GUI)
                             case 2 : 실제 설정된 블루투스 옵션과 선택한 옵션 모두  "사용" 인 경우 -> historyBack
                             case 3 : 실제 설정된 블루투스 옵션과 선택한 옵션 모두 "사용안함" 인 경우  ->  historyBack
                             case 4 : 실제 설정된 블루투스 옵션은 "사용안함" 이고 선택한 옵션이 "사용" 인 경우 -> historyBack
                             */
                            //case 1
                            if(!bluetoothDevice.enabled && useBluetooth) {
                                bluetoothDevice.enabled = true;
                                if(list_length > 0) {
                                    listFocus = 0;
                                    setFocusMode(MODE.LIST);
                                } else {
                                    setFocusMode(MODE.RADIO);
                                }
                                toggleUseBluetooth(rdoFocus==0);
                                data.complete();
                            }
                            // case 2,3,4
                            else {
                                if(useBluetooth){
                                    bluetoothDevice.enabled = true;
                                } else {
                                    bluetoothDevice.enabled = false;
                                }
                                data.complete(); // 설정 mainLayer subTitle updating 함수
                                settingMenuManager.historyBackAndPopup();
                            }
                            return true;
                        case 3:
                            data.complete();
                            LayerManager.historyBack();
                            return true;
                    }
            }
        }
        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingArea'>" +
            "<div class='table_title'>블루투스 장치 연결</div>" +
            "<div class='table_subTitle'>키보드, 마우스, 오디오 기기만 이용할 수 있습니다<br>장치 검색 버튼을 선택하면 블루투스 장치를 검색합니다</div>" +
            "<div class='radio_area'>" +
                "<div class='button' style='position: relative; float: left;'>"+
                    "<radioBtn>블루투스 사용</radioBtn>" +
                    "<div class='buttonLine'></div>"+
                "</div>"+
                "<div class='button' style='position: relative; float: left;'>"+
                    "<radioBtn>블루투스 사용 안함</radioBtn>" +
                    "<div class='buttonLine'></div>"+
                "</div>"+
            "</div>" +
            "<div id='listArea'></div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>동시 연결은 최대 3개까지<br>가능합니다 (오디오 장치는<br>1개만 이용 가능합니다)</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='button' style='position: relative'>"+
                "<btn class='onlyUse'>장치 삭제</btn>" +
                "<div class='buttonLine'></div>"+
            "</div>"+
            "<div class='button' style='position: relative'>"+
                "<btn class='onlyUse'>장치 검색</btn>" +
                "<div class='buttonLine'></div>"+
            "</div>"+
            "<sepline class='onlyUse'/>" +
            "<div class='button' style='position: relative'>"+
            "<btn>저장</btn>" +
            "<div class='buttonLine'></div>"+
            "</div>"+
            "<div class='button' style='position: relative'>"+
            "<btn>취소</btn>" +
            "<div class='buttonLine'></div>"+
            "</div>"+
            "</div>" +
            "</div>");

            setListArea(INSTANCE.div.find("#listArea"));
            createElement();
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function setListArea(div) {
            div.append("<div id='notConnect_area'>" +
            "<img id='notConnect_img' src='images/icon/icon_noresult.png'>" +
            "<div id='notConnect_text'>등록된 장치가 없습니다</div>" +
            "</div>");
            var itemList = $("<ul/>", {class: 'item_list'});
            for(var i=0; i<MAX_NUMBER_OF_DEVICE; i++) itemList.append(getListItem(i));
            div.append(itemList);

            function getListItem(index) {
                var item = $("<li/>", {class:'list_item'});
                item.append("<div class= 'list_check_box'>"+
                "<img class='list_check_box_unchecked' style='position: absolute;' src='images/pop_cb_dim_uncheck.png'>" +
                "<img class='list_check_box_checked' style='position: absolute;' src='images/pop_cb_dim_check.png'>" +
                "<img class='list_check_box_checked_red' style='position: absolute;' src='images/pop_cb_dim_check_red.png'>" +
                "</div>");
                item.append($("<div/>", {class:'list_icon'}));
                item.append($("<div/>", {class:'list_item_name'}));
                item.append($("<div/>", {class:'list_state'}));
                item.append("<div class= 'list_loading'>" +
                "<img class='list_loadingBg' src='images/loading/bg_loading_update.png'>" +
                "<img class='list_loadingWatch' src='images/loading/sample_loading_watching.png'>" +
                "</div>");
                item.append("<div class= 'list_focusingLine' style='margin-top: -78px; '></div>");
                //item.append("<div class= 'list_focusingLine'></div>");

                //2017.08.30 sw.nam 페어링 된 블루투스 장치가 6개가 되는 경우 리스트 포커스 영역이 겹치고 벗어나는 이슈 수정
                if (index === MAX_NUMBER_OF_DEVICE - 1) {
                    item.append("<div class= 'list_focusingLine' style='margin-top: -3px; '></div>");
                }else {
                    item.append("<div class= 'list_focusingLine'></div>");
                }

                return item;
            }
        }

        /**
         *  장치 연결 및 삭제 시 페어링 된 블루투스 장치 리스트 포커스를 refresh 하기 위해 사용.
         */
        function refreshListArea(macAddress) {
            log.printDbg("refreshListArea");
            list = bluetoothDevice.getPairedDevices(false,true);
            if(list) {
                list_length = list.length;
            } else {
                list_length  = 0;
            }
            showListArea(list_length);
            setDeviceList(list);
            INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).toggleClass("disable", INSTANCE.div.find(".list_item.checked").length <=0);
            for(var i = 0; i < list_length; i ++) {
                if(list[i].connected){
                    connectDevice(i);
                }
            }
            if(macAddress) {
                listFocus = getBeFocusingDeviceIdxByMac(macAddress);
                setFocusMode(MODE.LIST);
            }else {
                prevFocusedArea = MODE.LIST;
                listFocus = 0;

                prevFocusedIdx = listFocus;
                btnFocus = 2;
                setFocusMode(MODE.BUTTON);

            }
        }


        // device 에 장치 타입과 이름을 넣는 함수 .
        /**
         * 현재 페어링된 블루투스 디바이스 상태 setting function (장치 이름, 장치 아이콘)
         * @param list : 페어링된 블루투스 디바이스 리스트
         */
        function setDeviceList(list) {
            var noList = !list || list.length<=0;
            INSTANCE.div.find("#notConnect_area").toggle(noList);
            INSTANCE.div.find("ul.item_list").toggle(!noList);
            if(noList) return;
            var i=0;

            INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).removeClass("disable");
            var div = INSTANCE.div.find("ul.item_list li.list_item");
            div.removeClass("connected connecting checked");

            for(;i<list.length; i++) {
                var d = div.eq(i);
                d.show();
                d.find(".list_icon").removeClass("headset keyboard mouse audio");
                d.find(".list_icon").addClass(settingDataAdapter.getConvertedType(list[i].type));
                d.find(".list_item_name").text(list[i].name);
            }for(;i<6; i++) {
                div.eq(i).hide();
            }
        }

        /**
         * macAddress 을 이용해 원하는 장치의 index 를 얻어온다.
         * @param macAddress : 타겟 mac address
         */
        function getBeFocusingDeviceIdxByMac(macAddress) {
            var result;
            if(!list) {
                log.printDbg("Pared devices don't exist.. so return");
                return ;
            }
            for(var i= 0; i< list.length; i++) {
                if(macAddress == list[i].macAddress) {
                    result = i;
                    break;
                }else {
                    result = 0;
                }
            }
            return result;
        }

        /**
         * 페어링된 블루투스 리스트의 연결 여부 표시
         * @param index -> 선택된 블루투스 리스트 index
         */
        function connectDevice(index) {
            INSTANCE.div.find("ul.item_list li.list_item").eq(index).removeClass('connecting').addClass('connected');
        }

        /**
         * 블루투스 사용 & 사용안함 여부 판단
         * @param use
         *         :true  -> 사용
         *         :false -> 사용 안함
         */
        function toggleUseBluetooth(use) {
            useBluetooth = use;
            var listArea = INSTANCE.div.find("#listArea");

            if(bluetoothDevice.enabled) {
                INSTANCE.div.toggleClass("useBluetooth", useBluetooth);
            }
            var rdoBtn = INSTANCE.div.find(".radio_area radioBtn");
            rdoBtn.eq(0).toggleClass("selected", useBluetooth);
            rdoBtn.eq(1).toggleClass("selected", !useBluetooth);

            if(INSTANCE.div.hasClass("useBluetooth")) {
                listArea.css({opacity: 1.0});
                INSTANCE.div.find(".list_item").find(".list_state").css({visibility: "inherit"});
            }else {
                var opacity;
                if(list_length > 0) {
                    opacity = 0.3;
                }else {
                    opacity = 0;
                }
                listArea.css({opacity: opacity});
                INSTANCE.div.find(".list_item").find(".list_state").css({visibility: "hidden"});
            }
        }

        // [sw.nam] radio 토글, 블루투스 리스트, 장치검색 및 저장 프로세스 포커스 선택하는 함수.
        function setFocusMode(_focus) {
            if( ( !useBluetooth || list_length == 0 ) && _focus==1) _focus = 0;
            INSTANCE.div.find(".focus").removeClass("focus");
            focusMode = _focus;
            switch (focusMode) {
                case MODE.RADIO: setRadioFocus(rdoFocus); break;
                case MODE.LIST: setListFocus(listFocus); break;
                case MODE.BUTTON: setBtnFocus(btnFocus); break;
                case MODE.ANDROID: setAndroidFocus(3); break;
            }
        }

        function setRadioFocus(_focus) {
            log.printDbg(("setRadioFocus()"));
            INSTANCE.div.find(".radio_area radioBtn.focus").removeClass("focus");
            INSTANCE.div.find(".radio_area .buttonLine.focus").removeClass("focus");
            rdoFocus = _focus;
            INSTANCE.div.find(".radio_area radioBtn").eq(rdoFocus).addClass("focus");
            INSTANCE.div.find(".radio_area .buttonLine").eq(rdoFocus).addClass("focus");

        }

        function setListFocus(_focus) {
            INSTANCE.div.find("#listArea .list_item.focus").removeClass("focus");
            listFocus = _focus;
            INSTANCE.div.find("#listArea .list_item").eq(listFocus).addClass("focus");
            // [sw.nam] 오디오 기기인 경우 연결여부 판단하여 메뉴키 show & hide
            setSubArea();
        }

        function setBtnFocus(_focus) {
            if((!bluetoothDevice.enabled && useBluetooth) || (bluetoothDevice.enabled && !useBluetooth)) {
                _focus = Math.max(2, _focus);
                log.printDbg("buttonFocus :"+_focus);
            }
            if(INSTANCE.div.find("#rightMenu_btn_area btn").eq(_focus).hasClass("disable")) {
                log.printDbg("buttonFocus :"+_focus);
                setBtnFocus(_focus+1);
                return;
            }
            INSTANCE.div.find("#rightMenu_btn_area btn.focus").removeClass("focus");
            INSTANCE.div.find("#rightMenu_btn_area .buttonLine.focus").removeClass("focus");
            btnFocus = _focus;
            INSTANCE.div.find("#rightMenu_btn_area btn").eq(btnFocus).addClass("focus");
            INSTANCE.div.find("#rightMenu_btn_area .buttonLine").eq(btnFocus).addClass("focus");
        }

        function setAndroidFocus(_focus) {
            INSTANCE.div.find("#rightMenu_btn_area btn.focus").removeClass("focus");
            INSTANCE.div.find("#rightMenu_btn_area .buttonLine.focus").removeClass("focus");
            btnFocus = _focus;
            INSTANCE.div.find("#rightMenu_btn_area btn").eq(btnFocus).addClass("focus");
            INSTANCE.div.find("#rightMenu_btn_area .buttonLine").eq(btnFocus).addClass("focus");
            INSTANCE.div.find("#rightMenu_btn_area btn").eq(btnFocus).text("블루투스 설정");
            INSTANCE.div.find("#rightMenu_btn_area btn").eq(btnFocus-1).css({visibility: "hidden"});
        }

        /**
         * [sw.nam] 블루투스 on / off 및 UHD 셋탑 여부 초기 확인 함수
         */
        function initConfiguration() {
            log.printDbg("initConfiguration()");

            //[sw.nam] 안드로이드 박스 여부 (For GIGA genie) - 안드로이드인 경우 블루투스 화면을 안드로이드로 이동 시켜야 함
            if (KTW.CONSTANT.STB_TYPE.ANDROID === true) {
                useBluetooth = false;
                INSTANCE.div.find("#contents").find(".radio_area").css({ visibility: "hidden"});
                INSTANCE.div.find("#contents").find("#listArea").css({ visibility: "hidden"});

            } else {
                var data = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BLUETOOTH);
                var bluetooth = bluetoothDevice.enabled;

                // rcu_ble 지원 여부 확인 function .. 최초 1회만 확인해도 될것으로 보이나 우선 show 에 넣어둔다.
                check_rcu_ble = data[0];
                log.printDbg("check_rcu_ble  == "+check_rcu_ble);
                if (check_rcu_ble === "true") {
                    // [jh.lee] UHD2 와 같이 RCU_BLE 를 지원하는 셋탑 박스
                    // [jh.lee] App 실행시 UHD2 지원 모델이면 자동으로 enabled 가 on 되기 때문에 중복해서 설정하지 않는다.

                    // [sw.nam] radio 버튼이 불필요 하므로 UI hidden
                    INSTANCE.div.find("#contents").find(".radio_area").css({ visibility: "hidden"});
                    useBluetooth = true;
                    rdoFocus = 0;
                } else {
                    INSTANCE.div.find("#contents").find(".table_subTitle").text("키보드, 마우스, 오디오 기기만 이용할 수 있습니다");
                    check_rcu_ble = null;
                    if (bluetooth) {
                        useBluetooth = true;
                        rdoFocus = 0;
                    }else {
                        useBluetooth = false;
                        rdoFocus = 1;
                    }
                }
            }
        }

        /**
         * [sw.nam] 블루투스 기기정보 가져오기 및 추가 & 삭제 상황에 관련한 화면 업데이트 시 사용
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");
            latestPairedAudioDevices = [];
            if(KTW.CONSTANT.STB_TYPE.ANDROID === true) {
                // [sw.nam] android stb 의 경우 ui 재 구성
                INSTANCE.div.find("#contents").find(".table_subTitle").text("블루투스 장치를 등록, 연결 및 관리 하시려면 블루투스 설정 버튼을 선택하세요");
                INSTANCE.div.find("#rightMenu_text").css({visibility: "hidden"});
                INSTANCE.div.find("#rightMenu_btn_area btn").find('.onlyUse').css({visibility: "hidden"});

                setFocusMode(MODE.ANDROID);
                return ;
            }
            //[sw.nam] 등록된 기기 정보 가져오기
            list = bluetoothDevice.getPairedDevices(false,true);
            if(list) {
                list_length = list.length;
            } else {
                list_length  = 0;
            }
            toggleUseBluetooth(useBluetooth);
            showListArea(list_length);

            // 가져온 리스트 업데이트 할 때 사용되는 함수들
            setDeviceList(list);
            INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).toggleClass("disable", INSTANCE.div.find(".list_item.checked").length <=0);
            for(var i = 0; i < list_length; i ++) {
                if(list[i].connected){
                    connectDevice(i);
                }
            }

            if(check_rcu_ble) {
                // [sw.nam] 리스트가 있으면 리스트 첫번째로 포커스, 없으면 "장치 검색" 버튼으로 포커스 시킨다
                if(list_length > 0) {
                    listFocus = 0;
                    setFocusMode(MODE.LIST);
                }
                else {
                    btnFocus = 1;
                    setFocusMode(MODE.BUTTON);
                }
            } else {
                //2017 05.15 [sw.nam] 페어링된 리스트 여부에 관계 없이 사용 / 사용 안함에 default 포커스 위치하도록 수정
                if(useBluetooth){
                    rdoFocus = 0;
                }else {
                    rdoFocus = 1;
                }
                setFocusMode(MODE.RADIO);
            }
        }

        function showListArea(numOfList){

            var listArea = INSTANCE.div.find("#listArea");

            if(numOfList > 0) {
                listArea.find("ul.item_list").css({display: "block"});
                listArea.find("#notConnect_area").css({display: "none"});
                INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).removeClass("hide");
            } else {
                listArea.find("ul.item_list").css({display: "none"});
                listArea.find("#notConnect_area").css({display: "block"});
                INSTANCE.div.find("#rightMenu_btn_area btn").eq(0).addClass("hide");
            }
        }

        function showSearchingPopup() {
            log.printDbg("showSearchingPopup");

            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_BLUETOOTH_DEVICE_SEARCH_POPUP,
                    type: Layer.TYPE.POPUP,
                    linkage: true,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                        complete: function(result,key) {
                            onShowSearchPopup(result,key)
                        }
                    }
                },
                visible: true
            });

        }

        function showLimitPopup() {
            log.printDbg("showLimitPopup()");

            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_BT_DEVICE_LIMIT_POPUP,
                    type: Layer.TYPE.POPUP,
                    linkage: true,
                    priority: Layer.PRIORITY.POPUP
                },
                visible: true
            });
        }

        function showFairedDeviceConnnectionFailPopup() {
            log.printDbg("showConnectFailPopup()");
            if(focusMode == MODE.BUTTON && btnFocus == 0) {
                setBtnFocus(1);
            }
            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_BT_FAIRED_DEVICE_CONNECTION_FAIL_POPUP,
                    type: Layer.TYPE.POPUP,
                    linkage: true,
                    priority: Layer.PRIORITY.POPUP
                },
                visible: true
            });
        }

        /**
         * [jh.lee] showSearchPopup 응답
         * [jh.lee] 팝업의 버튼 인덱스 또는 블루투스 연결에 대한 상태가 결과로 리턴된다.
         */
        function onShowSearchPopup(result,key) {
            log.printDbg("onShowSearchPopup() :::" + JSON.stringify(result));
            log.printDbg("onShowSearchPopup() key:::" + key);
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_BLUETOOTH_DEVICE_SEARCH_POPUP,
                remove: true
            });
            log.printDbg("onShowSearchPopup() result:::" + result);
            if (result === -1) {
                log.printDbg("onShowSearchPopup() result:::  -1");
                // 팝업에서 back, exit 등의 외부 키입력으로 종료되는 경우
            } else if (result === 1) {
                log.printDbg("onShowSearchPopup() result:::  1");
                // 팝업의 취소 버튼을 누른 경우

            } else if (result && result.state === BluetoothInterface.STATE_STARTED ||
                result.state === BluetoothInterface.STATE_COMPLETE) {
                log.printDbg("onShowSearchPopup() result:::  success!~!"+result.state);
                // 디바이스는 찾아서 연결 시도 하였으나 타임아웃으로 연결 성공 처리된 경우(실제로 연결은 안됨) 또는
                // 디바이스를 찾아 연결까지 성공한 경우(실제로 연결됨)
                refreshListArea(result.device.macAddress);
                startSmallLoading();
                //readConfiguration();
                //[sw.nam] 2017.04.20  블루투스 혼자듣기 / 같이듣기 로직 추가
                if(result.device.type == BluetoothDevice.TYPE_HEADPHONE || result.device.type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) {
                    // log history..
                    // 2017.07.12 sw.nam
                    // kt600ej(UHD2), kt600el(UHD3) 셋탑의 경우 onShowSearchPopup 이 로직을 타는 시점에 paring 만 되고 연결이 되지 않아 connected 값이 "false"로 되어있는 타이밍 이슈가 발생 하게 된다.
                    // 검색된 디바이스에 연결을 시도하였으나 실제로 연결은 되지 않고 페어링만 되어있는 상태라고 해도 홈포탈에서는 결과값으로 COMPLETE를 받게 되기 때문에
                    // 오디오 장치라면 혼자듣기 / 같이듣기 선택 팝업을 이 분기문에서 띄워주어야 함, 혼자듣기 / 같이듣기 설정 상태를 바꾸는 것은 연결과 무관하기 때문에 이슈될만할 일이 없어보임.
                    // 각 STB 마다 응답 시간에 따른 타이밍 이슈 발생을 방지하기 위해, 기존 type 검사 후 bluetoothDevice.isAudioDeviceConnected() 로 연결된 오디오 장치를 재 검색하는 부분을 제거하고
                    // 바로 선택팝업을 띄우는 로직으로 수정함.
                    // 2017.07.14 sw.nam
                    // 혼자 / 같이듣기 팝업 을 onChangeDeviceConnect() 이벤트로 이동
                    // 장치 페어링 후 연결된 이력이 없는 장치를 별도의 리스트로 관리
                    // 화면을 벗어난 후 재 진입 상황은 고려하지 않고 블루투스 장치 연결 화면일 경우만 관리한다
                    var pushFlag = true;
                    if(latestPairedAudioDevices) {
                        for(var i= 0; i< latestPairedAudioDevices.length; i++) {
                            if(result.device.macAddress === latestPairedAudioDevices[i].macAddress) {
                                log.printDbg("It had been pushed to the list");
                                pushFlag = false;
                                break;
                            }
                        }
                    }
                    if(pushFlag) {
                        latestPairedAudioDevices.push(result.device);
                    }
                } else {
                    log.printDbg("thisisNotAudioDevice !");
                }
            }
        }

        function showDeviceSoundSettingPopup() {
            log.printDbg("showDeviceSoundSettingPopup()");
            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_BLUETOOTH_SOUND_SETTING_POPUP,
                    type: Layer.TYPE.POPUP,
                    linkage: true,
                    priority: Layer.PRIORITY.POPUP,
                    params: {
                    }
                },
                visible: true
            });
        }
        function onShowDeviceSoundSettingPopup(result) {
            log.printDbg("onShowDeviceSoundSettingPopup()___"+result);

            var listenAloneMode = result;

            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_BLUETOOTH_SOUND_SETTING_POPUP,
                remove: true
            });

            bluetoothDevice.setListenAloneMode(listenAloneMode);
        }

        /**
         * [jh.lee] showDeletePopup 응답
         */
        function onShowDeletePopup() {
            log.printDbg("onShowDeletePopup()");
            /// INSTANCE.div.find(".list_item").eq(listFocus).toggleClass("checked");
            for(var i = 0; i < list.length; i++){
                if (INSTANCE.div.find(".list_item").eq(i).hasClass("checked")) {
                    INSTANCE.div.find(".list_item").eq(i).removeClass("checked");
                    bluetoothDevice.removeDevice(list[i]);
                }
            }
            refreshListArea();
            KTW.managers.service.SimpleMessageManager.showMessageTextOnly("삭제되었습니다");
        }

        /**
         * sw.nam 오디오 장치 연결 이력을 관리하기 위해 존재하는 함수 같은데 web 3.0 에서는 현재 사용하지 않음.. 혹시 몰라 우선 로직은 남겨둠
         * @param is_add
         * @param name
         */
        function saveBluetoothDevice(is_add, name) {
            log.printDbg("saveBluetoothDevice()");
            var devices;
            var tmp;
            var arr;

            devices = JSON.parse(storageManager.ps.load(storageManager.KEY.BLUETOOTH_DEVICE));

            if (is_add) {
                if (devices) {
                    tmp = false;
                    for(var i = 0; i < devices.length; i++) {
                        if (devices[i] === name) {
                            tmp = true;
                        }
                    }
                    if (!tmp) {
                        devices.push(name);
                    }
                } else {
                    devices = [name];
                }
                storageManager.ps.save(storageManager.KEY.BLUETOOTH_DEVICE, JSON.stringify(devices));
            } else {
                arr = new Array();

                if (devices) {
                    for(var i = 0; i < devices.length; i++) {
                        if (devices[i] !== name) {
                            arr[arr.length] = devices[i];
                        }
                    }
                }
                storageManager.ps.save(storageManager.KEY.BLUETOOTH_DEVICE, JSON.stringify(arr));
            }
        }

        function createElement() {
            log.printDbg("createElement()");

            subArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "sub_area",
                    css:{
                        visibility: "hidden"
                    }
                },
                parent: INSTANCE.div.find("#contents")
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_option_related.png",
                    css: {
                        position: "absolute", left: 1577, top: 986
                    }
                },
                parent: subArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "sub_areaText",
                    css: {
                        position: "absolute", left: 1623, top: 987, "font-family": "RixHead L", "font-size": 26, color: "rgba(255, 255, 255, 0.3)",
                        "letter-spacing": -1.3
                    }
                },
                text: "",
                parent: subArea
            });
        }

        function saveConfiguration() {
            log.printDbg("saveConfiguration()");
            if (useBluetooth) {
                bluetoothDevice.enabled = true;
            }
            else {
                bluetoothDevice.enabled = false;
            }
        }

        function setSubArea() {
            log.printDbg("setSubArea()");
            if(focusMode !== MODE.LIST){
                log.printDbg("Focusing is not on list area.. return");
                return ;
            }
            if( !list[listFocus].connected && ( list[listFocus].type == BluetoothDevice.TYPE_HEADPHONE || list[listFocus].type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) ) {
                subArea.css({visibility: "inherit"});
                subArea.find(".sub_areaText").text("오디오 기기 연결");
            } else if(list[listFocus].connected && (list[listFocus].type == BluetoothDevice.TYPE_HEADPHONE || list[listFocus].type == BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN) )  {
                subArea.css({visibility: "inherit"});
                subArea.find(".sub_areaText").text("음성 듣기 옵션");
            } else {
                subArea.css({visibility: "hidden"});
            }
        }

        function startSmallLoading() {
            log.printDbg("startSmallLoading()");
            isFairing = true;

            INSTANCE.div.find(".list_item").eq(listFocus).find(".list_state").css({visibility: "hidden"});
            INSTANCE.div.find(".list_item").eq(listFocus).find(".list_loading").css({visibility: "inherit"});
            INSTANCE.div.find(".list_item").eq(listFocus).find(".list_loading .list_loadingWatch").addClass("loading");
            /* 2017.04.30 [sw.nam]
             블루투스 장치 연결과 관련된 interrupt 는 listenter인 onChangeDeviceConnect() 에서 처리되나,
             audioToggle() 을 했을 때 fail 이 되면서 listener 로부터 응답을 받지 못하는 경우가 생긴다.
             UI에서 정확한 audio 장치를 구분하지 못해서인지, 아니면 bluetooth 인터페이스의 문제인지 확인해봐야함.
             우선 loading 시작과 동시에 최대 1분 30초간 timeout을 두어 응답이 없는 경우 멈추고 연결 실패 팝업을 띄우도록 수정함
             ,문제가 된다면 로직 확인하여 정확한 원인 찾아낼 것
             */
            smallLoadingTimer = setTimeout(onSmallLoadingTimer,90000);

        }
        function stopSmallLoading() {
            log.printDbg("stopSmallLoading()");
            if(isFairing) {
                INSTANCE.div.find(".list_item").find(".list_state").css({visibility: "inherit"});
                INSTANCE.div.find(".list_item").find(".list_loading").css({visibility: "hidden"});
            }
            INSTANCE.div.find(".list_item").find(".list_loading .list_loadingWatch").removeClass("loading");
            isFairing = false;
            clearSmallLoadingTimer();
        }

        function onSmallLoadingTimer() {
            log.printDbg("onSmallLoadingTimer()");
            clearSmallLoadingTimer();
            stopSmallLoading();
            log.printDbg("focus mode = "+MODE.BUTTON+" btnFocus = "+btnFocus);
            showFairedDeviceConnnectionFailPopup();
        }
        function clearSmallLoadingTimer() {
            log.printDbg("clearSmallLoadingTimer()");
            if(smallLoadingTimer){
                clearTimeout(smallLoadingTimer);
                smallLoadingTimer = null;
            }
        }
    };

    KTW.ui.layer.setting.setting_bluetoothConnect.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_bluetoothConnect.constructor = KTW.ui.layer.setting.setting_bluetoothConnect;

    KTW.ui.layer.setting.setting_bluetoothConnect.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_bluetoothConnect.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();