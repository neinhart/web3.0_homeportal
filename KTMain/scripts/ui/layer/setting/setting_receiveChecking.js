/**
 * Created by Yun on 2017-01-25.
 *
 * 설정 > 시스템설정 > 수신 품질 측정 (OTS only)
 */


(function () {
    KTW.ui.layer.setting.setting_receiveChecking  = function ReceiveChecking(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;

        var log = KTW.utils.Log;
        var DEF = KTW.oipf.Def;
        var util = KTW.utils.util;

        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var stateManager = KTW.managers.service.StateManager;
        //var vodAdapter = KTW.oipf.AdapterHandler.vodAdapter;
        var networkManager = KTW.managers.device.NetworkManager;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;

        var layerIdx;

        var clock;
        var clockArea;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame receiveChecking"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>수신품질 측정</span>" +

                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
          //  createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            log.printDbg("show receiveChecking");

            if (!options || !options.resume) {
                log.printDbg("new");
                $("head").append(styleSheet);
                Layer.prototype.show.call(this);
                focusIdx = 0;
                INSTANCE.div.find("#contents #receiveChecking_area").removeClass("show");
                buttonFocusRefresh(focusIdx, 1);

                var data = this.getParams();
                log.printDbg("thisMenuName:::::::::::"+data.name);
                if(data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.COLLECT_SIGNAL) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if(language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    }else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
                layerIdx = data.layerIdx;


            } else {
                log.printDbg("resume");
            }

         //   clock.show(clockArea);
        };

        this.hide = function () {
            log.printDbg("hide()");
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    // 왼쪽 메뉴로 포커스 이동시 사용
                    // if (menuIdx == 1) {
                    //     focusIdx = 0;
                    //     buttonFocusRefresh(focusIdx, 0);
                    // }
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.RIGHT:
                    // 오른쪽 메뉴로 포커스 이동시 사용
                    // if (menuIdx == 0) {
                    //     focusIdx = 0;
                    //     buttonFocusRefresh(focusIdx, 1);
                    // }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (index == 0) {
                        // INSTANCE.div.find("#contents #receiveChecking_area").addClass("show"); // show 가 붙어야 화면에 노출 보임
                        // [jh.lee] 채널 또는 VOD 정지
                        if (stateManager.isTVViewingState() === true) {
                            // [jh.lee] TV 인 경우 시청중인 채널 종료

                            // 2017.08.29 [sw.nam] 	WEBIIIHOME-3219
                            // 미가입 채널인 경우 stopChannel() 이 호출 될 때 mmi event 가 올라오는데
                            // 이때 lastOnlySelect 값이 false 이기때문에 clearNormalLayer 가 호출 되면서 설정 layer 가 닫히는 현상이 발생함.
                            // 이를 방지하기 위해 stop channel 호출 전에 강제로 mainChannelControl.resume 을 호출하여 lastOnlySelect 값을 true 로 set 해준다.
                            var mainChannelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                            mainChannelControl.resume();
                            navAdapter.stopChannel();
                        } else if (stateManager.isVODPlayingState() === true) {
                            // [jh.lee] VOD 상태 인 경우 시청중인 VOD 종료
                            //vodAdapter.stopPlayer(KTW.vod.Def.EXIT_TYPE.EXIT_FORCE);
                            // 2017.04.13 dhlee
                            // VOD 시청중일 때 VOD Module을 찾아서 stopVODPlaying API를 호출하도록 해야 한다.
                            //vodAdapter.stopPlayer(KTW.vod.Def.EXIT_TYPE.EXIT_FORCE);
                            var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                            if (vodModule) {
                                // 2017.04.13 dhlee result는 향후를 위해 일단 전달만 받아둔다.
                                var result = vodModule.execute({
                                    method: "stopVodWithEndPopup",
                                    params: {
                                        withPopup: false,
                                        notChangeService: true,
                                        channelObj: null
                                    }
                                });
                            }
                        }

                        KTW.managers.service.IframeManager.setType(KTW.managers.service.IframeManager.DEF.TYPE.NONE);

                        showCollectSignalDialog();

                    }else {
                        LayerManager.historyBack();
                    }
                    break;
            }
        }

        /**
         * [jh.lee] 품질 측정 팝업 생성
         */
        function showCollectSignalDialog() {
            log.printDbg("showCollectSignalDialog()");

            // [jh.lee] 신호 미약 팝업 생성을 막음
            networkManager.enableWeakSignal(false);

            //2017.08.28 sw.nam 팝업 생성 시 홈포탈 키 이외의 모든 키를 막기 위해 system_popup 레벨로 priority 변경 (2.0 형상 동일)
            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_RECEIVE_CHECKING_POPUP,
                    type: Layer.TYPE.POPUP,
                    linkage: true,
                    priority: Layer.PRIORITY.SYSTEM_POPUP,
                    params: {
                        complete: function(result) {

                            onResponsePopup(result);
                        }
                    }
                },
                visible: true
            });

        }

        function onResponsePopup(result) {

            log.printDbg("onResponsePopup()");

            var result_string = "";
            var present;
            var current_date;
            var zip_code;
            var zip_value;
            var result_data_length;
            var channel;


            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_RECEIVE_CHECKING_POPUP,
                remove: true
            });
            // [jh.lee] 모든 처리가 완료된 후 신호미약 팝업 생성 가능하도록 설정하기 위해 리스너 등록
            navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).addChannelEventListener(onChannelEvent);

            try {

                if (result) {

                    INSTANCE.div.find("#contents #receiveChecking_area").addClass("show"); // show 가 붙어야 화면에 노출 보임

                    current_date = util.getCurrentDateTime();

                    zip_code = basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_ZIP_CODE);
                    zip_code = zip_code.substring(zip_code.length - 2);
                    zip_value = parseInt("0x" + zip_code);

                    result_string = zip_value + "&" + basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_MANUFACTURER) + "/"
                    + basicAdapter.getConfigText(DEF.CONFIG.KEY.SIGNAL_MODEL_NUMBER) + "/"
                    + basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_HARDWARE) + "&"
                    + KTW.SMARTCARD_ID + "&" + basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_FIRMWARE)
                    + "&" + current_date + "&";
                    log.printDbg("6()");
                    // [jh.lee] 얻어온  result_data length
                    result_data_length = result.length;

                    for (var i = 0; i < result_data_length; i++) {
                        //119&0xa0/0x23/0x01&6157557684&0.1.4.2.2012.12.05&20130206152643&9,-48,10/10,-51,15/1,-51,15/1,
                        //공통표기내용 : 스마트카드번호, STB ID, 스마트카드 ID, ZIP코드, S/W 버전, 수집날짜 및 시간
                        result_string += result[i].transportStreamId + "," + result[i].signalLevel + "," + result[i].snr;

                        if (i < result_data_length - 1) {
                            result_string += "/";
                        }

                        if (result[i].name === "BS4") {
                            present = result[i];
                        }
                    }

                    makeQRcodeScreen(result_string, current_date, present);

                    channel = navAdapter.getCurrentChannel();

                    if (stateManager.isVODPlayingState() === true) {
                        log.printDbg("isVODPlayingState true!");
                        // [jh.lee] VOD 재생중에 수신품질 측정한 경우 VOD는 종료되지만 state 정리가 안됨
                        // [jh.lee] 그래서 수신품질 측정 완료된 시점에 state는 VOD 그래서 다른방식으로 채널튠해줘야함
                        //stateManager.exitToService({ "channel" : channel, "tune" : "true" });
                        // 2017.04.17 dhlee
                        // TODO 여기서 어떻게 처리해야 할까?
                        // 2017.04.19 sw.nam
                        // TODO VOD의 경우 change service 를 하면서 현재 채널로 tune 되게 되어있는데, 그래서 설정 메뉴 화면이 hide 되는 이슈가 발생.
                        KTW.managers.service.AppServiceManager.changeService({nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING });
                    } else {
                        // [jh.lee] 일반적으로 채널상태에서 수신품질 측정한 경우
                        // [jh.lee] 채널 재생
                        navAdapter.changeChannel(channel, true, true);
                    }
                }
            } catch (error) {
                log.printDbg(error.message);
                channel = navAdapter.getCurrentChannel();
                // [jh.lee] 채널 재생
                navAdapter.changeChannel(channel, true);
            }
        }

        function onChannelEvent() {
            navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN).removeChannelEventListener(onChannelEvent);
            networkManager.enableWeakSignal(true);
        }

        /*
         * *
         * [jh.lee] 품질측정 성공시 화면에 QR코드 화면 생성
         * */

        function makeQRcodeScreen(result_string, current_date, present) {
            log.printDbg("makeQRcodeScrean=== result_string , current_data, present ===="+result_string+  "    " + current_date+ "    " +present);
            var index = 0;


            //QRCode
            INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area #receiveChecking_QRArea").html("");
            INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area #receiveChecking_QRArea").qrcode({width: 325, height: 325, text: result_string });

            // s/c
            if(KTW.SMARTCARD_ID.length > 20) {

                INSTANCE.div.find("#contents #settingTable  #receiveChecking_area .receiveChecking_text").eq(0).text( "S/C(" + KTW.SMARTCARD_ID.substring(0, 20) + KTW.SMARTCARD_ID.substring(20)+ ")");
            } else
                INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area .receiveChecking_text").eq(0).text(  "S/C(" + KTW.SMARTCARD_ID + ")");

            INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area .receiveChecking_text").eq(1).text( "중계기(BS4)");
            INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area .receiveChecking_text").eq(2).text("수집날짜(" + current_date + ")");
            INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area .receiveChecking_text").eq(3).text("신호세기(" + present.signalLevel + "dBm)");
            INSTANCE.div.find("#contents #settingTable .table_item_div #receiveChecking_area .receiveChecking_text").eq(4).text("신호품질 S/N(" + present.snr + "dB)");
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>수신품질 측정</div>" +
            "<div class='table_subTitle'>QR코드 생성</div>" +
            "<div id='receiveChecking_area'>" +
            "<div id='receiveChecking_QRImg'>" +
            "<div id='receiveChecking_QRArea' style='position: absolute; margin-left: 25px; margin-top: 25px; width: 325px; height: 325px;'>" + "</div>"+
            "</div>" +
            "<div id='receiveChecking_textArea'>" +
            "<div class='receiveChecking_text'>S/C()</div>" +
            "<div class='receiveChecking_text'>중계기()</div>" +
            "<div class='receiveChecking_text'>수집날짜()</div>" +
            "<div class='receiveChecking_text'>신호세기()</div>" +
            "<div class='receiveChecking_text'>신호품질 S/N()</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>품질 측정 버튼을 선택하면<br>QR코드가 생성됩니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>품질 측정</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }
        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }
    };

    KTW.ui.layer.setting.setting_receiveChecking.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_receiveChecking.constructor = KTW.ui.layer.setting.setting_receiveChecking;

    KTW.ui.layer.setting.setting_receiveChecking.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_receiveChecking.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();