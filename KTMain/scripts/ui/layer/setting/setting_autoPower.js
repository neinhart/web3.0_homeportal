/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 자동 전원
 * 2.0 UI와 3.0 UI는 차이점이 존재한다.
 * 2.0 에서는 자동 전원 설정의 설정/해제 선택이 없다.
 * 해제인 경우 켜짐/꺼짐 시각을 00:00, 00:00 으로 변경해서 저장한다.
 * local storage에 저장된 값의 형식읜 repeat("true"/"false");start time("0000");end time;실행여부[executed]("false")
 * 실행여부에 대한 값은 PowerModeManager에서 사용하는 값으로 자동 전원 옵션을 저장하게 되면 무조건 "false" 로 set 한다.
 * 2.0 은 예전에 저전력 설정 기능이 있었던 것으로 보이는데 3.0 에는 해당 기능이 제공되지 않는다.
 *
 * PowerModeManager#setAutoPowerMode() 를 부면 데이터가 마치 5개인양 동작하는데 무슨 의미일까나...???
 */

(function () {
    KTW.ui.layer.setting.setting_autoPower  = function AutoPower(options) {
        KTW.ui.Layer.call(this, options);
        var INSTANCE = this;
        var styleSheet;
        var util = KTW.utils.util;
        var focusYIdx = 0;
        var lastFocusedYIdx = 0;
        var focusIdx = 0;
        var focusXIdx = 0;
        var menuIdx = 0;

        var lastFocusedMenuIdx = 0;
        var lastFocusedIdx;

        var KEY_CODE = KTW.KEY_CODE;
        var log = KTW.utils.Log;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var storageManager = KTW.managers.StorageManager;

        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;

        var tmpTimeValue = ["-", "-", "-", "-"];


        var isON = false;
        var isOFF = false;
        var repeatIndex = 0;
        var REPEAT = {
            ONCE: 0,
            EVERYDAY: 1,
            SELECTIVE: 2
            };

        // 요일 체크 Array (0 : 월요일 ~ 6: 일요일)
        var dayArray = [7];

        var startHH = "00";
        var startMM = "00";
        var endHH = "00";
        var endMM = "00";


        var data;

        var clockArea;
        var clock;

        var settingViewArea;
        var setTimeArea;
        var setRepeatArea;

        var saveBtnBlockFlag = false;


        var MENUTYPE = {
            AUTO_ON : 0,
            AUTO_OFF: 1,
            REPEAT_PERIOD : 2,
            DAY_CHECK: 3,
            RIGHT_MENU: 4
        };

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame autoPower"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>자동 전원</span>" +
                "</div>"+
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
       //     createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            readConfiguration();

            log.printDbg("show AutoPower");
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_AUTO_POWER) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }
                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
         //   clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide AutoPower");
         //   clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    if(menuIdx == MENUTYPE.AUTO_ON) {
                        // 켜짐, 꺼짐 시각 모두 체크가 되어 있지 않은 경우 반복 설정 영역으로 갈 수 없음
                        if(!INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").hasClass("checked") &&
                            !INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").hasClass("checked")) {
                            //  그렇다면 다시 꺼짐 시각 체크박스 영역으로 이동
                            menuIdx = MENUTYPE.AUTO_OFF;
                            focusIdx = 0;
                        }else {
                            // 반복 설정 영역으로 이동
                            menuIdx = MENUTYPE.REPEAT_PERIOD;
                            focusIdx = 0; // 현재 설정된 셋 값으로 가야해
                        }
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.AUTO_OFF) {
                        menuIdx = MENUTYPE.AUTO_ON;
                        focusIdx = 0;
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.REPEAT_PERIOD) {
                        menuIdx = MENUTYPE.AUTO_OFF;
                        focusIdx = 0;
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.DAY_CHECK) {
                        menuIdx = MENUTYPE.REPEAT_PERIOD;
                        focusIdx = 2;
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.RIGHT_MENU) {
                        if(!saveBtnBlockFlag) {
                            focusIdx --;
                            if(focusIdx < 0) {
                                focusIdx = 1;
                            }
                        }
                        setFocus(focusIdx,menuIdx);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if(menuIdx == MENUTYPE.AUTO_ON) {
                        // 자동 꺼짐으로 이동
                        menuIdx = MENUTYPE.AUTO_OFF;
                        focusIdx = 0; // 현재 설정된 셋 값으로 가야해
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.AUTO_OFF) {
                        // 켜짐, 꺼짐 시각 모두 체크가 되어 있지 않은 경우 반복 설정 영역으로 갈 수 없음
                        if(!INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").hasClass("checked") &&
                            !INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").hasClass("checked")) {
                            //  그렇다면 다시 꺼짐 시각 체크박스 영역으로 이동
                            menuIdx = MENUTYPE.AUTO_ON;
                            focusIdx = 0;
                        }else {
                            menuIdx = MENUTYPE.REPEAT_PERIOD;
                            focusIdx = repeatIndex; // 현재 설정된 셋 값으로 가자
                        }
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.REPEAT_PERIOD) {
                        // 요일 선택에 포커스가 가 있다면 요일로 갈 수 있어야 한다.
                        if(repeatIndex == REPEAT.SELECTIVE) {
                            menuIdx = MENUTYPE.DAY_CHECK;
                            focusIdx = 0;
                        }else {
                            menuIdx = MENUTYPE.AUTO_ON;
                            focusIdx = 0;
                        }
                        setFocus(focusIdx,menuIdx);
                    }else if(menuIdx == MENUTYPE.RIGHT_MENU) {
                        if(!saveBtnBlockFlag) {
                            focusIdx ++;
                            if(focusIdx > 1) {
                                focusIdx = 0;
                            }
                        }
                        setFocus(focusIdx,menuIdx);
                    }else if( menuIdx == MENUTYPE.DAY_CHECK) {
                        //자동 켜짐 시각 설정으로 이동
                        focusIdx = 0;
                        menuIdx = MENUTYPE.AUTO_ON;
                        setFocus(focusIdx,menuIdx) ;
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if(menuIdx == MENUTYPE.AUTO_ON || menuIdx == MENUTYPE.AUTO_OFF || menuIdx == MENUTYPE.REPEAT_PERIOD
                        || menuIdx == MENUTYPE.DAY_CHECK ) {
                        focusIdx --;
                        if(focusIdx < 0) {
                            LayerManager.historyBack();
                        }else {
                            setFocus(focusIdx,menuIdx);
                        }
                    }else if(menuIdx == MENUTYPE.RIGHT_MENU) {
                        // 임시 코딩
                        menuIdx = lastFocusedMenuIdx;
                        focusIdx = lastFocusedIdx;
                        setFocus(focusIdx,menuIdx);
                        break;
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    var limitIdx;

                    if(menuIdx == MENUTYPE.RIGHT_MENU) {
                        return true;
                    }
                    if( menuIdx == MENUTYPE.AUTO_ON) {
                        if(INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").hasClass("checked")) {
                            limitIdx = 2;
                        }else {
                            limitIdx = 0;
                        }
                    }else if(menuIdx == MENUTYPE.AUTO_OFF) {
                        if(INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").hasClass("checked")) {
                            limitIdx = 2;
                        }else {
                            limitIdx = 0;
                        }
                    }else if(menuIdx == MENUTYPE.REPEAT_PERIOD) {
                        limitIdx = 2;
                    }else if(menuIdx == MENUTYPE.DAY_CHECK) {
                        limitIdx = 6;
                    }
                    focusIdx++;
                    if(focusIdx > limitIdx ) {

                        lastFocusedMenuIdx = menuIdx;
                        lastFocusedIdx = focusIdx -1;
                        menuIdx = MENUTYPE.RIGHT_MENU;
                        if(saveBtnBlockFlag) {
                            // 저장 버튼이 blocked 처리 되어있는 경우 취소 영역으로 이동하게 한다. 영역으로 가지 못한다.
                            focusIdx = 1;
                        }else {
                            focusIdx = 0;
                        }

                    }
                    setFocus(focusIdx,menuIdx);
                    return true;
                case KEY_CODE.ENTER:
                    enterKeyEvent(focusIdx,menuIdx);
                    return true;
                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    if(menuIdx == MENUTYPE.AUTO_ON || menuIdx == MENUTYPE.AUTO_OFF) {
                        if(focusIdx > 0) {
                            inputTime(key_code - KEY_CODE.NUM_0)
                        }
                    }
                    return true;
                default:
                    return false;
            }
            log.printDbg("focusXidx ====  "+focusXIdx);
            log.printDbg("focusYidx ====  "+focusYIdx);
            log.printDbg("menuIdx   ====  "+menuIdx);
        };

        function inputTime(timeValue) {
            if (menuIdx == MENUTYPE.AUTO_ON) { // 자동 켜짐 시각
                if (focusIdx == 1) { // 시각

                    INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus span").removeClass("focus");
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .dot").addClass("focus");
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .hour").addClass("focus");

                   if (tmpTimeValue[0] == "-") {
                        if (timeValue >= 0 && timeValue <= 2) {
                            tmpTimeValue[0] = timeValue;
                            tmpTimeValue[1] = "-";
                        } else {
                            tmpTimeValue[0] = "0";
                            tmpTimeValue[1] = timeValue;
                        }
                    } else {
                        if (tmpTimeValue[0] == "2") {
                            if (timeValue >= 0 && timeValue <= 4) {
                                tmpTimeValue[1] = timeValue;
                            }
                        } else {
                            tmpTimeValue[1] = timeValue;
                        }
                        if(tmpTimeValue[0] == "2" && tmpTimeValue[1] == "4") {
                            tmpTimeValue[0] = "0";
                            tmpTimeValue[1] = "0";
                        }
                    }
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .hour").text(tmpTimeValue[0].toString() + (tmpTimeValue[1] == "-" ? "0" : tmpTimeValue[1].toString())/* + ":"*/);
                    if (tmpTimeValue[0] != "-" && tmpTimeValue[1] != "-") {
                        focusIdx = 2;
                        INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .hour").removeClass("focus");
                        INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .min").addClass("focus");
                    }
                } else {
                    if (tmpTimeValue[2] == "-") {
                        if (timeValue >= 0 && timeValue <= 5) {
                            tmpTimeValue[2] = timeValue;
                            tmpTimeValue[3] = "-";
                        } else {
                            tmpTimeValue[2] = "0";
                            tmpTimeValue[3] = timeValue;
                        }
                    } else {
                        tmpTimeValue[3] = timeValue;
                    }
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .min").text(tmpTimeValue[2].toString() + (tmpTimeValue[3] == "-" ? "0":tmpTimeValue[3].toString()));
                }
                if (tmpTimeValue[2] != "-" && tmpTimeValue[3] != "-") {
                    tmpTimeValue = ["-", "-", "-", "-"];

                    focusIdx = 0;
                    menuIdx = MENUTYPE.AUTO_OFF;
                    setFocus(focusIdx,menuIdx);
                }
            } else {
                // 자동 꺼짐 시각
                if (focusIdx == 1) {
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus span").removeClass("focus");
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .dot").addClass("focus");
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .hour").addClass("focus");

                    if (tmpTimeValue[0] == "-") {
                        if (timeValue >= 0 && timeValue <= 2) {
                            tmpTimeValue[0] = timeValue;
                            tmpTimeValue[1] = "-";
                        } else {
                            tmpTimeValue[0] = "0";
                            tmpTimeValue[1] = timeValue;
                        }
                    } else {
                        if (tmpTimeValue[0] == "2") {
                            if (timeValue >= 0 && timeValue <= 4) {
                                tmpTimeValue[1] = timeValue;
                            }
                        } else {
                            tmpTimeValue[1] = timeValue;
                        }
                        if(tmpTimeValue[0] == "2" && tmpTimeValue[1] == "4") {
                            tmpTimeValue[0] = "0";
                            tmpTimeValue[1] = "0";
                        }
                    }
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .hour").text(tmpTimeValue[0].toString() + (tmpTimeValue[1] == "-" ? "0" : tmpTimeValue[1].toString())/* + ":"*/);
                    if (tmpTimeValue[0] != "-" && tmpTimeValue[1] != "-") {
                        focusIdx = 2;
                        INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .hour").removeClass("focus");
                        INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .min").addClass("focus");
                    }
                } else {
                    if (tmpTimeValue[2] == "-") {
                        if (timeValue >= 0 && timeValue <= 5) {
                            tmpTimeValue[2] = timeValue;
                            tmpTimeValue[3] = "-";
                        } else {
                            tmpTimeValue[2] = "0";
                            tmpTimeValue[3] = timeValue;
                        }
                    } else {
                        tmpTimeValue[3] = timeValue;
                    }
                    INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .min").text(tmpTimeValue[2].toString() + (tmpTimeValue[3] == "-" ? "0":tmpTimeValue[3].toString()));
                }
                if (tmpTimeValue[2] != "-" && tmpTimeValue[3] != "-") {
                    focusIdx = 0; // 설정된 반복 idx
                    menuIdx = MENUTYPE.REPEAT_PERIOD;
                    setFocus(focusIdx,menuIdx);
                }
            }
        }

        /**
         * sw.nam
         * 어떤 영역에 포커스를 줄지 결정하는 함수 (키이벤트에 반응)
         */
        function setFocus(idx,menuType) {
            log.printDbg("setFocus()");

            // 먼저 현재 포커스 되어있는 영역들 다 해제
            INSTANCE.div.find("#contents .setTimeArea .checkBox").removeClass("focus");
            INSTANCE.div.find("#contents .setTimeArea .inputArea").removeClass("focus");
            INSTANCE.div.find("#contents .setTimeArea .inputArea span").removeClass("focus");
            INSTANCE.div.find("#contents .repeatArea .radioBtnBox").removeClass("focus");
            INSTANCE.div.find("#contents .repeatArea .dayBox").removeClass("focus");

            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div ").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");

            INSTANCE.div.find("#contents .repeatArea .dayArea").removeClass("hide dimmed");

            //요일선택 radio box 가 선택되어있지 않다면 hide
            if(!INSTANCE.div.find("#contents .repeatArea .radioBtnBox:eq(2)").hasClass("checked")) {
                INSTANCE.div.find("#contents .repeatArea .dayArea").addClass("hide");
            }

            // 메뉴 타입 별로 상황을 나눈다.
            switch(menuType) {
                case MENUTYPE.AUTO_ON:
                    if(idx == 0) {
                        INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").addClass("focus");
                    }else {
                        INSTANCE.div.find("#contents .setTimeArea .inputArea.ON").addClass("focus");
                        //시각 세부 설정 영역.
                        if(idx == 1) {
                            // hour
                            INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .hour").addClass("focus");
                        }else {
                            // min
                            INSTANCE.div.find("#contents .setTimeArea .inputArea.ON.focus .min").addClass("focus");
                        }
                        tmpTimeValue = ["-", "-", "-", "-"];
                    }
                    break;
                case MENUTYPE.AUTO_OFF:
                    if(idx == 0) {
                        INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").addClass("focus");
                    }else {
                        INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF").addClass("focus");
                        //시각 세부 설정 영역.
                        if(idx == 1) {
                            // hour
                            INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .hour").addClass("focus");
                        }else {
                            // min
                            INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF.focus .min").addClass("focus");
                        }
                        tmpTimeValue = ["-", "-", "-", "-"];
                    }
                    break;
                case MENUTYPE.REPEAT_PERIOD:
                    INSTANCE.div.find("#contents .repeatArea .radioBtnBox:eq("+idx+")").addClass("focus");

                    if(!INSTANCE.div.find("#contents .repeatArea .radioBtnBox:eq(2)").hasClass("checked")) {
                        // 요일 선택 버튼이 선택되어져 있는 경우가 아니면 opcity setting 을 해주어야 한다.
                        // 요일영역 opacity 해제
                        //INSTANCE.div.find("#contents .repeatArea .dayArea").removeClass("hide dimmed");
                        if(idx == 2) {
                            INSTANCE.div.find("#contents .repeatArea .dayArea").removeClass("hide");
                            INSTANCE.div.find("#contents .repeatArea .dayArea").addClass("dimmed");
                        }
                    }
                    break;
                case MENUTYPE.DAY_CHECK:
                    log.printDbg("DAY_CHECK "+idx);
                    INSTANCE.div.find("#contents .repeatArea .dayBox:eq("+idx+")").addClass("focus");
                    break;
                case MENUTYPE.RIGHT_MENU:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + idx + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + idx+ ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + idx+ ") .rightMenu_btn_line").addClass("focus");
                    break;

            }
        }

        /**
         * 자동전원 설정 해제 인 경우 UI 초기화 시키는 함수
         */
        function resetConfigurationUI() {
            log.printDbg("resetConfigurationUI");

            // 포커스 영역 해제
            INSTANCE.div.find("#contents .setTimeArea .checkBox").removeClass("focus");
            INSTANCE.div.find("#contents .setTimeArea .inputArea").removeClass("focus");
            INSTANCE.div.find("#contents .setTimeArea .inputArea span").removeClass("focus");
            INSTANCE.div.find("#contents .repeatArea .radioBtnBox").removeClass("focus");
            INSTANCE.div.find("#contents .repeatArea .dayBox").removeClass("focus");

            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div ").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");


            // 시각 설정 영역
            INSTANCE.div.find("#contents .checkBox.ON").removeClass("checked");
            //시각영역 dim 처리 하고
            INSTANCE.div.find("#contents .inputArea.ON").addClass("dimmed");
            //시각 영역 숫자 --:-- 만든다
            INSTANCE.div.find("#contents .inputArea.ON .hour").text("--");
            INSTANCE.div.find("#contents .inputArea.ON .min").text("--");
            //체크되어있으면 체크 해제하고,
            INSTANCE.div.find("#contents .checkBox.OFF").removeClass("checked");
            //시각영역 dim 처리 하고
            INSTANCE.div.find("#contents .inputArea.OFF").addClass("dimmed");
            //시각 영역 숫자 --:-- 만든다
            INSTANCE.div.find("#contents .inputArea.OFF .hour").text("--");
            INSTANCE.div.find("#contents .inputArea.OFF .min").text("--");


            // 반복영역 dimmed 처리
            INSTANCE.div.find("#contents .repeatArea").css({opacity: 0.3});


            // 라디오 영역
            // 현재 체크된 모든 라디오 버튼 체크 해제
            INSTANCE.div.find("#contents .repeatArea .radioBtnBox").removeClass("checked");
            // 요일영역 opacity 및 체크상태
            INSTANCE.div.find("#contents .repeatArea .dayArea").removeClass("hide dimmed");
            INSTANCE.div.find("#contents .repeatArea .dayArea").addClass("hide");
            INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox").removeClass("checked");

            // 우측 버튼영역 저장 dimm 처리 되어있는거 있으면 해제
            saveBtnBlockFlag = false;
            INSTANCE.div.find("#contents .saveButton").removeClass("blocked");

            setFocus(0,0);
            isON = false;
            isOFF = false;
            repeatIndex = REPEAT.ONCE;
            INSTANCE.div.find("#contents .repeatArea .radioBtnBox:eq("+repeatIndex+")").addClass("checked");
        }

        /**
         * 메뉴 선택... 해야된다
         * @param idx
         * @param menuType
         */
        function enterKeyEvent(idx, menuType) {
            log.printDbg("enterKeyEvent()");

            focusIdx = idx;
            menuIdx = menuType;

            switch(menuType) {
                case MENUTYPE.AUTO_ON:
                    if(focusIdx == 0) {
                        // 체크박스 체크
                        // 박스가 체크되어있는지 먼저 검사
                        if(INSTANCE.div.find("#contents .checkBox.ON").hasClass("checked")) {
                            //체크되어있으면 체크 해제하고, .
                            INSTANCE.div.find("#contents .checkBox.ON").removeClass("checked");
                            //시각영역 dim 처리 하고
                            INSTANCE.div.find("#contents .inputArea.ON").addClass("dimmed");
                            //시각 영역 숫자 --:-- 만든다
                            INSTANCE.div.find("#contents .inputArea.ON .hour").text("--");
                            INSTANCE.div.find("#contents .inputArea.ON .min").text("--");
                            // 켜짐 flag false 로 바꾼다.
                            isON = false;

                        }else {
                            // 체크가 안되어있는경우, 체크한다.
                            INSTANCE.div.find("#contents .checkBox.ON").addClass("checked");
                            // dim 제거
                            INSTANCE.div.find("#contents .inputArea.ON").removeClass("dimmed");
                            //시각 영역 숫자 00:00 만든다
                            startHH = "00";
                            startMM = "00";
                            INSTANCE.div.find("#contents .inputArea.ON .hour").text(startHH);
                            INSTANCE.div.find("#contents .inputArea.ON .min").text(startMM);
                            // 우측 시각 입력 영역으로이동
                            focusIdx =1;
                            setFocus(focusIdx,menuIdx);
                            isON = true;
                        }

                        // 켜짐, 꺼짐 영역 둘다 없는지 체크 안되어있는지 검사.
                        // 켜짐, 꺼짐 시각 모두 체크가 되어 있지 않은 경우 반복 설정 영역으로 갈 수 없음
                        if(!INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").hasClass("checked") &&
                            !INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").hasClass("checked")) {
                            // 반복 설정 영역 dimmed 처리 한다.
                            INSTANCE.div.find("#contents .repeatArea").css({opacity: 0.3});
                            resetBlockedSaveButton();
                        }else {
                            INSTANCE.div.find("#contents .repeatArea").css({opacity: 1.0});
                        }

                    }else if(focusIdx == 1 || focusIdx == 2 ) {
                        // 꺼짐 체크박스로 이동
                        // 자동꺼짐 체크 박스 영역으로 이동
                        menuIdx = MENUTYPE.AUTO_OFF;
                        focusIdx = 0;
                        setFocus(focusIdx,menuIdx);
                    }
                    break;
                case MENUTYPE.AUTO_OFF:
                    if(focusIdx == 0) {
                        // 체크박스 체크
                        // 박스가 체크되어있는지 먼저 검사
                        if(INSTANCE.div.find("#contents .checkBox.OFF").hasClass("checked")) {
                            //체크되어있으면 체크 해제하고,
                            INSTANCE.div.find("#contents .checkBox.OFF").removeClass("checked");
                            //시각영역 dim 처리 하고
                            INSTANCE.div.find("#contents .inputArea.OFF").addClass("dimmed");
                            //시각 영역 숫자 --:-- 만든다
                            INSTANCE.div.find("#contents .inputArea.OFF .hour").text("--");
                            INSTANCE.div.find("#contents .inputArea.OFF .min").text("--");

                            //  OFF 설정 해제
                            isOFF = false;
                        }else {
                            // 체크가 안되어있는경우, 체크한다.
                            INSTANCE.div.find("#contents .checkBox.OFF").addClass("checked");
                            // dim 제거
                            INSTANCE.div.find("#contents .inputArea.OFF").removeClass("dimmed");
                            //시각 영역 숫자 --:-- 만든다
                            endHH = "00";
                            endMM = "00";
                            INSTANCE.div.find("#contents .inputArea.OFF .hour").text(endHH);
                            INSTANCE.div.find("#contents .inputArea.OFF .min").text(endMM);
                            // 우측 시각 입력 영역으로이동
                            focusIdx =1;
                            setFocus(focusIdx,menuIdx);

                            //OFF 설정
                            isOFF = true;
                        }
                        // 켜짐, 꺼짐 영역 둘다 없는지 체크 안되어있는지 검사.
                        // 켜짐, 꺼짐 시각 모두 체크가 되어 있지 않은 경우 반복 설정 영역으로 갈 수 없음
                        if(!INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").hasClass("checked") &&
                            !INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").hasClass("checked")) {
                            // 반복 설정 영역 dimmed 처리 한다.
                            INSTANCE.div.find("#contents .repeatArea").css({opacity: 0.3});
                            resetBlockedSaveButton();
                        }else {
                            INSTANCE.div.find("#contents .repeatArea").css({opacity: 1.0});
                        }
                    }else if(focusIdx == 1 || focusIdx == 2) {
                        // 반복 설정 체크박스로 이동
                        menuIdx = MENUTYPE.REPEAT_PERIOD;
                        focusIdx = 0;
                        setFocus(focusIdx,menuIdx);
                    }
                    break;
                case MENUTYPE.REPEAT_PERIOD:
                    // 현재 체크된 모든 라디오 버튼 체크 해제
                    INSTANCE.div.find("#contents .repeatArea .radioBtnBox").removeClass("checked");
                    // 요일영역 opacity 해제
                    INSTANCE.div.find("#contents .repeatArea .dayArea").removeClass("hide dimmed");

                    // 저장버튼 dim 해제
                    INSTANCE.div.find("#contents .saveButton").removeClass("blocked");

                    //현재 인덱스에 위치한 라디오 버튼 선택.
                    INSTANCE.div.find("#contents .repeatArea .radioBtnBox:eq("+focusIdx+")").addClass("checked");
                    // 요일 선택인경우 , 요일 체크 영역으로 포커스 이동, 그렇지 않은 경우 우측 버튼 영역으로 이동
                    repeatIndex = focusIdx;
                    log.printDbg("repeatIndex " +repeatIndex);

                    if(focusIdx == REPEAT.SELECTIVE) {
                        log.printDbg("요일선택");
                        menuIdx = MENUTYPE.DAY_CHECK;
                        focusIdx = 0;
                        setFocus(focusIdx,menuIdx);
                        // 체크된 박스가 하나도 없다면 저장버튼 block 처리
                        checkSaveBtnisBlocked();
                    }else {
                        //요일 영역 hide
                        INSTANCE.div.find("#contents .repeatArea .dayArea").addClass("hide");
                        // 우측 메뉴로 이동
                        lastFocusedMenuIdx = menuIdx;
                        menuIdx = MENUTYPE.RIGHT_MENU;
                        lastFocusedIdx = focusIdx;
                        focusIdx = 0;
                        setFocus(focusIdx,menuIdx);
                        resetBlockedSaveButton();
                    }
                    break;
                case MENUTYPE.DAY_CHECK:
                    //현재 인덱스의 요일이 체크되었는지 확인
                    if(INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox:eq("+focusIdx+")").hasClass("checked")) {
                        // 체크되어있으면 체크 해제
                        INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox:eq("+focusIdx+")").removeClass("checked");
                        dayArray[focusIdx] = false;
                        checkSaveBtnisBlocked();
                    }else {
                        //체크 안되있으면 체크
                        INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox:eq("+focusIdx+")").addClass("checked");
                        dayArray[focusIdx] = true;

                        saveBtnBlockFlag = false;
                        INSTANCE.div.find("#contents .saveButton").removeClass("blocked");
                    }
                    break;
                case MENUTYPE.RIGHT_MENU:
                    if(focusIdx == 0 ) {
                        //TODO 설정 값 저장
                        if(KTW.CONSTANT.IS_UHD){
                            if (checkLowPower() === "1") {
                                // [sw.nam] 저전력 설정 되어 있는지 확인
                                basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.POWER_STATUS, 0);
                            }
                        }
                        startHH = INSTANCE.div.find("#contents .setTimeArea .inputArea.ON .hour").text();
                        startMM = INSTANCE.div.find("#contents .setTimeArea .inputArea.ON .min").text();
                        endHH = INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF .hour").text();
                        endMM = INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF .min").text();

                       // saveConfiguration(isRepeat,fromHH+fromMM,toHH+toMM);
                        var startTime = startHH + startMM;
                        var endTime = endHH + endMM;
                        saveConfiguration(repeatIndex,startTime,endTime,dayArray);

                        data.complete();
                        settingMenuManager.historyBackAndPopup();

                        return true;
                    }else {
                        LayerManager.historyBack();
                    }
                    break;
            }
        }

        /**
         * 저장버튼 block 여부 처리하는 함수
         * 요일 선택 인 경우 요일체크가 하나도 안되어 있으면 저장 못함
         */
        function checkSaveBtnisBlocked() {
            log.printDbg("checkSaveBtnBlockFlag");
            saveBtnBlockFlag = true;
            for(var i = 0; i < 7 ; i++) {
                if(dayArray[i] == true) {
                    saveBtnBlockFlag = false;
                }
            }
            if(saveBtnBlockFlag) {
                INSTANCE.div.find("#contents .saveButton").addClass("blocked");
            }else {
                INSTANCE.div.find("#contents .saveButton").removeClass("blocked");
            }
        }

        function resetBlockedSaveButton() {
            log.printDbg("resetBlockedSaveButton");
            saveBtnBlockFlag = false;
            INSTANCE.div.find("#contents .saveButton").removeClass("blocked");
        }


        function createView() {
            log.printDbg("createView");

            //1. 영역잡기
            settingViewArea = util.makeElement({
                tag: "<div />",
                attrs: {
                     class: "settingViewArea",
                     css: {
                         position: "absolute", left:123, top: 123, width: 1247, height: 835,
                         "border-top": "5px solid rgba(178, 177, 177, 0.25)", "border-bottom": "5px solid rgba(178, 177, 177, 0.25)",
                         "box-sizing" : "border-box"
                     }
                },
                parent: INSTANCE.div.find("#contents")
            });

            //제목
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "title",
                    css: {
                        position: "absolute", left: 0, top: 46, "font-size" : 48, "font-family" : "RixHead M",
                        color: "white", opacity: 0.7, "letter-spacing" : -2.4
                    }
                },
                text: "자동 전원",
                parent: settingViewArea
            });

            //소제목
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "subTitle",
                    css: {
                        position: "absolute", left: 0, top: 115, "font-size" : 26, "font-family" : "RixHead L",
                        color: "white", opacity: 0.4, "letter-spacing" : -1.3
                    }
                },
                text: "자동 전원 기능을 사용하시려면 체크 박스를 선택 후 원하는 시각을 직접 입력해 주세요 예) 22:10",
                parent: settingViewArea
            });

            // 시각 입력 영역
            setTimeArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "setTimeArea",
                    css: {
                        position: "absolute", left: 0, top: 200, width: 634, height: 151
                    }
                },
                parent: settingViewArea
            });

            // 켜짐 체크박스
            var checkBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "checkBox ON",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 285, height: 65
                    }
                },
                parent: setTimeArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "checkBox_Bg",
                    css: {
                        position: "absolute", left:0, top:0, width: 285, height: 65, "border" : "solid 5px rgb(210,51,47)", "box-sizing" : "border-box"
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_dim_uncheck_img",
                    src: "images/pop_cb_dim_uncheck.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_dim_check_img",
                    src: "images/pop_cb_dim_check.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_foc_uncheck_img",
                    src: "images/pop_cb_foc_uncheck.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_foc_check_img",
                    src: "images/pop_cb_foc_check_red.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "checkBox_text",
                    css: {
                        position: "absolute", left: 75, top: 20, width: 160, height: 30, color : "white"
                    }
                },
                text: "자동 켜짐 시각",
                parent: checkBox
            });

            // 켜짐 시각 입력 영역
            var inputArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "inputArea ON",
                    css: {
                        position: "absolute", left: 294, top: 0, width: 340, height: 65
                    }
                },
                parent: setTimeArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "inputArea_normal_img",
                    src: "images/setting/set_input_w330.png",
                    css: {
                        position: "absolute", left:0, top:0, width: 340, height: 65
                    }
                },
                parent: inputArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "inputArea_dim_img",
                    src: "images/setting/set_input_w330_d.png",
                    css: {
                        position: "absolute", left:0, top:0, width: 340, height: 65
                    }
                },
                parent: inputArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "inputArea_focus_img",
                    src: "images/pop_pw_box_foc.png",
                    css: {
                        position: "absolute", left:0, top:0, width: 340, height: 65
                    }
                },
                parent: inputArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "hour",
                    css: {
                        position: "absolute", color: "white"
                    }
                },
                text: "00",
                parent: inputArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "dot",
                    css: {
                        position: "absolute", color: "white"
                    }
                },
                text: ":",
                parent: inputArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "min",
                    css: {
                        position: "absolute", color: "white"
                    }
                },
                text: "00",
                parent: inputArea
            });

            // 꺼짐 체크박스
            var checkBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "checkBox OFF",
                    css: {
                        position: "absolute", left: 0, top: 76, width: 285, height: 65
                    }
                },
                parent: setTimeArea
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "checkBox_Bg",
                    css: {
                        position: "absolute", left:0, top:0, width: 285, height: 65, "border" : "solid 5px rgb(210,51,47)", "box-sizing" : "border-box"
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_dim_uncheck_img",
                    src: "images/pop_cb_dim_uncheck.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_dim_check_img",
                    src: "images/pop_cb_dim_check.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_foc_uncheck_img",
                    src: "images/pop_cb_foc_uncheck.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "checkBox_foc_check_img",
                    src: "images/pop_cb_foc_check_red.png",
                    css: {
                        position: "absolute", left: 13, top:12, width: 48, height: 40
                    }
                },
                parent: checkBox
            });
            // 꺼짐 텍스트
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "checkBox_text",
                    css: {
                        position: "absolute", left: 75, top: 20, width: 160, height: 30, color : "white"
                    }
                },
                text: "자동 꺼짐 시각",
                parent: checkBox
            });

            // 꺼짐 시각 입력 영역
            var inputArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "inputArea OFF",
                    css: {
                        position: "absolute", left: 294, top: 76, width: 340, height: 65
                    }
                },
                parent: setTimeArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "inputArea_normal_img",
                    src: "images/setting/set_input_w330.png",
                    css: {
                        position: "absolute", left:0, top:0, width: 340, height: 65
                    }
                },
                parent: inputArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "inputArea_dim_img",
                    src: "images/setting/set_input_w330_d.png",
                    css: {
                        position: "absolute", left:0, top:0, width: 340, height: 65
                    }
                },
                parent: inputArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "inputArea_focus_img",
                    src: "images/pop_pw_box_foc.png",
                    css: {
                        position: "absolute", left:0, top:0, width: 340, height: 65
                    }
                },
                parent: inputArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "hour",
                    css: {
                        position: "absolute", color: "white"
                    }
                },
                text: "00",
                parent: inputArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "dot",
                    css: {
                        position: "absolute", color: "white"
                    }
                },
                text: ":",
                parent: inputArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "min",
                    css: {
                        position: "absolute", color: "white"
                    }
                },
                text: "00",
                parent: inputArea
            });


            // 구분선
            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left:0, top: 391, width: 1247, height: 2, "background-color" : "rgba(255,255,255,0.1)"
                    }
                },
                parent: settingViewArea
            });

            //반복 설정 영역
            setRepeatArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "repeatArea",
                    css: {
                        position: "absolute", left: 0, top:393, width: 1247, height: 500
                    }
                },
                parent: settingViewArea
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 40, width: 120, height: 36, "font-size" : 33, "font-family" : "RixHead M",
                        "letter-spacing" : -1.65, color: "white" , opacity: 0.7
                    }
                },
                text: "반복 설정",
                parent: setRepeatArea
            });

            var radioButtonArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "radioBtnArea",
                    css: {
                        position: "absolute", left: 0, top: 98, width: 900, height: 79
                    }
                },
                parent: setRepeatArea
            });

            var text = ["한번만","매일","요일 선택"];
            for(var i = 0; i < 3; i++) {
                var box = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "radioBtnBox",
                        css: {
                            position: "absolute", left: i*285, top: 0, width: 285, height: 77
                        }
                    },
                    parent:radioButtonArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "boxBg",
                        css: {
                            position: "absolute", left:0, top:0, width: 285, height: 77
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "radioBtn_uncheck_img",
                        src: "images/rdo_btn_d.png",
                        css: {
                            position: "absolute", left:20, top:14, width: 48, height: 48
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "radioBtn_check_img",
                        src: "images/rdo_btn_select_d.png",
                        css: {
                            position: "absolute", left:20, top:14, width: 48, height: 48
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "radioBtn_foc_uncheck_img",
                        src: "images/rdo_btn_f.png",
                        css: {
                            position: "absolute", left:20, top:14, width: 48, height: 48
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "radioBtn_foc_check_img",
                        src: "images/rdo_btn_select_f.png",
                        css: {
                            position: "absolute", left:20, top:14, width: 48, height: 48
                        }
                    },
                    parent: box
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "radioBtn_text",
                        css: {
                            position: "absolute", left: 86, top:24, width: 200, height: 35
                        }
                    },
                    text : text[i],
                    parent: box
                });

            }

            var dayArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "dayArea",
                    css: {
                        position: "absolute", left: 0, top: 200, width: 1247, height: 100
                    }
                },
                parent: setRepeatArea
            });

            var day = ["월","화","수","목","금","토","일"];
            for(var i = 0; i < 7; i++) {

                var box = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "dayBox",
                        css: {
                            position: "absolute", left: (i*150) +(i*10), top: 0, width: 150, height: 77
                        }
                    },
                    parent: dayArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "boxBg",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 150, height: 77
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/pop_cb_dim_uncheck.png",
                        class: "checkBox_dim_uncheck_img",
                        css: {
                            position: "absolute" , left: 20, top: 18, width: 48, height: 40
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/pop_cb_dim_check.png",
                        class: "checkBox_dim_check_img",
                        css: {
                            position: "absolute" , left: 20, top: 18, width: 48, height: 40
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/pop_cb_foc_uncheck.png",
                        class: "checkBox_foc_uncheck_img",
                        css: {
                            position: "absolute" , left: 20, top: 18, width: 48, height: 40
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/pop_cb_foc_check_red.png",
                        class: "checkBox_foc_check_img",
                        css: {
                            position: "absolute" , left: 20, top: 18, width: 48, height: 40
                        }
                    },
                    parent: box
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "checkBox_text",
                        css: {
                            position: "absolute" , left: 83, top: 25, width: 45, height: 45
                        }
                    },
                    text: day[i],
                    parent: box
                });

            }

        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            createView();

            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>설정한 시각에 셋톱박스를<br>자동으로<br>켜고 끌 수 있습니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div saveButton'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * local storage 에 저장한다.
         *
         * value는 repeat;startTime;endTime;executed
         * executed 는 항상 "false"로 set 하고 PowerModeManager에서 사용한다.
         *
         * @param repeat 반복여부 (한번만: "false", 매일: "true")
         * @param startTime 자동 켜짐 시각
         * @param endTime 자동 꺼짐 시각
         */
/*        function setAutoPower(repeat, startTime, endTime) {
            log.printDbg("setAutoPower(), repeat = " + repeat + ", startTime = " + startTime + ", endTime = " + endTime);
            var value = "";
            if (startTime === endTime) {
                startTime = "0000";
                endTime = "0000";
            }
            value += repeat ? "true;" : "false;";
            value += startTime + ";" + endTime;
            //설정을 저장한 경우 무조건 false로 설정한다. 이후에 1회는 꼭 실행되어야 하므로...
            value += ";false";

            storageManager.ps.save(storageManager.KEY.AUTO_POWER, value);
        }*/

        function setAutoPower(repeat,startTime,endTime,array) {
            log.printDbg("setAutoPower");
            log.printDbg("setAutoPower_startTime "+startTime);
            log.printDbg("setAutoPower_endTime "+endTime);
            log.printDbg("setAutoPower_repeat "+repeat);
            log.printDbg("setAutoPower_array "+JSON.stringify(array));

            var value = "";
            if(startTime === endTime) {
                startTime = "----";
                endTime = "----";
            }

            /**
             * 설정 저장 포맷
             *  repeat;startTime ; endTime ;exequted;요일
             *  ex ) true;0000;0000;true;true:true ....
            *
            * */

            switch(repeat) {
                case REPEAT.ONCE :
                    value += "false;";
                    break;
                case REPEAT.EVERYDAY :
                    value += "true;";
                    break;
                case REPEAT.SELECTIVE :
                    value += "selective;";
                    break;
            }
            value += startTime + ";" + endTime ;

            //설정을 저장한 경우 무조건 executed 값은  false로 설정한다. 이후에 1회는 꼭 실행되어야 하므로...
            value += ";false";
            if(repeat == REPEAT.SELECTIVE) {
                // 요일 세팅..
                for(var i = 0; i < array.length; i++) {
                    if(array[i] == true) {
                        value += ";true";
                    }else  {
                        value += ";false";
                    }
                }
            }

            log.printDbg("autopower setting final value = "+value);
            storageManager.ps.save(storageManager.KEY.AUTO_POWER, value);
        }

        /**
         * 설정 값을 읽어온다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");
            var result;
            var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_AUTO_POWER);

            //요일 array 초기화
            for(var i = 0; i < 7; i++) {
                dayArray[i] = false;
                INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox:eq("+i+")").removeClass("checked");
            }

            result = settingData[0];


            startHH = result[1].substring(0, 2);
            startMM = result[1].substring(2);
            endHH = result[2].substring(0, 2);
            endMM = result[2].substring(2);

            log.printDbg("result "+JSON.stringify(result));

            if ( (result === undefined || result.length === 0 || result === "") || ( startHH === endHH && startMM === endMM )) { // 자동전원 기능 off 인 경우
                startHH = "--";
                startMM = "--";
                endHH = "--";
                endMM = "--";
                resetConfigurationUI();
            } else { // isSet

                // 자동 켜짐이 동작 상태인지 확인
                if(startHH =="--" && startMM == "--") {
                    // 자동켜짐이 동작하지 않는 상태이면 체크 해제
                    INSTANCE.div.find("#contents .checkBox.ON").removeClass("checked");
                    //시각영역 dim 처리 하고
                    INSTANCE.div.find("#contents .inputArea.ON").addClass("dimmed");
                }else {
                    // 동작하는 경우, 체크한다
                    INSTANCE.div.find("#contents .checkBox.ON").addClass("checked");
                    INSTANCE.div.find("#contents .inputArea.ON").removeClass("dimmed");
                }
                // 자동 꺼짐이 동작 상태인지 확인
                if(endHH == "--" && endMM == "--") {
                    // 자동켜짐이 동작하지 않는 상태이면 체크 해제
                    INSTANCE.div.find("#contents .checkBox.OFF").removeClass("checked");
                    //시각영역 dim 처리 하고
                    INSTANCE.div.find("#contents .inputArea.OFF").addClass("dimmed");
                }else {
                    // 동작하는 경우, 체크한다
                    INSTANCE.div.find("#contents .checkBox.OFF").addClass("checked");
                    INSTANCE.div.find("#contents .inputArea.OFF").removeClass("dimmed");
                }
                // 켜짐, 꺼짐 영역 둘다 없는지 체크 안되어있는지 검사.
                // 켜짐, 꺼짐 시각 모두 체크가 되어 있지 않은 경우 반복 설정 영역으로 갈 수 없음
                if(!INSTANCE.div.find("#contents .setTimeArea .checkBox.ON").hasClass("checked") &&
                    !INSTANCE.div.find("#contents .setTimeArea .checkBox.OFF").hasClass("checked")) {
                    // 반복 설정 영역 dimmed 처리 한다.
                    INSTANCE.div.find("#contents .repeatArea").css({opacity: 0.3});
                    saveBtnBlockFlag = false;
                }else {
                    INSTANCE.div.find("#contents .repeatArea").css({opacity: 1.0});
                }


                // 라디오 영역 버튼 처리
                if (result[0] == "true") {
                    repeatIndex = REPEAT.EVERYDAY;
                } else if(result[0] == "false") {
                    repeatIndex = REPEAT.ONCE;
                } else if(result[0] == "selective") {
                    // 요일 어떻게 할거야..
                    repeatIndex = REPEAT.SELECTIVE;
                    // 선택되있던 요일 체크.
                    for(var i = 0 ; i < 7; i ++) {
                        log.printDbg("result[4+"+i+"]()         "+result[4+i]);
                        if(result[4+i] == "true") {
                            dayArray[i] = true;
                            INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox:eq("+i+")").addClass("checked");
                        }else {
                            dayArray[i] = false;
                            INSTANCE.div.find("#contents .repeatArea .dayArea .dayBox:eq("+i+")").removeClass("checked");
                        }
                    }
                }
                enterKeyEvent(repeatIndex,MENUTYPE.REPEAT_PERIOD)
            }
            // set current set values..
             INSTANCE.div.find("#contents .setTimeArea .inputArea.ON .hour").text(startHH);
             INSTANCE.div.find("#contents .setTimeArea .inputArea.ON .min").text(startMM);
             INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF .hour").text(endHH);
             INSTANCE.div.find("#contents .setTimeArea .inputArea.OFF .min").text(endMM);


            menuIdx = MENUTYPE.AUTO_ON;
            focusIdx = 0;
            setFocus(focusIdx,menuIdx);

        }

        /**
         * 사용자의 설정 값을 저장한다.
         *
         * @param repeat 반복여부 (한번만: "false", 매일: "true")
         * @param startTime 자동 켜짐 시각
         * @param endTime 자동 꺼짐 시각
         */

        /**
         * 사용자의 설정 값을 저장한다.
         *  @param repeat 반복 여부 ( 0: 안함, 1: 매일, 2: 요일선택)
         * @param startTime 켜짐  시각 (--:-- 일 경우 동작 안함)
         * @param endTime 꺼짐 시각 (--:-- 일 경우 동작 안함)
         * @param array (index 0~ 6(월 ~일))
         */
        function saveConfiguration(repeat,startTime,endTime,array){
            log.printDbg("saveConfiguration()");

            setAutoPower(repeat,startTime,endTime,array);
        }

        function checkLowPower() {
            var on_off;

            // [jh.lee] 1 : ON, 0 : OFF
            on_off = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.POWER_STATUS);

            return on_off;
        }

    };

    KTW.ui.layer.setting.setting_autoPower.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_autoPower.constructor = KTW.ui.layer.setting.setting_autoPower;

    KTW.ui.layer.setting.setting_autoPower.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_autoPower.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();