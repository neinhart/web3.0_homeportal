/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 전체 초기화
 */

(function () {
    KTW.ui.layer.setting.setting_allReset  = function AllReset(options) {
        KTW.ui.Layer.call(this, options);

        var INSTANCE = this;
        var styleSheet;
        var focusIdx = 0, menuType = 0;
        var passwordValue = "";

        var KEY_CODE = KTW.KEY_CODE;

        var log = KTW.utils.Log;

        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var storageManager = KTW.managers.StorageManager;
        var authManager = KTW.managers.auth.AuthManager;
        var casAdapter = KTW.oipf.AdapterHandler.casAdapter;
        var messageManager = KTW.managers.MessageManager;
        var extensionAdapter = KTW.oipf.AdapterHandler.extensionAdapter;
        var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;

        var MAX_PASSWORD_LENGTH = 4;
        
        var isStartSSpeechRecognizer = false;

        var ID = {
            INIT_STB_DATA : "init_stb_data",
            INVALID : "invalid",
            COMPLETE : "complete"
        };

        var clockArea;
        var clock;



        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame allReset"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>전체 초기화</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            //  createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            createComponent();

            authManager.resetPWCheckCount();
            authManager.initPW();

            focusIdx = 0;
            menuType = 0;
            resetInputPassword();
            buttonFocusRefresh(focusIdx, menuType);
            log.printDbg("show AllReset");
            if(!options || !options.resume) {
                var data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SYSTEM_INITIALIZE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

         //   clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
         //   clock.hide();
            _stopSpeechRecognizer();
        };

        this.controlKey = function (key_code) {

            switch (key_code) {
                case KEY_CODE.UP:
                    if (menuType == 1) {
                        focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2);
                        buttonFocusRefresh(focusIdx, menuType);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (menuType == 1) {
                        focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2);
                        buttonFocusRefresh(focusIdx, menuType);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuType == 0) {
                        menuType = 1;
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, menuType);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (menuType == 0) {
                        deletePassword();
                    } else {

                        if(passwordValue.length == 4 && authManager.getPW().length == MAX_PASSWORD_LENGTH ) {
                            resetInputPassword();
                            authManager.initPW();
                        }
                        menuType = 0;
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, menuType);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (menuType == 0) {
                        menuType = 1;
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, menuType);
                    } else {
                        if(focusIdx == 0) {
                            checkPassword();
                        } else {
                            LayerManager.historyBack();
                        }
                    }
                    return true;
                default:

                    if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9) {
                        if(menuType == 0) {
                            if (passwordValue.length < 4) inputPassword(key_code - KEY_CODE.NUM_0);
                        }
                        return true;
                    }
                    return false;
            }
        };

        function resetInputPassword() {
            passwordValue = "";
            INSTANCE.div.find("#inputFocus_Text").text("");
        }

        function inputPassword(_value) {
            passwordValue += _value;
            log.printDbg("passwordValue::  "+passwordValue);

            authManager.inputPW(_value, MAX_PASSWORD_LENGTH);
            var starText = "";
            for (var idx = 0 ; idx < passwordValue.length ; idx ++) {
                starText += "*";
            }
            INSTANCE.div.find("#inputFocus_Text").text(starText);


            if (passwordValue.length == 4) {
                menuType = 1;
                focusIdx = 0;
                buttonFocusRefresh(focusIdx, menuType);
            }
        }

        function deletePassword() {
            if (passwordValue.length > 0 && authManager.getPW().length > 0) {

                passwordValue = passwordValue.substring(0, passwordValue.length-1);
                if(authManager.deletePW() === true) {
                    var starText = "";
                    for (var idx = 0; idx < passwordValue.length; idx++) {
                        starText += "*";
                    }
                    INSTANCE.div.find("#inputFocus_Text").text(starText);
                }
            }else {
                LayerManager.historyBack();
            }
        }

        function buttonFocusRefresh(index, _menuType) {
            INSTANCE.div.find("#inputPassword_area").addClass("unFocus");
            INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            if(_menuType == 0) {
                INSTANCE.div.find("#inputPassword_area").removeClass("unFocus");
                _startSpeechRecognizer();
            } else {
                INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                _stopSpeechRecognizer();
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
                "<div class='table_title'>전체 초기화</div>" +
                "<div class='table_subTitle'>전체 초기화를 진행하면 모든 개인 정보와 설정값이 초기화됩니다<br>※ 구매/성인인증 비밀번호는 초기화되지 않습니다</div>" +
            "</div>" +
            "<div id='inputPassword_area'>" +
                "<div id='input_Text'>olleh tv 비밀번호(성인인증)를 입력해 주세요</div>" +
                "<div id='inputBG_img'></div>" +
                "<div id='inputDefault_Text'>****</div>" +
                "<div id='inputFocus_Text'></div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>비밀번호 입력 후 저장<br>선택 시 모든 개인정보와<br>설정값을 초기화 합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_line'></div>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_line'></div>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function checkPassword() {
            var obj;

            obj = {
                type : authManager.AUTH_TYPE.AUTH_ADULT_PIN,
                callback : onAuthListener,
                loading : {
                    type : KTW.ui.view.LoadingDialog.TYPE.SETTING,
                    lock : true,
                    callback : null,
                    on_off : true
                },
                data : {
                    /* menu_id : menu_type*/
                }
            };

            authManager.checkUserPW(obj);
        }


        function onAuthListener(result) {
            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS) {
                // 2017.05.03 dhlee 오류 횟수 초기화
                authManager.resetPWCheckCount();
                authManager.initPW();
                changeInputText(true);
                onSuccess();
            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL) {
                changeInputText(false);
                onFail();
            } else if (result === authManager.RESPONSE.CANCEL ||
                result === authManager.RESPONSE.GOTO_INIT) {
                onCancel();
            }
        }
        function changeInputText(is_success) {
            if (is_success) {
                // 성공
                INSTANCE.div.find("#input_Text").text("olleh tv 비밀번호(성인인증)를 입력해 주세요");
            } else {
                // 실패
                INSTANCE.div.find("#input_Text").text("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
            }
        }

        /**
         * [sw.nam] 비밀번호 검증 성공 후
         */
        function onSuccess() {
            log.printDbg("onSuccess");

            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_ALL_RESET_CHECKING_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        complete : function () {

                            LayerManager.activateLayer({
                                obj: {
                                    id: KTW.ui.Layer.ID.SETTING_ALL_RESET_SYSTEM_POPUP,

                                    type: Layer.TYPE.POPUP,
                                    priority: Layer.PRIORITY.SYSTEM_POPUP,
                                    linkage: true,
                                    params: {
                                        complete : function () {

                                            onResponsePopup();
                                        }
                                    }
                                },
                                new: true,
                                visible: true
                            });

                        }
                    }
                },
                new: true,
                visible: true
            });

            //authManager.initPW();
            menuType = 0;
            focusIdx = 0;
            resetInputPassword();
            buttonFocusRefresh(focusIdx, menuType);
        }

        /**
         * [sw.nam] 전체 초기화 진행
         */
        function initSTBData() {

            log.printDbg("initSTBData");

            log.printDbg("initSTBData(), KTW.managers.auth.AuthManager.getTempPin() = " + KTW.managers.auth.AuthManager.getTempPin() +
                ",KTW.managers.auth.AuthManager.getPW() = " + KTW.managers.auth.AuthManager.getPW());

            //var params_data;
            var message;
            var tmp_pw = "";

            // 2017.06.20 dhlee
            // authManager.getPW()는 return 하는 값이 없음
            // getTempPin() 을 이용하도록 변경함.
            // TODO 원래 의도가 getPW()에서 return 하는 값이 input 값인데, 이 값이 유지되는 것이었는지 확인 되어야 함
            //tmp_pw = authManager.getPW();
            tmp_pw = KTW.managers.auth.AuthManager.getTempPin();

            setTimeout(function () {
                // [sw.nam] KTCAController 에서 처리하는 부분을 KTW 맞게 변경
                casAdapter.setParentalRating(19, tmp_pw);
                KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_CHANGE_AGE_LIMIT,
                    { pin : tmp_pw, age_limit : 19 }, onCaResponse);
                //KTCAController.resetAgeLimit(setting.tempPW);

                try {
                    message = {
                        "method" : "spc_optout",
                        "msgType" : "JSON",
                        "type" : "A",
                        "id": "",
                        "setting" : false,
                        "to" : KTW.CONSTANT.APP_ID.SMART_PUSH,
                        "from" : KTW.CONSTANT.APP_ID.HOME
                    };
                    messageManager.sendMessage(message);
                } catch (error) {
                    log.printDbg(error.message);
                }

                // 2017.04.04 dhlee local storage 초기화는 StorageManager에서 한다.
                storageManager.ps.initializeLocalStorage();

                if (KTW.CONSTANT.IS_UHD) {
                    // [sw.nam] UHD 인 경우에만 블루투스 reset
                    bluetoothDevice.resetDevices();
                }

                // [sw.nam] 초기화 완료 후 reboot
                reboot();
            }, 1000);
        }

        /**
         * PR 변경된 경우 수행해야 할 후처리
         *
         * @param success
         */
        function onCaResponse(success) {
            log.printDbg("onCaResponse(), success = " + success);

            if (success === true) {
                // 2017.05.29 dhlee
                // 성공 시 listener 를 호출한다.
                KTW.ca.CaHandler.checkParentalRatingLock(true);
            }
        }

        function onResponsePopup() {
            log.printDbg("onResponsePopup");
            // [sw.nam] 초기화 확인 팝업 종료
            //  LayerManager.historyBack();

            initSTBData();

        }

        function reboot() {

            log.printDbg("reboot");
            var params_data;


            setTimeout(function() {
                LayerManager.historyBack();
                LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.SETTING_ALL_RESET_REBOOTING_POPUP,
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.SYSTEM_POPUP,
                        linkage: true,
                        params: {}

                    },
                    new: true,
                    visible: true
                });

                setTimeout(function () {

                    setTimeout(function () {
                        //extensionAdapter.reboot();
                        try {
                            log.printDbg("doFactoryReset");
                            extensionAdapter.doFactoryReset();
                        } catch (error) {
                            log.printDbg(error.message);
                        }
                    }, 2000);
                }, 2000); // 1->2 sec. 혹시 몰라 showwindow 초기화 시간을 약간 기다린다. 2초로 변경.
            },3000);
        }


        /**
         * [sw.nam] 비밀번호 검증 실패
         */
        function onFail() {
            var params_data;
            log.printDbg("onFail");
            // [sw.nam] 비밀번호 초기화 및 포커스 재지정
            authManager.initPW();
            menuType = 0;
            focusIdx = 0;
            resetInputPassword();
            buttonFocusRefresh(focusIdx, menuType);
        }

        /**
         * [sw.nam] 비밀번호 3회 오류 팝업에서 캔슬
         */
        function onCancel() {
            log.printDbg("onCancel");
            // [sw.nam] 비밀번호 초기화 및 포커스 재지정
            authManager.initPW();
            menuType = 0;
            focusIdx = 0;
            resetInputPassword();
            buttonFocusRefresh(focusIdx, menuType);
        }


        function _startSpeechRecognizer() {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() isStartSSpeechRecognizer : " + isStartSSpeechRecognizer);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                _stopSpeechRecognizer();
            }
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.start(true);
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            isStartSSpeechRecognizer = true;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() isStartSSpeechRecognizer :  " + isStartSSpeechRecognizer);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.stop();
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);

                isStartSSpeechRecognizer = false;
            }
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
            // mode = KTW.oipf.Def.SR_MODE.PIN;
            // type = "recognized";
            // input = "000";
            if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                KTW.managers.auth.AuthManager.initPW();
                resetInputPassword();

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>4) {
                        length = 4;
                    }

                    for(var i=0;i<length;i++) {
                        _inputNum(input[i] , length);
                    }
                }
            }
        }

    };

    KTW.ui.layer.setting.setting_allReset.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_allReset.constructor = KTW.ui.layer.setting.setting_allReset;

    KTW.ui.layer.setting.setting_allReset.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_allReset.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();