/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 채널/VOD 설정 > 자막 방송 설정
 */

(function () {
    KTW.ui.layer.setting.setting_subtitle  = function Subtitle(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var INSTANCE = this;
        var styleSheet;

        var on_off;
        var size;
        var language;

        var onoffIndex = 0;
        var sizeIndex = 0;
        var brodLangIndex = 0;
        var selectedLanguageIndex = 0;

        var focusIdx = 0;
        var menuType = 0;
        var lastMenuType = 0;

        var clock;
        var clockArea;

        var data;

        var isSelectiveBoxArea = false;

        var isTranslationLanguageCaptionSupport = false;

        // 0: OnOFF 설정, 1: 방송사 제공 언어 , 2: 자동번역 언어, 3: 글자크기, 4: 저장/취소
        var MENU_TYPE = {
            ON_OFF : 0,
            BROD_LANG : 1,
            AUTO_TRAN: 2,
            TEXT_SIZE: 3,
            RIGHT_MENU: 4
        };

        var LANGUAGE_LIST = [
            { name: "사용 안 함 (OFF)", isSelected: false }, { name: "중국어 (Chinese)", isSelected: false }, { name: "영어 (English)", isSelected: false },
            { name: "불어 (French)", isSelected: false }, { name: "독일어 (German)", isSelected: false }, { name: "일어 (Japanese)", isSelected: false },
            { name: "크메르어 (Khmer)", isSelected: false }, { name: "스페인어 (Spanish)", isSelected: false }, { name: "우즈 베크어 (Uzbek)", isSelected: false },
            { name: "베트남어 (Vietnamese)", isSelected: false }
        ];
        var MAX_SHOWING_VAR = 4;
        var LANGUAGE_LIST_MAX_PAGE = 3;
        var SCROLL_BAR_SIZE = 90.3;


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setSubtitle"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
            "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
            "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
            "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
            "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
            "<span id='backgroundTitle'>자막 방송</span>" +
            "</div>"+
            "<div id='contentsArea'>" +
            "<div id='contents'></div>" +
            "</div>");

            if(KTW.CONSTANT.IS_BIZ) {
                createComponentForBiz()
            }else {
                createComponent();
            }

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);


            _setCCLanguageList();

            _readConfiguration();

            enterKeyEventHandler(onoffIndex, MENU_TYPE.ON_OFF);
            enterKeyEventHandler(sizeIndex, MENU_TYPE.TEXT_SIZE);
            enterKeyEventHandler(brodLangIndex, MENU_TYPE.BROD_LANG);
            //menuFocusChange();
            isSelectiveBoxArea = false;


            focusIdx = onoffIndex;
            menuType = 0;
            buttonFocusRefresh(focusIdx, menuType);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CLOSED_CAPTION) {
                                return true;
                            }

                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }

            }
            //KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(
            //    KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT, KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP,
            //    KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH, KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            //KTW.managers.service.IframeManager.changeIframe();

            //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            //   clock.hide();

        };

        this.controlKey = function (key_code) {

            if(isSelectiveBoxArea) {
                var isTrue = false;
                isTrue = setSelectiveBoxArea(key_code);
                return isTrue
            }

            switch(key_code) {
                case KEY_CODE.UP:
                    if (menuType == MENU_TYPE.ON_OFF && onoffIndex === 1) {
                        return true;
                    }
                    if (menuType == MENU_TYPE.RIGHT_MENU) {
                        buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2), menuType);
                    } else if (menuType == MENU_TYPE.BROD_LANG) {
                        menuType = MENU_TYPE.ON_OFF;
                        focusIdx = onoffIndex;
                        buttonFocusRefresh(focusIdx, menuType);
                    } else if (menuType == MENU_TYPE.AUTO_TRAN) {
                        menuType = MENU_TYPE.BROD_LANG;
                        focusIdx = brodLangIndex;
                        buttonFocusRefresh(focusIdx, menuType);
                    } else if (menuType == MENU_TYPE.TEXT_SIZE) {

                        //TODO 비즈 상품이지만 매시업이 안오는 경우 UI 확인할 것
                        if(KTW.CONSTANT.IS_BIZ && isTranslationLanguageCaptionSupport) {
                            menuType = MENU_TYPE.AUTO_TRAN;
                            focusIdx = 0; // 0은 selected, 1은 selective
                        }else {
                            menuType = MENU_TYPE.BROD_LANG;
                            focusIdx = brodLangIndex;
                        }

                        buttonFocusRefresh(focusIdx, menuType);
                    } else if(menuType == MENU_TYPE.ON_OFF) {
                        menuType = MENU_TYPE.TEXT_SIZE;
                        focusIdx = sizeIndex;
                        buttonFocusRefresh(focusIdx, menuType);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (menuType == MENU_TYPE.ON_OFF && onoffIndex == 1) {
                        return true;
                    }
                    if (menuType == MENU_TYPE.RIGHT_MENU) {
                        buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2), menuType);
                    } else if (menuType == MENU_TYPE.TEXT_SIZE) {
                        menuType = MENU_TYPE.ON_OFF;
                        focusIdx = onoffIndex;
                        buttonFocusRefresh(focusIdx, menuType);
                    } else if (menuType == MENU_TYPE.AUTO_TRAN) {
                        menuType = MENU_TYPE.TEXT_SIZE;
                        focusIdx = sizeIndex;
                        buttonFocusRefresh(focusIdx, menuType);
                    } else if (menuType == MENU_TYPE.BROD_LANG) {
                        if(KTW.CONSTANT.IS_BIZ && isTranslationLanguageCaptionSupport) {
                            menuType = MENU_TYPE.AUTO_TRAN;
                            focusIdx = 0;
                        }else {
                            menuType = MENU_TYPE.TEXT_SIZE;
                            focusIdx = sizeIndex;
                        }
                        buttonFocusRefresh(focusIdx, menuType);
                    } else if (menuType == MENU_TYPE.ON_OFF) {
                        menuType = MENU_TYPE.BROD_LANG;
                        focusIdx = brodLangIndex;
                        buttonFocusRefresh(focusIdx,menuType);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (menuType == MENU_TYPE.RIGHT_MENU) {
                        menuType = lastMenuType;
                        switch(menuType){
                            case MENU_TYPE.ON_OFF:
                                focusIdx = onoffIndex;
                                break;
                            case MENU_TYPE.BROD_LANG:
                                focusIdx = brodLangIndex;
                                break;
                            case MENU_TYPE.AUTO_TRAN:
                                focusIdx =0;
                                // 자동번역 언어 케이스 추가
                                ////
                                break;
                            case MENU_TYPE.TEXT_SIZE:
                                focusIdx = sizeIndex;
                                break;
                        }
                    } else {
                        if (focusIdx == 0) {
                            LayerManager.historyBack();
                            return true;
                        }
                        if (focusIdx > 0) {
                            focusIdx--;
                        }
                    }

                    buttonFocusRefresh(focusIdx, menuType);

                    return true;
                case KEY_CODE.RIGHT:
                    if(menuType == MENU_TYPE.RIGHT_MENU) {
                        return true;
                    }
                    var maxIdx;
                    switch (menuType) {
                        case MENU_TYPE.ON_OFF:
                            maxIdx = 1;
                            break;
                        case MENU_TYPE.BROD_LANG:
                            maxIdx = 2;
                            break;
                        case MENU_TYPE.AUTO_TRAN :
                            maxIdx = 0;
                            //추가 예외처리 필요
                            break;
                        case MENU_TYPE.TEXT_SIZE :
                            maxIdx = 2;
                            break;
                    }

                    if(focusIdx < maxIdx) {
                        focusIdx ++;
                    }else {
                        lastMenuType = menuType;
                        focusIdx = 0;
                        menuType = MENU_TYPE.RIGHT_MENU;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.ENTER:
                    lastMenuType = menuType;
                    enterKeyEventHandler(focusIdx, menuType);
                    return true;
                default:
                    return false;
            }

        };

        /**
         * 자동번역 언어 선택시 반응하는 key case
         * @param key_code
         */
        function setSelectiveBoxArea(key_code) {
            log.printDbg("setSelectiveBoxArea()");

            INSTANCE.div.find(".translate_selectivaArea .box").removeClass("focus");


            switch (key_code) {
                case KEY_CODE.UP:
                    focusIdx --;
                    if(focusIdx < 0 ) {
                        focusIdx = LANGUAGE_LIST.length-1;
                    }
                    updateLanguageListArea(focusIdx);
                    return true;
                    break;
                case KEY_CODE.DOWN:
                    focusIdx ++;
                    if(focusIdx  > LANGUAGE_LIST.length-1) {
                        focusIdx = 0;
                    }
                    updateLanguageListArea(focusIdx);
                    return true;
                    break;
                case KEY_CODE.LEFT:
                    focusIdx = 0;
                    //menuType = MENU_TYPE.AUTO_TRAN;
                    isSelectiveBoxArea = false;
                    LayerManager.deactivateLayer();
                    // buttonFocusRefresh(focusIdx,menuType);
                    return true;
                    break;
                case KEY_CODE.RIGHT:
                    lastMenuType = MENU_TYPE.AUTO_TRAN;
                    focusIdx = 0;
                    menuType = MENU_TYPE.RIGHT_MENU;
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                    break;
                case KEY_CODE.ENTER:
                    // 선택한 옵션 저장 후 글자크기 선택 텝으로 이동
                    log.printDbg("focusIdx == "+focusIdx);

                    selectedLanguageIndex = focusIdx;
                    setSelectedTransLanguageText(selectedLanguageIndex);

                    log.printDbg("focusIdx == "+focusIdx);
                    focusIdx = sizeIndex;
                    menuType = MENU_TYPE.TEXT_SIZE;
                    isSelectiveBoxArea = false;
                    buttonFocusRefresh(focusIdx,menuType);

                    return true;
                    break;
            }
            return false;

        }

        /**
         * 선택된 번역언어 텍스트 표시
         * @param index 선택된 언어
         */
        function setSelectedTransLanguageText(index) {
            log.printDbg("setSelectedTransLanguageText() "+index);

            for(var i =0; i < LANGUAGE_LIST.length; i++) {
                LANGUAGE_LIST[i].isSelected = false;
            }

            LANGUAGE_LIST[index].isSelected = true;
            INSTANCE.div.find(".translate_selectedArea .boxText").text(LANGUAGE_LIST[index].name);
        }

        /** sw.nam
         * 자동 번역 언어 선택 옵션 리스트 업데이트 (order by focusIdx) .
         * 매번 포커스 움직일때마다 수행 , 텍스트라서 별로 부하가 없으니까
         */
        function updateLanguageListArea(focusIdx) {
            log.printDbg("updateLanguageListArea() "+focusIdx);

            var startIdx;
            INSTANCE.div.find(".translate_selectiveArea .box").removeClass("isSelected");
            INSTANCE.div.find(".translate_selectiveArea .box").removeClass("focus");
            startIdx = focusIdx - (focusIdx % MAX_SHOWING_VAR);
            log.printDbg("startIdx == "+startIdx);
            for(var i = startIdx; i < startIdx+MAX_SHOWING_VAR; i++) {
                var targetLanguage = LANGUAGE_LIST[i];
                if(targetLanguage) {
                    log.printDbg("targetLanguage = "+targetLanguage.name+ " isSelected == "+targetLanguage.isSelected);
                    INSTANCE.div.find(".translate_selectiveArea .box:eq(" +i% MAX_SHOWING_VAR+ ") .boxText").text(LANGUAGE_LIST[i].name);
                    if(LANGUAGE_LIST[i].isSelected == true) {
                        INSTANCE.div.find(".translate_selectiveArea .box:eq(" +i% MAX_SHOWING_VAR+ ")").addClass("isSelected");
                        INSTANCE.div.find(".translate_selectiveArea .box:eq(" + i% MAX_SHOWING_VAR+") .boxCheckImg").attr({src:"images/popup/option_dropbox_select.png"});
                    }
                }else {
                    INSTANCE.div.find(".translate_selectiveArea .box:eq(" + i% MAX_SHOWING_VAR+") .boxText").text("");
                }
            }
            INSTANCE.div.find(".translate_selectiveArea .box:eq(" + focusIdx % MAX_SHOWING_VAR+ ")").addClass("focus");
            if(LANGUAGE_LIST[focusIdx].isSelected) {
                INSTANCE.div.find(".translate_selectiveArea .box:eq(" + focusIdx % MAX_SHOWING_VAR+ ") .boxCheckImg").attr({src:"images/popup/option_dropbox_select_f.png"});
            }

            //스크롤 set sw.nam(언어 개수가 고정이니까 우선 하드코딩 해둔다..시간없음)
            INSTANCE.div.find(".translate_selectiveArea .scroll_area .scroll_bar").css({top: SCROLL_BAR_SIZE * Math.floor( focusIdx  / MAX_SHOWING_VAR ) })

        }

        function enterKeyEventHandler(index, _menuType) {
            INSTANCE.div.find("#settingTable .table_item_div").removeClass("disable");
            switch(_menuType) {
                case MENU_TYPE.ON_OFF:
                    onoffIndex = index;
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" +onoffIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");
                    if (onoffIndex === 1) {
                        INSTANCE.div.find("#settingTable .table_item_div:eq(1)").addClass("disable");
                        INSTANCE.div.find("#settingTable .table_item_div:eq(2)").addClass("disable");
                        INSTANCE.div.find("#settingTable .table_item_div:eq(3)").addClass("disable");
                        focusIdx = 0;
                        menuType = MENU_TYPE.RIGHT_MENU;
                    } else {
                        focusIdx = brodLangIndex;
                        menuType = MENU_TYPE.BROD_LANG;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    break;
                case MENU_TYPE.BROD_LANG:
                    brodLangIndex = index;
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + brodLangIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");
                    if(KTW.CONSTANT.IS_BIZ && isTranslationLanguageCaptionSupport) {
                        focusIdx = 0;
                        menuType = MENU_TYPE.AUTO_TRAN;
                    }else {
                        focusIdx = sizeIndex;
                        menuType = MENU_TYPE.TEXT_SIZE;
                    }

                    buttonFocusRefresh(focusIdx, menuType);
                    break;
                case MENU_TYPE.AUTO_TRAN:
                    if(focusIdx == 0) {
                        // 선택 창 띄우기
                        //INSTANCE.div.find(".translate_selectedArea").css({visibility: "hidden"});
                        INSTANCE.div.find(".translate_selectiveArea").addClass("focus");
                        isSelectiveBoxArea = true;
                        focusIdx = selectedLanguageIndex;
                        updateLanguageListArea(focusIdx);
                    }
                    break;
                case MENU_TYPE.TEXT_SIZE:
                    sizeIndex = index;
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + sizeIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_f.png");
                    focusIdx = 0;
                    menuType = MENU_TYPE.RIGHT_MENU;
                    buttonFocusRefresh(focusIdx, menuType);
                    break;
                case MENU_TYPE.RIGHT_MENU:
                    if (index === 0) {
                        saveConfiguration();
                        data.complete();
                        settingMenuManager.historyBackAndPopup(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CLOSED_CAPTION,null);
                    } else {
                        LayerManager.historyBack();
                    }
                    break;
            }
        }

        function buttonFocusRefresh(index, _menuType) {
            INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#settingTable .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + onoffIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + brodLangIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + sizeIndex + ") .table_radio_img").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find(".translate_selectedArea").removeClass("focus");
            INSTANCE.div.find(".translate_selectiveArea").removeClass("focus");
            INSTANCE.div.find(".translate_selectedArea .boxCheckImg").attr({src: "images/arw_option_popup_dw.png"});

            switch (_menuType) {
                case MENU_TYPE.ON_OFF:
                    INSTANCE.div.find("#settingTable .table_item_div").removeClass("disable");
                    INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == onoffIndex ? "rdo_btn_select_f.png" : "rdo_btn_f.png"));
                    if (onoffIndex == 1) {
                        INSTANCE.div.find("#settingTable .table_item_div:eq(1)").addClass("disable");
                        INSTANCE.div.find("#settingTable .table_item_div:eq(2)").addClass("disable");
                        INSTANCE.div.find("#settingTable .table_item_div:eq(3)").addClass("disable");
                    }
                    break;
                case MENU_TYPE.BROD_LANG:
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == brodLangIndex ? "rdo_btn_select_f.png" : "rdo_btn_f.png"));
                    break;

                case MENU_TYPE.AUTO_TRAN:
                    if(index == 0) {
                        INSTANCE.div.find(".translate_selectedArea").addClass("focus");
                        INSTANCE.div.find(".translate_selectedArea .boxCheckImg").attr({src: "images/arw_option_popup_dw_f.png"});
                    } else {
                        INSTANCE.div.find(".translate_selectiveArea").addClass("focus");
                    }

                    break;

                case MENU_TYPE.TEXT_SIZE:
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#settingTable #table_radio_area:eq(2) .table_radio_btn:eq(" + index + ") img").attr("src", "images/" + (index == sizeIndex ? "rdo_btn_select_f.png" : "rdo_btn_f.png"));
                    break;
                case MENU_TYPE.RIGHT_MENU:
                    INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            // 비즈상품인 경우에만 자동번역언어 설정 보여줌
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div' style = 'position: absolute; width: 1247px; height: 254px;'>" +
            "<div class='table_title'>자막 방송</div>" +
            "<div class='table_infoText'>ON으로 설정하면 채널에서 별도 제공하는 자막 방송을 시청할 수 있습니다 (서비스 제공 중인 일부 단말에서 제공됩니다)</div>" +
            "<div id='table_radio_area' style='top: 153px;'>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>ON</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>OFF</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute; top: 256px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute; top: 258px; width: 1247px; height: 186px;'>" +
            "<div class='table_subTitle'>방송사 제공 언어</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>기본</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>한국어</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>영어</div>" +
            "</div>" +
            "</div>" +
            "</div>" +

                /***************/

            "<div class='table_divisionLine'style='position: absolute; top:444px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute; top: 446px; width: 1247px; height: 186px;'>" +
            "<div class='table_subTitle'>글자 크기</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='width: 221px;'>" +
            "<div class='table_radio_btn_bg' style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>작게</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 221px; margin-right: 40px;'>" +
            "<div class='table_radio_btn_bg' style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>중간(기본)</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 221px;'>" +
            "<div class='table_radio_btn_bg'style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>크게</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");


            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>자막 방송의 자동 ON/OFF<br>및 글자 크기와 언어를<br>설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_line'></div>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_line'></div>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }
        function createComponentForBiz() {
            // 비즈상품인 경우에만 자동번역언어 설정 보여줌
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div' style = 'position: absolute; width: 1247px; height: 254px;'>" +
            "<div class='table_title'>자막 방송</div>" +
            "<div class='table_infoText'>ON으로 설정하면 채널에서 별도 제공하는 자막 방송을 시청할 수 있습니다 (서비스 제공 중인 일부 단말에서 제공됩니다)</div>" +
            "<div id='table_radio_area' style='top: 153px;'>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>ON</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>OFF</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute; top: 256px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute; top: 258px; width: 1247px; height: 186px;'>" +
            "<div class='table_subTitle'>방송사 제공 언어</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>기본</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>한국어</div>" +
            "</div>" +
            "<div class='table_radio_btn'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>영어</div>" +
            "</div>" +
            "</div>" +
            "</div>" +

            /******  자동번역 언어 기능 추가 area   ********/
            "<div class='table_divisionLine'style='position: absolute; top:444px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute;  top: 446px; width: 1247px; height: 380px; z-index: 1;'>" +
            "<div class='table_subTitle'>자동번역 언어</div>" +
            "<div class= 'translate_selectedArea' style='position: absolute; top: 84px; width: 405px; height: 72px;'>" +
            "<img class='boxImage' src='images/setting/set_w254_drop.png' style='position: absolute; width: 405px; height: 72px;'>" +
            "<div class='boxFocusBg' style='position: absolute; width: 405px; height: 72px; background-color: rgb(210,51,47);'></div>"+
            "<span class='boxText' style='position: absolute; left: 30px; top: 21px; width: 320px; height: 34px;'>사용 안 함 (OFF)</span>"+
            "<img class='boxCheckImg' src='images/arw_option_popup_dw.png' style='position: absolute; left: 345px; top: 11px; width: 45px; height: 45px;'>"+
            "</div>"+

            "<div class= 'translate_selectiveArea' style='position: absolute; top: 84px; width: 405px; height: 288px;'>" +
            "<div class='box' style='position: absolute; width: 405px; height: 72px;'>" +
            "<img class='boxImage' src='images/setting/set_w254_drop_1.png' style='position: absolute; width: 405px; height: 72px;'>" +
            "<div class='boxFocusBg' style='position: absolute; width: 405px; height: 72px; background-color: rgb(210,51,47);'></div>"+
            "<span class='boxText' style='position: absolute; left: 30px; top: 21px; width: 320px; height: 34px;'>사용 안 함 (OFF)</span>"+
            "<img class='boxCheckImg' src='images/popup/option_dropbox_select.png' style='position: absolute; left: 345px; top: 11px; width: 45px; height: 45px;'>"+
            "</div>" +
            "<div class='box' style='position: absolute; top: 72px; width: 405px; height: 72px;'>" +
            "<img class='boxImage' src='images/setting/set_w254_drop_1.png' style='position: absolute; width: 405px; height: 72px;'>" +
            "<div class='boxFocusBg' style='position: absolute; width: 405px; height: 72px; background-color: rgb(210,51,47);'></div>"+
            "<span class='boxText' style='position: absolute; left: 30px; top: 21px; width: 320px; height: 34px;'>중국어 (Chinese)</span>"+
            "<img class='boxCheckImg' src='images/popup/option_dropbox_select.png' style='position: absolute; left: 345px; top: 11px; width: 45px; height: 45px;'>"+
            "</div>" +
            "<div class='box' style='position: absolute; top: 144px; width: 405px; height: 72px;'>" +
            "<img class='boxImage' src='images/setting/set_w254_drop_1.png' style='position: absolute; width: 405px; height: 72px;'>" +
            "<div class='boxFocusBg' style='position: absolute; width: 405px; height: 72px; background-color: rgb(210,51,47);'></div>"+
            "<span class='boxText' style='position: absolute; left: 30px; top: 21px; width: 320px; height: 34px;'>영어 (English)</span>"+
            "<img class='boxCheckImg' src='images/popup/option_dropbox_select.png' style='position: absolute; left: 345px; top: 11px; width: 45px; height: 45px;'>"+
            "</div>" +
            "<div class='box' style='position: absolute; top: 216px; width: 405px; height: 72px;'>" +
            "<img class='boxImage' src='images/setting/set_w254_drop_1.png' style='position: absolute; width: 405px; height: 72px;'>" +
            "<div class='boxFocusBg' style='position: absolute; width: 405px; height: 72px; background-color: rgb(210,51,47);'></div>"+
            "<span class='boxText' style='position: absolute; left: 30px; top: 21px; width: 320px; height: 34px;'>불어 (French)</span>"+
            "<img class='boxCheckImg' src='images/popup/option_dropbox_select.png' style='position: absolute; left: 345px; top: 11px; width: 45px; height: 45px;'>"+
            "</div>" +

            "<div class='scroll_area' style='position: absolute; left: 393px; top: 8px; width: 6px; height: 271px;'>" +
            "<div class ='scroll_bg' style='position: absolute; left:  2px; top: 0px; width: 2px; height: 271px; background-color: rgb(110,110,110);'></div>" +
            "<div class = 'scroll_bar' style='position: absolute; left: 0px; top: 0px; width: 5px; height: 90.3px; background-color: rgb(180,180,180);'></div>"+
            "</div>"+
            "</div>"+
            "</div>"+


                /***************/

            "<div class='table_divisionLine'style='position: absolute; top:633px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style = 'position: absolute; top: 635px; width: 1247px; height: 186px;'>" +
            "<div class='table_subTitle'>글자 크기</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='width: 221px;'>" +
            "<div class='table_radio_btn_bg' style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>작게</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 221px; margin-right: 40px;'>" +
            "<div class='table_radio_btn_bg' style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>중간(기본)</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='width: 221px;'>" +
            "<div class='table_radio_btn_bg'style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>크게</div>" +
            "</div>" +
/*            "<div class='table_radio_btn' style='width: 221px;'>" +
            "<div class='table_radio_btn_bg'style='width: 221px;'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>크게</div>" +
            "</div>" +*/
            "</div>" +
            "</div>" +
            "</div>");


            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>자막 방송의 자동 ON/OFF<br>및 글자 크기와 언어를<br>설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_line'></div>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_line'></div>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * [sw.nam]비즈상품인 경우에만 자동번역언어 매시업메시지 연동
         * @private
         */
        function _setCCLanguageList() {
            log.printDbg("_setCCLanguageList()");

            if(KTW.CONSTANT.IS_BIZ === true) {

                //sw.nam
                // 매쉬업 메시지 응답을 받는경우 vs 받지 않는 경우에 따라 노출되는 UI가 다르다.
                // 매쉬업 메시지 응답을 받은 경우 -> 자동 번역 언어 설정 박스 노출,
                // 매쉬업 메시지 응답을 받지 않는 경우 -> 번역 언어 박스 노출 x

                // 매시업 데이터 받아오기 전 가변 view 숨김.
                INSTANCE.div.find("#settingTable .table_divisionLine:eq(2)").css({visibility: "hidden"});
                INSTANCE.div.find("#settingTable .table_item_div:eq(2)").css({visibility: "hidden"});
                INSTANCE.div.find("#settingTable .table_item_div:eq(3)").css({visibility: "hidden"});

                settingDataAdapter.requestMashup_getCCLanguageList({callback:_cbCCLanguageList});
            }
        }



        function _cbCCLanguageList(options) {
            log.printDbg("_cbCCLanguageList() "+JSON.stringify(options));

            if(!options) {

                log.printDbg("options are null.. return");
                isTranslationLanguageCaptionSupport = false;

                // INSTANCE.div.find("#settingTable .table_divisionLine(1").css({visibility: "hidden"});

                // 매시업 데이터 받아오기 전 가변 view 숨김.
                INSTANCE.div.find("#settingTable .table_item_div:eq(3)").css({visibility: "inherit", top: "446px"});

                return;
            }

            INSTANCE.div.find("#settingTable .table_divisionLine:eq(2)").css({visibility: "inherit"});
            INSTANCE.div.find("#settingTable .table_item_div:eq(2)").css({visibility: "inherit"});
            INSTANCE.div.find("#settingTable .table_item_div:eq(3)").css({visibility: "inherit", top: "635px"});


            var message = options.msg;
            var list;
            var onLang;

            isTranslationLanguageCaptionSupport = true;

            if(message) {

                list = message.list;
                onLang = message.onLang;

                // languagelist를 셋팅한다.
                LANGUAGE_LIST = [];
                LANGUAGE_LIST[0] = {code:"NONE" , name : "사용 안함 (OFF)", isSelected: false };

                if(onLang == "NONE") {
                    LANGUAGE_LIST[0].isSelected = true;
                    selectedLanguageIndex = 0;
                }
                for(var i =0; i<list.length; i++) {

                    LANGUAGE_LIST[LANGUAGE_LIST.length] = {code: list[i].code, name : list[i].name , isSelected: false };

                    if(list[i].code == onLang) {
                        LANGUAGE_LIST[i].isSelected = true;
                        selectedLanguageIndex = i+1; // for 문 전에 "NONE" 값을 추가하므로 인덱스 선택 시 +1 해주어야 한다.
                    }
                }

                setSelectedTransLanguageText(selectedLanguageIndex);
            }

        }

        /**
         * 뷰 셋을 위한 configuration 값을 읽어온다. (자동번역언어는 매쉬업 처리되므로 별도 로직에서 처리)
         * @private
         */
        function _readConfiguration() {
            log.printDbg("_readConfiguration()");

            var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CLOSED_CAPTION);

            if(settingData !== undefined &&  settingData !== null && settingData.length === 3) {
                on_off = settingData[0];
                size = settingData[1];
                language = settingData[2];
                log.printDbg("_readConfiguration() on_off : "+ on_off + " , size : " + size + " , language : " + language);
            }

            if (on_off === KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.ON) {
                onoffIndex = 0;
            } else {
                onoffIndex = 1;
            }

           if (size === KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.SMALL) {
                sizeIndex = 0;
            } else if (size === KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.MEDIUM || size === KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.BASIC) {
               // 2018.03.16 sw.nam 신규 gui 적용
                sizeIndex = 1;
            } else if (size === KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.LARGE) {
                sizeIndex = 2;
            }

            if (language === KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.BASIC) {
                brodLangIndex = 0;
            } else if (language === KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.KOREAN) {
                brodLangIndex = 1;
            } else if (language === KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.ENGLISH) {
                brodLangIndex = 2;
            }

        }

        function saveConfiguration() {
            log.printDbg("saveConfiguration()");

            if (onoffIndex === 0) {
                on_off = KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.ON;
            }else {
                on_off = KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.OFF;
            }

            if (sizeIndex === 0) {
                size = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.SMALL;
            } else if (sizeIndex === 1 ) {
                size = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.MEDIUM;
            } else if (sizeIndex === 2 ) {
                size = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.LARGE;
            }


            if(brodLangIndex === 0) {
                language = KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.BASIC;
            }else if(brodLangIndex === 1) {
                language = KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.KOREAN;
            }else if(brodLangIndex === 2) {
                language = KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.ENGLISH;
            }

            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_ONOFF, on_off);
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_SIZE, size);
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_LANGUAGE, language);

            //TODO [sw.nam]자동 번역 언어 설정 값 펌웨어 완료되는대로 적용해야 함.

            // 자동번역언어 setting 값 저장
            if(KTW.CONSTANT.IS_BIZ && isTranslationLanguageCaptionSupport) {

                var isOn = "N";

                if(selectedLanguageIndex !== -1 && selectedLanguageIndex !== 0) {
                    isOn = "Y";
                }

                var mashup_caption_msg = {
                    from: KTW.CONSTANT.APP_ID.HOME,
                    to: KTW.CONSTANT.APP_ID.MASHUP,
                    method: "mashup_setCCLanguage",
                    isOn : isOn,
                    lang : LANGUAGE_LIST[selectedLanguageIndex].code
                };

                KTW.managers.MessageManager.sendMessage(mashup_caption_msg, null);

            }
        }
    };

    KTW.ui.layer.setting.setting_subtitle.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_subtitle.constructor = KTW.ui.layer.setting.setting_subtitle;

    KTW.ui.layer.setting.setting_subtitle.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_subtitle.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();