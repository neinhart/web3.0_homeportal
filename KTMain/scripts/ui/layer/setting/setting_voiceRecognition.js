/**
 * Created by Yun on 2016-11-23.
 */

(function () {
    KTW.ui.layer.setting.setting_voiceRecognition  = function VoiceRecognition(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var audioTtsAdapter = KTW.oipf.AdapterHandler.audioTtsAdapter;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var clock;
        var clockArea;

        var INSTANCE = this;
        var styleSheet;
        var focusXIdx = 0;
        var focusYIdx = 0;
        var lastFocusedYIdx = 0;
        var isRightFocus = false;

        var data;

        //var on_off = null;
        //var language = null;

        var voiceOnoffIndex = 0;
        var voiceTypeIdx = 0;

        //var isGuideOn = false;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame screenNarration"});
            //[sw.nam]
            // 화면 해설방송 UI와 동일하여 같은 UI 차용 .. 현재 구조상 css class name 이 햇갈릴 수 있으나 추후 코드 최적화 때 유사 UI 끼리  통합 예정

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<img id='backIcon' src='images/homeshot_default.png'>" +
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>음성가이드</span>" +
                "<div id='pig_dim'></div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
            createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            KTW.ui.LayerManager.showVBOBackground("bg_default");
            _readConfiguration();

            selectIconRefresh(voiceOnoffIndex, 0);
            selectIconRefresh(voiceTypeIdx, 1);
            removeFocus();
            buttonFocusRefresh(voiceOnoffIndex, 0);

            log.printDbg("show ScreenNarration");
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_VOICE_RECOG) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
            KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(
                KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT, KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP,
                KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH, KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            KTW.managers.service.IframeManager.changeIframe();

            clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide ScreenNarration");
            KTW.ui.LayerManager.hideVBOBackground();
            clock.hide();
        };

        this.controlKey = function (key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    if(isRightFocus) {
                        focusXIdx = 0;
                        buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, -1, 2));
                    }else if(voiceOnoffIndex === 0) {
                        if(focusYIdx == 0) {
                            focusXIdx = voiceTypeIdx;
                        }else if(focusYIdx == 1) {
                            focusXIdx = voiceOnoffIndex;
                        }
                        buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, -1, 2));
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if(isRightFocus) {
                        focusXIdx = 0;
                        buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, 1, 2));
                    }else if(voiceOnoffIndex === 0) {
                        if(focusYIdx == 0) {
                            focusXIdx = voiceTypeIdx;
                        }else if(focusYIdx == 1) {
                            focusXIdx = voiceOnoffIndex;
                        }
                        buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, 1, 2));
                    }

                    return true;
                case KEY_CODE.LEFT:
                    if (isRightFocus) {
                        menuFocusChange();
                    } else {
                        if(focusXIdx == 0) {
                            LayerManager.historyBack();
                            return true;
                        }
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, -1, 2), focusYIdx);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(isRightFocus) {
                        return true;
                    }
                    if (focusXIdx == 1) {
                        menuFocusChange();
                    } else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, 1, 2), focusYIdx);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (isRightFocus) {
                        if (focusYIdx == 0) {
                            // 시스템 저장
                            _saveConfiguration();
                            menuFocusChange();
                            data.complete();
                            settingMenuManager.historyBackAndPopup();
                        } else {
                            LayerManager.historyBack();
                        }

                    } else {
                        selectIconRefresh(focusXIdx, focusYIdx);
                    }
                    return true;
                default:
                    return false;
            }
        };

        function menuFocusChange() {
            if (isRightFocus) {
                isRightFocus = false;
                focusXIdx = voiceOnoffIndex;
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                if(lastFocusedYIdx === 0) {
                    focusXIdx = voiceOnoffIndex;
                    focusYIdx = lastFocusedYIdx;
                }else if(lastFocusedYIdx === 1) {
                    focusXIdx = voiceTypeIdx;
                    focusYIdx = lastFocusedYIdx;
                    if(voiceOnoffIndex === 1) {
                        focusXIdx = voiceOnoffIndex;
                        focusYIdx = 0;
                    }
                }
                buttonFocusRefresh(focusXIdx, lastFocusedYIdx);
            } else {
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");

                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_text").addClass("focus");
                lastFocusedYIdx = focusYIdx;
                focusYIdx = 0;
                isRightFocus = true;
            }
        }

        function removeFocus() {
            log.printDbg("removeFocus");
            isRightFocus = false;
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
        }

        function selectIconRefresh(index_X, index_Y) {
            if (index_Y == 0) {
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").removeClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                focusXIdx = 0;
                voiceOnoffIndex = index_X;
                if(voiceOnoffIndex == 0) {
                    // 가이드 ON
                    log.printDbg("가이드 ON");
                    INSTANCE.div.find("#contents .table_noti_text").css({ opacity: 0.1});
                    INSTANCE.div.find("#contents .table_item_div").eq(1).css({ opacity: 1});
                    buttonFocusRefresh(voiceTypeIdx,1);
                } else {
                    // 가이드 OFF
                    log.printDbg("가이드 OFF");
                    INSTANCE.div.find("#contents .table_noti_text").css({ opacity: 0.4});
                    INSTANCE.div.find("#contents .table_item_div").eq(1).css({ opacity: 0.3});
                    menuFocusChange();
                }

            } else { // 음성설정 or summit step
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").removeClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                voiceTypeIdx = index_X;

                menuFocusChange();
            }
        }

        function buttonFocusRefresh(index_X, index_Y) {
            focusXIdx = index_X;
            focusYIdx = index_Y;
            if (isRightFocus) {
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +") .rightMenu_btn_text").addClass("focus");
            } else {
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");

                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ")").addClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_text").addClass("focus");
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div' style='position: absolute; width: 886px; height: 350px;'>" +
                     "<div class='table_title'>음성가이드 설정</div>" +
                     "<div class='table_subTitle'>셋톱박스를 향해 '올레 tv' 라고 부르면 음성가이드 기능이 동작합니다</div>" +
                     "<div id='table_radio_area'style='top: 183px; '>" +
                         "<div class='table_radio_btn'>" +
                             "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                             "<div class='table_radio_text'>음성가이드 ON</div>" +
                         "</div>" +
                         "<div class='table_radio_btn' style='left: 285px; width: 287px;'>" +
                             "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                             "<div class='table_radio_text' >음성가이드 OFF</div>" +
                         "</div>" +
                    "</div>" +
                    "<div class='table_noti_text' style='position: absolute; top: 290px; font-size: 23px; color: white; font-family: RixHead L;'> OFF설정 시 음성가이드 볼륨설정을 할 수 없습니다</div>" +
                "</div>" +
                "<div class='table_divisionLine'style='position: absolute; top:377px; width: 886px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div'style='position: absolute; top:377px; width: 886px; height: 350px;'>" +
                    "<div class='table_title'>가이드 음성 설정</div>" +
                    "<div class='table_subTitle'>가이드 음성을 설정할 수 있습니다</div>" +
                    "<div id='table_radio_area' style = 'top : 183px;'>" +
                        "<div class='table_radio_btn'>" +
                            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                            "<div class='table_radio_text'>가이드 1 (여성)</div>" +
                        "</div>" +
                        "<div class='table_radio_btn'style='left: 285px;'>" +
                            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                            "<div class='table_radio_text'>가이드 2 (아이)</div>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
            "</div>");

            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text1'>음성가이드 기능을<br>설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }
        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function _readConfiguration() {
            log.printDbg("readConfiguration()");

            var voiceGuide = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_VOICE_RECOG);
            var defaultVoiceMode;
            if(voiceGuide[0] === "ON") {
                voiceOnoffIndex = 0;
                INSTANCE.div.find("#contents .table_noti_text").css({ opacity: 0.1});
                INSTANCE.div.find("#contents .table_item_div").eq(1).css({ opacity: 1});
            } else if( voiceGuide[0] == "OFF") {
                voiceOnoffIndex = 1;
                INSTANCE.div.find("#contents .table_noti_text").css({ opacity: 0.4});
                INSTANCE.div.find("#contents .table_item_div").eq(1).css({ opacity: 0.3});
            } else {
                voiceOnoffIndex = 1;
            }

            defaultVoiceMode = audioTtsAdapter.getDefaultVoiceMode();
            if(defaultVoiceMode == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE ) {
                voiceTypeIdx = 0;
            }else if(defaultVoiceMode == KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD) {
                voiceTypeIdx = 1;
            }
        }

        function _saveConfiguration() {
            log.printDbg("saveConfiguration()");
            var saveText;
            var defaultVoiceMode;

            if(voiceOnoffIndex === 0 ){
                saveText = "ON";
            } else {
                saveText = "OFF";
            }
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE, saveText);
            //TODO voiceTypeIndex 값 저장 로직 추후 추가
            if(voiceTypeIdx == 0) {
                defaultVoiceMode = KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_FEMALE;
            }else {
                defaultVoiceMode = KTW.oipf.Def.AUDIO_TTS.VOICE.VOICE_CHILD;
            }
            var result = audioTtsAdapter.setDefaultVoiceMode(defaultVoiceMode);
            log.printDbg("set default voice mode is _+ "+result);
        }
    };

    KTW.ui.layer.setting.setting_voiceRecognition.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_voiceRecognition.constructor = KTW.ui.layer.setting.setting_voiceRecognition;

    KTW.ui.layer.setting.setting_voiceRecognition.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_voiceRecognition.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();