/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 채널 /VOD 설정 > 화면해설방송
 */

(function () {
    KTW.ui.layer.setting.setting_screenNarration  = function ScreenNarration(options) {
        KTW.ui.Layer.call(this, options);
        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var settingMenuAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var log = KTW.utils.Log;


        var INSTANCE = this;
        var styleSheet;
        var focusXIdx = 0;
        var focusYIdx = 0;
        var lastFocusYIdx = 0;
        var isRightFocus = false;
        var onoffIndex = 0;
        var languageIndex = 0;

        var clock;
        var clockArea;

        var data;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame screenNarration"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
               // "<div style= 'position: absolute; left: 1450px; top: 916px; width: 390px; height: 2px; background-color: rgba(255,255,255,0.15)'></div>"+
                "<span id='backgroundTitle'>화면 해설 방송</span>" +
                    //"<img id='pig_shadow' src='images/pig_shadow.png' alt='pig_shadow'>" +
                    //"<img id='pig_gra' src='images/pig_gra.png' alt='pig_gra'>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            log.printDbg("show ScreenNarration");
            _readConfiguration();
            lastFocusYIdx = 0;
            selectIconRefresh(onoffIndex, 0);
            selectIconRefresh(languageIndex, 1);
            menuFocusChange();
            buttonFocusRefresh(onoffIndex, 0);
            // isRightFocus = false;
            // focusXIdx = 0;
            // focusYIdx = 0;
            // buttonFocusRefresh(focusXIdx, focusYIdx);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_DESCRIPTIVE_VIDEO_SERVICE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    if(!isRightFocus) {
                        switch(focusYIdx) {
                            case 0:
                                focusXIdx = languageIndex;
                                break;
                            case 1:
                                focusXIdx = onoffIndex;
                                break;
                        }
                    }
                    buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, -1, 2));
                    return true;
                case KEY_CODE.DOWN:
                    if(!isRightFocus) {
                        switch(focusYIdx) {
                            case 0:
                                focusXIdx = languageIndex;
                                break;
                            case 1:
                                focusXIdx = onoffIndex;
                                break;
                        }
                    }
                    buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, 1, 2));
                    return true;
                case KEY_CODE.LEFT:
                    if (isRightFocus) {
                        menuFocusChange();
                    } else {
                        if(focusXIdx == 0) {
                            LayerManager.historyBack();
                            return true;
                        }
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, -1, 2), focusYIdx);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(isRightFocus){
                        return true;
                    }
                    if (focusXIdx == 1) {
                        menuFocusChange();
                    } else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, 1, 2), focusYIdx);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if (isRightFocus) {
                        if (focusYIdx == 0) {
                            // 시스템 저장
                            _saveConfiguration();

                            menuFocusChange();
                            data.complete();
                            settingMenuManager.historyBackAndPopup();
                        } else {
                            menuFocusChange();
                            LayerManager.historyBack();
                        }

                    } else {
                        selectIconRefresh(focusXIdx, focusYIdx);
                    }
                    return true;
                case KEY_CODE.BACK:
                case KEY_CODE.EXIT:
                    if(isRightFocus) {
                        menuFocusChange();
                    }
                default:
                    return false;
            }
        };

        function menuFocusChange() {
            if (isRightFocus) {
                isRightFocus = false;
                if(lastFocusYIdx == 0){
                    focusXIdx = onoffIndex;
                }else if(lastFocusYIdx ==1) {
                    focusXIdx = languageIndex;
                }else {
                    focusXIdx = onoffIndex;
                }
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                buttonFocusRefresh(focusXIdx, lastFocusYIdx);
            } else {
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");

                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0)").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_text").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_line").addClass("focus");
                lastFocusYIdx = focusYIdx;
                focusYIdx = 0;
                isRightFocus = true;
            }
        }

        function selectIconRefresh(index_X, index_Y) {
            if (index_Y == 0) {
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").removeClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                focusXIdx = 0;
                onoffIndex = index_X;
                buttonFocusRefresh(languageIndex, 1);
            } else {
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").removeClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                languageIndex = index_X;

                menuFocusChange();
            }
        }

        function buttonFocusRefresh(index_X, index_Y) {
            focusXIdx = index_X;
            focusYIdx = index_Y;
            if (isRightFocus) {
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +")").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +") .rightMenu_btn_text").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq("+ index_Y +") .rightMenu_btn_line").addClass("focus");
            } else {
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");

                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ")").addClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_text").addClass("focus");
            }
        }

        function removeFocus() {
            log.printDbg("removeFocus()");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div' style='position: absolute; width: 886px; height: 350px;'>" +
                    "<div class='table_title'>화면 해설 방송</div>" +
                    "<div class='table_subTitle'>시각 장애인을 위한 기능입니다<br>ON 설정 시 일부 방송에서 음성이 나오지 않을 수 있으며 이 경우 설정을 OFF 해 주세요</div>" +
                    "<div id='table_radio_area'>" +
                        "<div class='table_radio_btn'>" +
                            "<div class='table_radio_btn_bg'></div>" +
                            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                            "<div class='table_radio_text'>ON</div>" +
                        "</div>" +
                        "<div class='table_radio_btn'style='left: 285px;'>" +
                            "<div class='table_radio_btn_bg'></div>" +
                            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                            "<div class='table_radio_text'>OFF</div>" +
                        "</div>" +
                    "</div>"+
                "</div>"+
                "<div class='table_divisionLine'style='position: absolute; top:411px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div'style='position: absolute; top:413px; width: 886px; height: 350px;'>" +
                    "<div class='table_title'>언어 설정</div>" +
                    "<div class='table_subTitle'>신호하는 화면 해설 방송의 언어를 설정합니다</div>" +
                    "<div id='table_radio_area' style='top: 183px'>" +
                        "<div class='table_radio_btn'>" +
                            "<div class='table_radio_btn_bg'></div>" +
                            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                            "<div class='table_radio_text'>한국어</div>" +
                        "</div>" +
                        "<div class='table_radio_btn'style='left: 285px;'>" +
                            "<div class='table_radio_btn_bg'></div>" +
                            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                            "<div class='table_radio_text'>영어</div>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
            "</div>");

            if (KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.LONGPRESS_SHARP) === "true") {
                INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text1'>※ TIP<br> 채널 시청 중에 화면 해설<br>방송을 바로 이용하시려면<br>리모컨의 #키를 3초간<br>누르세요 (채널을 변경하면<br>기능이 해제됩니다)</div>" +
/*                "<div id='rightMenu_line'></div>" +
                "<div id='rightMenu_text2'>※ 채널 업/다운 시<br>기본 설정으로 돌아갑니다</div>" +*/
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
            } else {
                INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text1'>화면 해설 방송의 자동 ON/OFF<br>및 언어를 설정합니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
            }

        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }


        function _readConfiguration() {
            log.printDbg("_readConfiguration");
            var settingData = settingMenuAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_DESCRIPTIVE_VIDEO_SERVICE);

            onoffIndex = 1;
            if (settingData!== undefined && settingData !== null && settingData.length === 2 && settingData[0] === "1") {
                onoffIndex = 0;
            }

            languageIndex = 0;
            if(settingData!== undefined && settingData !== null && settingData.length === 2 && settingData[1] === "eng") {
                languageIndex = 1;
            }
        }

        function _saveConfiguration() {
            log.printDbg("_saveConfiguration");
            if(onoffIndex === 0 ) {
                KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.VISUAL_IMPAIRED , "1");
            }else {
                KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.VISUAL_IMPAIRED , "0");
            }

            if(languageIndex === 0 ) {
                KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.AUDIO_LANGUAGE , "kor");
            }else {
                KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.AUDIO_LANGUAGE , "eng");
            }
        }

    };
    KTW.ui.layer.setting.setting_screenNarration.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_screenNarration.constructor = KTW.ui.layer.setting.setting_screenNarration;

    KTW.ui.layer.setting.setting_screenNarration.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };



    KTW.ui.layer.setting.setting_screenNarration.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();