/**
 * Created by Yun on 2017-02-26.
 *
 * 설정 > 시스템설정 > 비콘 설정 (UHD 3 only)
 */

(function () {
    KTW.ui.layer.setting.setting_beacon  = function setBeacon(options) {
        KTW.ui.Layer.call(this, options);

        var INSTANCE = this;
        var styleSheet;

        var stepValue = ["-5dBm", "-1dBm", "3dBm", "7dBm", "10dBm"];
        var settingValue = [];
        var inputNumber = "";
        var focusIdx = 0;
        var menuType = 0;
        var prevMenuType = 0;

        var log = KTW.utils.Log;

        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var KEY_CODE = KTW.KEY_CODE;

        var settingDataAdapter =KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;


        var data;

        var clock;
        var clockArea;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setting_layer setBeacon"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<span id='backgroundTitle'>비콘 송신 설정</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
         //   createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("setting_beacon");


            readConfiguration();
            focusIdx = 0;
            menuType = 0;
            buttonFocusRefresh(focusIdx, menuType);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BEACON) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


         //   clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide StartChannel");

         //   clock.hide();
        };

        this.controlKey = function (key_code) {

            switch(key_code) {
                case KEY_CODE.UP:
                    if (menuType == 2) {
                        buttonFocusRefresh(focusIdx = (KTW.utils.util.getIndex(focusIdx, -1, 2)), menuType);
                        return true;
                    } else if (menuType == 1) {
                        focusIdx = 0;
                        menuType = 0;
                    } else if (menuType == 0 ) {
                        focusIdx = 0;
                        menuType = 1;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.DOWN:
                    if (menuType == 2) {
                        buttonFocusRefresh(focusIdx = (KTW.utils.util.getIndex(focusIdx, 1, 2)), menuType);
                        return true;
                    }else if(menuType == 1) {
                        menuType = 0;
                        focusIdx = 0;
                    }else if (menuType == 0) {
                        menuType = 1;
                        focusIdx = 0;
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuType == 1) {
                        deleteInputNumber();
                    } else if (menuType == 2) {
                        focusIdx = 0;
                        menuType = prevMenuType;
                        buttonFocusRefresh(focusIdx, menuType);
                    } else {
                        if (menuType == 0) {
                            if(focusIdx ==0) {
                                LayerManager.historyBack();
                            }
                            focusIdx = 0;

                        } else if (menuType == 2) {
                            menuType = 0;
                            focusIdx = 0;
                        }
                        buttonFocusRefresh(focusIdx, menuType);
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuType == 1) {
                        enterEventHandler(menuType);
                        focusIdx = 0;
                        prevMenuType = menuType;
                        menuType = 2;
                    } else {
                        if (menuType == 0) {
                            if (focusIdx == 0) {
                                focusIdx = 1;
                            } else {
                                focusIdx = 0;
                                prevMenuType = menuType;
                                menuType = 2;
                            }
                        }
                    }
                    buttonFocusRefresh(focusIdx, menuType);
                    return true;
                case KEY_CODE.ENTER:
                    enterEventHandler(menuType);
                    if (menuType == 1) {
                        focusIdx = 0;
                        prevMenuType = menuType;
                        menuType = 2;
                        buttonFocusRefresh(focusIdx, menuType);
                    }

                    return true;
                default:
                    log.printDbg("inputNumber : "+inputNumber);
                    if (key_code >= KEY_CODE.NUM_0 && key_code <= KEY_CODE.NUM_9 && menuType == 1) {
                        log.printDbg("inputNumber 1 length : "+inputNumber.length);
                        if(inputNumber.length >=5) {
                            inputNumber = "";
                        }
                        numberInputEvent(key_code - KEY_CODE.NUM_0);
                        return true;
                    }
                    return false;
            }
        };

        function deleteInputNumber() {
            if (inputNumber.length > 0) inputNumber = inputNumber.substring(0, inputNumber.length-1);

            INSTANCE.div.find("#beaconSetting_inputValue").text(inputNumber + " ms");

        }

        function numberInputEvent(inputNum) {
            inputNumber += inputNum;
            log.printDbg("numberInputEvent inputNumber : "+inputNumber);
            INSTANCE.div.find("#beaconSetting_inputValue").text(inputNumber + " ms");
        }

        function buttonFocusRefresh(index, _menuType) {
            INSTANCE.div.find("#beaconSetting_area #beaconSetting_minusBtn").removeClass("focus");
            INSTANCE.div.find("#beaconSetting_area #beaconSetting_plusBtn").removeClass("focus");
            INSTANCE.div.find("#beaconSetting_area #beaconSetting_inputArea").removeClass("focus");
            INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            switch(_menuType) {
                case 0:
                    INSTANCE.div.find("#beaconSetting_area " + (index == 0? "#beaconSetting_minusBtn":"#beaconSetting_plusBtn")).addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#beaconSetting_area #beaconSetting_inputArea").addClass("focus");
                    break;
                case 2:
                    INSTANCE.div.find("#rightMenu_btn_area .rightMenu_btn_div:eq(" +  index + ")").addClass("focus");
                    break;
            }
        }

        function enterEventHandler(_menuType) {
            switch(_menuType) {
                case 0:
                    if (focusIdx == 0) {
                        if (settingValue[0] > 0)settingValue[0] --;
                        INSTANCE.div.find("#beaconSetting_stepArea .beaconSetting_stepFocus_div").removeClass("show");
                    } else {
                        if (settingValue[0] < 4)settingValue[0] ++;
                    }
                    for (var idx = 0 ; idx < settingValue[0]+1 ; idx ++) {
                        log.printDbg("enterEventHandleridx : __"+idx);
                        INSTANCE.div.find("#beaconSetting_stepArea .beaconSetting_stepFocus_div:eq(" + idx + ")").addClass("show");
                    }
                    INSTANCE.div.find("#beaconSetting_settingValue").text(stepValue[settingValue[0]]);
                    break;
                case 1:
                    //if (!inputNumber) return;
                    INSTANCE.div.find("#beaconSetting_area #beaconSetting_inputArea").removeClass("focus");
                    var tmpInputNum = parseInt(inputNumber);
                    if (!tmpInputNum || tmpInputNum < 20) {
                        tmpInputNum = 20;
                    } else if (tmpInputNum > 10000) {
                        tmpInputNum = 10000;
                    } else {

                        tmpInputNum = (tmpInputNum%20 < 10 ? tmpInputNum - (tmpInputNum%20)  : tmpInputNum - (tmpInputNum%20)+20 );
                    }
                    inputNumber = tmpInputNum.toString();
                    settingValue[1] = parseInt(inputNumber);
                    INSTANCE.div.find("#beaconSetting_inputValue").text(inputNumber + " ms");
                    break;
                case 2:
                    if (focusIdx == 0) {

                        // 저장
                        try {
                            saveConfiguration();
                            data.complete();
                            settingMenuManager.historyBackAndPopup();
                            return ;
                        }catch(e){
                            log.printErr(e);
                        }
                    }
                    LayerManager.historyBack();
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='beaconSetting_area'>" +
            "<div id='beaconSetting_div_0'>" +
            "<div class='beaconSetting_title'>송신 세기</div>" +
            "<div class='beaconSetting_subTitle'>비콘 송신 세기를 설정합니다</div>" +
            "<div id='beaconSetting_minusBtn'>" +
            "<img class='beaconSetting_btnBG' src='images/pop_btn_w100.png'>" +
            "<span class='beaconSetting_btnText'>-</span>"+
            "<div class='beaconSetting_btnLine'></div>"+
            "</div>" +
            "<div id='beaconSetting_stepArea'>" +
            "<img class='beaconSetting_stepBG' src='images/setting/set_beacon_w350.png'>" +
            "<div class='beaconSetting_stepFocus_div'></div>" +
            "<div class='beaconSetting_stepFocus_div'></div>" +
            "<div class='beaconSetting_stepFocus_div'></div>" +
            "<div class='beaconSetting_stepFocus_div'></div>" +
            "<div class='beaconSetting_stepFocus_div'></div>" +
            "</div>" +
            "<div id='beaconSetting_plusBtn'>" +
            "<img class='beaconSetting_btnBG' src='images/pop_btn_w100.png'>" +
            "<span class='beaconSetting_btnText'>+</span>"+
            "<div class='beaconSetting_btnLine'></div>"+
            "</div>" +
            "<div id='beaconSetting_settingValue'>-1dBm</div>" +
            "<div id='beaconSetting_infoDiv_0'>*세기 범위 : -5dBm~+10dBm까지 설정 가능</div>" +
            "</div>" +
            "<div id='beaconSetting_div_1'>" +
            "<div class='beaconSetting_title'>송신 주기</div>" +
            "<div class='beaconSetting_subTitle'>비콘 송신 주기를 설정합니다</div>" +
            "<div id='beaconSetting_inputArea'>" +
            "<div class='beaconSetting_inputTitleDiv' style='position: relative; width: 158px; height: 65px; float: left;'>" +
                "<span class='beaconSetting_inputTitle'>주기 변경</span>" +
            "</div>" +
            "<div class='beaconSetting_inputDiv'>" +
            "<div class='beaconSetting_inputBG' style='width: 330px; height: 65px;'></div>" +
            "<div id='beaconSetting_inputValue'>760 ms</div>" +
            "</div>" +
            "</div>" +
            "<div id='beaconSetting_infoDiv_1'>*주기 시간 :</div>" +
            "<div id='beaconSetting_infoContents'>20ms(0.02초)~10000ms(10초)까지 설정 가능<br>20ms 단위로 설정 가능 (ex.220,240,260ms…)</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>비콘 송신 세기와 주기를<br>설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function readConfiguration() {
            log.printDbg("readConfiguration()");
            var sending_power_value;
            var sending_interval_value;
            var beaconData;

            beaconData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_BEACON,"specific");

            sending_power_value = beaconData[0];
            sending_interval_value = beaconData[1];

            log.printDbg("sending_power_value::::::"+sending_power_value);
            settingValue[0] = sending_power_value; // 송신 세기
            sending_power_value++;
            INSTANCE.div.find("#beaconSetting_stepArea .beaconSetting_stepFocus_div").removeClass("show");
            for (var idx = 0 ; idx < sending_power_value; idx ++) {
                log.printDbg("idx ==== "+idx);
                INSTANCE.div.find("#beaconSetting_stepArea .beaconSetting_stepFocus_div:eq(" + idx + ")").addClass("show");
            }
            INSTANCE.div.find("#beaconSetting_settingValue").text(stepValue[settingValue[0]]);

            settingValue[1] = parseInt(sending_interval_value); // 송신 주기
            INSTANCE.div.find("#beaconSetting_inputValue").text(sending_interval_value + " ms");
            inputNumber = sending_interval_value;
            log.printDbg("sending_interval_value::::::"+sending_interval_value);
        }

        function saveConfiguration(){
            var intervalValue;
            var powerValue;

            intervalValue = settingValue[1].toString();
            powerValue = settingValue[0].toString(); //  parseInt(settingValue[0]);
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.BEACON.POWER_LEVEL, powerValue);
            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.BEACON.INTERVAL, intervalValue);


            log.printDbg("Beacon Rate ="+settingValue[1]+" Power ="+powerValue);
        }
    };

    KTW.ui.layer.setting.setting_beacon.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_beacon.constructor = KTW.ui.layer.setting.setting_beacon;

    KTW.ui.layer.setting.setting_beacon.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_beacon.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();