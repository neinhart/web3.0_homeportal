/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > HDMI 전원 동기화
 *
 * */

(function () {

    KTW.ui.layer.setting.setting_hdmiSynchronize  = function HdmiSynchronize(options) {
        KTW.ui.Layer.call(this, options);

        var INSTANCE = this;
        var styleSheet;
        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var data;

        var clockArea;
        var clock;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame hdmiSynchronize"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<span id='backgroundTitle'>HDMI 전원 동기화</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
            createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            readConfiguration();
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_HDMI_CEC) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


         //   clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
         //   clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    if (menuIdx == 1) buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    if (menuIdx == 1) buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        if(focusIdx ==0)
                            LayerManager.historyBack();
                        else
                            buttonFocusRefresh(KTW.utils.util.indexMinus(focusIdx, 2), menuIdx);

                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        if (focusIdx  == 1) {
                            focusIdx = 0;
                            buttonFocusRefresh(focusIdx, 1);
                        } else {
                            buttonFocusRefresh(KTW.utils.util.indexPlus(focusIdx, 2), menuIdx);
                        }
                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    setSelectedButton(index);
                    selectedIdx = index;
                    /*                    if (focusIdx == 1) {
                     INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").addClass("disable");
                     }*/
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장
                        saveConfiguration();
                        data.complete();
                        settingMenuManager.historyBackAndPopup();
                        break;
                    }
                    LayerManager.historyBack();
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");

                    if(index == 1) {
                        INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").addClass("disable");
                    } else {
                        INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").removeClass("disable");
                    }
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    if(selectedIdx == 1) {
                        INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").addClass("disable");
                    } else {
                        INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").removeClass("disable");
                    }
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                    "<div class='table_title'>HDMI 전원 동기화</div>" +
                    "<div class='table_subTitle'>ON으로 설정하면 TV와 셋톱박스의 전원 관리가 간편해집니다</div>" +
                    "<div id='table_radio_area'>" +
                        "<div class='table_radio_btn'>" +
                            "<div class='table_radio_btn_bg'></div>" +
                            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                            "<div class='table_radio_text'>ON</div>" +
                        "</div>" +
                        "<div class='table_radio_btn' style='left: 285px;'>" +
                            "<div class='table_radio_btn_bg'></div>" +
                            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                            "<div class='table_radio_text'>OFF</div>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
                "<div id='table_itemInfo_div'>" +
                    "<div class='setOption_info_div'>" +
                        "<img class='check_img' src='images/setting/set_check.png'>" +
                        "<div class='setOption_info_text'>셋톱박스 전원을 켜면, TV 전원을 자동으로 켜기</div>" +
                    "</div>" +
                    "<div class='setOption_info_div'>" +
                        "<img class='check_img' src='images/setting/set_check.png'>" +
                        "<div class='setOption_info_text'>셋톱박스 전원을 끄면, TV 전원을 자동으로 끄기</div>" +
                    "</div>" +
                    "<div class='setOption_info_div'>" +
                        "<img class='check_img' src='images/setting/set_check.png'>" +
                        "<div class='setOption_info_text'><p>TV 전원을 끄면, 셋톱박스 전원을 자동으로 끄기</p><p>단, TV 전원을 켜도 셋톱박스 전원은 자동으로 켜지지 않습니다</p></div>" +
                    "</div>" +
                    "<div class='setOption_subInfo'>*TV모델에 따라 해당 기능이 지원되지 않을 수 있습니다</div>" +
                "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>TV와 셋톱박스가 HDMI<br>케이블로 연결되어 있을 경우<br>전원 동기화를 설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
                /*            "<img class='rightMenu_btn_img' src='images/btn_set_w282.png'>" +*/
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
                /*            "<img class='rightMenu_btn_img' src='images/btn_set_w282.png'>" +*/
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 설정된 값을 읽어온다.
         * OTS와 OTV를 다르게 처리해야 한다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var adapterData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_HDMI_CEC);
            var hdmi_cec;
            var hdmi_cec_fn;

            try {
                if (KTW.CONSTANT.IS_OTS) {
                    hdmi_cec = adapterData[0];
                } else {
                    hdmi_cec_fn = adapterData[1];
                    if (hdmi_cec_fn === "0") {
                        hdmi_cec = "false";
                    } else {
                        hdmi_cec = "true";
                    }
                }
            } catch (error) {
                log.printErr("readConfiguration(), error = " + error.message);
                hdmi_cec = "false";
            }

            if (hdmi_cec === "true") {
                focusIdx = 0;
            } else {
                focusIdx = 1;
            }
            selectedIdx = focusIdx;
            buttonFocusRefresh(focusIdx, 0);
            setSelectedButton(focusIdx);

            /*            if(focusIdx == 1) {
             INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").addClass("disable");
             }*/

        }

        /**
         * 사용자가 선택한 설정 값을 저장한다.
         * OTS 와 OTV 를 다르게 처리해야 한다.
         */
        function saveConfiguration() {
            log.printDbg("saveConfiguration(), selectedIdx = " + selectedIdx);

            var hdmi_cec;

            if (selectedIdx === 0) {
                hdmi_cec = "true";
            } else {
                hdmi_cec = "false";
            }
            try {
                if (KTW.CONSTANT.IS_OTS) {
                    basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.HDMI_CEC, hdmi_cec);
                }

                if (selectedIdx === 0) {
                    basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.HDMI_CEC_FN, "14");
                } else {
                    basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.HDMI_CEC_FN, "0");
                }
            } catch (error) {
                log.printErr("saveConfiguration(), error = " + error.message);
            }
        }

        function setSelectedButton(index) {
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            //INSTANCE.div.find("#contents #settingTable #table_itemInfo_div").removeClass("disable");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }
    };

    KTW.ui.layer.setting.setting_hdmiSynchronize.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_hdmiSynchronize.constructor = KTW.ui.layer.setting.setting_hdmiSynchronize;

    KTW.ui.layer.setting.setting_hdmiSynchronize.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_hdmiSynchronize.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();