/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 자녀안심설정 > 시청시간 제한 설정
 */

(function () {
    KTW.ui.layer.setting.setting_limitWatch  = function LimitWatch(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;
        var log = KTW.utils.Log;

        var focusYIdx = 0;
        var focusXIdx = 0;
        var menuIdx = 0;

        var blockTimeInfo = null;

        var blockTImeOnOffIndex = 1;
        var blockTImeRepeatIndex = 0;
        var blockStartTimeHH = "00";
        var blockStartTimemm = "00";
        var blockEndTimeHH = "00";
        var blockEndTimemm = "00";

        // 0 or 1, 입력받은 시간 값을 처리할 위치.
        var loc = 0;

        var historybackMenuIdx = 0;
        var historybackYIdx = 0;

        var data;

        var clock;
        var clockArea;

        this.init = function (cbCreate) {
            log.printDbg("init()");
            this.div.attr({class: "arrange_frame limitWatch"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>시청시간 제한</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.showView = function (options) {
            log.printDbg("showView()");
            /**
             * 설정 정보 얻어 옴
             */
            _readInfo();

            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            selectIconRefresh(blockTImeOnOffIndex, 0, 0);
            selectIconRefresh(blockTImeRepeatIndex, 0, 2);

            focusXIdx = blockTImeOnOffIndex;
            focusYIdx = 0;
            menuIdx = 0;

            INSTANCE.div.find("#contents #table_input_area #start_input_hour").text(blockStartTimeHH);
            INSTANCE.div.find("#contents #table_input_area #start_input_min").text(blockStartTimemm);
            INSTANCE.div.find("#contents #table_input_area #end_input_hour").text(blockEndTimeHH);
            INSTANCE.div.find("#contents #table_input_area #end_input_min").text(blockEndTimemm);

            buttonFocusRefresh(focusXIdx, focusYIdx, menuIdx);
            //console.log("show LimitWatch");
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_TIME_LIMIT) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


        //    clock.show(clockArea);
        };

        this.hideView = function (options) {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
        //    clock.hide();

            if(options === undefined) {
                KTW.managers.auth.AuthManager.initPW();
                KTW.managers.auth.AuthManager .resetPWCheckCount();
            }
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    if(menuIdx === 0 ) {
                        if(blockTImeOnOffIndex === 0) {
                            buttonFocusRefresh(blockTImeRepeatIndex, 0, 2);
                        }
                    }else if(menuIdx === 1) {
                        if(focusYIdx === 1) {
                            buttonFocusRefresh(0, 0, 1);
                        }else {
                            buttonFocusRefresh(0, 0, 0);
                        }
                    }else if(menuIdx === 2) {
                        buttonFocusRefresh(0, 1, 1);
                    }else if(menuIdx === 3) {
                        if(focusYIdx ===0) {
                            focusYIdx = 1;
                        }else {
                            focusYIdx = 0;
                        }
                        buttonFocusRefresh(0, focusYIdx, menuIdx);
                    }
                    loc = 0;
                    return true;
                case KEY_CODE.DOWN:
                    if(blockTImeOnOffIndex === 0) {
                        if(menuIdx ===0) {
                            buttonFocusRefresh(0, 0, 1);
                        }else if(menuIdx === 1){
                            if(focusYIdx === 0) {
                                buttonFocusRefresh(0, 1, 1);
                            }else {
                                buttonFocusRefresh(blockTImeRepeatIndex, 0, 2);
                            }
                            loc = 0;
                        }else if(menuIdx === 2) {
                            buttonFocusRefresh(0, 0, 0);
                        }
                        else if(menuIdx===3) {
                            if(focusYIdx ===0) {
                                focusYIdx = 1;
                            }else {
                                focusYIdx = 0;
                            }
                            buttonFocusRefresh(0, focusYIdx, menuIdx);
                        }
                    }else {
                        if(menuIdx===3) {
                            if(focusYIdx ===0) {
                                focusYIdx = 1;
                            }else {
                                focusYIdx = 0;
                            }
                            buttonFocusRefresh(0, focusYIdx, menuIdx);
                        }
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 3)  {
                        if(blockTImeOnOffIndex === 1){
                            buttonFocusRefresh(blockTImeOnOffIndex, 0, 0);
                            return true;
                        }
                        if(historybackMenuIdx === 0) {
                            buttonFocusRefresh(blockTImeOnOffIndex, 0, historybackMenuIdx);
                        }else if(historybackMenuIdx === 2) {
                            buttonFocusRefresh(blockTImeRepeatIndex, 0, historybackMenuIdx);
                        }else {
                            buttonFocusRefresh(0, historybackYIdx, historybackMenuIdx);
                        }
                    } else {
                        if(focusXIdx === 0) {
                            LayerManager.historyBack();
                            return true;
                        }
                        if(menuIdx === 1) {
                            if(focusXIdx === 1) {
                                if(focusYIdx === 0) {
                                    INSTANCE.div.find("#contents #table_input_area #start_input_hour").addClass("input_focus");
                                    INSTANCE.div.find("#contents #table_input_area #start_input_min").removeClass("input_focus");
                                }else {
                                    INSTANCE.div.find("#contents #table_input_area #end_input_hour").addClass("input_focus");
                                    INSTANCE.div.find("#contents #table_input_area #end_input_min").removeClass("input_focus");
                                }
                                focusXIdx = 0;
                                loc = 0;
                            }
                        }else {
                            buttonFocusRefresh(KTW.utils.util.indexMinus(focusXIdx, 2), focusYIdx, menuIdx);
                        }
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if ((menuIdx == 0 && focusXIdx == 1) || menuIdx == 1 || (menuIdx == 2 && focusXIdx == 1)) {
                        if(menuIdx === 1) {
                            if(focusXIdx === 0) {
                                if(focusYIdx === 0) {
                                    INSTANCE.div.find("#contents #table_input_area #start_input_hour").removeClass("input_focus");
                                    INSTANCE.div.find("#contents #table_input_area #start_input_min").addClass("input_focus");
                                }else {
                                    INSTANCE.div.find("#contents #table_input_area #end_input_hour").removeClass("input_focus");
                                    INSTANCE.div.find("#contents #table_input_area #end_input_min").addClass("input_focus");
                                }
                                focusXIdx = 1;
                                loc = 0;
                            }else {
                                historybackMenuIdx = menuIdx;
                                historybackYIdx = focusYIdx;
                                buttonFocusRefresh(focusXIdx, 0, 3);
                            }
                        }else {
                            historybackMenuIdx = menuIdx;
                            historybackYIdx = focusYIdx;
                            buttonFocusRefresh(focusXIdx, 0, 3);
                        }
                    } else {
                        buttonFocusRefresh(KTW.utils.util.indexPlus(focusXIdx, 2), focusYIdx, menuIdx);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    selectIconRefresh(focusXIdx, focusYIdx, menuIdx);
                    return true;
                case KEY_CODE.NUM_0:
                case KEY_CODE.NUM_1:
                case KEY_CODE.NUM_2:
                case KEY_CODE.NUM_3:
                case KEY_CODE.NUM_4:
                case KEY_CODE.NUM_5:
                case KEY_CODE.NUM_6:
                case KEY_CODE.NUM_7:
                case KEY_CODE.NUM_8:
                case KEY_CODE.NUM_9:
                    var number = KTW.utils.util.keyCodeToNumber(key_code);
                    if (menuIdx == 1) {
                        _keyNumber(number);
                    }
                    return true;
                default:
                    return false;
            }
        };


        function selectIconRefresh(index_X, index_Y, menuIndex) {
            log.printDbg("selectIconRefresh() index_X : " + index_X + " , index_Y : " + index_Y + " , menuIndex : " + menuIndex);
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn .table_radio_img").removeClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(0) .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    if (index_X == 0) {
                        INSTANCE.div.find("#contents #settingTable .table_item_div").removeClass("disable");
                        buttonFocusRefresh(0, 0, 1);
                    }
                    if (index_X == 1) {
                        INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) ").addClass("disable");
                        INSTANCE.div.find("#contents #settingTable .table_item_div:eq(2) ").addClass("disable");
                        buttonFocusRefresh(0, 0, 3);
                        INSTANCE.div.find("#contents #table_input_area #start_input_hour").text("00");
                        INSTANCE.div.find("#contents #table_input_area #start_input_min").text("00");

                        INSTANCE.div.find("#contents #table_input_area #end_input_hour").text("00");
                        INSTANCE.div.find("#contents #table_input_area #end_input_min").text("00");
                    }

                    blockTImeOnOffIndex = index_X;
                    break;
                case 1:
                    if(focusYIdx == 0) {
                        focusYIdx = 1;
                        focusXIdx = 0;
                        buttonFocusRefresh(focusXIdx, focusYIdx, 1);
                    }else {
                        // 다음 메뉴로
                        focusXIdx = blockTImeRepeatIndex;
                        focusYIdx = 0;
                        buttonFocusRefresh(focusXIdx, focusYIdx, 2);
                    }
                    break;
                case 2:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn .table_radio_img").removeClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area:eq(1) .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");

                    blockTImeRepeatIndex = index_X;
                    historybackMenuIdx = menuIdx;
                    historybackYIdx = focusYIdx;
                    focusXIdx = 0;
                    focusYIdx = 0;
                    buttonFocusRefresh(focusXIdx, focusYIdx, 3);
                    break;
                case 3:
                    if (focusYIdx == 0) {
                        // 시스템 저장
                        _setLimitTime(blockTImeOnOffIndex === 0 ? true : false , blockTImeRepeatIndex === 0 ? false : true ,
                            blockStartTimeHH+":"+blockStartTimemm , blockEndTimeHH + ":" +blockEndTimemm);

                        data.complete();
                        settingMenuManager.historyBackAndPopup();
                    } else {
                        LayerManager.historyBack();
                    }


                    break;
            }
        }

        function buttonFocusRefresh(index_X, index_Y, menuIndex) {
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");

            INSTANCE.div.find("#contents #table_input_area .table_input_div .table_input_img").removeClass("focus");
            INSTANCE.div.find("#contents #table_input_area #table_input_value1").removeClass("focus");
            INSTANCE.div.find("#contents #table_input_area #table_input_value2").removeClass("focus");

            INSTANCE.div.find("#contents #table_input_area #start_input_hour").removeClass("focus");
            INSTANCE.div.find("#contents #table_input_area #start_input_min").removeClass("focus");
            INSTANCE.div.find("#contents #table_input_area #end_input_hour").removeClass("focus");
            INSTANCE.div.find("#contents #table_input_area #end_input_min").removeClass("focus");

            INSTANCE.div.find("#contents #table_input_area #start_input_hour").removeClass("input_focus");
            INSTANCE.div.find("#contents #table_input_area #start_input_min").removeClass("input_focus");
            INSTANCE.div.find("#contents #table_input_area #end_input_hour").removeClass("input_focus");
            INSTANCE.div.find("#contents #table_input_area #end_input_min").removeClass("input_focus");

            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    focusXIdx = index_X;
                    INSTANCE.div.find("#contents #table_radio_area:eq(0) .table_radio_btn:eq(" + index_X + ")").addClass("focus");
                    INSTANCE.div.find("#contents #table_radio_area:eq(0) .table_radio_btn:eq(" + index_X + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #table_radio_area:eq(0) .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #table_radio_area:eq(0) .table_radio_btn:eq(" + index_X + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    focusXIdx = index_X;
                    focusYIdx = index_Y;
                    INSTANCE.div.find("#contents #table_input_area .table_input_div:eq(" + index_Y + ") .table_input_img").addClass("focus");
                    if (index_Y == 0) {

                        INSTANCE.div.find("#contents #table_input_area #table_input_value1").addClass("focus");
                        INSTANCE.div.find("#contents #table_input_area #start_input_hour").addClass("focus");
                        INSTANCE.div.find("#contents #table_input_area #start_input_min").addClass("focus");
                        if(focusXIdx === 0) {
                            INSTANCE.div.find("#contents #table_input_area #start_input_hour").addClass("input_focus");
                        }else {
                            INSTANCE.div.find("#contents #table_input_area #start_input_min").addClass("input_focus");
                        }
                    } else {
                        INSTANCE.div.find("#contents #table_input_area #table_input_value2").addClass("focus");
                        INSTANCE.div.find("#contents #table_input_area #end_input_hour").addClass("focus");
                        INSTANCE.div.find("#contents #table_input_area #end_input_min").addClass("focus");
                        if(focusXIdx === 0) {
                            INSTANCE.div.find("#contents #table_input_area #end_input_hour").addClass("input_focus");
                        }else {
                            INSTANCE.div.find("#contents #table_input_area #end_input_min").addClass("input_focus");
                        }
                    }
                    break;
                case 2:
                    focusXIdx = index_X;
                    INSTANCE.div.find("#contents #table_radio_area:eq(1) .table_radio_btn:eq(" + index_X + ")").addClass("focus");
                    INSTANCE.div.find("#contents #table_radio_area:eq(1) .table_radio_btn:eq(" + index_X + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #table_radio_area:eq(1) .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #table_radio_area:eq(1) .table_radio_btn:eq(" + index_X + ") .table_radio_text").addClass("focus");
                    break;
                case 3:
                    focusYIdx = index_Y;
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index_Y + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index_Y + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index_Y + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +

            "<div class='table_item_div' style='position: absolute; width: 1247px; height: 245px;'>" +
            "<div class='table_title'>시청시간 제한</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='position: absolute; left: 0px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>설정</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='position: absolute; left: 285px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img select' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>해제</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='table_divisionLine'style='position: absolute; top:241px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style='position: absolute; top: 243px; width: 1247px; height: 331px;'>" +
            "<div class='table_subTitle'>시간 입력</div>" +
            "<div class='table_infoText'>원하는 시간을 직접 입력해 주세요 (예. 22:10)</div>" +
            "<div id='table_input_area' >" +
            "<div class='table_input_div'>" +
            "<div class='table_input_text'>제한 시작 시각</div>" +
            "<div class='table_input_img'></div>" +
            "<div id='table_input_value1' class='input_text'>:</div>" +
            "<div id='start_input_hour'>00:</div>" +
            "<div id='start_input_min'>00</div>" +
            "</div>" +
            "<div class='table_input_div' style = 'top: 75px;'>" +
            "<div class='table_input_text'>제한 종료 시각</div>" +
            "<div class='table_input_img'></div>" +
            "<div id='table_input_value2' class='input_text'>:</div>" +
            "<div id='end_input_hour'>00:</div>" +
            "<div id='end_input_min'>00</div>" +
            "</div>" +
            "</div>" +
            "</div>" +

            "<div class='table_divisionLine'style='position: absolute; top:591px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
            "<div class='table_item_div' style='position: absolute; top: 593px; width: 1247px; height: 213px;'>" +
            "<div class='table_subTitle'>반복 설정</div>" +
            "<div id='table_radio_area' style = 'margin-top: 95px;'>" +
            "<div class='table_radio_btn'style='position: absolute; left: 0px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>한번만</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='position: absolute; left: 285px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>매일</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");


            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>" + (KTW.CONSTANT.IS_OTS ? "설정된 시간 동안 olleh tv<br>이용을 제한합니다":"설정된 시간 동안 olleh tv<br>이용을 제한합니다") + "</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function _readInfo() {

            blockTimeInfo  = _getLimitTime();

            if (blockTimeInfo !== undefined && blockTimeInfo !== null && blockTimeInfo[0] === false) {
                blockTImeOnOffIndex = 1;
                blockTImeRepeatIndex = 0;
                blockStartTimeHH = "00";
                blockStartTimemm = "00";
                blockEndTimeHH = "00";
                blockEndTimemm = "00";

            } else {
                // [jh.lee] 시청 제한 설정이 설정 인 경우
                try {
                    blockTImeOnOffIndex = 0;

                    if (blockTimeInfo[1] === true) {
                        blockTImeRepeatIndex = 1;
                    }

                    blockStartTimeHH = blockTimeInfo[2].substring(0, 2);
                    blockStartTimemm = blockTimeInfo[2].substring(3);
                    blockEndTimeHH = blockTimeInfo[3].substring(0, 2);
                    blockEndTimemm = blockTimeInfo[3].substring(3);
                } catch (e) {
                    log.printExec(e);
                }
            }
        }

        /**
         * 시청시간제한 정보를 가져와 파싱
         */
        function _getLimitTime() {
            var block_time = [];
            var limit_time;
            var split_value;

            // [jh.lee] 제한시간 디폴트 값을 설정
            /**
             *  Index 0 : 시청시간 제한 설정 On/Off
             *  Index 1 : 반복 설정 [ false : 한번만 , true : 매일]
             *  Index 2 : 제한 시작 시간
             *  Index 3 : 제한 종료 시간
             */
            block_time[0] = false;
            block_time[1] = false;
            block_time[2] = "0000";
            block_time[3] = "0000";

            try {

                var settingData = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_TIME_LIMIT);
                if(settingData !== undefined && settingData !== null && settingData.length === 1) {
                    limit_time = settingData[0];
                }else {
                    return block_time;
                }
                /**
                 *  N|20080411|18:04|18:10|1|Y
                 *  해지(N) 여부 |오늘 날짜 | 시작시간 | 끝시간 | 0: 매일, 1:한번만 | 설정(Y),해지(N) 여부
                 *  시작시간,끝시간 : HHMM
                 */
                // [jh.lee] split_value[0] : 시청 제한 설정
                // [jh.lee] split_value[4] : 반복 설정
                split_value = limit_time.split("|");
                block_time[0] = (split_value[0] === "Y") ? true : false;
                block_time[1] = (split_value[4] === "0") ? true : false;
                block_time[2] = split_value[2];
                block_time[3] = split_value[3];

                block_time[0] = (split_value[5] === "Y") ? true : block_time[0];

                return block_time;

            } catch(e) {
                log.printExec(e.message);
                return block_time;
            }
        }

        function _generateTime(hour, current_value, num) {
            // 숫자를 입력하고 나서 loc 값이 0 이면 포커스 이동
            if (num === null || num === undefined) {
                return current_value;
            }

            if (loc === 0) {
                if (hour) {
                    // 시간 영역에서 0,1,2를 제외한 숫자가 입력되면 앞은 0으로 자동 채움.
                    if (num > 2) {
                        current_value = "0" + num;
                    } else {
                        current_value = num + "0";
                        loc++;
                    }
                } else {
                    // 0~5 를 제외한 숫자가 입력될 경우 앞은 0 으로 자동 채움.
                    if (num > 5) {
                        current_value = "0" + num;
                    } else {
                        current_value = num + "0";
                        loc++;
                    }
                }
            }
            else if (loc === 1) {
                if (hour) {
                    if (current_value === "20") {
                        log.printDbg("num === "+num);
                        // 시간 영역에서 2를 먼저 입력했을 경우 0~4 까지만 입력 가능
                        if (num > 4) {
                            log.printDbg("num === "+num);
                            return current_value;
                        } else if (num === 4) {
                            // 24 입력시 00으로 자동 변경.
                            log.printDbg("num === "+num);
                            current_value = "00";
                            loc = 0;
                        } else {
                            current_value = "2" + num;
                            loc = 0;
                        }
                    } else {
                        log.printDbg("num === "+num);
                        current_value = current_value[0] + num;
                        loc = 0;
                    }
                } else {
                    current_value = current_value[0] + num;
                    loc = 0;
                }
            }


            return current_value;
        }

        function _keyNumber(number) {
            if (menuIdx === 1) {
                if(number >= 0 && number <= 9) {
                    // [jh.lee] 0~9 숫자가 입력 된 경우
                    if (focusYIdx === 0) {
                        if (focusXIdx === 0) {
                            blockStartTimeHH = _generateTime(true, blockStartTimeHH, number);
                            if (loc === 0) {
                                focusXIdx = 1;
                                buttonFocusRefresh(focusXIdx, focusYIdx, menuIdx);
                            }
                            INSTANCE.div.find("#contents #table_input_area #start_input_hour").text(blockStartTimeHH);
                        } else {
                            blockStartTimemm = _generateTime(false, blockStartTimemm, number);
                            if (loc === 0) {
                                focusXIdx = 0;
                                focusYIdx = 1;
                                /**
                                 * Input box 이동해야 함.
                                 */
                                buttonFocusRefresh(focusXIdx, focusYIdx, menuIdx);
                            }
                            INSTANCE.div.find("#contents #table_input_area #start_input_min").text(blockStartTimemm);
                        }
                    } else if(focusYIdx === 1) {
                        if (focusXIdx== 0) {
                            blockEndTimeHH = _generateTime(true, blockEndTimeHH, number);
                            if (loc === 0) {
                                focusXIdx = 1;
                                buttonFocusRefresh(focusXIdx, focusYIdx, menuIdx);
                            }
                            INSTANCE.div.find("#contents #table_input_area #end_input_hour").text(blockEndTimeHH);
                        } else {
                            blockEndTimemm = _generateTime(false, blockEndTimemm, number);
                            if (loc === 0) {
                                focusXIdx = 0;
                                focusYIdx =0;
                                menuIdx = 2;
                                /**
                                 * 반복설정 쪽으로 포커스 이동 해야 함.
                                 */
                                buttonFocusRefresh(focusXIdx, focusYIdx, menuIdx);
                            }
                            INSTANCE.div.find("#contents #table_input_area #end_input_min").text(blockEndTimemm);
                        }
                    }
                }
            }
        }

        /**
         * 시청제한 시간  설정값을 저장한다.
         *
         *          N|20080411|18:04|18:10|1 => 변경  N|20080411|18:04|18:10|1|Y
         *          설정(Y),해지(N) 여부 |오늘 날짜 | 시작시간 | 끝시간 | 0: 매일, 1:한번만
         *          해지(N) 여부 |오늘 날짜 | 시작시간 | 끝시간 | 0: 매일, 1:한번만 | 설정(Y),해지(N) 여부
         *          설정 해지무조건 N으로 변경하고 뒤로 옮김으로 서  옵저버에서 처리하도록 수정.
         *
         * @param enabled 설정여부 repeat 반복여부 starttime 시작시간 endtime 종료시간
         * @return
         * @type
         */
        function _setLimitTime(enabled, repeat, start_time, end_time) {
            var start_t;
            var end_t;
            var save_value = "";
            //var current_date;
            //var setTomorrow = false;
            var current_date;

            start_t = start_time;
            end_t = end_time;

            if (enabled === false || start_t === end_t) {
                // [jh.lee] 시청 시간 제한 설정이 해제이거나 시작, 끝 시간이 같은 경우
                start_t = "00:00";
                end_t = "00:00";
                blockStartTimeHH = "00";
                blockStartTimemm = "00";
                blockEndTimeHH = "00";
                blockEndTimemm = "00";
            }

            // 2017.09.28 dhlee
            // 한번만으로 설정되어 있는 상태인데 종료 시간이 현재시간보다 이전 시간인 경우 저장하는 날짜에 하루를 더해서 저장하도록 한다.
            // 만약 이렇게 하지 않으면 초기화 로직에 의해 설정한 값이 초기화 되어버리기 때문이다.
            current_date = new Date();
            if (!repeat) {
                var nowDate = new Date();
                var nowHour = nowDate.getHours();
                var nowMin = nowDate.getMinutes();
                var startHour = parseInt(start_t.substr(0, 2));
                var startMin = parseInt(start_t.substr(3, 2));
                var endHour = parseInt(end_t.substr(0, 2));
                var endMin = parseInt(end_t.substr(3, 2));
                log.printDbg("_setLimitTime(), nowHour = " + nowHour + ", nowMin = " + nowMin + ", startHour = "+startHour+ ", startMin = "+startMin+",  endHour = " + endHour + ", endMin = " + endMin);
                // 2017.12.06 sw.nam
                // 종료 시간이 시작 시간 보다 작은 경우 (다음 날까지 제한이 걸리도록 사용자가 의도한 경우) 에는 날짜를 다음날로 바꾸지 않는다 (WEBIIIHOME-3606)
                // 아래 두가지 케이스
                // 현재시간 > 종료시간 > 시작시간 --- > 날짜 +1 시킨다
                // 현재시간 > 시작시간 > 종료시간 --- > 날짜 + 1 시키지 않는다
                if ( !(startHour > endHour || (startHour === endHour && startMin > endMin)) && nowHour > endHour || (nowHour === endHour && nowMin >= endMin)) {
                    current_date.setDate(current_date.getDate() + 1);
                }

            }
            log.printDbg("_setLimitTime(), current_date = " + current_date);
            // [jh.lee] "N|20080411|18:04|18:10|1|Y
            // [jh.lee] 무조건 "N"으로 설정. 옵저버가 변경한다.
            save_value = "N";
            save_value += "|" + KTW.utils.util.getFormatDate(current_date, "yyyyMMdd");
            save_value += "|" + start_t + "|" + end_t;
            save_value += repeat ? "|0" : "|1";

            if (start_t === end_t) {
                save_value += "|N";
            } else {
                save_value += enabled ? "|Y" : "|N";
            }
            log.printDbg("_setLimitTime() save_value = "+ save_value);
            // [jh.lee] 시청시간제한 데이터  미들웨어에 저장
            KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.RESTRICTION_TIME, save_value);
        }

    };
    KTW.ui.layer.setting.setting_limitWatch.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_limitWatch.constructor = KTW.ui.layer.setting.setting_limitWatch;

    KTW.ui.layer.setting.setting_limitWatch.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_limitWatch.prototype.show = function (options) {
        KTW.ui.Layer.prototype.show.call(this, options);

        this.showView(options);
    };

    KTW.ui.layer.setting.setting_limitWatch.prototype.hide = function(options) {
        this.hideView(options);
        KTW.ui.Layer.prototype.hide.call(this, options);
    };

    KTW.ui.layer.setting.setting_limitWatch.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();