/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 메뉴 언어
 */

(function () {
    KTW.ui.layer.setting.setting_defaultLanguage  = function DefaultLanguage(options) {
        KTW.ui.Layer.call(this, options);
        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var LayerManager = KTW.ui.LayerManager;
        var Layer = KTW.ui.Layer;
        var log = KTW.utils.Log;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;

        var settingDataAdaptor = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var data;

        var clock;
        var clockArea;

        var LANG = {
            KOR : "kor",
            ENG : "eng"
        };

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame defaultLanguage"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>메뉴 언어</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

           // KTW.ui.LayerManager.stopLoading();

            readConfiguration();

            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.LANGUAGE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    return true;
                case KEY_CODE.ENTER:
       /*             if(LayerManager.isShowLoading()){
                        return true;
                    }*/
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:

                    setSelectedButton(index);
                    selectedIdx = index;
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장
                        saveConfiguration();
                    } else {
                        LayerManager.historyBack();
                    }

                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>메뉴 언어</div>" +
                "<div class='table_subTitle'>주요 메뉴 및 기본 카테고리의 언어를 변경할 수 있습니다</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn' style='top:20px;'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                "<div class='table_radio_text'>한국어</div>" +
                "</div>" +
                "<div class='table_radio_btn' style='top: 117px;'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>English</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>주요 메뉴에 표시되는<br> 언어를 설정합니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 설정된 값을 읽어서 selectedIdx 값을 set 한다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var language = settingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.LANGUAGE);

           if (language[0] === LANG.KOR) {
               focusIdx =0;
            } else {
               focusIdx = 1;
            }
            selectedIdx = focusIdx;
            buttonFocusRefresh(focusIdx, 0);
            setSelectedButton(focusIdx);
        }

        /**
         * 설정된 값을 write 한다.
         * TODO 2017.02.27 dhlee
         * TODO 기본 언어를 변경하는 경우 노출되는 메뉴명 등을 영문으로 변경해야 하므로 loading이 필요할 수 있다.
         */
        function saveConfiguration() {
            log.printDbg("saveConfiguration()");

            var language;
            //stopFlag = false;

            KTW.ui.LayerManager.startLoading({
                preventKey: true
            });

            if (selectedIdx === 0) {
                language = LANG.KOR;
            } else {
                language = LANG.ENG;
            }

            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.LANGUAGE, language);

            setTimeout(function() {
                //readConfiguration();
                LayerManager.stopLoading();
                settingMenuManager.updateAllSettingMenuSubtitle();
                data.complete();
                settingMenuManager.historyBackAndPopup(KTW.managers.data.MenuDataManager.MENU_ID.LANGUAGE,null);
            }, 500);
        }

        function setSelectedButton(index) {
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
        }
    };

    KTW.ui.layer.setting.setting_defaultLanguage.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_defaultLanguage.constructor = KTW.ui.layer.setting.setting_defaultLanguage;

    KTW.ui.layer.setting.setting_defaultLanguage.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_defaultLanguage.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();