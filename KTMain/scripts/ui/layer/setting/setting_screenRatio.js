/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 화면 비율
 * TV 화면 비율과 영상 출력 방벙 2가지를 설정 기능을 제공한다.
 */

(function() {
    KTW.ui.layer.setting.setting_screenRatio  = function ScreenRate(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager =  KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;

        var focusXIdx = 0;
        var focusYIdx = 0;
        var lastFocusedYIdx;
        var maxXIdx = 1;
        var isRightFocus = false;

        var selectedRatio, selectedOutput;
        var data;

        var clock;
        var clockArea;


        this.init = function(cbCreate) {
            this.div.attr({class: "arrange_frame screenRate"});
            maxXIdx = KTW.CONSTANT.IS_OTS ? 2 : 1;

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });
            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>화면 비율</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );
            createComponent();
        //    createClock();
            cbCreate(true);
        };

        this.show = function(options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show ScreenRate");

            isRightFocus = false;
            removeFocus();
            readConfiguration();
            buttonFocusRefresh(focusXIdx, focusYIdx);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_ASPECT_RATIO) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }


        //    clock.show(clockArea);
        };

        this.hide = function() {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
       //     clock.hide();
        };

        this.controlKey = function(key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    if(!isRightFocus) {
                        if (focusYIdx ==  0) {
                            focusXIdx = selectedOutput;
                        }
                        else if(focusYIdx == 1) {
                            focusXIdx = selectedRatio;
                        }
                    }
                    buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, -1, 2));
                    return true;
                case KEY_CODE.DOWN:
                    if(!isRightFocus) {
                        if (focusYIdx ==  0) {
                            focusXIdx = selectedOutput;
                        }
                        else if(focusYIdx == 1) {
                            focusXIdx = selectedRatio;
                        }
                    }
                    buttonFocusRefresh(focusXIdx, KTW.utils.util.getIndex(focusYIdx, 1, 2));
                    return true;
                case KEY_CODE.LEFT:
                    if (isRightFocus) {
                        menuFocusChange();
                    } else {
                        if(focusXIdx ==0) {
                            LayerManager.historyBack();
                        }
                        else {

                            if(KTW.CONSTANT.IS_OTS) {
                                if(selectedRatio === 0) {
                                    maxXIdx = 2;
                                } else {
                                    maxXIdx = 1;
                                }
                            }
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, -1, (focusYIdx == 0 ? 2 : maxXIdx + 1)), focusYIdx);
                        }

                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if(isRightFocus) {
                        return true;
                    }

                    if(KTW.CONSTANT.IS_OTS) {
                        if(selectedRatio === 0) {
                            maxXIdx = 2;
                        } else {
                            maxXIdx = 1;
                        }
                    }

                    if (focusXIdx == (focusYIdx == 0 ? 1 : maxXIdx)) {
                        lastFocusedYIdx = focusYIdx;
                        menuFocusChange();
                    } else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusXIdx, 1, (focusYIdx == 0 ? 2 : maxXIdx + 1)), focusYIdx);
                    }

                    return true;
                case KEY_CODE.ENTER:
                    if (isRightFocus) {
                        if (focusYIdx == 0) {
                            saveConfiguration();
                          //  menuFocusChange();
                            settingMenuManager.historyBackAndPopup();
                            return;

                        }
                        menuFocusChange();
                        LayerManager.historyBack();
                    } else {
                        if (focusYIdx == 0) {
                            selectedRatio = focusXIdx;
                        }
                        else if (focusYIdx == 1) {
                            selectedOutput = focusXIdx;
                        }

                        selectIconRefresh(focusXIdx, focusYIdx);
                        if(KTW.CONSTANT.IS_OTS) {
                            if(selectedRatio == 0) {
                                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(2)").css({visibility: "inherit"});
                            } else{
                                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(2)").css({visibility: "hidden"});
                                if(selectedOutput == 2) {

                                    selectedOutput = 0;

                                    INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                                    INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn .table_radio_img").removeClass("select");
                                    INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(" + selectedOutput + ") .table_radio_img").addClass("select");
                                    INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(" + selectedOutput + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                                    buttonFocusRefresh(0,1);

                                }
                            }
                        }

                    }
                    return true;
                default:
                    return false;
            }
        };

        function menuFocusChange() {
            if (isRightFocus) {
                isRightFocus = false;
                switch(lastFocusedYIdx) {
                    case 0:
                        focusXIdx = selectedRatio;
                        break;
                    case 1:
                        focusXIdx = selectedOutput;
                        break;
                }
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                buttonFocusRefresh(focusXIdx, lastFocusedYIdx);
            } else {

                INSTANCE.div.find("#contents #settingTable .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_text").removeClass("focus");

                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0)").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_text").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_line").addClass("focus");
                focusYIdx = 0;
                isRightFocus = true;
            }
        }

        function selectIconRefresh(index_X, index_Y) {

            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");

            if (index_Y == 0) {
                focusXIdx = selectedOutput;
                buttonFocusRefresh(focusXIdx, 1);
            } else {
                lastFocusedYIdx = focusYIdx;
                menuFocusChange();
            }
        }

        function buttonFocusRefresh(index_X, index_Y) {
            focusXIdx = index_X;
            focusYIdx = index_Y;
            if (isRightFocus) {
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index_Y + ")").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index_Y + ") .rightMenu_btn_text").addClass("focus");
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index_Y + ") .rightMenu_btn_line").addClass("focus");
            } else {

                INSTANCE.div.find("#contents #settingTable .table_radio_btn").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
                INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_text").removeClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ")").addClass("focus");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                INSTANCE.div.find("#contents #settingTable .table_item_div:eq(" + index_Y + ") .table_radio_btn:eq(" + index_X + ") .table_radio_text").addClass("focus");

            }
        }
        function removeFocus() {
            log.printDbg("removeFocus()");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");

            INSTANCE.div.find("#contents #settingTable .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable .table_radio_btn .table_radio_text").removeClass("focus");
        }
        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div' style='position: absolute; width: 1247px; height: 350px;'>" +
                    "<div class='table_title'>TV 화면 비율 설정</div>" +
                    "<div class='table_subTitle'>16:9비율은 와이드 TV에서 사용 가능합니다</div>" +
                    "<div id='table_radio_area'>" +
                        "<div class='table_radio_btn'>" +
                            "<div class ='table_radio_btn_bg'></div>"+
                            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                            "<div class='table_radio_text'>4:3</div>" +
                        "</div>" +
                        "<div class='table_radio_btn' style='left: 285px;'>" +
                            "<div class ='table_radio_btn_bg'></div>"+
                            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                            "<div class='table_radio_text'>16:9</div>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
                "<div class='table_divisionLine'style='position: absolute; top:411px; width: 1247px; height: 2px; background-color: rgba(255,255,255,0.1); '></div>"+
                "<div class='table_item_div'style='position: absolute; top:413px; width: 1247px; height: 350px;'>" +
                    "<div class='table_title'>영상 출력 방법 설정</div>" +
                    "<div class='table_subTitle'>원본 비율 유지는 영화를 볼 때 자주 사용됩니다</div>" +
                    "<div id='table_radio_area'>" +
                        "<div class='table_radio_btn'>" +
                            "<div class ='table_radio_btn_bg'></div>"+
                            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
                            "<div class='table_radio_text'>전체 화면</div>" +
                       "</div>" +
                       "<div class='table_radio_btn'style='left: 285px;'>" +
                            "<div class ='table_radio_btn_bg'></div>"+
                            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                            "<div class='table_radio_text'>원본 비율 유지</div>" +
                       "</div>" +
                    (KTW.CONSTANT.IS_OTS ?
                        ("<div class='table_radio_btn' style='left: 570px;'>" +
                        "<div class ='table_radio_btn_bg'></div>"+
                        "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                        "<div class='table_radio_text'>원본 비율 확대</div></div>") : "") +
                    "</div>" +
                "</div>" +
            "</div>");

            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text1'>TV 화면 비율과 영상 출력<br>방법을 설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class ='rightMenu_btn_line'></div>"+
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class ='rightMenu_btn_line'></div>"+
            "</div>" +
            "</div>" +
            "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 설정된 값을 읽어온다.
         * OTS 의 경우 4:3 설정일 때 영상 출력 방법에 "원본 비율 확대" 옵션이 설정될 수 있다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var aspectRatio;
            var videoMode;

            var result = settingDataAdapter.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SETTING_ASPECT_RATIO);

            aspectRatio = result[0];
            videoMode = result[1];

            if (util.isValidVariable(aspectRatio)) {
                if (aspectRatio === "4:3") {
                    selectedRatio = 0;
                } else {
                    selectedRatio = 1;
                }
            } else {
                // 정상적인 값이 아닌 경우 default 값인 16:9 로 설정한다.
                selectedRatio = 1;
                hwAdapter.setAVOutputTvAspectRatio("16:9");
            }

            if (util.isValidVariable(videoMode)) {
                if (videoMode === "stretch") {
                    selectedOutput = 0;
                } else if (videoMode === "normal") {
                    selectedOutput = 1;
                } else if (videoMode === "zoom") {
                    // OTS의 경우 4:3 비율이 4:3인 경우 "원본 비율 확대" (= zoom) option이 표시된다.
                    selectedOutput = 2;
                }
            } else {
                selectedOutput = 0;
                hwAdapter.setAVOutputVideoMode("stretch");
            }

            if(KTW.CONSTANT.IS_OTS) {
                if(selectedRatio == 0) {
                    INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(2)").css({visibility: "inherit"});
                } else{
                    INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(2)").css({visibility: "hidden"});
                }
            }

            focusXIdx = selectedRatio;
            focusYIdx =0;

            INSTANCE.div.find("#contents #settingTable  .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable  .table_radio_btn .table_radio_img").removeClass("select");

            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(0) .table_radio_btn:eq(" + selectedRatio + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(0) .table_radio_btn:eq(" + selectedRatio + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(" + selectedOutput + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable .table_item_div:eq(1) .table_radio_btn:eq(" + selectedOutput + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");

        }

        /**
         * 설정된 값을 저장한다.
         * OTS 의 경우 4:3 설정일 때 영상 출력 방법에 "원본 비율 확대" 옵션이 설정될 수 있다.
         */
        function saveConfiguration() {
            log.printDbg("saveConfiguration(), selectedRatio = " + selectedRatio + ", selectedOutput = " + selectedOutput);

            var aspectRatio;
            var videoMode;

            if (selectedRatio === 0) {
                aspectRatio = "4:3";
            } else {
                aspectRatio = "16:9";
            }

            if (selectedOutput === 0) {
                videoMode = "stretch";
            } else if (selectedOutput === 1) {
                videoMode = "normal";
            } else if (selectedOutput === 2) {
                videoMode = "zoom";
            }

            log.printDbg("the aspectRatio will be saved ===:"+aspectRatio);
            log.printDbg("the videoMode will be saved ===:"+videoMode);

            hwAdapter.setAVOutputTvAspectRatio(aspectRatio);
            hwAdapter.setAVOutputVideoMode(videoMode);

            data.complete();
        }
    };

    KTW.ui.layer.setting.setting_screenRatio.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_screenRatio.constructor = KTW.ui.layer.setting.setting_screenRatio;

    KTW.ui.layer.setting.setting_screenRatio.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };



    KTW.ui.layer.setting.setting_screenRatio.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();