/**
 * Created by Yun on 2017-01-25.
 *
 * 설정 > 시스템 설정 > [OTS skylife 관리자 설정 (hidden menu 임)] > 채널정보 출력설정
 */

(function () {
    KTW.ui.layer.setting.setting_channelInfoOutput  = function SetChannelInfoOutput(options) {
        KTW.ui.Layer.call(this, options);
        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager = KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var settingDataAdaptor = KTW.ui.adaptor.SettingDataAdaptor;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;
        var ON_OFF = {
            ON : "true",
            OFF : "false"
        };

        var data;

        var clock;
        var clockArea;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setChannelInfoOutput"});

            styleSheet = $("<link/>", {
             rel: "stylesheet",
             type: "text/css",
             href: "styles/setting/setting_layer/main.css"
             });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>채널정보 출력설정</span>" +
                "<div id='pig_dim'></div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
          //  createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show setChannelInfoOutput");
            readConfiguration();

            focusIdx = selectedIdx;
            buttonFocusRefresh(focusIdx, 0);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SKY_CHANNEL_INFO) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide setChannelInfoOutput");
          //  clock.hide();

        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    }else if(menuIdx ==0) {
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            var on_off;
            switch(menuIndex) {
                case 0:
                    selectRadioBtn(index);
                    selectedIdx =index;
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        if(selectedIdx == 0){
                            on_off = ON_OFF.ON;
                        } else {
                            on_off = ON_OFF.OFF;
                        }
                        // 시스템 저장
                        saveConfiguration(on_off);
                        data.complete();
                        settingMenuManager.historyBackAndPopup();
                        break;

                    }
                    LayerManager.historyBack();
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>채널정보 출력설정</div>" +
                "<div id='table_radio_area'>" +
                "<div class='table_radio_btn'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>채널정보 출력 ON</div>" +
                "</div>" +
                "<div class='table_radio_btn'>" +
                "<div class='table_radio_btn_bg'></div>" +
                "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
                "<div class='table_radio_text'>채널정보 출력 OFF</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>채널정보 관련 설정값을<br>변경할 수 있습니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>저장</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 설정 값을 읽어온다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            var data = settingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.SKY_CHANNEL_INFO,"specific");
            var value = data[0];

            if (util.isValidVariable(value) === false) {
                // default 값 (ON) 으로 설정

                log.printDbg("isValidVariable()"+value);
                value = ON_OFF.ON;
            }

            if (value === ON_OFF.ON) {
                selectedIdx = 0;
            } else {
                selectedIdx = 1;
            }

            selectRadioBtn(selectedIdx);
        }

        /**
         * 선택된 설정 값을 저장한다.
         */
        function saveConfiguration(on_off) {
            log.printDbg("saveConfiguration(), on_off = " + on_off);

            basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.CHANNEL_INFO_OUTPUT, on_off);
        }

        function selectRadioBtn(idx){
            log.printDbg("selectRadioBtn()");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + idx + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + idx + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");

        }

    };
    KTW.ui.layer.setting.setting_channelInfoOutput.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_channelInfoOutput.constructor = KTW.ui.layer.setting.setting_channelInfoOutput;

    KTW.ui.layer.setting.setting_channelInfoOutput.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };


    KTW.ui.layer.setting.setting_channelInfoOutput.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();