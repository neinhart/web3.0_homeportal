/**
 * Created by Yun on 2016-11-23.
 *
 *
 * 설정 > 시스템 설정 > 알림 메시지 설정 화면
 */

(function () {
    KTW.ui.layer.setting.setting_alertMessage  = function AlertMessage(options) {
        KTW.ui.Layer.call(this, options);

        var KEY_CODE = KTW.KEY_CODE;
        var log = KTW.utils.Log;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;

        var settingDataAdapter = KTW.ui.adaptor.SettingDataAdaptor;
        var wsSmartPushManager =  KTW.managers.http.wsSmartPushManager;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;
        var selectedIdx = 0;
        var requestObject;
        var data;

        var clockArea;
        var clock;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame alertMessage"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>알림 메시지</span>" +
                "<div id='pig_dim'></div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();

            // 2017-04-28 [sw.nam] 설정 clock 추가
          //  createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            log.printDbg("show AlertMessage");
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_ALERT_MESSAGE) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
            readConfiguration();

          //  clock.show(clockArea);
        };

        this.hide = function (options) {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide AlertMessage");

            if (requestObject) {
                wsSmartPushManager.ajaxStop(requestObject);
                requestObject = undefined;
            }

          //  clock.hide();
        };

        this.destroy = function(){
            log.printDbg("destroy()");
            if (requestObject) {
                wsSmartPushManager.ajaxStop(requestObject);
                requestObject = undefined;
            }
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    return true;
                case KEY_CODE.LEFT:
                    if (menuIdx == 1) {
                        focusIdx = selectedIdx;
                        buttonFocusRefresh(focusIdx, 0);
                    } else {
                        LayerManager.historyBack();
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                    }
                    return true;
                case KEY_CODE.ENTER:
                    if(LayerManager.isShowLoading()){
                        return true;
                    }
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    selectedIdx = index;
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장
                        saveConfiguration();
                        return;
                    }
                    LayerManager.historyBack();
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>알림 메시지</div>" +
            "<div class='table_subTitle'>알림 메시지를 설정하면 채널/VOD 시청 시 유용한 정보를 받아볼 수 있습니다<br>※ 알림 메시지는 공지 또는 홍보를 목적으로 TV 시청 시 화면에 노출되는 메시지입니다</div>" +
            "<div id='table_radio_area'>" +
            "<div class='table_radio_btn' style='top:20px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img select' src='images/rdo_btn_select_d.png'>" +
            "<div class='table_radio_text'>ON</div>" +
            "</div>" +
            "<div class='table_radio_btn' style='top: 117px;'>" +
            "<div class='table_radio_btn_bg'></div>" +
            "<img class='table_radio_img' src='images/rdo_btn_d.png'>" +
            "<div class='table_radio_text'>OFF</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>알림 메시지를 설정합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }

        /**
         * 현재 설정된 값을 읽어온다.
         */
        function readConfiguration() {
            log.printDbg("readConfiguration()");

            try{
                requestObject = settingDataAdapter.getBMailInfo(cbGetBMailInfo, KTW.SAID);
            } catch (error) {
                log.printDbg(error.message);
            }
        }
        /**
         * 변경된 설정 값을 저장한다.
         * 알림 메시지는 postMessage 를 통해 Smart Push Client App에 전달한다.
         */
        function saveConfiguration() {
            log.printDbg("saveConfiguration(), selectedIdx = " + selectedIdx);

            var message;

            // 2017.06.09 dhlee
            // 일반 모드 <-> 키즈 모드 전환 간에 참조하기 위하여 설정 값을 저장해 둔다.
            // 이 Local Storage 는 일반 모드 <-> 키즈 모드 전환 간에만 참조한다.
            // 설정에서는 항상 설정 값을 저장만 한다.
            KTW.managers.StorageManager.ps.save(KTW.managers.StorageManager.KEY.NORMAL_ALERT_MESSAGE, selectedIdx === 0 ? "ON" : "OFF");

            // 2017.06.09 sw.nam
            // 알림메시지 응답 타이밍 이슈 해결을 위해 로딩 추가(2sec)
            KTW.ui.LayerManager.startLoading({
                 preventKey: true
            });

            try {
                message = {
                    "method" : "spc_optout",
                    "msgType" : "JSON",
                    "type" : "A",
                    "id": "",
                    "setting" : selectedIdx == 0 ? false : true,
                    "to" : KTW.CONSTANT.APP_ID.SMART_PUSH,
                    "from" : KTW.CONSTANT.APP_ID.HOME
                };
                KTW.managers.MessageManager.sendMessage(message,onSendMessage);
            } catch (error) {
                log.printDbg("saveConfiguration(), error = " + error.message);
            }
            setTimeout(function() {
                //readConfiguration();
                LayerManager.stopLoading();
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectedIdx + ") .table_radio_img").addClass("select");
                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectedIdx + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                buttonFocusRefresh(selectedIdx, 0);
                data.complete();
                settingMenuManager.historyBackAndPopup();

            }, 2000);

        }

        function onSendMessage(message) {
            log.printDbg("onSendMessage  "+message);
        }

        function cbGetBMailInfo(success,result) {
            log.printDbg("cbGetBMailInfo()");
            var value;

            requestObject = undefined;
            if (success == false && result == "abort") {
                // [jh.lee] ajaxStop 시키는 경우 후처리 중지
                log.printDbg("success : " + success);
                log.printDbg("result : " + result);
                return;
            }

            if (success) {
                if (result.result === "0") {
                    if (result.exist === "F") {
                        log.printDbg("response(0000000), exist = " + result.exist);
                        value = 0;
                    } else {
                        log.printDbg("response(1111111), exist = " + result.exist);
                        value = 1;
                    }
                }

            }
            if(!value) {
                value = 0;
            }
            focusIdx = selectedIdx = value;

            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectedIdx + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + selectedIdx + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
            buttonFocusRefresh(focusIdx, 0);

        }

    };
    KTW.ui.layer.setting.setting_alertMessage.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_alertMessage.constructor = KTW.ui.layer.setting.setting_alertMessage;

    KTW.ui.layer.setting.setting_alertMessage.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_alertMessage.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();