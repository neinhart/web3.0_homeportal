/**
 * Created by Yun on 2017-01-25.
 *
 * 설정 > 시스템 설정 > [OTS skylife 관리자 설정 (hidden menu 임)] > CAS 정보
 */


(function () {
    KTW.ui.layer.setting.setting_casInfo  = function CasInfo(options) {
        KTW.ui.Layer.call(this, options);

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var DEF = KTW.oipf.Def;
        var casInfo;
        var util = KTW.utils.util;

        // [jh.lee] 리스트에 표시할 아이템 정보
        var data;
        // [jh.lee] 현재 포커스 모드
        var current_step = -1;
        // [jh.lee] 현재 인덱스
        var input_index = 0;

        var itemGroup;
        var item = [];

        var clock;
        var clockArea;


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame casInfo"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>CAS 정보</span>" +
                "<div id='pig_dim'></div>" +

                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            focusIdx = 0;
            buttonFocusRefresh(focusIdx, 1);
            
            readCASInfo();

            /*
            [sw.nam] 2.0에는 스크롤 기능, startLoading 등의 함수를 사용하는데 , 실제로 받아오는 casInfo 는 10개의 항목만 하드코딩 해서 받아옴..
            TODO 추후 flexible 한 데이터 로딩이 필요한 경우 수정
            */
            setCasInfo();
            if(!options || !options.resume) {
                var data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_CAS_INFO) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
       //     clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(0, menuIdx);
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(0, menuIdx);
                    return true;
                case KEY_CODE.LEFT:

                    LayerManager.historyBack();
                    // 왼쪽 메뉴로 포커스 이동시 사용
                    // if (menuIdx == 1) {
                    //     focusIdx = 0;
                    //     buttonFocusRefresh(focusIdx, 0);
                    // }
                    return true;
                case KEY_CODE.RIGHT:
                    // 오른쪽 메뉴로 포커스 이동시 사용
                    // if (menuIdx == 0) {
                    //     focusIdx = 0;
                    //     buttonFocusRefresh(focusIdx, 1);
                    // }
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    LayerManager.historyBack();
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0)").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(0) .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>CAS 정보</div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'>CAS 관련 정보를<br>확인할 수 있습니다</div>" +
                "<div id='rightMenu_btn_area'>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>확인</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");

            makeElement();
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function makeElement(){

            itemGroup = util.makeElement({
                tag: "<div/>",
                attrs: {
                    class: "table_contentsGroup",
                    css: {
                            position: "absolute", top: 167, width: 886, height: 500
                    }
                },
                parent:  INSTANCE.div.find("#contents #settingTable")
            });

            for(var i = 0; i< 10; i++) {

                item[i] =  util.makeElement({
                    tag: "<div/>",
                    attrs: {
                        class: "content",
                        css: {
                                "margin-top": 15
                        }
                    },
                    parent:  itemGroup
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "infoText",
                        css: {
                            "font-size" : 26, "font-family" : "RixHead L", color: "rgba(255,255,255,0.4)", "letter-spacing" : -1.3
                        }
                    },
                    text: "casInfo"+i+" :: casinfo!!!!!!!!",
                    parent: item[i]

                });
            }

        }

        function readCASInfo() {
            log.printDbg("readCASInfo()");

            var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;

            casInfo = [
                {"name":"ECM Metadata", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_ECMMETA)},
                {"name":"Zip Code", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_ZIP_CODE)},
                {"name":"Segments", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_SEGMENT)},
                {"name":"BAT ID", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_BOUQUET)},
                {"name":"Smartcard Version", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CARD)},
                {"name":"STB chipset NUID", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_NUID)},
                {"name":"STB CA S/N", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_SERIAL_NUM)},
                {"name":"Project Information", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.CAS_PROJECT_INFO)},
                {"name":"Chipset Version", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CHIPSET)},
                {"name":"Chipset Revision", "value":basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_CHIPSET_REV)}
            ];


        }

        function setCasInfo() {
            log.printDbg("setCasInfo()");


            for( var i = 0; i < casInfo.length; i++ ) {
                item[i].find(".infoText").text(casInfo[i].name + " : " + casInfo[i].value);
            }

        }
    };
    KTW.ui.layer.setting.setting_casInfo.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_casInfo.constructor = KTW.ui.layer.setting.setting_casInfo;

    KTW.ui.layer.setting.setting_casInfo.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };



    KTW.ui.layer.setting.setting_casInfo.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();