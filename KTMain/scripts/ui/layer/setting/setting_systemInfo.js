/**
 * Created by Yun on 2017-01-23.
 *
 * 설정 > 시스템 설정 > 시스템 정보
 */

"use strict";

(function () {
    KTW.ui.layer.setting.setting_systemInfo  = function SystemInfo(options) {
        KTW.ui.Layer.call(this, options);

        /*Constance numbers*/
        var NUMBER_OF_ITEM_PER_PAGE = 13;
        //var SUCCESS = 0;
        var NONEED = 1;
        //var FAIL = 2;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
        var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
        var DEF = KTW.oipf.Def;
        //var menuDataManager = KTW.managers.data.MenuDataManager;

        var INSTANCE = this;
        var styleSheet;

        var focusIdx = 0;
        var menuIdx = 0;

        var item = []; // 항목 element 를 담을 배열
        var titleItem = []; // 항목 element 를 담을 배열
        var contentItem = []; // 항목 element 를 담을 배열
        var scroll = null;
        var scrollArea = null;

        var clock;
        var clockArea;

        var colorKeyArea;

        /*flags*/
        var isScrollArea = false;
        var isCheckingSystemUpgrade = false;

        // [sw.nam] 현재 가리키고 있는 스크롤 페이지에서 보여줄 데이터.
        //var shoingInfoDataArr = [];

        var infoTitleArr = [];
        var infoContentsIdx = 0;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setting_layer systemInfo"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });


            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>시스템 정보</span>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            //시스템 정보 데이터 가져오기
            initData();

            // 현재 보여질 데이터 페이지 업데이트
            infoContentsIdx = 0;
            updateInfoArea(infoContentsIdx);

            // 2018.01.16 sw.nam
            // 스크롤이 오른쪽으로 오면서 스크롤에 포커스 되는 정책이 없어짐.
            var scrollBoolean = false;
/*            if(infoTitleArr) {
                scroll.show({
                    maxPage: getMaxPage(),
                    curPage: getCurrentPage()
                });
                if(getMaxPage() > 1) {
                    scrollBoolean = true;
                    menuIdx = 0;
                }else {
                    menuIdx = 1;
                }
                setScrollFocus(scrollBoolean);
            }else {
                setScrollFocus(false);
                menuIdx = 1;
            }*/

            if(infoTitleArr) {
                scroll.show({
                    maxPage: getMaxPage(),
                    curPage: getCurrentPage()
                });

                if(getMaxPage() > 2) {
                    colorKeyArea.css({visibility: "inherit"});
                }else {
                    colorKeyArea.css({visibility: "hidden"});
                }
            }

            buttonFocusRefresh(0, 1);

            if(!options || !options.resume) {
                var data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SYSTEM_INFO) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

          //  clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

            scroll.hide();
            isCheckingSystemUpgrade = false;
       //     clock.hide();

        };

        this.controlKey = function (key_code) {
            log.printDbg("key_code_"+key_code);
            if(isCheckingSystemUpgrade) {
                log.printDbg("is checking system upgrade..");
                return true;
            }
            switch (key_code) {
                case KEY_CODE.UP:
/*                    if(isScrollArea) {

                        infoContentsIdx  = infoContentsIdx - (NUMBER_OF_ITEM_PER_PAGE - (infoContentsIdx % NUMBER_OF_ITEM_PER_PAGE));
                        if(infoContentsIdx < 0) {
                            infoContentsIdx = (infoTitleArr.length -1) - ( (infoTitleArr.length -1) % NUMBER_OF_ITEM_PER_PAGE );
                        }
                        setCurrentPage();

                    }else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 3), 1);
                    }*/
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 3), 1);
                    return true;
                case KEY_CODE.DOWN:
/*                    if(isScrollArea) {
                        infoContentsIdx  = infoContentsIdx + (NUMBER_OF_ITEM_PER_PAGE - (infoContentsIdx % NUMBER_OF_ITEM_PER_PAGE));

                        if(infoContentsIdx > infoTitleArr.length-1) {
                            infoContentsIdx = 0;
                        }
                        setCurrentPage();

                    } else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 3), 1);
                    }*/
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 3), 1);
                    return true;
                case KEY_CODE.RED:
                   // if(isScrollArea) {

                        infoContentsIdx  = infoContentsIdx - (NUMBER_OF_ITEM_PER_PAGE - (infoContentsIdx % NUMBER_OF_ITEM_PER_PAGE));
                        if(infoContentsIdx < 0) {
                            infoContentsIdx = (infoTitleArr.length -1) - ( (infoTitleArr.length -1) % NUMBER_OF_ITEM_PER_PAGE );
                        }
                        setCurrentPage();
                    //}
                    return true;
                case KEY_CODE.BLUE:
                    //if(isScrollArea) {
                        infoContentsIdx  = infoContentsIdx + (NUMBER_OF_ITEM_PER_PAGE - (infoContentsIdx % NUMBER_OF_ITEM_PER_PAGE));

                        if(infoContentsIdx > infoTitleArr.length-1) {
                            infoContentsIdx = 0;
                        }
                        setCurrentPage();
                   // }
                    return true;
                case KEY_CODE.LEFT:
             /*       if(isScrollArea){
                        LayerManager.historyBack();
                    }else{
                        setScrollFocus(true);
                        focusIdx = 0;
                        buttonFocusRefresh(0, 0);
                    }*/
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.RIGHT:
/*                    if(isScrollArea){
                        focusIdx = 0;
                        buttonFocusRefresh(0, 1);
                        setScrollFocus(false);
                    }*/
                    return true;
                case KEY_CODE.ENTER:
/*                    if(isScrollArea) {
                        setScrollFocus(false);
                        focusIdx = 0;
                        buttonFocusRefresh(0, 1);
                    } else{
                        pressEnterKeyEvent(focusIdx, 1);
                    }*/
                    pressEnterKeyEvent(focusIdx, 1);
                    return true;
                case KEY_CODE.BACK:
                    // 2017.06.16 sw.nam
                    // 특정 셋탑에서 이전 키 입력 시 이례적으로 이전 채널 이동(?) 이 일어나는 이슈가 발생하여 back키 처리하도록 코드 추가
                    LayerManager.historyBack();
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    break;
                case 1:

                    if (index == 0) {
                        // 시스템 업그레이드
                        checkSystemUpgrade();
                    } else if (index == 1) {
                        // 채널 정보 업데이트
                        checkChannelInfoUpdate();
                    } else {
                        // 오픈 소스 라이선스
                        KTW.ui.LayerManager.activateLayer({
                            obj: {
                                id: "open_source_license_popup",
                                type: KTW.ui.Layer.TYPE.POPUP,
                                linkage: true,
                                priority: KTW.ui.Layer.PRIORITY.POPUP,
                                view :  KTW.ui.view.popup.OpenSourceLicensePopup,
                                params: {
                                }
                            },
                            visible: true
                        });
                    }
                    break;
            }
        }

        function checkSystemUpgrade() {
            log.printDbg("checkSystemUpgrade()");

            //var em = KTW.oipf.oipfObjectFactory.createExtendedChannelDatabase();
            //var result = em.requestFirmwareDownload();

            // 2017.07.24 dhlee
            // Extended APIs for KT_20170713.pdf 문서 기준으로 requestFirmwareDownload() function은 void로 정의되어 있다.
            // 따라서 result 값은 의미가 없음.
            var result = KTW.oipf.AdapterHandler.navAdapter.requestFirmwareDownload();

            if(util.isValidVariable(result) === false ) {
                result = NONEED;
            }
            isCheckingSystemUpgrade = true;
            setTimeout(function() {
                LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.SETTING_SYSTEMINFO_SYSTEM_UPGRAGE_POPUP,
                        type: Layer.TYPE.POPUP,
                        priority: Layer.PRIORITY.POPUP,
                        linkage: true,
                        params: {
                            complete: function() {
                                onResponseUpgrade();
                            },
                            result : result
                        }
                    },
                    new: true,
                    visible: true
                });

            }, 2000);

        }

        function onResponseUpgrade(result) {
            log.printDbg("onResponseUpgrade()");
            isCheckingSystemUpgrade = false;
            if(result == 0 ) {

            }
        }

        function checkChannelInfoUpdate() {
            log.printDbg("checkChannelInfoUpdate()");
            // show popup
            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_SYSTEMINFO_CHANNEL_INFO_UPDATE_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                    }
                },
                visible: true
            });

            setTimeout(function() {
                updateChannel()
            },1000);
        }

        function updateChannel() {
            log.printDbg("updateChannel()");

            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_SYSTEMINFO_CHANNEL_INFO_UPDATE_POPUP,
                remove: true
            });
            /*
             var em = KTW.oipf.oipfObjectFactory.createExtendedChannelDatabase();
             var result = em.updateDIDatabase();
             */
            var result = KTW.oipf.AdapterHandler.navAdapter.updateDIDatabase();
            log.printDbg("updateChannel() result = " + result);

            //var result = true;
            LayerManager.activateLayer({
                obj: {
                    id: KTW.ui.Layer.ID.SETTING_SYSTEMINFO_CHANNEL_INFO_UPDATE_POPUP,
                    type: Layer.TYPE.POPUP,
                    priority: Layer.PRIORITY.POPUP,
                    linkage: true,
                    params: {
                        result : result
                    }
                },
                visible: true
            });
        }


        function buttonFocusRefresh(index, menuIndex) {
            focusIdx = index;
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #settingTable #table_radio_area1 .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area1 .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area1 .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area1 .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            switch (menuIndex) {
                case 0:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                    break;
                case 1:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
            }
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            var html = "<div id='settingTable_area'>" +
                "<div id='settingTable'>";
            html += "</div></div>";
            INSTANCE.div.find("#contents").append(html);
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>현재 셋톱박스 소프트웨어<br>버전과 서비스, 채널 버전<br>정보를 확인 및 업데이트<br>합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>시스템 업그레이드</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>채널 정보 업데이트</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>오픈소스 라이선스</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");

            createElement();
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /*        *
         [sw.nam] 2017 3.10,

         가변 데이터 핸들링을 위한 엘레먼트 생성 및 스크롤 엘레먼트 추가 */
        function createElement () {
            log.printDbg("createElement()");

            for(var i = 0; i < NUMBER_OF_ITEM_PER_PAGE; i++) {

                item[i] = util.makeElement({
                    tag: "<div />",
                    parent:  INSTANCE.div.find("#settingTable")
                });

                titleItem[i] = util.makeElement({
                    tag: "<div/>",
                    attrs: {
                        css: {
                            width: 330, height: 56, float: "left"
                        }
                    },
                    parent:  item[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "infoTitle_text",
                        css: {
                            position: "absolute", "text-overflow": "ellipsis", "white-space": "nowrap", "word-wrap": "normal", overflow: "hidden"

                        }
                    },
                    text: "",
                    parent: titleItem[i]
                });

                contentItem[i] = util.makeElement({
                    tag: "<div/>",
                    attrs: {
                        css: {
                            width: 800, height: 56, float: "left"
                        }
                    },
                    parent:  item[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "infoContents_text"
                    },
                    text: "",
                    parent: contentItem[i]
                });
            }
            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "scroll",
                    css: {
                        position: "absolute", left: 1369, top: 140, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                forceHeight: true,
                height: 723
            });

            //2017.11.07 sw.nam
            //적청키 엘레먼트 추가
            colorKeyArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "colorKey",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 1920, height: 1080, visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: 1343, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
            tag: "<img />",
            attrs: {
                class: "blueKey",
                src: "images/icon/icon_pagedown.png",
                css: {
                    position: "absolute", left: 1371, top: 904, width: 29, height: 32
                }
            },
            parent: colorKeyArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "keyText",
                    css: {
                        position: "absolute", left: 1343, top: 939, width: 109, height: 26, "letter-spacing" : -1.2,
                        "font-size" : 24, color: "rgba(255,255,255,0.77)", "font-family" : "RixHead L"
                    }
                },
                text: "페이지",
                parent: colorKeyArea
            });
        }


        function initData() {
            log.printDbg("initData");

            var language;
            var moduleDataManager = KTW.managers.module.ModuleDataManager;
            var moduleManager = KTW.managers.module.ModuleManager;

            language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();

            // [jh.lee] 언바운드 App 버전이 잘못되는 경우 예외처리 (undefined)
            var appstore_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.APP_STORE_VERSION);
            var oam_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.OAM_VERSION);
            var mashup_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.MASHUP_VERSION);
            var push_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.SMART_PUSH_VERSION);
            var vod_sdk_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.CASTIS_SDK_VERSION);

            log.printDbg("appstore_version : " + appstore_version);
            log.printDbg("oam_version : " + oam_version);
            log.printDbg("mashup_version : " + mashup_version);
            log.printDbg("push_version : " + push_version);

            appstore_version = (appstore_version === "undefined" ? "" : appstore_version);
            oam_version = (oam_version === "undefined" ? "" : oam_version);
            mashup_version = (mashup_version === "undefined" ? "" : mashup_version);
            push_version = (push_version === "undefined" ? "" : push_version);
            vod_sdk_version = (vod_sdk_version === "undefined" ? "" : vod_sdk_version);

            //[sw.nam] 2017.04.18 모듈버전 정보 추가
            //[sw.nam] 2017.05.24 모듈 로딩 path 정보 추가 (S / L)

            var moduleVersion;
            var moduleData = [];
            var module;

            for(var key in KTW.managers.module.Module.ID) {
                if(key !== KTW.managers.module.Module.ID.MODULE_FRAMEWORK) {
                    module = KTW.managers.module.ModuleDataManager.getModuleData(KTW.managers.module.Module.ID[key]);
                    if(module) {
                        if(moduleManager.getModule(module.moduleId)) {
                            //로딩된 모듈이 있는 경우
                            moduleData.push(moduleManager.getModule(module.moduleId));
                        }else {
                            //로딩된 모듈이 없는 경우
                            moduleData.push(module);
                        }
                    }
                }
            }

            if (KTW.CONSTANT.IS_OTS) {
                // [sw.nam] OTS
                if (language === "eng") {
                    infoTitleArr = new Array();
                    infoTitleArr.push({"infoTitle":"가입자 ID", "infoCon":KTW.SAID});
                    infoTitleArr.push({"infoTitle":"업데이트 날짜", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SOFTWARE_UPDATE_DATE)});
                    infoTitleArr.push({"infoTitle":"펌웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_FIRMWARE)});
                    infoTitleArr.push({"infoTitle":"Main 소프트웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MAIN_SOFTWARE)});
                    infoTitleArr.push({"infoTitle":"미들웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MIDDLEWARE)});
                    infoTitleArr.push({"infoTitle":"네비게이터 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.NAVIGATOR)});
                    infoTitleArr.push({"infoTitle":"CA Adaptor 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CAADAPTOR)});
                    infoTitleArr.push({"infoTitle":"KT-CAS Library 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_VERIFIRE)});
                    infoTitleArr.push({"infoTitle":"Glue 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_SOFTWARE)});
                    infoTitleArr.push({"infoTitle":"로더 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_LOADER)});
                    infoTitleArr.push({"infoTitle":"서비스 관리자 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.OBSERVER)});
                    // 2017.09.06 dhlee
                    // 버전 정보 규격 통일
                    var portalVersion = "W3_"+KTW.UI_VERSION + "_" + KTW.UI_DATE + "-" + KTW.RELEASE_MODE + "\n" + "(menu:" + KTW.managers.data.MenuDataManager.getMenuVersion() + ")";
                    infoTitleArr.push({"infoTitle":"포털 서비스 버전", "infoCon": portalVersion});
                    //2017.05.15 sw.nam 포털 서비스 하위 항목을 다음 페이지로 넘기라는 gui 요청에 따라 공백 추가
                    //2018.01.16 sw.nam 고도화 GUI 에서 전체적인 width 가 늘어나 포털 서비스 버전이 한줄에 표현 가능하므로 필요 없을것 같아 임시로 공백을 제거
                    //infoTitleArr.push({"infoTitle":" ", "infoCon": " "});
                    infoTitleArr.push({"infoTitle":"S/W Download Status", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.DOWNLOAD_STATUS)});
                    infoTitleArr.push({"infoTitle":"IP 주소", "infoCon":hwAdapter.networkInterface.getIpAddress()});
                    infoTitleArr.push({"infoTitle":"MAC 주소", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MAC_ADDRESS)});
                    infoTitleArr.push({"infoTitle":"ATTS 주소", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_ATTSADDRESS)});
                    infoTitleArr.push({"infoTitle":"Smartcard SerialNo", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_CHIP)});
                    infoTitleArr.push({"infoTitle":"Smartcard No", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SMARTCARD_ID)});

                    //[sw.nam] 상품코드가 여러줄일경우 최대 6코드씩 나누어서 한줄에 표현한다(GUI 가이드 width 기준으로 갯수를 정함)
                    var text = basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_PRODUCTCODE);
                    var tmp_text = text.split(" ");
                    var input_text = "";
                    var idx =0;
                    log.printDbg("text ===== "+tmp_text);
                    if(tmp_text !== null && tmp_text.length > 0) {
                        while(idx < tmp_text.length) {
                            for(var i =0; i < 9; i ++) {
                                input_text += tmp_text[idx]+" ";
                                idx++;
                                if(idx == tmp_text.length) {
                                    break;
                                }
                            }
                            if(idx <= 9) {
                                infoTitleArr.push({"infoTitle": "상품코드", "infoCon": input_text });
                            } else {
                                infoTitleArr.push({"infoTitle":"    ", "infoCon": input_text });
                            }
                            input_text = "";
                        }
                    } else {
                        infoTitleArr.push({"infoTitle":"상품코드", "infoCon":"   "});
                    }

                    infoTitleArr.push({"infoTitle":"시리얼 번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SERAILNUMBER)});
                    infoTitleArr.push({"infoTitle":"Tune Status", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.TUNE_STATE)});
                    infoTitleArr.push({"infoTitle":"Manufacturer ID ", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_MANUFACTURER)});
                    infoTitleArr.push({"infoTitle":"하드웨어 버전 ", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_HARDWARE)});
                    infoTitleArr.push({"infoTitle":"Driver 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_DRIVER)});
                    infoTitleArr.push({"infoTitle":"모델 번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SIGNAL_MODEL_NUMBER)});
                    infoTitleArr.push({"infoTitle":"CAK Version", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CAAPI)});
                    infoTitleArr.push({"infoTitle":"Smartcard Version", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CARD)});
                    infoTitleArr.push({"infoTitle":"채널정보 업데이트 날짜", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.DI_UPDATEPERIOD)});
                    infoTitleArr.push({"infoTitle":"채널정보 버전(BDI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.BDI_VERSION)});
                    infoTitleArr.push({"infoTitle":"채널정보 버전(PDI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.PDI_VERSION)});
                    infoTitleArr.push({"infoTitle":"채널정보 버전(ADI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ADI_VERSION)});
                    infoTitleArr.push({"infoTitle":"네트워크 연결 상태", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.NETWORKSTATUE)});
                    infoTitleArr.push({"infoTitle":"VOD SDK 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.CASTIS_SDK_VERSION)});
                    infoTitleArr.push({"infoTitle":"앱스토어 서비스 버전", "infoCon":appstore_version});
                    infoTitleArr.push({"infoTitle":"OAM 서비스 버전", "infoCon":oam_version});
                    infoTitleArr.push({"infoTitle":"매시업 매니져 버전", "infoCon":mashup_version});
                    infoTitleArr.push({"infoTitle":"Push 서비스 버전", "infoCon":push_version});
                    // 2017.04.27 dhlee 이름 변경
                    infoTitleArr.push({"infoTitle":"키즈 공용제어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.REMOTE_AGENT)});
                    //[sw.nam]2017.05.16 음성클라이언트 버전 정보 추가
                    // 2017.05.17 dhlee 기가지니는 제외
                    if (!KTW.CONSTANT.STB_TYPE.ANDROID) {
                        var voice_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.VOICE_VERSION);
                        if(voice_version === "undefined") {
                            infoTitleArr.push({"infoTitle":"음성 클라이언트 버전", "infoCon":"  "});
                        } else {
                            infoTitleArr.push({"infoTitle":"음성 클라이언트 버전", "infoCon":voice_version});
                        }
                    }

                    for (var idx = 0; idx < moduleData.length; idx++) {
                        infoTitleArr.push(getModuleVersionInfo(moduleData[idx]));
                    }
                }
                else {
                    infoTitleArr = new Array();

                    infoTitleArr.push({"infoTitle":"가입자 ID", "infoCon":KTW.SAID});
                    infoTitleArr.push({"infoTitle":"업데이트 날짜", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SOFTWARE_UPDATE_DATE)});
                    infoTitleArr.push({"infoTitle":"펌웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_FIRMWARE)});
                    infoTitleArr.push({"infoTitle":"Main 소프트웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MAIN_SOFTWARE)});
                    infoTitleArr.push({"infoTitle":"미들웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MIDDLEWARE)});
                    infoTitleArr.push({"infoTitle":"네비게이터 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.NAVIGATOR)});
                    infoTitleArr.push({"infoTitle":"CA Adaptor 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CAADAPTOR)});
                    infoTitleArr.push({"infoTitle":"KT-CAS Library 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_VERIFIRE)});
                    infoTitleArr.push({"infoTitle":"Glue 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_SOFTWARE)});
                    infoTitleArr.push({"infoTitle":"로더 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_LOADER)});
                    infoTitleArr.push({"infoTitle":"서비스 관리자 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.OBSERVER)});
                    // 2017.09.06 dhlee
                    // 버전 정보 통일
                    var portalVersion = "W3_"+KTW.UI_VERSION + "_" + KTW.UI_DATE + "-" + KTW.RELEASE_MODE + "\n" + "(menu:" + KTW.managers.data.MenuDataManager.getMenuVersion() + ")";
                    infoTitleArr.push({"infoTitle":"포털 서비스 버전", "infoCon": portalVersion});
                    //2017.05.15 sw.nam 포털 서비스 하위 항목을 다음 페이지로 넘기라는 gui 요청에 따라 공백 추가
                    //2018.01.16 sw.nam 고도화 GUI 에서 전체적인 width 가 늘어나 포털 서비스 버전이 한줄에 표현 가능하므로 필요 없을것 같아 임시로 공백을 제거
                    //infoTitleArr.push({"infoTitle":" ", "infoCon": " "});
                    infoTitleArr.push({"infoTitle":"S/W Download Status", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.DOWNLOAD_STATUS)});
                    infoTitleArr.push({"infoTitle":"IP 주소", "infoCon":hwAdapter.networkInterface.getIpAddress()});
                    infoTitleArr.push({"infoTitle":"MAC 주소", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MAC_ADDRESS)});
                    infoTitleArr.push({"infoTitle":"ATTS 주소", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_ATTSADDRESS)});
                    infoTitleArr.push({"infoTitle":"스마트카드 일련번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_CHIP)});
                    infoTitleArr.push({"infoTitle":"스마트카드 번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SMARTCARD_ID)});

                    //[sw.nam] 상품코드가 여러줄일경우 최대 12코드씩 나누어서 한줄에 표현한다(GUI 가이드 width 기준으로 갯수를 정함)
                    var text = basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_PRODUCTCODE);
                    var tmp_text = text.split(" ");
                    var input_text = "";
                    var idx =0;
                    if(tmp_text !== null && tmp_text.length > 0) {
                        while(idx < tmp_text.length) {
                            for(var i =0; i < 9; i ++) {
                                input_text += tmp_text[idx]+" ";
                                idx++;
                                if(idx == tmp_text.length) {
                                    break;
                                }
                            }
                            if(idx <= 9) {
                                infoTitleArr.push({"infoTitle": "상품코드", "infoCon": input_text });
                            } else {
                                infoTitleArr.push({"infoTitle":"    ", "infoCon": input_text });
                            }
                            input_text = "";
                        }
                    }else {
                        infoTitleArr.push({"infoTitle":"상품코드", "infoCon":"   "});
                    }
                    infoTitleArr.push({"infoTitle":"시리얼 번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SERAILNUMBER)});
                    infoTitleArr.push({"infoTitle":"Tune Status", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.TUNE_STATE)});
                    infoTitleArr.push({"infoTitle":"Manufacturer ID ", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_MANUFACTURER)});
                    infoTitleArr.push({"infoTitle":"하드웨어 버전 ", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_HARDWARE)});
                    infoTitleArr.push({"infoTitle":"Driver 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_DRIVER)});
                    infoTitleArr.push({"infoTitle":"모델 번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SIGNAL_MODEL_NUMBER)});
                    infoTitleArr.push({"infoTitle":"CAK 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CAAPI)});
                    infoTitleArr.push({"infoTitle":"스마트카드 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CARD)});
                    infoTitleArr.push({"infoTitle":"채널정보 업데이트 날짜", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.DI_UPDATEPERIOD)});
                    infoTitleArr.push({"infoTitle":"채널정보 버전(BDI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.BDI_VERSION)});
                    infoTitleArr.push({"infoTitle":"채널정보 버전(PDI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.PDI_VERSION)});
                    infoTitleArr.push({"infoTitle":"채널정보 버전(ADI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ADI_VERSION)});
                    infoTitleArr.push({"infoTitle":"네트워크 연결 상태", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.NETWORKSTATUE)});
                    infoTitleArr.push({"infoTitle":"VOD SDK 버전", "infoCon": vod_sdk_version});
                    infoTitleArr.push({"infoTitle":"앱스토어 서비스 버전", "infoCon":appstore_version});
                    infoTitleArr.push({"infoTitle":"OAM 서비스 버전", "infoCon":oam_version});
                    // 2017.04.27 dhlee 항목 삭제 (KT 요청)
                    //infoTitleArr.push({"infoTitle":"개인방송 버전", "infoCon":individual_version});
                    infoTitleArr.push({"infoTitle":"매시업 매니져 버전", "infoCon":mashup_version});
                    infoTitleArr.push({"infoTitle":"Push 서비스 버전", "infoCon":push_version});
                    //2017.04.27 [sw.nam] 이름 수정
                    infoTitleArr.push({"infoTitle":"키즈 공용제어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.REMOTE_AGENT)});
                    //[sw.nam]2017.05.16 음성클라이언트 버전 정보 추가
                    // 2017.05.17 dhlee 기가지니는 제외
                    if (!KTW.CONSTANT.STB_TYPE.ANDROID) {
                        var voice_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.VOICE_VERSION);
                        if(voice_version === "undefined") {
                            infoTitleArr.push({"infoTitle":"음성 클라이언트 버전", "infoCon":"  "});
                        } else {
                            infoTitleArr.push({"infoTitle":"음성 클라이언트 버전", "infoCon":voice_version});
                        }
                    }

                    for (var idx = 0; idx < moduleData.length; idx++) {
                        infoTitleArr.push(getModuleVersionInfo(moduleData[idx]));
                    }
                }
            } else {
                // [jh.lee] OTV
                infoTitleArr = new Array();

                infoTitleArr.push({"infoTitle":"가입자 ID", "infoCon":KTW.SAID});
                infoTitleArr.push({"infoTitle":"업데이트 날짜", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.SOFTWARE_UPDATE_DATE)});
                infoTitleArr.push({"infoTitle":"펌웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_FIRMWARE)});
                infoTitleArr.push({"infoTitle":"Main 소프트웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MAIN_SOFTWARE)});
                infoTitleArr.push({"infoTitle":"미들웨어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MIDDLEWARE)});
                infoTitleArr.push({"infoTitle":"네비게이터 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.NAVIGATOR)});
                infoTitleArr.push({"infoTitle":"CA Adaptor 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_CAADAPTOR)});

                //var tmp = basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_VERIFIRE).substring(0,23);
                infoTitleArr.push({"infoTitle":"KT-CAS Library 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_VERIFIRE)});

                infoTitleArr.push({"infoTitle":"Glue 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_SOFTWARE)});
                infoTitleArr.push({"infoTitle":"로더 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_LOADER)});
                infoTitleArr.push({"infoTitle":"서비스 관리자 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.OBSERVER)});
                // TODO 2.0 형상이랑 똑같은 형식으로 정보 보여주도록 수정, 추후 확인
                var portalVersion = "W3_"+KTW.UI_VERSION + " " + KTW.UI_DATE + " - " + KTW.RELEASE_MODE + "\n" + "(menu:" + KTW.managers.data.MenuDataManager.getMenuVersion() + ")";
                infoTitleArr.push({"infoTitle":"포털 서비스 버전", "infoCon": portalVersion});
                ////2017.05.15 sw.nam 포털 서비스 하위 항목을 다음 페이지로 넘기라는 gui 요청에 따라 공백 추가
                //2018.01.16 sw.nam 고도화 GUI 에서 전체적인 width 가 늘어나 포털 서비스 버전이 한줄에 표현 가능하므로 필요 없을것 같아 임시로 공백을 제거
                //infoTitleArr.push({"infoTitle":" ", "infoCon": " "});
                infoTitleArr.push({"infoTitle":"IP 주소", "infoCon":hwAdapter.networkInterface.getIpAddress()});
                infoTitleArr.push({"infoTitle":"MAC 주소", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.MAC_ADDRESS)});
                infoTitleArr.push({"infoTitle":"ATTS 주소", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_ATTSADDRESS)});
                infoTitleArr.push({"infoTitle":"스마트카드 일련번호", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ID_CHIP)});

                //[sw.nam] 상품코드가 여러줄일경우 최대 6코드씩 나누어서 한줄에 표현한다(GUI 가이드 width 기준으로 갯수를 정함)
                var text = basicAdapter.getConfigText(DEF.CONFIG.KEY.INFO_PRODUCTCODE);
                var tmp_text = text.split(" ");
                var input_text = "";
                var idx =0;
                if(tmp_text !== null && tmp_text.length > 0) {
                    while(idx < tmp_text.length) {
                        for(var i =0; i < 9; i ++) {
                            input_text += tmp_text[idx]+" ";
                            idx++;
                            if(idx == tmp_text.length) {
                                break;
                            }
                        }
                        if(idx <= 9) {
                            infoTitleArr.push({"infoTitle": "상품코드", "infoCon": input_text });
                        } else {
                            infoTitleArr.push({"infoTitle":"    ", "infoCon": input_text });
                        }
                        input_text = "";
                    }
                } else {
                    infoTitleArr.push({"infoTitle":"상품코드", "infoCon":"   "});
                }
                infoTitleArr.push({"infoTitle":"Driver 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.VERSION_DRIVER)});
                infoTitleArr.push({"infoTitle":"채널정보 업데이트 날짜", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.DI_UPDATEPERIOD)});
                infoTitleArr.push({"infoTitle":"채널정보 버전(BDI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.BDI_VERSION)});
                infoTitleArr.push({"infoTitle":"채널정보 버전(PDI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.PDI_VERSION)});
                infoTitleArr.push({"infoTitle":"채널정보 버전(ADI)", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.ADI_VERSION)});
                infoTitleArr.push({"infoTitle":"네트워크 연결 상태", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.NETWORKSTATUE)});
                infoTitleArr.push({"infoTitle":"VOD SDK 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.CASTIS_SDK_VERSION)});
                infoTitleArr.push({"infoTitle":"앱스토어 서비스 버전", "infoCon":appstore_version});
                infoTitleArr.push({"infoTitle":"OAM 서비스 버전", "infoCon":oam_version});
                // 2017.04.27 dhlee 항목 삭제 (KT 요청)
                //infoTitleArr.push({"infoTitle":"개인방송 버전", "infoCon":individual_version});
                infoTitleArr.push({"infoTitle":"매시업 매니져 버전", "infoCon":mashup_version});
                infoTitleArr.push({"infoTitle":"Push 서비스 버전", "infoCon":push_version});
                // 2017.04.27 sw.nam 키즈 공용제어 -> 키즈 공용제어 버전 이름 변경
                infoTitleArr.push({"infoTitle":"키즈 공용제어 버전", "infoCon":basicAdapter.getConfigText(DEF.CONFIG.KEY.REMOTE_AGENT)});
                //[sw.nam]2017.05.16 음성클라이언트 버전 정보 추가
                if (!KTW.CONSTANT.STB_TYPE.ANDROID) {
                    var voice_version = basicAdapter.getConfigText(DEF.CONFIG.KEY.VOICE_VERSION);
                    if(voice_version === "undefined") {
                        infoTitleArr.push({"infoTitle":"음성 클라이언트 버전", "infoCon":"  "});
                    } else {
                        infoTitleArr.push({"infoTitle":"음성 클라이언트 버전", "infoCon":voice_version});
                    }
                }

                for (var idx = 0; idx < moduleData.length; idx++) {
                    infoTitleArr.push(getModuleVersionInfo(moduleData[idx]));
                }
            }
        }

        function getModuleVersionInfo (moduleData) {
            //[sw.nam] 2017.04.20 모듈버전 정보 추가
            //TODO 추후 버전 표시 내용 검토 필
            // 2017.04.27 dhlee
            // 우리집맞춤TV의 경우 명칭은 편성정보가 있는 경우 편성정보의 name 필드 + "모듈 버전" 을 사용
            // 편성되저 있지 앟은 경우 다른 모듈 이름과 동일하게 처리

            var moduleInfo = {
                infoTitle: moduleData.moduleName + " 버전"
            };

            if (moduleData.loadLocation === KTW.managers.module.Def.LOAD_LOCATION.SERVER) {
                moduleInfo.infoTitle = moduleData.moduleName + " 모듈 버전";
                moduleInfo.infoCon = moduleData.version + " (S)";
            }
            else if (moduleData.loadLocation === KTW.managers.module.Def.LOAD_LOCATION.LOCAL) {
                moduleInfo.infoCon = moduleData.version + " (L)";
            }
            else {
                if (moduleData.version === moduleData.localVersion && moduleData.serverLoading !== "true" && moduleData.serverLoading !== true) {
                    moduleInfo.infoCon = moduleData.version + " (L)";
                }
                else {
                    moduleInfo.infoTitle = moduleData.moduleName + " 모듈 버전";
                    moduleInfo.infoCon = moduleData.version + " (S)";
                }
            }

            if (moduleData.moduleId === KTW.managers.module.Module.ID.MODULE_FAMILY_HOME) {
                var menu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function(tempMenu) {
                        if (tempMenu.catType === KTW.DATA.MENU.CAT.SUBHOME && tempMenu.parent === null) {
                            return true;
                        }
                    }
                })[0];
                if (menu) {
                    moduleInfo.infoTitle = menu.name +" 모듈 버전"
                }
            }

            return moduleInfo;
        }

        function updateInfoArea(idx) {
            log.printDbg("updataInfoArea()");

            var startIdx = idx;

            for(var i = idx, j=0 ; i < startIdx + NUMBER_OF_ITEM_PER_PAGE, j < NUMBER_OF_ITEM_PER_PAGE ; i++, j++) {

                item[j].find(".infoTitle_text").text("");
                item[j].find(".infoContents_text").text("");

                if( i > infoTitleArr.length -1) {

                    item[j].find(".infoTitle_text").css({visibility: "hidden"});
                    item[j].find(".infoContents_text").css({visibility: "hidden"});
                } else {

                    item[j].find(".infoTitle_text").css({visibility: "inherit"});
                    item[j].find(".infoContents_text").css({visibility: "inherit"});
                    item[j].find(".infoTitle_text").text(infoTitleArr[i].infoTitle);
                    item[j].find(".infoContents_text").text(infoTitleArr[i].infoCon);
                }

                log.printDbg("startIdx()"+ startIdx);
                if( startIdx == 0 ){
                    item[0].find(".infoContents_text").css({
                        color: "rgba(221,175,120,1.0)", "font-family" : "RixHead B"
                    })
                }else {
                    item[0].find(".infoContents_text").css({
                        color: "rgba(255,255,255,0.7)" , "font-family" : "RixHead M"
                    });
                }

            }
        }
        function setScrollFocus(b){
            log.printDbg("setFocus()");
            if(b) {
                scroll.setFocus(true);
                isScrollArea = true;
                colorKeyArea.css({visibility: "inherit"});
            }
            else{
                scroll.setFocus(false);
                isScrollArea = false;
                colorKeyArea.css({visibility: "hidden"});
            }
        }

        function getMaxPage() {
            return Math.ceil(infoTitleArr.length / NUMBER_OF_ITEM_PER_PAGE);

        };

        function getCurrentPage() {
            return Math.floor(infoContentsIdx / NUMBER_OF_ITEM_PER_PAGE);
        };

        function setCurrentPage() {
            updateInfoArea(infoContentsIdx);
            scroll.setCurrentPage(getCurrentPage());
        }
    };
    KTW.ui.layer.setting.setting_systemInfo.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_systemInfo.constructor = KTW.ui.layer.setting.setting_systemInfo;

    KTW.ui.layer.setting.setting_systemInfo.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);
        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_systemInfo.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();