/**
 * Created by Yun on 2016-11-23.
 *
 * 설정 > 시스템 설정 > 이동식 디스크 연결
 */

(function () {
    KTW.ui.layer.setting.setting_usbConnect  = function UsbConnect(options) {
        KTW.ui.Layer.call(this, options);

        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var log = KTW.utils.Log;

        var deviceManager = KTW.managers.device.DeviceManager;
        //var stateManager = KTW.managers.service.StateManager;

        var INSTANCE = this;
        var styleSheet;
        var usb_mounted = false;
        var focusIdx = 0;
        var wizgame_response_timer = null;

        var KEY_CODE = KTW.KEY_CODE;

        var USB_APP = KTW.CONSTANT.USB_APP;
        var STORAGE = {
            POPUP_ID : "storage_popup",
            APP_CCID : KTW.RELEASE_MODE === "BMT" ?
                USB_APP.CCID.BMT : USB_APP.CCID.LIVE,
            APP_LOCATOR : KTW.RELEASE_MODE === "BMT" ?
                USB_APP.LOCATOR.BMT : USB_APP.LOCATOR.LIVE
        };

        var clock;
        var clockArea;


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame usbConnect"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
               // "<div style= 'position: absolute; left: 1450px; top: 916px; width: 390px; height: 2px; background-color: rgba(255,255,255,0.15)'></div>"+
                "<span id='backgroundTitle'>이동식 디스크 연결</span>" +
                    //"<img id='pig_shadow' src='images/pig_shadow.png' alt='pig_shadow'>" +
                    //"<img id='pig_gra' src='images/pig_gra.png' alt='pig_gra'>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
       //     createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            KTW.ui.LayerManager.showVBOBackground("bg_default");
            readConfiguration();

            focusIdx = 0;
            buttonFocusRefresh(focusIdx);
            infoTextRefresh();

            if(!options || !options.resume) {
                log.printDbg("new show()");
                var data = this.getParams();
                log.printDbg("thisMenuName:::::::::::"+data.name);
                if(data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_USB) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if(language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    }else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

         //   clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
   
        //    clock.hide();
        };

        this.controlKey = function (key_code) {
            switch (key_code) {
                case KEY_CODE.UP:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, -1, 2));
                    return true;
                case KEY_CODE.DOWN:
                    buttonFocusRefresh(focusIdx = KTW.utils.util.getIndex(focusIdx, 1, 2));
                    return true;
                case KEY_CODE.LEFT:
                    LayerManager.historyBack();
                    return true;
                case KEY_CODE.ENTER:
                    if (usb_mounted && focusIdx == 0) {
                        // 확인
                        tuneUSBAppDataChannel();

                    } else {
                        LayerManager.historyBack();
                    }
                    return true;

                default:
                    return false;
            }

        };

        function buttonFocusRefresh(index) {
            INSTANCE.div.find("#rightMenu_btn_area_0").removeClass("hide");
            INSTANCE.div.find("#rightMenu_btn_area_1").removeClass("hide");
            if (usb_mounted) {
                INSTANCE.div.find("#rightMenu_btn_area_1").addClass("hide");
                INSTANCE.div.find("#rightMenu_btn_area_0 .rightMenu_btn_div").removeClass("focus");
                INSTANCE.div.find("#rightMenu_btn_area_0 .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
            } else {
                INSTANCE.div.find("#rightMenu_btn_area_0").addClass("hide");
            }
        }

        function infoTextRefresh() {
            INSTANCE.div.find("#settingTable .table_subTitle").html((usb_mounted?"연결된 이동식 디스크 내용을 확인합니다":"이동식 디스크가 연결되어 있지 않습니다"));
            INSTANCE.div.find("#rightMenu_area #rightMenu_text").html((usb_mounted?"확인버튼을 선택하면<br>연결된 이동식 디스크에<br>저장되어 있는 파일들을<br> 확인하실 수 있습니다":"연결단자에 이동식<br>디스크를 연결하여 주시기<br>바랍니다"));
        }

        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<div id='settingTable'>" +
                "<div class='table_item_div'>" +
                "<div class='table_title'>이동식 디스크 연결</div>" +
                "<div class='table_subTitle'></div>" +
                "</div>" +
                "</div>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
                "<div id='rightMenu_bg'>" +
                "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
                "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
                "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
                "</div>" +
                "<div id='rightMenu_text'></div>" +
                "<div id='rightMenu_btn_area_0'>" +
                "<div class='rightMenu_btn_div'>" +

                "<div class='rightMenu_btn_text'>확인</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "<div class='rightMenu_btn_div'>" +
                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "<div id='rightMenu_btn_area_1'>" +
                "<div class='rightMenu_btn_div'>" +

                "<div class='rightMenu_btn_text'>취소</div>" +
                "<div class='rightMenu_btn_line'></div>" +
                "</div>" +
                "</div>" +
                "</div>");
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function readConfiguration() {
            log.printDbg("readConfiguration()");

            // [sw.nam] 콜백 등록
            deviceManager.storageDevice.setConnectionListener(onStorageConnectionListener);

            usb_mounted = deviceManager.storageDevice.isConnect;
            infoTextRefresh();

        }
        function onStorageConnectionListener(result) {
            log.printDbg("onStorageConnectionListener()::"+ JSON.stringify(result));
            if (result) {
                // [jh.lee] 연결
                usb_mounted = true;
            } else {
                // [jh.lee] 해제
                usb_mounted = false;
            }
            buttonFocusRefresh(focusIdx);
            infoTextRefresh();
        }
        
        function tuneUSBAppDataChannel() {
            log.printDbg("tuneUSBAppDataChannel()");

            // 2017 03.10, [sw.nam] USB app 실행. USB 연결시 뜨는 팝업로직 그대로 적용.
            var obj = {
                type : KTW.CONSTANT.APP_TYPE.USB,
                param : STORAGE.APP_LOCATOR
            };

            // wiz-game 상태라면 app 실행되지 않도록 처리
            // 이는 USB app으로 이동하는 과정에 wiz-game에서
            // lcw로 tune처리하는 로직에 의해 USB app으로 이동하지
            // 못하기 때문에 wiz-game 상태에서는
            // 실질적으는 popup만 닫는형태로 처리함 (정상시나리오라고 일단 간주)
            // wizgame 실행여부를 명시적으로 확인하여 동작하도록 수정함
            var message = {
                "from" : KTW.CONSTANT.APP_ID.HOME,
                "method" : KTW.managers.MessageManager.METHOD.EXT.AS_WIZGAME,
                "to" : KTW.CONSTANT.APP_ID.APPSTORE
            };

            wizgame_response_timer = setTimeout(function() {
                wizgame_response_timer = null;
                // [dj.son] AppServiceManager 로 수정
                //stateManager.exitToService(obj);
                KTW.managers.service.AppServiceManager.changeService({
                    nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                    obj: obj
                });
            }, 3000);

            KTW.managers.MessageManager.sendMessage(message, function(response) {
                if (wizgame_response_timer !== null) {
                    clearTimeout(wizgame_response_timer);
                    wizgame_response_timer = null;

                    if (response && response.result == 0) {
                        // [dj.son] AppServiceManager 로 수정
                        //stateManager.exitToService(obj);
                        KTW.managers.service.AppServiceManager.changeService({
                            nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                            obj: obj
                        });
                    } else {
                        log.printWarn("don't launch because of running wizGame");
                    }
                }
            });

        }

    };

    KTW.ui.layer.setting.setting_usbConnect.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_usbConnect.constructor = KTW.ui.layer.setting.setting_usbConnect;

    KTW.ui.layer.setting.setting_usbConnect.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };

    KTW.ui.layer.setting.setting_usbConnect.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

})();