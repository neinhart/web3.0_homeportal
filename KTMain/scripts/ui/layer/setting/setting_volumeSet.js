/**
 * Created by Yun on 2016-11-23.
 */
/*
 *
 * [sw.nam]
 * 2017.3.23
 * TODO : UHD3 STB 형상에서 직접 테스트 해봐야 함.. 현재 기능적 이슈 존재함.
 *
 *
 * */

(function () {
    KTW.ui.layer.setting.setting_volumeSet  = function VolumeSet(options) {
        KTW.ui.Layer.call(this, options);

        var AV_OUTPUT_DEVICE = KTW.oipf.Def.AV_OUTPUT.DEVICE;
        var AUDIO_TTS = KTW.oipf.Def.AUDIO_TTS;
        var VOLUM_STEP = 5;

        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var settingMenuManager = KTW.managers.service.SettingMenuManager;

        var INSTANCE = this;
        var styleSheet;

        var hwAdapter = KTW.oipf.adapters.HWAdapter;
        var audioTtsAdapter = KTW.oipf.AdapterHandler.audioTtsAdapter;
        var localSystem = oipfObjectFactory.createConfigurationObject().localSystem;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var clock;
        var clockArea;

        var data;


        var menuIdx = 0;
        var focusIdx = 0;

        var volumeModule = null;

        var volSet = 0;
        var originalVolSet;
        var volumeValue = 5;
        /** TTS 재생 ID.*/
        var speech_id = null;
        /** 메뉴 활성화 여부 - true : 원거리 음성 인식이 ON인 경우, false : 원거리 음성 인식이 OFF인 경우*/
        var is_menu_enable = false;

        //var layerIdx;

        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame volumeSet"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                //"<img id='backIcon' src='images/icon/bg_icon_setting.png'>" +
                "<img id='backIcon' src='images/homeshot_default.png'>" +
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
               // "<div style= 'position: absolute; left: 1450px; top: 916px; width: 390px; height: 2px; background-color: rgba(255,255,255,0.15)'></div>"+
                "<span id='backgroundTitle'>음성가이드 볼륨</span>" +
                    //"<img id='pig_shadow' src='images/pig_shadow.png' alt='pig_shadow'>" +
                "<div id='pig_dim'></div>" +
                    //"<img id='pig_gra' src='images/pig_gra.png' alt='pig_gra'>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
            createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);
            KTW.ui.LayerManager.showVBOBackground("bg_default");
            audioTtsAdapter.addSpeechListener(onSpeechStateChange);

            readConfiguration();

            menuIdx = 0;
            focusIdx = 0;
            buttonFocusRefresh(volSet, menuIdx);
            if(!options || !options.resume) {
                data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.SETTING_SPEAKER) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }
            KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(
                KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT, KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP,
                KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH, KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT);
            KTW.managers.service.IframeManager.changeIframe();
            clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();
            log.printDbg("hide VolumeSet");
            stopSampleSound();
            KTW.ui.LayerManager.hideVBOBackground();
            clock.hide();
        };

        this.controlKey = function (key_code) {
            switch(key_code) {
                case KEY_CODE.UP:
                    if (menuIdx == 0) {
                        if (volSet == 10) return true;
                        log.printDbg("volSet is ===  "+volSet);
                        volSet = ++volSet;
                        volSet = volSet > VOLUM_STEP ? VOLUM_STEP : volSet;
                        buttonFocusRefresh(volSet, menuIdx);
                        startSampleSound();
                    } else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, 2), menuIdx);
                    }
                    return true;
                case KEY_CODE.DOWN:
                    if (menuIdx == 0) {
                        if (volSet == 0) return true;
                        log.printDbg("volSet is ===  "+volSet);
                        volSet = --volSet;
                        volSet = volSet < 0 ? 0 : volSet;
                        buttonFocusRefresh(volSet, menuIdx);
                        startSampleSound();

                    } else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, 2), menuIdx);
                    }
                    return true;
                case KEY_CODE.LEFT:
                    if(menuIdx == 0) {
                        volumeModule.setVolume(originalVolSet / VOLUM_STEP);
                        volumeValue = originalVolSet;
                        LayerManager.historyBack();
                    }


                    focusIdx = 0;
                    buttonFocusRefresh(volSet, 0);
                    return true;
                case KEY_CODE.RIGHT:
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    return true;
                case KEY_CODE.ENTER:
                    pressEnterKeyEvent(focusIdx, menuIdx);
                    return true;
                default:
                    return false;
            }
        };

        function pressEnterKeyEvent(index, menuIndex) {
            switch(menuIndex) {
                case 0:
                    focusIdx = 0;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case 1:
                    if (focusIdx == 0) {
                        // 시스템 저장
                        try {
                            log.printDbg("keyOk() - volumeValue : " + volSet);
                            volumeModule.setVolume(volSet / VOLUM_STEP);
                            volumeValue = volSet;
                            data.complete();
                            settingMenuManager.historyBackAndPopup();
                        }catch(e){
                            log.printExec(e);
                        }
                    } else {
                        volumeModule.setVolume(originalVolSet / VOLUM_STEP);
                        volumeValue = originalVolSet;
                        LayerManager.historyBack();
                    }

                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            menuIdx = menuIndex;
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            if (menuIndex == 0) {
                volSet = index;
                INSTANCE.div.find("#contents #volumeFocus_area").css("-webkit-transform", "translateY(" + (-index * 93.6) + "px)");//46.8
                INSTANCE.div.find("#contents #volumeUnFocus_area").css("-webkit-transform", "translateY(" + (-index * 93.6) + "px)");//46.8
                if(volSet < 1) {
                    INSTANCE.div.find("#contents #volumeFocus_area #volumeDownArrow_img").css({visibility: "hidden"});
                }else if(volSet > 4) {
                    INSTANCE.div.find("#contents #volumeFocus_area #volumeUpArrow_img").css({visibility: "hidden"});
                }
                else {
                    INSTANCE.div.find("#contents #volumeFocus_area #volumeUpArrow_img").css({visibility: "inherit"});
                    INSTANCE.div.find("#contents #volumeFocus_area #volumeDownArrow_img").css({visibility: "inherit"});
                }
                INSTANCE.div.find("#contents #volumeFocus_area #volumeFocus_text").text(index);
                INSTANCE.div.find("#contents #volumeUnFocus_area #volumeUnFocus_text").text(index);
                INSTANCE.div.find("#contents #volumeShow_div #volumeSet_text").text(index);
                if (index == 0) {
                    INSTANCE.div.find("#contents #volumeShow_div #volumeIcon_img").attr("src", "images/icon/icon_set_vol_mute.png");
                } else {
                    INSTANCE.div.find("#contents #volumeShow_div #volumeIcon_img").attr("src", "images/icon/icon_set_vol.png");
                }
                setVolumeFocus(true);
            } else {
                focusIdx = index;
                setVolumeFocus(false);
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
            }
        }
        function setVolumeFocus(b) {
            if(b) {
                INSTANCE.div.find("#contents #volumeFocus_area").css({visibility:"inherit"});
                INSTANCE.div.find("#contents #volumeUnFocus_area").css({visibility:"hidden"});
            }else {
                INSTANCE.div.find("#contents #volumeFocus_area").css({visibility:"hidden"});
                INSTANCE.div.find("#contents #volumeUnFocus_area").css({visibility:"inherit"});
            }
        }
        function createComponent() {
            INSTANCE.div.find("#contents").html("");
            INSTANCE.div.find("#contents").append("<table id='settingTable'>" +
            "<tr>" +
            "<td>" +
            "<div class='table_item_div'>" +
            "<div class='table_title'>음성가이드 볼륨</div>" +
            "<div class='table_subTitle'>셋톱박스에 내장된 스피커 볼륨을 설정하세요<br>음성가이드가 설정된 크기의 볼륨으로 제공됩니다</div>" +
            "</div>" +
            "</td>" +
            "</tr>" +
            "<tr>" +
            "<td>" +
            "<table id='volumeStep_area'>" +
            "<tr><td></td></tr>" +
            "<tr><td></td></tr>" +
            "<tr><td></td></tr>" +
            "<tr><td></td></tr>" +
            "<tr><td></td></tr>" +
            "</table>" +
            "<div id='volumeUp_text'>볼륨 +</div>" +
            "<div id='volumeDown_text'>볼륨 -</div>" +
            "<div id='volumeBar_img'>" +
            "<img id='volumeBar_Top_img' style='position: absolute; margin-top: -15px;' src='images/setting/vol_set_up.png'>" +
            "<img id='volumeBar_center_img' src='images/setting/vol_set.png'>" +
            "<img id='volumeBar_bottom_img' src='images/setting/vol_set_dw.png'>" +
            "</div>" +
            "<div id='volumeShow_div'>" +
            "<img id='volumeIcon_img' src='images/icon/icon_set_vol.png'>" +
            "<div id='volumeSet_text'>" + volSet + "</div>" +
            "</div>" +
            "<div id='volumeFocus_area'>" +
            "<img id='volumeUpArrow_img' src='images/setting/arw_scr_up.png'>" +
            "<img id='volumeFocus_img' src='images/setting/focus_scr.png'>" +
            "<img id='volumeDownArrow_img' src='images/setting/arw_scr_dw.png'>" +
            "<div id='volumeFocus_text'>" + volSet + "</div>" +
            "</div>" +
            "<div id='volumeUnFocus_area'>" +
            "<img id='volumeUnFocus_img' src='images/setting/set_vol_dim.png'>"+
            "<span id='volumeUnFocus_text'>"+volSet+ "</span>"+
            "</div>"+
            "</td>" +
            "</tr>" +
            "</table>");
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>셋톱박스의 스피커에서<br>나오는 음성가이드 볼륨을<br>조절합니다</div>" +
            "<div id='rightMenu_btn_area'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "</div>" +
            "</div>" +
            "</div>");
        }
        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        function readConfiguration() {
            log.printDbg("readConfiguration()");
            if( !volumeModule ){
                try {
                    volumeModule = KTW.oipf.AdapterHandler.extensionAdapter.getEmbeddedSpeakerVolumeController();
                    volumeValue = volumeModule.getVolume() * VOLUM_STEP;
                }catch(e){
                    log.printErr(e);
                    log.printErr("Cannot load embedded volume controller!");
                }
            } else {
                log.printDbg("readConfiguration() - volumeModule.getVolume() : "+volumeModule.getVolume());
                volumeValue = volumeModule.getVolume() * VOLUM_STEP;
            }
            volumeValue = Math.floor(volumeValue);
            log.printDbg("readConfiguration() - volumeValue : " + volumeValue);

            volSet = volumeValue;
            originalVolSet = volSet;
        }

        /**
         * 샘플 사운드를 재생한다.
         */
        function startSampleSound() {
            log.printDbg("called startSampleSound() - speech_id : " + speech_id + ", Volume : " + (volSet / VOLUM_STEP));
            if (speech_id) {//샘플 사운드 재생 중인 경우
                //기존 샘플 사운드 재생 종료
                audioTtsAdapter.stopSpeech(speech_id);
            } else {//샘플 사운드 재생 안되고 있는 경우
                //AV 오디오 OFF
                if (localSystem.mediaAudioEnabled !== undefined) {
                    localSystem.mediaAudioEnabled = false;
                }
                //내장 스피커 ON
                hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.INTERNAL_SPEAKER, true);
                //메인 스피커 OFF
                hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.MAIN, false);
            }

            //볼륨 설정
            volumeModule.setVolume(volSet / VOLUM_STEP);
            //TTS 재생
            speech_id = audioTtsAdapter.startSpeech("올레티비");
            log.printDbg("startSampleSound() - startSpeech, speech_id : " + speech_id);
            //TTS 재생 실패 시 원복
            if(speech_id === 0) {
                stopSampleSound();
            }
        }

        /**
         * 샘플 사운드 재생을 중지한다.
         */
        function stopSampleSound() {
            log.printDbg("called stopSampleSound() - speech_id : " + speech_id);
            //샘플 사운드 재생 안되고 있는 경우
            if (!speech_id) {
                log.printDbg("stopSampleSound() - return, invalid speech_id");
                return;
            }
            //TTS 재생 중지
            if (speech_id) {
                audioTtsAdapter.stopSpeech(speech_id);
            }
            speech_id = null;
            //내장 스피커 OFF
            hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.INTERNAL_SPEAKER, false);
            //메인 스피커 ON
            hwAdapter.setEnableAVOutput(AV_OUTPUT_DEVICE.MAIN, true);
            //AV 오디오 ON
            if (localSystem.mediaAudioEnabled !== undefined) {
                localSystem.mediaAudioEnabled = true;
            }
        }


        /**
         * 음성 재생 동작의 상태가 변경되는 경우 발생하는 이벤트 Listener
         *
         * @param id 음성 재생 동작을 나타내는 유니크 아이디
         * @param state 음성 재생 동작 상태
         * @see KTW.oipf.Def.AUDIO_TTS.STATE
         */
        function onSpeechStateChange(id, state) {
            log.printDbg("called onSpeechStateChange() - id : " + id + ", state : " + state);
            if (speech_id === id &&
                (state === AUDIO_TTS.STATE.STATE_ERROR || state === AUDIO_TTS.STATE.STATE_END)) {
                stopSampleSound();
            }
        }
    };
    KTW.ui.layer.setting.setting_volumeSet.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_volumeSet.constructor = KTW.ui.layer.setting.setting_volumeSet;

    KTW.ui.layer.setting.setting_volumeSet.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };



    KTW.ui.layer.setting.setting_volumeSet.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();