/**
 * Created by Yun on 2017-01-25.
 *
 * 설정 > 시스템 설정 > 중계기 (OTS Only)
 */

(function () {
    KTW.ui.layer.setting.setting_repeater  = function SetRepeater(options) {
        KTW.ui.Layer.call(this, options);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var KEY_CODE = KTW.KEY_CODE;
        var Layer = KTW.ui.Layer;
        var LayerManager =KTW.ui.LayerManager;
        var hwAdapter = KTW.oipf.adapters.HWAdapter;
        var INSTANCE = this;
        var styleSheet;

        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        var stateManager = KTW.managers.service.StateManager;
        //var vodAdapter = KTW.oipf.AdapterHandler.vodAdapter;
        var networkManager = KTW.managers.device.NetworkManager;
        //var DEF = KTW.oipf.Def;

        var focusIdx = 0;
        var selectedElementIdx = 0;
        var selectedRepeaterName = "";
        var menuIdx = 0;
        var isCustom = false;
        var state = KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW;
        var prevState = null; //[sw.nam] 화면 navigation을 위한 이전 뷰의 state 를 저장.

        var trans_ponder_list;
        var trans_info;

        var clock;
        var clockArea;

        // element  instance

        var isScrollArea = false;

        var item = [];
        var scrollArea = null;
        var scroll = null;
        var colorKeyArea;
        var transponderIdx = 0;


        /* page instance */
        var currentSetPage;

        var NUMBER_OF_PREVIEW_LIST = 14;


        /* instance from Homportal 2.0*/

        var is_saving= false;
        var MESSAGES = [ "주파수를 수동 조정할 수 있습니다",
            "지정하신 중계기를 탐색중입니다   잠시만 기다려 주세요" ];
        var MOD_DVB_S = "0";
        var MOD_DVB_S2_QPSK = "1";
        var MOD_DVB_S2_8PSK = "2";

        var MOD_SYSTEM_DVB_S2 = "1";
        var MOD_SYSTEM_DVB_S = "0";

        var MOD_QPSK = 20;
        var MOD_8PSK = 21;

        // 전송 규격
        var  MODULATION = [
            [MOD_DVB_S, "DVB_S"],
            [MOD_DVB_S2_QPSK, "DVB_S2_QPSK"],
            [MOD_DVB_S2_8PSK, "DVB_S2_8PSK"] ];

        var LINEAR_HORIZONTAL = "0";
        var LINEAR_VERTICAL = "1";

        // 편파 설정
        var BIASED_WAVE = [
            [  LINEAR_HORIZONTAL,  "수평 -18V" ],
            [  LINEAR_VERTICAL, "수직 -13V" ] ];

        var FEC_INNER_1OVER2_CONV = 40;
        var FEC_INNER_2OVER3_CONV = 41;
        var FEC_INNER_3OVER4_CONV = 42;
        var FEC_INNER_5OVER6_CONV = 43;
        var FEC_INNER_7OVER8_CONV = 44;

        // FEC 1 - MOD_DVB_S
        var  FEC_DVB_S = [
            [FEC_INNER_1OVER2_CONV, "1/2"],
            [FEC_INNER_2OVER3_CONV, "2/3"],
            [FEC_INNER_3OVER4_CONV, "3/4"],
            [FEC_INNER_5OVER6_CONV, "5/6"],
            [FEC_INNER_7OVER8_CONV, "7/8"] ];

        var FEC_INNER_4OVER5_CONV = 50;
        var FEC_INNER_3OVER5_CONV = 51;
        var FEC_INNER_8OVER9_CONV = 52;
        var FEC_INNER_9OVER10_CONV = 53;

        // FEC2 - MOD_DVB_S2_QPSK
        var FEC_DVB_S2_QPSK = [
            [FEC_INNER_1OVER2_CONV, "1/2"],
            [FEC_INNER_2OVER3_CONV, "2/3"],
            [FEC_INNER_3OVER4_CONV, "3/4"],
            [FEC_INNER_4OVER5_CONV, "4/5"],
            [FEC_INNER_5OVER6_CONV, "5/6"],
            [FEC_INNER_3OVER5_CONV, "3/5"],
            [FEC_INNER_8OVER9_CONV, "8/9"],
            [FEC_INNER_9OVER10_CONV, "9/10"] ];

        // FEC3 - MOD_DVB_S2_8PSK
        var FEC_DVB_S2_8PSK = [
            [FEC_INNER_2OVER3_CONV, "2/3"],
            [FEC_INNER_3OVER4_CONV, "3/4"],
            [FEC_INNER_5OVER6_CONV, "5/6"],
            [FEC_INNER_3OVER5_CONV, "3/5"],
            [FEC_INNER_8OVER9_CONV, "8/9"],
            [FEC_INNER_9OVER10_CONV, "9/10"] ];

        var ROLL_OFF_NOT_DEFINED = "3";
        var ROLL_OFF_A020 = "2";
        var ROLL_OFF_A025 = "1";
        var ROLL_OFF_A035 = "0";

        // Role-off - MOD_DVB_S2_QPSK, MOD_DVB_S2_8PSK 인 경우
        var ROLLOFF = [
            [ROLL_OFF_NOT_DEFINED, "None"],
            [ROLL_OFF_A020, "0.20"],
            [ROLL_OFF_A025, "0.25"],
            [ROLL_OFF_A035, "0.35"] ];

        // Role-off MOD_DVB_S 인 경우.
        var ROLLOFF_NONE = [
            [ROLL_OFF_NOT_DEFINED, "Not Available"] ];

        var ROLL_OFF_SET;
        var FEC_SET;

        //var input_index = 0;

        var submit_index = 0;
        var selected_edit_index = 0;

        var view_edit = false;
        var view_detail = false;
        //var select_user_ts = false;
        var result_data;
        var ts_info;
        var additional_info ;

        var select_liner = 0;
        var select_roll_off = 0;
        var select_modul = 0;
        var select_fec = 0;
        var symbol_rate = "";
        var frequency = "";

        //var pagerView;

        var collect_timer;

        //var data;

        // [jh.lee] 채널 또는 VOD 가 중계기 탐색으로 중지되는 경우 flag를 true로 설정
        var stop_flag = false;
        // [jh.lee] VOD 재생중 중지된 경우 (채널튠이 아닌 exitToService로 상태정리 필요)
        var vod_stop_flag = false;

        //focus이동
        //var focus_box1 = null;
//////////////////////////////////////////


        this.init = function (cbCreate) {
            this.div.attr({class: "arrange_frame setRepeater"});

            styleSheet = $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "styles/setting/setting_layer/main.css"
            });

            this.div.html("<div id='background'>" +
                "<div id='backDim'style=' position: absolute; width: 1920px; height: 1080px; background-color: black; opacity: 0.9;'></div>"+
                "<img id='bg_menu_dim_up' style='position: absolute; left:0px; top: 0px; width: 1920px; height: 280px; ' src='images/bg_menu_dim_up.png'>"+
                "<img id='bg_menu_dim_up'  style='position: absolute; left:0px; top: 912px; width: 1920px; height: 168px;' src ='images/bg_menu_dim_dw.png'>"+
                "<img id='backgroundTitleIcon' src ='images/ar_history.png'>"+
                "<span id='backgroundTitle'>중계기</span>" +
                "<div id='pig_dim'></div>" +
                "</div>" +
                "<div id='contentsArea'>" +
                "<div id='contents'></div>" +
                "</div>"
            );

            createComponent();
        //    createClock();

            cbCreate(true);
        };

        this.show = function (options) {
            $("head").append(styleSheet);
            Layer.prototype.show.call(this);

            is_saving = false;

            trans_ponder_list = hwAdapter.getTSInfos();
            //[sw.nam] 받아온 중계기로 초기 화면을 그려준다.
            //초기 인덱스 설정
            transponderIdx =1;
            unselectTransponderButton();
            // 현재 보여지는 페이지의 중계기 리스트 업데이트
            updateListArea(transponderIdx);
            selectTransponderButton(transponderIdx);
            buttonFocusRefresh(transponderIdx, 0);

            // 페이지 갯수 정의
            if(trans_ponder_list) {
                scroll.show({
                    maxPage: getMaxPage(),
                    curPage: getCurrentPage()
                });

            }

            if(isScrollArea){
                setScrollFocus(false);
                setColorKeyArea(true);
            }
            setColorKeyArea(true);

            // [sw.nam] state 변경을 통해 해당 view로 점프하는 것 같다. 초기 view 는 LIST view(중계기가 보이는 view)
            state = KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW;
            // state = KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW;
           // selectedElementIdx = focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
            selectedElementIdx = focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
            isCustom = false;
            changeViewState(state);
            if(!options || !options.resume) {
                var data = this.getParams();
                log.printDbg("thisMenuName:::::::::::" + data.name);
                if (data.name == undefined) {
                    var thisMenu;
                    var language;
                    thisMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getNormalMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.TRANSPONDER) {
                                return true;
                            }
                        }
                    })[0];
                    language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
                    if (language === "kor") {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.name);
                    } else {
                        INSTANCE.div.find("#backgroundTitle").text(thisMenu.englishItemName);
                    }

                } else {
                    INSTANCE.div.find("#backgroundTitle").text(data.name);
                }
            }

        //    clock.show(clockArea);
        };

        this.hide = function () {
            Layer.prototype.hide.call(this);
            styleSheet = styleSheet.detach();

            clearTimeout(collect_timer); // TODO 상세보기 아닐떄는 timer 를 반드시 멈춰줘야함
            collect_timer = null;
            networkManager.enableWeakSignal(true);

            if(stop_flag) {
                navAdapter.changeChannel(navAdapter.getCurrentChannel(), true);
                stop_flag = false;
            }

         //   clock.hide();

        };

        this.controlKey = function (key_code) {
            var consumed = false;
            var beingSetPage;
            if( is_saving ){
                // 저장에 있을 경우 키를 막는다
                return true;
            }
            switch (key_code) {
                case KEY_CODE.UP:
                    log.printDbg("state == "+state);
                    if(state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW && menuIdx == 0) {
                        if(isScrollArea) {
                            transponderIdx = transponderIdx - ( NUMBER_OF_PREVIEW_LIST + (transponderIdx % NUMBER_OF_PREVIEW_LIST));

                            if(transponderIdx < 0) {
                                transponderIdx = trans_ponder_list.length-1 - ((trans_ponder_list.length-1) % NUMBER_OF_PREVIEW_LIST);
                            }
                        } else {
                            transponderIdx = transponderIdx -2;

                            if (transponderIdx < 0) {
                                transponderIdx = trans_ponder_list.length - 1;
                            }
                        }
                        beingSetPage = getCurrentPage();
                        if( currentSetPage != beingSetPage ){
                            unselectTransponderButton();
                            setCurrentPage();
                        }
                        focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;

                        if(!isScrollArea) {
                            buttonFocusRefresh(focusIdx,menuIdx);
                        }
                    }

                    else {
                        log.printDbg("state == "+state);
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, (state == KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW? 2 : (menuIdx == 0 ? 6 : (isCustom? 3 : 2)))), menuIdx);
                        if(state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW && menuIdx == 0 ) {
                            selected_edit_index = focusIdx;
                        }
                    }

                    log.printDbg("transponderIdx == "+transponderIdx);
                    log.printDbg("focusIdx == "+focusIdx);
                    consumed = true;
                    return consumed;

                case KEY_CODE.DOWN:
                    log.printDbg("state == "+state);
                    if(state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW && menuIdx == 0) {
                        if(isScrollArea) {
                            transponderIdx = transponderIdx + (NUMBER_OF_PREVIEW_LIST - (transponderIdx % NUMBER_OF_PREVIEW_LIST));
                        } else {
                            // 마지막 포커스 끝처리
                            if(transponderIdx +2 > trans_ponder_list.length-1) {
                                transponderIdx++;
                            }else {
                                transponderIdx = transponderIdx +2;
                            }
                        }
                        if(transponderIdx >  trans_ponder_list.length-1) {
                            transponderIdx = 0;
                        }

                        beingSetPage = getCurrentPage();
                        if( currentSetPage != beingSetPage ) {
                            unselectTransponderButton();
                            setCurrentPage();
                        }
                        focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;

                        if(!isScrollArea) {
                            buttonFocusRefresh(focusIdx,menuIdx);
                        }
                    }
                    else {
                        log.printDbg("state == "+state);
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, (state == KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW? 2 : (menuIdx == 0 ? 6 : (isCustom? 3 : 2)))), menuIdx);
                        if(state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW && menuIdx == 0 ) {
                            selected_edit_index = focusIdx;
                        }
                    }
                    log.printDbg("transponderIdx == "+transponderIdx);
                    log.printDbg("focusIdx == "+focusIdx);
                    consumed = true;
                    return consumed;
                case KEY_CODE.RED:
                    if(state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW && menuIdx == 0) {
                        if(isScrollArea) {
                            transponderIdx = transponderIdx - (transponderIdx % NUMBER_OF_PREVIEW_LIST);
                            setScrollFocus(false);
                        }else {
                            if(transponderIdx % NUMBER_OF_PREVIEW_LIST === 0) {
                                //이미 가장 상위 인덱스를 가리키고 있는 경우
                                transponderIdx = transponderIdx - NUMBER_OF_PREVIEW_LIST;// + (transponderIdx % NUMBER_OF_PREVIEW_LIST));
                                if(transponderIdx < 0) {
                                    transponderIdx = trans_ponder_list.length-1 - ((trans_ponder_list.length-1) % NUMBER_OF_PREVIEW_LIST);
                                }
                                //페이지 전환
                                unselectTransponderButton();
                                setCurrentPage();
                            }else {
                                transponderIdx = transponderIdx - (transponderIdx % NUMBER_OF_PREVIEW_LIST);
                            }
                        }
                        focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                        buttonFocusRefresh(focusIdx,menuIdx);
                    }
                    consumed = true;
                    return consumed;
                case KEY_CODE.BLUE:
                    if(state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW && menuIdx == 0) {
                        if(isScrollArea) {
                            transponderIdx = transponderIdx  - (transponderIdx % NUMBER_OF_PREVIEW_LIST) + (NUMBER_OF_PREVIEW_LIST-1);
                            if(transponderIdx >  trans_ponder_list.length-1) {
                                transponderIdx = trans_ponder_list.length-1;
                            }
                            setScrollFocus(false);
                        }else {
                            if(transponderIdx == trans_ponder_list.length-1 || transponderIdx % NUMBER_OF_PREVIEW_LIST == NUMBER_OF_PREVIEW_LIST -1) {
                                //이미 가장 하위 인덱스를 가리키고 있는 경우
                                // 마지막 페이지인 경우
                                if(transponderIdx == trans_ponder_list.length -1) {
                                    if(trans_ponder_list.length < transponderIdx + NUMBER_OF_PREVIEW_LIST) {
                                        transponderIdx = trans_ponder_list.length-1;
                                    }else {
                                        transponderIdx = NUMBER_OF_PREVIEW_LIST - 1;
                                    }
                                }else {
                                    transponderIdx = transponderIdx + NUMBER_OF_PREVIEW_LIST;
                                    if(transponderIdx > trans_ponder_list.length -1) {
                                        transponderIdx = trans_ponder_list.length - 1;
                                    }
                                }
                                //페이지 전환
                                unselectTransponderButton();
                                setCurrentPage();
                            }else {
                                // 현재 페이지의 가장 아래로 보낸다.
                                transponderIdx = transponderIdx - (transponderIdx % NUMBER_OF_PREVIEW_LIST) + (NUMBER_OF_PREVIEW_LIST-1);
                                if(transponderIdx > trans_ponder_list.length-1) {
                                    // 마지막 페이지 일 경우 예외처리
                                    transponderIdx = trans_ponder_list.length-1;
                                }
                            }
                        }
                        focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                        buttonFocusRefresh(focusIdx,menuIdx);
                    }
                    consumed = true;
                    return consumed;
                case KEY_CODE.LEFT:
                    if (state == KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW || state == KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW) {
                        changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW);
                        if(collect_timer) {
                            clearTimeout(collect_timer);
                            collect_timer = null;
                        }
                        return true;
                    }
                    if (state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW && menuIdx == 0) {
                        if (isScrollArea) {
                            LayerManager.historyBack();
                            return true;
                        }

                        if(transponderIdx % 2 == 0) {
                            // 좌측 인덱스인경우 나가기
                            LayerManager.historyBack();
                        }else {
                            transponderIdx --;
                        }
                        focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                        buttonFocusRefresh(focusIdx,menuIdx);
                        //removeFocusHighlight();

                    }
                    if (state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW && menuIdx == 1) {

                        transponderIdx = selectedElementIdx;
                        setCurrentPage();
                        focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                        buttonFocusRefresh(focusIdx, 0);
                        consumed = true;
                        return consumed;
                    }
                    if(state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW && menuIdx == 0) {
                        log.printDbg("selected_edit_index" + selected_edit_index);
                        if (selected_edit_index === 0) {
                            select_modul = (--select_modul + MODULATION.length) % MODULATION.length;
                            select_fec = 0;
                        } else if (selected_edit_index === 1) { // 신호 주파수 입력
                            keyDel();
                        } else if (selected_edit_index === 2) {
                            select_liner = (--select_liner + BIASED_WAVE.length) % BIASED_WAVE.length;
                        } else if (selected_edit_index === 3) { // 심볼레이트
                            keyDel();
                        } else if (selected_edit_index === 4) {
                            select_fec = (--select_fec + FEC_SET.length) % FEC_SET.length;
                        } else if (selected_edit_index === 5) {
                            select_roll_off = (--select_roll_off + ROLL_OFF_SET.length) % ROLL_OFF_SET.length;
                        }
                        setEditData();
                        consumed = true;
                        return consumed;
                    }
                    if(state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW && menuIdx == 1) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 0);
                        consumed = true;
                        return consumed;
                    }
                    return true;
                case KEY_CODE.RIGHT:
                    if (state == KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW || state == KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW) return true;
                    if( menuIdx ==0 && state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW ) {

                        if(isScrollArea) {
                            buttonFocusRefresh(focusIdx, 0);
                            setScrollFocus(false);
                        } else {
                            //selectedElementIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                            //selectTransponderButton(selectedElementIdx);
                            if(transponderIdx % 2 == 1) {
                                // 우측 인덱스인 경우 버튼영역으로 이동
                                focusIdx = 0;
                                buttonFocusRefresh(focusIdx, 1);
                            }else {
                                transponderIdx ++;
                                if(transponderIdx > trans_ponder_list.length-1) {
                                    focusIdx = 0;
                                    buttonFocusRefresh(focusIdx, 1);
                                }else {
                                    focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                                    buttonFocusRefresh(focusIdx,menuIdx);
                                }
                            }
                        }
                        consumed = true;
                        return consumed;
                    }
                    if( menuIdx == 1 && state == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW) {
                        consumed = true;
                        return consumed;
                    }

                    if( menuIdx == 0 && state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW) {
                        log.printDbg("selected_edit_index" + selected_edit_index);
                        if (selected_edit_index === 0) {
                            select_modul = ++select_modul % MODULATION.length;
                            select_fec = 0;
                        } else if (selected_edit_index === 2) {
                            select_liner = ++select_liner % BIASED_WAVE.length;
                        } else if (selected_edit_index === 4) {
                            select_fec = ++select_fec % FEC_SET.length;
                        } else if (selected_edit_index === 5) {

                            select_roll_off = ++select_roll_off % ROLL_OFF_SET.length;
                        }
                        setEditData();
                        consumed = true;
                        return consumed;
                    }

                    if( menuIdx == 1 && state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW) {
                        consumed = true;
                        return consumed;
                    }

                    if (menuIdx == 0) {
                        focusIdx = 0;
                        buttonFocusRefresh(focusIdx, 1);
                        consumed = true;
                        return consumed;
                    }
                case KEY_CODE.ENTER:
                    if(isScrollArea) {
                        buttonFocusRefresh(focusIdx, 0);
                        setScrollFocus(false);
                    }else {
                        pressEnterKeyEvent(focusIdx, menuIdx);
                    }
                    consumed = true;
                    return true;
                case KEY_CODE.BACK:
                    if (state == KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW || state == KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW) {
                        if(collect_timer) {
                            clearTimeout(collect_timer);
                            collect_timer = null;
                        }
                        changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW);
                        return true;
                    }else if(state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW){
                        // sw.nam 중계기 사용자 설정 화면에서 전환될 수 있는 화면은 2가지 경우이므로
                        // 해당 경우에 맞게 처리해주어야 함
                        if(prevState == KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW) {
                            changeViewState(prevState);
                        } else if((prevState = KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW)) {
                            isCustom = true;
                            showDetail();
                        }
                        return true;
                    }else {
                       return false;
                    }
                    break;
            }

            if (consumed === false && menuIdx == 0 && state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW) {
                var number = util.keyCodeToNumber(key_code);
                if (number >= 0) {
                    consumed = keyNumber(number);
                }
            }
            return consumed;

        };

        function keyNumber(number) {
            if (state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW) {
                if (selected_edit_index === 1) {
                    if (frequency.length < 14) {
                        frequency += number;
                        setEditData();
                    }
                } else if (selected_edit_index === 3) {
                    if (symbol_rate.length < 14) {
                        symbol_rate += number;
                        setEditData();
                    }
                }
            }

            if (state == KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW || state == state == KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW || state == KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW) {
                // [jh.lee] 중계기 설정 보기, 또는 중계기 사용자 수정일 경우 숫자키 소진
                return true;
            }

            return false;
        }

        function keyDel() {

            if (selected_edit_index === 1) {
                if (frequency.length > 0) {
                    frequency = frequency.substring(0, frequency.length - 1);
                    setEditData();
                }
            } else if (selected_edit_index === 3) {
                if (symbol_rate.length > 0) {
                    symbol_rate = symbol_rate.substring(0, symbol_rate.length - 1);
                    setEditData();
                }
            }

        }

        function setEditData() {
            setFECAndRoleOffData(select_modul);

            //TODO : 설정 된 설정 화면에 SET 하는 과정
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area .repeatSetting_div").eq(0).find(".repeatSetting_btnValue").text(MODULATION[select_modul][1]);
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area .repeatSetting_div").eq(1).find(".repeatSetting_btnValue").text(frequency);
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area .repeatSetting_div").eq(2).find(".repeatSetting_btnValue").text(BIASED_WAVE[select_liner][1]);
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area .repeatSetting_div").eq(3).find(".repeatSetting_btnValue").text(symbol_rate);
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area .repeatSetting_div").eq(4).find(".repeatSetting_btnValue").text(FEC_SET[select_fec][1]);
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area .repeatSetting_div").eq(5).find(".repeatSetting_btnValue").text(ROLL_OFF_SET[select_roll_off][1]);

        }

        function changeViewState(viewState) {
            prevState = state;
            state = viewState;
            for (var idx = 0 ; idx < 5; idx ++) {
                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area" + idx).addClass("hide");
            }
            INSTANCE.div.find("#contents #settingTable #table_radio_area").addClass("hide");
            INSTANCE.div.find("#contents #settingTable #repeatInfo_area").addClass("hide");
            INSTANCE.div.find("#contents #settingTable #repeatSetting_area").addClass("hide");
            INSTANCE.div.find("#contents #settingTable #table_subTitle").addClass("hide");
            switch(viewState) {
                case KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW:
                    if(trans_ponder_list) {
                        scroll.show({
                            maxPage: getMaxPage(),
                            curPage: getCurrentPage()
                        });
                    }
                    if(stop_flag) {
                        navAdapter.changeChannel(navAdapter.getCurrentChannel(), true);
                        stop_flag = false;
                    }
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_text").html("중계기를 선택하거나<br>설정을 변경할 수 있습니다");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #table_radio_area").removeClass("hide");
                    INSTANCE.div.find("#backgroundTitle").text("중계기");
                    INSTANCE.div.find("#contents #settingTable #table_title").text("중계기");

                    transponderIdx = selectedElementIdx;
                    setCurrentPage();
                    focusIdx = transponderIdx % NUMBER_OF_PREVIEW_LIST;
                    //focusIdx = selectedElementIdx % NUMBER_OF_PREVIEW_LIST;
                    buttonFocusRefresh(focusIdx, 0);
                    setColorKeyArea(true);
                    networkManager.enableWeakSignal(true);
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW:
                    // 중계기 상세 보기 -> timer 로 계속 업데이트 된다.
                    var result = "";
                    var value;
                    var signal_level;
                    var signal_quality;

                    scroll.hide();
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_text").html("중계기 정보 및 신호 상태를<br>확인할 수 있습니다");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area2").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #table_subTitle").removeClass("hide");

                    INSTANCE.div.find("#backgroundTitle").text(selectedRepeaterName);
                    INSTANCE.div.find("#contents #settingTable #table_title").text(selectedRepeaterName);

                    // step 1. 수신 상태 체크
                    if (result_data) {
                        if (additional_info[3] > -120) {
                            result = "수신상태가 양호합니다";
                        } else {
                            result = "수신상태가 미약합니다";
                        }
                    }
                    if(result) {
                        INSTANCE.div.find("#contents #settingTable #table_subTitle").text("[" + selectedRepeaterName + "] " + result);
                    }else {
                        INSTANCE.div.find("#contents #settingTable #table_subTitle").text("");
                    }

                    // step 2. 신호 레벨 & 품질 업데이트

                    if (ts_info) {
                        value = 120 + additional_info[3];

                        if (value < 0) {
                            value = 0;
                        } else if (value > 100) {
                            value = 100;
                        }
                    }
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelBar").css({width: 0});
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelValue").text("");

                    if (result_data) {

                        signal_level = parseInt(value * (310 / 168));
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelBar").css({width: parseInt((signal_level / 100) * 100)});
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelValue").text(additional_info[3] + "dm SNR " + additional_info[4] + "dB");
                    }
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityBar").css({width: 0});
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityValue").text("");

                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData1").text(" Mhz");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData2").text(" Ks/sec");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData3").text(" Y");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData4").text("");

                    if (result_data) {

                        signal_quality = 100 - parseInt(additional_info[1] * 100 / 65535);

                        signal_quality = parseInt(signal_quality * (310 / 168));

                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityBar").css({width: parseInt((signal_level / 100) * 100)});
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityValue").text(getBERForm(additional_info[1]));

                        // step 3. 신호주파수 입력

                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData1").text(result_data.frequency + " Mhz");

                        // step 4. 심볼레이트 입력
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData2").text(result_data.symbolrate + " Ks/sec");

                        // step 5. 편파 설정
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData3").text((result_data.polarization === "0" ? "수평 -18":"수직 -13") + " Y");

                        // step 6. FEC
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData4").text(getFECInner(trans_ponder_list[transponderIdx].fecInner));
                    }

                    focusIdx = selectedElementIdx % NUMBER_OF_PREVIEW_LIST;
                    buttonFocusRefresh(focusIdx, 1);
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW:
                    scroll.hide();
                    isCustom = true;
                    // 중계기 상세 보기 -> timer 로 계속 업데이트 된다.
                    var result = "";
                    var value;
                    var signal_level;
                    var signal_quality;

                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_text").html("중계기 정보 및 신호 상태를<br>확인할 수 있습니다");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #table_subTitle").removeClass("hide");

                    INSTANCE.div.find("#backgroundTitle").text(selectedRepeaterName);
                    INSTANCE.div.find("#contents #settingTable #table_title").text(selectedRepeaterName);

                    // step 1. 수신 상태 체크
                    if (result_data) {
                        if (additional_info[3] > -120) {
                            result = "수신상태가 양호합니다";
                        } else {
                            result = "수신상태가 미약합니다";
                        }
                    }
                    if(result) {
                        INSTANCE.div.find("#contents #settingTable #table_subTitle").text("[" + selectedRepeaterName + "] " + result);
                    }else {
                        INSTANCE.div.find("#contents #settingTable #table_subTitle").text("");
                    }


                    // step 2. 신호 레벨 & 품질 업데이트

                    if (ts_info) {
                        value = 120 + additional_info[3];

                        if (value < 0) {
                            value = 0;
                        } else if (value > 100) {
                            value = 100;
                        }
                    }

                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelBar").css({width: 0});
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelValue").text("");

                    if (result_data) {

                        signal_level = parseInt(value * (310 / 168));
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelBar").css({width: parseInt((signal_level / 100) * 100)});
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_levelArea #repeatInfo_levelValue").text(additional_info[3] + "dm SNR " + additional_info[4] + "dB");
                    }

                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityBar").css({width: 0});
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityValue").text("");

                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData1").text(" Mhz");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData2").text(" Ks/sec");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData3").text(" Y");
                    INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData4").text("");

                    if (result_data) {

                        signal_quality = 100 - parseInt(additional_info[1] * 100 / 65535);

                        signal_quality = parseInt(signal_quality * (310 / 168));

                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityBar").css({width: parseInt((signal_level / 100) * 100)});
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_qualityArea #repeatInfo_qualityValue").text(getBERForm(additional_info[1]));

                        // step 3. 신호주파수 입력

                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData1").text(result_data.frequency + " Mhz");

                        // step 4. 심볼레이트 입력
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData2").text(result_data.symbolrate + " Ks/sec");

                        // step 5. 편파 설정
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData3").text((result_data.polarization === "0" ? "수평 -18":"수직 -13") + " Y");

                        // step 6. FEC
                        INSTANCE.div.find("#contents #settingTable #repeatInfo_area #repeatInfo_detailArea #repeatInfo_detailData4").text(getFECInner(trans_ponder_list[transponderIdx].fecInner));
                    }

                    focusIdx = selectedElementIdx % NUMBER_OF_PREVIEW_LIST;
                    buttonFocusRefresh(0, 1);
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW:

                    scroll.hide();
                    isCustom = false;
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_text").html("현재 설정된 설정값을<br>저장합니다");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #repeatSetting_area").removeClass("hide");
                    INSTANCE.div.find("#contents #settingTable #table_subTitle").removeClass("hide");
                    INSTANCE.div.find("#backgroundTitle").text(selectedRepeaterName);
                    INSTANCE.div.find("#contents #settingTable #table_title").text(selectedRepeaterName);
                    INSTANCE.div.find("#contents #settingTable #table_subTitle").text("주파수를 수동 조정할 수 있습니다");

                    setEditData();
                    buttonFocusRefresh(focusIdx, 0);
                    break;
            }
        }

        function pressEnterKeyEvent(index, menuIndex) {
            switch (state) {
                case KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW:
                    switch (menuIndex) {
                        case 0:
                            selectTransponderButton(index);
                            //selectedElementIdx = focusIdx;
                            selectedElementIdx = transponderIdx;
                            focusIdx = 0;
                            buttonFocusRefresh(focusIdx, 1);
                            trans_info = trans_ponder_list[transponderIdx];
                            break;
                        case 1: // rightMenu
                            if (isCustom) { // 사용자 정의 메뉴
                                if (focusIdx == 0) {
                                    showEdit();
                                } else if (focusIdx == 1) {
                                    showDetail();
                                } else {
                                    clearTimeout(collect_timer);
                                    collect_timer = null;
                                    LayerManager.historyBack();
                                }
                            } else { // 일반 메뉴
                                if (focusIdx == 0) {
                                    // 상세 화면으로 들어간다
                                    showDetail();
                                } else {
                                    clearTimeout(collect_timer);
                                    collect_timer = null;
                                    LayerManager.historyBack();
                                }
                            }
                            break;
                    }
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW:

                    clearTimeout(collect_timer);
                    collect_timer = null;
                    changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW);
                    //networkManager.enableWeakSignal(true);
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW:
                    if (focusIdx == 0) {
                        showEdit();
                        //changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW);
                    } else {
                        changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW);
                        //networkManager.enableWeakSignal(true);
                    }
                    clearTimeout(collect_timer);
                    collect_timer = null;
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW:
                    switch (menuIndex) {
                        case 0:
                            //focusIdx = selectedElementIdx;
                            buttonFocusRefresh(0, 1);
                            break;
                        case 1:
                            if (focusIdx == 0) {
                                //저장
                                is_saving = true;
                                startSaving();
                                focusIdx = selectedElementIdx % NUMBER_OF_PREVIEW_LIST;
                                clearTimeout(collect_timer);
                                collect_timer = null;
                            } else {
                                focusIdx = selectedElementIdx % NUMBER_OF_PREVIEW_LIST;
                                changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW);
                                clearTimeout(collect_timer);
                                collect_timer = null;
                            }
                            break;
                    }
                    break;
            }
        }

        function buttonFocusRefresh(index, menuIndex) {
            switch (state) {
                case KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW:
                    focusIdx = index;
                    menuIdx = menuIndex;
                    removeFocusHighlight();
                    switch (menuIndex) {
                        case 0:
                            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ")").addClass("focus");
                            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").attr("src", "images/rdo_btn_f.png");
                            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_f.png");
                            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").addClass("focus");
                        /*    if (INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").text().indexOf("사용자", 0) > -1 ||
                                INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_text").text().indexOf("User", 0) > -1) {*/
                            if (trans_ponder_list[selectedElementIdx].name.indexOf("사용자", 0) > -1 ||
                                trans_ponder_list[selectedElementIdx].name.indexOf("User", 0) > -1) {
                                log.printDbg("correct");
                                isCustom = true;
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0").addClass("hide");
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1").removeClass("hide");
                            } else {
                                log.printDbg("Wrong");
                                isCustom = false;
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1").addClass("hide");
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0").removeClass("hide");
                            }

                            setColorKeyArea(true);
                            break;
                        case 1:
                            if (trans_ponder_list[selectedElementIdx].name.indexOf("사용자", 0) > -1 ||
                                trans_ponder_list[selectedElementIdx].name.indexOf("User", 0) > -1) {
                                log.printDbg("correct");
                                isCustom = true;
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0").addClass("hide");
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1").removeClass("hide");
                            } else {
                                log.printDbg("Wrong");
                                isCustom = false;
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1").addClass("hide");
                                INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0").removeClass("hide");
                            }
                            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area" + (isCustom? 1 : 0) + " .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area" + (isCustom? 1 : 0) + " .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area" + (isCustom? 1 : 0) + " .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");

                            setColorKeyArea(false);
                            break;
                    }
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW:
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area2 .rightMenu_btn_div:eq(0)").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area2 .rightMenu_btn_div:eq(0) .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area2 .rightMenu_btn_div:eq(0) .rightMenu_btn_line").addClass("focus");
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW:
                    focusIdx = index;
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3 .rightMenu_btn_div").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3 .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3 .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3 .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3 .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area3 .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                    break;
                case KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW:
                    focusIdx = index;
                    menuIdx = menuIndex;
                    INSTANCE.div.find("#contents #repeatSetting_area .repeatSetting_div .repeatSetting_btnDiv").removeClass("focus");
                    INSTANCE.div.find("#contents #repeatSetting_area .repeatSetting_div .repeatSetting_btnValue").removeClass("focus");
                    INSTANCE.div.find("#contents #repeatSetting_area #repeatSetting_focusArrow_area").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4 .rightMenu_btn_div").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4 .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
                    INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4 .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
                    switch (menuIndex) {
                        case 0:
                            INSTANCE.div.find("#contents #repeatSetting_area .repeatSetting_div:eq(" + index + ") .repeatSetting_btnDiv").addClass("focus");
                            INSTANCE.div.find("#contents #repeatSetting_area .repeatSetting_div:eq(" + index + ") .repeatSetting_btnValue").addClass("focus");
                            INSTANCE.div.find("#contents #repeatSetting_area #repeatSetting_focusArrow_area").addClass("focus");
                            INSTANCE.div.find("#contents #repeatSetting_area #repeatSetting_focusArrow_area").css("top", (index * 91) + "px");
                            break;
                        case 1:
                            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4 .rightMenu_btn_div:eq(" + index + ")").addClass("focus");
                            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4 .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_text").addClass("focus");
                            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area4 .rightMenu_btn_div:eq(" + index + ") .rightMenu_btn_line").addClass("focus");
                            break;
                    }
                    break;
            }
        }

        function createComponent() {
            var tmpHtml = "";
            INSTANCE.div.find("#contents").html("");
            tmpHtml = "<div id='settingTable'>" +
            "<div class='table_item_div'>" +
            "<div id='table_title'>중계기</div>" +
            "<div id='table_subTitle' class='hide'>[] 수신상태가 양호합니다</div>";
            tmpHtml += "<div id='table_radio_area' class='hide'>" +
            "</div>";
            tmpHtml += "<div id='repeatInfo_area' class='hide'>" +
            "<div id='repeatInfo_levelArea'>" +
            "<div id='repeatInfo_levelTitle'>신호 레벨</div>" +
            "<div id='repeatInfo_levelBarArea' style = 'position: relative;'>" +
            "<span class='repeatInfo_barValueText'>0</span>" +
            "<div class='repeatInfo_barGray'></div>" +
            "<div id='repeatInfo_levelBar'></div>" +
            "<span class='repeatInfo_barValueText'>100</span>" +
            "<div id='repeatInfo_levelValue'>-54dBm SNR 13dB</div>" +
            "</div>" +
            "</div>" +
            "<div id='repeatInfo_qualityArea'>" +
            "<div id='repeatInfo_qualityTitle'>신호 품질</div>" +
            "<div id='repeatInfo_qualityBarArea'style = 'position: relative;'>" +
            "<span class='repeatInfo_barValueText'>0</span>" +
            "<div class='repeatInfo_barGray'></div>" +
            "<div id='repeatInfo_qualityBar'></div>" +
            "<span class='repeatInfo_barValueText'>100</span>" +
            "<div id='repeatInfo_qualityValue'>0.7E-1</div>" +
            "</div>" +
            "</div>" +
            "<div id='repeatInfo_detailArea'>" +
            "<div style='width: 410px; height: 55px;'>" +
            "<span class='repeatInfo_detailText'>신호주파수 입력</span>" +
            "<span id='repeatInfo_detailData1' class='repeatInfo_detailText' style='left: 194px;'>11785 Mhz</span>" +
            "</div>" +
            "<div style='width: 410px; height: 55px;'>" +
            "<span class='repeatInfo_detailText'>심볼 레이트</span></td>" +
            "<span id='repeatInfo_detailData2' class='repeatInfo_detailText'style='left: 194px;'>21300 ks/sec</span>" +
            "</div>" +
            "<div style='width: 410px; height: 55px;'>" +
            "<span class='repeatInfo_detailText'>편파설정</span>" +
            "<span id='repeatInfo_detailData3' class='repeatInfo_detailText' style='left: 194px;'>수평-18V11785</span>" +
            "</div>" +
            "<div style='width: 410px; height: 55px;'>" +
            "<span class='repeatInfo_detailText'>FEC</span>" +
            "<span id='repeatInfo_detailData4' class='repeatInfo_detailText' style='left: 194px;'>7/8</span>" +
            "</div>" +
            "</div>" +
            "</div>";
            tmpHtml += "<div id='repeatSetting_area'>" +
            "<div class='repeatSetting_div'>" +
            "<span class='repeatSetting_btnTitle'>전송 규격</span>" +
            "<div class='repeatSetting_btnDiv'></div>" +
            "<div class='repeatSetting_btnValue'>DVB-S</div>" +
            "</div>" +
            "<div class='repeatSetting_div'>" +
            "<span class='repeatSetting_btnTitle'>신호주파수 입력</span>" +
            "<div class='repeatSetting_btnDiv'></div>" +
            "<div class='repeatSetting_btnValue'>11797</div>" +
            "<span class='repeatSetting_subTitle'>Mhz</span>" +
            "</div>" +
            "<div class='repeatSetting_div'>" +
            "<span class='repeatSetting_btnTitle'>편파설정</span>" +
            "<div class='repeatSetting_btnDiv'></div>" +
            "<div class='repeatSetting_btnValue'>수평-18V</div>" +
            "</div>" +
            "<div class='repeatSetting_div'>" +
            "<span class='repeatSetting_btnTitle'>심볼 레이트</span>" +
            "<div class='repeatSetting_btnDiv'></div>" +
            "<div class='repeatSetting_btnValue'>21300</div>" +
            "<span class='repeatSetting_subTitle'>Ks/sec</span>" +
            "</div>" +
            "<div class='repeatSetting_div'>" +
            "<span class='repeatSetting_btnTitle'>FEC</span>" +
            "<div class='repeatSetting_btnDiv'></div>" +
            "<div class='repeatSetting_btnValue'>1/2</div>" +
            "</div>" +
            "<div class='repeatSetting_div'>" +
            "<span class='repeatSetting_btnTitle'>Roll-Off</span>" +
            "<div class='repeatSetting_btnDiv'></div>" +
            "<div class='repeatSetting_btnValue'>None</div>" +
            "</div>" +
            "<div id='repeatSetting_focusArrow_area'>" +
            "<img id='repeatSetting_focusArrow_left' src='images/popup/arw_option_select_l_f.png'>" +
            "<img id='repeatSetting_focusArrow_right' src='images/popup/arw_option_select_r_f.png'>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>";
            INSTANCE.div.find("#contents").append(tmpHtml);
            INSTANCE.div.find("#contents").append("<div id='rightMenu_area'>" +
            "<div id='rightMenu_bg'>" +
            "<img id='menuTop_bg' src='images/set_bg_btn_t.png'>" +
            "<img id='menuCenter_bg' src='images/set_bg_btn.png'>" +
            "<img id='menuBottom_bg' src='images/set_bg_btn_b.png'>" +
            "</div>" +
            "<div id='rightMenu_text'>중계기를 선택하거나<br>설정을 변경할 수 있습니다</div>" +
            "<div id='rightMenu_btn_area0'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>보기</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "<div id='rightMenu_btn_area1' class='hide'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>설정</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>보기</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "<div id='rightMenu_btn_area2' class='hide'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>확인</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "<div id='rightMenu_btn_area3' class='hide'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>변경</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "<div id='rightMenu_btn_area4' class='hide'>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>저장</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "<div class='rightMenu_btn_div'>" +
            "<div class='rightMenu_btn_text'>취소</div>" +
            "<div class='rightMenu_btn_line'></div>" +
            "</div>" +
            "</div>" +
            "</div>");

            createTransporterElements();
        }



        function createTransporterElements() {
            log.printDbg("makeTransporterElements()");

            for(var i = 0; i < NUMBER_OF_PREVIEW_LIST; i++) {

                item[i] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "table_radio_btn"
                    },
                    parent: INSTANCE.div.find("#table_radio_area")
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/rdo_btn_d.png",
                        class: "table_radio_img"
                    },
                    parent: item[i]
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "table_radio_text"
                    },
                    text: "중계기 이름",
                    parent: item[i]
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "table_radio_btn_line",
                        css: {
                            position: "absolute", float: "left", height: 77, width: 285
                        }
                    },
                    parent: item[i]
                });

            }


            //scrollArea
            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "scroll",
                    css: {
                        position: "absolute", left: 1369, top: 161, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 723,
                forceHeight: true
            });

            colorKeyArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "colorKey",
                    css: {
                        position: "absolute", left: 0, top: 0,width: 1920, height: 1080, visibility: "hidden"
                    }
                },
                parent: INSTANCE.div
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: 1343, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "redKey",
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: 1371, top: 904, width: 29, height: 32
                    }
                },
                parent: colorKeyArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "keyText",
                    css: {
                        position: "absolute", left: 1343, top: 939, width: 60, height: 25, "letter-spacing" : -1.1,
                        "font-size" : 22, color: "rgba(255,255,255,0.77)", "font-family" : "RixHead L"
                    }
                },
                text: "페이지",
                parent: colorKeyArea
            });

        }
        function setColorKeyArea(b) {
            log.printDbg("setColorKeyArea");
            if(b) {
                colorKeyArea.css({visibility: "inherit"});
            }else {
                colorKeyArea.css({visibility: "hidden"});
            }
        }

        // 2017-04-28 [sw.nam] 설정 clock 추가
        function createClock() {
            log.printDbg("createClock()");

            clockArea = KTW.utils.util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "clock_area",
                    css: {
                        visibility: "hidden", overflow: "visible"
                    }
                },
                parent: INSTANCE.div
            });
            clock = new KTW.ui.component.Clock({
                parentDiv: clockArea
            });
        }

        /**
         * 현재 보여지는 페이지의 중계기 리스트를 그려준다
         * @param idx 업데이트의 기준이 될 인덱스 로부터 시작 인덱스(startIdx) 를 구하여 화면을 그려준다.
         */
        function updateListArea(idx) {
            log.printDbg("updataInfoArea()");
            var startIdx = idx - (idx % NUMBER_OF_PREVIEW_LIST);
            for(var listIdx = startIdx, elementIdx=0 ; listIdx < startIdx + NUMBER_OF_PREVIEW_LIST, elementIdx < NUMBER_OF_PREVIEW_LIST ; listIdx++, elementIdx++) {

                item[elementIdx].find(".table_radio_text").text("");

                if( listIdx > trans_ponder_list.length -1) {

                    item[elementIdx].css({visibility: "hidden"});
                } else {

                    item[elementIdx].css({visibility: "inherit"});
                    item[elementIdx].find(".table_radio_text").text(trans_ponder_list[listIdx].name);
                    if(selectedRepeaterName == trans_ponder_list[listIdx].name) {

                        selectTransponderButton(elementIdx);
                    }
                }
            }
        }

        function getMaxPage() {
            return Math.ceil(trans_ponder_list.length / NUMBER_OF_PREVIEW_LIST);
        };
        function getCurrentPage() {
            return Math.floor(transponderIdx / NUMBER_OF_PREVIEW_LIST);
        };
        function setCurrentPage() {
            updateListArea(transponderIdx);
            currentSetPage = getCurrentPage();
            scroll.setCurrentPage(currentSetPage);
        }

        function tuneTS() {
            hwAdapter.tune(trans_ponder_list[transponderIdx]);
        }

        function startSaving() {
            log.printDbg("startSaving");

            // set modSystem
            trans_info.modulationSystem = "" + convModSystem(select_modul);
            log.printDbg("modulationSystem " + convModSystem(select_modul));

            // set modulation
            trans_info.modulation = "" + convModulation(select_modul);
            log.printDbg("modulation " + convModulation(select_modul));

            // set frequency
            trans_info.frequency = "" + frequency;
            log.printDbg("frequency " + "" + frequency);

            // set symbolrate
            trans_info.symbolrate = "" + symbol_rate;
            log.printDbg("symbolrate " +  "" + symbol_rate);

            // set polarization
            trans_info.polarization = "" + BIASED_WAVE[select_liner][0];
            log.printDbg("polarization " + BIASED_WAVE[select_liner][0]);

            // set FEC
            trans_info.fecInner = "" + FEC_SET[select_fec][0];
            log.printDbg("fecInner " + FEC_SET[select_fec][0]);

            // set RollOff
            trans_info.rollOff = "" + ROLL_OFF_SET[select_roll_off][0];
            log.printDbg("rollOff " + ROLL_OFF_SET[select_roll_off][0]);

            INSTANCE.div.find("#contents #settingTable .table_item_div #table_subTitle").text(MESSAGES[1]);

            setTimeout(function() {
                //container.stopAV();

                if (stateManager.isTVViewingState() === true) {
                    // [jh.lee] TV 인 경우 시청중인 채널 종료
                    navAdapter.stopChannel();
                    stop_flag = true;
                } else if (stateManager.isVODPlayingState() === true) {
                    // [jh.lee] VOD 상태 인 경우 시청중인 VOD 종료
                    // R6. ACAP 확인 결과 저장 수행 시에는 VOD 재생 중이던 것을 STOP하지 않음.
                    // 그래서 동일하게 STOP 하지 않도록 수정.
//                vodAdapter.stopPlayer(OTW.vod.Def.EXIT_TYPE.EXIT_FORCE);
//                stop_flag = true;
//                vod_stop_flag = true;
                }

                hwAdapter.setTSInfo(transponderIdx, trans_info, true);
            }, 100);

            setTimeout(searchTS, 2000);
        }

        function convModSystem(mod) {
            // [jh.lee] MOD_DVB_S 는 형식이 문자열, mod 는 숫자. 비교위해서 Number 사용
            if (mod === Number(MOD_DVB_S)) {
                return MOD_SYSTEM_DVB_S;
            } else {
                return MOD_SYSTEM_DVB_S2;
            }
        }
        function convModulation(mod) {
            if (mod === MOD_DVB_S2_8PSK) {
                return MOD_8PSK;
            }
            else {
                return MOD_QPSK;
            }
        }

        function convMod(modulation, mod_system) {
            var mod = MOD_DVB_S;

            if (mod_system === MOD_SYSTEM_DVB_S2) {
                if (modulation === MOD_QPSK) {
                    mod = MOD_DVB_S2_QPSK;
                } else if (modulation === MOD_8PSK) {
                    mod = MOD_DVB_S2_8PSK;
                }
            }
            return mod;
        }
        function searchTS() {
            var collect_list;
            var list_length;
            var exist = false;

            collect_list = hwAdapter.getCollectibleList();
            if (collect_list && collect_list.length) {
                list_length = collect_list.length;
            } else {
                list_length = 0;
            }

            for(var i = 0; i < list_length; i++) {
                log.printDbg(trans_info.modulation + "|" + collect_list[i].modulation);
                log.printDbg(trans_info.modulationSystem + "|" + collect_list[i].modulationSystem);
                log.printDbg(trans_info.polarization + "|" + collect_list[i].polarization);
                log.printDbg(trans_info.symbolrate + "|" + collect_list[i].symbolrate);
                log.printDbg(trans_info.frequency + "|" + collect_list[i].frequency);
                log.printDbg("=======================================================");

                if (trans_info.modulation === collect_list[i].modulation &&
                    trans_info.modulationSystem === collect_list[i].modulationSystem &&
                    trans_info.polarization === collect_list[i].polarization &&
                    trans_info.symbolrate === collect_list[i].symbolrate &&
                    trans_info.frequency === collect_list[i].frequency) {

                    if (trans_info.rollOff === "3" || trans_info.rollOff === collect_list[i].rollOff) {
                        exist = true;
                    }
                }
            }

            if (exist) {

                KTW.managers.service.SimpleMessageManager.showMessageTextOnly("지정하신 중계기 탐색이 끝났습니다");
            } else {
                KTW.managers.service.SimpleMessageManager.showMessageTextOnly("지정하신 중계기 탐색에 실패했습니다");

            }
            INSTANCE.div.find("#contents #settingTable .table_item_div #table_subTitle").text(MESSAGES[0]);
            trans_ponder_list = hwAdapter.getTSInfos();
            is_saving = false;

            setTimeout(function() {
                changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.LIST_VIEW);
            }, 1000);

        }


        function showDetail() {
            log.printDbg("showDetail()");
            view_detail = true;
            view_edit = false;
            result_data = undefined;
            ts_info = undefined;

            //createDetail // 현재 값 보여주는곳

            if(isCustom) {
                changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW);
            } else {
                changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW);
            }

            submit_index = 0;

            if (stateManager.isTVViewingState() === true) {
                // [jh.lee] TV 인 경우 시청중인 채널 종료
                navAdapter.stopChannel();
                stop_flag = true;
            } else if (stateManager.isVODPlayingState() === true) {
                // [jh.lee] VOD 상태 인 경우 시청중인 VOD 종료
                // 2017.04.13 dhlee
                // VOD 시청중일 때 VOD Module을 찾아서 stopVODPlaying API를 호출하도록 해야 한다.
                //vodAdapter.stopPlayer(KTW.vod.Def.EXIT_TYPE.EXIT_FORCE);
                var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                if (vodModule) {
                    // 2017.04.13 dhlee result는 향후를 위해 일단 전달만 받아둔다.
                    var result = vodModule.execute({
                        method: "stopVodWithEndPopup",
                        params: {
                            withPopup: false,
                            notChangeService: true,
                            channelObj: null
                        }
                    });
                }
                stop_flag = true;
                vod_stop_flag = true;
            }

            transponderIdx = selectedElementIdx;
            tuneTS();
            collect_timer = setTimeout(collectInfo, 2000);

            // [jh.lee] 신호 품질 측정시 위성신호 미약을 감지하여 팝업이 생성되는 것을 차단
            networkManager.enableWeakSignal(false);
        }
        function collectInfo() {

            result_data = trans_ponder_list[transponderIdx]; // ts 수집 결과값을 넣어줌

            ts_info = hwAdapter.collectOneTransponder(result_data.name, result_data.transportStreamId);
            //[11, 0, 0, -51, 15]
            additional_info = hwAdapter.getAdditionalTunerInfos();

            if(isCustom) {
                changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.USER_DETAIL_VIEW);
            } else {
                changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.DETAIL_VIEW);
            }

            //setFocus();

            clearTimeout(collect_timer); // TODO 상세보기 아닐떄는 timer 를 반드시 멈춰줘야함
            //보여주고 나서 3초단위로 계속 수집..신호가 변경되므로...
            collect_timer = setTimeout(collectInfo, 5000);
        }

        function showEdit() { // 설정
            log.printDbg("showEdit()");
            // releaseLock(); //TODO 2.0 코드 들고옴. 로직 분석 중

            view_edit = true;
            view_detail = false;

            select_liner = 0;
            select_fec = 0;
            select_roll_off = 0;
            select_modul = 0;

            setFECAndRoleOffData(parseInt(trans_info.modulation));
            setFECAndRollOffFocus(trans_info.fecInner, trans_info.rollOff);
            setWave(trans_info.polarization);
            symbol_rate = trans_info.symbolrate;
            frequency = trans_info.frequency;

            selected_edit_index = 0;
            networkManager.enableWeakSignal(false);
            changeViewState(KTW.ui.layer.setting.setting_repeater.STATE.SETTING_VIEW);
        }

        function setFECAndRoleOffData(mode) {
            log.printDbg("mode : " + mode);
            switch (parseInt(mode)) {
                case 1 :
                    select_modul = 1;
                    FEC_SET = FEC_DVB_S2_QPSK;
                    ROLL_OFF_SET = ROLLOFF;
                    break;
                case 2 :
                    select_modul = 2;
                    FEC_SET = FEC_DVB_S2_8PSK;
                    ROLL_OFF_SET = ROLLOFF;
                    break;
                default://case MOD_DVB_S :
                    select_modul = 0;
                    select_roll_off = 0;
                    FEC_SET = FEC_DVB_S;
                    ROLL_OFF_SET = ROLLOFF_NONE;
                    break;
            }
        }

        function setFECAndRollOffFocus(fec, roll_off) {
            log.printDbg("fec : " + fec);
            log.printDbg("rollOff : " + roll_off);

            for (var i = 0; i < FEC_SET.length; i++) {
                if (FEC_SET[i][0] == fec) {
                    select_fec = i;
                }
            }

            for (var i = 0; i < ROLL_OFF_SET.length; i++) {
                if (ROLL_OFF_SET[i][0] == roll_off) {
                    select_roll_off = i;
                }
            }
        }

        function setWave(polar) {
            for (var i = 0; i < BIASED_WAVE.length; i++) {
                if (BIASED_WAVE[i][0] == polar) {
                    select_liner = i;
                }
            }
        }

        function getFECInner(value) {
            log.printDbg("fecInnner : "  + value);
            switch (parseInt(value)) {
                case 40:
                    return "1/2";
                case 41:
                    return "2/3";
                case 42:
                    return "3/4";
                case 43:
                    return "5/6";
                case 44:
                    return "7/8";
                case 50:
                    return "4/5";
                case 51:
                    return "3/5";
                case 52:
                    return "8/9";
                case 53:
                    return "9/10";
                default:
                    return "7/8";
            }
        }


        function getBERForm(ber) {
            var count;
            var re;
            var ret;

            if (ber === 0) {
                return "0.0E-7";
            }
            count = 0;
            re = ber / 65535;

            while (re * 10 < 1) {
                count++;
                re *= 10;
            }
            if (re * 100 % 10 > 5) {
                re = re + 0.1;
            }
            ret = ("" + re).substring(0, 3);

            return ret + "E-" + count;
        }


        function unselectTransponderButton() {
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            //selectedElementIdx = focusIdx;
        }
        function selectTransponderButton(index) {
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").removeClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img").addClass("select");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn:eq(" + index + ") .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            selectedRepeaterName = item[index].find(".table_radio_text").text();
            log.printDbg("selectedRepeaterName+++______"+selectedRepeaterName);
        }

        function removeFocusHighlight() {
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn").removeClass("focus");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img").attr("src", "images/rdo_btn_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_img.select").attr("src", "images/rdo_btn_select_d.png");
            INSTANCE.div.find("#contents #settingTable #table_radio_area .table_radio_btn .table_radio_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0 .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1 .rightMenu_btn_div").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0 .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1 .rightMenu_btn_div .rightMenu_btn_text").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area0 .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
            INSTANCE.div.find("#contents #rightMenu_area #rightMenu_btn_area1 .rightMenu_btn_div .rightMenu_btn_line").removeClass("focus");
        }

        function setScrollFocus(b) {
            log.printDbg("setFocus()");

            if (b) {
                scroll.setFocus(true);
                isScrollArea = true;
            }
            else {
                scroll.setFocus(false);
                isScrollArea = false;
            }
        }
    };

    KTW.ui.layer.setting.setting_repeater.prototype = new KTW.ui.Layer();
    KTW.ui.layer.setting.setting_repeater.constructor = KTW.ui.layer.setting.setting_repeater;

    KTW.ui.layer.setting.setting_repeater.prototype.create = function (cbCreate) {
        KTW.ui.Layer.prototype.create.call(this);

        this.init(cbCreate);
    };



    KTW.ui.layer.setting.setting_repeater.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };

    Object.defineProperty(KTW.ui.layer.setting.setting_repeater, "STATE", {
        value: {LIST_VIEW: 0, DETAIL_VIEW: 1, USER_DETAIL_VIEW: 2, SETTING_VIEW: 3},
        writable: false,
        configurable: false
    });

})();