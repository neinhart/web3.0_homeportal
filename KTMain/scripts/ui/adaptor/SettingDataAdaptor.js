/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SettingDataAdaptor</code>
 *
 * Setting data adaptor for Setting_main and other submenu layers in components(SubMenu_System, SubMenu_Kids, SubMenu_ChannelVod)
 *
 * Getting Action duplicated between at least 2 layers. These are Setting main layer and SubMenu layer.
 *
 * If setting_main does not use APIs for setting(some specific layers don't have to show information on main layer), It's doesn't matter whether this adapter is being used or not.
 *
 * @author sw.nam
 * @since 2017-03-02
 */

KTW.ui.adaptor.SettingDataAdaptor =(function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var storageManager = KTW.managers.StorageManager;
    var hwAdapter = KTW.oipf.AdapterHandler.hwAdapter;
    var wsSmartPushManager =  KTW.managers.http.wsSmartPushManager;
    var basicAdapter = KTW.oipf.AdapterHandler.basicAdapter;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var menuDataManager = KTW.managers.data.MenuDataManager;
    var DEF = KTW.oipf.Def;

    var LayerManager = KTW.ui.LayerManager;
    var Layer = KTW.ui.Layer;

    var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;
    var androidAdapter = KTW.oipf.adapters.AndroidAdapter;
    // 각 메뉴데이터에 의해 오브젝트가 담겨질 array
    var result = [];

    /*
     *  synchronized  configuration data.
     *  각 서브메뉴 레이어에서 필요한 데이터를 받아와 result 로 리턴.
     *  각 메뉴마다 필요로하는 데이터가 다르므로, 메뉴별 배열 형태에 주의해야 함.
     *  필요할 떄마다 추가적으로 result 에 push 해주어야 함.
     *
     *  parameters
     * menuId : 메인 혹은 특정 설정메뉴 고유 ID.
     *  fromWhat : 함수 호출 주체  ( main or specific layer.)
     *
     *  ## 설정 메인 메뉴와 특정 메뉴에서 필요로 하는 object 가 동일한 경우, flag를 사용하지 않아도 무방하다.
     *  ## 설정 메인 메뉴 미리보기에서 설정값을 보여 줄 필요가 없으면, 어뎁터를 거치지 않고 상세 메뉴 레이어 화면에서 직점 개발해도 무방하다.
     *
     * */
    function _getConfigurationInfoByMenuId(menuId,fromWhat) {

        result = [];

        switch(menuId) {

            // 자녀안심설정
            case menuDataManager.MENU_ID.SETTING_CHANGE_PIN:
                result.push();
                break;
            case menuDataManager.MENU_ID.SETTING_PARENTAL_RATING:
                var parentalRating = KTW.oipf.AdapterHandler.casAdapter.getParentalRating();
                if(parentalRating !== undefined && parentalRating !== null) {
                    result.push(parentalRating);
                }else {
                    parentalRating = 0;
                    result.push(parentalRating);
                }

                var tempData = basicAdapter.getConfigText(DEF.CONFIG.KEY.ADULT_MENU_DISPLAY);
                if(tempData === undefined || tempData === null) {
                    tempData = "true";
                    basicAdapter.setConfigText(DEF.KEY.ADULT_MENU_DISPLAY, tempData);
                }
                result.push(tempData);
                break;
            case menuDataManager.MENU_ID.SETTING_TIME_LIMIT:
                KTW.managers.service.AppServiceManager.clearTimeRestrictOptions();

                var block_time = [];
                block_time[0] = false;
                block_time[1] = false;
                block_time[2] = "0000";
                block_time[3] = "0000";
                var limit_time = basicAdapter.getConfigText(DEF.CONFIG.KEY.RESTRICTION_TIME);
                if (limit_time === undefined || limit_time === null) {
                    result.push(block_time);
                } else {
                    result.push(limit_time);
                }
                break;
            case menuDataManager.MENU_ID.SETTING_BLOCKED_CHANNEL:
                var iptvBlockList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.FAVOURITE_BLOCKED);
                if (iptvBlockList !== undefined && iptvBlockList !== null && iptvBlockList.length>0) {
                    result.push(iptvBlockList);
                } else {
                    result.push([]);
                }

                if (KTW.CONSTANT.IS_OTS) {
                    var skyBlockList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED);
                    if(skyBlockList !== undefined && skyBlockList !== null && skyBlockList.length>0) {
                        result.push(skyBlockList);
                    } else {
                        result.push([]);
                    }
                }
                break;

            // 채널 / VOD 설정
            case menuDataManager.MENU_ID.SETTING_FAVORITE_CHANNEL:
                var iptvFavoriteList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
                if(iptvFavoriteList !== undefined && iptvFavoriteList !== null && iptvFavoriteList.length>0) {
                    result.push(iptvFavoriteList);
                }else {
                    result.push([]);
                }
                if (KTW.CONSTANT.IS_OTS) {
                    var skyFavoriteList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE);
                    if(skyFavoriteList !== undefined && skyFavoriteList !== null && skyFavoriteList.length>0) {
                        result.push(skyFavoriteList);
                    }else {
                        result.push([]);
                    }
                }
                break;
            case menuDataManager.MENU_ID.SETTING_SKIPPED_CHANNEL: //OTV- 숨김채널 OTS - 건너뛰기 채널
                if(KTW.CONSTANT.IS_OTS) {
                    var iptvJumpingList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SKIPPED);
                    if(iptvJumpingList !== undefined && iptvJumpingList !== null && iptvJumpingList.length>0) {
                        result.push(iptvJumpingList);
                    }else {
                        result.push([]);
                    }

                    var skyJumpingList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SKIPPED);

                    if(skyJumpingList !== undefined && skyJumpingList !== null && skyJumpingList.length>0) {
                        result.push(skyJumpingList);
                    }else {
                        result.push([]);
                    }
                }else {
                    var iptvHideList = navAdapter.getChannelList(DEF.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SKIPPED);
                    result.push(iptvHideList);
                }
                break;
            case menuDataManager.MENU_ID.SETTING_CHANNEL_GUIDE:
                var displayTIme = storageManager.ps.load(storageManager.KEY.MINIGUIDE_DISPLAY_TIME);
                if (displayTIme === null) {
                    displayTIme = "3000";
                    storageManager.ps.save(storageManager.KEY.MINIGUIDE_DISPLAY_TIME, displayTIme);
                }
                result.push(displayTIme);
                break;
            case menuDataManager.MENU_ID.SETTING_DESCRIPTIVE_VIDEO_SERVICE:
                var on_off = basicAdapter.getConfigText(DEF.CONFIG.KEY.VISUAL_IMPAIRED);
                var language = basicAdapter.getConfigText(DEF.CONFIG.KEY.AUDIO_LANGUAGE);
                if (util.isValidVariable(on_off) === false) {
                    result.push("0");
                }else {
                    result.push(on_off);
                }

                if (util.isValidVariable(language) === false) {
                    result.push("kor");
                }else {
                    result.push(language);
                }
                break;
            case menuDataManager.MENU_ID.SETTING_CLOSED_CAPTION:
                var on_off = basicAdapter.getConfigText(DEF.CONFIG.KEY.SUBTITLE_ONOFF);
                var size = basicAdapter.getConfigText(DEF.CONFIG.KEY.SUBTITLE_SIZE);
                var language = basicAdapter.getConfigText(DEF.CONFIG.KEY.SUBTITLE_LANGUAGE);
                if (util.isValidVariable(on_off) === false) {
                    on_off = DEF.CLOSED_CAPTION.ON_OFF.OFF;
                }
                if (util.isValidVariable(size) === false) {
                    size = DEF.CLOSED_CAPTION.FONT_SIZE.MEDIUM;
                }
                if (util.isValidVariable(language) === false) {
                    language = DEF.CLOSED_CAPTION.LANGUAGE.BASIC;
                }
                result.push(on_off);
                result.push(size);
                result.push(language);
                break;
            case menuDataManager.MENU_ID.SETTING_SERIES_AUTO_PLAY:
                var value = storageManager.ps.load(storageManager.KEY.SERIES_AUTO_PLAY);
                if (value === undefined || value === null) {
                    value = "0";
                }
                result.push(value);
                break;
            // 시스템 설정
            case menuDataManager.MENU_ID.SETTING_ASPECT_RATIO :
                result.push(hwAdapter.getAVOutputTvAspectRatio());
                result.push(hwAdapter.getAVOutputVideoMode());
                break;
            case menuDataManager.MENU_ID.SETTING_RESOLUTION :
                result.push(hwAdapter.getAVOutputHDVideoFormat());
                break;
            case menuDataManager.MENU_ID.SETTING_AUTO_POWER :
                result.push(_getAutoPower());
                break;
            case menuDataManager.MENU_ID.SETTING_STANDBY_MODE :
                // 2017.04.10 dhlee 자동대기모드 기능은 네비게이터에서 제공함에 따라 Configuration 으로 변경
                result.push(basicAdapter.getConfigText(DEF.CONFIG.KEY.AUTO_STANDBY));
                //result.push(storageManager.ps.load(storageManager.KEY.AUTO_STANDBY));
                break;
            case menuDataManager.MENU_ID.SETTING_SOUND :
                result.push(hwAdapter.getAVOutputDigitalAudioMode());
                break;
            case menuDataManager.MENU_ID.SETTING_VOICE_GUIDE:
            case menuDataManager.MENU_ID.SETTING_VOICE_RECOG :
                //on/off
                result.push(basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE));
                var volumeModule = KTW.oipf.AdapterHandler.extensionAdapter.getEmbeddedSpeakerVolumeController();
                var volume = volumeModule.getVolume();
                var defaultVoiceMode = KTW.oipf.AdapterHandler.audioTtsAdapter.getDefaultVoiceMode();
                var triggerVoiceType = basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE_TRIGGER_VOICE_TYPE);

                log.printDbg("volueme = "+volume);
                // 가이드 볼륨
                if(volume || volume === 0) {
                    result.push(volumeModule.getVolume() * 5);
                }else {
                    // [sw.nam] getVolume 이 정상적인 값을 반환하지 않는 경우, default 값(3)으로 강제 설정하도록 예외처리 추가
                    volumeModule.setVolume(0.6);
                    result.push("3");
                }

                // 가이드 음성 or 호출어
                if (triggerVoiceType !== null && triggerVoiceType !== "undefined" && triggerVoiceType !== undefined && triggerVoiceType !== "null") {
                    // 호출어
                    result.push(triggerVoiceType);
                }else {
                    // 가이드 음성
                    result.push(defaultVoiceMode);
                }
                break;
            case menuDataManager.MENU_ID.SETTING_SPEAKER :
                result.push(basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VOICE_GUIDE));
                var volumeModule = KTW.oipf.AdapterHandler.extensionAdapter.getEmbeddedSpeakerVolumeController();
                var volume = volumeModule.getVolume();
                if(volume || volume === 0) {
                    result.push(volumeModule.getVolume() * 5);
                }else {
                    volumeModule.setVolume(1);
                    result.push("5");
                }
                break;
            case menuDataManager.MENU_ID.SETTING_GIGA_GINI_AUDIO_OUTPUT :
                result.push(androidAdapter.getAudioOutputDevice());
                break;
            case menuDataManager.MENU_ID.SETTING_BLUETOOTH :
                result.push(basicAdapter.getConfigText(DEF.CONFIG.KEY.BLE_RCU_SUPPORT));
                break;
            case menuDataManager.MENU_ID.SETTING_POSTER_SHAPE :
                result.push(storageManager.ps.load(storageManager.KEY.VOD_LIST_MODE));
                break;
            case menuDataManager.MENU_ID.SETTING_HDMI_CEC :
                result.push(basicAdapter.getConfigText(DEF.CONFIG.KEY.HDMI_CEC));
                result.push(basicAdapter.getConfigText(DEF.CONFIG.KEY.HDMI_CEC_FN));
                break;
            case menuDataManager.MENU_ID.LANGUAGE :
                var language;
                language = basicAdapter.getConfigText(DEF.CONFIG.KEY.LANGUAGE);
                if(util.isValidVariable(language) == false) {
                    language = "kor";
                }
                result.push(language);
                break;
            case menuDataManager.MENU_ID.SETTING_START_CHANNEL :
                var value;
                var cug_data;
                value = _getCurrentStartingChannel();
                cug_data = _getCugChannelData();
                if(value === null || value === undefined || value === "") {
                    value = "가이드 채널";
                }
                result.push(cug_data);
                result.push(value);
                log.printDbg("START_CHANNEL's Cug value is == : "+value);
                break;
            case menuDataManager.MENU_ID.SETTING_SYSTEM_INFO :
                // 시스템 정보
                break;
            case menuDataManager.MENU_ID.TRANSPONDER : // 중계기(OTS)
                break;
            case menuDataManager.MENU_ID.COLLECT_SIGNAL : // 수신품질 측정 (OTS)
                break;

            // OTS 관리자 모드 ID
            case menuDataManager.MENU_ID.SKY_LNB:
                break;
            case menuDataManager.MENU_ID.SKY_EMERGENCY:
                var value;
                try {
                    value = basicAdapter.getConfigText(DEF.CONFIG.KEY.BMAIL);
                } catch (error) {
                    log.printErr("getConfigText, error = " + error.message);
                }
                result.push(value);
                break;
            case menuDataManager.MENU_ID.SKY_CHANNEL_INFO:
                var value;
                try {
                    value = basicAdapter.getConfigText(DEF.CONFIG.KEY.CHANNEL_INFO_OUTPUT);
                } catch (error) {
                    log.printErr("getConfigText, error = " + error.message);
                }
                result.push(value);
                break;
            case menuDataManager.MENU_ID.SKY_CLOSED_CAPTION:
                var value;
                try {
                    value = basicAdapter.getConfigText(DEF.CONFIG.KEY.SUBTITLE_ONOFF);
                } catch (error) {
                    log.printErr("getConfigText, error = " + error.message);
                }
                result.push(value);
                break;
            case menuDataManager.MENU_ID.SETTING_BEACON:
                var sending_power;
                var sending_interval;
                try{
                    sending_power = basicAdapter.getConfigText(DEF.CONFIG.KEY.BEACON.POWER_LEVEL);
                    sending_interval = basicAdapter.getConfigText(DEF.CONFIG.KEY.BEACON.INTERVAL);
                    log.printDbg("sending_power : "+sending_power+" sending_interval : "+sending_interval);
                    if( !sending_power || sending_power === "undefined" || sending_power === "[RELEASE_MODE]" ) {
                        sending_power = 2;
                    }
                    if( !sending_interval || sending_interval === "undefined" || sending_interval === "[RELEASE_MODE]" ) {
                        sending_interval = "760";
                    }
                } catch (e) {
                    log.printDbg("Failed to get configuration");
                    log.printErr(e);
                    sending_power = 2;
                    sending_interval = "760";
                }
                result.push(sending_power);
                result.push(sending_interval);
                break;
            default:
                break;
        }
        return result;

    }

    function _getBMailInfo(callback, said) {
        var request_object = null;

        try {
            request_object = wsSmartPushManager.checkOptOut(callback, said);
        } catch (error) {
            log.printDbg(error.message);
        }
        return request_object;
    }

    /**
     * 자동 전원 key 값 받아와서 세팅하는 함수
     *  2018.01.24 sw.nam 요일 선택 기능이 추가되면서 array 길이가 늘어남 3 - > 7
     * @returns {number[]}
     * @private
     */
    function _getAutoPower() {
        var autoPowerTime = [3];
        var value;
        var tempArr = [];

        autoPowerTime[0] = false;
        autoPowerTime[1] = "0000";
        autoPowerTime[2] = "0000";

        try {
            value = storageManager.ps.load(storageManager.KEY.AUTO_POWER);

            if (value === undefined || value === null || value === "") {
                return autoPowerTime;
            }

            tempArr = value.split(";");
  /*          autoPowerTime[0] = tempArr[0];//(tempArr[0] === "true") ? true : false;
            autoPowerTime[1] = tempArr[1];
            autoPowerTime[2] = tempArr[2];*/
            for(var i =0; i < tempArr.length; i++) {
                autoPowerTime[i] = tempArr[i];
            }

            return autoPowerTime;
        } catch (error) {
            log.printErr("getAutoPower(), error = " + error.message);
            return autoPowerTime;
        }
    }

    function _getCugChannelData() {
        log.printDbg("_getStartingChannel()");

        var cug_data;

        // Load CUG Data
        try {
            cug_data = JSON.parse(storageManager.ps.load(storageManager.KEY.CUST_CUG_INFO));
        } catch(e) {
            cug_data = undefined;
        }
        log.printDbg("cugData readConfiguration : " + JSON.stringify(cug_data));

        return cug_data;

    }

    function _getCurrentStartingChannel() {
        log.printDbg("_getCurrentStartingChannel()");

        var value = null;
        value = basicAdapter.getConfigText(DEF.CONFIG.KEY.DEFAULT_CHANNEL);
        log.printDbg("_getCurrentStartingChannel's value is : "+value);
        return value;
    }

    //[sw.nam] bluetooth 장치 아이콘 converting g함수
    function _getConvertedType(type) {
        log.printDbg("_getConvertedType  = "+type);
        switch(type){
            case BluetoothDevice.TYPE_AD2P_SNK_UNKNOWN:
                type = "audio";
                break;
            case BluetoothDevice.TYPE_HEADPHONE:
                type = "headset";
                break;
            case BluetoothDevice.TYPE_KEYBOARD:
                type = "keyboard";
                break;
            case BluetoothDevice.TYPE_MOUSE:
                type = "mouse";
                break;
            default:
                type = "audio";
        }
        return type;
    }

    function _showInitConfirmPopup(title , callback) {
        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: [title],
                cssObj: {}
            }],
            arrButton: [{id: "save", name: "확인"},{id: "cancel", name: "취소"}],
            isShowSaid : false ,
            cbAction: function (buttonId) {
                var consumed = false;
                if(buttonId === "save" || buttonId === "cancel") {
                    KTW.ui.LayerManager.deactivateLayer({id:"channel_init_confirm_popup"});
                    if(callback) {
                        callback(buttonId);
                    }
                    consumed = true;
                }
                return consumed;
            }
        };


        popupData.arrMessage.push({
            type: "MSG_52",
            message: ["초기화하시겠습니까?"],
            cssObj: {}
        });

        popupData.arrMessage.push({
            type: "MSG_38",
            message: ["확인 버튼을 선택하면" , "채널 정보가 초기화됩니다"],
            cssObj: {}
        });


        var localParams = {data : popupData};

        var layerOptions = {
            obj: {
                id: "channel_init_confirm_popup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: localParams
            },
            visible: true,
            cbActivate: function () {}
        };

        KTW.ui.LayerManager.activateLayer(layerOptions);

    }

    /**
     * 메시업으로 자동 번역 언어 리스트 받아오는 메시지를 전송한다. (비즈상품)
     * @private
     */
    function _requestMashup_getCCLanguageList(options) {
        log.printDbg("_requestMashup_getCCLanguageList");

        if(KTW.CONSTANT.IS_BIZ === true) {
            var mashup_caption_msg = {
                from: KTW.CONSTANT.APP_ID.HOME,
                to: KTW.CONSTANT.APP_ID.MASHUP,
                method: "mashup_getCCLanguageList"
            };

            var isTimeout = false;
            KTW.ui.LayerManager.startLoading({
                preventKey: true,
                timeout: 3 * 1000,
                cbTimeout: function () {
                    if (!isTimeout) {
                        isTimeout = true;
                        if(options && options.subTitleText) {
                            options.callback({
                                subTitleText : options.subTitleText
                            });
                        }else if(options) {
                            options.callback();
                        }

                    }
                }
            });

            KTW.managers.MessageManager.sendMessage(mashup_caption_msg, function (res_msg) {
                if (!isTimeout) {
                    isTimeout = true;
                    KTW.ui.LayerManager.stopLoading();
                    if(options && options.subTitleText) {
                        options.callback({
                            subTitleText : options.subTitleText,
                            msg : res_msg
                        });
                    }else if(options) {
                        options.callback({
                             msg : res_msg
                        });
                    }

                }
            });
        }
    }

    return {

        getConfigurationInfoByMenuId : _getConfigurationInfoByMenuId,
        getBMailInfo : _getBMailInfo,
        getConvertedType : _getConvertedType,
        showInitConfirmPopup : _showInitConfirmPopup,
        requestMashup_getCCLanguageList : _requestMashup_getCCLanguageList

    };
})();