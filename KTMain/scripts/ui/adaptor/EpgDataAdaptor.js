/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgDataAdaptor</code>
 *
 * 각종 편성표를 위한 epg data 및 채널 리스트 캐시 관리 adaptor
 *
 * @author dj.son
 * @since 2016-09-01
 */

KTW.ui.adaptor.EpgDataAdaptor = (function() {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var programManager = KTW.oipf.AdapterHandler.programSearchAdapter;
    var epgUtil = KTW.utils.epgUtil;
    var CHANNEL_CONFIG = KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG;

    var arrChannelList = [];
    var arrChannelListUpdate = [];
    var cbChannelListUpdate = null;

    var IS_TIME_UNIT_SEC = epgUtil.DATA.IS_TIME_UNIT_SEC;
    var isInit = false;

    /**
     * [dj.son] init
     */
    function _init () {
        log.printDbg("_init()");

        if (!isInit) {
            // 선호채널 뿐만 아니라 숨김채널, 제한채널 등의 channel list update event 도 이걸 통해 넘어온다.
            navAdapter.addChannelListUpdateListener(channelListUpdateListener);
            KTW.oipf.AdapterHandler.basicAdapter.addConfigChangeEventListener(onConfigDataChangeListener);
            isInit = true;
        }
    }

    /**
     * [dj.son] 채널 리스트 업데이트 리스너
     *
     * 캐시된 채널 리스트 update 여부를 저장하고 등록된 callback 을 호출한다.
     */
    function channelListUpdateListener (listId) {
        log.printDbg("channelListUpdateListener() " + listId);

        if (!listId || listId === CHANNEL_CONFIG.FAVOURITE_KIDS_CARE || listId === CHANNEL_CONFIG.FAVOURITE_SKIPPED) {
            for (var i in arrChannelListUpdate) {
                arrChannelListUpdate[i] = true;
            }
        }
        else  {
            arrChannelListUpdate[listId] = true;
        }

        // TODO 현재 보여지고 있는 view 에게 전달할 방법 찾아야 함;;;
        if (cbChannelListUpdate) {
            cbChannelListUpdate(listId);
        }
    }

    /**
     * 성인 메뉴 숨김 설정 값이 변경된 경우 epgUtil.reconstructionChList()가 다시 수행되어야 한다.
     *
     * @param evt
     */
    function onConfigDataChangeListener(evt) {
        if (evt) {
            log.printDbg("onConfigDataChangeListener() evt.key = " + evt.key);

            if (evt.key === KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY ||
                evt.key === KTW.oipf.Def.CONFIG.KEY.USE_KIDS_MODE) {
                for (var i in arrChannelListUpdate) {
                    arrChannelListUpdate[i] = true;
                }
            }
        }
    }

    /**
     * [dj.son] 채널 리스트 업데이트 callback 등록
     */
    function _setCallbackChannelListUpdate (callback) {
        log.printDbg("_setCallbackChannelListUpdate()");

        cbChannelListUpdate = callback;
    }

    /**
     * [dj.son] favId 별 채널 리스트 리턴
     *
     * 우선적으로 캐시된 리스트를 리턴하며, 리스트가 없거나 update 가 필요한 상태면 navAdapter 로부터 다시 리스트를 얻어온다.
     */
    function _getChannelList (arrFavId) {
        log.printDbg("_getChannelList() " + arrFavId);

        var result = [], favId;
        var length;

        length = arrFavId.length;
        for (var i = 0; i < length; i++) {
            favId = arrFavId[i];
            if (!arrChannelList[favId] || arrChannelList[favId].length === 0 || arrChannelListUpdate[favId]) {
                if (favId === KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_MODE ||
                    favId === KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_KIDS_MODE) {
                    if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                        arrChannelList[favId] = epgUtil.reconstructionChList(navAdapter.getChannelList(favId));
                    } else {
                        var temp_list;
                        var skipped_list = [];
                        var kids_list = [];

                        temp_list = navAdapter.getChannelList(CHANNEL_CONFIG.FAVOURITE_SKIPPED);
                        var temp_length;
                        if (temp_list && temp_list.length > 0) {
                            temp_length = temp_list.length;
                            for (var j = 0; j < temp_length; j++) {
                                skipped_list.push(temp_list[j]);
                            }
                        }
                        temp_list = navAdapter.getChannelList(favId);
                        temp_length = temp_list.length;
                        for (var j = 0; j < temp_length; j++) {
                            if (skipped_list.indexOf(temp_list[j]) === -1) {
                                kids_list.push(temp_list[j]);
                            }
                        }
                        arrChannelList[favId] = epgUtil.reconstructionChList(kids_list);
                    }
                } else {
                    arrChannelList[favId] = epgUtil.reconstructionChList(navAdapter.getChannelList(favId));
                }
                arrChannelListUpdate[favId] = false;
            }

            result = result.concat(arrChannelList[favId]);
        }

        return result;
    }

    /**
     * [dj.son] OTV 장르별 채널 리스트 리턴
     *
     */
    function _getGenreChannelList (genreCode) {
        log.printDbg("_getGenreChannelList() " + genreCode);

        var result = [];

        if (!arrChannelList[genreCode] || arrChannelListUpdate[genreCode]) {
            var list = navAdapter.getChannelList(CHANNEL_CONFIG.FAVOURITE_FULL_EPG);
            var listLength = list.length;
            var tempList = [];

            for (var i = 0; i < listLength; i++) {
                if (epgUtil.getChGenre(list[i]) === genreCode) {
                    tempList.push(list[i]);
                }
            }

            arrChannelListUpdate[genreCode] = false;
            arrChannelList[genreCode] = epgUtil.reconstructionChList(tempList);
        }

        return result.concat(arrChannelList[genreCode]);
    }

    /**
     * [dj.son] 채널들의 programmes 리스트 리턴
     *
     * @param channelList - 채널 리스트 Array
     * @returns {Array}
     */
    function _getProgrammesList (options) {
        log.printDbg("_getProgrammesList()");

        var programmesList = [];
        var tempProgList = [];

        // 2017.05.03 dhlee
        // 예외처리 추가
        //if (options && options.channelList) {
        if (options && options.channelList && options.channelList.length > 0) {
            var chLength = options.channelList.length;
            for (var i = 0; i < chLength; i++) {
                tempProgList[options.channelList[i].data.sid] = [];
            }

            var list = programManager.getPrograms(options.startTime, null, null, options.channelList);
            var listLength = list.length;

            for (var i = 0; i < listLength; i++) {
                tempProgList[list[i].channel.data.sid].push(list[i]);
            }

            for (var i = 0; i < chLength; i++) {
                programmesList.push(epgUtil.revisePrograms(options.channelList[i], tempProgList[options.channelList[i].data.sid]));
            }
        }

        return programmesList;
    }

    /**
     * [dj.son] OTS 프로그램 검색 메뉴의 programmes 리스트 리턴 (현재 시간 기준)
     *
     * @param options - genreCode : 장르 코드 (ProgrammeSearchAdapter.PROGRAM_GENRE 참조)
     *                   orderByTime : true 일 경우 시간순 정렬, false 일 경우 알파벳 순 정렬
     *
     * @returns {Programme Array}
     */
    function _getGenreProgrammesList (options) {
        log.printDbg("_getProgrammesList()");

        if (!options || !options.genreCode) {
            log.printDbg("genreCode options is not exist... so return");
            return [];
        }

        var genreCode = options.genreCode;
        var order = options.orderByTime ? "T" : "N";
        var curTime = (new Date()).getTime() / 1000;

        var programmesList = [];
        var list = navAdapter.getChannelList(CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);

        if (list && list.length > 0) {
            var tmpList = programManager.getGenreProgramList(list, curTime, genreCode, null, order);

            if (tmpList && tmpList.length > 0) {
                programmesList = tmpList;
            }
        }

        return programmesList;
    }

    /**
     * [dj.son] programme 리스트에서 referenceTime + 90분까지의 programme 리스트 생성
     *
     * @param options programmesList - programme 리스트 array
     *                 referenceTime - 기준 시간 Date
     * @returns {Array}
     */
    function _makeProgrammesSection (options) {
        log.printDbg("_makeProgrammesSection()");

        var startTime = options.referenceTime.getTime();
        var endTime = epgUtil.reviseDate(new Date(startTime + (90 * 60 * 1000))).getTime();
        if (IS_TIME_UNIT_SEC) {
            startTime = Math.floor(startTime / 1000);
            endTime = Math.floor(endTime / 1000);
        }
        var progLength = options.programmesList.length;
        var programme = null;
        var progStartTime = null;
        var progDuration = null;
        var progEndTime = null;
        var tempProgList = [];

        for (var j = 0; j < progLength; j++) {
            programme = options.programmesList[j];
            progStartTime = options.programmesList[j].startTime;
            progDuration = options.programmesList[j].duration;
            progEndTime = progStartTime + progDuration;

            if (progStartTime == null || progDuration == null || progEndTime <= startTime) {
                continue;
            }

            if (progStartTime > (endTime - 60)) {
                break;
            }

            tempProgList.push(programme);
        }

        return tempProgList;
    }

    return {
        init: _init,

        setCallbackChannelListUpdate: _setCallbackChannelListUpdate,

        getChannelList: _getChannelList,
        getGenreChannelList: _getGenreChannelList,
        getProgrammesList: _getProgrammesList,
        getGenreProgrammesList: _getGenreProgrammesList,
        makeProgrammesSection: _makeProgrammesSection
    };
})();