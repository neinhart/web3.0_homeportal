/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelPreView</code>
 *
 * 미가입 채널 중 N minutes 미리보기를 제공하는 채널의 미리보기 View
 *
 * @author dhlee
 * @since 2016-08-08
 */

"use strict";

(function() {
    KTW.ui.view.ChannelPreView = function(parent) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var PREVIEW_IMAGE_PATH = KTW.CONSTANT.IMAGE_PATH + "channelpreview/";

        var previewtenSecTimer = null;

        var remainTime = 0;         // preview 남은 시간
        var fullmode_preview_container_div = null;
        var fullepg_preview_container_div = null;
        var fullmode_homemenu_container_div = null;

        var parent = parent;

        var initRemainTime = 0;

        var textPreview = null;

        var textRemainTime = null;
        var textRemainTimeUnit = null;

        var textHomeMenuRemainTime = null;
        var textHomeMenuRemainTimeUnit = null;

        var circleImg = null;


        var iframeMode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;

        var isShow = false;

        var pause_clock = 0;

        var params = null;

        this.create = function() {
            log.printDbg("create()");

            createElement(parent.div);
        };

        this.show = function(options) {
            log.printDbg("show()");


            if(options !== undefined && options !== null && options.resume === true) {
                if(pause_clock>0) {
                    log.printDbg("show(), options = " + JSON.stringify(options));
                    var elapse_time = (new Date().getTime() -  pause_clock) / 1000;
                    remainTime -= Math.ceil(elapse_time);
                    log.printDbg("show(), elapse_time = " + elapse_time + " , remainTime : " + remainTime + " , pause_clock : " + pause_clock);

                    if(previewtenSecTimer !== null) {
                        clearPreviewTimer();
                    }

                    updatePreViewTime();

                    var checkTime = remainTime-10;
                    log.printDbg("show() checkTime : " + checkTime);
                    previewtenSecTimer = setTimeout(checkPreViewTime, checkTime*1000);

                    isShow = true;
                }
            }else {
                if(parent.getParams() !== undefined && parent.getParams() !== null) {
                    pause_clock = 0;
                    params = parent.getParams().data;
                    log.printDbg("show(), params = " + JSON.stringify(params));
                }
                // remainTime은 ChannelPreviewManager 에서 setData() -> activate() 까지의 시간을 뺀 값.
                if(params !== undefined && params !== null) {
                    pause_clock = 0;
                    remainTime = parseInt(params.remain_time);
                    initRemainTime = remainTime;
                    log.printDbg("show(), remainTime = " + remainTime);

                    if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
                        fullmode_preview_container_div.css("display" , "");
                        fullepg_preview_container_div.css("display" , "none");
                        fullmode_homemenu_container_div.css("display" , "none");
                        fullepg_preview_container_div.css({top:73});
                    }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE_HOME) {
                        fullmode_homemenu_container_div.css("display" , "none");
                        fullmode_preview_container_div.css("display" , "");
                        fullepg_preview_container_div.css("display" , "none");
                    }else {
                        if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE) {
                            fullepg_preview_container_div.css({top:73});
                        }else {
                            fullepg_preview_container_div.css({top:181});
                        }
                        fullmode_homemenu_container_div.css("display" , "none");
                        fullmode_preview_container_div.css("display" , "none");
                        fullepg_preview_container_div.css("display" , "");
                    }
                    updatePreViewTime();
                }

                if(previewtenSecTimer !== null) {
                    clearPreviewTimer();
                }

                var checkTime = remainTime-10;
                log.printDbg("show() checkTime : " + checkTime);
                previewtenSecTimer = setTimeout(checkPreViewTime, checkTime*1000);

                //preview_container_div.css("display" , "");
                isShow = true;
            }
        };

        this.hide = function(options) {
            log.printDbg("hide()");
            clearPreviewTimer();
            isShow = false;
            pause_clock = new Date().getTime();
        };

        this.changeIframeMode = function(mode) {
            log.printDbg("changeIframeMode() isShow : " + isShow + " , mode : " + mode);
            iframeMode = mode;

            if(isShow === true) {
                updatePreViewTime();
            }
        };

        /**
         * create preview screen element
         *
         * parent_div - preview_container_div - channel_preview_bg_img
         *                                    - channel_preview_progress_img
         *                                    - channel_preview_text_shadow
         *                                    - channel_preview_text
         *                                    - preview_time_container_div - preview_time_text_shadow
         *                                                                 - preview_time_text
         *                                                                 - preview_time_unit_text_shadow
         *                                                                 - preview_time_unit_text
         *
         * @param parent_div
         *
         * @private
         */
        function createElement(parent_div) {
            log.printDbg("createElement()");

            // ISU 상품과 UP-Selling 상품이 있음. 2가지 상품에 대한 preview 이미지가 다름(현 웹 홈포털)
            // up-selling과 isu 모두 동일하게 처리한다.
            fullmode_preview_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_preview_container",
                    css: {
                        position: "absolute", left: 1475, top: 180, width: 338, height: 183,
                        display: "none"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "preview_bg_img",
                    src: PREVIEW_IMAGE_PATH + "bg_1min_preview.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 338, height: 183
                    }
                },
                parent: fullmode_preview_container_div
            });

            // progress image 로서 animation 이 필요할 수 있음
            circleImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "preview_progress_img",
                    src: PREVIEW_IMAGE_PATH + "img_progress_1min_preview_w.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 338, height: 183
                    }
                },
                parent: fullmode_preview_container_div
            });


            // "미리보기" text
            // 10초부터는 미리보기 -> "남은 시간" 으로 변경 되어야 한다.
            textPreview = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_text",
                    class: "font_m",
                    css: {
                        position: "absolute",
                        left: 30,
                        top: 292-180,
                        width:120,
                        height:29,
                        "font-size": 27,
                        color: "rgba(255, 255, 255, 1)",
                        "text-align": "center",
                        "letter-spacing":-1.35
                    }
                },
                text: "미리보기",
                parent: fullmode_preview_container_div
            });

            var preview_time_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_preview_time_container",
                    css : {
                        position: "absolute", left: 30, top: 225-180, width: 120, height: 50
                    }
                },
                parent: fullmode_preview_container_div
            });


            // 시간 text
            textRemainTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_time_text",
                    class: "font_m",
                    css : {
                        position: "absolute",
                        color: "rgba(255,255,255,1)",
                        left:0,
                        top:225-225,
                        height:50,
                        "font-size": 48,
                        "letter-spacing":-2.4
                    }
                },
                parent: preview_time_container_div
            });

            // 시간 단위(분/초) text
            textRemainTimeUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_time_unit_text",
                    class: "font_m",
                    css : {
                        position: "absolute", top:236-225 , height: 29,
                        color: "rgba(255,255,255,1)",
                        "font-size": 27
                    }
                },
                parent: preview_time_container_div
            });


            fullepg_preview_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "pip_iframe_preview_area",
                    css: {
                        position: "absolute" , left:1767,top: 73 , width: 105, height: 94 , display:"none"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "preview_background",
                    src: "images/pipview/bg_1min_preview_s.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 94, height: 94
                    }
                },
                parent: fullepg_preview_container_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "preview_foreground",
                    src: "images/pipview/img_progress_1min_preview_w_s.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 94, height: 94,display:""
                    }
                },
                parent: fullepg_preview_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_time",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top : 36, width:0,height: 27,
                        color: "rgba(255, 255, 255, 1)", "font-size": 25 , "text-align" : "left","letter-spacing":-1.25
                    }
                },
                parent: fullepg_preview_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_unit",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top : 40, width:20,height: 20,
                        color: "rgba(255, 255, 255, 1)", "font-size": 18 , "text-align" : "left"
                    }
                },
                parent: fullepg_preview_container_div
            });

            fullmode_homemenu_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_preview_container",
                    css: {
                        position: "absolute", left: 1670, top: 180, width: 183, height: 183,
                        display: "none"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "preview_bg_img",
                    src: PREVIEW_IMAGE_PATH + "bg_1min_preview_home.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 183, height: 183
                    }
                },
                parent: fullmode_homemenu_container_div
            });

            // progress image 로서 animation 이 필요할 수 있음
            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "preview_progress_img",
                    src: PREVIEW_IMAGE_PATH + "home_img_progress_1min_preview_w.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 183, height: 183
                    }
                },
                parent: fullmode_homemenu_container_div
            });

            var preview_homemenu_time_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_preview_time_container",
                    css : {
                        position: "absolute", left: 30, top: 253-180, width: 120, height: 50
                    }
                },
                parent: fullmode_homemenu_container_div
            });


            // 시간 text
            textHomeMenuRemainTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_time_text",
                    class: "font_m",
                    css : {
                        position: "absolute",
                        color: "rgba(255,255,255,1)",
                        left:0,
                        top:225-225,
                        height:50,
                        "font-size": 48,
                        "letter-spacing":-2.4
                    }
                },
                parent: preview_homemenu_time_container_div
            });


            // 시간 단위(분/초) text
            textHomeMenuRemainTimeUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "preview_time_unit_text",
                    class: "font_m",
                    css : {
                        position: "absolute", top:236-225 , height: 29,
                        color: "rgba(255,255,255,1)",
                        "font-size": 27
                    }
                },
                parent: preview_homemenu_time_container_div
            });

        }

        /**
         * preview timer callback
         *
         * @private
         */
        function checkPreViewTime() {
            log.printDbg("checkPreViewTime(), remainTime = " + remainTime);

            previewtenSecTimer = null;
            if(remainTime>10) {
                remainTime = 10;
            }else {
                remainTime--;
            }
            updatePreViewTime();
            if (remainTime <= 0) {
                stopPreView();
            }else {
                previewtenSecTimer = setTimeout(checkPreViewTime, 1000);
            }
        }

        /**
         * update preview scene (remain time, display text, image)
         *
         * @param forced
         *
         * @private
         */
        function updatePreViewTime() {
            log.printDbg("updatePreViewTime() remainTime = " + remainTime);

            if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
                fullmode_preview_container_div.css("display" , "");
                fullmode_homemenu_container_div.css("display" , "none");
                fullepg_preview_container_div.css("display" , "none");

                if (remainTime <= 10) {
                    textPreview.text("남은 시간");
                }else {
                    textPreview.text("미리보기");
                }
                textPreview.css("display" , "");

                var timeTextLength = 0;
                var timeUnitTextLength = 0;
                // 60으로 나누어 떨어지면 "분"으로 표시하고 그렇지 않으면 "초"로 표시한다.
                // ISU, up-selling 구분 없이 처리한다.
                if (remainTime <= 10) {
                    timeTextLength = util.getTextLength(remainTime.toString() , "RixHead M" , 48 , -2.4);
                    timeUnitTextLength = util.getTextLength("초" , "RixHead M" , 27);

                    textRemainTime.text(remainTime.toString());
                    textRemainTimeUnit.text("초");

                    textRemainTime.css({ color: "rgba(255, 96, 108, 1)"});
                    circleImg.attr("src", PREVIEW_IMAGE_PATH + "img_progress_1min_preview.png");

                }else {
                    textRemainTime.css({ color: "rgba(255, 255, 255, 1)"});
                    circleImg.attr("src", PREVIEW_IMAGE_PATH + "img_progress_1min_preview_w.png");

                    if (initRemainTime % 60 === 0 && initRemainTime >= 60) {
                        timeTextLength = util.getTextLength((initRemainTime / 60).toString() , "RixHead M" , 48 , -2.4);
                        timeUnitTextLength = util.getTextLength("분" , "RixHead M" , 27);

                        textRemainTime.text((initRemainTime / 60).toString());
                        textRemainTimeUnit.text("분");
                    } else {
                        timeTextLength = util.getTextLength(initRemainTime.toString() , "RixHead M" , 48 , -2.4);
                        timeUnitTextLength = util.getTextLength("초" , "RixHead M" , 27);

                        textRemainTime.text(initRemainTime.toString());
                        textRemainTimeUnit.text("초");
                    }
                }



                var startLeft = (120 - (timeTextLength+4+timeUnitTextLength))/2;

                textRemainTime.css({left:startLeft});

                textRemainTime.css({width:timeTextLength});

                startLeft+=timeTextLength;
                startLeft+=4;
                textRemainTimeUnit.css({left:startLeft});

                textRemainTimeUnit.css({width:timeUnitTextLength});
            }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE_HOME) {
                fullmode_preview_container_div.css("display" , "none");
                fullepg_preview_container_div.css("display" , "none");
                fullmode_homemenu_container_div.css("display" , "");

                var timeTextLength = 0;
                var timeUnitTextLength = 0;
                // 60으로 나누어 떨어지면 "분"으로 표시하고 그렇지 않으면 "초"로 표시한다.
                // ISU, up-selling 구분 없이 처리한다.
                if (remainTime <= 10) {
                    timeTextLength = util.getTextLength(remainTime.toString() , "RixHead M" , 48 , -2.4);
                    timeUnitTextLength = util.getTextLength("초" , "RixHead M" , 27);

                    textHomeMenuRemainTime.text(remainTime.toString());
                    textHomeMenuRemainTimeUnit.text("초");

                    textHomeMenuRemainTime.css({ color: "rgba(255, 96, 108, 1)"});

                    fullmode_homemenu_container_div.find("#preview_progress_img").attr("src", "images/channelpreview/home_img_progress_1min_preview.png");
                }else {
                    textHomeMenuRemainTime.css({ color: "rgba(255, 255, 255, 1)"});

                    fullmode_homemenu_container_div.find("#preview_progress_img").attr("src", "images/channelpreview/home_img_progress_1min_preview_w.png");


                    if (initRemainTime % 60 === 0 && initRemainTime >= 60) {
                        timeTextLength = util.getTextLength((initRemainTime / 60).toString() , "RixHead M" , 48 , -2.4);
                        timeUnitTextLength = util.getTextLength("분" , "RixHead M" , 27);

                        textHomeMenuRemainTime.text((initRemainTime / 60).toString());
                        textHomeMenuRemainTimeUnit.text("분");
                    } else {
                        timeTextLength = util.getTextLength(initRemainTime.toString() , "RixHead M" , 48 , -2.4);
                        timeUnitTextLength = util.getTextLength("초" , "RixHead M" , 27);

                        textHomeMenuRemainTime.text(initRemainTime.toString());
                        textHomeMenuRemainTimeUnit.text("초");
                    }
                }

                var startLeft = (120 - (timeTextLength+4+timeUnitTextLength))/2;

                textHomeMenuRemainTime.css({left:startLeft});

                textHomeMenuRemainTime.css({width:timeTextLength});

                startLeft+=timeTextLength;
                startLeft+=4;
                textHomeMenuRemainTimeUnit.css({left:startLeft});

                textHomeMenuRemainTimeUnit.css({width:timeUnitTextLength});

            }else {
                if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE) {
                    fullepg_preview_container_div.css({top:73});
                }else {
                    fullepg_preview_container_div.css({top:181});
                }
                fullmode_homemenu_container_div.css("display" , "none");
                fullmode_preview_container_div.css("display" , "none");
                fullepg_preview_container_div.css("display" , "");

                var time = "";
                var unit = "";
                if (initRemainTime > 0) {
                    if (remainTime <= 10) {
                        time = "" + remainTime;
                        unit = "초";
                    }else {
                        if(initRemainTime % 60 === 0 && initRemainTime >= 60) {
                            time = ""+ (initRemainTime / 60);
                            unit = "분";
                        }else {
                            time = "" + initRemainTime;
                            unit = "초";
                        }
                    }

                    timeTextLength = util.getTextLength(time , "RixHead M" , 25 , -1.25);
                    timeUnitTextLength = util.getTextLength(unit , "RixHead M" , 18);

                    var startLeft = (94 - (timeTextLength+4+timeUnitTextLength))/2;

                    if(remainTime<=10) {
                        fullepg_preview_container_div.find("#preview_foreground").attr("src", "images/pipview/sample_progress_1min_preview_w_s.png");
                        fullepg_preview_container_div.find("#preview_time").css({ color: "rgba(255, 96, 108, 1)"});
                    }else {
                        fullepg_preview_container_div.find("#preview_foreground").attr("src", "images/pipview/img_progress_1min_preview_w_s.png");
                        fullepg_preview_container_div.find("#preview_time").css({ color: "rgba(255, 255, 255, 1)"});
                    }

                    fullepg_preview_container_div.find("#preview_time").text(time);
                    fullepg_preview_container_div.find("#preview_unit").text(unit);
                    fullepg_preview_container_div.find("#preview_time").css({left:startLeft,width:timeTextLength});
                    startLeft+=timeTextLength;
                    startLeft+=4;
                    fullepg_preview_container_div.find("#preview_unit").css({left:startLeft,width:timeUnitTextLength});
                }
            }

        }

        /**
         * clear preview timer.
         *
         * @private
         */
        function clearPreviewTimer() {
            log.printDbg("before clearPreviewTimer()");
            if (previewtenSecTimer !== null) {
                clearTimeout(previewtenSecTimer);
                log.printDbg("after clearPreviewTimer()");
            }
            previewtenSecTimer = null;
        }


        /**
         * stop Channel Preview
         *
         * @private
         */
        function stopPreView() {
            log.printDbg("stopPreView()");

            clearPreviewTimer();
            KTW.managers.service.ChannelPreviewManager.stop(true, true);
        }
    };

    KTW.ui.view.ChannelPreView.prototype = new KTW.ui.View();
    KTW.ui.view.ChannelPreView.prototype.constructor = KTW.ui.view.ChannelPreView;

    KTW.ui.view.ChannelPreView.prototype.create = function() {
        this.create();
    };
    KTW.ui.view.ChannelPreView.prototype.show = function() {
        this.show();
    };
    KTW.ui.view.ChannelPreView.prototype.hide = function() {
        this.hide();
    };
    KTW.ui.view.ChannelPreView.prototype.handleKeyEvent = function(key_code) {
        return false;
    };
})();