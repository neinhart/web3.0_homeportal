/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>PromoTriggerView</code>
 *
 * @author dhlee
 * @since 2016-07-06
 *
 * Promo Channel에 보여지는 trigger view
 */

"use strict";

(function() {
    KTW.ui.view.PromoTriggerView = function() {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var callback;

        this.create = function () {
            callback = this.parent.params.callback;
            createElement(this.parent.div);
        };

        this.show = function () {
            log.printDbg("show()");
        };

        this.hide = function () {
            log.printDbg("hide()");
        };

        /**
         * Promo Trigger를 위한 element를 생성한다.
         *
         * @param parent_div
         */
        function createElement(parent_div) {
            var promo_image_path = KTW.CONSTANT.IMAGE_PATH + "promotrigger/";
            parent_div.attr({class: "arrange_frame"});

            var promo_container_div = util.makeElement({
                tag   : "<div />",
                attrs : {
                    id : "promo_trigger_area",
                    css: {
                        position: "absolute", left: 1616, top: 229, width: 230, height: 102
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag   : "<img />",
                attrs : {
                    id : "promo_trigger_bg_img",
                    src: promo_image_path + "bg_watch_right.png",
                    css: {
                        position: "absolute", left: 0, top: 0
                    }
                },
                parent: promo_container_div
            });

            util.makeElement({
                tag   : "<img />",
                attrs : {
                    id : "promo_trigger_key_r_img",
                    src: promo_image_path + "key_r_b.png",
                    css: {
                        position: "absolute", left: 41, top: 34
                    }
                },
                parent: promo_container_div
            });

            util.makeElement({
                tag   : "<span />",
                attrs : {
                    id   : "promo_trigger_text",
                    class: "font_l",
                    css  : {
                        position: "absolute", left: 79, top: 32,
                        color: "rgb(255, 255, 255)",
                        "font-size": 30
                    }
                },
                text  : "바로보기",
                parent: promo_container_div
            });
        }

        /**
         * key event 처리
         *
         * @param key_code
         */
        this.controlKey = function (key_code) {
            var consumed = false;

            switch (key_code) {
                case KTW.KEY_CODE.RED:
                    log.printDbg("controlKey(), pressed RED");
                    callback();
                    consumed = true;
                    break;
            }

            return consumed;
        }
    };

    KTW.ui.view.PromoTriggerView.prototype = new KTW.ui.View();
    KTW.ui.view.PromoTriggerView.prototype.constructor = KTW.ui.view.PromoTriggerView;

    KTW.ui.view.PromoTriggerView.prototype.create = function () {
        this.create();
    };

    KTW.ui.view.PromoTriggerView.prototype.show = function () {
        this.show();
    };

    KTW.ui.view.PromoTriggerView.prototype.hide = function () {
        this.hide();
    };

    KTW.ui.view.PromoTriggerView.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();
