/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MiniEpgRelatedRightMenuView</code>
 *
 * 채널 미니 가이드 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */
"use strict";

(function() {
    KTW.ui.view.MiniEpgRelatedRightMenuView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;


        var parent = parent;
        var div = null;

        var topMenu = null;
        var middleMenu = null;
        var bottomMenu = null;
        var callbackFunc = null;

        var isShowTopMenu = true;
        var topMenuList = null;

        var isShowCaption = true;
        var isCaptionDimmed = false;

        var isShowVoice = true;
        var isVoiceDimmed = false;
        var voiceList = null;
        var voiceFocusIndex = 0;

        var isShowBlueToothVoice = true;
        var isBlueToothVoiceDimmed = false;
        var blueToothVoiceList = null;
        var blueToothVoiceFocusIndex = 0;

        var isShowBlueToothListening = true;
        var isBlueToothListeningDimmed = false;
        var blueToothListeningList = null;
        var blueToothListeningFocusIndex = 0;


        var isShowBottomMenu = true;
        var bottomMenuList = null;


        var topMenuBtnList = [];
        var middleCaptionBtn = null;
        var middleVoiceBtn = null;
        var middleBlueToothVoiceBtn = null;
        var middleBlueToothListeningBtn = null;

        var bottomMenuBtnList = [];

        var focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
        var focusIndex = -1;

        var focusBtn = null;

        var voiceDropDown = null;
        var isShowVoiceDropDown = false;
        var focusVoiceDropDownIndex = -1;

        var bluetoothVoiceDropDown = null;
        var isShowbluetoothVoiceDropDown = false;
        var focusbluetoothVoiceDropDownIndex = -1;


        var bluetoothListeningDropDown = null;
        var isShowbluetoothListeningDropDown = false;
        var focusbluetoothListeningDropDownIndex = -1;


        var kidsModeLayerId = "KidsModePopup";
        var isHideAction = false;

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        this.create = function () {
            log.printDbg("create()");
        };

        this.getDiv = function () {
            return div;
        };

        this.show = function (options) {
            log.printDbg("show()");
            if(options !== undefined && options !== null) {
                topMenu = options.top_menu;
                middleMenu = options.middle_menu;
                bottomMenu = options.bottom_menu;
                callbackFunc = options.miniepg_callback;

                if(topMenu !== undefined && topMenu !== null) {
                    isShowTopMenu = topMenu.is_show_top_menu;
                    topMenuList = topMenu.channel_genre;
                }

                if(middleMenu !== undefined && middleMenu !== null) {
                    var captionMenu = middleMenu.caption;
                    if(captionMenu !== undefined && captionMenu !== null) {
                        isShowCaption = captionMenu.is_show_caption;
                        isCaptionDimmed = captionMenu.is_dimmed;
                    }

                    var voiceMenu = middleMenu.voice;
                    if(voiceMenu !== undefined && voiceMenu !== null) {
                        isShowVoice = voiceMenu.is_show_voice;
                        isVoiceDimmed = voiceMenu.is_dimmed;
                        voiceList = voiceMenu.voicelist;
                        voiceFocusIndex = voiceMenu.voice_focus;
                    }

                    var blueToothVoiceMenu = middleMenu.bluetooth_voice_lang;
                    if(blueToothVoiceMenu !== undefined && blueToothVoiceMenu !== null) {
                        isShowBlueToothVoice = blueToothVoiceMenu.is_show_bluetooth_voice_lang;
                        isBlueToothVoiceDimmed = blueToothVoiceMenu.is_dimmed;
                        blueToothVoiceList = blueToothVoiceMenu.bluetooth_voice_lang_list;
                        blueToothVoiceFocusIndex = blueToothVoiceMenu.bluetooth_voice_lang_focus;
                    }

                    var blueToothListeningMenu = middleMenu.bluetooth_listening;
                    if(blueToothListeningMenu !== undefined && blueToothListeningMenu !== null) {
                        isShowBlueToothListening = blueToothListeningMenu.is_show_bluetooth_listening;
                        isBlueToothListeningDimmed = blueToothListeningMenu.is_dimmed;
                        blueToothListeningList = blueToothListeningMenu.bluetooth_listening_list;
                        blueToothListeningFocusIndex = blueToothListeningMenu.bluetooth_listening_focus;
                    }
                }

                if(bottomMenu !== undefined && bottomMenu !== null) {
                    isShowBottomMenu = bottomMenu.is_show_bottom_menu;
                    bottomMenuList = bottomMenu.app_list;
                }

            }

            isHideAction = false;
            _createElement();
            _updateData();


            if(isShowTopMenu === true) {
                focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;

                if(topMenuBtnList !== null && topMenuBtnList.length>0) {
                    focusBtn = topMenuBtnList[0];
                }
            } else if( isShowCaption === true && isCaptionDimmed === false) {
                focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                focusBtn = middleCaptionBtn;
            } else if( isShowVoice === true && isVoiceDimmed === false) {
                focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                focusBtn = middleVoiceBtn;
            } else if( isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                focusBtn = middleBlueToothVoiceBtn;
            } else if( isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                focusBtn = middleBlueToothListeningBtn;
            } else if( isShowBottomMenu === true ) {
                focusMenu =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;

                if(bottomMenuList !== null && bottomMenuList.length>0) {
                    focusBtn = bottomMenuList[0];
                }
            }

        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");

            _hideAll();

            div.css("display", "none");
            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.focus = function () {
            log.printDbg("focus()");


            if(focusBtn !== null) {
                focusBtn.btn_default_img.css("display" , "none");
                focusBtn.btn_focus_div.css("display" , "");
                focusBtn.btn_text.css("color", "rgba(255,255,255,1)");
                if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU
                    && (focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU)) {
                    focusBtn.btn_down_img.attr("src", "images/popup/arw_option_popup_dw_f.png");
                }

                focusBtn.btn_text.removeClass("font_l");
                focusBtn.btn_text.addClass("font_m");
            }
        };

        this.blur = function () {
            log.printDbg("blur()");

            if(focusBtn !== null) {
                focusBtn.btn_default_img.css("display" , "");
                focusBtn.btn_focus_div.css("display" , "none");

                focusBtn.btn_text.css("color", "rgba(255,255,255,1)");

                focusBtn.btn_text.removeClass("font_m");
                focusBtn.btn_text.addClass("font_l");

                if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU &&
                    (focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU)) {
                    focusBtn.btn_down_img.attr("src", "images/popup/arw_option_popup_dw_f.png");
                }
            }

            _hideVoiceDropDown();
            _hideBlueToothVoiceDropDown();
            _hideBlueToothListeningDropDown();
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;
            if(isHideAction === true) {
                return consumed;
            }

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    if(isShowVoiceDropDown === true) {
                        _focusMoveVoiceDropDown(true);
                    }else if(isShowbluetoothVoiceDropDown === true) {
                        _focusMoveBlueToothVoiceDropDown(true);
                    }else if(isShowbluetoothListeningDropDown === true) {
                        _focusMoveBlueToothListeningDropDown(true);
                    } else {
                        _focusMove(true);
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    if(isShowVoiceDropDown === true) {
                        _focusMoveVoiceDropDown(false);
                    }else if(isShowbluetoothVoiceDropDown === true) {
                        _focusMoveBlueToothVoiceDropDown(false);
                    }else if(isShowbluetoothListeningDropDown === true) {
                        _focusMoveBlueToothListeningDropDown(false);
                    }else {
                        _focusMove(false);
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU) {
                        if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU) {
                            if(callbackFunc !== null) {
                                parent.hideView(null , null , null , callbackFunc , focusMenu , focusIndex , null , null);
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU) {
                            if(isShowVoiceDropDown === false) {
                                _showVoiceDropDown();
                            }else {
                                if(voiceFocusIndex !== focusVoiceDropDownIndex) {
                                    if(callbackFunc !== null) {
                                        parent.hideView(null , null , null , callbackFunc , focusMenu , focusIndex , voiceList , focusVoiceDropDownIndex);
                                    }
                                }else {
                                    _hideVoiceDropDown();
                                }
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU) {
                            if(isShowbluetoothVoiceDropDown === false) {
                                _showBlueToothVoiceDropDown();
                            }else {
                                if(blueToothVoiceFocusIndex !== focusbluetoothVoiceDropDownIndex) {
                                    if(callbackFunc !== null) {
                                        parent.hideView(null , null , null , callbackFunc , focusMenu , focusIndex , blueToothVoiceList , focusbluetoothVoiceDropDownIndex);
                                    }
                                }else {
                                    _hideBlueToothVoiceDropDown();
                                }

                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU) {
                            if(isShowbluetoothListeningDropDown === false) {
                                _showBlueToothListeningVoiceDropDown();
                            }else {
                                if(blueToothListeningFocusIndex !== focusbluetoothListeningDropDownIndex) {
                                    if(callbackFunc !== null) {
                                        parent.hideView(null , null , null , callbackFunc , focusMenu , focusIndex , blueToothListeningList , focusbluetoothListeningDropDownIndex);
                                    }
                                }else {
                                    _hideBlueToothListeningDropDown();
                                }

                            }


                        }
                    }else {
                        if(callbackFunc !== null) {
                            parent.hideView(null , null , null , callbackFunc , focusMenu , focusIndex , null , null);
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    if(isShowVoiceDropDown === true) {
                        _hideVoiceDropDown();
                        consumed = true;
                    }else if(isShowbluetoothVoiceDropDown === true) {
                        _hideBlueToothVoiceDropDown();
                        consumed = true;
                    }else if(isShowbluetoothListeningDropDown === true) {
                        _hideBlueToothListeningDropDown();
                        consumed = true;
                    }

                    break;
            }

            return consumed;
        };

        function _focusMoveVoiceDropDown(isUp) {

            var item = voiceDropDown.dropbox_item_list[focusVoiceDropDownIndex];

            if(item !== null) {
                item.default_img.css("display" , "");
                item.default_text.css("display" , "");

                item.focus_div.css("display" , "none");
                item.focus_text.css("display" , "none");

                if(focusVoiceDropDownIndex === voiceFocusIndex) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select.png");
                }
            }

            if(isUp === true) {
                focusVoiceDropDownIndex--;
            }else {
                focusVoiceDropDownIndex++;
            }

            if(focusVoiceDropDownIndex<0) {
                focusVoiceDropDownIndex = voiceDropDown.dropbox_item_list.length - 1;
            }else if(focusVoiceDropDownIndex>=voiceDropDown.dropbox_item_list.length) {
                focusVoiceDropDownIndex = 0;
            }

            item = voiceDropDown.dropbox_item_list[focusVoiceDropDownIndex];

            if(item !== null) {
                item.default_img.css("display" , "none");
                item.default_text.css("display" , "none");

                item.focus_div.css("display" , "");
                item.focus_text.css("display" , "");

                if(focusVoiceDropDownIndex === voiceFocusIndex) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");
                }
            }
        }




        function _focusMoveBlueToothVoiceDropDown(isUp) {

            var item = bluetoothVoiceDropDown.dropbox_item_list[focusbluetoothVoiceDropDownIndex];

            if(item !== null) {
                item.default_img.css("display" , "");
                item.default_text.css("display" , "");

                item.focus_div.css("display" , "none");
                item.focus_text.css("display" , "none");

                if(focusbluetoothVoiceDropDownIndex === blueToothVoiceFocusIndex) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select.png");
                }
            }

            if(isUp === true) {
                focusbluetoothVoiceDropDownIndex--;
            }else {
                focusbluetoothVoiceDropDownIndex++;
            }

            if(focusbluetoothVoiceDropDownIndex<0) {
                focusbluetoothVoiceDropDownIndex = bluetoothVoiceDropDown.dropbox_item_list.length - 1;
            }else if(focusbluetoothVoiceDropDownIndex>=bluetoothVoiceDropDown.dropbox_item_list.length) {
                focusbluetoothVoiceDropDownIndex = 0;
            }

            item = bluetoothVoiceDropDown.dropbox_item_list[focusbluetoothVoiceDropDownIndex];

            if(item !== null) {
                item.default_img.css("display" , "none");
                item.default_text.css("display" , "none");

                item.focus_div.css("display" , "");
                item.focus_text.css("display" , "");

                if(focusbluetoothVoiceDropDownIndex === blueToothVoiceFocusIndex) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");
                }
            }
        }

        function _focusMoveBlueToothListeningDropDown(isUp) {
            var item = bluetoothListeningDropDown.dropbox_item_list[focusbluetoothListeningDropDownIndex];

            if(item !== null) {
                item.default_img.css("display" , "");
                item.default_text.css("display" , "");

                item.focus_div.css("display" , "none");
                item.focus_text.css("display" , "none");

                if(focusbluetoothListeningDropDownIndex === blueToothListeningFocusIndex) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select.png");
                }
            }

            if(isUp === true) {
                focusbluetoothListeningDropDownIndex--;
            }else {
                focusbluetoothListeningDropDownIndex++;
            }

            if(focusbluetoothListeningDropDownIndex<0) {
                focusbluetoothListeningDropDownIndex = bluetoothListeningDropDown.dropbox_item_list.length - 1;
            }else if(focusbluetoothListeningDropDownIndex>=bluetoothListeningDropDown.dropbox_item_list.length) {
                focusbluetoothListeningDropDownIndex = 0;
            }

            item = bluetoothListeningDropDown.dropbox_item_list[focusbluetoothListeningDropDownIndex];

            if(item !== null) {
                item.default_img.css("display" , "none");
                item.default_text.css("display" , "none");

                item.focus_div.css("display" , "");
                item.focus_text.css("display" , "");

                if(focusbluetoothListeningDropDownIndex === blueToothListeningFocusIndex) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");
                }
            }
        }

        function _showVoiceDropDown() {
            isShowVoiceDropDown = true;

            var itemList = voiceDropDown.dropbox_item_list;

            if(itemList !== null && itemList.length>0) {
                for(var i=0;i<itemList.length;i++) {
                    var item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.css("display" , "");

                    item.focus_div.css("display" , "none");
                    item.focus_text.css("display" , "none");

                    item.checked_img.css("display" , "none");

                    if(voiceFocusIndex === i) {
                        item.default_img.css("display" , "none");
                        item.default_text.css("display" , "none");

                        item.focus_div.css("display" , "");
                        item.focus_text.css("display" , "");

                        item.checked_img.css("display" , "");

                        item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");

                        focusVoiceDropDownIndex = i;
                    }
                }
            }
            voiceDropDown.root_div.css("display" , "");

            middleVoiceBtn.btn_root_div.css("display" , "none");
        }


        function _showBlueToothVoiceDropDown() {
            isShowbluetoothVoiceDropDown = true;

            var itemList = bluetoothVoiceDropDown.dropbox_item_list;

            if(itemList !== null && itemList.length>0) {
                for(var i=0;i<itemList.length;i++) {
                    var item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.css("display" , "");

                    item.focus_div.css("display" , "none");
                    item.focus_text.css("display" , "none");

                    item.checked_img.css("display" , "none");

                    if(blueToothVoiceFocusIndex === i) {
                        item.default_img.css("display" , "none");
                        item.default_text.css("display" , "none");

                        item.focus_div.css("display" , "");
                        item.focus_text.css("display" , "");

                        item.checked_img.css("display" , "");

                        item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");

                        focusbluetoothVoiceDropDownIndex = i;
                    }
                }
            }
            bluetoothVoiceDropDown.root_div.css("display" , "");

            middleBlueToothVoiceBtn.btn_root_div.css("display" , "none");
        }

        function _showBlueToothListeningVoiceDropDown() {
            isShowbluetoothListeningDropDown = true;

            var itemList = bluetoothListeningDropDown.dropbox_item_list;

            if(itemList !== null && itemList.length>0) {
                for(var i=0;i<itemList.length;i++) {
                    var item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.css("display" , "");

                    item.focus_div.css("display" , "none");
                    item.focus_text.css("display" , "none");

                    item.checked_img.css("display" , "none");

                    if(blueToothListeningFocusIndex === i) {
                        item.default_img.css("display" , "none");
                        item.default_text.css("display" , "none");

                        item.focus_div.css("display" , "");
                        item.focus_text.css("display" , "");

                        item.checked_img.css("display" , "");

                        item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");

                        focusbluetoothListeningDropDownIndex = i;
                    }
                }
            }
            bluetoothListeningDropDown.root_div.css("display" , "");

            middleBlueToothListeningBtn.btn_root_div.css("display" , "none");
        }

        function _hideVoiceDropDown() {
            if(isShowVoiceDropDown === true) {
                isShowVoiceDropDown = false;
                middleVoiceBtn.btn_root_div.css("display" , "");
                voiceDropDown.root_div.css("display" , "none");
            }
        }

        function _hideBlueToothVoiceDropDown() {
            if(isShowbluetoothVoiceDropDown === true) {
                isShowbluetoothVoiceDropDown = false;
                middleBlueToothVoiceBtn.btn_root_div.css("display" , "");
                bluetoothVoiceDropDown.root_div.css("display" , "none");
            }
        }

        function _hideBlueToothListeningDropDown() {
            if(isShowbluetoothListeningDropDown === true) {
                isShowbluetoothListeningDropDown = false;
                middleBlueToothListeningBtn.btn_root_div.css("display" , "");
                bluetoothListeningDropDown.root_div.css("display" , "none");
            }
        }

        function _updateData() {
            if(isShowCaption === true && isCaptionDimmed === true) {
                middleCaptionBtn.btn_root_div.css({opacity: 0.3});
            }


            if(isShowVoice === true && isVoiceDimmed === false) {
                middleVoiceBtn.btn_text.text(voiceList[voiceFocusIndex].langname);
            }else if(isShowVoice === true && isVoiceDimmed === true) {
                middleVoiceBtn.btn_root_div.css({opacity: 0.3});
            }

            if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                middleBlueToothVoiceBtn.btn_text.text(blueToothVoiceList[blueToothVoiceFocusIndex].langname);
            }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === true) {
                middleBlueToothVoiceBtn.btn_root_div.css({opacity: 0.3});
            }

            if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                middleBlueToothListeningBtn.btn_text.text(blueToothListeningList[blueToothListeningFocusIndex].langname);
            }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === true){
                middleBlueToothListeningBtn.btn_root_div.css({opacity: 0.3});
            }
        }

        function _focusMove(isUpKey) {
            if(focusBtn !== null) {
                focusBtn.btn_default_img.css("display" , "");
                focusBtn.btn_focus_div.css("display" , "none");

                focusBtn.btn_text.css("color", "rgba(255,255,255,1)");
                if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU &&
                    (focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU)) {
                    focusBtn.btn_down_img.attr("src", "images/popup/arw_option_popup_dw_f.png");
                }

                focusBtn.btn_text.removeClass("font_m");
                focusBtn.btn_text.addClass("font_l");
            }


            if(isUpKey) {
                if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU) {
                    if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL) {
                        if( isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if( isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;

                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;

                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;

                            focusBtn = middleVoiceBtn;
                        }else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;

                            focusBtn = middleCaptionBtn;
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL) {
                        focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;
                        focusBtn = topMenuBtnList[focusIndex];
                    } else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL) {
                        //[sw.nam] 2017.05.04  -> 오디오 채널일 경우 오디오 채널 연관메뉴 key navigation 로직 추가
                        focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                        focusBtn = topMenuBtnList[focusIndex];
                    }
                }else if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU) {
                    if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU) {
                        if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            //[sw.nam] 2017.05.04  -> 오디오 채널일 경우 오디오 채널 연관메뉴 key navigation 로직 추가
                            var currentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(currentChannel) === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL;
                            }else {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                            }
                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                            focusBtn = middleVoiceBtn;
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU) {
                        if(isShowCaption === true && isCaptionDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                            focusBtn = middleCaptionBtn;
                        }else if(isShowTopMenu === true){
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            //[sw.nam] 2017.05.04  -> 오디오 채널일 경우 오디오 채널 연관메뉴 key navigation 로직 추가
                            var currentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(currentChannel) === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL;
                            }else {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                            }
                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                            focusBtn = middleBlueToothVoiceBtn;
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU) {
                        if(isShowVoice === true && isVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                            focusBtn = middleVoiceBtn;
                        }
                        else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                            focusBtn = middleCaptionBtn;
                        }else if(isShowTopMenu === true){
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            //[sw.nam] 2017.05.04  -> 오디오 채널일 경우 오디오 채널 연관메뉴 key navigation 로직 추가
                            var currentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(currentChannel) === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL;
                            }else {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                            }
                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                            focusBtn = middleBlueToothListeningBtn;
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU) {
                        if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false){
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                            focusBtn = middleBlueToothVoiceBtn;
                        } else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                            focusBtn = middleVoiceBtn;
                        }
                        else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                            focusBtn = middleCaptionBtn;
                        }else if(isShowTopMenu === true){
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            //[sw.nam] 2017.05.04  -> 오디오 채널일 경우 오디오 채널 연관메뉴 key navigation 로직 추가
                            var currentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(currentChannel) === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL;
                            }else {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                            }
                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }
                    }
                }else if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU) {
                    if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK) {
                        if( isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;

                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;

                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;

                            focusBtn = middleVoiceBtn;
                        }else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;

                            focusBtn = middleCaptionBtn;
                        }else if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            var currentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(currentChannel) === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL;
                            }else {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                            }
                            focusBtn = topMenuBtnList[focusIndex];
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET){
                        focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;
                        focusBtn = bottomMenuBtnList[focusIndex];
                    }
                }
            }else {
                if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU) {
                    if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL) {
                        focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL;
                        focusBtn = topMenuBtnList[focusIndex];
                        //[sw.nam] 2017.05.04  -> 오디오 채널일 경우 오디오 채널 연관메뉴 key navigation 로직 추가
                    } else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL &&
                        KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel()) === true) {
                        log.printDbg("it's on Audio channel!");
                        focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL;
                        focusBtn = topMenuBtnList[focusIndex];
                    } else {
                        if(isShowCaption === true && isCaptionDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;

                            focusBtn = middleCaptionBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;

                            focusBtn = middleVoiceBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;

                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;

                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }
                    }

                }else if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU){
                    if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU) {
                        if(isShowVoice === true && isVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                            focusBtn = middleVoiceBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;

                            focusBtn = topMenuBtnList[focusIndex];
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU) {
                        if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;

                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                            focusBtn = middleCaptionBtn;
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU) {
                        if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;
                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;

                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                            focusBtn = middleCaptionBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                            focusBtn = middleVoiceBtn;
                        }
                    }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU) {
                        if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }else if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;

                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;
                            focusBtn = middleCaptionBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;
                            focusBtn = middleVoiceBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;
                            focusBtn = middleBlueToothVoiceBtn;
                        }
                    }
                }else if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU){
                    if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK){
                        focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;
                        focusBtn = bottomMenuBtnList[focusIndex];
                    }else {
                        if(isShowTopMenu === true) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL;

                            focusBtn = topMenuBtnList[focusIndex];
                        }else if(isShowCaption === true && isCaptionDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU;

                            focusBtn = middleCaptionBtn;
                        }else if(isShowVoice === true && isVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU;

                            focusBtn = middleVoiceBtn;
                        }else if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU;

                            focusBtn = middleBlueToothVoiceBtn;
                        }else if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU;

                            focusBtn = middleBlueToothListeningBtn;
                        }else if(isShowBottomMenu === true ) {
                            focusMenu = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU;
                            focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET;

                            focusBtn = bottomMenuBtnList[focusIndex];
                        }
                    }
                }
            }

            if(focusBtn !== null) {
                focusBtn.btn_default_img.css("display" , "none");
                focusBtn.btn_focus_div.css("display" , "");
                focusBtn.btn_text.css("color", "rgba(255,255,255,1)");
                if(focusMenu === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU
                    &&(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU
                    || focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU)) {
                    focusBtn.btn_down_img.attr("src", "images/popup/arw_option_popup_dw_f.png");
                }

                focusBtn.btn_text.removeClass("font_l");
                focusBtn.btn_text.addClass("font_m");
            }
        }


        function _hideAll() {
            log.printDbg("_hideAll()");
        }

        function _createElement() {
            log.printDbg("_createElement()");
            var startPosY = 87;
            var voicedropDownStartY =0;
            var bluetoothvoicedropDownStartY = 0;
            var bluetoothlisteningdropDownStartY = 0;


            if(isShowTopMenu === true) {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "channel_genre_title",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 35 , top: 52 , width: 170, height: 26,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24 , "text-align": "left", "letter-spacing":-1.2
                        }
                    },
                    text: "편성표 바로가기",
                    parent: div
                });
                if(topMenuList !== undefined && topMenuList !== null && topMenuList.length>0)  {
                    for(var i=0;i<topMenuList.length;i++) {
                        var btnDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "top_menu_btn_" + (i+1),
                                css: {
                                    position: "absolute",
                                    left: 35, top: startPosY, overflow:"visible"
                                }
                            },
                            parent: div
                        });

                        var defaultImg = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "top_menu_btn_default" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 0, width:300, height: 65,
                                    display: "" ,
                                    "background-color": "rgba(200,200,200,1)",
                                    opacity:0.3
                                }
                            },
                            parent: btnDiv
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "" ,
                                css: {
                                    position: "absolute",
                                    left: 2, top: 2, width:296, height: 61,
                                    "background-color": "rgba(160,160,160,1)",
                                    opacity:1
                                }
                            },
                            parent: defaultImg
                        });


                        var btnFocusDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "top_menu_btn_focus" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 0, width:300, height: 65,
                                    "background-color": "rgba(210,51,47,1)",
                                    display: "none"
                                }
                            },
                            parent: btnDiv
                        });

                        var btnText = util.makeElement({
                            tag: "<span />",
                            attrs: {
                                id: "top_menu_btn_text",
                                class: "font_l",
                                css: {
                                    position: "absolute", left: 0 , top: 19 , width: 300, height: 32,
                                    color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center", "letter-spacing":-1.5
                                }
                            },
                            text: topMenuList[i].name,
                            parent: btnDiv
                        });

                        var btn = {
                            btn_root_div : btnDiv,
                            btn_default_img : defaultImg,
                            btn_focus_div : btnFocusDiv,
                            btn_text : btnText
                        } ;
                        topMenuBtnList[topMenuBtnList.length] = btn;
                        startPosY+=65;

                        startPosY+=9;
                    }

                    startPosY-=9;

                    if(isShowCaption || isShowVoice || isShowBlueToothVoice || isShowBlueToothListening) {
                        var tempDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "",
                                css: {
                                    position: "absolute",
                                    left: 35, top: startPosY, width:300, height: 88
                                }
                            },
                            parent: div
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 43, width:300, height: 2,
                                    "background-color": "rgba(255,255,255,0.1)"
                                }
                            },
                            parent: tempDiv
                        });

                        startPosY+=88;
                    }
                }

            }

            if(isShowCaption === true) {
                var btnDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_caption_menu_btn",
                        css: {
                            position: "absolute",
                            left: 35, top: startPosY, overflow:"visible"
                        }
                    },
                    parent: div
                });

                var defaultImg = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_caption_menu_default" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            display: "" ,
                            "background-color": "rgba(200,200,200,1)",
                            opacity:0.3
                        }
                    },
                    parent: btnDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "" ,
                        css: {
                            position: "absolute",
                            left: 2, top: 2, width:296, height: 61,
                            "background-color": "rgba(160,160,160,1)",
                            opacity:1
                        }
                    },
                    parent: defaultImg
                });



                var btnFocusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_caption_menu_focus" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            "background-color": "rgba(210,51,47,1)",
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });

                var btnText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "top_menu_btn_text",
                        class: "font_l",
                        css: {
                            position: "absolute", left: 0 , top: 19 , width: 300, height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center", "letter-spacing":-1.5
                        }
                    },
                    text: "자막 설정",
                    parent: btnDiv
                });

                var btn = {
                    btn_root_div : btnDiv,
                    btn_default_img : defaultImg,
                    btn_focus_div : btnFocusDiv,
                    btn_text : btnText
                } ;

                startPosY+=65;
                middleCaptionBtn = btn;
            }
            if(isShowVoice === true) {
                if(isShowCaption === true) {
                    startPosY+=39;
                }
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "voice_title",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 35 , top: startPosY , width: 100, height: 26,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24 , "text-align": "left", "letter-spacing":-1.2
                        }
                    },
                    text: "음성 변경",
                    parent: div
                });

                voicedropDownStartY=startPosY;

                startPosY+=36;

                var btnDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_voice_menu_btn",
                        css: {
                            position: "absolute",
                            left: 35, top: startPosY, overflow:"visible"
                        }
                    },
                    parent: div
                });

                var defaultImg = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_menu_voice_btn_default" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            display: "" ,
                            "background-color": "rgba(200,200,200,1)",
                            opacity:0.3
                        }
                    },
                    parent: btnDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "" ,
                        css: {
                            position: "absolute",
                            left: 2, top: 2, width:296, height: 61,
                            "background-color": "rgba(160,160,160,1)",
                            opacity:1
                        }
                    },
                    parent: defaultImg
                });



                var btnFocusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_menu_voice_btn_focus" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            "background-color": "rgba(210,51,47,1)",
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });

                var btnText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "middle_menu_voice_btn_text",
                        class: "font_l",
                        css: {
                            position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var downImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "middle_menu_voice_btn_down_img" ,
                        src: "images/popup/arw_option_popup_dw.png",
                        css: {
                            position: "absolute",
                            left: 238 ,
                            top: 10,
                            width: 45,
                            height: 45
                        }
                    },
                    parent: btnDiv
                });

                var btn = {
                    btn_root_div : btnDiv,
                    btn_default_img : defaultImg,
                    btn_focus_div : btnFocusDiv,
                    btn_text : btnText,
                    btn_down_img : downImg
                } ;

                middleVoiceBtn = btn;

                startPosY+=65;
            }
            if(isShowBlueToothVoice === true) {
                if(isShowCaption === true || isShowVoice === true) {
                    startPosY+=39;
                }
                var result = KTW.managers.device.DeviceManager.bluetoothDevice.checkKTBlueToothA2DPDevice();
                var titleString = result === true ? "이어폰 음성 변경" : "블루투스 음성 변경";
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "blue_tooth_voice_title",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 35 , top: startPosY , width: 190, height: 26,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24 , "text-align": "left", "letter-spacing":-1.2
                        }
                    },
                    text: titleString,
                    parent: div
                });

                bluetoothvoicedropDownStartY = startPosY;

                startPosY+=36;

                var btnDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_blue_tooth_voice_menu_btn",
                        css: {
                            position: "absolute",
                            left: 35, top: startPosY, overflow:"visible"
                        }
                    },
                    parent: div
                });

                var defaultImg = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_menu_blue_tooth_voice_btn_default" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            display: "" ,
                            "background-color": "rgba(200,200,200,1)",
                            opacity:0.3
                        }
                    },
                    parent: btnDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "" ,
                        css: {
                            position: "absolute",
                            left: 2, top: 2, width:296, height: 61,
                            "background-color": "rgba(160,160,160,1)",
                            opacity:1
                        }
                    },
                    parent: defaultImg
                });



                var btnFocusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_menu_blue_tooth_voice_btn_focus" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            "background-color": "rgba(210,51,47,1)",
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });

                var btnText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "middle_menu_blue_tooth_voice_btn_text",
                        class: "font_l",
                        css: {
                            position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var downImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "middle_menu_blue_tooth_voice_btn_down_img" ,
                        src: "images/popup/arw_option_popup_dw.png",
                        css: {
                            position: "absolute",
                            left: 238 ,
                            top: 10,
                            width: 45,
                            height: 45
                        }
                    },
                    parent: btnDiv
                });

                var btn = {
                    btn_root_div : btnDiv,
                    btn_default_img : defaultImg,
                    btn_focus_div : btnFocusDiv,
                    btn_text : btnText,
                    btn_down_img : downImg
                } ;


                middleBlueToothVoiceBtn = btn;

                startPosY+=65;
            }

            if(isShowBlueToothListening === true) {
                if(isShowCaption === true || isShowVoice === true || isShowBlueToothVoice === true) {
                    startPosY+=39;
                }
                var result = KTW.managers.device.DeviceManager.bluetoothDevice.checkKTBlueToothA2DPDevice();
                var titleString = ((result === true) ? "이어폰 음성 듣기" : "블루투스 음성 듣기");
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "blue_tooth_listening_title",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 35 , top: startPosY , width: 190, height: 26,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24 , "text-align": "left", "letter-spacing":-1.2
                        }
                    },
                    text: titleString,
                    parent: div
                });

                bluetoothlisteningdropDownStartY = startPosY;

                startPosY+=36;

                var btnDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_blue_tooth_listening_menu_btn",
                        css: {
                            position: "absolute",
                            left: 35, top: startPosY, overflow:"visible"
                        }
                    },
                    parent: div
                });

                var defaultImg = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_menu_blue_tooth_listening_btn_default" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            display: "" ,
                            "background-color": "rgba(200,200,200,1)",
                            opacity:0.3
                        }
                    },
                    parent: btnDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "" ,
                        css: {
                            position: "absolute",
                            left: 2, top: 2, width:296, height: 61,
                            "background-color": "rgba(160,160,160,1)",
                            opacity:1
                        }
                    },
                    parent: defaultImg
                });



                var btnFocusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "middle_menu_blue_tooth_listening_btn_focus" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            "background-color": "rgba(210,51,47,1)",
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });

                var btnText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "middle_menu_blue_tooth_listening_btn_text",
                        class: "font_l",
                        css: {
                            position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var downImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "middle_menu_blue_tooth_listening_btn_down_img" ,
                        src: "images/popup/arw_option_popup_dw.png",
                        css: {
                            position: "absolute",
                            left: 238 ,
                            top: 10,
                            width: 45,
                            height: 45
                        }
                    },
                    parent: btnDiv
                });

                var btn = {
                    btn_root_div : btnDiv,
                    btn_default_img : defaultImg,
                    btn_focus_div : btnFocusDiv,
                    btn_text : btnText,
                    btn_down_img : downImg
                } ;


                middleBlueToothListeningBtn = btn;
                startPosY+=65;
            }



            if(isShowBottomMenu === true) {
                if(isShowCaption || isShowVoice || isShowBlueToothVoice || isShowBlueToothListening ) {
                    var tempDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "",
                            css: {
                                position: "absolute",
                                left: 35, top: startPosY, width:300, height: 88
                            }
                        },
                        parent: div
                    });

                    util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "" ,
                            css: {
                                position: "absolute",
                                left: 0, top: 43, width:300, height: 2,
                                "background-color": "rgba(255,255,255,0.1)"
                            }
                        },
                        parent: tempDiv
                    });

                    startPosY+=88;
                }else {
                    if(isShowTopMenu === true) {
                        var tempDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "",
                                css: {
                                    position: "absolute",
                                    left: 35, top: startPosY, width:300, height: 88
                                }
                            },
                            parent: div
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 43, width:300, height: 2,
                                    "background-color": "rgba(255,255,255,0.1)"
                                }
                            },
                            parent: tempDiv
                        });

                        startPosY+=88;

                    }
                }

                if(bottomMenuList !== undefined && bottomMenuList !== null && bottomMenuList.length>0)  {
                    for(var i=0;i<bottomMenuList.length;i++) {
                        var btnDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "bottom_menu_btn_" + (i+1),
                                css: {
                                    position: "absolute",
                                    left: 35, top: startPosY, overflow:"visible"

                                }
                            },
                            parent: div
                        });

                        var defaultImg = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "bottom_menu_btn_default" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 0, width:300, height: 65,
                                    display: "" ,
                                    "background-color": "rgba(200,200,200,1)",
                                    opacity:0.3
                                }
                            },
                            parent: btnDiv
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "" ,
                                css: {
                                    position: "absolute",
                                    left: 2, top: 2, width:296, height: 61,
                                    "background-color": "rgba(160,160,160,1)",
                                    opacity:1
                                }
                            },
                            parent: defaultImg
                        });



                        var btnFocusDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "bottom_menu_btn_focus" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 0, width:300, height: 65,
                                    "background-color": "rgba(210,51,47,1)",
                                    display: "none"
                                }
                            },
                            parent: btnDiv
                        });

                        var btnText = util.makeElement({
                            tag: "<span />",
                            attrs: {
                                id: "bottom_menu_btn_text",
                                class: "font_l",
                                css: {
                                    position: "absolute", left: 0 , top: 19 , width: 300, height: 32,
                                    color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center", "letter-spacing":-1.5
                                }
                            },
                            text: bottomMenuList[i].name,
                            parent: btnDiv
                        });

                        var btn = {
                            btn_root_div : btnDiv,
                            btn_default_img : defaultImg,
                            btn_focus_div : btnFocusDiv,
                            btn_text : btnText
                        } ;
                        bottomMenuBtnList[bottomMenuBtnList.length] = btn;
                        startPosY+=65;
                        startPosY+=9;
                    }
                }
            }



            if(isShowVoice === true && isVoiceDimmed === false && voiceList.length>1) {
                var middlecount = voiceList.length - 2;
                if(middlecount<0) {
                    middlecount = 0;
                }

                var dropDownDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "voice_dropdown_div",
                        css: {
                            position: "absolute",
                            left: -5, top: (voicedropDownStartY - 1) , width:384 , height : 102 + 110 +  65*middlecount
                        }
                    },
                    parent: div
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_top_img" ,
                        src: "images/popup/sdw_option_popup_top.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 384,
                            height: 102
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_middle_img" ,
                        src: "images/popup/sdw_option_popup_m.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102,
                            width: 384,
                            height: 65*middlecount
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_btm_img" ,
                        src: "images/popup/sdw_option_popup_btm.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102+65*middlecount,
                            width: 384,
                            height: 110
                        }
                    },
                    parent: dropDownDiv
                });

                var dropboxItemList = [];
                var tempY = 37;
                for(var x=0;x<voiceList.length;x++) {
                    var btnDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "drop_box_btn_" + (x+1),
                            css: {
                                position: "absolute",
                                left: 40, top: tempY, width:300, height: 65
                            }
                        },
                        parent: dropDownDiv
                    });


                    var defaultImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: x === 0 ? "images/popup/bg_option_dropbox.png" : "images/popup/bg_option_dropbox_2.png",
                            css: {
                                position: "absolute",
                                left: 0 ,
                                top: 0,
                                width: 300,
                                height: 65
                            }
                        },
                        parent: btnDiv
                    });

                    var btnFocusDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "" ,
                            css: {
                                position: "absolute",
                                left: 0, top: 0, width:300, height: 65,
                                "background-color": "rgba(210,51,47,1)",
                                display: "none"
                            }
                        },
                        parent: btnDiv
                    });


                    var defaultText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_l",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                            }
                        },
                        text: voiceList[x].langname,
                        parent: btnDiv
                    });

                    var focusText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_m",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                            }
                        },
                        text: voiceList[x].langname,
                        parent: btnDiv
                    });


                    var checkedImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: "images/popup/option_dropbox_select.png",
                            css: {
                                position: "absolute",
                                left: 238 ,
                                top: 10,
                                width: 45,
                                height: 45
                            }
                        },
                        parent: btnDiv
                    });

                    var dropboxItem = {
                        root_div : btnDiv,
                        default_img : defaultImg,
                        focus_div : btnFocusDiv,
                        default_text : defaultText,
                        focus_text : focusText ,
                        checked_img : checkedImg
                    };

                    dropboxItemList[dropboxItemList.length] = dropboxItem;

                    tempY+=65;
                }
                voiceDropDown = {
                    root_div : dropDownDiv,
                    dropbox_item_list :   dropboxItemList
                };

                voiceDropDown.root_div.css("display" , "none");
            }


            if(isShowBlueToothVoice === true && isBlueToothVoiceDimmed === false && blueToothVoiceList.length>1) {
                var middlecount = blueToothVoiceList.length - 2;
                if(middlecount<0) {
                    middlecount = 0;
                }

                var dropDownDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "bluetooth_voice_dropdown_div",
                        css: {
                            position: "absolute",
                            left: -5, top: (bluetoothvoicedropDownStartY - 1) , width:384 , height : 102 + 110 +  65*middlecount
                        }
                    },
                    parent: div
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_top_img" ,
                        src: "images/popup/sdw_option_popup_top.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 384,
                            height: 102
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_middle_img" ,
                        src: "images/popup/sdw_option_popup_m.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102,
                            width: 384,
                            height: 65*middlecount
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_btm_img" ,
                        src: "images/popup/sdw_option_popup_btm.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102+65*middlecount,
                            width: 384,
                            height: 110
                        }
                    },
                    parent: dropDownDiv
                });

                var dropboxItemList = [];
                var tempY = 37;
                for(var x=0;x<blueToothVoiceList.length;x++) {
                    var btnDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "drop_box_btn_" + (x+1),
                            css: {
                                position: "absolute",
                                left: 40, top: tempY, width:300, height: 65
                            }
                        },
                        parent: dropDownDiv
                    })


                    var defaultImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: x === 0 ? "images/popup/bg_option_dropbox.png" : "images/popup/bg_option_dropbox_2.png",
                            css: {
                                position: "absolute",
                                left: 0 ,
                                top: 0,
                                width: 300,
                                height: 65
                            }
                        },
                        parent: btnDiv
                    });

                    var btnFocusDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "" ,
                            css: {
                                position: "absolute",
                                left: 0, top: 0, width:300, height: 65,
                                "background-color": "rgba(210,51,47,1)",
                                display: "none"
                            }
                        },
                        parent: btnDiv
                    });


                    var defaultText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_l",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                            }
                        },
                        text: blueToothVoiceList[x].langname,
                        parent: btnDiv
                    });

                    var focusText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_m",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                            }
                        },
                        text: blueToothVoiceList[x].langname,
                        parent: btnDiv
                    });


                    var checkedImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: "images/popup/option_dropbox_select.png",
                            css: {
                                position: "absolute",
                                left: 238 ,
                                top: 10,
                                width: 45,
                                height: 45
                            }
                        },
                        parent: btnDiv
                    });

                    var dropboxItem = {
                        root_div : btnDiv,
                        default_img : defaultImg,
                        focus_div : btnFocusDiv,
                        default_text : defaultText,
                        focus_text : focusText ,
                        checked_img : checkedImg
                    };

                    dropboxItemList[dropboxItemList.length] = dropboxItem;

                    tempY+=65;
                }
                bluetoothVoiceDropDown = {
                    root_div : dropDownDiv,
                    dropbox_item_list :   dropboxItemList
                };

                bluetoothVoiceDropDown.root_div.css("display" , "none");
            }

            if(isShowBlueToothListening === true && isBlueToothListeningDimmed === false && blueToothListeningList.length>1) {
                var middlecount = blueToothListeningList.length - 2;
                if(middlecount<0) {
                    middlecount = 0;
                }

                var dropDownDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "bluetooth_listening_dropdown_div",
                        css: {
                            position: "absolute",
                            left: -5, top:(bluetoothlisteningdropDownStartY-1) , width:384 , height : 102 + 110 +  65*middlecount
                        }
                    },
                    parent: div
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_top_img" ,
                        src: "images/popup/sdw_option_popup_top.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 384,
                            height: 102
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_middle_img" ,
                        src: "images/popup/sdw_option_popup_m.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102,
                            width: 384,
                            height: 65*middlecount
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_btm_img" ,
                        src: "images/popup/sdw_option_popup_btm.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102+65*middlecount,
                            width: 384,
                            height: 110
                        }
                    },
                    parent: dropDownDiv
                });

                var dropboxItemList = [];
                var tempY = 37;
                for(var x=0;x<blueToothListeningList.length;x++) {
                    var btnDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "drop_box_btn_" + (x+1),
                            css: {
                                position: "absolute",
                                left: 40, top: tempY, width:300, height: 65
                            }
                        },
                        parent: dropDownDiv
                    })


                    var defaultImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: x === 0 ? "images/popup/bg_option_dropbox.png" : "images/popup/bg_option_dropbox_2.png",
                            css: {
                                position: "absolute",
                                left: 0 ,
                                top: 0,
                                width: 300,
                                height: 65
                            }
                        },
                        parent: btnDiv
                    });

                    var btnFocusDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "" ,
                            css: {
                                position: "absolute",
                                left: 0, top: 0, width:300, height: 65,
                                "background-color": "rgba(210,51,47,1)",
                                display: "none"
                            }
                        },
                        parent: btnDiv
                    });


                    var defaultText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_l",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                            }
                        },
                        text: blueToothListeningList[x].langname,
                        parent: btnDiv
                    });

                    var focusText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_m",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 300, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                            }
                        },
                        text: blueToothListeningList[x].langname,
                        parent: btnDiv
                    });


                    var checkedImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: "images/popup/option_dropbox_select.png",
                            css: {
                                position: "absolute",
                                left: 238 ,
                                top: 10,
                                width: 45,
                                height: 45
                            }
                        },
                        parent: btnDiv
                    });

                    var dropboxItem = {
                        root_div : btnDiv,
                        default_img : defaultImg,
                        focus_div : btnFocusDiv,
                        default_text : defaultText,
                        focus_text : focusText ,
                        checked_img : checkedImg
                    };

                    dropboxItemList[dropboxItemList.length] = dropboxItem;

                    tempY+=65;
                }
                bluetoothListeningDropDown = {
                    root_div : dropDownDiv,
                    dropbox_item_list :   dropboxItemList
                };

                bluetoothListeningDropDown.root_div.css("display" , "none");
            }
        }
    }
})();