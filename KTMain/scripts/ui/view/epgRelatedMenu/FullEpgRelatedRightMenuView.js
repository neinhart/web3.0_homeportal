/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>FullEpgRelatedRightMenuView</code>
 *
 * 채널 미니 가이드 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */
"use strict";
(function() {
    KTW.ui.view.FullEpgRelatedRightMenuView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;


        var parent = parent;
        var div = null;

        var topGenre = null;
        var middleGenre = null;
        var bottomGenre = null;
        var focusGenre = -1;
        var focusIndex = -1;
        var callbackFunc = null;

        var topGenreCount = 0;
        var middleGenreCount = 0;
        var bottomGenreCount = 0;
        var pageTotalCount = 0;

        var topGenreStartPage = -1;
        var middleGenreStartPage = -1;
        var bottomGenreStartPage = -1;

        var topGenreStartIndex = -1;
        var middleGenreStartIndex = -1;
        var bottomGenreStartIndex = -1;

        var focusIndexByPage = -1;

        var arrayPage = [];

        var showPageIndex = -1;

        var moveIndexByPage = -1;

        var isHideAction = false;

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        this.create = function () {
            log.printDbg("create()");
        };

        this.getDiv = function () {
            return div;
        };

        this.show = function (options) {
            log.printDbg("show()");

            if (options !== undefined && options !== null) {
                topGenre = options.top_genre;
                middleGenre = options.middle_genrc;
                bottomGenre = options.bottom_genrc;
                focusGenre = options.focus_genrc;
                focusIndex = options.focus_index;
                callbackFunc = options.fullepg_callback;

                var total = 0;
                if(topGenre !== undefined && topGenre !== null) {
                    topGenreCount = topGenre.length;
                    if(topGenreCount>0) {
                        total+=topGenreCount;
                    }
                }

                if(middleGenre !== undefined && middleGenre !== null) {
                    middleGenreCount = middleGenre.length;
                    if(middleGenreCount>0) {
                        total+=middleGenreCount;
                        if(topGenreCount>0) {
                            total+=1;
                        }
                    }
                }

                if(bottomGenre !== undefined && bottomGenre !== null) {
                    bottomGenreCount = bottomGenre.length;
                    if(bottomGenreCount>0) {
                        total += bottomGenreCount;
                        if(topGenreCount>0 || middleGenreCount>0) {
                            total += 1;
                        }
                    }
                }

                pageTotalCount = Math.floor(total / 12);

                var temp = total % 12;
                if(temp>0) {
                    pageTotalCount++;
                }
            }

            isHideAction = false;
            _createElement();

            if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.TOP_GENRE) {
                showPageIndex = topGenreStartPage;
            }else if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.MIDDLE_GENRE) {
                showPageIndex = middleGenreStartPage;
                var temp = 11 - topGenreCount;
                if(focusIndex>=temp) {
                    showPageIndex = bottomGenreStartPage;
                }
            }else if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.BOTTOM_GENRE) {
                showPageIndex = bottomGenreStartPage;
            }
            arrayPage[showPageIndex].pageDiv.css("display","");
        };

        this.hide = function () {
            log.printDbg("hide()");

            _hideAll();

            div.css("display", "none");
        };

        this.focus = function () {
            log.printDbg("focus()");

            var btnchild = arrayPage[showPageIndex].btn_child;

            btnchild[focusIndexByPage].btn_default_img.css("display" , "none");
            var isNow = btnchild[focusIndexByPage].is_now;
            if(isNow !== undefined && isNow !== null) {
                btnchild[focusIndexByPage].btn_now_img.attr("src", "images/popup/option_dropbox_select_f.png");
            }

            btnchild[focusIndexByPage].btn_focus_div.css("display" , "");

            btnchild[focusIndexByPage].btn_text.removeClass("font_l");
            btnchild[focusIndexByPage].btn_text.css("color", "rgba(255,255,255,1)");
            btnchild[focusIndexByPage].btn_text.addClass("font_m");

            moveIndexByPage = focusIndexByPage;
        };

        this.blur = function () {
            log.printDbg("blur()");
            var btnchild = arrayPage[showPageIndex].btn_child;

            var isNow = btnchild[moveIndexByPage].is_now;
            btnchild[moveIndexByPage].btn_default_img.css("display" , "");
            if(isNow !== undefined && isNow !== null) {
                btnchild[moveIndexByPage].btn_now_img.attr("src", "images/popup/option_dropbox_select.png");
            }
            btnchild[moveIndexByPage].btn_focus_div.css("display" , "none");
            btnchild[moveIndexByPage].btn_text.css("color", "rgba(255,255,255,1)");
            btnchild[moveIndexByPage].btn_text.removeClass("font_m");
            btnchild[moveIndexByPage].btn_text.addClass("font_l");
            focusIndexByPage = moveIndexByPage;

            // arrayPage[showPageIndex].pageDiv.css("display","none");
            //
            // if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.TOP_GENRE) {
            //     showPageIndex = topGenreStartPage;
            // }else if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.MIDDLE_GENRE) {
            //     showPageIndex = middleGenreStartPage;
            //     var temp = 11 - topGenreCount;
            //     if(focusIndex>=temp) {
            //         showPageIndex = bottomGenreStartPage;
            //     }
            // }else if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.BOTTOM_GENRE) {
            //     showPageIndex = bottomGenreStartPage;
            // }
            // arrayPage[showPageIndex].pageDiv.css("display","");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            if(isHideAction === true) {
                return consumed;
            }

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    _focusMove(true);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    _focusMove(false);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    var btnchild = arrayPage[showPageIndex].btn_child;
                    var menuId = btnchild[moveIndexByPage].menuid;
                    if (menuId === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL && !KTW.managers.service.FavoriteChannelManager.isFavoriteChannelListExist()) {
                        parent.showFavEmptyPopup();

                    }else {
                        parent.hideView(null , null , null , callbackFunc , menuId , null , null , null);
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    break;
            }

            return consumed;
        };

        function _focusMove(isUpKey) {

            var btnchild = arrayPage[showPageIndex].btn_child;

            var isNow = btnchild[moveIndexByPage].is_now;
            if(isNow !== undefined && isNow !== null) {
                btnchild[moveIndexByPage].btn_now_img.attr("src", "images/popup/option_dropbox_select.png");
            }
            btnchild[moveIndexByPage].btn_default_img.css("display" , "");

            btnchild[moveIndexByPage].btn_focus_div.css("display" , "none");
            btnchild[moveIndexByPage].btn_text.css("color", "rgba(255,255,255,1)");

            btnchild[moveIndexByPage].btn_text.removeClass("font_m");
            btnchild[moveIndexByPage].btn_text.addClass("font_l");

            if(isUpKey === true) {
                moveIndexByPage--;
                if(moveIndexByPage>=0 && btnchild[moveIndexByPage].genre_type === -1) {
                    moveIndexByPage--;
                }
            }else {
                moveIndexByPage++;
                if(moveIndexByPage<12) {
                    if(btnchild[moveIndexByPage].genre_type === undefined) {
                        moveIndexByPage=12;
                    }else {
                        if(btnchild[moveIndexByPage].genre_type === -1) {
                            moveIndexByPage++;
                        }
                    }
                }
            }

            if(moveIndexByPage<0) {
                arrayPage[showPageIndex].pageDiv.css("display","none");

                showPageIndex--;
                if(showPageIndex<0) {
                    showPageIndex = pageTotalCount-1;
                }

                arrayPage[showPageIndex].pageDiv.css("display","");

                btnchild = arrayPage[showPageIndex].btn_child;

                for(var x=btnchild.length-1;x>=0;x--) {
                    if(btnchild[x].genre_type !== undefined) {
                        if(btnchild[x].genre_type !== -1) {
                            moveIndexByPage = x;
                            break;
                        }
                    }
                }
            }else if(moveIndexByPage>=12) {
                arrayPage[showPageIndex].pageDiv.css("display","none");

                showPageIndex++;
                if(pageTotalCount<=showPageIndex) {
                    showPageIndex = 0;
                }
                arrayPage[showPageIndex].pageDiv.css("display","");

                btnchild = arrayPage[showPageIndex].btn_child;

                for(var x=0;x<btnchild.length;x++) {
                    if(btnchild[x].genre_type !== -1) {
                        moveIndexByPage = x;
                        break;
                    }
                }
            }

            btnchild = arrayPage[showPageIndex].btn_child;

            btnchild[moveIndexByPage].btn_default_img.css("display" , "none");

            var isNow = btnchild[moveIndexByPage].is_now;
            if(isNow !== undefined && isNow !== null) {
                btnchild[moveIndexByPage].btn_now_img.attr("src", "images/popup/option_dropbox_select_f.png");
            }
            btnchild[moveIndexByPage].btn_focus_div.css("display" , "");

            btnchild[moveIndexByPage].btn_text.css("color", "rgba(255,255,255,1)");

            btnchild[moveIndexByPage].btn_text.removeClass("font_l");
            btnchild[moveIndexByPage].btn_text.addClass("font_m");
        }


        function _hideAll() {
            log.printDbg("_hideAll()");
        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");
            arrayPage[0].pageDiv.css("display","");
        }



        function _createElement() {
            log.printDbg("_createElement()");

            for(var i=0;i<pageTotalCount;i++) {
                _createPageDiv(i);
            }
            _updateUI();
        }

        function _createPageDiv(pageIndex) {
            log.printDbg("_createPageDiv() pageIndex : " + pageIndex);
            var startPosY = 87;

            var pageRootDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "page"+ (pageIndex+1) + "_div",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:381, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        display:"none"
                    }
                },
                parent: div
            });

            var btnChild = [];

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "page" + (pageIndex+1) + "_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 35 , top: 52 , width: 250, height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24 , "text-align": "left" , "letter-spacing":-1.2
                    }
                },
                text: pageIndex === 0 ? "채널 편성표" : (KTW.CONSTANT.IS_OTS === true ? "장르별 프로그램" : "장르별 채널"),
                parent: pageRootDiv
            });

            if(pageIndex === 0) {
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "page" + (pageIndex + 1) + "_down_img",
                        src: "images/popup/arw_related_dw.png",
                        css: {
                            position: "absolute",
                            left: 161,
                            top: 1001,
                            width: 36,
                            height: 21
                        }
                    },
                    parent: pageRootDiv
                });
            }

            if(pageIndex !== 0) {
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "page_up_img",
                        src: "images/popup/arw_related_up.png",
                        css: {
                            position: "absolute",
                            left: 161 ,
                            top: 85,
                            width: 36,
                            height: 21
                        }
                    },
                    parent: pageRootDiv
                });

                startPosY = 126;
            }


            for(var x=0;x<12;x++) {
                var btnDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "page" + (pageIndex+1) + "_btn_" + (x+1),
                        css: {
                            position: "absolute",
                            left: 35, top: startPosY, overflow:"visible",
                            display:"none"
                        }
                    },
                    parent: pageRootDiv
                });

                var defaultImg = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "page" + (pageIndex+1) + "_btn_" + (x+1) + "_default" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            display: "none" ,
                            "background-color": "rgba(200,200,200,1)",
                            opacity:0.3
                        }
                    },
                    parent: btnDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "" ,
                        css: {
                            position: "absolute",
                            left: 2, top: 2, width:296, height: 61,
                            "background-color": "rgba(159,159,159,1)",
                            opacity:1
                        }
                    },
                    parent: defaultImg
                });

                var btnFocusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "page" + (pageIndex+1) + "_btn_" + (x+1) + "_focus" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:300, height: 65,
                            "background-color": "rgba(210,51,47,1)",
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });

                var nowImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "page" + (pageIndex+1) + "_btn_" + (x+1) + "_now" ,
                        src: "images/popup/option_dropbox_select.png",
                        css: {
                            position: "absolute",
                            left: 251 ,
                            top: 10,
                            width: 45,
                            height: 45,
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });


                var btnText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "page" + (pageIndex+1) + "_btn_" + (x+1) + "_text",
                        class: "font_l",
                        css: {
                            position: "absolute", left: 0 , top: 17 , width: 300, height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center", "letter-spacing":-1.5,
                            display:"none"
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var lineBarDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "page" + (pageIndex+1) + "_btn_" + (x+1) + "_line" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 31, width:299, height: 2,
                            "background-color": "rgba(255,255,255,0.1)",
                            display: "none"
                        }
                    },
                    parent: btnDiv
                });

                var titleText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 0 , top: 58 , width: 250, height: 26,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24 , "text-align": "left", "letter-spacing":-1.2
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var btn = {
                    btn_startY : startPosY,
                    btn_root_div : btnDiv,
                    btn_default_img : defaultImg,
                    btn_now_img : nowImg,
                    btn_focus_div : btnFocusDiv,
                    btn_text : btnText,
                    line_bar : lineBarDiv,
                    title_text : titleText
                } ;
                btnChild[btnChild.length] = btn;

                startPosY+=65;
                startPosY+=9;
            }

            var page = {
                pageDiv:pageRootDiv,
                btn_child:btnChild
            };

            arrayPage[pageIndex] = page;
        }

        function _updateUI(){
            log.printDbg("_updateUI()");
            var startPageIndex=0;
            var btnCount=0;
            var page = null;

            /**
             * Top Genre UI Update
             */
            if(topGenreCount>0) {
                topGenreStartPage = startPageIndex;
                topGenreStartIndex = btnCount;
                var btnchild = arrayPage[startPageIndex].btn_child;
                for(var x=0;x<topGenreCount;x++) {

                    btnchild[btnCount].genre_type = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.TOP_GENRE;
                    btnchild[btnCount].menuid = topGenre[x].menuid;
                    btnchild[btnCount].dataIndex = x;

                    btnchild[btnCount].btn_default_img.css("display" , "");
                    btnchild[btnCount].btn_text.text(topGenre[x].name);

                    btnchild[btnCount].btn_text.css("display" , "");

                    /**
                     * 연관메뉴를 호출한 장르별 채널 여부 체크
                     */
                    if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.TOP_GENRE
                    && x === focusIndex) {

                        var width = KTW.utils.util.getTextLength(topGenre[x].name , "RixHead L" ,  30 , -1.5);
                        if(width>227) {
                            btnchild[btnCount].btn_text.css("font-size" , 28);
                            btnchild[btnCount].btn_text.css("letter-spacing" , -1.4);

                            btnchild[btnCount].btn_text.css({top:19});
                            width = KTW.utils.util.getTextLength(topGenre[x].name , "RixHead L" ,  28 , -1.4);
                        }

                        /**
                         * 11px는 체크이미지의 우측 여백이 존재하여 그 부분을 계산상 제외시킴.
                         */
                        var totalwidth = width + 45 - 11;
                        var margin = (300 - totalwidth) / 2;

                        btnchild[btnCount].btn_text.css({left:margin});
                        btnchild[btnCount].btn_text.css({width:width});
                        btnchild[btnCount].btn_now_img.css({left:(margin+width)});
                        btnchild[btnCount].btn_default_img.children().css("background-color", "rgba(102,102,102,1)");

                        btnchild[btnCount].btn_now_img.css("display" , "");
                        btnchild[btnCount].is_now = true;
                        focusIndexByPage = btnCount;
                    }


                    btnchild[btnCount].btn_root_div.css("display" , "");
                    btnCount++;

                    /**
                     * 한페이지가 넘어 간 경우 페이지 이동함.
                     */
                    if(btnCount>=12) {
                        startPageIndex++;
                        btnCount = 0;
                        btnchild = arrayPage[startPageIndex].btn_child;
                    }
                }

                /**
                 * 페이지에서 마지막 부분에 라인 추가함.
                 */
                if(middleGenreCount>0 || bottomGenreCount>0) {
                    var tempStartY = btnchild[btnCount].btn_startY;
                    tempStartY-=9;

                    btnchild[btnCount].btn_root_div.css({top:tempStartY});

                    btnchild[btnCount].btn_startY = tempStartY;

                    btnchild[btnCount].btn_root_div.css({height:92});
                    btnchild[btnCount].line_bar.css("display" , "");
                    btnchild[btnCount].btn_root_div.css("display" , "");

                    btnchild[btnCount].genre_type = -1;
                    btnchild[btnCount].dataIndex = -1;

                    btnchild[btnCount].title_text.text(KTW.CONSTANT.IS_OTS === true ? "장르별 프로그램" : "장르별 채널");

                    tempStartY+=92;

                    btnCount++;

                    if(btnCount>=btnchild.length) {
                        startPageIndex++;
                        btnCount = 0;
                    }else {
                        for(var j=btnCount;j<btnchild.length;j++) {
                            btnchild[j].btn_root_div.css({top:tempStartY});
                            btnchild[j].btn_startY = tempStartY;
                            tempStartY+=65;
                            tempStartY+=9;
                        }
                    }
                }
            }

            /**
             * Middle Genre UI Update
             */
            if(middleGenreCount>0) {
                middleGenreStartPage = startPageIndex;
                middleGenreStartIndex = btnCount;

                var btnchild = arrayPage[startPageIndex].btn_child;
                for(var x=0;x<middleGenreCount;x++) {
                    btnchild[btnCount].btn_default_img.css("display" , "");
                    btnchild[btnCount].btn_text.text(middleGenre[x].name);

                    btnchild[btnCount].btn_text.css("display" , "");

                    btnchild[btnCount].genre_type = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.MIDDLE_GENRE;
                    btnchild[btnCount].menuid = middleGenre[x].menuid;
                    btnchild[btnCount].dataIndex = x;

                    /**
                     * 연관메뉴를 호출한 장르별 채널 여부 체크
                     */
                    if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.MIDDLE_GENRE
                        && x === focusIndex) {
                        btnchild[btnCount].btn_default_img.children().css("background-color", "rgba(102,102,102,1)");

                        var width = KTW.utils.util.getTextLength(middleGenre[x].name , "RixHead L" ,  30 , -1.5);
                        if(width>227) {
                            btnchild[btnCount].btn_text.css("font-size" , 28);
                            btnchild[btnCount].btn_text.css("letter-spacing" , -1.4);
                            btnchild[btnCount].btn_text.css({top:19});

                            width = KTW.utils.util.getTextLength(middleGenre[x].name , "RixHead L" ,  28 , -1.4);
                        }
                        var totalwidth = width + 45 - 11;
                        var margin = (300 - totalwidth) / 2;

                        btnchild[btnCount].btn_text.css({left:margin});
                        btnchild[btnCount].btn_text.css({width:width});
                        btnchild[btnCount].btn_now_img.css({left:(margin+width)});

                        btnchild[btnCount].btn_now_img.css("display" , "");
                        btnchild[btnCount].btn_default_img.children().css("background-color", "rgba(102,102,102,1)");

                        btnchild[btnCount].is_now = true;

                        focusIndexByPage = btnCount;
                    }

                    btnchild[btnCount].btn_root_div.css("display" , "");
                    btnCount++;
                    if(btnCount>=12) {
                        startPageIndex++;
                        btnCount = 0;
                        btnchild = arrayPage[startPageIndex].btn_child;
                    }
                }

                if(bottomGenreCount>0) {
                    var tempStartY = btnchild[btnCount].btn_startY;
                    tempStartY-=9;

                    btnchild[btnCount].btn_root_div.css({top:tempStartY});

                    btnchild[btnCount].btn_startY = tempStartY;

                    btnchild[btnCount].btn_root_div.css({height:92});
                    btnchild[btnCount].line_bar.css("display" , "");
                    btnchild[btnCount].btn_root_div.css("display" , "");

                    btnchild[btnCount].genre_type = -1;
                    btnchild[btnCount].dataIndex = -1;

                    btnchild[btnCount].title_text.text("TV APP 채널");

                    tempStartY+=92;

                    btnCount++;

                    if(btnCount>=btnchild.length) {
                        startPageIndex++;
                        btnCount = 0;
                    }else {
                        for(var j=btnCount;j<btnchild.length;j++) {
                            btnchild[j].btn_root_div.css({top:tempStartY});
                            btnchild[j].btn_startY = tempStartY;
                            tempStartY+=65;
                            tempStartY+=9;
                        }
                    }
                }
            }

            /**
             * Bottom Genre UI Update
             */
            if(bottomGenreCount>0) {
                bottomGenreStartPage = startPageIndex;
                bottomGenreStartIndex = btnCount;

                var btnchild = arrayPage[startPageIndex].btn_child;
                for(var x=0;x<bottomGenreCount;x++) {
                    btnchild[btnCount].btn_default_img.css("display" , "");
                    btnchild[btnCount].btn_text.text(bottomGenre[x].name);

                    btnchild[btnCount].btn_text.css("display" , "");

                    btnchild[btnCount].genre_type = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.BOTTOM_GENRE;
                    btnchild[btnCount].menuid = bottomGenre[x].menuid;
                    btnchild[btnCount].dataIndex = x;

                    /**
                     * 연관메뉴를 호출한 장르별 채널 여부 체크
                     */
                    if(focusGenre === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.FULL_EPG.BOTTOM_GENRE
                        && x === focusIndex) {

                        var width = KTW.utils.util.getTextLength(bottomGenre[x].name , "RixHead L" ,  30 , -1.5);
                        if(width>227) {
                            btnchild[btnCount].btn_text.css("font-size" , 28);
                            btnchild[btnCount].btn_text.css("letter-spacing" , -1.4);
                            btnchild[btnCount].btn_text.css({top:19});
                            width = KTW.utils.util.getTextLength(bottomGenre[x].name , "RixHead L" ,  28 , -1.4);
                        }

                        var totalwidth = width + 45 - 11;
                        var margin = (300 - totalwidth) / 2;

                        btnchild[btnCount].btn_text.css({left:margin});
                        btnchild[btnCount].btn_text.css({width:width});
                        btnchild[btnCount].btn_now_img.css({left:(margin+width)});

                        btnchild[btnCount].btn_now_img.css("display" , "");
                        btnchild[btnCount].btn_default_img.children().css("background-color", "rgba(102,102,102,1)");

                        btnchild[btnCount].is_now = true;

                        focusIndexByPage = btnCount;
                    }

                    btnchild[btnCount].btn_root_div.css("display" , "");
                    btnCount++;
                    if(btnCount>=12) {
                        startPageIndex++;
                        btnCount = 0;
                        btnchild = arrayPage[startPageIndex].btn_child;
                    }
                }
            }
        }

    }
})();