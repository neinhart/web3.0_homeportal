/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgRelatedLeftMenuView</code>
 *
 * 채널 미니 가이드 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */
(function() {
    KTW.ui.view.EpgRelatedLeftMenuView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;


        var parent = parent;
        var div = null;

        var currentChannel = null;
        var isOTS = false;
        var isShowFavoritedChannel = false;
        var isShowProgramDetail = false;
        var isShowTwoChannel = false;
        var isShowFourChannel = false;
        var isShowHitChannel = false;
        var isShowGenreOption = false;

        var callbackFunc = null;

        var focusIndex =  -1;


        /**
         * 선호채널 등록 및 자세히
         */
        var registerFavoritedChannelDiv = null;
        var textChNumChName = null;
        var imgChannelThumbnail = null;
        var imgChannelIframe = null;
        //var imgChannelThumbnailsdw = null;
        var registerFavoritedChannelBtnDiv = null;
        var detailViewBtnDiv = null;

        /**
         * 실시간 인기채널
         */
        var realTimehitChannelDiv = null;
        var realTimehitChanneldefaultImgDiv = null;

        /**
         * 2채널 동시시청
         */
        var twoChannelDiv = null;
        var twoChanneldefaultImgDiv = null;

        /**
         * 4채널 동시시청
         */
        var fourChannelDiv = null;
        var fourChanneldefaultImgDiv = null;


        var genreOption = null;

        var TEXT_GENREOPTION = ["시작시간 순" , "가나다 순"];

        var focusGenreoptionIndex = 0;
        var moveGenreoptionIndex = 0;

        var isShowGrenreDropBox = false;

        var isHideAction = false;

        this.setParentDiv  = function(parentdiv) {
            div = parentdiv;
        };

        this.create = function () {
            log.printDbg("create()");
        };

        this.getDiv = function () {
            return div;
        };

        this.show = function (options) {
            log.printDbg("show()");
            if(options !== undefined && options !== null) {
                currentChannel = options.channel;
                isOTS = options.is_ots_channel;

                isShowFavoritedChannel = options.is_show_favorited_channel;
                if(isShowFavoritedChannel === true && focusIndex < 0) {
                    focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                }

                isShowProgramDetail = options.is_show_program_detail;
                if(isShowProgramDetail === true && focusIndex < 0) {
                    focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                }

                isShowHitChannel = options.is_show_hit;
                if(isShowHitChannel === true && focusIndex < 0) {
                    focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                }

                isShowTwoChannel = options.is_show_2ch;
                if(isShowTwoChannel === true && focusIndex < 0) {
                    focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                }

                isShowFourChannel = options.is_show_4ch;
                if(isShowFourChannel === true && focusIndex < 0) {
                    focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                }


                isShowGenreOption = options.is_show_genre_option;
                if(isShowGenreOption === true && focusIndex < 0) {
                    focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                }

                focusGenreoptionIndex = options.focus_option_index;

                callbackFunc = options.left_menu_callback_func;
            }

            isHideAction = false;
            _createElement();
            _focusUpdate();
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");

            _hideAll();

            div.css("display", "none");
            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.focus = function () {
            log.printDbg("focus()");
            _focusUpdate();
        };

        this.blur = function () {
            log.printDbg("blur()");
            if(isShowFavoritedChannel) {
                $("#favorited_on_btn_background").css("display", "none");
                $("#favorited_off_btn_background").css("display", "");
                $("#text_register_favorited_channel").css("color", "rgba(255,255,255,1)");
                $("#text_register_favorited_channel").removeClass("font_m");
                $("#text_register_favorited_channel").addClass("font_l");


                /*
                if(isOTS === false) {
                    imgChannelThumbnailsdw.css("display" , "none");
                }
                */
            }

            if(isShowProgramDetail === true) {
                $("#focus_detail_background").css("display", "none");
                $("#detail_btn_background").css("display", "");
                $("#text_channel_detail").css("color", "rgba(255,255,255,1)");
                $("#text_channel_detail").removeClass("font_m");
                $("#text_channel_detail").addClass("font_l");
            }

            if(isShowHitChannel === true) {
                realTimehitChanneldefaultImgDiv.find(".multiScreen").removeClass("focus");
                realTimehitChannelDiv.find("#realtime_ch_focus").css("display","none");

                realTimehitChannelDiv.find("#realtime_ch_title").css({top:6});
                realTimehitChanneldefaultImgDiv.css({top:20});
            }

            if(isShowTwoChannel === true) {
                twoChanneldefaultImgDiv.find(".multiScreen").removeClass("focus");
                twoChannelDiv.find("#two_ch_focus").css("display","none");

                twoChannelDiv.find("#two_ch_title").css({top:6});
                twoChanneldefaultImgDiv.css({top:20});
            }
            if(isShowFourChannel === true) {
                fourChanneldefaultImgDiv.find(".multiScreen").removeClass("focus");
                fourChannelDiv.find("#four_ch_focus").css("display","none");

                fourChannelDiv.find("#four_ch_title").css({top:6});
                fourChanneldefaultImgDiv.css({top:20});
            }



            if(isShowGenreOption === true) {
                genreOption.drop_box_sdw.css("display" , "none");
                genreOption.item2.root_div.css("display" , "none");

                genreOption.item1.default_btn.css("display" , "");
                genreOption.item1.focus_btn.css("display" , "none");
                genreOption.item1.text_btn.text(TEXT_GENREOPTION[focusGenreoptionIndex]);
                genreOption.item1.text_btn.removeClass("font_m");
                genreOption.item1.text_btn.addClass("font_l");

                genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                genreOption.item1.arw_dw_img.css("display" , "");
                genreOption.item1.arw_dw_img.attr("src", "images/popup/arw_option_popup_dw.png");

                genreOption.item1.select_check_img.css("display" , "none");

                genreOption.item1.root_div.css("display" , "");

                isShowGrenreDropBox = false;

            }
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            if(isHideAction === true) {
                return consumed;
            }

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    if(isShowGrenreDropBox === false) {
                        if( isShowFavoritedChannel === true &&
                            focusIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                            if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFourChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowTwoChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            } else if(isShowHitChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }else if(isOTS === true && isShowProgramDetail === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }
                        }else if(isShowProgramDetail === true && isOTS === true &&
                            focusIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                            if(isShowFavoritedChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFourChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowTwoChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            } else if(isShowHitChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                            if( isShowProgramDetail === true  && isOTS === true) {
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }else if(isShowFavoritedChannel === true) {
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFourChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowTwoChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH) {
                            if(isShowHitChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }else if( isShowProgramDetail === true  && isOTS === true) {
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }else if(isShowFavoritedChannel === true) {
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFourChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                            if(isShowTwoChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            } else if(isShowHitChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }else if( isShowProgramDetail === true  && isOTS === true) {
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }else if(isShowFavoritedChannel === true) {
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION) {
                            if (isShowFourChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            } else if (isShowTwoChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            } else if (isShowHitChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            } else if (isShowProgramDetail === true && isOTS === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            } else if (isShowFavoritedChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }
                        }
                        _focusUpdate();
                    }else {
                        moveGenreoptionIndex--;
                        if(moveGenreoptionIndex<0) {
                            moveGenreoptionIndex = 1;
                        }

                        genreOption.item1.default_btn.css("display" , "");
                        genreOption.item1.focus_btn.css("display" , "none");
                        genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                        //genreOption.item1.select_check_img.css("display" , "none");

                        genreOption.item1.text_btn.removeClass("font_m");
                        genreOption.item1.text_btn.addClass("font_l");

                        genreOption.item2.default_btn.css("display" , "");
                        genreOption.item2.focus_btn.css("display" , "none");
                        genreOption.item2.text_btn.css("color", "rgba(255,255,255,1)");

                        genreOption.item2.text_btn.removeClass("font_m");
                        genreOption.item2.text_btn.addClass("font_l");
                        //genreOption.item2.select_check_img.css("display" , "none");

                        if(moveGenreoptionIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.OPTION_TYPE.SORT_START_TIME) {
                            genreOption.item1.default_btn.css("display" , "none");
                            genreOption.item1.focus_btn.css("display" , "");
                            genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");

                            genreOption.item1.text_btn.removeClass("font_l");
                            genreOption.item1.text_btn.addClass("font_m");
                        }else {
                            genreOption.item2.default_btn.css("display" , "none");
                            genreOption.item2.focus_btn.css("display" , "");
                            genreOption.item2.text_btn.css("color", "rgba(255,255,255,1)");

                            genreOption.item2.text_btn.removeClass("font_l");
                            genreOption.item2.text_btn.addClass("font_m");
                        }

                        if(focusGenreoptionIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.OPTION_TYPE.SORT_START_TIME) {
                            if(focusGenreoptionIndex === moveGenreoptionIndex) {
                                genreOption.item1.select_check_img.attr("src", "images/popup/option_dropbox_select_f.png");
                            }else {
                                genreOption.item1.select_check_img.attr("src", "images/popup/option_dropbox_select.png");
                            }
                        }else {
                            if(focusGenreoptionIndex === moveGenreoptionIndex) {
                                genreOption.item2.select_check_img.attr("src", "images/popup/option_dropbox_select_f.png");
                            }else {
                                genreOption.item2.select_check_img.attr("src", "images/popup/option_dropbox_select.png");
                            }
                        }

                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    if(isShowGrenreDropBox === false) {
                        if( isShowFavoritedChannel === true &&
                            focusIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                            if(isOTS === true && isShowProgramDetail === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }else if(isShowHitChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            } else if(isShowTwoChannel === true)  {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            }else if(isShowFourChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }
                        }else if(isOTS === true && isShowProgramDetail === true &&
                            focusIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                            if(isShowHitChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }else if(isShowTwoChannel === true)  {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            }else if(isShowFourChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFavoritedChannel === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                            if(isShowTwoChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            }else if(isShowFourChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFavoritedChannel === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isOTS === true && isShowProgramDetail === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH) {
                            if(isShowFourChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }else if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFavoritedChannel === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isOTS === true && isShowProgramDetail === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            } else if(isShowHitChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                            if(isShowGenreOption === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION;
                            }else if(isShowFavoritedChannel === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isOTS === true && isShowProgramDetail === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            } else if(isShowHitChannel === true) {
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }else if(isShowTwoChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            }
                        }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION) {
                            if(isShowFavoritedChannel === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED;
                            }else if(isOTS === true && isShowProgramDetail === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL;
                            }else if(isShowHitChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH;
                            }else if(isShowTwoChannel === true){
                                focusIndex =  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH;
                            }else if(isShowFourChannel === true){
                                focusIndex = KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH;
                            }
                        }
                        _focusUpdate();
                    }else {
                        moveGenreoptionIndex++;
                        if(moveGenreoptionIndex>1) {
                            moveGenreoptionIndex = 0;
                        }

                        genreOption.item1.default_btn.css("display" , "");
                        genreOption.item1.focus_btn.css("display" , "none");
                        genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                        genreOption.item1.text_btn.removeClass("font_m");
                        genreOption.item1.text_btn.addClass("font_l");

                        genreOption.item2.default_btn.css("display" , "");
                        genreOption.item2.focus_btn.css("display" , "none");
                        genreOption.item2.text_btn.css("color", "rgba(255,255,255,1)");
                        genreOption.item2.text_btn.removeClass("font_m");
                        genreOption.item2.text_btn.addClass("font_l");

                        if(moveGenreoptionIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.OPTION_TYPE.SORT_START_TIME) {
                            genreOption.item1.default_btn.css("display" , "none");
                            genreOption.item1.focus_btn.css("display" , "");
                            genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");

                            genreOption.item1.text_btn.removeClass("font_l");
                            genreOption.item1.text_btn.addClass("font_m");
                        }else {
                            genreOption.item2.default_btn.css("display" , "none");
                            genreOption.item2.focus_btn.css("display" , "");
                            genreOption.item2.text_btn.css("color", "rgba(255,255,255,1)");

                            genreOption.item2.text_btn.removeClass("font_l");
                            genreOption.item2.text_btn.addClass("font_m");
                        }

                        if(focusGenreoptionIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.OPTION_TYPE.SORT_START_TIME) {
                            if(focusGenreoptionIndex === moveGenreoptionIndex) {
                                genreOption.item1.select_check_img.attr("src", "images/popup/option_dropbox_select_f.png");
                            }else {
                                genreOption.item1.select_check_img.attr("src", "images/popup/option_dropbox_select.png");
                            }
                        }else {
                            if(focusGenreoptionIndex === moveGenreoptionIndex) {
                                genreOption.item2.select_check_img.attr("src", "images/popup/option_dropbox_select_f.png");
                            }else {
                                genreOption.item2.select_check_img.attr("src", "images/popup/option_dropbox_select.png");
                            }
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    if(isShowGenreOption === true && focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION) {
                        if(isShowGrenreDropBox === false) {
                            genreOption.drop_box_sdw.css("display" , "");

                            genreOption.item1.default_btn.css("display" , "");
                            genreOption.item1.focus_btn.css("display" , "none");
                            genreOption.item1.text_btn.text(TEXT_GENREOPTION[0]);
                            genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                            genreOption.item1.arw_dw_img.attr("src", "images/popup/arw_option_popup_dw.png");
                            genreOption.item1.arw_dw_img.css("display" , "none");
                            genreOption.item1.select_check_img.css("display" , "none");
                            genreOption.item1.root_div.css("display" , "");

                            genreOption.item2.default_btn.css("display" , "");
                            genreOption.item2.focus_btn.css("display" , "none");
                            genreOption.item2.text_btn.text(TEXT_GENREOPTION[1]);
                            genreOption.item2.text_btn.css("color", "rgba(255,255,255,1)");
                            genreOption.item2.text_btn.css("display" , "");

                            genreOption.item2.arw_dw_img.attr("src", "images/popup/arw_option_popup_dw.png");
                            genreOption.item2.arw_dw_img.css("display" , "none");
                            genreOption.item2.select_check_img.css("display" , "none");
                            genreOption.item2.root_div.css("display" , "");

                            if(focusGenreoptionIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.OPTION_TYPE.SORT_START_TIME) {
                                genreOption.item1.default_btn.css("display" , "none");
                                genreOption.item1.focus_btn.css("display" , "");
                                genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                                genreOption.item1.select_check_img.attr("src", "images/popup/option_dropbox_select.png");
                                genreOption.item1.select_check_img.css("display" , "");
                            }else {
                                genreOption.item2.default_btn.css("display" , "none");
                                genreOption.item2.focus_btn.css("display" , "");
                                genreOption.item2.text_btn.css("color", "rgba(255,255,255,1)");
                                genreOption.item2.select_check_img.attr("src", "images/popup/option_dropbox_select.png");
                                genreOption.item2.select_check_img.css("display" , "");
                            }
                            moveGenreoptionIndex = focusGenreoptionIndex;

                            isShowGrenreDropBox = true;
                        }else {
                            if(focusGenreoptionIndex !== moveGenreoptionIndex) {
                                if(callbackFunc !== null) {
                                    parent.hideView(callbackFunc , focusIndex , moveGenreoptionIndex , null , null , null , null , null);
                                }
                            }else {
                                isShowGrenreDropBox = false;
                                genreOption.drop_box_sdw.css("display" , "none");
                                genreOption.item2.root_div.css("display" , "none");

                                genreOption.item1.default_btn.css("display" , "");
                                genreOption.item1.focus_btn.css("display" , "none");
                                genreOption.item1.text_btn.text(TEXT_GENREOPTION[focusGenreoptionIndex]);
                                genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                                genreOption.item1.arw_dw_img.css("display" , "");
                                genreOption.item1.arw_dw_img.attr("src", "images/popup/arw_option_popup_dw.png");

                                genreOption.item1.select_check_img.css("display" , "none");

                                genreOption.item1.root_div.css("display" , "");
                            }
                        }
                    }else {
                        var ret = true;
                        /**
                         * 키즈모드 정책 상 2채널 동시시청/4채널 동시시청/실시간인기채널 진입 시 안내팝업 노출함.
                         */
                        if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH ||
                            focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH ||
                            focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                            if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                                ret = false;
                            }
                        }

                        if(ret === true){
                            if(callbackFunc !== null) {
                                parent.hideView(callbackFunc , focusIndex , null , null , null , null , null , null);
                            }
                        }else {
                            KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup();
                        }
                    }


                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    if(isShowGrenreDropBox === true) {
                        isShowGrenreDropBox = false;
                        _focusUpdate();
                        consumed = true;
                    }
                    break;
            }

            return consumed;
        };


        function _hideAll() {
            log.printDbg("_hideAll()");
        }

        function _focusUpdate() {
            log.printDbg("_focusUpdate()");
            if(isShowFavoritedChannel) {
                $("#favorited_on_btn_background").css("display", "none");
                $("#favorited_off_btn_background").css("display", "");
                $("#text_register_favorited_channel").css("color", "rgba(255,255,255,1)");
                $("#text_register_favorited_channel").removeClass("font_m");
                $("#text_register_favorited_channel").addClass("font_l");

            }

            if(isShowProgramDetail === true) {
                $("#focus_detail_background").css("display", "none");
                $("#detail_btn_background").css("display", "");
                $("#text_channel_detail").css("color", "rgba(255,255,255,1)");
                $("#text_channel_detail").removeClass("font_m");
                $("#text_channel_detail").addClass("font_l");
            }

            if(isShowTwoChannel === true) {
                twoChanneldefaultImgDiv.find(".multiScreen").removeClass("focus");
                twoChannelDiv.find("#two_ch_focus").css("display","none");
                twoChannelDiv.find("#two_ch_title").css({top:6});
                twoChanneldefaultImgDiv.css({top:20});
            }

            if(isShowFourChannel === true) {
                fourChanneldefaultImgDiv.find(".multiScreen").removeClass("focus");
                fourChannelDiv.find("#four_ch_focus").css("display","none");

                fourChannelDiv.find("#four_ch_title").css({top:6});
                fourChanneldefaultImgDiv.css({top:20});
            }

            if(isShowHitChannel === true) {
                realTimehitChanneldefaultImgDiv.find(".multiScreen").removeClass("focus");
                realTimehitChannelDiv.find("#realtime_ch_focus").css("display","none");

                realTimehitChannelDiv.find("#realtime_ch_title").css({top:6});
                realTimehitChanneldefaultImgDiv.css({top:20});
            }

            if(isShowGenreOption === true) {
                genreOption.drop_box_sdw.css("display" , "none");
                genreOption.item2.root_div.css("display" , "none");

                genreOption.item1.default_btn.css("display" , "");
                genreOption.item1.focus_btn.css("display" , "none");
                genreOption.item1.text_btn.text(TEXT_GENREOPTION[focusGenreoptionIndex]);

                genreOption.item1.text_btn.removeClass("font_m");
                genreOption.item1.text_btn.addClass("font_l");

                genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                genreOption.item1.arw_dw_img.attr("src", "images/popup/arw_option_popup_dw.png");
                genreOption.item1.arw_dw_img.css("display" , "");
                genreOption.item1.select_check_img.css("display" , "none");

                genreOption.item1.root_div.css("display" , "");

            }

            if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                $("#favorited_off_btn_background").css("display", "none");
                $("#favorited_on_btn_background").css("display", "");
                $("#text_register_favorited_channel").css("color" , "rgba(255,255,255,1)");

                $("#text_register_favorited_channel").removeClass("font_l");
                $("#text_register_favorited_channel").addClass("font_m");


            }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                $("#detail_btn_background").css("display", "none");
                $("#focus_detail_background").css("display", "");
                $("#text_channel_detail").css("color" , "rgba(255,255,255,1)");

                $("#text_channel_detail").removeClass("font_l");
                $("#text_channel_detail").addClass("font_m");

            }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                realTimehitChanneldefaultImgDiv.find(".multiScreen").addClass("focus");
                realTimehitChannelDiv.find("#realtime_ch_focus").css("display","");
                realTimehitChannelDiv.find("#realtime_ch_title").css({top:0});
                realTimehitChanneldefaultImgDiv.css({top:28});
            }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH) {
                twoChanneldefaultImgDiv.find(".multiScreen").addClass("focus");
                twoChannelDiv.find("#two_ch_focus").css("display","");
                twoChannelDiv.find("#two_ch_title").css({top:0});
                twoChanneldefaultImgDiv.css({top:28});
            }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                fourChanneldefaultImgDiv.find(".multiScreen").addClass("focus");
                fourChannelDiv.find("#four_ch_focus").css("display","");
                fourChannelDiv.find("#four_ch_title").css({top:0});
                fourChanneldefaultImgDiv.css({top:28});
            }else if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION) {
                genreOption.drop_box_sdw.css("display" , "none");
                genreOption.item2.root_div.css("display" , "none");

                genreOption.item1.default_btn.css("display" , "none");
                genreOption.item1.focus_btn.css("display" , "");
                genreOption.item1.text_btn.text(TEXT_GENREOPTION[focusGenreoptionIndex]);
                genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                genreOption.item1.arw_dw_img.attr("src", "images/popup/arw_option_popup_dw.png");

                genreOption.item1.text_btn.removeClass("font_l");
                genreOption.item1.text_btn.addClass("font_m");


                genreOption.item1.arw_dw_img.css("display" , "");

                genreOption.item1.select_check_img.css("display" , "none");

                genreOption.item1.root_div.css("display" , "");
            }

        }


        function _isHiddenPromoChannel(ch) {
            log.printDbg("isHiddenPromoChannel()");
            var isHiddenPromoChannel = false;


            isHiddenPromoChannel =  KTW.oipf.AdapterHandler.navAdapter.isHiddenPromoChannel(ch);
            return isHiddenPromoChannel;
        }

        function _getDefaultPromoChannel(currentChannel) {
            log.printDbg("_getDefaultPromoChannel()");
            var defaultPromoChannelList = [];
            var defaultPromoChannel = null;
            if(KTW.CONSTANT.IS_OTS === true &&
                (currentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || currentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2)) {
                defaultPromoChannel = KTW.oipf.AdapterHandler.navAdapter.getSkyPlusChannel();
            }else {
                defaultPromoChannel = KTW.oipf.AdapterHandler.navAdapter.getPromoChannel();
            }

            defaultPromoChannelList = KTW.utils.epgUtil.reconstructionChList([defaultPromoChannel], true);

            return defaultPromoChannelList;
        }
        
        /**
         * 1920X1080 해상도 기준으로 leftDiv left : 1171임
         * @private
         */
        function _createElement() {
            log.printDbg("_createLeftView()");
            var startPosTop = 0;
            var h = 0;

            startPosTop += 53;

            var h = 0;
            h = _createRegisterFavoritedChaannelDiv(startPosTop , isShowFavoritedChannel , isShowProgramDetail);
            startPosTop += h;

            if(isShowHitChannel === true) {
                startPosTop-=8;
                h = _createHitChannelDiv(startPosTop);
                startPosTop += h;
            }

            if(isShowTwoChannel === true) {
                startPosTop-=3;
                startPosTop-=8;
                h = _createTwoChannelDiv(startPosTop);
                startPosTop += h;
            }else {
                if (isOTS === false && isShowFavoritedChannel === true) {
                    startPosTop += 30;
                }
            }

            if(isShowFourChannel === true) {
                startPosTop-=3;
                startPosTop-=8;
                h = _createFourChannelDiv(startPosTop);
                startPosTop += h;
            }else {
                if (isOTS === false && isShowTwoChannel == false && isShowFavoritedChannel === true) {
                    startPosTop += 30;
                }
            }

            if(isShowGenreOption === true) {
                h = _createGenreOptionDiv(startPosTop);
                startPosTop += h;
            }

            if(isShowFavoritedChannel === true) {
                var isHiddenPromoChannel = _isHiddenPromoChannel(currentChannel);
                var defaultPromoChannelList = null;
                if(isHiddenPromoChannel === true) {
                    var defaultPromoChannelList = _getDefaultPromoChannel(currentChannel);
                    if(defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
                        if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(defaultPromoChannelList[0]) === true) {
                            $("#text_register_favorited_channel").text("선호 채널 해제");
                            $("#icon_favorited_on_off").attr("src" , "images/icon/icon_favorite.png");
                        }
                        // 아니면 선호 채널 등록
                        else {
                            $("#text_register_favorited_channel").text("선호 채널 등록");
                            $("#icon_favorited_on_off").attr("src" , "images/icon/icon_favorite_d.png");
                        }
                    }
                }else {
                    if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(currentChannel) === true) {
                        $("#text_register_favorited_channel").text("선호 채널 해제");
                        $("#icon_favorited_on_off").attr("src" , "images/icon/icon_favorite.png");
                    }
                    // 아니면 선호 채널 등록
                    else {
                        $("#text_register_favorited_channel").text("선호 채널 등록");
                        $("#icon_favorited_on_off").attr("src" , "images/icon/icon_favorite_d.png");
                    }
                }

                if(focusIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                    $("#favorited_on_btn_background").css("display", "");
                    $("#text_register_favorited_channel").css("color" , "rgba(255,255,255,1)");

                }else {
                    $("#favorited_off_btn_background").css("display", "");
                    $("#text_register_favorited_channel").css("color", "rgba(255,255,255,1)");
                }

                $("#text_register_favorited_channel").css("display", "");
                $("#icon_favorited_on_off").css("display", "");


                if(KTW.CONSTANT.IS_OTS !== true) {

                   setTimeout(function () {
                       var cp = _getCurrentProgram(currentChannel);

                       var isBlocked = KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(currentChannel);


                       if (isBlocked === true) {
                           imgChannelThumbnail.css("display", "none");
                           imgChannelIframe.css("display" , "");
                           imgChannelIframe.attr("src" , "images/iframe/block_ch.jpg");
                       } else if (KTW.utils.epgUtil.isAgeLimitProgram(cp)) {
                           imgChannelThumbnail.css("display", "none");
                           imgChannelIframe.css("display" , "");
                           imgChannelIframe.attr("src" , "images/iframe/block_age.jpg");
                       } else if (currentChannel.desc === 2) {
                           imgChannelThumbnail.css("display", "none");
                           imgChannelIframe.css("display" , "");
                           imgChannelIframe.attr("src" , "images/iframe/block_adult_ch.jpg");
                       } else {
                           try {
                               imgChannelIframe.css("display" , "none");
                               imgChannelThumbnail.css("display", "");
                               if(isHiddenPromoChannel === true) {
                                   if(defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
                                       imgChannelThumbnail.css("background-size", "278px 155px");
                                       var imageUrl = util.getImageUrl(
                                           KTW.DATA.HTTP.THUMBNAIL_IMAGE_SERVER + defaultPromoChannelList[0].sid+".png", KTW.CONSTANT.POSTER_TYPE.THUMBNAIL, 278, 155);
                                       //imgChannelThumbnail.css("background-image", "url(http://image.ktipmedia.co.kr/channel/CH_" + defaultPromoChannelList[0].sid+".png)");
                                       imgChannelThumbnail.css("background-image", "url(" + imageUrl +")");
                                   }
                               }else {
                                   imgChannelThumbnail.css("background-size", "278px 155px");
                                   var imageUrl = util.getImageUrl(
                                       KTW.DATA.HTTP.THUMBNAIL_IMAGE_SERVER + currentChannel.sid+".png", KTW.DATA.HTTP.THUMBNAIL_IMAGE_SERVER, 278, 155);
                                   imgChannelThumbnail.css("background-image", "url(" + imageUrl + ")");
                               }
                           }catch (e) {
                               log.printDbg(e);
                           }
                       }
                    }, 10);
                }
            }

            if(isShowGenreOption === true) {
                genreOption.drop_box_sdw.css("display" , "none");
                genreOption.item2.root_div.css("display" , "none");

                genreOption.item1.default_btn.css("display" , "");
                genreOption.item1.focus_btn.css("display" , "none");
                genreOption.item1.text_btn.text(TEXT_GENREOPTION[focusGenreoptionIndex]);
                genreOption.item1.text_btn.css("color", "rgba(255,255,255,1)");
                genreOption.item1.text_btn.css("display", "");
                genreOption.item1.arw_dw_img.css("display" , "");
                genreOption.item1.select_check_img.css("display" , "none");

                genreOption.item1.root_div.css("display" , "");
            }

        }

        function _createRegisterFavoritedChaannelDiv(startPosTop , isShowFavoritedChannel , isShowProgramDetail) {
            /**
             * OTV는 271
             */
            var favheight = 0;

            if(isShowFavoritedChannel === true || isShowProgramDetail === true) {
                if(isOTS) {
                    favheight = 173+33;
                }else {
                    favheight = 271+16;
                }

                registerFavoritedChannelDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "register_favorited_channel_div",
                        css: {
                            position: "absolute",
                            left: 1199 - 1171, top: startPosTop, width:312, height: favheight

                        }
                    },
                    parent: div
                });

                textChNumChName = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "font_m cut",
                        css: {
                            position: "absolute", left: 1217-1199 , top: 0 , width: 278, height: 34,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24 , "letter-spacing":-1.2
                        }
                    },
                    text: "",
                    parent: registerFavoritedChannelDiv
                });

                if(currentChannel !== undefined && currentChannel !== null) {
                    textChNumChName.text(util.numToStr(currentChannel.majorChannel, 3) + " " + currentChannel.name);
                }

                if (isOTS === true) {
                    if(isShowFavoritedChannel === false || isShowProgramDetail === false) {
                        favheight = 131;
                    }
                    var startTopPos = 34;
                    if(isShowFavoritedChannel === true) {
                        registerFavoritedChannelBtnDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "register_favorited_btn_div",
                                css: {
                                    position: "absolute",
                                    left: 1216-1199, top: startTopPos, width:278, height: 65

                                }
                            },
                            parent: registerFavoritedChannelDiv
                        });
                        startTopPos+=65;
                        startTopPos+=9;
                    }

                    if(isShowProgramDetail === true) {
                        detailViewBtnDiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "detail_btn_div",
                                css: {
                                    position: "absolute",
                                    left: 1216-1199, top: startTopPos, width:278, height: 65
                                }
                            },
                            parent: registerFavoritedChannelDiv
                        });

                        /**
                         * 자세히 버튼 세부 Element 추가함.
                         */
                        var defaultImg = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "detail_btn_background" ,
                                css: {
                                    position: "absolute",
                                    left: 0, top: 0, width:278, height: 65,
                                    display: "none" ,
                                    "background-color": "rgba(200,200,200,1)",
                                    opacity:0.3
                                }
                            },
                            parent: detailViewBtnDiv
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "" ,
                                css: {
                                    position: "absolute",
                                    left: 2, top: 2, width:274, height: 61,
                                    "background-color": "rgba(160,160,160,1)",
                                    opacity:1
                                }
                            },
                            parent: defaultImg
                        });

                        util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "focus_detail_background",
                                css: {
                                    position: "absolute",
                                    left: 0, top: 0, width:278, height: 65,
                                    "background-color": "rgba(210,51,47,1)",
                                    display:"none"
                                }
                            },
                            parent: detailViewBtnDiv
                        });

                        util.makeElement({
                            tag: "<span />",
                            attrs: {
                                id: "text_channel_detail",
                                class: "font_l",
                                css: {
                                    position: "absolute", left: 101 , top: 19 , width: 100, height: 30,
                                    color: "rgba(255,255,255 1)", "font-size": 30 , "letter-spacing":-1.5
                                }
                            },
                            text: "자세히",
                            parent: detailViewBtnDiv
                        });



                    }

                }else {
                    /*
                    imgChannelThumbnailsdw = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "",
                            src: "images/popup/op_btn_fav_ch.png",
                            css: {
                                position: "absolute",
                                left: 0 ,
                                top: 17,
                                width: 312,
                                height: 254
                            }
                        },
                        parent: registerFavoritedChannelDiv
                    });
                    */

                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "",
                            src: "images/iframe/default_thumb.jpg",
                            css: {
                                position: "absolute",
                                left: 1216-1199 ,
                                top: 34,
                                width: 278,
                                height: 155
                            }
                        },
                        parent: registerFavoritedChannelDiv
                    });

                    imgChannelThumbnail = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "",
                            css: {
                                position: "absolute",
                                left: 1216-1199 ,
                                top: 34,
                                width: 278,
                                height: 155
                            }
                        },
                        parent: registerFavoritedChannelDiv
                    });

                    imgChannelIframe = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "",
                            src: "",
                            css: {
                                position: "absolute",
                                left: 1216-1199 ,
                                top: 34,
                                width: 278,
                                height: 155
                            }
                        },
                        parent: registerFavoritedChannelDiv
                    });


                    registerFavoritedChannelBtnDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "register_favorited_btn_div",
                            css: {
                                position: "absolute",
                                left: 1216-1199, top: 189, width:278, height: 65

                            }
                        },
                        parent: registerFavoritedChannelDiv
                    });
                }


                var defaultImg = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "favorited_off_btn_background" ,
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:278, height: 65,
                            display: "none" ,
                            "background-color": "rgba(200,200,200,1)",
                            opacity:0.3
                        }
                    },
                    parent: registerFavoritedChannelBtnDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "" ,
                        css: {
                            position: "absolute",
                            left: 2, top: 2, width:274, height: 61,
                            "background-color": "rgba(160,160,160,1)",
                            opacity:1
                        }
                    },
                    parent: defaultImg
                });


                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "favorited_on_btn_background",
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:278, height: 65,
                            "background-color": "rgba(210,51,47,1)" , display:"none"
                        }
                    },
                    parent: registerFavoritedChannelBtnDiv
                });


                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_register_favorited_channel",
                        class: "font_l",
                        css: {
                            position: "absolute", left: 17 , top: 19 , width: 180, height: 30,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30 , display:"none", "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: registerFavoritedChannelBtnDiv
                });



                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "icon_favorited_on_off",
                        src: "images/icon/icon_favorite.png",
                        css: {
                            position: "absolute",
                            left: 232 ,
                            top: 21,
                            width: 25,
                            height: 24,
                            display:"none"
                        }
                    },
                    parent: registerFavoritedChannelBtnDiv
                });
            }else {
                return favheight;
            }

            return favheight;
        }

        function _createTwoChannelDiv(startPosTop) {
            var posTop = 0;
            /**
             * 2채널 동시시청
             */
            twoChannelDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1183 - 1171, top: startPosTop, width:344, height: 228+8

                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "two_ch_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1217-1183, top: 6, width: 160, height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24 , "letter-spacing":-1.2
                    }
                },
                text: "2채널 동시시청",
                parent: twoChannelDiv
            });


            twoChanneldefaultImgDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1199 - 1183 , top: 20, width:312, height: 189
                    }
                },
                parent: twoChannelDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    class : "multiScreen",
                    src: "images/popup/op_btn_2ch.png",
                    css: {
                        position: "absolute"
                    }
                },
                parent: twoChanneldefaultImgDiv
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "two_ch_focus",
                    class : "focus_red_border_box",
                    css: {
                        position: "absolute",
                        left: 12, top: 29, width:321, height: 187, display:"none"
                    }
                },
                parent: twoChannelDiv
            });


            posTop = 228+8;
            return posTop;
        }


        function _createFourChannelDiv(startPosTop) {
            var posTop = 0;
            /**
             * 4채널 동시시청
             */
            fourChannelDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1183 - 1171, top: startPosTop, width:344, height: 228+8

                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "four_ch_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1217-1183, top: 6, width: 160, height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24 , "letter-spacing":-1.2
                    }
                },
                text: "4채널 동시시청",
                parent: fourChannelDiv
            });


            fourChanneldefaultImgDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1199 - 1183 , top: 20, width:312, height: 189
                    }
                },
                parent: fourChannelDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    class : "multiScreen",
                    src: "images/popup/op_btn_4ch.png",
                    css: {
                        position: "absolute"
                    }
                },
                parent: fourChanneldefaultImgDiv
            });

            
            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "four_ch_focus",
                    class : "focus_red_border_box",
                    css: {
                        position: "absolute",
                        left: 12, top: 29, width:321, height: 187, display:"none"
                    }
                },
                parent: fourChannelDiv
            });



            posTop = 228+8;
            return posTop;
        }


        function _createHitChannelDiv(startPosTop) {


            var posTop = 0;
            /**
             * 실시간 인기채널
             */
            realTimehitChannelDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1183 - 1171, top: startPosTop, width:344, height: 228+8

                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "realtime_ch_title",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1217-1183, top: 0, width: 160, height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24 , "letter-spacing":-1.2
                    }
                },
                text: "실시간 인기채널",
                parent: realTimehitChannelDiv
            });


            realTimehitChanneldefaultImgDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1199 - 1183 , top: 20, width:312, height: 189
                    }
                },
                parent: realTimehitChannelDiv
            });



            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    class : "multiScreen",
                    src: "images/popup/op_btn_popul.png",
                    css: {
                        position: "absolute"
                    }
                },
                parent: realTimehitChanneldefaultImgDiv
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "realtime_ch_focus",
                    class : "focus_red_border_box",
                    css: {
                        position: "absolute",
                        left: 12, top: 29, width:321, height: 187, display:"none"
                    }
                },
                parent: realTimehitChannelDiv
            });


            posTop = 228+8;
            return posTop;
        }

        function _createGenreOptionDiv(startPosTop) {

            log.printDbg('_createGenreOptionDiv() startPosTop = ' + startPosTop);
            /**
             *             92 + 213
             */

            startPosTop -= 40;
            var genreOptionDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1176 - 1171, top: startPosTop, width:344, height: 92+213

                    }
                },
                parent: div
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "" ,
                    css: {
                        position: "absolute",
                        left: 1216-1176, top: 45, width:278, height: 2,
                        "background-color": "rgba(255,255,255,0.1)",
                        display: ""
                    }
                },
                parent: genreOptionDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1216-1176, top: 92, width: 160, height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24 , "letter-spacing":-1.2
                    }
                },
                text: "정렬 기준",
                parent: genreOptionDiv
            });

            var dropdownboxDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 0, top: 93, width:362, height: 212,
                        display:"none"
                    }
                },
                parent: genreOptionDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/popup/sdw_option_popup_top_w278.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 362,
                        height: 102
                    }
                },
                parent: dropdownboxDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/popup/sdw_option_popup_btm_w278.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 102,
                        width: 362,
                        height: 110
                    }
                },
                parent: dropdownboxDiv
            });


            var btnDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1216-1176, top: 92+38, width:278, height: 65,
                        display:""
                    }
                },
                parent: genreOptionDiv
            });

            var defaultImg1 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/popup/bg_option_dropbox_w278.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 278,
                        height: 65,
                        display:"none"
                    }
                },
                parent: btnDiv1
            });

            var btnFocusDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:278, height: 65,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv1
            });

            var btnText1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 32 , top: 19 , width: 150, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35,
                        display:"none"
                    }
                },
                text: "",
                parent: btnDiv1
            });

            var arwdwownImg1 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_popup_dw.png",
                    css: {
                        position: "absolute",
                        left: 216,
                        top: 10,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnDiv1
            });


            var selectcheckImg1 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/option_dropbox_select.png",
                    css: {
                        position: "absolute",
                        left: 216,
                        top: 10,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnDiv1
            });



            var btnDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1216-1176, top: 92+38+65, width:278, height: 65,
                        display:"none"
                    }
                },
                parent: genreOptionDiv
            });

            var defaultImg2 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/popup/bg_option_dropbox_w278.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 278,
                        height: 65,
                        display:"none"
                    }
                },
                parent: btnDiv2
            });

            var btnFocusDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:278, height: 65,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv2
            });

            var btnText2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 32 , top: 19 , width: 150, height: 29,
                        color: "rgba(255, 255, 255, 1)", "font-size": 27 , "text-align": "left",
                        display:"none","letter-spacing":-1.35
                    }
                },
                text: "",
                parent: btnDiv2
            });

            var arwdwownImg2 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_dw_f.png",
                    css: {
                        position: "absolute",
                        left: 216,
                        top: 10,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnDiv2
            });


            var selectcheckImg2 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/option_dropbox_select_f.png",
                    css: {
                        position: "absolute",
                        left: 216,
                        top: 10,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnDiv2
            });

            genreOption = {
                root_div : genreOptionDiv,
                drop_box_sdw : dropdownboxDiv,
                item1 : {
                    root_div : btnDiv1,
                    default_btn : defaultImg1,
                    focus_btn : btnFocusDiv1,
                    text_btn : btnText1,
                    arw_dw_img : arwdwownImg1,
                    select_check_img : selectcheckImg1
                },
                item2 : {
                    root_div : btnDiv2,
                    default_btn : defaultImg2,
                    focus_btn : btnFocusDiv2,
                    text_btn : btnText2,
                    arw_dw_img : arwdwownImg2,
                    select_check_img : selectcheckImg2
                }
            };

            return 92+213;
        }



        function _searchProgramList(ch) {
            log.printDbg('_searchProgramList() ch = ' + JSON.stringify(ch));

            var programManager = KTW.oipf.adapters.ProgramSearchAdapter;
            var prog_list = programManager.getPrograms(null, null, null, ch);
            log.printDbg('_searchProgramList() programManager.getPrograms = ' + JSON.stringify(prog_list));

            return prog_list;
        }

        function _getCurrentProgram(ch) {
            log.printDbg('_getCurrentProgram() ch.ccid = ' + ch.ccid);
            var currentProgram = null;
            var prog_list = _searchProgramList(ch);

            if(prog_list !== undefined && prog_list !== null && prog_list.length>0) {
                var prog_index = 0;
                var now = new Date().getTime();
                if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                    now = Math.floor(now/1000);
                }

                var prog = prog_list[prog_index];
                while(prog.startTime + prog.duration < now && prog_index < prog_list.length - 1) {
                    prog_index++;
                    prog = prog_list[prog_index];
                    if (prog == null) {
                        prog_index = -1;
                        break;
                    }
                }

                if(prog_index>=0) {
                    currentProgram = prog_list[prog_index];
                }
            }

            return currentProgram;
        }
    }


})();