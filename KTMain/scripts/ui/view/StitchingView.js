/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>StitchingView.js</code>
 *
 * 실시간 인기 채널 화면
 * @author dhlee
 * @since 2016-08-01
 */

"use strict";

(function() {
    KTW.ui.view.StitchingView = function(options) {
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;

        // MediaExtension class' Constants
        var COMPONENT_TYPE = {
            COMPONENT_TYPE_VIDEO : 0,
            COMPONENT_TYPE_AUDIO : 1,
            COMPONENT_TYPE_SUBTITLE : 2
        };

        // 기준 좌표
        var BASE_VALUE = {
            START_LEFT: 195,
            START_TOP: 102,
            GAP_WIDTH: 523,
            GAP_HEIGHT: 295,
            MAX_CHANNEL: 9
        };

        // stitching channel을 통해 9개의 채널 정보가 제공된다면 1 + 9, 6개가 제공된다면 1 + 6 이된다.
        var MAX_AUDIO_COMPONENT = 10;

        var parent = options;

        var vsData = null;

        var focusIndex = 0;
        var pageIndex = 0;
        var isFocused = false;
        var isTuned = false;

        var stitchingChannelData = undefined;    // 실시간 인기 채널 데이터
        var stitchingChannelControl = undefined; // 실시간 인기 채널 vbo
        var currentChannelLocator = undefined;   // 현재 tune 된 실시간 인기채널 locator
        var audioComponent = undefined;          // audio component
        var channelInformation = [];        // 실시간 인기 채널 상의 각 채널 정보
        var mainPrevChannel = undefined;    // 실시간 인기 채널 튠하기 이전의 메인 채널

        var scrollArea = null;
        var scroll = null;
        var isScrollFocus = false;
        var stitching_focus_div = null;

        var upImg = null;
        var downImg = null;

        this.create = function() {
            log.printDbg("createView()");
            isFocused = false;
            _createElement();

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 720,
                forceHeight: true
            });
        };

        this.show = function(data) {
            log.printDbg("show(), data = " + JSON.stringify(data));
            vsData = data;

            focusIndex = 0;
            pageIndex = 0;
            isFocused = false;
            isTuned = false;
            isScrollFocus = false;

            stitchingChannelData = vsData[pageIndex];

            // 2017.04.27 dhlee
            // 채널가이드 > 실시간인기채널로 올때 NONE으로 해줘야 한다.
            KTW.managers.service.IframeManager.setType(KTW.managers.service.IframeManager.DEF.TYPE.NONE);
            //(document.getElementById("stitching_background_img")).style.visibility = "inherit";

            // init() 를 정의하여 channel control을 생성하고
            _initStitching();
            //_tuneChannel();

            (document.getElementById("stitching_background_div")).style.visibility = "inherit";
            (document.getElementById("stitching_channel_info")).style.visibility = "inherit";

            _setFocus();

            scroll.show({
                maxPage: vsData.length,
                curPage: pageIndex
            });


        };

        this.hide = function() {
            log.printDbg("hide()");

            // first, set element's visibility to hidden
            //(document.getElementById("stitching_background_img")).style.visibility = "hidden";
            (document.getElementById("stitching_background_div")).style.visibility = "hidden";
            (document.getElementById("stitching_channel_info")).style.visibility = "hidden";
            scroll.hide();

            _removeListener();
            _stopStitching(false, null);


            vsData = undefined;
            stitchingChannelData = undefined;       // 실시간 인기 채널 데이터
            stitchingChannelControl = undefined;    // 실시간 인기 채널 vbo
            currentChannelLocator = undefined;      // 현재 tune 된 실시간 인기채널 locator
            audioComponent = undefined;             // audio component
            channelInformation = [];                // 실시간 인기 채널 상의 각 채널 정보
            mainPrevChannel = undefined;            // 실시간 인기 채널 튠하기 전의 메인 채널
        };

        // TODO 2016.08.03 UI/GUI 릴리즈 후 정리해야 함.
        this.controlKey = function(keyCode) {
            log.printDbg("controlKey(), keyCode = " + keyCode + ", isFocused = " + isFocused);

            var consumed = false;

            if (!isFocused) {
                return;
            }

            switch(keyCode) {
                case KTW.KEY_CODE.UP:
                    var tmpFocusIdx = focusIndex - 3;
                    if (isScrollFocus) {
                        tmpFocusIdx  = tmpFocusIdx < 0 ? tmpFocusIdx : (tmpFocusIdx - 3 < 0 ? tmpFocusIdx - 3 : tmpFocusIdx - 6);
                    }
                    moveChannel(tmpFocusIdx);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    var tmpFocusIdx = focusIndex + 3;
                    if (isScrollFocus) {
                        tmpFocusIdx  = tmpFocusIdx > BASE_VALUE.MAX_CHANNEL - 1 ? tmpFocusIdx : (tmpFocusIdx + 3 > BASE_VALUE.MAX_CHANNEL - 1 ? tmpFocusIdx + 3 : tmpFocusIdx + 6);
                    }
                    moveChannel(tmpFocusIdx);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    if (focusIndex % 3 === 0) {
                        KTW.ui.LayerManager.historyBack();
                    }
                    else {
                        moveChannel(focusIndex - 1);
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    if (isScrollFocus) {
                        isScrollFocus = false;
                        _setFocus();
                    }
                    else {
                        moveChannel(focusIndex + 1);
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    if (!isScrollFocus) {
                        _keyOk();
                    }
                    consumed = true;
                    return consumed;
            }

            return consumed;
        };

        /**
         * 실시간 인기 채널 화면 구성을 위한 element 들을 생성한다.
         *
         * @private
         */
        function _createElement() {
            log.printDbg("_createElement()");

            var stitching_image_path = KTW.CONSTANT.IMAGE_PATH + "stitching/";

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "stitching_background_div",
                    css: {
                        "background-color": "rgb(0, 0, 0)",
                        position: "absolute", left: 0, top: 0, width: 1920, height: 1080
                    }
                },
                parent: parent.div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "stitching_vbo_div"
                },
                parent: parent.div
            });

            var stitching_info_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "stitching_channel_info"
                },
                parent: parent.div
            });



            upImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    src: stitching_image_path + "arw_related_up.png",
                    css: {
                        position: "absolute", left: 942, top: 50
                    }
                },
                parent: stitching_info_div
            });

            downImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    src: stitching_image_path + "arw_related_dw.png",
                    css: {
                        position: "absolute", left: 943, top: 1009
                    }
                },
                parent: stitching_info_div
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 195, top: 70, width: 3, height: 24,
                        "background-color": "rgba(255, 255, 255, 0.5)"
                    }
                },
                parent: stitching_info_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "stitching_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 208, top: 69, color: "rgba(255, 255, 255, 0.7)", "font-size": 27,
                        "letter-spacing": -1.35
                    }
                },
                parent: stitching_info_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "font_l",
                    css: {
                        position: "absolute", left: 1349, top: 73, width: 373, height: 30, "text-align": "right",
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 23, "letter-spacing": -1.15
                    }
                },
                text: "※지상파는 지역별 자체방송이 제공됩니다",
                parent: stitching_info_div
            });

            // stitching channel focus div
            stitching_focus_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "stitching_focus_div",
                    class : "focus_red_border_box" ,
                    css: {
                        position: "absolute", left: 188, top: 144 , width: 495, height: 284 , visibility: "hidden"
                    }
                },
                parent: stitching_info_div
            });

            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1831, top: 143, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: parent.div
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: 1803, top: 904
                    }
                },
                parent: parent.div
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: 1831, top: 904
                    }
                },
                parent: parent.div
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 1803, top: 939, "font-size": 22, color: "rgba(255, 255, 255, 0.3)",
                        "letter-spacing": -1.1
                    }
                },
                text: "페이지",
                parent: parent.div
            });

        }

        function _setFocus() {
            log.printDbg("_setFocus(), focusIndex = " + focusIndex);


            stitching_focus_div.css({
                left: (188 + (524 * (focusIndex % 3))),
                top: (114  + (295 * parseInt(focusIndex / 3)))
            });

            // stitching_focus_div.css({
            //     left: (BASE_VALUE.START_LEFT + BASE_VALUE.GAP_WIDTH * (focusIndex % 3)),
            //     top: (BASE_VALUE.START_TOP + BASE_VALUE.GAP_HEIGHT * parseInt(focusIndex / 3))
            // });

            scroll.setCurrentPage(pageIndex);
            
            if(pageIndex === 0) {
                upImg.css("display" , "none");
                downImg.css("display" , "");
            }else {
                upImg.css("display" , "");
                if((vsData.length - 1) === pageIndex) {
                    downImg.css("display" , "none");
                }else {
                    downImg.css("display" , "");
                }
            }


            if (isScrollFocus) {
                //parent.div.find("#stitching_focus_div div").css({"background-color": "rgba(128, 128, 128, 1)"});
                stitching_focus_div.css({"visibility": "hidden"});
                scroll.setFocus(true);
            }
            else {
                //parent.div.find("#stitching_focus_div div").css({"background-color": "rgba(255, 255, 255, 1)"});
                stitching_focus_div.css({"visibility": "inherit"});
                scroll.setFocus(false);
            }
        }

        /**
         * 팝업을 보여준다.`
         *
         * @private
         */
        function _showPopup() {
            log.printDbg("_showPopup()");

            var popupData = {
                arrMessage: [{
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                    message: [KTW.ERROR.EX_CODE.HOME.EX003.title],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                    message: [KTW.ERROR.EX_CODE.HOME.EX003.message[0]],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_30,
                    message: [KTW.ERROR.EX_CODE.HOME.EX003.message[1]],
                    cssObj: {}
                }, {
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_28,
                    message: [KTW.ERROR.EX_CODE.HOME.EX003.message[2]],
                    cssObj: { color: "rgb(180, 180, 180)"}
                }],

                arrButton: [{id: "ok", name: "확인"}],

                cbAction: function (id) {
                    if (id === "ok") {
                        KTW.ui.LayerManager.deactivateLayer({
                            id: "stitching_channel_change_fail_popup",
                            remove: true
                        });
                    }
                }
            };

            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: "stitching_channel_change_fail_popup",
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData
                    }
                },
                visible: true
            });
        }

        /**
         * OK Key에 대한 동작을 수행한다.
         * focus 된 채널(stitching channel 중 하나) 정보를 기준으로 해당 채널이 유효한 경우 tune을 시도한다.
         *
         * @private
         */
        function _keyOk() {
            log.printDbg("_keyOk()");

            var channel;

            // ots 의 경우 스카이 채널링이 있으면 sky 채널링에서만 찾는다.
            // 만약 sky 채널링이 없는 경우에는 kt 채널링에서 찾는다.
            // otv 는 kt 채널링만 탐색~
            // getChannelByNum API에서 채널링 검사를 진행하기 때문에 첫파라미터로 olleh tv, 두번째 파라미터로 skylife 를 넘겨준다
            channel = navAdapter.getChannelByNum(channelInformation[focusIndex][0], channelInformation[focusIndex][1]);

            if (channel == null || channel == undefined) {
                _showPopup();
            } else {
                if (stitchingChannelControl) {
                    _removeListener();
                }
                // 먼저 element의 visibility를 hidden으로 변경한다.
                (document.getElementById("stitching_background_div")).style.visibility = "hidden";
                (document.getElementById("stitching_channel_info")).style.visibility = "hidden";

                _stopStitching(true, channel);
            }
        }

        /**
         * StitchingChannelControl을 생성하고 채널 event 수신을 위한 listener 등록 등 초기화 함수
         *
         * @private
         */
        function _initStitching() {
            log.printDbg("_initStitching()");
            _startStitching();

            // stitching channel control에 listener 등록
            if (stitchingChannelControl) {
                _removeListener();
                _addListener();
            }

            // 노출될 화면 사이즈 및 channel tune
            stitchingChannelControl.setVideoSize(195, 120, 1530, 861);

            _tuneChannel();
        }

        /**
         * next/prev stitching channel tune
         * stitching channel은 3개이다.
         * UI 상으로는 페이지 전환처럼 되어 있지만, 실제로는 channel tune을 해야 한다.
         * channel 정보는 OC로 내려온다.
         *
         * @private
         */
        function _tuneChannel() {
            log.printDbg("_tuneChannel()");

            var targetChannel;

            if (currentChannelLocator === stitchingChannelData.locator) {
                log.printDbg("_tuneChannel(), same channel, so return");
                return;
            }

            isFocused = false;

            // 재생할 stitching channel locator 저장
            currentChannelLocator = stitchingChannelData.locator;
            if (currentChannelLocator !== null && currentChannelLocator !== undefined) {
                targetChannel = navAdapter.getChannelByTriplet(currentChannelLocator);
                //_startStitching();
                //
                //// stitching channel control에 listener 등록
                //if (stitchingChannelControl) {
                //    _removeListener();
                //    _addListener();
                //}

                // 노출될 화면 사이즈 및 channel tune
                //stitchingChannelControl.setVideoSize(195, 110, 1530, 861);
                stitchingChannelControl.changeChannel(targetChannel);
            }
        }

        function moveChannel (tmpFocusIdx) {
            log.printDbg("moveChannel(), focusIndex = " + focusIndex + ", tmpFocusIdx = " + tmpFocusIdx + ", pageIndex = " + pageIndex);

            var bTune = false;

            if (tmpFocusIdx < 0) {
                focusIndex = tmpFocusIdx + BASE_VALUE.MAX_CHANNEL;
                pageIndex = pageIndex - 1 < 0 ? vsData.length - 1 : pageIndex - 1;
                bTune = true;
            }
            else if (tmpFocusIdx >= BASE_VALUE.MAX_CHANNEL) {
                focusIndex = tmpFocusIdx - BASE_VALUE.MAX_CHANNEL;
                pageIndex = pageIndex + 1 > vsData.length - 1 ? 0 : pageIndex + 1;
                bTune = true;
            }
            else {
                focusIndex = tmpFocusIdx;
            }

            if (bTune) {
                stitchingChannelData = vsData[pageIndex];
                //(document.getElementById("stitching_channel_info")).style.visibility = "hidden";
                _tuneChannel();
            }

            _setFocus();

            _setAudioComponent();
        }

        /**
         * stitching vbo를 생성하고 stitching channel 로 tune 하기 이전 채널을 저장한다.
         *
         * @private
         */
        function _startStitching() {
            log.printDbg("_startStitching()");

            if (mainPrevChannel === null || mainPrevChannel === undefined) {
                navAdapter.stopChannel();
                stitchingChannelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.STITCHING, true);

                // TODO 2016.08.03 dhlee
                // DOM Tree 상에 생성되는 element 들에 대해서 정리해야 한다.
                // Layer가 생성될 때 해당 Layer의 id는 Layer#create() 에서 set 하고 있다.
                // Layer ID를 정리해야 한다.
                if(!document.getElementById("stitching_vbo_object")) {
                    stitchingChannelControl.vbo.setParentElement("stitching_vbo_div");
                }

                stitchingChannelControl.start();
                mainPrevChannel = KTW.oipf.AdapterHandler.extensionAdapter.getRealCurrentChannel();

                if (mainPrevChannel === null) {
                    try {
                        mainPrevChannel = navAdapter.getChannelControl(KTW.nav.Def.CONSTANT.MAIN, false).vbo.currentChannel;
                        if (mainPrevChannel != null) {
                            log.printDbg("_startStitching(), main_vbo.currentChannel = " + JSON.stringify(mainPrevChannel));
                        }
                    } catch (e) {
                        log.printDbg(e);
                        mainPrevChannel = navAdapter.getChannelControl(KTW.nav.Def.CONSTANT.MAIN, false).getCurrentChannel();
                        if (mainPrevChannel != null) {
                            log.printDbg("_startStitching(), main_vbo.getCurrentChannel() = " + JSON.stringify(mainPrevChannel));
                        }
                    }
                }
            }
        }

        /**
         * stitching channel 서비스를 중지하고 사용자가 선택한 channel 로 tune 한다.
         *
         * 실시간 인기 채널 종료시 수행작업
         * - 실시간 인기채널 중지
         * - 메인 vbo 채널 튠
         *   1. 실시간 인기 채널에서 특정 채널을 튠하는 경우 (두 가지 경우 모두 미니EPG 생성됨)
         *      - 동일 채널인 경우 강제로 튠
         *      - 동일 채널이 아닌 경우 일반 튠
         *   2. 그외의 경우
         *      - 채널업다운, 예약, DCA, 이전키, 핫키, 종료키, 시청시간제한, 대기모드 등
         *        위 상황에서는 현재 실제 메인 vbo 채널정보와 실시간 인기채널 진입전 채널을 비교하여
         *        같을 경우 같은 채널이라 간주하여 튠처리를 하고, 다를 경우 외부에서 튠이 이미 된상태로 간주하여 튠처리 안함
         *        R5에서는 홈포탈이 실시간인기채널를 관리하면서 레이어가 추가되었음.(스택타입)
         *        실시간 인기 채널 레이어 위에 이전 스택을 살리면서 뜨는 메뉴 또는 App으로 인해 이전키를 누르면
         *        이전 스택으로 이동하면서 실시간 인기채널이 다시 뜨는 문제가 있음.
         *        검색, 마이메뉴, TV다시보기 등 메뉴가 뜨는 경우는 controlKey에서 핫키 처리로 해결
         *        쇼핑, 위젯 등 App이 뜨는 경우는 키처리를 홈포탈이 하지 않기 때문에 obs_hide message 이벤트로 처리
         *
         * @param isSelect   true: 사용자가 특정 채널을 선택해서 tune을 해야 한다.
         * @param channel
         * @private
         */
        function _stopStitching(isSelect, channel) {
            log.printDbg("_stopStitching(), isSelect = " + isSelect);

            // TODO 2016.08.03 Dongho Lee
            // 로직 및 관련 시나리오 분석이 더 필요하다. (50% 이해함)
            if (mainPrevChannel != null || mainPrevChannel != undefined) {
                var runningStitchingCC = true;
                if (stitchingChannelControl) {
                    if (stitchingChannelControl.getState() === KTW.nav.Def.CONTROL.STATE.STOPPED) {
                        runningStitchingCC = false;
                    } else {
                        stitchingChannelControl.stop();
                    }
                }

                if (isSelect === true && (channel != null && channel != undefined)) {
                    if (channel.ccid === mainPrevChannel.ccid) {
                        log.printDbg("_stopStitching(), channel.ccid === mainPreChannel.ccid");
                        navAdapter.changeChannel(channel, undefined, true);
                    } else {
                        log.printDbg("_stopStitching(), channel.ccid != mainPrevChannel.ccid");
                        navAdapter.changeChannel(channel);
                    }
                    // 실시간 인기 채널에서 선택하여 채널튠하는 상태를 기억 (why???)
                    isTuned = true;
                    navAdapter.removeChannelControl(KTW.nav.Def.CONTROL.STITCHING);
                } else {
                    if (isTuned === false) {
                        var realCurrentChannel = KTW.oipf.AdapterHandler.extensionAdapter.getRealCurrentChannel();
                        if (realCurrentChannel === null) {
                            realCurrentChannel = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN, false).vbo.currentChannel;
                        }
                        /**
                         * 1. 대기모드로 진입
                         * - 채널 중지 자동으로 진행. 동작모드로 전환 시 자동으로 채널 튠. 즉, 대기모드 진입하는 경우는 이부분으로 진입하지 않도록 처리
                         * 2. 시청시간제한 진입
                         * - 시청시간제한 진입시 가장 먼저 obs_hide message가 전달되고 StichingView의 hide가 호출되어 아래 로직에 진입하게 됨.
                         *   이전 채널과 동일하기 때문에 시청시간제한 아이프레임이 나오기전 잠깐 메인 AV가 보일 수 있음(R4 실시간 인기채널 에서도 동일)
                         */
                        if (KTW.managers.device.PowerModeManager.powerState !== KTW.oipf.Def.CONFIG.POWER_MODE.ACTIVE_STANDBY) {
                            if (realCurrentChannel && mainPrevChannel.ccid && (realCurrentChannel.ccid === mainPrevChannel.ccid)) {
                                log.printDbg("_stopStitching(), realCurrentChannel.ccid === mainPrevChannel.ccid");
                                navAdapter.changeChannel(mainPrevChannel, runningStitchingCC, true);
                            }
                        }
                        navAdapter.removeChannelControl(KTW.nav.Def.CONTROL.STITCHING);
                    }
                }
            }
        }

        /**
         * set audio component
         * should check connected bluetooth device
         *
         * @private
         */
        function _setAudioComponent() {
            log.printDbg("_setAudioComponent()");

            try {
                audioComponent = stitchingChannelControl.getComponents(COMPONENT_TYPE.COMPONENT_TYPE_AUDIO);
                try {
                    if (audioComponent && audioComponent.length == MAX_AUDIO_COMPONENT) {
                        log.printDbg("_setAudioComponent(), select component = " + (focusIndex+1));
                        // default audio device의 경우 별도로 설정하지 않는다.
                        stitchingChannelControl.selectComponent(audioComponent[focusIndex+1]);
                        if (bluetoothDevice.isAudioDeviceConnected() === true) {
                            log.printDbg("_setAudioComponent(), BT.isAudioDeviceConnected() is true");
                            // 연결된 BT 디바이스가 존재하면 BT 디바이스에 음성이 나오도록 설정한다.
                            stitchingChannelControl.selectComponent(audioComponent[focusIndex+1], bluetoothDevice.getBtAudioDevice());
                        }
                    }
                } catch (e) {
                    log.printDbg(e.message);
                }
            } catch (e) {
                log.printDbg(e.message);
            }
        }

        /**
         * 실시간 인기 채널에서 focus 된 채널의 정보를 획득한다.
         *
         * @private
         */
        function _getChannelInformation() {
            log.printDbg("_getChannelInformation()");
            var desc;
            var otvChannel = [0, 0];
            var otsChannel = [0, 0];
            var currentTime = "";
            var nextTime = "";

            try {
                if (!stitchingChannelControl) {
                    log.printDbg("_getChannelInformation() stitchingChannelControl is null");
                    // stitching vbo 객체가 없다면 return
                    return;
                }

                // 실시간 인기채널에서만 사용하기 때문에 vbo 직접 접근. 향후 이동될수도 있음
                desc = stitchingChannelControl.vbo.getPMTDescriptors(0x81);
                log.printDbg("_getChannelInformation : " + desc[0].contents);

                /** 총 60bytes
                 * for (i=0;i<9;i++) {
                 *      otv_ch_num       24      uimsbf (unsigned char 2bytes)
                 *      ots_ch_num       24      uimsbf (unsigned char 2bytes)
                 * }
                 * current_time         96      uimsbf (unsigned char 12bytes, YYYYMMDDHHMM)
                 * Next_time            96      uimsbf (unsigned char 12bytes, YYYYMMDDHHMM)
                 */
                for (var i = 0, j = 0; i < desc[0].contents.length; i++) {
                    if (i < 36) {
                        if (i % 4 < 2) {
                            otvChannel = otvChannel.concat([desc[0].contents[i]]);
                        } else if (i % 4 < 4) {
                            otsChannel = otsChannel.concat([desc[0].contents[i]]);
                        }

                        if (i % 4 == 3) {
                            channelInformation[j] = [util.convertToInt(otvChannel), util.convertToInt(otsChannel)];
                            otvChannel = [0,0];
                            otsChannel = [0,0];
                            j++;
                        }
                    } else {
                        if (i < 48) {
                            currentTime += (desc[0].contents[i] - 48);
                        } else {
                            nextTime += (desc[0].contents[i] - 48);
                        }
                    }
                }
            } catch(e) {
                log.printDbg(e.message);
            }
        }

        /**
         * PMT 변경 시 이벤트 처리
         */
        function onPMTChanged() {
            log.printDbg("onPMTChanged");
            isFocused = true;

            setTimeout(function() {
                _setAudioComponent();
                _getChannelInformation();
            }, 100);
        }

        /**
         * Stitching 채널 변경 시 이벤트 처리
         *
         * Stitching 채널은 현재 웹 홈포털 기준으로 3개의 채널이 제공되고 있다.
         * 상/하 방향키 등을 통해서 다음 페이지로 넘어갈 때 실제로는 다음 stitching channel로 tune 하게 된다.
         *
         * @param channelEvent
         */
        function onChannelChanged(channelEvent) {
            log.printDbg("onChannelChanged(), channelEvent = " + JSON.stringify(channelEvent));

            if (util.isValidVariable(channelEvent.blocked_reason) === true) {
                log.printDbg("onChannelChanged() error state = " + channelEvent.blocked_reason);

                // 채널 전환 시 error 발생한 경우 error popup 노출
                // TODO 2016.08.03 Dongho Lee
                // 추후 showErrorPopup()을 그대로 사용할지 확인 후 수정이 필요하다면 수정해야 함
                // UI 릴리즈 후 코드 다시 정리해야 함.

                KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.EX_CODE.HOME.EX004, null, function() {
                    log.printDbg("error popup callback");

                    // error popup이 닫히면 실시간 인기채널을 종료
                    KTW.ui.LayerManager.deactivateLayer({id: parent.id, remove: false});
                }, false);
            }
            else {
                log.printDbg("onChannelChanged() success");

                // 채널 전환 성공 시 stitching_channel_info group의 visibility를 inherit으로 변경
                (document.getElementById("stitching_title_text")).innerText = stitchingChannelData.title;
                (document.getElementById("stitching_channel_info")).style.visibility = "inherit";
            }
        }

        /**
         * listener 등록
         *
         * @private
         */
        function _addListener() {
            log.printDbg("_addListener()");

            // Bluetooth 장치가 연결여부 callback listener 등록
            // connect/disconnect event 전달 시 audio component를 새롭게 select 처리함
            // TODO 단, 실제로는 connect 되었을때만 처리하면 될 것 같은데
            // 우선은 audio bluetooth 장치가 매번 연결/해제 될때마다 처리하도록 함
            bluetoothDevice.setConnectionListener(_setAudioComponent);

            if (stitchingChannelControl) {
                stitchingChannelControl.addEventListener("PMTChanged", onPMTChanged);
                stitchingChannelControl.addChannelEventListener(onChannelChanged);
            }
        }

        /**
         * listener 해제
         *
         * @private
         */
        function _removeListener() {
            log.printDbg("_removeListener()");

            // Bluetooth 장치가 연결여부 callback listener 등록
            bluetoothDevice.setConnectionListener(null);

            if (stitchingChannelControl) {
                stitchingChannelControl.removeEventListener("PMTChanged", onPMTChanged);
                stitchingChannelControl.removeChannelEventListener(onChannelChanged);
            }
        }
    }
})();