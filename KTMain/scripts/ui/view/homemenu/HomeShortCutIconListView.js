"use strict";

(function() {
    var util = KTW.utils.util;
    var log = KTW.utils.Log;

    var data = null;
    var focusIndex = 0;
    var bFocus = false;

    var div = null;
    var parent = null;

    var otmaddClassName = null;
    
    var ShortCutIconList = [
        {className : "my_menu" , menuKorName : "마이 메뉴" , menuEngName : "My Menu" , divObj : null , menuId : ""},
        {className : "search_menu" , menuKorName : "" , menuEngName : "" , divObj : null , menuId : ""},
        {className : "favorite_menu" , menuKorName : "" , menuEngName : "" , divObj : null , menuId : "T000000000013"},
        {className : "recommend_menu" , menuKorName : "" , menuEngName : "" , divObj : null, menuId : "MENU07003"},
        {className : "noti_alram_menu" , menuKorName : "" , menuEngName : "" , divObj : null, menuId : "MENU00700"},
        {className : "olleh_tv_mobile_menu" , menuKorName : "올레 tv 모바일" , menuEngName : "olleh tv mobile" , divObj : null, menuId : ""},
        {className : "setting_menu" , menuKorName : "" , menuEngName : "" , divObj : null , menuId : ""}
    ];




    function initData () {
        log.printDbg("initData()");

    }



    function initFocusIndex () {
        focusIndex = 0;

    }

    function setFocus () {
        ShortCutIconList[focusIndex].divObj.rootDiv.addClass("focus");
        if(focusIndex === 5) {
            parent.showOtmPairingNotiMessage();
        }else {
            parent.hideOtmPairingNotiMessage();
        }
    }

    function setUnFocus () {
        ShortCutIconList[focusIndex].divObj.rootDiv.removeClass("focus");
    }

    function _focus () {
        log.printDbg("_focus()");

        div.addClass("focus");

        otmaddClassName = parent.getOTMClassName();

        if (otmaddClassName !== null) {
            ShortCutIconList[5].divObj.shortCutIcon.removeClass("connect1 connect2 disconnect").addClass(otmaddClassName);
            ShortCutIconList[5].divObj.shortCutName.removeClass("connect1 connect2 disconnect").addClass(otmaddClassName);
            ShortCutIconList[5].divObj.isConnectIcon.removeClass("connect1 connect2 disconnect").addClass(otmaddClassName);
            ShortCutIconList[5].divObj.isConnectIcon.attr("src", getOtmIcon(otmaddClassName));
        }
        
        setFocus();

        /**
         * [sw.nam]
         * 0-depth 좌측 메뉴 focusing 된 상태이면 otmPairing 푸시 메시지를 수신하여 icon을 업데이트 한다.
         */
        KTW.managers.otmPairing.OTMPManager.addPairingStateListener(otmIconChangeListener);

    }


    function otmIconChangeListener() {
        log.printDbg("otmIconChangeListener");
        otmaddClassName = parent.getOTMClassName();

        if (otmaddClassName !== null) {
            ShortCutIconList[5].divObj.shortCutIcon.removeClass("connect1 connect2 disconnect").addClass(otmaddClassName);
            ShortCutIconList[5].divObj.shortCutName.removeClass("connect1 connect2 disconnect").addClass(otmaddClassName);
            ShortCutIconList[5].divObj.isConnectIcon.removeClass("connect1 connect2 disconnect").addClass(otmaddClassName);
            ShortCutIconList[5].divObj.isConnectIcon.attr("src", getOtmIcon(otmaddClassName));
        }

    }

    function _blur () {
        log.printDbg("_blur()");
        setUnFocus();

        div.removeClass("focus");

        if(otmaddClassName !== null) {
            ShortCutIconList[5].divObj.shortCutIcon.removeClass(otmaddClassName);
            ShortCutIconList[5].divObj.shortCutName.removeClass(otmaddClassName);
            ShortCutIconList[5].divObj.isConnectIcon.removeClass(otmaddClassName);
        }
        initFocusIndex();

        KTW.managers.otmPairing.OTMPManager.removePairingStateListener(otmIconChangeListener);

        parent.hideOtmPairingNotiMessage();
    }

    function move (direction) {
        log.printDbg("move(" + direction + ")");

        setUnFocus();

        if (direction === "down") {
            focusIndex++;

            if (focusIndex >= ShortCutIconList.length) {
                focusIndex = 0;
            }

            if (KTW.CONSTANT.IS_BIZ && ShortCutIconList[focusIndex].className === "olleh_tv_mobile_menu") {
                ++focusIndex;

                if (focusIndex >= ShortCutIconList.length) {
                    focusIndex = 0;
                }
            }
        }
        else {
            focusIndex--;

            if (focusIndex < 0) {
                focusIndex = ShortCutIconList.length - 1;
            }

            if (KTW.CONSTANT.IS_BIZ && ShortCutIconList[focusIndex].className === "olleh_tv_mobile_menu") {
                --focusIndex;

                if (focusIndex >= ShortCutIconList.length) {
                    focusIndex = 0;
                }
            }
        }

        setFocus();
    }

    function keyOk (keyCode) {
        var focusMenu = null;
        log.printDbg("keyOk() focusIndex : " + focusIndex);
        focusMenu = getIconFocusMenuData(focusIndex);
        
        if(focusMenu !== null) {
            log.printDbg("keyOk() getIconFocusMenuData() return focusMenu ok");
            if (!focusMenu) {
                log.printDbg("menu data is not find... so return... (focusIndex : " + focusIndex + ")");
                return;
            }

            KTW.managers.service.MenuServiceManager.jumpMenu({
                menu: focusMenu,
                jump: false,
                cbJumpMenu: function () {}
            });

            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.HOME_MENU,
                act: keyCode,
                catId: focusMenu.id,
                catName: focusMenu.name
            });
        }else {
            /**
             * 이 부분 추후 추가 되어야 함.
             */

            if(focusIndex !== 5) {
                var searchMenuId = ShortCutIconList[focusIndex].menuId;
                if(searchMenuId != null && searchMenuId !== "") {

                    log.printDbg("keyOk() focusIndex : " + focusIndex + " , searchMenuId : " + searchMenuId);

                    var searchMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                        cbCondition: function (tmpMenu) {
                            if (tmpMenu.id === searchMenuId) {
                                log.printDbg("search OK tmpMenu.name : " + tmpMenu.name + " , searchId : " + searchMenuId);
                                return true;
                            }
                        }
                    })[0];
                    if(searchMenu !== undefined && searchMenu !== null) {
                        KTW.managers.service.MenuServiceManager.jumpMenu({
                            menu: searchMenu,
                            jump: false,
                            cbJumpMenu: function () {}
                        });

                        KTW.managers.UserLogManager.collect({
                            type: KTW.data.UserLog.TYPE.HOME_MENU,
                            act: keyCode,
                            catId: searchMenu.id,
                            catName: searchMenu.name
                        });
                    }
                }
                
            }else {
                log.printDbg("go to OTMP");
                KTW.managers.module.ModuleManager.getModuleForced(
                    KTW.managers.module.Module.ID.MODULE_FAMILY_HOME,
                    function(subhomeModule) {
                        if (subhomeModule) {
                            subhomeModule.execute({
                                method: "showOTMParing",
                                params: {
                                }
                            });
                        } else {
                        }
                    }
                );
            }
        }
    }

    function getIconFocusMenuData (iconFocusIdx) {
        log.printDbg("getIconFocusMenuData()");

        var userMenu = null;
        var CAT_TYPE = "";

        switch (iconFocusIdx) {
            case 0:
                CAT_TYPE = KTW.DATA.MENU.CAT.SUBHOME;
                break;
            case 1:
                CAT_TYPE = KTW.DATA.MENU.CAT.SRCH;
                break;
            case 6:
                CAT_TYPE = KTW.DATA.MENU.CAT.CONFIG;
                break;
        }

        var tmpMenuData = KTW.managers.data.MenuDataManager.getMenuData();

        if (tmpMenuData && tmpMenuData.length > 0 && CAT_TYPE !== "") {
            for (var i = 0; i < tmpMenuData.length; i++) {
                if (tmpMenuData[i].catType === CAT_TYPE) {
                    userMenu = tmpMenuData[i];
                    log.printDbg("getIconFocusMenuData() iconFocusIdx : " + iconFocusIdx + " , name : " + userMenu.name + " , engname : " + userMenu.englishItemName);
                    break;
                }
            }
        }

        return userMenu;
    }

    function getOtmIcon(className) {
        var isEnglish = parent.getLanguage() === "eng" ? true : false;
        var srcImagePath = "";
        if(className === "disconnect") {
            if(isEnglish) {
                srcImagePath = "images/home/icon_pairing_off_eng.png";
            }else {
                srcImagePath = "images/home/icon_pairing_off.png";
            }
        }
        return srcImagePath;
    }

    KTW.ui.view.HomeShortCutIconListView = function(tmpParent) {
        parent = tmpParent;
        otmaddClassName = parent.getOTMClassName();
        this.create = function () {
            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class : "shortcut_icon_list_view",
                    css: {
                        position: "absolute"
                    }
                },
                parent: parent.getDiv()
            });

            for(var i=0;i<(ShortCutIconList.length - 1); i++) {
                /**
                 * .shortcut_icon_list_view.focus {left:69px;top:110px;}
                 *  첫번째 라인 위치 : 231
                 *  두뻔째 라인 위치 : 354
                 */
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class : "home_line",
                        css: {
                            position: "absolute" , top : ((231 - 110) + (354-231)*i)
                        }
                    },
                    parent: div
                });
            }

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class : "ar_back",
                    css: {
                        position: "absolute"
                    }
                },
                parent: div
            });
            var isEnglish = parent.getLanguage() === "eng" ? true : false;
            for(var i=0;i<ShortCutIconList.length; i++) {
                var shortCutMenu = getIconFocusMenuData(i);

                if (shortCutMenu !== null) {
                    log.printDbg("create() i : " + i + " , name : " + shortCutMenu.name + " , engname : " + shortCutMenu.englishItemName);
                    if(i !== 0) {
                        ShortCutIconList[i].menuKorName = shortCutMenu.name;
                        ShortCutIconList[i].menuEngName = shortCutMenu.englishItemName;
                    }
                }
                else {
                    var searchMenuId = ShortCutIconList[i].menuId;
                    if(searchMenuId != null && searchMenuId !== "") {

                        log.printDbg("create() i : " + i + " , searchMenuId : " + searchMenuId);

                        var searchMenu = KTW.managers.data.MenuDataManager.searchMenu({
                            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                            cbCondition: function (tmpMenu) {

                                if (tmpMenu.id === searchMenuId) {
                                    log.printDbg("search OK tmpMenu.name : " + tmpMenu.name + " , searchId : " + searchMenuId);
                                    return true;
                                }
                            }
                        })[0];
                        if(searchMenu !== undefined && searchMenu !== null) {
                            log.printDbg("searchMenu () i : " + i + " , name : " + searchMenu.name + " , engname : " + searchMenu.englishItemName);
                            
                            if(ShortCutIconList[i].className === "recommend_menu") {
                                ShortCutIconList[i].menuKorName = "추천";
                                
                            }else {
                                ShortCutIconList[i].menuKorName = searchMenu.name;
                            }
                            ShortCutIconList[i].menuEngName = searchMenu.englishItemName;
                        }
                    }
                }
                
                
                var rootdiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class : ShortCutIconList[i].className,
                        css: {
                            position: "absolute"
                        },
                        index: i,
                        'data-voiceable-action':"",
                        'data-voiceable-context': parent.getParent().id
                    },
                    parent: div
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class : "focus_box",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: rootdiv
                });

                ShortCutIconList[i].divObj = {rootDiv : rootdiv };

                
                if(ShortCutIconList[i].className === "olleh_tv_mobile_menu") {

                    if (KTW.CONSTANT.IS_BIZ) {
                        rootdiv.addClass("disabled");
                    }

                    var tempDiv = null;
                    tempDiv = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            class : otmaddClassName === null ? "short_cut_icon" : "short_cut_icon " +  otmaddClassName, 
                            css: {
                                position: "absolute"
                            }
                        },
                        parent: rootdiv
                    });

                    ShortCutIconList[i].divObj.shortCutIcon = tempDiv;

                    tempdiv = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            class : otmaddClassName === null ? "short_cut_name cut" : "short_cut_name cut " +  otmaddClassName,
                            css: {
                                position: "absolute"
                            }
                        },
                        text: isEnglish ? ShortCutIconList[i].menuEngName : ShortCutIconList[i].menuKorName,
                        parent: rootdiv
                    });

                    ShortCutIconList[i].divObj.shortCutName = tempdiv;

                    tempdiv = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            src: getOtmIcon(otmaddClassName),
                            class : otmaddClassName === null ? "is_connect_icon" : "is_connect_icon " + otmaddClassName,
                            css: {
                                position: "absolute"
                            }
                        },
                        parent: rootdiv
                    });

                    ShortCutIconList[i].divObj.isConnectIcon = tempdiv;

                    if(isEnglish) {
                        tempdiv.addClass("eng");
                    }
                    
                }else {
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            css: {
                                position: "absolute"
                            }
                        },
                        parent: rootdiv
                    });

                    var tempdiv = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            class : "cut" ,
                            css: {
                                position: "absolute"
                            }
                        },
                        text: isEnglish ? ShortCutIconList[i].menuEngName : ShortCutIconList[i].menuKorName,
                        parent: rootdiv
                    });

                    if(isEnglish) {
                        tempdiv.addClass("eng");
                    }

                    ShortCutIconList[i].divObj.shortCutName = tempdiv;
                }
            }
       };

        this.destroy = function () {
        };

        this.focus = function () {
            bFocus = true;
            _focus();
        };

        this.blur = function () {
            bFocus = false;
            _blur();
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey() keyCode : " + keyCode);
            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    move("up");
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    move("down");
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk(KTW.KEY_CODE.OK);
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.isFocusAvailable = function () {
            return true;
        };


        this.changeLanguage = function () {
            log.printDbg("changeLanguage()");
            var isEnglish = parent.getLanguage() === "eng" ? true : false;
            for(var i=0;i<ShortCutIconList.length; i++) {

                ShortCutIconList[i].divObj.shortCutName.text(isEnglish ? ShortCutIconList[i].menuEngName : ShortCutIconList[i].menuKorName);

                if(isEnglish) {
                    ShortCutIconList[i].divObj.shortCutName.addClass("eng");
                }else {
                    ShortCutIconList[i].divObj.shortCutName.removeClass("eng");
                }
            }
        };

        this.resume = function () {
            // setFocus();
            _focus();
        };

        this.clear = function () {
            div.children().remove();
        };

        this.onNodeMatchListener = function (element) {
            var el = $(element);
            var elementIndex = Number(el.attr("index"));

            setUnFocus();

            focusIndex = elementIndex;

            setFocus();

            setTimeout(function () {
                keyOk();
            }, 1 * 1000);
        };
    };


})();
