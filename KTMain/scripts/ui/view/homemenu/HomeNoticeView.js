"use strict";

KTW.ui.view.HomeNoticeView = function(parent) {
    //alias
    var util = KTW.utils.util;
    var log = KTW.utils.Log;

    var SPEED = 40;

    var noticeData = null;
    var pointData = null;

    var div = null;
    var parent = parent;

    var noticeObj = {};
    var pointObj = {};

    var testCount = 0;
    var homeMenuTestCount = 0;
    var shortcutIconTestCount = 0;

    function createElements() {
        div = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", overflow: "visible"
                }
            },
            parent: parent.div
        });

        noticeObj.rootDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "notice_area",
                css: {
                    position: "absolute", left: 390, top: 998, overflow: "visible", visibility: "hidden"
                }
            },
            parent: div
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 0, width: 60, height: 30,
                    "font-size": 25, "letter-spacing": -1.25, color: "rgba(255, 255, 255, 0.5)", "font-family": "RixHead M"
                }
            },
            text: "공지",
            parent: noticeObj.rootDiv
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 59, top: 2, width: 2, height: 22,
                    "background-color": "rgba(70, 70, 70, 1)"
                }
            },
            parent: noticeObj.rootDiv
        });
        noticeObj.noticebox = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "notice_box",
                css: {
                    position: "absolute", left: 78, top: 0, width: 904, height: 30, overflow: "hidden",
                    "white-space": "nowrap", "font-size": 25, "letter-spacing": -1.3, color: "rgba(180, 180, 180, 1)",
                    "font-family": "RixHead L"
                }
            },
            parent: noticeObj.rootDiv
        });
        noticeObj.noticespan = util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute"
                }
            },
            parent: noticeObj.noticebox
        });



        pointObj.rootDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "point_area",
                css: {
                    position: "absolute", left: 285, top: 999, width: 1100, height: 50, visibility: "hidden"
                }
            },
            parent: div
        })


        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    float: "left", "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(180, 180, 180, 0.5)", "font-family": "RixHead L"
                }
            },
            text: "KT 멥버십",
            parent: pointObj.rootDiv
        });

        pointObj.ktmembership = util.makeElement({
            tag: "<span />",
            attrs: {
                name: "kt_membership",
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(225, 163, 88, 0.5)", "font-family": "RixHead M"
                }
            },
            parent: pointObj.rootDiv
        });



        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    float: "left", "margin-left": 15, "margin-top": 1, width: 2, height: 22, "background-color": "rgba(56, 56, 56, 1)"
                }
            },
            parent: pointObj.rootDiv
        });



        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(180, 180, 180, 0.5)", "font-family": "RixHead L"
                }
            },
            text: "TV포인트",
            parent: pointObj.rootDiv
        });
        pointObj.tvpoint = util.makeElement({
            tag: "<span />",
            attrs: {
                name: "tv_point",
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(225, 163, 88, 0.5)", "font-family": "RixHead M"
                }
            },
            parent: pointObj.rootDiv
        });




        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    float: "left", "margin-left": 15, "margin-top": 1, width: 2, height: 22, "background-color": "rgba(56, 56, 56, 1)"
                }
            },
            parent: pointObj.rootDiv
        });



        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(180, 180, 180, 0.5)", "font-family": "RixHead L"
                }
            },
            text: "TV쿠폰",
            parent: pointObj.rootDiv
        });
        pointObj.tvpcoupon = util.makeElement({
            tag: "<span />",
            attrs: {
                name: "tv_coupon",
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(225, 163, 88, 0.5)", "font-family": "RixHead M"
                }
            },
            parent: pointObj.rootDiv
        });


        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    float: "left", "margin-left": 15, "margin-top": 1, width: 2, height: 22, "background-color": "rgba(56, 56, 56, 1)"
                }
            },
            parent: pointObj.rootDiv
        });


        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(180, 180, 180, 0.5)", "font-family": "RixHead L"
                }
            },
            text: "콘텐츠 이용권",
            parent: pointObj.rootDiv
        });

        pointObj.contentscoupon = util.makeElement({
            tag: "<span />",
            attrs: {
                name: "contents_coupon",
                css: {
                    float: "left", "margin-left": 15, "font-size": 22, "letter-spacing": -1.1,
                    color: "rgba(225, 168, 88, 0.5)", "font-family": "RixHead M"
                }
            },
            parent: pointObj.rootDiv
        });

        pointObj.checktime = util.makeElement({
            tag: "<span />",
            attrs: {
                name: "check_time",
                css: {
                    float: "left", "margin-left": 15, "font-size": 20, "letter-spacing": -1.0,
                    color: "rgba(108, 108, 108, 0.78)", "font-family": "RixHead L"
                }
            },
            parent: pointObj.rootDiv
        });

    }

    function initData () {
        // 공지사항 데이터 가져오기
        noticeData = KTW.managers.data.MenuDataManager.getEmergencyNotice();
        if (noticeData === "") {
            noticeData = null;
        }

        try {
            /**
             * [dj.son] ACAP 로직 적용, 비즈 상품인 경우 포인트 정보 미노출
             */
            if (KTW.CONSTANT.IS_BIZ) {
                pointData = null;
            }
            else {
                // 2017.06.08 dhlee
                // STBPOINT는 2.0에서만 사용하는 key 임
                //pointData = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.STBPOINT));
                var tempPointData = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.STB_POINT_CACHE));
                if (tempPointData && tempPointData.UpdateTime) {
                    if (tempPointData.StarPoint === null || tempPointData.StarPoint === undefined) {
                        tempPointData.StarPoint = 0;
                    }
                    if (tempPointData.KtCoupon === null || tempPointData.KtCoupon === undefined) {
                        tempPointData.KtCoupon = 0;
                    }
                    if (tempPointData.TvMoney === null || tempPointData.TvMoney === undefined) {
                        tempPointData.TvMoney = 0;
                    }
                    if (tempPointData.TvPoint === null || tempPointData.TvPoint === undefined) {
                        tempPointData.TvPoint = 0;
                    }
                    if (tempPointData.NumOfCoupon === null || tempPointData.NumOfCoupon === undefined) {
                        tempPointData.NumOfCoupon = 0;
                    }
                    pointData = tempPointData;
                }
            }
        }
        catch (e) {
            log.printErr(e.message);
            pointData = null;
        }
    }

    function makeNoticeMessage () {
        var result = "";
        
        // if(parent.getGuiTestCode()) {
        //     result = "KT 올레 tv 10, 15 , 25, 34 채널변경 공지 2015년 8월 30일부터 적용됩니다.다시한번 확인 해 주세요.서비스가 일시적으로 원활하지 않습니다.잠시 후 다시 이용해 주세요";
        // }else {
        //     if (noticeData) {
        //         var title = noticeData.title || "";
        //         var desc = noticeData.desc || "";
        //
        //         result = title + " " + desc.replace("\n", " ");
        //     }
        // }

        if (noticeData) {
            var title = noticeData.title || "";
            var desc = noticeData.desc || "";

            result = title + " " + desc.replace("\n", " ");
        }

        return result;
    }

    function setNotice (message) {
        noticeObj.noticespan.text(message);

        KTW.utils.util.clearAnimation(noticeObj.noticespan);
        KTW.utils.util.startTextAnimation({
            targetBox: noticeObj.noticebox,
            speed: SPEED,
            delayTime: 3
        });
    }


    function setPoint () {

        pointObj.ktmembership.text(util.setComma(pointData.StarPoint) + "P");
        pointObj.tvpcoupon.text(util.setComma((pointData.TvMoney-0) + (pointData.KtCoupon-0)) + "원");
        pointObj.tvpoint.text(util.setComma(pointData.TvPoint) + "P");
        pointObj.contentscoupon.text(util.setComma(pointData.NumOfCoupon) + "개");


        var checkDate = new Date(pointData.UpdateTime);

        pointObj.checktime.text(util.setComma("(" + KTW.utils.util.getFormatDate(checkDate, "yy.MM.dd HH") + "시 기준)"));
    }

    this.create = function() {
        // 공지사항 element 생성
        createElements();
    };
    
    this.show = function(options) {
        initData();
        log.printDbg("show() testCount : " + testCount);
        if(options === "home_menu_1") {
            noticeObj.rootDiv.css({left: 197});
            pointObj.rootDiv.css({left: 197});
        }else if(options === "home_menu_2") {
            noticeObj.rootDiv.css({left: 134});
            pointObj.rootDiv.css({left: 134});
        }else {
            noticeObj.rootDiv.css({left: 390});
            pointObj.rootDiv.css({left: 390});
        }

        if (noticeData !== null && noticeData !== "" && noticeData !== undefined) {
            setNotice(makeNoticeMessage());
            noticeObj.rootDiv.css({visibility: "inherit"});
            pointObj.rootDiv.css({visibility: "hidden"});
        }
        else {
            if (pointData) {
                setPoint();
                noticeObj.rootDiv.css({visibility: "hidden"});
                pointObj.rootDiv.css({visibility: "inherit"});
            }
            else {
                noticeObj.rootDiv.css({visibility: "hidden"});
                pointObj.rootDiv.css({visibility: "hidden"});
            }
        }

        div.css({visibility: "inherit"});
    };
    
    this.hide = function() {
        div.css({visibility: "hidden"});

        noticeObj.noticespan.off("webkitAnimationEnd").css({
            opacity: 1,
            "-webkit-transform": "",
            "-webkit-animation": ""
        });
    };
    
    this.destroy = function() {

        // 그 외 메모리 해제
    };

};