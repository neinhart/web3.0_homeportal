//"use strict";

KTW.ui.view.HomeMenuView = function(parent) {
    //alias
    var util = KTW.utils.util;
    var log = KTW.utils.Log;

    var MAX_SHOW_MENU = 9;

    var parent = parent;
    var div = null;

    // 0 : menuArea , 1 : short cut Icon Area
    var focusMode = 0; 
    var menuStartIdx = 0;
    var focusIndex = 0;
    var menuData = null;

    var shortcutIconListView = null;
    var homeNoticeView = null;
    var focusView = null;

    var menuListDiv = null;
    var menuPreviewListDiv = null;

    var language = "kor";
    var otmClassName = null;
    var otmclassTestCount = 0;
    
    var testPosterType = 0;
    var testStartIndex = 1;

    function initMenuData (bAutoExit) {
        log.printDbg("initMenuData() bAutoExit : " + bAutoExit);

        menuData = [];

        var tmpMenuData = KTW.managers.data.MenuDataManager.getMenuData();

        if (tmpMenuData && tmpMenuData.length > 0) {
            for (var i = 0; i < tmpMenuData.length; i++) {
                var tmpMenu = tmpMenuData[i];

                if (!tmpMenu.displayHidden) {
                    menuData.push(tmpMenu);
                    /**
                     * Test Code
                     */
                    // if(parent.getGuiTestCode()) {
                    //     if(testPosterType > 0) {
                    //         testPosterType = testStartIndex;
                    //         setCategoryBanner(i);
                    //         testStartIndex++;
                    //         if(testStartIndex > 9) {
                    //             testStartIndex = 1;
                    //         }
                    //     }
                    // }
                }
            }
        }



        /**
         * Test Code
         */
        
        testPosterType = 1;
    }

    function setCategoryBanner(idx) {
        log.printDbg("setCategoryBanner() idx : " + idx);

        var tempMenu = menuData[idx];
        var count = 0;

        if (!tempMenu) {
            log.printDbg("setCategoryBanner menu data is not find... so return... [idx] : " + idx + ") + menuData.length :  " + menuData.length );
            return;
        }

        var startType = testPosterType;

        for (var i = 0; i < tempMenu.children.length; i++) {
            var type = "0" + startType;
            var imagePath = "images/test/homemenu/banner_cate_" + startType + ".png";
            log.printDbg("setCategoryBanner() imagePath : " + imagePath);
            tempMenu.children[i].w3MenuImgType = type;
            tempMenu.children[i].w3ListImgUrl = imagePath;
            startType++;
            if(startType>9) {
                startType = 1;
            }
        }
    }
    
    function createHomeMenuElements () {
        log.printDbg("createHomeMenuElements()");

        menuListDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "home_menu_list",
                class: "font_l",
                css: {
                    position: "absolute", overflow: "visible"
                }
            },
            parent: div
        });

        menuPreviewListDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "home_menu_preview_list",
                css: {
                    position: "absolute"
                }
            },
            parent: div
        });

        for (var i = 0; i < MAX_SHOW_MENU; i++) {
            var homeMenu = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "home_menu",
                    css: {
                        position: "relative" 
                    },
                    index: i,
                    "data-voiceable-action": "",
                    "data-voiceable-context": parent.id
                },
                parent: menuListDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "menu_name cut",
                    css: {
                        position: "absolute"
                    }
                },
                parent: homeMenu
            });

            var tempdiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "focus_box focus_box_red_line",
                    css: {
                        position: "absolute"
                    }
                },
                parent: homeMenu
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/arw_menu_focus.png",
                    css: {
                        position: "absolute", left:332 ,top:27 ,width:19,height:31
                    }
                },
                parent: tempdiv
            });

        }

        util.makeElement({
            tag: "<img />",
            attrs: {
                class: "arw_down_img",
                src: "images/arw_menu_dw.png",
                css: {
                    position: "absolute", left: 4, top: 875, width: 33, height: 20, visibility: "hidden"
                }
            },
            parent: menuListDiv
        });
    }

    function drawMenu () {
        log.printDbg("drawMenu()");

        for (var i = 0; i < MAX_SHOW_MENU; i++) {
            var menu = menuData[menuStartIdx + i];
            var name = "";

            if (menu) {
                name = language === "eng" ? ((menu.englishItemName === undefined ||  menu.englishItemName === null) ? menu.name : menu.englishItemName) : menu.name;
                if (!name || name.length === "") {
                    name = menu.name;
                }
            }

            menuListDiv.children().eq(i).find(".menu_name").text(name);
        }
    }

    /**
     * 1-2Depath의 우측 메뉴 리스트 노출 해야 함.
     */
    function drawFocusMenu () {
        log.printDbg("drawFocusMenu()  , menuStartIdx : " + menuStartIdx + " , focusIndex : " + focusIndex);

        menuPreviewListDiv.children().remove();
        
        var menu = menuData[menuStartIdx + focusIndex];

        if (!menu) {
            log.printDbg("drawFocusMenu menu data is not find... so return... [menuStartIdx + focusIndex] : " + (menuStartIdx + focusIndex) + ") + menuData.length :  " + menuData.length );
            return;
        }
        
        log.printDbg("drawFocusMenu() focus menu name : " + menu.name + " , menu.id :  : " + menu.id );

        var tempMenuList = [];
        var childMenu = null;
        for (var i = 0; i < menu.children.length; i++) {
            childMenu = menu.children[i];
            if (childMenu.catType === KTW.DATA.MENU.CAT.APPGM) {
                continue;
            }
            else {
                tempMenuList.push(childMenu);
            }
        }

        var childrenLength = tempMenuList.length;

        for (var i = 0 , j = 0; i < MAX_SHOW_MENU && j < childrenLength; i++ , j++) {

            childMenu = tempMenuList[j];
            log.printDbg("drawFocusMenu() childmenu ID : " + childMenu.id + " , childmenu.name : "
                + childMenu.name + " , childmenu.w3MenuImgType : "
                + childMenu.w3MenuImgType + " , childmenu.w3ListImgUrl : " + childMenu.w3ListImgUrl);

            /**
             * 이미지 top에서 text top 까지 24
             */
            var tempspan = util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "cut",
                    css: {
                        position: "absolute" , top : (24 + (i*95))
                    }
                },
                parent: menuPreviewListDiv
            });


            var name = "";

            if (childMenu) {
                name = language === "eng" ? ((childMenu.englishItemName === undefined ||  childMenu.englishItemName === null) ? childMenu.name : childMenu.englishItemName) : childMenu.name;
                if (!name || childMenu.length === "") {
                    name = childMenu.name;
                }
            }

            tempspan.text(name);
            
            if (childMenu.w3MenuImgType && childMenu.w3MenuImgType !== "" && childMenu.w3ListImgUrl && childMenu.w3ListImgUrl !== "") {
                var count = 0;
                var iamgeHeight = 0;

                if (childMenu.w3MenuImgType === "01") {
                    iamgeHeight = 84;
                }
                else if (childMenu.w3MenuImgType === "02") {
                    count = 1;
                    iamgeHeight = 178;
                }
                else if (childMenu.w3MenuImgType === "03") {
                    count = 2;
                    iamgeHeight = 272;
                }
                else if (childMenu.w3MenuImgType === "04") {
                    count = 3;
                    iamgeHeight = 366;
                }
                else if (childMenu.w3MenuImgType === "05") {
                    count = 4;
                    iamgeHeight = 460;
                }
                else if (childMenu.w3MenuImgType === "06") {
                    count = 5;
                    iamgeHeight = 554;
                }
                else if (childMenu.w3MenuImgType === "07") {
                    count = 6;
                    iamgeHeight = 648;
                }
                else if (childMenu.w3MenuImgType === "08") {
                    count = 7;
                    iamgeHeight = 742;
                }
                else if (childMenu.w3MenuImgType === "09") {
                    count = 8;
                    iamgeHeight = 836;
                }

                if (MAX_SHOW_MENU - i > count) {
                    util.makeElement({
                        tag: "<img />",
                        attrs: {
                            src: childMenu.w3ListImgUrl,
                            css: {
                                position: "absolute", top:i*95 ,height: iamgeHeight
                            }
                        },
                        parent: menuPreviewListDiv
                    });
                    i += count;
                }
                else {
                    tempspan.css({visibility: "inherit"});
                }

            }
            else {
                tempspan.css({visibility: "inherit"});
            }
        }

    }

    function setFocus () {
        log.printDbg("setFocus()");

        if (focusMode === 0) {
            menuListDiv.addClass("focus");
            menuPreviewListDiv.addClass("focus");

            menuListDiv.children().removeClass("focus");
            menuListDiv.children().eq(focusIndex).addClass("focus");
        }
        else {
            menuListDiv.removeClass("focus");
            menuPreviewListDiv.removeClass("focus");
        }
    }

    function setFocusView (view) {
        log.printDbg("setFocusView()");

        if (focusView === view) {
            return;
        }

        if (focusView) {
            focusView.blur();
        }

        focusView = view;
        focusView.focus();
    }

    function keyUp () {
        log.printDbg("keyUp() menuStartIdx : " + menuStartIdx + " , focusIndex : " + focusIndex);

        var focusMenu = menuData[menuStartIdx + focusIndex];

        if (!focusMenu) {
            log.printDbg("menu data is not find... so return... [menuStartIdx + focusIndex] : " + (menuStartIdx + focusIndex) + ")");
            return;
        }

        var curMenu = focusMenu;

        if (focusMode === 0) {
            focusIndex--;

            if (focusIndex < 0) {
                if (menuData.length < MAX_SHOW_MENU) {
                    focusIndex = menuData.length - 1;
                }
                else {
                    menuStartIdx--;

                    if (menuStartIdx < 0) {
                        menuStartIdx = menuData.length - MAX_SHOW_MENU;
                        focusIndex = (MAX_SHOW_MENU - 1);
                    }
                    else {
                        focusIndex = 0;
                    }
                    drawMenu();
                }
            }
            drawFocusMenu();
            setFocus();
        }
        

        KTW.managers.UserLogManager.collect({
            type: KTW.data.UserLog.TYPE.HOME_MENU,
            act: KTW.KEY_CODE.UP,
            contsId: curMenu.id,
            contsName: curMenu.name,
            catId: "",
            catName: ""
        });
    }

    function keyDown () {
        log.printDbg("keyDown() menuStartIdx : " + menuStartIdx + " , focusIndex : " + focusIndex);

        var focusMenu = menuData[menuStartIdx + focusIndex];

        if (!focusMenu) {
            log.printDbg("menu data is not find... so return... [menuStartIdx + focusIndex] : " + (menuStartIdx + focusIndex) + ")");
            return;
        }

        var curMenu = focusMenu;

        if (focusMode === 0) {
            focusIndex++;

            if (menuData.length < MAX_SHOW_MENU) {
                if (focusIndex > menuData.length - 1) {
                    focusIndex = 0;
                }
            }
            else {
                if (focusIndex >= MAX_SHOW_MENU) {
                    menuStartIdx++;
                    focusIndex = MAX_SHOW_MENU - 1;
                    if ((menuStartIdx + focusIndex) >= menuData.length) {
                        menuStartIdx = 0;
                        focusIndex = 0;
                    }
                    drawMenu();
                }
            }

            drawFocusMenu();
            setFocus();
        }


        KTW.managers.UserLogManager.collect({
            type: KTW.data.UserLog.TYPE.HOME_MENU,
            act: KTW.KEY_CODE.DOWN,
            contsId: curMenu.id,
            contsName: curMenu.name,
            catId: "",
            catName: ""
        });
    }

    function keyLeft () {
        log.printDbg("keyLeft()");

        var userMenu;

        if (focusView === shortcutIconListView) {
            return ;
        }


        if (focusMode === 0  ) {
            if (shortcutIconListView.isFocusAvailable()) {
                setFocusView(shortcutIconListView);
                // 2017.05.04 dhlee 공지사항 내용을 포인트로 교체
                homeNoticeView.show("short_cut_icon");

                if (focusMode === 0) {
                    userMenu = menuData[menuStartIdx + focusIndex];
                }
                parent.menuUnFocus();
                focusMode = 1;
                setFocus();
            }
        }
       

        if (userMenu) {
            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.HOME_MENU,
                act: KTW.KEY_CODE.LEFT,
                contsId: userMenu.id,
                contsName: userMenu.name,
                catId: "",
                catName: ""
            });
        }
    }

    //TODO sw.nam 홈메뉴 좌측에서 우측으로 넘어올 때 로그 규격 적재 해야 하나 ? 하면 어떻게 ?
    function keyRight (_this) {
        log.printDbg("keyRight()");

        if (focusView == shortcutIconListView) {
            focusMode = 0;
            setFocusView(_this);

            // 2017.05.04 dhlee 포인트를 공지사항으로 교체
            if (menuData.length > MAX_SHOW_MENU) {
                homeNoticeView.show("home_menu_1");
            }
            else {
                homeNoticeView.show("home_menu_2");
            }

/*            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.HOME_MENU,
                act: KTW.KEY_CODE.RIGHT,
                catId: "",
                catName: ""
            });*/
            parent.menuFocus();
            setFocus();
        }
        else {
            if (focusMode === 0) {
                keyOk(KTW.KEY_CODE.RIGHT);
            }
            
        }
    }

    function keyOk (keyCode) {
        log.printDbg("keyOk()");

        var focusMenu = menuData[menuStartIdx + focusIndex];

        if (!focusMenu) {
            log.printDbg("menu data is not find... so return... (menuStartIdx + focusIndex : " + (menuStartIdx + focusIndex) + ")");
            return;
        }

        KTW.managers.service.MenuServiceManager.jumpMenu({
            menu: focusMenu,
            jump: false,
            cbJumpMenu: function () {}
        });

        KTW.managers.UserLogManager.collect({
            type: KTW.data.UserLog.TYPE.HOME_MENU,
            act: keyCode,
            contsId: focusMenu.id,
            contsName: focusMenu.name,
            catId: "",
            catName: ""
        });
    }

    function setLanguage (lang) {
        log.printDbg("setLanguage() lang : " + lang);

        if (lang) {
            language = lang;
        }
        else {
            var tmpLanguage = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.LANGUAGE);
            if (tmpLanguage === "kor" || tmpLanguage === "eng") {
                language = tmpLanguage;
            }
        }
    }

    function onStorageChange (evt) {
        log.printDbg("onStorageChange() evt.key : " + evt.key);

        if (evt.key === KTW.oipf.Def.CONFIG.KEY.LANGUAGE) {
            setLanguage(evt.value);
            drawMenu();
            drawFocusMenu();
            shortcutIconListView.changeLanguage();
        }
    }
    


    this.create = function() {
        log.printDbg("create()");

        div = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", overflow: "visible"
                }
            },
            parent: parent.div
        });
        setLanguage();

        // shortcutIconList 생성
        shortcutIconListView = new KTW.ui.view.HomeShortCutIconListView(this);
        shortcutIconListView.create();

        // 2017.05.04 dhlee noticeView 생성
        homeNoticeView = new KTW.ui.view.HomeNoticeView(parent);
        homeNoticeView.create();

        // 홈 메뉴 element 생성
        createHomeMenuElements();

        KTW.oipf.AdapterHandler.basicAdapter.addConfigChangeEventListener(onStorageChange);
    };

    this.getDiv = function() {
        return div;
    };

    this.getParent = function() {
        return parent;
    };

    this.show = function(options) {
        log.printDbg("show(" + log.stringify(options) + ")");

        KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);
        KTW.managers.service.IframeManager.changeIframe();

        setLanguage();

        if (!options || !options.resume) {
            // [dj.son] 어차피 UI 구조상 매번 element 생성해야하기 때문에, 매번 메뉴 데이터 가져오는 걸로 수정
            initMenuData();

            focusMode = 0;
            menuStartIdx = 0;
            focusIndex = 0;

            setFocus();
            drawMenu();
            drawFocusMenu();

            if (menuData.length > MAX_SHOW_MENU) {
                div.find(".arw_down_img").css({visibility: ""});
            }
            else {
                div.find(".arw_down_img").css({visibility: "hidden"});
            }
        }
        else {
            if (focusView === shortcutIconListView) {
                shortcutIconListView.resume();
            }
        }

        // 메뉴 포커스
        setFocusView(focusView || this);
        if (focusView === shortcutIconListView) {
            homeNoticeView.show("short_cut_icon");
        }
        else {
            if (menuData.length > MAX_SHOW_MENU) {
                homeNoticeView.show("home_menu_1");
            }else {
                homeNoticeView.show("home_menu_2");
            }
        }
    };

    this.hide = function(options) {
        if (!options || !options.pause) {
            focusView = null;
            this.blur();
            shortcutIconListView.blur();
        }

        // 2017.05.04 dhlee 공지사항 hide
        homeNoticeView.hide(options);
    };

    this.focus = function () {
        drawFocusMenu();
        setFocus();
    };

    this.blur = function () {
        drawFocusMenu();
        setFocus();
    };

    this.destroy = function () {
        homeNoticeView.destroy();
        homeNoticeView = null;
    };

    this.controlKey = function (keyCode) {
        log.printDbg("controlKey() keyCode : " + keyCode);
        var consumed = false;

        if (focusView == shortcutIconListView) {
            consumed = shortcutIconListView.controlKey(keyCode);
        }

        if (!consumed) {
            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    keyLeft();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk(keyCode);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    KTW.ui.LayerManager.historyBack();
                    consumed = true;
                    break;
            }
        }

        return consumed;
    };


    this.getLanguage = function () {
        return language;
    };
    
    this.getOTMClassName = function() {
        var list = KTW.managers.otmPairing.OTMPManager.getPairingList();

        if (list === undefined || list === null) {
            otmClassName = null;
        }
        else {
            if (list.length > 0) {
                if (list.length>1) {
                    otmClassName = "connect2";
                }
                else {
                    otmClassName =  "connect1";
                }
            }
            else {
                otmClassName = "disconnect";
            }
        }
        return otmClassName;
    };

    this.showOtmPairingNotiMessage = function () {
        parent.showOtmPairingNotiMessage();
    };

    this.hideOtmPairingNotiMessage = function () {
        parent.hideOtmPairingNotiMessage();
    };

    this.onNodeMatchListener = function (element) {
        var el = $(element);

        if (el.hasClass("home_menu")) {
            if (focusMode !== 0) {
                keyRight(this);
            }

            var elementIndex = Number(el.attr("index"));

            focusIndex = menuStartIdx + elementIndex;

            drawFocusMenu();
            setFocus();

            setTimeout(function () {
                keyOk();
            }, 0);
        }
        else {
            if (focusMode === 0) {
                keyLeft();
            }

            shortcutIconListView.onNodeMatchListener(element);
        }
    };
};