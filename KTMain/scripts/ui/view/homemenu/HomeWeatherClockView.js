"use strict";

KTW.ui.view.HomeWeatherClockView = function(parent) {
    //alias
    var util = KTW.utils.util;
    var log = KTW.utils.Log;

    var weatherData = null;
    var timer = null;
    var clockCount = 0;

    var div = null;
    var parent = parent;

    var divObj = {};

    function createElements() {
        div = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", overflow: "visible"
                }
            },
            parent: parent.div
        });


        // Weather & Clock
        divObj.rootDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 1456, top: 42, width: 391, height: 55,
                    opacity: 0, "-webkit-transform": "translateZ(0)"
                }
            },
            parent: div
        });



        divObj.clockmin1 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_min_1",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 0
                }
            },
            parent: divObj.rootDiv
        });

        divObj.clockmin0 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_min_0",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 1
                }
            },
            parent: divObj.rootDiv
        });

        divObj.clockcolon = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_colon",
                src: "images/icon/timenumber/h_img_time_home.png",
                css: {
                    float: "right", "margin-top": 20, "margin-right": 4
                }
            },
            parent: divObj.rootDiv
        });

        divObj.clockhour1 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_hour_1",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 6
                }
            },
            parent: divObj.rootDiv
        });

        divObj.clockhour0 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_hour_0",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 1
                }
            },
            parent: divObj.rootDiv
        });


        divObj.divisionbar = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "division_bar",
                css: {
                    float: "right", "margin-top": 15, "margin-right": 20, width: 2, height: 26, "background-color": "rgba(255, 255, 255, 0.2)" , display : "none"
                }
            },
            parent: divObj.rootDiv
        });

        divObj.weathericon = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "weather_icon",
                css: {
                    float: "right", "margin-right": 20 , display : "none"
                }
            },
            parent: divObj.rootDiv
        });

        divObj.weatherdegree = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "weather_degree",
                src: "images/home/weather_degree.png",
                css: {
                    float: "right", "margin-top": 13 , "margin-right": 12 , display : "none"
                }
            },
            parent: divObj.rootDiv
        });



        divObj.temp3 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "temp_3",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 4 , display : "none"
                }
            },
            parent: divObj.rootDiv
        });

        divObj.tempdot = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "temp_dot",
                src: "images/icon/timenumber/time_dot.png",
                css: {
                    float: "right", "margin-top": 41, "margin-right": 5 , display : "none"
                }
            },
            parent: divObj.rootDiv
        });

        divObj.temp1 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "temp_1",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 5 , display : "none"
                }
            },
            parent: divObj.rootDiv
        });

        divObj.temp0 = util.makeElement({
            tag: "<img />",
            attrs: {
                name: "temp_0",
                css: {
                    float: "right", "margin-top": 12, "margin-right": 1 , display : "none"
                }
            },
            parent: divObj.rootDiv
        });

        divObj.tempminusbar = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "temp_minus_bar",
                css: {
                    float: "right", "margin-top": 27, "margin-right": 7, width: 11, height: 2,
                    "background-color": "rgba(255, 255, 255, 0.7)", display : "none"
                }
            },
            parent: divObj.rootDiv
        });
    }


    function initUI () {

        divObj.rootDiv.css({ "-webkit-transition": "", opacity: 0.8 });

        clockCount = 0;

        setClock();

        if (checkWeatherUpdate()) {
            updateWeather();
        }
        else {
            setWeather();
        }
    }

    function checkWeatherUpdate () {
        if (weatherData) {
            var now = new Date();

            if (now.getTime() - weatherData.checkTime > 60 * 60 * 1000) {
                return true;
            }
        }
        else {
            return true;
        }

        return false;
    }

    function updateWeather () {
        var custEnv = JSON.parse(KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.CUST_ENV));
        var dongCd;
        if(!custEnv || !custEnv.dongCd) {
            dongCd = "";
        } else {
            dongCd = "?dc=" + custEnv.dongCd;
        }

        var url = KTW.DATA.WEATHER.URL + dongCd;
        KTW.managers.http.AjaxFactory.createAjax("HomeWeatherClockView", true, "get", "json", url, null, true, 0, function (success, result) {
            if (success && result && result.status == 200 && result.weather) {
                if (!weatherData) {
                    divObj.rootDiv.children().css({visibility: "inherit"});
                }

                weatherData = {
                    checkTime: (new Date().getTime()),
                    weather: result.weather
                };

                setWeather();
            }
        });
    }

    function setClock () {
        var now = new Date();
        var hours = now.getHours();
        var min = now.getMinutes();

        divObj.clockhour0.attr("src", "images/icon/timenumber/time_number_" + Math.floor(hours / 10) + ".png");
        divObj.clockhour1.attr("src", "images/icon/timenumber/time_number_" + (hours % 10) + ".png");
        divObj.clockmin0.attr("src", "images/icon/timenumber/time_number_" + Math.floor(min / 10) + ".png");
        divObj.clockmin1.attr("src", "images/icon/timenumber/time_number_" + (min % 10) + ".png");
    }

    function setWeather () {


        var temp = KTW.utils.util.formatComma(weatherData.weather.temp, 1);
        var absTemp = Math.abs(temp);
        var intTemp = Math.floor(absTemp);
        var decimalTemp = (absTemp * 10) - (intTemp * 10);

        var isFloat = false;

        if (KTW.utils.util.isFloat(weatherData.weather.temp) && decimalTemp > 0) {
            isFloat = true;
        }

        divObj.divisionbar.css({display: ""});
        divObj.weathericon.attr("src", "images/home/weather_" + weatherData.weather.icon + ".png");
        divObj.weathericon.css({display: ""});
        divObj.weatherdegree.css({display: ""});

        if (weatherData.weather.temp < 0) {
            divObj.tempminusbar.css({display: ""});
        }
        else {
            divObj.tempminusbar.css({display: "none"});
        }

        if (isFloat) {
            if (absTemp < 10) {
                divObj.temp0.css({display: "none"});
                divObj.temp1.attr("src", "images/icon/timenumber/time_number_" + (intTemp % 10) + ".png");
                divObj.temp1.css({display: ""});
                divObj.tempdot.css({display: ""});
                divObj.temp3.css({display: ""}).attr("src", "images/icon/timenumber/time_number_" + (decimalTemp % 10) + ".png");
            }
            else {
                divObj.temp0.css({display: ""}).attr("src", "images/icon/timenumber/time_number_" + Math.floor(intTemp / 10) + ".png");
                divObj.temp1.attr("src", "images/icon/timenumber/time_number_" + (intTemp % 10) + ".png");
                divObj.temp1.css({display: ""});
                divObj.tempdot.css({display: ""});
                divObj.temp3.css({display: ""}).attr("src", "images/icon/timenumber/time_number_" + (decimalTemp % 10) + ".png");
            }
        }
        else {
            if (absTemp < 10) {
                divObj.temp0.css({display: "none"});
                divObj.temp1.attr("src", "images/icon/timenumber/time_number_" + (intTemp % 10) + ".png");
                divObj.temp1.css({display: ""});
                divObj.tempdot.css({display: "none"});
                divObj.temp3.css({display: "none"});
            }
            else {
                divObj.temp0.css({display: ""}).attr("src", "images/icon/timenumber/time_number_" + Math.floor(intTemp / 10) + ".png");
                divObj.temp1.attr("src", "images/icon/timenumber/time_number_" + (intTemp % 10) + ".png");
                divObj.temp1.css({display: ""});
                divObj.tempdot.css({display: "none"});
                divObj.temp3.css({display: "none"});
            }
        }
    }

    function startShow () {
        if (!timer) {
            initUI();

            timer = checkUI();
        }
    }

    function checkUI () {
        return setTimeout(function () {
            ++clockCount;

            if (clockCount === 20) {
                setClock();
                clockCount = 0;

                if (checkWeatherUpdate()) {
                    updateWeather();
                }
            }

            divObj.rootDiv.css({
                "-webkit-transition": KTW.CONSTANT.STB_TYPE.ANDROID ? "" : "opacity 0.3s 0.2s",
                opacity: 1
            });


            timer = checkUI();
        }, 3 * 1000);
    }

    function endShow () {
        clearTimeout(timer);
        timer = null;
    }

    this.create = function() {
        // 날씨 시간 element 생성
        createElements();
    };

    this.show = function() {
        startShow();
    };

    this.hide = function() {
        endShow();
    };

    this.destroy = function() {
        endShow();
    };
};