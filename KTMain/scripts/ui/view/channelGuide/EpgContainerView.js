/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgContainerView</code>
 *
 * Epg 관련 View 들을 관리하는 Container View
 * 추가로 홈샷 및 연관 메뉴 처리
 * @author dj.son
 * @since 2016-08-12
 */

(function() {
    KTW.ui.view.EpgContainerView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var _this = this;

        var parent = parent;
        var div = null;

        var arrEpgView = [];
        var curEpgView = null;

        var epgContainerDiv = null;
        var bgIcon = null;
        //var bgLine = null;
        //var shadowBG = null;

        var isHistoryBack = false;
        var isHotKey = false;

        this.language = parent.language;



        function createElement () {
            log.printDbg("createElement()");

            div = util.makeElement({
                tag: "<div />",
                attrs: {},
                parent: parent.div
            });

            //2017. 06.03 sw.nam EPG Container view 와 디폴트 홈샷 이미지 영역 layer 생성 시점 변경( 방송 예약 목록 옵션팝업 호출 시 홈샷 이미지에 덮여 보이는 이슈 개선)
            
            /*bgLine = util.makeElement({
                tag: "<div />",
                attrs: {
                    css:{
                        position: "absolute", left: 1450, top: 916, width: 390, height: 2, "background-color" : "rgba(255,255,255,0.15)", visibility: "hidden"
                    }
                },
                parent: div
            });*/

            epgContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {},
                parent: div
            });
            
        }

        function createEpgView (menuId) {
            log.printDbg("createEpgView(" + menuId + ")");

            var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;

            var epgView = null;

            switch (menuId) {
                case "DEFAULT_GUIDE_VIEW":
                    epgView = new KTW.ui.view.DefaultGuideView({parent: _this});
                    break;
                case MENU_ID.WATCH_4CHANNEL:  // 4채널 동시 시청
                //case MENU_ID.REAL_TIME_CHANNEL: // 실시간 인기 채널
                    epgView = new KTW.ui.view.ImageAppView({parent: _this, menuId: menuId});
                    break;
                case MENU_ID.AUDIO_CHANNEL: //오디오 채널
                    epgView = new KTW.ui.view.AudioChannelView({parent:_this, menuId: menuId});
                    break;
                case MENU_ID.TODAY_CHANNEL_GUIDE:
                    // 오늘의 채널 가이드 메뉴
                    epgView = new KTW.ui.view.TodayChannelGuideView({parent: _this, menuId: menuId});
                    break;

                case MENU_ID.COMMUNITY_CHANNEL:
                    // 우리만의 채널 메뉴
                    epgView = new KTW.ui.view.CommunityChannelView({parent: _this, menuId: menuId});
                    break;
                case MENU_ID.TV_APP_CHANNEL:
                    // TV앱 /쇼핑 채널 메뉴
                    epgView = new KTW.ui.view.TvAppView({parent: _this, menuId: menuId});
                    break;
                case MENU_ID.BROADCAST_RESERVATION_LIST :
                    // 방송 예약목록
                    epgView = new KTW.ui.view.ReservationListView({parent: _this, menuId: menuId});
                    break;
                case MENU_ID.GENRE_CHANNEL:
                case MENU_ID.PROGRAM_SEARCH:
                case MENU_ID.REAL_TIME_CHANNEL: // 실시간 인기 채널
                    // 장르별 채널 & 프로그램 검색(ots)
                    if (KTW.CONSTANT.IS_OTS === true && menuId === MENU_ID.REAL_TIME_CHANNEL) {                        
                        epgView = new KTW.ui.view.ImageAppView({parent: _this, menuId: menuId});
                    }
                    else {
                        epgView = new KTW.ui.view.GenreChannelView({parent: _this, menuId: menuId});
                    }
                    break;
                case MENU_ID.GENRE_TERRESTRIAL:
                case MENU_ID.GENRE_DRAMA:
                case MENU_ID.GENRE_MOVIE:
                case MENU_ID.GENRE_SPORTS:
                case MENU_ID.GENRE_ANIMATION:
                case MENU_ID.GENRE_DOCUMENTARY:
                case MENU_ID.GENRE_NEWS:
                case MENU_ID.GENRE_SOCIAL:
                case MENU_ID.GENRE_RELIGION:
                    // 장르별 채널 extension
                    epgView = new KTW.ui.view.GenreChannelViewExtension({parent: _this, menuId: menuId});
                    break;
                case MENU_ID.PROGRAM_MOVIE:
                case MENU_ID.PROGRAM_NEWS:
                case MENU_ID.PROGRAM_ENTERTAINMENT:
                case MENU_ID.PROGRAM_SPORTS:
                case MENU_ID.PROGRAM_KIDS:
                case MENU_ID.PROGRAM_MUSIC:
                case MENU_ID.PROGRAM_ART:
                case MENU_ID.PROGRAM_SOCIETY:
                case MENU_ID.PROGRAM_EDU:
                case MENU_ID.PROGRAM_LEISURE:
                case MENU_ID.PROGRAM_ETC:
                    epgView = new KTW.ui.view.ProgramSearchingViewExtension({parent: _this, menuId: menuId});
                    break;
                case MENU_ID.POPULAR_ALL:
                case MENU_ID.POPULAR_TERRESTRIAL:
                case MENU_ID.POPULAR_HOMESHOPPING:
                case MENU_ID.POPULAR_SPORTS:
                case MENU_ID.POPULAR_MOVIE:
                case MENU_ID.POPULAR_DRAMA:
                case MENU_ID.POPULAR_ANIMATION:
                case MENU_ID.POPULAR_KIDS:
                case MENU_ID.POPULAR_ENTERTAINMENT:
                case MENU_ID.POPULAR_NEWS:
                case MENU_ID.POPULAR_DOCUMENTARY:
                    /**
                     * jjh1117 2017/10/31
                     * 실시간 인기채널 하위 메뉴 이동 시 RealTimeChannelViewExtension를 신규로 생성하지 않고
                     * 하나의 View만 생성하여 그 View 공유해서 사용함.
                     * (Stiching vbo 생성/소멸 시 멈추는 현상이 있으므로)
                     */
                    for(var i=0;i<11;i++) {
                        epgView = arrEpgView["MENU091" + (10+i)];
                        if(epgView) {
                            break;
                        }
                    }

                    if (!epgView) {
                        epgView = new KTW.ui.view.RealTimeChannelViewExtension({parent: _this, menuId: menuId});
                    }
                    else {
                        epgView.setMenuId(menuId);
                    }
                    break;

                case MENU_ID.ENTIRE_CHANNEL_LIST:
                case MENU_ID.MY_FAVORITED_CHANNEL:
                case MENU_ID.OLLEH_TV_CHANNEL:
                case MENU_ID.UHD_CHANNEL:
                case MENU_ID.SKYLIFE_UHD_CHANNEL:
                case MENU_ID.MOVIE_CHOICE:
                    // 전체 채널, 선호 채널, olleh tv, OTV UHD, OTS UHD 메뉴
                    epgView = new KTW.ui.view.FullEpgView({parent: _this, menuId: menuId});
                    break;
            }

            if (!epgView) {
                var todayChannelGuideMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: [parent.getGuideMenu()],
                    cbCondition: function (menu) {
                        if (menu.id === menuId && menu.catType === KTW.DATA.MENU.CAT.CHSUB) {
                            return true;
                        }
                    }
                })[0];

                if (todayChannelGuideMenu) {
                    epgView = new KTW.ui.view.TodayChannelGuideView({parent: _this, menuId: todayChannelGuideMenu.id});
                }
            }

            if (epgView) {
                epgView.create();
            }

            return epgView;
        }

        function getEpgView (menuId) {
            log.printDbg("getEpgView()");

            var epgView = arrEpgView[menuId];

            if (!epgView) {
                epgView = createEpgView(menuId);
                arrEpgView[menuId] = epgView;
            }

            return epgView;
        }

        this.create = function () {
            log.printDbg("create()");

            createElement();
        };

        this.getDiv = function () {
            return div;
        };

        this.getEpgContainerDiv = function () {
            return epgContainerDiv;
        };

        this.show = function (options) {
            log.printDbg("show()");

            this.language = parent.language;

            if (options && options.jumpObj) {
                log.printDbg("jumpObj exist... so return...");
            }
            else {
                if (curEpgView) {
                    curEpgView.show(options);
                }
            }

            if (!options || !options.resume) {
                isHistoryBack = false;
                isHotKey = false;
            }
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (curEpgView) {
                curEpgView.hide(options);
            }

            if (!options || !options.pause) {
                curEpgView = null;
            }
        };

        this.focus = function () {
            log.printDbg("focus()");

            if (curEpgView) {

                curEpgView.focus();
            }
        };

        this.blur = function () {
            log.printDbg("blur()");
            if (curEpgView) {
                curEpgView.blur();
            }
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()  isHistoryBack : " + isHistoryBack);

            var consumed = false;

            if (curEpgView) {
                consumed = curEpgView.controlKey(keyCode);
            }
            if (!consumed) {
                switch (keyCode) {
                    case KTW.KEY_CODE.LEFT:
                        if (isHistoryBack) {
                            if (!isHotKey) {
                                KTW.ui.LayerManager.historyBack();
                            }
                            else if (curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_TERRESTRIAL
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_HOMESHOPPING
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_SPORTS
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_MOVIE
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_DRAMA
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ANIMATION
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_KIDS
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ENTERTAINMENT
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_NEWS
                                || curEpgView.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_DOCUMENTARY) {
                                this.setFocusGuideMenu();
                            }
                        }
                        else {
                            this.setFocusGuideMenu();
                        }
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.BACK:
                        if (isHistoryBack) {
                            KTW.ui.LayerManager.historyBack();
                        }
                        else {
                            this.setFocusGuideMenu();
                        }

                        consumed = true;
                        break;
                    case KTW.KEY_CODE.FULLEPG :
                    case KTW.KEY_CODE.FULLEPG_OTS :
                        var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                            cbCondition: function (menu) {
                                if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                                    return true;
                                }
                            }
                        })[0];

                        if (epgMenu) {
                            this.jumpTargetEpgView({
                                targetMenu: epgMenu
                            });
                        }
                        consumed = true;
                        break;
                }
            }

            return consumed;
        };

        this.checkSetVideoScreenPositonTimer = function () {
            parent.checkSetVideoScreenPositonTimer();
        };

        this.setVideoScreenPosition = function (options) {
            parent.setVideoScreenPosition(options);
        };

        this.setFocusGuideMenu = function () {
            parent.setFocusView(parent.getChannelGuideMenuView());
            //isHistoryBack = false;
        };

        this.jumpTargetEpgView = function (jumpObj) {
            log.printDbg("jumpTargetEpgView()");

            if (isHistoryBack) {
                jumpObj.isLayerHide = true;
                jumpObj.isHotKey = true;
            }

            parent.getChannelGuideMenuView().jumpTargetMenu(jumpObj);
        };

        this.setEpgView = function (options) {
            log.printDbg("setEpgView() ");
            var epgView = getEpgView(options.menu.id);

            if (curEpgView && curEpgView !== epgView) {
                curEpgView.hide();
            }

            if (epgView) {
                log.printDbg("setEpgView() STEP#1");
                // OTV 에서 ReservationView 는 시리즈 예약 때문에 비동기로 데이터 가져옴.
                // 따라서 이 경우에는 데이터를 다 가져온 후 DefaultGuideView 를 show 하도록 처리
                // 그 외의 경우에는 원래 로직대로 처리
                // 비동기로 데이터 가져오는 동안에는 로딩에 의해 키 동작 안하기 때문에 이슈 없음
                if (!KTW.CONSTANT.IS_OTS && options.menu.id === KTW.managers.data.MenuDataManager.MENU_ID.BROADCAST_RESERVATION_LIST) {
                    log.printDbg("setEpgView() STEP#2");
                    epgView.initData(function () {
                        log.printDbg("setEpgView() STEP#3");
                        if (curEpgView === epgView) {
                            log.printDbg("setEpgView() STEP#4");
                            var epgDataStatus = epgView.getDataStatus();
                            if (epgDataStatus !== KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE) {
                                curEpgView.hide();
                                epgView = getEpgView("DEFAULT_GUIDE_VIEW");
                                epgView.initData({menuId: options.menu.id, dataStatus: epgDataStatus});
                                epgView.show();
                                curEpgView = epgView;
                            }
                        }
                    });
                }
                else {
                    log.printDbg("setEpgView() STEP#5");
                    epgView.initData();
                    var epgDataStatus = epgView.getDataStatus();
                    if (epgDataStatus !== KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE) {
                        log.printDbg("setEpgView() STEP#6");

                        if (curEpgView && curEpgView === epgView) {
                            curEpgView.hide();
                        }
                        epgView = getEpgView("DEFAULT_GUIDE_VIEW");
                        epgView.initData({menuId: options.menu.id, dataStatus: epgDataStatus});
                    }
                }

                /**
                 * jjh1117 2017/10/31
                 * 실시간 인기채널 하위 메뉴 이동 시 해당 메뉴 정보 전달 함.
                 */
                if (KTW.CONSTANT.IS_OTS === false && options.menu.parent.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                    epgView.setMenuId(options.menu.id);
                }

                epgView.show();
            }

            curEpgView = epgView;

            //isHistoryBack = false;

            log.printDbg("setEpgView() options.isJump : " + options.isJump);
            log.printDbg("setEpgView() options.isLayerHide : " + options.isLayerHide);
            log.printDbg("setEpgView() this.isFullScreen() : " + this.isFullScreen());
            log.printDbg("setEpgView() options.menu.parent.id : " + options.menu.parent.id);
            if (options.isJump && options.isLayerHide && this.isFullScreen()) {
                isHistoryBack = true;
            }

            isHotKey = options.isHotKey;
        };

        this.setBackground = function (bgMode) {
            parent.setBackground(bgMode);
        };

        this.isFullScreen = function () {
            if (curEpgView) {
                return curEpgView.isFullScreen();
            }
        };

        this.isFocusAvailable = function () {
            if (curEpgView) {
                return curEpgView.isFocusAvailable();
            }
        };

        this.mainChannelTune = function (options) {
            if (options && options.channel) {
                if (KTW.managers.service.StateManager.isVODPlayingState()) {
                    KTW.managers.module.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                        if (vodModule) {
                            vodModule.execute({
                                method: "stopVodWithChannelSelection",
                                params: {
                                    channelObj: options.channel
                                }
                            });
                        }
                    });
                }
                else {
                    var mainControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                    mainControl.changeChannel(options.channel, options.onlySelect);
                }
            }
        };

        this.isJumpAvailable = function (menuId) {
            if (curEpgView) {
                if (curEpgView.menuId === menuId) {
                    return false;
                }
            }

            return true;
        }

        this.getGuideMenu = function () {
            return parent.getGuideMenu();
        }

        /**
         * 채널 가이드 내부에서 실시간 인기 채널로 jump 하기 위한 함수
         */
        this.jumpPopularMenu = function (menuId) {
            log.printDbg("jumpPopularMenu()");

            if (!menuId) {
                menuId = KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL;
            }

            var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (tmpMenu) {
                    if (tmpMenu.id === menuId) {
                        return true;
                    }
                }
            })[0];

            if (epgMenu) {
                this.jumpTargetEpgView({
                    targetMenu: epgMenu
                });
            }
        }

        this.getAudioChannelList = function() {
            if (curEpgView) {
                return curEpgView.getAudioChannelList();
            }
        };
    };
})();