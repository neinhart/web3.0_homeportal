/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>GenreChannelViewExtension</code>
 *
 * @author sw.nam
 * @since 2017-01-05
 * 장르별 채널 view implementation
 */

"use strict";

(function() {

    /* UtilityAPIs */
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var epgUtil = KTW.utils.epgUtil;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;


    var COMPONENT_TYPE = {
        COMPONENT_TYPE_VIDEO : 0,
        COMPONENT_TYPE_AUDIO : 1,
        COMPONENT_TYPE_SUBTITLE : 2
    };

    var MAX_AUDIO_COMPONENT = 10;

    /* Elements*/
    var div = null;

    /* Flags */
    var isCreate = false;
    var vsData = null;
    var totalPage = 0;
    var curPage = 0;
    var focusIndex = 0;

    var stitchingChannelData = undefined;    // 실시간 인기 채널 데이터
    var stitchingChannelControl = undefined; // 실시간 인기 채널 vbo
    var currentChannelLocator = undefined;   // 현재 tune 된 실시간 인기채널 locator
    var audioComponent = undefined;          // audio component
    var channelInformation = [];        // 실시간 인기 채널 상의 각 채널 정보
    var mainPrevChannel = undefined;    // 실시간 인기 채널 튠하기 이전의 메인 채널

    var isFocused = false;
    var isTuned = false;

    var upImg = null;
    var downImg = null;
    var stitching_focus_div = null;
    var pageGuide = null;

    var isRightFocused  = false;

    var setFocusAudioSelectTimer = null;

    var curMenu;

    var maxChannelCount = 0;
    var nextPageItemFocusIndex = -1;

    var topText = null;

    var initStitchingTimer = null;

    var thisParent = null;


    function createElement (_this) {

        var stitching_image_path = KTW.CONSTANT.IMAGE_PATH + "stitching/";

        div = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "real_time_channel_view_extension",
                css: {
                }
            },
            parent: _this.parent.getEpgContainerDiv()
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 0, width: 1920, height: 1080 , "background-color": "rgba(0,0,0,1)"
                }
            },
            parent: div
        });

        var bg_div = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "stitching_background_div",
                css: {
                    position: "absolute", left: 418, top: 122, width: 1454, height: 817
                }
            },
            parent: div
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "stitching_vbo_div"
            },
            parent:  bg_div
        });


        upImg = util.makeElement({
            tag: "<img />",
            attrs: {
                src: stitching_image_path + "arw_related_up.png",
                css: {
                    position: "absolute", left: 1125, top: 97 , display : "none"
                }
            },
            parent: div
        });

        downImg = util.makeElement({
            tag: "<img />",
            attrs: {
                src: stitching_image_path + "arw_related_dw.png",
                css: {
                    position: "absolute", left: 1125, top: 995 , display : "none"
                }
            },
            parent: div
        });

        topText = util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_l",
                css: {
                    position: "absolute", left: 1486, top: 71, width: 420, height: 30, "text-align": "left",
                    color: "rgba(255, 255, 255, 0.3)", "font-size": 23, "letter-spacing": -1.15 , display : "none"
                }
            },
            text: "※지상파는 지역별 자체방송이 제공됩니다",
            parent: div
        });


        // stitching channel focus div
        stitching_focus_div = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "stitching_focus_div",
                class : "focus_dim_box" ,
                css: {
                    position: "absolute", left: 411, top: 115 , width: 475, height: 272 , display : "none"
                }
            },
            parent: div
        });

        // util.makeElement({
        //     tag: "<div />",
        //     attrs: {
        //         class : "focus_black_1px_border_box" ,
        //         css: {
        //             position: "absolute", left: 0, top: 0, width: (475-12), height: (272-12)
        //         }
        //     },
        //     parent: stitching_focus_div
        // });

        
        pageGuide = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "",
                css: {
                    position: "absolute", left: 424, top: 990 , width : 171 , height : 43 , display : "none"
                }
            },
            parent: div
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pageup.png",
                css: {
                    position: "absolute", left: 412 - 412, top: 993 - 993
                }
            },
            parent: pageGuide
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pagedown.png",
                css: {
                    position: "absolute", left: 440 - 412, top: 993 - 993
                }
            },
            parent: pageGuide
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 474 - 412 , top: 995 - 993 , "font-size": 24, color: "rgba(255, 255, 255, 0.3)",
                    "letter-spacing": -1.2
                }
            },
            text: "페이지",
            parent: pageGuide
        });



    }

    function keyLeft (_this) {
        log.printDbg("keyLeft()");
        var consume = false;
        if(focusIndex === 0 || focusIndex === 3 || focusIndex === 6) {

        }else {
            focusIndex -=1;
            consume = true;
            moveChannel();
        }

        return consume;
    }

    function keyRight (_this) {
        log.printDbg("keyRight()");
        focusIndex++;
        moveChannel();
        return true;
    }

    function keyUp (_this) {
        log.printDbg("keyUp");
        focusIndex-=3;

        moveChannel();
        return true;
    }

    function keyDown(_this) {
        log.printDbg("keyDown");
        focusIndex+=3;
        moveChannel();
        return true;
    }

    function keyRed(_this) {
        log.printDbg("keyRed()");
        if(totalPage>1) {
            if(focusIndex === 0) {
                focusIndex-=maxChannelCount;
            }else {
                focusIndex = 0;
            }
            moveChannel();
        }else if(totalPage === 1) {
            if(focusIndex !== 0) {
                focusIndex = 0;
                moveChannel();
            }
        }
        return true;
    }

    function keyBlue(_this) {
        log.printDbg("keyBlue()");
        if(totalPage>1) {
            if(focusIndex === (maxChannelCount - 1)) {
                focusIndex+=maxChannelCount;
            }else {
                focusIndex = (maxChannelCount - 1);
            }
            moveChannel();
        }else if(totalPage === 1) {
            if(focusIndex !== (maxChannelCount - 1)) {
                focusIndex = (maxChannelCount - 1);
                moveChannel();
            }
        }
        return true;
    }


    function keyOk(_this) {
        log.printDbg("_keyOk()");

        var channel;

        // ots 의 경우 스카이 채널링이 있으면 sky 채널링에서만 찾는다.
        // 만약 sky 채널링이 없는 경우에는 kt 채널링에서 찾는다.
        // otv 는 kt 채널링만 탐색~
        // getChannelByNum API에서 채널링 검사를 진행하기 때문에 첫파라미터로 olleh tv, 두번째 파라미터로 skylife 를 넘겨준다
        channel = navAdapter.getChannelByNumOnAll(channelInformation[focusIndex][0] , true);

        if (channel == null || channel == undefined) {
            _showPopup();
        } else {
            if (stitchingChannelControl) {
                _removeListener();
            }
            // 먼저 element의 visibility를 hidden으로 변경한다.
            // (document.getElementById("stitching_background_div")).style.visibility = "hidden";
            // (document.getElementById("stitching_channel_info")).style.visibility = "hidden";

            _stopStitching(true, channel);
        }
    }

    function _showPopup() {
        log.printDbg("_showPopup()");

        var popupData = {
            arrMessage: [{
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                message: [KTW.ERROR.EX_CODE.HOME.EX003.title],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                message: [KTW.ERROR.EX_CODE.HOME.EX003.message[0]],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_30,
                message: [KTW.ERROR.EX_CODE.HOME.EX003.message[1]],
                cssObj: {}
            }, {
                type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_28,
                message: [KTW.ERROR.EX_CODE.HOME.EX003.message[2]],
                cssObj: { color: "rgb(180, 180, 180)"}
            }],

            arrButton: [{id: "ok", name: "확인"}],

            cbAction: function (id) {
                if (id === "ok") {
                    KTW.ui.LayerManager.deactivateLayer({
                        id: "stitching_channel_change_fail_popup",
                        remove: true
                    });
                }
            }
        };

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: "stitching_channel_change_fail_popup",
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP,
                view : KTW.ui.view.popup.BasicPopup,
                params: {
                    data : popupData
                }
            },
            visible: true
        });
    }

    function moveChannel () {
        log.printDbg("before moveChannel(), focusIndex = " + focusIndex + " , curPage : " + curPage + " , totalPage : " + totalPage + " , maxChannelCount : " + maxChannelCount);

        var bTune = false;

        if(focusIndex<0) {
            bTune = true;
            curPage--;
            if(curPage<=0) {
                curPage = totalPage;
            }
            nextPageItemFocusIndex = maxChannelCount  + focusIndex;
            //focusIndex = -1;

        }else if(focusIndex>(maxChannelCount - 1)) {
            bTune = true;
            curPage++;
            if(curPage>totalPage) {
                curPage = 1;
            }
            nextPageItemFocusIndex = focusIndex - maxChannelCount;
            //focusIndex = 0;
        }


        if (bTune) {
            stitchingChannelData = vsData[curPage - 1];
            stitching_focus_div.css("display" , "none");
            _tuneChannel();

            if(totalPage > 1) {
                if (curPage === 1) {
                    upImg.css("display" , "none");
                    downImg.css("display" , "");
                }else if(curPage === totalPage) {
                    upImg.css("display" , "");
                    downImg.css("display" , "none");
                }else {
                    upImg.css("display" , "");
                    downImg.css("display" , "");
                }
            }
        }else {
            _setFocus();
            startAudioFocusSelect();
        }

        log.printDbg("after moveChannel(), focusIndex = " + focusIndex + " , curPage : " + curPage + " , totalPage : " + totalPage + " , maxChannelCount : " + maxChannelCount);
    }


    function _setFocus() {
        log.printDbg("_setFocus(), focusIndex = " + focusIndex);

        stitching_focus_div.css({
            left: (411 + (497 * (focusIndex % 3))),
            top: (115  + (281 * parseInt(focusIndex / 3)))
        });

        stitching_focus_div.css("display" , "");
    }

    /**
     * stitching vbo를 생성하고 stitching channel 로 tune 하기 이전 채널을 저장한다.
     *
     * @private
     */
    function _startStitching() {
        log.printDbg("_startStitching()");

        if (mainPrevChannel === null || mainPrevChannel === undefined) {
            navAdapter.stopChannel();
            stitchingChannelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.STITCHING, true);

            // TODO 2016.08.03 dhlee
            // DOM Tree 상에 생성되는 element 들에 대해서 정리해야 한다.
            // Layer가 생성될 때 해당 Layer의 id는 Layer#create() 에서 set 하고 있다.
            // Layer ID를 정리해야 한다.
            if(!document.getElementById("stitching_vbo_object")) {
                stitchingChannelControl.vbo.setParentElement("stitching_vbo_div");
            }

            stitchingChannelControl.start();
            mainPrevChannel = KTW.oipf.AdapterHandler.extensionAdapter.getRealCurrentChannel();

            if (mainPrevChannel === null) {
                try {
                    mainPrevChannel = navAdapter.getChannelControl(KTW.nav.Def.CONSTANT.MAIN, false).vbo.currentChannel;
                    if (mainPrevChannel != null) {
                        log.printDbg("_startStitching(), main_vbo.currentChannel = " + JSON.stringify(mainPrevChannel));
                    }
                } catch (e) {
                    log.printDbg(e);
                    mainPrevChannel = navAdapter.getChannelControl(KTW.nav.Def.CONSTANT.MAIN, false).getCurrentChannel();
                    if (mainPrevChannel != null) {
                        log.printDbg("_startStitching(), main_vbo.getCurrentChannel() = " + JSON.stringify(mainPrevChannel));
                    }
                }
            }
        }
    }

    /**
     * stitching channel 서비스를 중지하고 사용자가 선택한 channel 로 tune 한다.
     *
     * 실시간 인기 채널 종료시 수행작업
     * - 실시간 인기채널 중지
     * - 메인 vbo 채널 튠
     *   1. 실시간 인기 채널에서 특정 채널을 튠하는 경우 (두 가지 경우 모두 미니EPG 생성됨)
     *      - 동일 채널인 경우 강제로 튠
     *      - 동일 채널이 아닌 경우 일반 튠
     *   2. 그외의 경우
     *      - 채널업다운, 예약, DCA, 이전키, 핫키, 종료키, 시청시간제한, 대기모드 등
     *        위 상황에서는 현재 실제 메인 vbo 채널정보와 실시간 인기채널 진입전 채널을 비교하여
     *        같을 경우 같은 채널이라 간주하여 튠처리를 하고, 다를 경우 외부에서 튠이 이미 된상태로 간주하여 튠처리 안함
     *        R5에서는 홈포탈이 실시간인기채널를 관리하면서 레이어가 추가되었음.(스택타입)
     *        실시간 인기 채널 레이어 위에 이전 스택을 살리면서 뜨는 메뉴 또는 App으로 인해 이전키를 누르면
     *        이전 스택으로 이동하면서 실시간 인기채널이 다시 뜨는 문제가 있음.
     *        검색, 마이메뉴, TV다시보기 등 메뉴가 뜨는 경우는 controlKey에서 핫키 처리로 해결
     *        쇼핑, 위젯 등 App이 뜨는 경우는 키처리를 홈포탈이 하지 않기 때문에 obs_hide message 이벤트로 처리
     *
     * @param isSelect   true: 사용자가 특정 채널을 선택해서 tune을 해야 한다.
     * @param channel
     * @private
     */
    function _stopStitching(isSelect, channel) {
        log.printDbg("_stopStitching(), isSelect = " + isSelect);

        // TODO 2016.08.03 Dongho Lee
        // 로직 및 관련 시나리오 분석이 더 필요하다. (50% 이해함)
        if (mainPrevChannel != null || mainPrevChannel != undefined) {
            var runningStitchingCC = true;
            if (stitchingChannelControl) {
                if (stitchingChannelControl.getState() === KTW.nav.Def.CONTROL.STATE.STOPPED) {
                    runningStitchingCC = false;
                } else {
                    stitchingChannelControl.stop();
                }
            }

            if (isSelect === true && (channel != null && channel != undefined)) {
                //KTW.ui.LayerManager.clearNormalLayer();
                if (channel.ccid === mainPrevChannel.ccid) {
                    log.printDbg("_stopStitching(), channel.ccid === mainPreChannel.ccid");
                    navAdapter.changeChannel(channel, undefined, true);
                } else {
                    log.printDbg("_stopStitching(), channel.ccid != mainPrevChannel.ccid");
                    navAdapter.changeChannel(channel);
                }
                // 실시간 인기 채널에서 선택하여 채널튠하는 상태를 기억 (why???)
                isTuned = true;
                navAdapter.removeChannelControl(KTW.nav.Def.CONTROL.STITCHING);

                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.ETC_MENU,
                    act: KTW.KEY_CODE.OK,
                    menuId: curMenu.id,
                    menuName: curMenu.name,
                    locator: currentChannelLocator,
                    chIdx: ((curPage-1)*maxChannelCount) + (focusIndex+1)
                });

                vsData = undefined;
                stitchingChannelData = undefined;       // 실시간 인기 채널 데이터
                stitchingChannelControl = undefined;    // 실시간 인기 채널 vbo
                currentChannelLocator = undefined;      // 현재 tune 된 실시간 인기채널 locator
                audioComponent = undefined;             // audio component
                channelInformation = [];                // 실시간 인기 채널 상의 각 채널 정보
                mainPrevChannel = undefined;            // 실시간 인기 채널 튠하기 전의 메인 채널
                
            } else {
                if (isTuned === false) {
                    var realCurrentChannel = KTW.oipf.AdapterHandler.extensionAdapter.getRealCurrentChannel();
                    if (realCurrentChannel === null) {
                        realCurrentChannel = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN, false).vbo.currentChannel;
                    }
                    /**
                     * 1. 대기모드로 진입
                     * - 채널 중지 자동으로 진행. 동작모드로 전환 시 자동으로 채널 튠. 즉, 대기모드 진입하는 경우는 이부분으로 진입하지 않도록 처리
                     * 2. 시청시간제한 진입
                     * - 시청시간제한 진입시 가장 먼저 obs_hide message가 전달되고 StichingView의 hide가 호출되어 아래 로직에 진입하게 됨.
                     *   이전 채널과 동일하기 때문에 시청시간제한 아이프레임이 나오기전 잠깐 메인 AV가 보일 수 있음(R4 실시간 인기채널 에서도 동일)
                     */
                    if (KTW.managers.device.PowerModeManager.powerState !== KTW.oipf.Def.CONFIG.POWER_MODE.ACTIVE_STANDBY) {
                        if (realCurrentChannel && mainPrevChannel.ccid && (realCurrentChannel.ccid === mainPrevChannel.ccid)) {
                            log.printDbg("_stopStitching(), realCurrentChannel.ccid === mainPrevChannel.ccid");
                            navAdapter.changeChannel(mainPrevChannel, runningStitchingCC, true);
                        }
                    }
                    navAdapter.removeChannelControl(KTW.nav.Def.CONTROL.STITCHING);
                }
            }
        }
    }
    

    /**
     * StitchingChannelControl을 생성하고 채널 event 수신을 위한 listener 등록 등 초기화 함수
     * @private
     */
    function _initStitching() {
        log.printDbg("_initStitching()");
        _startStitching();
        initStitchingTimer = null;
        // stitching channel control에 listener 등록
        if (stitchingChannelControl) {
            _removeListener();
            _addListener();
        }

        // 노출될 화면 사이즈 및 channel tune
        if(thisParent !== null) {
            thisParent.checkSetVideoScreenPositonTimer();

        }

        stitchingChannelControl.setVideoSize(418, 122, 1454, 817);
        _tuneChannel();
    }

    /**
     * 실시간 인기 채널에서 focus 된 채널의 정보를 획득한다.
     *
     * @private
     */
    function _getChannelInformation() {
        log.printDbg("_getChannelInformation()");
        var desc;
        var otvChannel = [0, 0];
        var otsChannel = [0, 0];


        if(maxChannelCount>0) {
            try {
                if (!stitchingChannelControl) {
                    log.printDbg("_getChannelInformation() stitchingChannelControl is null");
                    // stitching vbo 객체가 없다면 return
                    return;
                }

                // 실시간 인기채널에서만 사용하기 때문에 vbo 직접 접근. 향후 이동될수도 있음
                desc = stitchingChannelControl.vbo.getPMTDescriptors(0x81);
                log.printDbg("_getChannelInformation : " + desc[0].contents);

                /** 총 60bytes
                 * for (i=0;i<9;i++) {
                 *      otv_ch_num       24      uimsbf (unsigned char 2bytes)
                 *      ots_ch_num       24      uimsbf (unsigned char 2bytes)
                 * }
                 * current_time         96      uimsbf (unsigned char 12bytes, YYYYMMDDHHMM)
                 * Next_time            96      uimsbf (unsigned char 12bytes, YYYYMMDDHHMM)
                 */
                var dataLength = (maxChannelCount*2*2);

                if(desc[0].contents.length>=dataLength) {
                    for (var i = 0, j = 0; i < dataLength; i++) {
                        if (i % 4 < 2) {
                            otvChannel = otvChannel.concat([desc[0].contents[i]]);
                        } else if (i % 4 < 4) {
                            otsChannel = otsChannel.concat([desc[0].contents[i]]);
                        }

                        if (i % 4 == 3) {
                            channelInformation[j] = [util.convertToInt(otvChannel), util.convertToInt(otsChannel)];
                            otvChannel = [0,0];
                            otsChannel = [0,0];
                            j++;
                        }
                    }
                }
            } catch(e) {
                log.printDbg(e.message);
            }
            
        }
    }
    /**
     * PMT 변경 시 이벤트 처리
     */
    function onPMTChanged() {
        log.printDbg("onPMTChanged");
        if(isFocused === false) {
            isFocused = true;
        }

        try {
            audioComponent = stitchingChannelControl.getComponents(COMPONENT_TYPE.COMPONENT_TYPE_AUDIO);
            try {
                if(audioComponent) {
                    maxChannelCount = ((audioComponent.length) - 1) ;
                }
            } catch (e) {
                log.printDbg(e.message);
            }
        } catch (e) {
            log.printDbg(e.message);
        }

        _getChannelInformation();
        if(maxChannelCount>0) {
            startAudioFocusSelect();
        }

        if(isRightFocused === true) {
            if(nextPageItemFocusIndex>=0) {
                focusIndex = nextPageItemFocusIndex;
                if((maxChannelCount-1)<focusIndex) {
                    focusIndex = (maxChannelCount-1);
                }
                nextPageItemFocusIndex = -1;
            }

            stitching_focus_div.removeClass("focus_dim_box");
            stitching_focus_div.addClass("focus_red_border_box");
        }else {
            stitching_focus_div.removeClass("focus_red_border_box");
            stitching_focus_div.addClass("focus_dim_box");
        }
        _setFocus();

    }

    function startAudioFocusSelect() {
        stopAudioFocusSelect();
        setFocusAudioSelectTimer = setTimeout(_setAudioComponent, 100);
    }

    function stopAudioFocusSelect() {

        if(setFocusAudioSelectTimer !== null) {
            clearTimeout(setFocusAudioSelectTimer);
            setFocusAudioSelectTimer = null;
        }
    }

    /**
     * Stitching 채널 변경 시 이벤트 처리
     *
     * Stitching 채널은 현재 웹 홈포털 기준으로 3개의 채널이 제공되고 있다.
     * 상/하 방향키 등을 통해서 다음 페이지로 넘어갈 때 실제로는 다음 stitching channel로 tune 하게 된다.
     *
     * @param channelEvent
     */
    function onChannelChanged(channelEvent) {
        log.printDbg("onChannelChanged(), channelEvent = " + JSON.stringify(channelEvent));

        if (util.isValidVariable(channelEvent.blocked_reason) === true) {
            log.printDbg("onChannelChanged() error state = " + channelEvent.blocked_reason);

            // 채널 전환 시 error 발생한 경우 error popup 노출
            // TODO 2016.08.03 Dongho Lee
            // 추후 showErrorPopup()을 그대로 사용할지 확인 후 수정이 필요하다면 수정해야 함
            // UI 릴리즈 후 코드 다시 정리해야 함.

            KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.EX_CODE.HOME.EX004, null, function() {
                log.printDbg("error popup callback");

                // error popup이 닫히면 실시간 인기채널을 종료
                KTW.ui.LayerManager.deactivateLayer({id: parent.id, remove: false});
            }, false);
        }
        else {
            log.printDbg("onChannelChanged() success");

            try {
                audioComponent = stitchingChannelControl.getComponents(COMPONENT_TYPE.COMPONENT_TYPE_AUDIO);
                try {
                    if(audioComponent) {
                        maxChannelCount = ((audioComponent.length) - 1) ;
                    }
                } catch (e) {
                    log.printDbg(e.message);
                }
            } catch (e) {
                log.printDbg(e.message);
            }

            if(maxChannelCount>0) {
                startAudioFocusSelect();
            }

            isFocused = true;
        }
    }

    /**
     * listener 등록
     *
     * @private
     */
    function _addListener() {
        log.printDbg("_addListener()");

        // Bluetooth 장치가 연결여부 callback listener 등록
        // connect/disconnect event 전달 시 audio component를 새롭게 select 처리함
        // TODO 단, 실제로는 connect 되었을때만 처리하면 될 것 같은데
        // 우선은 audio bluetooth 장치가 매번 연결/해제 될때마다 처리하도록 함
        KTW.managers.device.DeviceManager.bluetoothDevice.setConnectionListener(_setAudioComponent);

        if (stitchingChannelControl) {
            stitchingChannelControl.addEventListener("PMTChanged", onPMTChanged);
            stitchingChannelControl.addChannelEventListener(onChannelChanged);
        }
    }

    /**
     * listener 해제
     *
     * @private
     */
    function _removeListener() {
        log.printDbg("_removeListener()");

        // Bluetooth 장치가 연결여부 callback listener 등록
        KTW.managers.device.DeviceManager.bluetoothDevice.setConnectionListener(null);

        if (stitchingChannelControl) {
            stitchingChannelControl.removeEventListener("PMTChanged", onPMTChanged);
            stitchingChannelControl.removeChannelEventListener(onChannelChanged);
        }
    }

    /**
     * set audio component
     * should check connected bluetooth device
     *
     * @private
     */
    function _setAudioComponent() {
        log.printDbg("_setAudioComponent()");

        if(focusIndex>=0) {
            try {
                // log.printDbg("before _setAudioComponent() getComponents() ");
                // audioComponent = stitchingChannelControl.getComponents(COMPONENT_TYPE.COMPONENT_TYPE_AUDIO);
                // log.printDbg("after _setAudioComponent() getComponents() ");
                try {
                    if (audioComponent && audioComponent.length >= (maxChannelCount + 1)) {
                        log.printDbg("_setAudioComponent(), select component = " + (focusIndex+1));
                        // default audio device의 경우 별도로 설정하지 않는다.
                        stitchingChannelControl.selectComponent(audioComponent[focusIndex+1]);
                        if (KTW.managers.device.DeviceManager.bluetoothDevice.isAudioDeviceConnected() === true) {
                            log.printDbg("_setAudioComponent(), BT.isAudioDeviceConnected() is true");
                            // 연결된 BT 디바이스가 존재하면 BT 디바이스에 음성이 나오도록 설정한다.
                            stitchingChannelControl.selectComponent(audioComponent[focusIndex+1], KTW.managers.device.DeviceManager.bluetoothDevice.getBtAudioDevice());
                        }
                    }
                } catch (e) {
                    log.printDbg(e.message);
                }
            } catch (e) {
                log.printDbg(e.message);
            }
        }
    }

    function _tuneChannel() {
        log.printDbg("_tuneChannel()");

        var targetChannel;

        // if (currentChannelLocator === stitchingChannelData.locator) {
        //     log.printDbg("_tuneChannel(), same channel, so return");
        //     return;
        // }

        isFocused = false;

        // 재생할 stitching channel locator 저장
        currentChannelLocator = stitchingChannelData.locator;
        if (currentChannelLocator !== null && currentChannelLocator !== undefined) {
            targetChannel = navAdapter.getChannelByTriplet(currentChannelLocator);

            channelInformation = [];
            maxChannelCount = 0;
            stitchingChannelControl.changeChannel(targetChannel);
        }
    }


    /**
     * jjh1117 2017/10/31
     * 실시간 인기 채널 데이터를 OCManager로부터 가져온다.
     * 이 부분 추후 장르별로 OC 편성 후에 추가 개발 해야 함.
     */
    function _loadVSData(menuid) {
        log.printDbg("_loadVSData()");
        var vsData = [];
        /**
         * example
         *
         * 실시간 인기채널 1위-9위|dvb://5.1B9.1B9
         * 실시간 인기채널 10위-18위|dvb://5.1BB.1BB
         * 실시간 인기채널 19위-27위|dvb://5.1BE.1BE
         */

        if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL) {
            var originalData = KTW.managers.data.OCManager.getVSData();
            log.printDbg("_loadVSData(), originalData = " + originalData);
            if (originalData != null && originalData != undefined) {
                var parsingData = originalData.split("\n");
                if (parsingData != null && parsingData.length > 0) {
                    var length = parsingData.length;
                    var temp = null;

                    for (var i = 0; i < length; i++) {
                        temp = parsingData[i].split("|");
                        if (temp && temp.length === 2) {
                            vsData[i] = {
                                title: temp[0],
                                locator: temp[1]
                            };
                            temp = null;
                        }else {
                            break;
                        }
                    }
                } else {
                    log.printDbg("_loadVSData(), parsingData is null or length <= 0, so return");
                    return null;
                }
            } else {
                log.printDbg("_loadVSData(), originalData is null, so return");
                return null;
            }
        }else {
            if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_TERRESTRIAL) {
                vsData[0] = {
                    title: "지상파/종편",
                    locator: "1.2B0.2B0"
                };

            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_HOMESHOPPING) {
                vsData[0] = {
                    title: "홈쇼핑",
                    locator: "1.2B1.2B1"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_SPORTS) {
                vsData[0] = {
                    title: "스포츠/레져",
                    locator: "1.2B2.2B2"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_MOVIE) {
                vsData[0] = {
                    title: "영화/시리즈",
                    locator: "1.2B3.2B3"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_DRAMA) {
                vsData[0] = {
                    title: "드라마/여성",
                    locator: "1.2B4.2B4"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ANIMATION) {
                vsData[0] = {
                    title: "애니",
                    locator: "1.2B5.2B5"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_KIDS) {
                vsData[0] = {
                    title: "유아/교육",
                    locator: "1.2B6.2B6"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ENTERTAINMENT) {
                vsData[0] = {
                    title: "오락/음악",
                    locator: "1.2B7.2B7"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_NEWS) {
                vsData[0] = {
                    title: "뉴스/경제",
                    locator: "1.2B8.2B8"
                };
            }else if(menuid === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_DOCUMENTARY) {
                vsData[0] = {
                    title  : "다큐/교양",
                    locator: "1.2B9.2B9"
                };
            }
        }
        log.printDbg("_loadVSData(), vsData = " + JSON.stringify(vsData));
        return vsData;
    }

    KTW.ui.view.RealTimeChannelViewExtension = function(options) {
        KTW.ui.view.EpgView.call(this, options);
        log.printDbg("constructor() options.menuId : " + options.menuId);
        this.curMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === options.menuId) {
                    return true;
                }
            }
        })[0];

        if(this.curMenu !== undefined && this.curMenu !== null) {
            log.printDbg("constructor() this.curMenu.name : " + this.curMenu.name);
        }

        this.create = function() {
            log.printDbg("create()");

            if (!isCreate) {
                createElement(this);
                isCreate = true;
                isFocused = false;
            }
        };

        this.initData = function () {
            //this.initEpgData();
        };

        this.setMenuId = function(menuId) {
            log.printDbg("setMenuId() menuId : " + menuId);
            this.menuId = menuId;
            this.curMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id === menuId) {
                        return true;
                    }
                }
            })[0];

            if(this.curMenu !== undefined && this.curMenu !== null) {
                log.printDbg("setMenuId() this.curMenu.name : " + this.curMenu.name);
                curMenu = this.curMenu;
            }

            topText.css("display" , "");
            // if(this.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_ALL || this.menuId === KTW.managers.data.MenuDataManager.MENU_ID.POPULAR_TERRESTRIAL) {
            //     bottomText.css("display" , "");
            // }else {
            //     bottomText.css("display" , "");
            // }
        };

        this.show = function (options) {
            log.printDbg("show()");

            vsData = null;
            isTuned = false;
            isFocused = false;
            stitchingChannelData = null;
            //isRightFocused = false;
            nextPageItemFocusIndex = -1;
            if(this.curMenu !== undefined && this.curMenu !== null) {
                log.printDbg("show() this.curMenu.name : " + this.curMenu.name);
            }

            this.parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });


            vsData = _loadVSData(this.curMenu.id);
            if(vsData !== undefined && vsData !== null && vsData.length>0) {
                totalPage = vsData.length;
                curPage = 1;
                stitchingChannelData = vsData[curPage - 1];
                focusIndex = 0;
            }
            // if (!options || !options.resume) {
            //     vsData = _loadVSData(this.curMenu.id);
            //     if(vsData !== undefined && vsData !== null && vsData.length>0) {
            //         totalPage = vsData.length;
            //         curPage = 1;
            //         stitchingChannelData = vsData[curPage - 1];
            //         focusIndex = 0;
            //     }
            // }
            // else {
            //     if (div.hasClass("focus")) {
            //
            //     }
            //     else {
            //
            //     }
            // }

            thisParent = this.parent;

            initStitchingTimer = setTimeout(_initStitching, 10);

            upImg.css("display" , "none");
            downImg.css("display" , "none");
            stitching_focus_div.css("display" , "none");
            pageGuide.css("display" , "none");

            div.css({visibility: "inherit"});

            if (options && options.resume && isRightFocused) {
                this.focus();
            }
            else {
                isRightFocused = false;
            }
        };

        this.hide = function (options) {
            log.printDbg("hide()");

            div.css({visibility: "hidden"});
            //
            if (!options || !options.pause) {
            }
            else {
            }

            if(initStitchingTimer !== null) {
                clearTimeout(initStitchingTimer);
                initStitchingTimer = null;
            }

            this.blur(options);

            stopAudioFocusSelect();

            _removeListener();
            _stopStitching(false, null);


            vsData = undefined;
            stitchingChannelData = undefined;       // 실시간 인기 채널 데이터
            stitchingChannelControl = undefined;    // 실시간 인기 채널 vbo
            currentChannelLocator = undefined;      // 현재 tune 된 실시간 인기채널 locator
            audioComponent = undefined;             // audio component
            channelInformation = [];                // 실시간 인기 채널 상의 각 채널 정보
            mainPrevChannel = undefined;            // 실시간 인기 채널 튠하기 전의 메인 채널

        };

        this.focus = function () {
            log.printDbg("focus()");

            isRightFocused = true;
            if(totalPage>1) {
                upImg.css("display" , "none");
                downImg.css("display" , "");
                pageGuide.css("display" , "");
            }else {
                upImg.css("display" , "none");
                downImg.css("display" , "none");
                pageGuide.css("display" , "none");
            }

            if(maxChannelCount>0) {
                stitching_focus_div.removeClass("focus_dim_box");
                stitching_focus_div.addClass("focus_red_border_box");
                _setFocus();
            }

            var last_stack_layer = KTW.ui.LayerManager.getLastLayerInStack();
            if (last_stack_layer != null) {
                log.printDbg("focus() last_stack_layer ID : " + last_stack_layer.id);
            }
        };

        this.blur = function (options) {
            log.printDbg("blur()");

            if (!options || !options.pause) {
                isRightFocused = false;
            }

            div.removeClass("focus");
            upImg.css("display" , "none");
            downImg.css("display" , "none");

            stitching_focus_div.removeClass("focus_red_border_box");
            stitching_focus_div.addClass("focus_dim_box");

            pageGuide.css("display" , "none");
            
        };

        this.remove = function () {
            log.printDbg("remove()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");
            if (!isFocused || stitchingChannelControl === undefined || stitchingChannelControl === null) {
                return;
            }
            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft(this);
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue(this);
                    consumed = true;
                    break;

            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;
        };
        this.isFocusAvailable = function () {
            return true;
        };

        this.getDataStatus = function () {
            return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
        }
    };

    KTW.ui.view.RealTimeChannelViewExtension.prototype = new KTW.ui.view.EpgView();
    KTW.ui.view.RealTimeChannelViewExtension.prototype.constructor = KTW.ui.view.RealTimeChannelViewExtension;

})();