/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>ReservationListView</code>
 *
 * @author sw.nam
 * @since 2017-01-19
 *
 * 방송예약목록 view
 */

(function() {

    /* Constance numbers */
    var NUMBER_OF_FOCUSED_ITEM = 6;

    /* UtilityAPIs */
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var reservationManager = KTW.managers.service.ReservationManager;

    /* global_*/
    var reservationList = null;
    var dynamicList = null;
    var transactionId = null;

    /*index*/
    var programIndex = 0;

    /* option box 컨트롤 하는 index 및 case string*/
    var optionBoxMaxCount = 0;
    var optionBoxInBoxIndex =0;
    var optionBoxInBoxString = ["CANCEL_RESERVATION","RESERVATION_SERIES","JUMP_TO_CHANNEL"];

    /*flag*/
    var isFocusedItemArea = true;
    var isOptionBoxArea = true;

    var div = null;

    /* has reservation list*/
    var rlv_itemGroup = null;
    var item = [0]; // Division to include  reservation information
    var programArea = [0];
    var channelArea = [0];
    var reservationInfoArea = [0];
    var seriesInfoArea = [0];

    /* subArea*/
    var subArea = null;

    /*scroll*/
    var scrollArea = null;
    var scroll = null;


    /*popUp*/
    var optionBox = null;

    var selectedProgram = null;

    KTW.ui.view.ReservationListView = function(options) {

        var parent = options.parent;
        var menuId = options.menuId;
        var _this = this;

        var curMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === menuId) {
                    return true;
                }
            }
        })[0];

        function createElement() {

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "reservation_list_view",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080 , visibility : "hidden"
                    }
                },
                parent: parent.getEpgContainerDiv()
            });

            rlv_itemGroup = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_itemGroup",
                    css: {
                        position: "absolute"
                    }
                },
                parent: div
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "divisionLine_TOP",
                    css: {
                        position: "absolute"
                    }
                },
                parent: rlv_itemGroup
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "divisionLine_BOTTOM",
                    css: {
                        position: "absolute"
                    }
                },
                parent: rlv_itemGroup
            });

            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1829-576), top: 149, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 702,
                forceHeight: true
            });

            subArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "sub_area"
                },
                parent: div
            });

            var keyGuide = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "subArea_key_guide_area",
                    css: {
                        position: "absolute", overflow: "visible", visibility: "hidden"
                    }
                },
                parent: subArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: (1803 - 576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: (1831-576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1803 - 576), top: 939, width: 150, height: 24,
                        "font-size": 22, "font-family": "RixHead L", "letter-spacing": -1.1, color: "rgba(255, 255, 255, 0.3)"
                    }
                },
                text: "페이지",
                parent: keyGuide
            });

            if (KTW.CONSTANT.IS_OTS) {
                var autoBootingGuideArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "subArea_auto_booting_guide_area",
                        css: {
                            position: "absolute", overflow: "visible", visibility: "hidden"
                        }
                    },
                    parent: subArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "subArea_textTips",
                        css: {
                            position: "absolute", left: (635-576), top: 1000, width: 900, "font-size": 24, color: "rgba(255, 255, 255, 0.25)",
                            "font-family" : "RixHead L", "letter-spacing": -1.2
                        }
                    },
                    text: "※ 자동 켜짐 ON을 하면 예약한 프로그램이 시작할 때 셋톱박스가 자동으로 켜져요!",
                    parent: autoBootingGuideArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/icon/icon_option_related.png",
                        css: {
                            position: "absolute", left: (1700-576), top: 1001
                        }
                    },
                    parent: autoBootingGuideArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "subArea_text",
                        css: {
                            position: "absolute", left: (1734-576), top: 1001, width: 300, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                            "font-family" : "RixHead L", "letter-spacing": -1.2
                        }
                    },
                    text: "셋톱박스 자동 켜짐 해제",
                    parent: autoBootingGuideArea
                });
            }




            //make each item elements.
            for(var i =0; i < NUMBER_OF_FOCUSED_ITEM; i++) {

                item[i] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "rlv_item",
                        css: {
                            height: (i === 0 || i === (NUMBER_OF_FOCUSED_ITEM - 1)) ? 140 : 139
                        }

                    },
                    parent: rlv_itemGroup
                });
                util.makeElement({
                    tag:"<div />",
                    attrs: {
                        class: "rlv_itemTopLine",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: item[i]
                });

                programArea[i] = util.makeElement({
                    tag:"<div />",
                    attrs: {
                        class: "rlv_programArea",
                        css: {
                            position: "absolute" , width : 1000, height:40
                        }
                    },
                    parent: item[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "rlv_programName font_l cut",
                        css: {
                            position: "absolute" , left: 2 ,  top : 0, width : 900 , height: 36,
                            "font-size": 34, "letter-spacing": -1.7, color: "rgba(255, 255, 255, 0.7)"
                        }
                    },
                    text: "프로그램이름",
                    parent: programArea[i]
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "rlv_ageIcon",
                        css: {
                            position: "absolute" , left : 0 , top : 0 , width: 32, height: 32 , display : "none"
                        }
                    },
                    parent: programArea[i]
                });

                channelArea[i] = util.makeElement({
                    tag:"<div />",
                    attrs: {
                        class: "rlv_channelArea",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: item[i]
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "rlv_chIcon",
                        src: "images/icon/icon_sky_epg.png",
                        css: {
                            float: "left", display: "none"
                        }
                    },
                    parent: channelArea[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "rlv_chNum",
                        css: {
                            /*  "font-family": "RixHead L",*/ float: "left"
                        }
                    },
                    parent: channelArea[i]
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "rlv_chName",
                        css: {
                            /*"font-family": "RixHead L",*/ float: "left"
                        }
                    },
                    parent: channelArea[i]
                });

                util.makeElement({
                    tag:"<div />",
                    attrs: {
                        class: "rlv_itemBottomLine no"+i,
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: item[i]
                });

                reservationInfoArea[i] = util.makeElement({
                    tag:"<div />",
                    attrs: {
                        class: "rlv_reservationInfoArea",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: item[i]
                });
                util.makeElement({
                    tag:"<span />",
                    attrs: {
                        class: "rlv_rlText",
                        css: {
                            /*"font-family": "RixHead L", */float: "left"
                        }
                    },
                    text: "예약정보 여기",
                    parent: reservationInfoArea[i]
                });

                seriesInfoArea[i] = util.makeElement({
                    tag:"<div />",
                    attrs: {
                        class: "rlv_seriesInfoArea",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: item[i]
                });
                util.makeElement({
                    tag:"<span />",
                    attrs: {
                        class: "rlv_slText",
                        css: {
                            /*"font-family": "RixHead L",*/ float: "left"
                        }
                    },
                    text: "시리즈 예약",
                    parent: seriesInfoArea[i]
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/icon/icon_book_s.png",
                        class: "rlv_slImg",
                        css: {

                        }
                    },
                    parent: seriesInfoArea[i]
                });

                

            }; // end of forLoop


            /* popupItem div 생성하기*/
            optionBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBox",
                    css: {
                        position: "absolute", left: (1461-576), top: 125 , width:309,height:199 ,display: "none"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "option_2_background",
                    src: "images/option_dropbox_2.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 309, height: 136
                    }
                },
                parent: optionBox
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "option_3_background",
                    src: "images/option_dropbox_3.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 309, height: 199
                    }
                },
                parent: optionBox
            });

            var tempDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBoxInBox_0", /* 예약취소 */
                    css: {
                        position: "absolute", left : 4 , top : 4 , width: 301, height: 68
                    }
                },
                parent: optionBox
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBoxInBox_0_focus_background",
                    css: {
                        position: "absolute", left : 0 , top : 0 , width: 301, height: 65 , "background-color": "rgba(210,51,47,1)" , display:"none"
                    }
                },
                parent: tempDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class : "rlv_optionBoxInBox_0_Text" ,
                    css : {
                        position: "absolute", left: 32, top: 19, width: 250, height: 31,
                        "font-size": 27, "font-family": "RixHead L", "letter-spacing": -1.35, color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: "예약취소",
                parent: tempDiv
            });

            tempDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBoxInBox_1",
                    css: {
                        position: "absolute", left : 4 , top : 67 , width: 301, height: 68
                    }
                },
                parent: optionBox
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBoxInBox_1_focus_background",
                    css: {
                        position: "absolute", left : 0 , top : 0 , width: 301, height: 65 , "background-color": "rgba(210,51,47,1)" , display:"none"
                    }
                },
                parent: tempDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class : "rlv_optionBoxInBox_1_Text" ,
                    css : {
                        position: "absolute", left: 32, top: 19, width: 250, height: 31,
                        "font-size": 27, "font-family": "RixHead L", "letter-spacing": -1.35, color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: "예약취소",
                parent: tempDiv
            });


            tempDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBoxInBox_2",
                    css: {
                        position: "absolute", left : 4 , top : 130 , width: 301, height: 68
                    }
                },
                parent: optionBox
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_optionBoxInBox_2_focus_background",
                    css: {
                        position: "absolute", left : 0 , top : 0 , width: 301, height: 65 , "background-color": "rgba(210,51,47,1)" , display:"none"
                    }
                },
                parent: tempDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    class : "rlv_optionBoxInBox_2_Text" ,
                    css : {
                        position: "absolute", left: 32, top: 19, width: 250, height: 31,
                        "font-size": 27, "font-family": "RixHead L", "letter-spacing": -1.35, color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: "예약취소",
                parent: tempDiv
            });
            
            

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "rlv_autoStartInfoArea",
                    css: {
                        position: "absolute", visibility: "hidden"
                    }
                },
                parent: $(".rlv_item")

            });
            util.makeElement({
                tag:"<span />",
                attrs: {
                    class: "rlv_asText",
                    css: {
                        /*"font-family": "RixHead L", */float: "left"
                    }
                },
                text: "자동 켜짐 ON",
                parent: $(".rlv_autoStartInfoArea")
            });


        };

        function keyLeft() {
            log.printDbg("keyLeft()");

            var consume = false;
            var channelMaxPage = getMaxPage();

            if(isOptionBoxArea){

                setFocusOptionArea(isOptionBoxArea);

                return true;
            }

            return consume;
        };

        function keyRight() {
            log.printDbg("keyRight()");

            var programList = getReservationList();
            selectedProgram = programList[programIndex];

            if(isOptionBoxArea){
                return true;
            }
            if(isFocusedItemArea) {

                // option popup을 띄우기 전에 시리즈 예약 가능한 프로그램인지 알아본다.( except for list form mashup )

                if(!KTW.CONSTANT.IS_OTS) {
                    if(!isThisProgramFromMashUp(selectedProgram)) {
                        reservationManager.checkSeriesProgram(selectedProgram,cbCheckSeriesProgram);
                    } else {
                        //시리즈 프로그램
                        // 시리즈 프로그램인 경우, 시리즈 예약취소, 채널 바로가기 버튼 두가지를 보여주어야 함.
                        optionBoxMaxCount =  makeOptionBoxItem(false,true);
                        setFocusOptionArea(isOptionBoxArea);
                    }
                } else { // OTS
                    optionBoxMaxCount = makeOptionBoxItem();
                    setFocusOptionArea(isOptionBoxArea);
                }
            }
            else {
                isFocusedItemArea = true;
                setScrollFocus();
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
            }

            return true;
        };

        function keyUp() {
            log.printDbg("keyUp()");
            var pageBeforeMove = getCurrentPage();
            if(isOptionBoxArea){

                unsetFocusOptionBoxInBoxArea(optionBoxInBoxIndex);
                optionBoxInBoxIndex--;
                if(optionBoxInBoxIndex < 0){
                    optionBoxInBoxIndex = optionBoxInBoxString.length-1;
                }
                setFocusOptionBoxInBoxArea(optionBoxInBoxIndex);
                return;
            }

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_m");
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_l");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:0});
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 34);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.7);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 0.7)"});

            if(isFocusedItemArea){
                programIndex --;

            } else { // scrollArea

                programIndex = programIndex - ( NUMBER_OF_FOCUSED_ITEM + (programIndex % NUMBER_OF_FOCUSED_ITEM));
            }

            if(programIndex < 0) {
                programIndex = reservationList.length -1;
            }
            if(isFocusedItemArea){
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_l");
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_m");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:-2});
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 38);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.9);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
            }

            //set channel index...
            changeFocusedProgramIndex(programIndex,pageBeforeMove);

        };

        function keyDown() {
            log.printDbg("keyDown()");
            var pageBeforeMove = getCurrentPage();

            if(isOptionBoxArea){

                unsetFocusOptionBoxInBoxArea(optionBoxInBoxIndex);
                optionBoxInBoxIndex++;
                if(optionBoxInBoxIndex > optionBoxInBoxString.length-1){
                    optionBoxInBoxIndex = 0;
                }
                setFocusOptionBoxInBoxArea(optionBoxInBoxIndex);
                return;
            }

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_m");
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_l");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:0});
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 34);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.7);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 0.7)"});
            
            if(isFocusedItemArea) {
                programIndex ++;
            } else {
                programIndex = programIndex + (NUMBER_OF_FOCUSED_ITEM - (programIndex % NUMBER_OF_FOCUSED_ITEM));
            }

            if(programIndex > reservationList.length -1) {
                programIndex = 0;
            }
            if(isFocusedItemArea){
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_l");
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_m");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:-2});
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 38);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.9);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
            }

            changeFocusedProgramIndex(programIndex,pageBeforeMove);

        };

        function keyRed () {
            log.printDbg("keyRed()");

            if (isOptionBoxArea){
                return;
            }

            var pageBeforeMove = getCurrentPage();

            if (isFocusedItemArea) {
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_m");
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_l");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:0});
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 34);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.7);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 0.7)"});

                if (programIndex % NUMBER_OF_FOCUSED_ITEM === 0 && getMaxPage() > 1) {
                    programIndex = programIndex - NUMBER_OF_FOCUSED_ITEM;

                    if (programIndex < 0) {
                        programIndex = (getMaxPage() - 1) * NUMBER_OF_FOCUSED_ITEM;
                    }
                }
                else {
                    programIndex = pageBeforeMove * NUMBER_OF_FOCUSED_ITEM;
                }

                item[0].addClass("focus");

                item[0].find(".rlv_programName").removeClass("font_l");
                item[0].find(".rlv_programName").addClass("font_m");
                item[0].find(".rlv_programName").css({top:-2});
                item[0].find(".rlv_programName").css('font-size', 38);
                item[0].find(".rlv_programName").css('letter-spacing', -1.9);
                item[0].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
            }
            else {
                isFocusedItemArea = true;

                programIndex = pageBeforeMove * NUMBER_OF_FOCUSED_ITEM;
                item[0].addClass("focus");

                item[0].find(".rlv_programName").removeClass("font_l");
                item[0].find(".rlv_programName").addClass("font_m");
                item[0].find(".rlv_programName").css({top:-2});
                item[0].find(".rlv_programName").css('font-size', 38);
                item[0].find(".rlv_programName").css('letter-spacing', -1.9);
                item[0].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
                setScrollFocus();
            }

            //set channel index...
            changeFocusedProgramIndex(programIndex, pageBeforeMove);
        }

        function keyBlue () {
            log.printDbg("keyBlue()");

            if (isOptionBoxArea){
                return;
            }

            var pageBeforeMove = getCurrentPage();
            var reservationList = getReservationList();
            var nextPage = pageBeforeMove + 1;
            if (nextPage > getMaxPage() - 1) {
                nextPage = 0;
            }

            if (isFocusedItemArea) {
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_m");
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_l");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:0});
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 34);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.7);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 0.7)"});

                if ((programIndex % NUMBER_OF_FOCUSED_ITEM === NUMBER_OF_FOCUSED_ITEM - 1 || programIndex === reservationList.length - 1) && getMaxPage() > 1) {
                    programIndex = (nextPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;

                    if (programIndex > reservationList.length - 1) {
                        programIndex = reservationList.length - 1;
                    }
                }
                else {
                    programIndex = (pageBeforeMove * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
                    if (programIndex > reservationList.length - 1) {
                        programIndex = reservationList.length - 1;
                    }
                }

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_l");
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_m");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:-2});
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 38);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.9);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
            }
            else {
                isFocusedItemArea = true;

                programIndex = (pageBeforeMove * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
                if (programIndex > reservationList.length - 1) {
                    programIndex = reservationList.length - 1;
                }
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_l");
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_m");

                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:-2});
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 38);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.9);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
                setScrollFocus();
            }

            //set channel index...
            changeFocusedProgramIndex(programIndex, pageBeforeMove);
        }

        function keyOK () {
            log.printDbg("keyOK()");

            var programList = getReservationList();
            selectedProgram = programList[programIndex];
            var sid = null;
            var channel = null;

            if(isFocusedItemArea) {

                if(isOptionBoxArea){
                    // key 동작
                    switch(optionBoxInBoxString[optionBoxInBoxIndex]) {

                        case "CANCEL_RESERVATION" :
                            setFocusOptionArea(isOptionBoxArea);
                            if(isThisProgramFromMashUp(selectedProgram)) {
                                // cancel the reservation on seriesList
                                reservationManager.removeSeries(selectedProgram.transid,true,cbRemoveSeries);
                            } else {
                                reservationProgramme(selectedProgram);
                                setRevList(cbSeriesRevListForUpdate);
                            }

                            break;

                        case "RESERVATION_SERIES" :
                            setFocusOptionArea(isOptionBoxArea);
                            log.printDbg("RESERVATION_SERIES.transactionId:::::::"+transactionId);
                            reservationManager.removeShort(selectedProgram,false);
                            setTimeout(function() {
                                reservationManager.addSeries(transactionId, true, cbAddSeries);
                            }, 0);

                            break;

                        case "JUMP_TO_CHANNEL" :
                            setFocusOptionArea(isOptionBoxArea);
                            sid = selectedProgram.sid;
                            if(sid){
                                channel = navAdapter.getChannelBySID(sid);
                            } else{
                                channel = selectedProgram.channel;
                            }
                            setFocusChannelTune(channel);

                            break;

                        default:
                            break;
                    }

                } else {

                    // option popup을 띄우기 전에 시리즈 예약 가능한 프로그램인지 알아본다.( except for list form mashup )
                    if(!KTW.CONSTANT.IS_OTS) {
                        if(!isThisProgramFromMashUp(selectedProgram)) {
                            reservationManager.checkSeriesProgram(selectedProgram,cbCheckSeriesProgram);
                        } else {
                            //시리즈 프로그램
                            optionBoxMaxCount = makeOptionBoxItem(false,true);
                            setFocusOptionArea(isOptionBoxArea);
                        }
                    } else { // OTS
                        optionBoxMaxCount = makeOptionBoxItem();
                        setFocusOptionArea(isOptionBoxArea);
                    }

                }
            } else {
                isFocusedItemArea = true;
                setScrollFocus();
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
            }

        };

        function keyContext () {
            log.printDbg("keyContext()");

            selectedProgram = reservationList[programIndex];
            var list = reservationManager.getReservAutoList();
            var listLength = list.length;
            var hasAuto = false;
            var selectIdx = 0;
            var thisItemElement = item[programIndex % NUMBER_OF_FOCUSED_ITEM];


            for (var i = 0; i < listLength; i++ ) {
                if (selectedProgram.channelID === list[i].channelID && selectedProgram.startTime === list[i].startTime) {
                    // 자동 전원 켜짐 설정 여부 확인
                    hasAuto = true;
                    selectIdx = i;
                    break;
                }
            }

            if (hasAuto) {
                // 자동 켜짐 해제
                list.splice(selectIdx, 1);
            }
            else {
                // 자동 켜짐 설정
                list.push({
                    channelID: selectedProgram.channelID,
                    startTime: selectedProgram.startTime
                });
            }

            reservationManager.saveReservAutoList(list);

            drawReservedProgramList(dynamicList);
        }

        function setFocusChannelTune (channel) {
            log.printDbg("setFocusChannelTune()");

            parent.mainChannelTune({channel: channel});
        }

        function reservationProgramme (program) {
            log.printDbg("reservationProgramme()");

            reservationManager.add( program, true, function (result) {
                var RESULT = reservationManager.ADD_RESULT;
                if (result === RESULT.SUCCESS_RESERVE || result === RESULT.SUCCESS_CANCEL
                    || result === RESULT.SUCCESS_SERIES_RESERVE || result === RESULT.SUCCESS_SERIES_CANCEL
                    || result === RESULT.DUPLICATE_RESERVE) {
                }
            });
        }

        this.create = function () {
            log.printDbg("create()");

            createElement();
        };

        this.initData = function (cbInitData) {
            log.printDbg("initData()");

            rlv_itemGroup.css({display: "none"});

            setRevList(function (list) {
                cbSeriesRevListForInit(list);

                if (cbInitData) {
                    cbInitData();
                }
            });
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3);

            parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
            
            if (!options || !options.resume) {
                div.css({visibility: "inherit"});

            }
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {

                // hide
                div.css({visibility: "hidden"});

                if (div.hasClass("focus")) {
                    this.blur();
                }
            }
            else {
            }

        };

        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");
            var reservationList = getReservationList();
            programIndex = 0;
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_l");
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_m");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:-2});
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 38);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.9);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
            drawReservedProgramList();

            //turn on the scroll visibility
            if (reservationList && reservationList.length > 0) {
                scroll.show({
                    maxPage: getMaxPage(),
                    curPage: getCurrentPage()
                });
                setScrollFocus();

                if (getMaxPage() <= 1) {
                    subArea.find("[name=subArea_key_guide_area]").css({visibility: "hidden"});
                }
                else {
                    subArea.find("[name=subArea_key_guide_area]").css({visibility: "inherit"});
                }
            }

            isOptionBoxArea = true;
            setFocusOptionArea(isOptionBoxArea);
        };

        this.blur = function () {
            log.printDbg("blur()");
            div.removeClass("focus");
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").removeClass("font_m");
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").addClass("font_l");
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({top:0});
            
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('font-size', 34);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css('letter-spacing', -1.7);
            item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({color: "rgba(255, 255, 255, 0.7)"});

            item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("option");
            
            //optionBox.removeClass("focus");

            scroll.hide();
            isFocusedItemArea = true;

            programIndex = 0;

            drawReservedProgramList();

            isOptionBoxArea = true;
            setFocusOptionArea(isOptionBoxArea);

            //parent.setGuideMenuTitle(curMenu.parent);
        };

        this.remove = function () {
            log.printDbg("remove()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft();
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOK();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    if(isOptionBoxArea) {
                        setFocusOptionArea(isOptionBoxArea);
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    // 자동켜짐.
                    if(KTW.CONSTANT.IS_OTS && isFocusedItemArea) {
                        keyContext();
                    }
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;
        };

        this.isFocusAvailable = function () {
            log.printDbg("isFocusAvailable()");
            return isReservationListExist(this);
        };

        this.getDataStatus = function () {
            log.printDbg("getDataStatus()");
            if (!isReservationListExist(this)) {
                return KTW.ui.view.EpgView.DATA_STATUS.NO_RESERVATION;
            }
            else {
                return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
            }
        };

        function isReservationListExist () {
            log.printDbg("isReservationListExist()");

            var list = getReservationList();

            if(list && list.length > 0) {
                return true;
            }else {
                reservationList = null;

                return false;
            }
        };

        function isThisProgramFromMashUp (program) {

            return program.sid != undefined ? true : false ;

        }

        function setRevList (callback) {
            log.printDbg("setRevList()");

            reservationList = null;

            if (!KTW.CONSTANT.IS_OTS) {
                KTW.managers.service.ReservationManager.getSeriesRevList({ preventKey: true, callback: callback });
            }
            else {
                reservationList =  KTW.managers.service.ReservationManager.getRevListSortingByOption("startingTime","asc");
                callback();
            }
        };

        function getReservationList () {
            log.printDbg("getReservationList()");
            return reservationList;
        };

        function getMaxPage() {
            return Math.ceil(reservationList.length / NUMBER_OF_FOCUSED_ITEM);

        };

        function getCurrentPage() {
            return Math.floor(programIndex / NUMBER_OF_FOCUSED_ITEM);
        };

        function setScrollFocus() {

            log.printDbg("setFocus()");

            if (isFocusedItemArea) {
                scroll.setFocus(false);
                subArea.find("[name=subArea_auto_booting_guide_area]").css({visibility: "inherit"});
            }
            else {
                scroll.setFocus(true);
                subArea.find("[name=subArea_auto_booting_guide_area]").css({visibility: "hidden"});
            }
        };

        function drawReservedProgramList(programList){
            log.printDbg("drawReservedProgramList");

            var reservationProgList = programList;
            var program = null;
            var thisItemElement = null;
            var today = new Date().getDay();
            var dayForProgram = new Date();
            var autoList;
            if (!programList) {
                reservationProgList = getReservationList();
                if (!reservationProgList || reservationProgList.length < 1) {
                    return;
                }
            } else {
                reservationProgList = programList;
            }

            if (KTW.CONSTANT.IS_OTS) {
                autoList = reservationManager.getReservAutoList();
                checkAutoStartSubArea();
            }else {
                checkAutoStartSubArea();
            }

            for (var i =0; i < NUMBER_OF_FOCUSED_ITEM; i++) {
                thisItemElement = item[i];
                thisItemElement.css({ display: ""});

                //[sw.nam] 2017.05.19 화면에 노출 가능한 총 갯수보다 예약된 목록이 모자라는 경우 공간은 남겨두는 것으로 UI 수정
                thisItemElement.find(".rlv_chIcon").css({display: "none"});
                thisItemElement.find(".rlv_rlText").text("");
                thisItemElement.find(".rlv_ageIcon").css({display: "none"});
                thisItemElement.find(".rlv_programName").text("");
                thisItemElement.find(".rlv_programName").css({width:900});

                thisItemElement.find(".rlv_programName").removeClass("font_m");
                thisItemElement.find(".rlv_programName").addClass("font_l");

                thisItemElement.find(".rlv_programName").css({top:0});
                thisItemElement.find(".rlv_programName").css('font-size', 34);
                thisItemElement.find(".rlv_programName").css('letter-spacing', -1.7);
                thisItemElement.find(".rlv_programName").css({color: "rgba(255, 255, 255, 0.7)"});



                thisItemElement.find(".rlv_chName").text("");
                thisItemElement.find(".rlv_chNum").text("");
                thisItemElement.find(".rlv_reservationInfoArea").css({ visibility: "hidden" });
                thisItemElement.find(".rlv_seriesInfoArea").css({ visibility: "hidden"});

                if(KTW.CONSTANT.IS_OTS) {
                    thisItemElement.find(".rlv_autoStartInfoArea").css({ visibility: "hidden"});
                }

                program = reservationProgList[i];
                if(program) {
                    var channel   = null;
                    var chName    = null;
                    var chIcon    = null;
                    var chNum     = null;
                    var age       = null;
                    var programName = program.name;
                    var startTime = null;

                    if(isThisProgramFromMashUp(program)) {
                        // 시리즈 목록
                        var sid = program.sid;
                        channel = navAdapter.getChannelBySID(sid);
                        if(channel){
                            chName  = channel.name;
                            chNum   = channel.majorChannel;
                        }
                        //2017.05.29 sw.nam
                        // 시리즈 예약 프로그램 object는  SIDescriptors 값이 존재하지 않기 때문에
                        // epgUtil.getAge 를 사용 하면 무조건 null 을 리턴받게 되므로
                        // 시리즈 예약의 경우에만 자체적으로 조건을 걸어 age 값을 tuning 한다. ( 2.0 형상도 동일)
                        age = program.age;
                        if(age > 0 && age <=16) {
                            age = age + 3;
                        }
                        thisItemElement.find(".rlv_reservationInfoArea").css({ visibility: "hidden" });
                        thisItemElement.find(".rlv_seriesInfoArea").css({ visibility: "inherit"});
                        // thisItemElement.find(".rlv_autoStartInfoArea").css({ visibility: "hidden"});

                        //[sw.nam] 2017.04.24 채널아이콘 추가
                        chIcon = "";
                        if(KTW.CONSTANT.IS_OTS) {
                            thisItemElement.find(".rlv_chIcon").css({display: "block"});
                            if (channel && channel.favIDs.indexOf(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO) > -1) {
                                chIcon = "images/icon/icon_sky_epg.png";
                            }
                            else{
                                chIcon = "images/icon/icon_olleh_epg.png";
                            }
                            thisItemElement.find(".rlv_chIcon").attr({src: ""});
                            thisItemElement.find(".rlv_chIcon").attr({src: chIcon});
                        } else {
                            thisItemElement.find(".rlv_chIcon").css({display: "none"});
                        }

                    } else {
                        // 단편예약 목록
                        if(program.channel) {
                            chName = program.channel.name;
                            chNum = program.channel.majorChannel;
                        }
                        startTime = program.startTime * 1000;
                        dayForProgram.setTime(startTime);
                        // 연령 아이콘
                        age = KTW.utils.epgUtil.getAge(program);

                        var startTimeTxt = null;
                        if(today == dayForProgram.getDay()) {
                            startTimeTxt = "오늘 "+util.getTimeString(startTime)+" 방영";
                        }

                        else {
                            startTimeTxt = "내일 "+util.getTimeString(startTime)+" 방영";
                        }
                        thisItemElement.find(".rlv_rlText").text(startTimeTxt);
                        thisItemElement.find(".rlv_reservationInfoArea").css({ visibility: "inherit" });
                        // thisItemElement.find(".rlv_seriesInfoArea").css({ visibility: "hidden"});

                        //[sw.nam] 2017.04.24 채널아이콘 추가
                        chIcon = "";
                        if(KTW.CONSTANT.IS_OTS) {

                            if(program.channel) {
                                if (program.channel.favIDs.indexOf(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO) > -1) {
                                    chIcon = "images/icon/icon_sky_epg.png";
                                }
                                else{
                                    chIcon = "images/icon/icon_olleh_epg.png";
                                }
                                thisItemElement.find(".rlv_chIcon").css({display: "block"});
                                thisItemElement.find(".rlv_chIcon").attr({src: ""});
                                thisItemElement.find(".rlv_chIcon").attr({src:chIcon});
                            }


                            // sw.nam 자동 켜짐 text show/hide 체크 로직 위치 변경
                            if (autoList) {
                                var isSetAuto = false;
                                for (var index = 0; index < autoList.length; index++) {
                                    if (program.channelID === autoList[index].channelID && program.startTime === autoList[index].startTime) {
                                        // 자동 전원 켜짐 설정 여부 확인
                                        isSetAuto = true;
                                        break;
                                    }
                                }
                                if (isSetAuto){
                                    thisItemElement.find(".rlv_autoStartInfoArea").css({ visibility: "inherit"});
                                }
                                else {
                                    thisItemElement.find(".rlv_autoStartInfoArea").css({ visibility: "hidden"});
                                }
                            }
                        } else {
                            thisItemElement.find(".rlv_chIcon").css({display: "none"});
                        }

                    }

                    thisItemElement.find(".rlv_ageIcon").css({display: "block"});
                    var ageImg;
                    if (age && age > 0) {
                        ageImg = "images/icon/icon_age_list_" + age + ".png"
                    } else if (age === 0) {
                        ageImg = "images/icon/icon_age_list_all.png"
                    } else {
                        ageImg = "";
                        thisItemElement.find(".rlv_ageIcon").css({display: "none"});
                    }
                    thisItemElement.find(".rlv_ageIcon").attr({src: ageImg});

                    if(thisItemElement.hasClass("focus")) {
                        thisItemElement.find(".rlv_programName").removeClass("font_l");
                        thisItemElement.find(".rlv_programName").addClass("font_m");
                        thisItemElement.find(".rlv_programName").css({top:-2});
                        thisItemElement.find(".rlv_programName").css('font-size', 38);
                        thisItemElement.find(".rlv_programName").css('letter-spacing', -1.9);
                        thisItemElement.find(".rlv_programName").css({color: "rgba(255, 255, 255, 1)"});
                    }

                    thisItemElement.find(".rlv_programName").text(programName);
                    if(chNum && chName) {
                        thisItemElement.find(".rlv_chName").text(chName);
                        thisItemElement.find(".rlv_chNum").text(util.numToStr(chNum,3,null));
                    }

                    movePositionAgeIcon(i);

                }
            };

        };

        /**
         *  subArea 영역 자동켜짐 문구 표시 여부 체크하는 함수
         *
         */
        function checkAutoStartSubArea() {
            log.printDbg("checkAutoStartSubArea");
            var autoList;
            var hasAuto;
            var program;

            if(div.hasClass("focus") && isFocusedItemArea) {
                subArea.find("[name=subArea_auto_booting_guide_area]").css({visibility: "inherit"});

                hasAuto = false;
                autoList = reservationManager.getReservAutoList();
                program = reservationList[programIndex];

                for (var i = 0; i < autoList.length; i++) {
                    if (program.channelID === autoList[i].channelID && program.startTime === autoList[i].startTime) {
                        // 자동 전원 켜짐 설정 여부 확인
                        hasAuto = true;
                        break;
                    }
                }

                if (hasAuto){
                    subArea.find("[name=subArea_auto_booting_guide_area] .subArea_text").text("자동 켜짐 해제");
                }
                else {
                    subArea.find("[name=subArea_auto_booting_guide_area] .subArea_text").text("자동 켜짐 설정");
                }

            } else {
                subArea.find("[name=subArea_auto_booting_guide_area]").css({visibility: "hidden"});
            }
        }

        function changeFocusedProgramIndex(programIdx,prevPage) {
            log.printDbg("changeFocusedProgramIndex");

            var revList = getReservationList();
            var currentPage = getCurrentPage();
            var startIdx = programIdx - (programIdx % NUMBER_OF_FOCUSED_ITEM);
            dynamicList = [];

            for(var i = 0; i < NUMBER_OF_FOCUSED_ITEM; i++) {
                if(currentPage > 0 ) {
                    if(startIdx + i > revList.length-1) {
                        break;
                    }
                    dynamicList[i] = revList[startIdx+i];
                }
                else{
                    if(i > revList.length-1){
                        break;
                    }
                    dynamicList[i] = revList[i];
                }
            }
            if(prevPage !== undefined ) {
                if(prevPage === currentPage) {
                    log.printDbg("prevPage____"+prevPage);
                    if(KTW.CONSTANT.IS_OTS) {
                        checkAutoStartSubArea();
                    }

                    for(var i=0;i<dynamicList.length;i++) {
                        movePositionAgeIcon(i);
                    }
                    return;
                }
            }
            drawReservedProgramList(dynamicList);
            scroll.setCurrentPage(currentPage);
        }

        function movePositionAgeIcon(index) {
            var program = item[index].find(".rlv_programName").text();

            log.printDbg("movePositionAgeIcon() index : "+ index + " , text : " +  program);
            if(item[index].hasClass("focus") === true) {
                var length = util.getTextLength(program , "RixHead M" , 38,-1.9);

                log.printDbg("movePositionAgeIcon() focus length : "+ length );

                if(item[index].hasClass("option") === true) {
                    if(length>590) {
                        item[index].find(".rlv_ageIcon").css({left: 599});
                    }else {
                        item[index].find(".rlv_ageIcon").css({left: length+9});
                    }
                }else {
                    if(length>900) {
                        item[index].find(".rlv_ageIcon").css({left: 909});
                    }else {
                        item[index].find(".rlv_ageIcon").css({left: length+9});
                    }
                }
            }else {
                var length = util.getTextLength(program , "RixHead L" , 34,-1.7);
                log.printDbg("movePositionAgeIcon() unfocus length : "+ length );

                if(length>900) {
                    item[index].find(".rlv_ageIcon").css({left: 909});
                }else {
                    item[index].find(".rlv_ageIcon").css({left: length+9});
                }
            }

        }

        function setFocusOptionArea(option) {

            log.printDbg("setFocusOptionArea():::::::::::::::::::::::::::"+option);


            if(!option) {
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("option");
                if((programIndex % NUMBER_OF_FOCUSED_ITEM) === 0) {
                    optionBox.css({top: 125 });
                }else if((programIndex % NUMBER_OF_FOCUSED_ITEM) === 1) {
                    optionBox.css({top: (125+140) });
                }else if((programIndex % NUMBER_OF_FOCUSED_ITEM) === 2) {
                    optionBox.css({top: 404 });
                }else if((programIndex % NUMBER_OF_FOCUSED_ITEM) === 3) {
                    optionBox.css({top: 543});
                }else if((programIndex % NUMBER_OF_FOCUSED_ITEM) === 4) {
                    optionBox.css({top: 682});
                }else {
                    optionBox.css({top: 821});
                }

                if(optionBoxMaxCount === 3 && (programIndex % NUMBER_OF_FOCUSED_ITEM) === 5) {
                    optionBox.css({top: 765});
                }

                optionBox.css("display" , "");
                //optionBox.addClass("focus");
                isOptionBoxArea = true;
                setFocusOptionBoxInBoxArea(optionBoxInBoxIndex);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({width:590});
            } else {
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("option");
                //optionBox.removeClass("focus");
                isOptionBoxArea = false;
                unsetFocusOptionBoxInBoxArea(optionBoxInBoxIndex);
                optionBox.css("display", "none");
                optionBoxInBoxIndex = 0;
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].find(".rlv_programName").css({width:900});
            }
            movePositionAgeIcon(programIndex % NUMBER_OF_FOCUSED_ITEM);
        }

        function setFocusOptionBoxInBoxArea(idx) {
            optionBox.find(".rlv_optionBoxInBox_"+idx+ "_focus_background").css("display" , "");

            optionBox.find(".rlv_optionBoxInBox_"+idx+ "_Text").css({top: 18,"font-size": 29  , "font-family": "RixHead M"});
        }
        function unsetFocusOptionBoxInBoxArea(idx) {
            optionBox.find(".rlv_optionBoxInBox_"+idx+ "_focus_background").css("display" , "none");
            optionBox.find(".rlv_optionBoxInBox_"+idx+ "_Text").css({top: 19,"font-size": 27  , "font-family": "RixHead M"});
        }



        /*
         *  Callback functions for reservationManager
         *
         *
         * cb RemoveSeries 로직 동작 타이밍 이슈 발생하는듯.. 확인필요.
         *
         */
        function cbSeriesRevListForInit(list) {
            log.printDbg("cbSeriesRevListForInit()");

            var seriesList = list;
            var convertedSeriesList = [];
            var singleList = KTW.managers.service.ReservationManager.getRevListSortingByOption("startingTime","asc");
            if(!KTW.CONSTANT.IS_OTS) {
                if(seriesList) {
                    var j = 0;
                    for(var i = seriesList.length-1; i >= 0; i --) {
                        convertedSeriesList[j] = seriesList[i];
                        j++;
                    }
                    reservationList =  convertedSeriesList.concat(singleList);
                } else {
                    reservationList = singleList;
                }
            }
            if(isReservationListExist()) {
                rlv_itemGroup.css({display: ""});
                drawReservedProgramList();
                if(div.css("visibility") === "hidden") {
                    div.css({visibility: "inherit"});
                }
            } else {
                rlv_itemGroup.css({display: "none"});
                if(div.hasClass("focus")){

                }
                if(div.css("visibility") === "inherit") {
                    div.css({visibility: "hidden"});
                }
            }


        };

        function cbSeriesRevListForUpdate(list) {
            log.printDbg("cbSeriesRevListForUpdate()::"+ list);

            var seriesList = list;
            var convertedSeriesList = [];
            var singleList = KTW.managers.service.ReservationManager.getRevListSortingByOption("startingTime","asc");
            if(!KTW.CONSTANT.IS_OTS) {
                if(seriesList) {
                    var j = 0;
                    for(var i = seriesList.length-1; i >= 0; i-- ) {
                        convertedSeriesList[j] = seriesList[i];
                        j++;
                    }
                    reservationList =  convertedSeriesList.concat(singleList);
                } else {
                    reservationList = singleList;
                }
            }
            if (isReservationListExist()) {
                log.printDbg("isReservationListExist_______________"+ list);
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");
                if(programIndex > reservationList.length-1){
                    programIndex = reservationList.length-1;
                }
                item[programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
                changeFocusedProgramIndex(programIndex);
                scroll.show({
                    maxPage: getMaxPage(),
                    curPage: getCurrentPage()
                });
                setScrollFocus();

                if (getMaxPage() <= 1) {
                    subArea.find("[name=subArea_key_guide_area]").css({visibility: "hidden"});
                }
                else {
                    subArea.find("[name=subArea_key_guide_area]").css({visibility: "inherit"});
                }

            }
            else {
                setTimeout(function() {
                    //2017.05.04 [sw.nam] 메쉬업 메니져 호출 후 바로 메쉬업 메니져로 호출하게 되면서 delay가 발생하는것으로 보임.. 모든 작업 종료 후 호출하도록 수정
                    rlv_itemGroup.css({display: "none"});
                    parent.setFocusGuideMenu();
                    var reservationListMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === menuId) {
                                return true;
                            }
                        }
                    })[0];
                    parent.setEpgView({menu: reservationListMenu});
                }, 0);
            }
            div.css({visibility: "inherit"});
        };

        function cbRemoveSeries(result) {
            log.printDbg("cbRemoveSeries()============"+result);
            //sw.nam 2017.09.04
            // 매쉬업 서비스가 원활하지 않은 경우 result 값이 오지 않기 때문에 success 로 올 때만 처리되도록 예외처리 추가
            if(!result) {
                return ;
            }
            setTimeout(function() {
                setRevList(cbSeriesRevListForUpdate);
            }, 0);


        };

        function cbAddSeries(result) {
            log.printDbg("cbAddSeries()============"+result);
            //sw.nam 2017.09.04
            // 매쉬업 서비스가 원활하지 않은 경우 result 값이 오지 않기 때문에 success 로 올 때만 처리되도록 예외처리 추가
            if(!result) {
                return;
            }
            setTimeout(function() {
                setRevList(cbSeriesRevListForUpdate);
            }, 0);

        };

        function cbCheckSeriesProgram(b,transId) {
            log.printDbg("cbCheckSeriesProgram().transId:::::::"+transId);

            optionBoxMaxCount = makeOptionBoxItem(b,false);
            transactionId = transId;
            setFocusOptionArea(isOptionBoxArea);
        }

        /**
         * 현재 선택된 프로그램 기준에 따라 옵션 박스를 flexible 하게 배치
         * @param
         * isAvailable : 시리즈 예약 가능 여부
         * isSeries    : 이미 시리즈예약 된 프로그램
         * */
        function makeOptionBoxItem(isAvailable,isSeries) {
            log.printDbg("makeOptionBoxItem() _ isSeriesAvailable "+isAvailable);

            var isChannelObjExist;
            var optionCount = 0;
            if(isAvailable) {
                optionBox.find(".option_2_background").css("display" , "none");
                optionBox.find(".option_3_background").css("display","");

                // // 시리즈 예약이 가능한 단편 예약 프로그램 옵션박스 편집
                if(optionBoxInBoxString.length < 3 && optionBoxInBoxString[1].indexOf("RESERVATION_SERIES") < 0) {
                    optionBoxInBoxString.splice(1,0,"RESERVATION_SERIES");
                }
                if(optionBoxInBoxString.length > 2 && optionBoxInBoxString[2].indexOf("JUMP_TO_CHANNEL") < 0) {
                    optionBoxInBoxString.splice(1,1,"JUMP_TO_CHANNEL");
                }

                optionBox.find(".rlv_optionBoxInBox_0_focus_background").css("display " , "none");
                optionBox.find(".rlv_optionBoxInBox_1_focus_background").css("display " , "none");
                optionBox.find(".rlv_optionBoxInBox_2_focus_background").css("display " , "none");


                optionBox.find(".rlv_optionBoxInBox_0_Text").text("예약취소");
                optionBox.find(".rlv_optionBoxInBox_1_Text").text("시리즈 예약");
                optionBox.find(".rlv_optionBoxInBox_2_Text").text("채널 바로가기");
                optionBox.find(".rlv_optionBoxInBox_0").css("display" , "");
                optionBox.find(".rlv_optionBoxInBox_1").css("display" , "");
                optionBox.find(".rlv_optionBoxInBox_2").css("display" , "");
                optionCount = 3;
            }else {

                optionBox.find(".option_3_background").css("display" , "none");
                optionBox.find(".option_2_background").css("display","");

                optionBox.find(".rlv_optionBoxInBox_0_focus_background").css("display " , "none");
                optionBox.find(".rlv_optionBoxInBox_1_focus_background").css("display " , "none");
                optionBox.find(".rlv_optionBoxInBox_2_focus_background").css("display " , "none");


                // 일반 단편 예약 프로그램 옵션박스 편집
                if(optionBoxInBoxString.length > 2 && optionBoxInBoxString[1].indexOf("RESERVATION_SERIES") > -1) {
                    optionBoxInBoxString.splice(1,1);
                }
                // 예약된 프로그램의 채널 정보가 없는 경우 --> 채널 바로가기 버튼 삭제
                if(isSeries) {
                    optionBox.find(".rlv_optionBoxInBox_0_Text").text("시리즈 예약취소");
                    var channel;
                    channel = navAdapter.getChannelBySID(selectedProgram.sid);
                    if(!channel) {
                        isChannelObjExist = false;
                    }else {
                        isChannelObjExist = true;
                    }
                }else {
                    optionBox.find(".rlv_optionBoxInBox_0_Text").text("예약취소");
                    if(!selectedProgram.channel) {
                        isChannelObjExist = false;
                    }else {
                        isChannelObjExist = true;
                    }
                }

                if(isChannelObjExist) {
                    // 채널 정보가 있는데 이전 채널 빌드 때 이미 삭제되어 예약 취소 버튼만 있는 경우 --> JUMP_TO_CHANNEL 추가
                    if(optionBoxInBoxString.length <= 1) {
                        optionBoxInBoxString.splice(1,0,"JUMP_TO_CHANNEL");
                    }
                    optionBox.find(".rlv_optionBoxInBox_1_Text").text("채널 바로가기");
                    optionBox.find(".rlv_optionBoxInBox_1").css("display" , "");
                    optionCount = 2;
                }else {
                    if(optionBoxInBoxString.length > 1 && optionBoxInBoxString[1].indexOf("JUMP_TO_CHANNEL") > -1) {
                        optionBoxInBoxString.splice(1,1);
                    }
                    optionBox.find(".rlv_optionBoxInBox_1").css("display", "none");
                    optionCount = 1;
                }
                optionBox.find(".rlv_optionBoxInBox_2").css("display", "none");
            }
            
            return optionCount;
        }
    };
})();
