/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
/**
 * <code>TvAppView</code>
 * TV앱/ 쇼핑채널 View.
 * @author sw.nam
 * @since 2016-12-20
 */

(function() {
    KTW.ui.view.TvAppView = function (options) {
        KTW.ui.view.EpgView.call(this, options);

        this.curMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === options.menuId) {
                    return true;
                }
            }
        })[0];

        var CHANNELS_PER_SECTOR = 3;
        var SECTOR_PER_PAGE = 3;
        var CHANNELS_PER_PAGE = 9;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        var epgUtil = KTW.utils.epgUtil;
        var ocImageDataManager = KTW.managers.data.OCImageDataManager;

        var div = null;
        var appListArea = null;
        var subArea = null;
        var scrollArea = null;
        var scroll = null;

        var isFocusAppArea = true;

        var _this = this;

        var parent = options.parent;
        var menuId = options.menuId;

        //set basic elements
        function createElement () {
            log.printDbg("TV_APP_CHANNEL");

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "tv_app_channel_view",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                    }
                },
                parent: parent.getEpgContainerDiv()
            });

            appListArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "appList",
                    css: {
                        position: "absolute", overflow: "visible"
                    }
                },
                parent: div

            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "dim_vodlist_btm",
                    src: "images/dim_vodlist_btm.png",
                    css: {
                        position: "absolute", left: 0, top: 912, width: 1344, height: 168
                    }
                },
                parent: div
            });

            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1829-576), top: 149, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 702,
                forceHeight: true
            });



            subArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "tv_app_sub_area"
                },
                parent: div
            });

            var keyGuide = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "tv_app_sub_area_key_guide_area",
                    css: {
                        position: "absolute", overflow: "visible", visibility: "hidden"
                    }
                },
                parent: subArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: (1803 - 576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: (1831-576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1803 - 576), top: 939, width: 150, height: 24,
                        "font-size": 22, "font-family": "RixHead L", "letter-spacing": -1.1, color: "rgba(255, 255, 255, 0.3)"
                    }
                },
                text: "페이지",
                parent: keyGuide
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/key_shadow.png",
                    css: {
                        position: "absolute", left: (1115-576), top: 912 , width : 805 , height:168
                    }
                },
                parent: subArea
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_option_related.png",
                    css: {
                        position: "absolute", left: (1789-576), top: 1001  , width : 27 , height:22
                    }
                },
                parent: subArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1823 - 576), top: 1000, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                        "letter-spacing": -1.2
                    }
                },
                text: "옵션",
                parent: subArea
            });
        }

        /**
         * [dj.son] create page
         */
        function createPage (options) {
            var element = null;

            if (options.pageIdx >= 0 && options.pageIdx < _this.getChannelMaxPage()) {
                element = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "tv_app_page",
                        pageIdx: options.pageIdx,
                        css: {
                            overflow: "visible"
                        }
                    }
                });

                for (var i = 0; i < CHANNELS_PER_PAGE / CHANNELS_PER_SECTOR; i++) {
                    var sectorIdx = (options.pageIdx * (CHANNELS_PER_PAGE / CHANNELS_PER_SECTOR)) + i;

                    if (sectorIdx < getMaxSector()) {
                        var sector = createSector({
                            channelList: getChannelListBySector(sectorIdx),
                            sectorIdx: sectorIdx
                        });

                        element.append(sector);
                    }
                }
            }

            return element;
        }

        /**
         * [dj.son] create sector in page
         */
        function createSector (options) {
            var element = null;
            var appLogoInfo = ocImageDataManager.getAppLogoInfo();

            if (options.channelList && options.channelList.length > 0) {
                element = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "tv_app_sector",
                        sectorIdx: options.sectorIdx,
                        css: {
                            overflow: "visible"
                        }
                    }
                });


                for (var i = 0; i < options.channelList.length; i++) {
                    element.append(createChannelElement({
                        channel: options.channelList[i],
                        channelIdx: (options.sectorIdx * CHANNELS_PER_SECTOR) + i,
                        imagePath: appLogoInfo.path
                    } , i));
                }
            }

            return element;
        }


        /**
         * [dj.son] create tv app
         */
        function createChannelElement (options , idx) {
            var element = null;

            if (options && options.channel) {
                element = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "tv_app",
                        channelIdx: options.channelIdx,
                        css: {
                            position: "absolute", left : (400*idx)
                        }
                    }
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "tv_app_focus_img",
                        src: "images/app_focus_w350.png",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: element
                });

                var imgArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "tv_app_img_area",
                        css: {
                            position: "absolute", overflow: "visible"/*, "-webkit-transition": "-webkit-transform 0.5s"*/
                        }
                    },
                    parent: element
                });
                var appLogo = getAppLogo(options.channel.data.sid);
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "tv_app_img",
                        src: appLogo,
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: imgArea
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "tv_app_img tv_app_dim_img",
                        src: "images/app_dim_w330.png",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: imgArea
                });



                var textArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "tv_app_text_area",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: element
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "tv_app_name",
                        css: {
                            position: "absolute", height: 35, overflow: "visible" , "text-align": "left", "font-size": 30, "letter-spacing": -1.5
                        }
                    },
                    text: util.numToStr(options.channel.data.majorChannel, 3) + " " + options.channel.data.name,
                    parent: textArea
                });
            }

            return element;
        }

        function getAppLogo (sid) {
            var appLogoInfo = KTW.managers.data.OCImageDataManager.getAppLogoInfo();
            // 편성되지 않은 경우 사용할 default image
            var src = "images/default_app_w370.png";

            if (appLogoInfo && appLogoInfo.imageList && appLogoInfo.imageList.length > 0) {
                for (var i = 0; i < appLogoInfo.imageList.length; i++) {
                    var targetSrc = appLogoInfo.imageList[i];
                    if (targetSrc.indexOf(sid) > -1) {
                        src = appLogoInfo.path + targetSrc;
                        break;
                    }
                }
            }

            return src;
        }


        function init () {
            log.printDbg("init()");

            isFocusAppArea = true;

            appListArea.children().remove();

            for (var i = 0; i < 2; i++) {
                appListArea.append(createPage({
                    pageIdx: i
                }));
            }

            //setFocusArea();
            //setFocusChannel();
        }

        /**
         * [dj.son] 페이지별 채널 리스트 리턴
         */
        function getChannelListByPage (pageIdx) {
            log.printDbg("getChannelListByPage(" + pageIdx + ")");

            var result = [];
            var channelList = _this.getChannelList();
            var chLength = channelList.length;
            var startIdx = pageIdx * CHANNELS_PER_PAGE;

            if (pageIdx < 0 || pageIdx >= _this.getChannelMaxPage()) {
                return channelList;
            }

            for (var i = 0; i < CHANNELS_PER_PAGE; i++) {
                if (startIdx + i < chLength) {
                    result.push(channelList[startIdx + i]);
                }
                else {
                    break;
                }
            }

            return result;
        }

        /**
         * [dj.son] sector 의 max number 리턴
         */
        function getMaxSector () {
            return Math.ceil(_this.getChannelList().length / CHANNELS_PER_SECTOR);
        }

        /**
         * [dj.son] sector 별 채널 리스트 리턴
         */
        function getChannelListBySector (sectorIdx) {
            log.printDbg("getChannelListBySector(" + sectorIdx + ")");

            var result = [];
            var channelList = _this.getChannelList();
            var chLength = channelList.length;
            var startIdx = sectorIdx * CHANNELS_PER_SECTOR;

            if (sectorIdx < 0 || sectorIdx >= getMaxSector()) {
                return channelList;
            }

            for (var i = 0; i < CHANNELS_PER_SECTOR; i++) {
                if (startIdx + i < chLength) {
                    result.push(channelList[startIdx + i]);
                }
                else {
                    break;
                }
            }

            return result;
        }

        function isChannelListExist () {
            var channelList = _this.getChannelList();
            return channelList && channelList.length > 0 ? true : false;
        }

        function setFocusedArea () {
            log.printDbg("setFocusedArea()");

            if (isFocusAppArea) {
                appListArea.addClass("focus");
                scroll.setFocus(false);
            }
            else {
                appListArea.removeClass("focus");
                scroll.setFocus(true);
            }
        }

        function setFocusChannel () {
            log.printDbg("setFocusChannel()");

            appListArea.find(".tv_app").removeClass("focus");
            appListArea.find(".tv_app[channelIdx=" + _this.getChannelIdx() + "]").addClass("focus");
        }

        function moveFocusPage (options) {
            log.printDbg("moveFocusPage()");

            var curPageIdx = _this.getChannelPage();
            var maxPage = _this.getChannelMaxPage();

            if (curPageIdx === options.nextPageIdx) {
                return;
            }

            var allRemoveElements = ".tv_app_page_slide_down_delete, .tv_app_page_slide_down_delete_2, .tv_app_page_slide_down_delete_3"
                + ", .tv_app_page_slide_up_delete, .tv_app_page_slide_up_delete_2, .tv_app_page_slide_up_delete_3";
            appListArea.find(allRemoveElements).remove();

            var allRemoveClass = "tv_app_page_slide_up_add tv_app_page_slide_up_add_2 tv_app_page_slide_up_add_3"
                + " tv_app_page_slide_up_middle_fade_out + tv_app_page_slide_up_middle_fade_out_2 tv_app_page_slide_up_middle_fade_out_3"
                + " tv_app_page_slide_up_middle"
                + " tv_app_page_slide_down_middle_fade_in tv_app_page_slide_down_middle_fade_in_2 tv_app_page_slide_down_middle_fade_in_3"
                + " tv_app_page_slide_down_add tv_app_page_slide_down_add_2 tv_app_page_slide_down_add_3"
                + " tv_app_page_slide_down_add_hidden"
                + " tv_app_page_slide_down_middle";
            appListArea.children().removeClass(allRemoveClass);

            var existPage_0 = appListArea.find(".tv_app_page").first();
            var existPage_1 = appListArea.find(".tv_app_page").last();

            var appendPageIdx = options.nextPageIdx;
            if (!options.isUp) {
                appendPageIdx = options.nextPageIdx + 1;

                if (appendPageIdx > maxPage - 1) {
                    appendPageIdx = appendPageIdx - maxPage;
                }
            }

            var addPage = createPage({
                pageIdx: appendPageIdx
            });

            if (options.isUp) {
                appListArea.prepend(addPage);

                var existPageIdx_0 = existPage_0.attr("pageidx");

                if (existPageIdx_0 == "0") {
                    var addPageChildrenLength = addPage.children().length;

                    if (addPageChildrenLength === 1) {
                        addPage.addClass("tv_app_page_slide_up_add_3");
                        existPage_0.addClass("tv_app_page_slide_up_middle_fade_out_3");
                        existPage_1.addClass("tv_app_page_slide_up_delete_3");
                    }
                    else if (addPageChildrenLength === 2) {
                        addPage.addClass("tv_app_page_slide_up_add_2");
                        existPage_0.addClass("tv_app_page_slide_up_middle_fade_out_2");
                        existPage_1.addClass("tv_app_page_slide_up_delete_2");
                    }
                    else {
                        addPage.addClass("tv_app_page_slide_up_add");
                        existPage_0.addClass("tv_app_page_slide_up_middle_fade_out");
                        existPage_1.addClass("tv_app_page_slide_up_delete");
                    }
                }
                else {
                    addPage.addClass("tv_app_page_slide_up_add");
                    existPage_0.addClass("tv_app_page_slide_up_middle");
                    existPage_1.addClass("tv_app_page_slide_up_delete");
                }
            }
            else {
                appListArea.append(addPage);

                var existPageIdx_0 = existPage_0.attr("pageidx");

                if (parseInt(existPageIdx_0, 10) == maxPage - 1) {
                    var existPageChildrenLength = existPage_0.children().length;

                    if (existPageChildrenLength === 1) {
                        existPage_0.addClass("tv_app_page_slide_down_delete_3");
                        existPage_1.addClass("tv_app_page_slide_down_middle_fade_in_3");
                        addPage.addClass("tv_app_page_slide_down_add_3");
                    }
                    else if (existPageChildrenLength === 2) {
                        existPage_0.addClass("tv_app_page_slide_down_delete_2");
                        existPage_1.addClass("tv_app_page_slide_down_middle_fade_in_2");
                        addPage.addClass("tv_app_page_slide_down_add_2");
                    }
                    else {
                        existPage_0.addClass("tv_app_page_slide_down_delete");
                        existPage_1.addClass("tv_app_page_slide_down_middle_fade_in");
                        addPage.addClass("tv_app_page_slide_down_add");
                    }
                }
                else {
                    existPage_0.addClass("tv_app_page_slide_down_delete");
                    existPage_1.addClass("tv_app_page_slide_down_middle");
                    if (appendPageIdx === 0) {
                        addPage.addClass("tv_app_page_slide_down_add_hidden");
                    }
                    else {
                        addPage.addClass("tv_app_page_slide_down_add");
                    }
                }
            }

            _this.changeChannelIdx({
                nextChannelIdx: options.nextChannelIdx
            });
            setFocusChannel();
            scroll.setCurrentPage(_this.getChannelPage());
        }

        function setFocusChannelTune () {
            log.printDbg("setFocusChannelTune()");

            var channelList = _this.getChannelList();
            var channelIdx = _this.getChannelIdx();
            var focusChannel = channelList[channelIdx];

            _this.parent.mainChannelTune({channel: focusChannel});
        }

        function showRelatedMenu (_this) {
            log.printDbg("showRelatedMenu()");

            var isShowFavoritedChannel = false;
            var isShowProgramDetail = false;
            var isShowTwoChannel = false;
            var isShowFourChannel = true;
            var isShowHitChannel = true;
            var isShowGenreOption = false;

            var focusOptionIndex = 0;

            var focusMenuId = KTW.managers.data.MenuDataManager.MENU_ID.TV_APP_CHANNEL;


            var epgrelatedmenuManager = KTW.managers.service.EpgRelatedMenuManager;

            var curChannel = _this.getCurrentChannel();

            if(navAdapter.isAudioChannel(curChannel) === true) {
                isShowFavoritedChannel = false;
            }

            epgrelatedmenuManager.showFullEpgRelatedMenu(curChannel, function (menuIndex) {
                cbRelateMenuLeft({
                    _this: _this,
                    menuIndex: menuIndex
                });
            }, isShowFavoritedChannel, isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel, isShowGenreOption, focusOptionIndex, function (menuId) {
                cbRelateMenuRight({
                    _this: _this,
                    menuId: menuId
                });
            }, focusMenuId);
        }

        function cbRelateMenuLeft (options) {
            log.printDbg("cbRelateMenuLeft()");

            KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

            if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                // 선호채널 등록 / 해제
                var curChannel = options._this.getCurrentChannel();
                KTW.managers.service.EpgRelatedMenuManager.favoriteChannelOnOff(curChannel);
            }
            else if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                // OTS 자세히 버튼
                // TODO FullEpgProgramDetailPopup 호출해야 함
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                // 4채널
                var locator = KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();

                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                    catId: KTW.managers.data.MenuDataManager.MENU_ID.TV_APP_CHANNEL,
                    contsId: "",
                    locator: locator,
                    reqPathCd: ""
                });
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                // 실시간 인기 채널
                //KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();

                options._this.parent.jumpPopularMenu();
            }
        }

        function cbRelateMenuRight (options) {
            log.printDbg("cbRelateMenuRight()");

            KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

            if (options._this.menuId !== options.menuId) {
                // TODO 가이드 메뉴 가져오는 로직으로 수정
                var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.id === options.menuId) {
                            return true;
                        }
                    }
                })[0];

                if (epgMenu) {
                    options._this.parent.jumpTargetEpgView({
                        targetMenu: epgMenu
                    });
                }
                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                    catId: KTW.managers.data.MenuDataManager.MENU_ID.TV_APP_CHANNEL,
                    contsId: "",
                    locator: "",
                    reqPathCd: ""
                });
            }
        }

        function keyLeft () {
            log.printDbg("keyLeft()");

            var consume = false;

            if (isFocusAppArea) {
                var channelIdx = _this.getChannelIdx();

                if (channelIdx % CHANNELS_PER_SECTOR === 0) {
                    isFocusAppArea = false;
                    _this.changeChannelIdx({
                        nextChannelIdx: channelIdx - (channelIdx % CHANNELS_PER_PAGE)
                    });
                    return consume;
                    // if(scroll.maxPage > 1) {
                    //     setFocusedArea();
                    // } else {
                    //     return consume;
                    // }
                }
                else {
                    _this.changeChannelIdx({
                        nextChannelIdx: channelIdx - 1
                    });
                    setFocusChannel();
                }

                consume = true;
            }

            return consume;
        }

        function keyRight () {
            log.printDbg("keyRight()");

            if (isFocusAppArea) {
                var channelIdx = _this.getChannelIdx();
                var channelLength = _this.getChannelList().length;
                var nextChannelIdx = channelIdx + 1;
                if (nextChannelIdx > channelLength - 1) {
                    nextChannelIdx = 0;
                }
                var curPageIdx = _this.getChannelPage();
                var nextPageIdx = curPageIdx + 1;
                if (nextPageIdx >= _this.getChannelMaxPage()) {
                    nextPageIdx = 0;
                }

                if (_this.getChannelMaxPage() > 1) {
                    if (channelIdx === channelLength - 1 || channelIdx % CHANNELS_PER_PAGE === CHANNELS_PER_PAGE - 1) {
                        moveFocusPage({
                            nextPageIdx: nextPageIdx,
                            nextChannelIdx: nextChannelIdx
                        });
                    }
                    else {
                        _this.changeChannelIdx({
                            nextChannelIdx: nextChannelIdx
                        });
                        setFocusChannel();
                    }
                }
                else {
                    _this.changeChannelIdx({
                        nextChannelIdx: nextChannelIdx
                    });
                    setFocusChannel();
                }
            }
            else {
                isFocusAppArea = true;
                setFocusedArea();
                setFocusChannel();
            }

            return true;
        }

        function keyUp () {
            log.printDbg("keyUp()");

            var channelIdx = _this.getChannelIdx();
            var channelLength = _this.getChannelList().length;
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx - 1;
            if (nextPageIdx < 0) {
                nextPageIdx = _this.getChannelMaxPage() - 1;
            }

            if (isFocusAppArea) {
                if (getMaxSector() > 1) {
                    var nextChannelIdx = channelIdx - CHANNELS_PER_SECTOR;

                    if (getMaxSector() > SECTOR_PER_PAGE) {
                        if (nextChannelIdx < 0) {
                            nextChannelIdx = ((getMaxSector() - 1) * CHANNELS_PER_SECTOR) + channelIdx;
                            if (nextChannelIdx > channelLength - 1) {
                                nextChannelIdx = channelLength - 1;
                            }

                            moveFocusPage({
                                nextPageIdx: nextPageIdx,
                                nextChannelIdx: nextChannelIdx,
                                isUp: true
                            });
                        }
                        else {
                            if (nextChannelIdx < curPageIdx * CHANNELS_PER_PAGE) {
                                moveFocusPage({
                                    nextPageIdx: nextPageIdx,
                                    nextChannelIdx: nextChannelIdx,
                                    isUp: true
                                });
                            }
                            else {
                                _this.changeChannelIdx({
                                    nextChannelIdx: nextChannelIdx
                                });
                                setFocusChannel();
                            }
                        }
                    }
                    else {
                        if (nextChannelIdx < 0) {
                            nextChannelIdx = ((getMaxSector() - 1) * CHANNELS_PER_SECTOR) + channelIdx;
                            if (nextChannelIdx > channelLength - 1) {
                                nextChannelIdx = channelLength - 1;
                            }
                        }

                        _this.changeChannelIdx({
                            nextChannelIdx: nextChannelIdx
                        });
                        setFocusChannel();
                    }
                }
            }
            else {
                if (_this.getChannelMaxPage() > 1) {
                    var nextChannelIdx = channelIdx - (CHANNELS_PER_PAGE + (channelIdx % CHANNELS_PER_SECTOR));
                    if (nextChannelIdx < 0) {
                        channelIdx = channelLength -1;
                        nextChannelIdx = channelIdx - (channelIdx % CHANNELS_PER_PAGE) ;
                    }
                    moveFocusPage({
                        nextPageIdx: nextPageIdx,
                        nextChannelIdx: nextChannelIdx,
                        isUp: true
                    });
                }
            }
        }

        function keyDown () {
            log.printDbg("keyDown()");

            var channelIdx = _this.getChannelIdx();
            var channelLength = _this.getChannelList().length;
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx + 1;
            if (nextPageIdx >= _this.getChannelMaxPage()) {
                nextPageIdx = 0;
            }
            var maxSector = getMaxSector();

            if (isFocusAppArea) {
                if (getMaxSector() > 1) {
                    var nextChannelIdx = channelIdx + CHANNELS_PER_SECTOR;

                    if (getMaxSector() > SECTOR_PER_PAGE) {
                        if (nextChannelIdx > channelLength - 1) {
                            var nextSector = Math.ceil((channelIdx + 1) / CHANNELS_PER_SECTOR);

                            if ((nextSector % SECTOR_PER_PAGE === 0 && nextSector == maxSector - 1) || nextSector > maxSector - 1) {
                                if (nextPageIdx > 0) {
                                    nextChannelIdx = channelLength - 1;
                                }
                                else {
                                    var curSector = Math.floor(channelIdx / CHANNELS_PER_SECTOR);
                                    nextChannelIdx = channelIdx - (CHANNELS_PER_SECTOR * curSector);
                                }

                                moveFocusPage({
                                    nextPageIdx: nextPageIdx,
                                    nextChannelIdx: nextChannelIdx
                                });
                            }
                            else {
                                nextChannelIdx = channelLength - 1;
                                _this.changeChannelIdx({
                                    nextChannelIdx: nextChannelIdx
                                });
                                setFocusChannel();
                            }
                        }
                        else {
                            if (nextChannelIdx >= (curPageIdx + 1) * CHANNELS_PER_PAGE) {
                                moveFocusPage({
                                    nextPageIdx: nextPageIdx,
                                    nextChannelIdx: nextChannelIdx
                                });
                            }
                            else {
                                _this.changeChannelIdx({
                                    nextChannelIdx: nextChannelIdx
                                });
                                setFocusChannel();
                            }
                        }
                    }
                    else {
                        if (nextChannelIdx > channelLength - 1) {
                            var curSector = Math.floor(channelIdx / CHANNELS_PER_SECTOR);
                            if (curSector === getMaxSector() - 1) {
                                nextChannelIdx = channelIdx - (CHANNELS_PER_SECTOR * curSector);
                            }
                            else {
                                nextChannelIdx = channelLength - 1;
                            }
                        }

                        _this.changeChannelIdx({
                            nextChannelIdx: nextChannelIdx
                        });
                        setFocusChannel();
                    }
                }
            }
            else {
                if (_this.getChannelMaxPage() > 1) {
                    var nextChannelIdx = channelIdx + CHANNELS_PER_SECTOR;
                    var tempPageIdx = Math.floor(nextChannelIdx / CHANNELS_PER_PAGE);

                    if (tempPageIdx === curPageIdx) {
                        if (nextChannelIdx < channelLength) {
                            nextChannelIdx = nextChannelIdx + CHANNELS_PER_SECTOR;
                            tempPageIdx = Math.floor(nextChannelIdx / CHANNELS_PER_PAGE);
                            if (tempPageIdx === curPageIdx) {
                                if (nextChannelIdx < channelLength) {
                                    nextChannelIdx = nextChannelIdx + CHANNELS_PER_SECTOR;
                                }
                            }
                        }
                    }

                    if (nextChannelIdx > channelLength - 1) {
                        nextChannelIdx = 0;
                    }

                    moveFocusPage({
                        nextPageIdx: nextPageIdx,
                        nextChannelIdx: nextChannelIdx
                    });
                }
            }
        }

        function keyRed () {
            log.printDbg("keyRed()");

            var channelIdx = _this.getChannelIdx();
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx - 1;
            if (nextPageIdx < 0) {
                nextPageIdx = _this.getChannelMaxPage() - 1;
            }

            var nextChannelIdx = channelIdx;

            if (isFocusAppArea) {
                if (channelIdx % CHANNELS_PER_PAGE > 0) {
                    nextChannelIdx = curPageIdx * CHANNELS_PER_PAGE;

                    _this.changeChannelIdx({
                        nextChannelIdx: nextChannelIdx
                    });
                    setFocusChannel();
                }
                else {
                    if (_this.getChannelMaxPage() > 1) {
                        nextChannelIdx = nextPageIdx * CHANNELS_PER_PAGE;

                        moveFocusPage({
                            nextPageIdx: nextPageIdx,
                            nextChannelIdx: nextChannelIdx,
                            isUp: true
                        });
                    }
                }
            }
            else {
                isFocusAppArea = true;

                nextChannelIdx = curPageIdx * CHANNELS_PER_PAGE;
                _this.changeChannelIdx({
                    nextChannelIdx: nextChannelIdx
                });

                setFocusedArea();
                setFocusChannel();
            }
        }

        function keyBlue () {
            log.printDbg("keyBlue()");

            var channelIdx = _this.getChannelIdx();
            var channelLength = _this.getChannelList().length;
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx + 1;
            if (nextPageIdx >= _this.getChannelMaxPage()) {
                nextPageIdx = 0;
            }
            var nextChannelIdx = channelIdx;

            var bMovePage = true;

            if (isFocusAppArea) {
                if (curPageIdx === _this.getChannelMaxPage() - 1) {
                    if (channelIdx < channelLength - 1) {
                        nextChannelIdx = channelLength - 1;
                        bMovePage = false;
                    }
                }
                else {
                    if (channelIdx % CHANNELS_PER_PAGE < CHANNELS_PER_PAGE - 1) {
                        nextChannelIdx = (curPageIdx * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;
                        bMovePage = false;
                    }
                }


                if (bMovePage) {
                    if (_this.getChannelMaxPage() > 1) {
                        nextChannelIdx = (nextPageIdx * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;

                        if (nextChannelIdx > channelLength - 1) {
                            nextChannelIdx = channelLength - 1;
                        }

                        moveFocusPage({
                            nextPageIdx: nextPageIdx,
                            nextChannelIdx: nextChannelIdx
                        });
                    }
                }
                else {
                    _this.changeChannelIdx({
                        nextChannelIdx: nextChannelIdx
                    });
                    setFocusChannel();
                }
            }
            else {
                isFocusAppArea = true;

                nextChannelIdx = (curPageIdx * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;
                if (nextChannelIdx > channelLength - 1) {
                    nextChannelIdx = channelLength - 1;
                }
                _this.changeChannelIdx({
                    nextChannelIdx: nextChannelIdx
                });

                setFocusedArea();
                setFocusChannel();
            }
        }

        function keyOk () {
            log.printDbg("keyOk()");

            if(isFocusAppArea) {
                setFocusChannelTune();
            } else {
                isFocusAppArea = true;
                setFocusedArea();
                setFocusChannel();
            }
        }


        this.create = function () {
            log.printDbg("create()");

            createElement();
        };

        this.getDiv = function () {
            return div;
        };

        this.initData = function () {
            // 채널 리스트, 프로그램 리스트, channelIdx 설정
            this.initEpgData();
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3);

            if (!options || !options.resume) {
                init();
            }else {
                if (div.hasClass("focus")) {
                    parent.setVideoScreenPosition({
                        mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
                    });

                }
            }
            div.css({visibility: "inherit"});
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});

                if (div.hasClass("focus")) {
                    this.blur();
                }
            }
            else {
                var allRemoveElements = ".tv_app_page_slide_down_delete, .tv_app_page_slide_down_delete_2, .tv_app_page_slide_down_delete_3"
                    + ", .tv_app_page_slide_up_delete, .tv_app_page_slide_up_delete_2, .tv_app_page_slide_up_delete_3";
                appListArea.find(allRemoveElements).remove();

                var allRemoveClass = "tv_app_page_slide_up_add tv_app_page_slide_up_add_2 tv_app_page_slide_up_add_3"
                    //+ " tv_app_page_slide_up_middle_fade_out + tv_app_page_slide_up_middle_fade_out_2 tv_app_page_slide_up_middle_fade_out_3"
                    + " tv_app_page_slide_up_middle"
                    + " tv_app_page_slide_down_middle_fade_in tv_app_page_slide_down_middle_fade_in_2 tv_app_page_slide_down_middle_fade_in_3"
                    + " tv_app_page_slide_down_add tv_app_page_slide_down_add_2 tv_app_page_slide_down_add_3"
                    //+ " tv_app_page_slide_down_add_hidden"
                    + " tv_app_page_slide_down_middle";
                appListArea.children().removeClass(allRemoveClass);

                appListArea.find(".tv_app_page_slide_up_middle_fade_out, .tv_app_page_slide_up_middle_fade_out_2, .tv_app_page_slide_up_middle_fade_out_3").removeClass("community_page_slide_up_middle2").addClass("tv_app_page_slide_down_add_hidden");
            }
        };

        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");


            isFocusAppArea = true;
            setFocusedArea();
            setFocusChannel();

            if (isChannelListExist()) {
                scroll.show({
                    maxPage: _this.getChannelMaxPage(),
                    curPage: _this.getChannelPage()
                });

                if (_this.getChannelMaxPage() <= 1) {
                    appListArea.addClass("noScroll");
                    subArea.find("[name=tv_app_sub_area_key_guide_area]").css({visibility: "hidden"});
                }
                else {
                    subArea.find("[name=tv_app_sub_area_key_guide_area]").css({visibility: "inherit"});

                }
            }

            parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.removeClass("focus");
            if(appListArea.hasClass("noScroll")) {
                appListArea.removeClass("noScroll");
            }

            isFocusAppArea = true;

            scroll.hide();
            appListArea.children().css({"-webkit-transform": "translateY(0px)"});

            this.initData();
            init();
        };

        this.remove = function () {};

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft();
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    // 2017.06.23 dhlee
                    // scroll에 포커스가 있는 상태이더라도 content menu가 동작 해야 한다. (UI 변경 요청 사항 반영)
                    showRelatedMenu(this);
                    /*
                     if (isFocusAppArea) {
                     showRelatedMenu(this);
                     }
                     */
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;//isChannelListExist(this);
        };

        this.isFocusAvailable = function () {
            return isChannelListExist(this);
        };

        this.getDataStatus = function () {
            if (!isChannelListExist(this)) {
                return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
            }
            else {
                return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
            }
        };
    };

    KTW.ui.view.TvAppView.prototype = new KTW.ui.view.EpgView();
    KTW.ui.view.TvAppView.prototype.constructor = KTW.ui.view.TvAppView;
})();