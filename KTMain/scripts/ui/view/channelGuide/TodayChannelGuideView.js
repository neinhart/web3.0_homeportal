/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>TodayChannelGuideView</code>
 *
 * @author dj.son
 * @since 2016-11-08
 */


(function() {

    var DATA_TYPE = {
        BANNER: 0,
        RECOMMEND: 1,
        CHANNEL_BANNER: 2
    };

    var FOCUS_HEIGHT = {
        BANNER: 559,
        RECOMMEND: 475,
        CHANNEL_BANNER: 280
    };

    var BANNER_TYPE = {
        B1: "B1",
        B2: "B2",
        B3: "B3",
        B4: "B4"
    };


    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var _this = null;

    var parent = null;
    var menu = null;

    var div = null;
    var subHomeArea = null;
    var channelBannerArea = null;
    var keyGuide = null;

    var scroll = null;

    var data = null;
    var subHomeData = null;
    var arrIsMyContents = [];

    var defaultHomeShowIdx = 0;

    var isScrollFocus = false;
    var focusAreaIdx = 0;
    var focusItemIdx = 0;
    var maxPage;

    /**
     * [dj.son] 기본 element 생성
     */
    function createElement () {
        log.printDbg("createElement()");

        div = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide",
                css: {
                    position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                }
            },
            parent: parent.getEpgContainerDiv()
        });

        subHomeArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_subhome_area",
                css: {
                    position: "absolute", left: 70, top: 122, overflow: "visible", "-webkit-transition": "-webkit-transform 0.5s linear"
                }
            },
            parent: div
        });

        var scrollArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_scroll",
                css: {
                    position: "absolute", left: 1254, top: 142, overflow: "visible"
                }
            },
            parent: div
        });
        scroll = new KTW.ui.component.Scroll({
            parentDiv: scrollArea,
            height: 726
        });

        keyGuide = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_key_guide_area",
                css: {
                    position: "absolute", left: 1227, top: 904, overflow: "visible", visibility: "hidden"
                }
            },
            parent: div
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pageup.png",
                css: {
                    position: "absolute"
                }
            },
            parent: keyGuide
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pagedown.png",
                css: {
                    position: "absolute", left: 28
                }
            },
            parent: keyGuide
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 0, top: 35, width: 150, height: 35,
                    "font-size": 22, "font-family": "RixHead L", "letter-spacing": -1.1, color: "rgba(255, 255, 255, 0.3)"
                }
            },
            text: "페이지",
            parent: keyGuide
        });

        var relatedArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_channel_related_area",
                css: {
                    position: "absolute", left: 982, top: 1001, width: 300, visibility: "hidden", overflow: "visible"
                }
            },
            parent: div
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "relative", float: "right", top: -2, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                    "letter-spacing": -1.2
                }
            },
            parent: relatedArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_option_related.png",
                css: {
                    position: "relative", float: "right", "margin-right": 7
                }
            },
            parent: relatedArea
        });

    }

    /**
     * Banner 이미지의 width return
     */
    function getBannerImageWidth (options) {
        var imgWidth = 0;

        switch (options.data.templateType) {
            case BANNER_TYPE.B1:
                imgWidth = options.isFocus ? 1194 : 840;
                break;
            case BANNER_TYPE.B2:
                if (options.data.templateLoc === "1") {
                    imgWidth = options.isFocus ? 702 : 504;
                }
                else {
                    imgWidth = options.isFocus ? 465 : 336;
                }
                break;
            case BANNER_TYPE.B3:
                imgWidth = 366;
                break;
            case BANNER_TYPE.B4:
                imgWidth = options.isFocus ? 590 : 420;
                break;
        }

        return imgWidth;
    }

    /**
     * Banner 이미지의 width return
     */
    function getBannerWidth (bannerData) {
        var bannerWidth = 0;

        switch (bannerData.templateType) {
            case BANNER_TYPE.B1:
                bannerWidth = 1140;
                break;
            case BANNER_TYPE.B2:
                if (bannerData.templateLoc === "1") {
                    bannerWidth = 684;
                }
                else {
                    bannerWidth = 474;
                }
                break;
            case BANNER_TYPE.B3:
                bannerWidth = 377;
                break;
            case BANNER_TYPE.B4:
                bannerWidth = 567;
                break;
        }

        return bannerWidth;
    }

    function getBannerImageSize (options) {
        var bannerSize = {};

        switch (options.bannerData.templateType) {
            case BANNER_TYPE.B1:
                if (options.isFocus) {
                    bannerSize.width = 1154;
                    bannerSize.height = 564;
                }
                else {
                    bannerSize.width = 1120;
                    bannerSize.height = 548;
                }
                break;
            case BANNER_TYPE.B2:
                if (options.bannerData.templateLoc === "1") {
                    if (options.isFocus) {
                        bannerSize.width = 691;
                        bannerSize.height = 475;
                    }
                    else {
                        bannerSize.width = 659;
                        bannerSize.height = 453;
                    }
                }
                else {
                    if (options.isFocus) {
                        bannerSize.width = 458;
                        bannerSize.height = 475;
                    }
                    else {
                        bannerSize.width = 436;
                        bannerSize.height = 453;
                    }
                }
                break;
            case BANNER_TYPE.B3:
                if (options.isFocus) {
                    bannerSize.width = 384;
                    bannerSize.height = 475;
                }
                else {
                    bannerSize.width = 366;
                    bannerSize.height = 453;
                }
                break;
            case BANNER_TYPE.B4:
                if (options.isFocus) {
                    bannerSize.width = 581;
                    bannerSize.height = 609;
                }
                else {
                    bannerSize.width = 533;
                    bannerSize.height = 575;
                }
                break;
        }

        return bannerSize;
    }

    function getDefaultBannerImageSrc (bannerData) {
        var src = "";

        switch (bannerData.templateType) {
            case BANNER_TYPE.B1:
                src = KTW.COMMON_IMAGE.BANNER_DEFAULT + "default_recommend_big_w1194.png";
                break;
            case BANNER_TYPE.B2:
                if (bannerData.templateLoc === "1") {
                    src = KTW.COMMON_IMAGE.BANNER_DEFAULT + "default_recommend_big_w702.png";
                }
                else {
                    src = KTW.COMMON_IMAGE.BANNER_DEFAULT + "default_recommend_big_w465.png";
                }
                break;
            case BANNER_TYPE.B3:
                src = KTW.COMMON_IMAGE.BANNER_DEFAULT + "default_recommend_big_w390.png";
                break;
            case BANNER_TYPE.B4:
                src = KTW.COMMON_IMAGE.BANNER_DEFAULT + "default_recommend_big_w590.png";
                break;
        }

        return src;
    }

    function createBanner (options) {
        log.printDbg("createBanner(" + log.stringify(options) + ")");

        var bannerData = options.data;


        var bannerClassName = "today_channel_guide_banner_" + bannerData.templateType;
        if (bannerData.templateType === BANNER_TYPE.B2) {
            bannerClassName += "_" + bannerData.templateLoc;
        }

        var bannerWidth = getBannerWidth(bannerData);


        var banner = util.makeElement({
            tag: "<div />",
            attrs: {
                class: bannerClassName,
                index: options.index,
                css: {
                    position: "relative", float: "left", width: bannerWidth, height: 453, overflow: "visible"
                }
            }
        });

        if (bannerData.templateType !== BANNER_TYPE.B1) {
            var focusBannerImageSize = getBannerImageSize({bannerData: bannerData, isFocus: true});
            // sdw
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "today_channel_guide_banner_sdw",
                    src: "images/banner_focus_w398_shadow_l.png",
                    css: {
                        position: "absolute", "margin-left": 0, width: 68, height: 551
                    }
                },
                parent: banner
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "today_channel_guide_banner_sdw",
                    src: "images/banner_focus_w398_shadow_m.png",
                    css: {
                        position: "absolute", "margin-left": 68, width: (focusBannerImageSize.width - 60), height: 551
                    }
                },
                parent: banner
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "today_channel_guide_banner_sdw",
                    src: "images/banner_focus_w398_shadow_r.png",
                    css: {
                        position: "absolute", "margin-left": (focusBannerImageSize.width + 8), width: 68, height: 551
                    }
                },
                parent: banner
            });
        }

        // focus bar
        util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_focus_bar",
                css: {
                    position: "absolute", "background-color": "rgba(0, 0, 0, 1)", border: "solid red 6px", "box-sizing": "border-box"
                }
            },
            parent: banner
        });

        // poster
        util.makeElement({
            tag: "<img />",
            attrs: {
                class: "today_channel_guide_banner_img",
                //src: data.imgUrl,
                css: {
                    position: "absolute", border: "none"
                }
            },
            parent: banner
        }).error(function () {
            $(this).attr("src", getDefaultBannerImageSrc(bannerData));
        }).attr("src", bannerData.imgUrl);


        return banner;
    }

    function createBannerArea (options) {
        log.printDbg("createBannerArea()");

        var bannerArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_child_area today_channel_guide_banner_area",
                index: options.index,
                css: {
                    overflow: "visible"
                }
            },
            parent: subHomeArea
        });

        var maxNum = null;
        switch (options.data[0].templateType) {
            case BANNER_TYPE.B1:
                maxNum = 1;
                break;
            case BANNER_TYPE.B2:
                maxNum = 2;
                break;
            case BANNER_TYPE.B3:
                maxNum = 3;
                break;
            case BANNER_TYPE.B4:
                maxNum = 2;
                break;
        }
        maxNum = maxNum > options.data.length ? options.data.length : maxNum;

        for (var i = 0; i < maxNum; i++) {
            var banner = createBanner({
                data: options.data[i],
                index: i
            });
            bannerArea.append(banner);
        }
    }

    function createRecommend (options) {
        log.printDbg("createRecommend(" + log.stringify(options) + ")");

        var recommendData = options.data;

        var recommend = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_recommend",
                index: options.index,
                css: {
                    position: "relative", float: "left", width: 231, height: 370, overflow: "visible"
                }
            }
        });
        var recommendImgArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_recommend_img_area",
                css: {
                    position: "absolute", width: 276, height: 207, overflow: "visible" /*"-webkit-transition": "-webkit-transform 0.5s"*/
                }
            },
            parent: recommend
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_recommend_focus_bar",
                css: {
                    position: "absolute", "margin-left": -14, "margin-top": -21, width: 224, height: 314,
                    "background-color": "rgba(0, 0, 0, 1)", border: "solid red 6px", "box-sizing": "border-box"
                }
            },
            parent: recommendImgArea
        });
        var imgBox;
        if (options.index === 4 && options.dataLength > 5) {
            imgBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_recommend_img today_channel_guide_recommend_img_blur",
                    css: {
                        position: "absolute", overflow: "hidden", "-webkit-transform": "translateZ(0)"
                    }
                },
                parent: recommendImgArea
            });
        }
        util.makeElement({
            tag: "<img />",
            attrs: {
                class: "today_channel_guide_recommend_img",
                src: KTW.utils.util.getImageUrl(recommendData.imgUrl, KTW.CONSTANT.POSTER_TYPE.VERTICAL, 0, 0),
                css: {
                    position: "absolute"
                }
            },
            parent: imgBox ? imgBox : recommendImgArea
        }).error(function () {
            $(this).attr("src", KTW.COMMON_IMAGE.BANNER_DEFAULT + "default_recommend_mini_w210.png");
        });

        if (options.index < 4 || options.dataLength <= 5) {
            var recommendTagArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_recommend_tag_area",
                    css: {
                        position: "absolute", overflow: "visible"
                    }
                },
                parent: recommendImgArea
            });

            var imgList = util.checkPosterResolutionIcon({
                "hdr" : recommendData.isHdrYn,
                "resolCd" : recommendData.resolCd,
                "isDvdYn" : recommendData.isDvdYn
            });

            for (var j = 0; j < imgList.length; j++) {
                var tagSrc = "";

                if (imgList[j] === "HDR") {
                    tagSrc = "images/icon/tag_poster_hdr.png";
                }
                else if (imgList[j] === "UHD") {
                    tagSrc = "images/icon/tag_poster_uhd.png";
                }
                else if (imgList[j] === "DVD") {
                    tagSrc = "images/icon/tag_poster_mine.png";
                }

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_recommend_tag",
                        src: tagSrc,
                        css: {
                            position: "absolute", "margin-top": (j * 35) - 1, width: 70, height: 35
                        }
                    },
                    parent: recommendTagArea
                });
            }

            if (util.isLimitAge(recommendData.prInfo)) {
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_recommend_lock",
                        src: "images/img_vod_locked.png",
                        css: {
                            position: "absolute", left: 0, top: 0
                        }
                    },
                    parent: recommendImgArea
                });
            }

            var recommendInfoArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_recommend_info_area",
                    css: {
                        position: "absolute", height: 100, overflow: "visible"
                    }
                },
                parent: recommend
            });
            var titleArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_recommend_info_title_area",
                    css: {
                        position: "absolute", width: "100%", height: 40, overflow: "hidden"
                    }
                },
                parent: recommendInfoArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "today_channel_guide_recommend_name",
                    css: {
                        position: "absolute", left: 0, top: 0, height: 40, "font-size": 30, color: "rgb(255, 255, 255)",
                        "letter-spacing": -1.5, "white-space": "nowrap", "text-overflow": "ellipsis", overflow: "hidden"
                    }
                },
                text: recommendData.itemName,
                parent: titleArea
            });

            var subInfoArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_recommend_sub_info_area",
                    css: {
                        position: "absolute", width: "100%", height: 40, overflow: "visible"
                    }
                },
                parent: recommendInfoArea
            });
            var star = recommendData.mark;
            var num = parseInt(star);
            var num2 = star * 10 % 10;
            var num3 = parseInt(num2 / 2) + num2 % 2;
            var markSrc = null;
            for (var j = 0; j < 5; j++) {
                if (num >= j + 1) {
                    markSrc = "./images/starpoint/img_rating_filled_20.png";
                }
                else if (num < j + 1 && star > j) {
                    if (num2 === 9) {
                        markSrc = "./images/starpoint/img_rating_filled_20.png";
                    }
                    else {
                        markSrc = "./images/starpoint/img_rating_half_20_" + num3 + ".png";
                    }
                }
                else {
                    markSrc = "./images/starpoint/img_rating_empty_20.png";
                }


                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_recommend_mark",
                        src: markSrc,
                        css: {
                            position: "relative", float: "left", width: 20, height: 18, "margin-right": 4
                        }
                    },
                    parent: subInfoArea
                });
            }

            if (recommendData.wonYn === "Y" || recommendData.wonYn === "y") {
                // 유료 아이콘
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_recommend_price_icon",
                        src: "images/icon/icon_buy.png",
                        css: {
                            position: "relative", float: "left", "margin-top": -7, "margin-right": 4
                        }
                    },
                    parent: subInfoArea
                });
            }
            else if (recommendData.wonYn === "N" || recommendData.wonYn === "n") {
                // 무료 텍스트
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "today_channel_guide_recommend_price_info",
                        css: {
                            position: "relative", float: "left", "margin-right": 4, "font-size": 23, "letter-spacing": "-0.05em",
                            "font-family": "RixHead M", color: "rgb(255, 255, 255)"
                        }
                    },
                    text: "무료",
                    parent: subInfoArea
                });
            }
            else {
                // 암것도 안보여줌
            }

            var prInfo = util.transPrInfo(recommendData.prInfo);

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "today_channel_guide_recommend_rating_icon",
                    src: "images/icon/icon_age_list_" + prInfo + ".png",
                    css: {
                        position: "relative", float: "left", "margin-top": -7
                    }
                },
                parent: subInfoArea
            });
        }
        else {
            var recommendBlurArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_recommend_blur_area",
                    css: {
                        position: "absolute", width: 276, height: 207, overflow: "visible" /*"-webkit-transition": "-webkit-transform 0.5s"*/
                    }
                },
                parent: recommend
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "today_channel_guide_recommend_blur_title",
                    css: {
                        position: "absolute", "margin-left": 19, "font-family": "RixHead M", "font-size": 26, "letter-spacing": -1.3
        }
                },
                text: "전체보기",
                parent: recommendBlurArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "today_channel_guide_recommend_blur_num",
                    css: {
                        position: "absolute", "margin-left": 19, "font-size": 45, "letter-spacing": -1.35
                    }
                },
                text: options.dataLength,
                parent: recommendBlurArea
            });
        }


        return recommend;
    }

    function createRecommendArea (options) {
        log.printDbg("createRecommendArea()");

        var recommendArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_child_area today_channel_guide_recommend_area",
                index: options.index,
                css: {
                    overflow: "visible"
                }
            },
            parent: subHomeArea
        });
        util.makeElement({
            tag: "<div />",
            attrs: {
                class: "today_channel_guide_recommend_title",
                css: {
                    position: "absolute", "margin-left": 0, "margin-top": -60, width: 3, height: 24
                }
            },
            parent: recommendArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "today_channel_guide_recommend_title",
                css: {
                    position: "absolute", "margin-left": 15, "margin-top": -60, width: 1100, height: 40, "font-size": 27,
                    "letter-spacing": -1.35, "font-family": "RixHead B", "white-space": "nowrap"
                }
            },
            text: options.data[0].recomTitle,
            parent: recommendArea
        });
        var recommendContainer = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", "margin-left": 0, "margin-top": 0, overflow: "visible"
                }
            },
            parent: recommendArea
        });

        var maxNum = 5;
        maxNum = maxNum > options.data.length ? options.data.length : maxNum;

        for (var i = 0; i < maxNum; i++) {
            var recommend = createRecommend({
                data: options.data[i],
                index: i,
                dataLength: options.data.length
            });
            recommendContainer.append(recommend);
        }
    }

    function createChannelBannerArea () {
        log.printDbg("createChannelBannerArea()");

        if (!channelBannerArea) {
            // channel banner area (실시간 인기채널, 4채널 동시시청) - 고정된 데이터이기 때문에 여기서 전부 생성
            channelBannerArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "today_channel_guide_child_area today_channel_guide_channel_banner_area",
                    css: {
                        overflow: "visible"
                    }
                },
                parent: subHomeArea
            });
            for (var i = 0; i < 2; i++) {
                var channelBanner = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "today_channel_guide_channel_banner",
                        index: i,
                        css: {
                            position: "relative",
                            float: "left",
                            width: 568,
                            height: 280,
                            overflow: "visible"
                        }
                    },
                    parent: channelBannerArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_channel_banner_sdw",
                        src: "images/minibanner_focus_w580_shadow_l.png",
                        css: {
                            position: "absolute", "margin-left": -58, "margin-top": -51
                        }
                    },
                    parent: channelBanner
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_channel_banner_sdw",
                        src: "images/minibanner_focus_w580_shadow_m.png",
                        css: {
                            position: "absolute", "margin-left": 29, "margin-top": -51, width: 494, height: 382
                        }
                    },
                    parent: channelBanner
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_channel_banner_sdw",
                        src: "images/minibanner_focus_w580_shadow_r.png",
                        css: {
                            position: "absolute", "margin-left": 523, "margin-top": -51
                        }
                    },
                    parent: channelBanner
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "today_channel_guide_channel_banner_focus_bar",
                        css: {
                            position: "absolute", "margin-left": -21, "margin-top": -14, width: 594, height: 308,
                            "background-color": "rgba(0, 0, 0, 1)", border: "solid red 6px", "box-sizing": "border-box"
                        }
                    },
                    parent: channelBanner
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "today_channel_guide_channel_banner_img",
                        src: i === 0 ? "images/banner_4ch_w555.png" : "images/banner_popul_w555.png",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: channelBanner
                });
            }
        }

        return channelBannerArea;
    }

    function _initData () {
        log.printDbg("_initData()");

        isScrollFocus = false;
        focusAreaIdx = 0;
        focusItemIdx = 0;

        data = [];

        var subHomeData = KTW.managers.data.SubHomeDataManager.getSubHome("VC", menu.id);

        // test
        // 추천 데이터
        //var movieSubHomeData = KTW.managers.data.SubHomeDataManager.getSubHome("6907", "10000000000000197010");
        //subHomeData.recommend = movieSubHomeData.recommend;

        //var musicSubHomeData = KTW.managers.data.SubHomeDataManager.getSubHome("6909", "10000000000000197014");
        //subHomeData.event = musicSubHomeData.event;
        //
        //subHomeData.event[0].templateType = "B2";
        //subHomeData.event[0].templateLoc = "1";
        //subHomeData.event[0].imgUrl = "./images/sample/banner_sample_w659_2.png";
        //
        //subHomeData.event[1].templateType = "B2";
        //subHomeData.event[1].templateLoc = "2";
        //subHomeData.event[1].imgUrl = "./images/sample/banner_sample_w436_2.png";
        //
        //
        //subHomeData.event[0].templateType = "B1";
        //subHomeData.event[0].imgUrl = "./images/sample/banner_sample_w1121.png";
        //


        if (subHomeData.event && subHomeData.event.length > 0) {
            data.push({
                type: DATA_TYPE.BANNER,
                data: subHomeData.event
            });
        }

        if (subHomeData.recommend && subHomeData.recommend.length > 0) {
            var tmpRecommend = [];
            for (var i = 0; i < subHomeData.recommend.length; i++) {
                var rec = subHomeData.recommend[i];

                if (!tmpRecommend[rec.position]) {
                    tmpRecommend[rec.position] = [];
                }

                tmpRecommend[rec.position].push(rec);
            }

            for (var key in tmpRecommend) {
                data.push({
                    type: DATA_TYPE.RECOMMEND,
                    data: tmpRecommend[key]
                });
            }
        }

        data.push({ type: DATA_TYPE.CHANNEL_BANNER, data: [{}, {}] });

        var page = 0;
        var tmpHeight = 122;
        for (var i = 0; i < data.length; i++) {
            tmpHeight += getFocusHeightByType(data[i].type);

            if (tmpHeight >= KTW.CONSTANT.RESOLUTION.HEIGHT) {
                ++page;
                tmpHeight = 122 + getFocusHeightByType(data[i].type);
            }

            data[i].page = page;
        }

        // test
        //window.testData = data;
    }

    function initDraw () {
        log.printDbg("initDraw()");

        subHomeArea.children().remove();

        for (var i = 0; i < data.length; i++) {
            var container = null;

            if (data[i].type === DATA_TYPE.BANNER) {
                container = createBannerArea({
                    data: data[i].data,
                    index: i
                });
            }
            else if (data[i].type === DATA_TYPE.RECOMMEND) {
                container = createRecommendArea({
                    data: data[i].data,
                    index: i
                });
            }
            else {
                // 하위 고정 배너들...
                container = createChannelBannerArea();
            }

            if (container) {
                subHomeArea.append(container);
            }
        }

        scroll.show({
            maxPage: getMaxPage(),
            curPage: 0
        });
    }

    function drawRelatedArea () {
        log.printDbg("drawRelatedArea()");

        var focusArea = subHomeArea.children().eq(focusAreaIdx);

        var relatedArea = div.find(".today_channel_guide_channel_related_area");

        if (div.hasClass("focus") && focusArea.hasClass("today_channel_guide_recommend_area") && focusArea.hasClass("focus")) {
            var isCheck = true;

            if (focusItemIdx === 4 && data[focusAreaIdx].data.length > 5) {
                isCheck = false;
            }

            if (isCheck) {
                if (arrIsMyContents[focusAreaIdx + "_" + focusItemIdx]) {
                    relatedArea.find("span").text("찜해제");
                }
                else {
                    relatedArea.find("span").text("찜하기");
                }

                relatedArea.css({visibility: "inherit"});
            }
            else {
                relatedArea.css({visibility: "hidden"});
            }
        }
        else {
            relatedArea.css({visibility: "hidden"});
        }
    }

    function getMaxFocusArea () {
        return subHomeArea.children().length;
    }

    function getMaxItemLength () {
        var focusArea = subHomeArea.children().eq(focusAreaIdx);
        if (focusArea.hasClass("today_channel_guide_recommend_area")) {
            return focusArea.find("div .today_channel_guide_recommend").length;
        }
        else {
            return focusArea.children().length;
        }
    }

    function getFocusHeightByType (type) {
        switch (type) {
            case DATA_TYPE.BANNER:
                return FOCUS_HEIGHT.BANNER;
            case DATA_TYPE.RECOMMEND:
                return FOCUS_HEIGHT.RECOMMEND;
            case DATA_TYPE.CHANNEL_BANNER:
                return FOCUS_HEIGHT.CHANNEL_BANNER;
        }
    }

    function getMaxPage () {
        return data[data.length - 1].page + 1;
    }

    function setFocus () {
        log.printDbg("setFocus()");

        var arrArea = subHomeArea.children();
        var focusArea = arrArea.eq(focusAreaIdx);
        if (!focusArea.hasClass("focus")) {
            arrArea.removeClass("focus");
            focusArea.addClass("focus");
        }

        var arrItem = null;
        if (focusArea.hasClass("today_channel_guide_recommend_area")) {
            arrItem = focusArea.find("div .today_channel_guide_recommend");
        }
        else {
            arrItem = focusArea.children();
        }
        var focusItem = arrItem.eq(focusItemIdx);
        if (!focusItem.hasClass("focus")) {
            arrItem.removeClass("focus");
            focusItem.addClass("focus");
        }

        //
        var targetTop = 0;

        for (var i = 0; i < data.length; i++) {
            if (data[i].page === data[focusAreaIdx].page) {
                break;
            }
            else {
                targetTop += getFocusHeightByType(data[i].type);
            }
        }

        subHomeArea.css({"-webkit-transform": "translateY(-" + targetTop + "px)"});


        setTitleAnimation(focusArea);
        drawRelatedArea();
    }

    function setTitleAnimation (focusArea) {
        util.clearAnimation(div.find(".today_channel_guide_recommend_area .today_channel_guide_recommend .today_channel_guide_recommend_info_area .today_channel_guide_recommend_info_title_area span"));

        if (div.hasClass("focus")) {
            var targetBox = focusArea.find(".today_channel_guide_recommend.focus .today_channel_guide_recommend_info_area .today_channel_guide_recommend_info_title_area");

            if (targetBox && targetBox.length > 0) {
                util.startTextAnimation({
                    targetBox: targetBox
                });
            }
        }
    }

    function moveItemFocus (nextFocusAreaIdx) {
        log.printDbg("moveItemFocus(" + nextFocusAreaIdx + ")");

        if (focusItemIdx === nextFocusAreaIdx) {
            return;
        }

        focusItemIdx = nextFocusAreaIdx;

        setFocus();
    }

    function moveAreaFocus (options) {
        log.printDbg("moveAreaFocus(" + log.stringify(options) + ")");

        if (focusAreaIdx === options.nextFocusAreaIdx) {
            return;
        }

        focusAreaIdx = options.nextFocusAreaIdx;
        if (options.nextFocusItemIdx) {
            focusItemIdx = options.nextFocusItemIdx;
        }
        else {
            focusItemIdx = 0;
        }

        setFocus();
    }

    function movePage (options) {
        log.printDbg("movePage(" + log.stringify(options) + ")");

        var nextFocusItemIdx = 0;

        if (options.bFocusLastItem) {
            for (var i = data.length - 1; i >= 0; i--) {
                if (data[i].page === options.nextPage) {

                    nextFocusItemIdx = data[i].data.length - 1;

                    if (i === focusAreaIdx) {
                        moveItemFocus(nextFocusItemIdx);
                    }
                    else {
                        moveAreaFocus({
                            nextFocusAreaIdx: i,
                            nextFocusItemIdx: nextFocusItemIdx
                        });
                    }
                    break;
                }
            }
        }
        else {
            for (var i = 0; i < data.length; i++) {
                if (data[i].page === options.nextPage) {

                    if (i === focusAreaIdx) {
                        moveItemFocus(nextFocusItemIdx);
                    }
                    else {
                        moveAreaFocus({
                            nextFocusAreaIdx: i,
                            nextFocusItemIdx: nextFocusItemIdx
                        });
                    }
                    break;
                }
            }
        }
    }

    function keyUp () {
        log.printDbg("keyUp()");

        var nextFocusAreaIdx = focusAreaIdx - 1;

        if (nextFocusAreaIdx < 0) {
            nextFocusAreaIdx = getMaxFocusArea() - 1;
        }

        moveAreaFocus({
            nextFocusAreaIdx: nextFocusAreaIdx
        });
    }

    function keyDown () {
        log.printDbg("keyDown()");

        var nextFocusAreaIdx = focusAreaIdx + 1;

        if (nextFocusAreaIdx > getMaxFocusArea() -1) {
            nextFocusAreaIdx = 0;
        }

        moveAreaFocus({
            nextFocusAreaIdx: nextFocusAreaIdx
        });
    }

    function keyLeft () {
        log.printDbg("keyLeft()");

        if (focusItemIdx > 0) {
            var nextFocusItemIdx = focusItemIdx - 1;
            moveItemFocus(nextFocusItemIdx);

            return true;
        }
    }

    function keyRight () {
        log.printDbg("keyRight()");

        var nextFocusItemIdx = focusItemIdx + 1;

        if (nextFocusItemIdx < getMaxItemLength()) {
            moveItemFocus(nextFocusItemIdx);
        }
        else {
            keyDown();
        }

        return true;
    }

    function keyOk () {
        log.printDbg("keyOk()");

        if (data[focusAreaIdx].type === DATA_TYPE.CHANNEL_BANNER) {
            if (focusItemIdx === 0) {
                var locator = KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();

                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.SUBHOME,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                    catId: menu.id,
                    contsId: "",
                    locator: locator,
                    reqPathCd: ""
                });
            }
            else {
                //KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();

                parent.jumpPopularMenu();
            }
        }
        else {
            jumpBySubHomeData(data[focusAreaIdx].data[focusItemIdx]);
        }
    }

    function keyRed () {
        log.printDbg("keyRed()");

        var targetFocusAreaIdx = focusAreaIdx;
        var targetPage = data[focusAreaIdx].page - 1;

        for (var i = focusAreaIdx - 1; i >= 0; i--) {
            if (data[i].page !== data[focusAreaIdx].page) {
                break;
            }
            else {
                targetFocusAreaIdx = i;
            }
        }

        if (targetFocusAreaIdx === focusAreaIdx) {
            if (focusItemIdx > 0) {
                targetPage = data[focusAreaIdx].page
            }
        }
        else {
            targetPage = data[focusAreaIdx].page
        }

        if (targetPage < 0) {
            targetPage = getMaxPage() - 1;
        }

        movePage({
            nextPage: targetPage
        });
    }

    function keyBlue () {
        log.printDbg("keyBlue()");

        var targetFocusAreaIdx = focusAreaIdx;
        var targetPage = data[focusAreaIdx].page + 1;

        for (var i = focusAreaIdx + 1; i < data.length; i++) {
            if (data[i].page !== data[focusAreaIdx].page) {
                break;
            }
            else {
                targetFocusAreaIdx = i;
            }
        }

        if (targetFocusAreaIdx === focusAreaIdx) {
            if (focusItemIdx < data[focusAreaIdx].data.length - 1) {
                targetPage = data[focusAreaIdx].page
            }
        }
        else {
            targetPage = data[focusAreaIdx].page
        }

        if (targetPage > getMaxPage() - 1) {
            targetPage = 0;
        }

        movePage({
            nextPage: targetPage,
            bFocusLastItem: true
        });
    }

    function keyContext () {
        log.printDbg("keyContext()");

        var consumed = false;

        var relatedArea = div.find(".today_channel_guide_channel_related_area");

        if (relatedArea.css("visibility") === "visible") {
            setPlayListNxt();
            consumed = true;
        }

        return consumed;
    }

    function jumpBySubHomeData (data) {
        log.printDbg("jumpBySubHomeData(" + log.stringify(data) + ")");

        if (data && data.itemType) {
            var params = {
                itemType: data.itemType,
                req_cd: data.req_cd ? data.req_cd : 67
            };

            var jumpType = KTW.data.EntryLog.JUMP.TYPE.VOD_DETAIL;

            switch (data.itemType) {
                case KTW.DATA.MENU.ITEM.CATEGORY:
                case KTW.DATA.MENU.ITEM.SERIES:
                    params.cat_id = data.itemId;
                    jumpType = KTW.data.EntryLog.JUMP.TYPE.CATEGORY;
                    break;
                case KTW.DATA.MENU.ITEM.CONTENT:
                    params.const_id = data.itemId;
                    jumpType = KTW.data.EntryLog.JUMP.TYPE.VOD_DETAIL;
                    break;
                case KTW.DATA.MENU.ITEM.INT_M:
                case KTW.DATA.MENU.ITEM.INT_U:
                case KTW.DATA.MENU.ITEM.INT_W:
                    params.locator = data.locator;
                    params.parameter = data.parameter;
                    jumpType = KTW.data.EntryLog.JUMP.TYPE.DATA;
                    break;
            }

            KTW.managers.service.MenuServiceManager.jumpByMenuData(params);

            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.JUMP_TO,
                act: KTW.data.EntryLog.JUMP.CODE.SUBHOME,
                jumpType: jumpType,
                catId: menu.id,
                contsId:  data[focusItemIdx].itemId,
                locator: "",
                reqPathCd: ""
            });
        }
    }

    /**
     * 추천 컨텐츠의 찜 정보 획득
     */
    function initCheckMyContents () {
        log.printDbg("initCheckMyContents()");

        arrIsMyContents = [];
        var tmpArray = [];
        var contsIdList = "";

        for (var i = 0; i < data.length; i++) {
            if (data[i].type === DATA_TYPE.RECOMMEND) {
                for (var j = 0; j < data[i].data.length; j++) {
                    tmpArray.push(i + "_" + j);
                    contsIdList += data[i].data[j].itemId + "|";
                }
            }
        }

        if (contsIdList.length > 0) {
            contsIdList = contsIdList.substring(0, contsIdList.length - 1);

            KTW.managers.http.amocManager.checkMyContentW3(contsIdList, menu.id, "H", "01", "A", "N", function (success, data) {
                if (data && data.playCheckList) {
                    var arrResult = data.playCheckList.split("|");

                    for (var i = 0; i < arrResult.length; i++) {
                        if (arrResult[i] === "Y") {
                            arrIsMyContents[tmpArray[i]] = true;
                        }
                        else {
                            arrIsMyContents[tmpArray[i]] = false;
                        }
                    }

                    drawRelatedArea();
                }
            });
        }
        else {
            arrIsMyContents = [];
        }
    }

    /**
     * 찜하기 / 찜해제
     *
     * API 호출만 하고 결과는 신경쓰지 않는다.
     */
    function setPlayListNxt () {
        var itemType = data[focusAreaIdx].data[focusItemIdx].itemType;
        var itemId = data[focusAreaIdx].data[focusItemIdx].itemId;
        var flag = "1";
        var categoryId = null;
        var hdYn = "";

        arrIsMyContents[focusAreaIdx + "_" + focusItemIdx] = !arrIsMyContents[focusAreaIdx + "_" + focusItemIdx];

        if (arrIsMyContents[focusAreaIdx + "_" + focusItemIdx]) {
            categoryId = data[focusAreaIdx].data[focusItemIdx].catId;
        }
        else {
            flag = "2";
        }

        switch (data[focusAreaIdx].data[focusItemIdx].resolCd) {
            case "HD":
                hdYn = "Y";
                break;
            case "SD":
                hdYn = "N";
                break;
            case "FHD":
                hdYn = "F";
                break;
            case "UHD":
                hdYn = "U";
                break;
        }

        // 2017.05.30 dhlee
        // 찜하기 할 때 오늘의채널가이드 cat_id를 넣으면 찜목록이나 상세 등에서 이상 동작하게 된다.
        // 실제 찜하기 할 때는 해당 컨텐츠가 편성된 시제 cat_id를 넣어야 하는데 그 정보를 알기 위해서는 추가적인 API를 호출해야 한다.
        // 따라서 여기서는 cat_id를 지정하지 않고 찜하도록 하고, 찜목록 쪽에서 관련 처리를 하도록 한다. from 김승열 연구원
        //KTW.managers.http.amocManager.setPlayListNxt(1, itemType, itemId, categoryId, flag, hdYn);
        KTW.ui.LayerManager.startLoading({preventKey:true});

        KTW.managers.http.amocManager.setPlayListNxt(1, itemType, itemId, null, flag, hdYn , _callbackSetPlayListNxt);
    }

    function _callbackSetPlayListNxt(success , data) {
        KTW.ui.LayerManager.stopLoading();

        if (success) {
            drawRelatedArea();
            if (arrIsMyContents[focusAreaIdx + "_" + focusItemIdx]) {
                // 2017.09.04 dhlee 우리집 맞춤 TV > 우리집 TV로 메뉴명 변경 (from KT)
                var my_menu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (!menu.parent && menu.catType === KTW.DATA.MENU.CAT.SUBHOME) {
                            return true;
                        }
                    }
                })[0];
                KTW.managers.service.SimpleMessageManager.showMessageTextOnly("VOD를 찜 하였습니다. " + my_menu.name + ">찜한 목록에서 확인할 수 있습니다");
            }
            else {
                KTW.managers.service.SimpleMessageManager.showMessageTextOnly("VOD 찜을 해제하였습니다");
            }
            // 2017.07.06 dhlee
            // 우리집맞춤TV 에서 제공되는 찜목록 캐시를 위하여 찜/찜해제 등의 동작을 하는 경우
            // 지니프릭스 가이드 내용대로 flag set을 위한 API 호출을 추가한다.
            var fhModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_FAMILY_HOME);
            if (fhModule) {
                fhModule.execute({
                    method: "setWishListFlag"
                });
            }
        }
        else {
            if(data === "timeout") {
                KTW.utils.util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP , undefined , KTW.ERROR.CODE.E006.split("\n"), null, null, true);
            }
        }
    }


    KTW.ui.view.TodayChannelGuideView = function(options) {

        parent = options.parent;
        menu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === options.menuId) {
                    return true;
                }
            }
        })[0];

        this.create = function () {
            log.printDbg("create()");

            _this = this;
            createElement();
        };

        this.getDiv = function () {
            return div;
        };

        this.initData = function () {
            _initData();
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3);
            
            if (!options || !options.resume) {
                initDraw();

                div.css({visibility: "inherit"});
            }
            else {
                // 2017.07.06 dhlee
                // WEBIIIHOME-2628 이슈 수정 (resume 될 때 다시 animation 되도록 setFocus() 호출
                setFocus();
            }
            parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});

                this.blur();

                defaultHomeShowIdx++;
            }

        };

        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");

            initCheckMyContents();

            scroll.setCurrentPage(0);
            subHomeArea.css({"-webkit-transition": "-webkit-transform 0.5s"});
            setFocus();


            if (getMaxFocusArea() > 2) {
                keyGuide.css({visibility: ""});
            }
            else {
                keyGuide.css({visibility: "hidden"});
            }
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.removeClass("focus");

            isScrollFocus = false;
            focusAreaIdx = 0;
            focusItemIdx = 0;
            subHomeArea.css({"-webkit-transition": "", "-webkit-transform": "translateY(0px)"});

            drawRelatedArea();

            util.clearAnimation(div.find(".today_channel_guide_recommend_info_title_area span"));

            subHomeArea.children().first().css({"-webkit-transform": ""});
        };

        this.remove = function () {};

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft();
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    if (!isScrollFocus) {
                        keyContext();
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                //case KTW.KEY_CODE.CONTEXT:
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;
        };

        this.isFocusAvailable = function () {
            return true;
        };

        this.getDataStatus = function () {
            return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
        };
    };
})();