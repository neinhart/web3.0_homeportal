/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>AudioChannelView</code>
 * @author sw.nam
 * @since 2016-12-08
 */

(function() {

    var FIXED_FOCUSING_AREA = 1;
    var IMAGE_PATH = KTW.CONSTANT.IMAGE_PATH;

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var epgUtil = KTW.utils.epgUtil;

    var CHANNELS_PER_PREVIEW = 7;

    /*define <div> class*/
    var div = null;

    var itemGroup = null;
    var item = [0];
    var channelArea = [0];

    var channel = [];

    var isFocusAudioArea = true; // the flag for scrolling or not


    KTW.ui.view.AudioChannelView = function (options) {
        KTW.ui.view.EpgView.call(this, options);

        var parent = options.parent;

        var _this = this;
        function createElement () {

            log.printDbg("createElement()");

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "audio_channel_view",
                    css: {
                        visibility: "hidden"
                    }
                },
                parent: parent.getEpgContainerDiv()
            });
            

            itemGroup = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "ac_itemGroup",
                    css: {
                        position: "absolute"
                    }
                },
                parent: div
            });

            for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
                item[i] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "ac_item"
                    },
                    parent: itemGroup
                });

                if (KTW.CONSTANT.IS_OTS === true) {
                    channelArea[i] = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "ac_ots_channelArea",
                            css: {
                                position: "absolute",  float: "left"
                            }
                        },
                        parent: item[i]
                    });
                }
                else {
                    channelArea[i] = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            class: "ac_otv_channelArea",
                            css: {
                                position: "absolute",  float: "left"
                            }
                        },
                        parent: item[i]
                    });
                }
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: IMAGE_PATH + "audio_full_epg/img_audio_guide_2_sm.png",
                        class: "ac_cdImg",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: channelArea[i]
                });

            };


            if (KTW.CONSTANT.IS_OTS === true) {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "ac_chNum",
                        css: {
                            position: "absolute"
                        }
                    },
                    text: "채널 번호",
                    parent: $(".ac_ots_channelArea")
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "ac_chName",
                        css: {
                            position: "absolute"
                        }
                    },
                    text: "채널 이름",
                    parent: $(".ac_ots_channelArea")
                });




                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "ac_ots_programArea",
                        css : {
                            position: "absolute"
                        }
                    },
                    parent: $(".ac_item")
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "ac_programName",
                        css: {
                            position: "absolute"
                        }
                    },
                    text: "프로그램 이름",
                    parent: $(".ac_ots_programArea")
                });


                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "ac_ageIcon",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: $(".ac_ots_programArea")
                });



            }
            else {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "ac_chNum",
                        css: {
                            position: "absolute"
                        }
                    },
                    text: "채널 번호",
                    parent: $(".ac_otv_channelArea")
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "ac_chName",
                        css: {
                            position: "absolute"
                        }
                    },
                    text: "채널 이름",
                    parent: $(".ac_otv_channelArea")
                });



                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "ac_otv_programArea",
                        css : {
                            position: "absolute"
                        }
                    },
                    parent: $(".ac_item")
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "ac_programName",
                        css: {
                            position: "absolute"
                        }
                    },
                    text: "프로그램 이름",
                    parent: $(".ac_otv_programArea")
                });


                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "ac_ageIcon",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: $(".ac_otv_programArea")
                });
            }
            
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/dim_epg_btm.png",
                    class: "audio_channel_view_preview_dim_btm",
                    css: {
                        position: "absolute"
                    }
                },
                parent: itemGroup
            });

        }


        function reviseProgrammeList (programmesList) {
            log.printDbg("reviseProgrammeList()");

            if (programmesList) {
                var length = programmesList.length;

                if (length < 3) {
                    programmesList.unshift(null);
                }
            }

            return programmesList;
        }

        function drawChannelArea () {
            log.printDbg("drawChannelArea()");

            var curChannelList = _this.getChannelList();
            var channel = null;
            var thisItemElements =  null;

            for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
                thisItemElements = item[i];
                channel = curChannelList[i];
                if (channel) {
                    var chName = channel.data.name;
                    var chNum = channel.data.majorChannel;
                    if (KTW.CONSTANT.IS_OTS === true) {
                        thisItemElements.find(".ac_chNum").text(util.numToStr(chNum,3,null));

                        thisItemElements.find(".ac_cdImg").attr("src", IMAGE_PATH + "audio_full_epg/img_audio_guide_2_sm.png");

                        thisItemElements.find(".ac_chName").text("");
                        thisItemElements.find(".ac_chName").removeClass("line_1");
                    }
                    else {
                        thisItemElements.find(".ac_cdImg").attr("src", IMAGE_PATH + "audio_full_epg/img_audio_guide_2_sm.png");
                        thisItemElements.find(".ac_chName").text(chName);
                        thisItemElements.find(".ac_chNum").text(util.numToStr(chNum,3,null));
                    }

                    thisItemElements.find(".ac_cdImg").css({visibility: "inherit"});
                }
                else {
                    thisItemElements.find(".ac_cdImg").css({visibility: "hidden"});
                    thisItemElements.find(".ac_chName").text("");
                    thisItemElements.find(".ac_chNum").text("");
                }
            }
        }

        function drawProgramArea () {
            log.printDbg("drawProgramArea()");

            var programName = null;
            var singer = null;

            //2017 07.21 sw.nam
            // 이전에 포커스 되었던 프로그램이 2줄 이었는데 다음에 포커스 된 프로그램 정보가 없는경우 혹은 "업데이트 중입니다" 인 경우 이전 2줄영역의 text 잔상이 남게되어
            // 프로그램 영역 drawing 될 때마다 2줄 텍스트 영역 초기화하도록 추가
            for ( var i = 0; i < CHANNELS_PER_PREVIEW; i++ ) {
                item[i].find(".ac_ots_programArea").find(".ac_programName").css({width:1079});

                if (KTW.CONSTANT.IS_OTS === true) {
                    /**
                     * OTS 편성표
                     */
                    var curChannelList = _this.getChannelList();
                    channel = curChannelList[i];

                    if (channel) {
                        item[i].find(".ac_ots_programArea").find(".ac_programName").text(channel.data.name);
                    }
                    else {
                        item[i].find(".ac_ots_programArea").find(".ac_programName").text("");
                    }
                }
                else {
                    /**
                     * OTV 편성표
                     */

                    var curProgramList = reviseProgrammeList(_this.getPreviewProgrammesList());
                    var entireProgramList = reviseProgrammeList(_this.getProgrammesList());

                    var thisItemElements = null;
                    var thisProgram = null;
                    var nextProgram = null;
                    var thisChannelProgramList = null;

                    thisItemElements = item[i];
                    thisProgram = curProgramList[i];
                    // 채널리스트 받아오는데 시간이 필요하므로 그에대한 예외처리가 필요하다.
                    if (thisProgram) {
                        programName = thisProgram.name;
                        singer = epgUtil.getSinger(thisProgram);
                        thisChannelProgramList = entireProgramList[i];
                        var totalProgramName = "";
                        if (!singer) {
                            totalProgramName = programName;
                        }
                        else {
                            totalProgramName = programName+" / "+singer;
                        }
                        log.printDbg("drawProgramArea() OTV i : [" + i + "] totalProgramName : " + totalProgramName);
                        item[i].find(".ac_otv_programArea").find(".ac_programName").text(totalProgramName);

                    }
                    else {
                        thisItemElements.find(".ac_otv_programArea span").text("");
                        thisItemElements.find(".ac_otv_programArea img").css("visibility" , "inherit");

                    }
                }
            }
        }

        function setFocusChannelTune () {
            log.printDbg("setFocusChannelTune()");

            var channelList = _this.getChannelList();
            var channelIdx = _this.getChannelIdx();
            var focusChannel = channelList[channelIdx];

            if (KTW.CONSTANT.IS_OTS) {
                //var mainControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                _this.parent.mainChannelTune({channel: focusChannel, onlySelect: true});
            }
            else {
                _this.parent.mainChannelTune({channel: focusChannel, onlySelect: false});
            }

        }

        function isChannelListExist () {
            log.printDbg("isChannelListExist()");
            var channelList = _this.getChannelList();
            return channelList && channelList.length > 0 ? true : false;
        }

        function keyLeft () {
            log.printDbg("keyLeft()");

            var consumed = false;
            // if (isChannelListExist()) {
            //     var channelMaxPage = _this.getChannelMaxPage();
            //
            //     if (isFocusAudioArea && channelMaxPage > 1) {
            //
            //         isFocusAudioArea = false;
            //         setFocusArea();
            //         consumed = true;
            //     }
            // }

            return consumed;
        }

        function keyRight () {

            log.printDbg("keyRight()");

            if (isChannelListExist()) {
                if (!isFocusAudioArea) {
                    isFocusAudioArea = true;
                }
                else {

                }
            }
        }

        function keyUp () {
            log.printDbg("keyUp()");
        }

        function keyDown () {
            log.printDbg("keyDown()");
        }

        function keyOk () {
            log.printDbg("keyOk()");

        }

        function checkProgrammeList (_this) {
            log.printDbg("checkProgrammeList()");

            var isExist = false;
            var programmesList = reviseProgrammeList(_this.getProgrammesList());

            if (programmesList && programmesList.length > 0) {
                for (var i = 0; i < programmesList.length; i++) {
                    var progList = programmesList[i];

                    if (progList && progList.length > 0 && progList[0].reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NORMAL) {
                        isExist = true;
                        break;
                    }
                }
            }

            return isExist;
        }

        function isAudioChannel (channel) {
            var isExist = false;

            if (channel && channel.data && channel.data.ccid) {
                var channelList = KTW.ui.adaptor.EpgDataAdaptor.getChannelList([KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_AUDIO]);

                if (channelList && channelList.length > 0) {
                    for (var i = 0; i < channelList.length; i++) {
                        if (channel.data.ccid === channelList[i].data.ccid) {
                            isExist = true;
                            break;
                        }
                    }
                }
            }

            return isExist;
        }


        this.create = function () {

            log.printDbg("create()");

            //_this = this;
            createElement();

        };

        this.getDiv = function () {
            return div;
        };

        this.initData = function () {
            // 채널 리스트, 프로그램 리스트, channelIdx 설정
            this.initEpgData();
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            if (!options || !options.resume) {
                this.parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2);

                log.printDbg("show()");
                div.css({visibility: "inherit"});


                if(isChannelListExist()) {
                    // preview 채널 번호, 이름 받아와서 set.
                    drawChannelArea(); // 채널영역
                    drawProgramArea(); // 프로그램영역
                }
            }
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});

                if (div.hasClass("focus")) {
                    this.blur();
                }

            }
            else {
            }

            // if (KTW.CONSTANT.IS_OTS) {
            //     this.returnToBackChannelOnOTS();
            // }
        };

        this.focus = function () {
            log.printDbg("focus()");

            /**
             * jjh1117 2017/10/17
             * 이 부분은 오디오편성표 핫키로 진입 시 이쪽으로 진입하게 됨.
             */
            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel()) === true) {
                log.printDbg("focus() STEP#1");
                KTW.ui.LayerManager.clearNormalLayer();
                var params = {
                    focus_channel : KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel()
                };

                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel());
                KTW.managers.service.IframeManager.changeIframe(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);

                KTW.ui.LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG,
                        type: KTW.ui.Layer.TYPE.NORMAL,
                        priority: KTW.ui.Layer.PRIORITY.NORMAL,
                        view : KTW.ui.view.AudioChannelFullEpgView,
                        linkage : false,
                        params: params
                    },
                    clear_normal: false,
                    visible: true,
                    cbActivate: function() {

                    }
                });
            }else {
                log.printDbg("focus() STEP#2");
                var chList = this.getChannelList();
                if(chList !== undefined && chList !== null && chList.length>0) {
                    var _this = this;
                    var params = {
                        focus_channel : chList[0].data,
                        is_jump : true,
                        parent_view : _this
                    };

                    KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG , chList[0].data);
                    KTW.managers.service.IframeManager.changeIframe(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);

                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG,
                            type: KTW.ui.Layer.TYPE.NORMAL,
                            priority: KTW.ui.Layer.PRIORITY.NORMAL,
                            view : KTW.ui.view.AudioChannelFullEpgView,
                            linkage : false,
                            params: params
                        },
                        clear_normal: false,
                        visible: true,
                        cbActivate: function() {

                        }
                    });
                }
            }
        };

        this.blur = function () {

            log.printDbg("blur()");

            // Hide focus line, division

            for( var i = 0; i < CHANNELS_PER_PREVIEW; i++ ) {
                if(i !== 1) {
                    item[i].removeClass("opacity");
                }
            }

            item[FIXED_FOCUSING_AREA].removeClass("focus");


            isFocusAudioArea = true;

            if(div.hasClass("focus"))
                div.removeClass("focus");

            if(div.hasClass("ots"))
                div.removeClass("ots");

            this.initEpgData();

            drawChannelArea();
            drawProgramArea();
            itemGroup.find(".ac_ageIcon").css("visibility" , "hidden");

            if (KTW.CONSTANT.IS_OTS) {
                this.returnToBackChannelOnOTS();
            }
        };

        this.remove = function () {};

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()..."+keyCode);

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft();
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk();
                    consumed = true;
                    break;

                case KTW.KEY_CODE.FULLEPG :
                case KTW.KEY_CODE.FULLEPG_OTS :
                    // 2017.05.15 dhlee
                    // 오디오채널 편성표가 실행된 상태에서 편성표 핫키를 누르면 historyback 하도록 함
                    // TODO 이게 맞는 로직인지 체크 필요(동작 자체는 맞지만 전체 시나리오 상에서 맞는지 확인되어야 함

                    /**
                     * [dj.son] [WEBIIIHOME-1013] 현재 채널이 오디오 채널이고 오디오 편성표 화면이 show 상태이면, 화면 내리도록 수정 (핫키 동작)
                     */
                    var curChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                    if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(curChannel)) {
                        KTW.ui.LayerManager.clearNormalLayer();
                        consumed = true;
                    }
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;
        };

        this.isFocusAvailable = function () {
            return isChannelListExist(this);
        };

        this.getDataStatus = function () {
            if (!isChannelListExist(this)) {
                if (KTW.CONSTANT.IS_OTS) {
                    return KTW.ui.view.EpgView.DATA_STATUS.WEAK_SIGNAL;
                }
                else {
                    var isSkipped = false;
                    var skippedChannelList = KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SKIPPED);

                    for (var i = skippedChannelList.length - 1; i >= 0; i++) {
                        if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(skippedChannelList[i])) {
                            isSkipped = true;
                            break;
                        }
                    }

                    if (isSkipped) {
                        return KTW.ui.view.EpgView.DATA_STATUS.SKIPPED;
                    }
                    else {
                        return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
                    }
                }
            }
            else {
                return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
            }
        };

        this.returnToBackChannelOnOTS = function () {
            log.printDbg("returnToBackChannelOnOTS");

            if (KTW.CONSTANT.IS_OTS) {
                var mainControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                var channel = mainControl.getCurrentChannel();

                if (isAudioChannel(channel)) {
                    channel = mainControl.getBackChannel();
                    this.parent.mainChannelTune({channel: channel, onlySelect: true});
                }
            }
        };

        this.getAudioChannelList = function() {
            var channelList = _this.getChannelList();
            return channelList && channelList.length > 0 ?  channelList : [];
        };
    };

    KTW.ui.view.AudioChannelView.prototype = new KTW.ui.view.EpgView();
    KTW.ui.view.AudioChannelView.prototype.constructor = KTW.ui.view.AudioChannelView;

})();