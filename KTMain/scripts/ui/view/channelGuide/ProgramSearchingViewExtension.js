/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>ProgramSearchingViewExtension</code>
 *
 * @author sw.nam
 * @since 2017-02-01
 *
 *
 * 프로그램 검색 View (for OTS STB only))
 *
 */

(function() {

    /* Constance Numbers */
    var NUMBER_OF_PREVIEW_ITEM =  6;
    var NUMBER_OF_FOCUSED_ITEM =  6;

    /* UtilityAPIs */
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var programSearchAdapter =  KTW.oipf.AdapterHandler.programSearchAdapter;
    var epgDataAdapter = KTW.ui.adaptor.EpgDataAdaptor;

    /* Global objects & values*/
    var ps_programList = null;
    var customizableList = null;
    var ps_programIndex = 0;


    /* element array & values*/
    var div = null;


    var itemGroup = null;
    var item = [0];

    var subArea = null;

    /* scroll */
    var scrollArea = null;
    var scroll = null;


    /* flag */
    var isCreate = false;
    var isProgramListExist = false;
    var isFocusedItemArea = true;
    var isSortedByTime = true;

    var focusOptionIndex = 0;

    KTW.ui.view.ProgramSearchingViewExtension = function(options) {
        KTW.ui.view.EpgView.call(this, options);


        var parent = options.parent;
        var menuId = options.menuId;
        var _this = this;

        var curMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === menuId) {
                    return true;
                }
            }
        })[0];

        function createElement() {

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "program_searching_extension_view",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                    }
                },
                parent: parent.getEpgContainerDiv()
            });

            itemGroup = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "ps_itemGroup",
                    css: {
                        position: "absolute", display: "none"
                    }
                },
                parent: div
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "divisionLine_TOP",
                    css: {
                        position: "absolute"
                    }
                },
                parent: itemGroup
            });
            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "divisionLine_BOTTOM",
                    css: {
                        position: "absolute"
                    }
                },
                parent: itemGroup
            });

            scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1829-576), top: 149, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 702,
                forceHeight: true
            });

            subArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "sub_area",
                    css:{
                        visibility: "hidden"
                    }
                },
                parent: div
            });

            var keyGuide = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "programme_search_sub_area_key_guide_area",
                    css: {
                        position: "absolute", overflow: "visible", visibility: "hidden"
                    }
                },
                parent: subArea
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: (1803 - 576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: (1831-576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1803 - 576), top: 939, width: 150, height: 24,
                        "font-size": 22, "font-family": "RixHead L", "letter-spacing": -1.1, color: "rgba(255, 255, 255, 0.3)"
                    }
                },
                text: "페이지",
                parent: keyGuide
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_option_related.png",
                    css: {
                        position: "absolute", left: (1789-576), top: 1001
                    }
                },
                parent: subArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1823-576), top: 1000, width:100, height:26 , "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                        "letter-spacing": -1.2
                    }
                },
                text: "옵션",
                parent: subArea
            });

            for(var i = 0; i < NUMBER_OF_PREVIEW_ITEM; i++) {

                item[i] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "ps_item"
                    },
                    parent: itemGroup
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "ps_itemBottomLine no"+i,
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: item[i]
                });
            };

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "ps_itemTopLine",
                    css: {
                        position: "absolute"
                    }
                },
                parent: $(".ps_item")
            });

            util.makeElement({
                tag:"<div />",
                attrs: {
                    class: "ps_programArea",
                    css: {
                        position: "absolute"
                    }
                },
                parent: $(".ps_item")
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "ps_programName",
                    css: {
                       /* "font-family": "RixHead L"*/
                    }
                },
                text: "프로그램이름",
                parent: $(".ps_programArea")
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "ps_channelArea",
                    css: {
                        position: "absolute"
                    }
                },
                parent: $(".ps_item")
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "ps_chIcon",
                    src: "images/icon/icon_sky_epg.png",
                    css: {
                        float: "left"
                    }
                },
                parent: $(".ps_channelArea")
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "ps_chNum",
                    css: {
                        /*                        "font-family": "RixHead L"*/
                    }
                },
                text: "999",
                parent: $(".ps_channelArea")
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "ps_chName",
                    css: {
                        /*            "font-family": "RixHead L"*/
                    }
                },
                text: "알티프로그램",
                parent: $(".ps_channelArea")
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "ps_programInfoArea",
                    css:{
                        position: "absolute"
                    }
                },
                parent: $(".ps_item")
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "ps_piText"
                },
                text: "방영정보 여기",
                parent: $(".ps_programInfoArea")
            });

            util.makeElement({
                tag:"<div />",
                attrs: {
                    class: "ps_reservationInfoArea",
                    css: {
                        position: "absolute", display: "none"
                    }
                },
                parent: $(".ps_item")
            });
            util.makeElement({
                tag:"<span />",
                attrs: {
                    class: "ps_riText",
                    css: {
                       /* "font-family": "RixHead L",*/ float: "left"
                    }
                },
                text: "시청 예약",
                parent: $(".ps_reservationInfoArea")
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_book.png",
                    class: "ps_riImg"
                },
                parent: $(".ps_reservationInfoArea")
            });
        };

        function keyLeft() {
            log.printDbg("keyLeft()");

            var consume = false;
            var channelMaxPage = getMaxPage();

            return consume;
        };

        function keyRight() {
            log.printDbg("keyRight()");

            if (!isFocusedItemArea){
                isFocusedItemArea = true;
                setScrollFocus();
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
            }
            return true;
        };

        function keyUp() {
            log.printDbg("keyUp()");

            var curPage = getChannelPage();

            item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

            if(isFocusedItemArea) {
                ps_programIndex --;
            } else { //scrollArea
                ps_programIndex = ps_programIndex - (NUMBER_OF_FOCUSED_ITEM + (ps_programIndex % NUMBER_OF_FOCUSED_ITEM));
            }

            if(ps_programIndex < 0) {
                ps_programIndex = ps_programList.length -1;
            }
            if(isFocusedItemArea){
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
            }

            var nextPage = getChannelPage();

            if (curPage !== nextPage) {
                changeFocusedProgramIndex(ps_programIndex);
            }
        };

        function keyDown() {
            log.printDbg("keyDown()");

            var curPage = getChannelPage();

            item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

            if(isFocusedItemArea) {
                ps_programIndex ++;
            } else {
                ps_programIndex = ps_programIndex + (NUMBER_OF_FOCUSED_ITEM - (ps_programIndex % NUMBER_OF_FOCUSED_ITEM));
            }

            if(ps_programIndex > ps_programList.length -1) {
                ps_programIndex = 0;
            }
            if(isFocusedItemArea){
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
            }

            var nextPage = getChannelPage();

            if (curPage !== nextPage) {
                changeFocusedProgramIndex(ps_programIndex);
            }
        };

        function keyRed () {
            log.printDbg("keyRed()");

            var curPage = getChannelPage();

            if (isFocusedItemArea) {
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

                if (ps_programIndex % NUMBER_OF_FOCUSED_ITEM === 0 && getMaxPage() > 1) {
                    ps_programIndex = ps_programIndex - NUMBER_OF_FOCUSED_ITEM;

                    if (ps_programIndex < 0) {
                        ps_programIndex = (getMaxPage() - 1) * NUMBER_OF_FOCUSED_ITEM;
                    }
                }
                else {
                    ps_programIndex = curPage * NUMBER_OF_FOCUSED_ITEM;
                }

                item[0].addClass("focus");
            }
            else {
                isFocusedItemArea = true;

                ps_programIndex = curPage * NUMBER_OF_FOCUSED_ITEM;
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                setScrollFocus();
            }

            var nextPage = getChannelPage();

            if (curPage !== nextPage) {
                changeFocusedProgramIndex(ps_programIndex);
            }
        }

        function keyBlue () {
            log.printDbg("keyBlue()");

            var curPage = getChannelPage();

            var nextPage = curPage + 1;
            if (nextPage > getMaxPage() - 1) {
                nextPage = 0;
            }

            if (isFocusedItemArea) {
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

                if ((ps_programIndex % NUMBER_OF_FOCUSED_ITEM === NUMBER_OF_FOCUSED_ITEM - 1 || ps_programIndex === ps_programList.length - 1) && getMaxPage() > 1) {
                    ps_programIndex = (nextPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;

                    if (ps_programIndex > ps_programList.length - 1) {
                        ps_programIndex = ps_programList.length - 1;
                    }
                }
                else {
                    ps_programIndex = (curPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
                    if (ps_programIndex > ps_programList.length - 1) {
                        ps_programIndex = ps_programList.length - 1;
                    }
                }

                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");
            }
            else {
                isFocusedItemArea = true;

                ps_programIndex = (curPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
                if (ps_programIndex > ps_programList.length -1) {
                    ps_programIndex = ps_programList.length - 1;
                }
                item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                setScrollFocus();
            }

            nextPage = getChannelPage();

            if (curPage !== nextPage) {
                changeFocusedProgramIndex(ps_programIndex);
            }
        }

        function keyOK() {
            log.printDbg("keyOK()");

            var programIdx = ps_programIndex;
            var selectedProgram = ps_programList[programIdx];
            var startTime = selectedProgram.startTime * 1000;
            var programDuration = selectedProgram.duration * 1000;
            var currentTime = (new Date()).getTime();

            if(isFocusedItemArea) {

                if( startTime < currentTime && startTime + programDuration >= currentTime ) {
                    // 현재 시청가능한 프로그램
                    setFocusChannelTune();
                } else if(startTime > currentTime) {
                    // 시청 예약
                    reservationProgramme(selectedProgram);
                }
            }else {

            }

        };

        function setFocusChannelTune () {
            log.printDbg("setFocusChannelTune()");

            var selectedProgram = ps_programList[ps_programIndex];
            var selectedChannel = selectedProgram.channel;

            _this.parent.mainChannelTune({channel: selectedChannel});
        }

        function reservationProgramme (program) {
            log.printDbg("reservationProgramme()");

            KTW.managers.service.ReservationManager.add(program, true, function (result) {
                var RESULT = KTW.managers.service.ReservationManager.ADD_RESULT;
                if (result === RESULT.SUCCESS_RESERVE || result === RESULT.SUCCESS_CANCEL
                    || result === RESULT.SUCCESS_SERIES_RESERVE || result === RESULT.SUCCESS_SERIES_CANCEL
                    || result === RESULT.DUPLICATE_RESERVE) {
                    drawProgramList(customizableList, isSortedByTime);
                }
            });
        }

        function setProgrammeList () {
            var programGenreCode = getProgramGenreCode(menuId);
            ps_programList = epgDataAdapter.getGenreProgrammesList({genreCode: programGenreCode, orderByTime: isSortedByTime});

            if (ps_programList && ps_programList.length > 0) {
                isProgramListExist = true;
            }
            else {
                isProgramListExist = false;
            }
        }

        this.create = function() {
            log.printDbg("create()");

            if (!isCreate) {
                createElement();
                isCreate = true;
            }
        };

        this.initData = function () {
            isSortedByTime = true;
            setProgrammeList();
        };

        this.show = function (options) {
            log.printDbg("show()");

            parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3);

            div.css({visibility: "inherit"});

            drawProgramList(null, isSortedByTime);
            parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");

            if (!options || !options.pause) {

                // hide
                div.css({visibility: "hidden"});

                if (div.hasClass("focus")) {
                    this.blur();
                }
            }
            else {
            }
        };

        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");

            item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

            drawProgramList(null, isSortedByTime);

            if (isProgramListExist) {
                scroll.show({
                    maxPage: getMaxPage(),
                    curPage: getChannelPage()
                });
                setScrollFocus();

                if (getMaxPage() > 1) {
                    subArea.find("[name=programme_search_sub_area_key_guide_area]").css({visibility: "inherit"});
                }
                else {
                    subArea.find("[name=programme_search_sub_area_key_guide_area]").css({visibility: "hidden"});
                }
            }

            subArea.css({visibility: "inherit"});
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.removeClass("focus");

            item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");

            scroll.hide();
            isFocusedItemArea = true;
            ps_programIndex = 0;
            var flagToUpdateList = true;
            drawProgramList(null, flagToUpdateList);
            subArea.css({visibility: "hidden"});

        };

        this.remove = function () {
            log.printDbg("remove()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft();
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    consumed = keyRed();
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOK();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    // 2017.06.23 dhlee
                    // scroll에 포커스가 있는 상태이더라도 content menu가 동작 해야 한다. (UI 변경 요청 사항 반영)
                    showRelatedMenu(this);
                    /*
                    if (isFocusedItemArea) {
                        showRelatedMenu(this);
                    }
                    */
                    consumed = true;
                    break;
            }
            return consumed;
        };

        this.isFullScreen = function () {
            log.printDbg("isFullScreen()");

            return false;
        };
        this.isFocusAvailable = function () {

            var isAvailable = isProgramListExist;
            return isAvailable;
        };

        this.getDataStatus = function () {
            if (!isProgramListExist) {
                if (KTW.CONSTANT.IS_OTS) {
                    return KTW.ui.view.EpgView.DATA_STATUS.WEAK_SIGNAL;
                }
                else {
                    return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
                }
            }
            else {
                return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
            }
        };

        function getProgramGenreCode(menuId) {
            log.printDbg("getProgramGenreCode()");

            var programGenreCodeList = programSearchAdapter.PROGRAM_GENRE;
            var programGenreCode = null;
            var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;

            switch(menuId) {
                case MENU_ID.PROGRAM_MOVIE:
                    programGenreCode = programGenreCodeList.MOVIE_DRAMA;
                    break;
                case MENU_ID.PROGRAM_NEWS:
                    programGenreCode = programGenreCodeList.NEW_AFFAIR;
                    break;
                case MENU_ID.PROGRAM_ENTERTAINMENT:
                    programGenreCode = programGenreCodeList.SHOW_GAME;
                    break;
                case MENU_ID.PROGRAM_SPORTS:
                    programGenreCode = programGenreCodeList.SPORTS;
                    break;
                case MENU_ID.PROGRAM_KIDS:
                    programGenreCode = programGenreCodeList.CHILDREN;
                    break;
                case MENU_ID.PROGRAM_MUSIC:
                    programGenreCode = programGenreCodeList.MUSIC;
                    break;
                case MENU_ID.PROGRAM_ART:
                    programGenreCode = programGenreCodeList.ART_CULTURE;
                    break;
                case MENU_ID.PROGRAM_SOCIETY:
                    programGenreCode = programGenreCodeList.SOCIAL_ECONOMICS;
                    break;
                case MENU_ID.PROGRAM_EDU:
                    programGenreCode = programGenreCodeList.EDUCATION_SCIENCE;
                    break;
                case MENU_ID.PROGRAM_LEISURE:
                    programGenreCode = programGenreCodeList.LEISURE_HOBBIES;
                    break;
                case MENU_ID.PROGRAM_ETC:
                    programGenreCode = programGenreCodeList.ETC;
                    break;
                default:
                    programGenreCode = programGenreCodeList.CHILDREN;
                    break;
            }
            return programGenreCode;
        };

        function drawProgramList(programList, sortingOption) {
            log.printDbg("drawProgramList()");

            var thisItemElement = null;
            var thisProgramList = programList;
            var thisProgram = null;
            var today = new Date().getDay();
            var dayForProgram = new Date();

            if (sortingOption !== undefined && sortingOption !== null && isSortedByTime !== sortingOption) {
                isSortedByTime = sortingOption;
                setProgrammeList();
            }

            if (!thisProgramList) {
                thisProgramList = ps_programList;
            }

            if (!thisProgramList.length) {
                itemGroup.css({display: "none"});
                return ;
            }

            for (var i = 0; i < NUMBER_OF_PREVIEW_ITEM; i++) {
                thisItemElement = item[i];
                thisProgram = thisProgramList[i];

                thisItemElement.find(".ps_reservationInfoArea").css({display: "none"}); //프로그램 예약여부 확인 후 display
                thisItemElement.find(".ps_chIcon").css({display: "none"});
                thisItemElement.find(".ps_programName").text("");
                thisItemElement.find(".ps_chName").text("");
                thisItemElement.find(".ps_chNum").text("");
                thisItemElement.find(".ps_piText").text("");

                if(thisProgram){
                    itemGroup.css({display: ""});
                    thisItemElement.css({ display: ""});


                    var programName = thisProgram.name;
                    var channel = thisProgram.channel;
                    var chNum = thisProgram.channel.majorChannel;
                    var chName = thisProgram.channel.name;
                    var startTime = thisProgram.startTime * 1000;
                    dayForProgram.setTime(startTime);

                    /* 프로그램 예약여부 확인 */
                    if (KTW.managers.service.ReservationManager.isReserved(thisProgram)) {
                        thisItemElement.find(".ps_reservationInfoArea").css({display: ""});
                    }
                    /* 채널아이콘 결정 */
                    var icon = "";
                    if (navAdapter.isBlockedChannel(channel)) {
                        icon = "images/icon/icon_block.png";
                        thisItemElement.find(".ps_channelArea span").css({"margin-top": 2});

                    }
                    else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channel)) {
                        icon = "images/icon/icon_fav.png";
                        thisItemElement.find(".ps_channelArea span").css({"margin-top": 2});
                    }
                    else {
                        icon = "images/icon/icon_sky_epg.png";
                        thisItemElement.find(".ps_channelArea span").css({"margin-top": 0});
                    }

                    thisItemElement.find(".ps_chIcon").css({display: "block"});
                    thisItemElement.find(".ps_chIcon").attr({src: icon});

                    thisItemElement.find(".ps_programName").text(programName);
                    thisItemElement.find(".ps_chName").text(chName);
                    thisItemElement.find(".ps_chNum").text(util.numToStr(chNum,3,null));

                    var startTimeTxt = null;
                    if(today == dayForProgram.getDay())
                        startTimeTxt = "오늘 "+util.getTimeString(startTime)+" 방영";
                    else
                        startTimeTxt = "내일 "+util.getTimeString(startTime)+" 방영";

                    thisItemElement.find(".ps_piText").text(startTimeTxt);
                }

            }
        };

        function changeFocusedProgramIndex (programIdx) {
            log.printDbg("changeFocusedProgramIndex");

            var currentPage = getChannelPage();

            var startIdx = programIdx - (programIdx % NUMBER_OF_FOCUSED_ITEM);
            customizableList = [0];

            for(var i = 0; i < NUMBER_OF_FOCUSED_ITEM; i++) {
                if(currentPage > 0 ) {
                    if(startIdx + i > ps_programList.length-1) {
                        break;
                    }
                    customizableList[i] = ps_programList[startIdx+i];
                }
                else{
                    if(i > ps_programList.length-1){
                        break;
                    }
                    customizableList[i] = ps_programList[i];
                }
            }

            drawProgramList(customizableList,isSortedByTime);
            // scroll page 변경.
            scroll.setCurrentPage(getChannelPage());
        }

        function getCurrentChannel() {

            return ps_programList[ps_programIndex].channel;
        }

        function getMaxPage() {
            return Math.ceil(ps_programList.length / NUMBER_OF_FOCUSED_ITEM);
        };

        function getChannelPage() {
            return Math.floor(ps_programIndex / NUMBER_OF_FOCUSED_ITEM);
        };

        function setScrollFocus() {

            log.printDbg("setScrollFocus()");

            if (isFocusedItemArea) {
                scroll.setFocus(false);
            }
            else {
                scroll.setFocus(true);
            }
        };

        function showRelatedMenu (_this) {
            log.printDbg("showRelatedMenu()");

            var isShowFavoritedChannel = true;
            var isShowProgramDetail = true;
            var isShowTwoChannel = false;
            var isShowFourChannel = true;
            var isShowHitChannel = true;
            var isShowGenreOption = false;


            var focusMenuId = menuId;//KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST;

            var epgrelatedmenuManager = KTW.managers.service.EpgRelatedMenuManager;

            var selectedProgram = ps_programList[ps_programIndex];
            var curChannel = selectedProgram.channel;

            if(navAdapter.isAudioChannel(curChannel) === true) {
                isShowFavoritedChannel = false;
                isShowProgramDetail = false;
            }

            if(selectedProgram !== undefined && selectedProgram !== null ) {
                if(selectedProgram.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL || selectedProgram.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                    isShowProgramDetail = false;
                }
            }

            //var isBlocked = KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(curChannel);

//            if(isBlocked === true || curChannel.desc === 2) {
            if(curChannel.desc === 2) {
                isShowProgramDetail = false;
            }

            epgrelatedmenuManager.showFullEpgRelatedMenu(curChannel, function (menuIndex, optionType) {
                cbRelateMenuLeft({
                    _this: _this,
                    menuIndex: menuIndex,
                    optionType: optionType

                });
            }, isShowFavoritedChannel, isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel, true, focusOptionIndex, function (menuId) {
                cbRelateMenuRight({
                    _this: _this,
                    menuId: menuId
                });
            }, focusMenuId);
        }

        function cbRelateMenuLeft (options) {
            log.printDbg("cbRelateMenuLeft()");

            KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

            if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                // 선호채널 등록 / 해제
                var curChannel = getCurrentChannel();
                KTW.managers.service.EpgRelatedMenuManager.favoriteChannelOnOff(curChannel,cbFavoriteChannelOnOff);

            }
            else if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                // OTS 자세히 버튼
                var selectedProgramme = ps_programList[ps_programIndex];


                var now = new Date().getTime();
                if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                    now = Math.floor(now/1000);
                }


                var isShowReservation = true;

                if(selectedProgramme !== undefined && selectedProgramme !== null
                    && selectedProgramme.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL
                    && selectedProgramme.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                    if(selectedProgramme.startTime + selectedProgramme.duration <= now) {
                        /**
                         * 과거 프로그램
                         */
                        isShowReservation = false;
                    }else {
                        if(now<=selectedProgramme.startTime) {
                            isShowReservation = true;
                        }else {
                            isShowReservation = false;
                        }
                    }
                    var curChannel = getCurrentChannel();
                    KTW.managers.service.EpgRelatedMenuManager.activateFullEpgProgrammeDetailPopup({
                        channel : curChannel,
                        program : selectedProgramme,
                        is_show_reservation : isShowReservation,
                        full_epg_callback_func : function (btnType) {
                            cbFullEpgProgramDetail({btnType: btnType, _this: options._this});
                        }
                    });

                }
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                // 4채널
                var locator = KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();

                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                    catId: options._this.menuId,
                    contsId: "",
                    locator: locator,
                    reqPathCd: ""
                });
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                // 실시간 인기 채널
                //KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();

                options._this.parent.jumpPopularMenu();
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION ){
                var flagToUpdateList = isSortedByTime;

                if(options.optionType === 0) {
                    //시간순 정렬
                    flagToUpdateList = true
                }else {
                    // 가나다순 정렬
                    flagToUpdateList = false;
                }
                focusOptionIndex = options.optionType;

                
                if(flagToUpdateList != isSortedByTime) {

                    item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].removeClass("focus");
                    ps_programIndex = 0;
                    if(isFocusedItemArea)
                        item[ps_programIndex % NUMBER_OF_FOCUSED_ITEM].addClass("focus");

                    drawProgramList(null, flagToUpdateList);
                    // scroll page 변경.
                    scroll.setCurrentPage(getChannelPage());
                }
            }
        };


        function cbFullEpgProgramDetail (options) {
            log.printDbg("cbFullEpgProgramDetail() btnType : " + options.btnType);
            KTW.managers.service.EpgRelatedMenuManager.deactivateFullEpgProgrammeDetailPopup();
            // 시청 btn : 0 , 예약 버튼 : 1 , 닫기 : 2
            if (options.btnType === 0) {
                setFocusChannelTune(options._this);
            }
            else if(options.btnType === 1) {
                var selectedProgramme = ps_programList[ps_programIndex];

                reservationProgramme({
                    programme: selectedProgramme,
                    _this: options._this
                });
            }
        }

        function cbRelateMenuRight (options) {
            log.printDbg("cbRelateMenuRight()");

            KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

            if (options._this.menuId !== options.menuId) {
                // TODO 가이드 메뉴 가져오는 로직으로 수정
                var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.id === options.menuId) {
                            return true;
                        }
                    }
                })[0];

                if (epgMenu) {
                    options._this.parent.jumpTargetEpgView({
                        targetMenu: epgMenu
                    });
                }
                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                    catId: options._this.menuId,
                    contsId: "",
                    locator: "",
                    reqPathCd: ""
                });
            }
        }

        function cbFavoriteChannelOnOff(){
            log.printDbg("cbFavoriteChannelOnOff()");
            changeFocusedProgramIndex(ps_programIndex);
            //drawProgramList(customizableList, isSortedByTime);
        };
    };
})();