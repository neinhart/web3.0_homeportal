/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>GenreChannelView</code>
 *
 * @author sw.nam
 * @since 2017-01-04
 *
 * 장르별 채널(otv) & 프로그램 검색(ots)
 */

(function() {

    var NUMBER_OF_OTV_PREVIEW_ITEM = 9;
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var div = null;
    var menuDiv = null;

    KTW.ui.view.GenreChannelView = function (options) {

        var parent = options.parent;
        this.menuId = options.menuId;

        function createElement() {
            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "genre_channel_view",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 919, height: 1080
                    }
                },
                parent: parent.getEpgContainerDiv()
            });

            menuDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, width: 919, height: 1080
                    }
                },
                parent:div
            });

            for (var i = 0; i < NUMBER_OF_OTV_PREVIEW_ITEM; i++) {
                var item = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "item",
                        css: {
                            position: "absolute", left: (625 - 576), top: (146 + (i*94)), width: 393, height: 40
                        }
                    },
                    parent: menuDiv
                });


                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "cut",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 393, height: 40, "font-size" : 38, "letter-spacing" : -1.9,
                            color: "rgba(255, 255, 255, 0.3)", "font-family": "RixHead L"
                        }
                    },
                    parent: item
                });
            }
        }

        function setMenuName (curMenu) {
            log.printDbg("setMenuName()");

            for (var i = 0; i < NUMBER_OF_OTV_PREVIEW_ITEM; i++) {
                if (i < curMenu.children.length) {
                    var menuName = parent.language === "eng" ? curMenu.children[i].englishItemName : curMenu.children[i].name;
                    if (!menuName || menuName === "") {
                        menuName = curMenu.children[i].name;
                    }
                    menuDiv.children().eq(i).find("span").text(menuName);
                }
                else {
                    menuDiv.children().eq(i).find("span").text("");
                }
            }
        }

        this.create = function () {
            log.printDbg("create()");

            if (!div) {
                createElement();
            }
        };

        this.initData = function () {};

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            var _this = this;
            var curMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: [parent.getGuideMenu()],
                cbCondition: function (menu) {
                    if (menu.id === _this.menuId) {
                        return true;
                    }
                }
            })[0];

            setMenuName(curMenu);

            parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R1);

            if (!options || !options.resume) {
                div.css({visibility: "inherit"});
            }

            parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});
                this.blur();
            }
            else {
            }
        };

        this.focus = function () {
            log.printDbg("focus()");

        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.remove = function () {
            log.printDbg("remove()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");
            return false;
        };

        this.isFullScreen = function () {
            return;
        };

        this.isFocusAvailable = function () {
            return false;
        };

        this.getDataStatus = function () {
            return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
        };
    };


})();