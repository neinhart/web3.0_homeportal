/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>CommunityChannelView</code>
 *
 * @author sw.nam
 * @since 2016-12-23
 */

(function() {
    KTW.ui.view.CommunityChannelView = function (options) {
        KTW.ui.view.EpgView.call(this, options);

        this.curMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === options.menuId) {
                    return true;
                }
            }
        })[0];

        var CHANNELS_PER_SECTOR =  4;
        var SECTOR_PER_PAGE = 2;
        var CHANNELS_PER_PAGE = 8;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
        var ocImageDataManager = KTW.managers.data.OCImageDataManager;

        var div = null;
        var listArea = null;
        var subArea = null;

        var scroll = null;

        /* for Focus View */
        var isFocusListArea = true;

        var _this = this;

        var parent = options.parent;
        var menuId = options.menuId;

        var pipTimer = null;

        /**
         * [dj.son] create community channel view frame
         */
        function createElement () {
            log.printDbg("COMMUNITY CHANNEL");

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "community_channel_view",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                    }
                },
                parent: parent.getEpgContainerDiv()
            });

            listArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "community_channel_list_area",
                    css: {
                        position: "absolute", overflow: "visible"
                    }
                },
                parent: div

            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "dim_vodlist_btm",
                    src: "images/dim_vodlist_btm.png",
                    css: {
                        position: "absolute", left: 0, top: 912, width: 1344, height: 168
                    }
                },
                parent: div
            });

            /**
             * jjh1117 2017/12/20
             * background Image 추가 해야 함.
             */

            var scrollArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1829-576), top: 149, overflow: "visible", visibility: "hidden"
                    }
                },
                parent: div
            });

            scroll = new KTW.ui.component.Scroll({
                parentDiv: scrollArea,
                height: 702,
                forceHeight: true
            });



            subArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "community_channel_sub_area"
                },
                parent: div
            });

            var keyGuide = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "community_channel_sub_area_key_guide_area",
                    css: {
                        position: "absolute", overflow: "visible", visibility: "hidden"
                    }
                },
                parent: subArea
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: (1803 - 576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: (1831-576), top: 904
                    }
                },
                parent: keyGuide
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1803 - 576), top: 939, width: 150, height: 24,
                        "font-size": 22, "font-family": "RixHead L", "letter-spacing": -1.1, color: "rgba(255, 255, 255, 0.3)"
                    }
                },
                text: "페이지",
                parent: keyGuide
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/key_shadow.png",
                    css: {
                        position: "absolute", left: (1115-576), top: 912 , width : 805 , height:168
                    }
                },
                parent: subArea
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_option_related.png",
                    css: {
                        position: "absolute", left: (1789-576), top: 1001  , width : 27 , height:22
                    }
                },
                parent: subArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: (1823 - 576), top: 1000, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                        "letter-spacing": -1.2
                    }
                },
                text: "옵션",
                parent: subArea
            });
        }

        /**
         * [dj.son] create community page
         */
        function createPage (options) {
            var element = null;

            if (options.pageIdx >= 0 && options.pageIdx < _this.getChannelMaxPage()) {
                element = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "community_page",
                        pageIdx: options.pageIdx,
                        css: {
                            overflow: "visible"
                        }
                    }
                });

                for (var i = 0; i < CHANNELS_PER_PAGE / CHANNELS_PER_SECTOR; i++) {
                    var sectorIdx = (options.pageIdx * (CHANNELS_PER_PAGE / CHANNELS_PER_SECTOR)) + i;
                    log.printDbg("createPage() sectorIdx : " + sectorIdx);
                    log.printDbg("createPage()  getMaxSector() return : " + getMaxSector());
                    log.printDbg("createPage()  _this.getChannelList().length  : " + _this.getChannelList().length);
                    
                    if (sectorIdx <= getMaxSector()) {
                        var sector = createSector({
                            channelList: getChannelListBySector(sectorIdx),
                            sectorIdx: sectorIdx
                        });

                        element.append(sector);
                    }
                }
            }
            return element;
        }

        /**
         * [dj.son] create community sector in page
         */
        function createSector (options) {
            var element = null;
            var cugLogoInfo = ocImageDataManager.getCugLogoInfo();

            if (options.channelList && options.channelList.length > 0) {
                element = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "community_sector",
                        sectorIdx: options.sectorIdx,
                        css: {
                            overflow: "visible"
                        }
                    }
                });


                for (var i = 0; i < options.channelList.length; i++) {
                    element.append(createChannelElement({
                        channel: options.channelList[i],
                        channelIdx: (options.sectorIdx * CHANNELS_PER_SECTOR) + i,
                        imagePath: cugLogoInfo.path
                    } , i));
                }
            }

            return element;
        }

        /**
         * [dj.son] create community channel
         */
        function createChannelElement (options , idx) {
            var element = null;

            if (options && options.channel) {
                element = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "community_channel",
                        channelIdx: options.channelIdx,
                        css: {
                            position: "absolute", left : (295*idx)
                        }
                    }
                });

                var imgArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "community_channel_img_area",
                        css: {
                            position: "absolute", overflow: "visible"/*, "-webkit-transition": "-webkit-transform 0.5s"*/
                        }
                    },
                    parent: element
                });
                // [sw.nam] 2017.08.03 cug 이미지 데이터 연동 로직 추가
                var cugLogo = getCugLogo(options.channel.data.sid);
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "community_channel_img",
                        src: cugLogo,
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: imgArea
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "album_dim",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: imgArea
                });
                
                

                var focusArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focus_red_border_box community_channel_focus_area",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: element
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focus_black_border_box",
                        css: {
                            position: "absolute", left : 0 , top : 0 , width: 255, height: 255
                        }
                    },
                    parent: focusArea
                });




                var textArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "community_channel_text_area",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: element
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "community_channel_name cut",
                        css: {
                            position: "absolute", height: 35, "text-align": "left", "font-size": 30, "letter-spacing": -1.5
                        }
                    },
                    text: util.numToStr(options.channel.data.majorChannel, 3) + " " + options.channel.data.name,
                    parent: textArea
                });
            }

            return element;
        }

        function getCugLogo (sid) {
            var cugLogoInfo = KTW.managers.data.OCImageDataManager.getCugLogoInfo();
            // 편성되지 않은 경우 사용할 default image
            var src = "images/default_cug.png";

            if (cugLogoInfo && cugLogoInfo.imageList && cugLogoInfo.imageList.length > 0) {
                for (var i = 0; i < cugLogoInfo.imageList.length; i++) {
                    var targetSrc = cugLogoInfo.imageList[i];
                    if (targetSrc.indexOf(sid) > -1) {
                        src = cugLogoInfo.path + targetSrc;
                        break;
                    }
                }
            }

            return src;
        }

        /**
         * [dj.son] init element, 변수
         */
        function init () {
            log.printDbg("init()");

            isFocusListArea = true;

            listArea.children().remove();

            for (var i = 0; i < 2; i++) {
                listArea.append(createPage({
                    pageIdx: i
                }));
            }

            setFocusArea();
            setFocusChannel();
        }

        /**
         * [dj.son] 페이지별 채널 리스트 리턴
         */
        function getChannelListByPage (pageIdx) {
            log.printDbg("getChannelListByPage(" + pageIdx + ")");

            var result = [];
            var channelList = _this.getChannelList();
            var chLength = channelList.length;
            var startIdx = pageIdx * CHANNELS_PER_PAGE;

            if (pageIdx < 0 || pageIdx >= _this.getChannelMaxPage()) {
                return channelList;
            }

            for (var i = 0; i < CHANNELS_PER_PAGE; i++) {
                if (startIdx + i < chLength) {
                    result.push(channelList[startIdx + i]);
                }
                else {
                    break;
                }
            }

            return result;
        }

        /**
         * [dj.son] sector 의 max number 리턴
         */
        function getMaxSector () {
            return Math.ceil(_this.getChannelList().length / CHANNELS_PER_SECTOR);
        }

        /**
         * [dj.son] sector 별 채널 리스트 리턴
         */
        function getChannelListBySector (sectorIdx) {
            log.printDbg("getChannelListBySector(" + sectorIdx + ")");

            var result = [];
            var channelList = _this.getChannelList();
            var chLength = channelList.length;
            var startIdx = sectorIdx * CHANNELS_PER_SECTOR;

            if (sectorIdx < 0 || sectorIdx >= getMaxSector()) {
                return channelList;
            }

            for (var i = 0; i < CHANNELS_PER_SECTOR; i++) {
                if (startIdx + i < chLength) {
                    result.push(channelList[startIdx + i]);
                }
                else {
                    break;
                }
            }

            return result;
        }

        function isChannelListExist () {
            var channelList = _this.getChannelList();
            return channelList && channelList.length > 0 ? true : false;
        }

        function setFocusArea () {
            log.printDbg("setFocus()");

            if (isFocusListArea) {
                listArea.addClass("focus");
                scroll.setFocus(false);
            }
            else {
                listArea.removeClass("focus");
                scroll.setFocus(true);
            }
        }

        function setFocusChannel () {
            log.printDbg("setFocusChannel()");

            listArea.find(".community_channel").removeClass("focus");
            listArea.find(".community_channel[channelIdx=" + _this.getChannelIdx() + "]").addClass("focus");
        }

        

        function moveFocusPage (options) {
            log.printDbg("moveFocusPage()");

            var curPageIdx = _this.getChannelPage();
            var maxPage = _this.getChannelMaxPage();

            if (curPageIdx === options.nextPageIdx) {
                return;
            }

            listArea.find(".community_page_slide_up_delete, .community_page_slide_up_delete2, .community_page_slide_down_delete, .community_page_slide_down_delete_2").remove();

            var allRemoveClass = "preview_community_page"
                + " community_page_slide_down_add"
                + " community_page_slide_up_add"
                + " community_page_slide_down_add_2"
                + " community_page_slide_up_middle"
                + " community_page_slide_up_middle2"
                + " community_page_slide_down_middle"
                + " community_page_slide_down_middle2"
                + " community_page_slide_down_add_hidden";

            listArea.children().removeClass(allRemoveClass);

            var existPage1  = listArea.find(".community_page").first();
            var existPage2  = listArea.find(".community_page").last();

            var appendPageIdx = options.nextPageIdx;
            if (!options.isUp) {
                log.printDbg("isDown");
                appendPageIdx = options.nextPageIdx + 1;

                if (appendPageIdx > maxPage - 1) {
                    appendPageIdx = appendPageIdx - maxPage;
                }
            }
            var addPage = createPage({
                pageIdx: appendPageIdx
            });

            if (options.isUp) {
                log.printDbg("isUp");
                listArea.prepend(addPage);

                if (appendPageIdx === maxPage - 1) {
                    //마지막 페이지인 경우
                    //1.위쪽에 페이지 추가
                    addPage.addClass("community_page_slide_up_add");
                    //2. 기존 페이지 중 상위 페이지는 위에서 아래로 내리면서 안보이게 지운다.
                    existPage1.addClass("community_page_slide_up_middle2");
                    //3.기존 페이지 중 하위 페이지는 아래로 내리면서 삭제
                    existPage2.addClass("community_page_slide_up_delete");
                }
                else {
                    //1.위쪽에 페이지 추가
                    addPage.addClass("community_page_slide_up_add");
                    //2.기존 페이지 중 상위 페이지는 위에서 아래로 내린다.
                    existPage1.addClass("community_page_slide_up_middle");
                    //3.기존 페이지 중 하위 페이지는 아래로 내리면서 삭제
                    existPage2.addClass("community_page_slide_up_delete");
                }
            }
            else {
                log.printDbg("isDown");
                listArea.append(addPage);

                var curPageChannelList = getChannelListByPage(curPageIdx);

                if (curPageIdx === maxPage - 1) {
                    //마지막 페이지인 경우

                    //2. 기존 페이지 중 상위 페이지는 위로 올리면서 지운다.
                    existPage1.addClass("community_page_slide_down_delete");

                    //1. 아래에 페이지 추가 (지우면서)
                    //3. 기존 페이지 중 하위 페이지는 아래서 위로 올려 보여준다.
                    if (curPageChannelList.length > CHANNELS_PER_SECTOR) {
                        addPage.addClass("community_page_slide_down_add");
                        existPage2.addClass("community_page_slide_down_middle");
                    }
                    else {
                        addPage.addClass("community_page_slide_down_add_2");
                        existPage2.addClass("community_page_slide_down_middle2");
                    }
                }
                else {
                    //1. 아래에 페이지 추가
                    //   addPage.addClass("community_page_slide_down_add");
                    //2. 기존 페이지 중 상위 페이지는 위로 올리면서 지운다.
                    existPage1.addClass("community_page_slide_down_delete");
                    //3. 기존 페이지 중 하위 페이지는 아래서 위로 올려 보여준다.
                    existPage2.addClass("community_page_slide_down_middle");

                    if (appendPageIdx ===0) {
                        addPage.addClass("community_page_slide_down_add_hidden");
                    } else {
                        addPage.addClass("community_page_slide_down_add");
                    }
                }
            }

            _this.changeChannelIdx({
                nextChannelIdx: options.nextChannelIdx
            });
            setFocusChannel();
            scroll.setCurrentPage(_this.getChannelPage());
        }

        function keyLeft () {
            log.printDbg("keyLeft()");

            var consume = false;

            if (isFocusListArea) {
                var channelIdx = _this.getChannelIdx();

                if (channelIdx % CHANNELS_PER_SECTOR === 0) {

                    isFocusListArea = false;
                    _this.changeChannelIdx({
                        nextChannelIdx: channelIdx - (channelIdx % CHANNELS_PER_PAGE)
                    });
                    return consume;
                    // if(scroll.maxPage > 1) {
                    //     setFocusArea();
                    // } else {
                    //     return consume;
                    // }
                }
                else {
                    _this.changeChannelIdx({
                        nextChannelIdx: channelIdx - 1
                    });
                    setFocusChannel();
                }
                consume = true;
            }
            return consume;
        }

        function keyRight () {
            log.printDbg("keyRight()");

            if (isFocusListArea) {
                var channelIdx = _this.getChannelIdx();
                var channelLength = _this.getChannelList().length;
                var nextChannelIdx = channelIdx + 1;
                if (nextChannelIdx > channelLength - 1) {
                    nextChannelIdx = 0;
                }
                var curPageIdx = _this.getChannelPage();
                var nextPageIdx = curPageIdx + 1;
                if (nextPageIdx >= _this.getChannelMaxPage()) {
                    nextPageIdx = 0;
                }

                if (_this.getChannelMaxPage() > 1) {
                    if (channelIdx === channelLength - 1 || channelIdx % CHANNELS_PER_PAGE === CHANNELS_PER_PAGE - 1) {
                        moveFocusPage({
                            nextPageIdx: nextPageIdx,
                            nextChannelIdx: nextChannelIdx
                        });
                    }
                    else {
                        _this.changeChannelIdx({
                            nextChannelIdx: nextChannelIdx
                        });
                        setFocusChannel();
                    }
                }
                else {
                    _this.changeChannelIdx({
                        nextChannelIdx: nextChannelIdx
                    });
                    setFocusChannel();
                }
            }
            else {
                isFocusListArea = true;
                setFocusArea();
                setFocusChannel();
            }

            return true;
        }

        function keyUp () {
            log.printDbg("keyUp()");

            var channelIdx = _this.getChannelIdx();
            var channelLength = _this.getChannelList().length;
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx - 1;
            if (nextPageIdx < 0) {
                nextPageIdx = _this.getChannelMaxPage() - 1;
            }

            if (isFocusListArea) {
                if (getMaxSector() > 1) {
                    var nextChannelIdx = channelIdx - CHANNELS_PER_SECTOR;

                    if (getMaxSector() > SECTOR_PER_PAGE) {
                        if (nextChannelIdx < 0) {
                            nextChannelIdx = ((getMaxSector() - 1) * CHANNELS_PER_SECTOR) + channelIdx;
                            if (nextChannelIdx > channelLength - 1) {
                                nextChannelIdx = channelLength - 1;
                            }

                            moveFocusPage({
                                nextPageIdx: nextPageIdx,
                                nextChannelIdx: nextChannelIdx,
                                isUp: true
                            });
                        }
                        else {
                            if (nextChannelIdx < curPageIdx * CHANNELS_PER_PAGE) {
                                moveFocusPage({
                                    nextPageIdx: nextPageIdx,
                                    nextChannelIdx: nextChannelIdx,
                                    isUp: true
                                });
                            }
                            else {
                                _this.changeChannelIdx({
                                    nextChannelIdx: nextChannelIdx
                                });
                                setFocusChannel();
                            }
                        }
                    }
                    else {
                        if (nextChannelIdx < 0) {
                            nextChannelIdx = ((getMaxSector() - 1) * CHANNELS_PER_SECTOR) + channelIdx;
                            if (nextChannelIdx > channelLength - 1) {
                                nextChannelIdx = channelLength - 1;
                            }
                        }

                        _this.changeChannelIdx({
                            nextChannelIdx: nextChannelIdx
                        });
                        setFocusChannel();
                    }
                }
            }
            else {
                if (_this.getChannelMaxPage() > 1) {
                    var nextChannelIdx = channelIdx - (CHANNELS_PER_PAGE + (channelIdx % CHANNELS_PER_SECTOR));

                    if (nextChannelIdx < 0) {
                        channelIdx = channelLength -1;
                        nextChannelIdx = channelIdx - (channelIdx % CHANNELS_PER_PAGE) ;
                    }

                    moveFocusPage({
                        nextPageIdx: nextPageIdx,
                        nextChannelIdx: nextChannelIdx,
                        isUp: true
                    });
                }
            }
        }

        function keyDown () {
            log.printDbg("keyDown()");

            var channelIdx = _this.getChannelIdx();
            var channelLength = _this.getChannelList().length;
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx + 1;
            if (nextPageIdx >= _this.getChannelMaxPage()) {
                nextPageIdx = 0;
            }
            var maxSector = getMaxSector();

            if (isFocusListArea) {
                if (getMaxSector() > 1) {
                    var nextChannelIdx = channelIdx + CHANNELS_PER_SECTOR;

                    if (getMaxSector() > SECTOR_PER_PAGE) {
                        if (nextChannelIdx > channelLength - 1) {
                            var nextSector = Math.ceil((channelIdx + 1) / CHANNELS_PER_SECTOR);

                            if ((nextSector % SECTOR_PER_PAGE === 0 && nextSector == maxSector - 1) || nextSector > maxSector - 1) {
                                if (nextPageIdx > 0) {
                                    nextChannelIdx = channelLength - 1;
                                }
                                else {
                                    var curSector = Math.floor(channelIdx / CHANNELS_PER_SECTOR);
                                    nextChannelIdx = channelIdx - (CHANNELS_PER_SECTOR * curSector);
                                }

                                moveFocusPage({
                                    nextPageIdx: nextPageIdx,
                                    nextChannelIdx: nextChannelIdx
                                });
                            }
                            else {
                                nextChannelIdx = channelLength - 1;
                                _this.changeChannelIdx({
                                    nextChannelIdx: nextChannelIdx
                                });
                                setFocusChannel();
                            }
                        }
                        else {
                            if (nextChannelIdx >= (curPageIdx + 1) * CHANNELS_PER_PAGE) {
                                moveFocusPage({
                                    nextPageIdx: nextPageIdx,
                                    nextChannelIdx: nextChannelIdx
                                });
                            }
                            else {
                                _this.changeChannelIdx({
                                    nextChannelIdx: nextChannelIdx
                                });
                                setFocusChannel();
                            }
                        }
                    }
                    else {
                        if (nextChannelIdx > channelLength - 1) {
                            var curSector = Math.floor(channelIdx / CHANNELS_PER_SECTOR);
                            if (curSector === getMaxSector() - 1) {
                                nextChannelIdx = channelIdx - (CHANNELS_PER_SECTOR * curSector);
                            }
                            else {
                                nextChannelIdx = channelLength - 1;
                            }
                        }

                        _this.changeChannelIdx({
                            nextChannelIdx: nextChannelIdx
                        });
                        setFocusChannel();
                    }
                }
            }
            else {
                if (_this.getChannelMaxPage() > 1) {
                    var nextChannelIdx = channelIdx + CHANNELS_PER_SECTOR;

                    if (channelIdx % CHANNELS_PER_PAGE < CHANNELS_PER_SECTOR) {
                        if (nextChannelIdx < channelLength) {
                            nextChannelIdx = nextChannelIdx + CHANNELS_PER_SECTOR;
                        }
                    }

                    if (nextChannelIdx > channelLength - 1) {
                        if (nextPageIdx > 0) {
                            nextChannelIdx = channelLength - 1;
                        }
                        else {
                            nextChannelIdx = 0;//nextChannelIdx - channelLength;

                        }
                    }

                    moveFocusPage({
                        nextPageIdx: nextPageIdx,
                        nextChannelIdx: nextChannelIdx
                    });
                }
            }
        }

        function keyRed () {
            log.printDbg("keyRed()");

            var channelIdx = _this.getChannelIdx();
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx - 1;
            if (nextPageIdx < 0) {
                nextPageIdx = _this.getChannelMaxPage() - 1;
            }

            if (isFocusListArea) {
                if (channelIdx % CHANNELS_PER_PAGE === 0 && _this.getChannelMaxPage() > 1) {
                    var nextChannelIdx = nextPageIdx * CHANNELS_PER_PAGE;

                    moveFocusPage({
                        nextPageIdx: nextPageIdx,
                        nextChannelIdx: nextChannelIdx,
                        isUp: true
                    });
                }
                else {
                    var nextChannelIdx = curPageIdx * CHANNELS_PER_PAGE;

                    _this.changeChannelIdx({
                        nextChannelIdx: nextChannelIdx
                    });
                    setFocusChannel();
                }
            }
            else {
                var nextChannelIdx = curPageIdx * CHANNELS_PER_PAGE;

                isFocusListArea = true;

                _this.changeChannelIdx({
                    nextChannelIdx: nextChannelIdx
                });
                setFocusArea();
                setFocusChannel();
            }
        }

        function keyBlue () {
            log.printDbg("keyBule()");

            var channelIdx = _this.getChannelIdx();
            var channelLength = _this.getChannelList().length;
            var curPageIdx = _this.getChannelPage();
            var nextPageIdx = curPageIdx + 1;
            if (nextPageIdx >= _this.getChannelMaxPage()) {
                nextPageIdx = 0;
            }

            if (isFocusListArea) {
                if (((channelIdx % CHANNELS_PER_PAGE === CHANNELS_PER_PAGE - 1) || channelIdx === channelLength - 1) && _this.getChannelMaxPage() > 1) {
                    var nextChannelIdx = (nextPageIdx * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;

                    if (nextChannelIdx > channelLength - 1) {
                        nextChannelIdx = channelLength - 1;
                    }

                    moveFocusPage({
                        nextPageIdx: nextPageIdx,
                        nextChannelIdx: nextChannelIdx
                    });
                }
                else {
                    var nextChannelIdx = (curPageIdx * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;

                    if (nextChannelIdx > channelLength - 1) {
                        nextChannelIdx = channelLength - 1;
                    }

                    _this.changeChannelIdx({
                        nextChannelIdx: nextChannelIdx
                    });
                    setFocusChannel();
                }
            }
            else {
                var nextChannelIdx = (curPageIdx * CHANNELS_PER_PAGE) + CHANNELS_PER_PAGE - 1;

                if (nextChannelIdx > channelLength - 1) {
                    nextChannelIdx = channelLength - 1;
                }

                isFocusListArea = true;

                _this.changeChannelIdx({
                    nextChannelIdx: nextChannelIdx
                });
                setFocusArea();
                setFocusChannel();
            }
        }

        function keyOk () {
            log.printDbg("keyOk()");
            if(isFocusListArea) {
                setFocusChannelTune();
            } else {
                isFocusListArea = true;
                setFocusArea();
                setFocusChannel();
            }
        }

        function setFocusChannelTune () {
            log.printDbg("setFocusChannelTune()");

            _this.parent.mainChannelTune({channel: _this.getCurrentChannel()});
        }

        function showRelatedMenu (_this) {
            log.printDbg("showRelatedMenu()");

            var isShowFavoritedChannel = false;
            var isShowProgramDetail = false;
            var isShowTwoChannel = false;
            var isShowFourChannel = true;
            var isShowHitChannel = true;
            var isShowGenreOption = false;

            var focusOptionIndex = 0;

            var focusMenuId = KTW.managers.data.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL;

            var epgrelatedmenuManager = KTW.managers.service.EpgRelatedMenuManager;

            var curChannel = _this.getCurrentChannel();

            if(navAdapter.isAudioChannel(curChannel) === true) {
                isShowFavoritedChannel = false;
            }

            epgrelatedmenuManager.showFullEpgRelatedMenu(curChannel, function (menuIndex) {
                cbRelateMenuLeft({
                    _this: _this,
                    menuIndex: menuIndex
                });
            }, isShowFavoritedChannel, isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel, isShowGenreOption, focusOptionIndex, function (menuId) {
                cbRelateMenuRight({
                    _this: _this,
                    menuId: menuId
                });
            }, focusMenuId);
        }

        function cbRelateMenuLeft (options) {
            log.printDbg("cbRelateMenuLeft()");

            KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

            if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                // 선호채널 등록 / 해제
                var curChannel = options._this.getCurrentChannel();
                KTW.managers.service.EpgRelatedMenuManager.favoriteChannelOnOff(curChannel);
            }
            else if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                // OTS 자세히 버튼
                // TODO FullEpgProgramDetailPopup 호출해야 함
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                // 4채널
                var locator = KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();

                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                    catId: KTW.managers.data.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL,
                    contsId: "",
                    locator: locator,
                    reqPathCd: ""
                });
            }
            else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
                // 실시간 인기 채널
                //KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();

                options._this.parent.jumpPopularMenu();
            }
        }

        function cbRelateMenuRight (options) {
            log.printDbg("cbRelateMenuRight()");

            KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

            if (options._this.menuId !== options.menuId) {
                var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.id === options.menuId) {
                            return true;
                        }
                    }
                })[0];

                if (epgMenu) {
                    options._this.parent.jumpTargetEpgView({
                        targetMenu: epgMenu
                    });
                }
                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                    catId: KTW.managers.data.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL,
                    contsId: "",
                    locator: "",
                    reqPathCd: ""
                });
            }
        }

        this.create = function () {
            log.printDbg("create()");

            createElement();
        };

        this.getDiv = function () {
            return div;
        };

        this.initData = function () {
            // 채널 리스트, 프로그램 리스트, channelIdx 설정
            this.initEpgData();
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            if (!options || !options.resume) {
                // show
                parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3);

                init();
            }

            this.parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
            
            div.css({visibility: "inherit"});
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});

                if (div.hasClass("focus")) {
                    this.blur();
                }

                listArea.children().remove();
            }
            else {
                listArea.find(".community_page_slide_up_delete, .community_page_slide_up_delete2, .community_page_slide_down_delete, .community_page_slide_down_delete_2").remove();

                var allRemoveClass = "community_page_slide_down_add"
                    + " community_page_slide_up_add"
                    + " community_page_slide_down_add_2"
                    + " community_page_slide_up_middle"
                    //+ " community_page_slide_up_middle2"
                    + " community_page_slide_down_middle"
                    + " community_page_slide_down_middle2";
                    //+ " community_page_slide_down_add_hidden";
                listArea.children().removeClass(allRemoveClass);

                listArea.find(".community_page_slide_up_middle2").removeClass("community_page_slide_up_middle2").addClass("community_page_slide_down_add_hidden");
            }
        };

        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");

            if (isChannelListExist()) {
                scroll.show({
                    maxPage: this.getChannelMaxPage(),
                    curPage: this.getChannelPage()
                });

                if (_this.getChannelMaxPage() <= 1) {
                    listArea.addClass("noScroll");
                    subArea.find("[name=community_channel_sub_area_key_guide_area]").css({visibility: "hidden"});
                }
                else {
                    subArea.find("[name=community_channel_sub_area_key_guide_area]").css({visibility: "inherit"});
                }
            }
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.removeClass("focus");
            if(listArea.hasClass("noScroll")) {
                listArea.removeClass("noScroll");
            }

            isFocusListArea = true;

            scroll.hide();
            listArea.children().css({"-webkit-transform" : "translateY(0px)"});
            this.initData();
            init();
        };

        this.remove = function () {};

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft();
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    // 2017.06.23 dhlee
                    // scroll에 포커스가 있는 상태이더라도 content menu가 동작 해야 한다. (UI 변경 요청 사항 반영)
                    showRelatedMenu(this);
                    /*
                     if (isFocusListArea) {
                     showRelatedMenu(this);
                     }
                     */
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;
        };

        this.isFocusAvailable = function () {
            return isChannelListExist(this);
        };

        this.getDataStatus = function () {
            if (!isChannelListExist(this)) {
                return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
            }
            else {
                return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
            }
        };
    };

    KTW.ui.view.CommunityChannelView.prototype = new KTW.ui.view.EpgView();
    KTW.ui.view.CommunityChannelView.prototype.constructor = KTW.ui.view.CommunityChannelView;

})();