/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgView</code>
 *
 * - 모든 편성표 view 의 부모 class
 * - 채널 / 프로그램 데이터, index 공유 및 관리 (다수의 편성표 view 들이 겹쳐서 보이는 경우는 없으므로)
 *
 * @author dj.son
 * @since 2016-09-01
 */

(function() {
    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var epgDataAdaptor = KTW.ui.adaptor.EpgDataAdaptor;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;
    var epgUtil = KTW.utils.epgUtil;
    var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;
    var GENRE = KTW.nav.Def.CHANNEL.GENRE;

    // TODO 메뉴 타입별로 페이지 별 채널 개수, 프리뷰 별 채널 개수 정의해야 함 (현재 플립북에는 오디오 편성표만 다름)
    var CHANNELS_PER_PAGE = 5;
    var CHANNELS_PER_PREVIEW = 7;

    var IS_TIME_UNIT_SEC = epgUtil.DATA.IS_TIME_UNIT_SEC;

    var channelList = null;
    var programmesList = null;

    var channelIdx = 0;

    var cbChannelListUpdate = null;

    var isGenre = null;

    function channelListUpdateListener (options) {
        log.printDbg("channelListUpdateListener(" + log.stringify(options) + ")");

        initChannelList(options.menuId);

        if (channelIdx > channelList.length - 1) {
            channelIdx = 0;
        }

        setProgrammesList(options.menuId);

        if (cbChannelListUpdate) {
            cbChannelListUpdate(options.listId);
        }
    }

    function setChannelMaxNum (menuId) {
        switch (menuId) {
            case MENU_ID.ENTIRE_CHANNEL_LIST:
            case MENU_ID.MY_FAVORITED_CHANNEL:
            case MENU_ID.OLLEH_TV_CHANNEL:
            case MENU_ID.UHD_CHANNEL:
            case MENU_ID.SKYLIFE_UHD_CHANNEL:
                CHANNELS_PER_PAGE = 6;
                CHANNELS_PER_PREVIEW = 7;
                break;
            case MENU_ID.AUDIO_CHANNEL:
                CHANNELS_PER_PAGE = 6;
                CHANNELS_PER_PREVIEW = 7;
                break;
            case MENU_ID.TV_APP_CHANNEL:
                CHANNELS_PER_PAGE = 9;
                CHANNELS_PER_PREVIEW = 12;
                break;
            case MENU_ID.COMMUNITY_CHANNEL:
                CHANNELS_PER_PAGE = 8;
                CHANNELS_PER_PREVIEW = 16;
                break;
            case MENU_ID.GENRE_TERRESTRIAL:
            case MENU_ID.GENRE_DRAMA:
            case MENU_ID.GENRE_MOVIE:
            case MENU_ID.GENRE_SPORTS:
            case MENU_ID.GENRE_ANIMATION:
            case MENU_ID.GENRE_DOCUMENTARY:
            case MENU_ID.GENRE_NEWS:
            case MENU_ID.GENRE_SOCIAL:
            case MENU_ID.GENRE_RELIGION:
                CHANNELS_PER_PAGE = 5;
                CHANNELS_PER_PREVIEW = 8;
                break;
            case MENU_ID.KIDS_CHANNEL:
                if (KTW.CONSTANT.IS_OTS) {
                    CHANNELS_PER_PAGE = 6;
                    CHANNELS_PER_PREVIEW = 7;
                }
                else {
                    CHANNELS_PER_PAGE = 5;
                    CHANNELS_PER_PREVIEW = 8;
                }
                break;
        }
    }

    /**
     * [dj.son] 메뉴 ID 에 따라 다른 채널 리스트 세팅 (전체 채널, 선호 채널, olleh tv)
     */
    function initChannelList (menuId) {
        log.printDbg("initChannelList()");

        var arrFavId = [], genreCode = null;
        isGenre = false;

        switch (menuId) {
            case MENU_ID.ENTIRE_CHANNEL_LIST:
                // 전체 채널
                if (KTW.CONSTANT.IS_OTS) {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_VIDEO);
                }
                else {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FULL_EPG);
                }
                break;
            case MENU_ID.MY_FAVORITED_CHANNEL:
                // 선호 채널
                if (KTW.CONSTANT.IS_OTS) {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE);
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
                }
                else {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
                }
                break;
            case MENU_ID.OLLEH_TV_CHANNEL:
                // OTS olleh tv 채널 리스트
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_VIDEO);
                break;
            case MENU_ID.AUDIO_CHANNEL: //오디오 채널
                if (KTW.CONSTANT.IS_OTS) {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_AUDIO);
                }
                else {
                    // 2017.05.15 dhlee surfable audio ring 을 이용하도록 한다.
                    //arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_AUDIO);
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SURFABLE_AUDIO);
                }
                break;
            case MENU_ID.COMMUNITY_CHANNEL: // 우리만의 채널
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_CUG);
                break;
            case MENU_ID.TV_APP_CHANNEL: // TV 앱 / 쇼핑 채널
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_TV_APP);
                break;
            case MENU_ID.UHD_CHANNEL: // OTV UHD 채널
            case MENU_ID.SKYLIFE_UHD_CHANNEL: // OTS UHD 채널
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_UHD);
                break;
            case MENU_ID.GENRE_TERRESTRIAL: // 지상파/종편/홈쇼핑
                isGenre = true;
                genreCode = GENRE.TERRESTRIAL;
                break;
            case MENU_ID.GENRE_DRAMA: // 드라마/오락/음악
                isGenre = true;
                genreCode = GENRE.DRAMA;
                break;
            case MENU_ID.GENRE_MOVIE: // 영화/시리즈
                isGenre = true;
                genreCode = GENRE.MOVIE;
                break;
            case MENU_ID.GENRE_SPORTS: // 스포츠/레져
                isGenre = true;
                genreCode = GENRE.SPORTS;
                break;
            case MENU_ID.GENRE_ANIMATION: // 애니/유아/교육
                isGenre = true;
                genreCode = GENRE.ANIMATION;
                break;
            case MENU_ID.GENRE_DOCUMENTARY: // 다큐/교양
                isGenre = true;
                genreCode = GENRE.DOCUMENTARY;
                break;
            case MENU_ID.GENRE_NEWS: // 뉴스/경제
                isGenre = true;
                genreCode = GENRE.NEWS;
                break;
            case MENU_ID.GENRE_SOCIAL: // 공공/공익/정보
                isGenre = true;
                genreCode = GENRE.SOCIAL;
                break;
            case MENU_ID.GENRE_RELIGION: // 종교/오픈
                isGenre = true;
                genreCode = GENRE.RELIGION;
                break;
            case MENU_ID.KIDS_CHANNEL:  // 키즈 채널 편성표
                if (KTW.CONSTANT.IS_OTS) {
                    if (KTW.oipf.AdapterHandler.hwAdapter.enableTuner) {
                        arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_MODE);
                    }
                    else {
                        arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_KIDS_MODE);
                    }
                }
                else {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_KIDS_MODE);
                }
                break;
            case MENU_ID.MOVIE_CHOICE: // OTS 무비 초이스
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_PPV);
                break;
        }

        if (isGenre) {
            channelList = epgDataAdaptor.getGenreChannelList(genreCode);
        }
        else {
            channelList = epgDataAdaptor.getChannelList(arrFavId);
        }
    }

    /**
     * [dj.son] 초기 변수 (index & 시간) 설정
     *
     * channelList 에서 현재 채널을 찾아서 index 설정
     */
    function initIndex (menuId) {
        log.printDbg("initIndex()");

        var curChannel = navAdapter.getCurrentChannel();
        var length = channelList.length;
        var bExist = false;

        // 초기 idx 설정
        channelIdx = 0;

        if (curChannel && curChannel.data) {
            for (var i = 0; i < length; i++) {
                if(!isGenre) {
                    if (curChannel.data.ccid == channelList[i].data.ccid) {
                        channelIdx = i;
                        bExist = true;
                        break;
                    }
                }
            }
        }

        // 2017.04.24 dhlee
        // 현재 채널이 맞춤 프로모 채널인 경우 default promo channel 을 획득하여 index 를 설정한다.
        if (bExist === false && navAdapter.isHiddenPromoChannel(curChannel)) {
            var promoChannel = navAdapter.getPromoChannel();
            for (var i = 0; i < length; i++) {
                if (promoChannel.data.ccid === channelList[i].data.ccid) {
                    channelIdx = i;
                    bExist = true;
                    break;
                }
            }
        }
    }

    /**
     * [dj.son] channelIdx 를 기준으로 편성표 화면에 보여지는 채널의 전체 programme 리스트 세팅
     */
    function setProgrammesList (menuId) {
        log.printDbg("setProgrammesList()");

        var startIdx = null;
        var chLength = channelList.length;
        var tmpChannelList = [];

        if (menuId === MENU_ID.TV_APP_CHANNEL || menuId === MENU_ID.COMMUNITY_CHANNEL || chLength <= 0) {
            programmesList = [];
            return;
        }

        startIdx = Math.floor(channelIdx / CHANNELS_PER_PAGE) * CHANNELS_PER_PAGE;

        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            var curIdx = startIdx + i;

            if(!(channelList[curIdx] === null) && !(channelList[curIdx] === undefined) ) {
                tmpChannelList.push(channelList[curIdx]);
            }
        }

        var startTime = null;
        switch (menuId) {
            case MENU_ID.GENRE_TERRESTRIAL:
            case MENU_ID.GENRE_DRAMA:
            case MENU_ID.GENRE_MOVIE:
            case MENU_ID.GENRE_SPORTS:
            case MENU_ID.GENRE_ANIMATION:
            case MENU_ID.GENRE_DOCUMENTARY:
            case MENU_ID.GENRE_NEWS:
            case MENU_ID.GENRE_SOCIAL:
            case MENU_ID.GENRE_RELIGION:
                startTime = (new Date()).getTime();
                break;
            case MENU_ID.KIDS_CHANNEL:
                if (!KTW.CONSTANT.IS_OTS) {
                    startTime = (new Date()).getTime();
                }
                break;
        }
        programmesList = epgDataAdaptor.getProgrammesList({
            channelList: tmpChannelList,
            startTime: startTime
        });
    }

    /**
     * [dj.son] channelIdx 변경 및 programme 리스트 업데이트
     *
     * @param options nextChannelIdx - nextChannelIdx
     *                 isProgrammesListUpdate - if true, programme 리스트 업데이트
     *                 isUp - nextChannelIdx direction 정보
     */
    function changeChannelIdx (options) {
        log.printDbg("changeChannelIdx(" + log.stringify(options) + ")");

        if (!options || options.nextChannelIdx == null || options.nextChannelIdx == undefined) {
            log.printDbg("options or options.nextChannelIdx is null... so return");
            return;
        }

        if (channelIdx == options.nextChannelIdx) {
            log.printDbg("current channelIdx & options.nextChannelIdx is equal... so return");
            return;
        }

        var chLength = channelList.length;
        if (options.nextChannelIdx < 0 || options.nextChannelIdx > chLength - 1) {
            log.printDbg("options.nextChannelIdx is out of range... so return");
            return;
        }

        channelIdx = options.nextChannelIdx;

        if (options.isProgrammesListUpdate) {
            setProgrammesList(options.menuId);
        }
    }

    /**
     * [dj.son] 특정 idx 가 포함된 channel list page 구성 후 리턴
     */
    function makeChannelSectionByIndex (idx) {
        log.printDbg("makeChannelSectionByIndex(" + idx + ")");

        var chList = [];
        var startIdx = Math.floor(idx / CHANNELS_PER_PAGE) * CHANNELS_PER_PAGE;

        for (var i = 0; i < CHANNELS_PER_PREVIEW; i++) {
            chList.push(channelList[startIdx + i]);
        }

        return chList;
    }

    /**
     * [dj.son] preview 화면의 프로그램 리스트 리턴
     */
    function makePreviewPrgrammesList () {
        log.printDbg("makePreviewPrgrammesList()");

        var arrResult = [];
        var curTime = (new Date()).getTime();
        if (IS_TIME_UNIT_SEC) {
            curTime = Math.floor(curTime / 1000);
        }

        var progList = null;
        var progLength = null;
        var programme = null;

        for (var i = 0; i < programmesList.length; i++) {
            progList = programmesList[i];
            progLength = progList.length;
            for (var j = 0; j < progLength; j++) {
                programme = progList[j];

                if (programme.startTime > curTime) {
                    break;
                }

                if (programme.startTime + programme.duration < curTime) {
                    continue;
                }

                arrResult[i] = programme;
                break;
            }
        }

        return arrResult;
    }

    /**
     * [dj.son] 특정 시간대의 programme list 구성 후 리턴
     */
    function makeProgrammesSectionByTime (time) {
        log.printDbg("makeProgrammesSectionByTime()");

        var arrProgrammes = [];

        var programmesListLength = programmesList.length;
        for (var i = 0; i < programmesListLength; i++) {
            arrProgrammes.push(epgDataAdaptor.makeProgrammesSection({
                programmesList: programmesList[i],
                referenceTime: time
            }));
        }

        return arrProgrammes;
    }

    KTW.ui.view.EpgView = function(options) {
        this.parent = options ? options.parent : null;
        this.menuId = options ? options.menuId : null;

        this.initEpgData = function () {
            log.printDbg("initEpgData()");

            epgDataAdaptor.init();

            setChannelMaxNum(this.menuId);

            // 채널 리스트 설정
            initChannelList(this.menuId);

            // 초기 변수 설정
            initIndex(this.menuId);

            // 초기 프로그램 리스트 설정
            setProgrammesList(this.menuId);
        };

        this.updateProgrammeList = function () {
            log.printDbg("updateProgrammeList()");

            setProgrammesList(this.menuId);
        };

        this.setCallbackChannelListUpdate = function (callback) {
            var menuId = this.menuId;

            if (callback) {
                epgDataAdaptor.setCallbackChannelListUpdate(function (listId) {
                    channelListUpdateListener({
                        listId: listId,
                        menuId: menuId
                    });
                });
            }
            else {
                epgDataAdaptor.setCallbackChannelListUpdate(null);
            }

            cbChannelListUpdate = callback;
        };

        // get
        this.getChannelList = function () {
            return channelList;
        };
        this.getChannelSectionByIndex = function (idx) {
            if (idx === null || idx === undefined) {
                idx = channelIdx;
            }

            return makeChannelSectionByIndex(idx);
        };
        this.getProgrammesList = function (forced) {
            if (forced) {
                setProgrammesList(this.menuId);
            }

            return programmesList.slice();
        };
        this.getProgrammesSectionByTime = function (time) {
            if (!time) {
                time = epgUtil.reviseDate(new Date());
            }

            return makeProgrammesSectionByTime(time);
        };
        this.getPreviewProgrammesList = function () {
            return makePreviewPrgrammesList();
        };
        this.getChannelIdx = function () {
            return channelIdx;
        };
        this.getChannelPage = function () {
            return Math.floor(channelIdx / CHANNELS_PER_PAGE);
        };
        this.getChannelMaxPage = function () {
            return Math.ceil(channelList.length / CHANNELS_PER_PAGE);
        };

        this.changeChannelIdx = function (options) {
            options.menuId = this.menuId;
            changeChannelIdx(options);
        };

        this.getCurrentChannel = function () {
            if (channelList) {
                return channelList[channelIdx];
            }
        }
    }

    Object.defineProperty(KTW.ui.view.EpgView, "DATA_STATUS", {
        value: {
            UPDATE: 0,
            NO_FAVOURITE: 1,
            NO_RESERVATION: 2,
            AVAILABLE: 3,
            WEAK_SIGNAL: 4,
            SKIPPED: 5
        },
        writable: false,
        configurable: false
    });
})();