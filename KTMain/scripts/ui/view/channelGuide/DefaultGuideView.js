/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>GenreChannelView</code>
 *
 * @author sw.nam
 * @since 2017-01-04
 *
 * 장르별 채널(otv) & 프로그램 검색(ots)
 */

(function() {
    KTW.ui.view.DefaultGuideView = function (options) {
        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var parent = options.parent;
        var div = null;
        var menuId = null;
        var dataStatus = null;

        function createElement() {
            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "default_guide_view",
                    css: {
                        position: "absolute", left: 576, top: 0, width: 1344, height: 1080
                    }
                },
                parent: parent.getEpgContainerDiv()
            });



            util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "up_line",
                    css: {
                        position: "absolute", left: (636-576), top: 122, width: 1130, height: 5,display:"none",
                        "background-color": "rgba(178, 177, 177, 0.25)"
                    }
                },
                parent: div
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "down_line",
                    css: {
                        position: "absolute", left: (636-576), top: 953, width: 1130, height: 5,display:"none",
                        "background-color": "rgba(178, 177, 177, 0.25)"
                    }
                },
                parent: div
            });

             util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "default_no_result_icon",
                    src: "images/icon/icon_noresult.png",
                    css: {
                        position: "absolute", width:82, height:76,display:"none"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "default_warning_icon",
                    src: "images/icon/icon_warning.png",
                    css: {
                        position: "absolute", left:(972 - 576) ,top:421 , width:80, height:80,display:"none"
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    name: "default_guide_message_1",
                    css: {
                        position: "absolute", "text-align": "center",
                        "font-size": 33, color: "rgba(255, 255, 255, 0.7)", "letter-spacing": -1.65,
                        "white-space": "pre"
                    }
                },
                parent: div
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    name: "default_guide_message_2",
                    css: {
                        position: "absolute", left: (693-576), top: 537, width: 780, height:26 , "text-align": "left",
                        "font-size": 24, color: "rgba(255, 255, 255, 0.5)", "letter-spacing": -1.2,display:"none"
                    }
                },
                parent: div
            });


            var defaultBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "default_guide_btn",
                    css: {
                        position: "absolute", left: (889-576), top: 608, width: 248, height: 66,
                        "background-color": "rgba(255, 255, 255, 0.15)" , display : "none"
                    }
                },
                parent: div
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 19, width: 248, "text-align": "center",
                        "font-size": 30, "letter-spacing": -1.5, "font-family": "RixHead L",
                        color: "rgba(255, 255, 255, 0.7)"
                    }
                },
                parent: defaultBtn
            });
        }

        function draw () {
            log.printDbg("draw()");

            if (dataStatus === KTW.ui.view.EpgView.DATA_STATUS.NO_FAVOURITE) {
                parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2);

                div.find("[name=up_line]").css("display" , "none");
                div.find("[name=down_line]").css("display" , "none");

                div.find("[name=default_warning_icon]").css("display" , "none");

                div.find("[name=default_no_result_icon]").css({left:(972-576) , top: 384});
                div.find("[name=default_no_result_icon]").css("display" , "");

                div.find("[name=default_guide_message_1]").css({left:0 , width:873 , top : 488}).text("등록된 선호 채널이 없습니다");
                div.find("[name=default_guide_message_2]").text("설정>채널/VOD 설정>선호 채널 메뉴에서 선호 채널 등록이 가능합니다");
                div.find("[name=default_guide_message_2]").css("display" , "");

                div.find("[name=default_guide_btn]").css("display" , "");
                div.find("[name=default_guide_btn] span").text("선호 채널 등록");

                /**
                 *  jjh1117 2018/01/08
                 *  WEBIIIHOME-3633 이슈 사항 수정내용 원복함.
                 */
                div.removeClass("focus");
                div.find("[name=default_guide_btn]").css({"background-color": "rgba(255, 255, 255, 0.15)"});
                div.find("[name=default_guide_btn] div").css({visibility: "hidden"});
                div.find("[name=default_guide_btn] span").css({color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead L"});
            }
            else {
                div.find("[name=default_guide_message_2]").css("display" , "none");
                div.find("[name=default_guide_btn]").css("display" , "none");
                div.find("[name=up_line]").css("display" , "none");
                div.find("[name=down_line]").css("display" , "none");
                div.find("[name=default_warning_icon]").css("display" , "none");
                div.find("[name=default_warning_icon]").css("display" , "none");

                var message = "채널 편성 정보를 업데이트하고 있습니다";
                if (dataStatus === KTW.ui.view.EpgView.DATA_STATUS.NO_RESERVATION) {
                    message = "예약한 프로그램 목록이 없습니다";

                    parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R3);

                    div.find("[name=up_line]").css("display" , "");
                    div.find("[name=down_line]").css("display" , "");


                    div.find("[name=default_no_result_icon]").css({left:(1160-576) , top: 459});
                    div.find("[name=default_no_result_icon]").css("display" , "");

                    div.find("[name=default_guide_message_1]").css({left:60 , width:1130 , top : 573});
                }
                else if (dataStatus === KTW.ui.view.EpgView.DATA_STATUS.WEAK_SIGNAL) {
                    message = "방송 신호가 고르지 못해\n편성정보를 불러올 수 없습니다";

                    parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2);

                    div.find("[name=default_no_result_icon]").css("display" , "none");
                    div.find("[name=default_warning_icon]").css("display" , "");

                    div.find("[name=default_guide_message_1]").css({left:0 , width:873 , top : 528});
                    div.find("[name=default_guide_message_1]").css({"line-height": 1.5});
                }
                else if (dataStatus === KTW.ui.view.EpgView.DATA_STATUS.SKIPPED) {
                    message = "채널 정보가 없습니다";

                    parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2);

                    div.find("[name=default_no_result_icon]").css({left:(972-576) , top: 423});
                    div.find("[name=default_no_result_icon]").css("display" , "");

                    div.find("[name=default_guide_message_1]").css({left:0 , width:873 , top : 528});
                }
                else {
                    parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2);

                    div.find("[name=default_no_result_icon]").css({left:(972-576) , top: 423});
                    div.find("[name=default_no_result_icon]").css("display" , "");

                    switch (menuId) {
                        case KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL:
                            message = "오디오 채널 정보를 업데이트하고 있습니다";
                            break;
                        case KTW.managers.data.MenuDataManager.MENU_ID.COMMUNITY_CHANNEL:
                            message = "우리만의 채널 정보를 업데이트하고 있습니다";
                            break;
                        case KTW.managers.data.MenuDataManager.MENU_ID.TV_APP_CHANNEL:
                            message = "TV 앱/쇼핑 채널 정보를 업데이트하고 있습니다";
                            break;
                        case KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL:
                            message = "선호 채널 편성 정보를 업데이트하고 있습니다";
                            break;
                    }

                    div.find("[name=default_guide_message_1]").css({left:0 , width:873 , top : 528});

                }
                div.find("[name=default_guide_message_1]").text(message);
            }
        }

        function keyOk () {
            log.printDbg("keyOk()");

            if (dataStatus === KTW.ui.view.EpgView.DATA_STATUS.NO_FAVOURITE) {
                KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                    menuId: KTW.managers.data.MenuDataManager.MENU_ID.SETTING_FAVORITE_CHANNEL,
                    updateSubTitle: false
                });
            }
        }

        function checkFavouriteListUpdate () {
            log.printDbg("checkFavouriteListUpdate()");

            if (menuId === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                var arrFavId = [];
                if (KTW.CONSTANT.IS_OTS) {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE);
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
                }
                else {
                    arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
                }
                var channelList = KTW.ui.adaptor.EpgDataAdaptor.getChannelList(arrFavId);

                if (channelList && channelList.length > 0) {
                    parent.setFocusGuideMenu();

                    var favMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL) {
                                return true;
                            }
                        }
                    })[0];
                    parent.setEpgView({menu: favMenu});
                }
            }
        }

        this.create = function () {
            log.printDbg("create()");

            createElement();
        };

        this.initData = function (options) {
            log.printDbg("initData(" + log.stringify(options) + ")");

            menuId = options.menuId;
            this.menuId = options.menuId;
            dataStatus = options.dataStatus;


            draw();
        };

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            if (!options || !options.resume) {
                div.css({visibility: "inherit"});
            }
            else {
                checkFavouriteListUpdate();
            }

            parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});
            }
        };

        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");
            div.find("[name=default_guide_btn]").css({"background-color": "rgba(210, 51, 47, 1)"});
            div.find("[name=default_guide_btn] div").css({visibility: "inherit"});
            div.find("[name=default_guide_btn] span").css({color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M"});
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.removeClass("focus");
            div.find("[name=default_guide_btn]").css({"background-color": "rgba(255, 255, 255, 0.15)"});
            div.find("[name=default_guide_btn] div").css({visibility: "hidden"});
            div.find("[name=default_guide_btn] span").css({color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead L"});
        };

        this.remove = function () {
            log.printDbg("remove()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.OK:
                    keyOk();
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return false;
        };

        this.isFocusAvailable = function () {
            return dataStatus === KTW.ui.view.EpgView.DATA_STATUS.NO_FAVOURITE;
        };

        this.getDataStatus = function () {
            return true;
        };

    };

    Object.defineProperty(KTW.ui.view.DefaultGuideView, "TYPE", {
        value: {
            UPDATE_CHANNEL: "UPDATE_CHANNEL",
            NO_FAVOURITE_CHANNEL: "NO_FAVOURITE_CHANNEL",
            NO_RESERVATION_PROGRAMME: "NO_RESERVATION_PROGRAMME"
        },
        writable: false,
        configurable: false
    });


})();