/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>AudioChannelFullEpgView.js</code>
 *
 * 채널 상에서 연령제한, 미가입, 제한 채널 등의 상황이 발생했을 때 background에 보여지는 image를 제공한다.
 * OTS 무비초이스 채널의 경우 무비초이스 채널의 기본 정보를 Background image 위에 보여준다.
 * Audio 채널의 경우 곡 정보 등을 보여준다.
 * VOD Loading 배경 이미지를 보여준다.
 *
 * @author dhlee
 * @since 2016-07-26
 */

"use strict";

KTW.ui.view.AudioChannelFullEpgView = function AudioChannelFullEpgView() {
    KTW.ui.View.call(this);
    var IMAGE_PATH = KTW.CONSTANT.IMAGE_PATH;
    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var audioFullEpgDiv = null;
    var channelListShadowRootDiv = null;
    var channelListRootDiv = null;
    var scrollArea = null;
    var indicator = null;

    var contextMenuDiv = null;
    var pageKeyGuideDiv = null;

    var maxColumn = 3;
    var maxRow = 5;

    var channelListColumn = [];
    
    var totalPage = 0;
    var curPage = 0;
    var channelList = null;

    var tuneAudioChannel = null;

    var focusX = 0;
    var focusY = 0;
    var isScrollFocus = false;

    var isChannelListShow = false;
    var isContextMenuShow = false;
    var isPageKeyGuideShow = false;
    var isJump = false;

    var callbackFuncRelateMenuRight = null;

    var parent_view = null;

    function _hideAll () {
        for(var i=0;i<maxColumn;i++) {
            var rowList = channelListColumn[i];
            if(rowList !== null && rowList.length>0) {
                for(var j=0;j<maxRow;j++) {
                    var channelItem = rowList[j];
                    /**
                     *  root_div : rootDiv,
                     *  default_channel_number_text :  defaultChannelNumberText,
                     *  default_channel_name_text :  defaultChannelNameText,
                     *  focus_div : focusDiv,
                     *  focus_channel_number_text :  focusChannelNumberText,
                     *  focus_channel_name_text :  focusChannelNameText,
                     *  cd_img : cdImg
                     */

                    channelItem.default_channel_number_text.text("");
                    channelItem.focus_channel_number_text.text("");
                    channelItem.default_channel_name_text.text("");
                    channelItem.focus_channel_name_text.text("");

                    channelItem.default_channel_number_text.css("color" , "rgba(255,255,255,0.5)");
                    channelItem.default_channel_name_text.css("color" , "rgba(255,255,255,0.5)");


                    channelItem.default_channel_number_text.css("display" , "none");
                    channelItem.focus_channel_number_text.css("display" , "none");
                    channelItem.default_channel_name_text.css("display" , "none");
                    channelItem.focus_channel_name_text.css("display" , "none");

                    channelItem.focus_div.css("display" , "none");
                    channelItem.cd_img.css("display" , "none");
                }
            }
        }
    }

    function _moveUpDown(isUp) {
        _unFocus();
        if(isUp) {
            focusY--;
            var max = (maxColumn*maxRow);

            if(focusY<0) {
                curPage--;
                if(curPage<=0) {
                    curPage = totalPage;
                    max = channelList.length - ((curPage-1)*(maxColumn*maxRow));

                    var tempX = Math.ceil( max / maxColumn);
                    if(tempX>0) {
                        tempX-=1;
                        focusY = tempX;
                    }

                    var tempX2 = max - (focusY*maxColumn);
                    tempX2 -=1;
                    if(focusX>tempX2) {
                        focusX = tempX2;
                    }
                }else {
                    focusY = 4;
                }
                _setPageFocus();
            }else {
                _setFocus();
            }

        }else {
            focusY++;
            var max = (maxColumn*maxRow);
            if(curPage === totalPage) {
                max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
            }

            if(((focusY*maxColumn) + (focusX + 1)) > max) {
                focusY=0;
                curPage++;
                if(curPage>totalPage) {
                    curPage = 1;
                }
                if(curPage === totalPage) {
                    max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
                    if(((focusY*maxColumn) + (focusX + 1)) > max) {
                        focusX = (max - (focusY*maxColumn) ) - 1;
                    }
                }
                _setPageFocus();
            }else {
                _setFocus();
            }
        }
    }
    
    function _moveLeftRight(isLeft) {
        if(isLeft) {
            if(focusX === 0){
                return false;
            }
            _unFocus();

            focusX--;
            _setFocus();
        }else {
            _unFocus();
            focusX++;
            if(focusX>=maxColumn) {
                focusY++;
                focusX=0;
            }
            
            var max = (maxColumn*maxRow);
            if(curPage === totalPage) {
                max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
            }

            if(((focusY*maxColumn) + (focusX + 1)) > max) {
                /**
                 * 한 페이지의 마지막에 포커스가 온 경우
                 * 다음 페이지 이동 함.
                 */
                focusX=0;
                focusY=0;
                curPage++;
                if(curPage>totalPage) {
                    curPage=1;
                }
                _setPageFocus();
            }else {
                _setFocus();
            }
        }
        return true;
    }
    
    function _unFocus() {
        var columnList = channelListColumn[focusX];
        var chItem = columnList[focusY];

        chItem.default_channel_number_text.css("display" , "");
        chItem.focus_channel_number_text.css("display" , "none");
        chItem.default_channel_name_text.css("display" , "");
        chItem.focus_channel_name_text.css("display" , "none");

        if(chItem.cd_img.css("display") !== "none") {
            chItem.default_channel_number_text.css("color" , "rgba(255,255,255,1)");
            chItem.default_channel_name_text.css("color" , "rgba(255,255,255,1)");
        }

        chItem.focus_div.css("display" , "none");
    }

    function _setFocus() {
        var columnList = channelListColumn[focusX];
        var chItem = columnList[focusY];

        chItem.default_channel_number_text.css("display" , "none");
        chItem.focus_channel_number_text.css("display" , "");
        chItem.default_channel_name_text.css("display" , "none");
        chItem.focus_channel_name_text.css("display" , "");

        chItem.focus_div.css("display" , "");

        var focusIndex = ((curPage-1)*(maxColumn*maxRow)) +  ((focusY*maxColumn) + focusX);

        var ch = channelList[focusIndex];
        
        KTW.managers.service.AudioIframeManager.setAudioFocusChannel(ch , true);
    }

    function _setPageFocus(isPageScroll) {
        /**
         * 먼저, fcousX , focusY는 정한 후에 setPageFocus를 호출 해야 함.
         */
        _hideAll();

        var max = (maxColumn*maxRow);
        if(totalPage === curPage) {
            max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
        }

        var j=0;
        var row=0;
        for(var i=0;i<max;i++) {
            var ch = channelList[((curPage-1)*(maxColumn*maxRow)) + i];
            var columnList = channelListColumn[j];

            if(columnList != null && columnList.length>0) {
                var chItem = columnList[row];
                /**
                 *  root_div : rootDiv,
                 *  default_channel_number_text :  defaultChannelNumberText,
                 *  default_channel_name_text :  defaultChannelNameText,
                 *  focus_div : focusDiv,
                 *  focus_channel_number_text :  focusChannelNumberText,
                 *  focus_channel_name_text :  focusChannelNameText,
                 *  cd_img : cdImg
                 */

                var chNum = util.numToStr(ch.majorChannel, 3);
                chItem.default_channel_number_text.text(chNum);
                chItem.focus_channel_number_text.text(chNum);
                chItem.default_channel_name_text.text(ch.name);
                chItem.focus_channel_name_text.text(ch.name);

                chItem.default_channel_number_text.css("display" , "");
                chItem.focus_channel_number_text.css("display" , "none");
                chItem.default_channel_name_text.css("display" , "");
                chItem.focus_channel_name_text.css("display" , "none");

                chItem.focus_div.css("display" , "none");

                if(isPageScroll === undefined || isPageScroll === null) {
                    if(j === focusX && row === focusY) {
                        chItem.default_channel_number_text.css("display" , "none");
                        chItem.default_channel_name_text.css("display" , "none");
                        chItem.focus_channel_number_text.css("display" , "");
                        chItem.focus_channel_name_text.css("display" , "");

                        chItem.focus_div.css("display" , "");

                        var focusIndex = ((curPage-1)*(maxColumn*maxRow)) +  ((focusY*maxColumn) + focusX);

                        var ch = channelList[focusIndex];

                        KTW.managers.service.AudioIframeManager.setAudioFocusChannel(ch , true);
                    }
                }

                if(tuneAudioChannel !== null) {
                    if(ch.ccid === tuneAudioChannel.ccid) {
                        chItem.cd_img.css("display" , "");

                        chItem.default_channel_number_text.css("color" , "rgba(255,255,255,1)");
                        chItem.default_channel_name_text.css("color" , "rgba(255,255,255,1)");
                    }
                }
            }

            j++;
            if(j>=maxColumn) {
                j = 0;
                row++;
            }
        }
        indicator.setCurrentPage(curPage - 1);
        if(isPageScroll === undefined || isPageScroll === null) {
            indicator.setFocus(false);
        }
    }

    function _setScrollUpDown(isUp) {

        if(isUp==true) {
            curPage--;
        }else {
            curPage++;
        }

        if(curPage<=0) {
            curPage = totalPage;
        }else if(curPage>totalPage) {
            curPage = 1;
        }

        focusX = 0;
        focusY = 0;

        _setPageFocus(true);
    }

    function _setScrollPageUpDown(isRed) {
        if(isScrollFocus === true) {
            if(isRed === true) {
                focusX = 0;
                focusY = 0;
            }


        }else {

        }

    }

    function _showChannelList() {
        log.printDbg("_showChannelList() isChannelListShow : " + isChannelListShow);

        /**
         * 스크롤은 여기서 같이 처리 해야 함.
         */
        if(isChannelListShow === false) {
            channelListShadowRootDiv.css("display" , "");
            channelListRootDiv.css("display" , "");
            scrollArea.css("display" , "");
            isChannelListShow = true;
            isScrollFocus = false;
            indicator.setFocus(false);
        }
    }

    function _hideChannelList() {
        log.printDbg("_hideChannelList() isChannelListShow : " + isChannelListShow);
        /**
         * 스크롤은 여기서 같이 처리 해야 함.
         */
        if(isChannelListShow === true) {
            channelListShadowRootDiv.css("display" , "none");
            channelListRootDiv.css("display" , "none");
            scrollArea.css("display" , "none");
            isChannelListShow = false;
            isScrollFocus = false;
            indicator.setFocus(false);
        }
    }

    function _showContextMenu() {
        log.printDbg("_showContextMenu() isContextMenuShow : " + isContextMenuShow);
        if(isContextMenuShow === false) {
            contextMenuDiv.css("display" , "");
            isContextMenuShow = true;
        }
    }

    function _hideContextMenu() {
        log.printDbg("_hideContextMenu() isContextMenuShow : " + isContextMenuShow);

        if(isContextMenuShow === true) {
            contextMenuDiv.css("display" , "none");
            isContextMenuShow = false;
        }
    }


    function _showPageKeyGuide() {
        log.printDbg("_showPageKeyGuide() isPageKeyGuideShow : " + isPageKeyGuideShow);
        if(isPageKeyGuideShow === false) {
            pageKeyGuideDiv.css("display" , "");
            isPageKeyGuideShow = true;
        }
    }

    function _hidePageKeyGuide() {
        log.printDbg("_hidePageKeyGuide() isPageKeyGuideShow : " + isPageKeyGuideShow);

        if(isPageKeyGuideShow === true) {
            pageKeyGuideDiv.css("display" , "none");
            isPageKeyGuideShow = false;
        }
    }



    function _focusChannelTune() {
        log.printDbg("_focusChannelTune() ");
        var focusIndex = ((curPage-1)*(maxColumn*maxRow)) +  ((focusY*maxColumn) + focusX);

        log.printDbg("_focusChannelTune() curPage : " + curPage + " , focusY : " + focusY + " , focusX : " + focusX + " , focusIndex : " + focusIndex);

        var ch = channelList[focusIndex];
        if(ch !== undefined && ch !== null) {
            log.printDbg("_focusChannelTune() ch : " + JSON.stringify(ch));
            log.printDbg("_focusChannelTune() tuneAudioChannel : " + JSON.stringify(tuneAudioChannel));
            var mainControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);

            if(tuneAudioChannel === null) {

                if (KTW.managers.service.StateManager.isVODPlayingState()) {
                    KTW.managers.module.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                        if (vodModule) {
                            vodModule.execute({
                                method: "stopVodWithChannelSelection",
                                params: {
                                    channelObj: ch
                                }
                            });
                        }
                    });
                }
                else {
                    tuneAudioChannel = ch;
                    mainControl.changeChannel(ch, true);
                    //KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , tuneAudioChannel);
                }
            }else {
                if(ch.ccid !== tuneAudioChannel.ccid) {
                    mainControl.changeChannel(ch, true);
                    tuneAudioChannel = ch;
                    KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG , tuneAudioChannel);
                }else {
                    KTW.ui.LayerManager.clearNormalLayer();
                }
            }
        }
    }

    function _showRelatedMenu () {
        log.printDbg("showRelatedMenu()");

        var isShowFavoritedChannel = false;
        var isShowProgramDetail = false;
        var isShowTwoChannel = false;
        var isShowFourChannel = true;
        var isShowHitChannel = true;
        var isShowGenreOption = false;
        var focusOptionIndex = 0;
        var focusMenuId = KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL;

        var epgRelatedMenuManager = KTW.managers.service.EpgRelatedMenuManager;


        epgRelatedMenuManager.showFullEpgRelatedMenu(null ,_callbackRelateMenuLeft , isShowFavoritedChannel
            , isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel
            , isShowGenreOption, focusOptionIndex, _callbackRelateMenuRight , focusMenuId);
    }

    function _callbackRelateMenuLeft (menuIndex) {
        log.printDbg("_callbackRelateMenuLeft()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if(menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
            // 4채널
            var locator = KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();

            //통계로그 추가
            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.JUMP_TO,
                act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                catId: KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL,
                contsId: "",
                locator: locator,
                reqPathCd: ""
            });



        }
        else if(menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
            if(parent_view !== null) {
                if(tuneAudioChannel === null) {
                    KTW.ui.LayerManager.deactivateLayer({id:KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
                    parent_view.jumpPopularMenu();
                }else {
                    KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();
                }
            }else {
                KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();
            }
        }


    }

    function _callbackRelateMenuRight (menuId) {
        log.printDbg("cbRelateMenuRight() menuId : " + menuId);



        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if (menuId !== KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL) {

            if(callbackFuncRelateMenuRight !== null) {
                //선호채널 체크 후에 등록된 선호채널이 없는 경우에는 deactivateLayer를 호출 하면 안됨.
                if(menuId === KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL
                    && KTW.managers.service.FavoriteChannelManager.isFavoriteChannelListExist() === false) {
                }else {
                    KTW.ui.LayerManager.deactivateLayer({id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
                }
                callbackFuncRelateMenuRight(menuId);
            }else {
                var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData   : KTW.managers.data.MenuDataManager.getMenuData(),
                    cbCondition: function (menu) {
                        if (menu.id === menuId) {
                            return true;
                        }
                    }
                })[0];



                if(epgMenu !== null) {

                    KTW.managers.service.MenuServiceManager.jumpMenu({
                        menu      : epgMenu,
                        jump      : true,
                        skipRequestShow: false
                    });

                }

            }
            //통계로그 추가
            KTW.managers.UserLogManager.collect({
                type     : KTW.data.UserLog.TYPE.JUMP_TO,
                act      : KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                jumpType : KTW.data.EntryLog.JUMP.TYPE.EPG,
                catId    : KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL,
                contsId  : "",
                locator  : "",
                reqPathCd: ""
            });
        }
    }

    function onServiceStateChange (options) {
        log.printDbg("onServiceStateChange() options.nextServiceState : " + options.nextServiceState );
        if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED ) {
            if(tuneAudioChannel === null) {
                KTW.ui.LayerManager.deactivateLayer({id:KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
            }
            return;
        }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
            // if(tuneAudioChannel === null) {
            //     KTW.ui.LayerManager.deactivateLayer({id:KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
            // }
            // return;
        }
    }


    this.createView = function() {
        log.printDbg("createView()");

        this.parent.div.attr({class: "arrange_frame"});

        audioFullEpgDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 0, top: 357,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 723
                }
            },
            parent: this.parent.div
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "audio_channel_fullepg_background",
                src: IMAGE_PATH + "audio_full_epg/shadow_audio_channel.png",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH,
                    height: 723
                }
            },
            parent: audioFullEpgDiv
        });

        channelListShadowRootDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 109, top: (460-357),
                    width: 1782, height: 596,opacity:0.65 , display : "none"
                }
            },
            parent: audioFullEpgDiv
        });

        var leftPos = 0;
        for(var i=0;i<maxColumn;i++) {
            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "ch_list_" + (i+1) + "_shw_l",
                    src: IMAGE_PATH + "audio_full_epg/audio_ch_box_shadow_l.png",
                    css: {
                        position: "absolute",
                        left: leftPos,
                        top: 0,
                        width: 166,
                        height: 596
                    }
                },
                parent: channelListShadowRootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "ch_list_" + (i+1) + "_shw_m",
                    src: IMAGE_PATH + "audio_full_epg/audio_ch_box_shadow_m.png",
                    css: {
                        position: "absolute",
                        left: (leftPos + 166),
                        top: 0,
                        width: 335,
                        height: 596
                    }
                },
                parent: channelListShadowRootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "ch_list_" + (i+1) + "_shw_r",
                    src: IMAGE_PATH + "audio_full_epg/audio_ch_box_shadow_r.png",
                    css: {
                        position: "absolute",
                        left: (leftPos + 166+335),
                        top: 0,
                        width: 165,
                        height: 596
                    }
                },
                parent: channelListShadowRootDiv
            });

            for(var j=0;j<maxRow;j++) {
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute", left: ((175-109) + (i*558)), top: ((496-460) + (j*93)),
                            width: 535, height: 92 , "background-color": "rgba(47,42,38,1)"
                        }
                    },
                    parent: channelListShadowRootDiv
                });
            }
            leftPos += 558;
        }


        channelListRootDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {

                    position: "absolute", left: 109, top: (460-357),
                    width: 1782, height: 596, display : "none"
                }
            },
            parent: audioFullEpgDiv
        });

        leftPos = 4;
        for(var i=0;i<maxColumn;i++) {
            var channelListRow = [];
            for(var j=0;j<maxRow;j++) {
                var rootDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute", left: ((175-109) + (i*558)), top: ((496-460) + (j*93)),
                            width: 535, height: 92
                        }
                    },
                    parent: channelListRootDiv
                });

                var defaultChannelNumberText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_default_channel_number",
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 45, top: 32, width: 54 , height: 36,
                            color: "rgba(255, 255, 255, 0.5)", "font-size": 34,"text-align" : "left", "letter-spacing":-1.7
                        }
                    },
                    text: "",
                    parent: rootDiv
                });

                var defaultChannelNameText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_default_channel_name",
                        class: "font_m cut" ,
                        css: {
                            position: "absolute", left: 127, top: 33, width: 303 , height: 32,
                            color: "rgba(255, 255, 255, 0.5)", "font-size": 30,"text-align" : "left", "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: rootDiv
                });


                var focusDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        class : "focus_red_border_box",
                        css: {
                            position: "absolute", left: 0, top: 0,
                            width: 535, height: 92 , "background-color": "rgba(40,39,38,1)" , opacity:1 , display:"none"
                        }
                    },
                    parent: rootDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        class : "focus_black_border_box",
                        css: {
                            position: "absolute", left: 0, top: 0,
                            width: (535 - 12), height: (92 - 12)
                        }
                    },
                    parent: focusDiv
                });

                var focusChannelNumberText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_focus_channel_number",
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 45, top: 31, width: 54 , height: 36,
                            color: "rgba(255, 255, 255, 1)", "font-size": 34,"text-align" : "left", "letter-spacing":-1.7 , display:"none"
                        }
                    },
                    text: "",
                    parent: rootDiv
                });

                var focusChannelNameText = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_focus_channel_name",
                        class: "font_m cut" ,
                        css: {
                            position: "absolute", left: 127, top: 31, width: 303 , height: 34,
                            color: "rgba(255, 255, 255, 1)", "font-size": 32,"text-align" : "left", "letter-spacing":-1.6 , display:"none"
                        }
                    },
                    text: "",
                    parent: rootDiv
                });




                var cdImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "cd_image",
                        src: IMAGE_PATH + "audio_full_epg/img_audio_guide_2_sm_w68.png",
                        css: {
                            position: "absolute",
                            left: 437,
                            top: 12,
                            width: 68,
                            height: 68,
                            display : "none"
                        }
                    },
                    parent: rootDiv
                });


                var obj = {
                    root_div : rootDiv,
                    default_channel_number_text :  defaultChannelNumberText,
                    default_channel_name_text :  defaultChannelNameText,
                    focus_div : focusDiv,
                    focus_channel_number_text :  focusChannelNumberText,
                    focus_channel_name_text :  focusChannelNameText,
                    cd_img : cdImg
                };

                channelListRow[channelListRow.length] = obj;
            }
            channelListColumn[channelListColumn.length] = channelListRow;

            leftPos+=381;
        }


        scrollArea = util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 99, top: (523-357), overflow: "visible"
                }
            },
            parent: audioFullEpgDiv
        });

        indicator = new KTW.ui.component.Scroll({
            parentDiv: scrollArea,
            height: 416,
            forceHeight: true,
            isAudioFullEpg : true
        });
        //scrollArea.append(indicator.getView());


        pageKeyGuideDiv =  util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 181, top: (998-357), overflow: "visible" , display: "none"
                }
            },
            parent: audioFullEpgDiv
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "",
                src: IMAGE_PATH + "icon/icon_pageup.png",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: 29,
                    height: 32
                }
            },
            parent: pageKeyGuideDiv
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "",
                src: IMAGE_PATH + "icon/icon_pagedown.png",
                css: {
                    position: "absolute",
                    left: (209-181),
                    top: 0,
                    width: 29,
                    height: 32
                }
            },
            parent: pageKeyGuideDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "",
                class: "font_l" ,
                css: {
                    position: "absolute", left: (242-181), top: 3, width: 100 , height: 26,
                    color: "rgba(255, 255, 255, 0.3)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                }
            },
            text: "페이지",
            parent: pageKeyGuideDiv
        });



        contextMenuDiv =  util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 1753, top: (1000-357), overflow: "visible" , display: "none"
                }
            },
            parent: audioFullEpgDiv
        });


        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "context_menu_img",
                src: IMAGE_PATH + "icon/icon_option_related.png",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 1,
                    width: 27,
                    height: 22
                }
            },
            parent: contextMenuDiv
        });


        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "text_context_menu_title",
                class: "font_l" ,
                css: {
                    position: "absolute", left: 34, top: 0, width: 100 , height: 26,
                    color: "rgba(255, 255, 255, 0.4)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                }
            },
            text: "옵션",
            parent: contextMenuDiv
        });
    };

    this.showView = function(options) {
        log.printDbg("showView() options : " + log.stringify(options));

        var focusChannel = null;
        callbackFuncRelateMenuRight = null;
        if (options !== undefined && options !== null && options.resume === true) {
            KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT, true);

            if(tuneAudioChannel === null) {

                var focusIndex = ((curPage-1)*(maxColumn*maxRow)) +  ((focusY*maxColumn) + focusX);

                log.printDbg("showView() curPage : " + curPage + " , focusY : " + focusY + " , focusX : " + focusX + " , focusIndex : " + focusIndex);

                var ch = channelList[focusIndex];

                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG , ch);
            }else {
                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG , tuneAudioChannel);
            }

        }else {
            var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.AUDIO_IFRAME);
            if(layer !== null) {
                layer.hideAudioEpgViewIcon();
            }
            
            isJump = false;
            if (this.parent.params && this.parent.params.focus_channel) {
                KTW.managers.service.AudioIframeManager.setAudioFocusChannel(this.parent.params.focus_channel , true);
                focusChannel = this.parent.params.focus_channel;
            }

            if (this.parent.params && this.parent.params.is_jump) {
                isJump = this.parent.params.is_jump;
            }


            if (this.parent.params && this.parent.params.call_back_relate_menu_right) {
                callbackFuncRelateMenuRight = this.parent.params.call_back_relate_menu_right;
            }

            if (this.parent.params && this.parent.params.parent_view) {
                parent_view = this.parent.params.parent_view;
            }else {
                parent_view = null;
            }



            isScrollFocus = false;
            focusX = 0;
            focusY = 0;

            var arrFavId = [];
            if (KTW.CONSTANT.IS_OTS) {
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_AUDIO);
            }
            else {
                arrFavId.push(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SURFABLE_AUDIO);
            }
            channelList = KTW.ui.adaptor.EpgDataAdaptor.getChannelList(arrFavId);


            totalPage = Math.ceil(channelList.length / (maxColumn*maxRow));
            curPage = 1;

            indicator.show({
                maxPage: totalPage,
                curPage: curPage - 1
            });


            var mainChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
            var initFocusChannel = null;

            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(mainChannel) === true) {
                tuneAudioChannel = mainChannel;
                initFocusChannel = tuneAudioChannel;

            }else {
                tuneAudioChannel = null;
                initFocusChannel = focusChannel;
            }


            for(var i=0;i<channelList.length;i++) {
                if(initFocusChannel.ccid === channelList[i].ccid) {
                    var page = Math.ceil((i+1) / (maxColumn*maxRow));
                    curPage = page;
                    var temp = ((i+1) - ((maxColumn*maxRow)*(curPage-1)));
                    if(temp>0) {
                        temp-=1;
                    }

                    var tempX = Math.ceil((temp+1) / maxColumn);
                    if(tempX>0) {
                        tempX-=1;
                        focusY = tempX;
                    }

                    focusX = temp - (focusY*maxColumn);
                    break;
                }
            }

            _showChannelList();
            _showContextMenu();
            _showPageKeyGuide();
            _setPageFocus();

            setTimeout(function () {
                KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT, true);
            }, 100);

            KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);
        }


    };

    this.returnToBackChannelOnOTS = function () {
        var mainControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
        var channel = mainControl.getCurrentChannel();

        if (isAudioChannel(channel)) {
            channel = mainControl.getBackChannel();
            this.parent.mainChannelTune({channel: channel, onlySelect: true});
        }

    };


    this.hideView = function(options) {
        log.printDbg("hideView()");

        if(options !== undefined && options !== null && options.pause === true) {
            log.printDbg("hideView() pause"); 
            if( KTW.managers.service.AudioIframeManager.getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG) {
                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE);
            }else if(KTW.managers.service.AudioIframeManager.getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
                var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.AUDIO_IFRAME);
                if(layer !== null) {
                    layer.showAudioEpgViewIcon();
                }
            }
        }else {
            if( KTW.managers.service.AudioIframeManager.getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG) {
                KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.NONE);
            }else if(KTW.managers.service.AudioIframeManager.getType() === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
                var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.AUDIO_IFRAME);
                if(layer !== null) {
                    layer.showAudioEpgViewIcon();
                }
            }

            KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
        }

    };

    this.controlKey = function (keyCode) {
        log.printDbg("controlKey() ");
        var consumed = false;
        switch (keyCode) {
            case KTW.KEY_CODE.FULLEPG :
            case KTW.KEY_CODE.FULLEPG_OTS :
                if(tuneAudioChannel !== null) {
                    KTW.ui.LayerManager.clearNormalLayer();
                    consumed = true;
                }
                 break;
            case KTW.KEY_CODE.UP:
                if(isScrollFocus === false) {
                    _moveUpDown(true);
                }else {
                    _setScrollUpDown(true);
                }
                consumed = true;
                break;
            case KTW.KEY_CODE.DOWN:
                if(isScrollFocus === false) {
                    _moveUpDown(false);
                }else {
                    _setScrollUpDown(false);
                }
                consumed = true;
                break;
            case KTW.KEY_CODE.LEFT:

                consumed = _moveLeftRight(true);
                if(consumed === false) {
                    if(tuneAudioChannel === null && isJump === false) {
                        KTW.ui.LayerManager.deactivateLayer({id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
                    }
                    consumed = true;
                }
                break;
            case KTW.KEY_CODE.RIGHT:
                if(isScrollFocus === false) {
                    _moveLeftRight(false);
                }else {
                    isScrollFocus = false;
                    indicator.setFocus(false);
                    _setFocus();
                }

                consumed = true;
                break;
            case KTW.KEY_CODE.OK :
                if(isScrollFocus === false) {
                    _focusChannelTune();
                }else {
                    isScrollFocus = false;
                    indicator.setFocus(false);
                    _setFocus();
                }
                consumed = true;
                break;
            case KTW.KEY_CODE.CONTEXT:
                if(isContextMenuShow === true) {
                    _showRelatedMenu();
                }
                consumed = true;
                break;
            case KTW.KEY_CODE.BACK :
                if(isJump === true && tuneAudioChannel === null) {
                    KTW.ui.LayerManager.clearNormalLayer();
                    consumed = true;
                }else if(tuneAudioChannel !== null) {
                    if(KTW.CONSTANT.IS_OTS) {
                        this.returnToBackChannelOnOTS();
                        consumed = true;
                    }else {
                        KTW.ui.LayerManager.clearNormalLayer();
                    }
                }
                break;
            case KTW.KEY_CODE.EXIT :
                if(tuneAudioChannel !== null) {
                    //KTW.ui.LayerManager.deactivateLayer({id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG});
                    consumed = true;
                }
                break;
            case KTW.KEY_CODE.RED :
                if(isScrollFocus === true) {
                    isScrollFocus = false;
                    indicator.setFocus(false);
                    focusX=0;
                    focusY=0;
                    _setFocus();
                }else {
                    _unFocus();
                    if(focusX === 0 && focusY ===0) {
                        curPage--;
                        if(curPage<=0) {
                            curPage = totalPage;
                        }
                        focusX=0;
                        focusY=0;
                        _setPageFocus();
                    }else {
                        focusX=0;
                        focusY=0;
                        _setFocus();
                    }

                }
                consumed = true;
                break;
            case KTW.KEY_CODE.BLUE :
                if(isScrollFocus === true) {
                    isScrollFocus = false;
                    indicator.setFocus(false);
                    if(curPage === totalPage) {
                        var max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
                        var tempX = Math.ceil( max / maxColumn);
                        if(tempX>0) {
                            tempX-=1;
                            focusY = tempX;
                        }

                        var tempX2 = max - (focusY*maxColumn);
                        tempX2 -=1;
                        if(tempX2>focusX) {
                            focusX = tempX2;
                        }
                    }else {
                        focusX=2;
                        focusY=4;
                    }
                    _setFocus();
                }else {
                    _unFocus();
                    if(curPage === totalPage) {
                        var max = channelList.length - ((curPage-1)*(maxColumn*maxRow));

                        var temp = (focusY*maxColumn) + focusX + 1;

                        if(max>temp) {
                            var tempX = Math.ceil( max / maxColumn);
                            if(tempX>0) {
                                tempX-=1;
                                focusY = tempX;
                            }

                            var tempX2 = max - (focusY*maxColumn);
                            tempX2 -=1;
                            if(tempX2>focusX) {
                                focusX = tempX2;
                            }
                            _setFocus();
                        }else {
                            curPage++;
                            if(curPage>totalPage) {
                                curPage = 1;
                            }

                            if(curPage === totalPage) {
                                var max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
                                var tempX = Math.ceil( max / maxColumn);
                                if(tempX>0) {
                                    tempX-=1;
                                    focusY = tempX;
                                }

                                var tempX2 = max - (focusY*maxColumn);
                                tempX2 -=1;
                                if(tempX2>focusX) {
                                    focusX = tempX2;
                                }
                            }else {
                                focusX=2;
                                focusY=4;
                            }
                            _setPageFocus();
                        }
                    }else {
                        if(focusX === 2 && focusY === 4) {
                            curPage++;
                            if(curPage>totalPage) {
                                curPage = 1;
                            }

                            if(curPage === totalPage) {
                                var max = channelList.length - ((curPage-1)*(maxColumn*maxRow));
                                var tempX = Math.ceil( max / maxColumn);
                                if(tempX>0) {
                                    tempX-=1;
                                    focusY = tempX;
                                }

                                var tempX2 = max - (focusY*maxColumn);
                                tempX2 -=1;
                                if(focusX>tempX2) {
                                    focusX = tempX2;
                                }
                            }else {
                                focusX=2;
                                focusY=4;
                            }
                            _setPageFocus();
                        }else {
                            focusX=2;
                            focusY=4;
                            _setFocus();
                        }
                    }
                }
                consumed = true;
                break;
        }

        return consumed;
    };
};

KTW.ui.view.AudioChannelFullEpgView.prototype = new KTW.ui.View();
KTW.ui.view.AudioChannelFullEpgView.prototype.constructor = KTW.ui.view.AudioChannelFullEpgView;

KTW.ui.view.AudioChannelFullEpgView.prototype.create = function() {
    this.createView();
};

KTW.ui.view.AudioChannelFullEpgView.prototype.show = function(options) {
    this.showView(options);
};

KTW.ui.view.AudioChannelFullEpgView.prototype.hide = function(options) {
    this.hideView(options);
};

KTW.ui.view.AudioChannelFullEpgView.prototype.handleKeyEvent = function (key_code) {
    return this.controlKey(key_code);
};

