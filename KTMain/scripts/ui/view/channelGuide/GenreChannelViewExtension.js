/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>GenreChannelViewExtension</code>
 *
 * @author sw.nam
 * @since 2017-01-05
 * 장르별 채널 view implementation
 */

"use strict";

(function() {

    /* Constance */
    var NUMBER_OF_PREV_ITEM = 8;
    var NUMBER_OF_FOCUSED_ITEM = 5;
    var DEFAULT_FOCUSED_ITEM_IDX = 0;

    /* UtilityAPIs */
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var epgUtil = KTW.utils.epgUtil;
    var navAdapter = KTW.oipf.AdapterHandler.navAdapter;

    /* Elements*/
    var div = null;
    var itemGroup = null;
    var item =[];

    /* scroll*/
    var scrollArea = null;
    var scroll = null;

    /* subArea*/
    var subArea = null;

    /* focus */
    //var focusBar = null;
    var focusProgramIdx = DEFAULT_FOCUSED_ITEM_IDX;

    /* Flags */
    var isCreate = false;
    var isFocusedAppArea = true;

    var pipTimer = null;

    var updateTimer = null;

    var maxPage = null;

    var historyDiv = null;

    function createElement (_this) {
        div = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "genre_channel_view_extension",
                css: {
                    visibility: "hidden"
                }
            },
            parent: _this.parent.getEpgContainerDiv()
        });


        var backgroundDiv = util.makeElement({
             tag: "<img />",
             attrs: {
                 class : "genre_channel_view_extension_background" ,
                 src: "images/bg_default.jpg",
                 css: {
                     position: "absolute"
                 }
             },
             parent: div
         });
        
        
        

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/bg_menu_dim_up.png",
                css: {
                    position: "absolute",left: 0, top: 0, width:1920,height:280
                }
            },
            parent: div
        });

        historyDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "genre_channel_view_extension_history",
                css: {
                    position: "absolute"
                }
            },
            parent: div

        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/ar_history.png",
                css: {
                    position: "absolute", left: 0, top: 0,width:46,height:46
                }
            },
            parent: historyDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "history_title",
                css: {
                    position: "absolute", left: 54, top: 0, width:500 , height:42,  "font-size": 40, color: "rgba(255, 255, 255, 0.12)",
                    "letter-spacing": -1.2, "font-family": "RixHead B"
                }
            },
            parent: historyDiv
        });






        itemGroup = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "itemGroup",
                css: {
                    position: "absolute", /*left: 540, top: 148, */overflow: "visible"
                    //"-webkit-transition": "transform 0.5s"
                }
            },
            parent: div

        });

        scrollArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "scroll",
                css: {
                    position: "absolute", left: 110, top: 143, overflow: "visible", visibility: "hidden"
                }
            },
            parent: div
        });

        scroll = new KTW.ui.component.Scroll({
            parentDiv: scrollArea,
            height: 720
        });

        subArea = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "sub_area",
                css:{
                    visibility: "hidden"
                }
            },
            parent: div
        });


        var nowTimeArea = util.makeElement({
            tag: "<div />",
            attrs: {
                css:{
                    position: "absolute", left: 1669, top: 62 , width:(1920-1169) , height: 32 , opacity:0.8
                }
            },
            parent: subArea
        });
        
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: (1669 - 1669), top: (71-62), "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                    "letter-spacing": -1.2
                }
            },
            text: "현재 시간",
            parent: nowTimeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "hour_10" ,
                css: {
                    position: "absolute", left: (1761 - 1669), top: (62 - 62)
                }
            },
            parent: nowTimeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "hour_1" ,
                css: {
                    position: "absolute", left: (1783-1669), top: (62 - 62)
                }
            },
            parent: nowTimeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/timenumber/h_img_time_home.png",
                css: {
                    position: "absolute", left: (1808-1669), top: (70 - 62)
                }
            },
            parent: nowTimeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "min_10" ,
                css: {
                    position: "absolute", left: (1815-1669), top: (62 - 62)
                }
            },
            parent: nowTimeArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "min_1" ,
                css: {
                    position: "absolute", left: (1837 - 1669), top: (62 - 62)
                }
            },
            parent: nowTimeArea
        });

        var changePageGuideArea = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "genre_channel_view_extension_change_page_guide_area"
            },
            parent: subArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pageup.png",
                css: {
                    position: "absolute", left: 82, top: 904
                }
            },
            parent: changePageGuideArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_pagedown.png",
                css: {
                    position: "absolute", left: 110, top: 904
                }
            },
            parent: changePageGuideArea
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 82, top: 939, "font-size": 22, color: "rgba(255, 255, 255, 0.3)",
                    "letter-spacing": -1.1
                }
            },
            text: "페이지",
            parent: changePageGuideArea
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/icon_option_related.png",
                css: {
                    position: "absolute", left: 1789, top: 1001
                }
            },
            parent: subArea
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1823, top: 1000, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                    "letter-spacing": -1.2
                }
            },
            text: "옵션",
            parent: subArea
        });

        for (var i =0; i < NUMBER_OF_PREV_ITEM; i++){
            var itemClass = "item";
            if (i > NUMBER_OF_FOCUSED_ITEM - 1) {
                itemClass += " full_screen_hide";
            }
            item[i] = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: itemClass,
                    css: {
                        overflow: "hidden"
                        //"-webkit-transition": "height 0.5s, opacity: 0.5s"
                    }
                },
                parent: itemGroup
            });

            var chArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "chArea",
                    css: {
                        float: "left"
                        //"-webkit-transition": "width 0.5s, height 0.5s, opacity: 0.5s"
                    }
                },
                parent: item[i]
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "icon",
                    css: {
                        position: "absolute", width: 22, height: 22
                    }
                },
                parent: chArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "chNum",
                    css: {
                        position: "absolute", "font-family": "RixHead B"
                    }
                },
                parent: chArea
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    class: "chName",
                    css: {
                        position: "absolute", "margin-left": 113, "margin-top": 52, width: 130,
                        "font-size": 28, "letter-spacing": -1.4, "font-family": "RixHead L"
                    }
                },
                parent: chArea
            });

            var pipArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "pipArea",
                    css: {
                        float: "left"
                        //"-webkit-transition": "width 0.5s, height 0.5s, opacity: 0.5s"
                    }
                },
                parent: item[i]
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "thumbnail_default",
                    src: "images/iframe/default_thumb.jpg",
                    css: {
                        position: "absolute"
                    }
                },
                parent: pipArea
            });

            var thumbnail = util.makeElement({
                tag: "<img />",
                attrs: {
                    class: "thumbnail",
                    css: {
                        position: "absolute"
                    }
                },
                parent: pipArea
            });
            thumbnail.error(function () {
                $(this).attr("src", "").css({display: "none"});
            });



            for (var j = 0; j < 3; j++) {
                var programClass = "programArea";
                if (j > 0) {
                    programClass += " full_screen_show";
                }
                var programArea = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: programClass,
                        css: {
                            float: "left"
                            //"-webkit-transition": "width 0.5s, height 0.5s, opacity: 0.5s"
                        }
                    },
                    parent: item[i]
                });

                var tempdiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "programArea_focus_bar focus_red_border_box",
                        css: {
                            position: "absolute", width: 455, height: 166,
                        }
                    },
                    parent: programArea
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focus_black_border_box",
                        css: {
                            position: "absolute", left : 0 , top : 0 , width: (455-12), height: (166-12)
                        }
                    },
                    parent: tempdiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/icon/icon_book.png",
                        class: "reservationIcon",
                        css: {
                            position: "absolute", "margin-left": 39, "margin-top": 42
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programName",
                        css: {
                            position: "absolute", "white-space": "nowrap", height: 45
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programName_2",
                        css: {
                            position: "absolute", "margin-left": 27, "margin-top": 70, width: 375, height: 45,
                            "font-size": 33, "letter-spacing": -1.65, "font-family": "RixHead M",
                            "white-space": "nowrap", "text-overflow": "ellipsis", overflow: "hidden"
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        class: "ageIcon",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: programArea
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programStartTime",
                        css: {
                            position: "absolute"/*, "font-family": "RixHead L"*/
                        }
                    },
                    parent: programArea
                });

                var progressBar = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "entireProgressBar",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: programArea
                });
                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "dynamicProgressBar",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: progressBar
                });
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class: "programEndTime",
                        css: {
                            position: "absolute"/*, "font-family": "RixHead L"*/
                        }
                    },
                    parent: programArea
                });
            }
        }

        util.makeElement({
            tag: "<img />",
            attrs: {
                class: "genre_channel_view_extension_preview_thumbnail_sdw",
                src: "images/sdw_guide_thumb.png",
                css: {
                    position: "absolute"
                }
            },
            parent: itemGroup
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                class: "genre_channel_view_extension_preview_program_sdw",
                src: "images/sdw_guide_thumb.png",
                css: {
                    position: "absolute"
                }
            },
            parent: itemGroup
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/dim_epg_btm.png",
                class: "genre_channel_view_extension_preview_dim_btm",
                css: {
                    position: "absolute"
                }
            },
            parent: itemGroup
        });

    }

    function drawChannelArea (_this) {
        log.printDbg("drawPreviewChannelArea()");

        //현재 보여지는 channel list 받아오기.
        var curChannelList = getCurChannelList(_this); // 현재 채널이  0~4 사이에 들어오도록 , 채널리스트를 보여준다.

        var channel = null;
        var chArea = null;
        var icon = null;

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            channel = curChannelList[i];
            chArea = item[i].find(".chArea");

            if (channel) {
                var chNum = channel.data.majorChannel;
                chArea.find(".chNum").text(util.numToStr(chNum,3,null));

                chArea.find(".icon").css({visibility: "inherit"});
                if (navAdapter.isBlockedChannel(channel)) {
                    icon = "images/icon/icon_block.png";
                }
                else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channel)) {
                    icon = "images/icon/icon_fav.png"
                }
                else {
                    chArea.find(".icon").css({visibility: "hidden"});
                }
                chArea.find(".icon").attr({src: icon});

                item[i].css({visibility: ""});
            }
            else {
                item[i].css({visibility: "hidden"});
            }
        }

        drawChannelName(_this);
    }

    function drawChannelName (_this) {
        log.printDbg("drawChannelName()");

        //현재 보여지는 channel list 받아오기.
        var curChannelList = getCurChannelList(_this); // 현재 채널이  0~4 사이에 들어오도록 , 채널리스트를 보여준다.
        var channelIdx = getFocusChannelElementIdx(_this);

        var channel = null;
        var chArea = null;

        var isFocus = div.hasClass("focus");
        //var width = isFocus ? 230 : 200;
        //var fontSize = isFocus ? 30: 28;
        var cssObj;

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            if (isFocus) {
                cssObj = {
                    "margin-left": 60, "margin-top": 83, width: 230, height: 35,
                    "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.5)",
                    overflow: "hidden", "text-overflow": "ellipsis", "white-space": "nowrap"
                };
                if (i === channelIdx) {
                    cssObj.color = "rgba(255, 255, 255, 1)";
                }
            }
            else {
                cssObj = {
                    "margin-left": 47, "margin-top": 69, width: 200, height: 35,
                    "font-size": 27, "letter-spacing": -1.35, color: "rgba(255, 255, 255, 0.5)",
                    overflow: "hidden", "text-overflow": "ellipsis", "white-space": "nowrap"
                };
            }

            channel = curChannelList[i];
            chArea = item[i].find(".chArea");

            if (channel) {
                var chName = channel.data.name;
                chArea.find(".chName").css(cssObj).text(chName);
            }
        }
    }

    function drawThumbnail (_this) {
        log.printDbg("drawThumbnail()");

        //현재 보여지는 channel list 받아오기.
        //var curChannelList = getCurChannelList(_this); // 현재 채널이  0~4 사이에 들어오도록 , 채널리스트를 보여준다.
        var curProgramList  = _this.getPreviewProgrammesList();
        //var channel = null;
        var pipArea = null;

        if (curProgramList == null || curProgramList === undefined || curProgramList.length <= 0) {
            log.printDbg("drawThumbnail(), curProgram is not exist. so, return");
            return;
        }

        var length = curProgramList.length < NUMBER_OF_PREV_ITEM ? curProgramList.length : NUMBER_OF_PREV_ITEM;
        for (var i = 0; i < length; i++) {
            pipArea = item[i].find(".pipArea");
            //var channel = curChannelList[i];
            var thumbImagePath = "";

            //thumbImagePath = "http://image.ktipmedia.co.kr/channel/CH_" + channel.data.sid + ".png";

            //var pr = KTW.oipf.AdapterHandler.casAdapter.getParentalRating();
            var pr = KTW.managers.auth.AuthManager.getPR();

            if (curProgramList[i].channel && curProgramList[i].channel.data) {
                if (navAdapter.isBlockedChannel(curProgramList[i].channel)) {
                    // blocked channel (시청 제한 채널)
                    thumbImagePath = "images/iframe/block_ch.jpg";
                }
                else if (curProgramList[i].channel.desc === 2) {
                    // 성인 채널
                    thumbImagePath = "images/iframe/block_adult_ch.jpg";
                }
                else if (pr !== 0 && KTW.utils.epgUtil.getAge(curProgramList[i]) >= pr) {
                    // 연령 제한 프로그램
                    thumbImagePath = "images/iframe/block_age.jpg";
                }
                else {
                    //thumbImagePath = "http://image.ktipmedia.co.kr/channel/CH_" + curProgramList[i].channel.data.sid + ".png";
                    thumbImagePath = util.getImageUrl(KTW.DATA.HTTP.THUMBNAIL_IMAGE_SERVER + curProgramList[i].channel.data.sid + ".png", KTW.CONSTANT.POSTER_TYPE.THUMBNAIL, 282, 158);
                }
            }

            pipArea.find(".thumbnail").css({display: ""}).attr({src: thumbImagePath});
        }
    }

    function drawProgramme (options) {
        log.printDbg("drawProgramme()");

        var _this = options._this;
        var programme = options.programme;
        var progElement = options.progElement;
        var isFocus = options.isFocus;

        var currentTime = (new Date()).getTime();

        var nameCssObj = {
            "margin-left": 24, "margin-top": 32, width: 253, "font-size": "", "font-family": ""
        };
        var isReserve = false;

        if (programme) {
            var programName = programme.name;

            var startTime = programme.startTime * 1000;
            var programDuration = programme.duration * 1000;
            var endTime = startTime + programDuration;

            if (div.hasClass("focus") && !options.isPreview) {
                /* 프로그램 예약여부 확인 */
                var reserved = KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED;
                if (!options.isFirstProgramme) {
                    reserved = KTW.managers.service.ReservationManager.isReserved(programme);
                }

                if (reserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                    progElement.find(".reservationIcon").css({visibility: "hidden"});
                    isReserve = false;

                    nameCssObj["margin-left"] = 39;
                    nameCssObj["margin-top"] = 42;
                    nameCssObj["width"] = 350;
                }
                else {
                    if (reserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
                        progElement.find(".reservationIcon").attr("src", "images/icon/icon_book_s.png").css({"margin-left": 39, "margin-top": 42, visibility: "inherit"});
                    }
                    else {
                        progElement.find(".reservationIcon").attr("src", "images/icon/icon_book.png").css({"margin-left": 39, "margin-top": 42, visibility: "inherit"});
                    }

                    isReserve = true;

                    nameCssObj["margin-left"] = 79;
                    nameCssObj["margin-top"] = 42;
                    nameCssObj["width"] = 310;
                }

                if (isFocus && isFocusedAppArea) {

                    if (reserved === KTW.managers.service.ReservationManager.RESERVED_RESULT.RESERVED_SERIES) {
                        progElement.find(".reservationIcon").attr("src", "images/icon/icon_book_s.png").css({"margin-left": 27, "margin-top": 23});
                    }else {
                        progElement.find(".reservationIcon").attr("src", "images/icon/icon_book.png").css({"margin-left": 27, "margin-top": 23});
                    }
                    nameCssObj["margin-left"] = isReserve ? 67 : 27;
                    nameCssObj["margin-top"] = 23;
                    nameCssObj["width"] = isReserve ? 329 : 369;

                    nameCssObj["font-size"] = 33;
                    nameCssObj["font-family"] = "RixHead M";

                    var arrProgrammeName = util.splitText(programName, "RixHead M", 33, nameCssObj["width"], -1.65);
                    var ageIconCssObj = { "margin-top": 23, visibility: "inherit"} ;

                    if (arrProgrammeName.length > 1) {
                        progElement.find(".programName").text(arrProgrammeName[0]).css(nameCssObj);
                        if (arrProgrammeName.length > 2) {
                            arrProgrammeName[1] += arrProgrammeName[2];
                        }
                        progElement.find(".programName_2").text(arrProgrammeName[1]);

                        var name2Length = util.getTextLength(arrProgrammeName[1], "RixHead M", 33, -1.65);

                        ageIconCssObj["margin-left"] = name2Length > 375 ? 375 + 7 + 27 : name2Length + 7 + 27;
                        ageIconCssObj["margin-top"] = 70;
                    }
                    else {
                        progElement.find(".programName").text(programName).css(nameCssObj);
                        progElement.find(".programName_2").text("");

                        var nameLength = util.getTextLength(programName, "RixHead M", 33, -1.65);

                        ageIconCssObj["margin-left"] = nameCssObj["margin-left"] + nameLength + 7;
                    }

                    // 연령 아이콘
                    var age = KTW.utils.epgUtil.getAge(programme);
                    var ageImg;
                    if (age && age > 0) {
                        ageImg = "images/icon/icon_age_list_" + age + (isFocusedAppArea ? ".png" : ".png");
                        progElement.find(".ageIcon").attr({src: ageImg}).css(ageIconCssObj);

                    }
                    else if (age === 0) {
                        ageImg = "images/icon/icon_age_list_all" + (isFocusedAppArea ? ".png" : ".png");
                        progElement.find(".ageIcon").attr({src: ageImg}).css(ageIconCssObj);
                    }
                    else {
                        ageImg = "";
                        progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
                    }
                }
                else {
                    progElement.find(".programName").text(programName).css(nameCssObj);
                    progElement.find(".programName_2").text("");
                    progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
                }
            }
            else {
                progElement.find(".reservationIcon").css({visibility: "hidden"});
                progElement.find(".programName").text(programName).css(nameCssObj);
                progElement.find(".programName_2").text("");
                progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
            }

            // 2017.05.01 dhlee
            // reviseType은 0 ~ 3 사이의 값이므로 program.reviseType 이 PROGRAM_REVISE_TYPE.NULL(0) 인 경우 false가 된다
            //if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
            if (programme.reviseType != undefined && programme.reviseType != null &&
                (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
                || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
                || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {
                //if (programme.reviseType && (programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.NULL
                //    || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.UPDATE
                //    || programme.reviseType === epgUtil.DATA.PROGRAM_REVISE_TYPE.CHANNEL_NAME)) {

                progElement.find(".entireProgressBar").css({visibility: "hidden"});
                progElement.find(".programStartTime").text("");
                progElement.find(".programEndTime").text("");
            }
            else {
                if(startTime < currentTime && startTime + programDuration >= currentTime) {
                    progElement.find(".programStartTime").text(util.getTimeString(startTime));
                    progElement.find(".programEndTime").text(util.getTimeString(endTime));
                    var width = Math.floor(((currentTime - startTime) / programDuration) * progElement.find(".entireProgressBar").width());
                    progElement.find(".dynamicProgressBar").css({width: width});
                    progElement.find(".entireProgressBar").css({visibility: "inherit"});
                }
                else {
                    progElement.find(".entireProgressBar").css({visibility: "hidden"});
                    progElement.find(".programStartTime").text(util.getTimeString(startTime) + " - " + util.getTimeString(endTime));
                    progElement.find(".programEndTime").text("");
                }
            }
        }
        else {
            progElement.find(".reservationIcon").css({visibility: "hidden"});
            progElement.find(".programName").text("");
            progElement.find(".programName_2").text("");
            progElement.find(".programStartTime").text("");
            progElement.find(".programEndTime").text("");
            progElement.find(".ageIcon").attr({src: ""}).css({visibility: "hidden"});
            progElement.find(".entireProgressBar").css({visibility: "hidden"});
        }
    }

    function drawProgrammeList (options) {
        log.printDbg("drawProgrammeList()");

        var focusChannelIdx  = getFocusChannelElementIdx(options._this);
        var channelIdx  = options.channelIdx;
        var programList = options.programList;
        var arrProgElement = options.programArea;
        var programmeIdx = 0;
        if(!options || !options.programList) {
            return ;
        }

        if (channelIdx === focusChannelIdx) {
            if (focusProgramIdx % 2 === 1) {
                programmeIdx = focusProgramIdx - 1;
            }
            else {
                programmeIdx = focusProgramIdx;
            }
        }

        for (var j = 0; j < arrProgElement.length; j++) {

            var element = $(arrProgElement[j]);
            var programme = programList[programmeIdx + j];

            drawProgramme({
                _this: options._this,
                programme: programme,
                progElement: element,
                isFocus: channelIdx === focusChannelIdx && focusProgramIdx % 2 === j,
                isPreview: options.isPreview,
                isFirstProgramme: focusProgramIdx === 0 && j === 0
            });
        }

    }

    function drawProgramArea (_this) {
        log.printDbg("drawProgramArea()");

        var programList = _this.getProgrammesList();

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            drawProgrammeList({
                _this: _this,
                channelIdx: i,
                programList: programList[i],
                programArea: item[i].find(".programArea"),
                isPreview: i < NUMBER_OF_FOCUSED_ITEM ? false : true
            });
        }

        /*
         var channelIdx  = getFocusChannelElementIdx(_this);
         var programList = _this.getProgrammesList();
         var thisChannelProgramList = null;

         for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
         thisChannelProgramList = programList[i];

         var arrProgElement = item[i].find(".programArea");
         var programmeIdx = 0;

         if (channelIdx === i) {
         if (focusProgramIdx % 2 === 1) {
         programmeIdx = focusProgramIdx - 1;
         }
         else {
         programmeIdx = focusProgramIdx;
         }
         }

         for (var j = 0; j < arrProgElement.length; j++) {
         var element = $(arrProgElement[j]);
         var programme = thisChannelProgramList[programmeIdx + j];

         if (i < NUMBER_OF_FOCUSED_ITEM || j === 0) {
         drawProgramme({
         _this: _this,
         programme: programme,
         progElement: element,
         isFocus: channelIdx === i && focusProgramIdx % 2 === j,
         isPreview: i < NUMBER_OF_FOCUSED_ITEM ? false : true
         });
         }
         }
         }
         */
    };

    function drawFocusPrgramList (_this) {
        log.printDbg("drawFocusPrgramList()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();

        for (var i = 0; i < NUMBER_OF_PREV_ITEM; i++) {
            var isDraw = false;

            if (channelIdx === i) {
                isDraw = true;
            }
            else {
                if (item[i].hasClass("focus")) {
                    isDraw = true;
                }
            }

            if (isDraw) {
                drawProgrammeList({
                    _this: _this,
                    channelIdx: i,
                    programList: programList[i],
                    programArea: item[i].find(".programArea"),
                    isPreview: i < NUMBER_OF_FOCUSED_ITEM ? false : true
                });
            }
        }
    }

    //현재 channel LIst 를 받아온다.
    function getCurChannelList (_this) {
        log.printDbg("getCurChannelList()");

        var channelList = _this.getChannelList();
        var curPage = _this.getChannelPage();
        var startIdx = curPage * NUMBER_OF_FOCUSED_ITEM;

        return channelList.slice(startIdx, startIdx + NUMBER_OF_PREV_ITEM);
    };

    function getFocusChannelElementIdx (_this) {
        return _this.getChannelIdx() % NUMBER_OF_FOCUSED_ITEM;
    }

    function getFocusProgramElementIdx () {
        return focusProgramIdx % 2;
    }

    function setFocusArea () {
        log.printDbg("setFocusArea()");

        if (isFocusedAppArea) {
            scroll.setFocus(false);
            itemGroup.addClass("focus");
        }
        else {
            scroll.setFocus(true);
            itemGroup.removeClass("focus");
        }
    }

    function setFocusEpgArea (_this) {
        log.printDbg("setFocusEpgArea()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var arrProgramArea = item[channelIdx].find(".programArea");
        var focusProgramArea = $(arrProgramArea[getFocusProgramElementIdx()]);

        itemGroup.children().removeClass("focus");
        item[channelIdx].addClass("focus");

        //focusBar.css({top: (channelIdx * 161)});

        itemGroup.find(".programArea").removeClass("focus");
        focusProgramArea.addClass("focus");
    }

    function isChannelListExist (_this) {
        log.printDbg("isChannelListExist()");

        var channelList = _this.getChannelList();
        return channelList && channelList.length > 0 ? true : false;
    };

    function isLastChannel (_this) {
        log.printDbg("isLastChannel()");

        var channelIdx = _this.getChannelIdx();
        var channelList = _this.getChannelList();
        var flag = false;

        if (channelIdx == channelList.length - 1)
            flag = true;

        return flag;
    };

    function getLastChannelIdx (_this) {
        log.printDbg("getLastChannelIdx");

        var channelList = _this.getChannelList();
        return channelList.length - 1;
    };

    function getCurrentProgrammeLength (_this) {
        log.printDbg("getCurrentProgrammeLength()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();
        var curProgramList = programList[channelIdx];

        return curProgramList.length;
    }

    function isLastPage (_this) {
        log.printDbg("isLastPage()");

        var curPage = _this.getChannelPage();
        maxPage = _this.getChannelMaxPage();

        return curPage == maxPage-1 ? true : false;
    };

    function isLastProgram (_this) {
        log.printDbg("isLastProgram()");

        var channelIdx  = getFocusChannelElementIdx(_this);
        var programList = _this.getProgrammesList();
        var curProgramList = programList[channelIdx];

        return focusProgramIdx === curProgramList.length - 1 ? true : false;
    };

    function moveFocusChannel (options) {
        log.printDbg("moveFocusChannel(" + options.nextChannelIdx + ")");

        if (options.nextChannelIdx == null || options.nextChannelIdx == undefined) {
            log.printDbg("nextChannelIdx is null... so return");
            return;
        }

        changeChannelIdx(options);

        if (focusProgramIdx > 1) {
            focusProgramIdx = 0;
        }
        else {
            if (focusProgramIdx >= getCurrentProgrammeLength(options._this)) {
                focusProgramIdx = 0;
            }
        }

        if (options.pageUpdate) {
            drawChannelArea(options._this);
            drawThumbnail(options._this);
            drawProgramArea(options._this);
        }
        else {
            //if (options.programUpdate) {
            //    drawProgramArea(options._this);
            //}

            drawFocusPrgramList(options._this);
        }

        //drawProgramArea(options._this);

        scroll.setCurrentPage(options._this.getChannelPage());
        setFocusEpgArea(options._this);
        setPipChannel(options._this);
    };

    function changeChannelIdx (options) {

        var channelIdx = options._this.getChannelIdx();
        var channelList = options._this.getChannelList();
        var length = channelList.length;

        if (options.nextChannelIdx == channelIdx) {
            log.printDbg("nextChannelIdx == current channel idx... so return");
            return;
        }
        if (options.nextChannelIdx < 0 || options.nextChannelIdx > length - 1) {
            log.printDbg("nextChannelIdx is out of range... so return");
            return;
        }

        var isUp = true;

        if ((options.nextChannelIdx > channelIdx && options.nextChannelIdx != length - 1)
            || (options.nextChannelIdx < channelIdx && options.nextChannelIdx == 0)) {
            isUp = false;
        }

        log.printDbg("isUp : " + isUp);

        options._this.changeChannelIdx({
            nextChannelIdx: options.nextChannelIdx,
            isProgrammesListUpdate: options.pageUpdate,
            isUp: isUp
        });

    }


    function showRelatedMenu (_this) {
        log.printDbg("showRelatedMenu()");

        var isShowFavoritedChannel = true;
        var isShowProgramDetail = false;
        var isShowTwoChannel = false;
        var isShowFourChannel = true;
        var isShowHitChannel = true;
        var isShowGenreOption = false;

        var focusOptionIndex = 0;

        var focusMenuId = _this.menuId;//KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST;

        var epgrelatedmenuManager = KTW.managers.service.EpgRelatedMenuManager;

        var curChannel = _this.getCurrentChannel();

        if(navAdapter.isAudioChannel(curChannel) === true) {
            isShowFavoritedChannel = false;
            isShowProgramDetail = false;
        }


        epgrelatedmenuManager.showFullEpgRelatedMenu(curChannel, function (menuIndex) {
            cbRelateMenuLeft({
                _this: _this,
                menuIndex: menuIndex
            });
        }, isShowFavoritedChannel, isShowProgramDetail , isShowTwoChannel, isShowFourChannel, isShowHitChannel, isShowGenreOption, focusOptionIndex, function (menuId) {
            cbRelateMenuRight({
                _this: _this,
                menuId: menuId
            });
        }, focusMenuId);
    }

    function cbRelateMenuLeft (options) {
        log.printDbg("cbRelateMenuLeft()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
            // 선호채널 등록 / 해제

            var curChannel = options._this.getCurrentChannel();
            KTW.managers.service.EpgRelatedMenuManager.favoriteChannelOnOff(curChannel,function () {
                cbFavoriteChannelOnOff(options._this);
            });

        }
        else if(options.menuIndex ===  KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
            /**
             * OTS 자세히 버튼이므로 나오지 않을 상황임.
             */
        }
        else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
            // 4채널
            var locator = KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();

            //통계로그 추가
            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.JUMP_TO,
                act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                catId: options._this.menuId,
                contsId: "",
                locator: locator,
                reqPathCd: ""
            });
        }
        else if(options.menuIndex === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {
            // 실시간 인기 채널
            //KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();

            options._this.parent.jumpPopularMenu();
        }
    }

    function cbRelateMenuRight (options) {
        log.printDbg("cbRelateMenuRight()");

        KTW.managers.service.EpgRelatedMenuManager.deactivateRelatedMenu();

        if (options._this.menuId !== options.menuId) {
            // TODO 가이드 메뉴 가져오는 로직으로 수정
            var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id === options.menuId) {
                        return true;
                    }
                }
            })[0];

            if (epgMenu) {
                options._this.parent.jumpTargetEpgView({
                    targetMenu: epgMenu
                });
            }
            //통계로그 추가
            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.JUMP_TO,
                act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                catId: options._this.menuId,
                contsId: "",
                locator: "",
                reqPathCd: ""
            });
        }
    }
    /* PIP implementation*/

    function startPip (_this) {
        log.printDbg("startPip()");

        var pipType = KTW.managers.service.PipManager.PIP_TYPE.FULL_EPG;

        if (pipTimer) {
            clearTimeout(pipTimer);
        }

        pipTimer = setTimeout(function () {
            KTW.managers.service.PipManager.activatePipLayer({pipType: pipType});
            setPipChannel(_this);

            pipTimer = null;
        }, 700);
    }

    function setPipChannel (_this) {
        log.printDbg("setPipChannel()");

        if (!KTW.managers.service.PipManager.isPipLayerShow()) {
            return;
        }

        var channelIdx  = _this.getChannelIdx() % NUMBER_OF_FOCUSED_ITEM;
        KTW.managers.service.PipManager.setPipSize({ left: 499, top: 122 + (channelIdx * 169), width: 296, height: 166 });

        KTW.managers.service.PipManager.changeChannel(_this.getCurrentChannel());
    }

    function endPip () {
        log.printDbg("endPip()");

        if (pipTimer) {
            clearTimeout(pipTimer);
            pipTimer = null;
        }

        KTW.managers.service.PipManager.deactivatePipLayer();
    }

    function cbFavoriteChannelOnOff (_this){
        log.printDbg("cbFavoriteChannelOnOff()");
        drawChannelArea(_this);
    };

    function keyLeft (_this) {
        log.printDbg("keyLeft()");

        var consume = false;
        var focusElementIdx = getFocusProgramElementIdx();

        if (isFocusedAppArea) {
            if (focusProgramIdx > 0) {
                --focusProgramIdx;
                //if (focusElementIdx === 0) {
                //    drawProgramArea(_this);
                //}
                //else {
                //    drawFocusPrgramList(_this);
                //}

                drawFocusPrgramList(_this);

                setFocusEpgArea(_this);
            }
            else {
                return consume;
                // if(maxPage <= 1) {
                //     // 페이지 수가 1개인 경우에 대한 예외처리 추가
                //     return consume;
                // }else {
                //     isFocusedAppArea = false;
                //     setFocusArea();
                //     drawFocusPrgramList(_this);
                // }
            }
            consume = true;
        }
        return consume;
    }

    function keyRight (_this) {
        log.printDbg("keyRight()");

        if (isFocusedAppArea) {
            if (!isLastProgram(_this)) {
                ++focusProgramIdx;

                drawFocusPrgramList(_this);
                setFocusEpgArea(_this);
            }
        }
        else {
            isFocusedAppArea = true;
            setFocusArea();

            drawFocusPrgramList(_this);
        }
        return true;
    }

    function keyUp (_this) {
        log.printDbg("keyUp");

        var channelIdx = _this.getChannelIdx();
        var curPage = _this.getChannelPage();
        var nextChannelIdx = channelIdx - 1;
        var pageUpdate = false;
        var programUpdate = false;

        if (isFocusedAppArea) {
            if (channelIdx % NUMBER_OF_FOCUSED_ITEM > 0) {
                if (focusProgramIdx > 1) {
                    programUpdate = true;
                }
            }
            else {
                if (channelIdx === 0) {
                    nextChannelIdx = getLastChannelIdx(_this);
                }
                pageUpdate = true;
            }
        }
        else {
            if (curPage > 0) {
                nextChannelIdx = curPage * NUMBER_OF_FOCUSED_ITEM - 1;
            }
            else {
                nextChannelIdx = getLastChannelIdx(_this);
            }
            pageUpdate = true;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyDown(_this) {
        log.printDbg("keyDown");

        var channelIdx = _this.getChannelIdx();
        var lastChannelIdx = getLastChannelIdx(_this);
        var curPage = _this.getChannelPage();
        var nextChannelIdx = channelIdx + 1;
        var pageUpdate = false;
        var programUpdate = false;

        if (isFocusedAppArea) {
            if (channelIdx < lastChannelIdx && nextChannelIdx % NUMBER_OF_FOCUSED_ITEM > 0) {
                if (focusProgramIdx > 1) {
                    programUpdate = true;
                }
            }
            else {
                if (channelIdx === lastChannelIdx) {
                    nextChannelIdx = 0;
                }
                pageUpdate = true;
            }
        }
        else {
            if (isLastPage(_this)) {
                nextChannelIdx = 0;
            }
            else {
                nextChannelIdx = (curPage + 1) * NUMBER_OF_FOCUSED_ITEM;
            }
            pageUpdate = true;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyRed (_this) {
        log.printDbg("keyRed");

        var channelIdx = _this.getChannelIdx();
        var curPage = _this.getChannelPage();
        var nextPage = curPage - 1;
        if (nextPage < 0) {
            nextPage = _this.getChannelMaxPage() - 1;
        }
        var nextChannelIdx = channelIdx - 1;
        var pageUpdate = false;
        var programUpdate = false;

        if (isFocusedAppArea) {
            if (channelIdx % NUMBER_OF_FOCUSED_ITEM > 0) {
                nextChannelIdx = curPage * NUMBER_OF_FOCUSED_ITEM;
                if (focusProgramIdx > 1) {
                    programUpdate = true;
                }
            }
            else {
                nextChannelIdx = nextPage * NUMBER_OF_FOCUSED_ITEM;
                pageUpdate = true;
            }
        }
        else {
            isFocusedAppArea = true;
            setFocusArea();
            drawFocusPrgramList(_this);

            nextChannelIdx = curPage * NUMBER_OF_FOCUSED_ITEM;
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyBlue (_this) {
        log.printDbg("keyBlue");

        var channelLength = _this.getChannelList().length;
        var channelIdx = _this.getChannelIdx();
        var curPage = _this.getChannelPage();
        var nextPage = curPage + 1;
        if (nextPage > _this.getChannelMaxPage() - 1) {
            nextPage = 0;
        }
        var nextChannelIdx = channelIdx - 1;
        var pageUpdate = false;
        var programUpdate = false;

        if (isFocusedAppArea) {
            if (channelIdx % NUMBER_OF_FOCUSED_ITEM === NUMBER_OF_FOCUSED_ITEM - 1 || channelIdx === channelLength - 1) {
                nextChannelIdx = (nextPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
                if (nextChannelIdx > channelLength - 1) {
                    nextChannelIdx = channelLength - 1;
                }
                pageUpdate = true;
            }
            else {
                nextChannelIdx = (curPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
                if (nextChannelIdx > channelLength - 1) {
                    nextChannelIdx = channelLength - 1;
                }

                if (focusProgramIdx > 1) {
                    programUpdate = true;
                }
            }
        }
        else {
            isFocusedAppArea = true;
            setFocusArea();
            drawFocusPrgramList(_this);

            nextChannelIdx = (curPage * NUMBER_OF_FOCUSED_ITEM) + NUMBER_OF_FOCUSED_ITEM - 1;
            if (nextChannelIdx > channelLength - 1) {
                nextChannelIdx = channelLength - 1;
            }
        }

        moveFocusChannel({
            _this: _this,
            nextChannelIdx: nextChannelIdx,
            pageUpdate: pageUpdate,
            programUpdate: programUpdate
        });
    }

    function keyOk (_this) {
        log.printDbg("keyOk()");

        if (isFocusedAppArea) {
            if (focusProgramIdx === 0) {
                setFocusChannelTune(_this);
            }
            else {
                var channelIdx  = getFocusChannelElementIdx(_this);
                var programList = _this.getProgrammesList();
                var curProgramList = programList[channelIdx];
                var selectedProgram = curProgramList[focusProgramIdx];

                reservationProgramme({
                    _this: _this,
                    program: selectedProgram
                });
            }
        }
        else {
            isFocusedAppArea = true;
            setFocusArea();

            drawFocusPrgramList(_this);
        }
    }

    function setFocusChannelTune (_this) {
        log.printDbg("setFocusChannelTune()");

        var channelList = _this.getChannelList();
        var channelIdx = _this.getChannelIdx();
        var focusChannel = channelList[channelIdx];
        _this.parent.mainChannelTune({channel: focusChannel});
    }

    function reservationProgramme (options) {
        log.printDbg("reservationProgramme()");

        if (!options || !options.program) {
            log.printDbg("programme is null.... so return");
            return;
        }

        KTW.managers.service.ReservationManager.add(options.program, true, function (result) {
            var RESULT = KTW.managers.service.ReservationManager.ADD_RESULT;
            if (result === RESULT.SUCCESS_RESERVE || result === RESULT.SUCCESS_CANCEL
                || result === RESULT.SUCCESS_SERIES_RESERVE || result === RESULT.SUCCESS_SERIES_CANCEL
                || result === RESULT.DUPLICATE_RESERVE) {
                drawProgramArea(options._this);
            }
        });
    }


    function startTimeUpdateTimer (_this) {
        log.printDbg("startTimeUpdateTimer()");

        if (!updateTimer) {
            updateTimer = timeUpdateTimer(_this);
        }
    }

    function timeUpdateTimer (_this) {
        return setTimeout(function () {

            checkUpdateProgramList(_this);
            drawProgramArea(_this);

            updateTimer = timeUpdateTimer(_this);
        }, 60 * 1000);
    }

    function endTimeUpdateTimer () {
        log.printDbg("endTimeUpdateTimer()");

        if (updateTimer) {
            clearTimeout(updateTimer);
            updateTimer = null;
        }
    }

    function checkUpdateProgramList (_this) {
        var programList = _this.getProgrammesList();
        var length = programList.length;
        var now = (new Date()).getTime();

        for (var i = 0; i < length; i++) {
            var programmeList = programList[i];
            var curProgramList = programmeList[0];

            if (curProgramList) {
                var endTime = (curProgramList.startTime + curProgramList.duration) * 1000;

                if (endTime < now) {
                    _this.getProgrammesList(true);
                    break;
                }
            }
        }
    }

    function checkProgrammeList (_this) {
        log.printDbg("checkProgrammeList()");

        var isExist = false;
        var programList = _this.getProgrammesList();

        if (programList && programList.length > 0) {
            for (var i = 0; i < programList.length; i++) {
                var progList = programList[i];
                if (progList && progList.length > 1) {
                    isExist = true;
                    break;
                }
            }
        }

        return isExist;
    }

    KTW.ui.view.GenreChannelViewExtension = function(options) {
        KTW.ui.view.EpgView.call(this, options);

        this.curMenu = KTW.managers.data.MenuDataManager.searchMenu({
            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
            cbCondition: function (menu) {
                if (menu.id === options.menuId) {
                    return true;
                }
            }
        })[0];

        this.create = function() {
            log.printDbg("create()");

            if (!isCreate) {
                createElement(this);
                isCreate = true;
            }
        };

        this.initData = function () {
            this.initEpgData();
        };

        this.show = function (options) {
            log.printDbg("show()");

            div.css({visibility: "inherit"});

            if (!options || !options.resume) {
                this.parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R2);

                drawChannelArea(this);
                drawThumbnail(this);
                drawProgramArea(this);

                setFocusEpgArea(this);
            }
            else {
                if (div.hasClass("focus")) {
                    
                    // 2017.07.03 dhlee
                    // WEBIIIHOME-2588 이슈 수정 (resume 시에 PIP를 start 시켜줘야 함)
                    startPip(this);
                }
               
            }

            this.parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });

            historyDiv.css("display" , "none");

            startTimeUpdateTimer(this);
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            div.css({visibility: "hidden"});

            if (!options || !options.pause) {
                this.blur();
            }
            else {
            }

            endPip();

            endTimeUpdateTimer();
        };

        this.focus = function () {
            log.printDbg("focus()");


            var lang = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();


            if (this.curMenu) {
                var name = lang === "eng" ? this.curMenu.englishItemName : this.curMenu.name;
                if (!name || name.length === 0) {
                    name = this.curMenu.name;
                }
                historyDiv.css("display" , "");
                div.find("[name=history_title]").text(name);
            }

            div.addClass("focus");

            drawChannelName(this);
            drawProgramArea(this);

            subArea.css({visibility: "inherit"});

            maxPage = this.getChannelMaxPage();
            scroll.show({
                maxPage: maxPage,
                curPage: this.getChannelPage()
            });
            if (maxPage > 1) {
                subArea.find("[name=genre_channel_view_extension_change_page_guide_area]").css({visibility: "inherit"});
            }
            else {
                subArea.find("[name=genre_channel_view_extension_change_page_guide_area]").css({visibility: "hidden"});
            }

            var today = new Date();

            var hour = util.numToStr(today.getHours(), 2);
            var min = util.numToStr(today.getMinutes(), 2);;
            
            subArea.find("[name=hour_10]").attr({src: "images/icon/timenumber/time_number_" + hour[0] + ".png"});
            subArea.find("[name=hour_1]").attr({src: "images/icon/timenumber/time_number_" + hour[1] + ".png"});
            subArea.find("[name=min_10]").attr({src: "images/icon/timenumber/time_number_" + min[0] + ".png"});
            subArea.find("[name=min_1]").attr({src: "images/icon/timenumber/time_number_" + min[1] + ".png"});

            isFocusedAppArea = true;
            setFocusArea();

            startPip(this);
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.find("[name=history_title]").text("");

            div.removeClass("focus");

            focusProgramIdx = 0;
            setFocusEpgArea(this);

            this.initEpgData();
            drawChannelArea(this);
            drawThumbnail(this);
            drawProgramArea(this);

            setFocusEpgArea(this);

            subArea.css({visibility: "hidden"});

            isFocusedAppArea = true;

            scroll.hide();
            endPip();

            historyDiv.css("display" , "none");

        };

        this.remove = function () {
            log.printDbg("remove()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    consumed = keyLeft(this);
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    consumed = keyRed(this);
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    break;
                case KTW.KEY_CODE.CONTEXT:
                    // 2017.06.23 dhlee
                    // scroll에 포커스가 있는 상태이더라도 content menu가 동작 해야 한다. (UI 변경 요청 사항 반영)
                    showRelatedMenu(this);
                    /*
                    if (isFocusedAppArea) {
                        showRelatedMenu(this);
                    }
                    */
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return isChannelListExist(this);
        };
        this.isFocusAvailable = function () {
            return isChannelListExist(this);
        };

        this.getDataStatus = function () {
            if (!isChannelListExist(this)) {
                return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
            }
            else {
                return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
                //if (!checkProgrammeList(this)) {
                //    return KTW.ui.view.EpgView.DATA_STATUS.UPDATE;
                //}
                //else {
                //    return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
                //}
            }
        }
    };

    KTW.ui.view.GenreChannelViewExtension.prototype = new KTW.ui.view.EpgView();
    KTW.ui.view.GenreChannelViewExtension.prototype.constructor = KTW.ui.view.GenreChannelViewExtension;

})();