/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelGuideMenuView</code>
 *
 * 채널 가이드 메뉴 리스트
 * @author dj.son
 * @since 2016-08-08
 */

(function() {
    KTW.ui.view.ChannelGuideMenuView = function(parent) {

        var MAX_MENU_LENGTH = 9;

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var guideMenu = null;
        var curMenu = null;
        var curChildMenuIdx = 0;

        var parent = parent;
        var div = null;

        var focusArea = null;
        var focusMenuArw = null;
        var menuArea = null;
        var changePageGuideArea = null;
        var pageText = null;

        var arBackImg = null;

        var setEpgViewTimer = null;

        var isJump = false;

        function _create () {
            log.printDbg("create()");

            div = util.makeElement({
                tag: "<div />",
                attrs: {},
                parent: parent.div
            });

            var element = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "channel_guide_menu_list focus",
                    css: {
                        position: "absolute", width: 1920, height: 1080
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "title_icon",
                    src: "images/ar_history.png",
                    css: {
                        position: "absolute", left: 129, top: 52, width: 46, height: 46
                    }
                },
                parent: element
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    name: "title",
                    css: {
                        position: "absolute", left: 183, top: 56, "font-size": 40, color: "rgba(255, 255, 255, 0.1)",
                        "letter-spacing": -1.2, "font-family": "RixHead B"
                    }
                },
                parent: element
            });

            focusArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "focus_box_red_line",
                    css: {
                        position: "absolute", left: 133, top: 122, width:409, height:94, overflow: "visible"
                    }
                },
                parent: element
            });


            focusMenuArw =  util.makeElement({
                tag: "<img />",
                attrs: {
                    css: {
                        position: "absolute", left: (521-133), top: (149 - 122) , width:19,height:31
                    }
                },
                parent: focusArea
            });


            menuArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "channel_guide_menu_area",
                    css: {
                        position: "absolute", left: 133, top: 122, width: 409, height: 846, overflow: "hidden"
                    }
                },
                parent: element
            });
            for (var i = 0; i < MAX_MENU_LENGTH; i++) {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        class : "font_l cut",
                        css: {
                            position: "absolute", left : 1, top : (24 + (94*i)), width : 376, height: 44 ,
                        }
                    },
                    parent: menuArea
                });
            }

            arBackImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "ar_back",
                    src: "images/ar_back.png",
                    css: {
                        position: "absolute",left: 53, top: 522, width:22,height:36
                    }
                },
                parent: element
            });

            changePageGuideArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "channel_guide_menu_list_page_guide_area"
                },
                parent: element
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    name: "channel_guide_menu_list_arw",
                    src: "images/arw_menu_dw.png",
                    css: {
                        position: "absolute", left: 135, top: 997
                    }
                },
                parent: changePageGuideArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pageup.png",
                    css: {
                        position: "absolute", left: 191, top: 994
                    }
                },
                parent: changePageGuideArea
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/icon/icon_pagedown.png",
                    css: {
                        position: "absolute", left: 219, top: 994
                    }
                },
                parent: changePageGuideArea
            });
            pageText = util.makeElement({
                tag: "<span />",
                attrs: {
                    class : "font_l" ,
                    css: {
                        position: "absolute", left: 255, top: 998, "font-size": 24, color: "rgba(255, 255, 255, 0.4)",
                        "letter-spacing": -1.2
                    }
                },
                text: "",
                parent: changePageGuideArea
            });
        }

        function drawMenuList () {
            log.printDbg("drawMenuList()");

            var startIndex = Math.floor(curChildMenuIdx / MAX_MENU_LENGTH) * MAX_MENU_LENGTH;
            for (var i = 0; i < MAX_MENU_LENGTH; i++) {
                if (KTW.CONSTANT.IS_OTS === false && curMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                    menuArea.children().eq(i).css({width:(241 - 19 -12) });
                }else {
                    menuArea.children().eq(i).css({width:376 });
                }

                if (startIndex + i < curMenu.children.length) {
                    var menuName = parent.language === "eng" ? curMenu.children[startIndex + i].englishItemName : curMenu.children[startIndex + i].name;
                    if (!menuName || menuName === "") {
                        menuName = curMenu.children[startIndex + i].name;
                    }
                    menuArea.children().eq(i).css({visibility: ""}).text(menuName);
                }
                else {
                    menuArea.children().eq(i).css({visibility: "hidden"});
                }
            }

        };

        function drawFocusMenu () {
            log.printDbg("drawFocusMenu()");
            if (KTW.CONSTANT.IS_OTS === false && curMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                focusArea.css({width : 241});
                focusMenuArw.css({left : (241 - 19 -12)});
            }else {
                focusArea.css({width : 409});
                focusMenuArw.css({left : (521-133)});
            }
            focusArea.css({top: 122 + (94 * (curChildMenuIdx % MAX_MENU_LENGTH))});
        };

        function setGuideMenuTitle (menu) {
            if (menu) {
                var name = this.language === "eng" ? menu.englishItemName : menu.name;
                if (!name || name.length === 0) {
                    name = menu.name;
                }

                div.find("[name=title]").text(name);
            }
        };

        function initGuideMenuList () {
            log.printDbg("initGuideMenuList()");

            drawMenuList();
            drawFocusMenu();

            if (curMenu.children.length > MAX_MENU_LENGTH) {
                changePageGuideArea.css({visibility: ""});
            }
            else {
                changePageGuideArea.css({visibility: "hidden"});
            }
        }

        function setNextGuideMenuList () {
            log.printDbg("setNextGuideMenuList()");

            curMenu = curMenu.children[curChildMenuIdx];
            curChildMenuIdx = 0;

            initGuideMenuList();
            setFocus();

            setGuideMenuTitle(curMenu);
        }

        function setPretGuideMenuList () {
            log.printDbg("setPretGuideMenuList()");

            var preMenu = curMenu;
            curMenu = curMenu.parent;
            curChildMenuIdx = 0;
            var childrenLength = curMenu.children.length;
            for (var i = 0; i < childrenLength; i++) {
                if (preMenu.id == curMenu.children[i].id) {
                    curChildMenuIdx = i;
                    break;
                }
            }

            initGuideMenuList();
            setFocus();

            setGuideMenuTitle(curMenu);
        }

        function setFocus () {
            log.printDbg("setFocus()");
            menuArea.children().removeClass("font_b");
            menuArea.children().addClass("font_l");
            menuArea.children().removeClass("focus");

            log.printDbg("setFocus() index : " + (curChildMenuIdx % MAX_MENU_LENGTH));
            menuArea.children().eq(curChildMenuIdx % MAX_MENU_LENGTH).addClass("font_b");
            menuArea.children().eq(curChildMenuIdx % MAX_MENU_LENGTH).addClass("focus");


            var totalCount = (curMenu.children.length < 10 ? "0" : "") + curMenu.children.length;
            var focusIndex = ((curChildMenuIdx + 1)  < 10 ? "0" : "") + (curChildMenuIdx + 1);

            pageText.text(focusIndex + " / " + totalCount);
        }

        function setEpgView (options) {
            if (setEpgViewTimer) {
                clearTimeout(setEpgViewTimer);
                setEpgViewTimer = null;
            }

            if (options.bTimeout) {
                setEpgViewTimer = setTimeout(function () {
                    parent.getEpgContainerView().setEpgView(options);
                    setEpgViewTimer = null;
                }, 300);
            }
            else {
                parent.getEpgContainerView().setEpgView(options);
            }
        }

        function moveFocus (options) {
            log.printDbg("moveFocus()");

            var curPage = Math.floor(curChildMenuIdx / MAX_MENU_LENGTH);
            var nextPage = Math.floor(options.nextChildMenuIdx / MAX_MENU_LENGTH);

            curChildMenuIdx = options.nextChildMenuIdx;

            if (curPage !== nextPage) {
                drawMenuList();
            }
            drawFocusMenu();
            setFocus();

            var targetMenu = curMenu.children[curChildMenuIdx];
            var prevMenu = curMenu.children[options.prevChildMenuIdx];

            setEpgView({menu: targetMenu, direction: "up", bTimeout: true});

            if(curMenu.id == KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                // sw.nam 장르별 멀티채널의 경우에만 상/하 네비게이션 로그 적재
                if(targetMenu) {
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.ETC_MENU,
                        act: options.keyCode,
                        menuId: prevMenu.id,
                        menuName: prevMenu.name
                    });
                }

            }
        }

        function keyUp () {
            log.printDbg("keyUp()");

            var nextChildMenuIdx = curChildMenuIdx - 1;
            if (nextChildMenuIdx < 0) {
                nextChildMenuIdx = curMenu.children.length - 1;
            }

            moveFocus({
                prevChildMenuIdx: curChildMenuIdx,
                nextChildMenuIdx: nextChildMenuIdx,
                keyCode: KTW.KEY_CODE.UP
            });
        }

        function keyDown () {
            log.printDbg("keyDown()");

            var nextChildMenuIdx = curChildMenuIdx + 1;
            if (nextChildMenuIdx > curMenu.children.length - 1) {
                nextChildMenuIdx = 0;
            }

            moveFocus({
                prevChildMenuIdx: curChildMenuIdx,
                nextChildMenuIdx: nextChildMenuIdx,
                keyCode: KTW.KEY_CODE.DOWN
            });
        }

        function keyRed () {
            log.printDbg("keyRed()");

            if (curChildMenuIdx % MAX_MENU_LENGTH !== 0) {
                var curPage = Math.floor(curChildMenuIdx / MAX_MENU_LENGTH);
                var nextChildMenuIdx = curPage * MAX_MENU_LENGTH;

                moveFocus({
                    prevChildMenuIdx: curChildMenuIdx,
                    nextChildMenuIdx: nextChildMenuIdx,
                    keyCode: KTW.KEY_CODE.RED
                });
            }
            else {
                if (curMenu.children.length > MAX_MENU_LENGTH) {
                    var nextChildMenuIdx = curChildMenuIdx - MAX_MENU_LENGTH;

                    if (nextChildMenuIdx < 0) {
                        nextChildMenuIdx = (Math.floor(curMenu.children.length / MAX_MENU_LENGTH) * MAX_MENU_LENGTH) + (curChildMenuIdx % MAX_MENU_LENGTH);

                        if (nextChildMenuIdx > curMenu.children.length - 1) {
                            nextChildMenuIdx = curMenu.children.length - 1;
                        }
                    }

                    moveFocus({
                        prevChildMenuIdx: curChildMenuIdx,
                        nextChildMenuIdx: nextChildMenuIdx,
                        keyCode: KTW.KEY_CODE.RED
                    });
                }
            }
        }

        function keyBlue () {
            log.printDbg("keyBlue()");

            var curPage = Math.floor(curChildMenuIdx / MAX_MENU_LENGTH);
            var curPageLastIdx = ((curPage + 1) * MAX_MENU_LENGTH) - 1;

            if (curPageLastIdx >= curMenu.children.length) {
                curPageLastIdx = curMenu.children.length - 1;
            }

            if (curChildMenuIdx !== curPageLastIdx) {
                moveFocus({
                    prevChildMenuIdx: curChildMenuIdx,
                    nextChildMenuIdx: curPageLastIdx,
                    keyCode: KTW.KEY_CODE.RED
                });
            }
            else {
                if (curMenu.children.length > MAX_MENU_LENGTH) {
                    var curPage = Math.floor(curChildMenuIdx / MAX_MENU_LENGTH);
                    var maxPage = Math.ceil(curMenu.children.length / MAX_MENU_LENGTH);

                    var nextChildMenuIdx = curChildMenuIdx + MAX_MENU_LENGTH;

                    if (curPage === maxPage - 1) {
                        if(curChildMenuIdx == curMenu.children.length -1) {
                            // 마지막 페이지에 마지막 인덱스를 가리키고 있는 경우
                            // 첫번째 페이지의 가장 마지막 인덱스로 간다.
                            nextChildMenuIdx = MAX_MENU_LENGTH -1;
                        }else {
                            nextChildMenuIdx = curChildMenuIdx % MAX_MENU_LENGTH;
                        }

                    }

                    if (nextChildMenuIdx > curMenu.children.length - 1) {
                        nextChildMenuIdx = curMenu.children.length - 1;
                    }

                    moveFocus({
                        prevChildMenuIdx: curChildMenuIdx,
                        nextChildMenuIdx: nextChildMenuIdx,
                        keyCode: KTW.KEY_CODE.BLUE
                    });
                }
            }
        }

        function keyOk (keyCode) {
            log.printDbg("keyOk()");

            var targetMenu = curMenu.children[curChildMenuIdx];
            var epgContainerView = parent.getEpgContainerView();

            if (targetMenu.children && targetMenu.children.length > 0) {
                if (checkPopularChannelBlock(targetMenu.children[0])) {
                    return;
                }

                setNextGuideMenuList();

                setEpgView({menu: curMenu.children[curChildMenuIdx], direction: "left"});
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.ETC_MENU,
                    act: keyCode,
                    menuId: targetMenu.id,
                    menuName: targetMenu.name
                });
            }
            else {
                log.printDbg("keyOk() targetMenu.id : " + targetMenu.id + " , targetMenu.name : " + targetMenu.name);
                if(targetMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL) {
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.ETC_MENU,
                        act: keyCode,
                        menuId: targetMenu.id,
                        menuName: targetMenu.name
                    });

                    var mainChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();

                    if( KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(mainChannel) === true) {
                        KTW.ui.LayerManager.clearNormalLayer();

                        var params = {
                            focus_channel : mainChannel
                        };

                        KTW.ui.LayerManager.activateLayer({
                            obj: {
                                id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG,
                                type: KTW.ui.Layer.TYPE.NORMAL,
                                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                view : KTW.ui.view.AudioChannelFullEpgView,
                                linkage : false,
                                params: params
                            },
                            clear_normal: false,
                            visible: true,
                            cbActivate: function() {

                            }
                        });
                    }else {
                        var chList = epgContainerView.getAudioChannelList();
                        if(chList !== undefined && chList !== null && chList.length>0) {
                            var params = {
                                focus_channel : chList[0].data ,
                                call_back_relate_menu_right : callbackAudioFullEpgRelateMenuRight,
                                parent_view : epgContainerView
                            };

                            KTW.managers.service.AudioIframeManager.setType(KTW.managers.service.AudioIframeManager.DEF.TYPE.PREVIEW_AUDIO_FULL_EPG , chList[0].data);
                            KTW.managers.service.IframeManager.changeIframe(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);

                            KTW.ui.LayerManager.activateLayer({
                                obj: {
                                    id: KTW.ui.Layer.ID.AUDIO_CHANNEL_FULL_EPG,
                                    type: KTW.ui.Layer.TYPE.NORMAL,
                                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                    linkage : false,
                                    view : KTW.ui.view.AudioChannelFullEpgView,
                                    params: params
                                },
                                clear_normal: false,
                                visible: true,
                                cbActivate: function() {

                                }
                            });
                        }
                    }
                }else {
                    if (setEpgViewTimer) {
                        setEpgView({menu: curMenu.children[curChildMenuIdx]});
                    }

                    if (epgContainerView.isFocusAvailable()) {
                        KTW.managers.UserLogManager.collect({
                            type: KTW.data.UserLog.TYPE.ETC_MENU,
                            act: keyCode,
                            menuId: targetMenu.id,
                            menuName: targetMenu.name
                        });
                        parent.setFocusView(epgContainerView);
                    }
                }

            }
        }


        function callbackAudioFullEpgRelateMenuRight (menuId) {
            log.printDbg("callbackAudioFullEpgRelateMenuRight()");

            var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id === menuId) {
                        return true;
                    }
                }
            })[0];

            if (epgMenu) {
                var epgContainerView = parent.getEpgContainerView();
                epgContainerView.jumpTargetEpgView({
                    targetMenu: epgMenu
                });
            }
        }

        function keyBack () {
            log.printDbg("keyBack() curMenu.id : " + curMenu.id);

            if (curMenu.id == guideMenu.id) {
                KTW.ui.LayerManager.historyBack();
            }
            else {
                if (isJump === true && KTW.CONSTANT.IS_OTS === false && curMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                    KTW.ui.LayerManager.historyBack();
                }
                else {
                    setPretGuideMenuList();

                    setEpgView({menu: curMenu.children[curChildMenuIdx], direction: "right"});

                    if (KTW.CONSTANT.IS_OTS === false && curMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                        setGuideMenuTitle(curMenu.parent);
                    }
                }
            }
        }

        function getTargetMenuIdx (target) {
            log.printDbg("getTargetMenuIdx()");

            var targetList = [guideMenu];

            while (targetList.length > 0) {
                var parentMenu = targetList.shift();

                if (parentMenu.children) {
                    var childrenLength = parentMenu.children.length;

                    for (var i = 0; i < childrenLength; i++) {
                        if (parentMenu.children[i].id === target.id) {
                            return i;
                        }
                    }

                    targetList = targetList.concat(parentMenu.children);
                }
            }
        }

        function checkPopularChannelBlock (targetMenu) {
            log.printDbg("checkPopularChannelBlock()");

            var isBlock = false;

            if (KTW.CONSTANT.IS_OTS === false && targetMenu && targetMenu.parent && targetMenu.parent.id === KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                if (KTW.managers.service.StateManager.isVODPlayingState()) {
                    var isCalled = false;
                    var popupData = {
                        arrMessage: [{
                            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                            message: ["알림"],
                            cssObj: {}
                        }, {
                            type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                            message: KTW.ERROR.EX_CODE.HOME.EX001.message,
                            cssObj: {}
                        }],
                        arrButton: [{id: "ok", name: "확인"}],
                        cbAction: function (buttonId) {
                            if (!isCalled) {
                                isCalled = true;

                                if (buttonId) {
                                    KTW.ui.LayerManager.deactivateLayer({
                                        id: "RealTimeLayerBlockGuidePopup",
                                        remove: true
                                    });
                                }
                            }
                        }
                    };

                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: "RealTimeLayerBlockGuidePopup",
                            type: KTW.ui.Layer.TYPE.POPUP,
                            priority: KTW.ui.Layer.PRIORITY.POPUP,
                            view : KTW.ui.view.popup.BasicPopup,
                            params: {
                                data : popupData
                            }
                        },
                        visible: true,
                        cbActivate: function () {}
                    });

                    isBlock = true;
                }
            }

            return isBlock;
        }


        this.create = function () {
            log.printDbg("create()");

            _create();
        };

        this.getDiv = function () {
            return div;
        };

        this.show = function (options) {
            log.printDbg("show()");

            if (!options || !options.resume) {
                guideMenu = parent.getGuideMenu();

                if (options && options.jumpObj) {
                    log.printDbg("jumpObj exist... so return...");
                    isJump = true;
                    return;
                }

                isJump = false;

                curMenu = guideMenu;
                curChildMenuIdx = 0;

                initGuideMenuList();
                setFocus();

                setEpgView({menu: curMenu.children[curChildMenuIdx], direction: ""});

                div.children().css({visibility: "inherit"});
            }
            
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            /**
             * [dj.son] hide 될때, setEpgViewTimer clear 하도록 수정
             */
            if (setEpgViewTimer) {
                clearTimeout(setEpgViewTimer);
            }

            setEpgViewTimer = null;
        };

        this.focus = function () {
            log.printDbg("focus()");

            div.children().addClass("focus");


            setGuideMenuTitle(curMenu);

            div.children().css({visibility: "inherit"});

            arBackImg.css("display" , "");

            focusArea.removeClass("focus_box_dim_line");
            focusArea.addClass("focus_box_red_line");

            focusMenuArw.attr("src", "images/arw_menu_focus.png");
        };

        this.blur = function () {
            log.printDbg("blur()");

            div.children().removeClass("focus");
            arBackImg.css("display" , "none");
            if (parent.getEpgContainerView().isFullScreen()) {
                div.children().css({visibility: "hidden"});
            }
            else {
                div.children().css({visibility: "inherit"});
            }

            focusArea.removeClass("focus_box_red_line");
            focusArea.addClass("focus_box_dim_line");

            focusMenuArw.attr("src", "images/arw_menu_dim.png");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RED:
                    keyRed();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BLUE:
                    keyBlue();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                case KTW.KEY_CODE.OK:
                    keyOk(keyCode);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.BACK:
                    keyBack();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.FULLEPG :
                case KTW.KEY_CODE.FULLEPG_OTS :
                    var epgMenu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                        cbCondition: function (menu) {
                            if (menu.id === KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                                return true;
                            }
                        }
                    })[0];

                    if (epgMenu) {
                        parent.getEpgContainerView().jumpTargetEpgView({
                            targetMenu: epgMenu
                        });
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.PLAY:
                    parent.getEpgContainerView().controlKey(keyCode);
                    break;
            }

            return consumed;
        };

        this.jumpTargetMenu = function (jumpObj) {
            log.printDbg("jumpTargetMenu()");

            if (checkPopularChannelBlock(jumpObj.targetMenu)) {
                return;
            }

            curMenu = jumpObj.targetMenu.parent;
            curChildMenuIdx = getTargetMenuIdx(jumpObj.targetMenu);

            setEpgView({
                menu: curMenu.children[curChildMenuIdx],
                isJump: true,
                isLayerHide: jumpObj.isLayerHide,
                isHotKey: jumpObj.isHotKey
            });

            var epgContainerView = parent.getEpgContainerView();

            if (!epgContainerView.isFullScreen()) {
                div.children().css({visibility: "inherit"});

                initGuideMenuList();
                setFocus();

                /**
                 * [dj.son] 실시간 인기 채널 하위 메뉴로 점프시, 포커스는 메뉴 리스트에 위치해야 함 (UX 팀 검수사항)
                 */
                if (epgContainerView.isFocusAvailable() && jumpObj.targetMenu.parent.id !== KTW.managers.data.MenuDataManager.MENU_ID.REAL_TIME_CHANNEL) {
                    //listVisibleAnimation({direction: "right"});
                    div.children().removeClass("focus");
                    parent.setFocusView(epgContainerView);
                }
                else {
                    parent.setFocusView(this);
                }
            }
            else {
                div.children().css({visibility: "hidden"});
                initGuideMenuList();
                setFocus();

                parent.setFocusView(epgContainerView);
            }
        }

        this.isJumpAvailable = function (menuId) {
            if (curMenu) {
                var targetMenu = curMenu.children[curChildMenuIdx];
                var MENU_ID = KTW.managers.data.MenuDataManager.MENU_ID;
                if (targetMenu.id === menuId
                    && (targetMenu.id === MENU_ID.GENRE_CHANNEL || targetMenu.id === MENU_ID.PROGRAM_SEARCH || targetMenu.id === MENU_ID.BROADCAST_RESERVATION_LIST)) {
                    return false;
                }
            }

            return true;
        }
    }
})();