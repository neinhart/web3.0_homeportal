/**
 * <code>ImageAppView</code>
 *
 * - 실시간 인기 채널 & 4채널 동시채널 view
 *
 * @author sw.nam
 * @since 2016-12-05
 */

(function() {

    var FOUR_CHANNEL_VIEW = "T000000000003";
    var CURRENT_POPULAR_CHANNEL = "T000000000004";

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var div = null;

    var isCreate = false;

    /*
     * [sw.nam] 기본 Element 생성
     *
     * */
    function createElement (_this) {

        div = util.makeElement({
            tag: "<div />",
            attrs: {
                class: "image_app_view",
                css: {
                    position: "absolute", left: 576, top: 0, width: 919, height: 1080
                }
            },
            parent: _this.parent.getEpgContainerDiv()
        });

        /**
         * jjh1117 2017/12/20
         *  background Image 추가 해야 함.
         */

        var container = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "image_app_view_container",
                css: {
                    position: "absolute"
                    /*"-webkit-transition": "-webkit-transform 0.5s"*/
                }
            },
            parent: div
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "appImageArea",
                src: "images/app_4ch_w486.png",
                css: {
                    position: "absolute"
                }
            },
            parent: container
        });



        util.makeElement({
            tag: "<div />",
            attrs: {
                id : "appBtnAgrea",
                css: {
                    position: "absolute"
                }
            },
            parent: container
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id : "appTextArea",
                css: {
                    position: "absolute",left: (636-636), height:32 ,
                    "text-align": "center", "font-size": 30, "font-family": "RixHead L", "letter-spacing": -1.5
                    /*"-webkit-transition": "color 0.5s"*/
                }
            },
            text: "바로가기",
            parent: container
        });

    }

    function keyOk (_this) {
        log.printDbg("keyOk()");

        if (_this.menuId == FOUR_CHANNEL_VIEW) {
            KTW.managers.service.EpgRelatedMenuManager.activateFourChannel();
        }
        else if (_this.menuId == CURRENT_POPULAR_CHANNEL) {
            if(KTW.CONSTANT.IS_OTS === true) {
                KTW.managers.service.EpgRelatedMenuManager.activateRealTimeChannel();
            }else {
                _this.parent.jumpPopularMenu();
            }

        }
    }


    KTW.ui.view.ImageAppView = function(options) {
        this.parent = options.parent;
        this.menuId = options.menuId;

        this.create = function () {
            log.printDbg("create()");

            if (!isCreate) {
                createElement(this);

                isCreate = true;
            }
        };

        this.getDiv = function () {
            return div;
        };

        this.initData = function () {};

        this.show = function (options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            this.parent.setBackground(KTW.ui.layer.ChannelGuideLayer.BG_MODE.R1);

            if (!options || !options.resume) {
                if (this.menuId == CURRENT_POPULAR_CHANNEL) {
                    log.printDbg("CURRENT_POPULAR_CHANNEL");

                    div.find("#appImageArea").attr("src", "images/app_rank_w486.png");
                } else if (this.menuId == FOUR_CHANNEL_VIEW) {
                    log.printDbg("FOUR_CHANNEL_VIEW");

                    div.find("#appImageArea").attr("src", "images/app_4ch_w486.png");
                } else {
                    // do what it wants
                }

                div.removeClass("focus");
            }

            this.parent.setVideoScreenPosition({
                mode: KTW.ui.layer.ChannelGuideLayer.VBO_MODE.FULL_SCREEN
            });
            
            div.css({visibility: "inherit"});
        };

        this.hide = function (options) {
            log.printDbg("hide(" + log.stringify(options) + ")");

            if (!options || !options.pause) {
                div.css({visibility: "hidden"});
            }
            else {
            }
        };
        this.focus = function () {
            log.printDbg("focus()");

            div.addClass("focus");
        };

        this.blur = function () {
            log.printDbg("blur()");
            div.removeClass("focus");
        };

        this.remove = function () {};

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:
                case KTW.KEY_CODE.RIGHT:
                case KTW.KEY_CODE.CONTEXT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk(this);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    this.parent.setFocusGuideMenu();
                    break;
            }

            return consumed;
        };

        this.isFullScreen = function () {
            return ;
        };

        this.isFocusAvailable = function () {
            return true;
        };

        this.getDataStatus = function () {
            return KTW.ui.view.EpgView.DATA_STATUS.AVAILABLE;
        };
    }
})();