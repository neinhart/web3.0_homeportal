/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>BackIframeView.js</code>
 *
 * 채널 상에서 연령제한, 미가입, 제한 채널 등의 상황이 발생했을 때 background에 보여지는 image를 제공한다.
 * OTS 무비초이스 채널의 경우 무비초이스 채널의 기본 정보를 Background image 위에 보여준다.
 * Audio 채널의 경우 곡 정보 등을 보여준다.
 * VOD Loading 배경 이미지를 보여준다.
 *
 * @author dhlee
 * @since 2016-07-26
 */

"use strict";

KTW.ui.view.BackIframeView = function BackIframeView(parent) {
    KTW.ui.View.call(this);

    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var last_back_iframe_type = KTW.managers.service.IframeManager.DEF.TYPE.NONE;
    var last_iframe_mode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;
    var parent = parent;

    /**
     * FullMode
     */
    var fullModeDiv = null;
    var imgfullModeBg = null;
    var imgforegroundBg = null;
    var textBlockedTitle = null;
    var textBlockedEnglishTitle = null;
    var textBlockedComment1 = null;
    var textBlockedComment2 = null;



    var textUnSubscribedTitle = null;
    var textUnSubscribedEnglishTitle = null;
    var textUnSubscribedComment1 = null;
    var textUnSubscribedComment2 = null;
    var textUnSubscribedComment3 = null;


    /**
     * 무비초이스 채널 정보
     */
    var movieChoiceDiv = null;
    var movieChoiceProgramDiv = null;
    var movieChoiceTextProgram = null;
    var movieChoiceTextPlayTime = null;
    var movieChoiceTextDuration = null;
    var movieChoiceTextBuy = null;
    var movieChoiceImgAge = null;
    var movieChoiceImgHd = null;
    var movieChoiceTextDesc = [];
    var movieChoiceNoProgramDiv = null;

    /**
     * SubHomeMode
     */
    var subHomeModeDiv = null;
    var subHomeIframeImg = null;
    /**
     * FullEpgMode
     */
    var fullEpgModeDiv = null;
    var fullEpgIframeImg = null;


    this.create = function() {
        log.printDbg("create()");

        var parent_div = parent.div;
        parent_div.attr({class: "arrange_frame"});

        fullModeDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 0, top: 0,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                }
            },
            parent: parent_div
        });

        // 전체 크기(1920 * 1080)의 iframe image element
        imgfullModeBg = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "back_iframe_full_bg",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH,
                    height: KTW.CONSTANT.RESOLUTION.HEIGHT
                }
            },
            parent: fullModeDiv
        });

        textBlockedTitle = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "blocked_title",
                class: "font_b",
                css: {
                    position: "absolute", left: 0, top: 255, width: 1920, height: 93,
                    color: "rgba(255, 255, 255, 1)", "font-size": 90 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textBlockedEnglishTitle = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "blocked_english_title",
                class: "font_b",
                css: {
                    position: "absolute", left: 0, top: 357, width: 1920, height: 30,
                    color: "rgba(255, 255, 255, 0.5)", "font-size": 28 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textUnSubscribedTitle = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "unsubscribed_title",
                class: "font_b",
                css: {
                    position: "absolute", left: 0, top: 255, width: 1920, height: 93,
                    color: "rgba(255, 255, 255, 1)", "font-size": 90 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textUnSubscribedEnglishTitle = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "unsubscribed_english_title",
                class: "font_b",
                css: {
                    position: "absolute", left: 0, top: 357, width: 1920, height: 30,
                    color: "rgba(255, 255, 255, 0.5)", "font-size": 28 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        imgforegroundBg = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "back_iframe_full_bg_foreground_bg",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH,
                    height: 700
                }
            },
            parent: fullModeDiv
        });



        textBlockedComment1 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "blocked_comment1",
                class: "font_b",
                css: {
                    position: "absolute", left: 0, top: 432, width: 1920, height: 32,
                    color: "rgba(255, 255, 255, 0.8)", "font-size": 29 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textBlockedComment2 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "blocked_comment2",
                class: "font_l",
                css: {
                    position: "absolute", left: 0, top: 474, width: 1920, height: 32,
                    color: "rgba(255, 255, 255, 0.8)", "font-size": 29 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textUnSubscribedComment1 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "unsubscribed_comment1",
                class: "font_m",
                css: {
                    position: "absolute", left: 0, top: 431, width: 1920, height: 32,
                    color: "rgba(255, 255, 255, 0.8)", "font-size": 30 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textUnSubscribedComment2 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "unsubscribed_comment2",
                class: "font_m",
                css: {
                    position: "absolute", left: 0, top: 476, width: 1920, height: 32,
                    color: "rgba(255, 255, 255, 0.8)", "font-size": 30 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textUnSubscribedComment3 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "unsubscribed_comment3",
                class: "font_l",
                css: {
                    position: "absolute", left: 0, top: 523, width: 1920, height: 29,
                    color: "rgba(255, 255, 255, 0.8)", "font-size": 27 , "text-align" : "center"
                }
            },
            text: "",
            parent: fullModeDiv
        });


        subHomeModeDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 0, top: 0, width: 1920, height: 1080
                }
            },
            parent: parent_div
        });

        // Subhome PIG 크기(555, 315)의 iframe image element
        subHomeIframeImg = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "back_iframe_subhome_pig_bg",
                css: {
                    position: "absolute",
                    left: KTW.CONSTANT.SUBHOME_PIG_STYLE.LEFT,
                    top: KTW.CONSTANT.SUBHOME_PIG_STYLE.TOP,
                    width: KTW.CONSTANT.SUBHOME_PIG_STYLE.WIDTH,
                    height: KTW.CONSTANT.SUBHOME_PIG_STYLE.HEIGHT
                }
            },
            parent: subHomeModeDiv
        });

        fullEpgModeDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 0, top: 0, width: 1920, height: 1080
                }
            },
            parent: parent_div
        });

        // Full Epg PIG 크기(555, 315)의 iframe image element
        fullEpgIframeImg = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "back_iframe_fullepg_pig_bg",
                css: {
                    position: "absolute",
                    left: KTW.CONSTANT.FULL_EPG_PIG_STYLE.LEFT,
                    top: KTW.CONSTANT.FULL_EPG_PIG_STYLE.TOP,
                    width: KTW.CONSTANT.FULL_EPG_PIG_STYLE.WIDTH,
                    height: KTW.CONSTANT.FULL_EPG_PIG_STYLE.HEIGHT
                }
            },
            parent: fullEpgModeDiv
        });



        //createTextView();

        if (KTW.CONSTANT.IS_OTS === true ) {
            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "back_iframe_ots_not_subscribed",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: KTW.CONSTANT.RESOLUTION.WIDTH,
                        height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        visibility: "hidden"
                    }
                },
                parent: parent_div
            });
        }
        _createOTSMovieChoiceView(fullModeDiv);

        _hideFullMode();
        _hideSubHomeMode();
        _hideFullEpgMode();
    };

    this.show = function() {
        log.printDbg("show()");

        var param = parent.getParams().data;
        var isupdate = param.isupdate;
        var backIframe = param.iframe;
        var iframeMode = param.iframemode;
        var type = param.type;


        if(isupdate === undefined || isupdate === null) {
            if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
                _hideSubHomeMode();
                _hideFullEpgMode();
            }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE) {
                _hideFullMode();
                _hideFullEpgMode();

            }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE) {
                _hideFullMode();
                _hideSubHomeMode();
            }

            log.printDbg("show(), last_back_iframe_type = " + last_back_iframe_type);
            log.printDbg("show(), backIframe = " + backIframe + " , iframeMode = " + iframeMode + " , type : " + type);

            if (backIframe) {
                if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
                    _showFullMode(backIframe , type);
                }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.SUB_HOME_MODE) {
                    _showSubHomeMode(backIframe,type);
                }else if(iframeMode === KTW.managers.service.IframeManager.DEF.MODE.FULL_EPG_MODE) {
                    _showFullEPgMode(backIframe , type);
                }

                last_back_iframe_type = type;
                last_iframe_mode = iframeMode;
            }

        }else {
            if(last_iframe_mode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
                _updateAudioChannelProgramData();
            }
        }
    };


    this.hide = function() {
        log.printDbg("hide()");

    };

    

    function _hideFullMode() {
        log.printDbg("_hideFullMode()");

        _hideMovieChoiceInfo();
        fullModeDiv.css("display", "none");
    }

    function _hideSubHomeMode() {
        log.printDbg("_hideSubHomeMode()");
        subHomeModeDiv.css("display", "none");

    }

    function _hideFullEpgMode() {
        log.printDbg("_hideFullEpgMode()");
        fullEpgModeDiv.css("display", "none");
    }

    function _showFullMode(iframe , type) {
        log.printDbg("_showFullMode()");
        var isForegroundText = false;
        var backgroundIframe = "";
        var foregroundIframe = "";

        isForegroundText = iframe.is_foreground_text;
        backgroundIframe = iframe.background_iframe;
        foregroundIframe = iframe.foreground_iframe;

        /**
         * 미가입 Movice Choice
         */
        if(type !== KTW.managers.service.IframeManager.DEF.TYPE.SKY_CHOICE) {
            movieChoiceDiv.css("display", "none");
        }

        /**
         * Blocked 채널 문구
         */
        textBlockedTitle.css("display", "none");
        textBlockedEnglishTitle.css("display" , "none");
        textBlockedComment1.css("display", "none");
        textBlockedComment2.css("display", "none");

        textUnSubscribedTitle.css("display", "none");
        textUnSubscribedEnglishTitle.css("display" , "none");
        textUnSubscribedComment1.css("display", "none");
        textUnSubscribedComment2.css("display", "none");
        textUnSubscribedComment3.css("display", "none");

        if(type !== KTW.managers.service.IframeManager.DEF.TYPE.NOT_SUBSCRIBED) {
            imgforegroundBg.css("display" , "none");
        }

        /**
         * OTV Default Iframe foregroud Image
         */
        if(type === KTW.managers.service.IframeManager.DEF.TYPE.AUDIO && KTW.CONSTANT.IS_OTS) {
            imgfullModeBg.css("display" , "none");
        }else {
            imgfullModeBg.attr("src", backgroundIframe);
            imgfullModeBg.css("display" , "");
        }



        var blockedTitle = null;
        var blockedEnglishTitle = null;
        var blockedComment1 = null;
        var blockedComment2 = null;

        var unsubscribedTitle = null;
        var unsubscribedEnglishTitle = null;
        var unsubscribedComment1 = null;
        var unsubscribedComment2 = null;
        var unsubscribedComment3 = null;


        switch (type) {
            case KTW.managers.service.IframeManager.DEF.TYPE.NONE :
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED : // 시청 연령제한
                blockedTitle = "시청연령 제한 프로그램";
                blockedEnglishTitle = "Restricted Program";
                blockedComment1 = "시청을 원하시면 비밀번호를 입력해 주세요";
                blockedComment2 = "설정>자녀안심설정에서 설정 및 변경이 가능합니다";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED_KIDS_MODE : // 시청 연령제한 키즈모드
                blockedTitle = "시청연령 제한 프로그램";
                blockedComment1 = "현재 키즈모드로 설정되어 있어 12세 이하 프로그램까지 시청 가능합니다";
                blockedComment2 = "연령이 제한된 프로그램을 시청하시려면 키즈>부모세상 에서 키즈모드를 종료해 주세요";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.TIME_RESTRICTED : // 시청 시간 제한
                blockedTitle = "시청시간 제한";
                blockedEnglishTitle = "Time Restriction";
                blockedComment1 = "시청을 원하시면 비밀번호를 입력해 주세요";
                blockedComment2 = "설정>자녀안심설정에서 설정 및 변경이 가능합니다";
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.NOT_SUBSCRIBED : // 미가입
                if(isForegroundText) {
                    unsubscribedTitle = "미가입 채널";
                    unsubscribedEnglishTitle = "Unsubscribed Channel";
                    unsubscribedComment1 = "가입이 필요한 유료채널입니다";
                    unsubscribedComment2 = "채널에 가입하면 시청이 가능합니다";
                    unsubscribedComment3 = "가입/해지 문의 국번 없이 100번(평일 09-18시 토요일 09시-12시)";
                }

                if(foregroundIframe !== undefined && foregroundIframe !== null && foregroundIframe.length>0) {
                    imgforegroundBg.attr("src" , foregroundIframe);
                    imgforegroundBg.css("display" , "");
                }else {
                    imgforegroundBg.css("display" , "none");
                }

                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.BLOCKED : // 시청제한 채널
                blockedTitle = "시청채널 제한";
                blockedEnglishTitle = "Blocked Channel";
                blockedComment1 = "시청을 원하시면 비밀번호를 입력해 주세요";
                blockedComment2 = "설정>자녀안심설정에서 설정 및 변경이 가능합니다";

                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.VOD_LOADING : // VOD
                break;
            case KTW.managers.service.IframeManager.DEF.TYPE.SKY_CHOICE : // SKY_CHOICE
                _showMovieChoiceInfo();
                break;
        }

        if(blockedTitle !== null) {
            textBlockedTitle.text(blockedTitle);
            if(type === KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED_KIDS_MODE) {
                textBlockedTitle.css({top:295-65});
            }else {
                textBlockedTitle.css({top:255});
            }
            textBlockedTitle.css("display", "");
        }

        if(blockedEnglishTitle !== null) {
            textBlockedEnglishTitle.text(blockedEnglishTitle);
            textBlockedEnglishTitle.css("display", "");
        }

        if(blockedComment1 !== null) {
            textBlockedComment1.text(blockedComment1);
            if(type === KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED_KIDS_MODE) {
                textBlockedComment1.css({top:432-65});
            }else {
                textBlockedComment1.css({top:432});
            }
            textBlockedComment1.css("display", "");
        }

        if(blockedComment2 !== null) {
            textBlockedComment2.text(blockedComment2);
            textBlockedComment2.css("display", "");

            if(type === KTW.managers.service.IframeManager.DEF.TYPE.PR_BLOCKED_KIDS_MODE) {
                textBlockedComment2.css({top:474-65});
            }else {
                textBlockedComment2.css({top:474});
            }
            textBlockedComment1.css("display", "");
        }

        if(unsubscribedTitle !== null) {
            textUnSubscribedTitle.text(unsubscribedTitle);
            textUnSubscribedTitle.css("display", "");
        }

        if(unsubscribedEnglishTitle !== null) {
            textUnSubscribedEnglishTitle.text(unsubscribedEnglishTitle);
            textUnSubscribedEnglishTitle.css("display", "");
        }

        if(unsubscribedComment1 !== null) {
            textUnSubscribedComment1.text(unsubscribedComment1);
            textUnSubscribedComment1.css("display", "");
        }

        if(unsubscribedComment2 !== null) {
            textUnSubscribedComment2.text(unsubscribedComment2);
            textUnSubscribedComment2.css("display", "");
        }

        if(unsubscribedComment3 !== null) {
            textUnSubscribedComment3.text(unsubscribedComment3);
            textUnSubscribedComment3.css("display", "");
        }

        fullModeDiv.css("display", "");
    }

    function _showSubHomeMode(iframe , type) {
        subHomeIframeImg.attr("src", iframe);
        subHomeIframeImg.css("display", "");

        subHomeModeDiv.css("display", "");
    }

    function _showFullEPgMode(iframe , type) {
        fullEpgIframeImg.attr("src", iframe);
        fullEpgIframeImg.css("display", "");

        fullEpgModeDiv.css("display", "");
    }

    function _createOTSMovieChoiceView(parent_div) {
        log.printDbg("_createOTSMovieChoiceView()");

        movieChoiceDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "movie_choice_info",
                css: {
                    position: "absolute",
                    left: 292,
                    top: 123,
                    width: 1630,
                    height: 551,
                    display: "none"
                }
            },
            parent: parent_div
        });

        // movice choice channel logo image
        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "movie_choice_logo_img",
                src: KTW.CONSTANT.IMAGE_PATH + "iframe/logo_moviechoice.png",
                css: {
                    position: "absolute",
                    left: 292-292,
                    top: 123-123,
                    display: ""
                }
            },
            parent: movieChoiceDiv
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "movie_choice_line_1",
                css: {
                    position: "absolute",
                    left: 292-292,
                    top: 182-123,
                    width: 1628,
                    height: 2,
                    "background-color": "rgba(255, 255, 255, 0.2)"
                }
            },
            parent: movieChoiceDiv
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "movie_choice_line_2",
                css: {
                    position: "absolute",
                    left: 292-292,
                    top: 604-123,
                    width: 1628,
                    height: 2,
                    "background-color": "rgba(255, 255, 255, 0.2)"
                }
            },
            parent: movieChoiceDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "rcu_order_text",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 293-292,
                    top: 628-123,
                    "font-size": 27,
                    color: "rgba(255, 255, 255, 0.7)",
                    "letter-spacing":-1.35
                }
            },
            text: "리모컨주문 : 확인버튼",
            parent: movieChoiceDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "tel_order_text",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 611-292,
                    top: 628-123,
                    "font-size": 27,
                    color: "rgba(255, 255, 255, 0.7)",
                    "letter-spacing":-1.35
                }
            },
            text: "전화주문 : 1577-3005",
            parent: movieChoiceDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "smart_card_text",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 928-292,
                    top: 628-123,
                    "font-size": 27,
                    color: "rgba(255, 255, 255, 0.7)",
                    "letter-spacing":-1.35

                }
            },
            text: "스마트카드번호 : " + KTW.SMARTCARD_ID,
            parent: movieChoiceDiv
        })

        _createProgramMovieChoiceInfo(movieChoiceDiv);
        _createNoProgramMovieChoiceInfo(movieChoiceDiv);
    }

    /**
     * OTS movie choice 채널에 편성된 프로그램 정보를 구성한다.
     *
     * @param parent_div
     * @private
     */
    function _createProgramMovieChoiceInfo(parent_div) {
        log.printDbg("_createProgramMovieChoiceInfo()");

        movieChoiceProgramDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "movie_choice_program",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 184-123,
                    width : 1630,
                    height:420,
                    display: "none"
                }
            },
            parent: parent_div
        });

        // 프로그램 제목
        movieChoiceTextProgram = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "program_name",
                class: "font_m",
                css: {
                    position: "absolute",
                    left: 292-292,
                    top: 227 - 184,
                    "font-size": 60,
                    color: "rgb(255, 255, 255)",
                    "letter-spacing":-3.0
                }
            },
            text : "",
            parent: movieChoiceProgramDiv
        });

        // 프로그램 편성 시간
        movieChoiceTextPlayTime = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "program_time",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 292-292,
                    top: 307 - 184,
                    width: 188,
                    height: 29,
                    "font-size": 27,
                    color: "rgba(255, 255, 255, 0.7)",
                    "letter-spacing":-1.35
                }
            },
            text: "",
            parent: movieChoiceProgramDiv
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "delimiter_line_1",
                css: {
                    position: "absolute",
                    left: 182,
                    top: 309 - 184 + 2,
                    width: 2,
                    height: 18,
                    "background-color": "rgba(255, 255, 255, 0.3)"
                }
            },
            parent: movieChoiceProgramDiv
        });

        movieChoiceTextDuration = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "running_time",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 182+2,
                    top: 306 - 184,
                    width: 93,
                    height: 29,
                    "font-size": 27,
                    color: "rgba(255, 255, 255, 0.7)",
                    "letter-spacing":-1.35,
                    "text-align": "center"
                }
            },
            text: "",
            parent: movieChoiceProgramDiv
        })

        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "delimiter_line_2",
                css: {
                    position: "absolute",
                    left: 277,
                    top: 309 - 184 + 2,
                    width: 2,
                    height: 18,
                    "background-color": "rgba(255, 255, 255, 0.3)"
                }
            },
            parent: movieChoiceProgramDiv
        });

        // 유/무료 텍스트
        // TODO: 무료도 있는지 확인: 웹 홈포털 코드에서는 무조건 유료이므로 일단은 고정 text로 제공
        movieChoiceTextBuy = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "fee_text",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 279,
                    top: 306 - 184,
                    width:76,
                    height:29,
                    "font-size": 27,
                    color: "rgba(255, 255, 255, 0.7)",
                    "letter-spacing":-1.35,
                    "text-align": "center"
                }
            },
            text: "유료",
            parent: movieChoiceProgramDiv
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                id: "delimiter_line_3",
                css: {
                    position: "absolute",
                    left: 353,
                    top: 309 - 184 + 2,
                    width: 2,
                    height: 18,
                    "background-color": "rgba(255, 255, 255, 0.3)"
                }
            },
            parent: movieChoiceProgramDiv
        });

        movieChoiceImgAge = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "rating_img",
                css: {
                    position: "absolute",
                    left: 371,
                    top: 300 - 184,
                    width:37,
                    height:37
                }
            },
            parent: movieChoiceProgramDiv
        });

        movieChoiceImgHd = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "definition_img",
                css: {
                    position: "absolute",
                    left: 419,
                    top: 302 - 184,
                    width:45,
                    height:32
                }
            },
            parent: movieChoiceProgramDiv
        });

        // 줄거리 (최대 4줄)
        // TODO 마지막 줄에서 더 긴 텍스트가 있는 경우 말줄임(...)으로 처래햐아 함.
        movieChoiceTextDesc[movieChoiceTextDesc.length] = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "program_desc1",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 371 - 184,
                    width: 1237,
                    height: 32,
                    "font-size": 30,
                    color: "rgba(255, 255, 255, 0.7)","letter-spacing":-1.5
                }
            },
            parent: movieChoiceProgramDiv
        });

        movieChoiceTextDesc[movieChoiceTextDesc.length] = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "program_desc2",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 371 - 184 + 32 + 19,
                    width: 1237,
                    height: 32,
                    "font-size": 30,
                    color: "rgba(255, 255, 255, 0.7)","letter-spacing":-1.5
                }
            },
            parent: movieChoiceProgramDiv
        });

        movieChoiceTextDesc[movieChoiceTextDesc.length] = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "program_desc3",
                class: "font_l",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 371 - 184 + 32 + 19 + 32 + 19,
                    width: 1237,
                    height: 32,
                    "font-size": 30,
                    color: "rgba(255, 255, 255, 0.7)","letter-spacing":-1.5
                }
            },
            parent: movieChoiceProgramDiv
        });

        movieChoiceTextDesc[movieChoiceTextDesc.length] = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "program_desc4",
                class: "font_l cut",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 371 - 184 + 32 + 19 + 32 + 19 + 32 + 19,
                    width: 1237,
                    height: 32,
                    "font-size": 30,
                    color: "rgba(255, 255, 255, 0.7)","letter-spacing":-1.5
                }
            },
            parent: movieChoiceProgramDiv
        });




    }

    /**
     * OTS movie choice 채널에 편성된 프로그램이 없을 때의 정보를 구성한다.
     * TODO GUI가 릴리즈 되지 않음.
     *
     * @param parent_div
     * @private
     */
    function _createNoProgramMovieChoiceInfo(parent_div) {
        log.printDbg("_createNoProgramMovieChoiceInfo()");

        movieChoiceNoProgramDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "movie_choice_no_program",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 184-123,
                    width : 1630,
                    height:420,
                    display: "none"
                }
            },
            parent: parent_div
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "",
                class: "font_m",
                css: {
                    position: "absolute",
                    left: 292-292,
                    top: 227 - 184,
                    "font-size": 60,
                    color: "rgb(255, 255, 255)","letter-spacing":-3.0
                }
            },
            text : "프로그램 정보가 없습니다",
            parent: movieChoiceNoProgramDiv
        });
    }

    /**
     * OTS Movie Choice 채널의 프로그램 정보를 보여준다.
     *
     * @private
     */
    function _showMovieChoiceInfo() {
        log.printDbg("_showMovieChoiceInfo()");

        var adapterHandler = KTW.oipf.AdapterHandler;
        var curChannel = adapterHandler.navAdapter.getCurrentChannel();
        var curTime = new Date().getTime();

        var programList = adapterHandler.programSearchAdapter.getPrograms(curTime, curTime, null, curChannel);
        var program = (programList && programList.length > 0) ? programList[0] : null;

        log.printDbg("_showMovieChoiceInfo(), program = " + program);

        movieChoiceProgramDiv.css("display" , "none");
        movieChoiceNoProgramDiv.css("display" , "none");

        if (util.isValidVariable(program) === true) {
            var tmp = new Date(program.startTime * 1000);
            var st = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
            tmp = new Date((program.startTime + program.duration) * 1000);
            var et = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();



            movieChoiceTextProgram.text(program.name);
            movieChoiceTextPlayTime.text(st + ' - ' + et);
            movieChoiceTextDuration.text(Math.round(program.duration/60) + "분");

            // 프로그램 연령 아이콘
            var age = KTW.utils.epgUtil.getAge(program);
            // dhlee 2017.05.30
            //var ageElement = document.getElementById("rating_img");
            if(age<0) {
                movieChoiceImgAge.css("display" , "none");
            }else {
                if (age === 0) {
                    movieChoiceImgAge.attr("src", KTW.CONSTANT.IMAGE_PATH + "icon/icon_age_txtlist_all.png");
                } else {
                    movieChoiceImgAge.attr("src", KTW.CONSTANT.IMAGE_PATH + "icon/icon_age_txtlist_" + age + ".png");
                }
                movieChoiceImgAge.css("display" , "");
            }

            // HD 채널 여부
            if (adapterHandler.navAdapter.isSkyHDChannel() === true) {
                movieChoiceImgHd.attr("src", KTW.CONSTANT.IMAGE_PATH + "icon/icon_tag_hd.png");
                movieChoiceImgHd.css("display" , "");
            } else {
                movieChoiceImgHd.attr("src", "");
                movieChoiceImgHd.css("display" , "none");
            }

            if (program.description !== null && program.description.length > 0) {
                var desc = program.description;

                var multiLineText = null;

                multiLineText = util.stringToMultiLine(desc , "RixHead L" , 30 , 1237,-1.5);

                for(var x=0;x<movieChoiceTextDesc.length;x++) {
                    movieChoiceTextDesc[x].text("");
                    movieChoiceTextDesc[x].css("display" , "none");
                }

                if(multiLineText !== null) {
                    log.printDbg("multiLineText.length : " + multiLineText.length);
                    if(multiLineText.length>4) {
                        for(var i=0;i<movieChoiceTextDesc.length;i++) {
                            if(i === (movieChoiceTextDesc.length - 1)) {
                                movieChoiceTextDesc[i].text(multiLineText[i] + multiLineText[i+1]);
                            }else {
                                movieChoiceTextDesc[i].text(multiLineText[i]);
                            }
                            movieChoiceTextDesc[i].css("display" , "");
                        }
                    }else {
                        for(var i=0;i<multiLineText.length;i++) {
                            movieChoiceTextDesc[i].text(multiLineText[i]);
                            movieChoiceTextDesc[i].css("display" , "");
                        }

                    }
                }
            }
            movieChoiceProgramDiv.css("display" , "");
        } else {
            // TODO invalid program인 경우 처리 (GUI가 없음)
            movieChoiceNoProgramDiv.css("display" , "");
        }

        movieChoiceDiv.css("display" , "");
    }

    function _hideMovieChoiceInfo() {
        movieChoiceDiv.css("display" , "none");
    }

};

KTW.ui.view.BackIframeView.prototype = new KTW.ui.View();
KTW.ui.view.BackIframeView.prototype.constructor = KTW.ui.view.BackIframeView;

KTW.ui.view.BackIframeView.prototype.create = function() {
    this.create();
};

KTW.ui.view.BackIframeView.prototype.show = function() {
    this.show();
};

KTW.ui.view.BackIframeView.prototype.hide = function() {
    this.hide();
};

KTW.ui.view.BackIframeView.prototype.handleKeyEvent = function(key_code) {
    return KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
};