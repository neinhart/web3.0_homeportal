/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MiniEpgLeftView</code>
 *
 * 채널 미니 가이드 좌측 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */

"use strict";

(function() {
    KTW.ui.view.MiniEpgLeftView = function(div) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var parentDiv = div;

        /**
         * 기본 미니가이드
         */
        var arwChannelListImg = null;
        var textChannelSearch = null;

        /**
         * 연관 컨텐츠 
         */
        var textRelatedContent = null;
        var mashupImg = null;
        var mashupImgLine = null;
        var arwRelatedContentImg = null;

        /**
         * 미래 프로그램 탐색
         */
        var programLineDiv = null;
        var arwProgramLeftImg = null;

        var leftType = KTW.ui.view.MiniEpgLeftView.TYPE.HIDE;
        /**
         *  HIDE : 인접채널 리스트 진입 시,
         * CHANNEL_SEARCH_SHOW : 기본 미니가이드 노출 시,
         * PROGRAM_SEARCH_SHOW : 미래 프로그램 탐색 시,
         * RELATED_CONTENT_SHOW : 기본 미니가이드 노출 시 연관컨텐츠가 존재 하는 경우
         */

        this.create = function () {
            log.printDbg("create()");
            _createElement();
        };


        this.show = function (type , mashUpImg) {
            log.printDbg("show() type : " + type + " mashUpImg : " + (mashUpImg === undefined ? "" : mashUpImg));
            if(type !== undefined && type !== null) {
                leftType = type;
                _hideAll()
                if(type === KTW.ui.view.MiniEpgLeftView.TYPE.HIDE) {
                } else if(type === KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW) {
                    textChannelSearch.css("display", "");
                    arwChannelListImg.css("display", "");
                } else if(type === KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW) {
                    if(mashUpImg !== undefined && mashUpImg !== null && mashUpImg !== "") {
                        mashupImg.attr("src", mashUpImg);
                        mashupImg.css("display", "");
                        mashupImgLine.css("display", "");
                    }else {
                        textRelatedContent.css("display", "");
                    }
                    arwRelatedContentImg.css("display", "");
                } else if(type ===  KTW.ui.view.MiniEpgLeftView.TYPE.PROGRAM_SEARCH_SHOW) {
                    programLineDiv.css("display", "");
                    arwProgramLeftImg.css("display", "");
                }else if(type ===  KTW.ui.view.MiniEpgLeftView.TYPE.PROGRESS_LINE_SHOW) {
                    programLineDiv.css("display", "");
                }
            }
        };

        this.hide = function () {
            log.printDbg("hide()");
            _hideAll();
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.getShowType = function () {
            log.printDbg("getShowType() leftType : " + leftType);
            return leftType;
        }
        
        this.updateMashupImg = function(mashUpImg) {
            log.printDbg("updateMashupImg() leftType : " + leftType + " mashUpImg : " + (mashUpImg === undefined ? "" : mashUpImg));
            if(leftType === KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW || leftType === KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW) {
                if(mashUpImg !== undefined && mashUpImg !== null && mashUpImg !== "") {
                    _hideAll();
                    mashupImg.css("display", "");
                    mashupImg.attr("src", mashUpImg);
                    mashupImgLine.css("display", "");
                    arwRelatedContentImg.css("display", "");

                    leftType = KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW;
                }
            }
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    consumed = true;
                    break;
            }

            return consumed;
        };

        /**
         * 좌측 채널 탐색 영역 DIV
         *  전체 화면(1920X1080)에서 left:0 , top:512 , width:292 , height:568 위치
         * MiniEpgMainView의 leftDiv에서는 left:0 , top:0 , width:292 , height:568 위치
         */
        function _createElement() {


            arwChannelListImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_ch_mini_img",
                    src: "images/miniepg/arw_ch_mini.png",
                    css: {
                        position: "absolute",
                        left: 149,
                        top: (854-512),
                        width: 24,
                        height: 38,
                        display:"none"
                    }
                },
                parent: parentDiv
            });

            textChannelSearch = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_search",
                    css: {
                        position: "absolute", left: 188, top: (860-512), width: 123, height: 30,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 28, "text-align": "left",
                        display:"none"
                    }
                },
                text: "채널 탐색",
                parent: parentDiv
            });




            textRelatedContent = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_related_content",
                    css: {
                        position: "absolute", left: 172, top: (861-512), width: 123, height: 30,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 28, "text-align": "left",
                        display:"none"
                    }
                },
                text: "연관VOD",
                parent: parentDiv
            });


            mashupImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "mash_img",
                    css: {
                        position: "absolute",
                        left: 61,
                        top: (748-512),
                        width: 240,
                        height: 172,
                        display:"none"
                    }
                },
                parent: parentDiv
            }).error(function () {
                log.printDbg("mashupImg.onerror()");
                $(this).attr("src", "images/banner/default_mashup.png");
            });

            mashupImgLine = util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/miniepg/banner_line_w240.png",
                    css: {
                        position: "absolute",
                        left: 58,
                        top: (745-512),
                        width: 246,
                        height: 178,
                        display:"none"
                    }
                },
                parent: parentDiv
            });

            arwRelatedContentImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_related_content_search_img",
                    src: "images/miniepg/arw_pop_menu_l.png",
                    css: {
                        position: "absolute",
                        left: 321,
                        top: (855-512),
                        width: 20,
                        height: 36,
                        display:"none"
                    }
                },
                parent: parentDiv
            });





            programLineDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "program_line_div",
                    css: {
                        position: "absolute", left: 0, top: (870-512), width: 370, height: 6,
                        "background-color": "rgba(255,255,255,0.3)",
                        display:"none"
                    }
                },
                parent: parentDiv
            });

            arwProgramLeftImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_program_left_img",
                    src: "images/miniepg/arw_view_mini_l2.png",
                    css: {
                        position: "absolute",
                        left: 335,
                        top: (811-512),
                        width: 15,
                        height: 26,
                        display:"none"
                    }
                },
                parent: parentDiv
            });
        }

        function _hideAll() {
            log.printDbg("_hideAll()");


            arwChannelListImg.css("display", "none");
            textChannelSearch.css("display", "none");

            textRelatedContent.css("display", "none");
            mashupImg.css("display", "none");
            mashupImgLine.css("display", "none");
            
            arwRelatedContentImg.css("display", "none");

            programLineDiv.css("display", "none");
            arwProgramLeftImg.css("display", "none");
        }
    };

    Object.defineProperty(KTW.ui.view.MiniEpgLeftView, "TYPE", {
        value:  {
            HIDE : 0,
            CHANNEL_SEARCH_SHOW : 1,
            PROGRAM_SEARCH_SHOW : 2,
            RELATED_CONTENT_SHOW : 3,
            PROGRESS_LINE_SHOW : 4
        }
    });
})();

