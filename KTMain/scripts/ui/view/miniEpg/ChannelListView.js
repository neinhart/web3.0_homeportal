/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelListView</code>
 *
 * 채널 미니 가이드 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */

"use strict";

(function() {
    KTW.ui.view.ChannelListView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var channelList  = null;
        var currentChannelIndex = -1;

        var otvTopChannelIndex = [];
        var otsTopChannelIndex = [];
        var focusChannelIndex = -1;
        var bottomChannelIndex = -1;

        var parent = parent;
        var div = null;

        var otvChannelListDiv = null;
        var otsChannelListDiv = null;

        var focusPreviewChannelDiv = null;
        var focusdcsPreviewChannelDiv = null;

        var otvChannelPreViewTopDivList = [];
        var otvChannelPreViewBottomDiv = null;

        var otsChannelPreViewTopDivList = [];
        var otsChannelPreViewBottomDiv = null;


        var pip_channel_control = null;
        var ch_pip_evt = null;

        var isShowPip = false;

        var iptvBlockedChannelList = null;
        var skylifeBlokedChannelList = null;


        // 현재 재생 중인 채널 Object
        var cur_channel = null;
        var isMainTuneChannel = false;

        var programSearchChannelList = [];
        var progamArrayList = null;

        var OTS_TOP_CH_LIST_NUM = 7;
        var OTV_TOP_CH_LIST_NUM = 4;

        var thumbnail_timer;
        var SKIP_DURATION = 300;

        var isDCSMode = false;


        function descheduleTimer() {
            if (thumbnail_timer) {
                clearTimeout(thumbnail_timer);
                thumbnail_timer = null;
            }
        }
        function keyDown () {
            log.printDbg("keyDown()");
            if(isMainTuneChannel === true) {
                return;
            }

            focusChannelIndex--;
            if(focusChannelIndex <0) {
                focusChannelIndex = channelList.length - 1;
            }

            updateChannelList();
            showFocusChannel();

            showTopChannelInfo();
            showBottomChannelInfo();

            if (KTW.CONSTANT.STB_TYPE.ANDROID) {
                if (thumbnail_timer) {
                    descheduleTimer();
                    thumbnail_timer = setTimeout(updateThumbnailArea, SKIP_DURATION);
                } else {
                    showTopChannelList();
                    showBottomChannel();
                    thumbnail_timer = setTimeout(descheduleTimer, SKIP_DURATION);
                }
            } else {
                showTopChannelList();
                showBottomChannel();
            }
        }

        function keyUp () {
            log.printDbg("keyUp()");
            if(isMainTuneChannel === true) {
                return;
            }

            focusChannelIndex++;
            if(focusChannelIndex >(channelList.length - 1)) {
                focusChannelIndex = 0;
            }

            updateChannelList();
            showFocusChannel();

            showTopChannelInfo();
            showBottomChannelInfo();

            if (KTW.CONSTANT.STB_TYPE.ANDROID) {
                if (thumbnail_timer) {
                    descheduleTimer();
                    thumbnail_timer = setTimeout(updateThumbnailArea, SKIP_DURATION);
                } else {
                    showTopChannelList();
                    showBottomChannel();
                    thumbnail_timer = setTimeout(descheduleTimer, SKIP_DURATION);
                }
            } else {
                showTopChannelList();
                showBottomChannel();
            }
        }

        function keyRight () {
            log.printDbg("keyRight()");
        }


        function keyLeft () {
            log.printDbg("keyLeft()");
        }

        function keyOk () {
            log.printDbg("keyOk()");
            if(channelList[focusChannelIndex].ccid !== cur_channel.ccid) {
                if(isMainTuneChannel === false) {
                    //_channelTuneHiddenPromo();

                    if(KTW.CONSTANT.IS_OTS === true && isDCSMode ===  true) {

                    }else {
                        KTW.managers.service.PipManager.clearPipTuneTimer();
                        pip_channel_control.stop();
                    }
                    KTW.oipf.AdapterHandler.navAdapter.changeChannel(channelList[focusChannelIndex]);
                    isMainTuneChannel = true;

                    KTW.ui.LayerManager.deactivateLayer({
                        id: KTW.ui.Layer.ID.MINI_EPG
                    });
                }
                return true;
            }else {
                return false;
            }
        }

        function keyBack () {
            log.printDbg("keyBack()");
        }

        this.create = function () {
            log.printDbg("create()");

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "miniEpgChannelList",
                    css: {
                        position: "absolute"
                    }
                },
                parent: parent.div
            });
            
            div.off("webkitTransitionEnd").on('webkitTransitionEnd', function () {
                log.printDbg("create() webkitTransitionEnd event called");
                div.off('webkitTransitionEnd');
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "channel_list_background",
                    src: "images/miniepg/ch_bg_left.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 642, height: 1080,
                        opacity:1
                    }
                },
                parent: div
            });


            if(KTW.CONSTANT.IS_OTS === true ) {
                otsChannelListDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "ots_channel_list",
                        css: {
                            position: "absolute", left: 0, top: 0, width:642, height: 1080,display:""
                        }
                    },
                    parent: div
                });

                var rootDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_preview_channel_div",
                        css: {
                            position: "absolute", left: 63, top: 749, width:332, height: 172 , display : "none"
                        }
                    },
                    parent: div
                });

                var otsFocusFavImg = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "ots_current_channel_favorite",
                        src: "images/icon/icon_ch_favorite.png",
                        css: {
                            position: "absolute", left: 2, top: 71, width: 30, height: 30,
                            display: "none"
                        }
                    },
                    parent: rootDiv
                });

                var otsFocusChannelNumber = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "ots_current_channel_number",
                        class: "font_b" ,
                        css: {
                            position: "absolute", left: 0, top: 70, width: 54 , height: 39,
                            color: "rgba(235, 182, 147, 1)", "font-size": 37 , "letter-spacing":-1.85
                        }
                    },
                    text: "",
                    parent: rootDiv
                });

                var otsFocusChannelName = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "ots_current_channel_name",
                        class: "font_m cut" ,
                        css: {
                            position: "absolute", left: 80, top: 69, width: 249 , height: 39,"letter-spacing":-1.85 ,
                            color: "rgba(240, 240, 240, 1)", "font-size": 37
                        }
                    },
                    text: "",
                    parent: rootDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "top_margin_div",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 332, height: 6,
                            "background-color": "rgba(209,67,63,1)"
                        }
                    },
                    parent: rootDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "bottom_margin_div",
                        css: {
                            position: "absolute", left: 0, top: 166, width: 332, height: 6,
                            "background-color": "rgba(209,67,63,1)"
                        }
                    },
                    parent: rootDiv
                });

                focusdcsPreviewChannelDiv = {
                    root_div : rootDiv,
                    ots_current_channel_favorite : otsFocusFavImg,
                    ots_current_channel_number : otsFocusChannelNumber,
                    ots_current_channel_name : otsFocusChannelName
                };


            }else {
                otvChannelListDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "otv_channel_list",
                        css: {
                            position: "absolute", left: 0, top: 0, width:642, height: 1080,display:""
                        }
                    },
                    parent: div
                });
            }


            focusPreviewChannelDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_preview_channel_div",
                    css: {
                        position: "absolute", left: 54, top: 731, width:358, height: 207
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/miniepg/thum_sdw_w330.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 358, height: 207
                    }
                },
                parent:  focusPreviewChannelDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute", left: (66 - 54), top: (742-731), width: 336, height: 189,
                        "background-color": "rgba(0,0,0,1)"
                    }
                },
                parent: focusPreviewChannelDiv
            });

            var tempdiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "focus_red_border_box",
                    css: {
                        position: "absolute", left: (58 - 54), top: (735-731), width: 350, height: 203
                    }
                },
                parent: focusPreviewChannelDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "focus_black_1px_border_box",
                    css: {
                        position: "absolute", left : 0 , top : 0 , width: (350-12), height: (203-12)
                    }
                },
                parent: tempdiv
            });


            if (KTW.CONSTANT.IS_OTS) {
                createOtsElement();
            } else {
                createOtvElement();
            }
        };

        this.getDiv = function () {
            return div;
        };

        this.show = function (options) {
            log.printDbg("show(), ");

            if (!options || !options.resume) {

            }

            if (KTW.CONSTANT.IS_OTS) {
                var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                log.printDbg("show(), KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE) : " + temp);
                if (temp != null) {
                    if(temp === "Y") {
                        isDCSMode = true;
                    }else {
                        isDCSMode = false;
                    }
                }

                log.printDbg("show(), isDCSMode: " + isDCSMode);
                KTW.managers.StorageManager.ps.addStorageDataChangeListener(_updateOTSDCSMode);
            }



            if(channelList === null || channelList.length<=0) {
                log.printDbg("channelIst is null or length = 0");
                return;
            }

            updateChannelList(currentChannelIndex);

            showTopChannelInfo();
            showBottomChannelInfo();
            showTopChannelList();
            showBottomChannel();
            isMainTuneChannel = false;
            //div.css({left:0});
            div.addClass("show");
            div.off("webkitTransitionEnd").on('webkitTransitionEnd', initUpdate);
        };

        function _updateOTSDCSMode(key) {
            log.printDbg('_updateOTSDCSMode() key : ' + key);
            if (key === KTW.managers.StorageManager.KEY.OTS_DCS_MODE) {
                log.printDbg('_updateOTSDCSMode() key : ' + key);
                var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                if (temp != null) {
                    if(temp === "Y") {
                        isDCSMode = true;
                    }else {
                        isDCSMode = false;
                    }

                    if(isDCSMode === false) {
                        _startPip();
                    }else {
                        _endPip();
                    }
                    showFocusChannel();
                }
            }
        }

        //var ch_pip = null;
        function _pipChEventListener (evt) {
            log.printDbg('_pipChEventListener() evt: ' + log.stringify(evt));

            var state = KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL;
            if(evt !== null) {
                var ch = evt.channel;
                if (evt.permission === KTW.nav.Def.CHANNEL.PERM.BLOCKED && evt.blocked_reason === KTW.nav.Def.CHANNEL.BLOCKED_REASON.USER_BLOCKED) {
                    state = KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED;
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.PR_BLOCKED) {
                    state = KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT;
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED) {
                    state = KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED;
                }
                else if (evt.permission === KTW.nav.Def.CHANNEL.PERM.PREVIEW) {
                    state = KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW;
                }

                var view = parent.getMiniEpgMainView();
                if(view !== undefined && view !== null) {
                    if(channelList[focusChannelIndex].ccid === ch.ccid) {
                        var chList = [];
                        chList.push(channelList[focusChannelIndex]);
                        var programArrayList = _getProgramList(chList);
                        view.updateProgramInfo(channelList[focusChannelIndex] , state , programArrayList[0]);
                    }
                }
            }
        }

        function initUpdate() {
            log.printDbg("initUpdate()");
            div.off("webkitTransitionEnd");
            //
            if(KTW.CONSTANT.IS_OTS === true && isDCSMode === true) {

            }else {
                _startPip();
            }
            showFocusChannel();
        }

        this.hide = function (options) {
            log.printDbg("hide()");


            if (KTW.CONSTANT.IS_OTS) {
                KTW.managers.StorageManager.ps.removeStorageDataChangeListener(_updateOTSDCSMode);
            }

            if(KTW.CONSTANT.IS_OTS === true && isDCSMode === true) {

            }else {
                _endPip();
            }
            div.removeClass("show");
            div.off('webkitTransitionEnd').on('webkitTransitionEnd', function () {
                log.printDbg("hide() webkitTransitionEnd event called");
                div.off('webkitTransitionEnd');
            });

            descheduleTimer();

            if (!options || !options.pause) {
                // hide 작업...
            }
        };

        this.closeView = function () {
            log.printDbg("closeView()");

            div.removeClass("show");
            div.off('webkitTransitionEnd').on('webkitTransitionEnd', function () {
                log.printDbg("hide() webkitTransitionEnd event called");
                div.off('webkitTransitionEnd');
            });
        };

        this.closePIP = function (callback) {
            _endPip();
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    consumed = keyOk();
                    break;
                case KTW.KEY_CODE.LEFT:
                    keyLeft();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    keyBack();
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.setChannelList = function(chList , initChannelIndex , curCh , iptvBlockedList , skylifeBlockedList) {
            log.printDbg("setChannelList() initChannelIndex : " + initChannelIndex );
            if(chList !== undefined && chList !== null) {
                log.printDbg("setChannelList() chList.length : " + chList.length);
            }

            channelList  = chList;
            currentChannelIndex = initChannelIndex;
            cur_channel = curCh;

            iptvBlockedChannelList = iptvBlockedList;
            skylifeBlokedChannelList = skylifeBlockedList;
        };

        function createOtvElement() {
            for(var i=0;i<OTV_TOP_CH_LIST_NUM;i++) {
                var otv_channel_index = (OTV_TOP_CH_LIST_NUM - 1) - i;
                otvChannelPreViewTopDivList[otv_channel_index] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "otv_preview_channel_top_div" + otv_channel_index,
                        css: {
                            position: "absolute", left: 71, top: 9+(i*178), width:312, height: 193
                        }
                    },
                    parent: otvChannelListDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "",
                        src: "images/miniepg/thum_sdw_w272.png",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 312, height: 193
                        }
                    },
                    parent:  otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "otv_preview_channel_default_img",
                        src: "images/iframe/default_thumb.jpg",
                        css: {
                            position: "absolute", left: 20, top: 5, width: 272, height: 153
                        }
                    },
                    parent:  otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "otv_preview_channel_img",
                        css: {
                            position: "absolute", left: 20, top: 5, width: 272, height: 153,
                            display:"none"
                        }
                    },
                    parent: otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "otv_iframe_channel_img",
                        src: "",
                        css: {
                            position: "absolute", left: 20, top: 5, width: 272, height: 153
                        }
                    },
                    parent:  otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "",
                        src: "images/miniepg/thum_gra_w278.png",
                        css: {
                            position: "absolute", left: 20, top: 60, width: 272, height: 98
                        }
                    },
                    parent:  otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "otv_fav_channel",
                        src: "images/icon/icon_fav.png",
                        css: {
                            position: "absolute", left: 29, top: 124, width: 22, height: 22
                        }
                    },
                    parent:  otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "otv_channel_number",
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 38, top: 122, width: 56 , height: 32,
                            color: "rgba(240, 240, 240, 1)", "font-size": 30
                        }
                    },
                    text: "",
                    parent: otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "otv_channel_name",
                        class: "font_l cut" ,
                        css: {
                            position: "absolute", left: 108, top: 122, width: 170 , height: 32,
                            color: "rgba(240, 240, 240, 1)", "font-size": 30
                        }
                    },
                    text: "",
                    parent: otvChannelPreViewTopDivList[otv_channel_index]
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "img_gra",
                        css: {
                            position: "absolute", left: 20, top: 5, width: 272, height: 153,
                            "background-color": "rgba(0,0,0,0.3)",
                            display:"none"
                        }
                    },
                    parent: otvChannelPreViewTopDivList[otv_channel_index]
                });
            }

            otvChannelPreViewBottomDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "otv_preview_channel_bottom_div",
                    css: {
                        position: "absolute", left: 71, top: 966, width:312, height: 193
                    }
                },
                parent: otvChannelListDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/miniepg/thum_sdw_w272.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 312, height: 193
                    }
                },
                parent:  otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "otv_preview_channel_default_img",
                    src: "images/iframe/default_thumb.jpg",
                    css: {
                        position: "absolute", left: 20, top: 5, width: 272, height: 153
                    }
                },
                parent:  otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "otv_preview_channel_img",
                    css: {
                        position: "absolute", left: 20, top: 5, width: 272, height: 153
                    }
                },
                parent: otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "otv_iframe_channel_img",
                    src: "",
                    css: {
                        position: "absolute", left: 20, top: 5, width: 272, height: 153,
                        display:"none"
                    }
                },
                parent:  otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/miniepg/thum_gra_w278_up.png",
                    css: {
                        position: "absolute", left: 20, top: 5, width: 272, height: 98
                    }
                },
                parent:  otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "otv_fav_channel",
                    src: "images/icon/icon_fav.png",
                    css: {
                        position: "absolute", left: 29, top: 19, width: 22, height: 22
                    }
                },
                parent:  otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "otv_channel_number",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 38, top: 17, width: 56 , height: 28,
                        color: "rgba(240, 240, 240, 0.7)", "font-size": 30
                    }
                },
                text: "",
                parent: otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "otv_channel_name",
                    class: "font_l cut" ,
                    css: {
                        position: "absolute", left: 108, top: 17, width: 170 , height: 32,
                        color: "rgba(240, 240, 240, 0.7)", "font-size": 30
                    }
                },
                text: "",
                parent: otvChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute", left: 20, top: 5, width: 272, height: 109,
                        "background-color": "rgba(0,0,0,0.3)"
                    }
                },
                parent: otvChannelPreViewBottomDiv
            });
        }

        function createOtsElement() {
            for(var i=0;i<OTS_TOP_CH_LIST_NUM;i++) {
                var ots_channel_index = (OTS_TOP_CH_LIST_NUM - 1) - i;
                otsChannelPreViewTopDivList[ots_channel_index] = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "ots_preview_channel_top_div" + ots_channel_index,
                        css: {
                            position: "absolute", left: 75, top: 50+(i*100), width:350, height: 40
                        }
                    },
                    parent: otsChannelListDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "ots_fav_channel",
                        src: "images/icon/icon_fav.png",
                        css: {
                            position: "absolute", left: 0, top: 3, width: 22, height: 22
                        }
                    },
                    parent:  otsChannelPreViewTopDivList[ots_channel_index]
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "ots_channel_number",
                        class: "font_b" ,
                        css: {
                            position: "absolute", left: 34, top: 2, width: 60 , height: 32,
                            color: i === 0 ? "rgba(235, 182, 147, 0.7)" : "rgba(235, 182, 147, 1)", "font-size": 30
                        }
                    },
                    text: "",
                    parent: otsChannelPreViewTopDivList[ots_channel_index]
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "ots_channel_name",
                        class: "font_m cut" ,
                        css: {
                            position: "absolute", left: 98, top: 0, width: 178 , height: 32,
                            color: "rgba(240, 240, 240, 1)", "font-size": 30
                        }
                    },
                    text: "",
                    parent: otsChannelPreViewTopDivList[ots_channel_index]
                });
            }

            otsChannelPreViewBottomDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "ots_preview_channel_bottom_div",
                    css: {
                        position: "absolute", left: 75, top: 992, width:350, height: 40
                    }
                },
                parent: otsChannelListDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "ots_fav_channel",
                    src: "images/icon/icon_fav.png",
                    css: {
                        position: "absolute", left: 0, top: 3, width: 22,
                        height: 22
                    }
                },
                parent:  otsChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "ots_channel_number",
                    class: "font_b" ,
                    css: {
                        position: "absolute", left: 34, top: 2, width: 60 , height: 32,
                        color: "rgba(235, 182, 147, 0.7)", "font-size": 30
                    }
                },
                text: "",
                parent: otsChannelPreViewBottomDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "ots_channel_name",
                    class: "font_m cut" ,
                    css: {
                        position: "absolute", left: 98, top: 0, width: 178 , height: 32,
                        color: "rgba(240, 240, 240, 1)", "font-size": 30
                    }
                },
                text: "",
                parent: otsChannelPreViewBottomDiv
            });
        }

        function showTopChannelInfo() {
            for (var i = 0; i < OTV_TOP_CH_LIST_NUM; i++) {
                if (otvTopChannelIndex[i] >= 0) {

                    $("#otv_preview_channel_top_div" + i + " #otv_channel_number").text(util.numToStr(channelList[otvTopChannelIndex[i]].majorChannel, 3));
                    $("#otv_preview_channel_top_div" + i + " #otv_channel_name").text(channelList[otvTopChannelIndex[i]].name);
                    $("#otv_preview_channel_top_div" + i + " #otv_channel_number").css("display", "");
                    $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css("display", "");

                    $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").css("display" , "none");
                    $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("display", "none");

                    if (KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[otvTopChannelIndex[i]])) {
                        $("#otv_preview_channel_top_div" + i + " #otv_fav_channel").attr("src", "images/icon/icon_block.png");
                        $("#otv_preview_channel_top_div" + i + " #otv_fav_channel").css("display", "");

                        $("#otv_preview_channel_top_div" + i + " #otv_channel_number").css({left: 38 + 24});
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css({left: 108 + 24});

                        $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css({width: 150});
                    } else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channelList[otvTopChannelIndex[i]]) === true) {
                        $("#otv_preview_channel_top_div" + i + " #otv_fav_channel").attr("src", "images/icon/icon_fav.png");
                        $("#otv_preview_channel_top_div" + i + " #otv_fav_channel").css("display", "");
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_number").css({left: 38 + 24});
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css({left: 108 + 24});
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css({width: 150});

                    } else {
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_number").css({left: 38});
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css({width: 170});
                        $("#otv_preview_channel_top_div" + i + " #otv_channel_name").css({left: 108});
                        $("#otv_preview_channel_top_div" + i + " #otv_fav_channel").css("display", "none");
                    }
                }
            }
        }

        function showTopChannelList() {
            log.printDbg("showTopChannelList()");

            if(KTW.CONSTANT.IS_OTS === true) {
                for(var i=0;i<OTS_TOP_CH_LIST_NUM;i++) {
                    if(otsTopChannelIndex[i]>=0) {
                        $("#ots_preview_channel_top_div" + i + " #ots_channel_number").text(util.numToStr(channelList[otsTopChannelIndex[i]].majorChannel, 3));
                        $("#ots_preview_channel_top_div" + i + " #ots_channel_name").text(channelList[otsTopChannelIndex[i]].name);
                        $("#ots_preview_channel_top_div" + i + " #ots_channel_number").css("display", "");
                        $("#ots_preview_channel_top_div" + i + " #ots_channel_name").css("display", "");

                        if(KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[otsTopChannelIndex[i]])) {
                            $("#ots_preview_channel_top_div" + i + " #ots_fav_channel").attr("src", "images/icon/icon_block.png");
                            $("#ots_preview_channel_top_div" + i + " #ots_fav_channel").css("display", "");
                        }else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channelList[otsTopChannelIndex[i]]) === true) {
                            $("#ots_preview_channel_top_div" + i + " #ots_fav_channel").attr("src", "images/icon/icon_fav.png");
                            $("#ots_preview_channel_top_div" + i + " #ots_fav_channel").css("display", "");
                        }else {
                            $("#ots_preview_channel_top_div" + i + " #ots_fav_channel").css("display", "none");
                        }
                        otsChannelPreViewTopDivList[i].css("display", "");
                    }else {
                        otsChannelPreViewTopDivList[i].css("display", "none");
                    }
                }
            }else {
                for(var i=0;i<OTV_TOP_CH_LIST_NUM;i++) {
                    if(otvTopChannelIndex[i]>=0) {
                        var listLength = programSearchChannelList.length;
                        var cp = null;
                        for(var x=0;x<listLength;x++) {
                            if (channelList[otvTopChannelIndex[i]].ccid === programSearchChannelList[x].ccid) {
                                if(progamArrayList !== null && progamArrayList.length>0 &&   progamArrayList[x] !== undefined &&  progamArrayList[x] !== null) {
                                    var progamList = progamArrayList[x];
                                    cp = _getCurrentProgram(progamList);
                                }
                                break;
                            }
                        }

                        /**
                         * Thumnail 노출 우선 순위
                         * 시청채널 제한 > 성인 채널 > 미가입채널(무비초이스 포함) > 연령제한 > 현재 Thumnail
                         */
                        if (KTW.managers.service.KidsModeManager.isKidsMode() === false
                            && KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[otvTopChannelIndex[i]])) {
                            $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("display", "none");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").css("display" , "");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").attr("src", "images/iframe/block_ch.jpg");
                        } else if (cp !== null && KTW.utils.epgUtil.isAgeLimitProgram(cp)) {
                            $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("display", "none");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").css("display" , "");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").attr("src", "images/iframe/block_age.jpg");
                        } else if (channelList[otvTopChannelIndex[i]].desc === 2) {
                            $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("display", "none");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").css("display", "");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").attr("src", "images/iframe/block_adult_ch.jpg");
                        } else if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(channelList[otvTopChannelIndex[i]])) { // 2017.07.13 dhlee 오디오 채널인 경우 처리
                            $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("display", "none");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").css("display" , "");
                            $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").attr("src", "images/iframe/block_audio.jpg");
                        } else {
                            try {
                                $("#otv_preview_channel_top_div" + i + " #otv_iframe_channel_img").css("display" , "none");
                                $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("display", "");
                                //$("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("background-image", "url(http://image.ktipmedia.co.kr/channel/CH_" + channelList[otvTopChannelIndex[i]].sid + ".png)");
                                $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("background-size", "278px 155px");
                                var imageUrl = util.getImageUrl(
                                    KTW.DATA.HTTP.THUMBNAIL_IMAGE_SERVER + channelList[otvTopChannelIndex[i]].sid + ".png", KTW.CONSTANT.POSTER_TYPE.THUMBNAIL, 278, 155);
                                $("#otv_preview_channel_top_div" + i + " #otv_preview_channel_img").css("background-image", "url(" + imageUrl + ")");
                            }catch (e) {
                                log.printDbg(e);
                            }
                        }

                        otvChannelPreViewTopDivList[i].css("display", "");

                        if(i === 3) {
                            $("#otv_preview_channel_top_div" + i + " #img_gra").css("display" , "");
                        }else {
                            if(otvTopChannelIndex[i+1]<0) {
                                $("#otv_preview_channel_top_div" + i + " #img_gra").css("display" , "");
                            }
                        }
                    }else {
                        otvChannelPreViewTopDivList[i].css("display", "none");

                    }
                }
            }
        }

        function showBottomChannelInfo() {
            if(bottomChannelIndex>=0) {
                $("#otv_preview_channel_bottom_div #otv_channel_number").text(util.numToStr(channelList[bottomChannelIndex].majorChannel, 3));
                $("#otv_preview_channel_bottom_div #otv_channel_name").text(channelList[bottomChannelIndex].name);
                $("#otv_preview_channel_bottom_div #otv_channel_number").css("display", "");
                $("#otv_preview_channel_bottom_div #otv_channel_name").css("display", "");

                $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").css("display", "none");
                $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("display", "none");

                if (KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[bottomChannelIndex])) {
                    $("#otv_preview_channel_bottom_div #otv_fav_channel").attr("src", "images/icon/icon_block.png");
                    $("#otv_preview_channel_bottom_div #otv_fav_channel").css("display", "");

                    $("#otv_preview_channel_bottom_div #otv_channel_number").css({left: 38 + 24});
                    $("#otv_preview_channel_bottom_div #otv_channel_name").css({left: 108 + 24});
                    $("#otv_preview_channel_bottom_div #otv_channel_name").css({width: 150});

                } else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channelList[bottomChannelIndex]) === true) {
                    $("#otv_preview_channel_bottom_div #otv_fav_channel").attr("src", "images/icon/icon_fav.png");
                    $("#otv_preview_channel_bottom_div #otv_fav_channel").css("display", "");

                    $("#otv_preview_channel_bottom_div #otv_channel_number").css({left: 38 + 24});
                    $("#otv_preview_channel_bottom_div #otv_channel_name").css({left: 108 + 24});
                    $("#otv_preview_channel_bottom_div #otv_channel_name").css({width: 150});

                } else {
                    $("#otv_preview_channel_bottom_div #otv_fav_channel").css("display", "none");

                    $("#otv_preview_channel_bottom_div #otv_channel_number").css({left: 38});
                    $("#otv_preview_channel_bottom_div #otv_channel_name").css({left: 108});
                    $("#otv_preview_channel_bottom_div #otv_channel_name").css({width: 170});
                }
            }
        }

        function showBottomChannel() {
            if(KTW.CONSTANT.IS_OTS === true) {
                if(bottomChannelIndex>=0) {
                    $("#ots_preview_channel_bottom_div #ots_channel_number").text(util.numToStr(channelList[bottomChannelIndex].majorChannel, 3));
                    $("#ots_preview_channel_bottom_div #ots_channel_name").text(channelList[bottomChannelIndex].name);
                    $("#ots_preview_channel_bottom_div #ots_channel_number").css("display", "");
                    $("#ots_preview_channel_bottom_div #ots_channel_name").css("display", "");

                    if(KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[bottomChannelIndex])) {
                        $("#ots_preview_channel_bottom_div #ots_fav_channel").attr("src", "images/icon/icon_block.png");
                        $("#ots_preview_channel_bottom_div #ots_fav_channel").css("display", "");
                    }
                    else if (KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channelList[bottomChannelIndex]) === true) {
                        $("#ots_preview_channel_bottom_div #ots_fav_channel").attr("src", "images/icon/icon_fav.png");
                        $("#ots_preview_channel_bottom_div #ots_fav_channel").css("display", "");
                    }else {
                        $("#ots_preview_channel_bottom_div #ots_fav_channel").css("display", "none");
                    }

                    otsChannelPreViewBottomDiv.css("display", "");
                }
            }else {
                if (bottomChannelIndex >= 0) {
                    var listLength = programSearchChannelList.length;
                    var cp = null;
                    for (var i = 0; i < listLength; i++) {
                        if (channelList[bottomChannelIndex].ccid === programSearchChannelList[i].ccid) {
                            if(progamArrayList !== null && progamArrayList.length>0 &&   progamArrayList[i] !== undefined &&  progamArrayList[i] !== null) {
                                var progamList = progamArrayList[i];
                                cp = _getCurrentProgram(progamList);
                            }
                            break;
                        }
                    }

                    if (KTW.managers.service.KidsModeManager.isKidsMode() === false
                        && KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[bottomChannelIndex])) {
                        $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("display", "none");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").css("display", "");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").attr("src", "images/iframe/block_ch.jpg");
                    } else if (cp !== null && KTW.utils.epgUtil.isAgeLimitProgram(cp)) {
                        $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("display", "none");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").css("display", "");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").attr("src", "images/iframe/block_age.jpg");
                    } else if (channelList[bottomChannelIndex].desc === 2) {
                        $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("display", "none");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").css("display", "");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").attr("src", "images/iframe/block_adult_ch.jpg");
                    } else if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(channelList[bottomChannelIndex])) { // 2017.07.13 dhlee 오디오 채널인 경우 처리
                        $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("display", "none");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").css("display", "");
                        $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").attr("src", "images/iframe/block_audio.jpg");
                    } else {
                        try {
                            $("#otv_preview_channel_bottom_div #otv_iframe_channel_img").css("display", "none");
                            $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("display", "");
                            var imageUrl = util.getImageUrl(KTW.DATA.HTTP.THUMBNAIL_IMAGE_SERVER + channelList[bottomChannelIndex].sid + ".png", KTW.CONSTANT.POSTER_TYPE.THUMBNAIL, 275, 155);
                            $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("background-image", "url(" + imageUrl + ")");
                            $("#otv_preview_channel_bottom_div #otv_preview_channel_img").css("background-size", "278px 155px");
                        }catch (e) {
                            log.printDbg(e);
                        }
                    }
                    otvChannelPreViewBottomDiv.css("display", "");
                }
            }
        }

        function showFocusChannel() {
            log.printDbg("showFocusChannel()");

            if(currentChannelIndex>=0 && currentChannelIndex<=(channelList.length-1)) {
                if(KTW.CONSTANT.IS_OTS === true && isDCSMode === true) {
                    focusPreviewChannelDiv.css("display", "none");
                    focusdcsPreviewChannelDiv.root_div.css("display", "");
                    $("#focus_preview_channel_div #ots_current_channel_number").text(util.numToStr(channelList[focusChannelIndex].majorChannel, 3));
                    $("#focus_preview_channel_div #ots_current_channel_name").text(channelList[focusChannelIndex].name);

                    if(KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(channelList[focusChannelIndex])) {
                        $("#focus_preview_channel_div #ots_current_channel_favorite").attr("src", "images/icon/icon_ch_blocked.png");
                        $("#focus_preview_channel_div #ots_current_channel_favorite").css("display" , "");

                        $("#focus_preview_channel_div #ots_current_channel_number").css({left:46});
                        $("#focus_preview_channel_div #ots_current_channel_name").css({left:124});
                        $("#focus_preview_channel_div #ots_current_channel_name").css({width:205});

                    }else if(KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(channelList[focusChannelIndex]) === true) {
                        $("#focus_preview_channel_div #ots_current_channel_favorite").attr("src", "images/icon/icon_ch_favorite.png");
                        $("#focus_preview_channel_div #ots_current_channel_favorite").css("display" , "");

                        $("#focus_preview_channel_div #ots_current_channel_number").css({left:46});
                        $("#focus_preview_channel_div #ots_current_channel_name").css({left:124});
                        $("#focus_preview_channel_div #ots_current_channel_name").css({width:205});

                    }else {
                        $("#focus_preview_channel_div #ots_current_channel_favorite").css("display" , "none");

                        $("#focus_preview_channel_div #ots_current_channel_number").css({left:0});
                        $("#focus_preview_channel_div #ots_current_channel_name").css({left:80});
                        $("#focus_preview_channel_div #ots_current_channel_name").css({width:249});
                    }
                }else {
                    if(isShowPip === true) {
                        if(KTW.CONSTANT.IS_OTS === true) {
                            focusdcsPreviewChannelDiv.root_div.css("display", "none");
                        }
                        focusPreviewChannelDiv.css("display", "");
                        KTW.managers.service.PipManager.changeChannel(channelList[focusChannelIndex]);
                    }
                }

                var listLength = programSearchChannelList.length;

                for(var i=0;i<listLength;i++) {
                    if(channelList[focusChannelIndex].ccid === programSearchChannelList[i].ccid) {
                        if(progamArrayList !== null && progamArrayList.length>0 &&   progamArrayList[i] !== undefined &&  progamArrayList[i] !== null) {
                            var progamList = progamArrayList[i];
                            var view = parent.getMiniEpgMainView();
                            if(view !== undefined && view !== null) {
                                if(channelList[currentChannelIndex].ccid ===  channelList[focusChannelIndex].ccid) {
                                    view.updateProgramInfo(channelList[focusChannelIndex] , view.getPermission() ,progamList);
                                }else {
                                    view.updateProgramInfo(channelList[focusChannelIndex] , KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN , progamList);
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        function updateChannelList(initChannelIndex) {
            var count = 0;

            if(initChannelIndex !== undefined && initChannelIndex !== null) {
                focusChannelIndex = initChannelIndex;
            }

            var tempIndex = focusChannelIndex;
            bottomChannelIndex = -1;
            programSearchChannelList = [];
            progamArrayList = null;

            if(KTW.CONSTANT.IS_OTS === true) {
                for(var i=0;i<otsTopChannelIndex.length;i++) {
                    otsTopChannelIndex[i]  = -1;
                }
                /**
                 * jjh1117 2017/09/04
                 * OTS에서도 현재 채널에 대해서는 프로그램 정보 얻어 와야 하므로
                 * 프로그램 조회 로직 추가함.
                 */
                while(count<OTS_TOP_CH_LIST_NUM) {
                    tempIndex = tempIndex+1;
                    if(tempIndex>=channelList.length) {
                        tempIndex=0;
                    }
                    if(focusChannelIndex === tempIndex) {
                        return;
                    }
                    otsTopChannelIndex[count] = tempIndex;
                    count++;
                }

                if(channelList.length>9) {
                    tempIndex = focusChannelIndex - 1;
                    if(tempIndex<0) {
                        tempIndex = channelList.length - 1;
                    }

                    if(focusChannelIndex === tempIndex) {
                        return;
                    }

                    bottomChannelIndex = tempIndex;
                }else {
                    tempIndex = tempIndex+1;
                    if(tempIndex>=channelList.length) {
                        tempIndex=0;
                    }
                    if(focusChannelIndex === tempIndex) {
                        return;
                    }
                    bottomChannelIndex = tempIndex;
                }
                /**
                 * jjh1117 2017/08/21
                 * Program 조회를 위해 리스트 구성
                 */
                if(focusChannelIndex>=0) {
                    programSearchChannelList.push(channelList[focusChannelIndex]);
                }
                progamArrayList = _getProgramList(programSearchChannelList);

            }
            // OTV case
            else {
                for(var i=0;i<otvTopChannelIndex.length;i++) {
                    otvTopChannelIndex[i]  = -1;
                }

                while(count<OTV_TOP_CH_LIST_NUM) {
                    tempIndex = tempIndex+1;
                    if(tempIndex>=channelList.length) {
                        tempIndex=0;
                    }
                    if(focusChannelIndex === tempIndex) {
                        return;
                    }
                    otvTopChannelIndex[count] = tempIndex;
                    count++;
                }

                if(channelList.length>6) {
                    tempIndex = focusChannelIndex - 1;
                    if(tempIndex<0) {
                        tempIndex = channelList.length - 1;
                    }

                    if(focusChannelIndex === tempIndex) {
                        return;
                    }

                    bottomChannelIndex = tempIndex;
                }else {
                    tempIndex = tempIndex+1;
                    if(tempIndex>=channelList.length) {
                        tempIndex=0;
                    }
                    if(focusChannelIndex === tempIndex) {
                        return;
                    }
                    bottomChannelIndex = tempIndex;
                }
                /**
                 * jjh1117 2017/08/21
                 * Program 조회를 위해 리스트 구성
                 */
                for(var x=0;x<OTV_TOP_CH_LIST_NUM;x++) {
                    if(otvTopChannelIndex[x]>=0) {
                        programSearchChannelList.push(channelList[otvTopChannelIndex[x]]);
                    }
                }

                if(focusChannelIndex>=0) {
                    programSearchChannelList.push(channelList[focusChannelIndex]);
                }

                if(bottomChannelIndex>=0) {
                    programSearchChannelList.push(channelList[bottomChannelIndex]);
                }

                progamArrayList = _getProgramList(programSearchChannelList);
            }
        }

        function _getProgramList(chList) {
            log.printDbg('_getProgramList() chList length : ' + chList.length);

            var startTime = null;

            // 2017.09.05 dhlee
            // reviseProgram은 EpgDataAdaptor 에서 수행하고 있으므로 여기서는 수행하지 않도록 한다.
            return KTW.ui.adaptor.EpgDataAdaptor.getProgrammesList({
                channelList: chList,
                startTime: startTime
            });
        }

        function _getCurrentProgram(programList) {
            var currentProgram = null;

            if(programList !== undefined && programList !== null && programList.length>0) {
                var prog_index = 0;

                var now = new Date().getTime();
                if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                    now = Math.floor(now/1000);
                }

                var prog = programList[prog_index];
                while(prog.startTime + prog.duration < now && prog_index < programList.length - 1) {
                    prog_index++;
                    prog = programList[prog_index];
                    if (prog == null) {
                        prog_index = -1;
                        break;
                    }
                }

                if(prog_index>=0) {
                    currentProgram = programList[prog_index];
                }
            }

            return currentProgram;
        }

        function _startPip () {
            log.printDbg("startPip()");

            KTW.managers.service.PipManager.activatePipLayer({pipType: KTW.managers.service.PipManager.PIP_TYPE.MINI_EPG});

            if (pip_channel_control == null)  {
                pip_channel_control = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
                if(pip_channel_control) {
                    pip_channel_control.addChannelEventListener(_pipChEventListener);
                }
            }
            isShowPip = true;
        }

        function _endPip () {
            log.printDbg("endPip()");

            if(isShowPip === true) {
                if (pip_channel_control === null)  {
                    pip_channel_control = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
                }
                pip_channel_control.removeChannelEventListener(_pipChEventListener);

                KTW.managers.service.PipManager.deactivatePipLayer();

                ch_pip_evt = null;

                isShowPip = false;
                pip_channel_control = null;
            }
        }

        function updateThumbnailArea() {
            showTopChannelList();
            showBottomChannel();
            descheduleTimer();
        }
    }
})();