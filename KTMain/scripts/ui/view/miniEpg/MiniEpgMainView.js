/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MiniEpgMainView</code>
 *
 * 채널 미니 가이드 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */

"use strict";

(function() {
    KTW.ui.view.MiniEpgMainView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var navAdapter = null;
        var layerManager = KTW.ui.LayerManager;
        var epgUtil = KTW.utils.epgUtil;
        var storageManager = KTW.managers.StorageManager;
        var authManager = KTW.managers.auth.AuthManager;
        var epgRelatedMenuManager = KTW.managers.service.EpgRelatedMenuManager;

        var parent = parent;
        var cpmsinfoDiv = null;
        var div = null;
        var leftDiv = null;
        var rightDiv = null;
        var rightTopProgramDiv = null;
        var rightBottomProgramTimeDiv = null;

        var MINIEPGLAYER_DEF = KTW.ui.layer.MiniEpgLayer.DEF;

        // iptv채널 리스트 , skylife 채널 리스트
        var kt_ch_list = [];
        var sky_ch_list = [];

        // 현재 재생 중인 채널 Object
        var cur_channel = null;
        // 현재 채널의 permission
        var last_perm = 0;
        /** 제한 또는 프로그램 위치 등의 state **/
        var cur_channel_state = MINIEPGLAYER_DEF.PROGRAM_STATE.NORMAL;

        // miniepg 내 채널 리스트에서 채널 UP/DOWN으로 채널 Tune를 했는지 여부
        var ischanneltune = false;

        // 현재 채널 리스트
        var ch_list = null;
        var cur_ch_index = -1;
        var ch_index = 0;

        // 현재 프로그램 정보 리스트
        var prog_list = [];
        var prog_index = 0;
        var move_index = 0;


        var pipChannelState = MINIEPGLAYER_DEF.PROGRAM_STATE.NORMAL;
        // PIP 프로그램 리스트
        var pip_prog_list = [];
        var pip_prog_index = 0;
        var pip_move_index = 0;

        // miniepg 정보 노출 시간(해당 시간 경과 후 자동으로 사라짐)
        var displayTime = 0;

        var progressUpdateTime = 10000;

        var paramUpdate = false;

        /**
         * MiniEpgLayer show 호출 시 전달 되는 정보
         */
        var data = null;
        var prev_channel = null;
        var fromkey = false;
        var type = null; // REQUESTED , PERM_UPDATED
        var detailkey = false;
        var relatedKey = false;
        //var visible = true;

        //채널리스트 여부
        var isChannelListViewShow = false;

        var isRelatedViewShow = false;

        var isOTSProgramDetailViewShow = false;

        var TIMER_TYPE = {
            HIDE: 0,	// MiniEPG 자동숨김 타이머
            CLOCK: 1	// MiniEPG 현재시간 및 프로그램 진행바 업데이트 타이머
        };
        var timer_list = [];

        var cpmsinfoview = null;
        var leftview = null;
        var rightTopview = null;
        var rightBottomview = null;

        var chlistFocusChannel = null;

        var isShowCPMSInfo = false;

        var isExistReleatedContentDescriptor = false;
        var releatedContentCategoryId = null;
        var checkReleatedsSid = "";

        var mashupTitle = null;
        var mashupCount = 0;
        var mashupData = null;
        var mashupExire = null;

        var iptvBlockedChannelList = null;
        var skylifeBlockedChannelList = null;

        var deactivateTimer = null;

        var nextServiceState = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;

        var isHiddenPromoChannel = false;
        var defaultPromoChannelList = null;

        var mashup_msg = {
            from: KTW.CONSTANT.APP_ID.HOME,
            to: KTW.CONSTANT.APP_ID.MASHUP,
            method: KTW.managers.MessageManager.METHOD.MASHUP.REQ_ASSOCIATE_INFO_DATA,
            service: "T",
            ots: "N",
            id : ""
        };

        var parentalRating = 0;
        var isAudioChannel = false;

        this.create = function () {
            log.printDbg("create()");
            navAdapter = KTW.oipf.AdapterHandler.navAdapter;

            _createminiEpgBaseElement();
            if (KTW.CONSTANT.IS_OTS) {
                _createOTSCPMSInfoView();
            }
            _createMiniEpgLeftView();
            _createMiniEpgRightTopView();
            _createMiniEpgRightBottomView();

            navAdapter.addChannelListUpdateListener(_callbackChListUpdate);

            /**
             * miniepg 정보 노출 시간 설정값을 Storage에서 읽어옴
             * Storage에서 설정값이 변경 되었을 때 변경이 발생 되었을 때 Event를 받아 처리
             */
            displayTime = storageManager.ps.load(storageManager.KEY.MINIGUIDE_DISPLAY_TIME);
            if (displayTime == null) {
                displayTime = 3000;
                storageManager.ps.save(storageManager.KEY.MINIGUIDE_DISPLAY_TIME, displayTime);
            }
            else {
                displayTime = Number(displayTime);
            }
            storageManager.ps.addStorageDataChangeListener(_updateHideTime);

            // 현재 채널 조회
            cur_channel = navAdapter.getCurrentChannel();

            chlistFocusChannel = cur_channel;

            // 채널 리스트 조회 및 현재 채널 리스트 선택
            _callbackChListUpdate('init()');

            KTW.oipf.AdapterHandler.basicAdapter.addConfigChangeEventListener(onChangeUserPreference);
        };

        this.getDiv = function () {
            return div;
        };

        this.getPermission = function() {
            log.printDbg("getPermission() cur_channel_state : " + cur_channel_state);
            return cur_channel_state;
        };
        
        this.getDefaultPromoChannel = function() {
            if(defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
                return defaultPromoChannelList[0];
            }else {
                return null;
            }
        };


        this.show = function (options) {
            log.printDbg("show()");

            var visible;
            fromkey = false;
            data = null;
            prev_channel = cur_channel;
            type = null;
            //visible = true;
            //cur_channel = null;
            chlistFocusChannel = cur_channel;
            ischanneltune = false;
            relatedKey = false;

            var params = parent.getParams();

            if(deactivateTimer !== null) {
                clearTimeout(deactivateTimer);
                deactivateTimer = null;
            }

            log.printDbg("show() params : " + params !== null ? JSON.stringify(params) : " is null");
            log.printDbg("show() options : " + (options !== undefined && options !== null) ? JSON.stringify(options) : " is null");
            log.printDbg("show() cur_channel_state = " + cur_channel_state);

            /**
             * options parameter 정보 분석
             */
            visible = _parseOptions(params);


            if(detailkey !== undefined && detailkey !== null && detailkey === true) {
                _hideChannelList();
                
                var currentProgram = prog_list[prog_index + move_index];
                parent.getOTSChannelProgramDetailView().setData(cur_channel , currentProgram , move_index === 0 ? false : true , cur_channel_state , _callbackProgramDetailPopup);
                parent.getOTSChannelProgramDetailView().show();
                isOTSProgramDetailViewShow = true;
                _clearTimer(TIMER_TYPE.HIDE);
                div.css({display:"none"});
                return true;
            }

            if(relatedKey !== undefined && relatedKey !== null && relatedKey === true) {
                _showRelatedContentView();
                return true;
            }

            // 2017.05.19 dhlee
            // 부팅 시 부팅 되는 채널이 제한 채널인 경우 홈메뉴 자동 종료 후 미니EPG가 노출되지 않음.
            // cur_channel_state 값을 현재 permission에 맞게 update를 해 주는 부분이 있어야만 나머지 로직이 정상 동작 되는 구조임.
            // 즉, 부팅 후 perm 값을 기준으로 cur_channel_state 값을 set 해줘야 이후 동작이 정상적으로 수행되게 됨
            // 따라서 여기로 위치를 옮김
            //var isSkyCh = util.isSkyChoiceChannel(cur_channel);
            // 현재 채널 Permission를 MiniEpgLayer Type에 맞쳐 변경함.
            cur_channel_state = _permissionToMiniEpgState(last_perm, util.isSkyChoiceChannel(cur_channel));

            /**
             * 부팅 시 채널 Event에 대해서는 미니Epg를 노출 하지 않음.
             */
            log.printDbg("show() KTW.managers.service.StateManager.appState : " + KTW.managers.service.StateManager.appState);
            if(KTW.managers.service.StateManager.appState < KTW.CONSTANT.APP_STATE.STARTED) {
                return false;
            }

            /**
             *  숨김채널에서 다른 채널로 이동한 경우
             *  이전에 숨김채널을 ch_list에 추가하였기 때문에
             *  다른 채널로 이동할 때 ch_list에서 해당 숨김 채널을 제거해준다
             *  채널을 변경한 경우 여기에서 cur_ch_index는 아직 이전 채널의 index 값이다
             */
            log.printDbg("show() type : " + type);

            if (type === KTW.nav.Def.CHANNEL.EVENT.REQUESTED) {
                if (ch_list !== null && cur_ch_index>=0 && navAdapter.isSkippedChannel(ch_list[cur_ch_index]) === true) {
                    ch_list.splice(cur_ch_index, 1);
                }else if(isHiddenPromoChannel === true){ //} && cur_ch_index>=0 && ch_list !== null && ch_list[cur_ch_index].ccid !== cur_channel.ccid) {
                    log.printDbg("show() cur_ch_index : " + cur_ch_index);
                    if(cur_ch_index>=0) {
                        log.printDbg("show() ch_list[cur_ch_index].ccid : " + ch_list[cur_ch_index].ccid);
                    }
                    log.printDbg("show() cur_channel.ccid : " + cur_channel.ccid);
                    if(cur_ch_index>=0 && ch_list !== null && ch_list[cur_ch_index].ccid !== cur_channel.ccid) {
                        var temp_ch_list1 = null;
                        var temp_ch_list2 = null;
                        if(cur_ch_index>0) {
                            temp_ch_list1 = ch_list.slice(0, cur_ch_index);
                        }
                        temp_ch_list2 = ch_list.slice(cur_ch_index+1, ch_list.length);

                        if(temp_ch_list1 !== null) {
                            ch_list = temp_ch_list1.concat(defaultPromoChannelList , temp_ch_list2);
                        }else {
                            ch_list = defaultPromoChannelList.concat(temp_ch_list2);
                        }
                    }
                    isHiddenPromoChannel = false;
                    log.printDbg("show() type : " + type + " , isHiddenPromoChannel : " + isHiddenPromoChannel);
                }

                if(isHiddenPromoChannel === false) {
                    _isHiddenPromoChannel(cur_channel);
                }
            }

            /**
             * Mini Epg가 Hide 되기 전에 정보를 Setting 해야 함.
             */
            //parentalRating = KTW.oipf.AdapterHandler.casAdapter.getParentalRating();
            parentalRating = KTW.managers.auth.AuthManager.getPR();
            isAudioChannel = navAdapter.isAudioChannel(cur_channel);

            /**
             * 부팅 후에 홈메뉴 상태에서 나가기 키를 눌렀을 때 미니가이드가 노출 되지 않도록 수정함.
             */
            if (options !== undefined && options !== null && options.resume === true) {
                log.printDbg("show()paramUpdate : " + paramUpdate);
                if(paramUpdate === false) {
                    if (cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL ||
                        cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                        parent.hide({pause:true});

                        isExistReleatedContentDescriptor = false;
                        releatedContentCategoryId = null;
                        checkReleatedsSid = "";

                        mashupTitle = null;
                        mashupCount = 0;
                        mashupData = null;
                        mashupExire = null;

                        return false;
                    }
                }else {
                    /**
                     * Jira 이슈 사항 수정 WEBIIIHOME-1214
                     * 설정에서 시청 제한 채널설정 후 나가기 키로 설정 화면 나갈 경우 미니가이드가 노출 되지 않는 이슈 사항 수정함.
                     * linkage 를 false로 호출 하고 있는 상황임.
                     * 따라서 ChannelControl에서 activateLayer를 하더라도 현재 설정화면이 노출 되고 있어
                     * addstack만 호출 됨.(Parameter Setting까지는 정상)
                     * 그 이후에 설정 화면이 나갈 경우 show( resume)이 호출 되어 아래와 같이 수정함.
                     */

                    var tempState = _permissionToMiniEpgState(last_perm, util.isSkyChoiceChannel(cur_channel));
                    if (tempState === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL ||
                        tempState === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                        parent.hide({pause:true});

                        isExistReleatedContentDescriptor = false;
                        releatedContentCategoryId = null;
                        checkReleatedsSid = "";

                        mashupTitle = null;
                        mashupCount = 0;
                        mashupData = null;
                        mashupExire = null;

                        return false;
                    }
                }
            }

            /**
             * 현재 채널 및 채널목록 값 보정
             */
            if (cur_channel === null) {
                cur_channel = navAdapter.getCurrentChannel();
                chlistFocusChannel = cur_channel;
            }

            /**
             * iptv채널에서 skylife채널로 변경이 가능하므로
             * miniepg가 호출 될 때마다 채널 리스트 선택 해야함
             */
            _selectChList();

            /**
             * init함수가 호출 될 때 채널 Tune이 되지 않은 경우
             * 채널 리스트 조회 및 현재 채널 리스트 선택이 null이므로
             */
            // 채널 리스트 조회 및 현재 채널 리스트 선택이 null이므로
            if (ch_list == null || ch_list.length < 1) {
                _callbackChListUpdate('called by miniEPG.showView() because ch_list is empty!');
            }

            /**
             * Main Channel permission 이 시청시간제한/시청연령제한/미가입 인 경우에
             * PIP 영상이 노출 되어 있다면 먼저 PIP Stop 시킨다.
             * 멀티화면 실행 중 main을 OTS UHD 채널로 tune 하는 경우는 pipView에서 mainChEvent를 받아 처리
             */
            if (last_perm === KTW.nav.Def.CHANNEL.PERM.BLOCKED || last_perm === KTW.nav.Def.CHANNEL.PERM.PR_BLOCKED ||
                last_perm === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED) {
                _hideChannelList();
            }

            //var isSkyCh = util.isSkyChoiceChannel(cur_channel);
            //// 현재 채널 Permission를 MiniEpgLayer Type에 맞쳐 변경함.
            //cur_channel_state = _permissionToMiniEpgState(last_perm, isSkyCh);

            /**
             * visible 체크 하여 false이면 deactivateLayer 호출함.
             * 멀티화면인 경우에는 미니가이드가 노출 되면 안 됨.
             */
            if (visible === false) {
                log.printDbg("show() visible : " + visible);

                if (last_perm === KTW.nav.Def.CHANNEL.PERM.OK && parent.isShowing() === true) {

                    parent.hide({pause:true});

                    isExistReleatedContentDescriptor = false;
                    releatedContentCategoryId = null;
                    checkReleatedsSid = "";

                    mashupTitle = null;
                    mashupCount = 0;
                    mashupData = null;
                    mashupExire = null;

                    return false;
                }
            }

            /**
             * 채널정보표시시간이 0초인 경우에도 miniEPG를 띄우지 않기 때문에 false로 return
             * 단 blocked or audio channel인 경우와 확인키를 눌러서 미니가이드를 호출한 경우는 제외
             */
            /*
            if (last_perm === KTW.nav.Def.CHANNEL.PERM.OK && fromkey !== true) {
                if (displayTime === 0) {
                    rightBottomview.resetData();
                    deactivateTimer = setTimeout(function(){
                        layerManager.deactivateLayer({
                            id: KTW.ui.Layer.ID.MINI_EPG
                        });
                    }, 1);
                    return false;
                }
            }
            */

            /**
             * 부가상품 가입 후에 진행 중 팝업이 노출 된 후에
             * 정상적으로 CAS로부터 Event가 올라와 정상적으로 영상이 터지는 시점에
             * 팝업을 닫도록 수정함.
             */
            layerManager.deactivateLayer({
                id: "ChannelJoinProcessingPopup"
            });

            /**
             * OTS audio 채널은 항상 fullEPG 형태로만 존재. miniEPG를 띄우는 경우는 없음.
             */
            if (KTW.CONSTANT.IS_OTS === true && isAudioChannel === true) {
                return false;
            }

            if (KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                cpmsinfoview.hide();
            }
            isShowCPMSInfo = false;

            /**
             * 먼저 채널리스트를 Hide 시킨다.(채널리스트가 존재 하는 경우 obs_notify_hide 호출해야 함)
             */
            _hideChannelList();

            /**
             * 연관 컨텐츠를 Hide 시킨다.
             */
            _hideReleatedContentView();
            log.printDbg("show() isHiddenPromoChannel : " + isHiddenPromoChannel);
            if(isHiddenPromoChannel === true) {
                var tmp_idx = -1;
                var ch_list_length = ch_list !== null ? ch_list.length : 0;

                if(ch_list_length>0 && defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
                    for (var i = 0; i < ch_list_length; i++) {
                        if (Number(ch_list[i].majorChannel) >= Number(defaultPromoChannelList[0].majorChannel)) {
                            tmp_idx = i;
                            break;
                        }
                    }

                    if(tmp_idx>=0) {
                        var cur_channel_list = epgUtil.reconstructionChList([cur_channel], true);
                        var temp_ch_list1 = null;
                        var temp_ch_list2 = null;

                        if(tmp_idx>0) {
                            temp_ch_list1 = ch_list.slice(0, tmp_idx);
                        }
                        temp_ch_list2 = ch_list.slice(tmp_idx+1, ch_list.length);


                        if(temp_ch_list1 !== null) {
                            ch_list = temp_ch_list1.concat(cur_channel_list , temp_ch_list2);
                        }else {
                            ch_list = cur_channel_list.concat(temp_ch_list2);
                        }
                    }
                }
            }
            _reviseChannelList();

            /**
             * 현재 채널에 대한 프로그램 정보 조회
             */
            _getCurrentChannelProgram();

            if (relatedKey === true) {
                log.printDbg("show(), relatedKey is true");
                checkRelateData(_showRelatedContentView);
                return true;
            }

            /**
             * 연관컨텐츠 존재 여부 체크
             */
            checkRelateData();

            /**
             * 기존에 입력하였던 비밀번호 및 인증을 초기화 함.
             */
            _initializeAuth();

            if ((last_perm === KTW.nav.Def.CHANNEL.PERM.OK || last_perm === KTW.nav.Def.CHANNEL.PERM.PREVIEW) && fromkey !== true) {
                if (displayTime === 0) {
                    rightBottomview.resetData();
                    deactivateTimer = setTimeout(function(){
                        layerManager.deactivateLayer({
                            id: KTW.ui.Layer.ID.MINI_EPG
                        });
                    }, 1);
                    return false;
                }
            }


            var serviceState = KTW.managers.service.StateManager.serviceState;

            if(serviceState !== KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
                if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                    && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);
                    parent.hide({pause: true});
                    return false;
                }else {
                    if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                        cpmsinfoview.hide();
                        isShowCPMSInfo = false;
                    }
                    _hideChannelList();
                    /**
                     * jjh1117 2017/09/04
                     * 연관정보/VOD 화면 노출 여부 체크 및 로직 단순화
                     */
                    if(isRelatedViewShow === true) {
                        _hideReleatedContentView();
                    }
                    _clearTimer();
                    _hideMiniEpgMainView();
                    return false;
                }
            }

            div.css({display:""});

            /**
             * 현재 프로그램 표시
             */
            _showCurrentChannelProgram();

            /**
             * Timer 활성화
             */

            /**
             * NORMAL , PREVIEW 상태가 아니면 미니가이드는 사라지면 안됨.
             */
            if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                _clearTimer(TIMER_TYPE.HIDE);
            }else {
                _setTimer(TIMER_TYPE.HIDE);
            }

            _setTimer(TIMER_TYPE.CLOCK);

            KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);


            // if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
            //     && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
            //     KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);
            //     nextServiceState = KTW.CONSTANT.SERVICE_STATE.TV_VIEWING;
            // }

            KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);


            return true;
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");

            log.printDbg("hide(), isRelatedViewShow = " + isRelatedViewShow);

            //visible = true;
            /**
             * 2017.03.24 jjh1117
             * 손대중연구원과 협의한 내용임.
             * deactivateLayer 될때는 options 없으며 homemenu가 활성화 되는 경우에는
             * miniEpgLayer에 hide(options)으로 호출 됨.
             * 따라서, Timer가 동작 하고 있을 때는 deactivateLayer를 한다.
             */
            
            if(rightTopview !== null) {
                rightTopview.hide();
            }

            if(options !== undefined && options !== null) {
                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    if (isRelatedViewShow === true && relatedKey === true) {
                        _hideReleatedContentView();
                    }
                    _hideMiniEpgMainView();
                    return;
                }
            }

            if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                cpmsinfoview.hide();
                isShowCPMSInfo = false;
            }

            _hideChannelList();
            /**
             * jjh1117 2017/09/04
             * 연관정보/VOD 화면 노출 여부 체크 및 로직 단순화
             */
            if (isRelatedViewShow === true) {
                _hideReleatedContentView();
            }
            else if(KTW.CONSTANT.IS_OTS === true && isOTSProgramDetailViewShow === true) {
                _hideOTSProgramDetailView();
            }
            _clearTimer();

        };

        this.focus = function () {
            log.printDbg("focus()");

        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        function _callbackMashupData() {
            if (mashupCount !== 0 || releatedContentCategoryId !== null) {
                startRelatedView();
            }
        }

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;
            //if(visible === false) {
            //    return consumed;
            //}

            if (cur_channel === null) {
                cur_channel = navAdapter.getCurrentChannel();
                chlistFocusChannel = cur_channel;
            }

            /**
             * 미가입채널도 일반채널과 동일하게 N초후 사라짐
             * (2017.02.14:UI팀 형다혜)
             */
            _checkHideTimer();

            // skyChoice purchase process
            if (isChannelListViewShow === false && isRelatedViewShow === false && rightTopview.isBuyMovieChoice() === true &&
                cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT && KTW.CONSTANT.IS_OTS === true && isOTSProgramDetailViewShow === false) {
                consumed = rightTopview.controlKey(keyCode);
                if (consumed === true) {
                    return consumed;
                }
            }

            if (cur_channel.ccid === chlistFocusChannel.ccid && isChannelListViewShow === false && isRelatedViewShow === false && isOTSProgramDetailViewShow === false) {
                if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                    && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW
                    && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED_NORMAL) {
                    /**
                     * 시청제한/시청연령제한/시청시간제한/미가입 채널은
                     * 기본 키 처리에 대해서는 MiniEpgRightTopProgramView에서 처리
                     */
                    consumed = rightTopview.controlKey(keyCode);
                    if (consumed === true) {
                        return consumed;
                    }
                }
            }

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    if (isChannelListViewShow === true) {
                        consumed = parent.getChannelListView().controlKey(keyCode);
                    } else {
                        /**
                         * jjh1117 2017/09/04
                         * 연관정보/VOD 화면 및 OTS 자세히 화면이 없는 경우 인접채널 리스트 노출
                         */
                        if(isRelatedViewShow === false && isOTSProgramDetailViewShow === false) {
                            _showChannelList(KTW.KEY_CODE.UP);
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    log.printDbg("controlKey(), DOWN Key event");
                    if (isChannelListViewShow === true) {
                        parent.getChannelListView().controlKey(keyCode);
                    } else {
                        /**
                         * jjh1117 2017/09/04
                         * 연관정보/VOD 화면 및 OTS 자세히 화면이 없는 경우 인접채널 리스트 노출
                         */
                        if(isRelatedViewShow === false && isOTSProgramDetailViewShow === false) {
                            _showChannelList(KTW.KEY_CODE.DOWN);
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    /**
                     * jjh1117 2017/09/04
                     * Clean AV 상태에서 LEFT 키에 대해서 매시업 정보 체크
                     * 해당 부분은 기존에 DOWN키에서 처리 했던 부분을 그대로 적용함.
                     */
                    if (parent.isShowing() === false) {
                        fromkey = false;
                        checkRelateData(_callbackMashupData);
                        consumed = true;
                    }else {
                        if (isRelatedViewShow === true) {
                            consumed = parent.getRelatedContentView().controlKey(keyCode);
                            /**
                             * jjh1117 2017/09/04
                             * 연관정보/VOD View에서 Left 키를 consumed 하지 않은 경우
                             * 연관정보/VOD 화면 닫음(첫번째 컨텐츠에서 LEFT 키 처리
                             */
                            if(consumed === false) {
                                if (isRelatedViewShow === true) {
                                    _hideReleatedContentView();
                                }
                                consumed = true;
                            }
                        } else if(isOTSProgramDetailViewShow === true) {
                            consumed = parent.getOTSChannelProgramDetailView().controlKey(keyCode);
                        } else {
                            if (isChannelListViewShow === false) {
                                consumed = _moveLeftRight(true);
                                if (consumed === false) {
                                    /**
                                     * jjh1117 2017/09/04
                                     * 현재 LEFT Show Type이 RELATED_CONTENT_SHOW 경우 연광정보/VOD View Show 처리함.
                                     */
                                    if(leftview.getShowType() === KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW
                                        && isRelatedViewShow === false) {
                                        _showRelatedContentView();
                                        consumed = true;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case KTW.KEY_CODE.RIGHT:
                    if (isRelatedViewShow === true) {
                        consumed = parent.getRelatedContentView().controlKey(keyCode);
                    } else if(isOTSProgramDetailViewShow === true) {
                        consumed = parent.getOTSChannelProgramDetailView().controlKey(keyCode);
                    } else {
                        if (isChannelListViewShow === true) {
                            /**
                             * jjh1117 2017/09/04
                             * 인접채널 리스트에서 우측 키를 눌렀을 때
                             * 포커스 채널이 Main AV 채널인지 아닌지와 프로그램 탐색이 가능한지 가능하지 않은지에 따라
                             * 처리함.
                             * */
                            if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED_NORMAL
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                                    _hideChannelList(_callbackFuncCloseChannelList);
                                }else {
                                    if(chlistFocusChannel.desc !== 2 ) {
                                        var currentProgram = null;
                                        currentProgram = prog_list[prog_index];
                                        if(currentProgram.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                                            if(prog_list.length>(prog_index+1)) {
                                                move_index = 0;
                                                pip_move_index = 0;
                                                _hideChannelList(_callbackFuncCloseChannelListProgramSearch);
                                            }
                                        }
                                    }
                                }
                            }else {
                                if(pipChannelState !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                                    if(pipChannelState !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED 
                                        && pipChannelState !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                                        
                                        if(chlistFocusChannel.desc !== 2 ) {
                                            var currentProgram = null;
                                            
                                            currentProgram = pip_prog_list[pip_prog_index];
                                            if(currentProgram.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                                                if(pip_prog_list.length>(pip_prog_index+1)) {
                                                    move_index = 0;
                                                    pip_move_index = 0;
                                                    _hideChannelList(_callbackFuncCloseChannelListProgramSearch);
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            consumed = true;
                        } else {
                            consumed = _moveLeftRight(false);
                        }

                    }
                    break;
                case KTW.KEY_CODE.OK:
                    if (isChannelListViewShow === true) {
                        consumed = parent.getChannelListView().controlKey(keyCode);
                        if(consumed === false) {
                            _hideChannelList(_callbackFuncCloseChannelList);
                            consumed = true;
                        }
                    } else if(isOTSProgramDetailViewShow === true) {
                        consumed = parent.getOTSChannelProgramDetailView().controlKey(keyCode);
                    } else {
                        if (isRelatedViewShow === true) {
                            consumed = parent.getRelatedContentView().controlKey(keyCode);
                        } else {
                            consumed = rightBottomview.controlKey(keyCode);
                        }
                    }

                    log.printDbg("return consumed : " + consumed);
                    if (consumed === false) {

                        if (parent.isShowing() === true) {
                            if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                                if(move_index === 0) {
                                    if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT && KTW.managers.service.KidsModeManager.isKidsMode() === true) {

                                    }else {
                                        _hideMiniEpgMainView(true);
                                    }
                                }
                            }else {
                                if(pip_move_index === 0) {
                                    KTW.oipf.AdapterHandler.navAdapter.changeChannel(chlistFocusChannel);
                                }
                            }
                            consumed = true;
                        }
                    }

                    break;
                case KTW.KEY_CODE.EXIT:
                    if (parent.isShowing() === true) {
                        consumed = true;
                        if (isChannelListViewShow === true) {
                            chlistFocusChannel = cur_channel;
                            _hideChannelList(_callbackFuncCloseChannelList);
                        } else if (isRelatedViewShow === true) {
                            _hideReleatedContentView();
                        } else if(isOTSProgramDetailViewShow === true) {
                            _hideOTSProgramDetailView();

                            if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                                || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                _hideMiniEpgMainView(true);
                            }else {
                                div.css({display:""});
                            }
                        }
                        if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                            || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                            _hideMiniEpgMainView(true);
                        } else {
                            // 2017.05.19 dhlee WEBIIIHOME-1334 이슈 수정
                            // 시청연령제한, 미가입, 시청제한 채널 등 각종 제한 상태에서 미니EPG는 항상 노출되어야 한다.
                            // 위 상태에서 인접 채널 리스트 실행하여 인접 채널 리스트에서 다른 채널로 이동한 후 우키를 누른다.
                            // 현재 채널 정보가 아닌 인접 채널 리스트에서 선택된 채널&프로그램 정보가 보여지는 상황이 된다.
                            // 이때 나가기 키를 누르면 반응이 없다. 따라서 아래 조건을 추가하여 해결한다.
                            if (chlistFocusChannel.ccid != cur_channel.ccid) {
                                chlistFocusChannel = cur_channel;
                                _showCurrentChannelProgram();
                            }
                        }
                    }
                    break;
                case KTW.KEY_CODE.BACK:
                    log.printDbg("controlKey(), BACK key event, isShowing() = " + parent.isShowing());
                    log.printDbg("controlKey(), isRelatedViewShow = " + isRelatedViewShow);
                    if (parent.isShowing() === true) {

                        if (isChannelListViewShow === true) {
                            consumed = true;
                            move_index = 0;
                            pip_move_index = 0;
                            _hideChannelList(_callbackFuncCloseChannelList);

                            if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                                || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                _hideMiniEpgMainView(true);
                            }else {
                                chlistFocusChannel= cur_channel;

                                move_index = 0;
                                var currentProgram = prog_list[prog_index + move_index];
                                var nextProgram = null;
                                if(prog_list.length<=(prog_index+move_index+1)) {
                                    nextProgram = null;
                                }else {
                                    nextProgram = prog_list[prog_index + move_index+1];
                                }
                                var temp_state = cur_channel_state;

                                if(mashupExire !== null) {
                                    var today = new Date();
                                    log.printDbg("_moveLeftRight() mashupExire : " + mashupExire + " , today time : " + today.getTime());
                                    if (mashupExire < today.getTime()) {
                                        mashupTitle = null;
                                        mashupCount = 0;
                                        mashupData = null;
                                        mashupExire = null;

                                        if(releatedContentCategoryId === null) {
                                            isExistReleatedContentDescriptor = false;
                                        }
                                    }
                                }

                                var channelInfo = {
                                    "channel" : cur_channel,
                                    "currentProgram" : currentProgram,
                                    "nextProgram" : nextProgram,
                                    "state" : temp_state,
                                    "onlyProgramUpdate" : false,
                                    "isPipChannel" : false,
                                    "parental_rating" : parentalRating,
                                    "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                                    "mainvbo_channel" : cur_channel,
                                    "isHiddenPromoChannel" : isHiddenPromoChannel
                                };

                                /**
                                 * jjh1117 2017/09/04
                                 * leftView 화면 업데이트
                                 */
                                if(mashupCount !== 0) {
                                    if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                        || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                    }else {
                                        var imageURL = null;
                                        if(mashupData !== null && mashupData.length>0) {
                                            imageURL = mashupData[0].img;
                                        }

                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW , imageURL);
                                    }
                                }else if(isExistReleatedContentDescriptor === true){
                                    if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                        || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                    }else {
                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW);
                                    }
                                }else {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                }

                                rightTopview.show(channelInfo);
                                rightBottomview.show(channelInfo);
                            }
                            consumed = true;

                        }else if(isOTSProgramDetailViewShow === true) {
                            consumed = true;
                            _hideOTSProgramDetailView();
                            if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                                || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                _hideMiniEpgMainView(true);
                            }else {
                                div.css({display:""});
                            }
                        }else if(isRelatedViewShow === true){
                            _hideReleatedContentView();
                            consumed = true;
                        } else {
                            if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                                if(move_index === 0) {
                                    return false;
                                }
                            }

                            if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                                || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                _hideMiniEpgMainView(true);
                            }else {
                                chlistFocusChannel= cur_channel;
                                
                                move_index = 0;
                                var currentProgram = prog_list[prog_index + move_index];
                                var nextProgram = null;
                                if(prog_list.length<=(prog_index+move_index+1)) {
                                    nextProgram = null;
                                }else {
                                    nextProgram = prog_list[prog_index + move_index+1];
                                }
                                var temp_state = cur_channel_state;

                                if(mashupExire !== null) {
                                    var today = new Date();
                                    log.printDbg("_moveLeftRight() mashupExire : " + mashupExire + " , today time : " + today.getTime());
                                    if (mashupExire < today.getTime()) {
                                        mashupTitle = null;
                                        mashupCount = 0;
                                        mashupData = null;
                                        mashupExire = null;

                                        if(releatedContentCategoryId === null) {
                                            isExistReleatedContentDescriptor = false;
                                        }
                                    }
                                }

                                var channelInfo = {
                                    "channel" : cur_channel,
                                    "currentProgram" : currentProgram,
                                    "nextProgram" : nextProgram,
                                    "state" : temp_state,
                                    "onlyProgramUpdate" : false,
                                    "isPipChannel" : false,
                                    "parental_rating" : parentalRating,
                                    "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                                    "mainvbo_channel" : cur_channel,
                                    "isHiddenPromoChannel" : isHiddenPromoChannel
                                };

                                /**
                                 * jjh1117 2017/09/04
                                 * leftView 화면 업데이트
                                */
                                if(mashupCount !== 0) {
                                    if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                        || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                    }else {
                                        var imageURL = null;
                                        if(mashupData !== null && mashupData.length>0) {
                                            imageURL = mashupData[0].img;
                                        }

                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW , imageURL);
                                    }
                                }else if(isExistReleatedContentDescriptor === true){
                                    if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                        || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                    }else {
                                        leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW);
                                    }
                                }else {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                }

                                rightTopview.show(channelInfo);
                                rightBottomview.show(channelInfo);
                            }
                            consumed = true;
                        }
                    }
                    break;
                case KTW.KEY_CODE.CONTEXT :
                    if (isRelatedViewShow === true) {
                        consumed = parent.getRelatedContentView().controlKey(keyCode);
                    }else if(isOTSProgramDetailViewShow === true) {
                        /**
                         * 자세히 화면이 노출 되었을 때는 연관메뉴 키 동작 하지 않도록 적용
                         */                        
                    } else {
                        if (parent.isShowing() === false) {
                            /**
                             * WEBIIIHOME-785 이슈 사항 해결
                             * permission이 OK인 경우 자막/화면 해설 정보 단말로부터 얻는다.
                             * 음성다중-->자막Only프로글매으로 진행 한 경우 업데이트 되지 않은
                             * 이슈가 있어 무조건 호출 하도록 수정함.
                             */
                            if (last_perm === KTW.nav.Def.CHANNEL.PERM.OK && navAdapter.isAudioChannel(cur_channel) === false) {
                                epgUtil.progInfo.updateInfo(KTW.nav.Def.CONTROL.MAIN);
                            }
                            else {
                                epgUtil.progInfo.init();
                            }

                            move_index = 0;
                        }
                        _showRelatedMenu();
                    }

                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.getRightTopView = function () {
            return rightTopview;
        };

        this.getRightBottomView = function () {
            return rightBottomview;
        };
        /**
         * 인접채널 리스트에서 포커스 이동 시 포커스 이동 채널에 대한 채널 정보 노출
         * @param ch
         */
        this.updateProgramInfo = function(ch , pipState , programlist , programmeID) {
            log.printDbg('updateProgramInfo() programlist = ' + JSON.stringify(programlist));
            var currentProgram = null;
            var nextProgram = null;

            if(ch !== undefined && ch !== null) {
                chlistFocusChannel = ch;
            }

            if(pipState !== null) {
                pipChannelState = pipState;
            }

            if(programlist !== undefined && programlist !== null) {
                pip_prog_list = programlist;
            }else {
                if(programmeID === undefined) {
                    var programManager = KTW.oipf.adapters.ProgramSearchAdapter;
                    pip_prog_list = programManager.getPrograms(null, null, null, chlistFocusChannel);
                    if(pip_prog_list !== undefined ) {
                        pip_prog_list = epgUtil.revisePrograms(chlistFocusChannel, pip_prog_list,true);
                    }
                }
            }

            pip_prog_index = 0;

            var now = new Date().getTime();
            if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                now = Math.floor(now/1000);
            }

            if(pip_prog_list !== undefined && pip_prog_list !== null && pip_prog_list.length>0) {
                var prog = pip_prog_list[pip_prog_index];
                while((prog.startTime + prog.duration) < now && pip_prog_index < pip_prog_list.length - 1) {
                    pip_prog_index++;
                    prog = pip_prog_list[pip_prog_index];
                    if (prog == null) {
                        pip_prog_index = -1;
                        break;
                    }
                }

                if(pip_prog_index>=0) {
                    var nextprog = null;
                    if(programmeID !== undefined && programmeID !== null) {
                        for(var i=pip_prog_index;i<pip_prog_list.length;i++) {
                            if(pip_prog_list[i].programmeID === programmeID) {
                                currentProgram = pip_prog_list[i];
                                nextprog = pip_prog_list[i+1];
                                break;
                            }
                        }

                        if(currentProgram === null) {
                            currentProgram = pip_prog_list[pip_prog_index];
                            nextprog = pip_prog_list[pip_prog_index+1];
                        }
                    }else {
                        currentProgram = pip_prog_list[pip_prog_index];
                        nextprog = pip_prog_list[pip_prog_index+1];
                    }
                    if(nextprog !== undefined && nextprog !== null) {
                        nextProgram = nextprog;
                    }

                    if(currentProgram !== null && currentProgram.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                        nextProgram = null;
                    }
                }

            }

            var state = pipState;
            if(chlistFocusChannel.ccid === cur_channel.ccid) {
                state = cur_channel_state;
                if(chlistFocusChannel.desc !== 2) {
                    prog_list = pip_prog_list;
                    prog_index = pip_prog_index;
                }
            }

            var channelInfo = {
                "channel" : chlistFocusChannel,
                "currentProgram" : currentProgram,
                "nextProgram" : nextProgram,
                "state" : state,
                "onlyProgramUpdate" : false,
                "isPipChannel" : true,
                "parental_rating" : parentalRating,
                "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                "mainvbo_channel" : cur_channel,
                "isHiddenPromoChannel" : isHiddenPromoChannel
            };
            rightTopview.show(channelInfo);
            rightBottomview.show(channelInfo);
            rightBottomview.hideOKKeyIcon();

        };

        this.startMovieChoicePPVPurchase = function() {
            log.printDbg('startMovieChoicePPVPurchase()');

            _clearTimer(TIMER_TYPE.HIDE);
            leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.PROGRESS_LINE_SHOW);
            rightBottomview.startMovieChoicePPVPurchase();
        };

        this.stopMovieChoicePPVPurchase = function() {
            log.printDbg('stopMovieChoicePPVPurchase()');
            _showCurrentChannelProgram();
            _checkHideTimer();
        };

        this.showOTSCPMSInfo = function(cpmsinfodata,focusIndex) {

            if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.PROGRESS_LINE_SHOW);
                rightTopview.hideJoinBtn();

                cpmsinfoview.show(cpmsinfodata , focusIndex);
                isShowCPMSInfo = true;
            }
        };

        this.hideOTSCPMSInfo = function() {
            if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                cpmsinfoview.hide();
                isShowCPMSInfo = false;
            }
        };

        this.updateFocusOTSCPMSInfo = function(focusIndex) {
            if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                cpmsinfoview.updateFocusIndex(focusIndex);
            }
        };

        this.getIsShowCPMSInfo = function() {
            return isShowCPMSInfo;
        };

        this.requestHideReleatedContentView = function() {
            _hideReleatedContentView();
        };

        /**
         * 2017.06.15 dhlee
         * 현재 채널에 대한 연관 콘텐츠와 연관 정보 존재 여부를 확인한다.
         */
        function checkRelateData(callback) {
            log.printDbg("checkRelateData()");

            if(checkReleatedsSid !== cur_channel.sid) {
                isExistReleatedContentDescriptor = false;
                releatedContentCategoryId = null;
                checkReleatedsSid = cur_channel.sid;
            }

            if(callback !== undefined && callback !== null) {
                _getCurrentChannelProgram();
            }

            if (navAdapter.isAudioChannel(cur_channel) === false &&
                prog_list[prog_index] != null && util.isPlayingProg(prog_list[prog_index]) === true) {
                // 연관 콘텐츠
                var tmp_prog = prog_list[prog_index];
                if (KTW.CONSTANT.IS_OTS === true && cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S) {
                    var sameKTCh = navAdapter.getSameKTChannel(cur_channel);
                    if (sameKTCh != null) {
                        var list = KTW.oipf.adapters.ProgramSearchAdapter.getPrograms(null, null, null, sameKTCh);
                        if (list !== undefined && list !== null && list.length > 0) {
                            tmp_prog = list[util.getPlayingProg(list)];
                        } else {
                            tmp_prog = null;
                        }
                    }
                }
                else if (cur_channel.desc === 1 || cur_channel.desc === 3) {
                    var list = KTW.oipf.adapters.ProgramSearchAdapter.getPrograms(null, null, null, cur_channel);
                    if (list !== undefined && list !== null && list.length > 0) {
                        tmp_prog = list[util.getPlayingProg(list)];
                    } else {
                        tmp_prog = null;
                    }
                }

                try {
                    if (tmp_prog != null) {
                        var ext = util.getExtendedEventDescriptor(tmp_prog.getSIDescriptors(0x4E));
                        if (ext != null && ext.vcId != null) {
                            log.printDbg('vcId : ' + ext.vcId);
                            isExistReleatedContentDescriptor = true;
                            releatedContentCategoryId = ext.vcId;
                        }
                    }
                }
                catch (e) {
                    log.printWarn('composeContextMenu e: ' + e.message);
                }
            }

            /**
             * MashUP 연동
             * DCA , 채널 +/- 으로 인하여 채널 변경이 일어난 경우에는
             * mashup 연동을 기본적으로 다시 해야 하며
             * 편성표에서 채널 변경이 일어 난 경우에서도 ismashupAPICalled를 초기화 하여
             * 다시 mashup 연동을 시도한다.
             */
            if(fromkey === undefined || fromkey === false ) {
                if(mashup_msg.id === "" || (mashup_msg.id !== cur_channel.sid)) {
                    mashupTitle = null;
                    mashupCount = 0;
                    mashupData = null;
                    mashupExire = null;
                    mashup_msg.ots = (cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) ? "Y" : "N";
                    mashup_msg.id = cur_channel.sid;
                    KTW.managers.MessageManager.sendMessage(mashup_msg, function (res_msg) {
                        _callbackMashupMsg(res_msg);
                        if (callback) {
                            callback();
                        }
                    });
                }else {
                    if (callback) {
                        callback();
                    }
                }
            }else {
                if(mashup_msg.id === "" || (mashup_msg.id !== cur_channel.sid)) {
                    mashupTitle = null;
                    mashupCount = 0;
                    mashupData = null;
                    mashupExire = null;
                    mashup_msg.ots = (cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) ? "Y" : "N";
                    mashup_msg.id = cur_channel.sid;
                    KTW.managers.MessageManager.sendMessage(mashup_msg, function (res_msg) {
                        _callbackMashupMsg(res_msg);
                        if (callback) {
                            callback();
                        }
                    });
                }else {
                    if(mashupExire !== null) {
                        var today = new Date();
                        log.printDbg("show() mashupExire : " + mashupExire + " , today time : " + today.getTime());
                        if(mashupExire<today.getTime()) {

                            if(releatedContentCategoryId === null) {
                                isExistReleatedContentDescriptor = false;
                            }

                            mashupTitle = null;
                            mashupCount = 0;
                            mashupData = null;
                            mashupExire = null;
                            mashup_msg.ots = (cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) ? "Y" : "N";
                            mashup_msg.id = cur_channel.sid;
                            KTW.managers.MessageManager.sendMessage(mashup_msg, function (res_msg) {
                                _callbackMashupMsg(res_msg);
                                if (callback) {
                                    callback();
                                }
                            });
                        }
                    }
                }
            }
        }

        function startRelatedView() {
            log.printDbg("startRelatedView(), mashupCount = " + mashupCount + ", relatedContentCategoryId = " + releatedContentCategoryId);

            var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.MINI_EPG);
            var clear_stack = true;
            if (layer) {
                clear_stack = false;
            }

            KTW.ui.LayerManager.activateLayer({
                obj         : {
                    id      : KTW.ui.Layer.ID.MINI_EPG,
                    type    : KTW.ui.Layer.TYPE.TRIGGER,
                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                    linkage : false,
                    params  : {
                        relatedKey: true
                    }
                },
                clear_normal: clear_stack,
                visible     : true,
                cbActivate  : function () {

                }
            });
        }
        /**
         * oipf configuration 값이  변경되었을때 호출되는 리스너 함수
         */
        function onChangeUserPreference(evt) {
            if (evt !== undefined && evt !== null) {
                log.printDbg("onChangeUserPreference() evt.key : " + evt.key + " , evt.value : " + evt.value);
                if (evt.key === KTW.oipf.Def.CONFIG.KEY.ADULT_MENU_DISPLAY) {
                    kt_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SURFABLE);
                    kt_ch_list = epgUtil.reconstructionChList(kt_ch_list , true);
                    if (KTW.CONSTANT.IS_OTS === true) {
                        sky_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SURFABLE);
                        sky_ch_list = epgUtil.reconstructionChList(sky_ch_list , true);
                    }
                    _selectChList();
                }else if(evt.key === KTW.oipf.Def.CONFIG.KEY.USE_KIDS_MODE) {
                    if(evt.value === "ON") {
                        kt_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_KIDS_MODE);
                        kt_ch_list = epgUtil.reconstructionChList(kt_ch_list , true);
                        if (KTW.CONSTANT.IS_OTS === true) {
                            sky_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_MODE);
                            sky_ch_list = epgUtil.reconstructionChList(sky_ch_list , true);
                        }
                    }else {
                        kt_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SURFABLE);
                        kt_ch_list = epgUtil.reconstructionChList(kt_ch_list , true);
                        if (KTW.CONSTANT.IS_OTS === true) {
                            sky_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SURFABLE);
                            sky_ch_list = epgUtil.reconstructionChList(sky_ch_list , true);
                        }
                    }
                    _selectChList();

                    KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
                    if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                        cpmsinfoview.hide();
                        isShowCPMSInfo = false;
                    }
                    _hideChannelList();
                    if(isRelatedViewShow === true) {
                        _hideReleatedContentView();
                    }
                    _clearTimer();
                    _hideMiniEpgMainView();
                }
            }
        }

        function _createminiEpgBaseElement() {
            /**
             * 하단 전체
             */
            if(KTW.CONSTANT.IS_OTS) {
                cpmsinfoDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                        }
                    },
                    parent: parent.div
                });
            }

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 512, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 568
                    }
                },
                parent: parent.div
            });



            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "miniepg_background",
                    src: "images/miniepg/bg_miniguide.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 1920,
                        height: 568,
                        opacity:0.9
                    }
                },
                parent: div
            });

            leftDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_search_div",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 370, height: 568
                    }
                },
                parent: div
            });

            rightDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "miniepg_right_area",
                    css: {
                        position: "absolute",
                        "-webkit-transition": "-webkit-transform 0.5s"
                    }
                },
                parent: div
            });

            rightTopProgramDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_program_info_div",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 1550, height: 358
                    }
                },
                parent: rightDiv
            });

            rightBottomProgramTimeDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "channel_program_time_div",
                    css: {
                        position: "absolute", left: 0, top: 358, width: 1550, height: 210
                    }
                },
                parent: rightDiv
            });
        }



        function onServiceStateChange (options) {
            log.printDbg("onServiceStateChange() options.nextServiceState : " + options.nextServiceState +
                " , cur_channel_state : " + cur_channel_state + " , parent.isShowing() : " + parent.isShowing() + " , nextServiceState : " + nextServiceState);
            if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED ||
                options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
                if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                    cpmsinfoview.hide();
                    isShowCPMSInfo = false;
                }
                _hideChannelList();
                if(isRelatedViewShow === true) {
                    _hideReleatedContentView();
                }
                _clearTimer();
                _hideMiniEpgMainView();
                return;

            }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV) {
                if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                    && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    if(parent.isShowing() === true) {
                        parent.hide({pause: true});
                    }
                    nextServiceState = KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV;
                    return;
                }else {
                    KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
                    if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                        cpmsinfoview.hide();
                        isShowCPMSInfo = false;
                    }
                    _hideChannelList();
                    if(isRelatedViewShow === true) {
                        _hideReleatedContentView();
                    }
                    _clearTimer();
                    _hideMiniEpgMainView();
                }
            }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.VOD || options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
                if(KTW.CONSTANT.IS_OTS && cpmsinfoview !== null) {
                    cpmsinfoview.hide();
                    isShowCPMSInfo = false;
                }
                _hideChannelList();
                if(isRelatedViewShow === true) {
                    _hideReleatedContentView();
                }
                _clearTimer();
                _hideMiniEpgMainView();
                return;
            }else if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
                if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                    && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    var lastLayer = KTW.ui.LayerManager.getLastLayerInStack();
                    KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);
                    if (lastLayer !== null && lastLayer.isShowing() === true && lastLayer.type === KTW.ui.Layer.TYPE.NORMAL) {

                    }else {
                        parent.show({resume:true});
                    }

                    return;
                }
            }
        }


        /**
         * OTS CPMS Info 정보 노출 DIV
         */
        function _createOTSCPMSInfoView() {
            cpmsinfoview = new KTW.ui.view.OTSCPMSInfoView(cpmsinfoDiv);
            cpmsinfoview.create();
        }

        /**
         * 좌측 채널 탐색 영역 DIV
         */
        function _createMiniEpgLeftView() {
            leftview = new KTW.ui.view.MiniEpgLeftView(leftDiv);
            leftview.create();
        }

        /**
         * 우측 상단 채널 정보 및 프로그램 정보 영역 DIV
         */
        function _createMiniEpgRightTopView() {
            rightTopview = new KTW.ui.view.MiniEpgRightTopProgramView(rightTopProgramDiv);
            rightTopview.setParent(parent.getMiniEpgMainView());
            rightTopview.create();
        }


        /**
         * 우측 하단 진행바,프로그램 시작/끝 시간 정보 , 프로그램 세부정보 아이콘 영역 DIV
         */
        function _createMiniEpgRightBottomView() {
            rightBottomview = new KTW.ui.view.MiniEpgRightBottomProgramTimeView(rightBottomProgramTimeDiv);
            rightBottomview.setParent(parent.getMiniEpgMainView());
            rightBottomview.create();
        }

        /**
         * 채널목록이 변경되는 경우 miniEPG에서 사용하는 channel list도 update 해주기 위해
         * navAdapter를 통해 등록하는 event listener
         */
        function _callbackChListUpdate(evt) {
            log.printDbg('_callbackChListUpdate() evt = ' + evt);
            var isKidsMode = KTW.managers.service.KidsModeManager.isKidsMode();
            if(isKidsMode === undefined) {
                return;
            }
            if(isKidsMode === true) {
                log.printDbg('_callbackChListUpdate() isKidsMode true');
                kt_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_KIDS_MODE);
                kt_ch_list = epgUtil.reconstructionChList(kt_ch_list , true);
                if (KTW.CONSTANT.IS_OTS === true) {
                    sky_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_KIDS_MODE);
                    sky_ch_list = epgUtil.reconstructionChList(sky_ch_list , true);
                }
            }else {
                log.printDbg('_callbackChListUpdate() isKidsMode false');
                kt_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SURFABLE);
                kt_ch_list = epgUtil.reconstructionChList(kt_ch_list , true);
                if (KTW.CONSTANT.IS_OTS === true) {
                    sky_ch_list = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SURFABLE);
                    sky_ch_list = epgUtil.reconstructionChList(sky_ch_list , true);
                }
            }

            var iptvBlockList = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_BLOCKED);
            if (iptvBlockList !== undefined && iptvBlockList !== null && iptvBlockList.length>0) {
                iptvBlockedChannelList = iptvBlockList;
            } else {
                iptvBlockedChannelList = null;
            }

            if (KTW.CONSTANT.IS_OTS) {
                var skyBlockList = navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_BLOCKED);
                if(skyBlockList !== undefined && skyBlockList !== null && skyBlockList.length>0) {
                    skylifeBlockedChannelList = skyBlockList;
                } else {
                    skylifeBlockedChannelList = null;
                }
            }

            _selectChList();
        }

        // 현재 재생 중인 채널에 따라 채널 리스트를 가져 온다.
        // DVB_S/DVB_S2 : skylife 채널 리스트
        // IPTV_SDS/IPTV_URI : kt iptv 채널 리스트
        function _selectChList() {
            log.printDbg('_selectChList() cur_channel.idType : ' + cur_channel.idType);

            switch (cur_channel.idType) {
                case KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS :
                case KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI :
                    ch_list = kt_ch_list;
                    break;
                case KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S :
                case KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2 :
                    ch_list = sky_ch_list;
                    break;
                default :
                    if (KTW.CONSTANT.IS_OTS === true) {
                        ch_list = sky_ch_list;
                    }
                    else {
                        ch_list = kt_ch_list;
                    }
                    break;
            }
            log.printDbg('_selectChList() return ch_list length : ' + ch_list.length);
        }


        function _updateHideTime(key) {
            if (key === storageManager.KEY.MINIGUIDE_DISPLAY_TIME) {
                log.printDbg('_updateHideTime() key : ' + key);
                var stime = storageManager.ps.load(storageManager.KEY.MINIGUIDE_DISPLAY_TIME);
                if (stime != null) {
                    displayTime = Number(stime);
                }
            }
        }

        function _parseOptions(params) {
            paramUpdate = false;
            var visible = true;
            /**
             * jjh1117 2016.10.14
             * options.obj
             *  params: {
             *       data : channel_event,
             *       visible:clear_stack,
             *       fromKey:true
             * }
             *  data : ChannelControl에서 호출 할때
             *  fromKey : LayerManager에서 호출 할때
             */
            log.printDbg('_parseOptions() params = ' + JSON.stringify(params));

            if(params !== undefined && params !== null) {
                fromkey = params.fromKey;
                data = params.data;
                visible = params.visible;
                detailkey = params.detailKey;
                relatedKey = params.relatedKey;
                if(detailkey !== undefined && detailkey !== null && detailkey === true) {
                    return  true;
                }
                if (relatedKey !== undefined && relatedKey !== null && relatedKey === true) {
                    return true;
                }

                // ChannelControl에서 호출 된 경우
                if(fromkey === undefined || fromkey === false) {
                    if((cur_channel !== null && data.channel.ccid === cur_channel.ccid) && last_perm !== data.permission) {
                        paramUpdate = true;
                    }
                    cur_channel = data.channel;
                    type = data.type;
                    chlistFocusChannel = cur_channel;
                    last_perm = data.permission;
                    log.printDbg('_parseOptions() last_perm = ' + last_perm);
                    if(visible === true) {
                        // 블루투스 사용하다가 재부팅을 하게 되면 하단 블루투스 연결 Feedback이 노출 되는데
                        // MiniEpg와 Feedback이 곁쳐 보일 수 있으므로 Feedback을 Hide 시켜야함.
                        KTW.managers.service.SimpleMessageManager.hide();
                    }
                }else {
                    if(data !== undefined && data !== null) {
                        if(cur_channel !== null && data.channel !== undefined) {
                            if(data.channel.ccid !== cur_channel.ccid) {
                                paramUpdate = true;
                                cur_channel = data.channel;
                                type = data.type;
                                chlistFocusChannel = cur_channel;
                                last_perm = data.permission;
                                log.printDbg('_parseOptions() last_perm = ' + last_perm);
                            }else {
                                if(last_perm !== data.permission) {
                                    paramUpdate = true;
                                    cur_channel = data.channel;
                                    type = data.type;
                                    chlistFocusChannel = cur_channel;
                                    last_perm = data.permission;
                                    log.printDbg('_parseOptions() last_perm = ' + last_perm);
                                }
                            }
                        }
                    }
                }

                /**
                 * permission이 OK인 경우 자막/화면 해설 정보 단말로부터 얻는다.
                 * 음성다중-->자막Only프로글매으로 진행 한 경우 업데이트 되지 않은
                 * 이슈가 있어 무조건 호출 하도록 수정함.
                 */
                if (last_perm === KTW.nav.Def.CHANNEL.PERM.OK && navAdapter.isAudioChannel(cur_channel) === false) {
                    epgUtil.progInfo.updateInfo(KTW.nav.Def.CONTROL.MAIN);
                }
                else {
                    epgUtil.progInfo.init();
                }
            }

            return visible;
        }

        // 현재 채널 Permission를 MiniEpgLayer Type에 맞쳐 변경함.
        function _permissionToMiniEpgState(temp_perm , isSkyCh) {
            log.printDbg('_permissionToMiniEpgState() last_perm : ' + temp_perm);
            var temp_state = MINIEPGLAYER_DEF.PROGRAM_STATE.NORMAL;

            if (temp_perm === KTW.nav.Def.CHANNEL.PERM.BLOCKED) {
                temp_state = MINIEPGLAYER_DEF.PROGRAM_STATE.BLOCKED;
            }
            else if (temp_perm === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED && isSkyCh === true) {
                temp_state = MINIEPGLAYER_DEF.PROGRAM_STATE.NOT_SUBSCRIBED_NORMAL;
            }
            else if (temp_perm === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED) {
                temp_state = MINIEPGLAYER_DEF.PROGRAM_STATE.NOT_SUBSCRIBED;
            }
            else if (temp_perm === KTW.nav.Def.CHANNEL.PERM.PR_BLOCKED) {
                temp_state = MINIEPGLAYER_DEF.PROGRAM_STATE.AGE_LIMIT;
            }
            else if (temp_perm === KTW.nav.Def.CHANNEL.PERM.PREVIEW) {
                temp_state = MINIEPGLAYER_DEF.PROGRAM_STATE.PREVIEW;
            }
            log.printDbg('_permissionToMiniEpgState() return temp_state : ' + temp_state);

            return temp_state;
        }


        function _hideChannelList(callbackFunc) {
            log.printDbg('_hideChannelList() isChannelListViewShow : ' + isChannelListViewShow);
            if (isChannelListViewShow === true) {
                isChannelListViewShow = false;
                _sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE);

                rightDiv.removeClass("channellist_show");
                parent.getChannelListView().hide();
                if(callbackFunc !== undefined && callbackFunc !== null) {
                    //parent.getChannelListView().closePIP();
                    callbackFunc();
                }
                //else {
                //    parent.getChannelListView().hide();
                //    isChannelListViewShow = false;
                //    rightDiv.removeClass("channellist_show");
                //    _sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE);
                //}
            }
        }

        /**
         * jjh1117 2017/09/04
         * 인접채널 리스트 닫힌 후에 프로그램 탐색을 바로 처리 하도록 수정함.
         */
        function _callbackFuncCloseChannelListProgramSearch() {
            log.printDbg('_callbackFuncCloseChannelListProgramSearch() ');
            //isChannelListViewShow = false;
            //
            _checkHideTimer();
            //
            //_sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE);
            //
            //rightDiv.removeClass("channellist_show");
            //parent.getChannelListView().closeView();
            _moveLeftRight(false);
        }

        function _callbackFuncCloseChannelList() {
            _showCurrentChannelProgram();

            //isChannelListViewShow = false;
            //
            _checkHideTimer();
            //
            //_sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE);
            //
            //rightDiv.removeClass("channellist_show");
            //parent.getChannelListView().closeView();
        }

        function _showChannelList(keyCode) {
            log.printDbg('_showChannelList() isChannelListViewShow : ' + isChannelListViewShow);

            if (isChannelListViewShow === false) {
                _reviseChannelList();

                var temp_ch_index = cur_ch_index;
                if(chlistFocusChannel !== null && chlistFocusChannel.ccid !== cur_channel.ccid) {
                    for (var i=0; i<ch_list.length; i++) {
                        if (Number(ch_list[i].majorChannel) >= Number(chlistFocusChannel.majorChannel)) {
                            temp_ch_index = i;
                            break;
                        }
                    }
                }

                /**
                 * 인접채널 리스트 노출 시작을 상/하로 이동후 화면 노출함.
                 */
                if(keyCode === KTW.KEY_CODE.UP) {
                    temp_ch_index++;
                    if(temp_ch_index >(ch_list.length - 1)) {
                        temp_ch_index = 0;
                    }
                }else {
                    temp_ch_index--;
                    if(temp_ch_index <0) {
                        temp_ch_index = ch_list.length - 1;
                    }
                }

                parent.getChannelListView().setChannelList(ch_list , temp_ch_index , cur_channel , iptvBlockedChannelList , skylifeBlockedChannelList);
                parent.getChannelListView().show();

                isChannelListViewShow = true;

                leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.HIDE);
                rightDiv.addClass("channellist_show");
                _clearTimer(TIMER_TYPE.HIDE);

                _sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW);


                /**
                 * 미리보기 가입 아이콘/뮤비초이스 구매 아이콘 노출 상태에서 인접채널 리스트 진입 시
                 * 아이콘 미노출
                 */
                rightBottomview.hideOKKeyIcon();
            }
        }

        function _showRelatedContentView() {
            log.printDbg('_showRelatedContentView() isRelatedViewShow : ' + isRelatedViewShow);

            if (mashupCount !== 0 || releatedContentCategoryId !== null) {
                if(isRelatedViewShow === false) {
                    parent.getRelatedContentView().setData( mashupTitle === null ? "연관 정보/VOD" : mashupTitle , releatedContentCategoryId , mashupCount , mashupData);
                    parent.getRelatedContentView().show();
                    isRelatedViewShow = true;
                    _clearTimer(TIMER_TYPE.HIDE);
                    div.css({display:"none"});

                    /**
                     * 연관 정보 / VOD 노출 할 때 obs_request_show를 호출 하지 않으면
                     * 매시업 앱이랑 같이 곁치는 이슈 해결함.
                     */
                    _sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW);
                }
            } else {
                if(relatedKey === true) {
                    _hideMiniEpgMainView();
                }
                isRelatedViewShow = false;
                relatedKey = false;
            }
        }

        function _hideReleatedContentView() {
            log.printDbg('_hideReleatedContentView() isRelatedViewShow : ' + isRelatedViewShow);

            if(isRelatedViewShow === true) {
                if (relatedKey === true) {
                    relatedKey = false;
                    isRelatedViewShow = false;
                    parent.getRelatedContentView().hide();

                    div.css({display:""});

                    layerManager.deactivateLayer({
                        id: KTW.ui.Layer.ID.MINI_EPG
                    });
                } else {
                    parent.getRelatedContentView().hide();
                    isRelatedViewShow = false;
                    _checkHideTimer();

                    div.css({display:""});
                    /**
                     * 연관 정보 / VOD 노출 할 때 obs_request_show를 호출 하지 않으면
                     * 매시업 앱이랑 같이 곁치는 이슈 해결함.
                     */
                    _sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE);
                }
            }
        }

        function _showOTSProgramDetailView(otsChannel , otsProgram , isOTSShowReservation , currentChannelState , miniEpgCallback) {
            log.printDbg('_showOTSProgramDetailView() isOTSProgramDetailViewShow : ' + isOTSProgramDetailViewShow + " ,  parent.isShowing() : " +  parent.isShowing());

            if(KTW.CONSTANT.IS_OTS === true && isOTSProgramDetailViewShow === false && parent.isShowing() === true) {
                _hideChannelList();
                
                parent.getOTSChannelProgramDetailView().setData(otsChannel , otsProgram , isOTSShowReservation , currentChannelState , miniEpgCallback);
                parent.getOTSChannelProgramDetailView().show();
                isOTSProgramDetailViewShow = true;
                _clearTimer(TIMER_TYPE.HIDE);
                div.css({display:"none"});
            }else {
                var layer = KTW.ui.LayerManager.getLayer(KTW.ui.Layer.ID.MINI_EPG);
                var clear_stack = true;
                if (layer) {
                    clear_stack = false;
                }


                KTW.ui.LayerManager.activateLayer({
                    obj: {
                        id: KTW.ui.Layer.ID.MINI_EPG,
                        type: KTW.ui.Layer.TYPE.TRIGGER,
                        priority: KTW.ui.Layer.PRIORITY.NORMAL,
                        linkage: false,
                        params: {
                            detailKey:true
                        }
                    },
                    clear_normal: clear_stack,
                    visible: true,
                    cbActivate: function() {

                    }
                });
            }

        }

        function _hideOTSProgramDetailView() {
            log.printDbg('_hideOTSProgramDetailView() isOTSProgramDetailViewShow : ' + isOTSProgramDetailViewShow);
            if(KTW.CONSTANT.IS_OTS === true && isOTSProgramDetailViewShow === true) {
                isOTSProgramDetailViewShow = false;
                parent.getOTSChannelProgramDetailView().hide();
                div.css({display:""});
            }
        }

        // 숨김채널 처리와 함께 채널리스트 및 채널 index값 설정
        // 숨김채널에서 miniEPG를 띄우는 경우에도 해당 채널이 channel list에 포함되어 있어야 한다.
        function _reviseChannelList() {
            var tmp_idx = -1;
            var ch_list_length = ch_list !== null ? ch_list.length : 0;

            if(ch_list_length>0) {
                for (var i=0; i<ch_list_length; i++) {
                    if (Number(ch_list[i].majorChannel) >= Number(cur_channel.majorChannel)) {
                        tmp_idx = i;
                        break;
                    }
                }
                cur_ch_index = tmp_idx < 0 ? ch_list.length - 1 : tmp_idx;
                if (ch_list_length > 0) {
                    if (cur_channel.majorChannel === ch_list[cur_ch_index].majorChannel) {
                        // cur_channel is already included in the ch_list
                        cur_channel = ch_list[cur_ch_index];
                    }
                    else {
                        // to add "desc" property
                        cur_channel = epgUtil.reconstructionChList([cur_channel], true)[0];
                        // cur_channel is a skipped channel
                        // so add cur_channel into ch_list because it is not included in the ch_list
                        ch_list = ch_list.slice(0, cur_ch_index).concat(cur_channel).concat(ch_list.slice(cur_ch_index, ch_list.length));
                    }
                }
                ch_index = cur_ch_index < 1 ? 0 : cur_ch_index;
            }
        }

        function _getCurrentChannelProgram() {
            log.printDbg('_getCurrentChannelProgram() ');
            
            var programManager = KTW.oipf.adapters.ProgramSearchAdapter;
            prog_list = programManager.getPrograms(null, null, null, cur_channel);
            move_index = 0;
            pip_move_index = 0;
            if(prog_list !== undefined ) {
                prog_list = epgUtil.revisePrograms(cur_channel, prog_list, true);
                prog_index = 0;


                var now = new Date().getTime();
                if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                    now = Math.floor(now/1000);
                }

                var prog = prog_list[prog_index];
                while((prog.startTime + prog.duration) < now && prog_index < prog_list.length - 1) {
                    prog_index++;
                    prog = prog_list[prog_index];
                    if (prog == null) {
                        prog_index = -1;
                        break;
                    }
                }
            }else {
                prog_index = -1;
            }
        }


        function _initializeAuth() {
            if (fromkey === undefined || fromkey === false) {
                // 미니가이드 비밀번호 입력 초기화
                authManager.initPW();
                authManager.resetPWCheckCount();
            }
        }

        function _showCurrentChannelProgram() {
            log.printDbg('_showCurrentChannelProgram()');

            var cp = null;
            var np = null;

            /**
             * 화면 초기화
             */

            rightDiv.removeClass("channellist_show");

            /**
             * jjh1117 2017/09/04
             * leftView 화면 업데이트
             */
            if(mashupCount !== 0 && chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {

                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                }else {
                    var imageURL = null;
                    if(mashupData !== null && mashupData.length>0) {
                        imageURL = mashupData[0].img;
                    }

                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW , imageURL);
                }



            }else if(isExistReleatedContentDescriptor === true && chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid){
                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                }else {
                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW);
                }
            }else {
                leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
            }


            rightTopview.hide();
            rightBottomview.hide();

            var temp_state = null;

            move_index = 0;
            pip_move_index = 0;

            if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                temp_state = cur_channel_state;
                if(prog_list !== undefined && prog_list !== null && prog_list.length > 0 && prog_index>=0) {
                    cp = prog_list[prog_index];
                    if((prog_index+1)>=prog_list.length) {
                        np = null;
                    }else {
                        np = prog_list[prog_index+1];
                    }
                }

                if(mashupExire !== null) {
                    var today = new Date();
                    log.printDbg("_showCurrentChannelProgram() mashupExire : " + mashupExire + " , today time : " + today.getTime());
                    if (mashupExire < today.getTime()) {
                        mashupTitle = null;
                        mashupCount = 0;
                        mashupData = null;
                        mashupExire = null;

                        if(releatedContentCategoryId === null) {
                            isExistReleatedContentDescriptor = false;
                        }
                    }
                }

                var channelInfo = {
                    "channel" : chlistFocusChannel,
                    "currentProgram" : cp,
                    "nextProgram" : np,
                    "state" : temp_state,
                    "onlyProgramUpdate" : false,
                    "isPipChannel" : false,
                    "parental_rating" : parentalRating,
                    "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                    "mainvbo_channel" : cur_channel,
                    "isHiddenPromoChannel" : isHiddenPromoChannel
                };
                rightTopview.show(channelInfo);
                rightBottomview.show(channelInfo);

            }else {
                temp_state = pipChannelState;
                if(pip_prog_list !== undefined && pip_prog_list !== null && pip_prog_list.length > 0 && pip_prog_index>=0) {
                    cp = pip_prog_list[pip_prog_index];
                    if((pip_prog_index+1)>=pip_prog_list.length) {
                        np = null;
                    }else {
                        np = pip_prog_list[pip_prog_index+1];
                    }
                }

                var channelInfo = {
                    "channel" : chlistFocusChannel,
                    "currentProgram" : cp,
                    "nextProgram" : np,
                    "state" : temp_state,
                    "onlyProgramUpdate" : false,
                    "isPipChannel" : false,
                    "parental_rating" : parentalRating,
                    "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                    "mainvbo_channel" : cur_channel,
                    "isHiddenPromoChannel" : isHiddenPromoChannel
                };
                rightTopview.show(channelInfo);
                rightBottomview.show(channelInfo);
            }
        }

        function _setTimer(type) {
            log.printDbg('setTimer(' + type + ')');

            if (timer_list[type] != null) {
                _clearTimer(type);
            }

            switch (type) {
                case TIMER_TYPE.HIDE:
                    timer_list[type] = setTimeout(_hideMiniEpgMainView, displayTime === 0 ? 3000 : displayTime);
                    break;
                case TIMER_TYPE.CLOCK:
                    timer_list[type] = setTimeout(_updateTime, progressUpdateTime);
                    break;
            }
        }

        function _clearTimer(type) {
            log.printDbg('clearTimer(' + type + ')');

            if (type == null) {
                for (var i=0; i<timer_list.length; i++) {
                    _clearTimer(i);
                }
            }
            else {
                switch (type) {
                    case TIMER_TYPE.HIDE:
                        if(timer_list[type] !== undefined && timer_list[type] !== null) {
                            clearTimeout(timer_list[type]);
                        }
                        break;
                    case TIMER_TYPE.CLOCK:
                        if(timer_list[type] !== undefined && timer_list[type] !== null) {
                            clearTimeout(timer_list[type]);
                        }
                        break;
                }
                timer_list[type] = null;
            }
        }

        /**
         * MiniEPG의 현재시간 및 프로그램 진행바를 업데이트 해주는 함수.
         */
        function _updateTime() {

            /**
             * permission이 OK인 경우 자막/화면 해설 정보 단말로부터 얻는다.
             * 음성다중-->자막Only프로글매으로 진행 한 경우 업데이트 되지 않은
             * 이슈가 있어 무조건 호출 하도록 수정함.
             */
            // if (last_perm === KTW.nav.Def.CHANNEL.PERM.OK && navAdapter.isAudioChannel(cur_channel) === false) {
            //     epgUtil.progInfo.updateInfo(KTW.nav.Def.CONTROL.MAIN);
            // }
            // else {
            //     epgUtil.progInfo.init();
            // }

            if(isChannelListViewShow === true) {
                var ret = rightBottomview.updateCurrentProgram();
                if(ret === false) {
                    var np = rightBottomview.getNextProgram();
                    if(np !== null) {
                        parent.getMiniEpgMainView().updateProgramInfo(chlistFocusChannel , null , null , np.programmeID);
                    }else {
                        parent.getMiniEpgMainView().updateProgramInfo(chlistFocusChannel , null , null);
                    }
                }
            }else if(isRelatedViewShow === false){
                var isUpdate = true;
                if(KTW.CONSTANT.IS_OTS === true) {
                    var isChoiceCh = util.isSkyChoiceChannel(cur_channel);

                    if(isChoiceCh === true) {
                        var step = rightTopview.getBuyMovieChoiceStep();
                        if(step === 2 || step === 3 || step === 4) {
                            isUpdate = false;
                        }
                    }else {
                        isUpdate = true;
                    }
                }else {
                    isUpdate = true;
                }
                if(isUpdate === true) {
                    rightBottomview.updateCurrentProgram();

                    if(prog_list !== undefined && prog_list !== null && prog_list.length > 0 && prog_index>=0) {
                        var now = new Date().getTime();
                        if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                            now = Math.floor(now/1000);
                        }
                        var cp = null;
                        if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                            cp = prog_list[prog_index];
                        }else {
                            cp = pip_prog_list[pip_prog_index];
                        }
                        if(cp !== null && cp.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL && cp.startTime !== undefined && cp.startTime !== null && cp.duration !== undefined && cp.duration !== null) {
                            if ((cp.startTime + cp.duration) < now) {

                                if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                                    prog_index++;
                                    if(prog_list.length <= prog_index){
                                        prog_index--;
                                    }else {
                                        if(move_index === 0) {
                                            /**
                                             * 포커스 되어 있는 프로그램이 처음에 위치 해 있는 경우
                                             */
                                            _showCurrentChannelProgram();
                                        }else {
                                            if(move_index === 1) {
                                                /**
                                                 * 포커스 되어 있는 프로그램이 두번째에 위치 해 있는 경우
                                                 */
                                                move_index--;
                                                _showCurrentChannelProgram();
                                            }else {
                                                move_index--;
                                            }
                                        }
                                    }
                                }else {
                                    pip_prog_index++;
                                    if(pip_prog_list.length <= pip_prog_index){
                                        pip_prog_index--;
                                    }else {
                                        if(pip_move_index === 0) {
                                            /**
                                             * 포커스 되어 있는 프로그램이 처음에 위치 해 있는 경우
                                             */
                                            _showCurrentChannelProgram();
                                        }else {
                                            if(pip_move_index === 1) {
                                                /**
                                                 * 포커스 되어 있는 프로그램이 두번째에 위치 해 있는 경우
                                                 */
                                                pip_move_index--;
                                                _showCurrentChannelProgram();
                                            }else {
                                                pip_move_index--;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

           _setTimer(TIMER_TYPE.CLOCK);
        }

        /**
         * 미니가이드 닫는 동작 수행
         */
        function _hideMiniEpgMainView() {
            log.printDbg('_hideMiniEpgMainView()');

            div.css({display:"none"});

            layerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.MINI_EPG
            });
        }

        function _moveLeftRight(isLeft){
            log.printDbg('_moveLeftRight() isLeft : ' + isLeft);
            var currentProgram = null;
            var nextProgram = null;

            var onlyProgramUpdate = true;

            var isBlocked = KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel);

            if(chlistFocusChannel.ccid === cur_channel.ccid) {
                isBlocked = false;
                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                    isBlocked = true;
                }
            }

            if(isBlocked === true || (chlistFocusChannel.ccid !== cur_channel.ccid && chlistFocusChannel.desc === 2 )) {
                if(isLeft === true) {
                    return false;
                }
                return true;
            } else {
                if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                    if(prog_list !== undefined && prog_list !== null && prog_list.length > 0 && prog_index>=0) {
                        if(isLeft === true) {
                            move_index--;
                            if(move_index<0) {
                                move_index = 0;
                                return false;
                            }
                            if(move_index === 0) {
                                onlyProgramUpdate = false;
                            }
                        }else {
                            move_index++;
                            var tempindex = prog_index + move_index;
                            if (tempindex > prog_list.length - 1) {
                                move_index--;
                                return false;
                            }
                        }

                        if(prog_list.length<=(prog_index+move_index)) {
                            currentProgram = null;
                            nextProgram = null;
                        }else {
                            currentProgram = prog_list[prog_index + move_index];
                            if(prog_list.length<=(prog_index+move_index+1)) {
                                nextProgram = null;
                            }else {
                                nextProgram = prog_list[prog_index + move_index+1];
                            }
                        }

                        var temp_state = cur_channel_state;

                        if(mashupExire !== null) {
                            var today = new Date();
                            log.printDbg("_moveLeftRight() mashupExire : " + mashupExire + " , today time : " + today.getTime());
                            if (mashupExire < today.getTime()) {
                                mashupTitle = null;
                                mashupCount = 0;
                                mashupData = null;
                                mashupExire = null;

                                if(releatedContentCategoryId === null) {
                                    isExistReleatedContentDescriptor = false;
                                }
                            }
                        }

                        var channelInfo = {
                            "channel" : chlistFocusChannel,
                            "currentProgram" : currentProgram,
                            "nextProgram" : nextProgram,
                            "state" : temp_state,
                            "onlyProgramUpdate" : onlyProgramUpdate,
                            "isPipChannel" : false,
                            "parental_rating" : parentalRating,
                            "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                            "mainvbo_channel" : cur_channel,
                            "isHiddenPromoChannel" : isHiddenPromoChannel
                        };
                        if(move_index === 0) {
                            /**
                             * jjh1117 2017/09/04
                             * leftView 화면 업데이트
                             */

                            if(mashupCount !== 0 && chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                }else {
                                    var imageURL = null;
                                    if(mashupData !== null && mashupData.length>0) {
                                        imageURL = mashupData[0].img;
                                    }

                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW , imageURL);
                                }
                            }else if(isExistReleatedContentDescriptor === true && chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid){
                                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                }else {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW);
                                }
                            }else {
                                leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                            }


                        }else {
                            leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.PROGRAM_SEARCH_SHOW);
                        }

                        rightTopview.show(channelInfo);
                        rightBottomview.show(channelInfo);
                        return true;

                    }else {
                        if(isLeft === true) {
                            return false;
                        }
                        return true;
                    }
                }else {
                    if(pip_prog_list !== undefined && pip_prog_list !== null && pip_prog_list.length > 0 && pip_prog_index>=0) {
                        if(isLeft === true) {
                            pip_move_index--;
                            if(pip_move_index<0) {
                                pip_move_index = 0;
                                return false;
                            }
                            if(pip_move_index === 0) {
                                onlyProgramUpdate = false;
                            }
                        }else {
                            pip_move_index++;
                            var tempindex = pip_prog_index + pip_move_index;
                            if (tempindex > pip_prog_list.length - 1) {
                                pip_move_index--;
                                return false;
                            }
                        }


                        currentProgram = pip_prog_list[pip_prog_index + pip_move_index];

                        if(pip_prog_list.length<=(pip_prog_index+pip_move_index)) {
                            currentProgram = null;
                            nextProgram = null;
                        }else {
                            currentProgram = pip_prog_list[pip_prog_index + pip_move_index];
                            if(pip_prog_list.length<=(pip_prog_index+pip_move_index+1)) {
                                nextProgram = null;
                            }else {
                                nextProgram = pip_prog_list[pip_prog_index + pip_move_index+1];
                            }
                        }

                        var temp_state = pipChannelState;

                        var channelInfo = {
                            "channel" : chlistFocusChannel,
                            "currentProgram" : currentProgram,
                            "nextProgram" : nextProgram,
                            "state" : temp_state,
                            "onlyProgramUpdate" : onlyProgramUpdate,
                            "isPipChannel" : false,
                            "parental_rating" : parentalRating,
                            "isblocked" : KTW.oipf.AdapterHandler.navAdapter.isBlockedChannel(chlistFocusChannel),
                            "mainvbo_channel" : cur_channel,
                            "isHiddenPromoChannel" : isHiddenPromoChannel
                        };
                        if(pip_move_index === 0) {
                            /**
                             * jjh1117 2017/09/04
                             * leftView 화면 업데이트
                             */
                            if(mashupCount !== 0 && chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                }else {
                                    var imageURL = null;
                                    if(mashupData !== null && mashupData.length>0) {
                                        imageURL = mashupData[0].img;
                                    }

                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW , imageURL);
                                }
                            }else if(isExistReleatedContentDescriptor === true && chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid){

                                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                                }else {
                                    leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.RELATED_CONTENT_SHOW);
                                }

                            }else {
                                leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.CHANNEL_SEARCH_SHOW);
                            }
                        }else {
                            leftview.show(KTW.ui.view.MiniEpgLeftView.TYPE.PROGRAM_SEARCH_SHOW);
                        }

                        rightTopview.show(channelInfo);
                        rightBottomview.show(channelInfo);
                        return true;

                    }else {
                        if(isLeft === true) {
                            return false;
                        }
                        return true;
                    }
                }
            }
        }

        /***************************************************
         * MashUp 서비스
         * response : {
         *     method : String
         *     from : String
         *     title : String
         *     res : String("Y" , "N")
         *     nCont : Number
         *     data : Array(Associate Data)
         * }
         ***************************************************/
        function _callbackMashupCaptionMsg(msg) {
            log.printDbg("_callbackMashupCaptionMsg() msg : " + JSON.stringify(msg));

            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: "CaptionSettingPopup",
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    view : KTW.ui.view.popup.CaptionSettingPopup,
                    params: {
                        data : msg
                    }
                },
                visible: true
            });
        }

        function _callbackMashupMsg(msg) {
            log.printDbg("_callbackMashupMsg() msg : " + JSON.stringify(msg));

            // msg = {
            //     data : [{data:{act:"3" , cid:"CV000000000014650703", aid:"MZWG3COGSGL080012700"},type:"3" , img:"test.html" , title: "TTTTT"} ,{data:{type:3 , data:"101", param : "search_txt=W"},type:"2" , img:"test.html" , title: "TTTTT"} , {data:{act:"3" , cid:"CV000000000014650703", aid:"MZWG3COGSGL080012700"},type:"3" , img:"test.html" , title: "TTTTT"} ] ,
            //     nCont : 3 ,
            //     expire : 1490268600000,
            //     res : "Y",
            //     title : "GENIE OST가 궁금하면?",
            //     method : "mashup_reqAssociateInfoData",
            //     from : "4e30.3020"
            // };
            if(msg !== undefined && msg !== null) {
                var res = msg.res;
                var title = msg.title;
                var nCont = msg.nCont;
                var data = msg.data;
                var expire = msg.expire;

                if(res !== undefined && res !== null &&
                    nCont !== undefined && nCont !== null &&
                    expire !== undefined && expire !== null &&
                    data !== undefined && data !== null && data.length>0) {
                    if(res === "Y" && nCont>0 && expire !== 0) {
                        if(isExistReleatedContentDescriptor === false) {
                            isExistReleatedContentDescriptor = true;
                        }
                        if(title !== undefined && title !== null && title !== "") {
                            mashupTitle = title;
                        }
                        mashupCount = nCont;
                        mashupData = data;
                        mashupExire = expire;

                        var toDay = new Date();
                        log.printDbg("_callbackMashupMsg() expire : " + expire + " , toDay.getTime() : " + toDay.getTime());
                        if(parent.isShowing() === true) {
                            log.printDbg("_callbackMashupMsg() isChannelListViewShow : " + isChannelListViewShow + " , isRelatedViewShow : " + isRelatedViewShow);

                            var imageURL = null;
                            if(mashupData !== null && mashupData.length>0) {
                                imageURL = mashupData[0].img;
                            }

                            if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                                leftview.updateMashupImg(imageURL);
                            }

                        }
                    }
                }
            }
        }

        function _callbackFavChannel() {
            log.printDbg("_callbackFavChannel() ");
            rightTopview.updateCHNameCHNum(true);
        }

        function _callbackProgramDetailPopup(btnIndex) {
            log.printDbg("_callbackProgramDetailPopup() btnIndex : " + btnIndex);
            if(btnIndex === 1) {
                /**
                 * 시청 예약
                 */
                var currentProgram = null;
                if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                    currentProgram = prog_list[prog_index + move_index];
                    KTW.managers.service.ReservationManager.add(currentProgram, true, function(result) {

                        if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                            || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                            _hideOTSProgramDetailView();
                            _hideMiniEpgMainView(true);
                        }else {
                            _hideOTSProgramDetailView();
                        }
                        return;
                    }, false);
                }else {
                    if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                        || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                        _hideOTSProgramDetailView();
                        _hideMiniEpgMainView(true);
                    }else {
                        _hideOTSProgramDetailView();
                    }
                }
            }else {
                if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                    || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                    _hideOTSProgramDetailView();
                    _hideMiniEpgMainView(true);
                }else {
                    _hideOTSProgramDetailView();
                }
            }

        }

        function _callbackFuncRelatedLeftMenu(focusIndex , genreOptionIndex) {
            log.printDbg("_callbackFuncRelatedMenu() focusIndex : " + focusIndex);
            epgRelatedMenuManager.deactivateRelatedMenu();

            if(focusIndex ===  epgRelatedMenuManager.MENU_INFO.LEFT_MENU.FAVORITED) {
                if(isHiddenPromoChannel === true) {
                    if(defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
                        epgRelatedMenuManager.favoriteChannelOnOff(defaultPromoChannelList[0] , _callbackFavChannel);
                    }
                }else {
                    epgRelatedMenuManager.favoriteChannelOnOff(cur_channel , _callbackFavChannel);
                }
            }else if(focusIndex ===  epgRelatedMenuManager.MENU_INFO.LEFT_MENU.DETAIL) {
                if(prog_list !== null && prog_list.length>0) {
                    var currentProgram = prog_list[prog_index + move_index];
                    if(currentProgram !== undefined && currentProgram !== null
                        && currentProgram.name !== epgUtil.DATA.PROGRAM_TITLE.NULL
                        && currentProgram.name !== epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                        _showOTSProgramDetailView(cur_channel ,currentProgram,  move_index === 0 ? false : true ,  cur_channel_state , _callbackProgramDetailPopup);
                    }
                }

            }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.LEFT_MENU.TWO_CH) {
                /**
                 * activateTwoChannel 내부에서 UHD 채널 체크 함.
                 */
                epgRelatedMenuManager.activateTwoChannel(_callbackMulti , _callbackSwap);
                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.PIP,
                    catId: "",
                    contsId: "",
                    locator: "",
                    reqPathCd: ""
                });
            }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.LEFT_MENU.FOUR_CH) {
                var locator = epgRelatedMenuManager.activateFourChannel();
                //통계로그 추가
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.JUMP_TO,
                    act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                    jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                    catId: "",
                    contsId: "",
                    locator: locator,
                    reqPathCd: ""
                });
            }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.LEFT_MENU.HIT_CH) {

                epgRelatedMenuManager.activateRealTimeChannel();
            }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.LEFT_MENU.GENRE_OPTION) {
                log.printDbg("_callbackFuncRelatedMenu() focusIndex : " + focusIndex + " , genreOptionIndex : " + genreOptionIndex);
            }

        }

        function _callbackFuncRelatedRightMenu(focusMenu , focusIndex , objectList , focusdropdownIndex) {
            var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;
            var channelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);

            log.printDbg("_callbackFuncRelatedRightMenu() focusMenu : " + focusMenu + " , focusIndex : " + focusIndex + " , focusdropdownIndex : " + focusdropdownIndex);
            epgRelatedMenuManager.deactivateRelatedMenu();
            if(focusMenu === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.TOP_MENU) {
                if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.TOTAL_CHANNEL) {
                    epgRelatedMenuManager.activateFullEpg();
                    //통계로그 추가
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.JUMP_TO,
                        act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                        jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                        catId: "",
                        contsId: "",
                        locator: "",
                        reqPathCd: ""
                    });

                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.F_CHANNEL) {
                    epgRelatedMenuManager.activateFullFavoriteChannelEpg();
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.JUMP_TO,
                        act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                        jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                        catId: "",
                        contsId: "",
                        locator: "",
                        reqPathCd: ""
                    });
                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.KIDS_CHANNEL
                    || focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.AUDIO_CHANNEL) {
                    if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel()) === true) {
                        epgRelatedMenuManager.activateFullAudioChannelEpg();
                    } else {
                        epgRelatedMenuManager.activateKidsModeFullEpg();
                    }
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.JUMP_TO,
                        act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                        jumpType: KTW.data.EntryLog.JUMP.TYPE.EPG,
                        catId: "",
                        contsId: "",
                        locator: "",
                        reqPathCd: ""
                    });
                } else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.TOP_MENU.KIDS_PROGRAM_DETAIL) {
                    if(chlistFocusChannel !== null && chlistFocusChannel.ccid === cur_channel.ccid) {
                        if(prog_list !== null && prog_list.length>0) {
                            var currentProgram = prog_list[prog_index + move_index];
                            if(currentProgram !== undefined && currentProgram !== null
                                && currentProgram.name !== epgUtil.DATA.PROGRAM_TITLE.NULL
                                && currentProgram.name !== epgUtil.DATA.PROGRAM_TITLE.UPDATE) {

                                _showOTSProgramDetailView(cur_channel ,currentProgram,  move_index === 0 ? false : true ,  cur_channel_state , _callbackProgramDetailPopup);

                            }
                        }
                    }
                }
            } else if(focusMenu === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU) {
                if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.CAPTION_MENU) {

                    if(KTW.CONSTANT.IS_BIZ === true) {
                        var mashup_caption_msg = {
                            from: KTW.CONSTANT.APP_ID.HOME,
                            to: KTW.CONSTANT.APP_ID.MASHUP,
                            method: "mashup_getCCLanguageList",
                        };

                        var isTimeout = false;
                        KTW.ui.LayerManager.startLoading({
                            preventKey: true,
                            timeout: 3 * 1000,
                            cbTimeout: function () {
                                if (!isTimeout) {
                                    isTimeout = true;
                                    _callbackMashupCaptionMsg(null);
                                }
                            }
                        });

                        KTW.managers.MessageManager.sendMessage(mashup_caption_msg, function (res_msg) {
                            if (!isTimeout) {
                                isTimeout = true;
                                KTW.ui.LayerManager.stopLoading();
                                _callbackMashupCaptionMsg(res_msg);
                            }
                        });
                    }else {
                        KTW.ui.LayerManager.activateLayer({
                            obj: {
                                id: "CaptionSettingPopup",
                                type: KTW.ui.Layer.TYPE.POPUP,
                                priority: KTW.ui.Layer.PRIORITY.POPUP,
                                view : KTW.ui.view.popup.CaptionSettingPopup,
                                params: {
                                    data : null
                                }
                            },
                            visible: true
                        });
                    }
                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.VOICE_MENU) {
                    if(objectList !== undefined && objectList !== null && focusdropdownIndex !== undefined && focusdropdownIndex !== null) {
                        var comp_a = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO);
                        channelControl.selectComponent(comp_a[objectList[focusdropdownIndex].lang_index]);
                    }
                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU) {
                    if(KTW.managers.device.DeviceManager.bluetoothDevice.isAudioDeviceConnected() === true) {
                        if(objectList !== undefined && objectList !== null && focusdropdownIndex !== undefined && focusdropdownIndex !== null) {
                            var langIndex = objectList[focusdropdownIndex].lang_index;
                            var comp_a = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO);
                            var bt_device = bluetoothDevice.getBtAudioDevice();
                            var cur_comp_a = channelControl.getCurrentActiveComponents(MediaExtension.COMPONENT_TYPE_AUDIO, bt_device)[0];
                            if(langIndex <0) {
                                if(langIndex === -10) {
                                    /**
                                     * 사운드 ON
                                     */
                                    if (cur_comp_a == null) {
                                        channelControl.selectComponent(comp_a[0], bt_device);
                                    }
                                }else if(langIndex === - 20) {
                                    /**
                                     * 사운드 OFF
                                     */
                                    if (cur_comp_a != null) {
                                        channelControl.unselectComponent(cur_comp_a, bt_device);
                                    }
                                }
                            }else {
                                log.printDbg('##### BLE selectComponent [' + comp_a[langIndex].pid + ']' + comp_a[langIndex].language);
                                channelControl.selectComponent(comp_a[langIndex], bt_device);
                            }

                        }
                    }
                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU) {
                    if(KTW.managers.device.DeviceManager.bluetoothDevice.isAudioDeviceConnected() === true) {
                        if(objectList !== undefined && objectList !== null && focusdropdownIndex !== undefined && focusdropdownIndex !== null) {
                            var langIndex = objectList[focusdropdownIndex].lang_index;
                            if(langIndex <0) {
                                if(langIndex === -10) {
                                    /**
                                     * 같이 듣기
                                     */
                                    var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;
                                    if(bluetoothDevice !== null) {
                                        bluetoothDevice.setListenAloneMode(false);
                                    }
                                }else if(langIndex === -20) {
                                    /**
                                     * 혼자 듣기
                                     */
                                    var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;
                                    if(bluetoothDevice !== null) {
                                        bluetoothDevice.setListenAloneMode(true);
                                    }
                                }
                            }
                        }
                    }
                }
            } else if(focusMenu === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU) {
                if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.BOOKMARK) {
                    epgRelatedMenuManager.activateBookmarkApp();
                    //TODO 통계로그 추가 .. child app 이 실행되면서 postmessage 통계 로그가 잡히는데 여기서 필요한건가 ?
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.JUMP_TO,
                        act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                        jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                        catId: "",
                        contsId: "",
                        locator: "",
                        reqPathCd: ""
                    });
                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.HOME_WIDGET) {
                    epgRelatedMenuManager.activateHomeWidget();
                    //TODO 통계로그 추가 .. child app 이 실행되면서 postmessage 통계 로그가 잡히는데 여기서 필요한건가 ?
                    KTW.managers.UserLogManager.collect({
                        type: KTW.data.UserLog.TYPE.JUMP_TO,
                        act: KTW.data.EntryLog.JUMP.CODE.CONTEXT,
                        jumpType: KTW.data.EntryLog.JUMP.TYPE.DATA,
                        catId: "",
                        contsId: "",
                        locator: "",
                        reqPathCd: ""
                    });

                }else if(focusIndex === epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.BOTTOM_MENU.KIDS_MODE_OFF) {
                    /**
                     * jjh1117 2017/04/17
                     * TODO 이 부분 추후 채워 넣어야 함.
                     */
                    KTW.ui.LayerManager.activateLayer({
                        obj: {
                            id: KTW.ui.Layer.ID.SETTING_AUTH_CHECK_PASSWORD_POPUP,
                            type: KTW.ui.Layer.TYPE.POPUP,
                            priority: KTW.ui.Layer.PRIORITY.POPUP,
                            linkage: false,
                            params: {
                                callback: _callbackFuncAuthCheck,
                                is_kids_mode_off : true
                            }
                        },
                        new: true,
                        visible: true
                    });


                }
            }
        }


        function _callbackFuncAuthCheck(success) {
            log.printDbg("_callbackFuncAuthCheck()");
            KTW.ui.LayerManager.deactivateLayer({
                id: KTW.ui.Layer.ID.SETTING_AUTH_CHECK_PASSWORD_POPUP
            });
            if(success !== undefined && success !== null && success === true) {
                KTW.managers.service.KidsModeManager.changeMode({
                    kidsMode: KTW.oipf.Def.KIDS_MODE.OFF
                });
            }
        }

        /**
         * 연관메뉴 노출
         */
        function _showRelatedMenu() {
            log.printDbg("_showRelatedMenu()");
            var isShowFavoritedChannel = true;
            var isShowProgramDetail = true;
            var isShowTwoChannel = true;
            var isShowFourChannel = true;
            var isShowHitChannel = true;
            var isShowGenreOption = false;

            var isShowCaption = true;
            var isDimmedCaption = false;
            var isShowVoice = true;
            var isDimmedVoice = false;
            var isShowBlueToothVoice = true;
            var isDimmedBlueToothVoice = false;
            var isShowBlueToothListening = true;
            var isDimmedBlueToothListening = false;

            var voicelanglist = [];
            var voicelangfocusIndex = -1;

            var bluetoothvoicelist = [];
            var bluetoothvoiceIndex = -1;

            var bluetoothlisteninglist = [];
            var bluetoothlisteningIndex = -1;

            var curChannel = navAdapter.getCurrentChannel();

            if(isAudioChannel === true) {
                isShowFavoritedChannel = false;
                isShowProgramDetail = false;
            }else {
                if(curChannel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S
                    && curChannel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) {
                    isShowProgramDetail = false;
                }

                if(isShowProgramDetail === true) {
                    var currentProgram = prog_list[prog_index + move_index];
                    if (currentProgram) {
                        if (currentProgram.name === epgUtil.DATA.PROGRAM_TITLE.NULL
                            || currentProgram.name === epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                            isShowProgramDetail = false;
                        }
                    } else {
                        isShowProgramDetail = false;
                    }
                }
            }


            if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED  || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                isShowTwoChannel = false;
            }else {
                if((curChannel.isUHD === true && KTW.CONSTANT.IS_OTS === true) || curChannel.desc === 2 || KTW.CONSTANT.IS_DCS === true) {
                    isShowTwoChannel = false;
                }
            }

            if (rightTopview.isBuyMovieChoice() === true &&
                cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                isShowTwoChannel = false;
            }

            /**
             * Left Menu 구성
             */
            var tempChannel = cur_channel;
            // /**
            //  * 맞춤프로모채널 인 경우 디폴트 프로모채널 정보를 전달함.
            //  */
            // if(isHiddenPromoChannel === true) {
            //     if(defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
            //         tempChannel = defaultPromoChannelList[0];
            //     }
            // }
            var leftmenuinfo =   epgRelatedMenuManager.makeLeftMenuInfo(tempChannel , _callbackFuncRelatedLeftMenu
                , isShowFavoritedChannel , isShowProgramDetail , isShowTwoChannel , isShowFourChannel , isShowHitChannel , isShowGenreOption
                , epgRelatedMenuManager.MENU_INFO.LEFT_MENU.OPTION_TYPE.SORT_START_TIME);


            /**
             * Right Menu 중에 상단 전체채널,선호채널 메뉴 구성
             */
            var guideMenu = KTW.managers.data.MenuDataManager.searchMenu({
                menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                cbCondition: function (menu) {
                    if (menu.id == KTW.managers.data.MenuDataManager.MENU_ID.CHANNEL_GUIDE) {
                        return true;
                    }
                }
            })[0];

            var channelgenrelist = [];
            var obj = epgRelatedMenuManager.searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST);
            if(obj !== null) {
                channelgenrelist[channelgenrelist.length] = obj;
            }

            obj = epgRelatedMenuManager.searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.MY_FAVORITED_CHANNEL);
            if(obj !== null) {
                channelgenrelist[channelgenrelist.length] = obj;
            }

            //2017.05.04 sw.nam - 오디오 채널일 경우 오디오 편성표 연관메뉴 추가
            if(isAudioChannel === true) {
                obj = epgRelatedMenuManager.searchMenu(guideMenu,KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL);
                if(obj !== null) {
                    channelgenrelist[channelgenrelist.length] = obj;
                }
            }

            var miniepgtopmenu = epgRelatedMenuManager.makeMiniEpgTopMenuInfo(true, channelgenrelist);

            /**
             * Right Menu 중에 하단 북마크,홈위젯 메뉴 구성
             */
            var appList = [{name:"북마크 보관함",value:epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU.BOOKMARK}
                ,{name:"홈 위젯",value:epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.BOTTOM_MENU.HOME_WIDGET}];
            var miniepgbttommenu = epgRelatedMenuManager.makeMiniEpgBottomMenuInfo(true , appList);


            /**
             * Right Menu 중에 중간 메뉴 구성
             */

            /**
             * 메뉴 노출 여부 체크
             */
            if(cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                || cur_channel_state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED || isAudioChannel === true) {
                isShowCaption = false;
                isShowVoice = false;
                isShowBlueToothVoice = false;
                if(isAudioChannel === true) {
                    var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;

                    if (KTW.CONSTANT.IS_UHD === true && bluetoothDevice.isAudioDeviceConnected() === true) {
                        isDimmedBlueToothListening = false;
                        bluetoothlisteningIndex = 0;
                        var tempbluetoothlistening1 = {
                            type: epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU,
                            langname: "같이 듣기",
                            lang_index: -10
                        };
                        var tempbluetoothlistening2 = {
                            type: epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU,
                            langname: "혼자 듣기",
                            lang_index: -20
                        };


                        if (bluetoothDevice.isListenAloneMode() === true) {
                            bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening2;
                            bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening1;

                        } else {
                            bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening1;
                            bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening2;
                        }
                        isShowBlueToothListening = true;
                    }else {
                        isShowBlueToothListening = false;
                    }

                }else {
                    isShowBlueToothListening = false;
                }
            }else {
                /**
                 * 자막 존재 여부 체크
                 */
                if (KTW.utils.epgUtil.progInfo.hasSubtitle() !== true) {
                    isShowCaption = false;
                    isDimmedCaption = false;
                }else {
                    isDimmedCaption = false;
                }

                /**
                 * 음성 변경 여부 체크
                 */
                voicelangfocusIndex = -1;
                var channelControl = navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);

                var desc_onoff = Number(KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VISUAL_IMPAIRED)) === 1;
                var cur_comp_a = channelControl.getCurrentActiveComponents(MediaExtension.COMPONENT_TYPE_AUDIO)[0];
                var comp_a = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO);
                var isAudioDescription = false;
                if(desc_onoff === true) {
                    for (var i=0; i<comp_a.length; i++) {
                        if (comp_a[i].audioDescription === true) {
                            isAudioDescription = true;
                            break;
                        }
                    }
                }
                //if (KTW.utils.epgUtil.progInfo.isMultiAudio() === true || isAudioDescription === true) {
                if (KTW.utils.epgUtil.progInfo.isMultiAudio() === true) {
                    isDimmedVoice = false;

                    for (var i=0; i<comp_a.length; i++) {
                        var templang = {
                            type : epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.VOICE_MENU,
                            langname : undefined,
                            lang_index : -1
                        };
                        if (comp_a[i].audioDescription === true) {
                            if (desc_onoff === true) {
                                templang.langname = "화면 해설 방송";
                                templang.lang_index = i;
                                voicelanglist[voicelanglist.length] = templang;
                            }
                        }
                    }

                    for (var i=0; i<comp_a.length; i++) {

                        var templang = {
                            type : epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.VOICE_MENU,
                            langname : undefined,
                            lang_index : -1
                        };
                        if (comp_a[i].audioDescription === true && desc_onoff === true) {
                            if (cur_comp_a.pid === comp_a[i].pid) {
                                voicelangfocusIndex = 0;
                            }
                        }
                        else {
                            templang.langname = epgUtil.progInfo.getAudioStr(comp_a[i].language);
                            templang.lang_index = i;
                            voicelanglist[voicelanglist.length] = templang;

                            if (cur_comp_a.pid === comp_a[i].pid) {
                                voicelangfocusIndex = voicelanglist.length - 1;
                            }
                        }

                    }
                }else {
                    isShowVoice = false;
                }

                var bluetoothDevice = KTW.managers.device.DeviceManager.bluetoothDevice;

                if (KTW.CONSTANT.IS_UHD === true && bluetoothDevice.isAudioDeviceConnected() === true) {
                    isDimmedBlueToothVoice = false;
                    bluetoothvoiceIndex = 0;

                    isDimmedBlueToothListening = false;
                    bluetoothlisteningIndex = 0;


                    /**
                     * 혼자듣기 설정 된 경우 음성변경 항목 사라짐.
                     */
                    if(bluetoothDevice.isListenAloneMode() === true) {
                        isShowVoice = false;
                    }

                    var bt_device = bluetoothDevice.getBtAudioDevice();
                    var cur_comp_a = null;
                    var cur_comp_a_list = channelControl.getCurrentActiveComponents(MediaExtension.COMPONENT_TYPE_AUDIO, bt_device);
                    if(cur_comp_a_list !== undefined && cur_comp_a_list !== null) {
                        if(cur_comp_a_list.length>0) {
                            cur_comp_a = cur_comp_a_list[0];
                        }
                    }


                    var comp_a = channelControl.getComponents(MediaExtension.COMPONENT_TYPE_AUDIO);

                    var tempbluetoothlistening1 = {
                        type : epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU,
                        langname : "같이 듣기",
                        lang_index : -10
                    };
                    var tempbluetoothlistening2 = {
                        type : epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.BLUE_TOOTH_VOICE_LE_MENU,
                        langname : "혼자 듣기",
                        lang_index : -20
                    };


                    if(bluetoothDevice.isListenAloneMode() === true) {
                        bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening2;
                        bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening1;

                    }else {
                        bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening1;
                        bluetoothlisteninglist[bluetoothlisteninglist.length] = tempbluetoothlistening2;
                    }

                    // 음성다중/화면해설인 경우(audio stream이 여러개) 언어 선택하도록 메뉴 구성
                    if (comp_a != null && comp_a.length > 1) {
                        var desc_onoff = Number(KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.VISUAL_IMPAIRED)) === 1;
                        if (cur_comp_a == null) {
                            cur_comp_a = comp_a[0];
                        }

                        for (var i=0; i<comp_a.length; i++) {
                            var templang = {
                                type : epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU,
                                langname : undefined,
                                lang_index : -1
                            };
                            if (comp_a[i].audioDescription === true) {
                                if (desc_onoff === true) {
                                    templang.langname = "화면 해설 방송";
                                    templang.lang_index = i;
                                    bluetoothvoicelist[bluetoothvoicelist.length] = templang;
                                }
                            }
                        }


                        /**
                         * 나머지 언어를 순서대로 입력
                         */
                        for (var i=0; i<comp_a.length; i++) {

                            var templang = {
                                type : epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.MINI_EPG.MIDDLE_MENU.BLUE_TOOTH_VOICE_MENU,
                                langname : undefined,
                                lang_index : -1
                            };
                            if (comp_a[i].audioDescription === true && desc_onoff === true) {
                                if (cur_comp_a.pid === comp_a[i].pid) {
                                    bluetoothvoiceIndex = 0;
                                }
                            }
                            else {
                                templang.langname = epgUtil.progInfo.getAudioStr(comp_a[i].language);
                                templang.lang_index = i;
                                bluetoothvoicelist[bluetoothvoicelist.length] = templang;

                                if (cur_comp_a.pid === comp_a[i].pid) {
                                    bluetoothvoiceIndex = bluetoothvoicelist.length - 1;
                                }
                            }

                        }

                        /**
                         * 화면해설 옵션 꺼져있을 때 화면해설 audio 제외하고 audio stream이 하나인 경우
                         * 음성다중/화면해설이 아닌 경우와 동일하게 구성
                         */
                        if(bluetoothvoicelist.length<2) {
                            isShowBlueToothVoice = false;
                            isDimmedBlueToothVoice = false;
                            bluetoothvoicelist = [];
                            bluetoothvoiceIndex = -1;
                        }

                    }else {
                        isShowBlueToothVoice = false;
                        isDimmedBlueToothVoice = false;
                        bluetoothvoicelist = [];
                        bluetoothvoiceIndex = -1;
                    }
                }else {
                    isShowBlueToothVoice = false;
                    isShowBlueToothListening = false;
                }
            }

            var captionmenu = epgRelatedMenuManager.makeCaptionMenuInfo(isShowCaption , isDimmedCaption);
            var voicemenu = epgRelatedMenuManager.makeVoiceMenuInfo(isShowVoice , isDimmedVoice , voicelanglist , voicelangfocusIndex);
            var bluetoothlang =  epgRelatedMenuManager.makeBlueToothVoiceMenuInfo(isShowBlueToothVoice , isDimmedBlueToothVoice , bluetoothvoicelist , bluetoothvoiceIndex);
            var bluetoothlistening =  epgRelatedMenuManager.makeBlueToothListeningMenuInfo(isShowBlueToothListening , isDimmedBlueToothListening , bluetoothlisteninglist , bluetoothlisteningIndex);

            var miniepgmiddlemenu = epgRelatedMenuManager.makeMiniEpgMiddleMenuInfo(captionmenu , voicemenu , bluetoothlang , bluetoothlistening);

            var miniepgrightmenuinfo = epgRelatedMenuManager.makeMiniEpgMenuInfo(miniepgtopmenu , miniepgmiddlemenu ,miniepgbttommenu ,_callbackFuncRelatedRightMenu);

            var rightmenuinfo = epgRelatedMenuManager.makeRightMenuInfo( epgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER.MINI_EPG, {},miniepgrightmenuinfo);

            var menuinfo = epgRelatedMenuManager.makeMenuInfo(leftmenuinfo ,rightmenuinfo);

            epgRelatedMenuManager.activateRelatedMenu(menuinfo);
        }


        /**
         * 멀티화면이 종료되면 호출되는 콜백 함수
         */
        function _callbackMulti() {
            log.printDbg("_callbackMulti()");
            // blocked 상태를 제외하고 iframe이 떠있는 경우 miniepg 호출
            // if ((KTW.CONSTANT.IS_OTS === false && isAudioChannel === true) ||
            //     (KTW.CONSTANT.IS_OTS === true && util.isSkyChoiceChannel(cur_channel) && epgUtil.getSkyChoiceData(cur_channel.majorChannel).result === false)) {
            //     log.printDbg("if()");
            //     setTimeout(function(){
            //
            //         var clear_stack = true;
            //
            //         layerManager.activateLayer({
            //             obj: {
            //                 id: KTW.ui.Layer.ID.MINI_EPG,
            //                 type: KTW.ui.Layer.TYPE.TRIGGER,
            //                 priority: KTW.ui.Layer.PRIORITY.NORMAL,
            //                 linkage: false,
            //                 params: {
            //                     fromKey:true
            //                 }
            //             },
            //             clear_normal: clear_stack,
            //             visible: true,
            //             cbActivate: function() {
            //
            //             }
            //         });
            //
            //     }, 100);
            // }else {
            //     log.printDbg("else()");
            // }
        }
        function _callbackSwap() {
            log.printDbg("_callbackSwap()");
            return true;
        }


        function _sendMessageToObserver(method) {
            var message = {
                "method": method,
                "from": KTW.CONSTANT.APP_ID.HOME,
                "to" : KTW.CONSTANT.APP_ID.OBSERVER };
            log.printDbg("_sendMessageToObserver() message : " + JSON.stringify(message));
            KTW.managers.MessageManager.sendMessage(message);
        }

        function _checkHideTimer() {
            var isSetTimer = true;
            if(KTW.CONSTANT.IS_OTS === true) {
                var isChoiceCh = util.isSkyChoiceChannel(cur_channel);
                if(isChoiceCh === true) {
                    var step = rightTopview.getBuyMovieChoiceStep();
                    if(step === 2 || step === 3 || step === 4) {
                        isSetTimer = false;
                    }
                }else {
                    isSetTimer = true;
                }
            }else {
                isSetTimer = true;
            }

            if(cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NORMAL
                && cur_channel_state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW) {
                isSetTimer = false;
            }

            if (isSetTimer === true && isRelatedViewShow === false && isChannelListViewShow === false) {
                _setTimer(TIMER_TYPE.HIDE);
            }else {
                _clearTimer(TIMER_TYPE.HIDE);
            }
        }

        function _isHiddenPromoChannel(ch) {
            log.printDbg("isHiddenPromoChannel()");
            isHiddenPromoChannel =  KTW.oipf.AdapterHandler.navAdapter.isHiddenPromoChannel(ch);

            log.printDbg("isHiddenPromoChannel() isHiddenPromoChannel : " + isHiddenPromoChannel) ;
            if(isHiddenPromoChannel === true) {
                _getDefaultPromoChannel();


                if (ch_list === null || ch_list.length < 1) {
                    _callbackChListUpdate('called by miniEPG.showView() because ch_list is empty!');
                }

                var tmp_idx = -1;
                var ch_list_length = ch_list !== null ? ch_list.length : 0;

                if(ch_list_length>0 && defaultPromoChannelList !== null && defaultPromoChannelList.length>0) {
                    for (var i = 0; i < ch_list_length; i++) {
                        if (Number(ch_list[i].majorChannel) >= Number(defaultPromoChannelList[0].majorChannel)) {
                            tmp_idx = i;
                            break;
                        }
                    }

                    if(tmp_idx>=0) {
                        var cur_channel_list = epgUtil.reconstructionChList([ch], true);
                        var temp_ch_list1 = null;
                        var temp_ch_list2 = null;

                        if(tmp_idx>0) {
                            temp_ch_list1 = ch_list.slice(0, tmp_idx);
                        }
                        temp_ch_list2 = ch_list.slice(tmp_idx+1, ch_list.length);


                        if(temp_ch_list1 !== null) {
                            ch_list = temp_ch_list1.concat(cur_channel_list , temp_ch_list2);
                        }else {
                            ch_list = cur_channel_list.concat(temp_ch_list2);
                        }

                        cur_ch_index = tmp_idx;
                    }
                }

                log.printDbg("isHiddenPromoChannel() ch_list.length : " + ch_list.length) ;
            }

            return isHiddenPromoChannel;
        }


        function _getDefaultPromoChannel() {
            log.printDbg("_getDefaultPromoChannel()");
            var defaultPromoChannel = null;
            if(KTW.CONSTANT.IS_OTS === true &&
                (cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || cur_channel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2)) {
                defaultPromoChannel = KTW.oipf.AdapterHandler.navAdapter.getSkyPlusChannel();
            }else {
                defaultPromoChannel = KTW.oipf.AdapterHandler.navAdapter.getPromoChannel();
            }

            defaultPromoChannelList = epgUtil.reconstructionChList([defaultPromoChannel], true);

            log.printDbg("_getDefaultPromoChannel() defaultPromoChannel : " + JSON.stringify(defaultPromoChannel));
        }
    }
})();