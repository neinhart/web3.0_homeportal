/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MiniEpgRightTopProgramView</code>
 *
 * 채널 미니 가이드 우측 상단 프로그램정보 및 채널 정보 화면 뷰
 * @author jjh1117
 * @since 2016-10-07
 */

"use strict";

(function() {
    KTW.ui.view.MiniEpgRightTopProgramView = function(div) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var parentDiv = div;

        var channelInfoDiv = null;
        var imgBlockedIcon = null;
        var textChannelNumber = null;
        var textChannelName = null;
        var imgChannelResolutionIcon = null;

        var currentProgramArea = null;

        var imgCurrentProgramReservationIcon = null;
        var textCurrentProgramName_1 = null;
        var textCurrentProgramName_2 = null;
        var imgCurrentProgramAge = null;
        var btnJoinDiv = null;
        var btnJoinText = null;

        var nextProgramArea = null;
        var textNextProgramName = null;
        var imgArwRight = null;
        var imgNextProgramReservationIcon = null;

        var passwordInputBoxDiv = null;
        var passwordInputBox = null;
        var textInputBox = null;

        var movieChoicePriceUnit = null;
        var movieChoicePrice = null;
        var btnOk = null;
        var btnCancel = null;

        var ch = null;
        var cp = null;
        var np = null;
        var state = null;
        var isProgramUpdate = null;
        var isPipChannel = false;


        var parentView = null;

        var restoreTimer = null;

        var parentalRating = 0;
        var isBlocked = false;
        var mainVboChannel = null;
        var isHiddenPromoChannel = null;
        /**
         * blocked 상태에서 비밀번호 입력 관련
         */
        var max_passwd_length = 4;
        var pw_fail_count = 0;
        
        var inputPinType = 0; // 1 : 성인인증 , 2 : 구매 핀

        /**
         * OTS 미가입 채널에서 CPMS 연동 시
         */
        var cpmsInfo = null;
        var otsPurchaseStep = 0;
        var otsPurchaseBtnIndex = -1;

        var MiniEpgTopProgamViewLayerId = "MiniepgTopProgamViewErrorPopup";


        var programFontSize = 51;

        this.create = function () {
            log.printDbg("create()");
            _createElement();
            _hideAll(false);
        };

        this.setParent  = function (parentview) {
            parentView = parentview;
        };

        /**
         *  var channelInfo = {
         *        channel : cur_channel,
         *        currentProgram : currentProgram,
         *        nextProgram : null,
         *        state : cur_channel_state,
         *        onlyProgramUpdate:false
         *        isPipChannel : true/false
         *        is_exist_releated_content_descriptor : true/false
         *        releatedContentCategoryId : ""
         *        mash_title : ""
         *      };
         */
        this.show = function (channelInfo) {
            log.printDbg("show() channelInfo : " + JSON.stringify(channelInfo));

            if(restoreTimer !== null) {
                clearTimeout(restoreTimer);
                restoreTimer = null;
            }

            currentProgramArea.css("-webkit-animation-play-state" , "paused");
            nextProgramArea.css("-webkit-animation-play-state" , "paused");

            if(channelInfo === undefined || channelInfo !== null) {

                otsPurchaseStep = 0;
                otsPurchaseBtnIndex = -1;

                if(channelInfo !== undefined) {
                    pw_fail_count = 0;

                    ch = channelInfo.channel;
                    cp = channelInfo.currentProgram;
                    np = channelInfo.nextProgram;
                    state = channelInfo.state;
                    isProgramUpdate = channelInfo.onlyProgramUpdate;
                    isPipChannel = (channelInfo.isPipChannel === undefined ||
                        channelInfo.isPipChannel === null) ? false : channelInfo.isPipChannel;

                    parentalRating = channelInfo.parental_rating;
                    isBlocked = channelInfo.isblocked;
                    mainVboChannel = channelInfo.mainvbo_channel;
                    isHiddenPromoChannel = channelInfo.isHiddenPromoChannel;
                }

                log.printDbg("show() state : " + state + " , isProgramUpdate : " + isProgramUpdate);

                /**
                 * UI 초기화
                 */
                _hideAll(isProgramUpdate);
                _updateInputBox("");

                /**
                 * 채널명/채널번호 업데이트
                 */
                _updateCHNameCHNum();

                /**
                 * OTS Movie Choice 구매 여부 체크
                 */
                if(isProgramUpdate !== undefined && isProgramUpdate === false) {
                    if (KTW.CONSTANT.IS_OTS === true) {
                        var now = new Date().getTime();
                        now = Math.floor(now/1000);

                        var isChoiceCh = util.isSkyChoiceChannel(ch);
                        if (isChoiceCh === true && mainVboChannel.ccid === ch.ccid && cp !== null && cp.startTime < now) {
                            // 구매한 채널
                            if (KTW.utils.epgUtil.getSkyChoiceData(ch.majorChannel).result === true) {
                                BuySkyCh.init();
                            }
                            else {
                                // 저장된 구매내역은 없지만 구매한 채널인지 여부 확인
                                // 우선은 구매 안된 컨텐츠로 처리하고,
                                // 구매목록 조회에 대한 callback 받아서 구매한 컨텐츠라면 다시 update
                                KTW.managers.http.oppvManager.searchHistory(_callbackSearchHistory, KTW.SMARTCARD_ID);
                                BuySkyCh.init(true);
                            }
                        }
                        else {
                            BuySkyCh.init();
                        }
                    }
                }

                if(isPipChannel === true) {
                    /**
                     * 인접채널 리스트가 노출 된 상태에서 포커스 된 인접채널의 정보 노출
                     * STEP#1 시청제한 채널인지 체크
                     * STEP#2 현재,다음프로그램의 시청연령 제한인지 체크
                     * STEP3 현재, 다음프로그램 화면 노출
                     */

                    /**
                     * [dj.son] [WEBIIIHOME-3569] 인접 채널 리스트로 갔을때 비밀번호 초기화
                     */
                    KTW.managers.auth.AuthManager.initPW();

                    if(state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                        isBlocked = false;
                        if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                            isBlocked = true;
                        }
                    }

                    textCurrentProgramName_1.css('font-size', 51);
                    textCurrentProgramName_1.css('letter-spacing', -2.55);

                    programFontSize = 51;

                    if(isBlocked === true) {
                        textCurrentProgramName_1.text('시청 제한 채널입니다');
                        textCurrentProgramName_2.text("");
                        textNextProgramName.text('');

                        if(isProgramUpdate === true){
                            currentProgramArea.css({left:60});
                            currentProgramArea.addClass("slidingProgram");
                        }else {
                            currentProgramArea.css({left:0});
                            currentProgramArea.removeClass("slidingProgram");
                        }

                        textCurrentProgramName_1.css("display", "");
                        textCurrentProgramName_2.css("display", "");
                        textNextProgramName.css("display", "");
                    }else if(ch.desc == 2) {
                        textCurrentProgramName_1.text(ch.name);
                        textCurrentProgramName_2.text("");
                        textNextProgramName.text('');

                        if(isProgramUpdate === true){
                            currentProgramArea.css({left:60});
                            currentProgramArea.addClass("slidingProgram");
                        }else {
                            currentProgramArea.css({left:0});
                            currentProgramArea.removeClass("slidingProgram");
                        }

                        textCurrentProgramName_1.css("display", "");
                        textCurrentProgramName_2.css("display", "");
                        textNextProgramName.css("display", "");
                    } else {

                        var currentProgramReservation = 0;

                        var isAgeLimit = false;

                        if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true && isProgramUpdate !== undefined && isProgramUpdate === true) {
                            if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(cp)>=parentalRating) {
                                isAgeLimit = true;
                            }
                        }else {
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true && isProgramUpdate !== undefined && isProgramUpdate === false) {
                                if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                                    isAgeLimit = true;
                                }else {
                                    if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                                        if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(cp)>=parentalRating) {
                                            isAgeLimit = true;
                                        }
                                    }
                                }
                            }
                        }

                        if(cp !== undefined && cp !== null) {
                            var tempProgramName = cp.name;

                            if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) === true) {
                                var singer = KTW.utils.epgUtil.getSinger(cp);
                                log.printDbg("singer : [" + singer + "]");
                                if (singer !== null) {
                                    log.printDbg("singer.length : [" + singer.length + "]");
                                    tempProgramName = cp.name + " / " + singer;
                                }
                            }else {
                                /**
                                 *  오디오 채널이 아닌 경우
                                 *  시청 예약 여부 체크
                                 */
                                if(isProgramUpdate === true && isAgeLimit === false){
                                    currentProgramReservation = KTW.managers.service.ReservationManager.isReserved(cp);
                                }
                            }

                            var multiLineText = null;
                            if(currentProgramReservation !== 0) {
                                /**
                                 * 프로그램 제목 Max 길이 : 1031 px
                                 * 예약 아이콘 width : 32px
                                 * margin : 12px
                                 */
                                multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , 1031 - 32 - 12 , -2.55);
                                textCurrentProgramName_1.css({width: 1031 - 32 - 12});
                                textCurrentProgramName_2.css({width: 1031 - 32 - 12});

                                textCurrentProgramName_1.css({left: 32 + 12});
                                textCurrentProgramName_2.css({left: 32 + 12});

                                if(currentProgramReservation === 1) {
                                    imgCurrentProgramReservationIcon.attr("src", "images/icon/icon_book.png");
                                }else {
                                    imgCurrentProgramReservationIcon.attr("src", "images/icon/icon_book_s.png");
                                    imgCurrentProgramReservationIcon.css({width:32});
                                    imgCurrentProgramReservationIcon.css({height:34});
                                }
                                imgCurrentProgramReservationIcon.css("display", "");
                            }else {
                                multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , 1031 , -2.55);
                            }


                            if(isAgeLimit == true && np !== null) {
                                textCurrentProgramName_1.text('시청연령 제한 프로그램입니다');
                                textCurrentProgramName_2.text("");
                                textCurrentProgramName_1.css("display", "");
                                textCurrentProgramName_2.css("display", "");
                            }else {
                                if(multiLineText !== null && multiLineText.length > 1) {
                                    /**
                                     * 채널번호,채널명을 위로 이동 시킴
                                     */
                                    channelInfoDiv.addClass("moveTop");

                                    textCurrentProgramName_1.css({top: 289-231-58});
                                    textCurrentProgramName_1.text(multiLineText[0]);

                                    if(multiLineText.length>2) {
                                        textCurrentProgramName_2.text(multiLineText[1]+multiLineText[2]);
                                        if(currentProgramReservation !== 0) {
                                            imgCurrentProgramAge.css({left: 1031 - 32 - 12 + 10});
                                        }else {
                                            imgCurrentProgramAge.css({left: 1031+10});
                                        }
                                    }else {
                                        textCurrentProgramName_2.text(multiLineText[1]);

                                        var length = util.getTextLength(multiLineText[1] , "RixHead L" , 51,-2.55);
                                        if(currentProgramReservation !== 0) {
                                            imgCurrentProgramAge.css({left: 32 + 12 + length+10});
                                        }else {
                                            imgCurrentProgramAge.css({left: length+10});
                                        }
                                    }
                                    if(currentProgramReservation !== 0) {
                                        imgCurrentProgramReservationIcon.css({top:289-231-58+6});
                                    }
                                }else {
                                    /**
                                     * 채널번호,채널명 위치 원복 시킴.
                                     */
                                    channelInfoDiv.removeClass("moveTop");

                                    textCurrentProgramName_1.text(tempProgramName);
                                    textCurrentProgramName_2.text("");

                                    var length = util.getTextLength(tempProgramName , "RixHead L" , 51,-2.55);
                                    if(currentProgramReservation !== 0) {
                                        imgCurrentProgramAge.css({left: 32 + 12 + length + 10});
                                    }else {
                                        imgCurrentProgramAge.css({left: length + 10});

                                    }
                                }

                                var age = KTW.utils.epgUtil.getAge(cp);
                                //var imgname = "images/icon/icon_age_txtlist_";
                                var imgname = "images/icon/miniepg/icon_age_miniepg_";
                                if(age !== undefined && age !== null) {
                                    if(age<0) {
                                        imgCurrentProgramAge.css("display", "none");
                                    }else {
                                        if(age === 0) {
                                            imgname += "all" + ".png";
                                        }else {
                                            imgname += age + ".png";
                                        }
                                        imgCurrentProgramAge.attr("src", imgname);
                                        imgCurrentProgramAge.css("display", "");
                                    }
                                }
                            }

                            if(isProgramUpdate === true){
                                currentProgramArea.css({left:60});
                                currentProgramArea.addClass("slidingProgram");
                            }else {
                                currentProgramArea.css({left:0});
                                currentProgramArea.removeClass("slidingProgram");
                            }

                        }else {
                            /**
                             * 채널번호,채널명 위치 원복 시킴.
                             */
                            channelInfoDiv.removeClass("moveTop");
                            currentProgramArea.removeClass("slidingProgram");
                            nextProgramArea.removeClass("slidingProgram");

                            textCurrentProgramName_1.text("");
                            textCurrentProgramName_2.text("");

                        }
                        var nextProgramReservation = 0;
                        if(np !== undefined && np !== null) {
                            textNextProgramName.css({left: 0});
                            textNextProgramName.css({width: 350});

                            var isAgeLimit = false;
                            if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true) {
                                if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(np)>=parentalRating) {
                                    isAgeLimit = true;
                                }
                            }
                            if(isAgeLimit === true) {
                                textNextProgramName.text("시청연령 제한 프로그램입니다");
                            }else {
                                if(ch.desc == 2 ) {
                                    textNextProgramName.text(ch.name);
                                }else {
                                    textNextProgramName.text(np.name);
                                }
                                nextProgramReservation = KTW.managers.service.ReservationManager.isReserved(np);

                                if(isProgramUpdate === true) {
                                    imgArwRight.css("display", "");
                                }

                                if(nextProgramReservation !== 0) {
                                    imgNextProgramReservationIcon.css({left:0});
                                    textNextProgramName.css({left: 32+12});
                                    textNextProgramName.css({width: 380-50});

                                    if(nextProgramReservation === 1) {
                                        imgNextProgramReservationIcon.attr("src", "images/icon/icon_book.png");
                                    }else {
                                        imgNextProgramReservationIcon.attr("src", "images/icon/icon_book_s.png");
                                        imgNextProgramReservationIcon.css({top:1});
                                        imgNextProgramReservationIcon.css({width:32});
                                        imgNextProgramReservationIcon.css({height:34});
                                    }

                                    imgNextProgramReservationIcon.css("display", "");
                                }
                            }
                        }else {
                            textNextProgramName.text("");
                        }

                        if(isProgramUpdate === true){
                            nextProgramArea.css({left:(1139+60)});
                            nextProgramArea.addClass("slidingProgram");
                        }else {
                            nextProgramArea.css({left:1139});
                            nextProgramArea.removeClass("slidingProgram");
                        }

                        textCurrentProgramName_1.css("display", "");
                        textCurrentProgramName_2.css("display", "");
                        textNextProgramName.css("display", "");

                        btnJoinDiv.css("display" , "none");
                    }
                }else {
                    /**
                     * 인접 채널 리스트가 노출 되지 않은 상태
                     */
                    if(mainVboChannel.ccid !== ch.ccid) {
                        /**
                         * 현재 재생 중인 AV 채널이 아닌 경우
                         */
                        textCurrentProgramName_1.css('font-size', 51);
                        textCurrentProgramName_1.css('letter-spacing', -2.55);

                        programFontSize = 51;

                        if( ch.desc == 2 ) {
                            textCurrentProgramName_1.text(ch.name);
                            textCurrentProgramName_2.text("");
                            textNextProgramName.text("");

                            textCurrentProgramName_1.css("display", "");
                            textCurrentProgramName_2.css("display", "");
                            textNextProgramName.css("display", "");

                            channelInfoDiv.removeClass("moveTop");
                            currentProgramArea.removeClass("slidingProgram");
                            nextProgramArea.removeClass("slidingProgram");

                        }else if(isBlocked === true) {
                            textCurrentProgramName_1.text("시청 제한 채널입니다");
                            textCurrentProgramName_2.text("");
                            textNextProgramName.text("");

                            textCurrentProgramName_1.css("display", "");
                            textCurrentProgramName_2.css("display", "");
                            textNextProgramName.css("display", "");

                            channelInfoDiv.removeClass("moveTop");
                            currentProgramArea.removeClass("slidingProgram");
                            nextProgramArea.removeClass("slidingProgram");
                        }else {
                            var currentProgramReservation = 0;
                            if(cp !== undefined && cp !== null) {
                                var tempProgramName = cp.name;

                                if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) === true) {
                                    var singer = KTW.utils.epgUtil.getSinger(cp);
                                    log.printDbg("singer : [" + singer + "]");
                                    if (singer !== null) {
                                        log.printDbg("singer.length : [" + singer.length + "]");
                                        tempProgramName = cp.name + " / " + singer;
                                    }
                                }else {
                                    /**
                                     *  오디오 채널이 아닌 경우
                                     *  시청 예약 여부 체크
                                     */
                                    if(isProgramUpdate === true){
                                        currentProgramReservation = KTW.managers.service.ReservationManager.isReserved(cp);
                                    }
                                }
                                var isAgeLimit = false;
                                if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true ) {
                                    if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(cp)>=parentalRating) {
                                        isAgeLimit = true;
                                    }
                                }

                                var multiLineText = null;
                                if(currentProgramReservation !== 0 && isAgeLimit === false) {
                                    /**
                                     * 프로그램 제목 Max 길이 : 1031 px
                                     * 예약 아이콘 width : 32px
                                     * margin : 12px
                                     */
                                    multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , 1031 - 32 - 12,-2.55);
                                    textCurrentProgramName_1.css({width: 1031 - 32 - 12});
                                    textCurrentProgramName_2.css({width: 1031 - 32 - 12});

                                    textCurrentProgramName_1.css({left: 32 + 12});
                                    textCurrentProgramName_2.css({left: 32 + 12});

                                    if(currentProgramReservation === 1) {
                                        imgCurrentProgramReservationIcon.attr("src", "images/icon/icon_book.png");
                                    }else {
                                        imgCurrentProgramReservationIcon.attr("src", "images/icon/icon_book_s.png");
                                        imgCurrentProgramReservationIcon.css({width:32});
                                        imgCurrentProgramReservationIcon.css({height:34});
                                    }
                                    imgCurrentProgramReservationIcon.css("display", "");
                                }else {
                                    multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , 1031,-2.55);
                                }



                                if(isAgeLimit == true) {
                                    textCurrentProgramName_1.text('시청연령 제한 프로그램입니다');
                                    textCurrentProgramName_2.text('');
                                    textCurrentProgramName_1.css("display", "");
                                    textCurrentProgramName_2.css("display", "");
                                }else {
                                    if(multiLineText !== null && multiLineText.length > 1) {
                                        /**
                                         * 채널번호,채널명을 위로 이동 시킴
                                         */
                                        channelInfoDiv.addClass("moveTop");

                                        textCurrentProgramName_1.css({top: 289-231-58});
                                        textCurrentProgramName_1.text(multiLineText[0]);

                                        if(multiLineText.length>2) {
                                            textCurrentProgramName_2.text(multiLineText[1]+multiLineText[2]);
                                            if(currentProgramReservation !== 0) {
                                                imgCurrentProgramAge.css({left: 1031 - 32 - 12 + 10});
                                            }else {
                                                imgCurrentProgramAge.css({left: 1031+10});
                                            }
                                        }else {
                                            textCurrentProgramName_2.text(multiLineText[1]);

                                            var length = util.getTextLength(multiLineText[1] , "RixHead L" , 51,-2.55);
                                            if(currentProgramReservation !== 0) {
                                                imgCurrentProgramAge.css({left: 32 + 12 + length+10});
                                            }else {
                                                imgCurrentProgramAge.css({left: length+10});
                                            }
                                        }
                                        if(currentProgramReservation !== 0) {
                                            imgCurrentProgramReservationIcon.css({top:289-231-58+6});
                                        }
                                    }else {
                                        /**
                                         * 채널번호,채널명 위치 원복 시킴.
                                         */
                                        channelInfoDiv.removeClass("moveTop");

                                        textCurrentProgramName_1.text(tempProgramName);
                                        textCurrentProgramName_2.text("");

                                        var length = util.getTextLength(tempProgramName , "RixHead L" , 51,-2.55);
                                        if(currentProgramReservation !== 0) {
                                            imgCurrentProgramAge.css({left: 32 + 12 + length + 10});
                                        }else {
                                            imgCurrentProgramAge.css({left: length + 10});

                                        }
                                    }

                                    var age = KTW.utils.epgUtil.getAge(cp);
                                    //var imgname = "images/icon/icon_age_txtlist_";
                                    var imgname = "images/icon/miniepg/icon_age_miniepg_";
                                    if(age !== undefined && age !== null) {
                                        if(age<0) {
                                            imgCurrentProgramAge.css("display", "none");
                                        }else {
                                            if(age === 0) {
                                                imgname += "all" + ".png";
                                            }else {
                                                imgname += age + ".png";
                                            }
                                            imgCurrentProgramAge.attr("src", imgname);
                                            imgCurrentProgramAge.css("display", "");

                                        }
                                    }
                                }

                                if(isProgramUpdate === true){
                                    currentProgramArea.css({left:60});
                                    currentProgramArea.addClass("slidingProgram");
                                }else {
                                    currentProgramArea.css({left:0});
                                    currentProgramArea.removeClass("slidingProgram");
                                }

                            }else {
                                /**
                                 * 채널번호,채널명 위치 원복 시킴.
                                 */
                                channelInfoDiv.removeClass("moveTop");
                                currentProgramArea.removeClass("slidingProgram");
                                nextProgramArea.removeClass("slidingProgram");

                                textCurrentProgramName_1.text("");
                                textCurrentProgramName_2.text("");
                            }
                            var nextProgramReservation = 0;
                            if(np !== undefined && np !== null) {
                                textNextProgramName.css({left: 0});
                                textNextProgramName.css({width: 350});

                                if(isPipChannel === false) {
                                    imgArwRight.css("display", "");
                                }

                                var isAgeLimit = false;
                                if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true) {
                                    if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(np)>=parentalRating) {
                                        isAgeLimit = true;
                                    }
                                }

                                if (isAgeLimit === true) {
                                    textNextProgramName.text("시청연령 제한 프로그램입니다");
                                }else {
                                    textNextProgramName.text(np.name);

                                    nextProgramReservation = KTW.managers.service.ReservationManager.isReserved(np);

                                    if(nextProgramReservation !== 0 && isAgeLimit === false) {
                                        imgNextProgramReservationIcon.css({left:0});
                                        textNextProgramName.css({left: 32+12});
                                        textNextProgramName.css({width: 380-50});

                                        if(nextProgramReservation === 1) {
                                            imgNextProgramReservationIcon.attr("src", "images/icon/icon_book.png");
                                        }else {
                                            imgNextProgramReservationIcon.attr("src", "images/icon/icon_book_s.png");
                                            imgNextProgramReservationIcon.css({top:1});
                                            imgNextProgramReservationIcon.css({width:32});
                                            imgNextProgramReservationIcon.css({height:34});
                                        }

                                        imgNextProgramReservationIcon.css("display", "");
                                    }
                                }
                            }else {
                                textNextProgramName.text("");
                            }

                            if(isProgramUpdate === true){
                                nextProgramArea.css({left:(1139+60)});
                                nextProgramArea.addClass("slidingProgram");
                            }else {
                                nextProgramArea.css({left:1139});
                                nextProgramArea.removeClass("slidingProgram");
                            }

                            textCurrentProgramName_1.css("display", "");
                            textCurrentProgramName_2.css("display", "");
                            textNextProgramName.css("display", "");
                        }
                    }else {
                        /**
                         * 현재 재생 중인 AV 채널인 경우
                         */
                        if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                            || (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT && KTW.managers.service.KidsModeManager.isKidsMode() === false)
                            || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                            /**
                             * 블럭 채널 이거나 미가입 채널인지 채널 인 경우 처리
                             * 가입 및 성인인증 Input box 노출 함.
                             */
                            _updateBlockedNotSubscribedChannel();
                        }else {
                            /**
                             * 일반 채널 인 경우
                             */
                            textCurrentProgramName_1.css('font-size', 51);
                            textCurrentProgramName_1.css('letter-spacing', -2.55);

                            programFontSize = 51;

                            if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW && isProgramUpdate === false && KTW.CONSTANT.IS_OTS === false) {
                                btnJoinText.text("가입");
                                btnJoinDiv.css("display" , "");
                            }else {
                                /**
                                 * 무비초이스 구매 관련 버튼 노출 여부 체크함.
                                 */
                                if (KTW.CONSTANT.IS_OTS === true &&
                                    KTW.oipf.AdapterHandler.navAdapter.isSkyChoiceChannel(ch) === true &&
                                    parentView.getRightTopView().isBuyMovieChoice() === true
                                    && mainVboChannel.ccid === ch.ccid && isProgramUpdate === false && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT &&
                                    state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                                    if(cp.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL || cp.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                                        btnJoinDiv.css("display" , "none");
                                    }else {
                                        btnJoinText.text("구매");
                                        btnJoinDiv.css("display" , "");
                                    }

                                }else {
                                    btnJoinDiv.css("display" , "none");
                                }
                            }


                            var currentProgramReservation = 0;
                            if(cp !== undefined && cp !== null) {
                                var tempProgramName = cp.name;

                                if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) === true) {
                                    var singer = KTW.utils.epgUtil.getSinger(cp);
                                    log.printDbg("singer : [" + singer + "]");
                                    if (singer !== null) {
                                        log.printDbg("singer.length : [" + singer.length + "]");
                                        tempProgramName = cp.name + " / " + singer;
                                    }
                                }else {
                                    /**
                                     *  오디오 채널이 아닌 경우
                                     *  시청 예약 여부 체크
                                     */
                                    if(isProgramUpdate === true){
                                        currentProgramReservation = KTW.managers.service.ReservationManager.isReserved(cp);
                                    }
                                }

                                var isAgeLimit = false;
                                if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true &&
                                    isProgramUpdate !== undefined && isProgramUpdate === true) {
                                    if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(cp)>=parentalRating) {
                                        isAgeLimit = true;
                                    }
                                }else {
                                    if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT &&
                                        KTW.managers.service.KidsModeManager.isKidsMode() === true &&
                                        isProgramUpdate !== undefined && isProgramUpdate === false) {
                                        if(tempProgramName === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE) {
                                            isAgeLimit = false;
                                        }else {
                                            isAgeLimit = true;
                                        }
                                    }
                                }

                                var multiLineText = null;
                                var programWidth = 1031;
                                if(currentProgramReservation !== 0 && isAgeLimit === false) {
                                    /**
                                     * 프로그램 제목 Max 길이 : 1031 px
                                     * 예약 아이콘 width : 32px
                                     * margin : 12px
                                     */
                                    multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , 1031 - 32 - 12,-2.55);
                                    textCurrentProgramName_1.css({width: 1031 - 32 - 12});
                                    textCurrentProgramName_2.css({width: 1031 - 32 - 12});

                                    textCurrentProgramName_1.css({left: 32 + 12});
                                    textCurrentProgramName_2.css({left: 32 + 12});

                                    if(currentProgramReservation === 1) {
                                        imgCurrentProgramReservationIcon.attr("src", "images/icon/icon_book.png");
                                    }else {
                                        imgCurrentProgramReservationIcon.attr("src", "images/icon/icon_book_s.png");
                                        imgCurrentProgramReservationIcon.css({width:32});
                                        imgCurrentProgramReservationIcon.css({height:34});
                                    }
                                    imgCurrentProgramReservationIcon.css("display", "");
                                }else {

                                    if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW && isProgramUpdate === false) {
                                        textCurrentProgramName_1.css({width: 821});
                                        textCurrentProgramName_2.css({width: 821});
                                        programWidth = 821;
                                    }
                                    multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , programWidth,-2.55);
                                }



                                if(isAgeLimit == true) {
                                    channelInfoDiv.removeClass("moveTop");

                                    textCurrentProgramName_1.text('시청연령 제한 프로그램입니다');
                                    textCurrentProgramName_2.text('');
                                    textCurrentProgramName_1.css("display", "");
                                    textCurrentProgramName_2.css("display", "");
                                }else {
                                    if(multiLineText !== null && multiLineText.length > 1) {
                                        /**
                                         * 채널번호,채널명을 위로 이동 시킴
                                         */
                                        channelInfoDiv.addClass("moveTop");

                                        textCurrentProgramName_1.css({top: 289-231-58});
                                        textCurrentProgramName_1.text(multiLineText[0]);

                                        if(multiLineText.length>2) {
                                            textCurrentProgramName_2.text(multiLineText[1]+multiLineText[2]);
                                            if(currentProgramReservation !== 0) {
                                                imgCurrentProgramAge.css({left: programWidth - 32 - 12 + 10});
                                            }else {
                                                imgCurrentProgramAge.css({left: programWidth+10});
                                            }
                                        }else {
                                            textCurrentProgramName_2.text(multiLineText[1]);

                                            var length = util.getTextLength(multiLineText[1] , "RixHead L" , 51,-2.55);
                                            if(currentProgramReservation !== 0) {
                                                imgCurrentProgramAge.css({left: 32 + 12 + length+10});
                                            }else {
                                                imgCurrentProgramAge.css({left: length+10});
                                            }
                                        }
                                        if(currentProgramReservation !== 0) {
                                            imgCurrentProgramReservationIcon.css({top:289-231-58+6});
                                        }
                                    }else {
                                        /**
                                         * 채널번호,채널명 위치 원복 시킴.
                                         */
                                        channelInfoDiv.removeClass("moveTop");

                                        textCurrentProgramName_1.text(tempProgramName);
                                        textCurrentProgramName_2.text("");

                                        var length = util.getTextLength(tempProgramName , "RixHead L" , 51,-2.55);
                                        if(currentProgramReservation !== 0) {
                                            imgCurrentProgramAge.css({left: 32 + 12 + length + 10});
                                        }else {
                                            imgCurrentProgramAge.css({left: length + 10});

                                        }
                                    }

                                    var age = KTW.utils.epgUtil.getAge(cp);
                                    //var imgname = "images/icon/icon_age_txtlist_";
                                    var imgname = "images/icon/miniepg/icon_age_miniepg_";
                                    if(age !== undefined && age !== null) {
                                        if(age<0) {
                                            imgCurrentProgramAge.css("display", "none");

                                        }else {
                                            if(age === 0) {
                                                imgname += "all" + ".png";
                                            }else {
                                                imgname += age + ".png";
                                            }
                                            imgCurrentProgramAge.attr("src", imgname);
                                            imgCurrentProgramAge.css("display", "");
                                        }
                                    }
                                }

                                if(isProgramUpdate === true){
                                    currentProgramArea.css({left:60});
                                    currentProgramArea.addClass("slidingProgram");
                                }else {
                                    currentProgramArea.css({left:0});
                                    currentProgramArea.removeClass("slidingProgram");
                                }

                            }else {
                                /**
                                 * 채널번호,채널명 위치 원복 시킴.
                                 */
                                channelInfoDiv.removeClass("moveTop");
                                currentProgramArea.removeClass("slidingProgram");
                                nextProgramArea.removeClass("slidingProgram");

                                textCurrentProgramName_1.text("");
                                textCurrentProgramName_2.text("");

                            }
                            var nextProgramReservation = 0;
                            if(np !== undefined && np !== null) {
                                textNextProgramName.css({left: 0});
                                textNextProgramName.css({width: 350});

                                if(isPipChannel === false) {
                                    if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW && isProgramUpdate === false && KTW.CONSTANT.IS_OTS === false) {
                                        imgArwRight.css("display", "none");
                                    }else {
                                        if (KTW.CONSTANT.IS_OTS === true &&
                                            KTW.oipf.AdapterHandler.navAdapter.isSkyChoiceChannel(ch) === true &&
                                            parentView.getRightTopView().isBuyMovieChoice() === true
                                            && mainVboChannel.ccid === ch.ccid && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT &&
                                            state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                                            imgArwRight.css("display", "none");
                                        }else {
                                            imgArwRight.css("display", "");
                                        }
                                    }
                                }

                                var isAgeLimit = false;
                                if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true) {
                                    if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(np)>=parentalRating) {
                                        isAgeLimit = true;
                                    }
                                }
                                if(isAgeLimit === true) {
                                    textNextProgramName.text("시청연령 제한 프로그램입니다");
                                }else {
                                    textNextProgramName.text(np.name);
                                    nextProgramReservation = KTW.managers.service.ReservationManager.isReserved(np);



                                    if(nextProgramReservation !== 0 && isAgeLimit === false) {
                                        imgNextProgramReservationIcon.css({left:0});
                                        textNextProgramName.css({left: 32+12});
                                        textNextProgramName.css({width: 380-50});

                                        if(nextProgramReservation === 1) {
                                            imgNextProgramReservationIcon.attr("src", "images/icon/icon_book.png");
                                        }else {
                                            imgNextProgramReservationIcon.attr("src", "images/icon/icon_book_s.png");
                                            imgNextProgramReservationIcon.css({top:1});
                                            imgNextProgramReservationIcon.css({width:32});
                                            imgNextProgramReservationIcon.css({height:34});
                                        }

                                        imgNextProgramReservationIcon.css("display", "");
                                    }
                                }
                            }else {
                                textNextProgramName.text("");
                            }

                            if(isProgramUpdate === true){
                                nextProgramArea.css({left:(1139+60)});
                                nextProgramArea.addClass("slidingProgram");
                            }else {
                                nextProgramArea.css({left:1139});
                                nextProgramArea.removeClass("slidingProgram");
                            }

                            textCurrentProgramName_1.css("display", "");
                            textCurrentProgramName_2.css("display", "");
                            textNextProgramName.css("display", "");
                        }
                    }
                }
            }
        };

        this.hide = function () {
            log.printDbg("hide()");
            _hideAll();
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey() keyCode : " + keyCode);

            var consumed = false;

            // skyChoice purchase process
            if (BuySkyCh.isBuying() === true &&
                state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                /**
                 * 무비초이스에서 미래프로그램 검색 할 경우 OK 키는 시청예약임.
                 */
                if((keyCode === KTW.KEY_CODE.ENTER || keyCode === KTW.KEY_CODE.OK) && isProgramUpdate !== undefined && isProgramUpdate === true) {
                    return consumed;
                }else {
                    /**
                     *  업데이트 중이거나 프로그램 정보가 없을 경우 미구매 상태라고 하더라고 구매 진행 안함.
                     */
                    if((keyCode === KTW.KEY_CODE.ENTER || keyCode === KTW.KEY_CODE.OK) && (cp.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL || cp.name === KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE)) {
                        return true;
                    }
                }

                consumed = BuySkyCh.controlKey(keyCode);
                if (consumed === true) {
                    return true;
                }
            }

            if(parentView.getIsShowCPMSInfo() === true) {

                if(keyCode === KTW.KEY_CODE.ENTER || keyCode === KTW.KEY_CODE.OK) {
                    log.printDbg("CPMS 선택 함");
                    _purchaseChannel(KTW.CONSTANT.IS_OTS === true && ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S);
                    consumed = true;
                }else if(keyCode === KTW.KEY_CODE.LEFT || keyCode === KTW.KEY_CODE.RIGHT) {
                    if(cpmsInfo.details.length>1) {
                        log.printDbg("CPMS 좌우 이동함");
                        if(keyCode === KTW.KEY_CODE.RIGHT) {
                            otsPurchaseBtnIndex++;
                        }else  {
                            otsPurchaseBtnIndex--;
                        }

                        if(otsPurchaseBtnIndex<0) {
                            otsPurchaseBtnIndex = 1;
                        }else {
                            if(otsPurchaseBtnIndex>1) {
                                otsPurchaseBtnIndex = 0;
                            }
                        }

                        parentView.updateFocusOTSCPMSInfo(otsPurchaseBtnIndex);
                    }
                    consumed = true;
                }else if(keyCode === KTW.KEY_CODE.EXIT) {
                    /**
                     * jjh1117 2017.02.10
                     * UI Flipbook 1.19(page 709)에 의해 나가기 키에 대해 아무 반응 없어야 함.
                     */
                    consumed = true;
                }

                return consumed;
            }

            switch (keyCode) {
                case KTW.KEY_CODE.ENTER :
                case KTW.KEY_CODE.OK :
                    if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                        /**
                         * 채널 결제 모듈 연동
                         */
                        _purchaseChannel(KTW.CONSTANT.IS_OTS === true && ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S);
                    }
                    else {
                        if(KTW.managers.service.KidsModeManager.isKidsMode() === true && state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                            return false;
                        }else {
                            _checkPW();
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT :
                    if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                        || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                        if(KTW.managers.service.KidsModeManager.isKidsMode() === true && state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                            return consumed;
                        }

                        var pw = KTW.managers.auth.AuthManager.getPW();
                        if(pw !== null && pw.length>0) {
                            _deleteNum();
                            consumed = true;
                        }else {
                            return consumed;
                        }
                    }else {
                        return consumed;
                    }
                    break;
                case KTW.KEY_CODE.RIGHT :
                    if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                        || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                         || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                        if(KTW.managers.service.KidsModeManager.isKidsMode() === true && state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                            return consumed;
                        }
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.UP :
                    break;
                case KTW.KEY_CODE.DOWN :
                    break;
                case KTW.KEY_CODE.CONTEXT :
                    break;
                case KTW.KEY_CODE.EXIT :
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK :
                    if (KTW.CONSTANT.IS_OTS === true && state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                        /**
                         * jjh1117 2016.11.02
                         * TODO 이 부분 추후 확인 후 처리 되어야 함.
                         *
                         */
                        // if (ots_purchase_step === 1) {
                        //     ots_purchase_step = 0;
                        //     miniEpgView.hideOTSPurchase(true);
                        //     consumed = true;
                        // }
                    }
                    consumed = false;
                    break;
            }

            if(KTW.managers.service.KidsModeManager.isKidsMode() === true && state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                consumed = false;
            }else {
                if(consumed === false) {
                    var number = util.keyCodeToNumber(keyCode);
                    if (number >= 0) {
                        if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                            || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                            _inputNum(number);
                            consumed = true;
                        }
                    }

                }
            }

            return consumed;
        };


        this.isBuyMovieChoice = function () {
            return BuySkyCh.isBuying();
        };

        this.getBuyMovieChoiceStep = function () {
            return BuySkyCh.getStep();
        };

        this.hideJoinBtn = function () {
            btnJoinDiv.css("display", "none");
        };

        this.preViewPurchaseChannel = function () {
            _purchaseChannel(KTW.CONSTANT.IS_OTS === true && ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S);
        };

        this.updateCHNameCHNum = function(favoriteupdate) {
            _updateCHNameCHNum(favoriteupdate);
        };

        /**
         * 우측 상단 채널 정보 및 프로그램 정보 노출 영역 DIV
         * 전체 화면(1920X1080)에서 left:292 , top:512 , width:1628 , height:358 위치
         * MiniEpgMainView의 leftDiv에서는 left:0 , top:0 , width:1628 , height:358 위치
         */
        function _createElement() {
            log.printDbg("_createElement()");
            _createChannelInfoElement();

            _createCurrentProgramInfoElement();

            _createJoinBoxElement();
            _createPasswordInputBoxElement();

            _createNextProgramInfoElement();

            _createMovieChoiceElement();
        }

        /**
         * 채널번호/채널명/시청제한채널 아이콘 / UHD 아이콘
         */
        function _createChannelInfoElement() {
            log.printDbg("_createChannelInfoElement()");
            channelInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "miniepgChannelNumberName",
                    css: {
                        position: "absolute", "-webkit-transition": "-webkit-transform 0.5s"
                    }
                },
                parent: parentDiv
            });

            imgBlockedIcon = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/icon/icon_ch_blocked.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 1,
                        width: 30,
                        height: 30
                    }
                },
                parent: channelInfoDiv
            });


            textChannelNumber = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 66, height: 36,
                        color: "rgba(235, 182, 147, 1)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: "",
                parent: channelInfoDiv
            });

            textChannelName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_name",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 73, top: 0, width: 600, height: 38,
                        color: "rgba(255, 254, 254, 0.8)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: "",
                parent: channelInfoDiv
            });


            imgChannelResolutionIcon = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/icon/icon_tag_uhd.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 2,
                        width: 58,
                        height: 32
                    }
                },
                parent: channelInfoDiv
            });
        }

        /**
         * 현재 프로그램 정보(예약아이콘+제목+연령아이콘) or 블럭 채널 / 미가입채널 시 안내 문구
         */
        function _createCurrentProgramInfoElement() {
            log.printDbg("_createCurrentProgramInfoElement()");

            /**
             * left: 0, top: 231, width: 1031, height: 358 - 231
             */
            currentProgramArea = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "miniepgCurrentProgram",
                    css: {
                        position: "absolute"
                    }
                },
                parent: parentDiv
            });



            imgCurrentProgramReservationIcon = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "current_program_reservation_img",
                    src: "images/icon/icon_book.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 295 - 231,
                        width: 26,
                        height: 28,
                        opacity:0.5
                    }
                },
                parent: currentProgramArea
            });


            textCurrentProgramName_1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "focus_program_name1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0, top: 289 - 231, width: 1031, height: 53,
                        color: "rgba(255, 255, 255, 1)", "font-size": 51 , "letter-spacing":-2.55
                    }
                },
                text: "",
                parent: currentProgramArea
            });

            textCurrentProgramName_2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "focus_program_name2",
                    class: "font_l cut" ,
                    css: {
                        position: "absolute", left: 0, top: 289-231, width: 1031, height: 53,
                        color: "rgba(255, 255, 255, 1)", "font-size": 51, "letter-spacing":-2.55
                    }
                },
                text: "",
                parent: currentProgramArea
            });

            /**
             * Program명과 간격은 10px 띄워야 함
             */
            imgCurrentProgramAge = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "current_program_age_img",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 294-231,
                        width: 45,
                        height: 45
                    }
                },
                parent: currentProgramArea
            });
        }

        /**
         * Blocked 채널에서 사용 할 Pin 인증 Input Box
         */
        function _createJoinBoxElement() {
            log.printDbg("_createJoinBoxElement()");

            btnJoinDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1268-370), top: 273, width: 210, height: 69,
                        "background-color": "rgba(210,51,47,1)"
                    }
                },
                parent: parentDiv
            });

            btnJoinText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_join",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 18, width: 210 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30, "text-align" : "center" , "letter-spacing":-1.5
                    }
                },
                text: "가입",
                parent: btnJoinDiv
            });
        }

        /**
         * Blocked 채널에서 사용 할 Pin 인증 Input Box
         */
        function _createPasswordInputBoxElement() {
            log.printDbg("_createPasswordInputBoxElement()");

            passwordInputBoxDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1178-370), top: 273, width: 300, height: 76
                    }
                },
                parent: div
            });

            passwordInputBox = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/miniepg/pw_box_w300_foc.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 300,
                        height: 68
                    }
                },
                parent: passwordInputBoxDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 89, top: 19, width: 300, height: 47,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 45
                    }
                },
                text: "* * * *",
                parent: passwordInputBoxDiv
            });

            textInputBox = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_input",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 89, top: 19, width: 300, height: 47,
                        color: "rgba(255, 255, 255, 1)", "font-size": 45
                    }
                },
                text: "",
                parent: passwordInputBoxDiv
            });

        }

        /**
         * 다음 프로그램 정보(예약아이콘+제목) 또한 미래 프로그램 탐색 우측 아이콘
         */
        function _createNextProgramInfoElement() {
            log.printDbg("_createNextProgramInfoElement()");

            imgArwRight = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_program_right_img",
                    src: "images/miniepg/arw_view_mini_r2.png",
                    css: {
                        position: "absolute",
                        left: (1479-370),
                        top: 309,
                        width: 15,
                        height: 26
                    }
                },
                parent: parentDiv
            });

            /**
             * left: 886, top: 305, width: 408, height: 41
             */
            nextProgramArea =  util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "miniepgNextProgram",
                    css: {
                        position: "absolute"
                    }
                },
                parent: parentDiv
            });

            textNextProgramName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "next_program_name",
                    class: "font_l cut" ,
                    css: {
                        position: "absolute", left: 0, top: 0, width: 390 , height: 41,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 39 , "letter-spacing":-2.55
                    }
                },
                text: "",
                parent: nextProgramArea
            });



            imgNextProgramReservationIcon = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "next_program_reservation_img",
                    src: "images/icon/icon_book.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 4,
                        width: 26,
                        height: 28,
                        opacity:0.5
                    }
                },
                parent: nextProgramArea
            });
        }

        /**
         * Blocked 채널에서 사용 할 Pin 인증 Input Box
         */
        function _createMovieChoiceElement() {
            var btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_div_ok",
                    css: {
                        position: "absolute", left: (1486-370), top: 785-512, width: 150, height: 68
                    }
                },
                parent: parentDiv
            });

            var defaultBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 150, height: 68,
                        "background-color": "rgba(255,255,255,0.1)"
                    }
                },
                parent: btnDiv
            });

            var focusBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 150, height: 68,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnDiv
            });

            var default_text = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "default_text",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 804-785, width: 150 , height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center" ,"letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: btnDiv
            });

            var focus_text = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "default_text",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 804-785, width: 150 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center" , "letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: btnDiv
            });

            var btnobj = {
                root_div : btnDiv,
                default_btn : defaultBtn,
                focus_btn : focusBtn,
                default_text : default_text,
                focus_text : focus_text
            };

            btnOk = btnobj;


            btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_div_cancel",
                    css: {
                        position: "absolute", left: (1647-370), top: 785-512, width: 150, height: 68
                    }
                },
                parent: parentDiv
            });

            defaultBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 150, height: 68,
                        "background-color": "rgba(255,255,255,0.1)"
                    }
                },
                parent: btnDiv
            });

            focusBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 150, height: 68,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnDiv
            });

            default_text = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "default_text",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 804-785, width: 150 , height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center" , "letter-spacing":-1.5
                    }
                },
                text: "취소",
                parent: btnDiv
            });

            focus_text = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "default_text",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 804-785, width: 150 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center" , "letter-spacing":-1.5
                    }
                },
                text: "취소",
                parent: btnDiv
            });

            btnobj = {
                root_div : btnDiv,
                default_btn : defaultBtn,
                focus_btn : focusBtn,
                default_text : default_text,
                focus_text : focus_text
            };

            btnCancel = btnobj;

            movieChoicePriceUnit = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "default_text",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: (1140-370), top: 809-512, width: 30 , height: 28,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 26,"text-align" : "left"
                    }
                },
                text: "원",
                parent: parentDiv
            });

            movieChoicePrice = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "default_text",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: ((1023-60) - 370), top: 797-512, width: 170 , height: 47,
                        color: "rgba(255, 255, 255, 1)", "font-size": 45,"text-align" : "right" , "letter-spacing":-2.55
                    }
                },
                text: "",
                parent: parentDiv
            });

        }


        function _hideAll(isProgramUpdate) {
            log.printDbg("_hideAll()");

            if(isProgramUpdate !== undefined && isProgramUpdate === false) {
                imgBlockedIcon.css("display", "none");
                textChannelNumber.css("display", "none");
                textChannelName.css("display", "none");
                imgChannelResolutionIcon.css("display", "none");
                channelInfoDiv.removeClass("moveTop");
                channelInfoDiv.css("display", "none");
            }
            currentProgramArea.css({left:0});
            currentProgramArea.removeClass("slidingProgram");

            nextProgramArea.css({left:1139});
            nextProgramArea.removeClass("slidingProgram");

            imgCurrentProgramReservationIcon.css({top:295-231});

            imgCurrentProgramReservationIcon.css({width:26});
            imgCurrentProgramReservationIcon.css({height:28});
            imgCurrentProgramReservationIcon.css("display", "none");

            textCurrentProgramName_1.css({top: 289-231});

            textCurrentProgramName_1.css({left: 0});
            textCurrentProgramName_2.css({left: 0});
            textCurrentProgramName_1.css({width: 1031});
            textCurrentProgramName_2.css({width: 1031});

            textCurrentProgramName_1.css("display", "none");
            textCurrentProgramName_2.css("display", "none");

            imgCurrentProgramAge.css("display", "none");

            btnJoinDiv.css("display", "none");

            textNextProgramName.css("display", "none");
            imgArwRight.css("display", "none");

            imgNextProgramReservationIcon.css({top:4});
            imgNextProgramReservationIcon.css({width:26});
            imgNextProgramReservationIcon.css({height:28});

            imgNextProgramReservationIcon.css("display", "none");

            passwordInputBoxDiv.css({left:(1178-370)});
            passwordInputBoxDiv.css("display", "none");

            _stopSpeechRecognizer();

            movieChoicePriceUnit.css("display", "none");
            movieChoicePrice.css("display", "none");
            btnOk.root_div.css("display", "none");
            btnCancel.root_div.css("display", "none");
        }

        function _startSpeechRecognizer(type) {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() type :  " + type + " , inputPinType : " + inputPinType);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            _stopSpeechRecognizer();

            KTW.oipf.AdapterHandler.speechRecognizerAdapter.start(true);
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            inputPinType = type;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() type :  " + inputPinType);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(inputPinType != 0) {
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.stop();
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);
            }

            inputPinType = 0;
        }


        /**
         * 채널번호/채널명,시청제한 아이콘 노출 , UHD 아이콘 노출
         * 미래 프로그램 검색 시에는 업데이트 하지 않음.
         */
        function _updateCHNameCHNum(favoriteupdate) {
            if(favoriteupdate !== undefined || (isProgramUpdate !== undefined && isProgramUpdate === false)) {

                var  posLeft = 0;
                var defaultPromoChannel = null;
                /**
                 * 시청제한 아이콘 노출 여부
                 */
                if(isHiddenPromoChannel === true) {
                    defaultPromoChannel = parentView.getDefaultPromoChannel();
                }

                if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED || (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN && isBlocked === true)) {
                    imgBlockedIcon.attr("src" , "images/icon/icon_ch_blocked.png");
                    imgBlockedIcon.css("display", "");
                    posLeft+=30;
                    posLeft+=17;
                }else if (isHiddenPromoChannel === false && KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(ch) === true) {
                    imgBlockedIcon.attr("src" , "images/icon/icon_ch_favorite.png");
                    imgBlockedIcon.css("display", "");
                    posLeft+=30;
                    posLeft+=17;
                }else if (isHiddenPromoChannel === true) {
                    if(ch.ccid === mainVboChannel.ccid) {
                        if(defaultPromoChannel !== null && KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(defaultPromoChannel) === true) {
                            imgBlockedIcon.attr("src" , "images/icon/icon_ch_favorite.png");
                            imgBlockedIcon.css("display", "");
                            posLeft+=30;
                            posLeft+=17;
                        }else {
                            imgBlockedIcon.css("display", "none");
                        }
                    }else {
                        if(KTW.managers.service.FavoriteChannelManager.isFavoriteChannel(ch) === true) {
                            imgBlockedIcon.attr("src" , "images/icon/icon_ch_favorite.png");
                            imgBlockedIcon.css("display", "");
                            posLeft+=30;
                            posLeft+=17;
                        }else {
                            imgBlockedIcon.css("display", "none");
                        }
                    }
                }else {
                    imgBlockedIcon.css("display", "none");
                }

                /**
                 * 채널번호 Udpate
                 */
                var chNum = util.numToStr(ch.majorChannel, 3);
                textChannelNumber.text(chNum);
                var textlength = util.getTextLength(chNum , "RixHead B" , 36 , -1.8);
                textChannelNumber.css({left:posLeft});
                textChannelNumber.css({width:textlength});
                textChannelNumber.css("display", "");


                posLeft+=textlength;
                posLeft+=(75-textlength);

                /**
                 * 채널명 Udpate
                 */
                textChannelName.css({left:posLeft});
                textlength = util.getTextLength(ch.name , "RixHead B" , 36 , -1.8);
                textChannelName.css({width:textlength});
                textChannelName.text(ch.name);
                textChannelName.css("display", "");
                posLeft+=textlength;
                posLeft+=17;

                /**
                 * UHD , FullHD , Audio 아이콘 Udpate
                 */

                if (ch.isFHD === true && KTW.CONSTANT.IS_OTS === false) {
                    imgChannelResolutionIcon.attr("src", "images/icon/icon_tag_fullhd.png");
                    imgChannelResolutionIcon.css({left:posLeft});
                    imgChannelResolutionIcon.css({width:82});
                    imgChannelResolutionIcon.css("display", "");
                }
                else if (ch.isUHD === true) {
                    imgChannelResolutionIcon.attr("src", "images/icon/icon_tag_uhd.png");
                    imgChannelResolutionIcon.css({left:posLeft});
                    imgChannelResolutionIcon.css({width:58});
                    imgChannelResolutionIcon.css("display", "");
                }else {
                    imgChannelResolutionIcon.css("display", "none");
                }

                channelInfoDiv.css("display", "");
            }

        }

        /**
         * 블럭채널 및 미가입 채널 안내 문구 및 버튼 노출
         */
        function _updateBlockedNotSubscribedChannel() {
            /**
             * 채널번호,채널명 위치 원복 시킴.
             */
            channelInfoDiv.removeClass("moveTop");

            /**
             *  MINI EPG PIP 인 경우 성인인증/가입 처리 하지 않고
             *  block 이유를 노출한다.
             */
            if(isPipChannel === false) {
                if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                    textCurrentProgramName_1.text('olleh tv 비밀번호(성인인증)를 입력해 주세요');
                }
                else if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                    if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                        textCurrentProgramName_1.text('시청연령 제한 프로그램입니다');
                    }else {
                        textCurrentProgramName_1.text('성인인증 비밀번호를 입력해 주세요');
                    }
                }
                else if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                    textCurrentProgramName_1.text('가입이 필요한 채널입니다. 가입하시려면 확인키를 눌러주세요');
                }
                textCurrentProgramName_2.text("");

                textCurrentProgramName_1.css('font-size', 36);
                textCurrentProgramName_1.css('letter-spacing', -1.18);

                programFontSize = 36;

                textCurrentProgramName_1.css("display", "");
                textCurrentProgramName_2.css("display", "");

                if (state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                    if(KTW.managers.service.KidsModeManager.isKidsMode() === false) {
                       passwordInputBoxDiv.css("display" , "");
                       passwordInputBox.attr("src" , "images/miniepg/pw_box_w300_foc.png");
                       passwordInputBox.css({opacity: 1});
                       textInputBox.css({opacity: 1});

                        _startSpeechRecognizer(1);
                    }
                }else {
                    btnJoinText.text("가입");
                    btnJoinDiv.css("display" , "");
                }
            }
        }

        /**
         * "프로그램 정보를 가져오는 중입니다" 문구 노출
         */
        function _updateMovieChoicePPVInfoSearching() {
            channelInfoDiv.removeClass("moveTop");
            textCurrentProgramName_1.text('프로그램 정보를 가져오는 중입니다');
            textCurrentProgramName_2.text("");

            textCurrentProgramName_1.css('font-size', 36);
            programFontSize = 36;
            textCurrentProgramName_1.css("display", "");
            textCurrentProgramName_2.css("display", "");

            imgCurrentProgramAge.css("display", "none");
            textNextProgramName.css("display", "none");
            imgNextProgramReservationIcon.css("display", "none");
            
            btnJoinDiv.css("display" , "none");
        }


        function _updateMovieChoicePPVPurchase( focusIndex , data) {
            channelInfoDiv.removeClass("moveTop");
            textCurrentProgramName_1.text("구매인증 번호 4자리를 입력해 주세요");
            textCurrentProgramName_2.text("");

            textCurrentProgramName_1.css('font-size', 36);
            programFontSize = 36;
            textCurrentProgramName_1.css("display", "");
            textCurrentProgramName_2.css("display", "");

            imgCurrentProgramAge.css("display", "none");
            textNextProgramName.css("display", "none");
            imgNextProgramReservationIcon.css("display", "none");

            passwordInputBoxDiv.css({left:(1178-370)});
            passwordInputBoxDiv.css("display" , "");
            passwordInputBox.attr("src" , "images/miniepg/pw_box_w300_foc.png");
            passwordInputBox.css({opacity: 1});
            textInputBox.css({opacity: 1});

            if (data != null && data.price != null) {
                movieChoicePriceUnit.css("display", "");
                movieChoicePrice.text(data.price);
                movieChoicePrice.css("display", "");
            }else {
                movieChoicePriceUnit.css("display", "none");
                movieChoicePrice.text("");
                movieChoicePrice.css("display", "none");
            }

            btnOk.root_div.css("display", "");
            btnCancel.root_div.css("display", "");

            _updateInputBox("");
            _focusMovieChoicePPVPurchase(focusIndex);
            KTW.managers.auth.AuthManager.initPW();

            btnJoinDiv.css("display" , "none");

            
        }


        /**
         * "구매해 주셔서 감사합니다. 잠시만 기다려 주세요" 문구 노출
         */
        function _updateMovieChoicePPVPurchaseSuccess() {
            channelInfoDiv.removeClass("moveTop");
            textCurrentProgramName_1.text('구매해 주셔서 감사합니다. 잠시만 기다려 주세요');
            textCurrentProgramName_2.text("");

            textCurrentProgramName_1.css('font-size', 36);
            programFontSize = 36;
            textCurrentProgramName_1.css("display", "");
            textCurrentProgramName_2.css("display", "");

            imgCurrentProgramAge.css("display", "none");
            textNextProgramName.css("display", "none");
            imgNextProgramReservationIcon.css("display", "none");

            passwordInputBoxDiv.css({opacity: 1});
            passwordInputBox.attr("src" , "images/miniepg/pw_box_w300_foc.png");
            passwordInputBox.css({opacity: 1});
            textInputBox.css({opacity: 1});
            passwordInputBoxDiv.css({left:(1178-370)});
            passwordInputBoxDiv.css("display", "none");

            movieChoicePriceUnit.css("display", "none");
            movieChoicePrice.css("display", "none");
            btnOk.root_div.css("display", "none");
            btnCancel.root_div.css("display", "none");

            btnJoinDiv.css("display" , "none");
        }


        function _focusMovieChoicePPVPurchase(focusIndex) {
            if(focusIndex === 0) {
                passwordInputBox.attr("src" , "images/miniepg/pw_box_w300_foc.png");
                passwordInputBox.css({opacity: 1});
                textInputBox.css({opacity: 1});

                btnOk.default_btn.css("display" , "");
                btnOk.default_text.css("display" , "");

                btnOk.focus_btn.css("display" , "none");
                btnOk.focus_text.css("display" , "none");

                btnCancel.default_btn.css("display" , "");
                btnCancel.default_text.css("display" , "");

                btnCancel.focus_btn.css("display" , "none");
                btnCancel.focus_text.css("display" , "none");
                _startSpeechRecognizer(2);
            }else if(focusIndex === 1) {
                passwordInputBox.attr("src" , "images/miniepg/pw_box_w300.png");
                passwordInputBox.css({opacity: 0.3});

                btnOk.default_btn.css("display" , "none");
                btnOk.default_text.css("display" , "none");

                btnOk.focus_btn.css("display" , "");
                btnOk.focus_text.css("display" , "");

                btnCancel.default_btn.css("display" , "");
                btnCancel.default_text.css("display" , "");

                btnCancel.focus_btn.css("display" , "none");
                btnCancel.focus_text.css("display" , "none");
                _stopSpeechRecognizer();
            }else if(focusIndex === 2) {
                passwordInputBox.attr("src" , "images/miniepg/pw_box_w300.png");
                passwordInputBox.css({opacity: 0.3});

                btnOk.default_btn.css("display" , "");
                btnOk.default_text.css("display" , "");

                btnOk.focus_btn.css("display" , "none");
                btnOk.focus_text.css("display" , "none");

                btnCancel.default_btn.css("display" , "none");
                btnCancel.default_text.css("display" , "none");

                btnCancel.focus_btn.css("display" , "");
                btnCancel.focus_text.css("display" , "");
                _stopSpeechRecognizer();
            }
        }


        /**
         * "프로그램 정보를 가져오는 중입니다" 문구 노출
         */
        function _updateMovieChoicePPVPurchaseFail(failType) {
            channelInfoDiv.removeClass("moveTop");
            if(failType === KTW.ui.layer.MiniEpgLayer.DEF.PURCHASE_TYPE.TIME_OVER) {
                textCurrentProgramName_1.text('죄송합니다. 주문 가능한 시간이 지났습니다');
            }else {
                textCurrentProgramName_1.text('죄송합니다. 지금 리모콘 주문이 되지 않습니다');

            }
            textCurrentProgramName_2.text("");

            textCurrentProgramName_1.css('font-size', 36);
            programFontSize = 36;
            textCurrentProgramName_1.css("display", "");
            textCurrentProgramName_2.css("display", "");

            imgCurrentProgramAge.css("display", "none");
            textNextProgramName.css("display", "none");
            imgNextProgramReservationIcon.css("display", "none");

            btnJoinDiv.css("display" , "none");
        }

        function _restoreMoviceChoicePPVPurchase() {
            parentView.stopMovieChoicePPVPurchase();

            BuySkyCh.init(true);
        }



        /**
         * Password Input Box GUI 업데이트 (초기화 및 입력된 Key 갯수만큼 화면에 * 표시함)
         */
        function _updateInputBox(password) {
            log.printDbg("updateInputBox()");
            if(password !== undefined && password !== null) {
                if(password.length >0) {
                    var inputText = "";
                    for(var i=0;i<password.length;i++) {
                        inputText+="*";
                        if(i !== (password.length-1)) {
                            inputText += " ";
                        }

                        textInputBox.text(inputText);
                    }
                }else {
                    textInputBox.text("");
                }
            }
        }

        /**
         * 시청 시간/ 연령 제한 iframe 떠있는 상태에서
         * 비밀번호 인증 프로세스 시작
         * */
        function _inputNum(num , passwordLength) {
            log.printDbg('_inputNum() num : ' + num);
            var authManager = KTW.managers.auth.AuthManager;

            if(passwordLength === undefined || passwordLength === null) {
                passwordLength = max_passwd_length;
            }
            if (authManager.inputPW(num, passwordLength) === true) {
                _updateInputBox(authManager.getPW());
                _stopSpeechRecognizer();
                setTimeout(function(){
                    _checkPW();
                }, 10);
            }
            else {
                log.printDbg('_inputNum() authManager.inputPW() return false');
                _updateInputBox(authManager.getPW());
            }
        }

        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum() {
            log.printDbg('_deleteNum() ');
            var authManager = KTW.managers.auth.AuthManager;

            authManager.deletePW();
            _updateInputBox(authManager.getPW());
        }

        /**
         * 입력한 비밀번호 일치여부 확인
         */
        function _checkPW() {
            var authManager = KTW.managers.auth.AuthManager;

            log.printDbg('_checkPW() ');

            var auth_type = (KTW.CONSTANT.IS_OTS === true && Number(KTW.SMARTCARD_ID) === 0) ?
                authManager.AUTH_TYPE.AUTH_ADULT_PIN_FOR_MINIEPG : authManager.AUTH_TYPE.AUTH_ADULT_PIN;
            var obj = {
                type : auth_type,
                callback : callbackFuncAuthResult,
                loading : {
                    type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock : true,
                    callback : null,
                    on_off : false
                }
            };

            /**
             * jjh1117 2016.11.12
             * Loading이 돌때 키 입력을 막아야 한다 이 부분 공유 해야함.
             */

            authManager.checkUserPW(obj);
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
            // mode = KTW.oipf.Def.SR_MODE.PIN;
            // type = "recognized";
            // input = "000";
            if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                KTW.managers.auth.AuthManager.initPW();
                _updateInputBox("");

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>4) {
                        length = 4;
                    }

                    for(var i=0;i<length;i++) {
                        if(inputPinType === 1) {
                            _inputNum(input[i] , length);
                        }else if(inputPinType === 2) {
                            BuySkyCh.inputNum(input[i] , length);
                        }
                    }
                }
            }
        }

        /**
         * AuthManager로 부터 인증 결과 Callback 함수
         */
        function callbackFuncAuthResult(result) {
            var authManager = KTW.managers.auth.AuthManager;
            var navAdapter = KTW.oipf.AdapterHandler.navAdapter;

            log.printInfo('callbackFuncAuthResult() result: ' + result);
            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS) {

                _updateInputBox("");

                var password = authManager.getPW();

                if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                    /**
                     * 제한채널 해제는 releaseParentalRating() 호출하면
                     * MW단에서 자연스럽게 channel tune 이후 channel event가 올라오기 때문에
                     * 별도로 channel tune 처리하지 않는다.
                     */
                    navAdapter.releaseChannelBlock();
                    KTW.oipf.AdapterHandler.casAdapter.releaseParentalRating(password);
                    authManager.initPW();
                    authManager.resetPWCheckCount();
                    _stopSpeechRecognizer();
                }
                else if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {

                    var is_sky_channel = false;
                    if (ch.idType === Channel.ID_DVB_S ||
                        ch.idType === Channel.ID_DVB_S2) {
                        is_sky_channel = true;
                    }

                    if (is_sky_channel === true) {
                        // sky channel의 경우 mmi 통신을 통해 시청연령제한 해제한다.
                        navAdapter.releaseChannelBlock();
                        KTW.ca.MmiHandler.releaseParentalRating(password);
                    }
                    else {
                        /**
                         * kt cas의 경우 시청연령제한 해제 후 cas response 를 받으면
                         * 현재채널을 다시 tune 처리해야 함
                         */
                        KTW.ca.CaHandler.requestCaMessage(KTW.ca.CaHandler.TAG.REQ_PARENTAL_RATING_LOCK,
                            { pin : password, lock : false }, callbackFuncCaResponse);
                    }
                    _stopSpeechRecognizer();
                }
            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL ||
                result === authManager.RESPONSE.CANCEL ||
                result === authManager.RESPONSE.GOTO_INIT) {
                authManager.initPW();

                setTimeout(function(){
                    _updateInputBox(authManager.getPW());
    			}, 50);

                _startSpeechRecognizer(1);

                if(authManager.getPWCheckCount()<3) {
                    textCurrentProgramName_1.text('비밀번호가 일치하지 않습니다. 다시 입력해 주세요');
                }
            } else if (result === authManager.RESPONSE.DISCONNECTED_NETWORK) {
                _startSpeechRecognizer(1);
                KTW.managers.service.SimpleMessageManager.showPasswordCheckNetworkErrorNotiMessage();
            }
        }

        function callbackFuncCaResponse() {
            var authManager = KTW.managers.auth.AuthManager;
            var navAdapter = KTW.oipf.AdapterHandler.navAdapter;

            log.printDbg('callbackFuncCaResponse() ');
            navAdapter.changeChannel(ch, false, true);
            KTW.ca.CaHandler.setParentalRatingUnlockTime();

            authManager.initPW();
            authManager.resetPWCheckCount();

            /**
             * TODO 이 부분 추후 처리 되어야 함.
             * ischanneltune를 MainView로 전달 되어야 함.
             */
            // ischanneltune = true;
        }


        /**
         * 구매내역 저장 안된 skyChoice 구매 컨텐츠 정보 update
         */
        var skyBuyList = [];
        function _callbackSearchHistory(result, data) {
            // 성공
            if (result && data.custPpvList.length > 0 && (data.custPpvList[0].rsltCd === "00" || data.custPpvList[0].rsltCd === "")) {
                // 우선 구매목록을 저장해놓고,
                // 현재 재생중인 PPV 컨텐츠 정보를 얻어와서 구매목록에 있는지 확인한다
                skyBuyList = data.custPpvList;
                var curChannel = mainVboChannel;
                KTW.managers.http.oppvManager.searchOppInfo(_callbackSearchOppInfo, curChannel.majorChannel, KTW.SMARTCARD_ID);
            }
        }

        function _callbackSearchOppInfo(result, data) {
            // 성공
            if(result && (data.rsltCd == "01" || data.rsltCd == "02" ||
                data.rsltCd == "03" || data.rsltCd == "04" ||
                data.rsltCd == "05")){
                // 구매목록에 현재 재생중인 PPV 정보가 있으면 storage 구매목록에 컨텐츠 추가하고 내용 update
                for (var i=0; i<skyBuyList.length; i++) {
                    if (skyBuyList[i].ppvSvcId == data.ppvSvcId) {
                        KTW.utils.epgUtil.saveSkyChoiceData(data);
                        BuySkyCh.init();
                        var curChannel = mainVboChannel;
                        if (data.chlNo == curChannel.majorChannel) {

                            parentView.getRightBottomView().hideOKKeyIcon();
                        }
                        break;
                    }
                }
            }
            skyBuyList = [];
        }



        /***************************************************
         * 미가입 채널 가입 프로세스 시작
         ***************************************************/


        function _purchaseChannel(is_dvb_ch) {
            if (ch === null) {
                ch = mainVboChannel;
            }
            //parentView.pauseTimer();

            if (is_dvb_ch === true) {
                if (otsPurchaseStep === 0) {
                    cpmsInfo = null;
                    KTW.managers.module.ModuleManager.getModuleForced(
                        KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT,
                        function(channelPaymentModule) {
                            //log.printDbg("actTrigger$getModuleForced(), vcId = " + local_eed.vcId + ", vaId = " + local_eed.vaId + ", linkType = " + local_eed.linkType);
                            if (channelPaymentModule) {
                                //KTW.ui.LayerManager.startLoading();
                                channelPaymentModule.execute({
                                    method: "getCPMSSearchInfo",
                                    params: {
                                        ch: ch,
                                        callback : callbackFuncCPMSInfo,
                                        smart_card_id : KTW.SMARTCARD_ID
                                    }
                                });
                            } else {
                                // TODO 2017.01.12 something error....how to?
                                //parentView.resumeTimer();
                            }
                        }
                    );
                }
                else if (otsPurchaseStep === 1) {
                    KTW.managers.module.ModuleManager.getModuleForced(
                        KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT,
                        function(channelPaymentModule) {
                            //log.printDbg("actTrigger$getModuleForced(), vcId = " + local_eed.vcId + ", vaId = " + local_eed.vaId + ", linkType = " + local_eed.linkType);
                            if (channelPaymentModule) {
                                cpmsInfo.detailIndex = otsPurchaseBtnIndex;
                                channelPaymentModule.execute({
                                    method: "showCPMSPurchasePopup",
                                    params: {
                                        cpmsInfo: cpmsInfo,
                                        callback : callbackFuncCPMSPurchasePopup
                                    }
                                });
                            } else {
                                // TODO 2017.01.12 something error....how to?
                                //parentView.resumeTimer();
                            }
                        }
                    );
                }
            }
            else {

                var isDVBChannel = false;
                if(ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S
                    || ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2) {
                    isDVBChannel = true;
                }else {
                    isDVBChannel = false;
                }

                KTW.managers.module.ModuleManager.getModuleForced(
                    KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT,
                    function(channelPaymentModule) {
                        //log.printDbg("actTrigger$getModuleForced(), vcId = " + local_eed.vcId + ", vaId = " + local_eed.vaId + ", linkType = " + local_eed.linkType);
                        if (channelPaymentModule) {

                            channelPaymentModule.execute({
                                method: "getChannelInfo",
                                params: {
                                    ch: ch,
                                    is_dvb_channel : isDVBChannel,
                                    callback : callbackFuncGetChannelInfo
                                }
                            });
                        } else {
                            // TODO 2017.01.12 something error....how to?
                            //parentView.resumeTimer();
                        }
                    }
                );
            }
        }

        /**
         * IPTV 부가상품 및 업셀링 가입 완료 callbackfunction
         */
        function callbackFuncGetChannelInfo(ret , isUpselling) {
            if(ret === true) {
                if(KTW.CONSTANT.IS_OTS === true) {

                }else {
                    if(isUpselling === false) {
                        /**
                         * 2017/05/26 jjh1117
                         * TODO POPUP 노출 해야 함.
                         */

                        KTW.ui.LayerManager.activateLayer({
                            obj: {
                                id: "ChannelJoinProcessingPopup",
                                type: KTW.ui.Layer.TYPE.NORMAL,
                                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                view : KTW.ui.view.popup.ChannelJoinProcessingPopup,
                                params: {
                                    data : {
                                    }
                                }
                            },
                            visible: true
                        });

                    }
                }
                KTW.managers.service.ChannelPreviewManager.stop(true);
            }
        }

        function callbackFuncCPMSPurchasePopup(ret) {
            if(ret === true) {
                //parentView.resumeTimer();
            }
        }

        function callbackFuncCPMSInfo(info) {
            if(info !== undefined &&info !== null) {
                cpmsInfo = info;
                otsPurchaseStep = 1;
                otsPurchaseBtnIndex = 0;
                parentView.showOTSCPMSInfo(cpmsInfo , otsPurchaseBtnIndex);
            }else {
                //parentView.resumeTimer();
            }
        }


        function _showErrorPopup(descArray) {
            var isCalled = false;
            var popupData = {
                arrMessage: [{
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                    message: ["안내"],
                    cssObj: {}
                }],
                arrButton: [{id: "ok", name: "확인"}],
                cbAction: function (buttonId) {
                    if (!isCalled) {
                        isCalled = true;
                        _callbackFuncMiniEpgTopProgramViewErrorPopup(buttonId);
                    }
                }
            };

            if(descArray !== undefined && descArray !== null && descArray.length>0) {
                popupData.arrMessage.push({
                    type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_45,
                    message: descArray,
                    cssObj: {}
                });
            }else {
                return;
            }

            KTW.ui.LayerManager.activateLayer({
                obj: {
                    id: MiniEpgTopProgamViewLayerId,
                    type: KTW.ui.Layer.TYPE.POPUP,
                    priority: KTW.ui.Layer.PRIORITY.POPUP,
                    view : KTW.ui.view.popup.BasicPopup,
                    params: {
                        data : popupData
                    }
                },
                visible: true,
                cbActivate: function () {}
            });
        }

        function _callbackFuncMiniEpgTopProgramViewErrorPopup(buttonId) {
            if (buttonId) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: MiniEpgTopProgamViewLayerId,
                    remove: true
                });
            }
        }

        /************************************************************************
         * skyChoice 구매 담당 모듈
         ************************************************************************/
        var BuySkyCh = (function() {
            //var oppvManager = KTW.managers.http.oppvManager;
            var authManager = KTW.managers.auth.AuthManager;

            var FOCUS_PURCHASE_FAIL_STEP = -1;
            var FOCUS_NON_MOVICE_CHOICE_CHANNEL = 0;
            var FOCUS_STEP1 = 1;
            var FOCUS_STEP2 = 2;
            var FOCUS_STEP3 = 3;
            var FOCUS_STEP4 = 4;
            var FOCUS_STEP5 = 5;
            /**
             * step
             * -1: 구매실패, 0: 일반채널(non skyChoice channel),
             * 	1: 구매, 	 2: 준비중입니다. 3: 비밀번호 확인/취소,	4: 구매 성공
             */
            var step = 0;
            /**
             * button index
             * step2) 0: password input, 1: 확인, 2: 취소
             */
            var btn_idx = 0;

            var skyChoiceData = null;
            var skyChoiceCustomInfo = null;

            var isSkyoppvCall = false;


            function callbackBuySkyChOnrecognized(type , input , mode) {
                log.printInfo('callbackBuySkyChOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
                // mode = KTW.oipf.Def.SR_MODE.PIN;
                // type = "recognized";
                // input = "000";
                if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                    KTW.managers.auth.AuthManager.initPW();
                    _updateInputBox("");

                    if(input !== undefined && input !== null && input.length>0) {
                        var length = input.length;
                        if(length>4) {
                            length = 4;
                        }

                        for(var i=0;i<length;i++) {
                            _inputNum(input[i] , length);
                        }
                    }
                }
            }

            function _init(is_sky_ch) {
                log.printDbg('_init() is_sky_ch: ' + is_sky_ch);
                step = is_sky_ch === true ? FOCUS_STEP1 : FOCUS_NON_MOVICE_CHOICE_CHANNEL;
                btn_idx = 0;
                skyChoiceData = null;
                skyChoiceCustomInfo = null;
                isSkyoppvCall = false;

                _stopSpeechRecognizer();
            }

            function _controlKey(key_code) {
                log.printDbg('_controlKey() key_code: ' + key_code);
                log.printDbg('_controlKey() step: ' + step + ', btn_idx: ' + btn_idx);
                var consume = false;

                switch (key_code) {
                    case KTW.KEY_CODE.UP :
                    case KTW.KEY_CODE.DOWN :
                        if(step === FOCUS_STEP1) {
                            consume = false;
                        }else {
                            consume = true;
                        }
                        break;
                    case KTW.KEY_CODE.LEFT :
                        if (step !== FOCUS_NON_MOVICE_CHOICE_CHANNEL && step === FOCUS_STEP3) {
                            _moveLeftRight(true);
                            consume = true;
                        }else if(isSkyoppvCall === true) {
                            consume = true;
                        }else if(step === FOCUS_STEP2) {
                            consume = true;
                        }else if(step === FOCUS_STEP1) {
                            consume = false;
                        }else {
                            consume = true;
                        }
                        break;
                    case KTW.KEY_CODE.RIGHT :
                        if (step !== FOCUS_NON_MOVICE_CHOICE_CHANNEL && step === FOCUS_STEP3) {
                            _moveLeftRight(false);
                            consume = true;
                        }else if(isSkyoppvCall === true) {
                            consume = true;
                        }else if(step === FOCUS_STEP2) {
                            consume = true;
                        }else if(step === FOCUS_STEP1) {
                            consume = false;
                        }else {
                            consume = true;
                        }
                        break;
                    case KTW.KEY_CODE.OK :
                        if(step >= FOCUS_STEP1) {
                            _keyOk();
                        }
                        consume = true;
                        break;
                    case KTW.KEY_CODE.BACK :
                        if (step === FOCUS_STEP3) {
                            _goBack();
                            consume = true;
                        }
                        break;
                    case KTW.KEY_CODE.EXIT :
                        if (step === FOCUS_STEP3) {
                            _goBack();
                            consume = true;
                        }else if(step === FOCUS_STEP5) {
                            consume = true;
                        }
                        break;
                    default :
                        // 구매핀 입력 단계에서 숫자 키 입력 시
                        // 비밀번호 입력창에서는 비밀번호 입력
                        // 포커스가 비밀번호 입력창이 아닌 버튼에 있더라도 DCA가 먹으면 안된다
                        // 관련이슈 [KTWHPTZOF-688]
                        if (step === FOCUS_STEP3) {
                            var num = util.keyCodeToNumber(key_code);
                            if (num >= 0) {
                                if (btn_idx === 0) {
                                    // input password
                                    _inputNum(num);
                                }
                                consume = true;
                            }
                        }
                        break;
                }
                log.printDbg('_controlKey() consume: ' + consume);
                return consume;
            }

            function _moveLeftRight(isLeft) {
                if (step === FOCUS_STEP3) {
                    if (isLeft === true) {
                        // 비밀번호 입력창에 입력된 비밀번호가 존재하는 경우 deleteNum
                        if (btn_idx === 0 && authManager.getPW().length > 0) {
                            _deleteNum();
                        }
                        else {
                            btn_idx--;
                            if (btn_idx < 0) {
                                btn_idx = 2;
                            }
                        }
                    }
                    else {
                        btn_idx++;
                        if (btn_idx > 2) {
                            btn_idx = 0;
                        }
                    }
                    _focusMovieChoicePPVPurchase(btn_idx);
                }else {
                    return;
                }
            }

            function _keyOk() {
                log.printDbg('_keyOk() step: ' + step);
                if (step === FOCUS_STEP1) {
                    var curChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();

                    log.printDbg('curChannel ccid : ' + curChannel.ccid + " , ch.ccid : " + ch.ccid);
                    if(ch.ccid === curChannel.ccid) {
                        parentView.startMovieChoicePPVPurchase();
                        //isskyoppvCall = true;
                        _updateMovieChoicePPVInfoSearching();
                        step = FOCUS_STEP2;
                        KTW.managers.module.ModuleManager.getModuleForced(
                            KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT,
                            function(channelPaymentModule) {
                                if (channelPaymentModule) {

                                    channelPaymentModule.execute({
                                        method: "searchOTSOppInfo",
                                        params: {
                                            callback : _callbackSearchOppInfo,
                                            ch_num: curChannel.majorChannel,
                                            smart_card_id : KTW.SMARTCARD_ID
                                        }
                                    });
                                } else {
                                    log.printDbg("JJH TESTcurChannel");
                                    // TODO 2017.01.12 something error....how to?
                                    step = -1;
                                    restoreTimer = setTimeout(_restoreMoviceChoicePPVPurchase, 3000);
                                }
                            }
                        );
                    }
                }
                else if (step === FOCUS_STEP3) {
                    // 확인
                    if (btn_idx === 0 || btn_idx === 1) {
                        _stopSpeechRecognizer();
                        _checkPW();
                    }
                    // 취소
                    else {
                        _goBack();
                    }
                }

            }

            function _goBack() {
                log.printDbg('_goBack()');
                parentView.stopMovieChoicePPVPurchase();
            }

            function _callbackSearchOppInfo(result, data) {
                log.printDbg('_callbackSearchOppInfo() result: ' + result + " , data : " + JSON.stringify(data));
                // sample parameters
                // result: true,
                // data: { "ppdSvcCloseDh":"","ppvSvcId":"",
                //         "chlNo":"","pgmNm":"","svcMthCd":"",
                //         "mstFileStatCd":"","svcOpenDh":"",
                //         "rsltMsg":"Fail certification",
                //         "scrbrNo\t":"","custNm":"",
                //         "svcCloseDh":"","scId":"",
                //         "etpsType":"","rsltCd":"70","rate":""},
                isSkyoppvCall = false;
                // success
                if(result && (data.rsltCd == "01" || data.rsltCd == "02" ||
                    data.rsltCd == "03" || data.rsltCd == "04" ||
                    data.rsltCd == "05")){
                    isSkyoppvCall = true;
                    /**
                     * jjh1117 2017.01.24
                     * Web 2.0 에서는 구매/취소 버튼에서 호출 하고 있었으나 이 부분이 빠졌으므로 해당 API를 STEP1에서
                     * STEP2로 이동시 강제로 호출 하도록 수정함.
                     */
                    skyChoiceData = data;

                    KTW.managers.module.ModuleManager.getModuleForced(
                        KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT,
                        function(channelPaymentModule) {
                            if (channelPaymentModule) {

                                channelPaymentModule.execute({
                                    method: "searchOTSContractInfo",
                                    params: {
                                        callback : _callbackContractInfo,
                                        smart_card_id : KTW.SMARTCARD_ID
                                    }
                                });
                            } else {
                                // TODO 2017.01.12 something error....how to?
                            }
                        }
                    );
                }
                // fail
                else {
                    step = -1;
                    if(data && (data.rsltCd == "11" || data.rsltCd == "14")){
                        _updateMovieChoicePPVPurchaseFail(KTW.ui.layer.MiniEpgLayer.DEF.PURCHASE_TYPE.TIME_OVER);
                    }else{
                        _updateMovieChoicePPVPurchaseFail(KTW.ui.layer.MiniEpgLayer.DEF.PURCHASE_TYPE.FAIL);
                    }
                    restoreTimer = setTimeout(_restoreMoviceChoicePPVPurchase, 3000);
                }
            }

            function _callbackContractInfo(result, data) {
                // sample parameters
                // result: true
                // data: {"ppdSvcCloseDh":"","ppvSvcId":"","chlNo":"","pgmNm":"","svcMthCd":"","mstFileStatCd":"","scrbrNo":"","svcOpenDh":"","rsltMsg":"실패","custNm":"","svcCloseDh":"","scId":"","etpsType":"","rsltCd":"10","rate":""}
                log.printDbg('_callbackContractInfo() result: ' + result);
                log.printDbg('_callbackContractInfo() data: ' + JSON.stringify(data));
                // success
                if (result === true && data !== undefined && data !== null && data.rsltCd === "00") {
                    skyChoiceCustomInfo = data;
                    authManager.initPW();
                    authManager.resetPWCheckCount();

                    step = FOCUS_STEP3;
                    btn_idx = 0;
                    var data = { price: util.formatComma(skyChoiceData.rate)};
                    _updateMovieChoicePPVPurchase(btn_idx , data);
                }
                // fail
                else {
                    var descArray = [ "선택된 VOD가 없습니다", "VOD를 먼저 선택해 주세요" ];
                    _showErrorPopup(descArray);
                }
            }

            function _inputNum(num , maxLength) {
                var passwardLength = max_passwd_length;
                if(maxLength !== undefined && maxLength !== null) {
                    passwardLength = maxLength;
                }
                if (authManager.inputPW(num, passwardLength) === true) {
                    _updateInputBox(authManager.getPW());
                    btn_idx = 1;
                    _focusMovieChoicePPVPurchase(btn_idx);
                }
                else {
                    _updateInputBox(authManager.getPW());
                }
            }

            /**
             * 입력한 비밀번호의 끝자리를 지운다
             */
            function _deleteNum() {
                authManager.deletePW();
                _updateInputBox(authManager.getPW());
            }

            function _checkPW() {
                if (KTW.managers.device.NetworkManager.isNetworkConnected() === true) {
                    _stopSpeechRecognizer();
                    // 비밀번호 확인하는 동안 키입력에 의한 동작을 막기 위해 step을 4로 변경
                    // (loading bar를 돌려 키입력을 1차적으로 막긴 하지만 loading bar가 풀리고 _checkPWListener()가 호출되기 전까지
                    // 짧은 시간차가 발생할 수 있으며, 이 때 키입력 시 동작하는 현상이 있어 이를 방지하려는 의도
                    // 비밀번호 인증 성공하면 step은 어차피 4이고
                    // 인증 실패하면 _checkPWListener()에서 step을 다시 3으로 변경하여 진행
                    step = FOCUS_STEP4;
                    var obj = {
                        type : authManager.AUTH_TYPE.AUTH_BUY_PIN,
                        callback : _checkPWListener,
                        loading : {
                            type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                            lock : true,
                            callback : null,
                            on_off : false
                        }
                    };
                    authManager.checkUserPW(obj);
                }
                else {
                    var descArray = ["(네트워크 접속 오류)", "죄송합니다" , "지금 리모콘 주문이 되지 않습니다", "잠시 후에 주문해 주세요", "문의 : 국번 없이 100번" ];
                    _showErrorPopup(descArray);
                }
            }

            function _checkPWListener(result) {
                log.printDbg('_checkPWListener() result: ' + result);
                authManager.initPW();
                if (result === authManager.RESPONSE.HDS_BUY_AUTH_SUCCESS) {
                    // 비밀번호 인증만 성공하면 미니가이드 문구를 구매성공 문구로 먼저 띄운다
                    // oppv 통신과정에서 실패하더라도 오류팝업 호출 후 다시 비밀번호 입력하도록 step3으로 돌아가면 된다
                    _stopSpeechRecognizer();

                    authManager.resetPWCheckCount();
                    _updateMovieChoicePPVPurchaseSuccess();
                    step = FOCUS_STEP5;
                    KTW.managers.module.ModuleManager.getModuleForced(
                        KTW.managers.module.Module.ID.MODULE_CHANNEL_PAYMENT,
                        function(channelPaymentModule) {
                            //log.printDbg("actTrigger$getModuleForced(), vcId = " + local_eed.vcId + ", vaId = " + local_eed.vaId + ", linkType = " + local_eed.linkType);
                            if (channelPaymentModule) {

                                channelPaymentModule.execute({
                                    method: "registOTSOppv",
                                    params: {
                                        callback : _purchaseProduct,
                                        smart_card_id : KTW.SMARTCARD_ID,
                                        ppvSvcId : skyChoiceData.ppvSvcId ,
                                        svcOpenDh : skyChoiceData.svcOpenDh,
                                        svcCloseDh : skyChoiceData.svcCloseDh ,
                                        usgAmt : skyChoiceData.rate,
                                        svcMthCd : skyChoiceData.svcMthCd,
                                        juminBizNo : "",
                                        telNo : "",
                                        etpsType : skyChoiceCustomInfo.etpsType,
                                        orderIpAddr : KTW.oipf.AdapterHandler.hwAdapter.networkInterface.getIpAddress(),
                                        parameters : skyChoiceData
                                    }
                                });
                            } else {
                                // TODO 2017.01.12 something error....how to?
                            }
                        }
                    );
                }
                else {
                    // 비밀번호 불일치
                    // show password incorrect simple message
                    if (authManager.getPWCheckCount() < 3) {
                        KTW.managers.service.SimpleMessageManager.showBuyPinCheckPasswordErrorNotiMessage();
                    }
                    step = FOCUS_STEP3;
                    btn_idx = 0;
                    authManager.initPW();

                    var data = { price: util.formatComma(skyChoiceData.rate)};

                    _updateMovieChoicePPVPurchase(btn_idx , data);
                }
            }

            function _purchaseProduct(result, data, params) {
                log.printDbg('_purchaseProduct() result: ' + result);
                log.printDbg('_purchaseProduct() data: ' + JSON.stringify(data));
                if (result === true) {
                    log.printDbg("_purchaseProduct() - data.rsltCd : " + data.rsltCd);
                    if(data.rsltCd == "00"){
                        // success to purchase the content
                        step = FOCUS_NON_MOVICE_CHOICE_CHANNEL;
                        KTW.utils.epgUtil.saveSkyChoiceData(params);
                        log.printDbg("_purchaseProduct() - cp : " + cp);
                        if(cp == null ) {
                            return;
                        }
                        log.printDbg("_purchaseProduct() - cp name : " + cp.name);

                        if (cp.name.indexOf("예고") > -1) {
                            //[hw.boo] 본편 이동은 본편 시작 시간에 맞춰 타이머 세팅하여 Tune
                            var is_set_feature = KTW.managers.service.ReservationManager.addFeaturePresentation(np);
                            if(is_set_feature) {
                                KTW.managers.service.SimpleMessageManager.showMovieChoicePPVStartNotiMessage();
                            }
                        }
                    }
                    else {
                        var descArray = ["죄송합니다" , "지금 리모콘 주문이 되지 않습니다", "잠시 후에 주문해 주세요", "문의 : 국번 없이 100번" ];
                        _showErrorPopup(descArray);
                        _startSpeechRecognizer(2);
                    }
                }
                else {
                    var descArray = ["(네트워크 접속 오류)", "죄송합니다" , "지금 리모콘 주문이 되지 않습니다", "잠시 후에 주문해 주세요", "문의 : 국번 없이 100번" ];
                    _showErrorPopup(descArray);
                    _startSpeechRecognizer(2);

                }
            }


            return {
                init: _init,
                controlKey: _controlKey,
                inputNum : _inputNum,
                isBuying: function() {
                    log.printDbg('isBuying() result: ' + step);
                    return step !== 0;
                },
                getStep: function() {
                    log.printDbg('getStep() result: ' + step);
                    return step;
                }

            };
        }());

    };
})();

