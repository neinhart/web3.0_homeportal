/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>RelatedContentView</code>
 *
 * 연관 컨텐츠 화면 뷰
 * @author jjh1117
 * @since 2017-01-16
 */

"use strict";

(function() {
    KTW.ui.view.RelatedContentView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        /**
         * jjh1117 2017/01/19
         * SMART STB 에서는 pagescroll animation를 하지 않도록 하기 위함.
         */
        var isShowTextAnimation = true;
        var isShowPageScrollAnimation = true;

        var parent = parent;
        var backgroundImage = null;
        var div = null;

        var textTitle = null;

        var mashupContentCount = 0;
        var mashupContentData = null;
        var relatedContentCount = 0;
        var relatedContentList = [];

        var posterRootDiv = null;
        var noResultDiv = null;

        var pageTotalCount = 0;
        var focusPageIndex = -1;

        /**
         * {
         *    page_root_div : pageDiv ,
         *    page_item_count : pagePosterAreaList.length,
         *    page_poster_area_list : pagePosterAreaList,
         *    page_content_info_area_list : pageContentInfoAreaList,
         *    page_poster_data_list : pagePosterDataList
         * }
         *
         */
        var pageInfoList = [];
        var posterIndex = 0;

        var relatedContentTitle = "연관 정보/VOD";
        var relatedCotentCategoryId = null;

        var isHideAction = false;

        var isFirstSlidingAnimation = false;
        var isSecondSlidingAnimation = false;
        var startLeft = -1 * KTW.CONSTANT.RESOLUTION.WIDTH;

        var mashup_start_application_msg = {
            from: KTW.CONSTANT.APP_ID.HOME,
            to: KTW.CONSTANT.APP_ID.MASHUP,
            method: KTW.managers.MessageManager.METHOD.MASHUP.REQ_START_APPLICATION,
            target_mashup_id : "",
            target_url : "",
            param : ""
        };

        function keyDown () {
            log.printDbg("keyDown()");

        }

        function keyUp () {
            log.printDbg("keyUp()");

        }

        function keyRight () {
            log.printDbg("keyRight()");

            if(isFirstSlidingAnimation === true || isSecondSlidingAnimation === true) {
                return;
            }

            var pageInfo = pageInfoList[focusPageIndex];
            if(pageInfo !== null) {
                pageInfo.page_poster_area_list[posterIndex].removeClass("focus");
                if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                    pageInfo.page_content_info_area_list[posterIndex].removeClass("focus");
                    var childDiv = pageInfo.page_text_box_list[posterIndex];
                    _resetTitleAnimation(childDiv);

                }
                $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).removeClass("focus");

                posterIndex++;
                if(posterIndex>=pageInfo.page_item_count){
                    posterIndex--;
                    if(pageTotalCount<=1) {
                        posterIndex = 0;
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                            var childDiv = pageInfo.page_text_box_list[posterIndex];
                            _setTitleAnimation(childDiv);
                        }
                        $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                        return;
                    }

                    isFirstSlidingAnimation = true;
                    isSecondSlidingAnimation = true;

                    var firstPageIndex = focusPageIndex;
                    var secondPageIndex = 0;
                    focusPageIndex++;
                    if(focusPageIndex>=pageTotalCount) {
                        focusPageIndex = 0;
                    }
                    secondPageIndex = focusPageIndex;
                    var firstPageInfo = pageInfoList[firstPageIndex];
                    var secondPageInfo = pageInfoList[secondPageIndex];

                    if(isShowPageScrollAnimation) {
                        startLeft = KTW.CONSTANT.RESOLUTION.WIDTH;
                        if(firstPageInfo.rear_dim_img !== null) {
                            startLeft = firstPageInfo.rear_img_left;
                            startLeft-= 79;
                        }
                        log.printDbg("keyRight() startLeft : " + startLeft);

                        secondPageInfo.page_root_div.css({left:startLeft});

                        if(secondPageInfo.front_dim_img !== null) {
                            secondPageInfo.front_dim_img.css("display" , "none");
                        }

                        if(secondPageInfo.rear_dim_img !== null) {
                            secondPageInfo.rear_dim_img.css("display" , "none");
                        }
                        secondPageInfo.page_root_div.css("display", "");

                        if(startLeft === KTW.CONSTANT.RESOLUTION.WIDTH) {
                            firstPageInfo.page_root_div.addClass("page_sliding_right_no_rear_img");
                            secondPageInfo.page_root_div.addClass("page_sliding_right_no_rear_img");
                        }else if(startLeft === 1722) {
                            firstPageInfo.page_root_div.addClass("page_sliding_right_default");
                            secondPageInfo.page_root_div.addClass("page_sliding_right_default");
                        }else if(startLeft === 1683) {
                            firstPageInfo.page_root_div.addClass("page_sliding_right_mash1");
                            secondPageInfo.page_root_div.addClass("page_sliding_right_mash1");
                        }else if(startLeft === 1654) {
                            firstPageInfo.page_root_div.addClass("page_sliding_right_mash2");
                            secondPageInfo.page_root_div.addClass("page_sliding_right_mash2");
                        }else if(startLeft === 1625) {
                            firstPageInfo.page_root_div.addClass("page_sliding_right_mash3");
                            secondPageInfo.page_root_div.addClass("page_sliding_right_mash3");
                        }

                        firstPageInfo.page_root_div.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {
                            log.printDbg("firstPageInfo.page_root_div  webkitTransitionEnd event");
                            if(startLeft === KTW.CONSTANT.RESOLUTION.WIDTH) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_right_no_rear_img");
                            }else if(startLeft === 1722) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_right_default");
                            }else if(startLeft === 1683) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_right_mash1");
                            }else if(startLeft === 1654) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_right_mash2");
                            }else if(startLeft === 1625) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_right_mash3");
                            }

                            firstPageInfo.page_root_div.off("webkitTransitionEnd");

                            isFirstSlidingAnimation = false;
                        });

                        secondPageInfo.page_root_div.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {
                            log.printDbg("secondPageInfo.page_root_div  webkitTransitionEnd event");
                            isSecondSlidingAnimation = false;

                            if(startLeft === KTW.CONSTANT.RESOLUTION.WIDTH) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_right_no_rear_img");
                            }else if(startLeft === 1722) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_right_default");
                            }else if(startLeft === 1683) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_right_mash1");
                            }else if(startLeft === 1654) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_right_mash2");
                            }else if(startLeft === 1625) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_right_mash3");
                            }

                            secondPageInfo.page_root_div.off("webkitTransitionEnd");

                            secondPageInfo.page_root_div.css({left:0});
                            firstPageInfo.page_root_div.css({left:KTW.CONSTANT.RESOLUTION.WIDTH});

                            if(secondPageInfo.front_dim_img !== null) {
                                secondPageInfo.front_dim_img.css("display" , "");
                            }

                            if(secondPageInfo.rear_dim_img !== null) {
                                secondPageInfo.rear_dim_img.css("display" , "");
                            }

                            /**
                             * 앞 반또막 Show 시켜야함.
                             */
                            posterIndex = 0;
                            pageInfo = pageInfoList[focusPageIndex];
                            pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                            if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                                pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                                var childDiv = pageInfo.page_text_box_list[posterIndex];
                                _setTitleAnimation(childDiv);
                            }
                            $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                        });

                    }else {
                        isFirstSlidingAnimation = false;
                        isSecondSlidingAnimation = false;

                        secondPageInfo.page_root_div.css({left:0});
                        firstPageInfo.page_root_div.css({left:KTW.CONSTANT.RESOLUTION.WIDTH});

                        if(secondPageInfo.front_dim_img !== null) {
                            secondPageInfo.front_dim_img.css("display" , "");
                        }

                        if(secondPageInfo.rear_dim_img !== null) {
                            secondPageInfo.rear_dim_img.css("display" , "");
                        }

                        /**
                         * 앞 반또막 Show 시켜야함.
                         */
                        posterIndex = 0;
                        pageInfo = pageInfoList[focusPageIndex];
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                            var childDiv = pageInfo.page_text_box_list[posterIndex];
                            _setTitleAnimation(childDiv);
                        }
                        $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                    }


                }else {
                    if(isFirstSlidingAnimation === false && isSecondSlidingAnimation === false) {
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                            var childDiv = pageInfo.page_text_box_list[posterIndex];
                            _setTitleAnimation(childDiv);
                        }
                        $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                    }
                }
            }
        }


        function keyLeft () {
            log.printDbg("keyLeft()");

            if(isFirstSlidingAnimation === true || isSecondSlidingAnimation === true) {
                /**
                 * Page 전환 애니메이션 처리 중
                 */
                return true;
            }

            if(focusPageIndex <0) {
                /**
                 * 첫번째 page의 맨 왼쪽 컨텐츠 인 경우에는
                 * 이전 화면으로 돌아감.
                 */
                return false;
            }

            var pageInfo = pageInfoList[focusPageIndex];
            if(pageInfo !== null) {
                pageInfo.page_poster_area_list[posterIndex].removeClass("focus");
                if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                    pageInfo.page_content_info_area_list[posterIndex].removeClass("focus");
                    var childDiv = pageInfo.page_text_box_list[posterIndex];
                    _resetTitleAnimation(childDiv);
                }


                $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).removeClass("focus");

                posterIndex--;
                if(posterIndex<0){
                    posterIndex = 0;

                    if(pageTotalCount<=1 || focusPageIndex === 0) {
                        /**
                         * 첫번째 page의 맨 왼쪽 컨텐츠 인 경우에는
                         * 이전 화면으로 돌아감.
                         */
                        return false;
                    }


                    isFirstSlidingAnimation = true;
                    isSecondSlidingAnimation = true;

                    var firstPageIndex = 0;
                    var secondPageIndex = focusPageIndex;

                    focusPageIndex--;
                    if(focusPageIndex<0) {
                        focusPageIndex = pageTotalCount-1;
                    }
                    firstPageIndex = focusPageIndex;

                    var firstPageInfo = pageInfoList[firstPageIndex];
                    var secondPageInfo = pageInfoList[secondPageIndex];

                    if(isShowPageScrollAnimation) {
                        startLeft = (-1 * KTW.CONSTANT.RESOLUTION.WIDTH);
                        if(firstPageInfo.rear_dim_img !== null) {
                            firstPageInfo.rear_dim_img.css("display" , "none");
                            startLeft = firstPageInfo.rear_img_left;
                            startLeft-= 79;

                            startLeft*= -1;
                        }
                        log.printDbg("keyLeft() startLeft : " + startLeft);

                        firstPageInfo.page_root_div.css({left:startLeft});

                        if(firstPageInfo.front_dim_img !== null) {
                            firstPageInfo.front_dim_img.css("display" , "none");
                        }

                        if(firstPageInfo.rear_dim_img !== null) {
                            firstPageInfo.rear_dim_img.css("display" , "none");
                        }

                        if(secondPageInfo.front_dim_img !== null) {
                            secondPageInfo.front_dim_img.css("display" , "none");
                        }

                        if(secondPageInfo.rear_dim_img !== null) {
                            secondPageInfo.rear_dim_img.css("display" , "none");
                        }

                        if(startLeft === (-1 * KTW.CONSTANT.RESOLUTION.WIDTH)){
                            firstPageInfo.page_root_div.addClass("page_sliding_left_no_rear_img");
                            secondPageInfo.page_root_div.addClass("page_sliding_left_no_rear_img");
                        }else if(startLeft === -1722) {
                            firstPageInfo.page_root_div.addClass("page_sliding_left_default");
                            secondPageInfo.page_root_div.addClass("page_sliding_left_default");
                        }else if(startLeft === -1683) {
                            firstPageInfo.page_root_div.addClass("page_sliding_left_mash1");
                            secondPageInfo.page_root_div.addClass("page_sliding_left_mash1");
                        }else if(startLeft === -1654) {
                            firstPageInfo.page_root_div.addClass("page_sliding_left_mash2");
                            secondPageInfo.page_root_div.addClass("page_sliding_left_mash2");
                        }else if(startLeft === -1625) {
                            firstPageInfo.page_root_div.addClass("page_sliding_left_mash3");
                            secondPageInfo.page_root_div.addClass("page_sliding_left_mash3");
                        }



                        firstPageInfo.page_root_div.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {

                            if(startLeft === (-1 * KTW.CONSTANT.RESOLUTION.WIDTH)){
                                firstPageInfo.page_root_div.removeClass("page_sliding_left_no_rear_img");
                            }else if(startLeft === -1722) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_left_default");
                            }else if(startLeft === -1683) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_left_mash1");
                            }else if(startLeft === -1654) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_left_mash2");
                            }else if(startLeft === -1625) {
                                firstPageInfo.page_root_div.removeClass("page_sliding_left_mash3");
                            }

                            firstPageInfo.page_root_div.off("webkitTransitionEnd");

                            isFirstSlidingAnimation = false;

                            firstPageInfo.page_root_div.css({left:0});

                            if(firstPageInfo.front_dim_img !== null) {
                                firstPageInfo.front_dim_img.css("display" , "");
                            }

                            if(firstPageInfo.rear_dim_img !== null) {
                                firstPageInfo.rear_dim_img.css("display" , "");
                            }

                            // /**
                            //  * 앞 반또막 Show 시켜야함.
                            //  */
                            pageInfo = pageInfoList[focusPageIndex];
                            posterIndex=pageInfo.page_item_count - 1;
                            pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                            if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                                pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                                var childDiv = pageInfo.page_text_box_list[posterIndex];
                                _setTitleAnimation(childDiv);
                            }
                            $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");

                        });

                        secondPageInfo.page_root_div.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {
                            if(startLeft === (-1 * KTW.CONSTANT.RESOLUTION.WIDTH)){
                                secondPageInfo.page_root_div.removeClass("page_sliding_left_no_rear_img");
                            }else if(startLeft === -1722) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_left_default");
                            }else if(startLeft === -1683) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_left_mash1");
                            }else if(startLeft === -1654) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_left_mash2");
                            }else if(startLeft === -1625) {
                                secondPageInfo.page_root_div.removeClass("page_sliding_left_mash3");
                            }


                            secondPageInfo.page_root_div.off("webkitTransitionEnd");
                            secondPageInfo.page_root_div.css({left:KTW.CONSTANT.RESOLUTION.WIDTH});
                            isSecondSlidingAnimation = false;
                        });
                    }else {
                        isFirstSlidingAnimation = false;
                        isSecondSlidingAnimation = false;

                        firstPageInfo.page_root_div.css({left:0});
                        secondPageInfo.page_root_div.css({left:KTW.CONSTANT.RESOLUTION.WIDTH});

                        if(firstPageInfo.front_dim_img !== null) {
                            firstPageInfo.front_dim_img.css("display" , "");
                        }

                        if(firstPageInfo.rear_dim_img !== null) {
                            firstPageInfo.rear_dim_img.css("display" , "");
                        }


                        pageInfo = pageInfoList[focusPageIndex];
                        posterIndex=pageInfo.page_item_count - 1;
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                            var childDiv = pageInfo.page_text_box_list[posterIndex];
                            _setTitleAnimation(childDiv);
                        }
                        $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                    }




                }else {
                    if(isFirstSlidingAnimation === false && isSecondSlidingAnimation === false) {
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                            var childDiv = pageInfo.page_text_box_list[posterIndex];
                            _setTitleAnimation(childDiv);
                        }

                        $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");

                    }
                }
            }

            /**
             * 포커스 이동
             */
            return true;
        }


        function _setTitleAnimation (childDiv) {
            log.printDbg("_setTitleAnimation()");

            if(isShowTextAnimation) {
                var findDiv = childDiv.find("span");
                findDiv.removeClass("line_1");
                findDiv.removeClass("cut");
                findDiv.removeClass("font_l");
                findDiv.addClass("font_m");

                util.clearAnimation(findDiv);
                findDiv.addClass("ani");

                util.startTextAnimation({
                    targetBox: childDiv
                    //speed: 125,
                    //delayTime: 1
                });
            }
        }

        function _resetTitleAnimation (childDiv) {
            log.printDbg("_resetTitleAnimation()");
            if(isShowTextAnimation) {
                var findDiv = childDiv.find("span");
                util.clearAnimation(findDiv);

                findDiv.addClass("cut");
                findDiv.removeClass("font_m");
                findDiv.addClass("font_l");

                findDiv.addClass("line_1");
                findDiv.removeClass("ani");
            }
        }


        function keyOk () {
            log.printDbg("keyOk()");
            var pageInfo = null;
            if(pageInfoList !== null && pageInfoList.length>0 && focusPageIndex < pageInfoList.length) {
                pageInfo = pageInfoList[focusPageIndex];
                if(pageInfo !== null && pageInfo.page_poster_data_list !== undefined &&
                    pageInfo.page_poster_data_list !== null && pageInfo.page_poster_data_list.length>0 &&
                    posterIndex < pageInfo.page_poster_data_list.length ) {
                    var posterdata = pageInfo.page_poster_data_list[posterIndex];
                    if(posterdata !== undefined && posterdata !== null) {
                        log.printDbg("posterData == " +posterdata.data_type);
                        if(posterdata.data_type === 0) {
                            var mashdata = posterdata.data;
                            if(mashdata !== undefined && mashdata !== null) {
                                var contentType = mashdata.type;
                                var contentInfo = mashdata.data;
                                if(contentType === "2") {
                                    /**
                                     * APP 실행
                                     */
                                    var appType = contentInfo.type;
                                    var appData = contentInfo.data;
                                    var appParam = contentInfo.param;
                                    log.printDbg("keyOk() appType : " + appType + " , appData : " + appData + " , appPram : " + appParam);

                                    KTW.ui.LayerManager.deactivateLayer({
                                        id: KTW.ui.Layer.ID.MINI_EPG
                                    });

                                    var obj = {};
                                    if(appType !== null) {
                                        if(appType === "1") {
                                            obj.type =KTW.CONSTANT.APP_TYPE.UNICAST;
                                            obj.param = appData;

                                            KTW.managers.service.AppServiceManager.changeService({
                                                nextServiceState: KTW.CONSTANT.SERVICE_STATE.OTHER_APP,
                                                obj: obj
                                            });

                                        }else if(appType === "2" || appType === "3") {
                                            mashup_start_application_msg.target_mashup_id = "";
                                            mashup_start_application_msg.target_url = "";
                                            mashup_start_application_msg.param = "";

                                            if(appType === "2") {
                                                mashup_start_application_msg.target_url = appData;
                                                mashup_start_application_msg.param = appParam;
                                            }else {
                                                mashup_start_application_msg.target_mashup_id = appData;
                                                mashup_start_application_msg.param = appParam;
                                            }
                                            KTW.managers.MessageManager.sendMessage(mashup_start_application_msg , _callbackMashupMsg);

                                        }else if(appType === "4") {
                                            var ch = KTW.oipf.AdapterHandler.navAdapter.getChannelBySID(Number(appData));
                                            if(ch !== null) {
                                                KTW.oipf.AdapterHandler.navAdapter.changeChannel(ch);
                                            }
                                        }
                                    }
                                }else if(contentType === "1" || contentType === "3" ) {
                                    var aid = contentInfo.aid;
                                    var cid = contentInfo.cid;
                                    var act = contentInfo.act;
                                    var req_path_cd = "20";


                                    if(act === "4") {
                                        //통계로그 추가
                                        var targetMenu = null;

                                        targetMenu = KTW.managers.data.MenuDataManager.searchMenu({
                                            menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                                            cbCondition: function (tmpMenu) {
                                                if (tmpMenu.id === message.cat_id) {
                                                    return true;
                                                }
                                            }
                                        })[0];

                                        if (targetMenu) {
                                            // var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenuId(message.cat_id);
                                            var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(targetMenu);

                                            if (moduleId) {
                                                KTW.managers.service.MenuServiceManager.jumpMenu({
                                                    moduleId: moduleId,
                                                    menu: targetMenu,
                                                    jump: true,
                                                    updateSubTitle: false,  // 설정 메뉴인 경우를 위해 추가
                                                    cbJumpMenu: function () {

                                                    }
                                                });
                                                return;
                                            }
                                            //postMessage를 통한 Jump
                                            KTW.managers.UserLogManager.collect({
                                                type: KTW.data.UserLog.TYPE.JUMP_TO,
                                                act: KTW.data.EntryLog.JUMP.CODE.RELATED,
                                                jumpType: KTW.data.EntryLog.JUMP.TYPE.CATEGORY,
                                                catId: cid,
                                                contsId: aid,
                                                reqPathCd: req_path_cd
                                            });

                                            KTW.ui.LayerManager.deactivateLayer({
                                                id: KTW.ui.Layer.ID.MINI_EPG
                                            });
                                        }
                                        else {
                                            showJumpCategoryFailPopup();
                                        }


                                    }else {
                                        var methodName = "";

                                        methodName = "showDetail";

                                        if(act === "3") {
                                            methodName = "watchContent";
                                        }

                                        KTW.ui.LayerManager.deactivateLayer({
                                            id: KTW.ui.Layer.ID.MINI_EPG
                                        });

                                        KTW.managers.module.ModuleManager.getModuleForced(
                                            KTW.managers.module.Module.ID.MODULE_VOD,
                                            function(vodModule) {
                                                if (vodModule) {
                                                    vodModule.execute({
                                                        method: methodName,
                                                        params: {
                                                            callback: _callbackFuncShowVodDetail,
                                                            cat_id: cid,
                                                            const_id: aid,
                                                            req_cd: req_path_cd
                                                        }
                                                    });
                                                } else {
                                                    // TODO 2017.01.12 something error....how to?
                                                }
                                            }
                                        );

                                        //통계로그 추가
                                        KTW.managers.UserLogManager.collect({
                                            type: KTW.data.UserLog.TYPE.JUMP_TO,
                                            act: KTW.data.EntryLog.JUMP.CODE.RELATED,
                                            jumpType: KTW.data.EntryLog.JUMP.TYPE.VOD_DETAIL,
                                            catId: cid,
                                            contsId: aid,
                                            reqPathCd: req_path_cd
                                        });
                                    }

                                }

                            }

                        }else if(posterdata.data_type === 1) {
                            KTW.ui.LayerManager.deactivateLayer({
                                id: KTW.ui.Layer.ID.MINI_EPG
                            });

                            KTW.managers.module.ModuleManager.getModuleForced(
                                KTW.managers.module.Module.ID.MODULE_VOD,
                                function(vodModule) {
                                    var rcData = posterdata.data;
                                    var itemType = rcData.ITEM_TYPE;
                                    var req_path_cd = "20";

                                    var parentCategoryID = "";
                                    var itemID = "";
                                    /**
                                     *  1 : 시리즈 카테고리 , 2 : 일반 컨텐츠 , 4 : 시리즈 회차
                                     */
                                    if(itemType === "1") {
                                        parentCategoryID = rcData.ITEM_ID;
                                    }else {
                                        parentCategoryID = rcData.PARENT_CAT_ID;
                                        itemID = rcData.ITEM_ID;
                                    }
                                    log.printDbg("RelatedContentView() select RCVOD = " + JSON.stringify(rcData));
                                    log.printDbg("RelatedContentView() select ShowDetail cat_id : " + parentCategoryID + " , itemID : " + itemID + " , req_path_cd : " + req_path_cd);
                                    if (vodModule) {
                                        vodModule.execute({
                                            method: "showDetail",
                                            params: {
                                                callback: _callbackFuncShowVodDetail,
                                                cat_id: parentCategoryID,
                                                const_id: itemID,
                                                req_cd: req_path_cd
                                            }
                                        });
                                        //통계로그 추가
                                        KTW.managers.UserLogManager.collect({
                                            type: KTW.data.UserLog.TYPE.JUMP_TO,
                                            act: KTW.data.EntryLog.JUMP.CODE.RELATED,
                                            jumpType: KTW.data.EntryLog.JUMP.TYPE.VOD_DETAIL,
                                            catId: parentCategoryID,
                                            contsId: itemID,
                                            reqPathCd: req_path_cd
                                        });
                                    } else {
                                        // TODO 2017.01.12 something error....how to?
                                    }
                                }
                            );
                        }
                    }
                }
            }


        }

        function showJumpCategoryFailPopup(msg) {
            if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                // 키즈모드일 때 안내 팝업 실행
                KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup(msg);
            } else {
                // 키즈모드가 아닐때 안내 팝업 실행
                log.printDbg("showJumpCategoryFailPopup(), normal mode");
                var isCalled = false;
                var popupData = {
                    arrMessage: [{
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.TITLE,
                        message: ["알림"],
                        cssObj: {}
                    }, {
                        type: KTW.ui.view.popup.BasicPopup.MSG_TYPE.MSG_38,
                        message: KTW.ERROR.CODE.E089,
                        cssObj: {}
                    }],
                    isShowSaid : true,
                    arrButton: [{id: "ok", name: "확인"}],
                    cbAction: function (buttonId) {
                        if (!isCalled) {
                            isCalled = true;

                            if (buttonId) {
                                KTW.ui.LayerManager.deactivateLayer({
                                    id: "CannotJumpCategoryPopup",
                                    remove: true
                                });
                            }
                        }
                    }
                };

                KTW.ui.LayerManager.activateLayer({
                    obj: {
                        id: "CannotJumpCategoryPopup",
                        type: KTW.ui.Layer.TYPE.POPUP,
                        priority: KTW.ui.Layer.PRIORITY.POPUP,
                        view : KTW.ui.view.popup.BasicPopup,
                        params: {
                            data : popupData
                        }
                    },
                    visible: true,
                    cbActivate: function () {}
                });
                //util.showErrorPopup(KTW.ERROR.CODE.E089.split("\n"), null, null, true);
            }
        }

        function _callbackFuncShowVodDetail(ret) {
            log.printDbg("callbackFuncShowVodDetail() ret : " + ret);

        }

        function keyBack () {
            log.printDbg("keyBack()");
        }


        this.create = function () {
            log.printDbg("create()");

            /**
             * left : -370으로 지정한 이유는 연관 정보/VOD 컨텐츠 노출 애니메이션이
             * 좌측에서 우측으로 이동하는 애니메이션이며
             * 애니메이션 구간을 짧게 해 달라는 요구 사항
             */

            backgroundImage = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_content_background",
                    src: "images/miniepg/related_poster_dim_bg.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 235,
                        width: KTW.CONSTANT.RESOLUTION.WIDTH,
                        height: 845
                        ,display:"none"
                    }
                },
                parent: parent.div
            });

            div = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "epgRelatedContent",
                    css: {
                        position: "absolute", left: -370, top: 235, width:KTW.CONSTANT.RESOLUTION.WIDTH, height: 845,display:"none"
                    }
                },
                parent: parent.div
            });




            var titleDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 117, top: 266, width: 780, height: 28
                    }
                },
                parent: div
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 3, width: 3, height: 24,
                        "background-color": "rgba(255,255,255,0.3)"
                    }
                },
                parent: titleDiv
            });

            textTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_b cut" ,
                    css: {
                        position: "absolute", left: 15, top: 0, width: 765 , height: 28,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 26,"text-align":"left"
                    }
                },
                text: relatedContentTitle,
                parent: titleDiv
            });

            posterRootDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 533-235, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 464
                    }
                },
                parent: div
            });

            noResultDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 533-235, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 464
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/icon/icon_noresult.png",
                    css: {
                        position: "absolute",
                        left: 919,
                        top: 194,
                        width: 82,
                        height: 76
                    }
                },
                parent: noResultDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 194 + 76 + 20, width: KTW.CONSTANT.RESOLUTION.WIDTH , height: 35,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 33,"text-align":"center" , "letter-spacing": -1.65
                    }
                },
                text: "연관 VOD가 없습니다",
                parent: noResultDiv
            });

            noResultDiv.css("display" , "none");
        };

        this.getDiv = function () {
            return div;
        };

        this.setData = function (title , categoryId , mashCount , mashData) {
            mashupContentCount = 0;
            mashupContentData = null;

            relatedContentTitle = title;
            relatedCotentCategoryId = categoryId;
            mashupContentCount = mashCount;


            mashupContentData = mashData;

            if(mashupContentData !== undefined && mashupContentData !== null) {
                if(mashupContentData.length !== mashupContentCount) {
                    mashupContentCount = mashupContentData.length;
                }
            }else {
                mashupContentCount = 0;
            }

            if(mashupContentCount > 3) {
                mashupContentCount = 3;
            }
        };

        this.show = function (options) {
            log.printDbg("show()");

            log.printDbg(options ? JSON.stringify(options) : "options is null");

            if (!options || !options.resume) {

            }
            isHideAction = false;
            pageTotalCount = 0;
            focusPageIndex = -1;
            pageInfoList = [];

            noResultDiv.css("display" , "none");
            posterRootDiv.children().remove();
            posterRootDiv.css("display" , "");

            textTitle.text(relatedContentTitle);

            relatedContentCount = 0;
            relatedContentList = [];

            if(relatedCotentCategoryId !== undefined && relatedCotentCategoryId !== null) {
                // 2017.06.26 dhlee
                // memory cache로 부터 획득하도록 수정
                //pkgCode : null;
                //var basePkgCode = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.PACKAGE_LIST_BASE);
                //var pkgCode = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.PACKAGE_LIST);

                var basePkgCode = KTW.managers.StorageManager.ms.load(KTW.managers.StorageManager.MEM_KEY.PKG_BASE_LIST);
                var pkgCode = KTW.managers.StorageManager.ms.load(KTW.managers.StorageManager.MEM_KEY.PKG_LIST);

                var totalpkgCode = "";
                if(basePkgCode !== undefined && basePkgCode !== null) {
                    totalpkgCode = basePkgCode;
                }

                if(pkgCode !== undefined && pkgCode !== null) {
                    if(totalpkgCode !== "") {
                        totalpkgCode+= ",";
                    }
                    totalpkgCode+= pkgCode;
                }

                log.printDbg("show() totalpkgCode : " + totalpkgCode);
                KTW.ui.LayerManager.startLoading({
                    preventKey: true
                });

                KTW.managers.http.VodRecmManager.getRelatedContents("LIVE_VOD", "M", KTW.SAID, totalpkgCode, "0", relatedCotentCategoryId , null, 30, callbackFuncRelatedContents);
            }else {
                _createPageElement();
                posterIndex = 0;

                var pageInfo = pageInfoList[focusPageIndex];
                if(pageInfo !== null) {

                    if(pageInfo.page_item_count>posterIndex) {
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");
                        }
                    }
                    pageInfo.page_root_div.css("display", "");
                    $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                }

                backgroundImage.css("display", "");
                div.css("display", "");

                div.addClass("show");
            }
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");

            _hideAll();
            isHideAction = true;
            div.removeClass("show");
            posterRootDiv.children().remove();

            backgroundImage.css("display", "none");
//            div.css("display", "none");
        };

        this.focus = function () {
            log.printDbg("focus()");

        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;
            if(isHideAction === true) {
                return consumed;
            }

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    keyUp();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    keyDown();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    keyRight();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    keyOk();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    /**
                     * Left 키 처리 여부를 return 받아 전달 함.
                     */
                    consumed = keyLeft();
                    //consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    keyBack();
                    consumed = true;
                    break;
            }

            return consumed;
        };


        function _hideAll() {
            log.printDbg("_hideAll()");

        }
        
        function _createPosterArea(relatedContentIndex , rootDiv , focusIndex) {
            // POSTER START
            var posterAreaDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "related_content_area_div",
                    class: "related_content_poster_area",
                    css: {
                        position: "absolute"
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_content_shw_left",
                    src: "images/miniepg/sdw_poster_h300_l.png",
                    class: "related_content_shw_left",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_content_shw_middle",
                    src: "images/miniepg/sdw_poster_h300.png",
                    class: "related_content_shw_middle",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_content_shw_right",
                    src: "images/miniepg/sdw_poster_h300_r.png",
                    class: "related_content_shw_right",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            });


            var posterimg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_content_img",
                    class: "related_content_img",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            }).error(function () {
                $(this).attr("src", "images/banner/default_recommend_mini_w210.png");
            }).attr("src", KTW.utils.util.getImageUrl(relatedContentList[relatedContentIndex].IMG_URL, KTW.CONSTANT.POSTER_TYPE.VERTICAL, 0, 0));



            var ishdr = "N";
            var isDVD = "N";
            var isHD = relatedContentList[relatedContentIndex].IS_HD;

            if(relatedContentList[relatedContentIndex].HDR_YN !== undefined &&
                relatedContentList[relatedContentIndex].HDR_YN !== null &&
                relatedContentList[relatedContentIndex].HDR_YN === "Y") {
                ishdr = "Y";
            }

            if(relatedContentList[relatedContentIndex].SMART_DVD_YN !== undefined &&
                relatedContentList[relatedContentIndex].SMART_DVD_YN !== null &&
                relatedContentList[relatedContentIndex].SMART_DVD_YN === "Y") {
                isDVD = "Y";
            }

            if(relatedContentList[relatedContentIndex].IS_HD !== undefined && relatedContentList[relatedContentIndex].IS_HD !== null) {
                isHD = relatedContentList[relatedContentIndex].IS_HD;
            }

            var obj = {
                "hdr" : ishdr,
                "resolCd" : isHD,
                "isDvdYn" : isDVD
            };

            var imglist = util.checkPosterResolutionIcon(obj);

            //imglist = ["HDR" , "DVD"];

            var iconCount = 0;
            for(var x=0;x<imglist.length;x++) {
                if(imglist[x] === "HDR") {
                    var lockimg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: iconCount === 0 ? "related_content_icon1" : "related_content_icon2",
                            src: "images/icon/tag_poster_hdr.png",
                            class: iconCount === 0 ? "related_content_icon1" : "related_content_icon2",
                            css:{
                                position: "absolute"
                            }
                        },
                        parent: posterAreaDiv
                    });
                }else if(imglist[x] === "UHD") {
                    var lockimg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: iconCount === 0 ? "related_content_icon1" : "related_content_icon2",
                            src: "images/icon/tag_poster_uhd.png",
                            class: iconCount === 0 ? "related_content_icon1" : "related_content_icon2",
                            css:{
                                position: "absolute"
                            }
                        },
                        parent: posterAreaDiv
                    });
                }else if(imglist[x] === "DVD") {
                    var lockimg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: iconCount === 0 ? "related_content_icon1" : "related_content_icon2",
                            src: "images/icon/tag_poster_mine.png",
                            class: iconCount === 0 ? "related_content_icon1" : "related_content_icon2",
                            css:{
                                position: "absolute"
                            }
                        },
                        parent: posterAreaDiv
                    });
                }
                iconCount++;

            }

            if(relatedContentList[relatedContentIndex].RATING !== undefined && relatedContentList[relatedContentIndex].RATING !== null) {
                if (util.isLimitAge(relatedContentList[relatedContentIndex].RATING)) {
                    var lockimg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "related_content_lock",
                            src: "images/img_vod_locked.png",
                            class: "related_content_lock",
                            css:{
                                position: "absolute"
                            }
                        },
                        parent: posterAreaDiv
                    });
                }
            }

            if(focusIndex !== undefined) {
                var tempdiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_line_" + focusIndex,
                        class: "focus_related_content_focus_line focus_red_border_box",
                        css: {
                            position: "absolute"
                        }
                    },
                    parent: rootDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        class: "focus_black_1px_border_box",
                        css: {
                            position: "absolute", left : 0 , top : 0 , width: (245-12), height: (345-12)
                        }
                    },
                    parent: tempdiv
                });
            }


            return posterAreaDiv;
        }
        
        function _addContentInfoArea(relatedContentIndex , rootDiv) {

            var contentObj = {};

            contentObj.contentInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "content_info_area",
                    class: "content_info_area",
                    css: {
                        position: "absolute",
                        display:""
                    }
                },
                parent: rootDiv
            });


            contentObj.textBox = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    class: "text_box",
                    css: {
                        position: "absolute"
                    }
                },
                parent: contentObj.contentInfoDiv
            });
            


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "title",
                    class: "content_title font_l cut line_1" ,
                    css: {
                        position: "absolute",
                        "font-size": 30,"text-align":"left"
                    }
                },
                text: relatedContentList[relatedContentIndex].ITEM_NAME,
                parent: contentObj.textBox
            });

            var starPointAgeIconDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    class: "star_point_age_icon" ,
                    css: {
                        position: "absolute",
                        display:""
                    }
                },
                parent: contentObj.contentInfoDiv
            });

            var starPointDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    class: "star_point" ,
                    css: {
                        position: "absolute",
                        display:""
                    }
                },
                parent: starPointAgeIconDiv
            });

            var star = parseFloat(relatedContentList[relatedContentIndex].OLLEH_RATING);
            var num = parseInt(star);
            var num2 = star * 10 % 10;
            var num3 = parseInt(num2 / 2) + num2 % 2;
            for (var ss = 0; ss < 5; ss++) {
                var starLeft = ss * 24;
                var starSrc = "";

                if (num >= ss + 1) {
                    starSrc = "images/starpoint/img_rating_filled_20.png";
                } else if (num < ss + 1 && star > ss) {
                    if (num2 === 9) {
                        starSrc = "images/starpoint/img_rating_filled_20.png"
                    } else {
                        starSrc = "images/starpoint/img_rating_half_20_" + num3 + ".png";
                    }
                } else {
                    starSrc = "images/starpoint/img_rating_empty_20.png";
                }
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: starSrc,
                        css: {
                            position: "absolute", left: starLeft, top: 0, width: 20, height: 18
                        }
                    },
                    parent: starPointDiv
                });
            }

            var buyAgeIconDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    class: "isbuy_age_icon" ,
                    css: {
                        position: "absolute",
                        display:""
                    }
                },
                parent: starPointAgeIconDiv
            });

            var left = 0;
            if(relatedContentList[relatedContentIndex].WON_YN  === "Y" || relatedContentList[relatedContentIndex].WON_YN  === "y") {
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/icon/icon_buy.png",
                        css: {
                            position: "absolute", left:0 , top : 0  , width: 32, height: 32
                        }
                    },
                    parent: buyAgeIconDiv
                });

                left = 36;
            }else if(relatedContentList[relatedContentIndex].WON_YN  === "N" || relatedContentList[relatedContentIndex].WON_YN  === "n") {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 0, top: 6, width: 50 , height: 25,
                            color: "rgba(255, 255, 255, 0.5)", "font-size": 23,"text-align":"left"
                        }
                    },
                    text: "무료",
                    parent: buyAgeIconDiv
                });
                left = 44;
            }else {
                left = 0;
            }



            var age = relatedContentList[relatedContentIndex].RATING;
            var fullimagepath = "";
            var imgname = "images/icon/icon_age_list_";
            if(age !== undefined && age !== null) {
                if(age === "0") {
                    imgname += "all" + ".png";
                }else {
                    if(age === "7" || age === "12" || age === "15" || age === "19") {
                        imgname += age + ".png";
                    }else {
                        imgname = "";
                    }
                }
                fullimagepath = imgname;
            }

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: fullimagepath,
                    css: {
                        position: "absolute",
                        left: left,
                        top: 0,
                        width: 32,
                        height: 32,
                        display: fullimagepath === "" ? "none" : ""
                    }
                },
                parent: buyAgeIconDiv
            });

            // END

            return contentObj;
        }
        
        function _addRelatedContent(relatedContentIndex , rootDiv , focusIndex) {
            var contentObj = {};

            contentObj.posterAreaDiv = _createPosterArea(relatedContentIndex , rootDiv , focusIndex);
            var contentInfoObj = _addContentInfoArea(relatedContentIndex , rootDiv );
            contentObj.contentInfoDiv = contentInfoObj.contentInfoDiv;
            contentObj.textBox = contentInfoObj.textBox;
            
            return contentObj;

        }


        function _addMashupContent( imageURL,rootDiv , focusIndex) {
            var posterAreaDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "mashup_content_area_div_" + focusIndex,
                    class: "mashup_content_poster_area",
                    css: {
                        position: "absolute"
                    }
                },
                parent: rootDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "mashup_content_shw_left",
                    src: "images/miniepg/sdw_poster_h300_l.png",
                    class: "mashup_content_shw_left",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "mashup_content_shw_middle",
                    src: "images/miniepg/sdw_poster_h300.png",
                    class: "mashup_content_shw_middle",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "mashup_content_shw_right",
                    src: "images/miniepg/sdw_poster_h300_r.png",
                    class: "mashup_content_shw_right",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            });

            var mashupContentImageId = "mashup_content_img" + focusIndex;

            var posterimg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: mashupContentImageId,
                    class: "mashup_content_img",
                    css:{
                        position: "absolute"
                    }
                },
                parent: posterAreaDiv
            }).error(function () {
                $(this).attr("src", "images/banner/default_mashup.png");
            }).attr("src", imageURL);


            var tempdiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_line_" + focusIndex,
                    class: "focus_mashup_content_focus_line focus_red_border_box",
                    css: {
                        position: "absolute"
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "focus_black_1px_border_box",
                    css: {
                        position: "absolute", left : 0 , top : 0 , width: (474-12), height: (344-12)
                    }
                },
                parent: tempdiv
            });

            return posterAreaDiv;
        }

        function _createPageElement() {
            isFirstSlidingAnimation = false;
            isSecondSlidingAnimation = false;
            
            var totalCount = mashupContentCount*2 + relatedContentCount;

            pageTotalCount = Math.floor(totalCount / 7);
            var tempb = totalCount % 7;
            if(tempb>0) {
                pageTotalCount++;
            }

            var tempmashupContentCount = mashupContentCount;
            var temprelatedContentCount =  relatedContentCount;
            var relatedContentIndex = 0;
            for(var i=0;i<pageTotalCount;i++) {
                /**
                 * 원래 사이즈는 1867이며 마지막 포스터 영역까지 고려하여 2067임.
                 */
                if(tempmashupContentCount> 0 || temprelatedContentCount>0) {
                    var pageDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "page_" + i,
                            css: {
                                position: "absolute", left: i*KTW.CONSTANT.RESOLUTION.WIDTH, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 464 ,
                                display : ""
                            }
                        },
                        parent: posterRootDiv
                    });


                    var frontDimImg = null;


                    if( i > 0) {
                        var rootdiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "",
                                css: {
                                    position: "absolute", left: -169, top: 0, width: 285, height: 464 ,
                                    display : ""

                                }
                            },
                            parent: pageDiv
                        });

                        _addRelatedContent(relatedContentIndex-1 , rootdiv );

                        frontDimImg = util.makeElement({
                            tag: "<img />",
                            attrs: {
                                id: "",
                                src: "images/miniepg/dim_related_left_h300.png",
                                css:{
                                    position: "absolute",
                                    left: 159,
                                    top: 37,
                                    width: 115,
                                    height: 300
                                }
                            },
                            parent: rootdiv
                        });

                    }

                    var leftPos = 10 + 69;

                    if(tempmashupContentCount> 0) {
                        leftPos = 0 + 69;
                    }

                    var pagePosterAreaList = [];
                    var pageContentInfoAreaList = [];
                    var pagePosterDataList = [];
                    var pageTextBoxAreaList = [];

                    var posterAreaDiv = null;
                    var contentInfoDiv = null;
                    for(var j=0;j<7;j++) {
                        var posterData = {};
                        posterAreaDiv = null;
                        contentInfoDiv = null;
                        if(tempmashupContentCount> 0) {
                            posterData.data_type = 0;
                            posterData.data = null;

                            var mashdata = mashupContentData[mashupContentCount-tempmashupContentCount];
                            var imageURL = null;
                            if(mashdata !== undefined && mashdata !== null
                                && mashdata.img !== undefined && mashdata.img !== null) {
                                imageURL = mashupContentData[mashupContentCount-tempmashupContentCount].img;
                                posterData.data = mashdata;
                            }

                            var rootdiv = util.makeElement({
                                tag: "<div />",
                                attrs: {
                                    id: "content_base_div_" + pagePosterAreaList.length,
                                    css: {
                                        position: "absolute", left: leftPos, top: 0, width: 516, height: 430 ,

                                        display : ""
                                    }
                                },
                                parent: pageDiv
                            });

                            posterAreaDiv = _addMashupContent(imageURL , rootdiv , pagePosterAreaList.length);

                            leftPos+=(38+425);
                            contentInfoDiv = {};
                            j++;

                            tempmashupContentCount--;
                            pagePosterAreaList[pagePosterAreaList.length] = posterAreaDiv;
                            pageContentInfoAreaList[pageContentInfoAreaList.length] = contentInfoDiv;
                            pageTextBoxAreaList[pageTextBoxAreaList.length] = null;

                        }
                        else if(temprelatedContentCount>0 ){
                            posterData.data_type = 1;
                            posterData.data = relatedContentList[relatedContentIndex];

                            var rootdiv = util.makeElement({
                                tag: "<div />",
                                attrs: {
                                    id: "content_base_div_" + pagePosterAreaList.length,
                                    css: {
                                        position: "absolute", left: leftPos, top: 0, width: 285, height: 464 ,
                                        display : ""

                                    }
                                },
                                parent: pageDiv
                            });

                            leftPos+=((38+210+36) - 38);

                            var contentObj = _addRelatedContent(relatedContentIndex , rootdiv , pagePosterAreaList.length );

                            temprelatedContentCount--;
                            relatedContentIndex++;
                            pagePosterAreaList[pagePosterAreaList.length] = contentObj.posterAreaDiv;
                            pageContentInfoAreaList[pageContentInfoAreaList.length] = contentObj.contentInfoDiv;
                            pageTextBoxAreaList[pageTextBoxAreaList.length] = contentObj.textBox;
                        }

                        pagePosterDataList[pagePosterDataList.length] = posterData;


                        if(tempmashupContentCount <=0 && temprelatedContentCount<=0) {
                            break;
                        }
                    }

                    var rearDimImg = null;
                    log.printDbg("_createPageElement() tempmashupContentCount : " + tempmashupContentCount + " , temprelatedContentCount : " + temprelatedContentCount + " , relatedContentCount : " + relatedContentCount);
                    if( tempmashupContentCount > 0 || temprelatedContentCount>0) {
                        var rootdiv = util.makeElement({
                            tag: "<div />",
                            attrs: {
                                id: "",
                                css: {
                                    position: "absolute", left: leftPos, top: 0, width: 285, height: 464 ,
                                    display : ""

                                }
                            },
                            parent: pageDiv
                        });

                        _addRelatedContent(relatedContentIndex , rootdiv );

                        rearDimImg = util.makeElement({
                            tag: "<img />",
                            attrs: {
                                id: "",
                                src: "images/miniepg/dim_related_right_h300.png",
                                css:{
                                    position: "absolute",
                                    left: 0,
                                    top: 37,
                                    width: 285,
                                    height: 300
                                }
                            },
                            parent: rootdiv
                        });

                    }


                    var pageInfo = {
                        page_root_div : pageDiv ,
                        front_dim_img : frontDimImg,
                        rear_dim_img : rearDimImg,
                        rear_img_left : leftPos,
                        page_item_count : pagePosterAreaList.length,
                        page_poster_area_list : pagePosterAreaList,
                        page_content_info_area_list : pageContentInfoAreaList,
                        page_poster_data_list : pagePosterDataList,
                        page_text_box_list : pageTextBoxAreaList
                    };
                    pageInfoList[pageInfoList.length] = pageInfo;



                }

            }
            focusPageIndex = 0;
        }

        function callbackFuncRelatedContents(isSuccess, result){
            log.printDbg("callbackFuncRelatedContents() isSuccess : " + isSuccess + " , result : " + result);
            KTW.ui.LayerManager.stopLoading();

            if(isSuccess && result && result.RESULT_CODE === 200 && result.RC_LIST && result.RC_LIST.length > 0){
                relatedContentList = result.RC_LIST;
                relatedContentCount = relatedContentList.length;
                _createPageElement();
                posterIndex = 0;
                backgroundImage.css("display", "");
                div.css("display", "");
                div.addClass("show");
                var pageInfo = pageInfoList[focusPageIndex];
                if(pageInfo !== null) {

                    pageInfo.page_root_div.css("display", "");
                    if(pageInfo.page_item_count>posterIndex) {
                        pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                        if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                            pageInfo.page_content_info_area_list[posterIndex].addClass("focus");

                            var childDiv = pageInfo.page_text_box_list[posterIndex];
                            _setTitleAnimation(childDiv);
                        }
                    }
                    $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");

                }


            } else {
                relatedContentCount = 0;
                relatedContentList = [];

                if(mashupContentCount>0) {
                    _createPageElement();
                    posterIndex = 0;
                    backgroundImage.css("display", "");

                    div.css("display", "");
                    div.addClass("show");
                    var pageInfo = pageInfoList[focusPageIndex];
                    if(pageInfo !== null) {
                        pageInfo.page_root_div.css("display", "");
                        if(pageInfo.page_item_count>posterIndex) {
                            pageInfo.page_poster_area_list[posterIndex].addClass("focus");
                            if(pageInfo.page_poster_data_list[posterIndex].data_type === 1) {
                                pageInfo.page_content_info_area_list[posterIndex].addClass("focus");

                                var childDiv = pageInfo.page_text_box_list[posterIndex];
                                _setTitleAnimation(childDiv);
                            }
                        }
                        $("#page_" + focusPageIndex + " #content_base_div_" + posterIndex + " #focus_line_"+ posterIndex ).addClass("focus");
                    }
                }else {
                    /**
                     * "연관 VOD가 없습니다" 문구 노출
                     */
                    noResultDiv.css("display" , "");
                    posterRootDiv.css("display" , "none");
                    backgroundImage.css("display", "");

                    div.css("display", "");
                    div.addClass("show");
                }
            }
        }


        function _callbackMashupMsg(msg) {
            log.printDbg("_callbackMashupMsg() msg : " + JSON.stringify(msg));
        }
    }
})();