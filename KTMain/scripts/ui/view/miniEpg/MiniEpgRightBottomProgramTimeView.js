/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>MiniEpgRightBottomProgramTimeView</code>
 *
 * 채널 미니 가이드 우측 하단 프로그램 시간 정보 및 프로그램 세부 정보 아이콘 화면 뷰
 * @author jjh1117
 * @since 2016-10-27
 */

"use strict";

(function() {
    KTW.ui.view.MiniEpgRightBottomProgramTimeView = function(div) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var epgutil = KTW.utils.epgUtil;
        var parentDiv = div;

        var currentProgramProgressBackgroundDiv = null;
        var progressCurrentPositionDiv = null;
        var nextProgramProgressBackgroundDiv = null;
        var progressNextPositionDiv = null;
        var textCurrentProgramTime = null;

        var textNextProgramTime = null;

        var currentTimeDiv = null;
        var textCurrentTime = null;
        var currentProgramDetailIconListDiv = null;
        var contextMenuIconDiv = null;
        var contextMenuText = null;
        var keyOkIconDiv = null;
        var textKeyOK = null;

        var iconMultiAudio = null;
        var iconSubTItles = null;
        var iconComment = null;
        var iconSign = null;



        var ch = null;
        var cp = null;
        var np = null;
        var state = null;
        var isProgramUpdate = null;
        var isPipChannel = false;

        var parentView = null;


        var isShowCaptionNoti = false;
        var captionNotiChannel = null;

        var isShowContextMenuIcon = true;

        var isShowCurrentTime = true;
        var isShowCurrentProgramTime = true;
        var isShowProgressBar = true;
        var isShowReservationIcon = true;


        var parentalRating = 0;
        var isBlocked = false;
        var mainVboChannel = null;
        var isHiddenPromoChannel = null;

        var reservationIconMovePos = (1084-370);

        this.create = function () {
            log.printDbg("create()");
            _createElement();
            _hideAll();
        };

        this.setParent  = function(parentview) {
            parentView = parentview;
        };


        this.updateCurrentProgram = function() {
            log.printDbg("updateCurrentProgram()");
            return _updateCurrentProgram();
        };


        /**
         *  var channelInfo = {
         *        channel : cur_channel,
         *        currentProgram : currentProgram,
         *        nextProgram : null,
         *        state : cur_channel_state,
         *        onlyProgramUpdate:false,
         *        isPipChannel : true/false
         *        "is_exist_releated_content_descriptor" : isExistReleatedContentDescriptor ,
         *         "releatedContentCategoryId" : releatedContentCategoryId === null ? "" : releatedContentCategoryId
         *      };
         */
        this.show = function (channelInfo) {
            var nowDate = new Date();
            var isShowNextProgramTime = true;
            var isShowProgramDetailIcon = true;

            isShowReservationIcon = false;
            isShowProgressBar = true;
            isShowCurrentProgramTime = true;
            isShowCurrentTime = true;
            isShowContextMenuIcon = true;

            log.printDbg("show() channelInfo : " + channelInfo === undefined ? "" : JSON.stringify(channelInfo));

            if(channelInfo !== undefined) {
                ch = channelInfo.channel;
                cp = channelInfo.currentProgram;
                np = channelInfo.nextProgram;
                state = channelInfo.state;
                isProgramUpdate = channelInfo.onlyProgramUpdate;
                isPipChannel = (channelInfo.isPipChannel === undefined ||
                channelInfo.isPipChannel === null) ? false : channelInfo.isPipChannel;


                parentalRating = channelInfo.parental_rating;
                isBlocked = channelInfo.isblocked;

                mainVboChannel = channelInfo.mainvbo_channel;

                isHiddenPromoChannel = channelInfo.isHiddenPromoChannel;
            }
            _hideAll();

            currentProgramProgressBackgroundDiv.css("display", "");
            nextProgramProgressBackgroundDiv.css("display", "");

            var promoch = null;
            if(KTW.CONSTANT.IS_OTS === true &&
                (mainVboChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S || mainVboChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2)) {
                promoch = KTW.oipf.AdapterHandler.navAdapter.getSkyPlusChannel();
            }else {
                promoch = KTW.oipf.AdapterHandler.navAdapter.getPromoChannel();
            }

            if(isPipChannel === true) {
                /**
                 * 인접채널 리스트 노출 상태
                 */

                if(state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                    isBlocked = false;
                    if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED) {
                        isBlocked = true;
                    }
                }

                if(isBlocked === true || ch.desc === 2 || promoch.ccid === ch.ccid) {
                    isShowProgressBar = false;
                    isShowCurrentProgramTime = false;
                    isShowNextProgramTime = false;
                    isShowProgramDetailIcon = false;
                    if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                        isShowContextMenuIcon = false;
                    }

                    if(mainVboChannel.ccid !== ch.ccid) {
                        isShowContextMenuIcon = false;
                    }

                    currentProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
                }else {
                    currentProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
                    nextProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.14)"});
                    if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                        isShowContextMenuIcon = false;
                    }else {
                        if(mainVboChannel.ccid !== ch.ccid) {
                            isShowContextMenuIcon = false;
                            isShowProgramDetailIcon = false;
                        }else {
                            if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                                || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                                || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                                isShowProgramDetailIcon = false;
                            }else {
                                isShowProgramDetailIcon = true;
                            }
                        }
                    }
                }
            }else {
                if(isProgramUpdate !== undefined && isProgramUpdate === true) {
                    isShowProgressBar = false;
                    isShowReservationIcon = true;
                }

                if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                    isShowContextMenuIcon = false;
                    if(isBlocked === true || ch.desc === 2 || promoch.ccid === ch.ccid) {
                        isShowProgressBar = false;
                        isShowCurrentProgramTime = false;
                        isShowNextProgramTime = false;
                        currentProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
                        isShowReservationIcon = false;
                    }else {
                        if(isProgramUpdate !== undefined && isProgramUpdate === true && KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true) {
                            var rate = parentalRating;
                            if(rate !== 0 && KTW.utils.epgUtil.getAge(cp)>=rate) {
                                isShowReservationIcon = false;
                            }
                        }
                    }
                }else {
                    isShowProgressBar = true;
                    isShowCurrentProgramTime = true;
                    isShowCurrentTime = true;
                    isShowNextProgramTime = true;
                    isShowContextMenuIcon = true;

                    if(isProgramUpdate !== undefined && isProgramUpdate === false) {
                        if (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                            || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                            || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                            isShowReservationIcon = false;

                            isShowProgressBar = false;
                            isShowNextProgramTime = false;
                            isShowProgramDetailIcon = false;

                            if(state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                                isShowCurrentProgramTime = false;
                            }

                            if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT && KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                                isShowProgressBar = true;
                            }

                        }else if(promoch.ccid === ch.ccid) {
                            isShowProgressBar = false;
                            isShowCurrentProgramTime = false;
                            isShowNextProgramTime = false;
                            currentProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
                            isShowReservationIcon = false;
                        }
                    }else {
                        if(isProgramUpdate !== undefined && isProgramUpdate === true && KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true) {
                            var rate = parentalRating;
                            if(rate !== 0 && KTW.utils.epgUtil.getAge(cp)>=rate) {
                                isShowReservationIcon = false;
                            }
                        }

                        if(KTW.CONSTANT.IS_OTS === false) {
                            isShowProgramDetailIcon = false;
                        }else {
                            if(ch.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S) {
                                isShowProgramDetailIcon = false;
                            }
                        }
                    }
                }

                if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) === true) {
                    isShowReservationIcon = false;
                }

                if(mainVboChannel.ccid !== ch.ccid) {
                    isShowContextMenuIcon = false;
                    isShowProgramDetailIcon = false;
                }
            }


            var now = (nowDate).getTime();
            var currentProgramReservation = 0;
            if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                now = Math.floor(now/1000);
            }

            if(cp !== undefined && cp !== null) {
                var ss = 0;
                if (cp.name === epgutil.DATA.PROGRAM_TITLE.NULL || cp.name === epgutil.DATA.PROGRAM_TITLE.UPDATE || cp.startTime == null || cp.duration == null) {
                    isShowProgressBar = false;
                    isShowCurrentProgramTime = false;
                    isShowProgramDetailIcon = false;
                    currentProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
                    nextProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
                }else {
                    ss = cp.startTime;
                    if (ss > now || (isProgramUpdate !== undefined && isProgramUpdate === true)) {
                        if(isPipChannel === false) {
                            isShowProgressBar = false;
                        }
                    }
                }

                var barLength = 1108;
                if(isShowProgressBar === true) {
                    var playBarLength = Math.floor((now - ss) * parseInt(barLength) / cp.duration);
                    playBarLength = playBarLength > barLength ? barLength : playBarLength;
                    progressCurrentPositionDiv.css("width", playBarLength);

                    progressCurrentPositionDiv.css('display', '');

                    /**
                     * jjh1117 2017/08/18
                     * GUI 변경 요청 사항
                     */
                    if(isPipChannel === true) {
                        progressCurrentPositionDiv.css({"background-color": "rgba(255,255,255,0.8)"});

                    }else {
                        progressCurrentPositionDiv.css({"background-color": "rgba(238,69,61,1)"});
                    }

                }else {
                    /**
                     * jjh1117 2017/08/18
                     * GUI 변경 요청 사항
                     */
                    if(isPipChannel === true) {
                        progressCurrentPositionDiv.css({"background-color": "rgba(255,255,255,0.8)"});

                    }else {
                        progressCurrentPositionDiv.css({"background-color": "rgba(238,69,61,1)"});
                    }

                    progressCurrentPositionDiv.css("width", 0);
                    progressNextPositionDiv.css("width" , 0);
                    progressCurrentPositionDiv.css('display', '');
                    progressNextPositionDiv.css('display', '');
                }

                if (isShowCurrentProgramTime === true){
                    var tmp = new Date(cp.startTime * 1000);
                    var st = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
                    tmp = new Date((cp.startTime + cp.duration) * 1000);
                    var et = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();

                    textCurrentProgramTime.text(st + ' - ' + et);
                    textCurrentProgramTime.css("display", "");
                }
                currentProgramReservation = KTW.managers.service.ReservationManager.isReserved(cp);

            }else {
                isShowProgressBar = false;
                isShowCurrentProgramTime = false;
                isShowProgramDetailIcon = false;
                if(isShowProgressBar === false) {
                    if (state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                        && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                        && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                        if(isBlocked !== true && ch.desc !== 2) {
                            progressCurrentPositionDiv.css('display', '');
                            progressNextPositionDiv.css('display', '');
                        }
                    }
                }

                isShowReservationIcon = false;
            }

            /**
             * Main VBO에서 재생 중인 채널 정보와 노출 해야 하는 채널이
             * 같지 않은 경우는 Program 상세 정보(자/해/수) 아이콘을
             * 노출 하지 않는다.
             */
            if(captionNotiChannel !== null){
                if(mainVboChannel.ccid !== captionNotiChannel.ccid) {
                    isShowCaptionNoti = false;
                    captionNotiChannel = null;
                }
            }

            if(isShowProgramDetailIcon === true) {
                var startLeft = 0;
                if (ch.idType === KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S) {
                    /**
                     * 음성다중 아이콘 활성화
                     */
                    if (KTW.utils.epgUtil.progInfo.isMultiAudio() === true) {
                        iconMultiAudio.css("display" , "inline-block");
                        startLeft+=84;
                        startLeft+=6;
                    }else {
                        iconMultiAudio.css("display" , "none");
                    }

                    if (cp.description != null && cp.description.length > 0) {
                        /**
                         *  자막 아이콘 활성화
                         */
                        if (cp.description.indexOf('(자)') > -1) {
                            iconSubTItles.css("display" , "inline-block");
                            _showCaptionNotiMessage();
                        }else {
                            iconSubTItles.css("display" , "none");
                        }
                        /**
                         * 해설 아이콘 활성화
                         */
                        if (cp.description.indexOf('(해)') > -1) {
                            iconComment.css("display" , "inline-block");
                        }else {
                            iconComment.css("display" , "none");
                        }

                        /**
                         * 수화 아이콘 활성화
                         */
                        if (cp.description.indexOf('(수)') > -1) {
                            iconSign.css("display" ,"inline-block");
                        }else {
                            iconSign.css("display" , "none");
                        }
                    }
                }
                else {
                    /**
                     * 음성다중 아이콘 활성화
                     */
                    if (KTW.utils.epgUtil.progInfo.isMultiAudio() === true && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {

                        iconMultiAudio.css("display" , "inline-block");
                    }else {
                        iconMultiAudio.css("display" , "none");
                    }

                    var cap = KTW.utils.epgUtil.progInfo.hasSubtitle();
                    var ad = KTW.utils.epgUtil.progInfo.isAudioDescription();

                    // 화면해설 피드백 문구 삭제 (KT 요청)
                    if (cap === true && ad === true && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                        iconSubTItles.css("display" , "inline-block");
                        iconComment.css("display" , "inline-block");

                        _showCaptionNotiMessage();

                    }
                    else if (cap === true && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                        iconComment.css("display" , "none");
                        iconSubTItles.css("display" , "inline-block");
                        _showCaptionNotiMessage();

                    }
                    else if (ad === true && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.UNKNOWN) {
                        iconSubTItles.css("display" , "none");
                        iconComment.css("display" ,  "inline-block");
                    }else {
                        iconSubTItles.css("display" , "none");
                        iconComment.css("display" , "none");
                    }
                }
                currentProgramDetailIconListDiv.css("display" , "");
            }else {
                currentProgramDetailIconListDiv.css("display" , "none");
            }

            /**
             * 현재 시간 정보 노출
             * 현재 프로그램 시간 정보가 노출 된 경우 우측으로 이동함.
             */
            textCurrentTime.text((nowDate.getHours() < 10 ? "0" : "") + nowDate.getHours() + ":" + (nowDate.getMinutes() < 10 ? "0" : "") + nowDate.getMinutes());
            if(isShowCurrentProgramTime === true) {
                currentTimeDiv.css({left:(553-370)});
            }else {
                currentTimeDiv.css({left:0});
            }
            if(isShowCurrentTime === true) {
                currentTimeDiv.css("display", "");
            }

            /**
             * 연관 메뉴
             */
            if(isShowContextMenuIcon === true) {
                var contextmenuTitle = "";
                reservationIconMovePos = 714;
                if(mainVboChannel.ccid === ch.ccid && (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                    || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT
                    || state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED)) {
                    contextmenuTitle = "편성표";
                    contextMenuText.text(contextmenuTitle);
                    contextMenuIconDiv.css({left:1009,width:179});
                    if(currentProgramReservation === KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                        reservationIconMovePos = 794 + 50;
                    }else {
                        reservationIconMovePos = 794;
                    }
                }else {
                    contextmenuTitle = "자막 | 음성 변경";
                    contextMenuText.text(contextmenuTitle);
                    contextMenuIconDiv.css({left:(1295-370),width:183});
                    if(currentProgramReservation === KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                        reservationIconMovePos = reservationIconMovePos+50;
                    }
                }

                contextMenuIconDiv.css("display", "");
            }else {
                if(currentProgramReservation === KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                    reservationIconMovePos = 967;
                }else {
                    reservationIconMovePos = 917;
                }
            }

            /**
             * 맞춤프로모에서는 시청예약 아이콘 노출 하지 않음. 동작도 하지 않음.
             */
            if(isShowReservationIcon === true && isHiddenPromoChannel !== null && isHiddenPromoChannel === true && mainVboChannel.ccid === ch.ccid) {
                isShowReservationIcon = false;
            }

            /**
             * 시청 예약 노출
             */
            if(isShowReservationIcon === true ) {
                if(cp.name !== epgutil.DATA.PROGRAM_TITLE.NULL && cp.name !== epgutil.DATA.PROGRAM_TITLE.UPDATE) {
                    if(currentProgramReservation === KTW.managers.service.ReservationManager.RESERVED_RESULT.NOT_RESERVED) {
                        textKeyOK.text("시청예약");
                    }else {
                        textKeyOK.text("시청예약 취소");
                    }
                    keyOkIconDiv.css({left:reservationIconMovePos});
                    keyOkIconDiv.css("display", "");
                }else {
                    isShowReservationIcon = false;
                }
            }

            /**
             * 미래 프로그램 시간 정보
             */
            if(np !== undefined && np !== null) {
                if (np.name === epgutil.DATA.PROGRAM_TITLE.NULL || cp.name === epgutil.DATA.PROGRAM_TITLE.UPDATE || np.startTime == null || np.duration == null) {
                    isShowNextProgramTime = false;
                }

                if(isShowNextProgramTime === true) {
                    var tmp = new Date(np.startTime * 1000);
                    var st = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
                    tmp = new Date((np.startTime + np.duration) * 1000);
                    var et = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();

                    textNextProgramTime.text(st + ' - ' + et);
                    textNextProgramTime.css("display", "");
                }
            }
            

        };

        this.hide = function () {
            log.printDbg("hide()");
            _hideAll();
        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    if(mainVboChannel.ccid === ch.ccid) {
                        if (state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                            && (state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT  ||
                            (state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT && KTW.managers.service.KidsModeManager.isKidsMode() === true))
                            && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                            if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.PREVIEW && isProgramUpdate !== undefined && isProgramUpdate === false) {
                                parentView.getRightTopView().preViewPurchaseChannel();
                                consumed = true;
                                return consumed;
                            }else {
                                if(isProgramUpdate !== undefined && isProgramUpdate === true && cp !== undefined && cp !== null
                                    && KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch) === false
                                    && cp.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL && cp.name !== epgutil.DATA.PROGRAM_TITLE.UPDATE ) {

                                    if(isHiddenPromoChannel !== null && isHiddenPromoChannel === true && mainVboChannel.ccid === ch.ccid) {
                                        consumed = true;
                                        return consumed;
                                    }

                                    if(isShowReservationIcon === true) {
                                        KTW.managers.service.ReservationManager.add(cp, true, function(result) {
                                            // 단편 예약
                                            if (result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_RESERVE ||//
                                                result === KTW.managers.service.ReservationManager.ADD_RESULT.DUPLICATE_RESERVE) {
                                                textKeyOK.text("시청예약 취소");
                                                reservationIconMovePos-=50;
                                                if(isShowContextMenuIcon === true) {
                                                    keyOkIconDiv.css({left:reservationIconMovePos});
                                                }else {
                                                    keyOkIconDiv.css({left:reservationIconMovePos});
                                                }
                                            }
                                            //시리즈 예약
                                            else if (result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_SERIES_RESERVE) {
                                                textKeyOK.text("시청예약 취소");
                                                reservationIconMovePos-=50;
                                                if(isShowContextMenuIcon === true) {
                                                    keyOkIconDiv.css({left:reservationIconMovePos});
                                                }else {
                                                    keyOkIconDiv.css({left:reservationIconMovePos});
                                                }
                                            }
                                            // 예약 취소
                                            else if (result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_CANCEL ||//
                                                result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_SERIES_CANCEL) {// 예약 취소
                                                textKeyOK.text("시청예약");
                                                reservationIconMovePos+=50;
                                                if(isShowContextMenuIcon === true) {
                                                    keyOkIconDiv.css({left:reservationIconMovePos});
                                                }else {
                                                    keyOkIconDiv.css({left:reservationIconMovePos});
                                                }
                                            }

                                            parentView.getRightTopView().show();
                                        }, false);

                                    }else {
                                        var isAgeLimit = false;
                                        if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true && isProgramUpdate !== undefined && isProgramUpdate === true) {
                                            if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(cp)>=parentalRating) {
                                                isAgeLimit = true;
                                            }
                                        }
                                        if(isAgeLimit) {
                                            KTW.managers.service.SimpleMessageManager.showMessageTextOnly("시청 예약할 수 없는 프로그램입니다");
                                        }
                                    }

                                    consumed = true;
                                }
                            }
                        }
                    }else {
                        if (state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.BLOCKED
                            && state !== KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.NOT_SUBSCRIBED) {
                            if(isProgramUpdate !== undefined && isProgramUpdate === true && cp !== undefined && cp !== null
                                && KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch) === false
                                && cp.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL && cp.name !== epgutil.DATA.PROGRAM_TITLE.UPDATE ) {

                                if(isHiddenPromoChannel !== null && isHiddenPromoChannel === true ) {
                                    consumed = true;
                                    return consumed;
                                }

                                if(isShowReservationIcon === true) {
                                    KTW.managers.service.ReservationManager.add(cp, true, function(result) {
                                        // 단편 예약
                                        if (result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_RESERVE ||//
                                            result === KTW.managers.service.ReservationManager.ADD_RESULT.DUPLICATE_RESERVE) {
                                            textKeyOK.text("시청예약 취소");
                                            reservationIconMovePos-=50;
                                            if(isShowContextMenuIcon === true) {
                                                keyOkIconDiv.css({left:reservationIconMovePos});
                                            }else {
                                                keyOkIconDiv.css({left:reservationIconMovePos});
                                            }
                                        }
                                        //시리즈 예약
                                        else if (result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_SERIES_RESERVE) {
                                            textKeyOK.text("시청예약 취소");
                                            reservationIconMovePos-=50;
                                            if(isShowContextMenuIcon === true) {
                                                keyOkIconDiv.css({left:reservationIconMovePos});
                                            }else {
                                                keyOkIconDiv.css({left:reservationIconMovePos});
                                            }
                                        }
                                        // 예약 취소
                                        else if (result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_CANCEL ||//
                                            result === KTW.managers.service.ReservationManager.ADD_RESULT.SUCCESS_SERIES_CANCEL) {// 예약 취소
                                            textKeyOK.text("시청예약");
                                            reservationIconMovePos+=50;
                                            if(isShowContextMenuIcon === true) {
                                                keyOkIconDiv.css({left:reservationIconMovePos});
                                            }else {
                                                keyOkIconDiv.css({left:reservationIconMovePos});
                                            }
                                        }

                                        parentView.getRightTopView().show();
                                    }, false);

                                }else {
                                    var isAgeLimit = false;
                                    if(KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) !== true && isProgramUpdate !== undefined && isProgramUpdate === true) {
                                        if(parentalRating !== 0 && KTW.utils.epgUtil.getAge(cp)>=parentalRating) {
                                            isAgeLimit = true;
                                        }
                                    }
                                    if(isAgeLimit) {
                                        KTW.managers.service.SimpleMessageManager.showMessageTextOnly("시청 예약할 수 없는 프로그램입니다");
                                    }
                                }

                                consumed = true;
                            }
                        }
                    }
                    break;
                case KTW.KEY_CODE.BACK:
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.hideOKKeyIcon = function () {
            log.printDbg("hideOKKeyIcon()");
            keyOkIconDiv.css("display", "none");
        };

        this.getNextProgram = function() {
            log.printDbg("getNextProgram()");
            return np;
        }

        this.startMovieChoicePPVPurchase = function () {
            log.printDbg("startMovieChoicePPVPurchase()");
            /**
             * 연관컨텐츠 메뉴 Hide
             * 구매 Icon Hide
             * 다음 프로그램 시작 정보 Hide
             */
            keyOkIconDiv.css("display", "none");
            textNextProgramTime.css("display", "none");
        };


        this.resetData = function () {
            isShowCaptionNoti = false;
            captionNotiChannel = null;
        };

        /**
         * 우측 하단 진행바 , Program 시작/끝 시간 , 프로그램 세부 정보 아이콘 다음 프로그램 시작/끝 시간 영역 DIV
         * 전체 화면(1920X1080)에서 left:292 , top:926 , width:1628 , height:210 위치
         * MiniEpgMainView의 leftDiv에서는 left:0 , top:358 , width:1628 , height:210 위치
         */
        function _createElement() {

            currentProgramProgressBackgroundDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "current_program_progress_background_div",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 1108, height: 6,
                        "background-color": "rgba(255,255,255,0.5)"
                    }
                },
                parent: parentDiv
            });

            progressCurrentPositionDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "progress_current_position_background_div",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 0, height: 6,
                        "background-color": "rgba(238,69,61,1)"
                    }
                },
                parent: parentDiv
            });


            nextProgramProgressBackgroundDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "next_program_progress_background_div",
                    css: {
                        position: "absolute", left: 1108, top: 0, width: 442, height: 6,
                        "background-color": "rgba(255,255,255,0.3)"
                    }
                },
                parent: parentDiv
            });

            progressNextPositionDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "progress_next_position_background_div",
                    css: {
                        position: "absolute", left: 1108, top: 0, width: 0, height: 6,
                        "background-color": "rgba(238,69,61,1)"
                    }
                },
                parent: parentDiv
            });


            textCurrentProgramTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "current_program_time",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 27, width: 185 , height: 28,
                        color: "rgba(235, 147, 147, 0.7)", "font-size": 26 , "letter-spacing":-1.3
                    }
                },
                text: "",
                parent: parentDiv
            });


            textNextProgramTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "next_program_time",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: (1508-370), top: 27, width: 185 , height: 28,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 26 , "letter-spacing":-1.3
                    }
                },
                text: "",
                parent: parentDiv
            });

            currentTimeDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (553-370), top: 27, width: 211, height: 27
                    }
                },
                parent: div
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "current_program_age_img",
                    src: "images/icon/icon_current_time.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 26,
                        height: 26
                    }
                },
                parent: currentTimeDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 37, top: 2, width: 91 , height: 26,
                        color: "rgba(255, 255, 255, 0.4)", "font-size": 24 , "letter-spacing":-1.2
                    }
                },
                text: "현재시간",
                parent: currentTimeDiv
            });

            textCurrentTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "current_time",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: (680-553), top: 1, width: 77 , height: 26,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 26 , "letter-spacing":-1.3
                    }
                },
                text: "",
                parent: currentTimeDiv
            });


            currentProgramDetailIconListDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 402, top: 27, width: 320, height: 32
                    }
                },
                parent: div
            });

            iconMultiAudio = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "icon_multi_audio",
                    src: "images/icon/icon_tag_multisound.png",
                    css: {
                        float: "left" ,
                        display : "inline-block",
                        "margin-right" : 3
                    }
                },
                parent: currentProgramDetailIconListDiv
            });

            iconSubTItles = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "icon_subtitle",
                    src: "images/icon/icon_tag_subtitles.png",
                    css: {
                        float: "left" ,
                        display : "inline-block",
                        "margin-right" : 3
                    }
                },
                parent: currentProgramDetailIconListDiv
            });

            iconComment = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "icon_comment",
                    src: "images/icon/icon_tag_comment.png",
                    css: {
                        float: "left" ,
                        display : "inline-block",
                        "margin-right" : 3
                    }
                },
                parent: currentProgramDetailIconListDiv
            });

            iconSign = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "icon_sign",
                    src: "images/icon/icon_tag_sign.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 51,
                        height: 32,
                        display:"none"
                    }
                },
                parent: currentProgramDetailIconListDiv
            });



            contextMenuIconDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1295-370), top: 27, width: 183, height: 26
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "context_menu_img",
                    src: "images/icon/icon_option_related_miniguide.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 27,
                        height: 22
                    }
                },
                parent: contextMenuIconDiv
            });

            contextMenuText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 37, top: 0, width: 150 , height: 26,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 24 , "letter-spacing":-1.2
                    }
                },
                text: "",
                parent: contextMenuIconDiv
            });

            keyOkIconDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: (1084-370), top: 24, width: 213, height: 28
                    }
                },
                parent: div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "key_ok_icon_img",
                    src: "images/miniepg/key_ok.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 55,
                        height: 28
                    }
                },
                parent: keyOkIconDiv
            });

            textKeyOK = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 55 + 7, top: 3, width: 151 , height: 28,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 24,"text-align":"left", "letter-spacing":-1.2
                    }
                },
                text: "시청예약" ,
                parent: keyOkIconDiv
            });


        }

        function _hideAll() {
            log.printDbg("_hideAll()");

            currentProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.5)"});
            currentProgramProgressBackgroundDiv.css("display", "none");
            progressCurrentPositionDiv.css("display", "none");

            nextProgramProgressBackgroundDiv.css({"background-color": "rgba(255,255,255,0.3)"});
            nextProgramProgressBackgroundDiv.css("display" , "none");


            progressNextPositionDiv.css("display" , "none");
            textCurrentProgramTime.css("display", "none");
            currentTimeDiv.css("display", "none");
            currentProgramDetailIconListDiv.css("display" , "none");

            iconMultiAudio.css("display" , "none");
            iconSubTItles.css("display" , "none");
            iconComment.css("display" , "none");
            iconSign.css("display" , "none");

            contextMenuIconDiv.css("display", "none");
            keyOkIconDiv.css("display", "none");
            textNextProgramTime.css("display", "none");

        }

        function _updateCurrentTime(nowDate) {
            if(isShowCurrentTime === true) {
                textCurrentTime.text((nowDate.getHours() < 10 ? "0" : "") + nowDate.getHours() + ":" + (nowDate.getMinutes() < 10 ? "0" : "") + nowDate.getMinutes());
            }
        }

        function _updateCurrentProgram() {
            //log.printDbg("_updateCurrentProgram() cp : " + JSON.stringify(cp));
            log.printDbg("_updateCurrentProgram() state : " + state);
            var now = new Date().getTime();
            if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                now = Math.floor(now/1000);
            }
            var nowDate = new Date();
            _updateCurrentTime(nowDate);
            if(isShowProgressBar === true) {
                if(cp !== null && cp.name !== epgutil.DATA.PROGRAM_TITLE.NULL && cp.name !== epgutil.DATA.PROGRAM_TITLE.UPDATE && cp.startTime !== undefined && cp.startTime !== null && cp.duration !== undefined && cp.duration !== null) {
                    if(now < ((cp.startTime + cp.duration)-5)) {
                        //log.printDbg("_updateCurrentProgram() now : " + now);
                        log.printDbg("_updateCurrentProgram() cp.startTime + cp.duration : " + (cp.startTime + cp.duration));

                        var barLength = 1108;
                        var ss = cp.startTime;

                        var playBarLength = Math.floor((now - ss) * parseInt(barLength) / cp.duration);
                        playBarLength = playBarLength > barLength ? barLength : playBarLength;
                        progressCurrentPositionDiv.css("width", playBarLength);
                        return true;
                    }else {
                        return false;
                    }

                }else {
                    return true;
                }
            }else {
                return true;
            }


        }


        function _showCaptionNotiMessage() {
            if(mainVboChannel.ccid === ch.ccid && captionNotiChannel === null && isShowCaptionNoti === false) {
                captionNotiChannel = mainVboChannel;
                isShowCaptionNoti = true;
                KTW.managers.service.SimpleMessageManager.showCaptionServiceNotiMessage();
            }
        }
    };

})();

