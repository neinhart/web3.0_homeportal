/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>OTSProgramDetailVIew</code>
 *
 * 연관 컨텐츠 화면 뷰
 * @author jjh1117
 * @since 2017-05-22
 */

"use strict";

(function() {
    KTW.ui.view.OTSProgramDetailView = function(parent) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var parent = parent;

        var popup_container_div = null;
        var btnIndex = 0;
        var isVod = false;

        var div = parent.div;
        var params = null;

        var programDetailDiv = null;
        var channelInfoDiv = null;
        var textChannelNumber = null;
        var textChannelName = null;

        var textProgramName_1 = null;
        var textProgramName_2 = null;

        var textProgramTime = null;
        var divLine = null;
        var textProgramDuration = null;

        var textProgramDescription_1 = null;
        var textProgramDescription_2 = null;

        var curChannel = null;
        var program = null;
        var isShowReservation = false;
        var state = null;

        var btnList = [];
        var btnfocusIndex = -1;

        var callbackFunc = null;

        var CHANNELTUNE_BTN_TYPE = 0;
        var PROGRAM_RESERVATION_BTN_TYPE = 1;
        var CANCEL_BTN_TYPE = 2;

        this.setData = function (otsChannel , otsProgram , isOTSShowReservation , currentChannelState , miniEpgCallback) {
            curChannel = otsChannel;
            program = otsProgram;
            isShowReservation = isOTSShowReservation;
            state = currentChannelState;
            callbackFunc = miniEpgCallback;
        };

        this.create = function () {
            log.printDbg("_createElement()");
            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,display:"none"
                    }
                },
                parent: div
            });


            programDetailDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "program_detail_div",
                    css: {
                        position: "absolute",
                        left: 0, top: 509, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 571
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_img",
                    src: KTW.CONSTANT.IMAGE_PATH + "miniepg/bg_miniguide.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 571,opacity:0.9
            
                    }
                },
                parent: programDetailDiv
            });


            _createChannelInfoElement();

            textProgramName_1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "focus_program_name1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 143, top: 739-509, width: 1313, height: 53,
                        color: "rgba(255, 255, 255, 1)", "font-size": 50 , "letter-spacing":-2.5
                    }
                },
                text: "",
                parent: programDetailDiv
            });

            textProgramName_2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "focus_program_name2",
                    class: "font_l cut" ,
                    css: {
                        position: "absolute", left: 143, top: 739-509, width: 1110, height: 53 , "letter-spacing":-2.5 ,
                        color: "rgba(255, 255, 255, 1)", "font-size": 50,display:"none"
                    }
                },
                text: "",
                parent: programDetailDiv
            });



            textProgramTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_time",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 143, top: 805-509, width: 236 , height: 29,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 27 , "letter-spacing":-1.35
                    }
                },
                text: "",
                parent: programDetailDiv
            });

            // 구분선
            divLine = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "delimiter_line",
                    css: {
                        position: "absolute", left: 392, top: 805-509 + 4, width: 2, height: 18,
                        "background-color": "rgba(255, 255, 255, 0.3)"
                    }
                },
                parent: programDetailDiv
            });

            // 프로그램 런닝타임: 70분 (실제 데이터는 showView 에서 set)
            textProgramDuration = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_duration_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 407, top: 805-509,width: 206 , height: 29,
                        color: "rgba(255, 255, 255, 0.7)",
                        "font-size": 27 , "letter-spacing":-1.35
                    }
                },
                parent: programDetailDiv
            });

            // 프로그램 description: 최대 2줄
            textProgramDescription_1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_description_text1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 140, top: 850-509 , width: 1336 , height:30,
                        color: "rgba(255, 255, 255,0.7)",
                        "font-size": 28 , "letter-spacing":-1.4
                    }
                },
                parent: programDetailDiv
            });

            textProgramDescription_2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_description_text2",
                    class: "font_l cut",
                    css: {
                        position: "absolute", left: 140, top: 850-509 + 38 , width: 1336 , height:30,
                        color: "rgba(255, 255, 255,0.7)",
                        "font-size": 28 , "letter-spacing":-1.4
                    }
                },
                parent: programDetailDiv
            });

            var leftStart = 140;
            for(var i=0;i<2;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "btn_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 949-509, width: 280, height: 62
                        }
                    },
                    parent: programDetailDiv
                });

                var defaultBtn = util.makeElement({
                    tag: "<div />",
                    class: "focus_dark_black_1px_border_box" ,
                    attrs: {
                        id: "default_btn_" + (i+1),
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "background-color": "rgba(0,0,0,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var textBtn = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center" , "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    text_btn : textBtn,
                    btn_type : -1
                };
                btnList[btnList.length] = btnobj;

                leftStart+=280;
                leftStart+=12;
            }

        };


        function _createChannelInfoElement() {
            log.printDbg("_createChannelInfoElement()");
            channelInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 140, top: 688-509, width: KTW.CONSTANT.RESOLUTION.WIDTH-140, height: 38
                    }
                },
                parent: programDetailDiv
            });

            textChannelNumber = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 66, height: 36,
                        color: "rgba(235, 182, 147, 1)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: "",
                parent: channelInfoDiv
            });

            textChannelName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_name",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 73, top: 0, width: 600, height: 38,
                        color: "rgba(255, 254, 254, 0.8)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: "",
                parent: channelInfoDiv
            });
        }


        this.show = function (options) {
            log.printDbg("show()");

            /**
             * 채널번호 Udpate
             */
            textChannelNumber.text(util.numToStr(curChannel.majorChannel, 3));
            textChannelNumber.css("display", "");

            /**
             * 채널명 Udpate
             */
            var textLength = util.getTextLength(curChannel.name , "RixHead B" , 36 , -1.8);
            textChannelName.css({width:textLength});
            textChannelName.text(curChannel.name);
            textChannelName.css("display", "");

            var isAgeLimit = false;
            if(isShowReservation === false) {
                if(state === KTW.ui.layer.MiniEpgLayer.DEF.PROGRAM_STATE.AGE_LIMIT) {
                    isAgeLimit = true;
                }
            }else {
                /*
                if(KTW.oipf.AdapterHandler.casAdapter.getParentalRating() !== 0 && KTW.utils.epgUtil.getAge(program)>=KTW.oipf.AdapterHandler.casAdapter.getParentalRating()) {
                    isAgeLimit = true;
                }
                */
                if ((KTW.managers.auth.AuthManager.getPR() !== 0) && (KTW.utils.epgUtil.getAge(program) >= KTW.managers.auth.AuthManager.getPR())) {
                    isAgeLimit = true;
                }
            }

            if(program !== undefined && program !== null) {
                var tempProgramName = null;
                if(isAgeLimit === true) {
                    tempProgramName = "시청연령 제한 프로그램입니다";
                }else {
                    tempProgramName = program.name;
                }
                var multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 51 , 1110 , -2.5);

                if(multiLineText !== null && multiLineText.length > 1) {
                    /**
                     * 채널번호,채널명을 위로 이동 시킴
                     */
                    channelInfoDiv.css({top: 120});
                    textProgramName_1.css({top: 739-509-58});
                    textProgramName_1.text(multiLineText[0]);

                    if(multiLineText.length>2) {
                        textProgramName_2.text(multiLineText[1]+multiLineText[2]);
                    }else {
                        textProgramName_2.text(multiLineText[1]);
                    }
                }else {
                    textProgramName_1.css({top: 739-509});
                    textProgramName_1.text(tempProgramName);
                    textProgramName_2.text("");
                }
            }


            /**
             * 시간 정보 표시
             */
            var tmp = new Date(program.startTime * 1000);
            var st = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
            tmp = new Date((program.startTime + program.duration) * 1000);
            var et = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
            var startDate = new Date(program.startTime * 1000);


            var today = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);

            var showTimeStr = null;

            if(today.getMonth() === startDate.getMonth() && today.getDate() === startDate.getDate()) {
                showTimeStr = "오늘 " + st + ' - ' + et;
            }else if(tomorrow.getMonth() === startDate.getMonth() && tomorrow.getDate() === startDate.getDate()) {
                showTimeStr = "내일 " + st + ' - ' + et;

            }else {
                showTimeStr = "(" + util.transDay(startDate.getDay(), 'kor') + ") " + st + ' - ' + et;
            }

            var programTimeLength = util.getTextLength(showTimeStr, "RixHead L" , 27 , -1.35);
            var durationLength = util.getTextLength(Math.round(program.duration/60) + "분" , "RixHead L" , 27 , -1.35);

            textProgramTime.text(showTimeStr);
            textProgramDuration.text(Math.round(program.duration/60) + "분");

            var startLeft = 143;
            textProgramTime.css({left:143,width:programTimeLength});
            startLeft+=programTimeLength;
            startLeft+=13;
            divLine.css({left:startLeft});
            startLeft+=2;
            startLeft+=13;
            textProgramDuration.css({left:startLeft,width:durationLength});

            var description = null;
            if(isAgeLimit === true) {
                description  = "";
            }else {
                description  = program.description;
                if(description === undefined || description === null) {
                    description = "";
                }
            }

            var multiLineText = util.stringToMultiLine(description , "RixHead L" , 28 , 1336 , -1.4);
            if(multiLineText !== null && multiLineText.length > 1) {
                textProgramDescription_1.text(multiLineText[0]);
                if(multiLineText.length>2) {
                    textProgramDescription_2.text(multiLineText[1]+multiLineText[2]);
                }else {
                    textProgramDescription_2.text(multiLineText[1]);
                }
            }else {
                textProgramDescription_1.text(description);
                textProgramDescription_2.text("");
            }

            if(isShowReservation === true && isAgeLimit === false) {
                var reservationBtn = btnList[0];
                reservationBtn.btn_type = PROGRAM_RESERVATION_BTN_TYPE;

                var reservation = KTW.managers.service.ReservationManager.isReserved(program);
                if(reservation === 0) {
                    reservationBtn.text_btn.text("시청예약");
                }else {
                    reservationBtn.text_btn.text("예약취소");

                }
                reservationBtn.default_btn.css("display" , "none");
                reservationBtn.focus_btn.css("display" , "");

                reservationBtn.text_btn.css("color" , "rgba(255,255,255,0.7)");
                reservationBtn.text_btn.removeClass("font_l");
                reservationBtn.text_btn.addClass("font_m");


                var closeBtn = btnList[1];

                closeBtn.btn_type = CANCEL_BTN_TYPE;
                closeBtn.text_btn.removeClass("font_m");
                closeBtn.text_btn.addClass("font_l");
                closeBtn.text_btn.text("닫기");
                closeBtn.default_btn.css("display" , "");
                closeBtn.focus_btn.css("display" , "none");

                closeBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                closeBtn.text_btn.removeClass("font_m");
                closeBtn.text_btn.addClass("font_l");

                closeBtn.root_div.css("display" , "none");

            }else {
                var closeBtn = btnList[0];
                closeBtn.btn_type = CANCEL_BTN_TYPE;
                closeBtn.text_btn.text("닫기");
                closeBtn.default_btn.css("display" , "none");
                closeBtn.focus_btn.css("display" , "");

                closeBtn.text_btn.css("color" , "rgba(255,255,255,0.7)");
                closeBtn.text_btn.removeClass("font_l");
                closeBtn.text_btn.addClass("font_m");

                closeBtn = btnList[1];
                closeBtn.root_div.css("display" , "none");
            }

            btnfocusIndex = 0;
            popup_container_div.css("display", "");
        };

        this.hide = function (options) {
            log.printDbg("hide()");
            log.printDbg(options ? JSON.stringify(options) : "options is null");
            popup_container_div.css("display", "none");
        };


        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");
            var consumed = false;

            switch(keyCode) {
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    if(isShowReservation) {
                        if(btnfocusIndex === 0 ) {
                            btnfocusIndex = 1;
                        }else {
                            btnfocusIndex = 0;
                        }
                        _updateUI();
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    _keyOk();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                case KTW.KEY_CODE.EXIT:
                    consumed = false;
                    break;
            }

            return consumed;
        };


        /**
         * 지난 VOD 보기 정보가 있는 경우에만 불리는 함수
         * 좌/우 방향키에 따른 focus 변경을 처리
         *
         */
        function _updateUI() {
            log.printDbg("_updateUI(), btnfocusIndex = " + btnfocusIndex + ", isShowReservation = " + isShowReservation);

            if(isShowReservation) {
                var reservationBtn = btnList[0];
                reservationBtn.default_btn.css("display" , "");
                reservationBtn.focus_btn.css("display" , "none");
                reservationBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                reservationBtn.text_btn.removeClass("font_m");
                reservationBtn.text_btn.addClass("font_l");

                var closeBtn = btnList[1];
                closeBtn.default_btn.css("display" , "");
                closeBtn.focus_btn.css("display" , "none");
                closeBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                closeBtn.text_btn.removeClass("font_m");
                closeBtn.text_btn.addClass("font_l");


                if(btnfocusIndex === 0) {
                    reservationBtn.default_btn.css("display" , "none");
                    reservationBtn.focus_btn.css("display" , "");
                    reservationBtn.text_btn.css("color" , "rgba(255,255,255,0.7)");
                    reservationBtn.text_btn.removeClass("font_l");
                    reservationBtn.text_btn.addClass("font_m");
                }else {
                    closeBtn.default_btn.css("display" , "none");
                    closeBtn.focus_btn.css("display" , "");
                    closeBtn.text_btn.css("color" , "rgba(255,255,255,0.7)");
                    closeBtn.text_btn.removeClass("font_l");
                    closeBtn.text_btn.addClass("font_m");
                }
            }
        }

        /**
         * 지난 VOD 보기 버튼에서 OK가 눌려졌을 때의 처리
         *
         * @private
         */
        function _keyOk() {
            log.printDbg("_keyOk()");

            if(btnList !== null && btnList.length>0 ) {
                if(callbackFunc !== null) {
                    callbackFunc(btnList[btnfocusIndex].btn_type);
                }
            }
        }


    }
})();