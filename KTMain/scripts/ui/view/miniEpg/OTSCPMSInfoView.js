/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>OTSCPMSInfoView</code>
 *
 * @author jjh1117
 * @since 2017-01-25
 */

"use strict";

(function() {
    KTW.ui.view.OTSCPMSInfoView = function(div) {

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var parentDiv = div;

        var parentView = null;


        var backgroundImg = null;
        var cpmsImg = null;

        var cpmsImg1 = null;
        var cpmsImg2 = null;

        var focusIndex = 0;

        var btnJoinLeft = null;
        var btnJoinRight = null;

        var cpmsInfoCount = 0;

        this.create = function () {
            log.printDbg("create()");
            _createElement();
            _createButtonElement();
        };

        this.setParent  = function(parentview) {
            parentView = parentview;
        };


        /**
         *  var channelInfo = {
         *        channel : cur_channel,
         *        currentProgram : currentProgram,
         *        nextProgram : null,
         *        state : cur_channel_state,
         *        onlyProgramUpdate:false,
         *        isPipChannel : true/false
         *      };
         */
        this.show = function (cpmsinfo , focusINX) {
            _hideAll();
            focusIndex = 0;
            cpmsInfoCount = 0;

            if(cpmsinfo !== undefined && cpmsinfo !== null && cpmsinfo.details !== undefined && cpmsinfo.details !== null) {
                cpmsInfoCount = cpmsinfo.details.length;
                if(cpmsinfo.details.length > 0) {
                    if(cpmsinfo.details.length>=2) {
                        cpmsImg1.attr("src", cpmsinfo.details[0].imginfo[0].imgpath);
                        cpmsImg2.attr("src", cpmsinfo.details[1].imginfo[0].imgpath);
                        cpmsImg1.css("display" , "");
                        cpmsImg2.css("display" , "");
                    }else {
                        cpmsImg.attr("src", cpmsinfo.details[0].imginfo[0].imgpath);
                        cpmsImg.css("display" , "");
                    }
                }
            }
            focusIndex = focusINX;

            _updateFocus();
        };

        this.hide = function () {
            log.printDbg("hide()");
            _hideAll();

        };

        this.focus = function () {
            log.printDbg("focus()");
        };

        this.blur = function () {
            log.printDbg("blur()");

        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (keyCode) {
            log.printDbg("controlKey()");

            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:

                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    consumed = true;
                    break;
            }

            return consumed;
        };

        this.updateFocusIndex = function (focusINX) {
            focusIndex = focusINX;
            _updateFocus();
        };

        function _updateFocus() {
            backgroundImg.css("display" , "");

            if(focusIndex === 0) {
                if(cpmsInfoCount === 1) {
                    btnJoinLeft.btn_root_div.css("display" , "");
                    btnJoinLeft.btn_focus_div.css("display" , "");
                    btnJoinLeft.btn_focus_text.css("display" , "");

                    // btnJoinLeft.btn_default_div.css("display" , "none");
                    // btnJoinLeft.btn_default_text.css("display" , "none");

                }else {
                    btnJoinLeft.btn_root_div.css("display" , "");
                    btnJoinLeft.btn_focus_div.css("display" , "");
                    btnJoinLeft.btn_focus_text.css("display" , "");

                    // btnJoinLeft.btn_default_div.css("display" , "none");
                    // btnJoinLeft.btn_default_text.css("display" , "none");

                    btnJoinRight.btn_root_div.css("display" , "");
                    // btnJoinRight.btn_default_div.css("display" , "");
                    // btnJoinRight.btn_default_text.css("display" , "");

                    btnJoinRight.btn_focus_div.css("display" , "none");
                    btnJoinRight.btn_focus_text.css("display" , "none");
                }
            }else if(focusIndex === 1) {
                btnJoinRight.btn_root_div.css("display" , "");
                btnJoinRight.btn_focus_div.css("display" , "");
                btnJoinRight.btn_focus_text.css("display" , "");

                // btnJoinRight.btn_default_div.css("display" , "none");
                // btnJoinRight.btn_default_text.css("display" , "none");

                btnJoinLeft.btn_root_div.css("display" , "");
                // btnJoinLeft.btn_default_div.css("display" , "");
                // btnJoinLeft.btn_default_text.css("display" , "");

                btnJoinLeft.btn_focus_div.css("display" , "none");
                btnJoinLeft.btn_focus_text.css("display" , "none");
            }
        }

        /**
         * 우측 하단 진행바 , Program 시작/끝 시간 , 프로그램 세부 정보 아이콘 다음 프로그램 시작/끝 시간 영역 DIV
         * 전체 화면(1920X1080)에서 left:292 , top:926 , width:1628 , height:210 위치
         * MiniEpgMainView의 leftDiv에서는 left:0 , top:358 , width:1628 , height:210 위치
         */
        function _createElement() {


            backgroundImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/iframe/iframe_moviechoice.jpg",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width : KTW.CONSTANT.RESOLUTION.WIDTH ,
                        height : KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: parentDiv
            });

            cpmsImg = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "cpms_img",
                    src: "",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 94,
                        width : KTW.CONSTANT.RESOLUTION.WIDTH ,
                        height : 555
                    }
                },
                parent: parentDiv
            });


            cpmsImg1 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "cpms_img_1",
                    src: "",
                    css: {
                        position: "absolute",
                        left: 247,
                        top: 117,
                        width : 795,
                        height : 540
                    }
                },
                parent: parentDiv
            });

            cpmsImg2 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "cpms_img_2",
                    src: "",
                    css: {
                        position: "absolute",
                        left: 1042,
                        top: 117,
                        width : 795,
                        height : 540
                    }
                },
                parent: parentDiv
            });
        }


        /**
         *  가입 버튼 Element
         */
        function _createButtonElement() {
            log.printDbg("_createButtonElement()");
            var btnDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_join_left",
                    css: {
                        position: "absolute",
                        left: 292, top: 523, width:270, height: 68
                    }
                },
                parent: parentDiv
            });




            var btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_join_left_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:270, height: 68,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv
            });



            var btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_join_left_focus_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 20 , width: 270, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center"
                    }
                },
                text: "가입",
                parent: btnDiv
            });

            var btn = {
                btn_root_div : btnDiv,
//                btn_default_div : btnDefaultDiv,
                btn_focus_div : btnFocusDiv,
//                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            } ;

            btnJoinLeft = btn;

            btnDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_join_right",
                    css: {
                        position: "absolute",
                        left: 1087, top: 523, width:270, height: 68
                    }
                },
                parent: parentDiv
            });

            // btnDefaultDiv = util.makeElement({
            //     tag: "<div />",
            //     attrs: {
            //         id: "btn_join_right_default" ,
            //         css: {
            //             position: "absolute",
            //             left: 0, top: 0, width:270, height: 68,
            //             "background-color": "rgba(255,255,255,0.1)",
            //             display: "none"
            //         }
            //     },
            //     parent: btnDiv
            // });


            btnFocusDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_join_left_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:270, height: 68,
                        "background-color": "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv
            });

            // btnDefaultText = util.makeElement({
            //     tag: "<span />",
            //     attrs: {
            //         id: "btn_join_right_default_text",
            //         class: "font_l",
            //         css: {
            //             position: "absolute", left: 0 , top: 20 , width: 270, height: 32,
            //             color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "center"
            //         }
            //     },
            //     text: "가입",
            //     parent: btnDiv
            // });

            btnFocusText = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "btn_join_right_focus_text",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 20 , width: 270, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center"
                    }
                },
                text: "가입",
                parent: btnDiv
            });

            btn = {
                btn_root_div : btnDiv,
//                btn_default_div : btnDefaultDiv,
                btn_focus_div : btnFocusDiv,
//                btn_default_text : btnDefaultText,
                btn_focus_text : btnFocusText
            } ;

            btnJoinRight = btn;
        }


        function _hideAll() {
            log.printDbg("_hideAll()");
            backgroundImg.css("display" , "none");
            cpmsImg.css("display" , "none");
            cpmsImg1.css("display" , "none");
            cpmsImg2.css("display" , "none");

            btnJoinLeft.btn_root_div.css("display" , "none");
            btnJoinRight.btn_root_div.css("display" , "none");
        }


    };

})();

