/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SimpleMessageButtonPopup</code>
 *
 * POPUP 유형 4. 중앙 하단 표시형 (Text + Button)
 * - 위치: 전체 화면을 기준으로 가운데 하단에 표시
 * - Text: 팝업의 좌측에 위치
 * - Button: 팝업 우측에 위치
 * - 해당 팝업 적용 case: 예약 옵션 선택 팝업
 *
 * [message]
 * default 로 보여지는 메인 Text 이다.
 *
 * [arrButton]
 * 팝업 우측에 위치할 Button 들에 대한 기술이다.
 * 각 item 에는 id, name, message 로 구성되어있으며, UI 에는 name 이 보여진다.
 * Button 에 포커스가 갈때마다 해당 message 가 좌측 메인 Text 에 보여진다. (데이터가 없을 경우, 위 [message] 가 보여진다)
 *
 * [cbAction]
 * cbAction 은 버튼이 눌렸을때 혹은 View 가 종료되었을때 호출되는 함수이다.
 * 버튼이 눌렸을때는 button id 를 넘겨주고, View 가 종료될때는 parameter 가 null 로 호출이 된다.
 *
 * ex)
 * var popupData = {
 *      message: "main message",
 *      arrButton: [{id: "shot", name: "이번만 예약", message: "이번만 예약"}, {id: "series", name: "시리즈 예약", message: "시리즈 예약"},  {id: "view", name: "지난 VOD 보기", message: "지난 VOD 보기"}],
 *      cbAction: function (buttonId) {}
 * };
 * KTW.ui.LayerManager.activateLayer({
 *      obj: {
 *          id: "TestLayer",
 *          type: KTW.ui.Layer.TYPE.POPUP,
 *          priority: KTW.ui.Layer.PRIORITY.POPUP,
 *          view : KTW.ui.view.popup.SimpleMessageButtonPopup,
 *          params: {
 *              data : popupData
 *          }
 *      },
 *      visible: true
 * });
 *
 * @author dj.son
 * @since 2017-01-27
 */

"use strict";

(function() {
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var totalMessageWidth = 0;

    function createElement (_this) {
        log.printDbg("_createElement()");

        var parentDiv = _this.parent.div;
        var buttonLength = _this.data.arrButton.length;
        var buttonLeft = 1794 - (184 * buttonLength) - (8 * (buttonLength - 1));
        var messageWidth = buttonLeft - 30 - 124;
        totalMessageWidth = messageWidth;
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: KTW.CONSTANT.IMAGE_PATH + "caption_bg.png",
                css: {
                    position: "absolute", left: 0, top: 939, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 141
                }
            },
            parent: parentDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "smbp_program_name",
                class : "font_l cut",
                css: {
                    position: "absolute", left: 124, top: 982, width: messageWidth, height: 70,
                    "font-size": 30, "letter-spacing": -1.5, color: "rgba(235, 182, 147, 1)"
                }
            },
            //text: _this.data.message,
            parent: parentDiv
        });

        util.makeElement({
            tag: "<span />",
            attrs: {
                name: "smbp_message",
                class : "font_l",
                css: {
                    position: "absolute", left: 124, top: 982, width: messageWidth, height: 70,
                    "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 1)"
                }
            },
            //text: _this.data.message,
            parent: parentDiv
        });


        for (var i = 0; i < buttonLength; i++) {
            var button = util.makeElement({
                tag: "<div />",
                attrs: {
                    name: "smbp_button",
                    css: {
                        position: "absolute", left: buttonLeft, top: 974, width: 184, height: 48,
                        "background-color": "rgba(255, 255, 255, 0.1)"
                    }
                },
                parent: parentDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 13, width: 184, height: 48, "text-align": "center",
                        "font-size": 26, "letter-spacing": -1.3, color: "rgba(255, 255, 255, 0.5)"
                    }
                },
                text: _this.data.arrButton[i].name,
                parent: button
            });

            buttonLeft += 192;
        }
    }

    function setButtonFocus (_this) {
        var arrButton = _this.parent.div.find("[name=smbp_button]");
        var focusButton = $(arrButton[_this.focusIdx]);
        var message = _this.data.message;
        var programName = "";
        if (_this.data.arrButton[_this.focusIdx].message && _this.data.arrButton[_this.focusIdx].message.length > 0) {
            message = _this.data.arrButton[_this.focusIdx].message;
            programName = _this.data.arrButton[_this.focusIdx].program_name;
        }

        arrButton.css({"background-color": "rgba(255, 255, 255, 0.1)"});
        arrButton.children().css({color: "rgba(255, 255, 255, 0.5)", "font-family": "RixHead L"});

        focusButton.css({"background-color": "rgba(210, 51, 47, 1)"});
        focusButton.children().css({color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M"});

        var messagelength = util.getTextLength(message, "RixHead L" , 30, -1.5);
        var programlength = util.getTextLength(programName, "RixHead L" , 30, -1.5);

        if((messagelength+programlength)>totalMessageWidth) {
            _this.parent.div.find("[name=smbp_program_name]").css({width:totalMessageWidth-messagelength});
            _this.parent.div.find("[name=smbp_program_name]").text(programName);

            _this.parent.div.find("[name=smbp_message]").css({left:124+(totalMessageWidth-messagelength)});
            _this.parent.div.find("[name=smbp_message]").css({width:messagelength});
            _this.parent.div.find("[name=smbp_message]").text(message);
        }else {
            _this.parent.div.find("[name=smbp_program_name]").css({width:programlength});
            _this.parent.div.find("[name=smbp_program_name]").text(programName);

            _this.parent.div.find("[name=smbp_message]").css({left:124+programlength+2});
            _this.parent.div.find("[name=smbp_message]").css({width:messagelength});
            _this.parent.div.find("[name=smbp_message]").text(message);
        }

    }

    KTW.ui.view.popup.SimpleMessageButtonPopup = function(options) {
        KTW.ui.View.call(this);

        this.data = null;
        this.focusIdx = -1;

        this.createView = function() {
            log.printDbg("create()");

            this.parent.div.attr({class: "arrange_frame"}).css({"font-family": "RixHead L"});
            this.data = this.parent.getParams().data;

            createElement(this);
        };

        this.showView = function (options) {
            log.printDbg("showView()");

            if (!options || !options.resume) {
                if (this.data.arrButton && this.data.arrButton.length > 0) {
                    this.focusIdx = 0;

                    setButtonFocus(this);
                }
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");

            if (!options || !options.pause) {
                if (this.data.cbAction) {
                    this.data.cbAction();
                }
            }
        };

        this.controlKey = function (keyCode) {
            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    if (this.focusIdx > -1) {
                        this.focusIdx = --this.focusIdx < 0 ? this.data.arrButton.length - 1 : this.focusIdx;
                        setButtonFocus(this);
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.RIGHT:
                    if (this.focusIdx > -1) {
                        this.focusIdx = ++this.focusIdx > this.data.arrButton.length - 1 ? 0 : this.focusIdx;
                        setButtonFocus(this);
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.OK:
                    if (this.focusIdx > -1) {
                        if (this.data.cbAction) {
                            this.data.cbAction(this.data.arrButton[this.focusIdx].id);
                        }
                        consumed = true;
                    }
                    break;
            }

            return consumed;
        };
    };

    KTW.ui.view.popup.SimpleMessageButtonPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.SimpleMessageButtonPopup.constructor = KTW.ui.view.popup.SimpleMessageButtonPopup;

    KTW.ui.view.popup.SimpleMessageButtonPopup.prototype.create = function() {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.SimpleMessageButtonPopup.prototype.show = function (options) {
        this.showView(options);
    };
    KTW.ui.view.popup.SimpleMessageButtonPopup.prototype.hide = function (options) {
        this.hideView(options);
    };
    KTW.ui.view.popup.SimpleMessageButtonPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };

})();