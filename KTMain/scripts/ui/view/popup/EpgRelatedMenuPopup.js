/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>EpgRelatedMenuPopup</code>
 *
 *  var related_menu_info = {
 *        left_menu_info : {
 *            "channel" : cur_channel,
 *            "is_ots_channel" : true/false,
 *            "is_show_favorited_channel" : true/false,
 *            "is_show_2ch" : true/false,
 *            "is_show_4ch" : true/false,
 *            "is_show_hit" : true/false
 *            left_menu_callback_func : callback (parameter : index) // is_show_favorited_channel : 0 , detail : 1
 *                                                                      is_show_2ch : 2 , is_show_4ch : 3,
 *                                                                      is_show_hit : 4
 *        },
 *        right_menu_info : {
 *            caller : 0 or 1  ,
 *             fullepg : {
 *                    top_genre : {},
 *                    middle_genrc : {},
 *                    bottom_genrc ; {},
 *                    focus_genrc : top_genrc/bottom_genrc/bottom_genrc
 *                    focus_index : 0 ~ (n-1),
 *                    fullepg_callback : callback(parameter : genre , index)
 *                                            genre : (top_genre/middle_genre/bottom_genrc)
 *                                            index (0 ~ (n-1)
 *            },
 *            miniepg : {
 *                    top_menu : {
 *                        is_show_total_channel : true/false ,
 *                        is_show_favorited_channel : true/false
 *                    },
 *                    middle_menu : {
 *                        is_show_caption : true/false,
 *                        is_show_voice : true/false,
 *                        is_show_bluetooth_voice : true/false ,
 *                        is_show_bluetooth_listening : true/false
 *                    },
 *                    bottom_menu ; {
 *                        is_show_bookmark : true/false
 *                        is_show_home_widget : true/false
 *                    },
 *                    miniepg_callback : callback(parameter : menu , index)
 *                            menu : top_menu/middle_menu/bottom_menu
 *                            index : 0 ~ (n-1)
 *            },
 *        }
 *      };
 * @author jjh1117
 * @since 2016-11-04
 */

"use strict";

(function () {
    KTW.ui.view.popup.EpgRelatedMenuPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var popupRelatedMenuDiv = null;

        var leftDiv = null;
        var rightDiv = null;
        var kidsDiv = null;

        var leftRelatedMenuView = null;
        var rightRelatedMenuView = null;
        var kidsRelatedMenuView = null;

        var params = null;

        var focusView = null;
        var div = null;
        var isHideAction = false;

        var isShowFavEmptyPopup = false;

        var isDCSMode = false;

        var isShowTwoChannel = false;

        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            div = this.parent.div;
            if(KTW.managers.service.KidsModeManager.isKidsMode()) {
                _createKidsModeView(div,this);
            }else {
                _createView(div,this);
            }
        };

        this.showView = function(options) {
            log.printDbg("show(" + log.stringify(options) + ")");

            if (!options || !options.resume) {
                if (KTW.CONSTANT.IS_OTS) {
                    var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                    log.printDbg("showView(), KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE) : " + temp);
                    if (temp != null) {
                        if(temp === "Y") {
                            isDCSMode = true;
                        }else {
                            isDCSMode = false;
                        }
                    }

                    log.printDbg("showView(), isDCSMode: " + isDCSMode);
                    KTW.managers.StorageManager.ps.addStorageDataChangeListener(_updateOTSDCSMode);
                }

                setTimeout(function() {
                    isShowFavEmptyPopup = false;
                    if(KTW.managers.service.KidsModeManager.isKidsMode()) {
                        _showKIdsModeView();
                    }else {
                        _showView();
                    }
                }, 5);
            }
        };

        this.hideView = function(leftcallbackFunc , leftinputparam1 , leftinputparam2 , rightcallbackFunc , rightparam1,rightparam2 , rightparam3 , rightparam4) {
            log.printDbg("hideView()");
            _hideView(leftcallbackFunc , leftinputparam1 , leftinputparam2 , rightcallbackFunc , rightparam1,rightparam2 , rightparam3 , rightparam4);
            // if (!options || !options.pause) {
            //     _hideView();
            // }


        };

        this.showFavEmptyPopup = function() {
            isShowFavEmptyPopup = true;
            _hideView();
        };

        this.destroy = function() {
            log.printDbg("destroy()");
            if (KTW.CONSTANT.IS_OTS) {
                KTW.managers.StorageManager.ps.removeStorageDataChangeListener(_updateOTSDCSMode);
            }
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;
            if(isHideAction === true) {
                return true;
            }

            switch (key_code) {
                case KTW.KEY_CODE.CONTEXT :
                    _hideView();
                    // setTimeout(function() {
                    //     KTW.ui.LayerManager.deactivateLayer({
                    //         id: "EpgRelatedMenuPopup"
                    //     });
                    // }, 600);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:
                case KTW.KEY_CODE.OK:
                    if(focusView !== null) {
                        focusView.controlKey(key_code);
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    if(KTW.managers.service.KidsModeManager.isKidsMode()) {

                    }else {
                        if(focusView === leftRelatedMenuView) {

                            focusView.blur();
                            focusView = rightRelatedMenuView;
                            focusView.focus();
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    if(KTW.managers.service.KidsModeManager.isKidsMode()) {
                        _hideView();
                        // setTimeout(function() {
                        //     KTW.ui.LayerManager.deactivateLayer({
                        //         id: "EpgRelatedMenuPopup"
                        //     });
                        // }, 600);
                    }else {
                        if(focusView === leftRelatedMenuView) {
                            _hideView();
                            // setTimeout(function() {
                            //     KTW.ui.LayerManager.deactivateLayer({
                            //         id: "EpgRelatedMenuPopup"
                            //     });
                            // }, 600);
                        }else {
                            focusView.blur();
                            focusView = leftRelatedMenuView;
                            focusView.focus();
                        }

                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    consumed = focusView.controlKey(key_code);

                    if(consumed === false) {
                        _hideView();
                        // setTimeout(function() {
                        //     KTW.ui.LayerManager.deactivateLayer({
                        //         id: "EpgRelatedMenuPopup"
                        //     });
                        // }, 600);
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.EXIT:
                    _hideView();
                    consumed = false;
                    break;
            }



            return consumed;
        };

        function _updateOTSDCSMode(key) {
            log.printDbg('_updateOTSDCSMode() key : ' + key);
            if (key === KTW.managers.StorageManager.KEY.OTS_DCS_MODE) {
                log.printDbg('_updateOTSDCSMode() key : ' + key);
                var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                if (temp != null) {
                    if(temp === "Y") {
                        isDCSMode = true;
                    }else {
                        isDCSMode = false;
                    }

                    if(KTW.managers.service.KidsModeManager.isKidsMode() === false) {
                        if(isShowTwoChannel === true) {
                            _hideView();
                        }
                    }
                }
            }
        }

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            _createBaseView(parent_div);

            _createLeftView(_this);
            _createRightView(_this);
        }

        function _createKidsModeView(parent_div , _this) {
            log.printDbg("_createKidsModeView()");
            parent_div.attr({class: "arrange_frame"});

            _createBaseKidsModeView(parent_div);
            _createKidsModeChildView(_this);

        }

        /**
         * 자세히 팝업을 화면에 보여준다.
         * 전달 받은 data(param) 에 따라 channel 및 program 관련 정보를 set 한다.
         * button 개수 또한 1개 ~ 2개가 될 수 있다. (반복 예약 OTS 지원 여부에 따라 4개까지 확대될 수 있다)
         *
         * @param data
         * @private
         */
        function _showKIdsModeView() {
            // log.printDbg("_showKIdsModeView(), data = " + JSON.stringify(params));
            // log.printDbg("_showKIdsModeView(), data.left_menu_info  = " + JSON.stringify(params.data.left_menu_info));
            // log.printDbg("_showKIdsModeView(), data.right_menu_info = " + JSON.stringify(params.data.right_menu_info));

            kidsRelatedMenuView.show(params.data.left_menu_info , params.data.right_menu_info.mini_epg);

            focusView = kidsRelatedMenuView;
            div.find(".epgRelatedMenu").addClass("kidsmodeshow");
        }

        function _showView() {

            if(KTW.managers.service.KidsModeManager.isKidsMode() === false) {
                isShowTwoChannel = params.data.left_menu_info.is_show_2ch;
                if(isShowTwoChannel === true && isDCSMode === true) {
                    params.data.left_menu_info.is_show_2ch = false;
                }
            }
            
            leftRelatedMenuView.show(params.data.left_menu_info);
            leftDiv.css("display" , "");

            if(params.data.right_menu_info.caller === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER.FULL_EPG) {
                rightRelatedMenuView.show(params.data.right_menu_info.full_epg);

            }else {
                rightRelatedMenuView.show(params.data.right_menu_info.mini_epg);
            }
            rightDiv.css("display" , "");

            focusView = leftRelatedMenuView;

            div.find(".epgRelatedMenu").addClass("show");
        }

        function _hideView(leftcallbackFunc , leftinputparam1 , leftinputparam2 , rightcallbackFunc , rightparam1,rightparam2 , rightparam3 , rightparam4) {
            isHideAction = true;
            log.printDbg("_hideView()");

            if (KTW.CONSTANT.IS_OTS) {
                KTW.managers.StorageManager.ps.removeStorageDataChangeListener(_updateOTSDCSMode);
            }

            if(KTW.managers.service.KidsModeManager.isKidsMode()) {
                div.find(".epgRelatedMenu").removeClass("kidsmodeshow");

            }else {
                div.find(".epgRelatedMenu").removeClass("show");
            }


            div.off("webkitTransitionEnd").on("webkitTransitionEnd", function () {
                popupContainerDiv.css("display", "none");
                setTimeout(function() {
                    KTW.ui.LayerManager.deactivateLayer({
                        id: "EpgRelatedMenuPopup",
                        onlyTarget: true
                    });
                    setTimeout(function() {
                        if(isShowFavEmptyPopup === true) {
                            KTW.managers.service.FavoriteChannelManager.showFavEmptyPopup();
                        }else {
                            if(leftcallbackFunc !== undefined && leftcallbackFunc !== null) {
                                if(leftinputparam2 !== null) {
                                    leftcallbackFunc(leftinputparam1 , leftinputparam2);
                                }else {
                                    leftcallbackFunc(leftinputparam1);
                                }
                            }else if(rightcallbackFunc !== undefined && rightcallbackFunc !== null) {
                                if(rightparam3 === null && rightparam4 === null) {
                                    rightcallbackFunc(rightparam1 , rightparam2);
                                }else {
                                    rightcallbackFunc(rightparam1 , rightparam2 , rightparam3 , rightparam4);
                                }
                            }
                        }
                    }, 10);
                }, 10);



            });
            // leftDiv.css("display" , "none");
            // rightDiv.css("display" , "none");
        }

        function _createBaseKidsModeView(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.6)"
                    }
                },
                parent: parent_div
            });

            popupRelatedMenuDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "epgRelatedMenu",
                    css: {
                        position: "absolute",
                        left: 809+425, top: 0, width:1111, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_popup_background",
                    src: "images/popup/pop_bg_option_kids2.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 1111,
                        height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: popupRelatedMenuDiv
            });
        }

        function _createBaseView(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.60)"
                    }
                },
                parent: parent_div
            });

            popupRelatedMenuDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    class: "epgRelatedMenu",
                    css: {
                        position: "absolute",
                        left: KTW.CONSTANT.RESOLUTION.WIDTH-730, top: 0, width:1479, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: popupContainerDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/arw_view_l.png",
                    css: {
                        position: "absolute",
                        left: 680, top: 512, width:32, height: 56
                    }
                },
                parent: popupRelatedMenuDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_popup_background",
                    src: "images/popup/pop_bg_option_3.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 1479,
                        height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: popupRelatedMenuDiv
            });




        }

        function _createKidsModeChildView(_this) {
            log.printDbg("_createKidsModeView()");

            kidsDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "kids_div",
                    css: {
                        position: "absolute",
                        left: 1533-809, top: 0, width:384, height: KTW.CONSTANT.RESOLUTION.HEIGHT

                    }
                },
                parent: popupRelatedMenuDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/arw_view_l.png",
                    css: {
                        position: "absolute",
                        left: 680, top: 512, width:32, height: 56
                    }
                },
                parent: popupRelatedMenuDiv
            });


            kidsRelatedMenuView = new KTW.ui.view.MiniEpgKidsModeRelatedMenuView(_this);
            kidsRelatedMenuView.setParentDiv(kidsDiv);
            kidsRelatedMenuView.create();

        }

        /**
         * 1920X1080 해상도 기준으로 leftDiv left : 1171임
         * @private
         */
        function _createLeftView(_this) {
            log.printDbg("_createLeftView()");
            leftDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "left_div",
                    css: {
                        position: "absolute",
                        left: 730, top: 0, width:365, height: KTW.CONSTANT.RESOLUTION.HEIGHT

                    }
                },
                parent: popupRelatedMenuDiv
            });
            leftRelatedMenuView = new KTW.ui.view.EpgRelatedLeftMenuView(_this);
            leftRelatedMenuView.setParentDiv(leftDiv);
            leftRelatedMenuView.create();
        }

        function _createRightView(_this) {
            log.printDbg("_createRightView()");
            rightDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "right_div",
                    css: {
                        position: "absolute",
                        left: 1098, top: 0, width:381, height: KTW.CONSTANT.RESOLUTION.HEIGHT,overflow: "visible"
                    }
                },
                parent: popupRelatedMenuDiv
            });

            if(params.data.right_menu_info.caller === KTW.managers.service.EpgRelatedMenuManager.MENU_INFO.RIGHT_MENU.CALLER.FULL_EPG) {
                rightRelatedMenuView = new KTW.ui.view.FullEpgRelatedRightMenuView(_this);
            }else {
                rightRelatedMenuView = new KTW.ui.view.MiniEpgRelatedRightMenuView(_this);

            }
            rightRelatedMenuView.setParentDiv(rightDiv);
            rightRelatedMenuView.create();


        }
    };

    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype.constructor = KTW.ui.view.popup.EpgRelatedMenuPopup;

    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype.create = function() {
        this.createView();
    };
    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype.hide = function(options) {
        //this.hideView(options);
    };
    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.EpgRelatedMenuPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };
})();
