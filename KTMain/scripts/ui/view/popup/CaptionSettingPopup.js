/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>CaptionSettingPopup</code>
 * @author jjh1117
 * @since 2016-11-24
 */

"use strict";

(function () {
    KTW.ui.view.popup.CaptionSettingPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var popupCaptionSettingDiv = null;

        var btnCaptionOnOff = null;
        var btnFavLangSelect = null;
        var btnTranslationLangSelect = null;
        var btnFontSize = null;
        var btnCaptionService = null;

        var btnSave = null;
        var btnCancel = null;

        var translationDropDown = null;


        var params = null;

        var selectCaptionBox = ["켜기", "끄기"]; //자막
        var selectLangBox = ["한국어", "영어", "방송기본"]; //선호언어
        var selectFontSizeBox = ["작게", "중간(기본)", "크게"]; //글자크기
        var selectCaptionServiceBox = [1, 2, 3, 4, 5, 6]; //자막서비스


        var DEFAULT_ON_OFF = KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.OFF;
        var DEFAULT_SIZE = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.MEDIUM;
        var DEFAULT_LANG = KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.BASIC;

        var on_off = "";
        var language = "";
        var size = "";

        var focusOnOffIndex = 0;
        var focusLangIndex = 0;
        var focusSizeIndex = 1;
        var focusCaptionServiceIndex = 0;

        var NONE_BTN_INDEX = -1;
        var CAPTION_ON_OFF_BTN_INDEX = 0;
        var LANG_SELECT_BTN_INDEX = 1;
        var LANG_TRANSLATION_BTN_INDEX = 2;
        var FONT_SIZE_BTN_INDEX = 3;
        var CAPTION_SERVICE_BTN_INDEX = 4;
        var SAVE_BTN_INDEX = 5;
        var CANCEL_BTN_INDEX = 6;

        var focusbtnIndex = NONE_BTN_INDEX;


        var isTranslationLanguageCaptionSupport = false;
        var translationLanguageCaptionData = null;
        var selectTranslationIndex = -1;
        var translationItemList = [];
        
        var isShowTranslationLangDropDown = false;

        var totalPageTranslationLang = 0;
        var curPageTranslationLang = 0;
        var focusdropdownIndex = -1;

        var scrollbarbackground = null;
        var scrollbarforeground = null;

        this.createView = function() {
            log.printDbg("createView()");
            params = this.parent.getParams();
            var div = this.parent.div;

            log.printDbg("createView() params : " + log.stringify(params));

           // //var temp = {"onLang":"NONE","list":[{"name":"영어 (English)","code":"en"},{"name":"중국어 (Chinese)","code":"zh"},{"name":"프랑스어 (French)","code":"fr"},{"name":"독일어 (German)","code":"de"},{"name":"일본어 (Japanese)","code":"ja"},{"name":"스페인어 (Spanese)","code":"es"},{"name":"베트남어 (Vietnam)","code":"vi"},{"name":"캄보디아어 (Cambodia)","code":"km"},{"name":"우즈베키스탄어 (Uzbekistan)","code":"uz"}],"method":"mashup_getCCLanguageList","from":"4e30.3020"}
           //  var temp = {"onLang":"ja","list":[{"name":"독일어 (German)","code":"de"},{"name":"일본어 (Japanese)","code":"ja"},{"name":"스페인어 (Spanese)","code":"es"},{"name":"베트남어 (Vietnam)","code":"vi"},{"name":"캄보디아어 (Cambodia)","code":"km"},{"name":"우즈베키스탄어 (Uzbekistan)","code":"uz"}],"method":"mashup_getCCLanguageList","from":"4e30.3020"}
           //
           // params.data = temp;



            if(params !== undefined && params !== null && params.data !== undefined && params.data !== null) {
                isTranslationLanguageCaptionSupport = true;
                translationLanguageCaptionData = params.data;
                
                _checkTranslationData();

                LANG_TRANSLATION_BTN_INDEX = 2;
                FONT_SIZE_BTN_INDEX = 3;
                CAPTION_SERVICE_BTN_INDEX = 4;
                SAVE_BTN_INDEX = 5;
                CANCEL_BTN_INDEX = 6;
            }else {
                FONT_SIZE_BTN_INDEX = 2;
                CAPTION_SERVICE_BTN_INDEX = 3;
                SAVE_BTN_INDEX = 4;
                CANCEL_BTN_INDEX = 5;
            }

            _createView(div,this);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            focusbtnIndex = NONE_BTN_INDEX;
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;


            switch (key_code) {
                case KTW.KEY_CODE.UP:
                    if(isTranslationLanguageCaptionSupport === true && focusbtnIndex === LANG_TRANSLATION_BTN_INDEX && isShowTranslationLangDropDown === true) {
                        _focusMoveTranslationLangDropDown(true);
                    }else {
                        _moveUpDown(true);
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    if(isTranslationLanguageCaptionSupport === true && focusbtnIndex === LANG_TRANSLATION_BTN_INDEX && isShowTranslationLangDropDown === true) {
                        _focusMoveTranslationLangDropDown(false);
                    }else {
                        if(focusbtnIndex !== SAVE_BTN_INDEX && focusbtnIndex !== CANCEL_BTN_INDEX) {
                            _moveUpDown(false);
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                    if(focusbtnIndex === SAVE_BTN_INDEX) {
                        if (focusOnOffIndex == 0) {
                            on_off = KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.ON;
                        } else if (focusOnOffIndex == 1) {
                            on_off = KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.OFF;
                        }



                        //kor:한글, eng:영어, Basic:기본
                        if (focusLangIndex == 0) {
                            language = KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.KOREAN;
                        } else if (focusLangIndex == 1) {
                            language = KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.ENGLISH;
                        } else {
                            language =  KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.BASIC;
                        }

                        //BASIC:기본, SMALL:작은글씨, MEDIUM:중간글씨, LARGE:큰글씨
                        if (focusSizeIndex == 0)
                            size = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.SMALL;
                        else if (focusSizeIndex == 1)
                            size = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.MEDIUM;
                        else if (focusSizeIndex == 2)
                            size = KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.LARGE;

                        if(on_off === KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.OFF) {

                            
                            if(isTranslationLanguageCaptionSupport === true) {
                                var mashup_caption_msg = {
                                    from: KTW.CONSTANT.APP_ID.HOME,
                                    to: KTW.CONSTANT.APP_ID.MASHUP,
                                    method: "mashup_setCCLanguage",
                                    isOn : "N",
                                    lang : translationItemList[0].code
                                };

                                KTW.managers.MessageManager.sendMessage(mashup_caption_msg, null);
                            }
                        }else {
                            if(isTranslationLanguageCaptionSupport === true) {
                                if(selectTranslationIndex !== -1 && selectTranslationIndex !== 0) {
                                    on_off = KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.OFF;

                                    var mashup_caption_msg = {
                                        from: KTW.CONSTANT.APP_ID.HOME,
                                        to: KTW.CONSTANT.APP_ID.MASHUP,
                                        method: "mashup_setCCLanguage",
                                        isOn : "Y",
                                        lang : translationItemList[selectTranslationIndex].code
                                    };

                                    KTW.managers.MessageManager.sendMessage(mashup_caption_msg, null);
                                }else {
                                    var mashup_caption_msg = {
                                        from: KTW.CONSTANT.APP_ID.HOME,
                                        to: KTW.CONSTANT.APP_ID.MASHUP,
                                        method: "mashup_setCCLanguage",
                                        isOn : "N",
                                        lang : translationItemList[0].code
                                    };

                                    KTW.managers.MessageManager.sendMessage(mashup_caption_msg, null);
                                }
                            }
                        }

                        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_ONOFF, on_off);
                        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_LANGUAGE, language);
                        KTW.oipf.AdapterHandler.basicAdapter.setConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_SIZE, size);



                        KTW.ui.LayerManager.deactivateLayer({
                            id: "CaptionSettingPopup"
                        });

                    }else if(focusbtnIndex === CANCEL_BTN_INDEX) {
                        KTW.ui.LayerManager.deactivateLayer({
                            id: "CaptionSettingPopup"
                        });
                    }else {
                        if(isTranslationLanguageCaptionSupport === true) {
                            if(focusbtnIndex === LANG_TRANSLATION_BTN_INDEX) {
                                if(isShowTranslationLangDropDown === true) {
                                    selectTranslationIndex = (curPageTranslationLang-1)*5 + focusdropdownIndex;

                                    var lang = _getTranslationLang(selectTranslationIndex);
                                    btnTranslationLangSelect.text_btn.text(lang);
                                    
                                    _hideTranslationLangDropDown();
                                    if(selectTranslationIndex === 0) {
                                        btnFavLangSelect.root_div.css({opacity:1});

                                    }else {
                                        btnFavLangSelect.root_div.css({opacity:0.3});
                                    }

                                    _moveUpDown(false);
                                }else {
                                    _showTranslationLangDropDown();
                                }
                            }else {
                                _moveUpDown(false);
                            }
                        }else {
                            _moveUpDown(false);
                        }                         
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    _moveLeftRight(false);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                    _moveLeftRight(true);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    KTW.ui.LayerManager.deactivateLayer({
                        id: "CaptionSettingPopup"
                    });
                    consumed = true;
                    break;
            }



            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});
            _createElement(parent_div);
        }


        function _showView(options) {
            log.printDbg("_showView()");
            if (!options || !options.resume) {
                log.printDbg("_showView(), data = " + JSON.stringify(params));
                _readData();

                
                if(isTranslationLanguageCaptionSupport === true) {
                    if(translationLanguageCaptionData !== null && selectTranslationIndex !== -1 ) {
                        if(focusOnOffIndex === 1) {
                            if(selectTranslationIndex !== 0) {
                                focusOnOffIndex = 0;
                            }
                        }
                    }
                }
                
                _UIUpdate();


                if(focusOnOffIndex === 1) {
                    btnFavLangSelect.root_div.css({opacity:0.3});

                    if(isTranslationLanguageCaptionSupport === true) {
                        btnTranslationLangSelect.root_div.css({opacity:0.3});
                    }
                    btnFontSize.root_div.css({opacity:0.3});
                    btnCaptionService.root_div.css({opacity:0.3});
                }else {
                    btnFavLangSelect.root_div.css({opacity:1});

                    if(isTranslationLanguageCaptionSupport === true) {
                        btnTranslationLangSelect.root_div.css({opacity: 1});

                        if(selectTranslationIndex !== 0) {
                            btnFavLangSelect.root_div.css({opacity:0.3});
                        }
                    }
                    btnFontSize.root_div.css({opacity:1});
                    btnCaptionService.root_div.css({opacity:1});
                }
                _moveUpDown(false);
            }
        }

        function _checkTranslationData() {
            var list = translationLanguageCaptionData.list;
            if(list !== undefined && list !== null && list.length>0) {
                var length = list.length;
                if(translationLanguageCaptionData.onLang === "NONE") {
                    selectTranslationIndex = translationItemList.length;
                }
                translationItemList[translationItemList.length] = {code:"NONE" , name : "사용 안함 (OFF)"};

                for(var i=0;i<length;i++) {
                    if(selectTranslationIndex === -1) {
                        if(translationLanguageCaptionData.onLang === list[i].code) {
                            selectTranslationIndex = translationItemList.length;
                        }
                    }
                    translationItemList[translationItemList.length] = list[i];
                }
            }
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _createElement(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: parent_div
            });

            var height = 0;
            if(isTranslationLanguageCaptionSupport === true) {
                popupCaptionSettingDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 1366, top: 161, width:502, height:880
                        }
                    },
                    parent: popupContainerDiv
                });
                height = 880;
            }else {
                // 대충 700으로 한 것임.
                popupCaptionSettingDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 1366, top: (161 + 128), width:502, height:(880-128)
                        }
                    },
                    parent: popupContainerDiv
                });
                height = (880-128);

            }

            var middleHeight = height - (96+118);

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_popup_background",
                    src: "images/popup/pop_bg_option_a1_top.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 502,
                        height: 96
                    }
                },
                parent: popupCaptionSettingDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_popup_background",
                    src: "images/popup/pop_bg_option_a1_m.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 96,
                        width: 502,
                        height: middleHeight
                    }
                },
                parent: popupCaptionSettingDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "related_popup_background",
                    src: "images/popup/pop_bg_option_a1_bottom.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 96 + middleHeight ,
                        width: 502,
                        height: 118
                    }
                },
                parent: popupCaptionSettingDiv
            });
            
            

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 339-289, width: 502 , height: 36,
                        color: "rgba(255, 255, 255, 1)", "font-size": 33,"text-align" : "center", "letter-spacing":-1.65
                    }
                },
                text: "자막설정",
                parent: popupCaptionSettingDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1415-1366, top: 384-289, width: 400, height: 2,
                        "background-color": "rgba(255,255,255,0.1)"
                    }
                },
                parent: popupCaptionSettingDiv
            });

            var startTitlePosY = 412-289;
            var startBtnPosY = 444-289;

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "subtitle_title",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 1415-1366, top: startTitlePosY, width: 100 , height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24,"text-align" : "left" , "letter-spacing":-1.2
                    }
                },
                text: "자막",
                parent: popupCaptionSettingDiv
            });

            var btnCaptionOnOffDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1416-1366, top: startBtnPosY, overflow:"visible"
                    }
                },
                parent: popupCaptionSettingDiv
            });

            var defaultCaptionOnOffBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_caption_onoff_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:396, height: 61,
                        display: "none" ,
                        "background-color": "rgba(160,160,160,1)",
                        border:2,
                        "border-style":"solid",
                        "border-color":"rgba(200,200,200,0.5)",
                        opacity:0.5
                    }
                },
                parent: btnCaptionOnOffDiv
            });

            var focusCaptionOnOffBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_caption_onoff_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 400, height: 65,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnCaptionOnOffDiv
            });

            var textCaptionOnOff = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_capton_on_off",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 20, width: 400 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center", "letter-spacing":-1.5
                    }
                },
                text: "켜기",
                parent: btnCaptionOnOffDiv
            });


            var arwCaptionOnOffLeft = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_l_f.png",
                    css: {
                        position: "absolute",
                        left: 2,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnCaptionOnOffDiv
            });

            var arwCaptionOnOffRight = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_r_f.png",
                    css: {
                        position: "absolute",
                        left: 353,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnCaptionOnOffDiv
            });

            btnCaptionOnOff = {
                root_div : btnCaptionOnOffDiv,
                default_btn : defaultCaptionOnOffBtn,
                focus_btn : focusCaptionOnOffBtn,
                text_btn : textCaptionOnOff,
                arw_left : arwCaptionOnOffLeft,
                arw_right : arwCaptionOnOffRight
            };

            startTitlePosY += 128;
            startBtnPosY += 128;


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "fav_lang_title",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 1415-1366, top: startTitlePosY, width: 200 , height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                    }
                },
                text: "방송사 제공 언어",
                parent: popupCaptionSettingDiv
            });

            var btnFavLangSelectDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1416-1366, top: startBtnPosY, overflow:"visible"
                    }
                },
                parent: popupCaptionSettingDiv
            });

            var defaultFavLangSelectBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_fav_lang_select_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:396, height: 61,
                        display: "none" ,
                        "background-color": "rgba(160,160,160,1)",
                        border:2,
                        "border-style":"solid",
                        "border-color":"rgba(200,200,200,0.5)",
                        opacity:0.5
                    }
                },
                parent: btnFavLangSelectDiv
            });

            var focusFavLangSelectBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_fav_lang_select_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 400, height: 65,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnFavLangSelectDiv
            });

            var textFavLangSelect = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_fav_lang_select",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 20, width: 400 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center", "letter-spacing":-1.5
                    }
                },
                text: "",
                parent: btnFavLangSelectDiv
            });


            var arwFavLangSelectLeft = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_l_f.png",
                    css: {
                        position: "absolute",
                        left: 2,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnFavLangSelectDiv
            });

            var arwFavLangSelectRight = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_r_f.png",
                    css: {
                        position: "absolute",
                        left: 353,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnFavLangSelectDiv
            });

            btnFavLangSelect = {
                root_div : btnFavLangSelectDiv,
                default_btn : defaultFavLangSelectBtn,
                focus_btn : focusFavLangSelectBtn,
                text_btn : textFavLangSelect,
                arw_left : arwFavLangSelectLeft,
                arw_right : arwFavLangSelectRight
            };
            startTitlePosY += 128;
            startBtnPosY += 128;

            if(isTranslationLanguageCaptionSupport === true) {

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "fav_lang_title",
                        class: "font_m" ,
                        css: {
                            position: "absolute", left: 1415-1366, top: startTitlePosY, width: 200 , height: 26,
                            color: "rgba(221, 175, 120, 1)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                        }
                    },
                    text: "자동번역 언어",
                    parent: popupCaptionSettingDiv
                });

                var btnTranslationLangSelectDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute", left: 1415-1366, top: startBtnPosY, overflow:"visible"
                        }
                    },
                    parent: popupCaptionSettingDiv
                });


                var defaultTranslationLangSelectBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "default_translation_lang_select_btn",
                        css: {
                            position: "absolute",
                            left: 0, top: 0, width:396, height: 61,
                            display: "none" ,
                            "background-color": "rgba(160,160,160,1)",
                            border:2,
                            "border-style":"solid",
                            "border-color":"rgba(200,200,200,0.5)",
                            opacity:0.5
                        }
                    },
                    parent: btnTranslationLangSelectDiv
                });

                var focusTranslationLangSelectBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_translation_lang_select_btn",
                        css: {
                            position: "absolute", left: 0, top: 0, width: 400, height: 65,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnTranslationLangSelectDiv
                });


                var textTranslationLangSelect = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_fav_lang_select",
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 28, top: 20, width: 320 , height: 32,
                            color: "rgba(255, 255, 255, 1)", "font-size": 26,"text-align" : "left", "letter-spacing":-1.3
                        }
                    },
                    text: "",
                    parent: btnTranslationLangSelectDiv
                });

                var arwTranslationLangSelectDown = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "arw_dw",
                        src: "images/popup/arw_option_popup_dw.png",
                        css: {
                            position: "absolute",
                            left: 338,
                            top: 8,
                            width: 45,
                            height: 45
                        }
                    },
                    parent: btnTranslationLangSelectDiv
                });


                btnTranslationLangSelect = {
                    root_div : btnTranslationLangSelectDiv,
                    default_btn : defaultTranslationLangSelectBtn,
                    focus_btn : focusTranslationLangSelectBtn,
                    text_btn : textTranslationLangSelect,
                    arw_dw : arwTranslationLangSelectDown
                };

                startTitlePosY += 128;
                startBtnPosY += 128;
            }

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "font_size_title",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 1415-1366, top: startTitlePosY, width: 100 , height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                    }
                },
                text: "글자크기",
                parent: popupCaptionSettingDiv
            });


            var btnFontSizeDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1415-1366, top: startBtnPosY,  overflow:"visible"
                    }
                },
                parent: popupCaptionSettingDiv
            });

            var defaultFontSizeBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_font_size_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:396, height: 61,
                        display: "none" ,
                        "background-color": "rgba(160,160,160,1)",
                        border:2,
                        "border-style":"solid",
                        "border-color":"rgba(200,200,200,0.5)",
                        opacity:0.5
                    }
                },
                parent: btnFontSizeDiv
            });

            var focusFontSizeBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_font_size_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 400, height: 65,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnFontSizeDiv
            });

            var textFontSize = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_font_size",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 20, width: 400 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center", "letter-spacing":-1.5
                    }
                },
                text: "",
                parent: btnFontSizeDiv
            });


            var arwFontSizeLeft = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_l_f.png",
                    css: {
                        position: "absolute",
                        left: 2,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnFontSizeDiv
            });

            var arwFontSizeRight = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_r_f.png",
                    css: {
                        position: "absolute",
                        left: 353,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnFontSizeDiv
            });

            btnFontSize = {
                root_div : btnFontSizeDiv,
                default_btn : defaultFontSizeBtn,
                focus_btn : focusFontSizeBtn,
                text_btn : textFontSize,
                arw_left : arwFontSizeLeft,
                arw_right : arwFontSizeRight
            };

            startTitlePosY += 128;
            startBtnPosY += 128;

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "caption_service_title",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 1415-1366, top: startTitlePosY, width: 200 , height: 26,
                        color: "rgba(221, 175, 120, 1)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                    }
                },
                text: "자막서비스",
                parent: popupCaptionSettingDiv
            });

            var btnCaptionServiceDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1415-1366, top: startBtnPosY, overflow:"visible"
                    }
                },
                parent: popupCaptionSettingDiv
            });
            

            var defaultCaptionServiceBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_caption_service_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:396, height: 61,
                        display: "none" ,
                        "background-color": "rgba(160,160,160,1)",
                        border:2,
                        "border-style":"solid",
                        "border-color":"rgba(200,200,200,0.5)",
                        opacity:0.5
                    }
                },
                parent: btnCaptionServiceDiv
            });



            var focusCaptionServiceBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_caption_service_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 400, height: 65,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnCaptionServiceDiv
            });

            var textCaptionService = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_caption_service",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 20, width: 400 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "center", "letter-spacing":-1.5
                    }
                },
                text: "1",
                parent: btnCaptionServiceDiv
            });


            var arwCaptionServiceLeft = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_l_f.png",
                    css: {
                        position: "absolute",
                        left: 2,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnCaptionServiceDiv
            });

            var arwCaptionServiceRight = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "arw_left",
                    src: "images/popup/arw_option_select_r_f.png",
                    css: {
                        position: "absolute",
                        left: 353,
                        top: 8,
                        width: 45,
                        height: 45
                    }
                },
                parent: btnCaptionServiceDiv
            });

            btnCaptionService = {
                root_div : btnCaptionServiceDiv,
                default_btn : defaultCaptionServiceBtn,
                focus_btn : focusCaptionServiceBtn,
                text_btn : textCaptionService,
                arw_left : arwCaptionServiceLeft,
                arw_right : arwCaptionServiceRight
            };

            startTitlePosY += 128;
            startBtnPosY += 128;

            var btnSaveDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1415-1366, top: startTitlePosY, width: 195, height: 62
                    }
                },
                parent: popupCaptionSettingDiv
            });


            // var defaultSaveBtn = util.makeElement({
            //     tag: "<img />",
            //     attrs: {
            //         id: "default_save_btn",
            //         src: "images/popup/pop_btn_w145.png",
            //         css: {
            //             position: "absolute",
            //             left: 0,
            //             top: 0,
            //             width: 145,
            //             height: 62
            //         }
            //     },
            //     parent: btnSaveDiv
            // });

            var defaultSaveBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_save_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:191, height: 58,
                        display: "none" ,
                        "background-color": "rgba(7,7,7,1)",
                        border:2,
                        "border-style":"solid",
                        "border-color":"rgba(200,200,200,0.5)",
                        opacity:0.5
                    }
                },
                parent: btnSaveDiv
            });

            var focusSaveBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_save_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 195, height: 62,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnSaveDiv
            });

            var textSave = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_save",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 940-923, width: 195 , height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center"
                    }
                },
                text: "저장",
                parent: btnSaveDiv
            });


            btnSave = {
                root_div : btnSaveDiv,
                default_btn : defaultSaveBtn,
                focus_btn : focusSaveBtn,
                text_btn : textSave
            };


            var btnCancelDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1620-1366, top: startTitlePosY, width: 195, height: 62
                    }
                },
                parent: popupCaptionSettingDiv
            });

            //
            // var defaultCancelBtn = util.makeElement({
            //     tag: "<img />",
            //     attrs: {
            //         id: "default_cancel_btn",
            //         src: "images/popup/pop_btn_w145.png",
            //         css: {
            //             position: "absolute",
            //             left: 0,
            //             top: 0,
            //             width: 145,
            //             height: 62
            //         }
            //     },
            //     parent: btnCancelDiv
            // });

            var defaultCancelBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "default_cancel_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:191, height: 58,
                        display: "none" ,
                        "background-color": "rgba(7,7,7,1)",
                        border:2,
                        "border-style":"solid",
                        "border-color":"rgba(200,200,200,0.5)",
                        opacity:0.5
                    }
                },
                parent: btnCancelDiv
            });

            var focusCancelBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_cancel_btn",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 195, height: 62,
                        "background-color": "rgba(210,51,47,1)",display:"none"
                    }
                },
                parent: btnCancelDiv
            });

            var textCancel = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_cancel",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 0, top: 940-923, width: 195 , height: 32,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center"
                    }
                },
                text: "취소",
                parent: btnCancelDiv
            });


            btnCancel = {
                root_div : btnCancelDiv,
                default_btn : defaultCancelBtn,
                focus_btn : focusCancelBtn,
                text_btn : textCancel
            };



            if(isTranslationLanguageCaptionSupport === true) {
                var dropDownDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "translation_lang_dropdown_div",
                        css: {
                            position: "absolute", left: 9, top: 375, width: 484, height: 401
                        }
                    },
                    parent: popupCaptionSettingDiv
                });


                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_top_img" ,
                        src: "images/popup/sdw_option_popup_cap_top.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 0,
                            width: 483,
                            height: 102
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_middle_img" ,
                        src: "images/popup/sdw_option_popup_cap_m.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102,
                            width: 483,
                            height: 184
                        }
                    },
                    parent: dropDownDiv
                });

                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "dropdown_btm_img" ,
                        src: "images/popup/sdw_option_popup_cap_btm.png",
                        css: {
                            position: "absolute",
                            left: 0 ,
                            top: 102+184,
                            width: 484,
                            height: 110
                        }
                    },
                    parent: dropDownDiv
                });




                var dropboxItemList = [];
                var tempY = 37;
                for(var x=0;x<5;x++) {
                    var btnDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "drop_box_btn_" + (x+1),
                            css: {
                                position: "absolute",
                                left: 40, top: tempY, width:400, height: 65
                            }
                        },
                        parent: dropDownDiv
                    });


                    var defaultImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: x === 0 ? "images/popup/bg_option_dropbox.png" : "images/popup/bg_option_dropbox_2.png",
                            css: {
                                position: "absolute",
                                left: 0 ,
                                top: 0,
                                width: 400,
                                height: 65
                            }
                        },
                        parent: btnDiv
                    });

                    var btnFocusDiv = util.makeElement({
                        tag: "<div />",
                        attrs: {
                            id: "" ,
                            css: {
                                position: "absolute",
                                left: 0, top: 0, width:400, height: 65,
                                "background-color": "rgba(210,51,47,1)",
                                display: "none"
                            }
                        },
                        parent: btnDiv
                    });


                    var defaultText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_l",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 320, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 26 , "text-align": "left", "letter-spacing":-1.3
                            }
                        },
                        text: "",
                        parent: btnDiv
                    });

                    var focusText = util.makeElement({
                        tag: "<span />",
                        attrs: {
                            id: "",
                            class: "font_m",
                            css: {
                                position: "absolute", left: 32 , top: 19 , width: 320, height: 32,
                                color: "rgba(255, 255, 255, 1)", "font-size": 26 , "text-align": "left", "letter-spacing":-1.3
                            }
                        },
                        text: "",
                        parent: btnDiv
                    });


                    var checkedImg = util.makeElement({
                        tag: "<img />",
                        attrs: {
                            id: "" ,
                            src: "images/popup/option_dropbox_select.png",
                            css: {
                                position: "absolute",
                                left: 338 ,
                                top: 10,
                                width: 45,
                                height: 45
                            }
                        },
                        parent: btnDiv
                    });

                    var dropboxItem = {
                        root_div : btnDiv,
                        default_img : defaultImg,
                        focus_div : btnFocusDiv,
                        default_text : defaultText,
                        focus_text : focusText ,
                        checked_img : checkedImg
                    };

                    dropboxItemList[dropboxItemList.length] = dropboxItem;

                    tempY+=65;
                }

                translationDropDown = {
                    root_div : dropDownDiv,
                    dropbox_item_list :   dropboxItemList
                };

                translationDropDown.root_div.css("display" , "none");


                scrollbarbackground = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "popup_container",
                        css: {
                            position: "absolute",
                            left: 432, top: 43, width: 2, height: 305,
                            "background-color": "rgba(110, 110, 110, 1)"
                        }
                    },
                    parent: dropDownDiv
                });

                scrollbarforeground = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "popup_container",
                        css: {
                            position: "absolute",
                            left: 431, top: 43, width: 5, height: 0,
                            "background-color": "rgba(180, 180, 180, 1)"
                        }
                    },
                    parent: dropDownDiv
                });

            }
        }


        function _focusMoveTranslationLangDropDown(isUp) {

            var totalItemCount = translationItemList.length;

            var itemList = translationDropDown.dropbox_item_list;

            var item = translationDropDown.dropbox_item_list[focusdropdownIndex];

            if(item !== null) {
                item.default_img.css("display" , "");
                item.default_text.css("display" , "");

                item.focus_div.css("display" , "none");
                item.focus_text.css("display" , "none");

                if(selectTranslationIndex=== ((curPageTranslationLang-1)*5 + focusdropdownIndex)) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select.png");
                }
            }

            var isPageUpdate = false;


            if(curPageTranslationLang === totalPageTranslationLang) {
                if(isUp === true) {
                    focusdropdownIndex--;
                }else {
                    focusdropdownIndex++;
                }

                var temp = totalItemCount - (5*(curPageTranslationLang-1));

                if(focusdropdownIndex<0) {
                    isPageUpdate = true;
                    curPageTranslationLang--;
                    if(curPageTranslationLang<=0) {
                        curPageTranslationLang = totalPageTranslationLang;
                        var temp = totalItemCount - (5*(curPageTranslationLang-1));
                        focusdropdownIndex = temp - 1;
                    }else {
                        focusdropdownIndex = 4;
                    }
                }else if(focusdropdownIndex>=temp) {
                    isPageUpdate = true;
                    curPageTranslationLang = 1;
                    focusdropdownIndex = 0;
                }

            }else {
                if(isUp === true) {
                    focusdropdownIndex--;
                }else {
                    focusdropdownIndex++;
                }

                if(focusdropdownIndex<0) {
                    isPageUpdate = true;
                    curPageTranslationLang--;
                    if(curPageTranslationLang<=0) {
                        curPageTranslationLang = totalPageTranslationLang;
                        var temp = totalItemCount - (5*(curPageTranslationLang-1));
                        focusdropdownIndex = temp - 1;
                    }else {
                        focusdropdownIndex = 4;
                    }
                }else if(focusdropdownIndex>=5) {
                    isPageUpdate = true;
                    curPageTranslationLang++;
                    focusdropdownIndex = 0;
                }
            }

            if(isPageUpdate === true) {
                var countList = 5;
                for(var i=0;i<itemList.length;i++) {
                    item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.text("");
                    item.focus_text.text("");
                    item.checked_img.css("display" , "none");
                    item.focus_text.css("display" , "none");
                    item.focus_div.css("display" , "none");
                }

                if(curPageTranslationLang === totalPageTranslationLang) {
                    countList = totalItemCount - (5*(totalPageTranslationLang-1));

                }else {
                    countList = 5;
                }

                for(var i=0;i<countList;i++) {
                    item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.text(_getTranslationLang((curPageTranslationLang-1)*5 + i));
                    item.focus_text.text(_getTranslationLang((curPageTranslationLang-1)*5 + i));

                    item.default_text.css("display" , "");

                    item.focus_div.css("display" , "none");
                    item.focus_text.css("display" , "none");

                    item.checked_img.css("display" , "none");

                    if(i === focusdropdownIndex) {
                        item.default_img.css("display" , "none");
                        item.default_text.css("display" , "none");

                        item.focus_div.css("display" , "");
                        item.focus_text.css("display" , "");
                    }

                    if(selectTranslationIndex === ((curPageTranslationLang-1)*5 + i)) {
                        item.checked_img.css("display" , "");
                        item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");
                    }
                }

                var scrollTotalHeight = 305;
                var scrollHeight = Math.floor(scrollTotalHeight / totalPageTranslationLang);
                scrollbarforeground.css({top:43+(curPageTranslationLang-1)*scrollHeight , height:scrollHeight});

            }else {
                var item = translationDropDown.dropbox_item_list[focusdropdownIndex];

                item.default_img.css("display" , "none");
                item.default_text.css("display" , "none");

                item.focus_div.css("display" , "");
                item.focus_text.css("display" , "");

                if(selectTranslationIndex === ((curPageTranslationLang-1)*5 + focusdropdownIndex)) {
                    item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");
                }
            }
        }

        function _hideTranslationLangDropDown() {
            if(isShowTranslationLangDropDown === true) {
                isShowTranslationLangDropDown = false;
                translationDropDown.root_div.css("display" , "none");

                btnTranslationLangSelect.root_div.css("display" , "");
            }
        }

        function _showTranslationLangDropDown() {
            isShowTranslationLangDropDown = true;
            var item = null;
            var itemList = translationDropDown.dropbox_item_list;

            if(itemList !== null && itemList.length>0) {
                for(var i=0;i<itemList.length;i++) {
                    item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.text("");
                    item.focus_text.text("");
                    item.checked_img.css("display" , "none");
                    item.focus_text.css("display" , "none");
                    item.focus_div.css("display" , "none");
                }

                var countList = 5;
                var totalItemCount = translationItemList.length;

                totalPageTranslationLang = Math.floor(totalItemCount / countList);
                if (totalItemCount % countList > 0) {
                    totalPageTranslationLang++;
                }


                curPageTranslationLang =  Math.floor((selectTranslationIndex+1) / countList);
                if ((selectTranslationIndex+1) % countList > 0) {
                    curPageTranslationLang++;
                }
                
                if(curPageTranslationLang === totalPageTranslationLang) {
                    countList = totalItemCount - (5*(totalPageTranslationLang-1));
                    
                }else {
                    countList = 5;
                }

                for(var i=0;i<countList;i++) {
                    item = itemList[i];
                    item.default_img.css("display" , "");
                    item.default_text.text(_getTranslationLang((curPageTranslationLang-1)*5 + i));
                    item.focus_text.text(_getTranslationLang((curPageTranslationLang-1)*5 + i));
                    
                    item.default_text.css("display" , "");

                    item.focus_div.css("display" , "none");
                    item.focus_text.css("display" , "none");

                    item.checked_img.css("display" , "none");

                    if(selectTranslationIndex=== ((curPageTranslationLang-1)*5 + i)) {
                        item.default_img.css("display" , "none");
                        item.default_text.css("display" , "none");
                    
                        item.focus_div.css("display" , "");
                        item.focus_text.css("display" , "");
                    
                        item.checked_img.css("display" , "");
                    
                        item.checked_img.attr("src", "images/popup/option_dropbox_select_f.png");
                        focusdropdownIndex = i;
                    }
                }
            }

            var scrollTotalHeight = 305;
            var scrollHeight = Math.floor(scrollTotalHeight / totalPageTranslationLang);
            scrollbarforeground.css({top:43+(curPageTranslationLang-1)*scrollHeight , height:scrollHeight});

            translationDropDown.root_div.css("display" , "");

            btnTranslationLangSelect.root_div.css("display" , "none");
        }


        function _readData() {
            try {
                on_off = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_ONOFF);
                language = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_LANGUAGE);
                size = KTW.oipf.AdapterHandler.basicAdapter.getConfigText(KTW.oipf.Def.CONFIG.KEY.SUBTITLE_SIZE);
            }
            catch (e) {
                on_off = DEFAULT_ON_OFF;
                language = DEFAULT_LANG;
                size = DEFAULT_SIZE;
            }

            if (on_off == KTW.oipf.Def.CLOSED_CAPTION.ON_OFF.ON) {
                focusOnOffIndex = 0;
            } else {
                focusOnOffIndex = 1;
            }

            if (language == KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.KOREAN) {
                focusLangIndex = 0;
            } else if (language == KTW.oipf.Def.CLOSED_CAPTION.LANGUAGE.ENGLISH) {
                focusLangIndex = 1;
            } else {
                focusLangIndex = 2;
            }

            if (size == KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.BASIC)
                focusSizeIndex = 1;
            else if (size == KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.SMALL)
                focusSizeIndex = 0;
            else if (size == KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.MEDIUM)
                focusSizeIndex = 1;
            else if (size == KTW.oipf.Def.CLOSED_CAPTION.FONT_SIZE.LARGE)
                focusSizeIndex = 2;
        }

        function _UIUpdate() {

            if(btnCaptionOnOff !== null) {
                btnCaptionOnOff.default_btn.css("display" , "");
                btnCaptionOnOff.focus_btn.css("display" , "none");
                btnCaptionOnOff.text_btn.removeClass("font_l");
                btnCaptionOnOff.text_btn.text(selectCaptionBox[focusOnOffIndex]);
                btnCaptionOnOff.text_btn.addClass("font_m");

                btnCaptionOnOff.arw_left.css("display" , "none");
                btnCaptionOnOff.arw_right.css("display" , "none");
            }

            if(btnFavLangSelect !== null) {
                btnFavLangSelect.default_btn.css("display" , "");
                btnFavLangSelect.focus_btn.css("display" , "none");
                btnFavLangSelect.text_btn.text(selectLangBox[focusLangIndex]);

                btnFavLangSelect.arw_left.css("display" , "none");
                btnFavLangSelect.arw_right.css("display" , "none");
            }

            if(isTranslationLanguageCaptionSupport === true) {

                if(btnTranslationLangSelect !== null) {
                    btnTranslationLangSelect.default_btn.css("display" , "");
                    btnTranslationLangSelect.focus_btn.css("display" , "none");
                    
                    var lang = _getTranslationLang(selectTranslationIndex);
                    btnTranslationLangSelect.text_btn.text(lang);
                }
            }

            if(btnFontSize !== null) {
                btnFontSize.default_btn.css("display" , "");
                btnFontSize.focus_btn.css("display" , "none");
                btnFontSize.text_btn.text(selectFontSizeBox[focusSizeIndex]);
                btnFontSize.arw_left.css("display" , "none");
                btnFontSize.arw_right.css("display" , "none");
            }

            if(btnCaptionService !== null) {
                focusCaptionServiceIndex = 0;
                btnCaptionService.default_btn.css("display" , "");
                btnCaptionService.focus_btn.css("display" , "none");
                btnCaptionService.text_btn.text(selectCaptionServiceBox[focusCaptionServiceIndex]);
                btnCaptionService.arw_left.css("display" , "none");
                btnCaptionService.arw_right.css("display" , "none");
            }


            if(btnSave !== null) {
                btnSave.default_btn.css("display", "");
                btnSave.focus_btn.css("display", "none");
                btnSave.text_btn.css("color", "rgba(255,255,255,1)");
            }

            if(btnCancel !== null){
                btnCancel.default_btn.css("display" , "");
                btnCancel.focus_btn.css("display" , "none");
                btnCancel.text_btn.css("color" , "rgba(255,255,255,1)");
            }

        }
        
        function _getTranslationLang(idx) {
            var lang = "";
            if(idx !== -1) {
                lang = translationItemList[idx].name;
            }

            return lang;
        }
        
        
        function _moveLeftRight(isLeft){
            if(focusbtnIndex === CAPTION_ON_OFF_BTN_INDEX || focusbtnIndex === LANG_SELECT_BTN_INDEX
                || focusbtnIndex === FONT_SIZE_BTN_INDEX || focusbtnIndex === CAPTION_SERVICE_BTN_INDEX){
                if(isLeft) {
                    if(focusbtnIndex === CAPTION_ON_OFF_BTN_INDEX) {
                        focusOnOffIndex--;
                        if(focusOnOffIndex<0) {
                            focusOnOffIndex = selectCaptionBox.length-1;
                        }
                        btnCaptionOnOff.text_btn.text(selectCaptionBox[focusOnOffIndex]);

                        if(focusOnOffIndex === 1) {
                            btnFavLangSelect.root_div.css({opacity:0.3});
                            if(isTranslationLanguageCaptionSupport === true) {
                                btnTranslationLangSelect.root_div.css({opacity:0.3});
                            }
                            btnFontSize.root_div.css({opacity:0.3});
                            btnCaptionService.root_div.css({opacity:0.3});
                        }else {
                            btnFavLangSelect.root_div.css({opacity:1});
                            btnFavLangSelect.default_btn.css({opacity:0.5});
                            btnFavLangSelect.text_btn.css({opacity:1});

                            if(isTranslationLanguageCaptionSupport === true) {
                                btnTranslationLangSelect.root_div.css({opacity:1});
                                btnTranslationLangSelect.default_btn.css({opacity:0.5});
                                btnTranslationLangSelect.text_btn.css({opacity:1});


                                if(selectTranslationIndex !== 0) {
                                    btnFavLangSelect.root_div.css({opacity:0.3});
                                }
                            }

                            btnFontSize.root_div.css({opacity:1});
                            btnFontSize.default_btn.css({opacity:0.5});
                            btnFontSize.text_btn.css({opacity:1});

                            btnCaptionService.root_div.css({opacity:1});
                            btnCaptionService.default_btn.css({opacity:0.5});
                            btnCaptionService.text_btn.css({opacity:1});
                        }

                    }else if(focusbtnIndex === LANG_SELECT_BTN_INDEX) {
                        focusLangIndex--;
                        if(focusLangIndex<0) {
                            focusLangIndex = selectLangBox.length-1;
                        }
                        btnFavLangSelect.text_btn.text(selectLangBox[focusLangIndex]);
                    }else if(focusbtnIndex === FONT_SIZE_BTN_INDEX) {
                        focusSizeIndex--;
                        if(focusSizeIndex<0) {
                            focusSizeIndex = selectFontSizeBox.length-1;
                        }
                        btnFontSize.text_btn.text(selectFontSizeBox[focusSizeIndex]);
                    }else if(focusbtnIndex === CAPTION_SERVICE_BTN_INDEX) {
                        focusCaptionServiceIndex--;
                        if(focusCaptionServiceIndex<0) {
                            focusCaptionServiceIndex = selectCaptionServiceBox.length-1;
                        }
                        btnCaptionService.text_btn.text(selectCaptionServiceBox[focusCaptionServiceIndex]);
                    }
                }else {
                    if(focusbtnIndex === CAPTION_ON_OFF_BTN_INDEX) {
                        focusOnOffIndex++;
                        if(focusOnOffIndex>(selectCaptionBox.length-1)) {
                            focusOnOffIndex = 0;
                        }
                        btnCaptionOnOff.text_btn.text(selectCaptionBox[focusOnOffIndex]);

                        if(focusOnOffIndex === 1) {
                            btnFavLangSelect.root_div.css({opacity:0.3});
                            if(isTranslationLanguageCaptionSupport === true) {
                                btnTranslationLangSelect.root_div.css({opacity:0.3});
                            }
                            btnFontSize.root_div.css({opacity:0.3});
                            btnCaptionService.root_div.css({opacity:0.3});
                        }else {
                            btnFavLangSelect.root_div.css({opacity:1});
                            if(isTranslationLanguageCaptionSupport === true) {
                                btnTranslationLangSelect.root_div.css({opacity:1});

                                if(selectTranslationIndex !== 0) {
                                    btnFavLangSelect.root_div.css({opacity:0.3});
                                }
                            }
                            btnFontSize.root_div.css({opacity:1});
                            btnCaptionService.root_div.css({opacity:1});
                        }

                    }else if(focusbtnIndex === LANG_SELECT_BTN_INDEX) {
                        focusLangIndex++;
                        if(focusLangIndex>(selectLangBox.length-1)) {
                            focusLangIndex = 0;
                        }
                        btnFavLangSelect.text_btn.text(selectLangBox[focusLangIndex]);
                    }else if(focusbtnIndex === FONT_SIZE_BTN_INDEX) {
                        focusSizeIndex++;
                        if(focusSizeIndex>(selectFontSizeBox.length-1)) {
                            focusSizeIndex = 0;
                        }
                        btnFontSize.text_btn.text(selectFontSizeBox[focusSizeIndex]);

                    }else if(focusbtnIndex === CAPTION_SERVICE_BTN_INDEX) {
                        focusCaptionServiceIndex++;
                        if(focusCaptionServiceIndex>(selectCaptionServiceBox.length-1)) {
                            focusCaptionServiceIndex = 0;
                        }
                        btnCaptionService.text_btn.text(selectCaptionServiceBox[focusCaptionServiceIndex]);
                    }
                }
            }else {
                if(focusbtnIndex === SAVE_BTN_INDEX) {
                    btnSave.default_btn.css("display" , "");
                    btnSave.focus_btn.css("display" , "none");
                    btnSave.text_btn.css("color" , "rgba(255,255,255,1)");

                    btnSave.text_btn.removeClass("font_m");
                    btnSave.text_btn.addClass("font_l");

                    focusbtnIndex = CANCEL_BTN_INDEX;
                }else if(focusbtnIndex === CANCEL_BTN_INDEX){
                    btnCancel.default_btn.css("display" , "");
                    btnCancel.focus_btn.css("display" , "none");
                    btnCancel.text_btn.css("color" , "rgba(255,255,255,1)");
                    btnCancel.text_btn.removeClass("font_m");
                    btnCancel.text_btn.addClass("font_l");
                    focusbtnIndex = SAVE_BTN_INDEX;
                }

                if(focusbtnIndex === SAVE_BTN_INDEX) {
                    btnSave.default_btn.css("display" , "none");
                    btnSave.focus_btn.css("display" , "");
                    btnSave.text_btn.css("color" , "rgba(255,255,255,1)");
                    btnSave.text_btn.removeClass("font_l");
                    btnSave.text_btn.addClass("font_m");


                }else if(focusbtnIndex === CANCEL_BTN_INDEX) {
                    btnCancel.default_btn.css("display" , "none");
                    btnCancel.focus_btn.css("display" , "");
                    btnCancel.text_btn.css("color" , "rgba(255,255,255,1)");
                    btnCancel.text_btn.removeClass("font_l");
                    btnCancel.text_btn.addClass("font_m");
                }

            }
        }

        function _moveUpDown(isUpKey){

            if(focusbtnIndex === CAPTION_ON_OFF_BTN_INDEX || focusbtnIndex === LANG_SELECT_BTN_INDEX
                || focusbtnIndex === LANG_TRANSLATION_BTN_INDEX || focusbtnIndex === FONT_SIZE_BTN_INDEX
                || focusbtnIndex === CAPTION_SERVICE_BTN_INDEX) {
                if(focusbtnIndex === CAPTION_ON_OFF_BTN_INDEX) {
                    btnCaptionOnOff.default_btn.css("display" , "");
                    btnCaptionOnOff.focus_btn.css("display" , "none");

                    btnCaptionOnOff.text_btn.css("font-size" , 27);
                    btnCaptionOnOff.text_btn.removeClass("font_m");
                    btnCaptionOnOff.text_btn.addClass("font_l");
                    btnCaptionOnOff.text_btn.css("color" , "rgba(255,255,255,1)");


                    btnCaptionOnOff.arw_left.css("display" , "none");
                    btnCaptionOnOff.arw_right.css("display" , "none");
                }else if(focusbtnIndex === LANG_SELECT_BTN_INDEX) {
                    btnFavLangSelect.default_btn.css("display", "");
                    btnFavLangSelect.focus_btn.css("display", "none");

                    btnFavLangSelect.text_btn.removeClass("font_m");
                    btnFavLangSelect.text_btn.addClass("font_l");

                    btnFavLangSelect.text_btn.css("font-size", 27);
                    btnFavLangSelect.text_btn.css("color", "rgba(255,255,255,1)");


                    btnFavLangSelect.arw_left.css("display", "none");
                    btnFavLangSelect.arw_right.css("display", "none");
                } else if(isTranslationLanguageCaptionSupport === true && focusbtnIndex === LANG_TRANSLATION_BTN_INDEX) {
                    btnTranslationLangSelect.default_btn.css("display", "");
                    btnTranslationLangSelect.focus_btn.css("display", "none");

                    btnTranslationLangSelect.text_btn.removeClass("font_m");
                    btnTranslationLangSelect.text_btn.addClass("font_l");

                    btnTranslationLangSelect.text_btn.css("font-size", 26);
                    btnTranslationLangSelect.text_btn.css("color", "rgba(255,255,255,1)");

                } else if(focusbtnIndex === FONT_SIZE_BTN_INDEX) {
                    btnFontSize.default_btn.css("display" , "");
                    btnFontSize.focus_btn.css("display" , "none");

                    btnFontSize.text_btn.removeClass("font_m");
                    btnFontSize.text_btn.addClass("font_l");
                    btnFontSize.text_btn.css("font-size" , 27);
                    btnFontSize.text_btn.css("color" , "rgba(255,255,255,1)");

                    btnFontSize.arw_left.css("display" , "none");
                    btnFontSize.arw_right.css("display" , "none");
                } else if(focusbtnIndex === CAPTION_SERVICE_BTN_INDEX) {
                    btnCaptionService.default_btn.css("display" , "");
                    btnCaptionService.focus_btn.css("display" , "none");

                    btnCaptionService.text_btn.removeClass("font_m");
                    btnCaptionService.text_btn.addClass("font_l");
                    btnCaptionService.text_btn.css("font-size" , 27);
                    btnCaptionService.text_btn.css("color" , "rgba(255,255,255,1)");

                    btnCaptionService.arw_left.css("display" , "none");
                    btnCaptionService.arw_right.css("display" , "none");
                }
            }
            else {
                if(focusbtnIndex === SAVE_BTN_INDEX) {
                    btnSave.default_btn.css("display" , "");
                    btnSave.focus_btn.css("display" , "none");

                    btnSave.text_btn.removeClass("font_m");
                    btnSave.text_btn.addClass("font_l");
                    btnSave.text_btn.css("color" , "rgba(255,255,255,1)");
                }else if(focusbtnIndex === CANCEL_BTN_INDEX){
                    btnCancel.text_btn.removeClass("font_m");
                    btnCancel.text_btn.addClass("font_l");
                    
                    btnCancel.default_btn.css("display" , "");
                    btnCancel.focus_btn.css("display" , "none");
                    btnCancel.text_btn.css("color" , "rgba(255,255,255,1)");
                }
            }

            if(isUpKey === true) {
                if(focusbtnIndex === SAVE_BTN_INDEX) {
                    if (focusOnOffIndex === 1) {
                        focusbtnIndex = CAPTION_ON_OFF_BTN_INDEX;
                    } else {
                        focusbtnIndex = CAPTION_SERVICE_BTN_INDEX;
                    }
                }else if(focusbtnIndex === CANCEL_BTN_INDEX) {
                    if (focusOnOffIndex === 1) {
                        focusbtnIndex = CAPTION_ON_OFF_BTN_INDEX;
                    } else {
                        focusbtnIndex = CAPTION_SERVICE_BTN_INDEX;
                    }
                }else {
                    focusbtnIndex--;
                    if(focusbtnIndex === LANG_SELECT_BTN_INDEX) {
                        if(isTranslationLanguageCaptionSupport === true && selectTranslationIndex !== 0) {
                            focusbtnIndex--;
                        }
                    }
                }
            }else {
                if(focusbtnIndex === 0) {
                    if(focusOnOffIndex === 1) {
                        focusbtnIndex = SAVE_BTN_INDEX;
                    }else {
                        focusbtnIndex++;

                        if(focusbtnIndex === LANG_SELECT_BTN_INDEX) {
                            if(isTranslationLanguageCaptionSupport === true && selectTranslationIndex !== 0) {
                                focusbtnIndex++;
                            }
                        }
                    }
                }else {
                    focusbtnIndex++;
                }
            }

            if(focusbtnIndex<0) {
                focusbtnIndex = 0;
            }
            if(focusbtnIndex>5) {
                focusbtnIndex = 5;
            }

            if(focusbtnIndex === CAPTION_ON_OFF_BTN_INDEX) {
                btnCaptionOnOff.default_btn.css("display" , "none");
                btnCaptionOnOff.focus_btn.css("display" , "");

                btnCaptionOnOff.text_btn.css("font-size" , 30);
                btnCaptionOnOff.text_btn.css("color" , "rgba(255,255,255,1)");
                btnCaptionOnOff.text_btn.removeClass("font_l");
                btnCaptionOnOff.text_btn.addClass("font_m");
                

                btnCaptionOnOff.arw_left.css("display" , "");
                btnCaptionOnOff.arw_right.css("display" , "");
            }else if(focusbtnIndex === LANG_SELECT_BTN_INDEX){
                btnFavLangSelect.default_btn.css("display" , "none");
                btnFavLangSelect.focus_btn.css("display" , "");

                btnFavLangSelect.text_btn.css("font-size" , 30);
                btnFavLangSelect.text_btn.css("color" , "rgba(255,255,255,1)");
                btnFavLangSelect.text_btn.removeClass("font_l");
                btnFavLangSelect.text_btn.addClass("font_m");

                btnFavLangSelect.arw_left.css("display" , "");
                btnFavLangSelect.arw_right.css("display" , "");



            } else if(isTranslationLanguageCaptionSupport === true && focusbtnIndex === LANG_TRANSLATION_BTN_INDEX) {
                btnTranslationLangSelect.default_btn.css("display" , "none");
                btnTranslationLangSelect.focus_btn.css("display" , "");

                btnTranslationLangSelect.text_btn.css("font-size" , 26);
                btnTranslationLangSelect.text_btn.css("color" , "rgba(255,255,255,1)");
                btnTranslationLangSelect.text_btn.removeClass("font_l");
                btnTranslationLangSelect.text_btn.addClass("font_m");
                
            } else if(focusbtnIndex === FONT_SIZE_BTN_INDEX) {
                btnFontSize.default_btn.css("display" , "none");
                btnFontSize.focus_btn.css("display" , "");

                btnFontSize.text_btn.css("font-size" , 30);
                btnFontSize.text_btn.css("color" , "rgba(255,255,255,1)");
                btnFontSize.text_btn.removeClass("font_l");
                btnFontSize.text_btn.addClass("font_m");

                btnFontSize.arw_left.css("display" , "");
                btnFontSize.arw_right.css("display" , "");

            } else if(focusbtnIndex === CAPTION_SERVICE_BTN_INDEX) {
                btnCaptionService.default_btn.css("display" , "none");
                btnCaptionService.focus_btn.css("display" , "");

                btnCaptionService.text_btn.css("font-size" , 30);
                btnCaptionService.text_btn.css("color" , "rgba(255,255,255,1)");
                btnCaptionService.text_btn.removeClass("font_l");
                btnCaptionService.text_btn.addClass("font_m");
                

                btnCaptionService.arw_left.css("display" , "");
                btnCaptionService.arw_right.css("display" , "");
            }else if(focusbtnIndex === SAVE_BTN_INDEX) {
                btnSave.default_btn.css("display" , "none");
                btnSave.focus_btn.css("display" , "");
                btnSave.text_btn.css("color" , "rgba(255,255,255,1)");
                btnSave.text_btn.removeClass("font_l");
                btnSave.text_btn.addClass("font_m");
            }else if(focusbtnIndex === CANCEL_BTN_INDEX) {
                btnCancel.default_btn.css("display" , "none");
                btnCancel.focus_btn.css("display" , "");
                btnCancel.text_btn.css("color" , "rgba(255,255,255,1)");
                btnCancel.text_btn.removeClass("font_l");
                btnCancel.text_btn.addClass("font_m");
            }

        }

    };

    KTW.ui.view.popup.CaptionSettingPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.CaptionSettingPopup.prototype.constructor = KTW.ui.view.popup.CaptionSettingPopup;

    KTW.ui.view.popup.CaptionSettingPopup.prototype.create = function() {
        this.createView();
    };
    KTW.ui.view.popup.CaptionSettingPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.CaptionSettingPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.CaptionSettingPopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.CaptionSettingPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };
})();
