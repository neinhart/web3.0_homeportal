/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>TimeRestrictedPopup</code>
 *
 * 시청 시간 제한 팝업으로 SYSTEM POPUP
 */

"use strict";

(function() {
    KTW.ui.view.popup.TimeRestrictedPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;
        var authManager = KTW.managers.auth.AuthManager;

        var popup_container_div = null;
        var div = null;

        var currentTimeDiv = null;
        var textTitle = null;
        var textCurrentTime = null;
        var passwordInputBoxDiv = null;
        var textInputBox = null;

        var maxPasswordLength = 4;

        var clockTimer = null;

        var isStartSSpeechRecognizer = false;

        this.create = function () {
            log.printDbg("create()");
            div = this.parent.div;
            _createView(div);
        };

        this.show = function () {
            log.printDbg("show()");
            _showView();
            _updateCurrentTime();
            _setTimer();
        };

        this.hide = function () {
            log.printDbg("hide()");
            _hideView();
            _clearTimer();
        };

        this.destroy = function () {
            log.printDbg("destroy()");
        };

        this.controlKey = function (key_code) {
            log.printDbg("controlkey(), key_code = " + key_code );
            var consumed = false;

            switch (key_code) {
                case KTW.KEY_CODE.LEFT:
                    var pw = authManager.getPW();
                    if(pw !== null && pw.length>0) {
                        _deleteNum();
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.OK:
                    KTW.managers.service.SimpleMessageManager.hide();
                    _checkPW();
                    consumed = true;
                    break;
            }

            if(consumed === false) {
                var number = util.keyCodeToNumber(key_code);
                if (number >= 0) {
                    _inputNum(number);
                }
            }

            consumed = true;
            return consumed;
        };

        /**
         * 자세히 팝업을 위한 element 들을 생성한다.
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "time_restricted_popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "time_restricted_popup_background_img",
                    src: "images/iframe/iframe_bg.jpg",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: KTW.CONSTANT.RESOLUTION.WIDTH,
                        height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: popup_container_div
            });




            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "blocked_title",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 255, width: 1920, height: 93,
                        color: "rgba(255, 255, 255, 1)", "font-size": 90, "text-align": "center" , "letter-spacing":-4.5
                    }
                },
                text: "시청시간 제한",
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "blocked_english_title",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 357, width: 1920, height: 30,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 28 , "text-align" : "center"
                    }
                },
                text: "Time Restriction",
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "blocked_comment1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0, top: 432, width: 1920, height: 32,
                        color: "rgba(255, 255, 255, 0.8)", "font-size": 29, "text-align": "center" , "letter-spacing":-1.45
                    }
                },
                text: "시청을 원하시면 비밀번호를 입력해 주세요",
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "blocked_comment2",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0, top: 474, width: 1920, height: 32,
                        color: "rgba(255, 255, 255, 0.8)", "font-size": 29, "text-align": "center" , "letter-spacing":-1.45
                    }
                },
                text: "설정>자녀안심설정에서 설정 및 변경이 가능합니다",
                parent: popup_container_div
            });

            var miniguideDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 512, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 568
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "miniepg_background",
                    src: "images/miniepg/bg_miniguide.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 1920,
                        height: 568,
                        opacity:0.9
                    }
                },
                parent: miniguideDiv
            });


            var type_txt = KTW.CONSTANT.IS_OTS === true ? "olleh tv skylife" : "olleh tv";
            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 371, top: 748-512, width: 800, height: 38,
                        color: "rgba(255, 254, 254, 0.8)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: type_txt,
                parent: miniguideDiv
            });

            textTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 371, top: 801 - 512, width: 1110, height: 38,
                        color: "rgba(255, 255, 255, 1)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: "olleh tv 비밀번호(성인인증)를 입력해 주세요",
                parent: miniguideDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "current_program_progress_background_div",
                    css: {
                        position: "absolute", left: 370, top: (870 - 512), width: 1108, height: 6,
                        "background-color": "rgba(255,255,255,0.5)"
                    }
                },
                parent: miniguideDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "next_program_progress_background_div",
                    css: {
                        position: "absolute", left: (370 + 1108), top: (870 - 512), width: 442, height: 6,
                        "background-color": "rgba(255,255,255,0.3)"
                    }
                },
                parent: miniguideDiv
            });

            currentTimeDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 370, top: (895 - 512), width: 211, height: 26
                    }
                },
                parent: miniguideDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "current_program_age_img",
                    src: "images/icon/icon_current_time.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 26,
                        height: 26
                    }
                },
                parent: currentTimeDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 37, top: 2, width: 91, height: 26,
                        color: "rgba(255, 255, 255, 0.4)", "font-size": 24, "letter-spacing":-1.2
                    }
                },
                text: "현재시간",
                parent: currentTimeDiv
            });

            textCurrentTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "current_time",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 134, top: 2, width: 77, height: 26,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 26, "letter-spacing":-1.3
                    }
                },
                text: "",
                parent: currentTimeDiv
            });
           _createPasswordInputBoxElement(miniguideDiv);
        }

        /**
         * Blocked 채널에서 사용 할 Pin 인증 Input Box
         */
        function _createPasswordInputBoxElement(miniguideDiv) {
            log.printDbg("_createPasswordInputBoxElement()");

            passwordInputBoxDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 1178, top: 780-512 , width: 300, height: 76
                    }
                },
                parent: miniguideDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/miniepg/pw_box_w300_foc.png",
                    css: {
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 300,
                        height: 68
                    }
                },
                parent: passwordInputBoxDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 89, top: 19, width: 300, height: 47,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 45
                    }
                },
                text: "* * * *",
                parent: passwordInputBoxDiv
            });

            textInputBox = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_input",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 89, top: 19, width: 300, height: 47,
                        color: "rgba(255, 255, 255, 1)", "font-size": 45
                    }
                },
                text: "",
                parent: passwordInputBoxDiv
            });

        }

        function _showView() {
            log.printDbg("_showView()");

            authManager.initPW();
            // 2017.05.03 dhlee 오류 횟수 초기화
            authManager.resetPWCheckCount();
            _updateInputBox(authManager.getPW());
            _startSpeechRecognizer();
        }

        function _hideView() {
            log.printDbg("_hideView()");
            _stopSpeechRecognizer();
        }

        function _setTimer() {
            log.printDbg('setTimer()');

            if (clockTimer != null) {
                _clearTimer();
            }
            clockTimer = setInterval(_updateCurrentTime, 60*1000);
        }

        function _clearTimer() {
            log.printDbg('clearTimer()');
            if(clockTimer !== null) {
                clearInterval(clockTimer);
                clockTimer = null;
            }
        }

        function _updateCurrentTime() {
            var nowDate = new Date();
            textCurrentTime.text((nowDate.getHours() < 10 ? "0" : "") + nowDate.getHours() + ":" + (nowDate.getMinutes() < 10 ? "0" : "") + nowDate.getMinutes());
        }


        /**
         * Password Input Box GUI 업데이트 (초기화 및 입력된 Key 갯수만큼 화면에 * 표시함)
         */
        function _updateInputBox(password) {
            log.printDbg("updateInputBox()");
            if(password !== undefined && password !== null) {
                if(password.length >0) {
                    var inputText = "";
                    for(var i=0;i<password.length;i++) {
                        inputText+="*";
                        if(i !== (password.length-1)) {
                            inputText += " ";
                        }

                        textInputBox.text(inputText);
                    }
                }else {
                    textInputBox.text("");
                }
            }
        }


        /**
         * 시청 시간/ 연령 제한 iframe 떠있는 상태에서
         * 비밀번호 인증 프로세스 시작
         * */
        function _inputNum(num , maxLength) {
            log.printDbg('_inputNum() num : ' + num);

            var authMaxLength = maxPasswordLength;
            if(maxLength !== undefined && maxLength !== null) {
                authMaxLength = maxLength;
            }

            if (authManager.inputPW(num, authMaxLength) === true) {
                _updateInputBox(authManager.getPW());
                log.printDbg('_inputNum() authManager.inputPW() return true');

                setTimeout(function(){
                    _checkPW();
                }, 10);
            }
            else {
                log.printDbg('_inputNum() authManager.inputPW() return false');
                _updateInputBox(authManager.getPW());
            }
        }

        /**
         * 입력한 비밀번호의 끝자리를 지운다
         */
        function _deleteNum() {
            log.printDbg('_deleteNum() ');
            authManager.deletePW();
            _updateInputBox(authManager.getPW());
        }

        /**
         * 입력한 비밀번호 일치여부 확인
         */
        function _checkPW() {
            log.printDbg('_checkPW() ');

            var auth_type = authManager.AUTH_TYPE.AUTH_ADULT_PIN;
            var obj = {
                type : auth_type,
                callback : callbackFuncAuthResult,
                loading : {
                    type : KTW.ui.view.LoadingDialog.TYPE.CENTER,
                    lock : true,
                    callback : null,
                    on_off : false
                }
            };

            /**
             * jjh1117 2016.11.12
             * Loading이 돌때 키 입력을 막아야 한다 이 부분 공유 해야함.
             */

            authManager.checkUserPW(obj);
        }


        /**
         * AuthManager로 부터 인증 결과 Callback 함수
         */
        function callbackFuncAuthResult(result) {
            log.printInfo('callbackFuncAuthResult() result: ' + result);
            if (result === authManager.RESPONSE.CAS_AUTH_SUCCESS) {
                // 2017.05.03 dhlee 오류 횟수 초기화
                authManager.resetPWCheckCount();
                KTW.oipf.AdapterHandler.extensionAdapter.releaseTimeRestricted();
            } else if (result === authManager.RESPONSE.CAS_AUTH_FAIL) {
                textTitle.text("비밀번호가 일치하지 않습니다. 다시 입력해 주세요");
                authManager.initPW();
                _updateInputBox(authManager.getPW());
            } else if (result === authManager.RESPONSE.GOTO_INIT) {
                authManager.initPW();
                _updateInputBox(authManager.getPW());
            } else if (result === authManager.RESPONSE.CANCEL) {
                KTW.managers.service.SimpleMessageManager.showAdultCheckPasswordErrorMaxCountNotiMessage();
                authManager.initPW();
                _updateInputBox(authManager.getPW());
            }
        }

        function _startSpeechRecognizer() {
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.start() isStartSSpeechRecognizer : " + isStartSSpeechRecognizer);
            log.printDbg("_startSpeechRecognizer() speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                _stopSpeechRecognizer();
            }
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.start(true);
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.addSpeechRecognizerListener(callbackOnrecognized);

            isStartSSpeechRecognizer = true;
        }

        function _stopSpeechRecognizer() {
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.stop() isStartSSpeechRecognizer :  " + isStartSSpeechRecognizer);
            log.printDbg("_stopSpeechRecognizer() speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized); ");
            if(isStartSSpeechRecognizer === true) {
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.stop();
                KTW.oipf.AdapterHandler.speechRecognizerAdapter.removeSpeechRecognizerListener(callbackOnrecognized);

                isStartSSpeechRecognizer = false;
            }
        }

        function callbackOnrecognized(type , input , mode) {
            log.printInfo('callbackOnrecognized() type: ' + type + " , input : " + input + " , mode : " + mode );
            // mode = KTW.oipf.Def.SR_MODE.PIN;
            // type = "recognized";
            // input = "000";
            if(mode === KTW.oipf.Def.SR_MODE.PIN && type === "recognized") {
                KTW.managers.auth.AuthManager.initPW();
                _updateInputBox("");

                if(input !== undefined && input !== null && input.length>0) {
                    var length = input.length;
                    if(length>maxPasswordLength) {
                        length = maxPasswordLength;
                    }

                    for(var i=0;i<length;i++) {
                        _inputNum(input[i] , length);
                    }
                }
            }
        }
    };

    KTW.ui.view.popup.TimeRestrictedPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.TimeRestrictedPopup.constructor = KTW.ui.view.popup.TimeRestrictedPopup;

    KTW.ui.view.popup.TimeRestrictedPopup.prototype.create = function() {
        this.create();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.TimeRestrictedPopup.prototype.show = function() {
        this.show();
    };
    KTW.ui.view.popup.TimeRestrictedPopup.prototype.hide = function() {
        this.hide();
    };
    KTW.ui.view.popup.TimeRestrictedPopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.TimeRestrictedPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };
})();