/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>ChannelJoinProcessingPopup</code>
 * @author jjh1117
 * @since 2017-03-02
 */

"use strict";

(function () {
    KTW.ui.view.popup.ChannelJoinProcessingPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var params = null;

        var textSaidString = null;

        var closeDiv = null;
        var rebootDiv = null;

        var focusIndex = 0;

        var callbackFunc = null;


        this.createView = function() {
            log.printDbg("create()");
            var div = this.parent.div;
            _createView(div,this);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;
            switch (key_code) {
                case KTW.KEY_CODE.CONTEXT :
                    consumed = true;
                    break;
                case KTW.KEY_CODE.UP:
                case KTW.KEY_CODE.DOWN:
                case KTW.KEY_CODE.OK:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:

                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.EXIT:
                    consumed = true;
                    break;
            }
            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});
            _createElement(parent_div);
        }


        function _showView() {
            log.printDbg("_showView()");
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _createElement(parent_div) {

            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 418 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center", "letter-spacing":-1.65
                    }
                },
                text: "가입",
                parent: popupContainerDiv
            });



            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 492 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 47,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 45 , "text-align": "center","letter-spacing":-2.25
                    }
                },
                text: "해당 채널 가입 처리 중입니다",
                parent: popupContainerDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 563 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(255, 255, 255, 1)", "font-size": 33 , "text-align": "center","letter-spacing":-1.65
                    }
                },
                text: "잠시만 기다려 주세요",
                parent: popupContainerDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 632 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 32,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5
                    }
                },
                text: "별도의 해지 신청이 없을 경우 자동 연장됩니다",
                parent: popupContainerDiv
            });
        }
    };

    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype.constructor = KTW.ui.view.popup.ChannelJoinProcessingPopup;

    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype.create = function() {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype.destroy = function() {
        this.destroyView();
    };
    KTW.ui.view.popup.ChannelJoinProcessingPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };

})();
