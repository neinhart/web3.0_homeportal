/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>FullEpgProgramDetailPopup</code>
 * params : {
 *      channel: current channel,
 *      program: current program
 *      is_show_reservation : true/false(현재 진행 중인 프로그램 인 경우에는 false)
 *      full_epg_callback_func : 시청 btn : 0 , 예약 버튼 : 1 , 닫기 버튼 : 2
 * }
 * 연관메뉴 팝업이 사리지고 자세히 팝업이 노출 되므로
 * 시청예약를 자세히 팝업에서 하는 것이 맞음.
 * 편성표 update를 하기 위해 callback 처리.
 */

"use strict";

(function() {
    KTW.ui.view.popup.FullEpgProgramDetailPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var channelInfoDiv = null;
        var textChannelNumber = null;
        var textChannelName = null;

        var textProgramName_1 = null;
        var textProgramName_2 = null;

        var textProgramTime = null;
        var textProgramDuration = null;
        var divLine = null;

        var textProgramDescription_1 = null;
        var textProgramDescription_2 = null;
        var textProgramDescription_3 = null;

        var curChannel = null;
        var program = null;
        var isShowReservation = false;
        var callbackFunc = null;

        var btnList = [];
        var btnfocusIndex = -1;

        var CHANNELTUNE_BTN_TYPE = 0;
        var PROGRAM_RESERVATION_BTN_TYPE = 1;
        var CANCEL_BTN_TYPE = 2;

        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            div = this.parent.div;
            _createView(div);
        };

        this.showView = function(options) {
            log.printDbg("show()");
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlkey(), key_code = " + key_code + ", btnfocusIndex = " + btnfocusIndex);
            var consumed = false;

            switch(key_code) {
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    if(btnfocusIndex === 0 ) {
                        btnfocusIndex = 1;
                    }else {
                        btnfocusIndex = 0;
                    }
                    _updateUI();
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    _keyOk();

                    KTW.ui.LayerManager.deactivateLayer({
                        id: this.parent.id
                    });

                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK:
                    KTW.ui.LayerManager.deactivateLayer({
                        id: this.parent.id
                    });

                    if(callbackFunc !== null) {
                        callbackFunc(CANCEL_BTN_TYPE);
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.EXIT:
                    KTW.ui.LayerManager.deactivateLayer({
                        id: this.parent.id
                    });

                    consumed = true;
                    break;
            }

            return consumed;
        };

        /**
         * 자세히 팝업을 위한 element 들을 생성한다.
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "delimiter_line",
                    css: {
                        position: "absolute", left: 404, top: 255, width: 1107, height: 460,
                        "background-color": "rgba(48, 47, 47, 1)"
                    }
                },
                parent: popup_container_div
            });

            _createChannelInfoElement();

            textProgramName_1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "focus_program_name1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: (404 + 113), top: 384, width: 880, height: 52,
                        color: "rgba(255, 255, 255, 1)", "font-size": 50,"text-align" : "center", "letter-spacing":-2.5
                    }
                },
                text: "",
                parent: popup_container_div
            });

            textProgramName_2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "focus_program_name2",
                    class: "font_l cut" ,
                    css: {
                        position: "absolute", left: (404 + 113), top: 384, width: 880, height: 52,
                        color: "rgba(255, 255, 255, 1)", "font-size": 50, "text-align" : "center" , "letter-spacing":-2.5,
                        display:"none"
                    }
                },
                text: "",
                parent: popup_container_div
            });



            textProgramTime = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_time",
                    class: "font_l" ,
                    css: {
                        position: "absolute", left: 818, top: 453 , width: 236 , height: 29,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 27 , "letter-spacing":-1.35
                    }
                },
                text: "",
                parent: popup_container_div
            });

            // 구분선
            divLine = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "delimiter_line",
                    css: {
                        position: "absolute", left: 1037, top: 457, width: 2, height: 18,
                        "background-color": "rgba(255, 255, 255, 0.3)"
                    }
                },
                parent: popup_container_div
            });

            // 프로그램 런닝타임: 70분 (실제 데이터는 showView 에서 set)
            textProgramDuration = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_duration_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 1052, top: 454,width: 80 , height: 29,
                        color: "rgba(255, 255, 255, 0.7)",
                        "font-size": 27 , "letter-spacing":-1.35
                    }
                },
                parent: popup_container_div
            });

            // 프로그램 description: 최대 3줄
            textProgramDescription_1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_description_text1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 472, top: 548 , width: 974 , height:30,
                        color: "rgba(255, 255, 255,0.7)",
                        "font-size": 28 , "letter-spacing":-1.4
                    }
                },
                parent: popup_container_div
            });

            textProgramDescription_2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_description_text2",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 472, top: (548 + 44) , width: 974 , height:30,
                        color: "rgba(255, 255, 255,0.7)",
                        "font-size": 28, "letter-spacing":-1.4
                    }
                },
                parent: popup_container_div
            });

            textProgramDescription_3 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "program_description_text3",
                    class: "font_l cut",
                    css: {
                        position: "absolute", left: 472, top: (548 + (44*2)) , width: 974 , height:30,
                        color: "rgba(255, 255, 255,0.7)",
                        "font-size": 28, "letter-spacing":-1.4
                    }
                },
                parent: popup_container_div
            });
        }

        function _createBtnElement() {
            var leftStart = 674;
            for(var i=0;i<2;i++) {
                var btnDiv =  util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "btn_div_"+(i+1),
                        css: {
                            position: "absolute", left: leftStart, top: 763, width: 280, height: 62
                        }
                    },
                    parent: popup_container_div
                });

                var defaultBtn = util.makeElement({
                    tag: "<img />",
                    attrs: {
                        id: "default_btn_" + (i+1),
                        src: "images/popup/pop_btn_w280.png",
                        css: {
                            position: "absolute",
                            left: 0,
                            top: 0,
                            width: 280,
                            height: 62
                        }
                    },
                    parent: btnDiv
                });

                var focusBtn = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "focus_btn_" + (i+1),
                        css: {
                            position: "absolute", left: 0, top: 0, width: 280, height: 62,
                            "background-color": "rgba(210,51,47,1)",display:"none"
                        }
                    },
                    parent: btnDiv
                });

                var textBtn = util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "text_btn_"+(i+1),
                        class: "font_l" ,
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30,"text-align" : "center", "letter-spacing":-1.5
                        }
                    },
                    text: "",
                    parent: btnDiv
                });

                var btnobj = {
                    root_div : btnDiv,
                    default_btn : defaultBtn,
                    focus_btn : focusBtn,
                    text_btn : textBtn,
                    btn_type : -1
                };
                btnList[btnList.length] = btnobj;

                leftStart+=280;
                leftStart+=12;
            }
        }


        function _createChannelInfoElement() {
            log.printDbg("_createChannelInfoElement()");
            channelInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 404, top: 305, width: 1107, height: 40
                    }
                },
                parent: popup_container_div
            });

            textChannelNumber = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_number",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 66, height: 40,
                        color: "rgba(235, 182, 147, 1)", "font-size": 38 , "letter-spacing":-1.9
                    }
                },
                text: "",
                parent: channelInfoDiv
            });

            textChannelName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_name",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 600, height: 38,
                        color: "rgba(255, 254, 254, 0.8)", "font-size": 36 , "letter-spacing":-1.8
                    }
                },
                text: "",
                parent: channelInfoDiv
            });
        }



        /**
         * 자세히 팝업을 화면에 노출한다.
         * 전달 받은 data(param)에 따라 2개의 button 이 존재할 수 있다.
         *
         * @param data
         * @private
         */
        function _showView() {
            log.printDbg("_showView()");

            curChannel = params.data.channel;
            program = params.data.program;
            isShowReservation = params.data.is_show_reservation;
            callbackFunc = params.data.full_epg_callback_func;
            /**
             * 채널번호 Udpate
             */
            var channelNumTextLength = util.getTextLength(util.numToStr(curChannel.majorChannel, 3) , "RixHead B" , 38 ,-1.9);
            var channelNameTextLength = util.getTextLength(curChannel.name , "RixHead B" , 36 ,-1.8);

            var totalLength = channelNumTextLength + 12 + channelNameTextLength;
            var marginLength = (1097 - totalLength)/2;

            var startLeft = marginLength;
            textChannelNumber.text(util.numToStr(curChannel.majorChannel, 3));
            textChannelNumber.css({left:startLeft});
            textChannelNumber.css("display", "");
            startLeft+=channelNumTextLength;
            startLeft+=12;
            /**
             * 채널명 Udpate
             */
            textChannelName.css({width:channelNameTextLength});
            textChannelName.text(curChannel.name);
            textChannelName.css({left:startLeft});
            textChannelName.css("display", "");


            if(program !== undefined && program !== null) {
                var tempProgramName = program.name;
                var multiLineText = util.stringToMultiLine(tempProgramName , "RixHead L" , 50 , 880, -2.5);

                if(multiLineText !== null && multiLineText.length > 1) {
                    /**
                     * 채널번호,채널명을 위로 이동 시킴
                     */
                    channelInfoDiv.css({top: 305});
                    textProgramName_1.css({top: 355});
                    textProgramName_2.css({top: 415});
                    textProgramName_2.css("display", "");

                    textProgramName_1.text(multiLineText[0]);

                    if(multiLineText.length>2) {
                        textProgramName_2.text(multiLineText[1]+multiLineText[2]);
                    }else {
                        textProgramName_2.text(multiLineText[1]);
                    }

                    textProgramTime.css({top:487});
                    textProgramDuration.css({top:487});
                    divLine.css({top:490});

                    textProgramDescription_1.css({top:540});
                    textProgramDescription_2.css({top:584});
                    textProgramDescription_3.css({top:629});

                }else {
                    textProgramName_1.text(tempProgramName);
                    textProgramName_2.text("");
                }
            }

            if (program !== null && program !== undefined && program.name !== KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL) {
                /**
                 * 시간 정보 표시
                 */
                var tmp = new Date(program.startTime * 1000);
                var st = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
                tmp = new Date((program.startTime + program.duration) * 1000);
                var et = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();

                var today = new Date();
                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);

                var showTimeStr = null;
                var startDate = new Date(program.startTime * 1000);

                if(today.getMonth() === startDate.getMonth() && today.getDate() === startDate.getDate()) {
                    showTimeStr = "오늘 " + st + ' - ' + et;
                }else if(tomorrow.getMonth() === startDate.getMonth() && tomorrow.getDate() === startDate.getDate()) {
                    showTimeStr = "내일 " + st + ' - ' + et;

                }else {
                    showTimeStr = "(" + util.transDay(startDate.getDay(), 'kor') + ") " + st + ' - ' + et;
                }

                var programTimeLength = util.getTextLength(showTimeStr, "RixHead L" , 27 , -1.35);
                var durationLength = util.getTextLength(Math.round(program.duration/60) + "분" , "RixHead L" , 27 , -1.35);

                totalLength = programTimeLength + 28 + durationLength;
                marginLength = (KTW.CONSTANT.RESOLUTION.WIDTH - totalLength)/2;
                startLeft = marginLength;

                textProgramTime.text(showTimeStr);
                textProgramTime.css({left:startLeft,width:programTimeLength});
                divLine.css({left:startLeft+programTimeLength+13});

                textProgramDuration.text(Math.round(program.duration/60) + "분");
                textProgramDuration.css({left:startLeft+programTimeLength+28});


                var description  = program.description;

                if(description !== undefined && description !== null && description.length>0 ) {
                    var multiLineText = util.stringToMultiLine(description , "RixHead L" , 28 , 974 , -1.4);
                    if(multiLineText !== null && multiLineText.length > 1) {
                        textProgramDescription_1.text(multiLineText[0]);
                        if(multiLineText.length>3) {
                            textProgramDescription_3.text(multiLineText[2] + multiLineText[3]);
                            textProgramDescription_2.text(multiLineText[1]);

                            textProgramDescription_3.css("display", "");
                            textProgramDescription_2.css("display", "");
                        }else if(multiLineText.length === 3) {
                            textProgramDescription_3.text(multiLineText[2]);
                            textProgramDescription_2.text(multiLineText[1]);

                            textProgramDescription_3.css("display", "");
                            textProgramDescription_2.css("display", "");


                        }else if(multiLineText.length === 2) {
                            textProgramDescription_3.text("");
                            textProgramDescription_2.text(multiLineText[1]);
                            textProgramDescription_2.css("display", "");
                        }
                    }else {
                        textProgramDescription_1.text(description);
                        textProgramDescription_2.text("");
                        textProgramDescription_3.text("");
                    }
                }else {
                    textProgramDescription_1.text("");
                    textProgramDescription_2.text("");
                    textProgramDescription_3.text("");
                }

            }

            _createBtnElement();

            if(isShowReservation !== undefined && isShowReservation !== null && isShowReservation === true) {
                var reservationBtn = btnList[0];
                reservationBtn.btn_type = PROGRAM_RESERVATION_BTN_TYPE;

                if (KTW.managers.service.ReservationManager.isReserved(program) === 0) {
                    reservationBtn.text_btn.text("시청 예약");
                }else if (KTW.managers.service.ReservationManager.isReserved(program) === 1 || KTW.managers.service.ReservationManager.isReserved(program) === 2){
                    reservationBtn.text_btn.text("예약 취소");
                }

                reservationBtn.default_btn.css("display" , "none");
                reservationBtn.focus_btn.css("display" , "");

                reservationBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                reservationBtn.text_btn.removeClass("font_l");
                reservationBtn.text_btn.addClass("font_m");

                var closeBtn = btnList[1];

                closeBtn.btn_type = CANCEL_BTN_TYPE;
                closeBtn.text_btn.text("닫기");
                closeBtn.default_btn.css("display" , "");
                closeBtn.focus_btn.css("display" , "none");

                closeBtn.text_btn.css("color" , "rgba(255,255,255,0.7)");
                closeBtn.text_btn.removeClass("font_m");
                closeBtn.text_btn.addClass("font_l");
            }else {
                var channeltuneBtn = btnList[0];
                channeltuneBtn.btn_type = CHANNELTUNE_BTN_TYPE;

                channeltuneBtn.text_btn.text("시청");
                channeltuneBtn.default_btn.css("display" , "none");
                channeltuneBtn.focus_btn.css("display" , "");

                channeltuneBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                channeltuneBtn.text_btn.removeClass("font_l");
                channeltuneBtn.text_btn.addClass("font_m");


                var closeBtn = btnList[1];

                closeBtn.btn_type = CANCEL_BTN_TYPE;
                closeBtn.text_btn.text("닫기");
                closeBtn.default_btn.css("display" , "");
                closeBtn.focus_btn.css("display" , "none");

                closeBtn.text_btn.css("color" , "rgba(255,255,255,0.7)");
                closeBtn.text_btn.removeClass("font_m");
                closeBtn.text_btn.addClass("font_l");
            }

            btnfocusIndex = 0;

        }

        /**
         * 지난 VOD 보기 정보가 있는 경우에만 불리는 함수
         * 좌/우 방향키에 따른 focus 변경을 처리
         *
         */
        function _updateUI() {
            log.printDbg("_updateUI(), btnfocusIndex = " + btnfocusIndex + ", isShowReservation = " + isShowReservation);

            var reservationBtn = btnList[0];
            reservationBtn.default_btn.css("display" , "");
            reservationBtn.focus_btn.css("display" , "none");
            reservationBtn.text_btn.css("color" , "rgba(255,255,255,1)");
            reservationBtn.text_btn.removeClass("font_m");
            reservationBtn.text_btn.addClass("font_l");

            var closeBtn = btnList[1];
            closeBtn.default_btn.css("display" , "");
            closeBtn.focus_btn.css("display" , "none");
            closeBtn.text_btn.css("color" , "rgba(255,255,255,1)");
            closeBtn.text_btn.removeClass("font_m");
            closeBtn.text_btn.addClass("font_l");

            if(btnfocusIndex === 0) {
                reservationBtn.default_btn.css("display" , "none");
                reservationBtn.focus_btn.css("display" , "");
                reservationBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                reservationBtn.text_btn.removeClass("font_l");
                reservationBtn.text_btn.addClass("font_m");
            }else {
                closeBtn.default_btn.css("display" , "none");
                closeBtn.focus_btn.css("display" , "");
                closeBtn.text_btn.css("color" , "rgba(255,255,255,1)");
                closeBtn.text_btn.removeClass("font_l");
                closeBtn.text_btn.addClass("font_m");
            }
        }

        /**
         * 지난 VOD 보기 버튼에서 OK가 눌려졌을 때의 처리
         *
         * @private
         */
        function _keyOk() {
            log.printDbg("_keyOk()");

            if(btnList !== null && btnList.length>0) {
                if(callbackFunc !== null) {
                    callbackFunc( btnList[btnfocusIndex].btn_type);
                }
            }else {
                if(callbackFunc !== null) {
                    callbackFunc(CANCEL_BTN_TYPE);
                }
            }
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }
    };

    KTW.ui.view.popup.FullEpgProgramDetailPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.FullEpgProgramDetailPopup.constructor = KTW.ui.view.popup.FullEpgProgramDetailPopup;

    KTW.ui.view.popup.FullEpgProgramDetailPopup.prototype.create = function() {
        this.createView();
    };
    KTW.ui.view.popup.FullEpgProgramDetailPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.FullEpgProgramDetailPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.FullEpgProgramDetailPopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.FullEpgProgramDetailPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };
})();