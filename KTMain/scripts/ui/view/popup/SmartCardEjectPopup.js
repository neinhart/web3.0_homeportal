/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SmartCardEjectPopup</code>
 * @author jjh1117
 * @since 2017-03-02
 */

"use strict";

(function () {
    KTW.ui.view.popup.SmartCardEjectPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var params = null;
        var dialogueId = null;

        var MAIN_MESSAGE = [
            ["수신카드가 빠졌거나 잘못 삽입되었습니다." , "수신카드 확인 후 다시 삽입하세요"],
            ["잘못된 수신카드가 삽입되었습니다." , "수신카드를 확인해 주세요"],
            ["시청권한이 없습니다. 시청하시려면 고객센터로 문의해 주세요"]
        ];

        var SUB_MESSAGE1 = "셋톱 박스 우측면의 수신카드가 보이지 않을 경우 덮개를 화살표 방향으로 밀어주세요";
        var SUB_MESSAGE2 = ["셋톱 박스 내부의 수신카드를 꺼낸 후 다시 삽입하세요" , "셋톱 박스 내부의 수신카드를 꺼내 수신카드 번호를 확인하세요 (12자리)"];

        var textMainMessage1 = null;
        var textMainMessage2 = null;
        var textMainMessage3 = null;

        var textSubMessage1 = null;
        var textSubMessage2 = null;
        var textSaidString = null;


        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            if(params !== null) {
                dialogueId = params.data.dialogue_id;
            }
            var div = this.parent.div;
            _createView(div,this);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if (!options || !options.resume) {
                _showView();
            }

            KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChangeListener);
        };

        this.hideView = function() {
            log.printDbg("hide()");

            KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChangeListener);
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;
            return consumed;
        };

        /**
         * [dj.son] OTHER_APP 상태로 넘어갈 경우 해당 화면이 늦게 사라지는 이슈가 있음 (다른 앱이 requestShow 를 호출할때까지 노출됨)
         *          따라서 OTHER_APP 상태로 변경시 곧바로 hidden 처리, 그 외 상태로 변경시에는 inherit 처리
         */
        function onServiceStateChangeListener (options) {
            if (options && options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                popupContainerDiv.css({visibility: "hidden"});
            }
            else {
                popupContainerDiv.css({visibility: "inherit"});
            }
        }

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});
            _createElement(parent_div);
        }


        function _showView() {
            log.printDbg("_showView()");

            if(dialogueId !== null) {
                if(dialogueId === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.INSERT_SMARTCARD) {
                    textMainMessage1.text(MAIN_MESSAGE[0][0]);
                    textMainMessage2.text(MAIN_MESSAGE[0][1]);

                    textMainMessage1.css("display", "");
                    textMainMessage2.css("display", "");
                }else if(dialogueId === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.DIALOG_ID_CARD_NOT_AUTHORIZED) {
                    textMainMessage1.text(MAIN_MESSAGE[2][0]);

                    textMainMessage1.css("display", "");
                    textMainMessage3.css("display", "");
                }else {
                    textMainMessage1.text(MAIN_MESSAGE[1][0]);
                    textMainMessage2.text(MAIN_MESSAGE[1][1]);

                    textMainMessage1.css("display", "");
                    textMainMessage2.css("display", "");
                }

                textSubMessage1.text(SUB_MESSAGE1);
                if(dialogueId === KTW.ca.MmiHandler.MMI_EVENT.DIALOGUE_ID.DIALOG_ID_CARD_NOT_AUTHORIZED) {
                    textSubMessage2.text(SUB_MESSAGE2[1]);
                }else {
                    textSubMessage2.text(SUB_MESSAGE2[0]);
                }
                textSubMessage1.css("display", "");
                textSubMessage2.css("display", "");


                var saidString = "";
                saidString += "SAID : ****";
                saidString +=  KTW.SAID.substring(4);
                textSaidString.text(saidString);
                textSaidString.css("display" , "");
            }
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _createElement(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 131 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:""
                    }
                },
                text: "수신카드 오류",
                parent: popupContainerDiv
            });


            var rootDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "root_div",
                    css: {
                        position: "absolute",
                        left: 431, top:179,width: 1058,height:638
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_l_img",
                    src: "images/popup/pop_bg_noti_02_l.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 49, height: 638
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_m_img",
                    src: "images/popup/pop_bg_noti_02_m.png",
                    css: {
                        position: "absolute", left: 49, top: 0, width: 960, height: 638
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_l_img",
                    src: "images/popup/pop_bg_noti_02_r.png",
                    css: {
                        position: "absolute", left: 1009, top: 0, width: 49, height: 638
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "center_img",
                    src: "images/popup/img_reception.png",
                    css: {
                        position: "absolute", left: 591-431, top: 363-179, width: 738, height: 282
                    }
                },
                parent: rootDiv
            });


            textMainMessage1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mainmessage_1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 256-179 , width: 1058, height: 34,
                        color: "rgba(255, 255, 255, 1)", "font-size": 32 , "text-align": "center",
                        display:"none"
                    }
                },
                text: "",
                parent: rootDiv
            });

            textMainMessage2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mainmessage_1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 303-179 , width: 1058, height: 34,
                        color: "rgba(255, 255, 255, 1)", "font-size": 32 , "text-align": "center",
                        display:"none"
                    }
                },
                text: "",
                parent: rootDiv
            });

            textMainMessage3 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mainmessage_1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 303-179 , width: 1058, height: 29,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 27 , "text-align": "center",
                        display:"none"
                    }
                },
                text: "(문의 : 국번없이 100번)",
                parent: rootDiv
            });


            textSubMessage1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "submessage_1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 548-431 , top: 664-179 , width: 882, height: 30,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 26 , "text-align": "left",
                        display:""
                    }
                },
                text: "",
                parent: rootDiv
            });

            textSubMessage2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "submessage_2",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 548-431 , top: 708-179 , width: 882, height: 30,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 26 , "text-align": "left",
                        display:""
                    }
                },
                text: "",
                parent: rootDiv
            });



            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/popup/pop_num_1.png",
                    css: {
                        position: "absolute", left: 495-431, top: 659-179, width: 37, height: 37
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/popup/pop_num_2.png",
                    css: {
                        position: "absolute", left: 495-431, top: 703-179, width: 37, height: 37
                    }
                },
                parent: rootDiv
            });



            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "sub_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 480 , top: 853 , width: 755, height: 29,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 26 , "text-align": "left",
                        display:""
                    }
                },
                text: "문제가 해결되지 않을 시 고객센터로 문의바랍니다(국번 없이 100번)",
                parent: popupContainerDiv
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1204, top: 855, width: 2, height: 22,
                        "background-color": "rgba(255, 255, 255, 0.2)"
                    }
                },
                parent: popupContainerDiv
            });

            textSaidString = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "said",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1216 , top: 856 , width: 250, height: 26,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 24 , "text-align": "left",
                        display:"none"
                    }
                },
                text: "",
                parent: popupContainerDiv
            });

        }
    };

    KTW.ui.view.popup.SmartCardEjectPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.SmartCardEjectPopup.prototype.constructor = KTW.ui.view.popup.SmartCardEjectPopup;

    KTW.ui.view.popup.SmartCardEjectPopup.prototype.create = function() {
        this.createView();
    };
    KTW.ui.view.popup.SmartCardEjectPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.SmartCardEjectPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.SmartCardEjectPopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.SmartCardEjectPopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };
})();
