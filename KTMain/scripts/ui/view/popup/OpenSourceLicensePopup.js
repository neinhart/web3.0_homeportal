/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>OpenSourceLicensePopup</code>
 * params : {
 * }
 */

"use strict";

(function() {
    KTW.ui.view.popup.OpenSourceLicensePopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popup_container_div = null;
        var div = null;
        var params = null;

        var btnList = [];
        var btnfocusIndex = 0;

        var textDesc = null;

        var scrollbarbackground = null;
        var scrollbarforeground = null;

        var contentsIndex = 0;
        var contentsSize = 0;
        var contentsTop = 0;
        var contentsArea = 411;

        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            div = this.parent.div;
            _createView(div);
        };

        this.showView = function(options) {
            log.printDbg("show()");
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
            _hideView();
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlkey(), key_code = " + key_code + ", btnfocusIndex = " + btnfocusIndex);
            var consumed = false;

            switch(key_code) {
                case KTW.KEY_CODE.UP:
                    if (contentsSize !== 1) {
                        if(contentsIndex > 0){
                            contentsIndex--;
                            textDesc.css("top", -(contentsArea * contentsIndex));
                            scrollbarforeground.css("top", 318 + contentsIndex * contentsTop);
                        }
                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.DOWN:
                    if (contentsSize !== 1) {
                        if(contentsIndex < contentsSize - 1){
                            contentsIndex++;
                            textDesc.css("top", -(contentsArea * contentsIndex));
                            scrollbarforeground.css("top", 318 + contentsIndex * contentsTop);
                        }

                    }
                    consumed = true;
                    break;
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                case KTW.KEY_CODE.BACK:
                case KTW.KEY_CODE.EXIT:
                    KTW.ui.LayerManager.deactivateLayer({
                        id: this.parent.id
                    });
                    consumed = true;
                    break;
            }

            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });


            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 390, top: 268, width: 1140, height: 510,
                        "background-color": "rgba(48, 47, 47, 1)"
                    }
                },
                parent: popup_container_div
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 183 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 47,
                        color: "rgba(255, 255, 255, 1)", "font-size": 45 , "text-align": "center",
                        display:"", "letter-spacing":-2.5
                    }
                },
                text: "오픈소스 라이선스",
                parent: popup_container_div
            });


            var textDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 461, top: 322, width: 1014, height: 411,
                        overflow:"hidden"
                    }
                },
                parent: popup_container_div
            });

            textDesc = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 0 , width: 1014,
                        color: "rgba(255, 255, 255, 1)", "font-size": 26 , "text-align": "left",
                        "white-space":"pre-line", "line-height": 1.48
                    }
                },
                text: "",
                parent: textDiv
            });
/*
            var textMsg1 = "Copyright © 2016, KT corp. All Rights Reserved.<br>" +
                "This product contains the following Open Source Software with licenses and notices below.<br>For any questions, please contact to osscenter@kt.com<br><br>";
            var textMsg2 = "<b>jquery 1.9.1</b><br>http://github.com/jquery/jquery/<br>Copyright © jQuery Foundation and other contributors<br>MIT License<br><br>" +
                "<b>Modernizr 2.8.3</b><br>http://github.com/Modernizr/Modernizr/<br>MIT License<br><br>";
*/
            //2017.10.23 sw.nam
            //변경된 라이선스 항목적용
            var textMsg1 = "Copyright © 2016, KT corp. All Rights Reserved.<br>" +
                "This product contains the following Open Source Software with licenses and notices below.<br>For any questions, please contact to osscenter@kt.com<br><br>";
            var textMsg2 = "<b>jquery</b><br>http://github.com/jquery/jquery<br>Copyright ©  JS Foundation and other contributors, https://js.foundation/<br>MIT License<br><br><br>" +
                "<b>jquery-qrcode</b><br>http://github.com/lrsjng/jquery-qrcode<br>Copyright ©  2016 Lars Jung (https://larsjung.de)<br>MIT License<br><br>" +
                "<b>jsencrypt</b><br>http://github.com/travist/jsencrypt<br>Copyright ©  2015 Form.io<br>MIT License<br><br><br>" +
                "<b>jszip</b><br>https://www.npmjs.org/package/jszip<br>Copyright © <br>MIT License<br><br>";

            var textMsg3 = '<b style="font-size: 32px;">The MIT License</b><br>'+
                    "Copyright © ＜year＞＜copyright holders＞"+"<br><br>"+
                'Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation '+
                'files (the "Software"), to deal<br>in the Software without restriction, including without limitation the rights to use, copy, '+
                "modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software "+
                "is furnished to do so, subject to the following conditions:<br><br>"+
                "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.<br><br>"+
                'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, ' +
            "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, " +
            "WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
            textDesc.html(textMsg1+textMsg2+textMsg3);



            scrollbarbackground = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 1478, top: 318, width: 3, height: 409,
                        "background-color": "rgba(255, 255, 255, 0.14)"
                    }
                },
                parent: popup_container_div
            });

            scrollbarforeground = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 1477, top: 318, width: 5, height: 0,
                        "background-color": "rgba(170, 170, 170, 1)"
                    }
                },
                parent: popup_container_div
            });

            setTimeout(function(){
                var contentsHeightPx = textDesc.css('height');
                var contentsHeight = Number(contentsHeightPx.substring(0, contentsHeightPx.indexOf("px")));

                contentsSize = Math.ceil(contentsHeight/contentsArea);
                contentsTop = Math.floor(contentsArea/contentsSize);

                if (contentsSize == 1) {
                    scrollbarbackground.css("visibility", "hidden");
                    scrollbarforeground.css("visibility", "hidden");
                }else {
                    scrollbarbackground.css("visibility", "visible");
                    scrollbarforeground.css("visibility", "visible");
                }

                if(contentsTop > 20){
                    scrollbarforeground.css("height", contentsTop);
                }
            },0);

            _createBtnElement();
        }

        function _createBtnElement() {
            //2017.05.08 sw.nam 취소버튼 제거

            var btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_div_1",
                    css: {
                        position: "absolute", left: 825, top: 838, width: 280, height: 62
                    }
                },
                parent: popup_container_div
            });

            var focusBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn_1",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 280, height: 62,
                        "background-color": "rgba(210,51,47,1)",display:""
                    }
                },
                parent: btnDiv
            });

            var textBtn = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn_1",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                        color: "rgba(255,255,255,1)", "font-size": 30,"text-align" : "center",
                        "letter-spacing" : -1.3
                    }
                },
                text: "확인",
                parent: btnDiv
            });

            var btnobj = {
                root_div : btnDiv,
                focus_btn : focusBtn,
                text_btn : textBtn,
                btn_type : -1
            };
            btnList[btnList.length] = btnobj;
        }
        /**
         * 오픈소스 라이선스 공지 화면을 노출한다.
         */
        function _showView() {
            log.printDbg("_showView()");

            btnfocusIndex = 0;
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }
    };

    KTW.ui.view.popup.OpenSourceLicensePopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.OpenSourceLicensePopup.constructor = KTW.ui.view.popup.OpenSourceLicensePopup;

    KTW.ui.view.popup.OpenSourceLicensePopup.prototype.create = function() {
        this.createView();
    };
    KTW.ui.view.popup.OpenSourceLicensePopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.OpenSourceLicensePopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.OpenSourceLicensePopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.OpenSourceLicensePopup.prototype.handleKeyEvent = function(key_code) {
        return this.controlKey(key_code);
    };
})();