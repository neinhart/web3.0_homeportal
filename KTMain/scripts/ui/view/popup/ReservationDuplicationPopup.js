"use strict";

(function () {
    //alias
    var util = KTW.utils.util;
    var log = KTW.utils.Log;

    var arrButton = [{id: "ok", name: "확인"}, {id: "cancel", name: "취소"}];

    KTW.ui.view.popup.ReservationDuplicationPopup = function(options) {
        KTW.ui.View.call(this);

        this.data = null;
        this.focusIdx = -1;

        function createElements (_this) {
            var parentDiv = _this.parent.div;
            var programme = _this.data.programme;

            var arrTitle = util.splitText(programme.name, "RixHead L", 35, 770);
            var pannelMiddleHeight = 70 + (45 * arrTitle.length);
            var buttonTop = 407 + (45 * arrTitle.length);

            var containerHeight = 469 + (45 * arrTitle.length);
            var containerTop = (1080 - containerHeight) / 2;

            util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.9)"
                    }
                },
                parent: parentDiv
            });

            var containerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: containerTop, overflow: "visible"
                    }
                },
                parent: parentDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 94, "text-align": "center",
                        "font-size": 33, "letter-spacing": -1.65, color: "rgba(221, 175, 120, 1)", "font-family": "RixHead B"
                    }
                },
                text: "중복 예약 알림",
                parent: containerDiv
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 94, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 61, "text-align": "center",
                        "font-size": 42, "letter-spacing": -2.1, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M"
                    }
                },
                text: "기존 예약된 다음 프로그램과 시작 시간이 겹칩니다",
                parent: containerDiv
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 155, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 84, "text-align": "center",
                        "font-size": 33, "letter-spacing": -1.65, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead L"
                    }
                },
                text: "예약된 프로그램 삭제 후 새로 시청 예약을 하시겠습니까?",
                parent: containerDiv
            });

            var pannelDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 529, top: 231, overflow: "visible"
                    }
                },
                parent: containerDiv
            });



            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/pop_bg_program_top.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 862, height: 20
                    }
                },
                parent: pannelDiv
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/pop_bg_program_m.png",
                    css: {
                        position: "absolute", left: 0, top: 20, width: 862, height: pannelMiddleHeight
                    }
                },
                parent: pannelDiv
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    src: "images/pop_bg_program_btm.png",
                    css: {
                        position: "absolute", left: 0, top: (20 + pannelMiddleHeight), width: 862, height: 20
                    }
                },
                parent: pannelDiv
            });

            var channelInfo = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 38, width: 862, height: 40,
                        "text-align": "center", "font-size": 27, "letter-spacing": -1.35
                    }
                },
                parent: pannelDiv
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        "font-family": "RixHead M", color: "rgba(255, 255, 255, 0.5)", "margin-right": 12
                    }
                },
                text: util.numToStr(programme.channel.majorChannel, 3),
                parent: channelInfo
            });
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        "font-family": "RixHead M", color: "rgba(255, 255, 255, 0.5)", "margin-right": 20
                    }
                },
                text: programme.channel.name,
                parent: channelInfo
            });
            util.makeElement({
                tag: "<img />",
                attrs: {
                    css: {
                        width: 2, height: 20, "background-color": "rgba(255, 255, 255, 0.2)", "margin-right": 20, "margin-bottom": -2
                    }
                },
                parent: channelInfo
            });
            var programmeStartTime = new Date(programme.startTime * 1000);
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        "font-family": "RixHead L", color: "rgba(255, 255, 255, 0.7)"
                    }
                },
                text: util.numToStr(programmeStartTime.getHours(), 2) + ":" + util.numToStr(programmeStartTime.getMinutes(), 2),
                parent: channelInfo
            });

            for (var i = 0; i < arrTitle.length; i++) {
                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: {
                            position: "absolute", left: 0, top: 80 + (i * 45), width: 862, height: 45, "text-align": "center",
                            "font-size": 35, "letter-spacing": -1.75, color: "rgba(255, 255, 255, 1)", "font-family": "RixHead L"
                        }
                    },
                    text: arrTitle[i],
                    parent: pannelDiv
                });
            }

            // button
            var buttonLeft = 674;

            for (var i = 0; i < arrButton.length; i++) {
                var buttonData = arrButton[i];

                var button = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        name: "rd_popup_button",
                        css: {
                            position: "absolute", left: buttonLeft, top: buttonTop, width: 280, height: 62,
                            "background-image": "url(./images/pop_btn_w280.png)"
                        }
                    },
                    parent: containerDiv
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        css: {
                            position: "absolute", left: 0, top: 17, width: 280, height: 35, "text-align": "center",
                            "font-size": 30, "letter-spacing": -1.5, color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead M"

                        }
                    },
                    text: buttonData.name,
                    parent: button
                });

                buttonLeft += 294;
            }
        }

        function setButtonFocus (_this) {
            var arrButton = _this.parent.div.find("[name=rd_popup_button]");
            var focusButton = $(arrButton[_this.focusIdx]);

            arrButton.css({ "background-color": "", "background-image": "url(./images/pop_btn_w280.png)" });
            arrButton.children().css({color: "rgba(255, 255, 255, 0.7)" });

            focusButton.css({ "background-color": "rgba(210, 51, 47, 1)", "background-image": "" });
            focusButton.children().css({color: "rgba(255, 255, 255, 1)" });
        }

        this.createView = function() {
            this.parent.div.attr({class: "arrange_frame"});
            this.data = this.parent.getParams().data;

            createElements(this);
        };

        this.showView = function (options) {
            if (!options || !options.resume) {
                this.focusIdx = 0;
                setButtonFocus(this);
            }
        }

        this.hideView = function (options) {
            if (!options || !options.pause) {
                if (this.data.cbAction) {
                    this.data.cbAction();
                }
            }
        };

        this.destroy = function() {};

        this.controlKey = function (keyCode) {
            var consumed = false;

            switch (keyCode) {
                case KTW.KEY_CODE.LEFT:
                    if (this.focusIdx > -1) {
                        this.focusIdx = --this.focusIdx < 0 ? arrButton.length - 1 : this.focusIdx;
                        setButtonFocus(this);
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.RIGHT:
                    if (this.focusIdx > -1) {
                        this.focusIdx = ++this.focusIdx > arrButton.length - 1 ? 0 : this.focusIdx;
                        setButtonFocus(this);
                        consumed = true;
                    }
                    break;
                case KTW.KEY_CODE.OK:
                    if (this.focusIdx > -1) {
                        if (this.data.cbAction) {
                            this.data.cbAction(arrButton[this.focusIdx].id);
                        }
                        consumed = true;
                    }
                    break;
            }

            return consumed;
        }
    };

    KTW.ui.view.popup.ReservationDuplicationPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.ReservationDuplicationPopup.prototype.constructor = KTW.ui.view.popup.ReservationDuplicationPopup;

    KTW.ui.view.popup.ReservationDuplicationPopup.prototype.create = function () {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.ReservationDuplicationPopup.prototype.show = function (options) {
        this.showView(options);
    };
    KTW.ui.view.popup.ReservationDuplicationPopup.prototype.hide = function (options) {
        this.hideView(options);
    };
    KTW.ui.view.popup.ReservationDuplicationPopup.prototype.handleKeyEvent = function (key_code) {
        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };
})();
