"use strict";
/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>PipMultiViewPopup</code>
 * 멀티화면 및 미니가이드 인접채널 PIP를 관리한다
 *
 * @author jjh1117
 * @since 2016. 12. 06.
 */

(function() {
    var size = -1;
    var pos_type = -1;

    KTW.ui.view.popup.PipMultiViewPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var div = null;

        var id = null;
        var div = null;

        var popupContainerDiv = null;
        var multiViewGuideDiv = null;
        var channelInfoDiv = null;

        var imgChannelInfoLeftBackground = null;
        var imgChannelInfoMiddleBackground = null;
        var imgChannelInfoRightBackground = null;

        var textChannelNumber = null;
        var textChannelName = null;
        var textChannelProgramName = null;

        var main_channel_control = null;
        var pip_channel_control = null;

        /**
         * 빠른 pip 채널전환을 위한 변수 및 timer
         */
        var is_fast_tune = false;
        var timer_fast_tune = null;
        var DELAY_FAST_TUNE = 500;

        var timer_hide = null;
        var timer_input = null;


        var textOKKeyGuide = null;
        var isShowingSignalIframe = false;

        var POS_TYPE = {
            RIGHT_DOWN : 0, // DEFAULT
            LEFT_UP : 1,
            RIGHT_UP : 2,
            LEFT_DOWN : 3
        };

        var SIZE = {
            DEFAULT : 0,
            MIDDLE : 1,
            LARGE : 2
        };

        var leftUpSizeList = [
            {left: 62, top: (54-2), width: 592, height: 333},
            {left: 62, top: (54-2), width: 704, height: 396},
            {left: 62, top: (54-2), width: 816, height: 459}
        ];

        var leftDownSizeList = [
            {left: 62, top: (640+2), width: 592, height: 333},
            {left: 62, top: (577+2), width: 704, height: 396},
            {left: 62, top: (514+2), width: 816, height: 459}
        ];

        var rightUpSizeList = [
            {left: 1266, top: (54-2), width: 592, height: 333},
            {left: 1154, top: (54-2), width: 704, height: 396},
            {left: 1042, top: (54-2), width: 816, height: 459}
        ];

        var rightDownSizeList = [
            {left: 1266, top: (640+2), width: 592, height: 333},
            {left: 1154, top: (577+2), width: 704, height: 396},
            {left: 1042, top: (514+2), width: 816, height: 459}
        ];

        var currentSizeList = null;

        // 현재 채널 리스트
        var ch_list = null;
        var ch_index = 0;

        // iptv채널 리스트 , skylife 채널 리스트
        var kt_ch_list = [];
        var sky_ch_list = [];

        var pipCurrentChannel = null;
        var mainCurrentChannel = null;

        /**
         * pip채널 변경을 처리중인지 여부
         */
        var pip_ch_change_req = false;

        var ch_pip = null;
        var ch_pip_evt = null;

        var callbackFuncHide = null;

        var isRequestTune = false;

        var isDCSMode = false;

        this.createView = function() {
            log.printDbg("create()");

            div = this.parent.div;

            if (size < 0) {
                size = SIZE.DEFAULT;
            }

            if (pos_type < 0) {
                pos_type = POS_TYPE.RIGHT_DOWN;
            }

            _createView(div);
        };

        this.showView= function(options) {
            log.printDbg("showView() ");

            if (!options || !options.resume) {
                var params = this.parent.getParams();
                if (params !== null) {
                    callbackFuncHide = params.data.callback_hide;
                }

                if(currentSizeList === null) {
                    if(pos_type === POS_TYPE.LEFT_UP) {
                        currentSizeList = leftUpSizeList;
                    }else if(pos_type === POS_TYPE.LEFT_DOWN) {
                        currentSizeList = leftDownSizeList;

                    }else if(pos_type === POS_TYPE.RIGHT_UP) {
                        currentSizeList = rightUpSizeList;
                    }else if(pos_type === POS_TYPE.RIGHT_DOWN) {
                        currentSizeList = rightDownSizeList;
                    }
                }

                _startPip();

                main_channel_control = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                pip_channel_control = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);


                KTW.oipf.AdapterHandler.navAdapter.addChannelListUpdateListener(_callbackChListUpdate);

                if (KTW.CONSTANT.IS_OTS) {
                    var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                    log.printDbg("showView(), KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE) : " + temp);
                    if (temp != null) {
                        if(temp === "Y") {
                            isDCSMode = true;
                        }else {
                            isDCSMode = false;
                        }
                    }

                    log.printDbg("showView(), isDCSMode: " + isDCSMode);
                    KTW.managers.StorageManager.ps.addStorageDataChangeListener(_updateOTSDCSMode);
                }

                _pipChannelPlay();
                _showGuide();
                _setTimer();

                /**
                 * [dj.son] [WEBIIIHOME-2936] 멀티 화면을 show 할때 마지막에 notify hide 를 날리기 때문에, 결국 vod 재생중 pause 를 누르면 OAM 광고가 노출됨.
                 * - 광고가 노출되지 않도록, VOD 재생중이면 notify hide 를 호출하지 않도록 수정
                 * - 기존에 왜 notify hide 를 날리는 지는 모르기 때문에, VOD 재생중일때만 아래 코드 skip 하도록 수정
                 *
                 * - TV_VIEWING 상태에서 멀티 화면이 show 되었을 경우, 채널 재핑으로 인해 팝업이 닫히지 않도록 (obs_hide 메세지가 오지 않도록) notifyHide 호출
                 */
                if (!KTW.managers.service.StateManager.isVODPlayingState()) {
                    setTimeout(function(){
                        _sendMessageToObserver(KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE);
                    } , 100);
                }
            }
        };

        this.hideView = function(options) {
            if(options !== undefined && options.pause !== undefined && options.pause === true) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: "PipMultiViewPopup",
                    onlyTarget: true //KTW.managers.service.StateManager.isVODPlayingState()
                });
                return;
            }

            KTW.oipf.AdapterHandler.navAdapter.removeChannelListUpdateListener(_callbackChListUpdate);

            if(main_channel_control !== null) {
                main_channel_control.removeChannelEventListener(_mainChEventListener);
            }

            if(pip_channel_control !== null) {
                pip_channel_control.removeChannelEventListener(_pipChEventListener);
            }
            channelQueue.release();

            KTW.managers.service.StateManager.removeServiceStateListener(onServiceStateChange);

            if (KTW.CONSTANT.IS_OTS) {
                KTW.managers.StorageManager.ps.removeStorageDataChangeListener(_updateOTSDCSMode);
            }

            _clearTimer();
            _endPip();

            if (callbackFuncHide !== null) {
                callbackFuncHide();
            }

            callbackFuncHide = null;
        };


        this.controlKey = function(key_code) {
            log.printDbg("controlKey()");
            var consumed = false;
            if (timer_input === null) {
                // swap 및 channel up/down은 bypass하고 단말에서 처리해야 한다
                // 수정된 펌웨어가 나올 때까지 우선 여기에서 계속 처리한다
                switch (key_code) {
                    case KTW.KEY_CODE.UP :
                        _moveUpDown(true);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.DOWN :
                        _moveUpDown(false);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.LEFT :
                        _moveLeftRight(true);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.RIGHT :
                        _moveLeftRight(false);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.OK :
                        _swapPip();

                        consumed = true;
                        break;
                    case KTW.KEY_CODE.BACK :
                    case KTW.KEY_CODE.EXIT :
                        /**
                         * [dj.son] VOD 재생 상태에서 멀티 화면 닫을때 onlyTarget 을 true 로 주면, vod 모듈 관련 layer 가 show 가 안되어서 키를 못받는 이슈가 있음
                         * - 이를 해결하기위해 vod 모듈쪽에서 멀티 화면 호출시 hide callback 에 예외처리 추가함
                         */
                        KTW.ui.LayerManager.deactivateLayer({
                            id: "PipMultiViewPopup",
                            onlyTarget: KTW.managers.service.StateManager.isVODPlayingState()
                        });
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.RED :
                        _changeSize(false);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.BLUE :
                        _changeSize(true);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.CONTEXT :
                        consumed = true;
                        return consumed;
                    default :
                        break;
                }

                if (consumed === true && key_code !== KTW.KEY_CODE.BACK && key_code !== KTW.KEY_CODE.EXIT) {
                    _showGuide();
                    _setTimer();
                }

                timer_input = setTimeout(function(){
                    timer_input = null;
                }, 500);
            }
            // 연속 키입력 방지 타이머에 걸리는 경우
            // 멀티화면에서 처리하는 키가 입력되면 키를 막고 동작하지 않는다
            else {
                log.printDbg('block continuous key input');
                switch (key_code) {
                    case KTW.KEY_CODE.UP :
                        _moveUpDown(true);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.DOWN :
                        _moveUpDown(false);
                        consumed = true;
                        break;
                    case KTW.KEY_CODE.LEFT :
                    case KTW.KEY_CODE.RIGHT :
                    case KTW.KEY_CODE.OK :
                    case KTW.KEY_CODE.BACK :
                    case KTW.KEY_CODE.EXIT :
                    case KTW.KEY_CODE.RED :
                    case KTW.KEY_CODE.BLUE :
                    case KTW.KEY_CODE.CONTEXT :
                        consumed = true;
                        break;
                    default :
                        break;
                }
            }

            // 핫키가 입력되면 멀티화면이 종료되며 핫키가 동작해야 하기 때문에
            // exitMulti()를 호출하되 키를 consume 하지는 않는다
            switch (key_code) {
                case KTW.KEY_CODE.MOVIE :
                case KTW.KEY_CODE.REWATCH :
                case KTW.KEY_CODE.MONTHLY :
                case KTW.KEY_CODE.FAVORITE :
                case KTW.KEY_CODE.RECOMMEND :
                case KTW.KEY_CODE.WATCH_LIST :
                case KTW.KEY_CODE.PURCHASE_LIST :
                case KTW.KEY_CODE.WISH_LIST :
                case KTW.KEY_CODE.MYMENU :
                case KTW.KEY_CODE.SETTING :
                case KTW.KEY_CODE.FULLEPG :
                case KTW.KEY_CODE.FULLEPG_OTS :
                case KTW.KEY_CODE.SKY_CHOICE :
                case KTW.KEY_CODE.SEARCH :
                case KTW.KEY_CODE.MENU :

            /**
             * [dj.son] 키즈 리모컨 핫키 추가
             */
                case KTW.KEY_CODE.KIDS_MENU :
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_1:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_2:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_3:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_4:
                    // hotKey에 의래 PIP가 닫히는 경우에는
                    // synchronous 하게 닫히도록 처리
                    pip_channel_control.stop();
                    break;
            }

            return consumed;
        };

        this.showGuideView = function() {
            _showGuide();
        };

        function _updateOTSDCSMode(key) {
            log.printDbg('_updateOTSDCSMode() key : ' + key);
            if (key === KTW.managers.StorageManager.KEY.OTS_DCS_MODE) {
                log.printDbg('_updateOTSDCSMode() key : ' + key);
                var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                if (temp != null) {
                    if(temp === "Y") {
                        isDCSMode = true;
                    }else {
                        isDCSMode = false;
                    }

                    if(isDCSMode === true) {
                        KTW.ui.LayerManager.deactivateLayer({
                            id: "PipMultiViewPopup",
                            onlyTarget: KTW.managers.service.StateManager.isVODPlayingState()
                        });
                    }

                }
            }
        }

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView() {
            log.printDbg("_createView()");
            div.attr({class: "arrange_frame"});
            _createBaseView(div);
        }

        function _createBaseView(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: parent_div
            });

            multiViewGuideDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 0, top: 975, width:KTW.CONSTANT.RESOLUTION.WIDTH, height: 105,
                        "background-color": "rgba(0,0,0,0.7)"
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/icon/icon_multi_size.png",
                    css: {
                        position: "absolute",
                        left: 1168 ,
                        top: 1000 - 975,
                        width: 51,
                        height: 28
                    }
                },
                parent: multiViewGuideDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1228 , top: 1003-975 , width: 100, height: 28,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 26 , "text-align": "left",
                        display:"" , "letter-spacing":-1.3
                    }
                },
                text: "크기 변경",
                parent: multiViewGuideDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/icon/icon_multi_move.png",
                    css: {
                        position: "absolute",
                        left: 1361 ,
                        top: 1000 - 975,
                        width: 37,
                        height: 28
                    }
                },
                parent: multiViewGuideDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1410 , top: 1003-975 , width: 100, height: 28,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 26 , "text-align": "left",
                        display:"", "letter-spacing":-1.3
                    }
                },
                text: "화면 이동",
                parent: multiViewGuideDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/key_ok.png",
                    css: {
                        position: "absolute",
                        left: 1546 ,
                        top: 1000 - 975,
                        width: 55,
                        height: 28
                    }
                },
                parent: multiViewGuideDiv
            });

            textOKKeyGuide = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1613 , top: 1003-975 , width: 100, height: 28,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 26 , "text-align": "left",
                        display:"", "letter-spacing":-1.3
                    }
                },
                text: "화면 전환",
                parent: multiViewGuideDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/key_back.png",
                    css: {
                        position: "absolute",
                        left: 1749 ,
                        top: 1000 - 975,
                        width: 55,
                        height: 28
                    }
                },
                parent: multiViewGuideDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1816 , top: 1003-975 , width: 100, height: 28,
                        color: "rgba(255, 255, 255, 0.7)", "font-size": 26 , "text-align": "left",
                        display:"", "letter-spacing":-1.3
                    }
                },
                text: "종료",
                parent: multiViewGuideDiv
            });

            channelInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:596, height: 102

                    }
                },
                parent: popupContainerDiv
            });


            imgChannelInfoLeftBackground = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/multi_info_l.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 14,
                        height: 102
                    }
                },
                parent: channelInfoDiv
            });

            imgChannelInfoMiddleBackground = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/multi_info_m.png",
                    css: {
                        position: "absolute",
                        left: 14 ,
                        top: 0,
                        width: 596 - 14 - 12,
                        height: 102
                    }
                },
                parent: channelInfoDiv
            });

            imgChannelInfoRightBackground = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/multi_info_r.png",
                    css: {
                        position: "absolute",
                        left: 14 + (596 - 14 - 12) ,
                        top: 0,
                        width: 12,
                        height: 102
                    }
                },
                parent: channelInfoDiv
            });


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/arw_type_2_dw.png",
                    css: {
                        position: "absolute",
                        left: 31 ,
                        top: 27,
                        width: 22,
                        height: 14
                    }
                },
                parent: channelInfoDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/pipview/arw_type_2_up.png",
                    css: {
                        position: "absolute",
                        left: 31 ,
                        top: 61,
                        width: 22,
                        height: 14
                    }
                },
                parent: channelInfoDiv
            });

            textChannelNumber = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_number",
                    class: "font_b" ,
                    css: {
                        position: "absolute", left: 76, top: 20, width: 70 , height: 28,
                        color: "rgba(255, 255, 255, 1)", "font-size": 26,"text-align" : "left", "letter-spacing":-1.3
                    }
                },
                text: "",
                parent: channelInfoDiv
            });

            textChannelName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_name",
                    class: "font_b cut" ,
                    css: {
                        position: "absolute", left: 132, top: 22, width: 300 , height: 26,
                        color: "rgba(255, 255, 255, 1)", "font-size": 24,"text-align" : "left", "letter-spacing":-1.2
                    }
                },
                text: "",
                parent: channelInfoDiv
            });

            textChannelProgramName = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_channel_program_name",
                    class: "font_l cut" ,
                    css: {
                        position: "absolute", left: 76, top: 55, width: 400 , height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30,"text-align" : "left", "letter-spacing":-1.5
                    }
                },
                text: "",
                parent: channelInfoDiv
            });


        }

        function _startPip () {
            log.printDbg("startPip()");
            KTW.managers.service.PipManager.activatePipLayer({pipType: KTW.managers.service.PipManager.PIP_TYPE.MULTI_VIEW});
            if(currentSizeList !== null && size>=0) {
                KTW.managers.service.PipManager.setPipSize(currentSizeList[size]);
            }
        }

        function _endPip () {
            log.printDbg("endPip()");
            setTimeout(function(){
                KTW.managers.service.PipManager.deactivatePipLayer();
            } , 10);
        }

        function _pipChannelPlay() {
            log.printDbg("_pipChannelPlay()");

            pipCurrentChannel = null;
            // VOD 시청 중 띄우는 경우에는 현재 채널을 호출
            // 일반 채널 시청중인 경우에는 이전 채널을 호출
            // 해당 채널이 없을 경우 promo 채널을 호출한다
            if (KTW.managers.service.StateManager.isVODPlayingState() === true) {
                pipCurrentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                if (KTW.CONSTANT.IS_OTS === true) {
                    // 케이블이 제거되어 신호미약 상태라면 iptv promo 채널로 변경
                    if (KTW.oipf.AdapterHandler.hwAdapter.enableTuner === false) {
                        pipCurrentChannel = KTW.oipf.AdapterHandler.navAdapter.getPromoChannel();
                    }
                    // 현재 채널이 iptv 채널이라면 이전 위성채널로 변경
                    else if (pipCurrentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ||
                        pipCurrentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                        pipCurrentChannel = main_channel_control.getBackChannel(true);
                    }
                }
            }
            // 채널을 시청 중이면 이전채널 호출
            else {
                pipCurrentChannel = main_channel_control.getBackChannel(KTW.CONSTANT.IS_OTS === true ? true : false);
            }

            // 이전채널이 자녀안심채널 설정 등으로 인해 surfable channel ring에서 제외된 경우 promo channel로 tune
            // 만약 promo channel도 자녀안심채널 설정으로 인해 surfable channel ring에서 제외된 경우 현재 채널로 tune
            if (pipCurrentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS ||
                pipCurrentChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI) {
                if (util.isFavID(pipCurrentChannel, ['SURFABLE']) === false) {
                    pipCurrentChannel = KTW.oipf.AdapterHandler.navAdapter.getPromoChannel();
                    if (util.isFavID(pipCurrentChannel, ['SURFABLE']) === false) {
                        pipCurrentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                    }
                }
            }
            else {
                if (util.isFavID(pipCurrentChannel, ['SKYLIFE_CHANNELS_SURFABLE']) === false) {
                    pipCurrentChannel = KTW.oipf.AdapterHandler.navAdapter.getSkyPlusChannel();
                    if (util.isFavID(pipCurrentChannel, ['SKYLIFE_CHANNELS_SURFABLE']) === false) {
                        pipCurrentChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                    }
                }
            }

            _callbackChListUpdate("init");

            ch_index = _getChIndex(pipCurrentChannel);

            // 타이머 없이 호출하면 changeChannel 내부 로직에서 vbo.setChannel을 찾지 못해 tune이 제대로 안되는 경우가 있다
            // 이유는 vbo의 visibility 적용이 끝나기 전에 호출되기 때문인 것으로 생각
            // 따라서 약간의 delay를 주고 호출

            setTimeout(function(){
                KTW.managers.service.PipManager.changeChannel(pipCurrentChannel);
                isRequestTune = true;
            }, 10);

            main_channel_control.addChannelEventListener(_mainChEventListener);
            mainCurrentChannel = main_channel_control.getCurrentChannel();



            pip_channel_control.addChannelEventListener(_pipChEventListener);

            KTW.managers.service.StateManager.addServiceStateListener(onServiceStateChange);
        }

        /**
         * 키입력 없을 시 가이드영역 자동숨김 타이머 등록
         */
        function _setTimer() {
            if (timer_hide !== null) {
                _clearTimer();
            }
            timer_hide = setTimeout(_hideGuide, 5000);
        }
        /**
         * 가이드영역 자동숨김 타이머 제거
         */
        function _clearTimer() {
            clearTimeout(timer_hide);
            timer_hide = null;
        }

        /**
         * 멀티화면의 키 가이드 영역을 숨긴다
         */
        function _hideGuide() {
            multiViewGuideDiv.css("display" , "none");
            channelInfoDiv.css("display" , "none");
            timer_hide = null;
        }

        /**
         * 멀티화면의 키 가이드 영역을 숨긴다
         */
        function _showGuide() {

            var channelinfoPos = {
                left : currentSizeList[size].left - 2,
                width : currentSizeList[size].width + 4,
                height: 102
            };

            if(KTW.managers.service.StateManager.isVODPlayingState() === true)  {
                textOKKeyGuide.text("채널 시청");
            }else {
                textOKKeyGuide.text("화면 전환");
            }


            if(pos_type === POS_TYPE.LEFT_DOWN || pos_type === POS_TYPE.RIGHT_DOWN) {
                /**
                 * PIP 영상 위의 채널 정보 위치
                 */
                channelinfoPos.top = currentSizeList[size].top - 102;
            }else {
                /**
                 * PIP 영상 하단 채널 정보 위치
                 */
                channelinfoPos.top = currentSizeList[size].top + currentSizeList[size].height;
            }

            channelInfoDiv.css(channelinfoPos);

            imgChannelInfoMiddleBackground.css({width:channelinfoPos.width -(12+14)});
            imgChannelInfoRightBackground.css({left:channelinfoPos.width -12});

            textChannelProgramName.css({width:channelinfoPos.width-152});

            multiViewGuideDiv.css("display" , "");
            channelInfoDiv.css("display" , "");

           // _showCurrentChannelInfo();
        }


        function onServiceStateChange (options) {
            log.printDbg("onServiceStateChange() options.nextServiceState : " + options.nextServiceState );
            if(options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED ||
                options.nextServiceState === KTW.CONSTANT.SERVICE_STATE.STANDBY) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: "PipMultiViewPopup"
                });
                return;
            }
        }

        function _moveLeftRight(isleftKey) {
            if (isleftKey === true) {
                pos_type--;
                if (pos_type < 0) {
                    pos_type = 3;
                }
            }
            else {
                pos_type++;
                if (pos_type > 3) {
                    pos_type = 0;
                }
            }

            if(pos_type === POS_TYPE.LEFT_UP) {
                currentSizeList = leftUpSizeList;
            }else if(pos_type === POS_TYPE.LEFT_DOWN) {
                currentSizeList = leftDownSizeList;

            }else if(pos_type === POS_TYPE.RIGHT_UP) {
                currentSizeList = rightUpSizeList;
            }else if(pos_type === POS_TYPE.RIGHT_DOWN) {
                currentSizeList = rightDownSizeList;
            }

            if(currentSizeList !== null && size>=0) {
                var curSizeObj = currentSizeList[size];
                curSizeObj.onlySetPipSize = true;
                KTW.managers.service.PipManager.setPipSize(curSizeObj);
            }
        }

        function _changeSize(isLarge) {
            var prev_size = size;
            if (isLarge === true) {
                if (size < SIZE.LARGE) {
                    size++;
                }
            }
            else {
                if (size > SIZE.DEFAULT) {
                    size--;
                }
            }
            if (size !== prev_size) {
                if(currentSizeList !== null && size>=0) {
                    var curSizeObj = currentSizeList[size];
                    curSizeObj.onlySetPipSize = true;
                    KTW.managers.service.PipManager.setPipSize(curSizeObj);
                }
            }
        }


        /**
         * 채널목록이 변경되는 경우 miniEPG에서 사용하는 channel list도 update 해주기 위해
         * navAdapter를 통해 등록하는 event listener
         */
        function _callbackChListUpdate(evt) {
            log.printDbg('_callbackChListUpdate() evt = ' + evt);
            kt_ch_list = KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_SURFABLE);
            kt_ch_list = KTW.utils.epgUtil.reconstructionChList(kt_ch_list , true);
            if (KTW.CONSTANT.IS_OTS === true) {
                sky_ch_list = KTW.oipf.AdapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_SURFABLE);
                sky_ch_list = KTW.utils.epgUtil.reconstructionChList(sky_ch_list , true);
            }


            _selectChList();
        }

        // 현재 재생 중인 채널에 따라 채널 리스트를 가져 온다.
        // DVB_S/DVB_S2 : skylife 채널 리스트
        // IPTV_SDS/IPTV_URI : kt iptv 채널 리스트
        function _selectChList() {
            log.printDbg('_selectChList() cur_channel.idType : ' + pipCurrentChannel.idType);

            switch (pipCurrentChannel.idType) {
                case KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS :
                case KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI :
                    ch_list = kt_ch_list;
                    break;
                case KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S :
                case KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S2 :
                    ch_list = sky_ch_list;
                    break;
                default :
                    if (KTW.CONSTANT.IS_OTS === true) {
                        ch_list = sky_ch_list;
                    }
                    else {
                        ch_list = kt_ch_list;
                    }
                    break;
            }
            log.printDbg('_selectChList() return ch_list length : ' + ch_list.length);
        }

        function _getChIndex(channel) {
            for (var i=0; i<ch_list.length; i++) {
                if (channel.ccid === ch_list[i].ccid) {
                    return i;
                }
                // 해당 채널이 skipped channel인 경우
                else if (channel.majorChannel < ch_list[i].majorChannel) {
                    return i - 0.5;
                }
            }
            return 0;
        }



        function _showCurrentChannelInfo(showChannel , evt) {
            log.printDbg("_showCurrentChannelInfo() showChannel: " + ((showChannel !== undefined && showChannel !== null) ? JSON.stringify(showChannel) : "is Null"));
            log.printDbg("_showCurrentChannelInfo() evt: " + ((evt !== undefined && evt !== null) ? JSON.stringify(evt) : "is Null"));
            var ch = null;
            if (showChannel !== undefined && showChannel !== null) {
                ch = showChannel;
            } else {
                if (evt !== undefined && evt !== null) {
                    pipCurrentChannel = KTW.utils.epgUtil.reconstructionChList([evt.channel] , true)[0];
                    ch_index = _getChIndex(pipCurrentChannel);
                }
            }

            if (ch_list !== null && ch_list.length > 0) {
                if (ch_index < ch_list.length) {
                    var tempIndex = ch_index % 1;

                    if (tempIndex === 0) {
                        textChannelNumber.text(util.numToStr(ch_list[ch_index].majorChannel, 3));
                        textChannelName.text(ch_list[ch_index].name);
                        ch = ch_list[ch_index];
                    } else {
                        textChannelNumber.text(util.numToStr(pipCurrentChannel.majorChannel, 3));
                        textChannelName.text(pipCurrentChannel.name);
                        ch = pipCurrentChannel;
                    }
                }
            }

            if(ch !== null) {
                var pip_prog_list = KTW.oipf.adapters.ProgramSearchAdapter.getPrograms(null, null, null, ch);
                var pip_prog_index = -1;
                var program = null;
                if(pip_prog_list !== undefined && pip_prog_list !== null && pip_prog_list.length>0) {

                    pip_prog_list = KTW.utils.epgUtil.revisePrograms(ch, pip_prog_list, true);
                    
                    pip_prog_index = 0;

                    var now = new Date().getTime();
                    if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                        now = Math.floor(now/1000);
                    }


                    var prog = pip_prog_list[pip_prog_index];


                    var prog = pip_prog_list[pip_prog_index];
                    while(prog.startTime + prog.duration < now && pip_prog_index < pip_prog_list.length - 1) {
                        pip_prog_index++;
                        prog = pip_prog_list[pip_prog_index];
                        if (prog == null) {
                            pip_prog_index = -1;
                            break;
                        }
                    }


                }

                if(pip_prog_index>=0) {
                    program = pip_prog_list[pip_prog_index];
                }
                
                if(program !== null) {
                    if(ch.desc === 2) {
                        textChannelProgramName.text(ch.name);
                    }else {
                        if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(ch, true) === true) {
                            var singer = KTW.utils.epgUtil.getSinger(program);
                            if (singer !== null) {
                                textChannelProgramName.text(program.name + " / " + singer);
                            }else {
                                textChannelProgramName.text(program.name);
                            }

                        }else {
                            textChannelProgramName.text(program.name);
                        }
                    }
                }else {
                    if(pip_prog_list !== undefined && pip_prog_list !== null && pip_prog_list.length>0) {
                        textChannelProgramName.text(KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL);
                    }else {
                        textChannelProgramName.text(KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE);
                    }
                }
            }
        }

        function _mainChEventListener(evt) {
            log.printDbg('_mainChEventListener evt: ' + JSON.stringify(evt));
            if (evt == null || evt.channel == null) {
                return;
            }

            var ch = evt.channel;
            // 메인채널 변경을 요청했으면(swap 등) false로 변경하여 초기화 해주고
            // 이 경우 변경 요청한 채널은 로컬변수 ch_main에 갖고 있으므로 update 생략


            var issearch = false;
            if (ch_list !== null && ch_list.length > 0) {
                for(var i=0;i<ch_list.length;i++) {
                    if(ch_list[i].ccid === ch.ccid) {
                        issearch = true;
                        if(ch_list[i].desc === 2) {
                            log.printDbg('_mainChEventListener ch_list[i].desc === 2');
                            KTW.ui.LayerManager.deactivateLayer({
                                id: "PipMultiViewPopup"
                            });
                            return;
                        }
                    }
                }
            }

            if(issearch === false) {
                ch = KTW.utils.epgUtil.reconstructionCh(ch)[0];
                if(ch.desc === 2) {
                    KTW.ui.LayerManager.deactivateLayer({
                        id: "PipMultiViewPopup"
                    });
                    return;
                }
            }

            mainCurrentChannel = ch;


            if ((ch.isUHD === true && KTW.CONSTANT.IS_OTS === true)) {
                KTW.managers.service.SimpleMessageManager.showUHDChannelErrorNotiMessage();
                KTW.ui.LayerManager.deactivateLayer({
                    id: "PipMultiViewPopup"
                });
                return;
            }

            if (evt.permission === KTW.nav.Def.CHANNEL.PERM.BLOCKED ||
                evt.permission === KTW.nav.Def.CHANNEL.PERM.PR_BLOCKED ||
                evt.permission === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED) {
                KTW.ui.LayerManager.deactivateLayer({
                    id: "PipMultiViewPopup"
                });
                return;
            }else {

            }
        }

        function _pipChEventListener(evt) {
            log.printDbg('_pipChEventListener() evt: ' + JSON.stringify(evt));
            if (evt != null) {
                var ch = evt.channel;

                if (ch != null) {
                    log.printDbg('_pipChEventListener() evt: ' + JSON.stringify(ch));
                    log.printDbg('_pipChEventListener() evt.type : ' + evt.type);
                    log.printDbg('_pipChEventListener() evt.permission : ' + evt.permission);

                    if (evt.type !== KTW.nav.Def.CHANNEL.EVENT.PERM_UPDATED) {
                        ch_pip = ch;
                    } else if (evt.type === KTW.nav.Def.CHANNEL.EVENT.PERM_UPDATED) {
                        if (ch_pip === null || ch_pip.ccid !== ch.ccid) {
                            ch_pip = ch;
                        }
                    }


                    if (ch_pip_evt !== null) {
                        var tempch = ch_pip_evt.channel;
                        log.printDbg('tempch.ccid : ' + tempch.ccid + ' , ch.ccid : ' + ch.ccid);
                        if (tempch.ccid === ch.ccid) {
                            log.printDbg('ch_pip_evt.permission : ' + ch_pip_evt.permission);

                            if (ch_pip_evt.permission === KTW.nav.Def.CHANNEL.PERM.BLOCKED ||
                                ch_pip_evt.permission === KTW.nav.Def.CHANNEL.PERM.PR_BLOCKED ||
                                ch_pip_evt.permission === KTW.nav.Def.CHANNEL.PERM.NOT_SUBSCRIBED) {

                                if (evt.permission === KTW.nav.Def.CHANNEL.PERM.PREVIEW) {
                                    return;
                                }
                            } else if (ch_pip_evt.permission === KTW.nav.Def.CHANNEL.PERM.PREVIEW
                                && ch_pip_evt.type === KTW.nav.Def.CHANNEL.EVENT.PERM_UPDATED) {

                                if (evt.type === KTW.nav.Def.CHANNEL.EVENT.REQUESTED) {
                                    return;
                                }
                            }

                        }
                    }
                    ch_pip_evt = evt;
                    if(channelQueue.is_empty === true) {
                        _showCurrentChannelInfo(null , ch_pip_evt);
                        isRequestTune = false;
                    }

                    // if(isRequestTune === true && ch_pip_evt.channel.ccid === pipCurrentChannel.ccid) {
                    //     _showCurrentChannelInfo(null , ch_pip_evt);
                    //     isRequestTune = false;
                    // }else if(isRequestTune === false){
                    //     _showCurrentChannelInfo(null , ch_pip_evt);
                    // }



                    _showGuide();
                    _setTimer();

                }
            }
        }



        /**
         * 멀티화면에서 상하키에 대한 처리
         * @param isUp : KEY_UP : true , KEY_DOWN : false
         * @private
         */

        var move_timer = null;
        function _moveUpDown(isUp) {
            log.printDbg('_moveUpDown()');
            if (isUp === true) {
                // skipped channel의 경우 getChIndex()에서 소수점으로 구분하기 때문에 이와 같이 처리한다
                ch_index = Math.floor(ch_index);
                ch_index++;
                if (ch_index >= ch_list.length) {
                    ch_index = 0;
                }
            }
            else {
                // skipped channel의 경우 getChIndex()에서 소수점으로 구분하기 때문에 이와 같이 처리한다
                ch_index = Math.ceil(ch_index);
                ch_index--;
                if (ch_index < 0) {
                    ch_index = ch_list.length - 1;
                }
            }
            pipCurrentChannel = ch_list[ch_index];
            // jquery gui 작업을 먼저 수행한 이후에 channel tune을 호출한다
            // channel tune을 하는 동안에는 jquery gui 동작포함 다 정지상태가 되기 때문에
            // 같이 호출하면 channel tune 하는 시간 동안 gui 동작 처리에 term이 발생하여 보기 싫어짐
            if(move_timer !== null) {
                clearTimeout(move_timer);
                move_timer = null;
            }
            move_timer = setTimeout(function(){
                _changeChannel(null, pipCurrentChannel);
                move_timer = null;
            }, 10);

        }


        /**
         * 채널변경 요청
         * 멀티화면에서 채널변경을 요청한 뒤 그에 대한 event를 받기 전까지 입력되는 채널변경 요청에 대해서는
         * 바로 채널변경을 요청하지 않고 queue에 쌓아두었다가 event를 받고나서 처리한다
         * 채널변경을 처리 중이 아닌 상태라면 채널변경 요청을 알리고(ch_change_req) 바로 채널변경을 요청한다
         */
        function _changeChannel(main_ch, pip_ch) {
            log.printDbg("_changeChannel()");

            log.printDbg('_changeChannel() pip_ch_change_req: ' + pip_ch_change_req);
            log.printDbg('main_ch: ' + (main_ch == null ? null : main_ch.name));
            log.printDbg('pip_ch: ' + (pip_ch == null ? null : pip_ch.name));

            if (timer_fast_tune != null || pip_ch_change_req === true) {
                is_fast_tune = true;
            }
            if (timer_fast_tune != null) {
                clearTimeout(timer_fast_tune);
            }

            // 멀티화면 채널 변경요청을 처리하는 중이라면 queue에 입력
            if (is_fast_tune === true) {
                _showCurrentChannelInfo(pip_ch);
                var main_channel = main_ch;
                if (main_channel == null) {
                    if (channelQueue.is_empty === false) {
                        main_channel = channelQueue.last.ch_main;
                    }
                }
                channelQueue.put(main_channel, pip_ch);
            }
            // 채널 변경요청 처리중이 아니라면 바로 채널 변경요청
            else {
                // swap을 연속으로 빠르게 요청하는 경우
                // main av에 pip channel tune 요청에 대한 event를 받기 전에 다시 main channel tune을 요청하면
                // channel_control 에서 동일 채널로 간주하고 미니가이드를 띄우게 되고, 이 때 stack을 날리면서 멀티화면은 종료된다
                // 이를 방지하기위해 changechannel을 호출할 때 forced를 true로 호출한다
                if (main_ch != null) {
                    main_channel_control.changeChannel(main_ch, true, true);
                }
                pip_ch_change_req = true;
                KTW.managers.service.PipManager.changeChannel(pip_ch);
                pipCurrentChannel = pip_ch;
                isRequestTune = true;
                _showCurrentChannelInfo(pipCurrentChannel , null);
            }

            _setFastTuneTimer();
        }

        function _setFastTuneTimer() {
            timer_fast_tune = setTimeout(function() {
                is_fast_tune = false;
                pip_ch_change_req = false;
                timer_fast_tune = null;
                // 채널변경 요청 queue가 쌓여 있으면 마지막으로 queue에 입력된 내용을 처리하고 queue를 비운다
                if (channelQueue.is_empty === false) {
                    // 여기에서 채널 변경할 때 _changeChannel()를 통해 변경요청하면
                    // empty가 아니고 req == true 상태이기 때문에 tune을 하는게 아니라 queue에 또 다시 쌓게 된다
                    // 따라서 여기에서는 queue의 마지막 채널들로 tune하고 queue를 release 해준다
                    var tmp_ch_main = channelQueue.last.ch_main,
                        tmp_ch_pip = channelQueue.last.ch_pip;
                    channelQueue.release();
                    _changeChannel(tmp_ch_main, tmp_ch_pip);
                }
            }, DELAY_FAST_TUNE);
        }

        /**
         * 현재화면과 멀티화면의 채널을 전환
         */
        function _swapPip() {
            log.printDbg('swapPip()');

            // vod 재생 중인 상태에서는 종료팝업을 띄운 후에 channel tune 하면서 멀티화면 기능을 종료한다
            if (KTW.managers.service.StateManager.isVODPlayingState() === true) {

                var channel = pipCurrentChannel;

                var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                if (vodModule) {
                    var result = vodModule.execute({
                        method: "stopVodWithEndPopup",
                        params: {
                            channelObj: channel,
                            withPopup: true,
                            notChangeService: false,
                            noRecommend : true
                        }
                    });
                } else {
                    // TODO 2017.01.17 dhlee something error.
                }
                return;
            }

            // 동일 채널인 경우 굳이 swap할 필요가 없지
            if (pipCurrentChannel.ccid === mainCurrentChannel.ccid) {
                return;
            }

            // swap을 연속으로 빠르게 요청하는 경우
            // main av에 pip channel tune 요청에 대한 event를 받기 전에 다시 main channel tune을 요청하면
            // channel_control 에서 동일 채널로 간주하고 미니가이드를 띄우게 되고, 이 때 stack을 날리면서 멀티화면은 종료된다
            // 이를 방지하기위해 changechannel을 호출할 때 forced를 true로 호출한다
            if (pipCurrentChannel.isUHD === true && KTW.CONSTANT.IS_OTS === true) {
                KTW.managers.service.SimpleMessageManager.showUHDChannelErrorNotiMessage();
                KTW.ui.LayerManager.deactivateLayer({
                    id: "PipMultiViewPopup"
                });
                return;
            }else {
                /**
                 * UI Flipbook 1.21 Page 996 부화면이 성인채널 인 경우 멀티화면 종료함.
                 */
                if(pipCurrentChannel.desc === 2) {
                    main_channel_control.changeChannel(pipCurrentChannel, true, true);
                    KTW.ui.LayerManager.deactivateLayer({
                        id: "PipMultiViewPopup"
                    });
                    return;
                }
            }

            _showGuide();
            _changeChannel(pipCurrentChannel, mainCurrentChannel);
            _setTimer();

            var prev_ch_main = mainCurrentChannel;
            mainCurrentChannel = pipCurrentChannel;
            pipCurrentChannel = prev_ch_main;

            ch_index = _getChIndex(pipCurrentChannel);
        }


        /**
         * OTS 신호미약 event listener
         */
        // function _weakSignalListener(state) {
        //     log.printDbg('_weakSignalListener() state = ' + state);
        //     //CONNECTED
        //     if (state === 0) {
        //        if (isShowingSignalIframe === true) {
        //            isShowingSignalIframe = false;
        //            KTW.managers.service.PipManager.showIframe(KTW.managers.service.PipManager.BACK_IFRAME_TYPE.ETC , null);
        //            _showCurrentChannelInfo(pipCurrentChannel);
        //            _showGuide();
        //            _setTimer();
        //        }
        //     }
        //     // DISCONNECTED
        //     else {
        //         KTW.managers.service.PipManager.showIframe(KTW.managers.service.PipManager.BACK_IFRAME_TYPE.WEAK_SIGNAL , null);
        //     }
        // }


        function _sendMessageToObserver(method) {
            var message = {
                "method": method,
                "from": KTW.CONSTANT.APP_ID.HOME,
                "to" : KTW.CONSTANT.APP_ID.OBSERVER };
            log.printDbg("_sendMessageToObserver() message : " + JSON.stringify(message));
            KTW.managers.MessageManager.sendMessage(message);
        }

        /**
         * 채널변경에 대한 event가 올라오기 전에 채널변경을 연속해서 빠르게 요청하는 경우
         * 바로 처리하지 않고 queue에 쌓아두었다가 마지막 입력에 대한 동작만 처리하도록 하기 위한 용도
         * main채널만 요청하는 경우는 swap 밖에 없기 때문에(CH_UP/DOWN은 MW가 처리함)
         * 이러한 경우는 queue를 사용하지 않는다
         * pip채널만 변경하거나 pip채널 및 main채널을 동시에 변경하는 경우에만 queue에 입력한다
         */
        var channelQueue = (function(){
            var last_queue = null;

            function _put(main_ch, pip_ch) {
                last_queue = {
                    ch_main: main_ch,
                    ch_pip: pip_ch
                };
            }
            function _release() {
                last_queue = null;
            }

            return {
                get is_empty() {
                    return last_queue == null;
                },
                get last() {
                    return last_queue;
                },
                put: _put,
                release: _release
            };
        }());

    };

    KTW.ui.view.popup.PipMultiViewPopup.id = "PipMultiViewPopup";
    KTW.ui.view.popup.PipMultiViewPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.PipMultiViewPopup.prototype.constructor = KTW.ui.view.popup.PipMultiViewPopup;

//Override create function
    KTW.ui.view.popup.PipMultiViewPopup.prototype.create = function() {
        this.createView();
    };

    KTW.ui.view.popup.PipMultiViewPopup.prototype.destroy = function() {
        this.hideView();
    };

    KTW.ui.view.popup.PipMultiViewPopup.prototype.show = function(options) {

        this.showView(options);
    };

    KTW.ui.view.popup.PipMultiViewPopup.prototype.hide = function(options) {
        this.hideView(options);
    };

    KTW.ui.view.popup.PipMultiViewPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);
        if (consumed === false) {
            return KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }
        return consumed;
    };


})();
