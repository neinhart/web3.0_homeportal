/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>TVPayDummyView</code>
 *
 * @author dj.son
 * @since 2018-03-07
 *
 * TV 페이 모듈 더미 팝업
 */

"use strict";

(function() {
    KTW.ui.view.popup.TVPayDummyView = function() {
        KTW.ui.View.call(this);

        this.hide = function () {
            KTW.utils.Log.printDbg("hide()");

            KTW.managers.service.TVPayManager.desctoryTVPayModule();
        };

        /**
         * key event 처리
         *
         * @param key_code
         */
        this.controlKey = function (key_code) {
            /**
             * [dj.son] 일반적인 네비 키 & ok 키 & 이전 키 & 나가기 키 등은 tv 페이 모듈에서 처리, 나머지 키 들은 전부 tv 페이 모듈을 죽이도록 수정
             * -
             */

            KTW.ui.LayerManager.deactivateLayer({id: this.parent.id});
        }
    };

    KTW.ui.view.popup.TVPayDummyView.prototype = new KTW.ui.View();
    KTW.ui.view.popup.TVPayDummyView.prototype.constructor = KTW.ui.view.TVPayDummyView;

    KTW.ui.view.popup.TVPayDummyView.prototype.create = function () {
        KTW.ui.View.prototype.create.call(this);
    };

    KTW.ui.view.popup.TVPayDummyView.prototype.show = function () {
        KTW.ui.View.prototype.show.call(this);
    };

    KTW.ui.view.popup.TVPayDummyView.prototype.hide = function () {
        KTW.ui.View.prototype.hide.call(this);

        this.hide();
    };

    KTW.ui.view.popup.TVPayDummyView.prototype.handleKeyEvent = function (key_code) {
        return this.controlKey(key_code);
    };
})();
