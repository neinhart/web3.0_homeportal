/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>NetworkErrorPopup</code>
 * @author jjh1117
 * @since 2017-03-02
 */

"use strict";

(function () {
    KTW.ui.view.popup.BizPurchaseSuccessPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var params = null;

        var bizProductTitle = null;
        var btnList = [];

        var parentDiv = null;
        
        var productName = null;
        var period = null;
        var productPrice = null;

        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();

            if (params !== null) {
                productName = params.data.productName;
                period = params.data.period;
                productPrice = params.data.price;
            }
            
            var div = this.parent.div;
            _createView(div,this);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;

            switch (key_code) {
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    break;

                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                case KTW.KEY_CODE.BACK:
                case KTW.KEY_CODE.EXIT:
                    KTW.ui.LayerManager.deactivateLayer({
                        id: this.parent.id
                    });
                    consumed = true;
            }
            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});
            parentDiv = parent_div;
        }


        function _showView() {
            log.printDbg("_showView()");
            _createElement(parentDiv);
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _createElement(parent_div) {

            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 332 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center", "letter-spacing":-1.65,
                        display:""
                    }
                },
                text: "결제 완료",
                parent: popupContainerDiv
            });


            bizProductTitle = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "product",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 407 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 54,
                        color: "rgba(255, 255, 255, 1)", "font-size": 52 , "text-align": "center","letter-spacing":-2.6,
                        display:""
                    }
                },
                text: productName,
                parent: popupContainerDiv
            });


            var productInfoDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute", left: 0, top: 481, width: 1920, height: 35,
                        "text-align": "center", "font-size": 30, "letter-spacing": -1.5
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 0.4)"
                    }
                },
                text: "가격 : ",
                parent: productInfoDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: util.setComma(Number(productPrice)) + "원",
                parent: productInfoDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead M", "font-size": 30 , color: "rgba(255, 255, 255, 0.3)" , "margin-left" : 12 , "margin-right" : 12
                    }
                },
                text: "|",
                parent: productInfoDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 0.4)"
                    }
                },
                text: "기간 : ",
                parent: productInfoDiv
            });


            var periodData = Number(period);
            var tempstr = "";
            if(periodData<24) {
                tempstr =  period + "시간 시청 가능";
            }else {
                var temp = Math.ceil(period / 24) ;
                tempstr =  temp + "일 간 시청 가능";
            }

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    css: {
                        "font-family": "RixHead L", "font-size": 30 , color: "rgba(255, 255, 255, 1)"
                    }
                },
                text: tempstr,
                parent: productInfoDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 578 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5,
                        display:""
                    }
                },
                text: "감사합니다. 결제가 완료되었습니다",
                parent: popupContainerDiv
            });



            var btnDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "btn_div_1",
                    css: {
                        position: "absolute", left: 825, top: 686, width: 280, height: 62
                    }
                },
                parent: popupContainerDiv
            });

            var focusBtn = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "focus_btn_1",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 280, height: 62,
                        "background-color": "rgba(210,51,47,1)",display:""
                    }
                },
                parent: btnDiv
            });

            var textBtn = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "text_btn_1",
                    class: "font_m" ,
                    css: {
                        position: "absolute", left: 0, top: 17, width: 280 , height: 32,
                        color: "rgba(255,255,255,1)", "font-size": 30,"text-align" : "center",
                        "letter-spacing" : -1.3
                    }
                },
                text: "닫기",
                parent: btnDiv
            });

            var btnobj = {
                root_div : btnDiv,
                focus_btn : focusBtn,
                text_btn : textBtn,
                btn_type : -1
            };
            btnList[btnList.length] = btnobj;
        }

    };

    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype.constructor = KTW.ui.view.popup.BizPurchaseSuccessPopup;

    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype.create = function() {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype.destroy = function() {
        this.destroyView();
    };
    KTW.ui.view.popup.BizPurchaseSuccessPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);
        return consumed;
    };

})();
