/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>NetworkErrorPopup</code>
 * @author jjh1117
 * @since 2017-03-02
 */

"use strict";

(function () {
    KTW.ui.view.popup.NetworkErrorPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var params = null;
        var dialogueId = null;

        //var OTV_MAIN_MESSAGE = ["셋톱 박스의 인터넷 연결 선이 빠져 있거나, 접속 불량입니다"];
        //
        //var OTS_MAIN_MESSAGE = ["셋톱 박스의 인터넷 연결 선이 빠져 있거나, 접속 불량입니다","olleh tv 모든 서비스를 이용하시려면 아래 내용을 확인해 주세요"];
        //
        //var SUB_MESSAGE1 = "모뎀/홈허브-셋톱박스(WAN)간 인터넷 연결상태를 확인해 주세요";
        //var SUB_MESSAGE2 = "모뎀/홈허브 전원 플러그를 빼고 다시 연결해 주세요";
        //var SUB_MESSAGE3 = "셋톱 박스 전원 플러그를 빼고 다시 연결해 주세요";

        //var textMainMessage1 = null;
        //var textMainMessage2 = null;
        //
        //var textSubMessage1 = null;
        //var textSubMessage2 = null;
        //var textSubMessage3 = null;
        var textSaidString = null;

        var closeDiv = null;
        var rebootDiv = null;

        var focusIndex = 0;

        var callbackFunc = null;


        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            if(params !== null) {
                if(KTW.CONSTANT.IS_OTS === true) {
                    callbackFunc = params.data.callback;
                }
            }
            var div = this.parent.div;
            _createView(div,this);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
        };

        this.destroyView = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;

            switch (key_code) {
                case KTW.KEY_CODE.LEFT:
                case KTW.KEY_CODE.RIGHT:
                    if(KTW.CONSTANT.IS_OTS === true) {
                        var focusBtnDiv = null;
                        if(focusIndex ===0) {
                            focusBtnDiv = closeDiv;
                        }else {
                            focusBtnDiv = rebootDiv;
                        }
                        _unFocusBtnView(focusBtnDiv);
                        if(key_code === KTW.KEY_CODE.RIGHT) {
                            focusIndex++;
                        }else {
                            focusIndex--;
                        }

                        if(focusIndex<0) {
                            focusIndex = 1;
                        }else if(focusIndex>1){
                            focusIndex = 0;
                        }

                        if(focusIndex ===0) {
                            focusBtnDiv = closeDiv;
                        }else {
                            focusBtnDiv = rebootDiv;
                        }

                        _foucsBtnView(focusBtnDiv);
                        consumed = true;
                    }
                    break;

                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    if(KTW.CONSTANT.IS_OTS === true) {
                        if(callbackFunc !== null) {
                            callbackFunc(focusIndex);
                            consumed = true;
                        }
                    }
                    break;
            }
            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});
            _createElement(parent_div);
        }


        function _showView() {
            log.printDbg("_showView()");
            var saidString = "";
            saidString += "SAID : ****";
            saidString +=  KTW.SAID.substring(4);
            textSaidString.text(saidString);
            textSaidString.css("display" , "");

            if(KTW.CONSTANT.IS_OTS === true) {
                var focusBtnDiv = closeDiv;
                _unFocusBtnView(focusBtnDiv);
                if(focusIndex ===0) {
                    focusBtnDiv = closeDiv;
                }else {
                    focusBtnDiv = rebootDiv;
                }
                _foucsBtnView(focusBtnDiv);
            }
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _createElement(parent_div) {
            var textMainMessage1;
            var textMainMessage2;

            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 130 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center", "letter-spacing":-1.65,
                        display:""
                    }
                },
                text: KTW.ERROR.EX_CODE.NETWORK.EX001.title,
                parent: popupContainerDiv
            });


            var rootDiv =  util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "root_div",
                    css: {
                        position: "absolute",
                        left: 431, top:179,width: 1058,height:638
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_l_img",
                    src: "images/popup/pop_bg_noti_02_l.png",
                    css: {
                        position: "absolute", left: 0, top: 0, width: 49, height: 638
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_m_img",
                    src: "images/popup/pop_bg_noti_02_m.png",
                    css: {
                        position: "absolute", left: 49, top: 0, width: 960, height: 638
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_l_img",
                    src: "images/popup/pop_bg_noti_02_r.png",
                    css: {
                        position: "absolute", left: 1009, top: 0, width: 49, height: 638
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "center_img",
                    src: "images/popup/pop_img_network_er.png",
                    css: {
                        position: "absolute", left: 591-431, top: 348-179, width: 738, height: 282
                    }
                },
                parent: rootDiv
            });

            textMainMessage1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mainmessage_1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 256-179 , width: 1058, height: 34,
                        color: "rgba(255, 255, 255, 1)", "font-size": 32 , "text-align": "center","letter-spacing":-1.6,
                        display:""
                    }
                },
                text: "",
                parent: rootDiv
            });

            textMainMessage2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "mainmessage_1",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 303-179 , width: 1058, height: 34,
                        color: "rgba(255, 255, 255, 1)", "font-size": 32 , "text-align": "center","letter-spacing":-1.6,
                        display:""
                    }
                },
                text: "",
                parent: rootDiv
            });


            if(KTW.CONSTANT.IS_OTS === true) {
                textMainMessage1.text(KTW.ERROR.EX_CODE.NETWORK.EX001.message[0]);
                textMainMessage2.text(KTW.ERROR.EX_CODE.NETWORK.EX001.message[1]);
            }else {
                textMainMessage1.text(KTW.ERROR.EX_CODE.NETWORK.EX001.message[0]);
                textMainMessage1.css({top:274-179});
                textMainMessage2.css("dislplay" , "none");
            }


            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/popup/pop_num_1.png",
                    css: {
                        position: "absolute", left: 568-431, top: 626-179, width: 37, height: 37
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/popup/pop_num_2.png",
                    css: {
                        position: "absolute", left: 568-431, top: 670-179, width: 37, height: 37
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "",
                    src: "images/popup/pop_num_3.png",
                    css: {
                        position: "absolute", left: 568-431, top: 714-179, width: 37, height: 37
                    }
                },
                parent: rootDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "submessage_1",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 621-431 , top: 631-179 , width: 882, height: 30,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 28 , "text-align": "left","letter-spacing":-1.4,
                        display:""
                    }
                },
                text: KTW.ERROR.EX_CODE.NETWORK.EX001.message[2],
                parent: rootDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "submessage_2",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 621-431 , top: 675-179 , width: 882, height: 30,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 28 , "text-align": "left","letter-spacing":-1.4,
                        display:""
                    }
                },
                text: KTW.ERROR.EX_CODE.NETWORK.EX001.message[3],
                parent: rootDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "submessage_3",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 621-431 , top: 720-179 , width: 882, height: 30,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 28 , "text-align": "left","letter-spacing":-1.4,
                        display:""
                    }
                },
                text: KTW.ERROR.EX_CODE.NETWORK.EX001.message[4],
                parent: rootDiv
            });



            var bottomMessage = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "sub_desc",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 493 , top: 853 , width: 755, height: 29,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 27 , "text-align": "left","letter-spacing":-1.35,
                        display:""
                    }
                },
                text: KTW.ERROR.EX_CODE.NETWORK.EX001.message[5],
                parent: popupContainerDiv
            });


            var line = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 1204, top: 855, width: 2, height: 22,
                        "background-color": "rgba(255, 255, 255, 0.2)"
                    }
                },
                parent: popupContainerDiv
            });

            textSaidString = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "said",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 1216 , top: 856 , width: 250, height: 26,
                        color: "rgba(255, 255, 255, 0.3)", "font-size": 24 , "text-align": "left","letter-spacing":-1.2,
                        display:"none"
                    }
                },
                text: "",
                parent: popupContainerDiv
            });

            if(KTW.CONSTANT.IS_OTS) {
                bottomMessage.css({top:925});
                line.css({top:927});
                textSaidString.css({top:928});

                closeDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute",
                            left: 673, top: 833, width: 280, height: 62,
                            border: "solid 2px rgba(190, 190, 190, 1)" , "background-color": "rgba(1, 1, 1, 0.5)"
                        }
                    },
                    parent: popupContainerDiv
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 0 , top: 18 , width: 280, height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5,
                            display:""
                        }
                    },
                    text: "닫기",
                    parent: closeDiv
                });

                rebootDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: "",
                        css: {
                            position: "absolute",
                            left: 967, top: 833, width: 280, height: 62,
                            border: "solid 2px rgba(190, 190, 190, 1)" , opacity:.5
                        }
                    },
                    parent: popupContainerDiv
                });

                util.makeElement({
                    tag: "<span />",
                    attrs: {
                        id: "",
                        class: "font_m",
                        css: {
                            position: "absolute", left: 0 , top: 18 , width: 280, height: 32,
                            color: "rgba(255, 255, 255, 0.7)", "font-size": 30 , "text-align": "center","letter-spacing":-1.5,
                            display:""
                        }
                    },
                    text: "재부팅",
                    parent: rebootDiv
                });
            }
        }


        function _foucsBtnView(buttonDiv) {
            buttonDiv.css({ "background-color": "rgba(210, 51, 47, 1)", border: "" });
            buttonDiv.find("span").css({ color: "rgba(255, 255, 255, 1)", "font-family": "RixHead M" });
        }

        function _unFocusBtnView(buttonDiv) {
            buttonDiv.css({ "background-color": "rgba(1, 1, 1, 0.5)", border: "solid 2px rgba(190, 190, 190, 0.5)" });
            buttonDiv.find("span").css({ color: "rgba(255, 255, 255, 0.7)", "font-family": "RixHead L" });
        }
    };

    KTW.ui.view.popup.NetworkErrorPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.NetworkErrorPopup.prototype.constructor = KTW.ui.view.popup.NetworkErrorPopup;

    KTW.ui.view.popup.NetworkErrorPopup.prototype.create = function() {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.NetworkErrorPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.NetworkErrorPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.NetworkErrorPopup.prototype.destroy = function() {
        this.destroyView();
    };
    KTW.ui.view.popup.NetworkErrorPopup.prototype.handleKeyEvent = function(key_code) {

        // log.printDbg("handleKeyEvent() this.enableDCA : " + this.enableDCA);
        // log.printDbg("handleKeyEvent() this.enableChannelKey : " + this.enableChannelKey);
        // log.printDbg("handleKeyEvent() this.enableMenuKey : " + this.enableMenuKey);
        // log.printDbg("handleKeyEvent() this.enableExitKey : " + this.enableExitKey);
        // log.printDbg("handleKeyEvent() this.enableBackKey : " + this.enableBackKey);
        // log.printDbg("handleKeyEvent() this.enableHotKey : " + this.enableHotKey);
        // log.printDbg("handleKeyEvent() this.enableHotKey : " + this.enableHotKey);
        //
        // log.printDbg("handleKeyEvent() this.hideAnyKey : " + this.hideAnyKey);
        // log.printDbg("handleKeyEvent() this.bypassKey : " + this.bypassKey);

        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };

})();
