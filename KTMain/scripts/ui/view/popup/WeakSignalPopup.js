/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>WeakSignalPopup</code>
 * @author jjh1117
 * @since 2017-03-02
 */

"use strict";

(function () {
    KTW.ui.view.popup.WeakSignalPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var popupContainerDiv = null;
        var params = null;
        var callbackFunc = null;

        var MAIN_MESSAGE = ["방송신호가 고르지 못합니다", '확인 버튼을 선택하면 선명한 화질로 계속 시청하실 수 있습니다'];

        var SUB_MESSAGE = "※ 일부 지상파 채널은 일반 화질로 제공";
        var closeDiv = null;

        var focusIndex = 0;

        this.createView = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            if(params !== null) {
                callbackFunc = params.data.callback;
            }
            var div = this.parent.div;
            _createView(div,this);
        };

        this.showView = function(options) {
            log.printDbg("showView()");
            if (!options || !options.resume) {
                _showView();
            }
        };

        this.hideView = function() {
            log.printDbg("hide()");
        };

        this.destroy = function() {
            log.printDbg("destroy()");
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey(), key_code = " + key_code);
            var consumed = false;

            switch (key_code) {
                case KTW.KEY_CODE.OK:
                case KTW.KEY_CODE.ENTER:
                    if (KTW.CONSTANT.IS_OTS === true) {
                        if(callbackFunc !== null) {
                            callbackFunc(focusIndex);
                        }
                        consumed = true;
                    }
                    break;
            }
            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView(parent_div , _this) {
            log.printDbg("_createView()");
            parent_div.attr({class: "arrange_frame"});
            _createElement(parent_div);
        }


        function _showView() {
            log.printDbg("_showView()");
        }

        function _hideView() {
            log.printDbg("_hideView()");
        }

        function _createElement(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT,
                        "background-color": "rgba(0, 0, 0, 0.85)"
                    }
                },
                parent: parent_div
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "popup_title_text",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 0 , top: 358 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(221, 175, 120, 1)", "font-size": 33 , "text-align": "center",
                        display:"", "letter-spacing":-1.65
                    }
                },
                text: "신호 미약",
                parent: popupContainerDiv
            });


            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 431 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 47,
                        color: "rgba(255, 255, 255, 0.9)", "font-size": 45 , "text-align": "center",
                        display:"", "letter-spacing":-2.25
                    }
                },
                text: MAIN_MESSAGE[0],
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 503 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 35,
                        color: "rgba(255, 255, 255, 1)", "font-size": 33 , "text-align": "center",
                        display:"", "letter-spacing":-1.65
                    }
                },
                text: MAIN_MESSAGE[1],
                parent: popupContainerDiv
            });



            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 572 , width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 32,
                        color: "rgba(255, 255, 255, 0.5)", "font-size": 30 , "text-align": "center",
                        display:"", "letter-spacing":-1.5
                    }
                },
                text: SUB_MESSAGE,
                parent: popupContainerDiv
            });



            closeDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute",
                        left: 820, top: 660, width: 280, height: 62,
                        "background-color": "rgba(210,51,47,1)"
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 280, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center",
                        display:"", "letter-spacing":-1.5
                    }
                },
                text: "확인",
                parent: closeDiv
            });
        }


    };

    KTW.ui.view.popup.WeakSignalPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.WeakSignalPopup.prototype.constructor = KTW.ui.view.popup.WeakSignalPopup;

    KTW.ui.view.popup.WeakSignalPopup.prototype.create = function() {
        this.createView();
        KTW.ui.View.prototype.create.call(this);
    };
    KTW.ui.view.popup.WeakSignalPopup.prototype.show = function(options) {
        this.showView(options);
    };
    KTW.ui.view.popup.WeakSignalPopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.WeakSignalPopup.prototype.destroy = function() {
        this.destroy();
    };
    KTW.ui.view.popup.WeakSignalPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);

        if (!consumed) {
            consumed = KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }

        return consumed;
    };
})();
