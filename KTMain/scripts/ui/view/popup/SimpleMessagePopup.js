/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>SimpleMessagePopup</code>
 *
 * POPUP 유형 4. 중앙 하단 표시형 (단순 피드백)
 * - 위치: 전체 화면을 기준으로 가운데 하단에 표시
 * - 표시 시간: 3초
 * - 글자 수: 20자 미만(한글 기준)
 * - 자동 종료: 단순 피드백이 떠 있는 상태에서 키 입력을 하면 단순 피드백이 사라지며 상황에 따른 동작 실행
 * - Text: 팝업의 중앙에 위치. 화면 표시 범위를 초과할 수 없음 (No Scroll)
 * - 해당 팝업 적용 case: 찜하기(등록)/삭제, 미니가이드에서 선호 채널 등록/취소, 예약/예약 취소, 설정 메뉴 저장 등
 *
 * @author dhlee
 * @since 2016-08-23
 */

"use strict";

(function() {
    KTW.ui.view.popup.SimpleMessagePopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var params;

        var closeTimer = null;
        var duration = 3 * 1000;    // 기본 표시 시간. 추후 CONSTANT value 로 정의될 수 있음

        var div = null;
        var popup_container_div;

        var callbackListener;   // 눌려지거나 닫혔을 때 전달 받는 callback listener

        var simpleMessageList = null;
        // var messageType = KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT;
        // var messageData = null;
        var messageElementList = [];

        this.createView = function() {
            log.printDbg("create()");


            div = this.parent.div;

            div.attr({class: "arrange_frame"});

            popup_container_div = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: div
            });



        };

        this.showView = function() {
            log.printDbg("showView()");

            simpleMessageList = [];
            messageElementList = [];

            params = this.parent.getParams();
            if(params !== null) {
                simpleMessageList = params.data.simple_message_list;
            }

            popup_container_div.children().remove();
            _createElement();
            _showView();

            if (closeTimer) {
                clearTimeout(closeTimer);
            }
            closeTimer = setTimeout(_hideView, duration);

        };

        this.hideView = function() {
            log.printDbg("hide()");
            if (closeTimer) {
                clearTimeout(closeTimer);
            }
            closeTimer = null;
        };

        this.destroyView = function() {
            log.printDbg("destroy()");

            if (closeTimer) {
                clearTimeout(closeTimer);
            }
            closeTimer = null;
        };

        /**
         * SimpleMessagePopup 배경 화면을 생성한다.
         *
         * @param parent_div
         * @private
         */
        function _createElement() {
            log.printDbg("_createElement()");

            var rootDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "root_div",
                    css: {
                        position: "absolute",
                        left: 0, top: 914, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 166
                    }
                },
                parent: popup_container_div
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "popup_bg_img",
                    src: KTW.CONSTANT.IMAGE_PATH + "caption_bg.png",
                    css: {
                        position: "absolute", left: 0, top: 166-141, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: 141
                    }
                },
                parent: rootDiv
            });

            if(simpleMessageList !== null && simpleMessageList.length>0) {
                for(var i=0;i<simpleMessageList.length;i++) {
                    var info = simpleMessageList[i];

                    if(info.message_type !== null) {
                        if(info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON) {
                            var iconImg = util.makeElement({
                                tag: "<img />",
                                attrs: {
                                    id: "" ,
                                    src: info.data,
                                    css: {
                                        position: "absolute",
                                        top: 985-914,
                                        width: 27,
                                        height: 22,
                                        display:"none"
                                    }
                                },
                                parent: rootDiv
                            });


                            var elementInfo = {
                                root_element : iconImg,
                                type : info.message_type
                            };
                            messageElementList[messageElementList.length] = elementInfo;
                        }else if(info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON_BIG) {
                            var iconImg = util.makeElement({
                                tag: "<img />",
                                attrs: {
                                    id: "" ,
                                    src: info.data,
                                    css: {
                                        position: "absolute",
                                        top: 985-922,
                                        width: 44,
                                        height: 38,
                                        display:"none"
                                    }
                                },
                                parent: rootDiv
                            });


                            var elementInfo = {
                                root_element : iconImg,
                                type : info.message_type
                            };
                            messageElementList[messageElementList.length] = elementInfo;

                        }else if(info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT) {
                            var textMessage = util.makeElement({
                                tag: "<span />",
                                attrs: {
                                    id: "",
                                    class: "font_l",
                                    css: {
                                        position: "absolute", top: 983-914 , height: 32,
                                        color: "rgba(255,255,255, 1)", "font-size": 30 , "text-align": "left",display:"none", "letter-spacing":-1.5
                                    }
                                },
                                parent: rootDiv
                            });

                            var elementInfo = {
                                root_element : textMessage,
                                type : info.message_type
                            };
                            messageElementList[messageElementList.length] = elementInfo;
                        }else if(info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.CONTEXT_TEXT) {
                            util.makeElement({
                                tag: "<img />",
                                attrs: {
                                    id: "" ,
                                    src: KTW.CONSTANT.IMAGE_PATH + "icon/icon_option_related.png",
                                    css: {
                                        position: "absolute",
                                        left: 1666,
                                        top: 985-917,
                                        width: 33,
                                        height: 27,
                                        display:""
                                    }
                                },
                                parent: rootDiv
                            });

                            var textMessage = util.makeElement({
                                tag: "<span />",
                                attrs: {
                                    id: "",
                                    class: "font_l",
                                    css: {
                                        position: "absolute", left:1711 , top: 985-917 , width:150 , height: 28,
                                        color: "rgba(255,255,255, 0.3)", "font-size": 26 , "text-align": "left",display:"", "letter-spacing":-1.3
                                    }
                                },
                                parent: rootDiv
                            });
                            textMessage.text(info.data);
                        }

                    }
                }
            }
        }

        /**
         * SimpleMessagePopup 을 보여준다.
         *
         * @private
         */
        function _showView() {
            log.printDbg("_showView()");

            var messageWidth = 0;
            if(simpleMessageList !== null && simpleMessageList.length>0) {
                var isMessage = false;
                for (var i = 0; i < simpleMessageList.length; i++) {
                    var info = simpleMessageList[i];
                    if (info !== null) {
                        if (info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT) {
                            if(isMessage === true) {
                                messageWidth+=9;
                            }

                            var messageLength = util.getTextLength(info.data , "RixHead L" , 30, -1.5);
                            messageWidth+=messageLength;
                            isMessage = true;
                        } else if (info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON) {
                            if(isMessage === true) {
                                messageWidth+=9;
                            }

                            messageWidth+=27;
                            isMessage = true;
                        }else if (info.message_type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON_BIG) {
                            if(isMessage === true) {
                                messageWidth+=9;
                            }

                            messageWidth+=44;
                            isMessage = true;
                        }
                    }
                }

                var startLeftPos = (KTW.CONSTANT.RESOLUTION.WIDTH - messageWidth)/2;
                isMessage = false;
                for(var j=0;j<messageElementList.length;j++) {
                    var messageElement = messageElementList[j];
                    if(messageElement !== null) {
                        if(messageElement.type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.TEXT) {
                            if(isMessage === true) {
                                startLeftPos+=9;
                            }
                            var messageLength = util.getTextLength(simpleMessageList[j].data , "RixHead L" , 30 , -1.5);

                            messageElement.root_element.css({left:startLeftPos , width:messageLength});
                            messageElement.root_element.text(simpleMessageList[j].data);
                            startLeftPos+=messageLength;
                            isMessage = true;
                        }else if(messageElement.type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON) {
                            if(isMessage === true) {
                                startLeftPos+=9;
                            }
                            messageElement.root_element.css({left:startLeftPos });

                            messageElement.root_element.attr("src", simpleMessageList[j].data);
                            startLeftPos+=27;

                            isMessage = true;
                        }else if(messageElement.type === KTW.managers.service.SimpleMessageManager.SIMPLE_MESSAGE.SIMPLE_MESSAGE_TYPE.ICON_BIG) {
                            if(isMessage === true) {
                                startLeftPos+=9;
                            }
                            messageElement.root_element.css({left:startLeftPos });

                            messageElement.root_element.attr("src", simpleMessageList[j].data);
                            startLeftPos+=44;

                            isMessage = true;
                        }
                        messageElement.root_element.css("display" , "");
                    }

                }

            }
        }

        function _hideView() {
            log.printDbg("_hideView()");

            KTW.ui.LayerManager.deactivateLayer({
                id: "SimpleMessagePopup",
                remove: true
            });
        }

    };

    KTW.ui.view.popup.SimpleMessagePopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.SimpleMessagePopup.constructor = KTW.ui.view.popup.SimpleMessagePopup;

    KTW.ui.view.popup.SimpleMessagePopup.prototype.create = function() {
        this.createView();
    };
    KTW.ui.view.popup.SimpleMessagePopup.prototype.show = function() {
        this.showView();
    };
    KTW.ui.view.popup.SimpleMessagePopup.prototype.hide = function() {
        this.hideView();
    };
    KTW.ui.view.popup.SimpleMessagePopup.prototype.destroy = function() {
        this.destroyView();
    };
    KTW.ui.view.popup.SimpleMessagePopup.prototype.handleKeyEvent = function(key_code) {
        return false;
    };

})();