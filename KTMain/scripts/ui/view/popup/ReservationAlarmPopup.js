"use strict";
/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

/**
 * <code>ReservationAlarmPopup</code>
 * 시청 예약 팝업
 *
 * @author jjh1117
 * @since 2016. 12. 12.
 */

(function() {
    //var size = -1;
    //var pos_type = -1;

    KTW.ui.view.popup.ReservationAlarmPopup = function(options) {
        KTW.ui.View.call(this);

        var log = KTW.utils.Log;
        var util = KTW.utils.util;

        var params = null;
        var div = null;

        //var id = null;
        //var div = null;

        var popupContainerDiv = null;
        var reservationAlaramDiv = null;

        var reservationAlaramPIPONDiv = null;
        var reservationAlaramPIPOFFDiv = null;

        var channelBoxPIPONDiv = null;
        var channelBoxPIPOFFDiv = null;
        

        var textChNumberPIPON = null;
        var textChNumberPIPOFF = null;

        var textChNamePIPON = null;
        var textChNamePIPOFF = null;

        var textBoxProgramPIPON = null;
        var textBoxProgramPIPOFF = null;
        
        var textChProgramPIPON = null;
        var textChProgramPIPOFF = null;

        var commentDivPIPON = null;
        var commentDivPIPOFF = null;

        var btnGroupDivPIPON = null;
        var btnGroupDivPIPOFF = null;
        var btnListPIPON = [];
        var btnListPIPOFF = [];

        var popupId = null;
        var reservationChannel = null;
        var reservationProgram = null;
        var callbackFunc = null;

        var pipChannelControl = null;

        var isShowPIP = true;

        var focusbtnIndex = 0;

        var TIMER_DELAY = 60 * 1000; // 1분

        var isDCSMode = false;
        /**
         * 팝업 표시시간 관련 타이머
         */
        this.timer = null;

        this.create = function() {
            log.printDbg("create()");
            params = this.parent.getParams();
            div = this.parent.div;

            if (params !== null) {
                popupId = params.data.popupid;
                reservationChannel = params.data.ch;
                reservationProgram = params.data.prog;
                callbackFunc = params.data.callback;
            }

            /**
             * DCS STB 이거나 해당 예약 채널이
             * kt 채널인데 hasSDChannel 값이 false 이거나(sky 채널은 hasSDChannel 값이 항상 false이므로 의미가 없음)
             * UHD 채널 인 경우 PIP 화면 미노출
             */
            pipChannelControl = KTW.oipf.AdapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.PIP);
            if ((reservationChannel.hasSDChannel === false && (reservationChannel.idType ===KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_SDS || reservationChannel.idType === KTW.nav.Def.CHANNEL.ID_TYPE.IPTV_URI)) ||
                // OTS UHD 채널이거나
                (KTW.CONSTANT.IS_OTS === true && reservationChannel.isUHD === true) ||
                // 다른 곳에서 이미 pip를 사용하고 있으면 일반예약알림팝업을 호출
                (pipChannelControl !== null && pipChannelControl.getPipMode() !== KTW.nav.Def.PIP_MODE.NONE)) {
                log.printDbg('createView() hasSDChannel: ' + reservationChannel.hasSDChannel);
                log.printDbg('createView() isUHD: ' + reservationChannel.isUHD);
                if(pipChannelControl !== null) {
                    log.printDbg('createView() getPipMode: ' + pipChannelControl.getPipMode());
                }

                isShowPIP = false;
            }

            if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                isShowPIP = false;
            }

            _createView(div);
        };

        this.showView = function() {
            log.printDbg("showView()");


            if (KTW.CONSTANT.IS_OTS) {
                var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                log.printDbg("showView(), KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE) : " + temp);
                if (temp != null) {
                    if(temp === "Y") {
                        isDCSMode = true;
                    }else {
                        isDCSMode = false;
                    }
                }

                log.printDbg("showView(), isDCSMode: " + isDCSMode);
                KTW.managers.StorageManager.ps.addStorageDataChangeListener(_updateOTSDCSMode);
            }

            if(isShowPIP === true && isDCSMode === false) {
                _startPip();
                reservationAlaramPIPONDiv.css("display" , "");
            }else {
                reservationAlaramPIPOFFDiv.css("display" , "");
            }
            _showChannelInfo();

            var btnobj = btnListPIPON[focusbtnIndex];
            btnobj.btn_default_img.css("display" , "none");
            btnobj.btn_focus_div.css("display" , "");
            if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                btnobj.btn_text.css("color", "rgba(81,51,0,1)");

            }else {
                btnobj.btn_text.css("color", "rgba(255,255,255,1)");
            }

            btnobj.btn_text.removeClass("font_l");
            btnobj.btn_text.addClass("font_m");

            btnobj = btnListPIPOFF[focusbtnIndex];
            btnobj.btn_default_img.css("display" , "none");
            btnobj.btn_focus_div.css("display" , "");
            if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                btnobj.btn_text.css("color", "rgba(81,51,0,1)");

            }else {
                btnobj.btn_text.css("color", "rgba(255,255,255,1)");
            }

            btnobj.btn_text.removeClass("font_l");
            btnobj.btn_text.addClass("font_m");
            

            var popupCount = KTW.utils.util.getLayerCount("#layer_area", "div", KTW.ui.view.popup.ReservationAlarmPopup.id);
            var delayTime = TIMER_DELAY;

            if (this.timer) {
                clearTimeout(this.timer);
                this.timer = null;
            }

            this.timer = setTimeout(function(){
                var onlyTarget = true;
                var layers = KTW.ui.LayerManager.getLayers(KTW.ui.view.popup.ReservationAlarmPopup.id);
                if (layers.length === 1) {
                    onlyTarget = false;
                }

                KTW.ui.LayerManager.deactivateLayer({
                    id: popupId,
                    onlyTarget: onlyTarget
                });
                //callbackFunc(focusbtnIndex ,reservationChannel);
                // 2017.07.18 dhlee timer에 의해 자동으로 닫힐 때는 무조건 바로보기 index를 넘겨야 함 (KT 검증이슈)
                callbackFunc(0 ,reservationChannel);
            }, delayTime);

        };

        this.hideView = function() {
            log.printDbg('hide() ');

            if (isShowPIP === true) {
                _endPip();
            }

            if (this.timer) {
                clearTimeout(this.timer);
                this.timer = null;
            }
            //_resetTitleAnimation();

            if (KTW.CONSTANT.IS_OTS) {
                KTW.managers.StorageManager.ps.removeStorageDataChangeListener(_updateOTSDCSMode);
            }

        };

        this.destroy = function() {
            log.printDbg('destroy() ');
            log.printDbg("destroyView()");

            if (isShowPIP === true) {
                KTW.managers.service.PipManager.deactivatePipLayer();
            }

            if (this.timer) {
                clearTimeout(this.timer);
                this.timer = null;
            }
        };

        this.controlKey = function(key_code) {
            log.printDbg("controlKey()");
            var btnobj = null;
            var consumed = false;
            switch (key_code) {
                case KTW.KEY_CODE.LEFT :

                    btnobj = btnListPIPON[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "");
                    btnobj.btn_focus_div.css("display" , "none");
                    btnobj.btn_text.css("color", "rgba(255,255,255,0.7)");
                    btnobj.btn_text.removeClass("font_m");
                    btnobj.btn_text.addClass("font_l");

                    btnobj = btnListPIPOFF[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "");
                    btnobj.btn_focus_div.css("display" , "none");
                    btnobj.btn_text.css("color", "rgba(255,255,255,0.7)");
                    btnobj.btn_text.removeClass("font_m");
                    btnobj.btn_text.addClass("font_l");

                    focusbtnIndex--;
                    if(focusbtnIndex<0) {
                        focusbtnIndex = 1;
                    }

                    btnobj = btnListPIPON[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "none");
                    btnobj.btn_focus_div.css("display" , "");

                    if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                        btnobj.btn_text.css("color", "rgba(81,51,0,1)");

                    }else {
                        btnobj.btn_text.css("color", "rgba(255,255,255,1)");
                    }

                    btnobj.btn_text.removeClass("font_l");
                    btnobj.btn_text.addClass("font_m");

                    btnobj = btnListPIPOFF[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "none");
                    btnobj.btn_focus_div.css("display" , "");

                    if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                        btnobj.btn_text.css("color", "rgba(81,51,0,1)");

                    }else {
                        btnobj.btn_text.css("color", "rgba(255,255,255,1)");
                    }

                    btnobj.btn_text.removeClass("font_l");
                    btnobj.btn_text.addClass("font_m");
                    

                    consumed = true;
                    break;
                case KTW.KEY_CODE.RIGHT :
                    btnobj = btnListPIPON[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "");
                    btnobj.btn_focus_div.css("display" , "none");
                    btnobj.btn_text.css("color", "rgba(255,255,255,0.7)");

                    btnobj.btn_text.removeClass("font_m");
                    btnobj.btn_text.addClass("font_l");

                    btnobj = btnListPIPOFF[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "");
                    btnobj.btn_focus_div.css("display" , "none");
                    btnobj.btn_text.css("color", "rgba(255,255,255,0.7)");

                    btnobj.btn_text.removeClass("font_m");
                    btnobj.btn_text.addClass("font_l");

                    focusbtnIndex++;
                    if(focusbtnIndex>1) {
                        focusbtnIndex = 0;
                    }
                    btnobj = btnListPIPON[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "none");
                    btnobj.btn_focus_div.css("display" , "");
                    
                    if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                        btnobj.btn_text.css("color", "rgba(81,51,0,1)");

                    }else {
                        btnobj.btn_text.css("color", "rgba(255,255,255,1)");
                    }


                    btnobj.btn_text.removeClass("font_l");
                    btnobj.btn_text.addClass("font_m");

                    btnobj = btnListPIPOFF[focusbtnIndex];
                    btnobj.btn_default_img.css("display" , "none");
                    btnobj.btn_focus_div.css("display" , "");

                    if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                        btnobj.btn_text.css("color", "rgba(81,51,0,1)");

                    }else {
                        btnobj.btn_text.css("color", "rgba(255,255,255,1)");
                    }


                    btnobj.btn_text.removeClass("font_l");
                    btnobj.btn_text.addClass("font_m");

                    consumed = true;
                    break;
                case KTW.KEY_CODE.OK :
                    var onlyTarget = true;
                    var layers = KTW.ui.LayerManager.getLayers(KTW.ui.view.popup.ReservationAlarmPopup.id);
                    if (layers.length === 1) {
                        onlyTarget = false;
                    }

                    KTW.ui.LayerManager.deactivateLayer({
                        id: popupId,
                        onlyTarget: onlyTarget
                    });

                    if (this.timer) {
                        clearTimeout(this.timer);
                        this.timer = null;
                    }

                    callbackFunc(focusbtnIndex ,reservationChannel);
                    consumed = true;
                    break;
                case KTW.KEY_CODE.BACK :
                case KTW.KEY_CODE.EXIT :
                    if (this.timer) {
                        clearTimeout(this.timer);
                        this.timer = null;
                    }

                    var onlyTarget = true;
                    var layers = KTW.ui.LayerManager.getLayers(KTW.ui.view.popup.ReservationAlarmPopup.id);
                    if (layers.length === 1) {
                        onlyTarget = false;
                    }

                    KTW.ui.LayerManager.deactivateLayer({
                        id: popupId,
                        onlyTarget: onlyTarget
                    });
                    consumed = true;
                    break;
                // 2017.06.16 dhlee
                // block num & color key
                case KTW.KEY_CODE.NUM_0 :
                case KTW.KEY_CODE.NUM_1 :
                case KTW.KEY_CODE.NUM_2 :
                case KTW.KEY_CODE.NUM_3 :
                case KTW.KEY_CODE.NUM_4 :
                case KTW.KEY_CODE.NUM_5 :
                case KTW.KEY_CODE.NUM_6 :
                case KTW.KEY_CODE.NUM_7 :
                case KTW.KEY_CODE.NUM_8 :
                case KTW.KEY_CODE.NUM_9 :
                case KTW.KEY_CODE.RED :
                case KTW.KEY_CODE.GREEN :
                case KTW.KEY_CODE.BLUE :
                    consumed = true;
                    break;
                default :
                    break;
            }
            return consumed;
        };

        /**
         *
         * @param parent_div
         * @private
         */
        function _createView() {
            log.printDbg("_createView()");
            div.attr({class: "arrange_frame"});
            _createBaseView(div);
        }

        function _createBaseView(parent_div) {
            popupContainerDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "popup_container",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                    }
                },
                parent: parent_div
            });


            reservationAlaramPIPONDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 1376, top: 399, width:492, height: 642 , display : "none"
                    }
                },
                parent: popupContainerDiv
            });

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/popup/pop_bg_option_b1.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 492,
                        height: 642
                    }
                },
                parent: reservationAlaramPIPONDiv
            });


            reservationAlaramPIPOFFDiv = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 1376, top: 619, width:492, height: 422 , display : "none"
                    }
                },
                parent: popupContainerDiv
            });

            var srcImg = "";

            if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                srcImg = "pop_bg_option_kids.png";
            }else {
                srcImg = "pop_bg_option_b2.png";
            }

            util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "" ,
                    src: "images/popup/" + srcImg,
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 492,
                        height: 422
                    }
                },
                parent: reservationAlaramPIPOFFDiv
            });
            
            
            if(KTW.managers.service.KidsModeManager.isKidsMode() === true) {
                channelBoxPIPOFFDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 49, top : 98 , width:387, height: 118,
                            border: "solid 2px rgba(190, 190, 190, 0.5)" , opacity:1
                        }
                    },
                    parent: reservationAlaramPIPOFFDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 0, width:387, height: 118,
                            "background-color": "rgba(177, 176, 176, 0.25)"
                        }
                    },
                    parent: channelBoxPIPOFFDiv
                });

            }else {
                channelBoxPIPONDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 49, top : 318 , width:387, height: 118,
                            border: "solid 2px rgba(190, 190, 190, 0.5)", opacity:1
                        }
                    },
                    parent: reservationAlaramPIPONDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 0, width:387, height: 118,
                            "background-color": "rgba(177, 176, 176, 0.2)"
                        }
                    },
                    parent: channelBoxPIPONDiv
                });

                channelBoxPIPOFFDiv = util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 49, top : 98 , width:387, height: 118,
                            border: "solid 2px rgba(190, 190, 190, 0.5)", opacity:1
                        }
                    },
                    parent: reservationAlaramPIPOFFDiv
                });

                util.makeElement({
                    tag: "<div />",
                    attrs: {
                        css: {
                            position: "absolute",
                            left: 0, width:387, height: 118,
                            "background-color": "rgba(177, 176, 176, 0.2)"
                        }
                    },
                    parent: channelBoxPIPOFFDiv
                });
                
            }



            textChNumberPIPON = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 27 , top: 25 , width: 58, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                    }
                },
                parent: channelBoxPIPONDiv
            });

            textChNumberPIPOFF = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_b",
                    css: {
                        position: "absolute", left: 27 , top: 25 , width: 58, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                    }
                },
                parent: channelBoxPIPOFFDiv
            });



            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "" ,
                    css: {
                        position: "absolute",
                        left: 90, top: 29, width:2, height: 24,
                        "background-color": "rgba(255,255,255,0.2)"
                    }
                },
                parent: channelBoxPIPONDiv
            });

            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "" ,
                    css: {
                        position: "absolute",
                        left: 90, top: 29, width:2, height: 24,
                        "background-color": "rgba(255,255,255,0.2)"
                    }
                },
                parent: channelBoxPIPOFFDiv
            });


            textChNamePIPON = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m cut",
                    css: {
                        position: "absolute", left: 103 , top: 26 , width: 261, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                    }
                },
                parent: channelBoxPIPONDiv
            });

            textChNamePIPOFF = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m cut",
                    css: {
                        position: "absolute", left: 103 , top: 26 , width: 261, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "left", "letter-spacing":-1.5
                    }
                },
                parent: channelBoxPIPOFFDiv
            });
            


            textBoxProgramPIPON = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute", left: 25, top: 67, width: 341, height: 34
                    }
                },
                parent: channelBoxPIPONDiv
            });

            textBoxProgramPIPOFF = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "",
                    css: {
                        position: "absolute", left: 25, top: 67, width: 341, height: 34
                    }
                },
                parent: channelBoxPIPOFFDiv
            });


            textChProgramPIPON = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 27 , top: 67 , height: 34,"white-space":"nowrap",
                        color: "rgba(255, 255, 255, 1)", "font-size": 32 , "text-align": "left", "letter-spacing":-1.6
                    }
                },
                parent: channelBoxPIPONDiv
            });

            textChProgramPIPOFF = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_m",
                    css: {
                        position: "absolute", left: 27 , top: 67 , height: 34,"white-space":"nowrap",
                        color: "rgba(255, 255, 255, 1)", "font-size": 32 , "text-align": "left", "letter-spacing":-1.6
                    }
                },
                parent: channelBoxPIPOFFDiv
            });
            


            commentDivPIPON = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 0, top : 469, width:492, height: 35
                    }
                },
                parent: reservationAlaramPIPONDiv
            });

            commentDivPIPOFF = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 0, top : 249, width:492, height: 35
                    }
                },
                parent: reservationAlaramPIPOFFDiv
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 0 , width: 492, height: 28,
                        color: "rgba(255, 255, 255, 1)", "font-size": 26 , "text-align": "center" , "letter-spacing":-1.3
                    }
                },
                text: "1분 후 예약한 프로그램으로 전환됩니다",
                parent: commentDivPIPON
            });

            util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 0 , width: 492, height: 28,
                        color: "rgba(255, 255, 255, 1)", "font-size": 26 , "text-align": "center" , "letter-spacing":-1.3
                    }
                },
                text: "1분 후 예약한 프로그램으로 전환됩니다",
                parent: commentDivPIPOFF
            });


            btnGroupDivPIPON = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 49, top : 524, width:390, height: 67
                    }
                },
                parent: reservationAlaramPIPONDiv
            });

            btnGroupDivPIPOFF = util.makeElement({
                tag: "<div />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: 49, top : 304, width:390, height: 67
                    }
                },
                parent: reservationAlaramPIPOFFDiv
            });


            var btnDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:192, height: 62
                    }
                },
                parent: btnGroupDivPIPON
            });


            var defaultImg1 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "show_channel_btn_default" ,
                    src: "images/popup/pop_btn_w190.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 192,
                        height: 62
                    }
                },
                parent: btnDiv1
            });

            var btnFocusDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:192, height: 62,
                        "background-color": KTW.managers.service.KidsModeManager.isKidsMode() ? "rgba(255,179,46,1)" :  "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv1
            });

            var btnText1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "top_menu_btn_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 192, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center" , "letter-spacing":-1.5
                    }
                },
                text: "바로보기",
                parent: btnDiv1
            });


            var btn = {
                btn_root_div : btnDiv1,
                btn_default_img : defaultImg1,
                btn_focus_div : btnFocusDiv1,
                btn_text : btnText1
            } ;

            btnListPIPON[btnListPIPON.length] = btn;


            var btnDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn",
                    css: {
                        position: "absolute",
                        left: 198, top: 0, width:192, height: 62
                    }
                },
                parent: btnGroupDivPIPON
            });


            var defaultImg2 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "show_channel_btn_default" ,
                    src: "images/popup/pop_btn_w190.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 192,
                        height: 62
                    }
                },
                parent: btnDiv2
            });

            var btnFocusDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:192, height: 62,
                        "background-color": KTW.managers.service.KidsModeManager.isKidsMode() ? "rgba(255,179,46,1)" :  "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv2
            });

            var btnText2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "top_menu_btn_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 192, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center" , "letter-spacing":-1.5
                    }
                },
                text: "닫기",
                parent: btnDiv2
            });

            btn = {
                btn_root_div : btnDiv2,
                btn_default_img : defaultImg2,
                btn_focus_div : btnFocusDiv2,
                btn_text : btnText2
            } ;

            btnListPIPON[btnListPIPON.length] = btn;


            var btnDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn",
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:192, height: 62
                    }
                },
                parent: btnGroupDivPIPOFF
            });


            var defaultImg1 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "show_channel_btn_default" ,
                    src: "images/popup/pop_btn_w190.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 192,
                        height: 62
                    }
                },
                parent: btnDiv1
            });

            var btnFocusDiv1 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:192, height: 62,
                        "background-color": KTW.managers.service.KidsModeManager.isKidsMode() ? "rgba(255,179,46,1)" :  "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv1
            });

            var btnText1 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "top_menu_btn_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 192, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center" , "letter-spacing":-1.5
                    }
                },
                text: "바로보기",
                parent: btnDiv1
            });


            var btn = {
                btn_root_div : btnDiv1,
                btn_default_img : defaultImg1,
                btn_focus_div : btnFocusDiv1,
                btn_text : btnText1
            } ;

            btnListPIPOFF[btnListPIPOFF.length] = btn;


            var btnDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn",
                    css: {
                        position: "absolute",
                        left: 198, top: 0, width:192, height: 62
                    }
                },
                parent: btnGroupDivPIPOFF
            });


            var defaultImg2 = util.makeElement({
                tag: "<img />",
                attrs: {
                    id: "show_channel_btn_default" ,
                    src: "images/popup/pop_btn_w190.png",
                    css: {
                        position: "absolute",
                        left: 0 ,
                        top: 0,
                        width: 192,
                        height: 62
                    }
                },
                parent: btnDiv2
            });

            var btnFocusDiv2 = util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "show_channel_btn_focus" ,
                    css: {
                        position: "absolute",
                        left: 0, top: 0, width:192, height: 62,
                        "background-color": KTW.managers.service.KidsModeManager.isKidsMode() ? "rgba(255,179,46,1)" :  "rgba(210,51,47,1)",
                        display: "none"
                    }
                },
                parent: btnDiv2
            });

            var btnText2 = util.makeElement({
                tag: "<span />",
                attrs: {
                    id: "top_menu_btn_text",
                    class: "font_l",
                    css: {
                        position: "absolute", left: 0 , top: 18 , width: 192, height: 32,
                        color: "rgba(255, 255, 255, 1)", "font-size": 30 , "text-align": "center" , "letter-spacing":-1.5
                    }
                },
                text: "닫기",
                parent: btnDiv2
            });

            btn = {
                btn_root_div : btnDiv2,
                btn_default_img : defaultImg2,
                btn_focus_div : btnFocusDiv2,
                btn_text : btnText2
            } ;

            btnListPIPOFF[btnListPIPOFF.length] = btn;
            
            
        }

        function _updateOTSDCSMode(key) {
            log.printDbg('_updateOTSDCSMode() key : ' + key);
            if (key === KTW.managers.StorageManager.KEY.OTS_DCS_MODE) {
                log.printDbg('_updateOTSDCSMode() key : ' + key);
                var temp = KTW.managers.StorageManager.ps.load(KTW.managers.StorageManager.KEY.OTS_DCS_MODE);
                if (temp != null) {
                    if(temp === "Y") {
                        isDCSMode = true;
                    }else {
                        isDCSMode = false;
                    }

                    if(isDCSMode === true) {
                        if(isShowPIP === true) {
                            _endPip();
                            reservationAlaramPIPONDiv.css("display" , "none");
                            reservationAlaramPIPOFFDiv.css("display" , "");
                        }
                    }else {
                        if(isShowPIP === true) {
                            _startPip();
                            reservationAlaramPIPONDiv.css("display" , "");
                            reservationAlaramPIPOFFDiv.css("display" , "none");
                        }
                    }
                }
            }
        }

        function _startPip () {
            log.printDbg("startPip()");
            KTW.managers.service.PipManager.activatePipLayer({pipType: KTW.managers.service.PipManager.PIP_TYPE.RESERVATION});
            KTW.managers.service.PipManager.setPipSize({left:1424,top:497,width:391,height:220});
            _pipChannelPlay();
        }

        function _endPip () {
            log.printDbg("endPip()");
            setTimeout(function(){
                KTW.managers.service.PipManager.deactivatePipLayer();
            } , 100);
        }

        function _pipChannelPlay() {
            log.printDbg("_pipChannelPlay()");
            setTimeout(function(){
                KTW.managers.service.PipManager.changeChannel(reservationChannel);
            }, 10);
        }

        function _showChannelInfo() {
            log.printDbg("_showCurrentChannelInfo() reservationProgram: " + ((reservationProgram !== undefined && reservationProgram !== null) ? JSON.stringify(reservationProgram) : "is Null"));

            if(reservationChannel !== undefined && reservationChannel !== null) {
                textChNumberPIPON.text(util.numToStr(reservationChannel.majorChannel, 3));
                textChNumberPIPOFF.text(util.numToStr(reservationChannel.majorChannel, 3));

                textChNamePIPON.text(reservationChannel.name);
                textChNamePIPOFF.text(reservationChannel.name);
            }

            if(reservationProgram !== undefined && reservationProgram !== null) {
                textChProgramPIPON.text(reservationProgram.name);
                textChProgramPIPON.addClass("cut");
                textChProgramPIPON.css({width:341});

                textChProgramPIPOFF.text(reservationProgram.name);
                textChProgramPIPOFF.addClass("cut");
                textChProgramPIPOFF.css({width:341});

                //_setTitleAnimation();
            }
        }

        // function _setTitleAnimation () {
        //     log.printDbg("_setTitleAnimation()");
        //     textChProgram.removeClass("cut");
        //     util.clearAnimation(textBoxProgram.find("span"));
        //     util.startTextAnimation({
        //         targetBox: textBoxProgram,
        //         //speed: 125,
        //         delayTime: 1
        //     });
        // }
        //
        // function _resetTitleAnimation () {
        //     log.printDbg("_resetTitleAnimation()");
        //     util.clearAnimation(textBoxProgram.find("span"));
        //     textChProgram.addClass("cut");
        // }

    };

    KTW.ui.view.popup.ReservationAlarmPopup.id = "ReservationAlarmPopup";
    KTW.ui.view.popup.ReservationAlarmPopup.prototype = new KTW.ui.View();
    KTW.ui.view.popup.ReservationAlarmPopup.prototype.constructor = KTW.ui.view.popup.ReservationAlarmPopup;

//Override create function
    KTW.ui.view.popup.ReservationAlarmPopup.prototype.create = function() {
        this.create();
    };

    KTW.ui.view.popup.ReservationAlarmPopup.prototype.destroy = function() {
        this.destroy();
    };

    KTW.ui.view.popup.ReservationAlarmPopup.prototype.show = function() {
        this.showView();
    };

    KTW.ui.view.popup.ReservationAlarmPopup.prototype.hide = function() {
        this.hideView();
    };

    KTW.ui.view.popup.ReservationAlarmPopup.prototype.handleKeyEvent = function(key_code) {
        var consumed = this.controlKey(key_code);
        if (consumed === false) {
            return KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        }
        return consumed;
    };


})();
