/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>BackIframeView.js</code>
 *
 * 채널 상에서 연령제한, 미가입, 제한 채널 등의 상황이 발생했을 때 background에 보여지는 image를 제공한다.
 * OTS 무비초이스 채널의 경우 무비초이스 채널의 기본 정보를 Background image 위에 보여준다.
 * Audio 채널의 경우 곡 정보 등을 보여준다.
 * VOD Loading 배경 이미지를 보여준다.
 *
 * @author dhlee
 * @since 2016-07-26
 */

"use strict";

KTW.ui.view.AudioIframeView = function AudioIframeView(parent) {
    KTW.ui.View.call(this);
    var IMAGE_PATH = KTW.CONSTANT.IMAGE_PATH;
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var last_back_iframe_type = KTW.managers.service.IframeManager.DEF.TYPE.NONE;
    var last_iframe_mode = KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE;
    var parent = parent;

    /**
     * FullMode
     */
    var fullModeDiv = null;
    var imgfullModeBg = null;


    /**
     * 오디오 채널 현재 재생 바
     */
    var audioBaseFrequencyDiv = null;
    var audioCurrentFrequencyDiv = null;
    var audioProgressBackgroundImg = null;



    /**
     * 오디오채널 채널번호/채널명
     */
    var textAudioChannelNumber = null;
    var textAudioChannelName = null;

    /**
     * 오디오 채널 프로그램 정보
     */
    var textAudioChannelProgram1 = null;
    var imgAgeIcon = null;
    var textAudioChannelProgram2 = null;
    var textAudioChannelProgram1Duration = null;

    var audioChannelListViewIconDiv = null;

    /**
     * AudioIframe 관련 주기적인 프로그램 업데이트를 위한 Timer
     */
    var audioChannelTimer = null;
    var audioFrequencyUpdateTimer = null;
    var curProgram = null;

    var backgroundImage = null;

    this.create = function() {
        log.printDbg("create()");

        var parent_div = parent.div;
        parent_div.attr({class: "arrange_frame"});

        fullModeDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 0, top: 0,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH, height: KTW.CONSTANT.RESOLUTION.HEIGHT
                }
            },
            parent: parent_div
        });

        // 전체 크기(1920 * 1080)의 iframe image element
        imgfullModeBg = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "back_iframe_full_bg",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: KTW.CONSTANT.RESOLUTION.WIDTH ,
                    height: KTW.CONSTANT.RESOLUTION.HEIGHT
                }
            },
            parent: fullModeDiv
        });


        textAudioChannelNumber = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "audio_channel_number",
                class: "font_m",
                css: {
                    position: "absolute", left: 411, top: 223, width: 100, height: 47,
                    color: "rgba(53, 41, 26, 1)", "font-size": 45 , "text-align" : "left", "letter-spacing":-1.85
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textAudioChannelName = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "audio_channel_name",
                class: "font_m cut",
                css: {
                    position: "absolute", left: 503, top: 225, width: 542, height: 39,
                    color: "rgba(53, 41, 26, 1)", "font-size": 37 , "text-align" : "left", "letter-spacing":-1.85
                }
            },
            text: "",
            parent: fullModeDiv
        });


        audioBaseFrequencyDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 408, top: 270, width: 888, height: 69,
                    "background-color": "rgba(226,216,199,1)"
                }
            },
            parent: fullModeDiv
        });

        audioCurrentFrequencyDiv = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "",
                css: {
                    position: "absolute", left: 408, top: 270, width: 0, height: 69,
                    "background-color": "rgba(136,95,62,1)"
                }
            },
            parent: fullModeDiv
        });


        audioProgressBackgroundImg = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "bg_audio_progress",
                src: IMAGE_PATH + "iframe/bg_audio_progress.png",
                css: {
                    position: "absolute",left: 406, top: 270, width: 892, height: 69
                }
            },
            parent: fullModeDiv
        });


        textAudioChannelProgram1 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "audio_channel_program_1",
                class: "font_m cut",
                css: {
                    position: "absolute", left: 411, top: 343, height: 38,"white-space":"nowrap",
                    color: "rgba(53, 41, 26, 1)", "font-size": 36 , "text-align" : "left" , "letter-spacing":-1.8
                }
            },
            text: "",
            parent: fullModeDiv
        });

        imgAgeIcon  = util.makeElement({
            tag: "<img />",
            attrs: {
                id: "",
                css: {
                    position: "absolute",left: 411, top: 343, width: 34, height: 34 , display : "none"
                }
            },
            parent: fullModeDiv
        });

        textAudioChannelProgram2 = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "audio_channel_program_2",
                class: "font_m cut",
                css: {
                    position: "absolute", left: 1298, top: 343, width: 600, height: 38,
                    color: "rgba(53, 41, 26, 1)", "font-size": 36 , "text-align" : "left", "letter-spacing":-1.8
                }
            },
            text: "",
            parent: fullModeDiv
        });

        textAudioChannelProgram1Duration  = util.makeElement({
            tag: "<span />",
            attrs: {
                id: "audio_channel_program1_duration",
                class: "font_m",
                css: {
                    position: "absolute", left: 412, top: 389, width: 150, height: 26,
                    color: "rgba(53, 41, 26, 1)", "font-size": 24 , "text-align" : "left", "letter-spacing":-1.2
                }
            },
            text: "",
            parent: fullModeDiv
        });


        audioChannelListViewIconDiv =  util.makeElement({
            tag: "<div />",
            attrs: {
                css: {
                    position: "absolute", left: 837, top: 978, overflow: "visible", display: "none"
                }
            },
            parent: fullModeDiv
        });

        util.makeElement({
            tag: "<img />",
            attrs: {
                id: "arw_down_img",
                src: IMAGE_PATH + "miniepg/arw_related_dw.png",
                css: {
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: 42,
                    height: 42
                }
            },
            parent: audioChannelListViewIconDiv
        });


        util.makeElement({
            tag: "<span />",
            attrs: {
                id: "text_audio_channel_list_view_title",
                class: "font_m" ,
                css: {
                    position: "absolute", left: 56, top: 6, width: 200 , height: 29,
                    color: "rgba(255, 255, 255, 0.7)", "font-size": 27,"text-align" : "left", "letter-spacing":-1.935
                }
            },
            text: "오디오 채널 편성표",
            parent: audioChannelListViewIconDiv
        });



    };

    this.show = function() {
        log.printDbg("show()");
        var param = parent.getParams().data;

        var currentType = param.iframe_type;
        var back_img = param.iframe_img;
        var isUpdate = param.is_update;
        log.printDbg("show() back_img : " + back_img);
        log.printDbg("show() isUpdate : " + isUpdate);
        if(back_img !== undefined && back_img !== null) {
            backgroundImage = back_img;
        }

        if(isUpdate !== undefined && isUpdate !== null && isUpdate === true) {
            _updateAudioChannelProgramData();
            if(currentType === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
                audioChannelListViewIconDiv.css("display" , "");
            }else {
                audioChannelListViewIconDiv.css("display" , "none");
            }
        }else {
            _showFullMode();
            if(currentType === KTW.managers.service.AudioIframeManager.DEF.TYPE.AUDIO_FULL_EPG) {
                audioChannelListViewIconDiv.css("display" , "");
            }else {
                audioChannelListViewIconDiv.css("display" , "none");
            }
        }
    };


    this.hide = function() {
        log.printDbg("hide()");
        _hideFullMode();
    };

    this.updateFocusAudioChannel = function() {
        
        if(audioChannelTimer !== null) {
            clearInterval(audioChannelTimer);
            audioChannelTimer = null;
        }

        if(audioFrequencyUpdateTimer !== null) {
            clearInterval(audioFrequencyUpdateTimer);
            audioFrequencyUpdateTimer = null;
        }
        
        if(last_iframe_mode === KTW.managers.service.IframeManager.DEF.MODE.FULL_MODE) {
            _updateAudioChannelProgramData();
        }
    };

    this.showAudioEpgViewIcon = function() {
        audioChannelListViewIconDiv.css("display" , "");
    };

    this.hideAudioEpgViewIcon = function() {
        audioChannelListViewIconDiv.css("display" , "none");
    };


    function _hideFullMode() {
        log.printDbg("_hideFullMode()");
        if(audioChannelTimer !== null) {
            clearInterval(audioChannelTimer);
            audioChannelTimer = null;
        }

        if(audioFrequencyUpdateTimer !== null) {
            clearInterval(audioFrequencyUpdateTimer);
            audioFrequencyUpdateTimer = null;
        }

        fullModeDiv.css("display", "none");
    }

    function _showFullMode() {
        log.printDbg("_showFullMode()");
        imgfullModeBg.attr("src", backgroundImage);
        _updateAudioChannelProgramData();

        fullModeDiv.css("display", "");
    }


    function _updateAudioChannelProgramData() {
        log.printDbg("_updateAudioChannelProgramData()");

        audioBaseFrequencyDiv.css("display", "");
        audioCurrentFrequencyDiv.css("display", "");
        audioProgressBackgroundImg.css("display" , "");

        textAudioChannelNumber.text("");
        textAudioChannelNumber.css("display", "");
        textAudioChannelName.text("");
        textAudioChannelName.css("display", "");


        textAudioChannelProgram1.text("");
        textAudioChannelProgram1.css({width:800});
        textAudioChannelProgram1.css("display", "");
        textAudioChannelProgram2.text("");
        textAudioChannelProgram2.css("display", "");

        textAudioChannelProgram1Duration.text("");
        textAudioChannelProgram1Duration.css("display", "");

        imgAgeIcon.css({left:277});
        imgAgeIcon.css("display" , "none");
        
        if(audioChannelTimer !== null) {
            clearInterval(audioChannelTimer);
            audioChannelTimer = null;
        }
        _callbackFuncProgramUpdateTimer();

        if(audioFrequencyUpdateTimer !== null) {
            clearInterval(audioFrequencyUpdateTimer);
            audioFrequencyUpdateTimer = null;
        }
        audioFrequencyUpdateTimer = setInterval(_callbackFuncFrequencyUpdateTimer, 50);
    }

    function _callbackFuncFrequencyUpdateTimer() {
        if(curProgram !== null) {
            var now = new Date().getTime();
            if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                now = Math.floor(now/1000);
            }

            var ss = curProgram.startTime;

            var playBarLength = Math.floor((now - ss) * parseInt(888) / curProgram.duration);
            audioCurrentFrequencyDiv.css({width:playBarLength});

            if((now - ss)>=curProgram.duration) {
                log.printDbg("_callbackFuncFrequencyUpdateTimer() update Program");
                if(audioChannelTimer !== null) {
                    clearInterval(audioChannelTimer);
                    audioChannelTimer = null;
                }
                _callbackFuncProgramUpdateTimer();
            }
        }

        // var frequencyWidth = Math.floor(Math.random() * 533) + 1;
        // audioCurrentFrequencyDiv.animate({
        //     width: frequencyWidth + 'px'});

    }

    function _callbackFuncProgramUpdateTimer() {
        var cur_channel = KTW.managers.service.AudioIframeManager.getAudioFocusChannel();//KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
        
        if(cur_channel !== null) {
            /**
             * 채널번호/채널명 노출
             */
            var chNum = util.numToStr(cur_channel.majorChannel, 3);
            textAudioChannelNumber.text(chNum);
            textAudioChannelName.text(cur_channel.name);

            var programManager = KTW.oipf.adapters.ProgramSearchAdapter;
            var prog_list = programManager.getPrograms(null, null, null, cur_channel);

            var prog_index = 0;

            if(prog_list !== undefined && prog_list !== null && prog_list.length>0) {
                //prog_list = KTW.utils.epgUtil.revisePrograms(cur_channel, prog_list, true);
                prog_index = 0;

                var now = new Date().getTime();
                if (KTW.utils.epgUtil.DATA.IS_TIME_UNIT_SEC === true) {
                    now = Math.floor(now/1000);
                }

                var prog = prog_list[prog_index];
                while(prog.startTime + prog.duration < now && prog_index < prog_list.length - 1) {
                    prog_index++;
                    prog = prog_list[prog_index];
                    if (prog == null) {
                        prog_index = -1;
                        break;
                    }
                }

                var mainChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();

                if(prog_index>=0) {
                    var prog1 = prog_list[prog_index];
                    var prog2 = prog_list[prog_index+1];
                    var singer = null;
                    if(prog1 !== undefined && prog1 !== null) {
                        singer = KTW.utils.epgUtil.getSinger(prog1);
                        log.printDbg("singer : [" + singer + "]");
                        var audioTitle = "";
                        if (singer !== null) {
                            log.printDbg("prog1 singer.length : [" + singer.length + "]");
                            audioTitle = prog1.name + " / " + singer;
                        }else {
                            audioTitle = prog1.name;

                        }

                        textAudioChannelProgram1.text(audioTitle);
                        curProgram = prog1;


                        var tmp = new Date(prog1.startTime * 1000);
                        var st = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();
                        tmp = new Date((prog1.startTime + prog1.duration) * 1000);
                        var et = (tmp.getHours() < 10 ? "0" : "") + tmp.getHours() + ":" + (tmp.getMinutes() < 10 ? "0" : "") + tmp.getMinutes();

                        textAudioChannelProgram1Duration.text(st + ' - ' + et);
                        textAudioChannelProgram1Duration.css("display", "");


                        var age = KTW.utils.epgUtil.getAge(prog1);
                        var ageImg = "";;
                        if (age && age > 0) {
                            ageImg = IMAGE_PATH + "iframe/icon_age_musicch_" + age + ".png";
                        }
                        else if (age === 0) {
                            ageImg = IMAGE_PATH + "iframe/icon_age_musicch_all.png";
                        }
                        else {
                            ageImg = "";
                        }

                        var audioTitleLength = KTW.utils.util.getTextLength(audioTitle , "RixHead M" , 36 , -1.8);

                        if(ageImg === "") {
                            imgAgeIcon.css({left:411});
                            imgAgeIcon.css("display" , "none");
                            if(KTW.CONSTANT.IS_OTS !== true) {
                                textAudioChannelProgram1.css({width:800});
                            }else {
                                textAudioChannelProgram1.css({width:(897 + 600)});
                            }
                        }else {
                            if(KTW.CONSTANT.IS_OTS !== true) {
                                if(audioTitleLength > (800 - 7 - 34)) {
                                    textAudioChannelProgram1.css({width:(800 - 7 - 34)});
                                    imgAgeIcon.attr({src: ageImg});
                                    imgAgeIcon.css({left: (411 + (800 - 7 - 34) + 7)});
                                    imgAgeIcon.css("display" , "");
                                }else {
                                    textAudioChannelProgram1.css({width:(800 - 7 - 34)});
                                    imgAgeIcon.attr({src: ageImg});
                                    imgAgeIcon.css({left: (411 + (audioTitleLength + 7))});
                                    imgAgeIcon.css("display" , "");

                                }
                            }else {
                                if(audioTitleLength>((897 + 600) - 7 - 34)) {
                                    textAudioChannelProgram1.css({width: ((897 + 600) - 7 - 34)});
                                    imgAgeIcon.attr({src: ageImg});
                                    imgAgeIcon.css({left: (411 + (((897 + 600) - 7 - 34) + 7))});
                                    imgAgeIcon.css("display" , "");
                                }else {
                                    textAudioChannelProgram1.css({width:(897 + 600)});
                                    imgAgeIcon.attr({src: ageImg});
                                    imgAgeIcon.css({left: (411 + (audioTitleLength + 7))});
                                    imgAgeIcon.css("display" , "");

                                }
                            }
                        }
                    }


                    if(KTW.CONSTANT.IS_OTS !== true) {
                        if(prog2 !== undefined && prog2 !== null) {
                            singer = KTW.utils.epgUtil.getSinger(prog2);
                            log.printDbg("singer : [" + singer + "]");
                            if (singer !== null) {
                                log.printDbg("prog2 singer.length : [" + singer.length + "]");
                                textAudioChannelProgram2.text(prog2.name + " / " + singer);
                            }else {
                                textAudioChannelProgram2.text(prog2.name);
                            }

                        }
                    }
                }else {
                    curProgram = null;
                    textAudioChannelProgram1.text(KTW.utils.epgUtil.DATA.PROGRAM_TITLE.NULL);

                    textAudioChannelProgram1Duration.text("");
                    textAudioChannelProgram1Duration.css("display", "");

                    imgAgeIcon.css({left:411});
                    imgAgeIcon.css("display" , "none");


                    if(audioChannelTimer !== null) {
                        clearInterval(audioChannelTimer);
                        audioChannelTimer = null;
                    }
                    audioChannelTimer = setInterval(_callbackFuncProgramUpdateTimer, 10000);
                }
            }else {
                curProgram = null;
                textAudioChannelProgram1.text(KTW.utils.epgUtil.DATA.PROGRAM_TITLE.UPDATE);

                textAudioChannelProgram1Duration.text("");
                textAudioChannelProgram1Duration.css("display", "");

                imgAgeIcon.css({left:411});
                imgAgeIcon.css("display" , "none");


                if(audioChannelTimer !== null) {
                    clearInterval(audioChannelTimer);
                    audioChannelTimer = null;
                }
                audioChannelTimer = setInterval(_callbackFuncProgramUpdateTimer, 10000);
            }

        }
        
    }

};

KTW.ui.view.AudioIframeView.prototype = new KTW.ui.View();
KTW.ui.view.AudioIframeView.prototype.constructor = KTW.ui.view.AudioIframeView;

KTW.ui.view.AudioIframeView.prototype.create = function() {
    this.create();
};

KTW.ui.view.AudioIframeView.prototype.show = function() {
    this.show();
};

KTW.ui.view.AudioIframeView.prototype.hide = function() {
    this.hideAudioEpgViewIcon();
    this.hide();
};

KTW.ui.view.AudioIframeView.prototype.handleKeyEvent = function(key_code) {
    return KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
};