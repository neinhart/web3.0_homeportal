"use strict";

KTW.ui.view.LoadingDialog = function() {
    KTW.ui.View.call(this);
    
    //alias
    var util = KTW.utils.util;
    var log = KTW.utils.Log;
    
    var params;
        
    //data
    var timeout_timer;
    
    this.createView = function() {
        
        this.enableDCA = false;
        this.enableChannelKey = false;
        this.enableHotKey = false;
        
        var div = this.parent.div;
        params = this.parent.getParams();

        params = params || {};
        
        log.printDbg("[LoadingDialog] start. params" + log.stringify(params));

        if (params.isHide) {
            div.css({visibility: "hidden"});
            return;
        }
        else {
            div.css({visibility: ""});
        }
        
		var prevAttr = div.attr('style');
        div.attr({class: "popup", style: "overflow:visible;" + prevAttr});
        
        // [jh.lee] R5 에서 변경된 로딩에 맞춘 center 값
        var left = 0;
        var top = 0;
        var width = KTW.CONSTANT.RESOLUTION.WIDTH;
        var height = KTW.CONSTANT.RESOLUTION.HEIGHT;

        if (params.boxSize && params.boxSize.left && params.boxSize.top && params.boxSize.width && params.boxSize.height) {
            left = params.boxSize.left;
            top = params.boxSize.top;
            width = params.boxSize.width;
            height = params.boxSize.height;
        }

        log.printDbg("[LoadingDialog] left:" + left + " top:" + top + " width:" + width + " height:" + height);

        var spinner_tag = util.makeElement({
            tag: "<div />",
            attrs: {
                id: "spinner",
                css: {
                    position: "absolute",
                    left: ((width - left) / 2) - 82,
                    top: ((height - top) / 2) - 14,
                    width: 164,
                    height: 28,
                    overflow: "hidden"
                }
            },
            parent: div
        });

        for (var i = 0; i < 6; i++) {
            util.makeElement({
                tag: "<div />",
                attrs: {
                    id: "circle_" + i,
                    class: "spin_circle",
                    css: {
                        "margin-top": 4
                    }
                },
                parent: spinner_tag
            });
        }

        if (params.subText && params.subText.length > 0) {
            util.makeElement({
                tag: "<span />",
                attrs: {
                    css: {
                        position: "absolute",
                        left: left,
                        top: ((height - top) / 2) - 14 + 50,
                        width: width,
                        height: 40,
                        overflow: "hidden",
                        "font-family": "RixHead L",
                        "font-size": 33,
                        "letter-spacing": -1.65,
                        "text-align": "center",
                        color: "rgba(255, 255, 255, 0.7)"
                    }
                },
                text: params.subText,
                parent: div
            });
        }
    };
    
    this.showView = function() {
        if (params.timeout) {
            timeout_timer = setTimeout(onTimeout, params.timeout);
        } else {
            timeout_timer = setTimeout(onTimeout, 30 * 1000);
        }
    };
    
    this.hideView = function() {

    };
    
    this.destroyView = function() {
        if (timeout_timer) {
            clearTimeout(timeout_timer);
        }
    };
    
    this.controlKey = function(keyCode) {
        
        /**
         * jm.kang
         * 이때는 모든키를 다 막아야 하는가 ? 
         * 예외처리 키는 없는가 ?. 
         */
        if (params.preventKey === true) {
            return true;
        }

        return false;
    };
    
    function onTimeout() {
        if (params.cbTimeout){
            params.cbTimeout();
        }
        else {
            //// [jh.lee] 비밀번호 인증 및 변경 과정에서 로딩타임아웃의 콜백이 없는 경우를 위해 비밀번호 락 해제 함수 호출
            //KTW.managers.auth.AuthManager.resetHandlingResult();
            //// showErrorPopup에서 오류팝업을 호출할 때 last layer가 popup이면 popup index를 변경하는 로직이 있는데
            //// last layer가 loading dialog로 잡혀서 popup index 변경이 이루어지지 않고 기존 팝업의 뒤에 뜨는 이슈가 존재
            //// [KTWHPTZOF-1605]
            //// 따라서 stopLoading을 먼저 호출한 뒤에 오류팝업을 호출하도록 수정
            //KTW.ui.LayerManager.stopLoading();
            //KTW.utils.util.showErrorPopup(KTW.ERROR.CODE.E020.split("\n"), "알림", null, true);
        }
        
        KTW.ui.LayerManager.stopLoading();
    }
};

KTW.ui.view.LoadingDialog.prototype = new KTW.ui.View();
KTW.ui.view.LoadingDialog.prototype.constructor = KTW.ui.view.LoadingDialog;

KTW.ui.view.LoadingDialog.prototype.create = function() {
	KTW.ui.View.prototype.create.call(this);
    this.createView();
};

KTW.ui.view.LoadingDialog.prototype.show = function() {
	var args = [].slice.call(arguments);
    this.showView(args[0]);
    KTW.ui.View.prototype.show.call(this, args);
};

KTW.ui.view.LoadingDialog.prototype.hide = function() {
    this.hideView();
    KTW.ui.View.prototype.hide.call(this);
};

KTW.ui.view.LoadingDialog.prototype.destroy = function() {
    this.destroyView();
    KTW.ui.View.prototype.destroy.call(this);
};

KTW.ui.view.LoadingDialog.prototype.handleKeyEvent = function(key_code) {
    return this.controlKey(key_code);
    /* mook_t popuptype 이 아님으로 this.controlKey(key_code);
    return KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);*/
};

Object.defineProperty(KTW.ui.view.LoadingDialog, "TYPE", {
    value:  {
        CENTER : 0,
        POSTER : 1,
        SETTING : 2,
        MY_SETTING : 3,
        MY_PREFERENCE : 4,
        UPSELLING_POPUP : 5
    }
});
