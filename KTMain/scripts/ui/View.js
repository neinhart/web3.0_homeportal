"use strict";

/**
 * View object
 * 
 * layer에 attach 되는 view
 */
KTW.ui.View = function() {
    // layer object
    this.parent = null;
    
    this.enableDCA = true;
    this.enableChannelKey = true;
    this.enableMenuKey = true;
    this.enableExitKey = true;
    this.enableBackKey = true;
    this.enableHotKey = true;

    // popup에서 소비하지 않는 key를 입력할 경우
    // 무조건 popup을 hide 처리하기 위한 property
    this.hideAnyKey = false;
    
    // SYSTEM POPUP인데 특정 키를 제외하고 모든 key를 bypass 해야되는 경우
    // key를 bypass해서 다른 UI에서 사용할 수 있도록 하기 위한 property
    this.bypassKey = false;
};

KTW.ui.View.prototype = {
    /**
     * layer 생성 후 create 됨
     */
    create : function() {
        // popup type인 경우 hot key 옵션에 따라 동작할 수 있도록 처리
        if (this.parent.type === KTW.ui.Layer.TYPE.POPUP) {
            var params = this.parent.getParams();
            
            this.enableDCA = params.enableDCA ? (params.enableDCA === "true") : true;
            this.enableChannelKey = params.enableChannelKey ? (params.enableChannelKey === "true") : true;
            this.enableMenuKey = params.enableMenuKey ? (params.enableMenuKey === "true") : true;
            this.enableExitKey = params.enableExitKey ? (params.enableExitKey === "true") : true;
            this.enableBackKey = params.enableBackKey ? (params.enableBackKey === "true") : true;
            this.enableHotKey = params.enableHotKey ? (params.enableHotKey === "true") : true;
            this.hideAnyKey = params.hideAnyKey ? (params.hideAnyKey === "true") : false;
            this.bypassKey = params.bypassKey ? (params.bypassKey === "true") : false;
            
            // SYSTEM popup이 아니라고 하더라도 bypassKey 값을 설정할 수 있다.
            // 그래서 혹시 잘못 설정해서 key 동작이 이상하게 처리되는 것을 막기 위해서
            // 보완코드 삽입함
            if (this.bypassKey === true && this.parent.priority !== KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                this.bypassKey = false;
            }
        }
    },
    /**
     * remove view
     * 
     * 실제 view는 layer에 의해서 제거되므로 memory 점유 object를 clear 해야 함
     */
    destroy : function() {},
    /**
     * show view
     * 
     * layer에 의해서 visible 상태가 되는데 내부 div tag가 hidden 상태라면
     * visibility를 visible 로 설정해야 함
     */
    show : function() {},
    /**
     * hide view
     * 
     * popup 등은 destroy 되지만 다른 ui의 경우 hide만 되는 경우가 있으므로
     * 해당 div tag의 visibility를 hidden 으로 설정해야 함
     */
    hide : function() {},
    /**
     * set parent object
     * 
     * @param parent layer obejct
     */
    setParent : function(parent) {
        this.parent = parent;
    },
    /**
     * return whether or not cousume key event
     * 
     * @param key_code
     * @returns {Boolean} true if consume key event, otherwise false
     */
    handleKeyEvent: function(key_code) {
        if (this.parent.type !== KTW.ui.Layer.TYPE.POPUP) {
            return false;
        }
        
        if (key_code === KTW.KEY_CODE.WINK) {
    		// 음성검색 핫키는 무조건 동작하도록 함
    		return false;
    	}
        else if (key_code === KTW.KEY_CODE.STAR) {
        	// 별표 키는 무조건 동작하도록 함
        	return false;
        }
        
        var layerManager = KTW.ui.LayerManager;
        var util = KTW.utils.util;
        
        // 어떤 key를 눌러도 popup이 사라져야 하는 경우 (ex. 자동대기 모드 popup)
        // 무조건 모든 key를 consume처리 한다.
        // callback이 없는 경우 의도적으로 deactivate 처리를 한다.
        if (this.hideAnyKey === true) {
            layerManager.deactivateLayer({
                id: this.parent.id,
                remove: true
            });
            return true;
        }
        
        // 검색 및 전체편성표 외의 hot key는 모두 처리하지 않도록 함
        // R5 Jira-1471 확인 후 발견하여 수정한 내용
        //    성인인증 팝업에서 핫키가 동작하도록 해야해서 return true 를 false로 변경
        //    단, VOD 재생 중에는 enableHotKey를 false로 변경하여 팝업을 생성하기 때문에 핫키 동작 막음.
        if (this.enableHotKey === true) {
            switch(key_code) {
                case KTW.KEY_CODE.MOVIE :
                case KTW.KEY_CODE.REWATCH :
                case KTW.KEY_CODE.MONTHLY :
                case KTW.KEY_CODE.FAVORITE :
                case KTW.KEY_CODE.RECOMMEND :
                case KTW.KEY_CODE.WATCH_LIST :
                case KTW.KEY_CODE.PURCHASE_LIST :
                case KTW.KEY_CODE.WISH_LIST :
                case KTW.KEY_CODE.SETTING :
                case KTW.KEY_CODE.MYMENU :
                case KTW.KEY_CODE.FULLEPG :
                case KTW.KEY_CODE.FULLEPG_OTS :
                case KTW.KEY_CODE.SKY_CHOICE :
                case KTW.KEY_CODE.SEARCH :
                case KTW.KEY_CODE.SHOPPING :
                    /**
                     * [dj.son] 키즈 리모컨 핫키 추가
                     */
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_1:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_2:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_3:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_4:
                    return false;
            }
        }
        
        // popup 형태의 view에서는 일반적으로 key를 모두 소진하는데
        // 예외적으로 DCA 또는 몇몇 key를 bypass 되어야 하는 경우가 존재함
        // 그래서 모든 key를 popup view에서 소진하고 
        // 특수한 경우에만 key를 bypass 하도록 상속 Object에서는
        // handleKeyEvent 함수를 override 해서 사용하되 
        // 부모 함수를 호출하도록 해야 하므로 필히 아래 코드형태로 처리되어야 함. 
        // KTW.ui.popup.Popup.prototype.handleKeyEvent = function(key_code) {
        //     this.controlKey(key_code);
        //     return KTW.ui.View.prototype.handleKeyEvent.call(this, key_code);
        // }
        if (this.enableDCA === true) {
            if (util.keyCodeToNumber(key_code) >= 0) {
                return false;
            }
        }
        
        if (this.enableChannelKey === true) {
            switch(key_code) {
                case KTW.KEY_CODE.CH_UP :
                case KTW.KEY_CODE.CH_DOWN :
                case KTW.KEY_CODE.PREV_CHANNEL :

            /**
             * [dj.son] 키즈 리모컨 채널 up / down 추가
             */
                case KTW.KEY_CODE.KIDS_CH_UP :
                case KTW.KEY_CODE.KIDS_CH_DOWN :
                    return false;
            }
        }
        
        // menu key의 경우 popup이 존재하더라도 동작해야 함.
        // 그래서 popup을 close 하고 menu key 를 bypass 하도록 했음
        // 혹시 popup에서 menu key가 동작하지 않아야 할 경우
        // this.enableMenuKey = false로 설정해야 함
        // menu key의 경우 popup에서 bypass 되고
        // LayerManager#handleHotKeyEvent()로 전달되는 구조이다.
        // 그래서 의도적으로 중첩된 popup 들을 제거할 필요없이
        // showwindow를 다시 activate 하면서 기존 popup들을 모두 제거한다.
        /**
         * [dj.son] 키즈 리모컨 홈키 추가
         */
        if (this.enableMenuKey === true && (key_code === KTW.KEY_CODE.MENU || key_code === KTW.KEY_CODE.KIDS_MENU)) {
            return false;
        }
        
        // 특정 popup의 경우 exit 및 back key가 동작하지 않아야 하는 경우가 있음
        // 그래서 this.enableExitKey 및 this.enableBackKey 를 false로 설정한 경우
        // popup이 닫히지 않도록 처리됨
        if ((this.enableExitKey === true && key_code === KTW.KEY_CODE.EXIT) || (this.enableBackKey === true && key_code === KTW.KEY_CODE.BACK)) {
            if (this.bypassKey === false) {
                layerManager.deactivateLayer({
                    id: this.parent.id,
                    remove: true
                });
            }
        }

        return this.bypassKey === false;
    },
    disableHotKey : function() {
        this.enableDCA = false;
        this.enableChannelKey = false;
        this.enableMenuKey = false;
        this.enableExitKey = false;
        this.enableBackKey = false;
        this.enableHotKey = false;
    }
}
