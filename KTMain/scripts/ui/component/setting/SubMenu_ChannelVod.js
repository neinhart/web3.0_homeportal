/**
 * Created by Yun on 2016-11-17.
 *  설정 > 채널 / VOD 설정 preview
 */
var SubMenu_ChannelVod = function (options) {
    var INSTANCE = options.instance;
    var settingMenu = options.menuData;
    var isFocus = false;
    var focusIdx = 0;
    var log = KTW.utils.Log;
    var util = KTW.utils.util;
    var settingMenuManager = KTW.managers.service.SettingMenuManager;
    var menuList = [];
    var isUpdatedMenuList = false;

    this.setSubTitle = function(index, str) {
        // 2017.04.17 dhlee
        if (menuList.length <= 0) {
            log.printDbg("setSubTitle(), menuList.length is less than equal 0");
            return;
        }
        menuList[index].subTitle = str;
        INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + index + ") .btnText_area2 .btnTitle_div2").text(menuList[index].subTitle);
    };
    this.getView = function () {
        log.printDbg("getView()");
        var html = "<div id='menuBox_area' class='channelVod'>";

        menuList = settingMenuManager.createSettingMenuList(settingMenu.children.length);
        for (var idx = 0; idx < menuList.length; idx++) {
            html += addMenuBoxItem(idx);
        }
        html += "</div>";
        return html;
    };

    this.focus = function () {
        log.printDbg("focus()");
        INSTANCE.div.find("#menuBox_area.channelVod").addClass("focus");
        INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(0)").addClass("focus");
        focusIdx = 0;
        isFocus = true;
        buttonFocusRefresh(focusIdx);
    };
    this.unFocus = function () {
        log.printDbg("unfocus()");
        INSTANCE.div.find("#menuBox_area.channelVod").removeClass("focus");
        INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn").removeClass("focus");
        isFocus = false;
    };
    this.show = function (options) {
        log.printDbg("show()");
        INSTANCE.div.find("#menuBox_area.channelVod").removeClass("hide");
        settingMenu = options;

        if(!menuList) {
            log.printDbg(" MenuList does not exist.. return back.");
        }
        if(!isUpdatedMenuList) {
            menuList =  settingMenuManager.updateSettingMenuItem(settingMenu,menuList);
            isUpdatedMenuList = true;
        }
        refreshTitle();

    };
    this.hide = function () {
        log.printDbg("hide()");
        INSTANCE.div.find("#menuBox_area.channelVod").addClass("hide");
    };

    this.menuTypeReset = function () {
    };

    this.getIsFocus = function () {
        return isFocus;
    };

    function addMenuBoxItem(idx) {
        var html = "<div class='menuBox_btn'>" +
            "<div class= 'btnLine'>" +"</div>"+
            "<div class='btnElm_div'>" +
            "<div class='btnElm_SdwDiv'>" +
            "<img class='btnSdwImg_center' src='images/setting/sdw_setting_w613.png'>" +
            "</div>";
        html += "<img class='btnImg_center' src='images/setting/set_menu_box_lg.png'>"+
        "<div class='btnFocus_div'></div>"+
        "<div class= btnFocusGreyDiv style ='position: absolute; left:1px; top: 1px; width: 557px; height: 140px; background-color: rgb(52,51,50);'></div>";
        html += "<div class='btnText_area1'>" +
        "<div class='btnTitle_div'></div></div>";
        html += "<div class='btnText_area2'>" +
        "<div class='btnTitle_div1'></div>";
        html += "<div class='btnTitle_div2'></div>";

        html += "</div></div></div>";
        return html;
    }
    function buttonFocusRefresh(index) {
        focusIdx = index;
        INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn").removeClass("focus");
        INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + index + ")").addClass("focus");

    }
    function refreshTitle() {
        var language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
        var name;

        for (var i = 0; i < settingMenu.children.length; i++) {
            if (language === "kor") {
                name = settingMenu.children[i].name;
            } else {
                name = settingMenu.children[i].englishItemName;
            }
            menuList[i].title = name;

            if(menuList[i].subTitle === undefined) {
                INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area1 .btnTitle_div").text(menuList[i].title);
                INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area1").css({ visibility:"inherit"});
                INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area2").css({ visibility:"hidden"});
            } else {
                INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div1").text(menuList[i].title);
                INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area1").css({ visibility:"hidden"});
                INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area2").css({ visibility:"inherit"});
            }
            INSTANCE.div.find("#menuBox_area.channelVod .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div2").text(menuList[i].subTitle);
        }
    }

    this.onKeyAction = function (keyCode) {
        var KEY_CODE = KTW.KEY_CODE;
        var keyAction = {isUsed : false};
        switch (keyCode) {
            case KEY_CODE.ENTER:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;

                if(menuList[focusIdx].id){
                    KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                        menuId: menuList[focusIdx].id,
                        name : menuList[focusIdx].title,
                        menuIdx: focusIdx,
                        updateSubTitle: true
                    });
                }
                //2017.05.30 [sw.nam] Add collecting log method (In case accessing to ETC_MENU)
                KTW.managers.UserLogManager.collect({
                    type: KTW.data.UserLog.TYPE.ETC_MENU,
                    act: keyCode,
                    menuId: menuList[focusIdx].id,
                    menuName: menuList[focusIdx].title
                });
                return keyAction;
            case KEY_CODE.UP:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if(focusIdx -2 < 0) {
                    buttonFocusRefresh(menuList.length-1);
                }else {
                    buttonFocusRefresh(util.getIndex(focusIdx, -2, menuList.length));
                }
                return keyAction;
            case KEY_CODE.DOWN:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if(focusIdx+2 > menuList.length-1) {
                    if((focusIdx+2 == menuList.length) && (menuList.length % 2 !== 0)) {
                        buttonFocusRefresh(menuList.length-1);
                    }else {
                        buttonFocusRefresh(0);
                    }
                }else {
                    buttonFocusRefresh(util.getIndex(focusIdx, 2, menuList.length));
                }
                return keyAction;
            //2017.11.07 sw.nam 적청키 navigation 추가
            // 채널/VOD 설정 메뉴는  스크롤이 없으므로 부가적인 예외처리는 추가하지 않음     
            case KEY_CODE.RED:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if(focusIdx != 0) {
                    buttonFocusRefresh(0);
                }
                return keyAction;
            case KEY_CODE.BLUE:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if(focusIdx != menuList.length-1) {
                    buttonFocusRefresh(menuList.length-1);
                }
                return keyAction;
            case KEY_CODE.LEFT:
                if (focusIdx%2 == 0) {
                    keyAction.prevMenu = true;
                    keyAction.isUsed = true;
                } else {
                    keyAction.prevMenu = false;
                    keyAction.isUsed = true;
                }
                buttonFocusRefresh(util.getIndex(focusIdx, -1, menuList.length));
                return keyAction;
            case KEY_CODE.RIGHT:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                buttonFocusRefresh(util.getIndex(focusIdx, 1, menuList.length));
                return keyAction;
            case KEY_CODE.BACK:
                keyAction.prevMenu = true;
                keyAction.isUsed = true;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };
};