/**
 * Created by Yun on 2016-11-17.
 *
 * 설정 > 시스템 설정 preview 컴포넌트
 *TODO
 *  2017 04.24 sw.nam 현재 2페이지 이상의 리스트가 구성되는 설정 메뉴는 시스템 설정 하나이므로
 * 페이지 스크롤링에 관련한 로직은 이 컴포넌트 내부에만 존재 함.
 * 추가로 필요한경우 다른 컴포넌트에서 스크롤 로직 적용해야 함. (일정상 시스템 설정 내부에만 존재)
 */

var SubMenu_System = function(options) {

    var INSTANCE = options.instance;
    var settingMenu = options.menuData;

    var isFocus = false;
    var focusIdx = 0;
    var toBeCurPage = 0;
    var curPage = 0;

    var log = KTW.utils.Log;
    var isAdmin = false;

    var settingMenuManager = KTW.managers.service.SettingMenuManager;

    var menuList = [];
    var isUpdatedMenuList = false;

    var menuListOTSAdmin = [];
    var menuListUHD3Beacon = null;

    var NUMBER_OF_PREVIEW_ITEM = 10;//12;

    var scroll;
    var colorKeyArea;

    var isScrollArea = false;

    this.setSubTitle = function(index, str) {
        log.printDbg("setSubTitle()");
        // 2017.04.17 dhlee
        if (menuList.length <= 0) {
            log.printDbg("setSubTitle(), menuList.length is less than equal 0");
            return;
        }
        if(!isAdmin) {
            menuList[index].subTitle = str;
            INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + index + ") .btnText_area2 .btnTitle_div2").html(menuList[index].subTitle);
        }else {
            menuListOTSAdmin[index].subTitle = str;
            INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + index + ") .btnText_area2 .btnTitle_div2").html(menuListOTSAdmin[index].subTitle);
        }
    };

    //sw.nam 시스템 해상도 리셋에 의한 text 변경을 위해 사용
    this.setSpecificSubTitle = function(index, str) {
        log.printDbg("setSpecificSubTitle()");
        var curPage = getCurrentPage();
        if (menuList.length <= 0) {
            log.printDbg("setSubTitle(), menuList.length is less than equal 0");
            return;
        }
        menuList[index].subTitle = str;
        // 2017.07.12 sw.nam
        // 시스템 설정 메뉴에서 여러 페이지의 메뉴 리스트가 하나의 UI 를 공유하므로 첫 페이지가 아닌 경우 subTitle 변경 안되도록 함.
        if(curPage !== 0) {
            log.printDbg("no change subTitle");
            return;
        }
        INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + index + ") .btnText_area2 .btnTitle_div2").html("");
        INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + index + ") .btnText_area2 .btnTitle_div2").html(menuList[index].subTitle);
    };

    this.getView = function() {
        log.printDbg("getView()");

        // create scroll Area --> 시스템 설정 메뉴는 갯수가 많으므로 스크롤 필요.
        var html = "<div id='menuBox_area' class='setSystem'>" +
            "<div id='normalMenuBox_div'>";

        menuList = settingMenuManager.createSettingMenuList(settingMenu.children.length);

        menuListOTSAdmin  = settingMenuManager.createHiddenSettingMenu(settingMenu);

        for (var idx = 0; idx < (menuList.length < NUMBER_OF_PREVIEW_ITEM+2 ? menuList.length: NUMBER_OF_PREVIEW_ITEM+2) ; idx++) {
            html += addMenuBoxItem(idx);
        }
        html += "</div><div id='adminMenuBox_div'>";
        for (var idx = 0 ; idx < menuListOTSAdmin.length ; idx ++) {
            html += addMenuBoxItem(idx);
        }
        html += "</div></div>";
        html +=  "<div id='scroll' style='position: absolute; left: 1829px; top: 16px; height: 682px; overflow: visible; visibility: hidden; '></div>";

        // 페이지 전환 key 가이드 추가.
        html += "<div class='colorKey' style='position: absolute; top: -148px; visibility: hidden;'>+" +
                    "<img class='redKey' src='images/icon/icon_pageup.png' style='position:absolute; left: 1803px; top: 904px; width: 29px; height 32px;'>"+
                    "<img class='blueKey' src='images/icon/icon_pagedown.png' style='position:absolute; left: 1831px; top: 904px; width: 29px; height 32px;'>"+
                    "<span class='keyText' style='position: absolute; left: 1803px; top: 939px; width: 109px; height:26px; letter-spacing: -1.1px; font-size: 22px; color: rgba(255,255,255,0.3);'>페이지</span>"+
                 "</div>";
        return html;
    };

    this.focus = function() {
        log.printDbg("focus()");

        INSTANCE.div.find("#menuBox_area.setSystem").addClass("focus");
        INSTANCE.div.find("#menuBox_area.setSystem " + (isAdmin ? "#adminMenuBox_div" : "#normalMenuBox_div") + " .menuBox_btn:eq(0)").addClass("focus");

        focusIdx = 0;
        isFocus = true;

        if(isScrollArea){
            //setScrollFocus(false);
        }

        if(!isAdmin) {
            scroll.show({
                maxPage: getMaxPage(),
                curPage: getCurrentPage()
            });
            updatePreviewList(focusIdx);
            INSTANCE.div.find(".colorKey").css({visibility:"inherit"});
        }
    };

    this.unFocus = function() {
        INSTANCE.div.find("#menuBox_area.setSystem").removeClass("focus");
        INSTANCE.div.find("#menuBox_area.setSystem .menuBox_btn").removeClass("focus");

        isFocus = false;

        if(!isAdmin) {
            if(scroll) {
                focusIdx =0;
                setCurrentPage();
                scroll.hide();
            }
            INSTANCE.div.find(".colorKey").css({visibility:"hidden"});
        }
    };

    this.show = function(options) {
        log.printDbg("show()");
        INSTANCE.div.find("#menuBox_area.setSystem").removeClass("hide");

        settingMenu = options;
        if(!menuList) {
            log.printDbg(" MenuList does not exist.. return back.");
        }
        if(!isUpdatedMenuList) {
            menuList =  settingMenuManager.updateSettingMenuItem(settingMenu,menuList);
            isUpdatedMenuList = true;
        }
        refreshTitle();
        updatePreviewList();
        if(!scroll) {
            log.printDbg("makeScroll")
            scroll = new KTW.ui.component.Scroll({
                parentDiv: INSTANCE.div.find("#scroll"),
                height: 682,
                forceHeight: true

            });
            INSTANCE.div.find(".colorKey .keyText").css({"font-family" : "RixHead L"});
        }

        if(isScrollArea){
          //  setScrollFocus(false);
        }
    };

    this.hide = function() {
        log.printDbg("hide()");
        INSTANCE.div.find("#menuBox_area.setSystem").addClass("hide");
    };

    this.destroy = function(){
        log.printDbg("destroy()");
    };

    this.menuTypeReset = function () {
        INSTANCE.div.find("#normalMenuBox_div").removeClass("hide");
        INSTANCE.div.find("#adminMenuBox_div").addClass("hide");
        isAdmin = false;
    };

    this.getIsFocus = function() {
        return isFocus;
    };

    this.menuListChange = function() {
        isAdmin = true;
        INSTANCE.div.find("#normalMenuBox_div").addClass("hide");
        INSTANCE.div.find("#adminMenuBox_div").removeClass("hide");
    };


    function refreshTitle() {
        log.printDbg("refreshTitle()");

        //var language = KTW.ui.adaptor.SettingDataAdaptor.getConfigurationInfoByMenuId(KTW.managers.data.MenuDataManager.MENU_ID.LANGUAGE);
        var language = KTW.managers.data.MenuDataManager.getCurrentMenuLanguage();
        var name;

        if(menuListOTSAdmin) {
            for (var i = 0; i < menuListOTSAdmin.length; i++) {
                if(menuListOTSAdmin[i].subTitle === undefined) {
                    INSTANCE.div.find("#menuBox_area.setSystem #adminMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area1 .btnTitle_div").text(menuListOTSAdmin[i].title);
                } else {
                    INSTANCE.div.find("#menuBox_area.setSystem #adminMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div1").text(menuListOTSAdmin[i].title);
                }
                INSTANCE.div.find("#menuBox_area.setSystem #adminMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div2").text(menuListOTSAdmin[i].subTitle);
            }
        }
        for (var i = 0; i < settingMenu.children.length; i++) {
            if (language === "kor") {
                name = settingMenu.children[i].name;
            } else {
                name = settingMenu.children[i].englishItemName;
            }
            menuList[i].title = name;

            if(menuList[i].subTitle === undefined) {
                INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area1 .btnTitle_div").text(menuList[i].title);
            } else {
                INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div1").text(menuList[i].title);
            }
            INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div2").text(menuList[i].subTitle);
        }

    }
    function addMenuBoxItem(idx) {
        var html = "<div class='menuBox_btn'>" +
            "<div class= 'btnLine'>" +"</div>"+
            "<div class='btnElm_div'>" +
            "<div class='btnElm_SdwDiv'>" +
            "<img class='btnSdwImg_center' src='images/setting/sdw_setting_w613.png'>" +
            "</div>";
        html += "<img class='btnImg_center' src='images/setting/set_menu_box_lg.png'>"+
        "<div class='btnFocus_div'></div>"+
        "<div class= btnFocusGreyDiv style ='position: absolute; left:1px; top: 1px; width: 557px; height: 140px; background-color: rgb(52,51,50);'></div>";
        html += "<div class='btnText_area1'>" +
        "<div class='btnTitle_div'></div></div>";
        html += "<div class='btnText_area2'>" +
        "<div class='btnTitle_div1'></div>";
        html += "<div class='btnTitle_div2'></div>";

        html += "</div></div></div>";
        return html;


    }

    function buttonFocusRefresh(index) {
        focusIdx = index;

        INSTANCE.div.find("#menuBox_area.setSystem .menuBox_btn").removeClass("focus");
        INSTANCE.div.find("#menuBox_area.setSystem " + (isAdmin ? "#adminMenuBox_div" : "#normalMenuBox_div") + " .menuBox_btn:eq(" + index % NUMBER_OF_PREVIEW_ITEM + ")").addClass("focus");
        // if (Math.floor(index / 10) != toBeCurPage) changePage(Math.floor(index / 10));
        // set scroll page.
        toBeCurPage = getCurrentPage();
        if(curPage != toBeCurPage) {
            setCurrentPage();
        }
    }

    /**
     * 2017.04.24 sw.nam 시스템 설정 수정 GUI 반영 [WEBIIIHOME-211]
     *
     */
    function updatePreviewList() {
        log.printDbg("updatePreviewList()");
        var startIdx = focusIdx - (focusIdx % (NUMBER_OF_PREVIEW_ITEM));

        for(var i =0; i < (NUMBER_OF_PREVIEW_ITEM+2); i ++) {

            if((startIdx+i) > menuList.length -1 ) {
                INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ")").css({visibility: "hidden"});
            } else {
                INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ")").css({visibility: "inherit"});
                if(menuList[startIdx+i].subTitle == undefined) {
                    INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area1 .btnTitle_div").text(menuList[startIdx+i].title);
                    INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area1").css({ visibility:"inherit"});
                    INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2").css({ visibility:"hidden"});
                } else {
                    INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div1").text(menuList[startIdx+i].title);
                    INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area1").css({ visibility:"hidden"});
                    INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2").css({ visibility:"inherit"});
                }
                INSTANCE.div.find("#menuBox_area.setSystem #normalMenuBox_div .menuBox_btn:eq(" + i + ") .btnText_area2 .btnTitle_div2").text(menuList[startIdx+i].subTitle);
            }
        }
    }

    //[sw.nam] 2017.04.24 설정 > 시스템 설정 페이지 스크롤 기능 반영 및 GUI 재구성 WEBIIIHOME-510 	WEBIIIHOME-312
    this.onKeyAction = function(keyCode) {
        var KEY_CODE = KTW.KEY_CODE;
        var keyAction = {isUsed: false};
        switch (keyCode) {
            case KEY_CODE.ENTER:
                if(isScrollArea) {
                 //   setScrollFocus(false);
                    buttonFocusRefresh(focusIdx);
                } else {
                    if (isAdmin) {
                        if (menuListOTSAdmin[focusIdx].id) {
                            KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                                menuId: menuListOTSAdmin[focusIdx].id,
                                name: menuListOTSAdmin[focusIdx].title,
                                menuIdx: focusIdx,
                                updateSubTitle: true
                            });

                            KTW.managers.UserLogManager.collect({
                                type: KTW.data.UserLog.TYPE.ETC_MENU,
                                act: keyCode,
                                menuId: menuListOTSAdmin[focusIdx].id,
                                menuName: menuListOTSAdmin[focusIdx].title
                            });

                        }
                    } else {
                        if (menuList[focusIdx].id) {
                            KTW.managers.service.SettingMenuManager.activateSettingMenuByMenuID({
                                menuId: menuList[focusIdx].id,
                                name: menuList[focusIdx].title,
                                menuIdx: focusIdx,
                                updateSubTitle: true
                            });

                            KTW.managers.UserLogManager.collect({
                                type: KTW.data.UserLog.TYPE.ETC_MENU,
                                act: keyCode,
                                menuId: menuList[focusIdx].id,
                                menuName: menuList[focusIdx].title
                            });
                        }
                    }
                }
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.UP:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if (isScrollArea) {
                    focusIdx = focusIdx - (NUMBER_OF_PREVIEW_ITEM + (focusIdx % NUMBER_OF_PREVIEW_ITEM));
                    if(focusIdx < 0) {
                        focusIdx = (menuList.length-1) - ((menuList.length-1) % NUMBER_OF_PREVIEW_ITEM);
                    }
                    setCurrentPage();
                } else {
                    if(focusIdx -2 < 0) {
                        buttonFocusRefresh(isAdmin ? menuListOTSAdmin.length-1 : menuList.length-1);
                    }else {
                        buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -2, (isAdmin ? menuListOTSAdmin.length : menuList.length)));
                    }
                }
                return keyAction;
            case KEY_CODE.DOWN:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if(isScrollArea) {
                    focusIdx = focusIdx + (NUMBER_OF_PREVIEW_ITEM - (focusIdx % NUMBER_OF_PREVIEW_ITEM));
                    if(focusIdx > menuList.length-1) {
                        focusIdx = 0;
                    }
                    setCurrentPage();
                } else {
                    if(isAdmin){
                        if(focusIdx+2 > menuListOTSAdmin.length-1) {
                            //[sw.nam] 2017.06.08 키포커스 끝처리 로직 추가
                            if((focusIdx+2 == menuListOTSAdmin.length) && (menuListOTSAdmin.length % 2 !== 0)) {
                                buttonFocusRefresh(menuListOTSAdmin.length-1);
                            }else {
                                buttonFocusRefresh(0);
                            }
                        }else {
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 2, menuListOTSAdmin.length));
                        }
                    }else {
                        if(focusIdx+2 > menuList.length-1) {
                            if((focusIdx+2 == menuList.length) && (menuList.length % 2 !== 0)) {
                                buttonFocusRefresh(menuList.length-1);
                            }else {
                                buttonFocusRefresh(0);
                            }
                        }else {
                            buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 2, menuList.length));
                        }
                    }
                }
                return keyAction;
            case KEY_CODE.RED:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;

                if(isScrollArea) {
                    // 현재 페이지의 가장 첫번쨰 텝으로 이동
                //    setScrollFocus(false);
                    focusIdx = curPage * NUMBER_OF_PREVIEW_ITEM;
                    buttonFocusRefresh(focusIdx);
                }else {
                    if(isAdmin) {
                        if(focusIdx != 0) {
                            buttonFocusRefresh(0);
                        }
                    }else {
                        var tmpPageIdx = curPage;
                        if(focusIdx == curPage * NUMBER_OF_PREVIEW_ITEM) {
                            if(curPage -1 < 0 ) {
                                tmpPageIdx =  getMaxPage() -1;
                            }else {
                                tmpPageIdx --;
                            }
                        }
                        focusIdx = tmpPageIdx * NUMBER_OF_PREVIEW_ITEM;
                        buttonFocusRefresh(focusIdx);
                    }
                }
                return keyAction;
            case KEY_CODE.BLUE:
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                if(isScrollArea) {
                //    setScrollFocus(false);
                    // 현재 페이지의 가장 마지막 텝으로 이동
                    if(curPage == getMaxPage() -1) {
                        //마지막 페이지인 경우에는 리스트의 마지막 인덱스로 포커스
                        focusIdx = menuList.length-1;
                    }else {
                        // 현재 페이지의 마지막 인덱스로 이동
                        focusIdx = (curPage * NUMBER_OF_PREVIEW_ITEM) + (NUMBER_OF_PREVIEW_ITEM -1);
                    }
                    buttonFocusRefresh(focusIdx);
                }else {
                    if(isAdmin) {
                        // 관리자 페이지는 1페이지이므로 페이지 전환이 필요없이 마지막 인덱스로 고정
                        if(focusIdx !== menuListOTSAdmin.length -1) {
                            buttonFocusRefresh(menuListOTSAdmin.length-1);
                        }
                    }else {
                        var tmpPageIdx = curPage;
                        //현재 포커스가 어디 위치해 있는지 확인
                        if(curPage == getMaxPage() -1) {
                            //마지막 페이지 인 경우
                            if(focusIdx == menuList.length-1) {
                                focusIdx = NUMBER_OF_PREVIEW_ITEM -1;
                            }else {
                                focusIdx = menuList.length-1;
                            }
                        }else {
                            // 현재 페이지의 마지막 인덱스로 이동
                            if(focusIdx == (tmpPageIdx * NUMBER_OF_PREVIEW_ITEM) + (NUMBER_OF_PREVIEW_ITEM) -1) {
                                tmpPageIdx ++;
                                // 이동된 임시 페이지 인덱스가 마지막 페이지인 경우 처리
                                if(tmpPageIdx = getMaxPage() -1) {
                                    focusIdx = menuList.length-1;
                                }
                            }
                            else {
                                focusIdx = (tmpPageIdx * NUMBER_OF_PREVIEW_ITEM) + (NUMBER_OF_PREVIEW_ITEM -1);
                            }
                        }
                        buttonFocusRefresh(focusIdx);
                    }
                }
                return keyAction;
            case KEY_CODE.LEFT:
                if(isScrollArea) {
                    keyAction.prevMenu = true;
                    keyAction.isUsed = true;
                }
                if (focusIdx % 2 == 0) {
                  /*  if(isAdmin) {
                        keyAction.prevMenu = true;
                        keyAction.isUsed = true;
                    } else {
                        keyAction.prevMenu = false;
                        keyAction.isUsed = true;
                        INSTANCE.div.find("#menuBox_area.setSystem .menuBox_btn").removeClass("focus");
                     //   setScrollFocus(true);
                    }*/
                    keyAction.prevMenu = true;
                    keyAction.isUsed = true;
                } else {
                    keyAction.prevMenu = false;
                    keyAction.isUsed = true;
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, -1, (isAdmin ? menuListOTSAdmin.length : menuList.length)));
                }
                return keyAction;
            case KEY_CODE.RIGHT:
                if(isScrollArea) {
                  //  setScrollFocus(false);
                    buttonFocusRefresh(focusIdx);
                } else {
                    buttonFocusRefresh(KTW.utils.util.getIndex(focusIdx, 1, (isAdmin ? menuListOTSAdmin.length : menuList.length)));
                }
                keyAction.prevMenu = false;
                keyAction.isUsed = true;
                return keyAction;
            case KEY_CODE.BACK:
                keyAction.prevMenu = true;
                keyAction.isUsed = true;
                return keyAction;
            default:
                keyAction.prevMenu = false;
                keyAction.isUsed = false;
                return keyAction;
        }
    };

    function setScrollFocus(b){
        log.printDbg("setFocus()");
        if(b) {
            scroll.setFocus(true);
            isScrollArea = true;
        }
        else{
            scroll.setFocus(false);
            isScrollArea = false;
        }
    }

    function getMaxPage() {
        log.printDbg("getMaxPage()");
        return Math.ceil(menuList.length / NUMBER_OF_PREVIEW_ITEM);
    };

    function getCurrentPage() {
        log.printDbg("getCurrentPage()");
        return Math.floor(focusIdx / NUMBER_OF_PREVIEW_ITEM);
    };

    function setCurrentPage() {
        log.printDbg("setCurrentPage()");
        updatePreviewList();
        curPage = getCurrentPage();
        scroll.setCurrentPage(curPage);
    }
};