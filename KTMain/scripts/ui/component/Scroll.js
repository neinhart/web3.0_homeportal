/**
 *  Copyright (c) 2016 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>Scroll</code>
 *
 * Scroll component
 * @author dj.son
 * @since 2016-12-01
 */


(function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    function createElement(_this) {
        log.printDbg("createElement()");

        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_bg",
                css: {
                    position: "absolute", left: 0, top: 19, width: 2, height: (_this.height - 38),
                    "background-color": "rgba(255, 255, 255, 0.1)"
                }
            },
            parent: _this.parentDiv
        });

        // 원의 중심점을 기준으로 UI 작성
        var firstCircle = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_first_circle",
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible"
                }
            },
            parent: _this.parentDiv
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/scroll/scroll_dim_d.png",
                css: {
                    position: "absolute", left: -18, top: -19
                }
            },
            parent: firstCircle
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_b",
                css: {
                    position: "absolute", left: -19, top: -12, width: 38, "text-align": "center",
                    "font-size": 22, "color": "rgba(29, 27, 24, 1)"
                }
            },
            text: "1",
            parent: firstCircle
        });

        util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_dot_area",
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible"
                }
            },
            parent: _this.parentDiv
        });

        var lastCircle = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_last_circle",
                css: {
                    position: "absolute", left: 0, top: _this.height, overflow: "visible"
                }
            },
            parent: _this.parentDiv
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/scroll/scroll_dim_d.png",
                css: {
                    position: "absolute", left: -18, top: -19
                }
            },
            parent: lastCircle
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_b",
                css: {
                    position: "absolute", left: -19, top: -12, width: 38, "text-align": "center",
                    "font-size": 22, "letter-spacing": -1.5, "color": "rgba(29, 27, 24, 1)"
                }
            },
            parent: lastCircle
        });

        var focusArea = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_focus_area",
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible", "-webkit-transition": "-webkit-transform 0.5s"
                }
            },
            parent: _this.parentDiv
        });
        var selectCircle = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_select_circle",
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible"
                }
            },
            parent: focusArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/scroll/scroll_sel_d.png",
                css: {
                    position: "absolute", left: -18, top: -19
                }
            },
            parent: selectCircle
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_b",
                css: {
                    position: "absolute", left: -19, top: -12, width: 38, "text-align": "center",
                    "font-size": 22, "letter-spacing": -1.5, "color": "rgba(255, 255, 255, 1)"
                }
            },
            parent: selectCircle
        });
        var focusCircle = util.makeElement({
            tag: "<div />",
            attrs: {
                name: "scroll_focus_circle",
                css: {
                    position: "absolute", left: 0, top: 0, overflow: "visible"
                }
            },
            parent: focusArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "scroll_arr_top",
                src: "images/scroll/scroll_arr_top.png",
                css: {
                    position: "absolute", left: -9, top: -50
                }
            },
            parent: focusCircle
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/scroll/scroll_sel.png",
                css: {
                    position: "absolute", left: -35, top: -35
                }
            },
            parent: focusCircle
        });
        util.makeElement({
            tag: "<span />",
            attrs: {
                class: "font_b",
                css: {
                    position: "absolute", left: -35, top: -15, width: 70, "text-align": "center",
                    "font-size": 30, "letter-spacing": -1.5, "color": "rgba(29, 27, 24, 1)"
                }
            },
            parent: focusCircle
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "scroll_arr_btm",
                src: "images/scroll/scroll_arr_btm.png",
                css: {
                    position: "absolute", left: -9, top: 40
                }
            },
            parent: focusCircle
        });
    }

    function initScroll (_this) {
        log.printDbg("initScroll()");

        var dotArea = _this.parentDiv.find("[name=scroll_dot_area]");

        if (_this.maxPage < 2) {
            _this.parentDiv.css({visibility: "hidden"});
            return;
        }
        else {
            if (!_this.forceHeight) {
                if (_this.maxPage < 5) {
                    _this.height = 420;
                }
                else {
                    _this.height = 723;
                }
            }

            if (_this.parentDiv.css("visibility") === "hidden") {
                _this.parentDiv.css({visibility: ""});
            }
        }


        if(_this.isAudioFullEpg !== undefined && _this.isAudioFullEpg === true) {
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_select_circle] img").attr("src", "images/scroll/scroll_sel_d_music.png");
        }

        _this.parentDiv.find("[name=scroll_bg]").css({height: (_this.height - 38)});
        _this.parentDiv.find("[name=scroll_last_circle]").css({top: _this.height});


        _this.distance = Math.floor(_this.height / (_this.maxPage - 1));

        dotArea.children().remove();

        if (_this.maxPage > 2 && _this.maxPage < 10) {
            var dotNum = _this.maxPage - 2;

            for (var i = 0; i < dotNum; i++) {
                var top = (_this.distance * (i + 1)) - 19;
                util.makeElement({
                    tag: "<img />",
                    attrs: {
                        src: "images/scroll/dot.png",
                        css: {
                            position: "absolute", left: -18, top: top
                        }
                    },
                    parent: dotArea
                });
            }
        }

        if (_this.maxPage < 100) {
            _this.parentDiv.find("[name=scroll_last_circle] span").text(_this.maxPage).css({top: -12, "font-size": 22});
        }
        else if (_this.maxPage < 1000) {
            _this.parentDiv.find("[name=scroll_last_circle] span").text(_this.maxPage).css({top: -9, "font-size": 18});
        }
        else {
            _this.parentDiv.find("[name=scroll_last_circle] span").text(_this.maxPage).css({top: -7, "font-size": 13});
        }
    }

    function setCurrentScroll (_this) {
        log.printDbg("setCurrentScroll()");

        var top = _this.distance * _this.curPage;

        if (_this.distance < 38 && _this.curPage > 0) {
            var tmpDistance = (_this.height - 76) / (_this.maxPage - 3);
            top = 38 + (tmpDistance * (_this.curPage - 1));

            if (_this.curPage === _this.maxPage - 2) {
                top = _this.height - 38;
            }
        }

        if (_this.curPage === _this.maxPage - 1) {
            top = _this.height;
        }

        /**
         * [dj.son] translateZ 를 사용하면 browser 내부에서 다른 Layer 계층으로 렌더링 됨 (여기서 Layer 는 browser 에서 출력하는 화면 단위임
         *          포커스 영역과 전체 scroll 영역이 다른 Layer 계층으로 렌더링 되므로, 전체 판에 opacity 를 줘도 내부적으로는 각각 opacity 가 먹게 되고,
         *          결과적으로 투명도 때문에 이미지가 겹쳐서 보이는 이슈 있음;;; (WEBIIIHOME-2918)
         */
        //_this.parentDiv.find("[name=scroll_focus_area]").css({"-webkit-transform": "translate3d(0px, " + top + "px, 0px)"});
        _this.parentDiv.find("[name=scroll_focus_area]").css({"-webkit-transform": "translateY(" + top + "px)"});
        _this.parentDiv.find("[name=scroll_focus_area] span").text(_this.curPage + 1);

        if (_this.curPage + 1 < 100) {
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_select_circle] span").css({top: -12, "font-size": 22});
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_focus_circle] span").css({top: -15, "font-size": 30});
        }
        else if (_this.curPage + 1 < 1000) {
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_select_circle] span").css({top: -9, "font-size": 18});
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_focus_circle] span").css({top: -12, "font-size": 26});
        }
        else {
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_select_circle] span").css({top: -7, "font-size": 13});
            _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_focus_circle] span").css({top: -10, "font-size": 21});
        }

        //var firstCircle = _this.parentDiv.find("[name=scroll_first_circle]");
        //var lastCircle = _this.parentDiv.find("[name=scroll_last_circle]");
        var focusCircle = _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_focus_circle]");

        if (_this.bFocus) {
            focusCircle.css({visibility: "inherit"});
        }
        else {
            focusCircle.css({visibility: "hidden"});
        }

        if (_this.curPage === 0) {
            //firstCircle.css({visibility: "hidden"});
            //lastCircle.css({visibility: "inherit"});
            focusCircle.find("[name=scroll_arr_top]").css({visibility: "hidden"});
            focusCircle.find("[name=scroll_arr_btm]").css({visibility: "inherit"});
        }
        else if (_this.curPage === _this.maxPage - 1) {
            //firstCircle.css({visibility: "inherit"});
            //lastCircle.css({visibility: "hidden"});
            focusCircle.find("[name=scroll_arr_top]").css({visibility: "inherit"});
            focusCircle.find("[name=scroll_arr_btm]").css({visibility: "hidden"});
        }
        else {
            //firstCircle.css({visibility: "inherit"});
            //lastCircle.css({visibility: "inherit"});
            focusCircle.find("[name=scroll_arr_top]").css({visibility: "inherit"});
            focusCircle.find("[name=scroll_arr_btm]").css({visibility: "inherit"});
        }
    }

    function setFocusScroll (_this) {
        log.printDbg("setFocusScroll()");

        var focusCircle = _this.parentDiv.find("[name=scroll_focus_area] [name=scroll_focus_circle]");

        if (_this.bFocus) {
            _this.parentDiv.css({opacity: 1});

            _this.parentDiv.find("[name=scroll_first_circle] img, [name=scroll_last_circle] img").attr("src", "images/scroll/scroll_dim.png");
            _this.parentDiv.find("[name=scroll_first_circle] span, [name=scroll_last_circle] span").css({
                color: "rgba(29, 27, 24, 0.8)"
            });

            focusCircle.css({visibility: "inherit"});

            _this.parentDiv.find("[name=scroll_first_circle], [name=scroll_last_circle]").css({
                "-webkit-transition": "",
                opacity: 1
            });
        }
        else {
            _this.parentDiv.css({opacity: 0.5});

            _this.parentDiv.find("[name=scroll_first_circle] img, [name=scroll_last_circle] img").attr("src", "images/scroll/scroll_dim_d.png");
            _this.parentDiv.find("[name=scroll_first_circle] span, [name=scroll_last_circle] span").css({
                color: "rgba(255, 255, 255, 0.5)"
            });

            focusCircle.css({visibility: "hidden"});

            _this.parentDiv.find("[name=scroll_first_circle], [name=scroll_last_circle]").css({
                "-webkit-transition": "",
                opacity: 1
            });
        }
    }


    KTW.ui.component.Scroll = function (options) {
        this.maxPage = 0;
        this.curPage = 0;
        this.height = options.height;
        this.distance = 0;
        this.parentDiv = options.parentDiv;
        this.bFocus = false;
        this.forceHeight = options.forceHeight;
        this.isAudioFullEpg = options.isAudioFullEpg;

        this.parentDiv.css({display: "none"});

        createElement(this);
    };

    KTW.ui.component.Scroll.prototype.show = function (options) {
        this.maxPage = options.maxPage;
        this.curPage = options.curPage;
        this.bFocus = options.bFocus;

        if (this.maxPage <= 1) {
            this.parentDiv.css({display: "none"});
        }
        else {
            initScroll(this);
            setCurrentScroll(this);
            setFocusScroll(this);

            this.parentDiv.css({display: ""});
        }
    };

    KTW.ui.component.Scroll.prototype.hide = function () {
        this.parentDiv.css({display: "none"});
    };

    KTW.ui.component.Scroll.prototype.setCurrentPage = function (curPage) {
        if (curPage >= 0 && curPage < this.maxPage) {
            this.curPage = curPage;

            setCurrentScroll(this);
            setFocusScroll(this);
        }
    };

    KTW.ui.component.Scroll.prototype.setFocus = function (focus) {
        if ((this.bFocus && focus) || (!this.bFocus && !focus)) {
            return;
        }

        this.bFocus = focus;
        setFocusScroll(this);
    };
})();