/**
 *  Copyright (c) 2017 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */


/**
 * <code>Clock</code>
 *
 * @author sw.nam
 * @since 2017-04-28
 *
 * 채널가이드 & 설정 화면 우측 상단 clock component
 *
 */

(function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var clockArea;
    var clockTimer = null;

    function createElement(_this) {
        log.printDbg("createElement()");
        clockArea = _this.parentDiv;

        util.makeElement({
            tag: "<span />",
            attrs: {
                css: {
                    position: "absolute", left: 1658, top: 79, width: 100, height: 30,
                    "font-size": 24, "font-family": "RixHead L", "letter-spacing": -1.2, color: "rgba(255, 255, 255, 0.35)"
                }
            },
            text: "현재시간",
            parent: clockArea
        });


        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_hour_0",
                css: {
                    position: "absolute", left: 1752, top: 75
                }
            },
            parent: clockArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_hour_1",
                css: {
                    position: "absolute", left: 1773, top: 75
                }
            },
            parent: clockArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                src: "images/icon/timenumber/h_img_time.png",
                css: {
                    position: "absolute", left: 1797, top: 83
                }
            },
            parent: clockArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_min_0",
                css: {
                    position: "absolute", left: 1805, top: 75
                }
            },
            parent: clockArea
        });
        util.makeElement({
            tag: "<img />",
            attrs: {
                name: "clock_min_1",
                css: {
                    position: "absolute", left: 1827, top: 75
                }
            },
            parent: clockArea
        });

    }

    function startClock () {
        log.printDbg("startClock()");
        if (!clockTimer) {
            clockTimer = checkClock();
            updateClockUI();
            clockArea.css({visibility: "inherit"});
        }
    }

    function checkClock () {
        var now = new Date();
        var next = new Date(now.getTime() + (60 * 1000));
        next.setMilliseconds(0);
        next.setSeconds(0);

        var delayTime = next - now + 10;

        return setTimeout(function () {
            updateClockUI();
            clockTimer = checkClock();
        }, delayTime);
    }

    function updateClockUI () {
        var now = new Date();
        var hours = now.getHours();
        var min = now.getMinutes();

        clockArea.find("[name=clock_hour_0]").attr("src", "images/icon/timenumber/time_number_s_" + Math.floor(hours / 10) + ".png");
        clockArea.find("[name=clock_hour_1]").attr("src", "images/icon/timenumber/time_number_s_" + (hours % 10) + ".png");
        clockArea.find("[name=clock_min_0]").attr("src", "images/icon/timenumber/time_number_s_" + Math.floor(min / 10) + ".png");
        clockArea.find("[name=clock_min_1]").attr("src", "images/icon/timenumber/time_number_s_" + (min % 10) + ".png");
    }

    function stopClock () {
        log.printDbg("stopClock()");
        clearTimeout(clockTimer);
        clockTimer = null;

        clockArea.css({visibility: "hidden"});
    }

    KTW.ui.component.Clock = function (options) {
        this.parentDiv = options.parentDiv;
        createElement(this);
    };

    KTW.ui.component.Clock.prototype.show = function (parentDiv) {
        log.printDbg("show clock()");

        if(parentDiv) {
            clockArea = parentDiv;
            this.parentDiv = clockArea;
        }
        this.parentDiv.css({visibility: "inherit"});
        startClock();
    };

    KTW.ui.component.Clock.prototype.hide = function () {
        log.printDbg("hide Clock()");
        this.parentDiv.css({visibility: "hidden"});
        stopClock();
    };

})();