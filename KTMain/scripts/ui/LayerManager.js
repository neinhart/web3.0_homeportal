"use strict";


/**
 * manage layers
 *
 * [dj.son] Layer 관리 Manager
 *
 * - 오로지 Layer 만 관리
 * - 기존 service 관련 로직 (ReceiveMessage, other app state, get Cus info, key priority 설정 등) 은 AppServiceManager 로 옮김
 */
KTW.ui.LayerManager = (function () {

    var log = KTW.utils.Log;
    var util = KTW.utils.util;

    var layer_array = [];
    var layer_stack = [];
    var module_layer_array = [];

    var appServiceManager = null;
    var adapterHandler = null;
    var stateManager = null;
    var moduleManager = null;

    /**
     * [dj.son] 기존 모듈 로딩중임을 체크하기 위한 로직 제거 -> 더 이상 사용하지 않음
     */
        //var loading_layer = null;
        //var cancel_loading_layer = false;

    var layer_div = "#layer_area";

    var is_show_loading = false;

    var vboBackgroundArea = null;

    var changeLayerListener = new KTW.utils.Listener();

    /**
     * init LayerManager
     *
     * set key event listener
     * 사용하는 키 등록
     * BackgroundLayer, VideoBroadcastLayer 생성
     */
    /**
     * init LayerManager
     *
     * set key event listener
     * 사용하는 키 등록
     * BackgroundLayer, VideoBroadcastLayer 생성
     *
     * @private
     */
    function _init() {
        log.printDbg("LayerManager init()");

        vboBackgroundArea = $("#vboBackgroundArea");

        appServiceManager = KTW.managers.service.AppServiceManager;
        adapterHandler = KTW.oipf.AdapterHandler;
        stateManager = KTW.managers.service.StateManager;
        moduleManager = KTW.managers.module.ModuleManager;

        // add key listener
        adapterHandler.basicAdapter.addKeyListener(_onKeyDown);
        adapterHandler.basicAdapter.setKeyset(KTW.KEY_SET.NORMAL);

        // add state change event listener
        stateManager.addServiceStateListener(onServiceStateChange);

        //createMiniEpgLayer();

        /**
         * [dj.son]
         *
         * 배경 이미지 하나를 가지고 -webkit-clip-path 값을 변화시키는 것보다,
         * 각각의 경우에 대해 (최대 3가지 - none, default, full epg) element 생성 후 visibility 로 제어하는게 더 빠름
         * (특히 av resize 와 동시에 작업하는 경우, 이게 더 낫다고 판단됨)
         *
         * 어차피 element 가 여러개더라도 똑같은 이미지를 쓰기 때문에, 메모리상 부담은 적음 (element 가 2개 더 생기는 정도)
         *
         * [2017.03.22] clip-path 사용하는 대신 main vbo 아래에 div 를 두는 것이 성능상 더 좋다는 가이드를 받음
         * [2017.04.27] bg_default 는 공통으로 사용되기 떄문에 이곳에서 생성 (채널가이드, 설정 에서 공통으로 사용)
         */
        var bg = util.makeElement({
            tag: "<img />", attrs: {
                id: "bg_default", src: "images/bg_default.jpg", css: {
                    position: "absolute",
                    display: "none",
                    "-webkit-clip-path": "polygon(0px 0px, 1920px 0px, 1920px 148px, 1365px 148px, 1365px 464px, 1920px 464px, 1920px 1080px, 0px 1080px)"
                }
            }
        });
        KTW.ui.LayerManager.addVBOBackground({
            id: "bg_default", background: bg
        });
    }

    /**
     * create MiniEpgLayer
     */
    function _activateMiniEpg() {
        log.printDbg("LayerManager createMiniEpgLayer()");
        createLayer({
            obj: {
                id: KTW.ui.Layer.ID.MINI_EPG,
                type: KTW.ui.Layer.TYPE.TRIGGER,
                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                linkage: false
            }, cbCreate: function (layer) {
                //layer.show();
            }
        });
    }

    /**
     * create or show Layer
     *
     * @param options - obj             : Layer parameters
     *                 - clear_normal    : if true, hide all NORMAL & POPUP layer
     *                 - visible         : activate 하지만 show 하지 않는 경우에만 false
     *                 - new             : obj.id 의 Layer 가 존재하더라도, Layer 를 새로 생성 (new Layer)
     *                                     obj.id 값 뒤에 number 값을 붙인다. (ex: Detail, Detail_1, Detail_2, ...)
     *                 - cbActivate      : activate 이후 호출되는 callback
     *
     *                 ex)
     *                         options = {
     *                             obj: {
     *                                 id: "layer_id",
     *                                 type: KTW.ui.Layer.TYPE.NORMAL,
     *                                 priority: KTW.ui.Layer.PRIORITY.NORMAL,
     *                                 view: view,
     *                                 linkage: true,
     *                                 param: {
     *                                     data: data,
     *                                     ....
     *                                 }
     *                             },
     *                             moduleId: "module id",
     *                             clear_normal: false,
     *                             visible: true,
     *                             new : true,
     *                             cbActivate: function() {}
     *                         }
     *
     * @private
     */
    function _activateLayer(options) {
        if (!options || !options.obj) {
            return;
        }

        /**
         * [dj.son] 부팅 중 - 메뉴 데이터 받기 전이라면 메세지 띄우도록 수정
         */
        if (stateManager.appState < KTW.CONSTANT.APP_STATE.STARTED) {
            if (options.obj.type === KTW.ui.Layer.TYPE.NORMAL) {
                log.printDbg("appState state is initialized... & target layer is NORMAL... so return... " + stateManager.appState);
                return;
            }
        }

        options.cbCheckRequestShow = function (resMsg) {
            if (resMsg && resMsg.res !== "yes") {
                log.printDbg("resMsg.res : " + resMsg.res + "... so return");
                return;
            }

            log.printDbg("_activateLayer() " + log.stringify(options));
            log.printDbg("layer_stack size = " + layer_stack.length);

            var prev_layer;

            createLayer({
                obj: options.obj, moduleId: options.moduleId, new: options.new, cbCreate: function (layer) {
                    var isShow = false;

                    if (!layer) {
                        log.printDbg("_activateLayer() not valid layer... so return");
                        return;
                    }

                    if (options.clear_normal) {
                        // hide all normal & popup layer
                        _clearNormalLayer(true);

                        // [dj.son] [WEBIIIHOME-682] 현재 layer 가 hide 상태였을 경우 _clearNormalLayer 에 의해 다시 한번 hide 가 호출되는데, 이때 param 이 reset 되는 이슈가 존재
                        // clearNormalLayer 를 호출할 경우 한번 더 param 값 setting 하도록 수정
                        if (util.isValidVariable(layer) && options.obj.params) {
                            layer.setParams(options.obj.params);
                        }
                    }

                    if (options.visible) {

                        var same_layer = _getLastLayerInStack({
                            key: "id", value: options.obj.id
                        });

                        if (same_layer && same_layer == layer) {
                            removeStack(layer);
                        }

                        addStack(layer);

                        var index = _getLayerIndexInStack(layer);

                        /**
                         * TRIGGER 일 경우,
                         */
                        if (layer.type == KTW.ui.Layer.TYPE.TRIGGER) {
                            // if 현재 stack 상 최상위에 위치할 경우, show
                            // else 자신의 linkage 값과 현재 최상위 NORMAL Layer 의 linkage 값이 모두 true 일 경우 show
                            if (index == layer_stack.length - 1) {
                                isShow = true;
                            }
                            else {
                                var lastNormalLayer = _getLastLayerInStack({
                                    key: "type", value: KTW.ui.Layer.TYPE.NORMAL
                                });

                                if (!lastNormalLayer || !lastNormalLayer.isShowing()) {
                                    isShow = true;
                                }
                                else {
                                    if (lastNormalLayer && lastNormalLayer.linkage && layer.linkage) {
                                        isShow = true;
                                    }
                                }
                            }
                        }
                        /**
                         * NORMAL 인 경우
                         */
                        else if (layer.type == KTW.ui.Layer.TYPE.NORMAL) {
                            // POPUP Layer 제거
                            _clearPopupLayer();

                            // 이전 NORMAL Layer hide
                            var normal_stack = _getLayerStackByType(KTW.ui.Layer.TYPE.NORMAL);

                            var length = normal_stack.length;

                            for (var i = length - 2; i >= 0; i--) {
                                prev_layer = normal_stack[i];

                                if (prev_layer.isShowing()) {
                                    prev_layer.hide({pause: true});
                                }
                            }

                            var arrTrigger = _getLayerStackByType(KTW.ui.Layer.TYPE.TRIGGER);
                            for (var i = 0; i < arrTrigger.length; i++) {
                                if (layer.linkage && arrTrigger[i].linkage) {
                                    if (!arrTrigger[i].isShowing()) {
                                        arrTrigger[i].show({resume: true});
                                    }
                                }
                                else {
                                    if (arrTrigger[i].isShowing()) {
                                        arrTrigger[i].hide({pause: true});
                                    }
                                }
                            }

                            isShow = true;
                        }
                        /**
                         * POPUP 인 경우
                         */
                        else if (layer.type == KTW.ui.Layer.TYPE.POPUP) {
                            // 타겟 Layer 가 LOADING 일 경우, 이전 Layer 에 아무것도 안함
                            // TODO 추후 Loading 에 대한 동작 확인 필요
                            if (layer.id !== "LoadingLayer") {
                                // 이전 POPUP hide
                                // 바로 이전 POPUP 의 linkage 가 true 일 경우는 show
                                var popup_stack = _getLayerStackByType(KTW.ui.Layer.TYPE.POPUP);

                                var length = popup_stack.length;

                                for (var i = length - 2; i >= 0; i--) {
                                    prev_layer = popup_stack[i];

                                    /**
                                     * [dj.son] 로딩 Layer 가 show 중인 경우, prev_layer 가 타겟 Layer 로 설정되는 이슈 수정
                                     */
                                    if (prev_layer === layer) {
                                        continue;
                                    }

                                    /**
                                     * [dj.son] 기본적으로 팝업은 바로 이전 팝업의 중복만 허용했지만,
                                     * 시청알림예약 팝업 같은 경우 여러개의 팝업이 중복되어서 띄워져야 한다.
                                     * 따라서, allPopupDuplicate 옵션을 추가해 해당 옵션이 true 일 경우는 모든 팝업간의 중복을 검사한다.
                                     */
                                    if ((options.allPopupDuplicate || i == length - 2) && prev_layer.linkage) {
                                        if (!prev_layer.isShowing()) {
                                            prev_layer.show();
                                        }
                                    }
                                    else {
                                        prev_layer.hide({pause: true});
                                    }
                                }
                            }

                            isShow = true;
                        }
                        else {
                            isShow = true;
                        }
                    }
                    else {
                        log.printDbg("_activateLayer() only create stack layer... so return");
                    }

                    if (isShow) {
                        layer.show();

                        if (layer.type > KTW.ui.Layer.TYPE.BACKGROUND && layer.id !== KTW.ui.Layer.ID.LOADING && layer.id !== KTW.ui.Layer.ID.TV_PAY_DUMMY_PUPUP) {
                            notifyAppVisible();
                            setKeyPriority();
                        }
                    }

                    if (options.cbActivate) {
                        options.cbActivate(true);
                    }

                    notifyChangeLayer();
                }
            });
        };

        checkRequestShow(options);
    }

    function checkRequestShow(options) {
        log.printDbg("checkRequestShow() " + log.stringify(options));

        /**
         * [dj.son] 부팅시 홈포탈 UI 를 그리는 경우에는 requestShow 를 skip 하고 그냥 띄우도록 수정
         */
        if (stateManager.appState !== KTW.CONSTANT.APP_STATE.STARTED) {
            options.cbCheckRequestShow();
            return;
        }

        /**
         * skipRequestShow
         */
        if (options.skipRequestShow) {
            options.cbCheckRequestShow();
            return;
        }

        /**
         * BACKGROUND, TRIGGER type 은 requestShow 미호출
         */
        if (options.obj.type < KTW.ui.Layer.TYPE.NORMAL) {
            options.cbCheckRequestShow();
        }
        /**
         * POPUP type 이면서 priority 가 SYSTEM_POPUP 이상이면 requestShow 미호출
         */
        else if (options.obj.type >= KTW.ui.Layer.TYPE.NORMAL && options.obj.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
            options.cbCheckRequestShow();
        }
        else {
            /**
             * 현재 last layer 가 NORMAL 이상이고 show 중이면 requestShow 호출, 그 외에는 미호출
             */
            var lastLayer = _getLastLayerInStack();
            
            // 2018.03.19 TaeSang.
            // 외부앱에서 VOD상세화면 요청 시 로딩 레이어가 먼저 노출되고, AMOC 등 네트워크 통신 후에 상세화면이 노출되게 되는데
            // 이 때, 마지막 레이어가 로딩레이어 이므로 requestShow를 호출을 스킵함으로인해
            // 로딩 레이어 이후에 열린 상세화면이 (AppServiceManager에 의해) Hide 되는 이슈가 있었음 (WEBIIIHOME-3782)
            // 따라서 requestShow를 스킵하는 로직에 로딩 레이어는 제외 시킴
            if (lastLayer && lastLayer.id === KTW.ui.Layer.ID.LOADING) {
                if (layer_stack.length > 1) {
                    lastLayer = layer_stack[layer_stack.length - 2];
                }
                else {
                    lastLayer = null;
                }
            }

            if (lastLayer && lastLayer.type >= KTW.ui.Layer.TYPE.NORMAL && lastLayer.isShowing()) {
                options.cbCheckRequestShow();
            }
            else {
                sendRequestShow(function (resMsg) {
                    options.cbCheckRequestShow(resMsg);
                });
            }
        }
    }

    /**
     * hide & remove Layer
     *
     * 대상 Layer type 이 NORMAL 미만일 경우, 대상 Layer 만 hide
     * 대상 Layer type 이 NORMAL 이상일 경우, 상위 Layer 들도 hide or remove
     *
     * @param {Object} options id - Layer id
     *                          hideAll - if true, all same layer removed from stack
     *                          remove - if true, layer removed from dom tree and layer_array,
     *                          onlyTarget - if true, only target layer deactivated
     */
    function _deactivateLayer(options) {
        if (!options || !options.id) {
            return;
        }

        log.printDbg("_deactivateLayer(" + log.stringify(options) + ")");

        var layer = _getLastLayerInStack({key: "id", value: options.id});

        var top_layer = null;

        if (layer) {
            log.printDbg(layer);

            /**
             * 타겟 Layer type 이 BACKGROUND, TRIGGER 일 경우 타 Layer 에 영향없이 stack 에서 제거
             */
            if (layer.type === KTW.ui.Layer.TYPE.BACKGROUND || layer.type === KTW.ui.Layer.TYPE.TRIGGER) {
                options.onlyTarget = true;
            }

            if (options.onlyTarget) {
                layer.hide();

                removeStack(layer);

                if (layer.type == KTW.ui.Layer.TYPE.POPUP || options.remove || layer.bRemove) {
                    removeLayer(layer);
                }
            }
            else {
                /**
                 * 타겟 Layer type 이 Normal, Popup 일 경우 자신보다 상위 Layer 들을 전부 deactivate 하고,
                 * 바로 하위에 위치한 Layer show
                 *
                 * 특정 Layer 내부에서 다른 Layer 를 hide 하는 경우, index 가 꼬이는 이슈 발생 (PipLayer 등)
                 * 따라서 length 변화가 있을 경우에 대한 예외처리 추가
                 */

                var index = _getLayerIndexInStack(layer);
                var stackLength = layer_stack.length;

                for (var i = stackLength - 1; i >= 0; i--) {
                    if (stackLength !== layer_stack.length) {
                        stackLength = layer_stack.length;
                        index = _getLayerIndexInStack(layer);
                    }

                    if (i <= index) {
                        break;
                    }

                    top_layer = layer_stack[i];

                    log.printDbg(top_layer);

                    if (top_layer) {
                        if (top_layer.priority < KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                            if (top_layer.isShowing()) {
                                top_layer.hide();
                            }

                            removeStack(top_layer);

                            if (top_layer.type == KTW.ui.Layer.TYPE.POPUP || top_layer.bRemove === true) {
                                removeLayer(top_layer);
                            }
                        }
                    }
                }

                layer.hide();

                removeStack(layer);

                if (layer.type == KTW.ui.Layer.TYPE.POPUP || options.remove || layer.bRemove) {
                    removeLayer(layer);
                }

                top_layer = _getLastLayerInStack();
                if (top_layer && top_layer.id === KTW.ui.Layer.ID.LOADING) {
                    top_layer = layer_stack[layer_stack.length - 2];
                }
                var arrTrigger = _getLayerStackByType(KTW.ui.Layer.TYPE.TRIGGER);

                // [dj.son] deactivate 시, top_layer 에 대한 다음 동작을 type 별로 세분화해서 적용하도록 수정
                // top_layer 가 BACKGROUND 일 경우, 아무것도 안함
                if (top_layer && top_layer.type > KTW.ui.Layer.TYPE.BACKGROUND) {
                    // top_layer 가 TRIGGER 일 경우, 모든 TRIGGER show (TRIGGER 는 NORMAL layer 가 있을때만 linkage 검사)
                    if (top_layer.type < KTW.ui.Layer.TYPE.NORMAL) {
                        // 현재 show 된 Layer 가 없으므로 stack 에 있는 trigger 검사 후 Show
                        for (var i = 0; i < arrTrigger.length; i++) {
                            if (!arrTrigger[i].isShowing()) {
                                arrTrigger[i].show({resume: true});
                            }
                        }
                    }
                    else {
                        // deactivate layer 가 POPUP 이고 top_layer 가 POPUP 일 경우, top_layer show (POPUP 은 같은 POPUP 끼리만 영향을 미치므로)
                        if (layer.type === KTW.ui.Layer.TYPE.POPUP && top_layer.type === KTW.ui.Layer.TYPE.POPUP) {
                            if (!top_layer.isShowing()) {
                                top_layer.show({resume: true});
                            }
                        }
                        // deactivate layer 가 NORMAL 이고 top_layer 가 NORMAL 일 경우
                        // top_layer show && 모든 TRIGGER 의 linkage 검사 후 TRIGGER show/hide
                        else if (layer.type === KTW.ui.Layer.TYPE.NORMAL && top_layer.type === KTW.ui.Layer.TYPE.NORMAL) {
                            if (!top_layer.isShowing()) {
                                top_layer.show({resume: true});
                            }

                            for (var i = 0; i < arrTrigger.length; i++) {
                                if (top_layer.linkage && arrTrigger[i].linkage) {
                                    if (!arrTrigger[i].isShowing()) {
                                        arrTrigger[i].show({resume: true});
                                    }
                                }
                                else {
                                    if (arrTrigger[i].isShowing()) {
                                        arrTrigger[i].hide({pause: true});
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (layer.type >= KTW.ui.Layer.TYPE.NORMAL) {
                /**
                 * [dj.son] [WEBIIIHOME-2692] Loading 이 떠 있는 상황에서 모든 하위 Normal Layer 가 deactivate 되었을 경우, AV resize 를 안하는 이슈 수정
                 * - 현재 top_layer 가 로딩일 경우, stack 상 그 다음에 있는 Layer 를 top_layer 로 설정
                 */
                top_layer = _getLastLayerInStack();
                if (top_layer && top_layer.id === KTW.ui.Layer.ID.LOADING) {
                    var topIndex = _getLayerIndexInStack(top_layer);
                    top_layer = layer_stack[topIndex - 1];
                }

                var isHide = true;

                /**
                 * [dj.son] layer stack 에 hide 된 popup 이 남아있을때를 대비한 예외처리 추가
                 */
                if (top_layer && top_layer.type >= KTW.ui.Layer.TYPE.NORMAL) {
                    if (top_layer.type === KTW.ui.Layer.TYPE.NORMAL) {
                        if (top_layer.isShowing()) {
                            isHide = false;
                        }
                    }
                    else {
                        for (var i = layer_stack.length - 1; i >= 0; i--) {
                            var tempLayer = layer_stack[i];

                            if (tempLayer.type < KTW.ui.Layer.TYPE.NORMAL) {
                                continue;
                            }

                            if (tempLayer.id === KTW.ui.Layer.ID.LOADING) {
                                continue;
                            }

                            if (tempLayer.isShowing()) {
                                isHide = false;
                                break;
                            }
                        }
                    }
                }

                if (isHide) {
                    /**
                     * [dj.son] [WEBIIIHOME-3593] 다른 앱이 떠 있는 상황이면, resize 하지 않도록 예외처리 추가
                     * 2018.01.04 [sw.nam] [WEBIIIHOME-3593] 원복되었던 이슈 다시 적용
                     */
                    if (!KTW.managers.service.StateManager.isOtherAppState()) {
                        KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);
                        KTW.managers.service.IframeManager.changeIframe();
                    }

                    _hideVBOBackground();

                    if (!options.blockSetPriority && layer.id !== KTW.ui.Layer.ID.LOADING) {
                        notifyAppVisible();
                    }
                }

                if (!options.blockSetPriority && layer.id !== KTW.ui.Layer.ID.LOADING) {
                    setKeyPriority();
                }
            }
            else {
                if (!options.blockSetPriority && layer.id === KTW.ui.Layer.ID.MINI_EPG) {
                    notifyAppVisible();
                    setKeyPriority();
                }
            }

            notifyChangeLayer();
        }
        else {
            layer = _getLayer(options.id);

            if (options.remove || (layer && layer.bRemove)) {
                if (layer) {
                    removeLayer(layer);
                }
            }
        }
    }

    function notifyAppVisible() {
        log.printDbg("notifyAppVisible()");

        var length = layer_stack.length;
        var layer = null;
        var preLayer = null;

        for (var i = length - 1; i >= 0; i--) {
            layer = layer_stack[i];

            /**
             * [dj.son] app visible 판단시, LOADING 은 조건에서 제외하도록 수정
             */
            if (layer && layer.isShowing() && layer.id !== KTW.ui.Layer.ID.LOADING && layer.id !== KTW.ui.Layer.ID.TV_PAY_DUMMY_PUPUP) {
                if (i > 0) {
                    preLayer = layer_stack[i - 1];
                }
                break;
            }

            layer = null;
        }

        if (layer) {
            if (layer.type >= KTW.ui.Layer.TYPE.NORMAL) {
                if (!preLayer || preLayer.type < KTW.ui.Layer.TYPE.NORMAL) {
                    sendRequestShow();
                }
            }
            else {
                sendNotifyHide();
            }
        }
        else {
            sendNotifyHide();
        }
    }

    function sendRequestShow(cbSendRequestShow) {
        KTW.managers.MessageManager.sendMessage({
            method: KTW.managers.MessageManager.METHOD.OBS.REQUEST_SHOW,
            from: KTW.CONSTANT.APP_ID.HOME,
            to: KTW.CONSTANT.APP_ID.OBSERVER
        }, function (resMsg) {
            /**
             * [dj.son] [WEBIIIHOME-2253] 다른 앱이 종료될때 hide 관련 메세지가 오기 전에 requestShow 를 요청하면 옵저버 로직에 따라 hide 메세지가 아예 안오고
             * 이 때문에 serviceState 가 변경이 되지 않아 다시 changeService 호출시, 같은 serviceState 라 아무런 동작을 안하는 이슈가 있음
             * - OTHER APP 이 떠 있는 상황에서 requestShow 메세지를 호출할 경우, 응답값이 yes 이면 TV_VIEWING or VOD 상태로 changeService 호출하도록 수정
             */
            if (resMsg && resMsg.res === "yes") {
                if (stateManager.isOtherAppState()) {
                    var changeOptions = {
                        nextServiceState: KTW.CONSTANT.SERVICE_STATE.TV_VIEWING, obj: {
                            channel: KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel(), tune: false
                        }
                    };

                    if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD) {
                        changeOptions.nextServiceState = KTW.CONSTANT.SERVICE_STATE.VOD;
                    }

                    appServiceManager.changeService(changeOptions);
                }
            }

            if (cbSendRequestShow) {
                cbSendRequestShow(resMsg);
            }
        });
    }

    function sendNotifyHide(cbSendNotifyHide) {
        KTW.managers.MessageManager.sendMessage({
            method: KTW.managers.MessageManager.METHOD.OBS.NOTIFY_HIDE,
            from: KTW.CONSTANT.APP_ID.HOME,
            to: KTW.CONSTANT.APP_ID.OBSERVER
        }, cbSendNotifyHide);
    }

    /**
     * set priority - 2.0 소스 참조
     */
    function setKeyPriority() {

        var length = layer_stack.length;
        var layer = null;
        var preLayer = null;
        var options = {
            priority: appServiceManager.KEY_PRIORITY.HIDE
        };

        for (var i = length - 1; i >= 0; i--) {
            layer = layer_stack[i];

            if (layer && layer.isShowing() && layer.id !== KTW.ui.Layer.ID.LOADING && layer.id !== KTW.ui.Layer.ID.TV_PAY_DUMMY_PUPUP) {
                if (i > 0) {
                    preLayer = layer_stack[i - 1];
                }
                break;
            }

            layer = null;
        }

        if (stateManager.isVODPlayingState()) {
            options.priority = appServiceManager.KEY_PRIORITY.VOD_HIDE;
        }

        if (layer) {
            if (layer.type >= KTW.ui.Layer.TYPE.NORMAL) {
                /**
                 * [dj.son] 팝업이 show 되는 경우 activateInpu 은 false 로 호출하도록 수정 (2.0 로직 따름)
                 */
                if (layer.priority >= KTW.ui.Layer.PRIORITY.POPUP) {
                    options.focus = false;
                }

                if (layer.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                    options.priority = appServiceManager.KEY_PRIORITY.SYSTEM_POPUP;

                    // [dj.son] 2.0 에서는 아래와 같은 이슈로 인해 OTS Network 오류 팝업 띄울때 windwoPriority 를 1 로 함
                    // TODO UI 가 달라지므로 아래와 같은 이슈가 발생될건지 아닌지는 확인 필요

                    // OTS STB에서 network error popup이 특정 stack layer 상위 뜰 경우
                    // windowPriority 값이 "99"로 설정되면서
                    // 단선 icon이 보이지 않는 문제가 있음
                    // 그래서 이 경우에는 windowPiority를 "1"로 낮추어서
                    // 단선 icon과 함께 노출하도록 함 [KTWEB-25]
                    //if (OTW.CONSTANT.IS_OTS === true &&
                    //    layer.child instanceof OTW.ui.popup.NetworkErrorDialog) {
                    //    if (_getLastStackLayer() !== null) {
                    //        window_priority = KEY_PRIORITY.POPUP;
                    //    }
                    //}
                }
                else if (layer.priority >= KTW.ui.Layer.PRIORITY.PURCHASE_POPUP) {
                    // TV_VIEWING 상태에서 구매 popup의 경우 priority "1"로 처리함
                    // 보통 STACK layer 상위에서 동작하지만
                    // OTM 이나 북마크 연동 시 AV 상태에서 노출될 수 있기 때문에
                    // "1"로 처리되도록 함
                    if (stateManager.isTVViewingState()) {
                        options.priority = appServiceManager.KEY_PRIORITY.POPUP;
                    }
                    else if (stateManager.isVODPlayingState()) {
                        // [dj.son] VOD 재생 상태 중 타 앱 실행중일때, PURCHASE_POPUP 일 경우는 타 앱 상위에 띄우도록 함
                        // TODO 확인 요망 구역
                        if (stateManager.isOtherAppState()) {
                            options.priority = appServiceManager.KEY_PRIORITY.VOD_POPUP;
                        }
                        else {
                            options.priority = appServiceManager.KEY_PRIORITY.SHOW;
                        }

                        //// VOD 로딩 상태에서 구매 POPUP 노출되는 경우(몰아보기 및 연속 보기 등)
                        //// VOD POPUP priority - 9 로 설정 (앱스토어, 위젯 key 등의 hot 키를 막기 위해서)
                        //// VOD 상태에서 기본 key priority가 "0" 상태이므로
                        //// 일반적으로 SHOW/HIDE 되는 경우 KEY_PRIORITY.SHOW 값으로 처리함
                        //// APP Store 가 떠 있을때 vodExitPopup 뜰때/사라질때 처리  때문에 조건 추가.
                        //if(show === false && is_show_other_app === true) {
                        //    // 만약 otherApp이 노출된 상태인데 hide 되는 경우라면
                        //    // 의도적으로 keyPriority를 "-1"로 설정한다.
                        //    // otherApp이 일차적으로 key를 사용하도록 하고
                        //    // bypass 되는 key 중 channel up/down과 같은 것을
                        //    // 직접 처리하기 위해서이다.
                        //    priority = KEY_PRIORITY.VOD_HIDE;
                        //} else if (show === true && is_show_other_app === true) {
                        //    priority = KEY_PRIORITY.VOD_POPUP;
                        //} else {
                        //    priority = KEY_PRIORITY.SHOW;
                        //}
                        //
                        //if (show === true && OTW.vod.VodInterface.onNextPlayBlock === true) {
                        //    priority = KEY_PRIORITY.VOD_POPUP;
                        //}
                    }
                }
                else if (layer.priority >= KTW.ui.Layer.PRIORITY.REMIND_POPUP) {
                    options.priority = appServiceManager.KEY_PRIORITY.REMIND_POPUP;

                    // TODO 확인 요망 구역

                    //priority = KEY_PRIORITY.HIDE;
                    //if (show === true) {
                    //    priority = KEY_PRIORITY.REMIND_POPUP;
                    //
                    //    // VOD 이어보기 상태라면 priority를 "9"로 처리하여
                    //    // 핫키가 수행되지 않도록 함
                    //    // 단, appstore 및 선호채널과 같이 직접 KeyEvent를 받지 못하는 경우는
                    //    // 핫키 동작이 수행한 이후 다시 이전 상태로 복구하는 시나리오로 처리해야 함
                    //    if (OTW.vod.VodInterface.onNextPlayBlock === true) {
                    //        priority = KEY_PRIORITY.VOD_POPUP;
                    //    }
                    //}
                }
                else if (layer.priority >= KTW.ui.Layer.PRIORITY.POPUP) {
                    options.priority = appServiceManager.KEY_PRIORITY.POPUP;

                    if (stateManager.isVODPlayingState()) {
                        // [dj.son] VOD 재생 상태 중 타 앱 실행중일때, PURCHASE_POPUP 일 경우는 타 앱 상위에 띄우도록 함
                        // TODO 확인 요망 구역
                        if (stateManager.isOtherAppState()) {
                            options.priority = appServiceManager.KEY_PRIORITY.VOD_POPUP;
                        }
                    }

                    //// VOD 로딩 상태에서 POPUP이 노출되는 경우(몰아보기 및 연속 보기 등)
                    //// VOD POPUP priority - 9 로 설정 (앱스토어, 위젯 key 등의 hot 키를 막기 위해서)
                    //// VOD 상태에서 popup이 닫히는 경우 key priority를 HIDE 로 변경
                    //if (stateManager.isVODPlayingState() === true) {
                    //    priority = KEY_PRIORITY.HIDE;
                    //    if (show === true) {
                    //        if (OTW.vod.VodInterface.onNextPlayBlock === true) {
                    //            priority = KEY_PRIORITY.VOD_POPUP;
                    //        }
                    //        else if (is_show_other_app === true) {
                    //            // other App이 떠 있는 경우에는 key priority를 VOD POPUP priority로 설정
                    //            priority = KEY_PRIORITY.VOD_POPUP;
                    //        }
                    //    }
                    //}
                    //else if (stateManager.isTVViewingState() === true) {
                    //    priority = KEY_PRIORITY.HIDE;
                    //    if (show === true) {
                    //        // 연동형 app 등 과의 key 처리 동작을 위해서
                    //        // keyPriority 값을 "1"로 처리하도록 함
                    //        // 단, VOD 상태에서는 이미 popup 이 "9"로 처리되기 때문에
                    //        // TV_VIEWING 상태에서만 처리되도록 함
                    //        priority = KEY_PRIORITY.POPUP;
                    //    }
                    //}
                    //
                    //// miniEPG context와 연동형 app이 겹쳐지지 않도록 하기 위해
                    //// 의도적으로 requestShow를 호출하기 때문에
                    //// 연관메뉴가 닫힐 때 의도적으로 notifyHide가 불려지도록
                    //// msg_type 값을 변경한다.
                    //// 어차피 이 시점에는 다른 reqShow에 의해서 뜨는 UI와
                    //// 겹쳐지지 않기 때문에 (miniEPG는 normal 형태로 노출됨)
                    //// miniEPG 연관메뉴 hide 처리시 무조건 호출하면 된다.
                    //// 추가로 VOD 상태에서 연관메뉴의 경우에는
                    //// otherApp이 노출될 수 있는 상황을 고려해서 requestShow를 호출하기 때문에
                    //// 동일하게 notifyHide가 처리되도록 한다. (KTWHPTZOF-1659)
                    //if (layer.id === OTW.ui.Layer.ID.MINI_EPG_CONTEXT ||
                    //    layer.id === OTW.vod.Def.VIEW_ID.CONTEXT_MENU_POPUP_ID) {
                    //    if (show === false) {
                    //        msg_type = OBS_MSG_TYPE.HIDE_FORCED;
                    //    }
                    //}
                }
                else {
                    options.priority = appServiceManager.KEY_PRIORITY.SHOW;
                }
            }
            else {
                // 미니 epg 일 경우, 숫자키 먹어야 할 경우도 있으므로 priority 를 SHOW 로 설정
                if (layer.id === KTW.ui.Layer.ID.MINI_EPG) {
                    options.priority = appServiceManager.KEY_PRIORITY.SHOW;
                    options.force = true;
                }
                else {
                    /**
                     * [dj.son] Trigger 들이 show 상태일때, 그 중 미니 epg 가 show 상태일 경우에도 priority 를 SHOW 로 설정하도록 수정
                     */
                    var arrTrigger = _getLayerStackByType(KTW.ui.Layer.TYPE.TRIGGER);
                    if (arrTrigger && arrTrigger.length > 0) {
                        for (var i = 0; i < arrTrigger.length; i++) {
                            var trigger = arrTrigger[i];
                            if (trigger.id === KTW.ui.Layer.ID.MINI_EPG && trigger.isShowing()) {
                                options.priority = appServiceManager.KEY_PRIORITY.SHOW;
                                options.force = true;
                            }
                        }
                    }
                }
            }
        }

        appServiceManager.setKeyPriority(options);
    }

    function _clearPopupLayer() {

        var layer = null;

        for (var i = layer_array.length - 1; i >= 0; i--) {
            layer = layer_array[i];

            if (layer && layer.type === KTW.ui.Layer.TYPE.POPUP && layer.priority < KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                var index = layer_stack.indexOf(layer);
                if (index > -1) {
                    layer_stack.splice(index, 1);
                }

                layer.hide();

                removeLayer(layer);
            }
        }
    }

    /**
     * go back stack layer
     */
    function _historyBack() {
        var cur_layer = null;

        /**
         * [dj.son] 현재 top layer 가 hide 상태면 deactivate 하도록 예외처리 추가
         *
         * - [WEBIIIHOME-3868] Loading 에 대한 예외처리 추가
         */
        for (var i = layer_stack.length - 1; i >= 0; i--) {
            cur_layer = layer_stack[i];

            if (cur_layer) {
                if (!cur_layer.isShowing()) {
                    _deactivateLayer({
                        id: cur_layer.id
                    });
                }
                else {
                    if (cur_layer.id !== KTW.ui.Layer.ID.LOADING) {
                        break;
                    }
                }
            }
            else {
                break;
            }
        }

        log.printDbg("_historyBack() cur_layer = " + cur_layer);

        if (cur_layer) {
            _deactivateLayer({
                id: cur_layer.id
            });
        }
    }

    /**
     * get layer index in stack
     *
     * @param layer
     * @returns index
     * @private
     */
    function _getLayerIndexInStack(layer) {
        var index = -1;

        for (var i = layer_stack.length - 1; i >= 0; i--) {
            if (layer == layer_stack[i]) {
                index = i;
                break;
            }
        }

        return index;
    }

    /**
     * return layer stack by type
     *
     * @param type layer type
     * @returns {Layer Stack Array}
     * @private
     */
    function _getLayerStackByType(type) {
        var startIndex = -1, endIndex = -1;

        var length = layer_stack.length;

        for (var i = 0; i < length; i++) {
            if (layer_stack[i].type == type) {
                startIndex = i;
                break;
            }
        }

        for (var i = length - 1; i >= 0; i--) {
            if (layer_stack[i].type == type) {
                endIndex = i;
                break;
            }
        }

        if (startIndex > -1 && endIndex > -1) {
            return layer_stack.slice(startIndex, endIndex + 1);
        }

        return [];
    }

    /**
     * return last layer in stack
     *
     * @param {Object} options if not exist, return last layer in stack
     *                         if exist key and value, return last layer in stack by key
     *                         ex)
     *                         options = {
     *                             key: "type",
     *                             value: KTW.ui.Layer.TYPE.NORMAL
     *                         }
     * @return {Layer}
     */
    function _getLastLayerInStack(options) {
        var layer = null, temp_layer;

        if (!options || !options.key || !options.value) {
            layer = layer_stack[layer_stack.length - 1];
        }
        else {
            for (var i = layer_stack.length - 1; i >= 0; i--) {
                temp_layer = layer_stack[i];
                if (temp_layer[options.key] == options.value) {
                    layer = layer_stack[i];
                    break;
                }
            }
        }

        return layer;
    }

    /**
     * add layer in stack
     *
     * @param layer - Layer
     */
    function addStack(layer) {
        log.printDbg("addStack(" + layer + ")");

        var index = 0;

        for (var i = layer_stack.length - 1; i >= 0; i--) {
            if (layer.type === layer_stack[i].type && layer.priority >= layer_stack[i].priority) {
                index = i + 1;
                break;
            }
            else if (layer.type > layer_stack[i].type) {
                index = i + 1;
                break;
            }
        }

        layer_stack.splice(index, 0, layer);

        log.printDbg("layer_stack size = " + layer_stack.length);
    }

    /**
     * remove layer in stack
     *
     * @param layer - Layer
     */
    function removeStack(layer) {
        log.printDbg("removeStack(" + layer + ")");

        if (layer) {
            var index = _getLayerIndexInStack(layer);

            if (index > -1) {
                layer_stack.splice(index, 1);
            }
        }
    }

    /**
     * hide & remain NORMAL Layer in Stack & remove POPUP layer
     */
    function _hideNormalLayer() {
        log.printDbg("_hideNormalLayer()");

        var isHide = false;

        _clearPopupLayer();

        for (var i = layer_stack.length - 1; i > -1; i--) {
            var layer = layer_stack[i];

            if (layer.type < KTW.ui.Layer.TYPE.NORMAL) {
                break;
            }
            else {
                if (layer.priority < KTW.ui.Layer.PRIORITY.SYSTEM_POPUP && layer.isShowing()) {
                    layer.hide({pause: true});
                    isHide = true;
                }
            }
        }

        /**
         * [dj.son] [WEBIIIHOME-460] hideNormalLayer 호출시 AV resize 안하는 이슈 수정
         */
        if (isHide) {
            KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);
            KTW.managers.service.IframeManager.changeIframe();

            _hideVBOBackground();

            /**
             * [dj.son] [WEBIIIHOME-1663] obs_hide 에서 무조건 HIDE 로 priority 를 설정해서 생긴 이슈 수정
             * - LayerManager 에서 key priority 를 처리하도록 hideNormalLayer 내에 key priority 설정 함수 호출하도록 수정
             */
            setKeyPriority();
        }
    }

    function _resumeNormalLayer(options) {
        log.printDbg("_resumeNormalLayer() " + log.stringify(options));

        /**
         * [dj.son] [WEBIIIHOME-3587] 기가지니에서 VOD 재생중 안드로이드 앱 실행하면 VOD 종료에 의한 resume 처리 때문에 requestShow 가 불리게 되는데,
         *                            이때 requestShow 가 android state change 이벤트보다 늦게 와 동작이 꼬이는 이슈가 발생함...
         *                            따라서, VOD -> TV_VIEWING 상태로 전환시 resume 수행할때, 먼저 resume 을 수행하고 requestShow 를 나중에 호출하도록 수정
         *                            이렇게 해도 문제 없는 이유는 VOD -> TV_VIEWING 상태로 전환시 반드시 채널 튠을 수행하기 때문에
         *                            다른 앱이 떠 있는 상황이 있을 수가 없기 때문
         *                            물론 VOD 재생중 양방향 채널, 풀 브라우저 실행 등을 수행할 수 있지만
         *                            이 경우 OTHER_APP 상태로 빠지고 다른 로직을 수행하기 때문에 문제 없음
         */

        var layer = _getLastLayerInStack({
            key: "type", value: KTW.ui.Layer.TYPE.NORMAL
        });

        if (layer) {
            if (!options || (options && !options.skipRequestShow && !options.delayRequestShow)) {
                checkRequestShow({
                    obj: {
                        type: layer.type, priority: layer.priority
                    }, cbCheckRequestShow: function (resMsg) {
                        if (!resMsg || resMsg.res === "yes") {
                            layer.show({resume: true});

                            if (layer.type === KTW.ui.Layer.TYPE.NORMAL) {
                                var arrTrigger = _getLayerStackByType(KTW.ui.Layer.TYPE.TRIGGER);

                                for (var i = 0; i < arrTrigger.length; i++) {
                                    var trigger = arrTrigger[i];
                                    if (layer.linkage && trigger.linkage && !trigger.isShowing()) {
                                        trigger.show({resume: true});
                                    }
                                    else if (trigger.isShowing() && (!layer.linkage || !trigger.linkage)) {
                                        trigger.hide({pause: true});
                                    }
                                }
                            }

                            setKeyPriority();
                        }
                    }
                });
            }
            else {
                layer.show({resume: true});

                if (layer.type === KTW.ui.Layer.TYPE.NORMAL) {
                    var arrTrigger = _getLayerStackByType(KTW.ui.Layer.TYPE.TRIGGER);

                    for (var i = 0; i < arrTrigger.length; i++) {
                        var trigger = arrTrigger[i];
                        if (layer.linkage && trigger.linkage && !trigger.isShowing()) {
                            trigger.show({resume: true});
                        }
                        else if (trigger.isShowing() && (!layer.linkage || !trigger.linkage)) {
                            trigger.hide({pause: true});
                        }
                    }
                }

                setKeyPriority();

                if (options.delayRequestShow) {
                    sendRequestShow();
                }
            }
        }
    }

    /**
     * hide & remove NORMAL Layer in Stack & remove POPUP layer
     *
     * [dj.son] stack 의 첫번째 NORMAL Layer 를 deactivate 처리 -> 상위의 Layer 들은 전부 deactivate 처리됨
     */
    function _clearNormalLayer(blockSetPriority) {
        log.printDbg("_clearNormalLayer()");

        _clearPopupLayer();

        var normal_stack = _getLayerStackByType(KTW.ui.Layer.TYPE.NORMAL);

        if (normal_stack.length > 0) {
            var firstNormalLayer = normal_stack[0];
            _deactivateLayer({
                id: firstNormalLayer.id, blockSetPriority: blockSetPriority
            });
        }


        //for (var i = length - 1; i > -1; i--) {
        //    var layer = layer_stack[i];
        //
        //    if (layer.type < KTW.ui.Layer.TYPE.NORMAL) {
        //        break;
        //    }
        //    else {
        //        if (layer.isShowing()) {
        //            layer.hide();
        //        }
        //
        //        removeStack(layer);
        //    }
        //}
    }

    /**
     * remove all System Popup Layer, only use on STANDBY service state
     */
    function clearSystemPopup(arrSkipLayerId) {
        log.printDbg("clearSystemPopup()");

        for (var i = layer_stack.length - 1; i > -1; i--) {
            var layer = layer_stack[i];
            var bSkip = false;

            if (layer.priority >= KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                if (arrSkipLayerId && arrSkipLayerId.length > 0) {
                    for (var j = 0; j < arrSkipLayerId.length; j++) {
                        if (arrSkipLayerId[j] === layer.id) {
                            bSkip = true;
                            break;
                        }
                    }
                }

                if (!bSkip) {
                    _deactivateLayer({
                        id: layer.id
                    });
                }
            }
            else {
                break;
            }
        }
    }

    /**
     * hide & remove NORMAL Layer in Stack without param layer
     *
     * [dj.son] [WEBIIIHOME-2976] parameter 로 넘어온 Layer id 들을 제외하고, 나머지 NORMAL, POPUP Layer 들을 deactivate 처리
     */
    function _clearNormalLayerExceptTarget(arrTargetLayerId) {
        log.printDbg("_clearNormalLayerExceptTarget()");

        arrTargetLayerId = arrTargetLayerId || [];

        for (var i = layer_stack.length - 1; i >= 0; i--) {
            var layer = layer_stack[i];
            var bRemove = true;

            /**
             * [dj.son] [WEBIIIHOME-3840] 장르별 채널의 경우 PIP Layer 까지 deactivate 하기 때문에 layer stack 이 변할 수 있음
             * 
             * - 문제가 되는 breack 문을 굳이 유지하지 않아도 되므로 삭제
             */
            if (layer && layer.type > KTW.ui.Layer.TYPE.TRIGGER && layer.priority < KTW.ui.Layer.PRIORITY.SYSTEM_POPUP) {
                for (var j = 0; j < arrTargetLayerId.length; j++) {
                    if (layer.id === arrTargetLayerId[j]) {
                        bRemove = false;
                        break;
                    }
                }

                if (bRemove) {
                    var index = layer_stack.indexOf(layer);
                    if (index > -1) {
                        layer_stack.splice(index, 1);
                    }

                    layer.hide();

                    if (layer.type == KTW.ui.Layer.TYPE.POPUP || layer.bRemove) {
                        removeLayer(layer);
                    }
                }
            }
        }

        var normal_stack = _getLayerStackByType(KTW.ui.Layer.TYPE.NORMAL);

        if (normal_stack && normal_stack.length > 0) {
            var topNormalLayer = normal_stack[normal_stack.length - 1];

            if (!topNormalLayer.isShowing()) {
                topNormalLayer.show({resume: true});
            }
        }
        else {
            KTW.oipf.AdapterHandler.basicAdapter.resizeScreen(0, 0, KTW.CONSTANT.RESOLUTION.WIDTH, KTW.CONSTANT.RESOLUTION.HEIGHT);
            KTW.managers.service.IframeManager.changeIframe();

            _hideVBOBackground();

            setKeyPriority();
        }
    }

    /**
     * create Layer
     *
     * @param  {Object} options - obj      - layer parameters (see _activateLayer)
     *                          - cbCreate - create callback
     *
     */
    function createLayer(options) {
        log.printDbg("createLayer()");

        //if (loading_layer) {
        //    log.printDbg("already trying activateLayer " + options.obj.id);
        //
        //    if (loading_layer.id == options.obj.id) {
        //        cancel_loading_layer = false;
        //    }
        //
        //    return;
        //}

        var layer = _getLayer(options.obj.id);
        var layerId = options.obj.id;
        var LayerClass = null;
        var isModuleLayer = false;

        if (options.new) {
            layerId = addNumberOnLayerId(layerId);

            if (layerId !== options.obj.id) {
                layer = null;
            }
        }

        if (!layer) {
            // 모듈 로딩
            if (util.isValidVariable(options.obj, ["id", "type", "priority"])) {
                if (options.moduleId && options.moduleId.length > 0) {
                    var module = moduleManager.getModule(options.moduleId);

                    if (module) {
                        LayerClass = module.getLayer(options.obj.id);
                        isModuleLayer = true;
                    }
                    else {
                        moduleManager.loadModule({
                            moduleId: options.moduleId, cbLoadModule: function (success) {
                                if (success) {
                                    createLayer(options);
                                }
                            }
                        });
                    }
                }
                else if (options.obj.id === KTW.ui.Layer.ID.HOME_MENU) {
                    LayerClass = KTW.ui.layer.HomeMenuLayer;
                }
                else if (options.obj.id === KTW.ui.Layer.ID.CHANNEL_GUIDE) {
                    LayerClass = KTW.ui.layer.ChannelGuideLayer;
                }
                else if (options.obj.id === KTW.ui.Layer.ID.MINI_EPG) {
                    LayerClass = KTW.ui.layer.MiniEpgLayer;
                }
                else if (options.obj.id === KTW.ui.Layer.ID.IFRAME) {
                    LayerClass = KTW.ui.layer.IframeLayer;
                }
                else if (options.obj.id === KTW.ui.Layer.ID.AUDIO_IFRAME) {
                    LayerClass = KTW.ui.layer.AudioIframeLayer;
                }
                else if (options.obj.id === KTW.ui.Layer.ID.OTS_STITCHING) {
                    LayerClass = KTW.ui.layer.StitchingLayer;
                }
                else if (options.obj.id == KTW.managers.service.PipManager.PIP_LAYER_ID) {
                    LayerClass = KTW.ui.layer.PipLayer;
                }
                else if (options.obj.id === KTW.ui.Layer.ID.CHANNEL_PREVIEW) {
                    LayerClass = KTW.ui.layer.ChannelPreViewLayer;
                }
                //[sw.nam] setting IDs  are added on 02/20
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_MAIN) {

                    LayerClass = KTW.ui.layer.setting.setting_main;
                }
                /*                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SPEAKER_VOLUME) {

                                    LayerClass = KTW.ui.layer.setting.setting_volumeSet;
                                }
                                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SPEECH_RECOGNITION) {

                                    LayerClass = KTW.ui.layer.setting.setting_voiceRecognition;
                                }*/
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_VOICE_GUIDE) {

                    LayerClass = KTW.ui.layer.setting.setting_voiceGuide;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_USB_CONNECT) {

                    LayerClass = KTW.ui.layer.setting.setting_usbConnect;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SUBTITLE) {

                    LayerClass = KTW.ui.layer.setting.setting_subtitle;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_START_CHANNEL) {

                    LayerClass = KTW.ui.layer.setting.setting_startChannel;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SOUND_TYPE) {

                    LayerClass = KTW.ui.layer.setting.setting_soundType;
                }

                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_AUDIO_OUTPUT) {

                    LayerClass = KTW.ui.layer.setting.setting_audioOutput;
                }

                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SHOW_OPTION) {

                    LayerClass = KTW.ui.layer.setting.setting_showOption;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SERIES_AUTO_PLAY) {

                    LayerClass = KTW.ui.layer.setting.setting_seriesAutoPlay;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SCREEN_RATIO) {

                    LayerClass = KTW.ui.layer.setting.setting_screenRatio;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SCREEN_NARRATION) {

                    LayerClass = KTW.ui.layer.setting.setting_screenNarration;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_RESOLUTION) {

                    LayerClass = KTW.ui.layer.setting.setting_resolution;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_PASSWORD) {

                    LayerClass = KTW.ui.layer.setting.setting_password;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_LIMIT_WATCH) {

                    LayerClass = KTW.ui.layer.setting.setting_limitWatch;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BLOCKED_CHANNEL) {

                    LayerClass = KTW.ui.layer.setting.setting_limitChannel;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_PARENTAL_RATING) {

                    LayerClass = KTW.ui.layer.setting.setting_parentalRating;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_MINIGUIDE_DISPLAY_TIME) {

                    LayerClass = KTW.ui.layer.setting.setting_infoShowTimer;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SKIPPED_CHANNEL) {

                    LayerClass = KTW.ui.layer.setting.setting_hideChannel;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_HDMI_SYNCHRONIZE) {

                    LayerClass = KTW.ui.layer.setting.setting_hdmiSynchronize;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_FAVORITE_CHANNEL) {

                    LayerClass = KTW.ui.layer.setting.setting_favorChannel;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_DEFAULT_LANGUAGE) {

                    LayerClass = KTW.ui.layer.setting.setting_defaultLanguage;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BLUETOOTH_CONNECT) {

                    LayerClass = KTW.ui.layer.setting.setting_bluetoothConnect;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_AUTO_STANDBY) {

                    LayerClass = KTW.ui.layer.setting.setting_autoStandbyMode;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_AUTO_POWER) {

                    LayerClass = KTW.ui.layer.setting.setting_autoPower;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SYSTEM_INITIALIZATION) {

                    LayerClass = KTW.ui.layer.setting.setting_allReset;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_ALERT_MESSAGE) {

                    LayerClass = KTW.ui.layer.setting.setting_alertMessage;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_REPEATER) {

                    LayerClass = KTW.ui.layer.setting.setting_repeater;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_RECEIVE_CHECKING) {

                    LayerClass = KTW.ui.layer.setting.setting_receiveChecking;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SYSTEM_INFO) {

                    LayerClass = KTW.ui.layer.setting.setting_systemInfo;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_LNB) {

                    LayerClass = KTW.ui.layer.setting.setting_setLNB;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_EMERGENCY_ALERT) {

                    LayerClass = KTW.ui.layer.setting.setting_emergencyAlert;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_CHANNEL_INFO_OUTPUT) {

                    LayerClass = KTW.ui.layer.setting.setting_channelInfoOutput;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_CLOSED_CAPTION) {

                    LayerClass = KTW.ui.layer.setting.setting_closedCaption;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_CAS_INFO) {

                    LayerClass = KTW.ui.layer.setting.setting_casInfo;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_JUMPING_CHANNEL) {

                    LayerClass = KTW.ui.layer.setting.setting_jumpingChannel;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BEACON) {

                    LayerClass = KTW.ui.layer.setting.setting_beacon;
                }

                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BLUETOOTH_DEVICE_SEARCH_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_bluetoothDeviceSearchPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BT_DEVICE_DELETE_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_btDeviceDelete;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BLUETOOTH_CONNECTION_FAIL_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_btDeviceConnectFail;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BT_FAIRED_DEVICE_CONNECTION_FAIL_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_btConnectFail;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BT_DEVICE_LIMIT_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_btSearchFail;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_BLUETOOTH_SOUND_SETTING_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_bluetoothSoundSettingPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_CHANGE_PASSWORD_POPUP) {
                    LayerClass = KTW.ui.layer.setting.popup.setting_changePassword;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_CHANGE_BUY_PASSWORD_POPUP) {
                    LayerClass = KTW.ui.layer.setting.popup.setting_changeBuyPassword;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_RESET_PASSWORD_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_resetPasswordPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_AUTH_CHECK_PASSWORD_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_authCheckPasswordPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_HDS_ADMIN_CHECK_PASSWORD_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_hdsAdminCheckPasswordPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_LNB_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_setLNB;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_MOBILE_BRANDS_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_mobileBrandsPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_PASSWORD_INIT_COMPLETE_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_resetComplete;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_WRONG_PASSWORD_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_wrongPasswordPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_ALL_RESET_SYSTEM_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_allResettingSystemPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_ALL_RESET_CHECKING_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_allResetCheckingPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_ALL_RESET_REBOOTING_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_allResetRebootingPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SOUND_AUDIO_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_alertSoundAudioPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_UHD_CHECKING_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_uhdCheckingPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_RESOLUTION_CHANGE_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_resolutionChangePopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_RESOLUTION_ALERT_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_resolutionAlertPopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SYSTEMINFO_SYSTEM_UPGRAGE_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_systemInfoSystemUpgradePopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_SYSTEMINFO_CHANNEL_INFO_UPDATE_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_systemInfoChannelInfoUpdatePopup;
                }
                else if (options.obj.id == KTW.ui.Layer.ID.SETTING_RECEIVE_CHECKING_POPUP) {

                    LayerClass = KTW.ui.layer.setting.popup.setting_receiveCheckingPopup;
                }


                else {
                    LayerClass = KTW.ui.Layer;
                }

                if (LayerClass) {
                    //if (options.obj.id === layerId) {
                    //    options.new = false;
                    //}

                    options.obj.id = layerId;

                    layer = new LayerClass(options.obj);

                    layer.bRemove = options.new;

                    if (isModuleLayer) {
                        addModuleLayerArray({
                            moduleId: options.moduleId, moduleLayer: layer
                        });
                    }
                }
            }

            log.printDbg("layer = " + layer);

            if (layer) {
                //loading_layer = layer;

                layer_array.push(layer);
                layer_array.sort(function (a, b) {
                    return a.priority - b.priority;
                });

                // layer에 별도 object parameter를 assign
                if (util.isValidVariable(layer) && options.obj.params) {
                    layer.setParams(options.obj.params);
                }

                // child view assign
                if (options.obj.view) {
                    createView({
                        parent: layer, view: options.obj.view
                    });
                }

                layer.create(function (success) {
                    if (success) {
                        layer.div.css("z-index", layer.priority);

                        if (options.obj.style) {
                            layer.div.css(options.obj.style);
                        }

                        if ($("#" + options.obj.id).length == 0) {
                            layer.div.appendTo(layer_div);
                        }

                        //loading_layer = null;

                        options.cbCreate(layer);
                    }
                    else {
                        removeLayer(layer);
                        //layer = loading_layer = null;
                    }
                });
            }
        }
        else {
            // layer에 별도 object parameter를 assign
            if (util.isValidVariable(layer) && options.obj.params) {
                layer.setParams(options.obj.params);
            }

            options.cbCreate(layer);
        }
    }

    function addNumberOnLayerId(layerId) {
        var num = -1;

        if (_getLayer(layerId)) {
            for (var i = 0; i < layer_array.length; i++) {
                if (layer_array[i].id.indexOf(layerId) >= 0) {
                    num++;
                }
            }
        }

        if (num > -1) {
            layerId += "_" + num;
        }

        return layerId;
    }

    /**
     *
     * @param options
     */
    function createView(options) {
        var view_created = true;
        var view = options.view;

        try {
            view.setParent(options.parent)
        } catch (e) {
            view_created = false;
        }

        if (view_created === false) {
            view = new options.view();
            view.setParent(options.parent);
        }

        options.parent.setChild(view);
    }

    /**
     *
     * @param id
     * @returns {*}
     */
    function _getLayer(id) {
        var layer = null, temp_layer = null;
        var length = layer_array.length;
        for (var i = 0; i < length; i++) {
            temp_layer = layer_array[i];
            if (temp_layer.id === id) {
                layer = temp_layer;
                break;
            }
        }

        log.printDbg("_getLayer() layer = " + layer);

        return layer;
    }

    /**
     * 특정 string 이 id 에 포함된 모든 Layer 를 찾아서 return (지니프릭스 요구사항)
     */
    function _getLayers(id) {
        var arrLayer = [];

        var layer = null;
        var length = layer_array.length;

        for (var i = 0; i < length; i++) {
            layer = layer_array[i];
            if (layer.id.indexOf(id) > -1) {
                arrLayer.push(layer);
            }
        }

        return arrLayer;
    }

    /**
     * Layer를 dom tree & layer_array 에서 제거
     *
     * @param {Layer} layer remove layer
     */
    function removeLayer(layer) {
        var index = layer_array.indexOf(layer);

        if (index >= 0) {
            layer_array.splice(index, 1);
            layer.remove();
        }
        removeModuleLayerArray(layer);
        layer = null;
    }

    function _onKeyDown(/** KeyEvent */ event, skipRequestShow) {

        var key_code = event.keyCode;
        var layer = null;
        var consumed = false;

        log.printDbg("_onKeyDown() key_code = " + key_code);

        //var length = layer_stack.length;

        if (!appServiceManager.checkFirstKey()) {
            log.printDbg("_onKeyDown() received first key event");

            appServiceManager.cancelCallbackProcessFirstKey();
            appServiceManager.processFirstKey();
        }

        // 2017.09.29 dhlee
        // WEBIIIHOME-3356 이슈 수정을 위해 AppServiceManager.js 의 processFirstKey() 에서 시청시간제한 상태인 경우
        // 첫키 프로세스를 수행하지 않고 return 하기 전에 clearNomral()을 수행하는 로직을 추가했다.
        // 이로 인하여 layer_stack 의 length가 달라지게 되므로 length 값을 구하는 위치를 아래와 같이 옮긴다.
        var length = layer_stack.length;

        for (var i = length - 1; i >= 0; i--) {
            layer = layer_stack[i];
            // 2017.09.29 dhlee
            // 조건에 layer가 null 인지를 판단하는 로직을 추가한다.
            if (layer && layer.isShowing()) {
                log.printDbg("_onKeyDown() layer = " + layer);

                //cancel_loading_layer = false;
                //if (loading_layer) {
                //    cancel_loading_layer = true;
                //}

                consumed = layer.handleKeyEvent(key_code);

                if (consumed) {
                    log.printDbg("_onKeyDown()  key_code(" + key_code + ") comsumed by " + layer.id + " Layer");

                    //if (cancel_loading_layer) {
                    //    removeLayer(loading_layer);
                    //    loading_layer = null;
                    //}

                    break;
                }

                // popup의 경우 최상위에 떠 있는 popup에게만 key를 전달하도록 함.
                if (layer.type === KTW.ui.Layer.TYPE.POPUP && layer.id !== KTW.ui.Layer.ID.LOADING) {
                    log.printDbg("_onKeyDown()  key_code(" + key_code + ") bypassed by " + layer.id + " POPUP layer");
                    break;
                }
            }
        }

        if (!consumed) {
            consumed = _handleHotKeyEvent(key_code, skipRequestShow);
            log.printDbg("_onKeyDown() _handleHotKeyEvent consumed = " + consumed);
        }

        if (consumed) {
            // key event를 소비했다면 system 쪽으로 내려가지 않도록 함
            try {
                event.preventDefault();
                event.stopPropagation();
            } catch (e) {
                log.printExec(e);
            }
        }
    }

    /**
     * handling hot key event
     */
    function _handleHotKeyEvent(key_code, skipRequestShow) {
        log.printDbg("_handleHotKeyEvent(" + key_code + ", " + skipRequestShow + ")");

        var consumed = false;

        /**
         * 어차피 시청 시간 제한 상태에서는 모든 키를 consume 처리하고 있으므로 (TimeRestrictedPopup 참조),
         * 시청 시간 제한 상태에서 파생되는 모든 화면들에 대해 키 처리를 여기서 일괄적으로 처리
         */
        if (KTW.managers.service.StateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED) {
            return true;
        }

        consumed = isConsumedNavKeyByVOD(key_code);

        if (!consumed && KTW.managers.service.KidsModeManager.isKidsMode()) {
            consumed = _handleHotKeyEventOnKidsMode(key_code, skipRequestShow);
        }

        if (!consumed) {
            consumed = KTW.managers.service.AudioIframeManager.controlKey(key_code);
        }


        if (!consumed) {
            switch (key_code) {
                case KTW.KEY_CODE.MENU :
                    // activate home
                    _activateHome({skipRequestShow: skipRequestShow, keyCode: key_code});
                    consumed = true;
                    break;
                case KTW.KEY_CODE.FULLEPG :
                case KTW.KEY_CODE.FULLEPG_OTS :
                    // DATA 상태에서 돌아오는 경우 promo channel 정보가 함께 전달된다.
                    // 2017.05.03 dhlee
                    // OTV에서 현재 채널이 오디오 채널인 경우 오디오 채널 편성표를 실행해야 한다.
                    // TODO 일단 아래처럼 처리하지만 추후 정리가 필요할 수 있음 (OTS도 고려해야 하고 등등)
                    var targetMenuId = KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST;
                    var curChannel = KTW.oipf.AdapterHandler.navAdapter.getCurrentChannel();
                    if (KTW.CONSTANT.IS_OTS) {
                        if (curChannel.idType !== KTW.nav.Def.CHANNEL.ID_TYPE.DVB_S) {
                            targetMenuId = KTW.managers.data.MenuDataManager.MENU_ID.OLLEH_TV_CHANNEL;
                        }
                    }
                    else {
                        if (KTW.oipf.AdapterHandler.navAdapter.isAudioChannel(curChannel)) {
                            targetMenuId = KTW.managers.data.MenuDataManager.MENU_ID.AUDIO_CHANNEL;
                        }

                    }
                    log.printDbg("_handleHotKeyEvent(), targetMenuId = " + targetMenuId);
                    var menu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tmpMenu) {
                            //if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                            if (tmpMenu.id === targetMenuId) {
                                log.printDbg("_handleHotKeyEvent(), find menu");
                                return true;
                            }
                        }
                    })[0];
                    if (menu) {
                        KTW.managers.service.MenuServiceManager.jumpMenu({
                            menu: menu,
                            moduleId: KTW.managers.module.Module.ID.MODULE_FRAMEWORK,
                            jump: true,
                            isHotKey: true,
                            skipRequestShow: skipRequestShow
                        });
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.EXIT :
                    // 홈포탈 UI가 노출된 상태가 아닐 경우에는 consumed 처리하지 않도록 함
                    // 홈포탈 UI가 노출된 상태면 all hide

                    if (!stateManager.isOtherAppState()) {
                        var last_layer = _getLastLayerInStack();
                        if (last_layer && last_layer.type >= KTW.ui.Layer.TYPE.NORMAL) {
                            _clearNormalLayer();
                            consumed = true;
                        }
                    }
                    break;
                case KTW.KEY_CODE.ENTER :
                case KTW.KEY_CODE.OK :
                    /**
                     * jjh1117 2016.10.13
                     */
                    if (stateManager.isTVViewingState() === true) {
                        var options = {key: "type", value: KTW.ui.Layer.TYPE.NORMAL};

                        var last_layer = _getLastLayerInStack(options);
                        if (last_layer === null || !last_layer.isShowing()) {
                            /**
                             * jjh1117 2016.10.13
                             * TODO 멀티화면 여부 체크
                             * 멀티화면이 존재 하는 경우에는 PIP 여부를 체크 한 후에 PIPView로 먼저 Key
                             */
                                // layer = _getLastLayer();
                                //
                                // log.printDbg("handleHotKeyEvent() OK ... last layer = " + layer);
                                //
                                // if (util.isValidVariable(layer) === true &&
                                //     layer.priority === OTW.ui.Layer.PRIORITY.PIP_VIDEO) {
                                //     show_pip = true;
                                // }
                                //
                                // if (show_pip === false) {
                                //     _activateLayer({ id: OTW.ui.Layer.ID.MINI_EPG, params: { data: { fromKey: true } } }, true);
                                //     consumed = true;
                                // }

                            var layer = _getLayer(KTW.ui.Layer.ID.MINI_EPG);
                            var clear_stack = true;
                            var dataParams = null;
                            if (layer) {
                                clear_stack = false;
                                dataParams = layer.getParams();

                                if (dataParams !== undefined && dataParams !== null) {
                                    var tempParams = {};
                                    tempParams.fromKey = true;

                                    if (dataParams.data !== undefined && dataParams.data !== null) {
                                        tempParams.data = dataParams.data;
                                    }
                                    dataParams = tempParams;
                                }
                                else {
                                    dataParams = {fromKey: true};
                                }
                            }
                            else {
                                dataParams = {fromKey: true};
                            }

                            _activateLayer({
                                obj: {
                                    id: KTW.ui.Layer.ID.MINI_EPG,
                                    type: KTW.ui.Layer.TYPE.TRIGGER,
                                    priority: KTW.ui.Layer.PRIORITY.NORMAL,
                                    linkage: false,
                                    params: dataParams
                                }, clear_normal: clear_stack, visible: true, skipRequestShow: skipRequestShow
                            });
                        }
                    }
                    // AV 상태이면 미니 epg 실행행ㅎ애
                    break;
                case KTW.KEY_CODE.DETAIL:
                    // 임시
                    //_activateLayer({ id: KTW.ui.Layer.ID.VOD_DETAIL }, true);
                    //consumed = true;
                    // ??? 뭔지 모르겠음
                    break;
                case KTW.KEY_CODE.BACK :
                    // 기존 R5 코드는 vod 재생중이고 stack 이 비어 있으면 showwindow 보여주도록 되어 있음.
                    // 각 모듈이 알아서 처리하는 걸로...
                    // [dj.son] Normal Layer 이상은 historyBack 처리
                    /**
                     * [dj.son] [WEBIIIHOME-3689] 현재 OTHER_APP 상태인 경우, 로직 수행하지 않도록 예외처리 추가
                     */
                    if (!stateManager.isOtherAppState()) {
                        var last_layer = _getLastLayerInStack();
                        if (last_layer && last_layer.type >= KTW.ui.Layer.TYPE.NORMAL) {
                            _historyBack();
                            consumed = true;
                        }
                    }

                    /**
                     * jjh1117 2017/04/07
                     * 아래 로직 추가는 미리보기 아이콘과 채널 전환 시 OSD가 겹쳐 보이는 이슈가 있어
                     * 이 부분을 해결 하기 위해 추가 하였음.(WEBIIIHOME-70)
                     * DCA에서는 정상이나 BACK키에서는 곁쳐 보여 적용함.
                     */
                    // if(consumed === false) {
                    //     KTW.managers.service.ChannelPreviewManager.stop(true);
                    // }
                    break;
                case KTW.KEY_CODE.CONTEXT :
                    // 기본적으로 각 모듈이 처리
                    // 어떻게 호출할지는 차근차근 나중에
                    if (stateManager.isTVViewingState() === true) {
                        var last_layer = _getLastLayerInStack();
                        if (!last_layer || last_layer.type < KTW.ui.Layer.TYPE.NORMAL) {
                            var layer = _getLayer(KTW.ui.Layer.ID.MINI_EPG);
                            if (layer !== null) {
                                consumed = layer.handleKeyEvent(key_code, skipRequestShow);
                                //consumed = true;
                            }
                        }
                    }
                    break;
                /**
                 * jjh1117 2017/09/04
                 * 미니가이드 연관컨텐츠 관련 키에 대해서
                 * DOWN키에서 --> LEFT 키로 변경됨.
                 */
                case KTW.KEY_CODE.LEFT:
                    if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.TV_VIEWING) {
                        var last_layer = _getLastLayerInStack();
                        if (!last_layer || last_layer.type < KTW.ui.Layer.TYPE.NORMAL) {
                            var layer = _getLayer(KTW.ui.Layer.ID.MINI_EPG);
                            if (layer !== null) {
                                consumed = layer.handleKeyEvent(key_code, skipRequestShow);
                                //consumed = true;
                            }
                        }
                    }
                    break;
                case KTW.KEY_CODE.MOVIE :
                case KTW.KEY_CODE.REWATCH :
                case KTW.KEY_CODE.MONTHLY:
                case KTW.KEY_CODE.KIDS:
                case KTW.KEY_CODE.RECOMMEND:
                    /**
                     * [dj.son] [2017-07-19] 영화, TV 다시보기 핫키 처리 추가
                     */
                    var targetId = null;
                    var menu = null;

                    if (key_code === KTW.KEY_CODE.MONTHLY) {
                        targetId = "10000000000000158022";
                    }
                    else if (key_code === KTW.KEY_CODE.MOVIE) {
                        targetId = "6907";
                    }
                    else if (key_code === KTW.KEY_CODE.REWATCH) {
                        targetId = "6905";
                    }
                    else if (key_code === KTW.KEY_CODE.RECOMMEND) {
                        targetId = KTW.managers.data.MenuDataManager.MENU_ID.RECOMMEND_VOD;
                    }
                    else if (key_code === KTW.KEY_CODE.KIDS) {
                        targetId = "KIDS";
                    }

                    if (targetId) {
                        // 2017.03.28 dhlee
                        // 월정액 핫키의 경우 2.0 에서는 특정 카테고리로 이동하도록 되어 있으나
                        // 3.0 에서는 UI 에서 월정액 가입 포털로 이동하도록 정의되어 있음
                        // 따라서 쇼핑 Hot key 처럼 네비게이터에서 처리(홈포털은 hp_showOtherApp 메시지 받으면 hide)
                        // 2017.04.05 dhlee
                        // KT 최종 요구사항 fix 2.0과 동일하게 월정액 편성된 특정 카테고리(하드코딩)로 이동하도록 함
                        var menu = null;

                        menu = KTW.managers.data.MenuDataManager.searchMenu({
                            menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tmpMenu) {
                                if (key_code === KTW.KEY_CODE.KIDS) {
                                    if (KTW.managers.data.MenuDataManager.isKidsMenu(tmpMenu)) {
                                        return true;
                                    }
                                }
                                else {
                                    if (tmpMenu.id === targetId) {
                                        return true;
                                    }
                                }
                            }
                        })[0];

                        if (menu) {
                            //if(key_code === KTW.KEY_CODE.KIDS) {
                            //    menu = KTW.managers.data.MenuDataManager.searchMenu({
                            //        menuData: KTW.managers.data.MenuDataManager.getMenuData(),
                            //        cbCondition: function (tmpMenu) {
                            //            if (tmpMenu.catType === KTW.DATA.MENU.CAT.KIDSSUB) {
                            //                return true;
                            //            }
                            //        }
                            //    })[0];
                            //}

                            // var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenuId(message.cat_id);
                            var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(menu);

                            log.printDbg("_handleHotKeyEvent(), KEY_CODE.MOVIE/KEY_CODE.REWATCH/KEY_CODE.MONTHLY/KEY_CODE.KIDS KEY_CODE.RECOMMEND moduleId = " + moduleId);
                            if (moduleId) {
                                /**
                                 * [dj.son] 키즈 핫키의 경우 토글 기능이 제공되지 않아야 하므로, 키즈 모듈이 키스 핫키임을 인지할 수 있도록 키 코드 전달
                                 */
                                KTW.managers.service.MenuServiceManager.jumpMenu({
                                    moduleId: moduleId,
                                    menu: menu,
                                    jump: true,
                                    skipRequestShow: skipRequestShow,
                                    bHotKey: true,
                                    keyCode: key_code
                                });
                            }
                        }
                        else {
                            log.printDbg("_handleHotKeyEvent(), cannot find monthly menu");
                            // TODO 체크 필요
                            util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP, undefined, KTW.ERROR.CODE.E039.split("\n"), "알림", null, true);
                        }
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.SEARCH :
                    // 검색
                    var menu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tempMenu) {
                            if (tempMenu.catType === KTW.DATA.MENU.CAT.SRCH) {
                                return true;
                            }
                        }
                    })[0];

                    KTW.managers.service.MenuServiceManager.jumpMenu({
                        menu: menu, jump: true, skipRequestShow: skipRequestShow, bHotKey: true
                    });

                    consumed = true;
                    break;
                case KTW.KEY_CODE.RECOMMEND :
                    // 추천VOD
                    // 기존코드로는 마이메뉴 추천 VOD 로 가야함
                    // 마찬가지로 어떤 모듈을 호출해야 하는지 알아야 함
                    // 근데 추천 키는 못봤는데;;;
                    break;
                case KTW.KEY_CODE.WATCH_LIST :
                    // 시청목록
                    // 기존 코드로는 마이메뉴 시청목록으로 가야함
                    // 마찬가지로 어떤 모듈을 호출해야 하는지 알아야 함
                    // 근데 시청목록 키는 못봤는데;;;
                    break;
                case KTW.KEY_CODE.PURCHASE_LIST :
                    // 구매목록
                    // 기존 코드로는 마이메뉴 구매목록 으로 가야함
                    // 마찬가지로 어떤 모듈을 호출해야 하는지 알아야 함
                    // 근데 구매목록 키는 못봤는데;;;
                    break;
                case KTW.KEY_CODE.WISH_LIST :
                    // 찜목록
                    // 기존 코드로는 찜목록 구매목록 으로 가야함
                    // 마찬가지로 어떤 모듈을 호출해야 하는지 알아야 함
                    // 근데 찜목록 키는 못봤는데;;;
                    break;
                case KTW.KEY_CODE.MYMENU :
                    // 마이메뉴
                    // 기존코드로는 마이메뉴로 가야함
                    // 마찬가지로 마이메뉴가 어떤 모듈인지 알아야함
                    // 2017.03.27 dhlee 우리집 맞춤 TV 메뉴로 이동
                    var menu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tempMenu) {
                            if (tempMenu.parent === null && tempMenu.catType === KTW.DATA.MENU.CAT.SUBHOME) {
                                return true;
                            }
                        }
                    })[0];

                    KTW.managers.service.MenuServiceManager.jumpMenu({
                        menu: menu, jump: true, skipRequestShow: skipRequestShow, bHotKey: true
                    });

                    consumed = true;
                    break;
                case KTW.KEY_CODE.SETTING :
                    // TV설정
                    // 기존코드로는 세팅으로 가야함
                    // 마찬가지로 세팅이 어떤 모듈인지 알아야함
                    var menu = KTW.managers.data.MenuDataManager.searchMenu({
                        menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tmpMenu) {
                            if (tmpMenu.parent === null && tmpMenu.catType === KTW.DATA.MENU.CAT.CONFIG) {
                                return true;
                            }
                        }
                    })[0];

                    KTW.managers.service.MenuServiceManager.jumpMenu({
                        menu: menu, jump: true, skipRequestShow: skipRequestShow, bHotKey: true
                    });

                    consumed = true;
                    break;
                case KTW.KEY_CODE.SKY_CHOICE :
                    // OTS 일 경우에만 전체 편성표 - Sky Choice 로 가야함

                    if (KTW.CONSTANT.IS_OTS) {
                        var menu = KTW.managers.data.MenuDataManager.searchMenu({
                            menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tmpMenu) {
                                //if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.ENTIRE_CHANNEL_LIST) {
                                if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.MOVIE_CHOICE) {
                                    log.printDbg("_handleHotKeyEvent(), find menu");
                                    return true;
                                }
                            }
                        })[0];
                        if (menu) {
                            KTW.managers.service.MenuServiceManager.jumpMenu({
                                menu: menu,
                                moduleId: KTW.managers.module.Module.ID.MODULE_FRAMEWORK,
                                jump: true,
                                isHotKey: true,
                                skipRequestShow: skipRequestShow,
                                bHotKey: true
                            });
                        }
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.FAVORITE :
                    changeFavoriteChannel();
                    break;
                case KTW.KEY_CODE.APP_STORE:
                    /**
                     * [dj.son] AppStore 는 2018년 4월까지만 운영,
                     * - 앱스토어 핫키시, 게임/생활/쇼핑 화면을 보여주기로 새로 정의됨 (플립북 1.4 버전 68 페이지)
                     * - 편성정보가 없을 경우, 알림 팝업 표시
                     */

                    var appGmMenu = KTW.managers.service.MenuServiceManager.getAppGmMenu();

                    if (appGmMenu) {
                        var moduleId = KTW.managers.service.MenuServiceManager.getModuleIdByMenu(appGmMenu);

                        log.printDbg("_handleHotKeyEvent(), KEY_CODE.APP_STORE moduleId = " + moduleId);
                        if (moduleId) {
                            KTW.managers.service.MenuServiceManager.jumpMenu({
                                moduleId: moduleId,
                                menu: appGmMenu,
                                jump: true,
                                skipRequestShow: skipRequestShow,
                                bHotKey: true
                            });
                        }
                    }
                    else {
                        log.printDbg("_handleHotKeyEvent(), cannot find AppGm menu");
                        util.showErrorPopup(KTW.ui.Layer.PRIORITY.POPUP, undefined, KTW.ERROR.CODE.E039.split("\n"), "알림", null, true);
                    }

                    consumed = true;
                    break;

                case KTW.KEY_CODE.KIDS_MENU :
                    if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                        _activateHome({skipRequestShow: skipRequestShow, keyCode: key_code});
                    }
                    else {
                        /**
                         * [dj.son] [KTUHDIII-766] 키즈 홈 키를 눌렀을때, 15초 후에 사라지지 않도록 예외처리 추가
                         */
                        KTW.managers.service.KidsModeManager.changeMode({
                            kidsMode: KTW.oipf.Def.KIDS_MODE.ON, jumpData: {
                                bHotKey: true
                            }
                        });
                    }

                    consumed = true;
                    break;
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_1:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_2:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_3:
                case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_4:

                    var kidsHotKeyMenu = KTW.managers.data.MenuDataManager.getKidsHotKeyMenu();
                    var arrChildMenu = null;
                    if (kidsHotKeyMenu && kidsHotKeyMenu.children && kidsHotKeyMenu.children.length > 0) {
                        arrChildMenu = kidsHotKeyMenu.children;
                    }

                    var childMenu = null;
                    var targetType = KTW.DATA.MENU.ITEM.CATEGORY;
                    var targetCategoryId = null;
                    var targetContentId = null;
                    var targetLocator = null;

                    /**
                     * [dj.son] 키즈 핫키들의 경우, 편성이 안되어 있으면, 하드코딩된 메뉴로 점프
                     */
                    if (key_code === KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_1) {
                        if (arrChildMenu) {
                            childMenu = arrChildMenu[0];
                        }
                        targetCategoryId = "10000000000000207860";
                    }
                    else if (key_code === KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_2) {
                        if (arrChildMenu) {
                            childMenu = arrChildMenu[1];
                        }
                        targetCategoryId = "10000000000000110279";
                    }
                    else if (key_code === KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_3) {
                        if (arrChildMenu) {
                            childMenu = arrChildMenu[2];
                        }
                        targetCategoryId = "10000000000000196415";
                    }
                    else if (key_code === KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_4) {
                        if (arrChildMenu) {
                            childMenu = arrChildMenu[3];
                        }
                        targetCategoryId = "10000000000000212969";
                    }

                    if (childMenu && childMenu.hsTargetId) {
                        targetType = childMenu.hsTargetType;

                        if (targetType === KTW.DATA.MENU.ITEM.CATEGORY) {
                            targetCategoryId = childMenu.hsTargetId;
                        }
                        else if (targetType === KTW.DATA.MENU.ITEM.SERIES || targetType === KTW.DATA.MENU.ITEM.CONTENT) {
                            targetContentId = childMenu.hsTargetId;
                        }

                        targetLocator = childMenu.hsTargetLocator;
                    }

                    /**
                     * [dj.son] 키즈 핫키 점프임을 알리기 위해 bHotKey 추가
                     */
                    var jumpData = {
                        itemType: targetType,
                        cat_id: targetCategoryId,
                        const_id: targetContentId,
                        locator: targetLocator,
                        bHotKey: true
                    };

                    if (KTW.managers.service.KidsModeManager.isKidsMode()) {
                        KTW.managers.service.MenuServiceManager.jumpByMenuData(jumpData);
                    }
                    else {
                        KTW.managers.service.KidsModeManager.changeMode({
                            kidsMode: KTW.oipf.Def.KIDS_MODE.ON, jumpData: jumpData
                        });
                    }

                    consumed = true;
                    break;
            }
        }


        if (consumed) {
            if (key_code === KTW.KEY_CODE.EXIT) {
                //2017.09.13 sw.nam
                //나가기 키에 대한 핫키 로그는 수집하지 않는다
                return consumed;
            }
            KTW.managers.UserLogManager.collect({
                type: KTW.data.UserLog.TYPE.HOT_KEY, act: key_code
            });
        }

        /**
         * [dj.son] [WEBIIIHOME-3592] 홈, 편성표 핫키 눌렀을때, PinMode 종료하도록 예외처리 추가
         * 2018.01.04 sw.nam [WEBIIIHOME-3592] 보완코드 다시 적용
         */
        if (key_code === KTW.KEY_CODE.MENU || key_code === KTW.KEY_CODE.FULLEPG || key_code === KTW.KEY_CODE.FULLEPG_OTS) {
            KTW.oipf.AdapterHandler.speechRecognizerAdapter.checkPinModeStop();
        }

        return consumed;
    }

    function changeFavoriteChannel() {
        log.printDbg("changeFavoriteChannel()");

        var favorite_channel = null;
        var favorite_ch_list = [];
        var check_index = 0;

        // Skylife 채널이 먼저 추가
        if (KTW.CONSTANT.IS_OTS === true) {
            favorite_ch_list = adapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.SKYLIFE_CHANNELS_FAVORITE);
            favorite_ch_list = Array.apply(this, favorite_ch_list).slice();
            check_index = favorite_ch_list.length;
        }

        // 이후 KT 채널을 리스트에 추가함
        var kt_favorite_ch_list = adapterHandler.navAdapter.getChannelList(KTW.oipf.Def.BROADCAST.CHANNEL_CONFIG.FAVOURITE_FAVORITE);
        kt_favorite_ch_list = Array.apply(this, kt_favorite_ch_list).slice();

        favorite_ch_list = favorite_ch_list.concat(kt_favorite_ch_list);

        if (favorite_ch_list.length > 0) {
            if (stateManager.isVODPlayingState()) {
                favorite_channel = getFavoriteChannel(favorite_ch_list, check_index);
                KTW.managers.module.ModuleManager.getModuleForced(KTW.managers.module.Module.ID.MODULE_VOD, function (vodModule) {
                    if (vodModule) {
                        vodModule.execute({
                            method: "stopVodWithChannelSelection", params: {
                                channelObj: favorite_channel
                            }
                        });
                    }
                });
            }
        }
        else {
            if (stateManager.serviceState === KTW.CONSTANT.SERVICE_STATE.OTHER_APP) {
                /**
                 * [dj.son] obs_show 메세지로 키가 오고 AppServiceManager 에서 changeService 처리함...
                 *          다만, obs_show 메세지 처리 부분에서는 채널 튠 부분이 없으므로, 여기서 채널 튠 하도록 수정
                 *          특이사항으로는 풀브라우저일 경우, obs_show 로 왔음에도 requestShow 를 요청하면 no 로 리턴됨;;;
                 *          따라서 2.0 로직대로 먼저 채널 튠 한다음, 채널 이벤트 받고 팝업을 show
                 */

                var channel_control = adapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
                channel_control.addChannelEventListener(onChannelChangeEvent);

                var promo = adapterHandler.navAdapter.getPromoChannel();
                var osd_state = adapterHandler.extensionAdapter.getOSDState();
                if (osd_state === 1) {
                    adapterHandler.extensionAdapter.requestDestroyFullBrowser(adapterHandler.navAdapter.getPromoChannel());
                }
                else {
                    adapterHandler.navAdapter.changeChannel(promo, true);
                }
            }
            else {
                KTW.managers.service.FavoriteChannelManager.showFavEmptyPopup();
            }
        }
    }

    function onChannelChangeEvent() {
        var channel_control = adapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
        channel_control.removeChannelEventListener(onChannelChangeEvent);

        KTW.managers.service.FavoriteChannelManager.showFavEmptyPopup();
    }

    /**
     * 선호채널 획득
     * 전체 선호채널 배열 순서가 [Skylife - KT] 순이기 때문에
     * 현재 채널을 기준으로 검색 시작, 종료 index 중 현재 채널번호 보다 큰 채널을 선택함.
     * 검색을 했음에도 불구하고 찾지 못했다면 가장 처음 선호채널을 선택함
     * 추가로 선호채널 ring이 Skylife 또는 KT 하나만 존재하는 경우
     * 현재 채널과 다른 type의 가장 첫 선호채널을 선택
     *
     * @param list - 전체 선호채널 array (OTS의 경우 : skylife 채널 + kt 채널 순)
     * @param index - skylife 선호채널 개수
     */
    function getFavoriteChannel(list, index) {

        var favorite_channel = null;
        var length = list.length;
        var offset = 0;
        var check_length = length;

        var channel_control = adapterHandler.navAdapter.getChannelControl(KTW.nav.Def.CONTROL.MAIN);
        var cur_channel = channel_control.vbo.currentChannel;
        log.printDbg("getFavoriteChannel() cur channel = " + JSON.stringify(cur_channel));

        // 현재 채널이 skylife 채널인지 KT 채널인지 먼저 확인
        var is_sky_channel = false;
        if (util.isValidVariable(cur_channel) === true) {
            if (cur_channel.idType === Channel.ID_DVB_S || cur_channel.idType === Channel.ID_DVB_S2) {
                is_sky_channel = true;
            }
        }

        if (is_sky_channel === true) {
            // 현재채널이 skylife 채널인 경우
            if (index > 0) {
                // skylife 선호채널이 존재하면 검색 종료 index 설정
                check_length = index;
            }
            else {
                // skylife 선호채널이 존재하지 않는다면
                // KT 선호채널 중 첫번째 것을 선택
                favorite_channel = list[0];
            }
        }
        else {
            // 현재채널이 KT 채널인 경우
            if (length - index > 0) {
                // KT 선호채널이 존재하면 검색 시작 index 설정
                offset = index;
            }
            else {
                // KT 선호채널이 존재하지 않는다면
                // Skylife 선호채널 중 첫번째 것을 선택
                favorite_channel = list[0];
            }
        }

        if (favorite_channel === null) {
            // 선호채널이 아직 assign 되어 있지 않다면
            // 검색할 선호채널 시작, 종료 index를 기준으로
            // 인접한 큰 채널번호를 가진 선호채널을 검색
            for (var i = offset; i < check_length; i++) {
                if (list[i].majorChannel > cur_channel.majorChannel) {
                    favorite_channel = list[i];
                    break;
                }
            }

            if (favorite_channel === null) {
                // 만약 검색을 통해서도 찾지 못했다면
                // 현재 채널과 동일한 type의 선호채널 ring에서
                // 인접한 큰 채널번호를 가진 채널이 없다는 것임
                // 결국 looping 에 의해 다음 ring의 가장 첫번째 선호채널을 선택함
                if (check_length === index) {
                    if (length === index) {
                        // 단, 다음 channel ring이 없다면 가장 첫번째 선호채널을 선택함
                        favorite_channel = list[0];
                    }
                    else {
                        favorite_channel = list[index];
                    }
                }
                else {
                    favorite_channel = list[0];
                }
            }
        }

        log.printDbg("getFavoriteChannel() favorite channel = " + JSON.stringify(favorite_channel));

        return favorite_channel;
    }

    function _handleHotKeyEventOnKidsMode(key_code, skipRequestShow) {
        log.printDbg("_handleHotKeyEventOnKidsMode(" + key_code + ", " + skipRequestShow + ")");

        var consumed = false;

        switch (key_code) {
            // 2017.08.01 dhlee
            // kids mode 일때 해당 hot key 들은 홈으로 전달 된다.
            case KTW.KEY_CODE.SHOPPING:
            case KTW.KEY_CODE.APP_STORE:
            case KTW.KEY_CODE.SEARCH:
            case KTW.KEY_CODE.MYMENU:
            case KTW.KEY_CODE.FAVORITE:
            case KTW.KEY_CODE.SKY_CHOICE :
            case KTW.KEY_CODE.MOVIE :
            case KTW.KEY_CODE.REWATCH :
            case KTW.KEY_CODE.MONTHLY:
            case KTW.KEY_CODE.WIDGET:
            case KTW.KEY_CODE.RECOMMEND:
                KTW.managers.service.KidsModeManager.activateCanNotMoveMenuInfoPopup();

                consumed = true;
                break;
            case KTW.KEY_CODE.KIDS:
                consumed = false;
                break;
            case KTW.KEY_CODE.FULLEPG :
            case KTW.KEY_CODE.FULLEPG_OTS :
                var menu = KTW.managers.data.MenuDataManager.searchMenu({
                    menuData: KTW.managers.data.MenuDataManager.getMenuData(), cbCondition: function (tmpMenu) {
                        if (tmpMenu.id === KTW.managers.data.MenuDataManager.MENU_ID.KIDS_CHANNEL) {
                            return true;
                        }
                    }
                })[0];

                KTW.managers.service.MenuServiceManager.jumpMenu({
                    menu: menu, jump: true, skipRequestShow: skipRequestShow, bHotKey: true
                });

                consumed = true;
                break;
            case KTW.KEY_CODE.STAR :
                // 키즈 모드시 * 키는 핫키로 간주하고, 키즈 모듈에 전달
                // 2017.04.13 dhlee 키즈 모드일 때 * 키를 입력하면 키즈 모듈 쪽 API를 call 한다.
                var kidsModule = moduleManager.getModule(KTW.managers.module.Module.ID.MODULE_KIDS);
                if (kidsModule) {
                    kidsModule.execute({
                        method: "kidsSTBOff", params: {}
                    });
                    consumed = true;
                }
                else {
                    log.printDbg("handleHotKeyEvent(), something error for kidsSTBOff() execute")
                }
                consumed = true;
                break;
        }

        return consumed;
    }

    function isConsumedNavKeyByVOD(keyCode) {
        if (stateManager.isVODPlayingState()) {
            log.printDbg("isConsumedNavKeyByVOD(" + keyCode + ")");

            var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
            if (vodModule) {
                return vodModule.execute({
                    method: "onKeyActionForVOD", params: {
                        keyCode: keyCode
                    }
                });
            }
            /*
                        var checkVodModule = false;

                        switch (keyCode) {
                            case KTW.KEY_CODE.PLAY:
                            case KTW.KEY_CODE.PAUSE:
                            case KTW.KEY_CODE.STOP:
                                var layer = _getLastLayerInStack();
                                if (layer && layer.type < KTW.ui.Layer.TYPE.POPUP) {
                                    checkVodModule = true;
                                }
                                break;
                            case KTW.KEY_CODE.CH_UP:
                            case KTW.KEY_CODE.CH_DOWN:
                                checkVodModule = true;
                                break;
                        }

                        if (checkVodModule) {
                            var vodModule = KTW.managers.module.ModuleManager.getModule(KTW.managers.module.Module.ID.MODULE_VOD);
                            if (vodModule) {
                                // 2017.04.13 dhlee result는 향후를 위해 일단 전달만 받아둔다.
                                vodModule.execute({
                                    method: "onKeyActionForVOD",
                                    params: {
                                        keyCode: keyCode
                                    }
                                });
                            }
                            return true;
                        }
                        */
        }
        return false;
    }

    function _isHotKey(keyCode) {
        log.printDbg("_isHotKey(" + keyCode + ")");

        var isHotKey = false;

        switch (keyCode) {
            case KTW.KEY_CODE.MENU :
            case KTW.KEY_CODE.FULLEPG :
            case KTW.KEY_CODE.FULLEPG_OTS :
            case KTW.KEY_CODE.MOVIE :
            case KTW.KEY_CODE.REWATCH :
            case KTW.KEY_CODE.MONTHLY:
            case KTW.KEY_CODE.SEARCH :
            case KTW.KEY_CODE.RECOMMEND :
            case KTW.KEY_CODE.KIDS :
            case KTW.KEY_CODE.WATCH_LIST :
            case KTW.KEY_CODE.PURCHASE_LIST :
            case KTW.KEY_CODE.WISH_LIST :
            case KTW.KEY_CODE.MYMENU :
            case KTW.KEY_CODE.SETTING :
            case KTW.KEY_CODE.SKY_CHOICE :
            case KTW.KEY_CODE.FAVORITE :
            case KTW.KEY_CODE.SHOPPING:
            case KTW.KEY_CODE.APP_STORE:
            case KTW.KEY_CODE.WIDGET:

            /**
             * [dj.son] 키즈 리모컨 관련 핫키 추가
             */
            case KTW.KEY_CODE.KIDS_MENU :
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_1:
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_2:
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_3:
            case KTW.KEY_CODE.KIDS_SERVICE_HOT_KEY_4:

                isHotKey = true;
                break;
        }

        return isHotKey;
    }

    /**
     * KTW.managers.service.StateManager serviceStateChange callback
     */
    // TODO [dj.son] 크게 할 일은 없지만 나중에 필요할수도 있음
    function onServiceStateChange(options) {
        log.printDbg("LayerManager onServiceStateChange() " + log.stringify(options));

        switch (options.nextServiceState) {
            case KTW.CONSTANT.SERVICE_STATE.TV_VIEWING :
                break;
            case KTW.CONSTANT.SERVICE_STATE.VOD :
                break;
            case KTW.CONSTANT.SERVICE_STATE.OTHER_APP :
                break;
            case KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_TV :
                break;
            case KTW.CONSTANT.SERVICE_STATE.OTHER_APP_ON_VOD :
                break;
            case KTW.CONSTANT.SERVICE_STATE.STANDBY :
                /**
                 * STANDBY 상태에서는 모든 layer 가 clear 될 수 있도록 수정
                 *
                 * [dj.son] [WEBIIIHOME-2154] 네트워크 팝업이 띄워져 있을 경우, 나중에 다시 power on 이 되더라도 노출되어야 함
                 * 따라서 clear 대상에서 제외하도록 수정
                 */
                clearSystemPopup(["NetworkErrorPopup", "WeakSignalPopup"]);
                _clearNormalLayer();
                break;
            case KTW.CONSTANT.SERVICE_STATE.TIME_RESTRICTED :
                break;
        }
    }

    // /**
    //  * hot key에 의해 특정 layer로 바로 이동하는 경우
    //  *
    //  * 1depth Layer의 경우 hideNormalLayer()를 호출하고
    //  * 이외의 경우에는 hideAllLayer()를 호출하는데
    //  * history back 기능이 동작해야하기 때문이다.
    //  * 추가로 hideAllLayer()를 호출하는 경우에 특정 시점의 back 기능이
    //  * 무조건 마지막 상태가 아니라 자연스럽게 이어져서 이전 화면으로
    //  * 이동해야 하기 때문에 Layer#restore() 호출을 통해서 처리되도록 한다.
    //  *
    //  * ex) vod 상세화면에서 시청목록 hot키를 누른 경우
    //  * 시청목록 화면에서 이전화면으로 이동해서 홈메뉴까지 왔을 때
    //  * VOD 상세화면이 아니라 VOD 1depth 메뉴 category 상태로 돌아와야 한다.
    //  */
    // function jumpLayer(id, params, toggle) {
    //     var layer = _getLayer(id);

    //     if (toggle === true) {
    //         if (layer !== null && layer.isShowing() === true) {
    //             hideNormalLayer();
    //             return;
    //         }
    //     }

    //     // activate stack layer
    //     var obj = {id: id};
    //     if (params) {
    //         obj.params = params;
    //     }

    //     var clear_stack = false;
    //     if (id === KTW.ui.Layer.ID.FULL_EPG) {
    //         clear_stack = true;
    //     }

    //     var deactivate_layer = false;
    //     var last_stack_layer = _getLastStackLayer(true);
    //     if (last_stack_layer !== null) {
    //     	// 핫키 등에 의해서 jump 하는 경우
    //   	// last stack layer가 실시간 인기채널이라면
    //  		// 의도적으로 deactivate 처리함
    //   		// 이렇게 하지 않으면 이전 키에 의해
    //    		// 실시간 인기채널 상태로 복귀하는 현상이 있기 때문에
    // 	    // 이렇게 stack layer에서 제거함
    //         if (last_stack_layer.id.indexOf(KTW.ui.Layer.ID.STICHING) === 0) {
    //         	deactivate_layer = true;
    //         }
    //         else {
    //         	// 홈메뉴로 jump 시 이전에 DETAIL layer를 가지고 있다면
    //          // 해당 layer를 제거하고 메뉴로 이동하도록 한다.
    // 	        // VOD 상태에서는 현재 play 중인 VOD DETAIL stack layer가
    //     	    // 이미 존재하기 때문에 이를 제거하도록 해야 함
    //         	if (stateManager.isVODPlayingState() === true) {
    //         		if (last_stack_layer.id.indexOf(KTW.ui.Layer.ID.VOD_DETAIL) === 0) {
    //                 	deactivate_layer = true;
    //            	 	}
    //         	}
    //         }

    //         if (deactivate_layer === true) {
    //         	_deactivateLayer(last_stack_layer.id, false, false, true);
    //         }
    //     }

    //     _activateLayer(obj, clear_stack);
    // }

    /**
     *
     * @private
     */
    function _activateHome(options) {
        log.printDbg("LayerManager _activateHome(" + log.stringify(options) + ")");

        options = options || {};

        // [dj.son] 최상위 Layer 가 홈메뉴일 경우 deactivate 처리
        var lastLayer = _getLastLayerInStack();
        var layerOptions = {
            clear_normal: true, visible: true, skipRequestShow: options.skipRequestShow, cbActivate: function () {
                if (options.cbActivateHome) {
                    options.cbActivateHome();
                }
            }
        };

        if (KTW.managers.service.KidsModeManager.isKidsMode()) {
            var kidsMenu = KTW.managers.data.MenuDataManager.getMenuData()[0];
            var bHotKey = false;
            if (options.keyCode === KTW.KEY_CODE.MENU || options.keyCode === KTW.KEY_CODE.KIDS_MENU) {
                bHotKey = true;
            }

            KTW.managers.service.MenuServiceManager.jumpMenu({
                menu: kidsMenu,
                isBoot: options.isBoot,
                bAutoExit: options.bAutoExit,
                skipRequestShow: options.skipRequestShow,
                bHotKey: bHotKey
            });
        }
        else {
            layerOptions.obj = {
                id: KTW.ui.Layer.ID.HOME_MENU,
                type: KTW.ui.Layer.TYPE.NORMAL,
                priority: KTW.ui.Layer.PRIORITY.NORMAL,
                linkage: true,
                params: {
                    isBoot: true, bAutoExit: options.bAutoExit
                }
            };

            if (lastLayer && lastLayer.id === layerOptions.obj.id && lastLayer.isShowing()) {
                _deactivateLayer({
                    id: layerOptions.obj.id
                });
            }
            else {
                _activateLayer(layerOptions);

                /**
                 * [dj.son] activateHome 함수가 호출될 경우, 첫홈키 로직 체크 후 호출
                 * - 이 상황이면 state 는 이미 다 체크된 상황이므로 걱정 안해도 됨
                 */
                if (options.keyCode && !appServiceManager.checkFirstHomeKey()) {
                    appServiceManager.processFirstHomeKey();
                }

                /**
                 * [dj.son] 비즈 상품인 경우,홈 화면 띄울때 비즈 메뉴 업데이트 하도록 수정
                 */
                if (KTW.CONSTANT.IS_BIZ && KTW.managers.data.OCManager.isUpdateBizMenu() && stateManager.appState > KTW.CONSTANT.APP_STATE.INITIALIZED) {
                    setTimeout(function () {
                        KTW.managers.data.OCManager.updateBizMenu();
                    }, 0);
                }
            }
        }
    }

    /**
     * show loading image
     * [jh.lee] 로딩팝업의 최대로딩 시간을 설정할 수 있는 변수 추가 (timeout)
     * [jjh1117] timeout_popup_invisible : 타임아웃 팝업 노출 여부
     *
     * preventKey, timeout, cbTimeout, boxSize, subText, isHide
     */
    function _startLoading(loadingData) {
        if (is_show_loading === true) {
            return;
        }

        log.printDbg("_startLoading()");
        _stopLoading();

        loadingData = loadingData ? loadingData : {};

        KTW.ui.LayerManager.activateLayer({
            obj: {
                id: KTW.ui.Layer.ID.LOADING,
                type: KTW.ui.Layer.TYPE.POPUP,
                priority: KTW.ui.Layer.PRIORITY.POPUP + 150,
                linkage: true,
                view: KTW.ui.view.LoadingDialog,
                params: loadingData
            }, visible: true
        });
        is_show_loading = true;
    }

    /**
     * hide loading image
     */
    function _stopLoading() {
        if (is_show_loading === true) {
            log.printDbg("_stopLoading()");

            _deactivateLayer({
                id: KTW.ui.Layer.ID.LOADING, onlyTarget: true
            });
            is_show_loading = false;
        }
    }

    function _isShowLoading() {
        return is_show_loading;
    }

    function addModuleLayerArray(options) {
        if (!module_layer_array[options.moduleId]) {
            module_layer_array[options.moduleId] = [];
        }

        module_layer_array[options.moduleId].push(options.moduleLayer);
    }

    function removeModuleLayerArray(moduleLayer) {
        for (var key in module_layer_array) {
            var array = module_layer_array[key];
            for (var i = 0; i < array.length; i++) {
                if (moduleLayer.id === array[i].id) {
                    array.splice(i, 1);
                    break;
                }
            }
        }
    }

    function _getModuleLayerArray(moduleId) {
        if (!moduleId) {
            return module_layer_array;
        }

        return module_layer_array[moduleId];
    }


    function _addVBOBackground(options) {
        var child = vboBackgroundArea.find("#" + options.id);

        if (!child || child.length === 0) {
            child = options.background;
            child.attr("id", options.id).css({display: "none"});
            vboBackgroundArea.append(child);
        }
    }

    function _removeVBOBackground(id) {
        vboBackgroundArea.find("#" + id).remove();
    }

    function _showVBOBackground(id) {
        vboBackgroundArea.children().css({display: "none"});
        vboBackgroundArea.find("#" + id).css({display: "block"});
    }

    function _hideVBOBackground() {
        vboBackgroundArea.children().css({display: "none"});
    }

    function _addChangeLayerListener(l) {
        changeLayerListener.addListener(l);
    }

    function _removeChangeLayerListener(l) {
        changeLayerListener.removeListener(l);
    }

    function notifyChangeLayer() {
        changeLayerListener.notify();
    }

    return {
        init: _init,

        activateLayer: _activateLayer,
        deactivateLayer: _deactivateLayer,
        historyBack: _historyBack,
        activateHome: _activateHome,
        activateMiniEpg: _activateMiniEpg,

        hideNormalLayer: _hideNormalLayer,
        resumeNormalLayer: _resumeNormalLayer,
        clearNormalLayer: _clearNormalLayer,
        clearNormalLayerExceptTarget: _clearNormalLayerExceptTarget,
        clearPopupLayer: _clearPopupLayer,

        onKeyDown: _onKeyDown,
        handleHotKeyEvent: _handleHotKeyEvent,
        isHotKey: _isHotKey,

        startLoading: _startLoading,
        stopLoading: _stopLoading,
        isShowLoading: _isShowLoading,

        getLastLayerInStack: _getLastLayerInStack,
        getLayer: _getLayer,
        getLayers: _getLayers,

        getModuleLayerArray: _getModuleLayerArray,

        addVBOBackground: _addVBOBackground,
        removeVBOBackground: _removeVBOBackground,
        showVBOBackground: _showVBOBackground,
        hideVBOBackground: _hideVBOBackground,

        addChangeLayerListener: _addChangeLayerListener,
        removeChangeLayerListener: _removeChangeLayerListener
    };
}());
