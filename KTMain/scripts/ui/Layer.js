"use strict";

(function() {
    /**
     *
     * 화면상 그려지는 모든 ui의 div 및 하위 elements를 포함
     * 기본적으로 priority 기반으로 정렬
     * 
     * @param {Object} options id       - id
     *                         type     - type
     *                         priority - priority
     *                         linkage  - 다른 layer 와의 공존여부, type 별로 약간씩 동작이 다름
     *                                      1. BACKGROUND    : 항상 show 기 때문에 의미없음
     *                                      2. TRIGGER       : 자신의 위치가 TRIGGER Layer 중 가장 최상위 이고, stack 상 최상위 Layer 와 자신의 linkage 값이 모두 true 일 경우 공존
     *                                      3. NORMAL        : TRIGGER 와의 공존 여부
     *                                      4. POPUP         : 다른 POPUP 과의 공존 여부
     *                                                         stack 상 자신의 위치가 최상위 혹은 최상위 Layer 바로 아래에 위치하고 자신의 linkage 값이 true 일 경우 다른 POPUP 과 공존
     */
    KTW.ui.Layer = function(options) {

        options = options || {};
        
        this.id = options.id;
        this.type = options.type;
        this.priority = options.priority;
        this.div = null;
        
        // external parameter
        this.params = null;
        
        // child view
        this.child = null;
        
        if (this.type < KTW.ui.Layer.NORMAL) {
            this.linkage = true;
        }
        else {
            this.linkage = options.linkage ? true : false;
        }
    };

    KTW.ui.Layer.prototype = {

        /**
         * create div of Layer 
         *
         * @param {Function} cbCreate create callback
         * 
         */
        create: function(cbCreate) {
            KTW.utils.Log.printDbg("Layer#created(" + this.id + ") : " + this.div);

            if (!this.div) {
                this.div = KTW.utils.util.makeElement({
                    tag: "<div />",
                    attrs: {
                        id: this.id
                    }
                });
                
                if (this.child) {
                    this.child.create();
                }
                
                // child view 에서 parent div를 visible 하게 만들 수 있기 때문에
                // create 시에 가장 마지막에 의도적으로 hidden 처리한다.
                //$("#" + this.id).css("visibility","hidden");
                //this.div.css("visibility","hidden");
                this.div.css("display","none");
            }

            if (cbCreate) {
                cbCreate(true);
            }
        },

        /**
         * remove div of Layer
         */
        remove: function() {
            KTW.utils.Log.printDbg("Layer#removed(" + this.id + ") : " + this.div);

            if (this.div) {
                this.div.remove();
                
                if (this.child) {
                    this.child.destroy();
                    this.child = null;
                }
            }
        },

        /**
         * change visibility to show Layer
         *
         * @param {Object} options resume - if true, history back
         */ 
        show: function(options) {
            KTW.utils.Log.printDbg("Layer#show(" + this.id + ")");

            if (this.child) {
                this.child.show(options);
            }
            
            //$("#" + this.id).css("visibility","inherit");
            this.div.css("display","");
        },

        /**
         * change visibility to hide Layer
         *
         * @param {Object} options pause - if true, Layer is exist in Layer Stack
         */
        hide: function(options) {
            KTW.utils.Log.printDbg("Layer#hide(" + this.id + ")");
            
            //$("#" + this.id).css("visibility","hidden");
            this.div.css("display","none");
            this.params = null;
            
            if (this.child !== null) {
                this.child.hide(options);
            }
        },

        /**
         * return whether or not showing layer
         * 
         * @returns {Boolean} if showing return true, otherwise false
         */
        isShowing: function() {
            //return this.div.css("visibility") !== "hidden";
            //return $("#" + this.id).css("visibility") !== "hidden";
            return $("#" + this.id).css("display") !== "none";
        },

        /**
         * setting object for external defined parameter
         * 
         * ex) menu index value to link specific homemeu
         * 
         * @param obj
         */
        setParams: function(params) {
            try { //순환참조 이슈로 stringify 에러남.
                KTW.utils.Log.printDbg("Layer#setParams() params = " + KTW.utils.Log.stringify(params));
            } catch(e) {
                KTW.utils.Log.printExec(e);
            }
            
            this.params = params;
        },

        /**
         * return parameter object
         * 
         * @returns {Object} 
         */
        getParams: function() {
            return this.params;
        },

        /**
         * set child view
         * 
         * @param child
         */
        setChild: function(child) {
            this.child = child;
        },

        /**
         * return whether or not cousume key event
         * 
         * @param key_code
         * @returns {Boolean} true if consume key event, otherwise false
         */
        handleKeyEvent: function(key_code) {
            // 만약 child view 가 존재하면 해당 view에게 key를 전달
            if (this.child !== null) {
                return this.child.handleKeyEvent(key_code);
            }
            return false;
        },

        getTypeString: function(type) {
            var type_str = "";

            for (var key in KTW.ui.Layer.TYPE) {
                if (KTW.ui.Layer.TYPE[key] === type) {
                    type_str = key;
                    break;
                }
            }
            
            return type_str;
        },

        toString: function() {
            return "[Layer: id=" + this.id  + ", type=" + this.getTypeString(this.type) + 
                ", priority=" + this.priority + ", visibility = " + this.isShowing() +
                ", linkage = " + this.linkage + "]";
        }
    };

    /**
     * Layer Priority define
     * BACKGROUND type : 1 - video 영역 상위 - graphic 영역 최하위에 그려지도록 함
     * TRIGGER type : 100
     * NORMAL type : 200 - 고정 값으로 stack 구조와 동일하게 layering 됨  
     * POPUP type : 300 - popup이 중첩적으로 뜨는 경우 자동으로 증가함
     *
     * popup type의 경우 system 및 purchase를 별도로 구분
     */
    Object.defineProperty(KTW.ui.Layer, "PRIORITY", {
        value: {
            /**
             * BACKGROUND type : 1 - video 영역 상위 - graphic 영역 최하위에 그려지도록 함
             */
            BACKGROUND : 1,

            /**
             * TRIGGER type : 100
             */
            TRIGGER: 100,

            /**
             * NORMAL type : 200
             */
            NORMAL : 200,

            /**
             * POPUP type : 300
             */
            POPUP : 300,

            /**
             * 예약알림 popup 전용 priority
             * 예약 알림 popup의 경우 appstore 등의 unbound app 보다
             * 상위에 노출되어야 하기 때문에 key priority 를 별도롤 처리한다.
             * 해당 priority로 처리해서 key priority가 조정될 수 있도록 한다.
             * 실제 key priority 처리는 LayerManager에서 처리한다.
             */
            REMIND_POPUP : 320,

            /**
             * 구매 popup 전용 priority
             * TV_VIEWING 상태에서 구매 또는 특정 key를 처리해야 하는 popup의 경우
             * 해당 priority로 처리해서 key priority가 조정될 수 있도록 한다.
             * 실제 key priority 처리는 LayerManager에서 처리한다.
             */
            PURCHASE_POPUP : 350,

            /**
             * 자동대기 및 자동전원 등의 system popup 처리용 priority
             * 해당 popup이 뜬 상태에서는 모든 key를 소진하도록 해야 하기 때문에
             * 실제 key priority가 99로 상당히 높게 처리된다.
             * 실제 key priority 처리는 LayerManager에서 처리한다.
             */
            SYSTEM_POPUP : 390
        },
        writable: false,
        configurable: false
    });

    /**
     * Layer Type define
     */
    Object.defineProperty(KTW.ui.Layer, "TYPE", {
        value:  {
            /**
             * BACKGROUND layer
             *  - 항상 UI 중 최하위에 존재
             *  - linkage 값에 상관없이 다른 layer 와 항상 공존
             *  - key event 처리 안함
             *  - 명시적으로만 stack 에서 제거 (can't history back)
             */ 
            BACKGROUND : 0,

            /**
             * TRIGGER layer
             *  - linkage 값에 따라 NORMAL 과 공존 가능
             *  - BACKGROUND, TRIGGER, POPUP 과는 항상 공존 가능
             *  - 명시적으로만 stack 에서 제거 (can't history back)
             */
            TRIGGER: 1,

            /**
             * NORMAL layer
             *  - 전체적인 UI를 덮는 기본적인 layer
             *  - linkage 값에 따라 TRIGGER 와 공존 가능
             */
            NORMAL : 2,

            /**
             * POPUP layer
             *  - 항상 UI 중 최상위에 위치함
             *  - POPUP 을 제외한 하위 layer 와 항상 공존
             *  - linkage 값에 따라 다른 POPUP 과 공존 가능
             */
            POPUP : 3
        },
        writable: false,
        configurable: false
    });

    /**
     * Layer Type define
     */
    Object.defineProperty(KTW.ui.Layer, "ID", {
        value:  {
            HOME_MENU: "HomeMenuLayer",
            CHANNEL_GUIDE: "ChannelGuideLayer",
            MINI_EPG: "MiniEpgLayer",
            IFRAME: "IframeLayer",
            AUDIO_IFRAME: "AudioIframeLayer",
            OTS_STITCHING: "StitchingLayer",
            CHANNEL_PREVIEW: "ChannelPreViewLayer",

            LOADING: "LoadingLayer",
            AUDIO_CHANNEL_FULL_EPG : "AudioChannelFullEpg",

            //FAMILY_HOME_MAIN: "MyHomeMain",                     // 우리집맞춤TV 메인
            //FAMILY_HOME_BUY_LIST: "MyHomeBuyList",              // 구매목록
            //FAMILY_HOME_CLOUD: "MyHomeCloud",                   // 마이클라우드 DVD
            //FAMILY_HOME_FOR_YOU: "MyHomeForYou",                // 당신을 위한 맞춤 추천
            //FAMILY_HOME_HELP: "MyHomeHelp",                     // 올레 100배 즐기기
            //FAMILY_HOME_MAILBOX: "MyHomeMailbox",               // 우편함
            //FAMILY_HOME_MY_PLAY_LIST: "MyHomeMyPlayList",       // 마이플레이리스트
            //FAMILY_HOME_MY_PRODUCT: "MyHOmeMyProduct",          // 가입정보
            //FAMILY_HOME_RECENTLY_LIST: "MyHomeRecentlyList",    // 최근시청목록
            //FAMILY_HOME_WISH_LIST: "FAMILY_HOME_WISH_LIST",     // 찜한목록
            //
            //SUBHOME_MAIN: "SubHomeMain",                    // 서브홈 레이어
            //
            //PAYMENT_VOD_POPUP: "PaymentVODPopup",           // VOD 구매 팝업
            //PAYMENT_CORNER_POPUP: "PaymentCornerPopup",     // 월정액 가입 팝업
            //
            //SEARCH_MAIN: "SearchMain",      // 검색 화면
            //SEARCH_RESULT: "SearchResult",  // 검색 결과 화면

            SETTING_MAIN: "SettingMain",                            // 설정 메인 화면
            //2017.06.13 sw.nam 설정 메뉴 JUMP 시나리오 관련 LayerID 추가
            SETTING_KIDS_CARE: "SettingKidsCare",                   // 자녀안심설정
            SETTING_CHANNEL: "SettingChannel",                      // 채널&VOD설정
            SETTING_SYSTEM: "SettingSystem",                        // 시스템설정

   //       SETTING_SPEAKER_VOLUME: "SettingSpeakerVolume",                 // 음성가이드 볼륨 설정
   //       SETTING_SPEECH_RECOGNITION: "SettingSpeechRecognition",   // 음성가이드
            SETTING_VOICE_GUIDE: "SettingSpeechRecognition",        // 음성가이드
            SETTING_USB_CONNECT: "SettingUsbConnect",               // 이동식 디스크 연결

            SETTING_SUBTITLE: "SettingSubtitle",                    // 자막 방송
            SETTING_START_CHANNEL: "SettingStartChannel",           // 시작 채널
            SETTING_SOUND_TYPE: "SettingSoundType",                 // 사운드
            SETTING_SHOW_OPTION: "SettingShowOption",               // VOD 보기 옵션 (메뉴명 변경)
            SETTING_SERIES_AUTO_PLAY: "SettingSeriesAutoPlay",      // 시리즈 몰아보기
            SETTING_SCREEN_RATIO: "SettingScreenRatio",             // 화면 비율
            SETTING_SCREEN_NARRATION: "SettingScreenNarration",     // 화면 해설 방송
            SETTING_RESOLUTION: "SettingResolution",                // 해상도
            SETTING_PASSWORD: "SettingPassword",                    // 비밀번호 변경/초기화
            SETTING_LIMIT_WATCH: "SettingLimitWatch",               // 시청 시간 제한
            SETTING_BLOCKED_CHANNEL: "SettingLimitChannel",         // 시청 채널 제한 (value 변경 필요함)
            SETTING_PARENTAL_RATING: "SettingParentalRating",       // 시청 연령 제한 (value 변경 필요함)
            SETTING_MINIGUIDE_DISPLAY_TIME: "SettingInfoShowTimer", // 채널 정보 표시 시간 (value 변경 필요함)
            SETTING_SKIPPED_CHANNEL: "SettingHideChannel",          // 채널 숨김 (value 변경 필요함)
            SETTING_HDMI_SYNCHRONIZE: "SettingHdmiSynchronize",     // HDMI 전원 동기화
            SETTING_FAVORITE_CHANNEL: "SettingFavorChannel",        // 선호 채 널 (value 변경 필요함)
            SETTING_DEFAULT_LANGUAGE: "SettingDefaultLanguage",     // 기본 언어
            SETTING_BLUETOOTH_CONNECT: "SettingBluetoothConnect",   // 블루투스 장치 연결
            SETTING_AUTO_STANDBY: "SettingAutoStandbyMode",         // 자동 대기 모드 (value 변경 필요함)
            SETTING_AUTO_POWER: "SettingAutoPower",                 // 자동 전원 설정
            SETTING_SYSTEM_INITIALIZATION: "SettingAllReset",       // 전체 초기화 (value 변경 필요함)
            SETTING_ALERT_MESSAGE: "SettingAlertMessage",           // 알림 메시지
            SETTING_REPEATER: "SettingRepeater",                    // 중계기
            SETTING_JUMPING_CHANNEL: "SettingJumpingChannel",       // 건너뛰기 채널,
            SETTING_RECEIVE_CHECKING: "SettingReceiveChecking",     // 수신품질 측정
            SETTING_SYSTEM_INFO: "SettingSystemInfo",               // 시스템 정보
            SETTING_AUDIO_OUTPUT: "SettingAudioOutput",             // 오디오 출력설정 (Only for GIGA Genie)
            // 설정 - 관리자 모드
            SETTING_LNB: "SettingLNB",                              // LNB 설정
            SETTING_EMERGENCY_ALERT: "SettingEmergencyAlert",       // 긴급 메시지
            SETTING_CHANNEL_INFO_OUTPUT: "SettingChannelInfoOutput",//채널정보 출력 설정
            SETTING_CLOSED_CAPTION:     "SettingClosedCaption",     // Closed 캡션 설정
            SETTING_CAS_INFO:           "SettingCasInfo",           // CAS 정보
            SETTING_BEACON:             "SettingBeacon",            // 비콘 설정

            SETTING_BLUETOOTH_SOUND_SETTING_POPUP: "SettingBluetoothSoundSettingPopup", // 블루투스 혼자듣기 & 같이듣기 선택 팝업
            SETTING_BLUETOOTH_DEVICE_SEARCH_POPUP: "SettingBluetoothDeviceSearchPopup", // 블루투스 검색 팝업
            SETTING_BT_DEVICE_DELETE_POPUP: "BtDeviceDeletePopup",     // 블루투스 장치 삭제 팝업
            SETTING_BLUETOOTH_CONNECTION_FAIL_POPUP: "SettingBluetoothConnectionFailPopup", // 블루투스 연결 실패 팝업(검색 후 연결 시)
            SETTING_BT_FAIRED_DEVICE_CONNECTION_FAIL_POPUP: "SettingBTFairedDeviceConnectionFailPopup", // 블루투스 연결 실패 팝업(검색 후 연결 시)
            SETTING_BT_DEVICE_LIMIT_POPUP: "SettingBluetoothSearchingFailPopup", // 블루투스 검색 실패 팝업(등록갯수 초과)
            SETTING_CHANGE_PASSWORD_POPUP: "ChangePasswordPopup",   // 비밀번호 변경 파업
            SETTING_CHANGE_BUY_PASSWORD_POPUP: "ChangeBuyPasswordPopup", // 구매 비밀번호 변경 팝업
            SETTING_RESET_PASSWORD_POPUP: "ResetPasswordPopup", // 비밀번호 초기화 팝업
            SETTING_AUTH_CHECK_PASSWORD_POPUP: "AuthCheckPasswordPopup", // 성인 인증 팝업
            SETTING_HDS_ADMIN_CHECK_PASSWORD_POPUP: "HDSAdminCheckPasswordPopup", // 관리자 비밀번호 팝업

            SETTING_LNB_POPUP:  "SetLNBPopup",                          //LNB 팝업
            SETTING_MOBILE_BRANDS_POPUP: "SettingMobileBrandsPopup",
            SETTING_PASSWORD_INIT_COMPLETE_POPUP: "SettingPasswordInitCompletePopup",

            SETTING_WRONG_PASSWORD_POPUP: "WrongPasswordPopup",          // 비밀번호 오류 팝업
            SETTING_ALL_RESET_SYSTEM_POPUP:  "AllResettingSystemPopup",  // 전체시스템 초기화 팝업
            SETTING_ALL_RESET_CHECKING_POPUP: "AllResetCheckingPopup",   // 전체 초기화중 알림 팝업
            SETTING_ALL_RESET_REBOOTING_POPUP: "AllResetRebootingPopup", // 전체 초기화 완료 팝업
            SETTING_SOUND_AUDIO_POPUP: "AlertSoundAudioPopup",           // 돌비 사운드 팝업 & 기가지니 오디오 출력 안내 팝업
            SETTING_UHD_CHECKING_POPUP : "UHDCheckingPopup",     // UHD 체크 팝업
            SETTING_RESOLUTION_CHANGE_POPUP: "ResolutionChangePopup", // 해상도 변경 팝업
            SETTING_RESOLUTION_ALERT_POPUP: "setting_resolutionAlertPopup",                  // 해상도 변경 알림 팝업
            SETTING_SYSTEMINFO_SYSTEM_UPGRAGE_POPUP: "setting_systemInfoSystemUpgradePopup", // 시스템 정보 시스템 업그레이드 팝업
            SETTING_SYSTEMINFO_CHANNEL_INFO_UPDATE_POPUP: "setting_systemInfoChannelInfoUpdatePopup", //시스템 정보 채널정보 업데이트 팝업

            SETTING_RECEIVE_CHECKING_POPUP : "setting_receiveCheckingPopup", // 수신 품질 측정 팝업


            VOD_PLAYER_LAYER: "VodPlayerLayer",                     // VOD 플레이어
            VOD_DETAIL_SINGLE_LAYER: "VodDetailSingleLayer",        // VOD 단편 상세 화면
            VOD_DETAIL_SERIES_LAYER: "VodDetailSeriesLayer",        // VOD 시리즈 상세 화면
            VOD_DETAIL_PACKAGE_LAYER: "VodDetailPackageLayer",      // VOD 패키지 상세 화면
            VOD_SELECT_CORNER_POPUP: "VodSelectCornerPopup",        // VOD 몰아보기 팝업
            VOD_END_POPUP: "VodEndPopup",                           // VOD 종료 팝업
            VOD_CONTINUE_POPUP: "VodContinuePopup",                  // VOD 이어보기 팝업


            TV_PAY_DUMMY_PUPUP: "TVPayDummyLayer"                  // TV 페이 모듈을 위한 더미 팝업
        },
        writable: false,
        configurable: false
    });
})();

