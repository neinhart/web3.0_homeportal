
/**
 * create the root namespace and making sure we're not overwriting it
 * KTW === KT TV Web 3.0
 */
var KTW = KTW || {};

/**
 * html title
 */
KTW.TITLE = "KT TV Web 3.0";

/**
 * version 정보는 major(1자리).minor(2자리).revision(3자리) 형태로 정의한다.
 * ie) 1.00.000
 * 
 * 보통 개발팀 내부 릴리즈 시에는 revision 값을 변경한다.
 * 내부 QA 릴리즈 시에는 minior 값을 변경한다.
 * 최초 정식 릴리즈 시에는 major 값을 변경한다.
 * 
 * 해당 값은 직접 수정되는 것이 아니고 version.properties 파일에서 정의된
 * version 값이 반영된다.
 */
KTW.UI_VERSION = "0.00.001";

/**
 * 모듈 버전 관리를 위한 framework 버전
 */
KTW.FRAMEWORK_VERSION = "0.0.0";

/**
 * update 날짜 (YYYY.MM.DD)
 */
KTW.UI_DATE = "2016.04.19";

/**
 * version 이 정리가 될때까지, build zip 파일끼리 비교 가능하도록 build 시간 추가
 */
KTW.UI_UPDATE_TIME = "00:11:22";

/**
 * app이 실행하는 PLATFORM 환경 변수
 * 1. PC : browser (chrome, ie)
 * 2. STB : webMW
 */
KTW.PLATFORM_EXE = window.oipfObjectFactory ? "STB" : "PC";


/**
 * app release 모드 환경 변수
 * 1. LIVE : LIVE 용
 * 2. BMT : BMT 테스트용
 * 3. TEST : Testbed 테스트용
 * 4. LOCAL : local 테스트용
 */
KTW.RELEASE_MODE = "BMT";

KTW.PERFOMANCE_TEST = false;

/**
 * DEMO 형상 지원 여부
 * miniSTB 때문에 일단 추가됨
 */
KTW.SUPPORT_DEMO = false;
KTW.DEMO = {
    MSTB : false,
    NFX : false
}

/**
 * log 설정
 */
KTW.LOG_OUTPUT = true;
/**
 * DBG : 0,
 * INFO : 1,
 * WARN : 2,
 * ERR : 3,
 */
KTW.LOG_LEVEL = 0;
/**
 * GUI 검증 등과 같이 테스트 코드로 동작하는 경우
 */
KTW.TEST_CODE = false;
KTW.QUICK_VOD_SUPPORT_TEST_CODE = false;
KTW.QUICK_VOD_TEST_CODE = false;

/**
 * pip 기능 활성화 여부 설정
 */
KTW.ENABLE_PIP = true;

/**
 * 통계 테스트 flag (개발용 테스트 flag)
 */
KTW.USER_LOG_TEST = false;