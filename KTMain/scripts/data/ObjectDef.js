/**
 * [dj.son] 2016.08.10 DA 정의서에 따라 VItem, VInternalItem, VCategory, VMenu, VInteractive 필드 수정
 *          VItem ITEM TYPE 정리
 */

function VItem(itemType, id, name, parent, newHot, prInfo, wonYn, templateNo, templateType, templateLoc, templateImg, templateName,
               menuImgUrl, platformGubun, imgUrl, webMenuImgUrl, notice) {
	this.itemType = itemType;
	this.id = id;
	this.name = this.decodeData(name);
    this.parent = parent;
    this.newHot = newHot;
	this.prInfo = prInfo;
	this.wonYn = wonYn;
    this.templateNo = templateNo;
    this.templateType = templateType;
    this.templateLoc = templateLoc;
    this.templateImg = templateImg;
    this.templateName = templateName;
    this.menuImgUrl = menuImgUrl;
    this.platformGubun = platformGubun;
	this.imgUrl = imgUrl;
	//this.webMenuImgUrl = webMenuImgUrl;
    this.notice = this.decodeData(notice);

    if (webMenuImgUrl !== null && webMenuImgUrl !== undefined && webMenuImgUrl !== "") {
        this.menuImgUrl = webMenuImgUrl;
    }
}
/**
 * [dj.son] 기존 R6 코드에서도 안쓰이지만 일단 남겨둠
 * TODO KTW.DATA.MENU.ITEM 에 같은 내용이 정의되어 있고, 아래 정의는 쓰이질 않음.
 */
VItem.TYPE = {
    MENU: -1,
    CATEGORY: 0,
    SERIES: 1,
    CONTENT: 2,
    INTERACTIVE: 3,
    SERIESCONTENT: 4,
    INTERACTIVE_UNICASTING: 7,
    WEBVIEW: 8
};
VItem.prototype.setParent = function(parent) {
	this.parent = parent;
};
VItem.prototype.decodeData = function(str) {
    var result = str;

    try {
        if (str) {
            result = decodeURIComponent(str);
        }
    }
    catch (e) {
        result = str;
    }

    return result;

};


function VInternalItem(itemType, id, name, parent, newHot, prInfo, wonYn, templateNo, templateType, templateLoc, templateImg, templateName,
                       menuImgUrl, platformGubun, imgUrl, webMenuImgUrl, notice,
                       itemCount, children) {
	VItem.call(this, itemType, id, name, parent, newHot, prInfo, wonYn, templateNo, templateType, templateLoc, templateImg, templateName,
                    menuImgUrl, platformGubun, imgUrl, webMenuImgUrl, notice);
	this.itemCount = itemCount;
	this.children = children;
}
VInternalItem.prototype = new VItem();
VInternalItem.prototype.constructor = VInternalItem;
VInternalItem.prototype.setChildren = function(children) {
	this.children = children;
}


function VCategory(itemType, id, name, parent, newHot, prInfo, wonYn, templateNo, templateType, templateLoc, templateImg, templateName,
                   menuImgUrl, platformGubun, imgUrl, webMenuImgUrl, notice, catType, isHd, isDolby, is51Ch, buyType, connerId, sortGb, itemCount,
                   listType, hybridYn, hsImgUrl, hsTargetType, hsTargetId, hsLocator, hsLocator2, hsKTCasLocator, englishItemName, webItemCount, webHsImgUrl,
                   w3HsImgUrl, itemSubText, englishItemSubText, previewImgUrl1, previewImgUrl2, previewImgUrl3, w3ListImgUrl, posterListType, bizBuyType, w3MenuImgType) {
	
	VInternalItem.call(this, itemType, id, name, parent, newHot, prInfo, wonYn, templateNo, templateType, templateLoc, templateImg, templateName,
                            menuImgUrl, platformGubun, imgUrl, webMenuImgUrl, notice, itemCount);
			
	this.catType = catType;
    this.isHd = isHd;
    this.isDolby = isDolby;
    this.is51Ch = is51Ch;
    this.buyType = buyType;
    this.connerId = connerId;
    this.sortGb = sortGb;
    this.listType = listType;
    this.hybridYn = hybridYn;
    this.hsImgUrl = hsImgUrl;
    this.hsTargetType = hsTargetType;
    this.hsTargetId = hsTargetId;
    this.hsLocator = hsLocator;
    this.hsLocator2 = hsLocator2;
    this.hsKTCasLocator = hsKTCasLocator;
    this.englishItemName = this.decodeData(englishItemName);
    //this.webItemCount = webItemCount;
    this.webHsImgUrl = webHsImgUrl;
    this.w3HsImgUrl = w3HsImgUrl;
    this.itemSubText = this.decodeData(itemSubText);
    this.englishItemSubText = this.decodeData(englishItemSubText);
    this.previewImgUrl1 = previewImgUrl1;
    this.previewImgUrl2 = previewImgUrl2;
    this.previewImgUrl3 = previewImgUrl3;
    this.w3ListImgUrl = w3ListImgUrl;
    this.posterListType = posterListType;

    /**
     * [dj.son] Biz 상품인 경우 bizBuyType 이 존재하고, Biz 상품이 아닌 경우 bizBuyType 이 아예 필드에서 없음;;;
     * - 어차피 menu.js 는 비즈 상품인지 아닌지 구분한 다음 다운받으므로, 일단 여기서 구분하기로함
     */
    if (KTW.CONSTANT.IS_BIZ) {
        this.bizBuyType = bizBuyType;
        this.w3MenuImgType = w3MenuImgType;
    }
    else {
        this.w3MenuImgType = bizBuyType;
    }

	
	if (webItemCount != null && webItemCount != undefined && webItemCount != "") {
		this.itemCount = webItemCount;
	}
}
VCategory.prototype = new VInternalItem();
VCategory.prototype.constructor = VCategory;


function VMenu(id, name, parent, itemCount, children, englishItemName, itemSubText, englishItemSubText) {
			
	VInternalItem.call(this, VItem.TYPE.MENU, id, name, parent, null, "01", "N", null, null, null, null, null,
                            null, "W", null, null, null, itemCount, children);
	
	this.englishItemName = englishItemName;
    this.itemSubText = "";
    this.englishItemSubText = "";

    if (itemSubText != null && itemSubText != undefined) {
        this.itemSubText = itemSubText;
    }
    if (englishItemSubText != null && englishItemSubText != undefined) {
        this.englishItemSubText = englishItemSubText;
    }
}
VMenu.prototype = new VInternalItem();
VMenu.prototype.constructor = VMenu;


// TODO imgUrl 데이터가 2개의 필드로 중복해서 넘어와서 하나의 필드명을 imgUrl_temp 로 변경하고 사용안함.
function VInteractive(itemType, id, name, parent, newHot, prInfo, wonYn, notice, imgUrl, locator, delDate, locator2, menuImgUrl,
                      parameter, templateNo, templateType, templateLoc, templateImg, templateName, ktCasLocator, platformGubun,
                      appImgUrl, webMenuImgUrl, w3ImgUrl, itemSubText, englishItemName) {

	VItem.call(this, itemType, id, name, parent, newHot, prInfo, wonYn, templateNo, templateType, templateLoc, templateImg, templateName,
                    menuImgUrl, platformGubun, imgUrl, webMenuImgUrl, notice);

    this.locator = locator;
    this.delDate = delDate;
    this.locator2 = locator2;
    this.parameter = parameter;
    this.ktCasLocator = ktCasLocator;
    this.appImgUrl = appImgUrl;
    this.w3ImgUrl = w3ImgUrl;
    this.itemSubText = itemSubText;
    // 2017.05.08 dhlee 영어 이름 추가
    this.englishItemName = englishItemName;
}
VInteractive.prototype = new VItem();
VInteractive.prototype.constructor = VInteractive;



