/**
 * log object 
 * 
 * (HOT_KEY type인 경우 사용)
 * 
 * @param type - LOG_TYPE
 * @param act - kind of action for logging 
 */
KTW.data.UserLog = function (data) {
    if (data) {
        this.LOG_TYPE = data.type;
        this.START_ACT = data.act;
        this.START_TS = data.timestamp;
    }
};

KTW.data.UserLog.prototype = {
    toJsonObject: function() {
        // string으로 변환 후 다시 parsing하는 이유는
        // undefined로 선언된 property를 제거하기 위함
        return JSON.parse(JSON.stringify(this));
    },
    
    toString: function() {
        return "UserLog:" + this.LOG_TYPE  + ", start_act=" + this.START_ACT + ", timestamp=" + this.START_TS;
    }
};

/**
 * use to collect log when it update oc data or change settings concern with menu
 * (MENU_UPDATE type 인 경우 사용)
 * 
 * @param type - log type (KTW.data.UserLog.TYPE.MENU_UPDATE)
 * @param act - update 방법 (KTW.data.UpdateLog.UPDATE 참조)
 * @param version - oc menu version
 */
KTW.data.UpdateLog = function (data) {
    KTW.data.UserLog.call(this, data);

    if (data) {
        this.MENU_VERSION = data.version;
    }
};

KTW.data.UpdateLog.prototype = new KTW.data.UserLog();
KTW.data.UpdateLog.prototype.constructor = KTW.data.UpdateLog;

KTW.data.UpdateLog.prototype.toString = function() {
    return KTW.data.UserLog.prototype.toString.call(this) + ", version=" + this.MENU_VERSION;
};

/**
 * use to collect log when user enter specific menu or screen
 * (ETC_MENU, JUMP_TO type 인 경우 사용)
 * 
 * @param type - log type
 *                       1. jump 기능 - KTW.data.UserLog.TYPE.JUMP_TO
 *                       2. 마이메뉴/설정 진입 - KTW.data.UserLog.TYPE.ETC_MENU
 * @param act - 진입 방법 (KTW.data.EntryLog.JUMP.CODE 참조) 및 keyCode
 * @param item - jump data (모든 property가 존재해야 하며 공백인 경우 "" 으로 전달)
 *                         1. jump 기능
 *                            { 
 *                                jumpType : "xxxx", // (KTW.data.EntryLog.JUMP.TYPE 참조)
 *                                catId : "xxxx",
 *                                contsId : "xxxx",
 *                                locator : "",
 *                                reqCd : "",
 *                            }
 *                         2. 마이메뉴/설정 진입
 *                            {
 *                                id : "xxx",
 *                                name : "xxx",
 *                            }
 */
KTW.data.EntryLog = function (data) {
    KTW.data.UserLog.call(this, data);

    if (data) {
        // JUMP_TO type인 경우
        // act 값은 무엇에 의해 jump 했는지 구분
        // jumpType 값은 jump의 결과화면에 따른 구분
        this.JUMP_TYPE = data.jumpType !== undefined ? data.jumpType : undefined;
        this.CATEGORY_ID = data.catId !== undefined ? data.catId : undefined;
        this.CONTENT_ID = data.contsId !== undefined ? data.contsId : undefined;
        this.LOCATOR = data.locator !== undefined ? data.locator : undefined;
        this.REQ_PATH_CD = data.reqCd !== undefined ? data.reqCd : undefined;

        // ETC_MENU type인 경우
        this.MENU_ID = data.menuId !== undefined ? data.menuId : undefined;
        this.MENU_NAME = data.menuName !== undefined ? data.menuName : undefined;

        //장르별 멀티채널의 경우 CH_IDX (인기채널 순위 정보) 포함
        this.CH_IDX = data.chIdx !== undefined ? data.chIdx : undefined
    }
};

KTW.data.EntryLog.prototype = new KTW.data.UserLog();
KTW.data.EntryLog.prototype.constructor = KTW.data.EntryLog;

KTW.data.EntryLog.prototype.toString = function() {
    var to_string = KTW.data.UserLog.prototype.toString.call(this);

    if (this.JUMP_TYPE !== undefined) to_string += ", jump_type=" + this.JUMP_TYPE;
    if (this.CATEGORY_ID !== undefined) to_string += ", cat_id=" + this.CATEGORY_ID;
    if (this.CONTENT_ID !== undefined) to_string += ", conts_id=" + this.CONTENT_ID;
    if (this.LOCATOR !== undefined) to_string += ", locator=" + this.LOCATOR;
    if (this.REQ_PATH_CD !== undefined) to_string += ", reqCd=" + this.REQ_PATH_CD;
    if (this.MENU_ID !== undefined) to_string += ", menu_id=" + this.MENU_ID;
    if (this.MENU_NAME !== undefined) to_string += ", menu_name=" + this.MENU_NAME;
    if (this.CH_IDX !== undefined) to_string += ", ch_idx=" + this.CH_IDX;
    
    return to_string;
};

/**
 * use to collect log when user navigate home menu or vod detail screen
 * (HOME_MENU, VOD_DETAIL type인 경우 사용)
 * 
 * @param type - log type
 *                       1. 홈메뉴 navigation - KTW.data.UserLog.TYPE.HOME_MENU
 *                       2. VOD 상세화면 navigation - KTW.data.UserLog.TYPE.VOD_DETAIL
 * @param act - 진입 방법 (상하좌우 및 확인, 이전 keyCode)
 * @param item - VOD data (모든 property가 존재해야 하며 공백인 경우 "" 으로 전달)
 *                         1. 홈메뉴 navigation
 *                            { 
 *                                catId : "xxxx",
 *                                catName : "xxxx",
 *                                contsId : "xxxx",
 *                                contsName : "xxxx",
 *                            }
 *                         2. VOD 상세화면 navigation
 *                            {
 *                                catId : "xxxx",
 *                                contsId : "xxxx",
 *                                contsName : "xxxx",
 *                                locator : "xxxx",
 *                                buttonId : "xxx", // (KTW.data.NavLog.BTN_ID 참조)
 *                                buttonValue : "xxx",
 *                            }
 */
KTW.data.NavLog = function (data) {
    KTW.data.EntryLog.call(this, data);

    if (data) {
        this.CATEGORY_NAME = data.catName !== undefined ? data.catName : undefined;
        this.CONTENT_NAME = data.contsName !== undefined ? data.contsName : undefined;

        // VOD 상세화면
        this.BUTTON_ID = data.buttonId !== undefined ? data.buttonId : undefined;
        this.BUTTON_VALUE = data.buttonValue !== undefined ? data.buttonValue : undefined;
    }
};

KTW.data.NavLog.prototype = new KTW.data.EntryLog();
KTW.data.NavLog.prototype.constructor = KTW.data.NavLog;

KTW.data.NavLog.prototype.toString = function() {
    var to_string = KTW.data.EntryLog.prototype.toString.call(this);

    if (this.CATEGORY_NAME !== undefined) to_string += ", cat_name=" + this.CATEGORY_NAME;
    if (this.CONTENT_NAME !== undefined) to_string += ", conts_name=" + this.CONTENT_NAME;
    if (this.BUTTON_ID !== undefined) to_string += ", button_id=" + this.BUTTON_ID;
    if (this.BUTTON_VALUE !== undefined) to_string += ", button_value=" + this.BUTTON_VALUE;
    
    return to_string;
};

/**
 * log type 
 */
Object.defineProperty(KTW.data.UserLog, "TYPE", {
    value:  {
        /* RCU hot key */
        HOT_KEY : "HOT_KEY",
        /* 홈메뉴 navigation */
        HOME_MENU : "HOME_MENU",
        /* VOD 상세화면 navigation */
        VOD_DETAIL : "VOD_DETAIL",
        /* 특정 화면 jump (홈샷, promo trigger 등) */
        JUMP_TO : "JUMP_TO",
        /* 기타 메뉴 진입 */
        ETC_MENU : "ETC_MENU",
        /* menu data update */
        MENU_UPDATE : "MENU_UPDATE",

        /* Module navigation*/
        //2017.06.16 sw.nam - 모듈별 navigation type 구분값 추가 (규격서 기반)
        SH_MENU : "SH_MENU", // subHome
        FH_MENU : "FH_MENU", // 우리집 맞춤 TV
        KIDS_MENU: "KIDS_MENU" // KIDS 모듈

    },
    writable: false,
    configurable: false
});

/**
 * log key code
 */
Object.defineProperty(KTW.data.UserLog, "KEY_CODE", {
    value:  {
        HOTKEY_HOME : "HOTKEY_HOME",
        HOTKEY_MYMENU : "HOTKEY_MYMENU",
        HOTKEY_SEARCH : "HOTKEY_SEARCH",
        HOTKEY_EPG : "HOTKEY_EPG",
        RIGHT : "RIGHT",
        LEFT : "LEFT",
        UP : "UP",
        DOWN : "DOWN",
        OK : "OK",
        BACK : "BACK"
    },
    writable: false,
    configurable: false
});

/**
 * MENU jump type 및 code
 */
Object.defineProperty(KTW.data.EntryLog, "JUMP", {
    value:  {
        TYPE : {
            /* 카테고리 */
            CATEGORY : "CATEGORY_JUMP",
            /* VOD 상세화면 */
            VOD_DETAIL : "DETAIL_JUMP",
            /* 양방향 */
            DATA : "INTERACTIVE_JUMP",
            /* 검색 */
            SEARCH : "SEARCH_JUMP",
            /* 마이메뉴, 설정, 채널가이드 */
            ETC : "ETC_JUMP",
            /* 2채널 동시시청, VOD-채널 동시시청 */
            PIP : "PIP_JUMP",
            /* 편성표 */
            EPG : "EPG_JUMP"
        },
        CODE : {
            /* 홈샷 */
            HOMESHOT : "JUMP_CATE_HOMESHOT",
            /* promo trigger */
            PROMO_CH : "JUMP_PROMO_CHANNEL",
            /* 공지사항 */
            NOTICE : "JUMP_NOTICE",
            /* 검색 */
            SEARCH : "JUMP_SEARCH",
            /* postMessage에 의해 카테고리 및 상세화면 등으로 이동하는 경우
             * 1. hp_showCategory
             * 2. hp_showContentDetail
             * 3. hp_showSeriesDetail
             */
            POST_MSG : "JUMP_POST_MSG",
            /*
             * 연관메뉴
             * - 미니가이드 연관콘텐츠
             * - 북마크 보관함
             * - 홈위젯
             */
            CONTEXT : "JUMP_CONTEXT",
            /*
             * 연관콘텐츠
             */
            RELATED : "JUMP_RELATED_CONTENTS",
            /**
             * 홈메뉴 좌측 추천 컨텐츠
             */
            MAIN_RECOMMEND : "JUMP_MAIN_RECOMMEND",
            /**
             * subhome 화면 추천 컨텐츠
             */
            SUBHOME_RECOMMEND: "JUMP_SUBHOME_RECOMMEND",
            SUBHOME: "JUMP_SUBHOME"
        }
   },
    writable: false,
    configurable: false
});

/**
 * MENU update type
 */
Object.defineProperty(KTW.data.UpdateLog, "UPDATE", {
    value:  {
        MENU : {
            /* oc update */
            OC : "MENU_UPDATE_OC",
            /* 성인메뉴 노출 설정 변경 */
            ADULT : "MENU_UPDATE_ADULT",
            /* cug update */
            CUG : "MENU_UPDATE_CUG"
        }
   },
    writable: false,
    configurable: false
});